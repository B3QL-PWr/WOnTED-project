import itertools
from typing import List, Tuple

import fastText
import numpy as np


class WordComparer:

    def __init__(self, similarity_threshold=0.8, min_word_length=4):
        self.ft = fastText.load_model('model.bin')
        self.words = None
        self.vecs = None
        self.threshold = similarity_threshold
        self.min_length = min_word_length

    @staticmethod
    def _similarity(v1: str, v2: str) -> float:
        cos_sim = np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))
        return cos_sim

    def _convert2vec(self, words: List[str]):
        self.words = [word for word in words if len(word) > self.min_length]
        self.vecs = [self.ft.get_word_vector(word) for word in words]

    def _compare_all(self) -> List[Tuple[str, str]]:
        similar = []
        for a, b in itertools.combinations(range(len(self.words)), 2):
            if self._similarity(self.vecs[a], self.vecs[b]) >= self.threshold:
                similar.append((self.words[a], self.words[b]))
        return similar

    def get_similar(self, words: List[str]) -> List[Tuple[str, str]]:
        self._convert2vec(words)
        return self._compare_all()


if __name__ == '__main__':
    text = """Litwo, Ojczyzno moja! ty jesteś jak zdrowie;
            Ile cię trzeba cenić, ten tylko się dowie,
            Kto cię stracił. Dziś piękność twą w całej ozdobie
            Widzę i opisuję, bo tęsknię po tobie.
            
            Panno święta, co Jasnej bronisz Częstochowy
            I w Ostrej świecisz Bramie! Ty, co gród zamkowy
            Nowogródzki ochraniasz z jego wiernym ludem!
            Jak mnie dziecko do zdrowia powróciłaś cudem
            (— Gdy od płaczącej matki, pod Twoją opiekę
            Ofiarowany martwą podniosłem powiekę;
            I zaraz mogłem pieszo, do Twych świątyń progu
            Iść za wrócone życie podziękować Bogu —)
            Tak nas powrócisz cudem na Ojczyzny łono!...
            Tymczasem, przenoś moją duszę utęsknioną
            Do tych pagórków leśnych, do tych łąk zielonych,
            Szeroko nad błękitnym Niemnem rozciągnionych;
            Do tych pól malowanych zbożem rozmaitem,
            Wyzłacanych pszenicą, posrebrzanych żytem;
            Gdzie bursztynowy świerzop, gryka jak śnieg biała,
            Gdzie panieńskim rumieńcem dzięcielina pała,
            A wszystko przepasane jakby wstęgą, miedzą
            Zieloną, na niej zrzadka ciche grusze siedzą."""
    words = list(filter(None, [''.join(filter(str.isalpha, lem)).lower() for lem in text.split(' ')]))
    wc = WordComparer()
    wc.get_similar(words)
