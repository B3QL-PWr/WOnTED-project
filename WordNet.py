import requests
import json
from tqdm import tqdm
from multiprocessing import Pool


def getall(word, whichelements=[]): #connect to API and get relations which are in whichelements
    ref = {}
    if len(whichelements)==0:
        whichelements=['hiponimia','meronimia/element taksonomiczny','meronimia/część',
                       'meronimia/element kolekcji','fuzzynimia_synsetów','egzemplarz',
                       'bliskoznaczność','cecha_definicyjna','potencjalny_odpowiednik/pot_odp_plWN-PWN',
                       'holonimia/część']
    # Base API URL.
    BASE_URL = "http://ws.clarin-pl.eu/lexrest/lex"
    data = {
        "task": "all",
        "lexeme": word,
        "tool": "plwordnet",
    }
    lista=[]
    response = requests.post(url=BASE_URL, json=data)
    jdata=json.loads(response.text)
    for word in jdata['results']['synsets']:
        for key in word['related'].keys():
            if key=="hiponimia" and key in whichelements: 
                stringlist = word['related']['hiponimia']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="hiperonimia" and key in whichelements: 
                stringlist = word['related']['hiperonimia']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="meronimia/element taksonomiczny" and key in whichelements: 
                stringlist = word['related']['meronimia/element taksonomiczny']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="meronimia/część" and key in whichelements: 
                stringlist = word['related']['meronimia/część']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="meronimia/element kolekcji" and key in whichelements: 
                stringlist = word['related']['meronimia/element kolekcji']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="fuzzynimia_synsetów" and key in whichelements: 
                stringlist = word['related']['fuzzynimia_synsetów']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="egzemplarz" and key in whichelements: 
                stringlist = word['related']['egzemplarz']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="bliskoznaczność" and key in whichelements: 
                stringlist = word['related']['bliskoznaczność']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="cecha_definicyjna" and key in whichelements: 
                stringlist = word['related']['cecha_definicyjna']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="potencjalny_odpowiednik/pot_odp_plWN-PWN" and key in whichelements: 
                stringlist = word['related']['potencjalny_odpowiednik/pot_odp_plWN-PWN']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
            if key=="holonimia/część" and key in whichelements: 
                stringlist = word['related']['holonimia/część']
                for i in stringlist:
                    string=str(i)
                    if key in ref:
                        ref[key].append(string[string.find('{')+1:string.find('.')])
                    else:
                        ref[key] = [string[string.find('{')+1:string.find('.')]]
    return ref

def getdictall(word, whichelements=[]): #return a word with its dictionary as a dictionary
    ref={}
    ref[word]=getall(word, whichelements)
    return ref

def getmultipleall(word, count, whichelements=[], workers=8): #!use this function!, get relations for words related to word 
    dictionarylist={}
    lista=[]
    dictionarylist.update(getdictall(word, whichelements))
    if(count>1):
        for onelist in tqdm(dictionarylist[word].values()):
            for oneword in onelist:
                dictionarylist.update(getdictall(oneword, whichelements))
    for k in dictionarylist.keys():
        lista.append(k)
    if (count>2):
        for i in tqdm(lista):
            for onelist in dictionarylist[i].values():
                for onelist in dictionarylist[word].values():
                    for oneword in onelist:
                        dictionarylist.update(getdictall(oneword, whichelements))
    return dictionarylist
            
