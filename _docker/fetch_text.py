import sys

from corpus_ccl.cclutils import read_ccl, sentence2str


def fetch_text(file='/tmp/2proc.xml'):
    doc = read_ccl(file)
    for par in doc.paragraphs():
        for sent in par.sentences():
            print(sentence2str(sent))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        fetch_text(sys.argv[1])
    else:
        fetch_text()
