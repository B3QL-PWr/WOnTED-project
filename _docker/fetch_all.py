import sys

from corpus_ccl.cclutils import read_ccl, base_sentence2str, sentence2str
from corpus_ccl.corpus_object_utils import get_pos
from json import dumps


def fetch_all(file='/tmp/2proc.xml'):
    doc = read_ccl(file)
    p_poss = []
    p_lemmas = []
    p_texts = []
    for par in doc.paragraphs():
        poss = []
        lemmas = []
        texts = []
        for sent in par.sentences():
            pos = []
            for token in sent.tokens():
                pos.append(get_pos(token_lexeme_or_tag=token, tagset='nkjp'))
            poss.append(pos)
            lemmas.append(base_sentence2str(sent))
            texts.append(sentence2str(sent))
        p_poss.append(poss)
        p_lemmas.append(lemmas)
        p_texts.append(texts)
    to_print = {'lemma': p_lemmas,
                'text': p_texts,
                'pos': p_poss}
    print(dumps(to_print))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        fetch_all(sys.argv[1])
    else:
        fetch_all()
