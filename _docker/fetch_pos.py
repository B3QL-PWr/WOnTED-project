import sys

from corpus_ccl.cclutils import read_ccl
from corpus_ccl.corpus_object_utils import get_pos


def fetch_pos(file='/tmp/2proc.xml'):
    doc = read_ccl(file)
    for par in doc.paragraphs():
        for sent in par.sentences():
            pos = []
            for token in sent.tokens():
                pos.append(get_pos(token_lexeme_or_tag=token, tagset='nkjp'))
            print(pos)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        fetch_pos(sys.argv[1])
    else:
        fetch_pos()
