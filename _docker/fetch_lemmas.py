import sys

from corpus_ccl.cclutils import read_ccl, base_sentence2str


def fetch_lemmas(file='/tmp/2proc.xml'):
    doc = read_ccl(file)
    for par in doc.paragraphs():
        for sent in par.sentences():
            print(base_sentence2str(sent))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        fetch_lemmas(sys.argv[1])
    else:
        fetch_lemmas()
