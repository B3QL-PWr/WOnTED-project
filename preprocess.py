import asyncio
import os
from glob import glob
from typing import List, Tuple, Dict

from pandas import read_csv
import aiohttp

BASE_URL = "http://ws.clarin-pl.eu/nlprest2/base"


def load_csv(path: str, field_name: str) -> Tuple[str, List[str]]:
    csv = read_csv(path, usecols=[field_name])
    return path.split('/')[1][:-4], csv[field_name]


def load_posts(path: str) -> Tuple[str, List[str]]:
    return load_csv(path, 'body')


def load_promoted_posts(path: str) -> Tuple[str, List[str]]:
    return load_csv(path, 'description')


async def fetch_clarin_data(client: aiohttp.ClientSession, text: str) -> Tuple[int, str]:
    url = f'{BASE_URL}/process'
    data = {
        'text': text,
        'lpmn': 'any2txt|wcrft2|liner2({\"model\": \"all\"})',
        'user': 'anonymous@example.org'
    }
    async with client.post(url, json=data) as r:
        return r.status, await r.text('utf-8')


def filter_existing(path, posts):
    paths = glob(f'{path}*.xml')
    filenames = [path.split('/')[-1] for path in paths]
    uuids = set(filename[:-4] for filename in filenames)
    return [(f'{posts[0]}_{uid}', text) for uid, text in enumerate(posts[1]) if f'{posts[0]}_{uid}' not in uuids]


async def process_posts(client: aiohttp.ClientSession, dest_path: str, posts: Tuple[str, List[str]]):
    filtered_posts = filter_existing(dest_path, posts)
    for filename, text in filtered_posts:
        print(f'Fetching {filename}')
        status, response = await fetch_clarin_data(client, text)
        if status == 200:
            with open(f'{dest_path}{filename}.xml', 'w+') as f:
                f.write(response)
        else:
            print(f'Error code {status}: {response}')


async def process_all_posts(src_path: str, dest_path: str):
    posts_files = glob(f'{src_path}mikroblog*')
    promoted_posts_files = glob(f'{src_path}/promoted*')

    async with aiohttp.ClientSession() as client:
        await asyncio.gather(*[
            process_posts(client, dest_path, load_posts(filename))
            for filename in posts_files
        ])
        await asyncio.gather(*[
            process_posts(client, dest_path, load_promoted_posts(filename))
            for filename in promoted_posts_files
        ])


async def fetch_clarin_keyword(client: aiohttp.ClientSession, word: str) -> Tuple[str, int, str]:
    result = await fetch_clarin_data(client, word)
    return (word,) + result


async def fetch_async_keywords(keywords: List[str]) -> Dict[str, str]:
    async with aiohttp.ClientSession() as client:
        tasks = [fetch_clarin_keyword(client, word) for word in keywords]
        results = {}
        for f in asyncio.as_completed(tasks):
            word, status, response = await f
            if status == 200:
                results[word] = response
            else:
                print(f'Error code {status}: {response}')
    return results


async def fetch_single_text_data(text: str):
    async with aiohttp.ClientSession() as client:
        await asyncio.gather(process_posts(client, '', ('temp', [text])))


def fetch_keywords(keywords: List[str]) -> Dict[str, str]:
    loop = asyncio.get_event_loop()
    return loop.run_until_complete(fetch_async_keywords(keywords))


def fetch_single_text(text: str):
    os.remove('temp_0.xml')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(fetch_single_text_data(text))


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(process_all_posts('data/', 'output/'))
    # print(fetch_keywords(['pies', 'kot', 'kierowanie']))
