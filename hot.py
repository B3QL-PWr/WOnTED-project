#get hot from last 12h
import wykop
import datetime
import pandas as pd
from bs4 import BeautifulSoup
app_key = 'vpKfZ7ouEo'
app_secret = 'PXUINzdJPj'
api=wykop.WykopAPI(app_key, app_secret)
dfmikroblog = pd.DataFrame(columns=['body', 'date', 'vote_count'])
mikroblog=api.get_stream_hot()
for i in range(0,25):
    dfmikroblog.loc[i]=[mikroblog[i]['body'], mikroblog[i]['date'], mikroblog[i]['vote_count']]
dfmikroblog['body'] = dfmikroblog['body'].str.replace('&quot', '')
dfmikroblog['body'] = dfmikroblog['body'].str.replace(';', '')
dfmikroblog['body'] = dfmikroblog['body'].str.replace('<br />', '')
dfmikroblog['hashtags'] = dfmikroblog.apply(lambda _: '', axis=1)
for i in range(0,len(dfmikroblog)):
    soup = BeautifulSoup(dfmikroblog['body'][i])
    dfmikroblog['body'][i]=soup.text
    if(soup.find('a')!=None):
        tags = soup.find_all('a')
        mylist=[]
        for t in tags:
            mylist.append(t.text)
        dfmikroblog['hashtags'][i]=', '.join(mylist)


now = datetime.datetime.now()
dfmikroblog.to_csv("mikroblog "+str(now.day)+"."+str(now.month)+","+str(now.hour)+":00.csv", encoding="utf-8", index = 0)
