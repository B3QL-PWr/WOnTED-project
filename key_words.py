import numpy as np
from keras.layers import Dense, Embedding, LSTM
from keras.models import Sequential
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import f1_score, precision_score, recall_score
from sklearn.svm import SVR

import wonted_dataset


class KeyWordsDetector:
    '''
    class for train model and predict keywords from datasets
    '''

    def __init__(self, owndataset=True, input_vector=11, classifier='SVR'):
        self.uid = classifier
        self.owndataset = owndataset
        if self.owndataset:
            self.wd = wonted_dataset.WontedDataset()
        if classifier == 'SVR':
            self.model = SVR(gamma='scale')
        elif classifier == 'RF':
            self.model = RandomForestRegressor(max_depth=None, random_state=0, n_estimators=100)
        elif classifier == 'LSTM':
            embed_dim = 128
            lstm_out = 196
            self.batch_size = 24

            self.model = Sequential()
            self.model.add(Embedding(2000, embed_dim, input_length=input_vector))
            self.model.add(LSTM(lstm_out, dropout=0.2, recurrent_dropout=0.2))
            self.model.add(Dense(2, activation='softmax'))
            self.model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        else:
            raise KeyError('Incorrect classifier')

    def labelToVec(self, label):
        return [[1, 0] if l == 0 else [0, 1] for l in label]

    def train_model(self):

        if self.uid == 'LSTM':
            x_train, x_test, y_train, y_test = self.wd.get_train_test(shuffled=False)
            y_train = np.asarray(self.labelToVec(y_train))
            self.model.fit(x_train[:, 1:], y_train, batch_size=self.batch_size, epochs=1, verbose=1)
            predicted = self.model.predict(x_test[:, 1:])
            predicted2 = []
            for i in range(len(predicted)):
                if predicted[i][1] > 0.3:
                    predicted2.append(1)
                else:
                    predicted2.append(0)

            print(self.uid, " precision: ", precision_score(y_test, predicted2, average='macro'))
            print(self.uid, " recall: ", recall_score(y_test, predicted2, average='macro'))
            print(self.uid, " fscore: ", f1_score(y_test, predicted2, average='macro'))

        else:
            x_train, x_test, y_train, y_test = self.wd.get_train_test(shuffled=True)
            self.model.fit(x_train[:, 1:], y_train)
            predictions = self.model.predict(x_test[:, 1:])

            for i in range(len(predictions)):
                if predictions[i] > 0.3:
                    predictions[i] = 1
                else:
                    predictions[i] = 0

            print(self.uid, " precision: ", precision_score(y_test, predictions, average='macro'))
            print(self.uid, " recall: ", recall_score(y_test, predictions, average='macro'))
            print(self.uid, " fscore: ", f1_score(y_test, predictions, average='macro'))

        return self.model

    def predict(self, name, model=None):
        if model != None:
            self.model = model
        if self.owndataset == True:
            keywords = []
            if self.uid == 'LSTM':
                predictions = self.model.predict(self.wd.get_val_dictionary()[name][:, 1:])
                mean = np.mean([p[1] for p in predictions])
                std = np.std([p[1] for p in predictions])
                for i in range(len(predictions)):
                    if predictions[i][1] > mean + std / 2:
                        keywords.append([self.wd.get_val_dictionary()[name][i, :1], predictions[i][1]])
            else:
                predictions = self.model.predict(self.wd.get_val_dictionary()[name][:, 1:])
                for i in range(len(predictions)):
                    print(self.wd.get_val_dictionary()[name][i, :1], predictions[i])
                mean = np.mean(predictions)
                std = np.std(predictions)
                for i in range(len(predictions)):
                    if predictions[i] >= mean + std / 2:
                        keywords.append([self.wd.get_val_dictionary()[name][i, :1], predictions[i]])
        else:
            keywords = []
            if self.uid == 'LSTM':
                predictions = self.model.predict(name[:, 1:])
                mean = np.mean([p[1] for p in predictions])
                std = np.std([p[1] for p in predictions])
                for i in range(len(predictions)):
                    if predictions[i][1] > mean + std / 2:
                        keywords.append(name[i, :1])
            else:
                predictions = self.model.predict(name[:, 1:])
                mean = np.mean(predictions)
                std = np.std(predictions)
                for i in range(len(predictions)):
                    if predictions[i] >= mean + std / 2:
                        keywords.append(name[i, :1])

        return keywords
