from math import log

from glob import glob
from json import dump, load
from typing import Dict, List

from xml_dataset import XMLDataset


class TF_IDF:
    def __init__(self, filename: str = None):
        self.corpus = []
        self.freq = []
        if filename is not None:
            self.load_freq(filename)

    def insert_corpus(self, datasets: List[XMLDataset]):
        for ds in datasets:
            self.corpus += ds.lemmas
        for doc in self.corpus:
            self.freq.append(self._get_freq_dict(doc))

    @staticmethod
    def _get_freq_dict(words: List[str]) -> Dict[(str, int)]:
        ret = {}
        for word in words:
            if word in ret:
                ret[word] += 1
            else:
                ret[word] = 1
        return ret

    def save_freq(self, filename: str = 'tfidf.json'):
        with open(filename, 'w') as f:
            dump(self.freq, f)

    def load_freq(self, filename: str = 'tfidf.json'):
        with open(filename, 'r') as f:
            self.freq = load(f)

    def tf(self, words: List[str]) -> Dict[(str, float)]:
        freq = self._get_freq_dict(words)
        count = sum(freq.values())
        ret = {key: value / count for key, value in freq.items()}
        return ret

    def idf(self, words: List[str]) -> Dict[(str, float)]:
        ret = {word: log(len(self.freq) / max(0.01, sum([1 for f in self.freq if word in f.keys()]))) for word in
               words}
        return ret

    def tf_idf(self, words: List[str] = None, tf: Dict[(str, float)] = None, idf: Dict[(str, float)] = None):
        if tf is not None and idf is not None:
            if tf.keys() != idf.keys():
                raise AttributeError('TF and IDF keys are not the same!')
        elif words is not None:
            tf = self.tf(words)
            idf = self.idf(words)
        else:
            raise AttributeError('Not enough parameters. Give either words or tf and idf')
        ret = {k: tf * idf for k, tf, idf in zip(tf.keys(), tf.values(), idf.values())}
        return ret


if __name__ == '__main__':
    ds_files = glob('processed_dataset/*.csv')
    print(ds_files)
    dss = [XMLDataset(csv=csv, keywords=False) for csv in ds_files]
    text = ['ala', 'mieć', 'kot']
    tool1 = TF_IDF()
    tool1.insert_corpus(dss)
    tool1.save_freq()
    print(f'Words in corpus: {sum([len(c) for c in tool1.corpus])}')
    print(f'Docs in corpus:  {len(tool1.corpus)}')
    tool2 = TF_IDF(filename='tfidf.json')
    print(tool2.tf(text))
    print(tool2.idf(text))
    print(tool2.tf_idf(text))
