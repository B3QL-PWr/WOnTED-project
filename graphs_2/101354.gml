graph [
  node [
    id 0
    label "robert"
    origin "text"
  ]
  node [
    id 1
    label "witek"
    origin "text"
  ]
  node [
    id 2
    label "Roberta"
  ]
  node [
    id 3
    label "witka"
  ]
  node [
    id 4
    label "Asseco"
  ]
  node [
    id 5
    label "Prokomu"
  ]
  node [
    id 6
    label "Gdynia"
  ]
  node [
    id 7
    label "Anwilu"
  ]
  node [
    id 8
    label "W&#322;oc&#322;awek"
  ]
  node [
    id 9
    label "zag&#322;&#281;bie"
  ]
  node [
    id 10
    label "sosnowiec"
  ]
  node [
    id 11
    label "Turowie"
  ]
  node [
    id 12
    label "Zgorzelec"
  ]
  node [
    id 13
    label "Anwil"
  ]
  node [
    id 14
    label "tur"
  ]
  node [
    id 15
    label "Prokom"
  ]
  node [
    id 16
    label "mistrzostwo"
  ]
  node [
    id 17
    label "europ"
  ]
  node [
    id 18
    label "puchar"
  ]
  node [
    id 19
    label "polski"
  ]
  node [
    id 20
    label "Koracza"
  ]
  node [
    id 21
    label "Saporty"
  ]
  node [
    id 22
    label "ULEB"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 22
  ]
]
