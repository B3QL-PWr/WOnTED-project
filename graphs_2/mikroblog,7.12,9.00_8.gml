graph [
  node [
    id 0
    label "granie"
    origin "text"
  ]
  node [
    id 1
    label "trzeba"
    origin "text"
  ]
  node [
    id 2
    label "dwa"
    origin "text"
  ]
  node [
    id 3
    label "pr&#243;bowanie"
  ]
  node [
    id 4
    label "instrumentalizacja"
  ]
  node [
    id 5
    label "nagranie_si&#281;"
  ]
  node [
    id 6
    label "wykonywanie"
  ]
  node [
    id 7
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 8
    label "pasowanie"
  ]
  node [
    id 9
    label "staranie_si&#281;"
  ]
  node [
    id 10
    label "wybijanie"
  ]
  node [
    id 11
    label "odegranie_si&#281;"
  ]
  node [
    id 12
    label "instrument_muzyczny"
  ]
  node [
    id 13
    label "dogrywanie"
  ]
  node [
    id 14
    label "rozgrywanie"
  ]
  node [
    id 15
    label "przygrywanie"
  ]
  node [
    id 16
    label "lewa"
  ]
  node [
    id 17
    label "wyst&#281;powanie"
  ]
  node [
    id 18
    label "robienie"
  ]
  node [
    id 19
    label "uderzenie"
  ]
  node [
    id 20
    label "zwalczenie"
  ]
  node [
    id 21
    label "gra_w_karty"
  ]
  node [
    id 22
    label "mienienie_si&#281;"
  ]
  node [
    id 23
    label "wydawanie"
  ]
  node [
    id 24
    label "pretense"
  ]
  node [
    id 25
    label "prezentowanie"
  ]
  node [
    id 26
    label "na&#347;ladowanie"
  ]
  node [
    id 27
    label "dogranie"
  ]
  node [
    id 28
    label "wybicie"
  ]
  node [
    id 29
    label "playing"
  ]
  node [
    id 30
    label "rozegranie_si&#281;"
  ]
  node [
    id 31
    label "ust&#281;powanie"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "dzianie_si&#281;"
  ]
  node [
    id 34
    label "otwarcie"
  ]
  node [
    id 35
    label "glitter"
  ]
  node [
    id 36
    label "igranie"
  ]
  node [
    id 37
    label "odgrywanie_si&#281;"
  ]
  node [
    id 38
    label "pogranie"
  ]
  node [
    id 39
    label "wyr&#243;wnywanie"
  ]
  node [
    id 40
    label "szczekanie"
  ]
  node [
    id 41
    label "brzmienie"
  ]
  node [
    id 42
    label "przedstawianie"
  ]
  node [
    id 43
    label "wyr&#243;wnanie"
  ]
  node [
    id 44
    label "rola"
  ]
  node [
    id 45
    label "grywanie"
  ]
  node [
    id 46
    label "migotanie"
  ]
  node [
    id 47
    label "&#347;ciganie"
  ]
  node [
    id 48
    label "naznaczanie"
  ]
  node [
    id 49
    label "dzia&#322;anie"
  ]
  node [
    id 50
    label "przepisywanie_si&#281;"
  ]
  node [
    id 51
    label "bycie"
  ]
  node [
    id 52
    label "wywodzenie"
  ]
  node [
    id 53
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 54
    label "zjawianie_si&#281;"
  ]
  node [
    id 55
    label "odst&#281;powanie"
  ]
  node [
    id 56
    label "rezygnowanie"
  ]
  node [
    id 57
    label "being"
  ]
  node [
    id 58
    label "widoczny"
  ]
  node [
    id 59
    label "appearance"
  ]
  node [
    id 60
    label "obecno&#347;&#263;"
  ]
  node [
    id 61
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 62
    label "sk&#322;anianie"
  ]
  node [
    id 63
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 64
    label "wychodzenie"
  ]
  node [
    id 65
    label "stawanie_si&#281;"
  ]
  node [
    id 66
    label "uczestniczenie"
  ]
  node [
    id 67
    label "pogrywanie"
  ]
  node [
    id 68
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 69
    label "post&#261;pienie"
  ]
  node [
    id 70
    label "porobienie"
  ]
  node [
    id 71
    label "uprawianie"
  ]
  node [
    id 72
    label "uleganie"
  ]
  node [
    id 73
    label "przestawanie"
  ]
  node [
    id 74
    label "rozwi&#261;zywanie"
  ]
  node [
    id 75
    label "przegrywanie"
  ]
  node [
    id 76
    label "mijanie"
  ]
  node [
    id 77
    label "zrzekanie_si&#281;"
  ]
  node [
    id 78
    label "opuszczanie"
  ]
  node [
    id 79
    label "wygranie"
  ]
  node [
    id 80
    label "pokonanie"
  ]
  node [
    id 81
    label "powodowanie"
  ]
  node [
    id 82
    label "zabijanie"
  ]
  node [
    id 83
    label "wskazywanie"
  ]
  node [
    id 84
    label "niszczenie"
  ]
  node [
    id 85
    label "pokrywanie"
  ]
  node [
    id 86
    label "wypadanie"
  ]
  node [
    id 87
    label "nadawanie"
  ]
  node [
    id 88
    label "t&#322;oczenie"
  ]
  node [
    id 89
    label "depopulation"
  ]
  node [
    id 90
    label "tryskanie"
  ]
  node [
    id 91
    label "otw&#243;r"
  ]
  node [
    id 92
    label "rytm"
  ]
  node [
    id 93
    label "przebijanie"
  ]
  node [
    id 94
    label "destruction"
  ]
  node [
    id 95
    label "strike"
  ]
  node [
    id 96
    label "pokrycie"
  ]
  node [
    id 97
    label "wyt&#322;oczenie"
  ]
  node [
    id 98
    label "spowodowanie"
  ]
  node [
    id 99
    label "przebicie"
  ]
  node [
    id 100
    label "respite"
  ]
  node [
    id 101
    label "powybijanie"
  ]
  node [
    id 102
    label "wskazanie"
  ]
  node [
    id 103
    label "nadanie"
  ]
  node [
    id 104
    label "pozabijanie"
  ]
  node [
    id 105
    label "zniszczenie"
  ]
  node [
    id 106
    label "try&#347;ni&#281;cie"
  ]
  node [
    id 107
    label "interruption"
  ]
  node [
    id 108
    label "wypadni&#281;cie"
  ]
  node [
    id 109
    label "zabrzmienie"
  ]
  node [
    id 110
    label "skok"
  ]
  node [
    id 111
    label "pobicie"
  ]
  node [
    id 112
    label "zrobienie"
  ]
  node [
    id 113
    label "towarzyszenie"
  ]
  node [
    id 114
    label "up&#281;dzanie_si&#281;"
  ]
  node [
    id 115
    label "hunt"
  ]
  node [
    id 116
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 117
    label "refund"
  ]
  node [
    id 118
    label "arrangement"
  ]
  node [
    id 119
    label "ujednolicenie"
  ]
  node [
    id 120
    label "wynagrodzenie"
  ]
  node [
    id 121
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 122
    label "zjawisko"
  ]
  node [
    id 123
    label "akrobacja_lotnicza"
  ]
  node [
    id 124
    label "zap&#322;ata"
  ]
  node [
    id 125
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 126
    label "g&#322;adki"
  ]
  node [
    id 127
    label "alteration"
  ]
  node [
    id 128
    label "reform"
  ]
  node [
    id 129
    label "erecting"
  ]
  node [
    id 130
    label "equalizer"
  ]
  node [
    id 131
    label "zdobycie"
  ]
  node [
    id 132
    label "ukszta&#322;towanie"
  ]
  node [
    id 133
    label "grade"
  ]
  node [
    id 134
    label "prosty"
  ]
  node [
    id 135
    label "uporz&#261;dkowanie"
  ]
  node [
    id 136
    label "podanie"
  ]
  node [
    id 137
    label "nagranie"
  ]
  node [
    id 138
    label "zako&#324;czenie"
  ]
  node [
    id 139
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 140
    label "dogadanie_si&#281;"
  ]
  node [
    id 141
    label "porozumiewanie_si&#281;"
  ]
  node [
    id 142
    label "nagrywanie"
  ]
  node [
    id 143
    label "podawanie"
  ]
  node [
    id 144
    label "ko&#324;czenie"
  ]
  node [
    id 145
    label "kszta&#322;towanie"
  ]
  node [
    id 146
    label "equation"
  ]
  node [
    id 147
    label "zdobywanie"
  ]
  node [
    id 148
    label "uk&#322;adanie"
  ]
  node [
    id 149
    label "equalization"
  ]
  node [
    id 150
    label "ujednolicanie"
  ]
  node [
    id 151
    label "correction"
  ]
  node [
    id 152
    label "amendment"
  ]
  node [
    id 153
    label "r&#243;wny"
  ]
  node [
    id 154
    label "badanie"
  ]
  node [
    id 155
    label "przedstawienie"
  ]
  node [
    id 156
    label "jedzenie"
  ]
  node [
    id 157
    label "podejmowanie"
  ]
  node [
    id 158
    label "usi&#322;owanie"
  ]
  node [
    id 159
    label "tasting"
  ]
  node [
    id 160
    label "kiperstwo"
  ]
  node [
    id 161
    label "zaznawanie"
  ]
  node [
    id 162
    label "przygotowywanie_si&#281;"
  ]
  node [
    id 163
    label "essay"
  ]
  node [
    id 164
    label "zarz&#261;dzanie"
  ]
  node [
    id 165
    label "koszt_ca&#322;kowity"
  ]
  node [
    id 166
    label "dopracowanie"
  ]
  node [
    id 167
    label "fabrication"
  ]
  node [
    id 168
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 169
    label "muzyka"
  ]
  node [
    id 170
    label "praca"
  ]
  node [
    id 171
    label "urzeczywistnianie"
  ]
  node [
    id 172
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 173
    label "zaprz&#281;ganie"
  ]
  node [
    id 174
    label "pojawianie_si&#281;"
  ]
  node [
    id 175
    label "realization"
  ]
  node [
    id 176
    label "wyrabianie"
  ]
  node [
    id 177
    label "use"
  ]
  node [
    id 178
    label "gospodarka"
  ]
  node [
    id 179
    label "przepracowanie"
  ]
  node [
    id 180
    label "przepracowywanie"
  ]
  node [
    id 181
    label "awansowanie"
  ]
  node [
    id 182
    label "zapracowanie"
  ]
  node [
    id 183
    label "wyrobienie"
  ]
  node [
    id 184
    label "doprowadzanie"
  ]
  node [
    id 185
    label "przedmiot"
  ]
  node [
    id 186
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 187
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 188
    label "creation"
  ]
  node [
    id 189
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 190
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 191
    label "act"
  ]
  node [
    id 192
    label "tentegowanie"
  ]
  node [
    id 193
    label "activity"
  ]
  node [
    id 194
    label "bezproblemowy"
  ]
  node [
    id 195
    label "wydarzenie"
  ]
  node [
    id 196
    label "wymy&#347;lanie"
  ]
  node [
    id 197
    label "obgadywanie"
  ]
  node [
    id 198
    label "poszczekiwanie"
  ]
  node [
    id 199
    label "bark"
  ]
  node [
    id 200
    label "anonsowanie"
  ]
  node [
    id 201
    label "zaanonsowanie"
  ]
  node [
    id 202
    label "odg&#322;os"
  ]
  node [
    id 203
    label "kozio&#322;"
  ]
  node [
    id 204
    label "karabin"
  ]
  node [
    id 205
    label "poddawanie"
  ]
  node [
    id 206
    label "burst"
  ]
  node [
    id 207
    label "dopasowanie_si&#281;"
  ]
  node [
    id 208
    label "przywo&#322;anie"
  ]
  node [
    id 209
    label "odpowiedni"
  ]
  node [
    id 210
    label "fit"
  ]
  node [
    id 211
    label "mianowanie"
  ]
  node [
    id 212
    label "odpowiadanie"
  ]
  node [
    id 213
    label "puszczenie"
  ]
  node [
    id 214
    label "dawanie"
  ]
  node [
    id 215
    label "zwracanie_si&#281;"
  ]
  node [
    id 216
    label "ukazywanie_si&#281;"
  ]
  node [
    id 217
    label "denuncjowanie"
  ]
  node [
    id 218
    label "urz&#261;dzanie"
  ]
  node [
    id 219
    label "wytwarzanie"
  ]
  node [
    id 220
    label "emission"
  ]
  node [
    id 221
    label "emergence"
  ]
  node [
    id 222
    label "ujawnianie"
  ]
  node [
    id 223
    label "wprowadzanie"
  ]
  node [
    id 224
    label "issue"
  ]
  node [
    id 225
    label "wyra&#380;anie"
  ]
  node [
    id 226
    label "sound"
  ]
  node [
    id 227
    label "cecha"
  ]
  node [
    id 228
    label "tone"
  ]
  node [
    id 229
    label "spirit"
  ]
  node [
    id 230
    label "rejestr"
  ]
  node [
    id 231
    label "kolorystyka"
  ]
  node [
    id 232
    label "teatr"
  ]
  node [
    id 233
    label "opisywanie"
  ]
  node [
    id 234
    label "representation"
  ]
  node [
    id 235
    label "zapoznawanie"
  ]
  node [
    id 236
    label "ukazywanie"
  ]
  node [
    id 237
    label "pokazywanie"
  ]
  node [
    id 238
    label "display"
  ]
  node [
    id 239
    label "demonstrowanie"
  ]
  node [
    id 240
    label "presentation"
  ]
  node [
    id 241
    label "czerpanie"
  ]
  node [
    id 242
    label "powt&#243;rzenie"
  ]
  node [
    id 243
    label "powtarzanie"
  ]
  node [
    id 244
    label "kopiowanie"
  ]
  node [
    id 245
    label "replication"
  ]
  node [
    id 246
    label "przej&#281;cie"
  ]
  node [
    id 247
    label "przejmowanie"
  ]
  node [
    id 248
    label "wzorowanie_si&#281;"
  ]
  node [
    id 249
    label "post&#281;powanie"
  ]
  node [
    id 250
    label "imposture"
  ]
  node [
    id 251
    label "uprzedzanie"
  ]
  node [
    id 252
    label "present"
  ]
  node [
    id 253
    label "program"
  ]
  node [
    id 254
    label "lekcewa&#380;enie"
  ]
  node [
    id 255
    label "uprawienie"
  ]
  node [
    id 256
    label "kszta&#322;t"
  ]
  node [
    id 257
    label "dialog"
  ]
  node [
    id 258
    label "p&#322;osa"
  ]
  node [
    id 259
    label "plik"
  ]
  node [
    id 260
    label "ziemia"
  ]
  node [
    id 261
    label "wykonywa&#263;"
  ]
  node [
    id 262
    label "czyn"
  ]
  node [
    id 263
    label "ustawienie"
  ]
  node [
    id 264
    label "scenariusz"
  ]
  node [
    id 265
    label "pole"
  ]
  node [
    id 266
    label "gospodarstwo"
  ]
  node [
    id 267
    label "uprawi&#263;"
  ]
  node [
    id 268
    label "function"
  ]
  node [
    id 269
    label "posta&#263;"
  ]
  node [
    id 270
    label "zreinterpretowa&#263;"
  ]
  node [
    id 271
    label "zastosowanie"
  ]
  node [
    id 272
    label "reinterpretowa&#263;"
  ]
  node [
    id 273
    label "wrench"
  ]
  node [
    id 274
    label "irygowanie"
  ]
  node [
    id 275
    label "ustawi&#263;"
  ]
  node [
    id 276
    label "irygowa&#263;"
  ]
  node [
    id 277
    label "zreinterpretowanie"
  ]
  node [
    id 278
    label "cel"
  ]
  node [
    id 279
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 280
    label "gra&#263;"
  ]
  node [
    id 281
    label "aktorstwo"
  ]
  node [
    id 282
    label "kostium"
  ]
  node [
    id 283
    label "zagon"
  ]
  node [
    id 284
    label "znaczenie"
  ]
  node [
    id 285
    label "zagra&#263;"
  ]
  node [
    id 286
    label "reinterpretowanie"
  ]
  node [
    id 287
    label "sk&#322;ad"
  ]
  node [
    id 288
    label "tekst"
  ]
  node [
    id 289
    label "zagranie"
  ]
  node [
    id 290
    label "radlina"
  ]
  node [
    id 291
    label "zestaw"
  ]
  node [
    id 292
    label "jawno"
  ]
  node [
    id 293
    label "rozpocz&#281;cie"
  ]
  node [
    id 294
    label "udost&#281;pnienie"
  ]
  node [
    id 295
    label "publicznie"
  ]
  node [
    id 296
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 297
    label "ewidentnie"
  ]
  node [
    id 298
    label "jawnie"
  ]
  node [
    id 299
    label "opening"
  ]
  node [
    id 300
    label "jawny"
  ]
  node [
    id 301
    label "otwarty"
  ]
  node [
    id 302
    label "bezpo&#347;rednio"
  ]
  node [
    id 303
    label "zdecydowanie"
  ]
  node [
    id 304
    label "disturbance"
  ]
  node [
    id 305
    label "flare"
  ]
  node [
    id 306
    label "&#347;wiecenie"
  ]
  node [
    id 307
    label "&#322;yskanie"
  ]
  node [
    id 308
    label "b&#322;ysk"
  ]
  node [
    id 309
    label "trafienie"
  ]
  node [
    id 310
    label "walka"
  ]
  node [
    id 311
    label "cios"
  ]
  node [
    id 312
    label "zdarzenie_si&#281;"
  ]
  node [
    id 313
    label "wdarcie_si&#281;"
  ]
  node [
    id 314
    label "pogorszenie"
  ]
  node [
    id 315
    label "d&#378;wi&#281;k"
  ]
  node [
    id 316
    label "poczucie"
  ]
  node [
    id 317
    label "coup"
  ]
  node [
    id 318
    label "reakcja"
  ]
  node [
    id 319
    label "contact"
  ]
  node [
    id 320
    label "stukni&#281;cie"
  ]
  node [
    id 321
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 322
    label "bat"
  ]
  node [
    id 323
    label "rush"
  ]
  node [
    id 324
    label "odbicie"
  ]
  node [
    id 325
    label "dawka"
  ]
  node [
    id 326
    label "zadanie"
  ]
  node [
    id 327
    label "&#347;ci&#281;cie"
  ]
  node [
    id 328
    label "st&#322;uczenie"
  ]
  node [
    id 329
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 330
    label "time"
  ]
  node [
    id 331
    label "odbicie_si&#281;"
  ]
  node [
    id 332
    label "dotkni&#281;cie"
  ]
  node [
    id 333
    label "charge"
  ]
  node [
    id 334
    label "dostanie"
  ]
  node [
    id 335
    label "skrytykowanie"
  ]
  node [
    id 336
    label "zagrywka"
  ]
  node [
    id 337
    label "manewr"
  ]
  node [
    id 338
    label "nast&#261;pienie"
  ]
  node [
    id 339
    label "uderzanie"
  ]
  node [
    id 340
    label "pogoda"
  ]
  node [
    id 341
    label "stroke"
  ]
  node [
    id 342
    label "ruch"
  ]
  node [
    id 343
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 344
    label "flap"
  ]
  node [
    id 345
    label "dotyk"
  ]
  node [
    id 346
    label "necessity"
  ]
  node [
    id 347
    label "trza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
]
