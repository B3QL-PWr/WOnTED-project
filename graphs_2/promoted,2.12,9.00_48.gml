graph [
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "przedsi&#281;biorca"
    origin "text"
  ]
  node [
    id 2
    label "brakowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 4
    label "praca"
    origin "text"
  ]
  node [
    id 5
    label "samo"
    origin "text"
  ]
  node [
    id 6
    label "polski"
    origin "text"
  ]
  node [
    id 7
    label "m&#281;&#380;czyzna"
    origin "text"
  ]
  node [
    id 8
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 9
    label "wolne"
    origin "text"
  ]
  node [
    id 10
    label "kobieta"
    origin "text"
  ]
  node [
    id 11
    label "wydawca"
  ]
  node [
    id 12
    label "wsp&#243;lnik"
  ]
  node [
    id 13
    label "kapitalista"
  ]
  node [
    id 14
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 15
    label "klasa_&#347;rednia"
  ]
  node [
    id 16
    label "osoba_fizyczna"
  ]
  node [
    id 17
    label "podmiot"
  ]
  node [
    id 18
    label "wykupienie"
  ]
  node [
    id 19
    label "bycie_w_posiadaniu"
  ]
  node [
    id 20
    label "wykupywanie"
  ]
  node [
    id 21
    label "instytucja"
  ]
  node [
    id 22
    label "issuer"
  ]
  node [
    id 23
    label "uczestnik"
  ]
  node [
    id 24
    label "sp&#243;lnik"
  ]
  node [
    id 25
    label "uczestniczenie"
  ]
  node [
    id 26
    label "bogacz"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "industrializm"
  ]
  node [
    id 29
    label "rentier"
  ]
  node [
    id 30
    label "finansjera"
  ]
  node [
    id 31
    label "sprawdza&#263;"
  ]
  node [
    id 32
    label "utylizowa&#263;"
  ]
  node [
    id 33
    label "przegl&#261;da&#263;"
  ]
  node [
    id 34
    label "przebiera&#263;"
  ]
  node [
    id 35
    label "lack"
  ]
  node [
    id 36
    label "przetwarza&#263;"
  ]
  node [
    id 37
    label "wygl&#261;da&#263;"
  ]
  node [
    id 38
    label "prze&#347;witywa&#263;"
  ]
  node [
    id 39
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 40
    label "scan"
  ]
  node [
    id 41
    label "examine"
  ]
  node [
    id 42
    label "survey"
  ]
  node [
    id 43
    label "przeszukiwa&#263;"
  ]
  node [
    id 44
    label "robi&#263;"
  ]
  node [
    id 45
    label "szpiegowa&#263;"
  ]
  node [
    id 46
    label "dress"
  ]
  node [
    id 47
    label "przemienia&#263;"
  ]
  node [
    id 48
    label "trim"
  ]
  node [
    id 49
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 50
    label "ceregieli&#263;_si&#281;"
  ]
  node [
    id 51
    label "zmienia&#263;"
  ]
  node [
    id 52
    label "upodabnia&#263;"
  ]
  node [
    id 53
    label "cull"
  ]
  node [
    id 54
    label "zastanawia&#263;_si&#281;"
  ]
  node [
    id 55
    label "rusza&#263;"
  ]
  node [
    id 56
    label "przerzuca&#263;"
  ]
  node [
    id 57
    label "krzy&#380;"
  ]
  node [
    id 58
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 59
    label "handwriting"
  ]
  node [
    id 60
    label "d&#322;o&#324;"
  ]
  node [
    id 61
    label "gestykulowa&#263;"
  ]
  node [
    id 62
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 63
    label "palec"
  ]
  node [
    id 64
    label "przedrami&#281;"
  ]
  node [
    id 65
    label "cecha"
  ]
  node [
    id 66
    label "hand"
  ]
  node [
    id 67
    label "&#322;okie&#263;"
  ]
  node [
    id 68
    label "hazena"
  ]
  node [
    id 69
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 70
    label "bramkarz"
  ]
  node [
    id 71
    label "nadgarstek"
  ]
  node [
    id 72
    label "graba"
  ]
  node [
    id 73
    label "pracownik"
  ]
  node [
    id 74
    label "r&#261;czyna"
  ]
  node [
    id 75
    label "k&#322;&#261;b"
  ]
  node [
    id 76
    label "pi&#322;ka"
  ]
  node [
    id 77
    label "chwyta&#263;"
  ]
  node [
    id 78
    label "cmoknonsens"
  ]
  node [
    id 79
    label "pomocnik"
  ]
  node [
    id 80
    label "gestykulowanie"
  ]
  node [
    id 81
    label "chwytanie"
  ]
  node [
    id 82
    label "obietnica"
  ]
  node [
    id 83
    label "spos&#243;b"
  ]
  node [
    id 84
    label "zagrywka"
  ]
  node [
    id 85
    label "kroki"
  ]
  node [
    id 86
    label "hasta"
  ]
  node [
    id 87
    label "wykroczenie"
  ]
  node [
    id 88
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 89
    label "czerwona_kartka"
  ]
  node [
    id 90
    label "paw"
  ]
  node [
    id 91
    label "rami&#281;"
  ]
  node [
    id 92
    label "kula"
  ]
  node [
    id 93
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 94
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 95
    label "do&#347;rodkowywanie"
  ]
  node [
    id 96
    label "odbicie"
  ]
  node [
    id 97
    label "gra"
  ]
  node [
    id 98
    label "musket_ball"
  ]
  node [
    id 99
    label "aut"
  ]
  node [
    id 100
    label "serwowa&#263;"
  ]
  node [
    id 101
    label "sport_zespo&#322;owy"
  ]
  node [
    id 102
    label "sport"
  ]
  node [
    id 103
    label "serwowanie"
  ]
  node [
    id 104
    label "orb"
  ]
  node [
    id 105
    label "&#347;wieca"
  ]
  node [
    id 106
    label "zaserwowanie"
  ]
  node [
    id 107
    label "zaserwowa&#263;"
  ]
  node [
    id 108
    label "rzucanka"
  ]
  node [
    id 109
    label "charakterystyka"
  ]
  node [
    id 110
    label "m&#322;ot"
  ]
  node [
    id 111
    label "znak"
  ]
  node [
    id 112
    label "drzewo"
  ]
  node [
    id 113
    label "pr&#243;ba"
  ]
  node [
    id 114
    label "attribute"
  ]
  node [
    id 115
    label "marka"
  ]
  node [
    id 116
    label "model"
  ]
  node [
    id 117
    label "narz&#281;dzie"
  ]
  node [
    id 118
    label "zbi&#243;r"
  ]
  node [
    id 119
    label "tryb"
  ]
  node [
    id 120
    label "nature"
  ]
  node [
    id 121
    label "discourtesy"
  ]
  node [
    id 122
    label "post&#281;pek"
  ]
  node [
    id 123
    label "transgresja"
  ]
  node [
    id 124
    label "zrobienie"
  ]
  node [
    id 125
    label "gambit"
  ]
  node [
    id 126
    label "rozgrywka"
  ]
  node [
    id 127
    label "move"
  ]
  node [
    id 128
    label "manewr"
  ]
  node [
    id 129
    label "uderzenie"
  ]
  node [
    id 130
    label "posuni&#281;cie"
  ]
  node [
    id 131
    label "myk"
  ]
  node [
    id 132
    label "gra_w_karty"
  ]
  node [
    id 133
    label "mecz"
  ]
  node [
    id 134
    label "travel"
  ]
  node [
    id 135
    label "zapowied&#378;"
  ]
  node [
    id 136
    label "statement"
  ]
  node [
    id 137
    label "zapewnienie"
  ]
  node [
    id 138
    label "d&#378;wi&#281;k"
  ]
  node [
    id 139
    label "koszyk&#243;wka"
  ]
  node [
    id 140
    label "pi&#322;ka_r&#281;czna"
  ]
  node [
    id 141
    label "obrona"
  ]
  node [
    id 142
    label "hokej"
  ]
  node [
    id 143
    label "zawodnik"
  ]
  node [
    id 144
    label "gracz"
  ]
  node [
    id 145
    label "bileter"
  ]
  node [
    id 146
    label "wykidaj&#322;o"
  ]
  node [
    id 147
    label "ujmowa&#263;"
  ]
  node [
    id 148
    label "zabiera&#263;"
  ]
  node [
    id 149
    label "bra&#263;"
  ]
  node [
    id 150
    label "rozumie&#263;"
  ]
  node [
    id 151
    label "get"
  ]
  node [
    id 152
    label "dochodzi&#263;"
  ]
  node [
    id 153
    label "cope"
  ]
  node [
    id 154
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 155
    label "ogarnia&#263;"
  ]
  node [
    id 156
    label "doj&#347;&#263;"
  ]
  node [
    id 157
    label "perceive"
  ]
  node [
    id 158
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 159
    label "kompozycja"
  ]
  node [
    id 160
    label "w&#322;&#243;cznia"
  ]
  node [
    id 161
    label "triarius"
  ]
  node [
    id 162
    label "dochodzenie"
  ]
  node [
    id 163
    label "rozumienie"
  ]
  node [
    id 164
    label "branie"
  ]
  node [
    id 165
    label "perception"
  ]
  node [
    id 166
    label "wpadni&#281;cie"
  ]
  node [
    id 167
    label "catch"
  ]
  node [
    id 168
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 169
    label "odp&#322;ywanie"
  ]
  node [
    id 170
    label "ogarnianie"
  ]
  node [
    id 171
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 172
    label "porywanie"
  ]
  node [
    id 173
    label "wpadanie"
  ]
  node [
    id 174
    label "doj&#347;cie"
  ]
  node [
    id 175
    label "przyp&#322;ywanie"
  ]
  node [
    id 176
    label "kszta&#322;t"
  ]
  node [
    id 177
    label "przedmiot"
  ]
  node [
    id 178
    label "traverse"
  ]
  node [
    id 179
    label "kara_&#347;mierci"
  ]
  node [
    id 180
    label "cierpienie"
  ]
  node [
    id 181
    label "symbol"
  ]
  node [
    id 182
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 183
    label "biblizm"
  ]
  node [
    id 184
    label "order"
  ]
  node [
    id 185
    label "gest"
  ]
  node [
    id 186
    label "ca&#322;us"
  ]
  node [
    id 187
    label "gesticulate"
  ]
  node [
    id 188
    label "pokazanie"
  ]
  node [
    id 189
    label "ruszanie"
  ]
  node [
    id 190
    label "pokazywanie"
  ]
  node [
    id 191
    label "linia_mi&#322;o&#347;ci"
  ]
  node [
    id 192
    label "wyklepanie"
  ]
  node [
    id 193
    label "chiromancja"
  ]
  node [
    id 194
    label "klepanie"
  ]
  node [
    id 195
    label "wyklepa&#263;"
  ]
  node [
    id 196
    label "szeroko&#347;&#263;_d&#322;oni"
  ]
  node [
    id 197
    label "dotykanie"
  ]
  node [
    id 198
    label "klepa&#263;"
  ]
  node [
    id 199
    label "linia_&#380;ycia"
  ]
  node [
    id 200
    label "linia_rozumu"
  ]
  node [
    id 201
    label "poduszka"
  ]
  node [
    id 202
    label "dotyka&#263;"
  ]
  node [
    id 203
    label "polidaktylia"
  ]
  node [
    id 204
    label "dzia&#322;anie"
  ]
  node [
    id 205
    label "koniuszek_palca"
  ]
  node [
    id 206
    label "paznokie&#263;"
  ]
  node [
    id 207
    label "pazur"
  ]
  node [
    id 208
    label "element_anatomiczny"
  ]
  node [
    id 209
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 210
    label "zap&#322;on"
  ]
  node [
    id 211
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 212
    label "knykie&#263;"
  ]
  node [
    id 213
    label "palpacja"
  ]
  node [
    id 214
    label "kostka"
  ]
  node [
    id 215
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 216
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 217
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 218
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 219
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 220
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 221
    label "powerball"
  ]
  node [
    id 222
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 223
    label "narz&#261;d_ruchu"
  ]
  node [
    id 224
    label "mi&#281;sie&#324;_&#322;okciowy"
  ]
  node [
    id 225
    label "ko&#347;&#263;_ramieniowa"
  ]
  node [
    id 226
    label "triceps"
  ]
  node [
    id 227
    label "element"
  ]
  node [
    id 228
    label "maszyna"
  ]
  node [
    id 229
    label "biceps"
  ]
  node [
    id 230
    label "robot_przemys&#322;owy"
  ]
  node [
    id 231
    label "cloud"
  ]
  node [
    id 232
    label "chmura"
  ]
  node [
    id 233
    label "p&#281;d"
  ]
  node [
    id 234
    label "skupienie"
  ]
  node [
    id 235
    label "grzbiet"
  ]
  node [
    id 236
    label "ob&#322;ok_Oorta"
  ]
  node [
    id 237
    label "pl&#261;tanina"
  ]
  node [
    id 238
    label "zjawisko"
  ]
  node [
    id 239
    label "oberwanie_si&#281;"
  ]
  node [
    id 240
    label "oberwa&#263;_si&#281;"
  ]
  node [
    id 241
    label "powderpuff"
  ]
  node [
    id 242
    label "burza"
  ]
  node [
    id 243
    label "zgi&#281;cie_&#322;okciowe"
  ]
  node [
    id 244
    label "r&#281;kaw"
  ]
  node [
    id 245
    label "miara"
  ]
  node [
    id 246
    label "d&#243;&#322;_&#322;okciowy"
  ]
  node [
    id 247
    label "listewka"
  ]
  node [
    id 248
    label "ko&#347;&#263;_czworoboczna_mniejsza"
  ]
  node [
    id 249
    label "metacarpus"
  ]
  node [
    id 250
    label "ko&#347;&#263;_czworoboczna_wi&#281;ksza"
  ]
  node [
    id 251
    label "ko&#347;&#263;_promieniowa"
  ]
  node [
    id 252
    label "ko&#347;&#263;_&#322;okciowa"
  ]
  node [
    id 253
    label "r&#261;cz&#281;ta"
  ]
  node [
    id 254
    label "kredens"
  ]
  node [
    id 255
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 256
    label "bylina"
  ]
  node [
    id 257
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 258
    label "pomoc"
  ]
  node [
    id 259
    label "wrzosowate"
  ]
  node [
    id 260
    label "pomagacz"
  ]
  node [
    id 261
    label "salariat"
  ]
  node [
    id 262
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 263
    label "delegowanie"
  ]
  node [
    id 264
    label "pracu&#347;"
  ]
  node [
    id 265
    label "delegowa&#263;"
  ]
  node [
    id 266
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 267
    label "korona"
  ]
  node [
    id 268
    label "wymiociny"
  ]
  node [
    id 269
    label "ba&#380;anty"
  ]
  node [
    id 270
    label "ptak"
  ]
  node [
    id 271
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 272
    label "najem"
  ]
  node [
    id 273
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 274
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 275
    label "zak&#322;ad"
  ]
  node [
    id 276
    label "stosunek_pracy"
  ]
  node [
    id 277
    label "benedykty&#324;ski"
  ]
  node [
    id 278
    label "poda&#380;_pracy"
  ]
  node [
    id 279
    label "pracowanie"
  ]
  node [
    id 280
    label "tyrka"
  ]
  node [
    id 281
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 282
    label "wytw&#243;r"
  ]
  node [
    id 283
    label "miejsce"
  ]
  node [
    id 284
    label "zaw&#243;d"
  ]
  node [
    id 285
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 286
    label "tynkarski"
  ]
  node [
    id 287
    label "pracowa&#263;"
  ]
  node [
    id 288
    label "czynno&#347;&#263;"
  ]
  node [
    id 289
    label "zmiana"
  ]
  node [
    id 290
    label "czynnik_produkcji"
  ]
  node [
    id 291
    label "zobowi&#261;zanie"
  ]
  node [
    id 292
    label "kierownictwo"
  ]
  node [
    id 293
    label "siedziba"
  ]
  node [
    id 294
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 295
    label "p&#322;&#243;d"
  ]
  node [
    id 296
    label "work"
  ]
  node [
    id 297
    label "rezultat"
  ]
  node [
    id 298
    label "activity"
  ]
  node [
    id 299
    label "bezproblemowy"
  ]
  node [
    id 300
    label "wydarzenie"
  ]
  node [
    id 301
    label "warunek_lokalowy"
  ]
  node [
    id 302
    label "plac"
  ]
  node [
    id 303
    label "location"
  ]
  node [
    id 304
    label "uwaga"
  ]
  node [
    id 305
    label "przestrze&#324;"
  ]
  node [
    id 306
    label "status"
  ]
  node [
    id 307
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 308
    label "chwila"
  ]
  node [
    id 309
    label "cia&#322;o"
  ]
  node [
    id 310
    label "rz&#261;d"
  ]
  node [
    id 311
    label "stosunek_prawny"
  ]
  node [
    id 312
    label "oblig"
  ]
  node [
    id 313
    label "uregulowa&#263;"
  ]
  node [
    id 314
    label "oddzia&#322;anie"
  ]
  node [
    id 315
    label "occupation"
  ]
  node [
    id 316
    label "duty"
  ]
  node [
    id 317
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 318
    label "obowi&#261;zek"
  ]
  node [
    id 319
    label "miejsce_pracy"
  ]
  node [
    id 320
    label "zak&#322;adka"
  ]
  node [
    id 321
    label "jednostka_organizacyjna"
  ]
  node [
    id 322
    label "wyko&#324;czenie"
  ]
  node [
    id 323
    label "firma"
  ]
  node [
    id 324
    label "czyn"
  ]
  node [
    id 325
    label "company"
  ]
  node [
    id 326
    label "instytut"
  ]
  node [
    id 327
    label "umowa"
  ]
  node [
    id 328
    label "&#321;ubianka"
  ]
  node [
    id 329
    label "dzia&#322;_personalny"
  ]
  node [
    id 330
    label "Kreml"
  ]
  node [
    id 331
    label "Bia&#322;y_Dom"
  ]
  node [
    id 332
    label "budynek"
  ]
  node [
    id 333
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 334
    label "sadowisko"
  ]
  node [
    id 335
    label "rewizja"
  ]
  node [
    id 336
    label "passage"
  ]
  node [
    id 337
    label "oznaka"
  ]
  node [
    id 338
    label "change"
  ]
  node [
    id 339
    label "ferment"
  ]
  node [
    id 340
    label "komplet"
  ]
  node [
    id 341
    label "anatomopatolog"
  ]
  node [
    id 342
    label "zmianka"
  ]
  node [
    id 343
    label "czas"
  ]
  node [
    id 344
    label "amendment"
  ]
  node [
    id 345
    label "odmienianie"
  ]
  node [
    id 346
    label "tura"
  ]
  node [
    id 347
    label "cierpliwy"
  ]
  node [
    id 348
    label "mozolny"
  ]
  node [
    id 349
    label "wytrwa&#322;y"
  ]
  node [
    id 350
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 351
    label "benedykty&#324;sko"
  ]
  node [
    id 352
    label "typowy"
  ]
  node [
    id 353
    label "po_benedykty&#324;sku"
  ]
  node [
    id 354
    label "endeavor"
  ]
  node [
    id 355
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 356
    label "mie&#263;_miejsce"
  ]
  node [
    id 357
    label "podejmowa&#263;"
  ]
  node [
    id 358
    label "dziama&#263;"
  ]
  node [
    id 359
    label "do"
  ]
  node [
    id 360
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 361
    label "bangla&#263;"
  ]
  node [
    id 362
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 363
    label "dzia&#322;a&#263;"
  ]
  node [
    id 364
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 365
    label "funkcjonowa&#263;"
  ]
  node [
    id 366
    label "zawodoznawstwo"
  ]
  node [
    id 367
    label "emocja"
  ]
  node [
    id 368
    label "office"
  ]
  node [
    id 369
    label "kwalifikacje"
  ]
  node [
    id 370
    label "craft"
  ]
  node [
    id 371
    label "przepracowanie_si&#281;"
  ]
  node [
    id 372
    label "zarz&#261;dzanie"
  ]
  node [
    id 373
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 374
    label "podlizanie_si&#281;"
  ]
  node [
    id 375
    label "dopracowanie"
  ]
  node [
    id 376
    label "podlizywanie_si&#281;"
  ]
  node [
    id 377
    label "uruchamianie"
  ]
  node [
    id 378
    label "d&#261;&#380;enie"
  ]
  node [
    id 379
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 380
    label "uruchomienie"
  ]
  node [
    id 381
    label "nakr&#281;canie"
  ]
  node [
    id 382
    label "funkcjonowanie"
  ]
  node [
    id 383
    label "tr&#243;jstronny"
  ]
  node [
    id 384
    label "postaranie_si&#281;"
  ]
  node [
    id 385
    label "odpocz&#281;cie"
  ]
  node [
    id 386
    label "nakr&#281;cenie"
  ]
  node [
    id 387
    label "zatrzymanie"
  ]
  node [
    id 388
    label "spracowanie_si&#281;"
  ]
  node [
    id 389
    label "skakanie"
  ]
  node [
    id 390
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 391
    label "podtrzymywanie"
  ]
  node [
    id 392
    label "w&#322;&#261;czanie"
  ]
  node [
    id 393
    label "zaprz&#281;ganie"
  ]
  node [
    id 394
    label "podejmowanie"
  ]
  node [
    id 395
    label "wyrabianie"
  ]
  node [
    id 396
    label "dzianie_si&#281;"
  ]
  node [
    id 397
    label "use"
  ]
  node [
    id 398
    label "przepracowanie"
  ]
  node [
    id 399
    label "poruszanie_si&#281;"
  ]
  node [
    id 400
    label "funkcja"
  ]
  node [
    id 401
    label "impact"
  ]
  node [
    id 402
    label "przepracowywanie"
  ]
  node [
    id 403
    label "awansowanie"
  ]
  node [
    id 404
    label "courtship"
  ]
  node [
    id 405
    label "zapracowanie"
  ]
  node [
    id 406
    label "wyrobienie"
  ]
  node [
    id 407
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 408
    label "w&#322;&#261;czenie"
  ]
  node [
    id 409
    label "transakcja"
  ]
  node [
    id 410
    label "biuro"
  ]
  node [
    id 411
    label "lead"
  ]
  node [
    id 412
    label "zesp&#243;&#322;"
  ]
  node [
    id 413
    label "w&#322;adza"
  ]
  node [
    id 414
    label "Polish"
  ]
  node [
    id 415
    label "goniony"
  ]
  node [
    id 416
    label "oberek"
  ]
  node [
    id 417
    label "ryba_po_grecku"
  ]
  node [
    id 418
    label "sztajer"
  ]
  node [
    id 419
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 420
    label "krakowiak"
  ]
  node [
    id 421
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 422
    label "pierogi_ruskie"
  ]
  node [
    id 423
    label "lacki"
  ]
  node [
    id 424
    label "polak"
  ]
  node [
    id 425
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 426
    label "chodzony"
  ]
  node [
    id 427
    label "po_polsku"
  ]
  node [
    id 428
    label "mazur"
  ]
  node [
    id 429
    label "polsko"
  ]
  node [
    id 430
    label "skoczny"
  ]
  node [
    id 431
    label "drabant"
  ]
  node [
    id 432
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 433
    label "j&#281;zyk"
  ]
  node [
    id 434
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 435
    label "artykulator"
  ]
  node [
    id 436
    label "kod"
  ]
  node [
    id 437
    label "kawa&#322;ek"
  ]
  node [
    id 438
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 439
    label "gramatyka"
  ]
  node [
    id 440
    label "stylik"
  ]
  node [
    id 441
    label "przet&#322;umaczenie"
  ]
  node [
    id 442
    label "formalizowanie"
  ]
  node [
    id 443
    label "ssanie"
  ]
  node [
    id 444
    label "ssa&#263;"
  ]
  node [
    id 445
    label "language"
  ]
  node [
    id 446
    label "liza&#263;"
  ]
  node [
    id 447
    label "napisa&#263;"
  ]
  node [
    id 448
    label "konsonantyzm"
  ]
  node [
    id 449
    label "wokalizm"
  ]
  node [
    id 450
    label "pisa&#263;"
  ]
  node [
    id 451
    label "fonetyka"
  ]
  node [
    id 452
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 453
    label "jeniec"
  ]
  node [
    id 454
    label "but"
  ]
  node [
    id 455
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 456
    label "po_koroniarsku"
  ]
  node [
    id 457
    label "kultura_duchowa"
  ]
  node [
    id 458
    label "t&#322;umaczenie"
  ]
  node [
    id 459
    label "m&#243;wienie"
  ]
  node [
    id 460
    label "pype&#263;"
  ]
  node [
    id 461
    label "lizanie"
  ]
  node [
    id 462
    label "pismo"
  ]
  node [
    id 463
    label "formalizowa&#263;"
  ]
  node [
    id 464
    label "organ"
  ]
  node [
    id 465
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 466
    label "makroglosja"
  ]
  node [
    id 467
    label "m&#243;wi&#263;"
  ]
  node [
    id 468
    label "jama_ustna"
  ]
  node [
    id 469
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 470
    label "formacja_geologiczna"
  ]
  node [
    id 471
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 472
    label "natural_language"
  ]
  node [
    id 473
    label "s&#322;ownictwo"
  ]
  node [
    id 474
    label "urz&#261;dzenie"
  ]
  node [
    id 475
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 476
    label "wschodnioeuropejski"
  ]
  node [
    id 477
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 478
    label "poga&#324;ski"
  ]
  node [
    id 479
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 480
    label "topielec"
  ]
  node [
    id 481
    label "europejski"
  ]
  node [
    id 482
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 483
    label "langosz"
  ]
  node [
    id 484
    label "zboczenie"
  ]
  node [
    id 485
    label "om&#243;wienie"
  ]
  node [
    id 486
    label "sponiewieranie"
  ]
  node [
    id 487
    label "discipline"
  ]
  node [
    id 488
    label "rzecz"
  ]
  node [
    id 489
    label "omawia&#263;"
  ]
  node [
    id 490
    label "kr&#261;&#380;enie"
  ]
  node [
    id 491
    label "tre&#347;&#263;"
  ]
  node [
    id 492
    label "robienie"
  ]
  node [
    id 493
    label "sponiewiera&#263;"
  ]
  node [
    id 494
    label "entity"
  ]
  node [
    id 495
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 496
    label "tematyka"
  ]
  node [
    id 497
    label "w&#261;tek"
  ]
  node [
    id 498
    label "charakter"
  ]
  node [
    id 499
    label "zbaczanie"
  ]
  node [
    id 500
    label "program_nauczania"
  ]
  node [
    id 501
    label "om&#243;wi&#263;"
  ]
  node [
    id 502
    label "omawianie"
  ]
  node [
    id 503
    label "thing"
  ]
  node [
    id 504
    label "kultura"
  ]
  node [
    id 505
    label "istota"
  ]
  node [
    id 506
    label "zbacza&#263;"
  ]
  node [
    id 507
    label "zboczy&#263;"
  ]
  node [
    id 508
    label "gwardzista"
  ]
  node [
    id 509
    label "melodia"
  ]
  node [
    id 510
    label "taniec"
  ]
  node [
    id 511
    label "taniec_ludowy"
  ]
  node [
    id 512
    label "&#347;redniowieczny"
  ]
  node [
    id 513
    label "europejsko"
  ]
  node [
    id 514
    label "specjalny"
  ]
  node [
    id 515
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 516
    label "weso&#322;y"
  ]
  node [
    id 517
    label "sprawny"
  ]
  node [
    id 518
    label "rytmiczny"
  ]
  node [
    id 519
    label "skocznie"
  ]
  node [
    id 520
    label "energiczny"
  ]
  node [
    id 521
    label "przytup"
  ]
  node [
    id 522
    label "ho&#322;ubiec"
  ]
  node [
    id 523
    label "wodzi&#263;"
  ]
  node [
    id 524
    label "lendler"
  ]
  node [
    id 525
    label "austriacki"
  ]
  node [
    id 526
    label "polka"
  ]
  node [
    id 527
    label "ludowy"
  ]
  node [
    id 528
    label "pie&#347;&#324;"
  ]
  node [
    id 529
    label "mieszkaniec"
  ]
  node [
    id 530
    label "centu&#347;"
  ]
  node [
    id 531
    label "lalka"
  ]
  node [
    id 532
    label "Ma&#322;opolanin"
  ]
  node [
    id 533
    label "krakauer"
  ]
  node [
    id 534
    label "doros&#322;y"
  ]
  node [
    id 535
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 536
    label "ojciec"
  ]
  node [
    id 537
    label "jegomo&#347;&#263;"
  ]
  node [
    id 538
    label "andropauza"
  ]
  node [
    id 539
    label "pa&#324;stwo"
  ]
  node [
    id 540
    label "bratek"
  ]
  node [
    id 541
    label "samiec"
  ]
  node [
    id 542
    label "ch&#322;opina"
  ]
  node [
    id 543
    label "twardziel"
  ]
  node [
    id 544
    label "androlog"
  ]
  node [
    id 545
    label "m&#261;&#380;"
  ]
  node [
    id 546
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 547
    label "Katar"
  ]
  node [
    id 548
    label "Libia"
  ]
  node [
    id 549
    label "Gwatemala"
  ]
  node [
    id 550
    label "Ekwador"
  ]
  node [
    id 551
    label "Afganistan"
  ]
  node [
    id 552
    label "Tad&#380;ykistan"
  ]
  node [
    id 553
    label "Bhutan"
  ]
  node [
    id 554
    label "Argentyna"
  ]
  node [
    id 555
    label "D&#380;ibuti"
  ]
  node [
    id 556
    label "Wenezuela"
  ]
  node [
    id 557
    label "Gabon"
  ]
  node [
    id 558
    label "Ukraina"
  ]
  node [
    id 559
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 560
    label "Rwanda"
  ]
  node [
    id 561
    label "Liechtenstein"
  ]
  node [
    id 562
    label "organizacja"
  ]
  node [
    id 563
    label "Sri_Lanka"
  ]
  node [
    id 564
    label "Madagaskar"
  ]
  node [
    id 565
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 566
    label "Kongo"
  ]
  node [
    id 567
    label "Tonga"
  ]
  node [
    id 568
    label "Bangladesz"
  ]
  node [
    id 569
    label "Kanada"
  ]
  node [
    id 570
    label "Wehrlen"
  ]
  node [
    id 571
    label "Algieria"
  ]
  node [
    id 572
    label "Uganda"
  ]
  node [
    id 573
    label "Surinam"
  ]
  node [
    id 574
    label "Sahara_Zachodnia"
  ]
  node [
    id 575
    label "Chile"
  ]
  node [
    id 576
    label "W&#281;gry"
  ]
  node [
    id 577
    label "Birma"
  ]
  node [
    id 578
    label "Kazachstan"
  ]
  node [
    id 579
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 580
    label "Armenia"
  ]
  node [
    id 581
    label "Tuwalu"
  ]
  node [
    id 582
    label "Timor_Wschodni"
  ]
  node [
    id 583
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 584
    label "Izrael"
  ]
  node [
    id 585
    label "Estonia"
  ]
  node [
    id 586
    label "Komory"
  ]
  node [
    id 587
    label "Kamerun"
  ]
  node [
    id 588
    label "Haiti"
  ]
  node [
    id 589
    label "Belize"
  ]
  node [
    id 590
    label "Sierra_Leone"
  ]
  node [
    id 591
    label "Luksemburg"
  ]
  node [
    id 592
    label "USA"
  ]
  node [
    id 593
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 594
    label "Barbados"
  ]
  node [
    id 595
    label "San_Marino"
  ]
  node [
    id 596
    label "Bu&#322;garia"
  ]
  node [
    id 597
    label "Indonezja"
  ]
  node [
    id 598
    label "Wietnam"
  ]
  node [
    id 599
    label "Malawi"
  ]
  node [
    id 600
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 601
    label "Francja"
  ]
  node [
    id 602
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 603
    label "partia"
  ]
  node [
    id 604
    label "Zambia"
  ]
  node [
    id 605
    label "Angola"
  ]
  node [
    id 606
    label "Grenada"
  ]
  node [
    id 607
    label "Nepal"
  ]
  node [
    id 608
    label "Panama"
  ]
  node [
    id 609
    label "Rumunia"
  ]
  node [
    id 610
    label "Czarnog&#243;ra"
  ]
  node [
    id 611
    label "Malediwy"
  ]
  node [
    id 612
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 613
    label "S&#322;owacja"
  ]
  node [
    id 614
    label "para"
  ]
  node [
    id 615
    label "Egipt"
  ]
  node [
    id 616
    label "zwrot"
  ]
  node [
    id 617
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 618
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 619
    label "Mozambik"
  ]
  node [
    id 620
    label "Kolumbia"
  ]
  node [
    id 621
    label "Laos"
  ]
  node [
    id 622
    label "Burundi"
  ]
  node [
    id 623
    label "Suazi"
  ]
  node [
    id 624
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 625
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 626
    label "Czechy"
  ]
  node [
    id 627
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 628
    label "Wyspy_Marshalla"
  ]
  node [
    id 629
    label "Dominika"
  ]
  node [
    id 630
    label "Trynidad_i_Tobago"
  ]
  node [
    id 631
    label "Syria"
  ]
  node [
    id 632
    label "Palau"
  ]
  node [
    id 633
    label "Gwinea_Bissau"
  ]
  node [
    id 634
    label "Liberia"
  ]
  node [
    id 635
    label "Jamajka"
  ]
  node [
    id 636
    label "Zimbabwe"
  ]
  node [
    id 637
    label "Polska"
  ]
  node [
    id 638
    label "Dominikana"
  ]
  node [
    id 639
    label "Senegal"
  ]
  node [
    id 640
    label "Togo"
  ]
  node [
    id 641
    label "Gujana"
  ]
  node [
    id 642
    label "Gruzja"
  ]
  node [
    id 643
    label "Albania"
  ]
  node [
    id 644
    label "Zair"
  ]
  node [
    id 645
    label "Meksyk"
  ]
  node [
    id 646
    label "Macedonia"
  ]
  node [
    id 647
    label "Chorwacja"
  ]
  node [
    id 648
    label "Kambod&#380;a"
  ]
  node [
    id 649
    label "Monako"
  ]
  node [
    id 650
    label "Mauritius"
  ]
  node [
    id 651
    label "Gwinea"
  ]
  node [
    id 652
    label "Mali"
  ]
  node [
    id 653
    label "Nigeria"
  ]
  node [
    id 654
    label "Kostaryka"
  ]
  node [
    id 655
    label "Hanower"
  ]
  node [
    id 656
    label "Paragwaj"
  ]
  node [
    id 657
    label "W&#322;ochy"
  ]
  node [
    id 658
    label "Seszele"
  ]
  node [
    id 659
    label "Wyspy_Salomona"
  ]
  node [
    id 660
    label "Hiszpania"
  ]
  node [
    id 661
    label "Boliwia"
  ]
  node [
    id 662
    label "Kirgistan"
  ]
  node [
    id 663
    label "Irlandia"
  ]
  node [
    id 664
    label "Czad"
  ]
  node [
    id 665
    label "Irak"
  ]
  node [
    id 666
    label "Lesoto"
  ]
  node [
    id 667
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 668
    label "Malta"
  ]
  node [
    id 669
    label "Andora"
  ]
  node [
    id 670
    label "Chiny"
  ]
  node [
    id 671
    label "Filipiny"
  ]
  node [
    id 672
    label "Antarktis"
  ]
  node [
    id 673
    label "Niemcy"
  ]
  node [
    id 674
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 675
    label "Pakistan"
  ]
  node [
    id 676
    label "terytorium"
  ]
  node [
    id 677
    label "Nikaragua"
  ]
  node [
    id 678
    label "Brazylia"
  ]
  node [
    id 679
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 680
    label "Maroko"
  ]
  node [
    id 681
    label "Portugalia"
  ]
  node [
    id 682
    label "Niger"
  ]
  node [
    id 683
    label "Kenia"
  ]
  node [
    id 684
    label "Botswana"
  ]
  node [
    id 685
    label "Fid&#380;i"
  ]
  node [
    id 686
    label "Tunezja"
  ]
  node [
    id 687
    label "Australia"
  ]
  node [
    id 688
    label "Tajlandia"
  ]
  node [
    id 689
    label "Burkina_Faso"
  ]
  node [
    id 690
    label "interior"
  ]
  node [
    id 691
    label "Tanzania"
  ]
  node [
    id 692
    label "Benin"
  ]
  node [
    id 693
    label "Indie"
  ]
  node [
    id 694
    label "&#321;otwa"
  ]
  node [
    id 695
    label "Kiribati"
  ]
  node [
    id 696
    label "Antigua_i_Barbuda"
  ]
  node [
    id 697
    label "Rodezja"
  ]
  node [
    id 698
    label "Cypr"
  ]
  node [
    id 699
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 700
    label "Peru"
  ]
  node [
    id 701
    label "Austria"
  ]
  node [
    id 702
    label "Urugwaj"
  ]
  node [
    id 703
    label "Jordania"
  ]
  node [
    id 704
    label "Grecja"
  ]
  node [
    id 705
    label "Azerbejd&#380;an"
  ]
  node [
    id 706
    label "Turcja"
  ]
  node [
    id 707
    label "Samoa"
  ]
  node [
    id 708
    label "Sudan"
  ]
  node [
    id 709
    label "Oman"
  ]
  node [
    id 710
    label "ziemia"
  ]
  node [
    id 711
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 712
    label "Uzbekistan"
  ]
  node [
    id 713
    label "Portoryko"
  ]
  node [
    id 714
    label "Honduras"
  ]
  node [
    id 715
    label "Mongolia"
  ]
  node [
    id 716
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 717
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 718
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 719
    label "Serbia"
  ]
  node [
    id 720
    label "Tajwan"
  ]
  node [
    id 721
    label "Wielka_Brytania"
  ]
  node [
    id 722
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 723
    label "Liban"
  ]
  node [
    id 724
    label "Japonia"
  ]
  node [
    id 725
    label "Ghana"
  ]
  node [
    id 726
    label "Belgia"
  ]
  node [
    id 727
    label "Bahrajn"
  ]
  node [
    id 728
    label "Mikronezja"
  ]
  node [
    id 729
    label "Etiopia"
  ]
  node [
    id 730
    label "Kuwejt"
  ]
  node [
    id 731
    label "grupa"
  ]
  node [
    id 732
    label "Bahamy"
  ]
  node [
    id 733
    label "Rosja"
  ]
  node [
    id 734
    label "Mo&#322;dawia"
  ]
  node [
    id 735
    label "Litwa"
  ]
  node [
    id 736
    label "S&#322;owenia"
  ]
  node [
    id 737
    label "Szwajcaria"
  ]
  node [
    id 738
    label "Erytrea"
  ]
  node [
    id 739
    label "Arabia_Saudyjska"
  ]
  node [
    id 740
    label "Kuba"
  ]
  node [
    id 741
    label "granica_pa&#324;stwa"
  ]
  node [
    id 742
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 743
    label "Malezja"
  ]
  node [
    id 744
    label "Korea"
  ]
  node [
    id 745
    label "Jemen"
  ]
  node [
    id 746
    label "Nowa_Zelandia"
  ]
  node [
    id 747
    label "Namibia"
  ]
  node [
    id 748
    label "Nauru"
  ]
  node [
    id 749
    label "holoarktyka"
  ]
  node [
    id 750
    label "Brunei"
  ]
  node [
    id 751
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 752
    label "Khitai"
  ]
  node [
    id 753
    label "Mauretania"
  ]
  node [
    id 754
    label "Iran"
  ]
  node [
    id 755
    label "Gambia"
  ]
  node [
    id 756
    label "Somalia"
  ]
  node [
    id 757
    label "Holandia"
  ]
  node [
    id 758
    label "Turkmenistan"
  ]
  node [
    id 759
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 760
    label "Salwador"
  ]
  node [
    id 761
    label "wydoro&#347;lenie"
  ]
  node [
    id 762
    label "du&#380;y"
  ]
  node [
    id 763
    label "doro&#347;lenie"
  ]
  node [
    id 764
    label "&#378;ra&#322;y"
  ]
  node [
    id 765
    label "doro&#347;le"
  ]
  node [
    id 766
    label "senior"
  ]
  node [
    id 767
    label "dojrzale"
  ]
  node [
    id 768
    label "wapniak"
  ]
  node [
    id 769
    label "dojrza&#322;y"
  ]
  node [
    id 770
    label "m&#261;dry"
  ]
  node [
    id 771
    label "doletni"
  ]
  node [
    id 772
    label "ludzko&#347;&#263;"
  ]
  node [
    id 773
    label "asymilowanie"
  ]
  node [
    id 774
    label "asymilowa&#263;"
  ]
  node [
    id 775
    label "os&#322;abia&#263;"
  ]
  node [
    id 776
    label "posta&#263;"
  ]
  node [
    id 777
    label "hominid"
  ]
  node [
    id 778
    label "podw&#322;adny"
  ]
  node [
    id 779
    label "os&#322;abianie"
  ]
  node [
    id 780
    label "g&#322;owa"
  ]
  node [
    id 781
    label "figura"
  ]
  node [
    id 782
    label "portrecista"
  ]
  node [
    id 783
    label "dwun&#243;g"
  ]
  node [
    id 784
    label "profanum"
  ]
  node [
    id 785
    label "mikrokosmos"
  ]
  node [
    id 786
    label "nasada"
  ]
  node [
    id 787
    label "duch"
  ]
  node [
    id 788
    label "antropochoria"
  ]
  node [
    id 789
    label "osoba"
  ]
  node [
    id 790
    label "wz&#243;r"
  ]
  node [
    id 791
    label "oddzia&#322;ywanie"
  ]
  node [
    id 792
    label "Adam"
  ]
  node [
    id 793
    label "homo_sapiens"
  ]
  node [
    id 794
    label "polifag"
  ]
  node [
    id 795
    label "pami&#281;&#263;"
  ]
  node [
    id 796
    label "powierzchnia_zr&#243;wnania"
  ]
  node [
    id 797
    label "zapalenie"
  ]
  node [
    id 798
    label "drewno_wt&#243;rne"
  ]
  node [
    id 799
    label "heartwood"
  ]
  node [
    id 800
    label "monolit"
  ]
  node [
    id 801
    label "formacja_skalna"
  ]
  node [
    id 802
    label "klaster_dyskowy"
  ]
  node [
    id 803
    label "komputer"
  ]
  node [
    id 804
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 805
    label "hard_disc"
  ]
  node [
    id 806
    label "&#347;mia&#322;ek"
  ]
  node [
    id 807
    label "kszta&#322;ciciel"
  ]
  node [
    id 808
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 809
    label "kuwada"
  ]
  node [
    id 810
    label "tworzyciel"
  ]
  node [
    id 811
    label "rodzice"
  ]
  node [
    id 812
    label "&#347;w"
  ]
  node [
    id 813
    label "pomys&#322;odawca"
  ]
  node [
    id 814
    label "rodzic"
  ]
  node [
    id 815
    label "wykonawca"
  ]
  node [
    id 816
    label "ojczym"
  ]
  node [
    id 817
    label "przodek"
  ]
  node [
    id 818
    label "papa"
  ]
  node [
    id 819
    label "zakonnik"
  ]
  node [
    id 820
    label "stary"
  ]
  node [
    id 821
    label "kochanek"
  ]
  node [
    id 822
    label "fio&#322;ek"
  ]
  node [
    id 823
    label "facet"
  ]
  node [
    id 824
    label "brat"
  ]
  node [
    id 825
    label "zwierz&#281;"
  ]
  node [
    id 826
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 827
    label "ma&#322;&#380;onek"
  ]
  node [
    id 828
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 829
    label "m&#243;j"
  ]
  node [
    id 830
    label "ch&#322;op"
  ]
  node [
    id 831
    label "pan_m&#322;ody"
  ]
  node [
    id 832
    label "&#347;lubny"
  ]
  node [
    id 833
    label "pan_domu"
  ]
  node [
    id 834
    label "pan_i_w&#322;adca"
  ]
  node [
    id 835
    label "mo&#347;&#263;"
  ]
  node [
    id 836
    label "specjalista"
  ]
  node [
    id 837
    label "&#380;ycie"
  ]
  node [
    id 838
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 839
    label "czas_wolny"
  ]
  node [
    id 840
    label "&#380;ona"
  ]
  node [
    id 841
    label "samica"
  ]
  node [
    id 842
    label "uleganie"
  ]
  node [
    id 843
    label "ulec"
  ]
  node [
    id 844
    label "m&#281;&#380;yna"
  ]
  node [
    id 845
    label "partnerka"
  ]
  node [
    id 846
    label "ulegni&#281;cie"
  ]
  node [
    id 847
    label "&#322;ono"
  ]
  node [
    id 848
    label "menopauza"
  ]
  node [
    id 849
    label "przekwitanie"
  ]
  node [
    id 850
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 851
    label "babka"
  ]
  node [
    id 852
    label "ulega&#263;"
  ]
  node [
    id 853
    label "aktorka"
  ]
  node [
    id 854
    label "partner"
  ]
  node [
    id 855
    label "kobita"
  ]
  node [
    id 856
    label "&#347;lubna"
  ]
  node [
    id 857
    label "panna_m&#322;oda"
  ]
  node [
    id 858
    label "zezwalanie"
  ]
  node [
    id 859
    label "return"
  ]
  node [
    id 860
    label "zaliczanie"
  ]
  node [
    id 861
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 862
    label "poddawanie"
  ]
  node [
    id 863
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 864
    label "burst"
  ]
  node [
    id 865
    label "przywo&#322;anie"
  ]
  node [
    id 866
    label "naginanie_si&#281;"
  ]
  node [
    id 867
    label "poddawanie_si&#281;"
  ]
  node [
    id 868
    label "stawanie_si&#281;"
  ]
  node [
    id 869
    label "przywo&#322;a&#263;"
  ]
  node [
    id 870
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 871
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 872
    label "poddawa&#263;"
  ]
  node [
    id 873
    label "postpone"
  ]
  node [
    id 874
    label "render"
  ]
  node [
    id 875
    label "zezwala&#263;"
  ]
  node [
    id 876
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 877
    label "subject"
  ]
  node [
    id 878
    label "poddanie_si&#281;"
  ]
  node [
    id 879
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 880
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 881
    label "poddanie"
  ]
  node [
    id 882
    label "nagi&#281;cie_si&#281;"
  ]
  node [
    id 883
    label "pozwolenie"
  ]
  node [
    id 884
    label "subjugation"
  ]
  node [
    id 885
    label "stanie_si&#281;"
  ]
  node [
    id 886
    label "kwitnienie"
  ]
  node [
    id 887
    label "przemijanie"
  ]
  node [
    id 888
    label "przestawanie"
  ]
  node [
    id 889
    label "starzenie_si&#281;"
  ]
  node [
    id 890
    label "menopause"
  ]
  node [
    id 891
    label "obumieranie"
  ]
  node [
    id 892
    label "dojrzewanie"
  ]
  node [
    id 893
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 894
    label "sta&#263;_si&#281;"
  ]
  node [
    id 895
    label "fall"
  ]
  node [
    id 896
    label "give"
  ]
  node [
    id 897
    label "pozwoli&#263;"
  ]
  node [
    id 898
    label "podda&#263;"
  ]
  node [
    id 899
    label "put_in"
  ]
  node [
    id 900
    label "podda&#263;_si&#281;"
  ]
  node [
    id 901
    label "klatka_piersiowa"
  ]
  node [
    id 902
    label "penis"
  ]
  node [
    id 903
    label "wzg&#243;rek_&#322;onowy"
  ]
  node [
    id 904
    label "brzuch"
  ]
  node [
    id 905
    label "ow&#322;osienie_&#322;onowe"
  ]
  node [
    id 906
    label "podbrzusze"
  ]
  node [
    id 907
    label "przyroda"
  ]
  node [
    id 908
    label "l&#281;d&#378;wie"
  ]
  node [
    id 909
    label "wn&#281;trze"
  ]
  node [
    id 910
    label "dziedzina"
  ]
  node [
    id 911
    label "powierzchnia"
  ]
  node [
    id 912
    label "macica"
  ]
  node [
    id 913
    label "pochwa"
  ]
  node [
    id 914
    label "przodkini"
  ]
  node [
    id 915
    label "baba"
  ]
  node [
    id 916
    label "babulinka"
  ]
  node [
    id 917
    label "ciasto"
  ]
  node [
    id 918
    label "ro&#347;lina_zielna"
  ]
  node [
    id 919
    label "babkowate"
  ]
  node [
    id 920
    label "po&#322;o&#380;na"
  ]
  node [
    id 921
    label "dziadkowie"
  ]
  node [
    id 922
    label "ryba"
  ]
  node [
    id 923
    label "ko&#378;larz_babka"
  ]
  node [
    id 924
    label "moneta"
  ]
  node [
    id 925
    label "plantain"
  ]
  node [
    id 926
    label "starszy_cz&#322;owiek"
  ]
  node [
    id 927
    label "samka"
  ]
  node [
    id 928
    label "p&#322;e&#263;_pi&#281;kna"
  ]
  node [
    id 929
    label "drogi_rodne"
  ]
  node [
    id 930
    label "female"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 7
    target 718
  ]
  edge [
    source 7
    target 719
  ]
  edge [
    source 7
    target 720
  ]
  edge [
    source 7
    target 721
  ]
  edge [
    source 7
    target 722
  ]
  edge [
    source 7
    target 723
  ]
  edge [
    source 7
    target 724
  ]
  edge [
    source 7
    target 725
  ]
  edge [
    source 7
    target 726
  ]
  edge [
    source 7
    target 727
  ]
  edge [
    source 7
    target 728
  ]
  edge [
    source 7
    target 729
  ]
  edge [
    source 7
    target 730
  ]
  edge [
    source 7
    target 731
  ]
  edge [
    source 7
    target 732
  ]
  edge [
    source 7
    target 733
  ]
  edge [
    source 7
    target 734
  ]
  edge [
    source 7
    target 735
  ]
  edge [
    source 7
    target 736
  ]
  edge [
    source 7
    target 737
  ]
  edge [
    source 7
    target 738
  ]
  edge [
    source 7
    target 739
  ]
  edge [
    source 7
    target 740
  ]
  edge [
    source 7
    target 741
  ]
  edge [
    source 7
    target 742
  ]
  edge [
    source 7
    target 743
  ]
  edge [
    source 7
    target 744
  ]
  edge [
    source 7
    target 745
  ]
  edge [
    source 7
    target 746
  ]
  edge [
    source 7
    target 747
  ]
  edge [
    source 7
    target 748
  ]
  edge [
    source 7
    target 749
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 751
  ]
  edge [
    source 7
    target 752
  ]
  edge [
    source 7
    target 753
  ]
  edge [
    source 7
    target 754
  ]
  edge [
    source 7
    target 755
  ]
  edge [
    source 7
    target 756
  ]
  edge [
    source 7
    target 757
  ]
  edge [
    source 7
    target 758
  ]
  edge [
    source 7
    target 759
  ]
  edge [
    source 7
    target 760
  ]
  edge [
    source 7
    target 761
  ]
  edge [
    source 7
    target 762
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 763
  ]
  edge [
    source 7
    target 764
  ]
  edge [
    source 7
    target 765
  ]
  edge [
    source 7
    target 766
  ]
  edge [
    source 7
    target 767
  ]
  edge [
    source 7
    target 768
  ]
  edge [
    source 7
    target 769
  ]
  edge [
    source 7
    target 770
  ]
  edge [
    source 7
    target 771
  ]
  edge [
    source 7
    target 772
  ]
  edge [
    source 7
    target 773
  ]
  edge [
    source 7
    target 774
  ]
  edge [
    source 7
    target 775
  ]
  edge [
    source 7
    target 776
  ]
  edge [
    source 7
    target 777
  ]
  edge [
    source 7
    target 778
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 780
  ]
  edge [
    source 7
    target 781
  ]
  edge [
    source 7
    target 782
  ]
  edge [
    source 7
    target 783
  ]
  edge [
    source 7
    target 784
  ]
  edge [
    source 7
    target 785
  ]
  edge [
    source 7
    target 786
  ]
  edge [
    source 7
    target 787
  ]
  edge [
    source 7
    target 788
  ]
  edge [
    source 7
    target 789
  ]
  edge [
    source 7
    target 790
  ]
  edge [
    source 7
    target 791
  ]
  edge [
    source 7
    target 792
  ]
  edge [
    source 7
    target 793
  ]
  edge [
    source 7
    target 794
  ]
  edge [
    source 7
    target 795
  ]
  edge [
    source 7
    target 796
  ]
  edge [
    source 7
    target 797
  ]
  edge [
    source 7
    target 798
  ]
  edge [
    source 7
    target 799
  ]
  edge [
    source 7
    target 800
  ]
  edge [
    source 7
    target 801
  ]
  edge [
    source 7
    target 802
  ]
  edge [
    source 7
    target 803
  ]
  edge [
    source 7
    target 804
  ]
  edge [
    source 7
    target 805
  ]
  edge [
    source 7
    target 806
  ]
  edge [
    source 7
    target 807
  ]
  edge [
    source 7
    target 808
  ]
  edge [
    source 7
    target 809
  ]
  edge [
    source 7
    target 810
  ]
  edge [
    source 7
    target 811
  ]
  edge [
    source 7
    target 812
  ]
  edge [
    source 7
    target 813
  ]
  edge [
    source 7
    target 814
  ]
  edge [
    source 7
    target 815
  ]
  edge [
    source 7
    target 816
  ]
  edge [
    source 7
    target 817
  ]
  edge [
    source 7
    target 818
  ]
  edge [
    source 7
    target 819
  ]
  edge [
    source 7
    target 820
  ]
  edge [
    source 7
    target 821
  ]
  edge [
    source 7
    target 822
  ]
  edge [
    source 7
    target 823
  ]
  edge [
    source 7
    target 824
  ]
  edge [
    source 7
    target 825
  ]
  edge [
    source 7
    target 826
  ]
  edge [
    source 7
    target 827
  ]
  edge [
    source 7
    target 828
  ]
  edge [
    source 7
    target 829
  ]
  edge [
    source 7
    target 830
  ]
  edge [
    source 7
    target 831
  ]
  edge [
    source 7
    target 832
  ]
  edge [
    source 7
    target 833
  ]
  edge [
    source 7
    target 834
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 836
  ]
  edge [
    source 7
    target 837
  ]
  edge [
    source 7
    target 838
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 839
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 10
    target 867
  ]
  edge [
    source 10
    target 868
  ]
  edge [
    source 10
    target 869
  ]
  edge [
    source 10
    target 870
  ]
  edge [
    source 10
    target 871
  ]
  edge [
    source 10
    target 872
  ]
  edge [
    source 10
    target 873
  ]
  edge [
    source 10
    target 874
  ]
  edge [
    source 10
    target 875
  ]
  edge [
    source 10
    target 876
  ]
  edge [
    source 10
    target 877
  ]
  edge [
    source 10
    target 878
  ]
  edge [
    source 10
    target 879
  ]
  edge [
    source 10
    target 880
  ]
  edge [
    source 10
    target 881
  ]
  edge [
    source 10
    target 882
  ]
  edge [
    source 10
    target 883
  ]
  edge [
    source 10
    target 884
  ]
  edge [
    source 10
    target 885
  ]
  edge [
    source 10
    target 886
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 887
  ]
  edge [
    source 10
    target 888
  ]
  edge [
    source 10
    target 889
  ]
  edge [
    source 10
    target 890
  ]
  edge [
    source 10
    target 891
  ]
  edge [
    source 10
    target 892
  ]
  edge [
    source 10
    target 893
  ]
  edge [
    source 10
    target 894
  ]
  edge [
    source 10
    target 895
  ]
  edge [
    source 10
    target 896
  ]
  edge [
    source 10
    target 897
  ]
  edge [
    source 10
    target 898
  ]
  edge [
    source 10
    target 899
  ]
  edge [
    source 10
    target 900
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 901
  ]
  edge [
    source 10
    target 902
  ]
  edge [
    source 10
    target 903
  ]
  edge [
    source 10
    target 904
  ]
  edge [
    source 10
    target 905
  ]
  edge [
    source 10
    target 906
  ]
  edge [
    source 10
    target 907
  ]
  edge [
    source 10
    target 908
  ]
  edge [
    source 10
    target 909
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 910
  ]
  edge [
    source 10
    target 911
  ]
  edge [
    source 10
    target 912
  ]
  edge [
    source 10
    target 913
  ]
  edge [
    source 10
    target 914
  ]
  edge [
    source 10
    target 915
  ]
  edge [
    source 10
    target 916
  ]
  edge [
    source 10
    target 917
  ]
  edge [
    source 10
    target 918
  ]
  edge [
    source 10
    target 919
  ]
  edge [
    source 10
    target 920
  ]
  edge [
    source 10
    target 921
  ]
  edge [
    source 10
    target 922
  ]
  edge [
    source 10
    target 923
  ]
  edge [
    source 10
    target 924
  ]
  edge [
    source 10
    target 925
  ]
  edge [
    source 10
    target 926
  ]
  edge [
    source 10
    target 927
  ]
  edge [
    source 10
    target 928
  ]
  edge [
    source 10
    target 929
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 930
  ]
]
