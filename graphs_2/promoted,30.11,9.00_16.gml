graph [
  node [
    id 0
    label "mimo"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "ogl&#261;dalno&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "tvp"
    origin "text"
  ]
  node [
    id 4
    label "stacja"
    origin "text"
  ]
  node [
    id 5
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "zdj&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "program"
    origin "text"
  ]
  node [
    id 8
    label "sw&#243;j"
    origin "text"
  ]
  node [
    id 9
    label "antenty"
    origin "text"
  ]
  node [
    id 10
    label "dobroczynny"
  ]
  node [
    id 11
    label "czw&#243;rka"
  ]
  node [
    id 12
    label "spokojny"
  ]
  node [
    id 13
    label "skuteczny"
  ]
  node [
    id 14
    label "&#347;mieszny"
  ]
  node [
    id 15
    label "mi&#322;y"
  ]
  node [
    id 16
    label "grzeczny"
  ]
  node [
    id 17
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 18
    label "powitanie"
  ]
  node [
    id 19
    label "dobrze"
  ]
  node [
    id 20
    label "ca&#322;y"
  ]
  node [
    id 21
    label "zwrot"
  ]
  node [
    id 22
    label "pomy&#347;lny"
  ]
  node [
    id 23
    label "moralny"
  ]
  node [
    id 24
    label "drogi"
  ]
  node [
    id 25
    label "pozytywny"
  ]
  node [
    id 26
    label "odpowiedni"
  ]
  node [
    id 27
    label "korzystny"
  ]
  node [
    id 28
    label "pos&#322;uszny"
  ]
  node [
    id 29
    label "moralnie"
  ]
  node [
    id 30
    label "warto&#347;ciowy"
  ]
  node [
    id 31
    label "etycznie"
  ]
  node [
    id 32
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 33
    label "nale&#380;ny"
  ]
  node [
    id 34
    label "nale&#380;yty"
  ]
  node [
    id 35
    label "typowy"
  ]
  node [
    id 36
    label "uprawniony"
  ]
  node [
    id 37
    label "zasadniczy"
  ]
  node [
    id 38
    label "stosownie"
  ]
  node [
    id 39
    label "taki"
  ]
  node [
    id 40
    label "charakterystyczny"
  ]
  node [
    id 41
    label "prawdziwy"
  ]
  node [
    id 42
    label "ten"
  ]
  node [
    id 43
    label "pozytywnie"
  ]
  node [
    id 44
    label "fajny"
  ]
  node [
    id 45
    label "dodatnio"
  ]
  node [
    id 46
    label "przyjemny"
  ]
  node [
    id 47
    label "po&#380;&#261;dany"
  ]
  node [
    id 48
    label "niepowa&#380;ny"
  ]
  node [
    id 49
    label "o&#347;mieszanie"
  ]
  node [
    id 50
    label "&#347;miesznie"
  ]
  node [
    id 51
    label "bawny"
  ]
  node [
    id 52
    label "o&#347;mieszenie"
  ]
  node [
    id 53
    label "dziwny"
  ]
  node [
    id 54
    label "nieadekwatny"
  ]
  node [
    id 55
    label "zale&#380;ny"
  ]
  node [
    id 56
    label "uleg&#322;y"
  ]
  node [
    id 57
    label "pos&#322;usznie"
  ]
  node [
    id 58
    label "grzecznie"
  ]
  node [
    id 59
    label "stosowny"
  ]
  node [
    id 60
    label "niewinny"
  ]
  node [
    id 61
    label "konserwatywny"
  ]
  node [
    id 62
    label "nijaki"
  ]
  node [
    id 63
    label "wolny"
  ]
  node [
    id 64
    label "uspokajanie_si&#281;"
  ]
  node [
    id 65
    label "bezproblemowy"
  ]
  node [
    id 66
    label "spokojnie"
  ]
  node [
    id 67
    label "uspokojenie_si&#281;"
  ]
  node [
    id 68
    label "cicho"
  ]
  node [
    id 69
    label "uspokojenie"
  ]
  node [
    id 70
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 71
    label "nietrudny"
  ]
  node [
    id 72
    label "uspokajanie"
  ]
  node [
    id 73
    label "korzystnie"
  ]
  node [
    id 74
    label "drogo"
  ]
  node [
    id 75
    label "cz&#322;owiek"
  ]
  node [
    id 76
    label "bliski"
  ]
  node [
    id 77
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 78
    label "przyjaciel"
  ]
  node [
    id 79
    label "jedyny"
  ]
  node [
    id 80
    label "du&#380;y"
  ]
  node [
    id 81
    label "zdr&#243;w"
  ]
  node [
    id 82
    label "calu&#347;ko"
  ]
  node [
    id 83
    label "kompletny"
  ]
  node [
    id 84
    label "&#380;ywy"
  ]
  node [
    id 85
    label "pe&#322;ny"
  ]
  node [
    id 86
    label "podobny"
  ]
  node [
    id 87
    label "ca&#322;o"
  ]
  node [
    id 88
    label "poskutkowanie"
  ]
  node [
    id 89
    label "sprawny"
  ]
  node [
    id 90
    label "skutecznie"
  ]
  node [
    id 91
    label "skutkowanie"
  ]
  node [
    id 92
    label "pomy&#347;lnie"
  ]
  node [
    id 93
    label "toto-lotek"
  ]
  node [
    id 94
    label "trafienie"
  ]
  node [
    id 95
    label "zbi&#243;r"
  ]
  node [
    id 96
    label "arkusz_drukarski"
  ]
  node [
    id 97
    label "&#322;&#243;dka"
  ]
  node [
    id 98
    label "four"
  ]
  node [
    id 99
    label "&#263;wiartka"
  ]
  node [
    id 100
    label "hotel"
  ]
  node [
    id 101
    label "cyfra"
  ]
  node [
    id 102
    label "pok&#243;j"
  ]
  node [
    id 103
    label "stopie&#324;"
  ]
  node [
    id 104
    label "obiekt"
  ]
  node [
    id 105
    label "minialbum"
  ]
  node [
    id 106
    label "osada"
  ]
  node [
    id 107
    label "p&#322;yta_winylowa"
  ]
  node [
    id 108
    label "blotka"
  ]
  node [
    id 109
    label "zaprz&#281;g"
  ]
  node [
    id 110
    label "przedtrzonowiec"
  ]
  node [
    id 111
    label "punkt"
  ]
  node [
    id 112
    label "turn"
  ]
  node [
    id 113
    label "turning"
  ]
  node [
    id 114
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 115
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 116
    label "skr&#281;t"
  ]
  node [
    id 117
    label "obr&#243;t"
  ]
  node [
    id 118
    label "fraza_czasownikowa"
  ]
  node [
    id 119
    label "jednostka_leksykalna"
  ]
  node [
    id 120
    label "zmiana"
  ]
  node [
    id 121
    label "wyra&#380;enie"
  ]
  node [
    id 122
    label "welcome"
  ]
  node [
    id 123
    label "spotkanie"
  ]
  node [
    id 124
    label "pozdrowienie"
  ]
  node [
    id 125
    label "zwyczaj"
  ]
  node [
    id 126
    label "greeting"
  ]
  node [
    id 127
    label "zdarzony"
  ]
  node [
    id 128
    label "odpowiednio"
  ]
  node [
    id 129
    label "odpowiadanie"
  ]
  node [
    id 130
    label "specjalny"
  ]
  node [
    id 131
    label "kochanek"
  ]
  node [
    id 132
    label "sk&#322;onny"
  ]
  node [
    id 133
    label "wybranek"
  ]
  node [
    id 134
    label "umi&#322;owany"
  ]
  node [
    id 135
    label "przyjemnie"
  ]
  node [
    id 136
    label "mi&#322;o"
  ]
  node [
    id 137
    label "kochanie"
  ]
  node [
    id 138
    label "dyplomata"
  ]
  node [
    id 139
    label "dobroczynnie"
  ]
  node [
    id 140
    label "lepiej"
  ]
  node [
    id 141
    label "wiele"
  ]
  node [
    id 142
    label "spo&#322;eczny"
  ]
  node [
    id 143
    label "hearing"
  ]
  node [
    id 144
    label "liczba"
  ]
  node [
    id 145
    label "kategoria"
  ]
  node [
    id 146
    label "pierwiastek"
  ]
  node [
    id 147
    label "rozmiar"
  ]
  node [
    id 148
    label "poj&#281;cie"
  ]
  node [
    id 149
    label "number"
  ]
  node [
    id 150
    label "cecha"
  ]
  node [
    id 151
    label "kategoria_gramatyczna"
  ]
  node [
    id 152
    label "grupa"
  ]
  node [
    id 153
    label "kwadrat_magiczny"
  ]
  node [
    id 154
    label "koniugacja"
  ]
  node [
    id 155
    label "instytucja"
  ]
  node [
    id 156
    label "siedziba"
  ]
  node [
    id 157
    label "miejsce"
  ]
  node [
    id 158
    label "droga_krzy&#380;owa"
  ]
  node [
    id 159
    label "urz&#261;dzenie"
  ]
  node [
    id 160
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 161
    label "przedmiot"
  ]
  node [
    id 162
    label "kom&#243;rka"
  ]
  node [
    id 163
    label "furnishing"
  ]
  node [
    id 164
    label "zabezpieczenie"
  ]
  node [
    id 165
    label "zrobienie"
  ]
  node [
    id 166
    label "wyrz&#261;dzenie"
  ]
  node [
    id 167
    label "zagospodarowanie"
  ]
  node [
    id 168
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 169
    label "ig&#322;a"
  ]
  node [
    id 170
    label "narz&#281;dzie"
  ]
  node [
    id 171
    label "wirnik"
  ]
  node [
    id 172
    label "aparatura"
  ]
  node [
    id 173
    label "system_energetyczny"
  ]
  node [
    id 174
    label "impulsator"
  ]
  node [
    id 175
    label "mechanizm"
  ]
  node [
    id 176
    label "sprz&#281;t"
  ]
  node [
    id 177
    label "czynno&#347;&#263;"
  ]
  node [
    id 178
    label "blokowanie"
  ]
  node [
    id 179
    label "set"
  ]
  node [
    id 180
    label "zablokowanie"
  ]
  node [
    id 181
    label "przygotowanie"
  ]
  node [
    id 182
    label "komora"
  ]
  node [
    id 183
    label "j&#281;zyk"
  ]
  node [
    id 184
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 185
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 186
    label "&#321;ubianka"
  ]
  node [
    id 187
    label "miejsce_pracy"
  ]
  node [
    id 188
    label "dzia&#322;_personalny"
  ]
  node [
    id 189
    label "Kreml"
  ]
  node [
    id 190
    label "Bia&#322;y_Dom"
  ]
  node [
    id 191
    label "budynek"
  ]
  node [
    id 192
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 193
    label "sadowisko"
  ]
  node [
    id 194
    label "po&#322;o&#380;enie"
  ]
  node [
    id 195
    label "sprawa"
  ]
  node [
    id 196
    label "ust&#281;p"
  ]
  node [
    id 197
    label "plan"
  ]
  node [
    id 198
    label "obiekt_matematyczny"
  ]
  node [
    id 199
    label "problemat"
  ]
  node [
    id 200
    label "plamka"
  ]
  node [
    id 201
    label "stopie&#324;_pisma"
  ]
  node [
    id 202
    label "jednostka"
  ]
  node [
    id 203
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 204
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 205
    label "mark"
  ]
  node [
    id 206
    label "chwila"
  ]
  node [
    id 207
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 208
    label "prosta"
  ]
  node [
    id 209
    label "problematyka"
  ]
  node [
    id 210
    label "zapunktowa&#263;"
  ]
  node [
    id 211
    label "podpunkt"
  ]
  node [
    id 212
    label "wojsko"
  ]
  node [
    id 213
    label "kres"
  ]
  node [
    id 214
    label "przestrze&#324;"
  ]
  node [
    id 215
    label "point"
  ]
  node [
    id 216
    label "pozycja"
  ]
  node [
    id 217
    label "warunek_lokalowy"
  ]
  node [
    id 218
    label "plac"
  ]
  node [
    id 219
    label "location"
  ]
  node [
    id 220
    label "uwaga"
  ]
  node [
    id 221
    label "status"
  ]
  node [
    id 222
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 223
    label "cia&#322;o"
  ]
  node [
    id 224
    label "praca"
  ]
  node [
    id 225
    label "rz&#261;d"
  ]
  node [
    id 226
    label "osoba_prawna"
  ]
  node [
    id 227
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 228
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 229
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 230
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 231
    label "biuro"
  ]
  node [
    id 232
    label "organizacja"
  ]
  node [
    id 233
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 234
    label "Fundusze_Unijne"
  ]
  node [
    id 235
    label "zamyka&#263;"
  ]
  node [
    id 236
    label "establishment"
  ]
  node [
    id 237
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 238
    label "urz&#261;d"
  ]
  node [
    id 239
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 240
    label "afiliowa&#263;"
  ]
  node [
    id 241
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 242
    label "standard"
  ]
  node [
    id 243
    label "zamykanie"
  ]
  node [
    id 244
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 245
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 246
    label "podj&#261;&#263;"
  ]
  node [
    id 247
    label "sta&#263;_si&#281;"
  ]
  node [
    id 248
    label "determine"
  ]
  node [
    id 249
    label "zrobi&#263;"
  ]
  node [
    id 250
    label "zareagowa&#263;"
  ]
  node [
    id 251
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 252
    label "draw"
  ]
  node [
    id 253
    label "allude"
  ]
  node [
    id 254
    label "zmieni&#263;"
  ]
  node [
    id 255
    label "zacz&#261;&#263;"
  ]
  node [
    id 256
    label "raise"
  ]
  node [
    id 257
    label "post&#261;pi&#263;"
  ]
  node [
    id 258
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 259
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 260
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 261
    label "zorganizowa&#263;"
  ]
  node [
    id 262
    label "appoint"
  ]
  node [
    id 263
    label "wystylizowa&#263;"
  ]
  node [
    id 264
    label "cause"
  ]
  node [
    id 265
    label "przerobi&#263;"
  ]
  node [
    id 266
    label "nabra&#263;"
  ]
  node [
    id 267
    label "make"
  ]
  node [
    id 268
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 269
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 270
    label "wydali&#263;"
  ]
  node [
    id 271
    label "zabroni&#263;"
  ]
  node [
    id 272
    label "pull"
  ]
  node [
    id 273
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 274
    label "wyuzda&#263;"
  ]
  node [
    id 275
    label "wzi&#261;&#263;"
  ]
  node [
    id 276
    label "odsun&#261;&#263;"
  ]
  node [
    id 277
    label "cenzura"
  ]
  node [
    id 278
    label "uwolni&#263;"
  ]
  node [
    id 279
    label "abolicjonista"
  ]
  node [
    id 280
    label "lift"
  ]
  node [
    id 281
    label "pom&#243;c"
  ]
  node [
    id 282
    label "deliver"
  ]
  node [
    id 283
    label "spowodowa&#263;"
  ]
  node [
    id 284
    label "release"
  ]
  node [
    id 285
    label "wytworzy&#263;"
  ]
  node [
    id 286
    label "wzbudzi&#263;"
  ]
  node [
    id 287
    label "odziedziczy&#263;"
  ]
  node [
    id 288
    label "ruszy&#263;"
  ]
  node [
    id 289
    label "take"
  ]
  node [
    id 290
    label "zaatakowa&#263;"
  ]
  node [
    id 291
    label "skorzysta&#263;"
  ]
  node [
    id 292
    label "uciec"
  ]
  node [
    id 293
    label "receive"
  ]
  node [
    id 294
    label "nakaza&#263;"
  ]
  node [
    id 295
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 296
    label "obskoczy&#263;"
  ]
  node [
    id 297
    label "bra&#263;"
  ]
  node [
    id 298
    label "u&#380;y&#263;"
  ]
  node [
    id 299
    label "get"
  ]
  node [
    id 300
    label "wyrucha&#263;"
  ]
  node [
    id 301
    label "World_Health_Organization"
  ]
  node [
    id 302
    label "wyciupcia&#263;"
  ]
  node [
    id 303
    label "wygra&#263;"
  ]
  node [
    id 304
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 305
    label "withdraw"
  ]
  node [
    id 306
    label "wzi&#281;cie"
  ]
  node [
    id 307
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 308
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 309
    label "poczyta&#263;"
  ]
  node [
    id 310
    label "obj&#261;&#263;"
  ]
  node [
    id 311
    label "seize"
  ]
  node [
    id 312
    label "aim"
  ]
  node [
    id 313
    label "chwyci&#263;"
  ]
  node [
    id 314
    label "przyj&#261;&#263;"
  ]
  node [
    id 315
    label "pokona&#263;"
  ]
  node [
    id 316
    label "arise"
  ]
  node [
    id 317
    label "uda&#263;_si&#281;"
  ]
  node [
    id 318
    label "otrzyma&#263;"
  ]
  node [
    id 319
    label "wej&#347;&#263;"
  ]
  node [
    id 320
    label "poruszy&#263;"
  ]
  node [
    id 321
    label "dosta&#263;"
  ]
  node [
    id 322
    label "jam"
  ]
  node [
    id 323
    label "zerwa&#263;"
  ]
  node [
    id 324
    label "kill"
  ]
  node [
    id 325
    label "bow_out"
  ]
  node [
    id 326
    label "przesta&#263;"
  ]
  node [
    id 327
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 328
    label "decelerate"
  ]
  node [
    id 329
    label "oddali&#263;"
  ]
  node [
    id 330
    label "odw&#322;&#243;czy&#263;"
  ]
  node [
    id 331
    label "przenie&#347;&#263;"
  ]
  node [
    id 332
    label "od&#322;o&#380;y&#263;"
  ]
  node [
    id 333
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 334
    label "przesun&#261;&#263;"
  ]
  node [
    id 335
    label "&#347;wiadectwo"
  ]
  node [
    id 336
    label "proces"
  ]
  node [
    id 337
    label "drugi_obieg"
  ]
  node [
    id 338
    label "zdejmowanie"
  ]
  node [
    id 339
    label "bell_ringer"
  ]
  node [
    id 340
    label "krytyka"
  ]
  node [
    id 341
    label "crisscross"
  ]
  node [
    id 342
    label "p&#243;&#322;kownik"
  ]
  node [
    id 343
    label "ekskomunikowa&#263;"
  ]
  node [
    id 344
    label "kontrola"
  ]
  node [
    id 345
    label "zdejmowa&#263;"
  ]
  node [
    id 346
    label "zjawisko"
  ]
  node [
    id 347
    label "zdj&#281;cie"
  ]
  node [
    id 348
    label "kara"
  ]
  node [
    id 349
    label "ekskomunikowanie"
  ]
  node [
    id 350
    label "przeciwnik"
  ]
  node [
    id 351
    label "znie&#347;&#263;"
  ]
  node [
    id 352
    label "zniesienie"
  ]
  node [
    id 353
    label "zwolennik"
  ]
  node [
    id 354
    label "czarnosk&#243;ry"
  ]
  node [
    id 355
    label "rozpasa&#263;_si&#281;"
  ]
  node [
    id 356
    label "instalowa&#263;"
  ]
  node [
    id 357
    label "oprogramowanie"
  ]
  node [
    id 358
    label "odinstalowywa&#263;"
  ]
  node [
    id 359
    label "spis"
  ]
  node [
    id 360
    label "zaprezentowanie"
  ]
  node [
    id 361
    label "podprogram"
  ]
  node [
    id 362
    label "ogranicznik_referencyjny"
  ]
  node [
    id 363
    label "course_of_study"
  ]
  node [
    id 364
    label "booklet"
  ]
  node [
    id 365
    label "dzia&#322;"
  ]
  node [
    id 366
    label "odinstalowanie"
  ]
  node [
    id 367
    label "broszura"
  ]
  node [
    id 368
    label "wytw&#243;r"
  ]
  node [
    id 369
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 370
    label "kana&#322;"
  ]
  node [
    id 371
    label "teleferie"
  ]
  node [
    id 372
    label "zainstalowanie"
  ]
  node [
    id 373
    label "struktura_organizacyjna"
  ]
  node [
    id 374
    label "pirat"
  ]
  node [
    id 375
    label "zaprezentowa&#263;"
  ]
  node [
    id 376
    label "prezentowanie"
  ]
  node [
    id 377
    label "prezentowa&#263;"
  ]
  node [
    id 378
    label "interfejs"
  ]
  node [
    id 379
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 380
    label "okno"
  ]
  node [
    id 381
    label "blok"
  ]
  node [
    id 382
    label "folder"
  ]
  node [
    id 383
    label "zainstalowa&#263;"
  ]
  node [
    id 384
    label "za&#322;o&#380;enie"
  ]
  node [
    id 385
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 386
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 387
    label "ram&#243;wka"
  ]
  node [
    id 388
    label "tryb"
  ]
  node [
    id 389
    label "emitowa&#263;"
  ]
  node [
    id 390
    label "emitowanie"
  ]
  node [
    id 391
    label "odinstalowywanie"
  ]
  node [
    id 392
    label "instrukcja"
  ]
  node [
    id 393
    label "informatyka"
  ]
  node [
    id 394
    label "deklaracja"
  ]
  node [
    id 395
    label "sekcja_krytyczna"
  ]
  node [
    id 396
    label "menu"
  ]
  node [
    id 397
    label "furkacja"
  ]
  node [
    id 398
    label "podstawa"
  ]
  node [
    id 399
    label "instalowanie"
  ]
  node [
    id 400
    label "oferta"
  ]
  node [
    id 401
    label "odinstalowa&#263;"
  ]
  node [
    id 402
    label "druk_ulotny"
  ]
  node [
    id 403
    label "wydawnictwo"
  ]
  node [
    id 404
    label "pot&#281;ga"
  ]
  node [
    id 405
    label "documentation"
  ]
  node [
    id 406
    label "column"
  ]
  node [
    id 407
    label "zasadzi&#263;"
  ]
  node [
    id 408
    label "punkt_odniesienia"
  ]
  node [
    id 409
    label "zasadzenie"
  ]
  node [
    id 410
    label "bok"
  ]
  node [
    id 411
    label "d&#243;&#322;"
  ]
  node [
    id 412
    label "dzieci&#281;ctwo"
  ]
  node [
    id 413
    label "background"
  ]
  node [
    id 414
    label "podstawowy"
  ]
  node [
    id 415
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 416
    label "strategia"
  ]
  node [
    id 417
    label "pomys&#322;"
  ]
  node [
    id 418
    label "&#347;ciana"
  ]
  node [
    id 419
    label "zakres"
  ]
  node [
    id 420
    label "izochronizm"
  ]
  node [
    id 421
    label "zasi&#261;g"
  ]
  node [
    id 422
    label "bridge"
  ]
  node [
    id 423
    label "distribution"
  ]
  node [
    id 424
    label "p&#322;&#243;d"
  ]
  node [
    id 425
    label "work"
  ]
  node [
    id 426
    label "rezultat"
  ]
  node [
    id 427
    label "ko&#322;o"
  ]
  node [
    id 428
    label "spos&#243;b"
  ]
  node [
    id 429
    label "modalno&#347;&#263;"
  ]
  node [
    id 430
    label "z&#261;b"
  ]
  node [
    id 431
    label "skala"
  ]
  node [
    id 432
    label "funkcjonowa&#263;"
  ]
  node [
    id 433
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 434
    label "offer"
  ]
  node [
    id 435
    label "propozycja"
  ]
  node [
    id 436
    label "o&#347;wiadczenie"
  ]
  node [
    id 437
    label "obietnica"
  ]
  node [
    id 438
    label "formularz"
  ]
  node [
    id 439
    label "statement"
  ]
  node [
    id 440
    label "announcement"
  ]
  node [
    id 441
    label "akt"
  ]
  node [
    id 442
    label "digest"
  ]
  node [
    id 443
    label "konstrukcja"
  ]
  node [
    id 444
    label "dokument"
  ]
  node [
    id 445
    label "o&#347;wiadczyny"
  ]
  node [
    id 446
    label "szaniec"
  ]
  node [
    id 447
    label "topologia_magistrali"
  ]
  node [
    id 448
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 449
    label "grodzisko"
  ]
  node [
    id 450
    label "tarapaty"
  ]
  node [
    id 451
    label "piaskownik"
  ]
  node [
    id 452
    label "struktura_anatomiczna"
  ]
  node [
    id 453
    label "bystrza"
  ]
  node [
    id 454
    label "pit"
  ]
  node [
    id 455
    label "odk&#322;ad"
  ]
  node [
    id 456
    label "chody"
  ]
  node [
    id 457
    label "klarownia"
  ]
  node [
    id 458
    label "kanalizacja"
  ]
  node [
    id 459
    label "przew&#243;d"
  ]
  node [
    id 460
    label "budowa"
  ]
  node [
    id 461
    label "ciek"
  ]
  node [
    id 462
    label "teatr"
  ]
  node [
    id 463
    label "gara&#380;"
  ]
  node [
    id 464
    label "zrzutowy"
  ]
  node [
    id 465
    label "warsztat"
  ]
  node [
    id 466
    label "syfon"
  ]
  node [
    id 467
    label "odwa&#322;"
  ]
  node [
    id 468
    label "catalog"
  ]
  node [
    id 469
    label "tekst"
  ]
  node [
    id 470
    label "sumariusz"
  ]
  node [
    id 471
    label "book"
  ]
  node [
    id 472
    label "stock"
  ]
  node [
    id 473
    label "figurowa&#263;"
  ]
  node [
    id 474
    label "wyliczanka"
  ]
  node [
    id 475
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 476
    label "HP"
  ]
  node [
    id 477
    label "dost&#281;p"
  ]
  node [
    id 478
    label "infa"
  ]
  node [
    id 479
    label "kierunek"
  ]
  node [
    id 480
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 481
    label "kryptologia"
  ]
  node [
    id 482
    label "baza_danych"
  ]
  node [
    id 483
    label "przetwarzanie_informacji"
  ]
  node [
    id 484
    label "sztuczna_inteligencja"
  ]
  node [
    id 485
    label "gramatyka_formalna"
  ]
  node [
    id 486
    label "zamek"
  ]
  node [
    id 487
    label "dziedzina_informatyki"
  ]
  node [
    id 488
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 489
    label "artefakt"
  ]
  node [
    id 490
    label "usuni&#281;cie"
  ]
  node [
    id 491
    label "parapet"
  ]
  node [
    id 492
    label "szyba"
  ]
  node [
    id 493
    label "okiennica"
  ]
  node [
    id 494
    label "prze&#347;wit"
  ]
  node [
    id 495
    label "pulpit"
  ]
  node [
    id 496
    label "transenna"
  ]
  node [
    id 497
    label "kwatera_okienna"
  ]
  node [
    id 498
    label "inspekt"
  ]
  node [
    id 499
    label "nora"
  ]
  node [
    id 500
    label "skrzyd&#322;o"
  ]
  node [
    id 501
    label "nadokiennik"
  ]
  node [
    id 502
    label "futryna"
  ]
  node [
    id 503
    label "lufcik"
  ]
  node [
    id 504
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 505
    label "casement"
  ]
  node [
    id 506
    label "menad&#380;er_okien"
  ]
  node [
    id 507
    label "otw&#243;r"
  ]
  node [
    id 508
    label "dostosowywa&#263;"
  ]
  node [
    id 509
    label "supply"
  ]
  node [
    id 510
    label "robi&#263;"
  ]
  node [
    id 511
    label "accommodate"
  ]
  node [
    id 512
    label "komputer"
  ]
  node [
    id 513
    label "umieszcza&#263;"
  ]
  node [
    id 514
    label "fit"
  ]
  node [
    id 515
    label "zdolno&#347;&#263;"
  ]
  node [
    id 516
    label "usuwa&#263;"
  ]
  node [
    id 517
    label "usuwanie"
  ]
  node [
    id 518
    label "dostosowa&#263;"
  ]
  node [
    id 519
    label "install"
  ]
  node [
    id 520
    label "umie&#347;ci&#263;"
  ]
  node [
    id 521
    label "umieszczanie"
  ]
  node [
    id 522
    label "installation"
  ]
  node [
    id 523
    label "collection"
  ]
  node [
    id 524
    label "robienie"
  ]
  node [
    id 525
    label "wmontowanie"
  ]
  node [
    id 526
    label "wmontowywanie"
  ]
  node [
    id 527
    label "fitting"
  ]
  node [
    id 528
    label "dostosowywanie"
  ]
  node [
    id 529
    label "usun&#261;&#263;"
  ]
  node [
    id 530
    label "dostosowanie"
  ]
  node [
    id 531
    label "pozak&#322;adanie"
  ]
  node [
    id 532
    label "proposition"
  ]
  node [
    id 533
    label "layout"
  ]
  node [
    id 534
    label "umieszczenie"
  ]
  node [
    id 535
    label "przest&#281;pca"
  ]
  node [
    id 536
    label "kopiowa&#263;"
  ]
  node [
    id 537
    label "podr&#243;bka"
  ]
  node [
    id 538
    label "kieruj&#261;cy"
  ]
  node [
    id 539
    label "&#380;agl&#243;wka"
  ]
  node [
    id 540
    label "rum"
  ]
  node [
    id 541
    label "rozb&#243;jnik"
  ]
  node [
    id 542
    label "postrzeleniec"
  ]
  node [
    id 543
    label "testify"
  ]
  node [
    id 544
    label "przedstawi&#263;"
  ]
  node [
    id 545
    label "zapozna&#263;"
  ]
  node [
    id 546
    label "pokaza&#263;"
  ]
  node [
    id 547
    label "represent"
  ]
  node [
    id 548
    label "typify"
  ]
  node [
    id 549
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 550
    label "uprzedzi&#263;"
  ]
  node [
    id 551
    label "attest"
  ]
  node [
    id 552
    label "wyra&#380;anie"
  ]
  node [
    id 553
    label "uprzedzanie"
  ]
  node [
    id 554
    label "representation"
  ]
  node [
    id 555
    label "zapoznawanie"
  ]
  node [
    id 556
    label "present"
  ]
  node [
    id 557
    label "przedstawianie"
  ]
  node [
    id 558
    label "display"
  ]
  node [
    id 559
    label "demonstrowanie"
  ]
  node [
    id 560
    label "presentation"
  ]
  node [
    id 561
    label "granie"
  ]
  node [
    id 562
    label "zapoznanie"
  ]
  node [
    id 563
    label "zapoznanie_si&#281;"
  ]
  node [
    id 564
    label "exhibit"
  ]
  node [
    id 565
    label "pokazanie"
  ]
  node [
    id 566
    label "wyst&#261;pienie"
  ]
  node [
    id 567
    label "uprzedzenie"
  ]
  node [
    id 568
    label "przedstawienie"
  ]
  node [
    id 569
    label "gra&#263;"
  ]
  node [
    id 570
    label "zapoznawa&#263;"
  ]
  node [
    id 571
    label "uprzedza&#263;"
  ]
  node [
    id 572
    label "wyra&#380;a&#263;"
  ]
  node [
    id 573
    label "przedstawia&#263;"
  ]
  node [
    id 574
    label "rynek"
  ]
  node [
    id 575
    label "nadawa&#263;"
  ]
  node [
    id 576
    label "wysy&#322;a&#263;"
  ]
  node [
    id 577
    label "energia"
  ]
  node [
    id 578
    label "nada&#263;"
  ]
  node [
    id 579
    label "tembr"
  ]
  node [
    id 580
    label "air"
  ]
  node [
    id 581
    label "wydoby&#263;"
  ]
  node [
    id 582
    label "emit"
  ]
  node [
    id 583
    label "wys&#322;a&#263;"
  ]
  node [
    id 584
    label "wydzieli&#263;"
  ]
  node [
    id 585
    label "wydziela&#263;"
  ]
  node [
    id 586
    label "wprowadzi&#263;"
  ]
  node [
    id 587
    label "wydobywa&#263;"
  ]
  node [
    id 588
    label "wprowadza&#263;"
  ]
  node [
    id 589
    label "wysy&#322;anie"
  ]
  node [
    id 590
    label "wys&#322;anie"
  ]
  node [
    id 591
    label "wydzielenie"
  ]
  node [
    id 592
    label "wprowadzenie"
  ]
  node [
    id 593
    label "wydobycie"
  ]
  node [
    id 594
    label "wydzielanie"
  ]
  node [
    id 595
    label "wydobywanie"
  ]
  node [
    id 596
    label "nadawanie"
  ]
  node [
    id 597
    label "emission"
  ]
  node [
    id 598
    label "wprowadzanie"
  ]
  node [
    id 599
    label "nadanie"
  ]
  node [
    id 600
    label "issue"
  ]
  node [
    id 601
    label "ulotka"
  ]
  node [
    id 602
    label "wskaz&#243;wka"
  ]
  node [
    id 603
    label "instruktarz"
  ]
  node [
    id 604
    label "danie"
  ]
  node [
    id 605
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 606
    label "restauracja"
  ]
  node [
    id 607
    label "cennik"
  ]
  node [
    id 608
    label "chart"
  ]
  node [
    id 609
    label "karta"
  ]
  node [
    id 610
    label "zestaw"
  ]
  node [
    id 611
    label "bajt"
  ]
  node [
    id 612
    label "bloking"
  ]
  node [
    id 613
    label "j&#261;kanie"
  ]
  node [
    id 614
    label "przeszkoda"
  ]
  node [
    id 615
    label "zesp&#243;&#322;"
  ]
  node [
    id 616
    label "blokada"
  ]
  node [
    id 617
    label "bry&#322;a"
  ]
  node [
    id 618
    label "kontynent"
  ]
  node [
    id 619
    label "nastawnia"
  ]
  node [
    id 620
    label "blockage"
  ]
  node [
    id 621
    label "block"
  ]
  node [
    id 622
    label "start"
  ]
  node [
    id 623
    label "skorupa_ziemska"
  ]
  node [
    id 624
    label "zeszyt"
  ]
  node [
    id 625
    label "blokowisko"
  ]
  node [
    id 626
    label "artyku&#322;"
  ]
  node [
    id 627
    label "barak"
  ]
  node [
    id 628
    label "stok_kontynentalny"
  ]
  node [
    id 629
    label "whole"
  ]
  node [
    id 630
    label "square"
  ]
  node [
    id 631
    label "siatk&#243;wka"
  ]
  node [
    id 632
    label "kr&#261;g"
  ]
  node [
    id 633
    label "obrona"
  ]
  node [
    id 634
    label "ok&#322;adka"
  ]
  node [
    id 635
    label "bie&#380;nia"
  ]
  node [
    id 636
    label "referat"
  ]
  node [
    id 637
    label "dom_wielorodzinny"
  ]
  node [
    id 638
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 639
    label "routine"
  ]
  node [
    id 640
    label "proceduralnie"
  ]
  node [
    id 641
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 642
    label "jednostka_organizacyjna"
  ]
  node [
    id 643
    label "sfera"
  ]
  node [
    id 644
    label "insourcing"
  ]
  node [
    id 645
    label "competence"
  ]
  node [
    id 646
    label "bezdro&#380;e"
  ]
  node [
    id 647
    label "poddzia&#322;"
  ]
  node [
    id 648
    label "podwini&#281;cie"
  ]
  node [
    id 649
    label "zap&#322;acenie"
  ]
  node [
    id 650
    label "przyodzianie"
  ]
  node [
    id 651
    label "budowla"
  ]
  node [
    id 652
    label "pokrycie"
  ]
  node [
    id 653
    label "rozebranie"
  ]
  node [
    id 654
    label "zak&#322;adka"
  ]
  node [
    id 655
    label "struktura"
  ]
  node [
    id 656
    label "poubieranie"
  ]
  node [
    id 657
    label "infliction"
  ]
  node [
    id 658
    label "spowodowanie"
  ]
  node [
    id 659
    label "przebranie"
  ]
  node [
    id 660
    label "przywdzianie"
  ]
  node [
    id 661
    label "obleczenie_si&#281;"
  ]
  node [
    id 662
    label "utworzenie"
  ]
  node [
    id 663
    label "str&#243;j"
  ]
  node [
    id 664
    label "twierdzenie"
  ]
  node [
    id 665
    label "obleczenie"
  ]
  node [
    id 666
    label "przygotowywanie"
  ]
  node [
    id 667
    label "przymierzenie"
  ]
  node [
    id 668
    label "wyko&#324;czenie"
  ]
  node [
    id 669
    label "przewidzenie"
  ]
  node [
    id 670
    label "reengineering"
  ]
  node [
    id 671
    label "scheduling"
  ]
  node [
    id 672
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 673
    label "okienko"
  ]
  node [
    id 674
    label "samodzielny"
  ]
  node [
    id 675
    label "swojak"
  ]
  node [
    id 676
    label "bli&#378;ni"
  ]
  node [
    id 677
    label "odr&#281;bny"
  ]
  node [
    id 678
    label "sobieradzki"
  ]
  node [
    id 679
    label "niepodleg&#322;y"
  ]
  node [
    id 680
    label "czyj&#347;"
  ]
  node [
    id 681
    label "autonomicznie"
  ]
  node [
    id 682
    label "indywidualny"
  ]
  node [
    id 683
    label "samodzielnie"
  ]
  node [
    id 684
    label "w&#322;asny"
  ]
  node [
    id 685
    label "osobny"
  ]
  node [
    id 686
    label "ludzko&#347;&#263;"
  ]
  node [
    id 687
    label "asymilowanie"
  ]
  node [
    id 688
    label "wapniak"
  ]
  node [
    id 689
    label "asymilowa&#263;"
  ]
  node [
    id 690
    label "os&#322;abia&#263;"
  ]
  node [
    id 691
    label "posta&#263;"
  ]
  node [
    id 692
    label "hominid"
  ]
  node [
    id 693
    label "podw&#322;adny"
  ]
  node [
    id 694
    label "os&#322;abianie"
  ]
  node [
    id 695
    label "g&#322;owa"
  ]
  node [
    id 696
    label "figura"
  ]
  node [
    id 697
    label "portrecista"
  ]
  node [
    id 698
    label "dwun&#243;g"
  ]
  node [
    id 699
    label "profanum"
  ]
  node [
    id 700
    label "mikrokosmos"
  ]
  node [
    id 701
    label "nasada"
  ]
  node [
    id 702
    label "duch"
  ]
  node [
    id 703
    label "antropochoria"
  ]
  node [
    id 704
    label "osoba"
  ]
  node [
    id 705
    label "wz&#243;r"
  ]
  node [
    id 706
    label "senior"
  ]
  node [
    id 707
    label "oddzia&#322;ywanie"
  ]
  node [
    id 708
    label "Adam"
  ]
  node [
    id 709
    label "homo_sapiens"
  ]
  node [
    id 710
    label "polifag"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 8
    target 689
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
]
