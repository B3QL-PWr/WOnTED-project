graph [
  node [
    id 0
    label "r&#243;wno"
    origin "text"
  ]
  node [
    id 1
    label "rok"
    origin "text"
  ]
  node [
    id 2
    label "temu"
    origin "text"
  ]
  node [
    id 3
    label "jednocze&#347;nie"
  ]
  node [
    id 4
    label "jednakowy"
  ]
  node [
    id 5
    label "pewnie"
  ]
  node [
    id 6
    label "regularnie"
  ]
  node [
    id 7
    label "niezmiennie"
  ]
  node [
    id 8
    label "dok&#322;adnie"
  ]
  node [
    id 9
    label "prosto"
  ]
  node [
    id 10
    label "miarowy"
  ]
  node [
    id 11
    label "stabilnie"
  ]
  node [
    id 12
    label "jednolicie"
  ]
  node [
    id 13
    label "identically"
  ]
  node [
    id 14
    label "r&#243;wny"
  ]
  node [
    id 15
    label "jednolity"
  ]
  node [
    id 16
    label "jednakowo"
  ]
  node [
    id 17
    label "regularny"
  ]
  node [
    id 18
    label "harmonijnie"
  ]
  node [
    id 19
    label "zwyczajny"
  ]
  node [
    id 20
    label "poprostu"
  ]
  node [
    id 21
    label "cz&#281;sto"
  ]
  node [
    id 22
    label "stale"
  ]
  node [
    id 23
    label "&#322;atwo"
  ]
  node [
    id 24
    label "prosty"
  ]
  node [
    id 25
    label "skromnie"
  ]
  node [
    id 26
    label "bezpo&#347;rednio"
  ]
  node [
    id 27
    label "elementarily"
  ]
  node [
    id 28
    label "niepozornie"
  ]
  node [
    id 29
    label "naturalnie"
  ]
  node [
    id 30
    label "meticulously"
  ]
  node [
    id 31
    label "punctiliously"
  ]
  node [
    id 32
    label "precyzyjnie"
  ]
  node [
    id 33
    label "dok&#322;adny"
  ]
  node [
    id 34
    label "rzetelnie"
  ]
  node [
    id 35
    label "niezmienny"
  ]
  node [
    id 36
    label "najpewniej"
  ]
  node [
    id 37
    label "pewny"
  ]
  node [
    id 38
    label "wiarygodnie"
  ]
  node [
    id 39
    label "mocno"
  ]
  node [
    id 40
    label "pewniej"
  ]
  node [
    id 41
    label "bezpiecznie"
  ]
  node [
    id 42
    label "zwinnie"
  ]
  node [
    id 43
    label "trwale"
  ]
  node [
    id 44
    label "porz&#261;dnie"
  ]
  node [
    id 45
    label "stabilny"
  ]
  node [
    id 46
    label "jednoczesny"
  ]
  node [
    id 47
    label "synchronously"
  ]
  node [
    id 48
    label "concurrently"
  ]
  node [
    id 49
    label "coincidentally"
  ]
  node [
    id 50
    label "simultaneously"
  ]
  node [
    id 51
    label "mundurowanie"
  ]
  node [
    id 52
    label "klawy"
  ]
  node [
    id 53
    label "dor&#243;wnywanie"
  ]
  node [
    id 54
    label "zr&#243;wnanie_si&#281;"
  ]
  node [
    id 55
    label "jednotonny"
  ]
  node [
    id 56
    label "taki&#380;"
  ]
  node [
    id 57
    label "dobry"
  ]
  node [
    id 58
    label "ca&#322;y"
  ]
  node [
    id 59
    label "mundurowa&#263;"
  ]
  node [
    id 60
    label "r&#243;wnanie"
  ]
  node [
    id 61
    label "zr&#243;wnanie"
  ]
  node [
    id 62
    label "miarowo"
  ]
  node [
    id 63
    label "zr&#243;wnywanie"
  ]
  node [
    id 64
    label "identyczny"
  ]
  node [
    id 65
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 66
    label "wzorcowy"
  ]
  node [
    id 67
    label "specyficzny"
  ]
  node [
    id 68
    label "niestandardowy"
  ]
  node [
    id 69
    label "odpowiedni"
  ]
  node [
    id 70
    label "p&#243;&#322;rocze"
  ]
  node [
    id 71
    label "martwy_sezon"
  ]
  node [
    id 72
    label "kalendarz"
  ]
  node [
    id 73
    label "cykl_astronomiczny"
  ]
  node [
    id 74
    label "lata"
  ]
  node [
    id 75
    label "pora_roku"
  ]
  node [
    id 76
    label "stulecie"
  ]
  node [
    id 77
    label "kurs"
  ]
  node [
    id 78
    label "czas"
  ]
  node [
    id 79
    label "jubileusz"
  ]
  node [
    id 80
    label "grupa"
  ]
  node [
    id 81
    label "kwarta&#322;"
  ]
  node [
    id 82
    label "miesi&#261;c"
  ]
  node [
    id 83
    label "summer"
  ]
  node [
    id 84
    label "odm&#322;adzanie"
  ]
  node [
    id 85
    label "liga"
  ]
  node [
    id 86
    label "jednostka_systematyczna"
  ]
  node [
    id 87
    label "asymilowanie"
  ]
  node [
    id 88
    label "gromada"
  ]
  node [
    id 89
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 90
    label "asymilowa&#263;"
  ]
  node [
    id 91
    label "egzemplarz"
  ]
  node [
    id 92
    label "Entuzjastki"
  ]
  node [
    id 93
    label "zbi&#243;r"
  ]
  node [
    id 94
    label "kompozycja"
  ]
  node [
    id 95
    label "Terranie"
  ]
  node [
    id 96
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 97
    label "category"
  ]
  node [
    id 98
    label "pakiet_klimatyczny"
  ]
  node [
    id 99
    label "oddzia&#322;"
  ]
  node [
    id 100
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 101
    label "cz&#261;steczka"
  ]
  node [
    id 102
    label "stage_set"
  ]
  node [
    id 103
    label "type"
  ]
  node [
    id 104
    label "specgrupa"
  ]
  node [
    id 105
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 106
    label "&#346;wietliki"
  ]
  node [
    id 107
    label "odm&#322;odzenie"
  ]
  node [
    id 108
    label "Eurogrupa"
  ]
  node [
    id 109
    label "odm&#322;adza&#263;"
  ]
  node [
    id 110
    label "formacja_geologiczna"
  ]
  node [
    id 111
    label "harcerze_starsi"
  ]
  node [
    id 112
    label "poprzedzanie"
  ]
  node [
    id 113
    label "czasoprzestrze&#324;"
  ]
  node [
    id 114
    label "laba"
  ]
  node [
    id 115
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 116
    label "chronometria"
  ]
  node [
    id 117
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 118
    label "rachuba_czasu"
  ]
  node [
    id 119
    label "przep&#322;ywanie"
  ]
  node [
    id 120
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 121
    label "czasokres"
  ]
  node [
    id 122
    label "odczyt"
  ]
  node [
    id 123
    label "chwila"
  ]
  node [
    id 124
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 125
    label "dzieje"
  ]
  node [
    id 126
    label "kategoria_gramatyczna"
  ]
  node [
    id 127
    label "poprzedzenie"
  ]
  node [
    id 128
    label "trawienie"
  ]
  node [
    id 129
    label "pochodzi&#263;"
  ]
  node [
    id 130
    label "period"
  ]
  node [
    id 131
    label "okres_czasu"
  ]
  node [
    id 132
    label "poprzedza&#263;"
  ]
  node [
    id 133
    label "schy&#322;ek"
  ]
  node [
    id 134
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 135
    label "odwlekanie_si&#281;"
  ]
  node [
    id 136
    label "zegar"
  ]
  node [
    id 137
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 138
    label "czwarty_wymiar"
  ]
  node [
    id 139
    label "pochodzenie"
  ]
  node [
    id 140
    label "koniugacja"
  ]
  node [
    id 141
    label "Zeitgeist"
  ]
  node [
    id 142
    label "trawi&#263;"
  ]
  node [
    id 143
    label "pogoda"
  ]
  node [
    id 144
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 145
    label "poprzedzi&#263;"
  ]
  node [
    id 146
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 147
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 148
    label "time_period"
  ]
  node [
    id 149
    label "tydzie&#324;"
  ]
  node [
    id 150
    label "miech"
  ]
  node [
    id 151
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 152
    label "kalendy"
  ]
  node [
    id 153
    label "term"
  ]
  node [
    id 154
    label "rok_akademicki"
  ]
  node [
    id 155
    label "rok_szkolny"
  ]
  node [
    id 156
    label "semester"
  ]
  node [
    id 157
    label "anniwersarz"
  ]
  node [
    id 158
    label "rocznica"
  ]
  node [
    id 159
    label "obszar"
  ]
  node [
    id 160
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 161
    label "long_time"
  ]
  node [
    id 162
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 163
    label "almanac"
  ]
  node [
    id 164
    label "rozk&#322;ad"
  ]
  node [
    id 165
    label "wydawnictwo"
  ]
  node [
    id 166
    label "Juliusz_Cezar"
  ]
  node [
    id 167
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 168
    label "zwy&#380;kowanie"
  ]
  node [
    id 169
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 170
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 171
    label "zaj&#281;cia"
  ]
  node [
    id 172
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 173
    label "trasa"
  ]
  node [
    id 174
    label "przeorientowywanie"
  ]
  node [
    id 175
    label "przejazd"
  ]
  node [
    id 176
    label "kierunek"
  ]
  node [
    id 177
    label "przeorientowywa&#263;"
  ]
  node [
    id 178
    label "nauka"
  ]
  node [
    id 179
    label "przeorientowanie"
  ]
  node [
    id 180
    label "klasa"
  ]
  node [
    id 181
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 182
    label "przeorientowa&#263;"
  ]
  node [
    id 183
    label "manner"
  ]
  node [
    id 184
    label "course"
  ]
  node [
    id 185
    label "passage"
  ]
  node [
    id 186
    label "zni&#380;kowanie"
  ]
  node [
    id 187
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 188
    label "seria"
  ]
  node [
    id 189
    label "stawka"
  ]
  node [
    id 190
    label "way"
  ]
  node [
    id 191
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 192
    label "spos&#243;b"
  ]
  node [
    id 193
    label "deprecjacja"
  ]
  node [
    id 194
    label "cedu&#322;a"
  ]
  node [
    id 195
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 196
    label "drive"
  ]
  node [
    id 197
    label "bearing"
  ]
  node [
    id 198
    label "Lira"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
]
