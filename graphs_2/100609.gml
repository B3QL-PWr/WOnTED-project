graph [
  node [
    id 0
    label "otrzyma&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "patronat"
    origin "text"
  ]
  node [
    id 2
    label "pan"
    origin "text"
  ]
  node [
    id 3
    label "licencja"
  ]
  node [
    id 4
    label "opieka"
  ]
  node [
    id 5
    label "nadz&#243;r"
  ]
  node [
    id 6
    label "sponsorship"
  ]
  node [
    id 7
    label "zezwolenie"
  ]
  node [
    id 8
    label "za&#347;wiadczenie"
  ]
  node [
    id 9
    label "licencjonowa&#263;"
  ]
  node [
    id 10
    label "rasowy"
  ]
  node [
    id 11
    label "pozwolenie"
  ]
  node [
    id 12
    label "hodowla"
  ]
  node [
    id 13
    label "prawo"
  ]
  node [
    id 14
    label "license"
  ]
  node [
    id 15
    label "instytucja"
  ]
  node [
    id 16
    label "czynno&#347;&#263;"
  ]
  node [
    id 17
    label "examination"
  ]
  node [
    id 18
    label "pomoc"
  ]
  node [
    id 19
    label "opieku&#324;cze_skrzyd&#322;a"
  ]
  node [
    id 20
    label "staranie"
  ]
  node [
    id 21
    label "belfer"
  ]
  node [
    id 22
    label "murza"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "ojciec"
  ]
  node [
    id 25
    label "samiec"
  ]
  node [
    id 26
    label "androlog"
  ]
  node [
    id 27
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 28
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 29
    label "efendi"
  ]
  node [
    id 30
    label "opiekun"
  ]
  node [
    id 31
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 32
    label "pa&#324;stwo"
  ]
  node [
    id 33
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 34
    label "bratek"
  ]
  node [
    id 35
    label "Mieszko_I"
  ]
  node [
    id 36
    label "Midas"
  ]
  node [
    id 37
    label "m&#261;&#380;"
  ]
  node [
    id 38
    label "bogaty"
  ]
  node [
    id 39
    label "popularyzator"
  ]
  node [
    id 40
    label "pracodawca"
  ]
  node [
    id 41
    label "kszta&#322;ciciel"
  ]
  node [
    id 42
    label "preceptor"
  ]
  node [
    id 43
    label "nabab"
  ]
  node [
    id 44
    label "pupil"
  ]
  node [
    id 45
    label "andropauza"
  ]
  node [
    id 46
    label "zwrot"
  ]
  node [
    id 47
    label "przyw&#243;dca"
  ]
  node [
    id 48
    label "doros&#322;y"
  ]
  node [
    id 49
    label "pedagog"
  ]
  node [
    id 50
    label "rz&#261;dzenie"
  ]
  node [
    id 51
    label "jegomo&#347;&#263;"
  ]
  node [
    id 52
    label "szkolnik"
  ]
  node [
    id 53
    label "ch&#322;opina"
  ]
  node [
    id 54
    label "w&#322;odarz"
  ]
  node [
    id 55
    label "profesor"
  ]
  node [
    id 56
    label "gra_w_karty"
  ]
  node [
    id 57
    label "w&#322;adza"
  ]
  node [
    id 58
    label "Fidel_Castro"
  ]
  node [
    id 59
    label "Anders"
  ]
  node [
    id 60
    label "Ko&#347;ciuszko"
  ]
  node [
    id 61
    label "Tito"
  ]
  node [
    id 62
    label "Miko&#322;ajczyk"
  ]
  node [
    id 63
    label "Sabataj_Cwi"
  ]
  node [
    id 64
    label "lider"
  ]
  node [
    id 65
    label "Mao"
  ]
  node [
    id 66
    label "p&#322;atnik"
  ]
  node [
    id 67
    label "zwierzchnik"
  ]
  node [
    id 68
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 69
    label "nadzorca"
  ]
  node [
    id 70
    label "funkcjonariusz"
  ]
  node [
    id 71
    label "podmiot"
  ]
  node [
    id 72
    label "wykupienie"
  ]
  node [
    id 73
    label "bycie_w_posiadaniu"
  ]
  node [
    id 74
    label "wykupywanie"
  ]
  node [
    id 75
    label "rozszerzyciel"
  ]
  node [
    id 76
    label "ludzko&#347;&#263;"
  ]
  node [
    id 77
    label "asymilowanie"
  ]
  node [
    id 78
    label "wapniak"
  ]
  node [
    id 79
    label "asymilowa&#263;"
  ]
  node [
    id 80
    label "os&#322;abia&#263;"
  ]
  node [
    id 81
    label "posta&#263;"
  ]
  node [
    id 82
    label "hominid"
  ]
  node [
    id 83
    label "podw&#322;adny"
  ]
  node [
    id 84
    label "os&#322;abianie"
  ]
  node [
    id 85
    label "g&#322;owa"
  ]
  node [
    id 86
    label "figura"
  ]
  node [
    id 87
    label "portrecista"
  ]
  node [
    id 88
    label "dwun&#243;g"
  ]
  node [
    id 89
    label "profanum"
  ]
  node [
    id 90
    label "mikrokosmos"
  ]
  node [
    id 91
    label "nasada"
  ]
  node [
    id 92
    label "duch"
  ]
  node [
    id 93
    label "antropochoria"
  ]
  node [
    id 94
    label "osoba"
  ]
  node [
    id 95
    label "wz&#243;r"
  ]
  node [
    id 96
    label "senior"
  ]
  node [
    id 97
    label "oddzia&#322;ywanie"
  ]
  node [
    id 98
    label "Adam"
  ]
  node [
    id 99
    label "homo_sapiens"
  ]
  node [
    id 100
    label "polifag"
  ]
  node [
    id 101
    label "wydoro&#347;lenie"
  ]
  node [
    id 102
    label "du&#380;y"
  ]
  node [
    id 103
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 104
    label "doro&#347;lenie"
  ]
  node [
    id 105
    label "&#378;ra&#322;y"
  ]
  node [
    id 106
    label "doro&#347;le"
  ]
  node [
    id 107
    label "dojrzale"
  ]
  node [
    id 108
    label "dojrza&#322;y"
  ]
  node [
    id 109
    label "m&#261;dry"
  ]
  node [
    id 110
    label "doletni"
  ]
  node [
    id 111
    label "punkt"
  ]
  node [
    id 112
    label "turn"
  ]
  node [
    id 113
    label "turning"
  ]
  node [
    id 114
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 115
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 116
    label "skr&#281;t"
  ]
  node [
    id 117
    label "obr&#243;t"
  ]
  node [
    id 118
    label "fraza_czasownikowa"
  ]
  node [
    id 119
    label "jednostka_leksykalna"
  ]
  node [
    id 120
    label "zmiana"
  ]
  node [
    id 121
    label "wyra&#380;enie"
  ]
  node [
    id 122
    label "starosta"
  ]
  node [
    id 123
    label "zarz&#261;dca"
  ]
  node [
    id 124
    label "w&#322;adca"
  ]
  node [
    id 125
    label "nauczyciel"
  ]
  node [
    id 126
    label "autor"
  ]
  node [
    id 127
    label "wyprawka"
  ]
  node [
    id 128
    label "mundurek"
  ]
  node [
    id 129
    label "szko&#322;a"
  ]
  node [
    id 130
    label "tarcza"
  ]
  node [
    id 131
    label "elew"
  ]
  node [
    id 132
    label "absolwent"
  ]
  node [
    id 133
    label "klasa"
  ]
  node [
    id 134
    label "stopie&#324;_naukowy"
  ]
  node [
    id 135
    label "nauczyciel_akademicki"
  ]
  node [
    id 136
    label "tytu&#322;"
  ]
  node [
    id 137
    label "profesura"
  ]
  node [
    id 138
    label "konsulent"
  ]
  node [
    id 139
    label "wirtuoz"
  ]
  node [
    id 140
    label "ekspert"
  ]
  node [
    id 141
    label "ochotnik"
  ]
  node [
    id 142
    label "pomocnik"
  ]
  node [
    id 143
    label "student"
  ]
  node [
    id 144
    label "nauczyciel_muzyki"
  ]
  node [
    id 145
    label "zakonnik"
  ]
  node [
    id 146
    label "urz&#281;dnik"
  ]
  node [
    id 147
    label "bogacz"
  ]
  node [
    id 148
    label "dostojnik"
  ]
  node [
    id 149
    label "mo&#347;&#263;"
  ]
  node [
    id 150
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 151
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 152
    label "kuwada"
  ]
  node [
    id 153
    label "tworzyciel"
  ]
  node [
    id 154
    label "rodzice"
  ]
  node [
    id 155
    label "&#347;w"
  ]
  node [
    id 156
    label "pomys&#322;odawca"
  ]
  node [
    id 157
    label "rodzic"
  ]
  node [
    id 158
    label "wykonawca"
  ]
  node [
    id 159
    label "ojczym"
  ]
  node [
    id 160
    label "przodek"
  ]
  node [
    id 161
    label "papa"
  ]
  node [
    id 162
    label "stary"
  ]
  node [
    id 163
    label "zwierz&#281;"
  ]
  node [
    id 164
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 165
    label "kochanek"
  ]
  node [
    id 166
    label "fio&#322;ek"
  ]
  node [
    id 167
    label "facet"
  ]
  node [
    id 168
    label "brat"
  ]
  node [
    id 169
    label "ma&#322;&#380;onek"
  ]
  node [
    id 170
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 171
    label "m&#243;j"
  ]
  node [
    id 172
    label "ch&#322;op"
  ]
  node [
    id 173
    label "pan_m&#322;ody"
  ]
  node [
    id 174
    label "&#347;lubny"
  ]
  node [
    id 175
    label "pan_domu"
  ]
  node [
    id 176
    label "pan_i_w&#322;adca"
  ]
  node [
    id 177
    label "Frygia"
  ]
  node [
    id 178
    label "sprawowanie"
  ]
  node [
    id 179
    label "dominion"
  ]
  node [
    id 180
    label "dominowanie"
  ]
  node [
    id 181
    label "reign"
  ]
  node [
    id 182
    label "rule"
  ]
  node [
    id 183
    label "zwierz&#281;_domowe"
  ]
  node [
    id 184
    label "J&#281;drzejewicz"
  ]
  node [
    id 185
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 186
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 187
    label "John_Dewey"
  ]
  node [
    id 188
    label "specjalista"
  ]
  node [
    id 189
    label "&#380;ycie"
  ]
  node [
    id 190
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 191
    label "Turek"
  ]
  node [
    id 192
    label "effendi"
  ]
  node [
    id 193
    label "obfituj&#261;cy"
  ]
  node [
    id 194
    label "r&#243;&#380;norodny"
  ]
  node [
    id 195
    label "spania&#322;y"
  ]
  node [
    id 196
    label "obficie"
  ]
  node [
    id 197
    label "sytuowany"
  ]
  node [
    id 198
    label "och&#281;do&#380;ny"
  ]
  node [
    id 199
    label "forsiasty"
  ]
  node [
    id 200
    label "zapa&#347;ny"
  ]
  node [
    id 201
    label "bogato"
  ]
  node [
    id 202
    label "Katar"
  ]
  node [
    id 203
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 204
    label "Libia"
  ]
  node [
    id 205
    label "Gwatemala"
  ]
  node [
    id 206
    label "Afganistan"
  ]
  node [
    id 207
    label "Ekwador"
  ]
  node [
    id 208
    label "Tad&#380;ykistan"
  ]
  node [
    id 209
    label "Bhutan"
  ]
  node [
    id 210
    label "Argentyna"
  ]
  node [
    id 211
    label "D&#380;ibuti"
  ]
  node [
    id 212
    label "Wenezuela"
  ]
  node [
    id 213
    label "Ukraina"
  ]
  node [
    id 214
    label "Gabon"
  ]
  node [
    id 215
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 216
    label "Rwanda"
  ]
  node [
    id 217
    label "Liechtenstein"
  ]
  node [
    id 218
    label "organizacja"
  ]
  node [
    id 219
    label "Sri_Lanka"
  ]
  node [
    id 220
    label "Madagaskar"
  ]
  node [
    id 221
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 222
    label "Tonga"
  ]
  node [
    id 223
    label "Kongo"
  ]
  node [
    id 224
    label "Bangladesz"
  ]
  node [
    id 225
    label "Kanada"
  ]
  node [
    id 226
    label "Wehrlen"
  ]
  node [
    id 227
    label "Algieria"
  ]
  node [
    id 228
    label "Surinam"
  ]
  node [
    id 229
    label "Chile"
  ]
  node [
    id 230
    label "Sahara_Zachodnia"
  ]
  node [
    id 231
    label "Uganda"
  ]
  node [
    id 232
    label "W&#281;gry"
  ]
  node [
    id 233
    label "Birma"
  ]
  node [
    id 234
    label "Kazachstan"
  ]
  node [
    id 235
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 236
    label "Armenia"
  ]
  node [
    id 237
    label "Tuwalu"
  ]
  node [
    id 238
    label "Timor_Wschodni"
  ]
  node [
    id 239
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 240
    label "Izrael"
  ]
  node [
    id 241
    label "Estonia"
  ]
  node [
    id 242
    label "Komory"
  ]
  node [
    id 243
    label "Kamerun"
  ]
  node [
    id 244
    label "Haiti"
  ]
  node [
    id 245
    label "Belize"
  ]
  node [
    id 246
    label "Sierra_Leone"
  ]
  node [
    id 247
    label "Luksemburg"
  ]
  node [
    id 248
    label "USA"
  ]
  node [
    id 249
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 250
    label "Barbados"
  ]
  node [
    id 251
    label "San_Marino"
  ]
  node [
    id 252
    label "Bu&#322;garia"
  ]
  node [
    id 253
    label "Wietnam"
  ]
  node [
    id 254
    label "Indonezja"
  ]
  node [
    id 255
    label "Malawi"
  ]
  node [
    id 256
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 257
    label "Francja"
  ]
  node [
    id 258
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 259
    label "partia"
  ]
  node [
    id 260
    label "Zambia"
  ]
  node [
    id 261
    label "Angola"
  ]
  node [
    id 262
    label "Grenada"
  ]
  node [
    id 263
    label "Nepal"
  ]
  node [
    id 264
    label "Panama"
  ]
  node [
    id 265
    label "Rumunia"
  ]
  node [
    id 266
    label "Czarnog&#243;ra"
  ]
  node [
    id 267
    label "Malediwy"
  ]
  node [
    id 268
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 269
    label "S&#322;owacja"
  ]
  node [
    id 270
    label "para"
  ]
  node [
    id 271
    label "Egipt"
  ]
  node [
    id 272
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 273
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 274
    label "Kolumbia"
  ]
  node [
    id 275
    label "Mozambik"
  ]
  node [
    id 276
    label "Laos"
  ]
  node [
    id 277
    label "Burundi"
  ]
  node [
    id 278
    label "Suazi"
  ]
  node [
    id 279
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 280
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 281
    label "Czechy"
  ]
  node [
    id 282
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 283
    label "Wyspy_Marshalla"
  ]
  node [
    id 284
    label "Trynidad_i_Tobago"
  ]
  node [
    id 285
    label "Dominika"
  ]
  node [
    id 286
    label "Palau"
  ]
  node [
    id 287
    label "Syria"
  ]
  node [
    id 288
    label "Gwinea_Bissau"
  ]
  node [
    id 289
    label "Liberia"
  ]
  node [
    id 290
    label "Zimbabwe"
  ]
  node [
    id 291
    label "Polska"
  ]
  node [
    id 292
    label "Jamajka"
  ]
  node [
    id 293
    label "Dominikana"
  ]
  node [
    id 294
    label "Senegal"
  ]
  node [
    id 295
    label "Gruzja"
  ]
  node [
    id 296
    label "Togo"
  ]
  node [
    id 297
    label "Chorwacja"
  ]
  node [
    id 298
    label "Meksyk"
  ]
  node [
    id 299
    label "Macedonia"
  ]
  node [
    id 300
    label "Gujana"
  ]
  node [
    id 301
    label "Zair"
  ]
  node [
    id 302
    label "Albania"
  ]
  node [
    id 303
    label "Kambod&#380;a"
  ]
  node [
    id 304
    label "Mauritius"
  ]
  node [
    id 305
    label "Monako"
  ]
  node [
    id 306
    label "Gwinea"
  ]
  node [
    id 307
    label "Mali"
  ]
  node [
    id 308
    label "Nigeria"
  ]
  node [
    id 309
    label "Kostaryka"
  ]
  node [
    id 310
    label "Hanower"
  ]
  node [
    id 311
    label "Paragwaj"
  ]
  node [
    id 312
    label "W&#322;ochy"
  ]
  node [
    id 313
    label "Wyspy_Salomona"
  ]
  node [
    id 314
    label "Seszele"
  ]
  node [
    id 315
    label "Hiszpania"
  ]
  node [
    id 316
    label "Boliwia"
  ]
  node [
    id 317
    label "Kirgistan"
  ]
  node [
    id 318
    label "Irlandia"
  ]
  node [
    id 319
    label "Czad"
  ]
  node [
    id 320
    label "Irak"
  ]
  node [
    id 321
    label "Lesoto"
  ]
  node [
    id 322
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 323
    label "Malta"
  ]
  node [
    id 324
    label "Andora"
  ]
  node [
    id 325
    label "Chiny"
  ]
  node [
    id 326
    label "Filipiny"
  ]
  node [
    id 327
    label "Antarktis"
  ]
  node [
    id 328
    label "Niemcy"
  ]
  node [
    id 329
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 330
    label "Brazylia"
  ]
  node [
    id 331
    label "terytorium"
  ]
  node [
    id 332
    label "Nikaragua"
  ]
  node [
    id 333
    label "Pakistan"
  ]
  node [
    id 334
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 335
    label "Kenia"
  ]
  node [
    id 336
    label "Niger"
  ]
  node [
    id 337
    label "Tunezja"
  ]
  node [
    id 338
    label "Portugalia"
  ]
  node [
    id 339
    label "Fid&#380;i"
  ]
  node [
    id 340
    label "Maroko"
  ]
  node [
    id 341
    label "Botswana"
  ]
  node [
    id 342
    label "Tajlandia"
  ]
  node [
    id 343
    label "Australia"
  ]
  node [
    id 344
    label "Burkina_Faso"
  ]
  node [
    id 345
    label "interior"
  ]
  node [
    id 346
    label "Benin"
  ]
  node [
    id 347
    label "Tanzania"
  ]
  node [
    id 348
    label "Indie"
  ]
  node [
    id 349
    label "&#321;otwa"
  ]
  node [
    id 350
    label "Kiribati"
  ]
  node [
    id 351
    label "Antigua_i_Barbuda"
  ]
  node [
    id 352
    label "Rodezja"
  ]
  node [
    id 353
    label "Cypr"
  ]
  node [
    id 354
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 355
    label "Peru"
  ]
  node [
    id 356
    label "Austria"
  ]
  node [
    id 357
    label "Urugwaj"
  ]
  node [
    id 358
    label "Jordania"
  ]
  node [
    id 359
    label "Grecja"
  ]
  node [
    id 360
    label "Azerbejd&#380;an"
  ]
  node [
    id 361
    label "Turcja"
  ]
  node [
    id 362
    label "Samoa"
  ]
  node [
    id 363
    label "Sudan"
  ]
  node [
    id 364
    label "Oman"
  ]
  node [
    id 365
    label "ziemia"
  ]
  node [
    id 366
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 367
    label "Uzbekistan"
  ]
  node [
    id 368
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 369
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 370
    label "Honduras"
  ]
  node [
    id 371
    label "Mongolia"
  ]
  node [
    id 372
    label "Portoryko"
  ]
  node [
    id 373
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 374
    label "Serbia"
  ]
  node [
    id 375
    label "Tajwan"
  ]
  node [
    id 376
    label "Wielka_Brytania"
  ]
  node [
    id 377
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 378
    label "Liban"
  ]
  node [
    id 379
    label "Japonia"
  ]
  node [
    id 380
    label "Ghana"
  ]
  node [
    id 381
    label "Bahrajn"
  ]
  node [
    id 382
    label "Belgia"
  ]
  node [
    id 383
    label "Etiopia"
  ]
  node [
    id 384
    label "Mikronezja"
  ]
  node [
    id 385
    label "Kuwejt"
  ]
  node [
    id 386
    label "grupa"
  ]
  node [
    id 387
    label "Bahamy"
  ]
  node [
    id 388
    label "Rosja"
  ]
  node [
    id 389
    label "Mo&#322;dawia"
  ]
  node [
    id 390
    label "Litwa"
  ]
  node [
    id 391
    label "S&#322;owenia"
  ]
  node [
    id 392
    label "Szwajcaria"
  ]
  node [
    id 393
    label "Erytrea"
  ]
  node [
    id 394
    label "Kuba"
  ]
  node [
    id 395
    label "Arabia_Saudyjska"
  ]
  node [
    id 396
    label "granica_pa&#324;stwa"
  ]
  node [
    id 397
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 398
    label "Malezja"
  ]
  node [
    id 399
    label "Korea"
  ]
  node [
    id 400
    label "Jemen"
  ]
  node [
    id 401
    label "Nowa_Zelandia"
  ]
  node [
    id 402
    label "Namibia"
  ]
  node [
    id 403
    label "Nauru"
  ]
  node [
    id 404
    label "holoarktyka"
  ]
  node [
    id 405
    label "Brunei"
  ]
  node [
    id 406
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 407
    label "Khitai"
  ]
  node [
    id 408
    label "Mauretania"
  ]
  node [
    id 409
    label "Iran"
  ]
  node [
    id 410
    label "Gambia"
  ]
  node [
    id 411
    label "Somalia"
  ]
  node [
    id 412
    label "Holandia"
  ]
  node [
    id 413
    label "Turkmenistan"
  ]
  node [
    id 414
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 415
    label "Salwador"
  ]
  node [
    id 416
    label "polski"
  ]
  node [
    id 417
    label "akademia"
  ]
  node [
    id 418
    label "nauka"
  ]
  node [
    id 419
    label "otworzy&#263;"
  ]
  node [
    id 420
    label "ksi&#261;&#380;ka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 416
    target 417
  ]
  edge [
    source 416
    target 418
  ]
  edge [
    source 417
    target 418
  ]
  edge [
    source 419
    target 420
  ]
]
