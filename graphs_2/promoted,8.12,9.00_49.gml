graph [
  node [
    id 0
    label "brak"
    origin "text"
  ]
  node [
    id 1
    label "konsekwencja"
    origin "text"
  ]
  node [
    id 2
    label "przez"
    origin "text"
  ]
  node [
    id 3
    label "jeszcze"
    origin "text"
  ]
  node [
    id 4
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 5
    label "nier&#243;wno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "w&#347;r&#243;d"
    origin "text"
  ]
  node [
    id 7
    label "pracownik"
    origin "text"
  ]
  node [
    id 8
    label "podej&#347;cie"
    origin "text"
  ]
  node [
    id 9
    label "rz&#261;dz&#261;ca"
    origin "text"
  ]
  node [
    id 10
    label "kontrola"
    origin "text"
  ]
  node [
    id 11
    label "absencja"
    origin "text"
  ]
  node [
    id 12
    label "chorobowe"
    origin "text"
  ]
  node [
    id 13
    label "zmiana"
    origin "text"
  ]
  node [
    id 14
    label "przepis"
    origin "text"
  ]
  node [
    id 15
    label "tym"
    origin "text"
  ]
  node [
    id 16
    label "ocenia&#263;"
    origin "text"
  ]
  node [
    id 17
    label "specjalista"
    origin "text"
  ]
  node [
    id 18
    label "nieistnienie"
  ]
  node [
    id 19
    label "odej&#347;cie"
  ]
  node [
    id 20
    label "defect"
  ]
  node [
    id 21
    label "gap"
  ]
  node [
    id 22
    label "odej&#347;&#263;"
  ]
  node [
    id 23
    label "kr&#243;tki"
  ]
  node [
    id 24
    label "wada"
  ]
  node [
    id 25
    label "odchodzi&#263;"
  ]
  node [
    id 26
    label "wyr&#243;b"
  ]
  node [
    id 27
    label "odchodzenie"
  ]
  node [
    id 28
    label "prywatywny"
  ]
  node [
    id 29
    label "stan"
  ]
  node [
    id 30
    label "niebyt"
  ]
  node [
    id 31
    label "nonexistence"
  ]
  node [
    id 32
    label "cecha"
  ]
  node [
    id 33
    label "faintness"
  ]
  node [
    id 34
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 35
    label "schorzenie"
  ]
  node [
    id 36
    label "strona"
  ]
  node [
    id 37
    label "imperfection"
  ]
  node [
    id 38
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 39
    label "wytw&#243;r"
  ]
  node [
    id 40
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 41
    label "produkt"
  ]
  node [
    id 42
    label "creation"
  ]
  node [
    id 43
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 44
    label "p&#322;uczkarnia"
  ]
  node [
    id 45
    label "znakowarka"
  ]
  node [
    id 46
    label "produkcja"
  ]
  node [
    id 47
    label "szybki"
  ]
  node [
    id 48
    label "jednowyrazowy"
  ]
  node [
    id 49
    label "bliski"
  ]
  node [
    id 50
    label "s&#322;aby"
  ]
  node [
    id 51
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 52
    label "kr&#243;tko"
  ]
  node [
    id 53
    label "drobny"
  ]
  node [
    id 54
    label "ruch"
  ]
  node [
    id 55
    label "z&#322;y"
  ]
  node [
    id 56
    label "mini&#281;cie"
  ]
  node [
    id 57
    label "odumarcie"
  ]
  node [
    id 58
    label "dysponowanie_si&#281;"
  ]
  node [
    id 59
    label "ruszenie"
  ]
  node [
    id 60
    label "ust&#261;pienie"
  ]
  node [
    id 61
    label "mogi&#322;a"
  ]
  node [
    id 62
    label "pomarcie"
  ]
  node [
    id 63
    label "opuszczenie"
  ]
  node [
    id 64
    label "zb&#281;dny"
  ]
  node [
    id 65
    label "spisanie_"
  ]
  node [
    id 66
    label "oddalenie_si&#281;"
  ]
  node [
    id 67
    label "defenestracja"
  ]
  node [
    id 68
    label "danie_sobie_spokoju"
  ]
  node [
    id 69
    label "&#380;ycie"
  ]
  node [
    id 70
    label "odrzut"
  ]
  node [
    id 71
    label "kres_&#380;ycia"
  ]
  node [
    id 72
    label "zwolnienie_si&#281;"
  ]
  node [
    id 73
    label "zdechni&#281;cie"
  ]
  node [
    id 74
    label "exit"
  ]
  node [
    id 75
    label "stracenie"
  ]
  node [
    id 76
    label "przestanie"
  ]
  node [
    id 77
    label "martwy"
  ]
  node [
    id 78
    label "wr&#243;cenie"
  ]
  node [
    id 79
    label "szeol"
  ]
  node [
    id 80
    label "die"
  ]
  node [
    id 81
    label "oddzielenie_si&#281;"
  ]
  node [
    id 82
    label "deviation"
  ]
  node [
    id 83
    label "wydalenie"
  ]
  node [
    id 84
    label "pogrzebanie"
  ]
  node [
    id 85
    label "&#380;a&#322;oba"
  ]
  node [
    id 86
    label "sko&#324;czenie"
  ]
  node [
    id 87
    label "withdrawal"
  ]
  node [
    id 88
    label "porozchodzenie_si&#281;"
  ]
  node [
    id 89
    label "zabicie"
  ]
  node [
    id 90
    label "agonia"
  ]
  node [
    id 91
    label "po&#322;o&#380;enie_lachy"
  ]
  node [
    id 92
    label "kres"
  ]
  node [
    id 93
    label "usuni&#281;cie"
  ]
  node [
    id 94
    label "relinquishment"
  ]
  node [
    id 95
    label "p&#243;j&#347;cie"
  ]
  node [
    id 96
    label "poniechanie"
  ]
  node [
    id 97
    label "zako&#324;czenie"
  ]
  node [
    id 98
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 99
    label "wypisanie_si&#281;"
  ]
  node [
    id 100
    label "zrobienie"
  ]
  node [
    id 101
    label "blend"
  ]
  node [
    id 102
    label "rozgrywa&#263;_si&#281;"
  ]
  node [
    id 103
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 104
    label "opuszcza&#263;"
  ]
  node [
    id 105
    label "impart"
  ]
  node [
    id 106
    label "wyrusza&#263;"
  ]
  node [
    id 107
    label "go"
  ]
  node [
    id 108
    label "seclude"
  ]
  node [
    id 109
    label "gasn&#261;&#263;"
  ]
  node [
    id 110
    label "przestawa&#263;"
  ]
  node [
    id 111
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 112
    label "odstawa&#263;"
  ]
  node [
    id 113
    label "rezygnowa&#263;"
  ]
  node [
    id 114
    label "i&#347;&#263;"
  ]
  node [
    id 115
    label "mija&#263;"
  ]
  node [
    id 116
    label "proceed"
  ]
  node [
    id 117
    label "korkowanie"
  ]
  node [
    id 118
    label "death"
  ]
  node [
    id 119
    label "k&#322;adzenie_lachy"
  ]
  node [
    id 120
    label "przestawanie"
  ]
  node [
    id 121
    label "machanie_r&#281;k&#261;"
  ]
  node [
    id 122
    label "zdychanie"
  ]
  node [
    id 123
    label "spisywanie_"
  ]
  node [
    id 124
    label "usuwanie"
  ]
  node [
    id 125
    label "tracenie"
  ]
  node [
    id 126
    label "ko&#324;czenie"
  ]
  node [
    id 127
    label "zwalnianie_si&#281;"
  ]
  node [
    id 128
    label "robienie"
  ]
  node [
    id 129
    label "opuszczanie"
  ]
  node [
    id 130
    label "wydalanie"
  ]
  node [
    id 131
    label "odrzucanie"
  ]
  node [
    id 132
    label "odstawianie"
  ]
  node [
    id 133
    label "ust&#281;powanie"
  ]
  node [
    id 134
    label "egress"
  ]
  node [
    id 135
    label "zrzekanie_si&#281;"
  ]
  node [
    id 136
    label "dzianie_si&#281;"
  ]
  node [
    id 137
    label "oddzielanie_si&#281;"
  ]
  node [
    id 138
    label "bycie"
  ]
  node [
    id 139
    label "wyruszanie"
  ]
  node [
    id 140
    label "odumieranie"
  ]
  node [
    id 141
    label "odstawanie"
  ]
  node [
    id 142
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 143
    label "mijanie"
  ]
  node [
    id 144
    label "wracanie"
  ]
  node [
    id 145
    label "oddalanie_si&#281;"
  ]
  node [
    id 146
    label "kursowanie"
  ]
  node [
    id 147
    label "drop"
  ]
  node [
    id 148
    label "zrezygnowa&#263;"
  ]
  node [
    id 149
    label "ruszy&#263;"
  ]
  node [
    id 150
    label "min&#261;&#263;"
  ]
  node [
    id 151
    label "zrobi&#263;"
  ]
  node [
    id 152
    label "leave_office"
  ]
  node [
    id 153
    label "retract"
  ]
  node [
    id 154
    label "opu&#347;ci&#263;"
  ]
  node [
    id 155
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 156
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 157
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 158
    label "przesta&#263;"
  ]
  node [
    id 159
    label "ciekawski"
  ]
  node [
    id 160
    label "statysta"
  ]
  node [
    id 161
    label "odczuwa&#263;"
  ]
  node [
    id 162
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 163
    label "skrupienie_si&#281;"
  ]
  node [
    id 164
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 165
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 166
    label "odczucie"
  ]
  node [
    id 167
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 168
    label "koszula_Dejaniry"
  ]
  node [
    id 169
    label "odczuwanie"
  ]
  node [
    id 170
    label "event"
  ]
  node [
    id 171
    label "rezultat"
  ]
  node [
    id 172
    label "skrupianie_si&#281;"
  ]
  node [
    id 173
    label "odczu&#263;"
  ]
  node [
    id 174
    label "dzia&#322;anie"
  ]
  node [
    id 175
    label "typ"
  ]
  node [
    id 176
    label "przyczyna"
  ]
  node [
    id 177
    label "wierno&#347;&#263;"
  ]
  node [
    id 178
    label "duration"
  ]
  node [
    id 179
    label "odczucia"
  ]
  node [
    id 180
    label "doznanie"
  ]
  node [
    id 181
    label "feel"
  ]
  node [
    id 182
    label "proces"
  ]
  node [
    id 183
    label "zmys&#322;"
  ]
  node [
    id 184
    label "smell"
  ]
  node [
    id 185
    label "postrze&#380;enie"
  ]
  node [
    id 186
    label "zdarzenie_si&#281;"
  ]
  node [
    id 187
    label "opanowanie"
  ]
  node [
    id 188
    label "os&#322;upienie"
  ]
  node [
    id 189
    label "zjawisko"
  ]
  node [
    id 190
    label "czucie"
  ]
  node [
    id 191
    label "zareagowanie"
  ]
  node [
    id 192
    label "przeczulica"
  ]
  node [
    id 193
    label "poczucie"
  ]
  node [
    id 194
    label "reakcja"
  ]
  node [
    id 195
    label "postrzega&#263;"
  ]
  node [
    id 196
    label "uczuwa&#263;"
  ]
  node [
    id 197
    label "widzie&#263;"
  ]
  node [
    id 198
    label "notice"
  ]
  node [
    id 199
    label "doznawa&#263;"
  ]
  node [
    id 200
    label "postrzeganie"
  ]
  node [
    id 201
    label "emotion"
  ]
  node [
    id 202
    label "pojmowanie"
  ]
  node [
    id 203
    label "uczuwanie"
  ]
  node [
    id 204
    label "owiewanie"
  ]
  node [
    id 205
    label "zaznawanie"
  ]
  node [
    id 206
    label "ogarnianie"
  ]
  node [
    id 207
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 208
    label "zagorze&#263;"
  ]
  node [
    id 209
    label "postrzec"
  ]
  node [
    id 210
    label "dozna&#263;"
  ]
  node [
    id 211
    label "wydarzenie"
  ]
  node [
    id 212
    label "ci&#261;gle"
  ]
  node [
    id 213
    label "stale"
  ]
  node [
    id 214
    label "ci&#261;g&#322;y"
  ]
  node [
    id 215
    label "nieprzerwanie"
  ]
  node [
    id 216
    label "doros&#322;y"
  ]
  node [
    id 217
    label "znaczny"
  ]
  node [
    id 218
    label "niema&#322;o"
  ]
  node [
    id 219
    label "wiele"
  ]
  node [
    id 220
    label "rozwini&#281;ty"
  ]
  node [
    id 221
    label "dorodny"
  ]
  node [
    id 222
    label "wa&#380;ny"
  ]
  node [
    id 223
    label "prawdziwy"
  ]
  node [
    id 224
    label "du&#380;o"
  ]
  node [
    id 225
    label "&#380;ywny"
  ]
  node [
    id 226
    label "szczery"
  ]
  node [
    id 227
    label "naturalny"
  ]
  node [
    id 228
    label "naprawd&#281;"
  ]
  node [
    id 229
    label "realnie"
  ]
  node [
    id 230
    label "podobny"
  ]
  node [
    id 231
    label "zgodny"
  ]
  node [
    id 232
    label "m&#261;dry"
  ]
  node [
    id 233
    label "prawdziwie"
  ]
  node [
    id 234
    label "znacznie"
  ]
  node [
    id 235
    label "zauwa&#380;alny"
  ]
  node [
    id 236
    label "wynios&#322;y"
  ]
  node [
    id 237
    label "dono&#347;ny"
  ]
  node [
    id 238
    label "silny"
  ]
  node [
    id 239
    label "wa&#380;nie"
  ]
  node [
    id 240
    label "istotnie"
  ]
  node [
    id 241
    label "eksponowany"
  ]
  node [
    id 242
    label "dobry"
  ]
  node [
    id 243
    label "ukszta&#322;towany"
  ]
  node [
    id 244
    label "do&#347;cig&#322;y"
  ]
  node [
    id 245
    label "&#378;ra&#322;y"
  ]
  node [
    id 246
    label "zdr&#243;w"
  ]
  node [
    id 247
    label "dorodnie"
  ]
  node [
    id 248
    label "okaza&#322;y"
  ]
  node [
    id 249
    label "mocno"
  ]
  node [
    id 250
    label "wiela"
  ]
  node [
    id 251
    label "bardzo"
  ]
  node [
    id 252
    label "cz&#281;sto"
  ]
  node [
    id 253
    label "wydoro&#347;lenie"
  ]
  node [
    id 254
    label "cz&#322;owiek"
  ]
  node [
    id 255
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 256
    label "doro&#347;lenie"
  ]
  node [
    id 257
    label "doro&#347;le"
  ]
  node [
    id 258
    label "senior"
  ]
  node [
    id 259
    label "dojrzale"
  ]
  node [
    id 260
    label "wapniak"
  ]
  node [
    id 261
    label "dojrza&#322;y"
  ]
  node [
    id 262
    label "doletni"
  ]
  node [
    id 263
    label "r&#243;&#380;nica"
  ]
  node [
    id 264
    label "imbalance"
  ]
  node [
    id 265
    label "brzydota"
  ]
  node [
    id 266
    label "miejsce"
  ]
  node [
    id 267
    label "poj&#281;cie"
  ]
  node [
    id 268
    label "krzywda"
  ]
  node [
    id 269
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 270
    label "pos&#322;uchanie"
  ]
  node [
    id 271
    label "skumanie"
  ]
  node [
    id 272
    label "orientacja"
  ]
  node [
    id 273
    label "zorientowanie"
  ]
  node [
    id 274
    label "teoria"
  ]
  node [
    id 275
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 276
    label "clasp"
  ]
  node [
    id 277
    label "forma"
  ]
  node [
    id 278
    label "przem&#243;wienie"
  ]
  node [
    id 279
    label "r&#243;&#380;nienie"
  ]
  node [
    id 280
    label "kontrastowy"
  ]
  node [
    id 281
    label "discord"
  ]
  node [
    id 282
    label "wynik"
  ]
  node [
    id 283
    label "strata"
  ]
  node [
    id 284
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 285
    label "obelga"
  ]
  node [
    id 286
    label "bias"
  ]
  node [
    id 287
    label "warunek_lokalowy"
  ]
  node [
    id 288
    label "plac"
  ]
  node [
    id 289
    label "location"
  ]
  node [
    id 290
    label "uwaga"
  ]
  node [
    id 291
    label "przestrze&#324;"
  ]
  node [
    id 292
    label "status"
  ]
  node [
    id 293
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 294
    label "chwila"
  ]
  node [
    id 295
    label "cia&#322;o"
  ]
  node [
    id 296
    label "praca"
  ]
  node [
    id 297
    label "rz&#261;d"
  ]
  node [
    id 298
    label "Rzym_Zachodni"
  ]
  node [
    id 299
    label "whole"
  ]
  node [
    id 300
    label "ilo&#347;&#263;"
  ]
  node [
    id 301
    label "element"
  ]
  node [
    id 302
    label "Rzym_Wschodni"
  ]
  node [
    id 303
    label "urz&#261;dzenie"
  ]
  node [
    id 304
    label "dysproporcja"
  ]
  node [
    id 305
    label "koszmarek"
  ]
  node [
    id 306
    label "wygl&#261;d"
  ]
  node [
    id 307
    label "dysharmonia"
  ]
  node [
    id 308
    label "szkarada"
  ]
  node [
    id 309
    label "ugliness"
  ]
  node [
    id 310
    label "salariat"
  ]
  node [
    id 311
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 312
    label "delegowanie"
  ]
  node [
    id 313
    label "pracu&#347;"
  ]
  node [
    id 314
    label "r&#281;ka"
  ]
  node [
    id 315
    label "delegowa&#263;"
  ]
  node [
    id 316
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 317
    label "warstwa"
  ]
  node [
    id 318
    label "p&#322;aca"
  ]
  node [
    id 319
    label "ludzko&#347;&#263;"
  ]
  node [
    id 320
    label "asymilowanie"
  ]
  node [
    id 321
    label "asymilowa&#263;"
  ]
  node [
    id 322
    label "os&#322;abia&#263;"
  ]
  node [
    id 323
    label "posta&#263;"
  ]
  node [
    id 324
    label "hominid"
  ]
  node [
    id 325
    label "podw&#322;adny"
  ]
  node [
    id 326
    label "os&#322;abianie"
  ]
  node [
    id 327
    label "g&#322;owa"
  ]
  node [
    id 328
    label "figura"
  ]
  node [
    id 329
    label "portrecista"
  ]
  node [
    id 330
    label "dwun&#243;g"
  ]
  node [
    id 331
    label "profanum"
  ]
  node [
    id 332
    label "mikrokosmos"
  ]
  node [
    id 333
    label "nasada"
  ]
  node [
    id 334
    label "duch"
  ]
  node [
    id 335
    label "antropochoria"
  ]
  node [
    id 336
    label "osoba"
  ]
  node [
    id 337
    label "wz&#243;r"
  ]
  node [
    id 338
    label "oddzia&#322;ywanie"
  ]
  node [
    id 339
    label "Adam"
  ]
  node [
    id 340
    label "homo_sapiens"
  ]
  node [
    id 341
    label "polifag"
  ]
  node [
    id 342
    label "krzy&#380;"
  ]
  node [
    id 343
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 344
    label "handwriting"
  ]
  node [
    id 345
    label "d&#322;o&#324;"
  ]
  node [
    id 346
    label "gestykulowa&#263;"
  ]
  node [
    id 347
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 348
    label "palec"
  ]
  node [
    id 349
    label "przedrami&#281;"
  ]
  node [
    id 350
    label "hand"
  ]
  node [
    id 351
    label "&#322;okie&#263;"
  ]
  node [
    id 352
    label "hazena"
  ]
  node [
    id 353
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 354
    label "bramkarz"
  ]
  node [
    id 355
    label "nadgarstek"
  ]
  node [
    id 356
    label "graba"
  ]
  node [
    id 357
    label "r&#261;czyna"
  ]
  node [
    id 358
    label "k&#322;&#261;b"
  ]
  node [
    id 359
    label "pi&#322;ka"
  ]
  node [
    id 360
    label "chwyta&#263;"
  ]
  node [
    id 361
    label "cmoknonsens"
  ]
  node [
    id 362
    label "pomocnik"
  ]
  node [
    id 363
    label "gestykulowanie"
  ]
  node [
    id 364
    label "chwytanie"
  ]
  node [
    id 365
    label "obietnica"
  ]
  node [
    id 366
    label "spos&#243;b"
  ]
  node [
    id 367
    label "zagrywka"
  ]
  node [
    id 368
    label "kroki"
  ]
  node [
    id 369
    label "hasta"
  ]
  node [
    id 370
    label "wykroczenie"
  ]
  node [
    id 371
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 372
    label "czerwona_kartka"
  ]
  node [
    id 373
    label "paw"
  ]
  node [
    id 374
    label "rami&#281;"
  ]
  node [
    id 375
    label "wysy&#322;a&#263;"
  ]
  node [
    id 376
    label "air"
  ]
  node [
    id 377
    label "wys&#322;a&#263;"
  ]
  node [
    id 378
    label "oddelegowa&#263;"
  ]
  node [
    id 379
    label "oddelegowywa&#263;"
  ]
  node [
    id 380
    label "zapaleniec"
  ]
  node [
    id 381
    label "wysy&#322;anie"
  ]
  node [
    id 382
    label "wys&#322;anie"
  ]
  node [
    id 383
    label "delegacy"
  ]
  node [
    id 384
    label "oddelegowywanie"
  ]
  node [
    id 385
    label "oddelegowanie"
  ]
  node [
    id 386
    label "droga"
  ]
  node [
    id 387
    label "ploy"
  ]
  node [
    id 388
    label "nasi&#261;kni&#281;cie"
  ]
  node [
    id 389
    label "nabranie"
  ]
  node [
    id 390
    label "nastawienie"
  ]
  node [
    id 391
    label "potraktowanie"
  ]
  node [
    id 392
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 393
    label "powaga"
  ]
  node [
    id 394
    label "woda"
  ]
  node [
    id 395
    label "oszwabienie"
  ]
  node [
    id 396
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 397
    label "ponacinanie"
  ]
  node [
    id 398
    label "pozostanie"
  ]
  node [
    id 399
    label "przyw&#322;aszczenie"
  ]
  node [
    id 400
    label "pope&#322;nienie"
  ]
  node [
    id 401
    label "porobienie_si&#281;"
  ]
  node [
    id 402
    label "wkr&#281;cenie"
  ]
  node [
    id 403
    label "zdarcie"
  ]
  node [
    id 404
    label "fraud"
  ]
  node [
    id 405
    label "podstawienie"
  ]
  node [
    id 406
    label "kupienie"
  ]
  node [
    id 407
    label "nabranie_si&#281;"
  ]
  node [
    id 408
    label "procurement"
  ]
  node [
    id 409
    label "ogolenie"
  ]
  node [
    id 410
    label "zamydlenie_"
  ]
  node [
    id 411
    label "wzi&#281;cie"
  ]
  node [
    id 412
    label "delivery"
  ]
  node [
    id 413
    label "oddzia&#322;anie"
  ]
  node [
    id 414
    label "ekskursja"
  ]
  node [
    id 415
    label "bezsilnikowy"
  ]
  node [
    id 416
    label "budowla"
  ]
  node [
    id 417
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 418
    label "trasa"
  ]
  node [
    id 419
    label "podbieg"
  ]
  node [
    id 420
    label "turystyka"
  ]
  node [
    id 421
    label "nawierzchnia"
  ]
  node [
    id 422
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 423
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 424
    label "rajza"
  ]
  node [
    id 425
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 426
    label "korona_drogi"
  ]
  node [
    id 427
    label "passage"
  ]
  node [
    id 428
    label "wylot"
  ]
  node [
    id 429
    label "ekwipunek"
  ]
  node [
    id 430
    label "zbior&#243;wka"
  ]
  node [
    id 431
    label "marszrutyzacja"
  ]
  node [
    id 432
    label "wyb&#243;j"
  ]
  node [
    id 433
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 434
    label "drogowskaz"
  ]
  node [
    id 435
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 436
    label "pobocze"
  ]
  node [
    id 437
    label "journey"
  ]
  node [
    id 438
    label "charakterystyka"
  ]
  node [
    id 439
    label "m&#322;ot"
  ]
  node [
    id 440
    label "znak"
  ]
  node [
    id 441
    label "drzewo"
  ]
  node [
    id 442
    label "pr&#243;ba"
  ]
  node [
    id 443
    label "attribute"
  ]
  node [
    id 444
    label "marka"
  ]
  node [
    id 445
    label "nas&#261;czenie"
  ]
  node [
    id 446
    label "strain"
  ]
  node [
    id 447
    label "przej&#281;cie"
  ]
  node [
    id 448
    label "przemokni&#281;cie"
  ]
  node [
    id 449
    label "nasycenie_si&#281;"
  ]
  node [
    id 450
    label "przenikni&#281;cie"
  ]
  node [
    id 451
    label "ulegni&#281;cie"
  ]
  node [
    id 452
    label "przepojenie"
  ]
  node [
    id 453
    label "ustawienie"
  ]
  node [
    id 454
    label "z&#322;amanie"
  ]
  node [
    id 455
    label "set"
  ]
  node [
    id 456
    label "gotowanie_si&#281;"
  ]
  node [
    id 457
    label "ponastawianie"
  ]
  node [
    id 458
    label "bearing"
  ]
  node [
    id 459
    label "z&#322;o&#380;enie"
  ]
  node [
    id 460
    label "umieszczenie"
  ]
  node [
    id 461
    label "w&#322;&#261;czenie"
  ]
  node [
    id 462
    label "ukierunkowanie"
  ]
  node [
    id 463
    label "powa&#380;anie"
  ]
  node [
    id 464
    label "osobisto&#347;&#263;"
  ]
  node [
    id 465
    label "znaczenie"
  ]
  node [
    id 466
    label "znawca"
  ]
  node [
    id 467
    label "trudno&#347;&#263;"
  ]
  node [
    id 468
    label "opiniotw&#243;rczy"
  ]
  node [
    id 469
    label "legalizacja_ponowna"
  ]
  node [
    id 470
    label "instytucja"
  ]
  node [
    id 471
    label "w&#322;adza"
  ]
  node [
    id 472
    label "perlustracja"
  ]
  node [
    id 473
    label "czynno&#347;&#263;"
  ]
  node [
    id 474
    label "legalizacja_pierwotna"
  ]
  node [
    id 475
    label "examination"
  ]
  node [
    id 476
    label "struktura"
  ]
  node [
    id 477
    label "prawo"
  ]
  node [
    id 478
    label "rz&#261;dzenie"
  ]
  node [
    id 479
    label "panowanie"
  ]
  node [
    id 480
    label "Kreml"
  ]
  node [
    id 481
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 482
    label "wydolno&#347;&#263;"
  ]
  node [
    id 483
    label "grupa"
  ]
  node [
    id 484
    label "osoba_prawna"
  ]
  node [
    id 485
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 486
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 487
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 488
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 489
    label "biuro"
  ]
  node [
    id 490
    label "organizacja"
  ]
  node [
    id 491
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 492
    label "Fundusze_Unijne"
  ]
  node [
    id 493
    label "zamyka&#263;"
  ]
  node [
    id 494
    label "establishment"
  ]
  node [
    id 495
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 496
    label "urz&#261;d"
  ]
  node [
    id 497
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 498
    label "afiliowa&#263;"
  ]
  node [
    id 499
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 500
    label "standard"
  ]
  node [
    id 501
    label "zamykanie"
  ]
  node [
    id 502
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 503
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 504
    label "activity"
  ]
  node [
    id 505
    label "bezproblemowy"
  ]
  node [
    id 506
    label "failure"
  ]
  node [
    id 507
    label "zasi&#322;ek_chorobowy"
  ]
  node [
    id 508
    label "zasi&#322;ek"
  ]
  node [
    id 509
    label "zwolnienie"
  ]
  node [
    id 510
    label "zapomoga"
  ]
  node [
    id 511
    label "wylanie"
  ]
  node [
    id 512
    label "oddalenie"
  ]
  node [
    id 513
    label "ulga"
  ]
  node [
    id 514
    label "za&#347;wiadczenie"
  ]
  node [
    id 515
    label "wypowiedzenie"
  ]
  node [
    id 516
    label "liberation"
  ]
  node [
    id 517
    label "zmniejszenie"
  ]
  node [
    id 518
    label "dowolny"
  ]
  node [
    id 519
    label "spowodowanie"
  ]
  node [
    id 520
    label "spowolnienie"
  ]
  node [
    id 521
    label "relief"
  ]
  node [
    id 522
    label "nieobecno&#347;&#263;"
  ]
  node [
    id 523
    label "uwolnienie"
  ]
  node [
    id 524
    label "orzeczenie"
  ]
  node [
    id 525
    label "release"
  ]
  node [
    id 526
    label "performance"
  ]
  node [
    id 527
    label "wolniejszy"
  ]
  node [
    id 528
    label "rewizja"
  ]
  node [
    id 529
    label "oznaka"
  ]
  node [
    id 530
    label "change"
  ]
  node [
    id 531
    label "ferment"
  ]
  node [
    id 532
    label "komplet"
  ]
  node [
    id 533
    label "anatomopatolog"
  ]
  node [
    id 534
    label "zmianka"
  ]
  node [
    id 535
    label "czas"
  ]
  node [
    id 536
    label "amendment"
  ]
  node [
    id 537
    label "odmienianie"
  ]
  node [
    id 538
    label "tura"
  ]
  node [
    id 539
    label "boski"
  ]
  node [
    id 540
    label "krajobraz"
  ]
  node [
    id 541
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 542
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 543
    label "przywidzenie"
  ]
  node [
    id 544
    label "presence"
  ]
  node [
    id 545
    label "charakter"
  ]
  node [
    id 546
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 547
    label "lekcja"
  ]
  node [
    id 548
    label "ensemble"
  ]
  node [
    id 549
    label "klasa"
  ]
  node [
    id 550
    label "zestaw"
  ]
  node [
    id 551
    label "poprzedzanie"
  ]
  node [
    id 552
    label "czasoprzestrze&#324;"
  ]
  node [
    id 553
    label "laba"
  ]
  node [
    id 554
    label "chronometria"
  ]
  node [
    id 555
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 556
    label "rachuba_czasu"
  ]
  node [
    id 557
    label "przep&#322;ywanie"
  ]
  node [
    id 558
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 559
    label "czasokres"
  ]
  node [
    id 560
    label "odczyt"
  ]
  node [
    id 561
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 562
    label "dzieje"
  ]
  node [
    id 563
    label "kategoria_gramatyczna"
  ]
  node [
    id 564
    label "poprzedzenie"
  ]
  node [
    id 565
    label "trawienie"
  ]
  node [
    id 566
    label "pochodzi&#263;"
  ]
  node [
    id 567
    label "period"
  ]
  node [
    id 568
    label "okres_czasu"
  ]
  node [
    id 569
    label "poprzedza&#263;"
  ]
  node [
    id 570
    label "schy&#322;ek"
  ]
  node [
    id 571
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 572
    label "odwlekanie_si&#281;"
  ]
  node [
    id 573
    label "zegar"
  ]
  node [
    id 574
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 575
    label "czwarty_wymiar"
  ]
  node [
    id 576
    label "pochodzenie"
  ]
  node [
    id 577
    label "koniugacja"
  ]
  node [
    id 578
    label "Zeitgeist"
  ]
  node [
    id 579
    label "trawi&#263;"
  ]
  node [
    id 580
    label "pogoda"
  ]
  node [
    id 581
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 582
    label "poprzedzi&#263;"
  ]
  node [
    id 583
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 584
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 585
    label "time_period"
  ]
  node [
    id 586
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 587
    label "implikowa&#263;"
  ]
  node [
    id 588
    label "signal"
  ]
  node [
    id 589
    label "fakt"
  ]
  node [
    id 590
    label "symbol"
  ]
  node [
    id 591
    label "bia&#322;ko"
  ]
  node [
    id 592
    label "immobilizowa&#263;"
  ]
  node [
    id 593
    label "poruszenie"
  ]
  node [
    id 594
    label "immobilizacja"
  ]
  node [
    id 595
    label "apoenzym"
  ]
  node [
    id 596
    label "zymaza"
  ]
  node [
    id 597
    label "enzyme"
  ]
  node [
    id 598
    label "immobilizowanie"
  ]
  node [
    id 599
    label "biokatalizator"
  ]
  node [
    id 600
    label "proces_my&#347;lowy"
  ]
  node [
    id 601
    label "dow&#243;d"
  ]
  node [
    id 602
    label "krytyka"
  ]
  node [
    id 603
    label "rekurs"
  ]
  node [
    id 604
    label "checkup"
  ]
  node [
    id 605
    label "odwo&#322;anie"
  ]
  node [
    id 606
    label "correction"
  ]
  node [
    id 607
    label "przegl&#261;d"
  ]
  node [
    id 608
    label "kipisz"
  ]
  node [
    id 609
    label "korekta"
  ]
  node [
    id 610
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 611
    label "najem"
  ]
  node [
    id 612
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 613
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 614
    label "zak&#322;ad"
  ]
  node [
    id 615
    label "stosunek_pracy"
  ]
  node [
    id 616
    label "benedykty&#324;ski"
  ]
  node [
    id 617
    label "poda&#380;_pracy"
  ]
  node [
    id 618
    label "pracowanie"
  ]
  node [
    id 619
    label "tyrka"
  ]
  node [
    id 620
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 621
    label "zaw&#243;d"
  ]
  node [
    id 622
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 623
    label "tynkarski"
  ]
  node [
    id 624
    label "pracowa&#263;"
  ]
  node [
    id 625
    label "czynnik_produkcji"
  ]
  node [
    id 626
    label "zobowi&#261;zanie"
  ]
  node [
    id 627
    label "kierownictwo"
  ]
  node [
    id 628
    label "siedziba"
  ]
  node [
    id 629
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 630
    label "patolog"
  ]
  node [
    id 631
    label "anatom"
  ]
  node [
    id 632
    label "sparafrazowanie"
  ]
  node [
    id 633
    label "zmienianie"
  ]
  node [
    id 634
    label "parafrazowanie"
  ]
  node [
    id 635
    label "zamiana"
  ]
  node [
    id 636
    label "wymienianie"
  ]
  node [
    id 637
    label "Transfiguration"
  ]
  node [
    id 638
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 639
    label "norma_prawna"
  ]
  node [
    id 640
    label "przedawnienie_si&#281;"
  ]
  node [
    id 641
    label "przedawnianie_si&#281;"
  ]
  node [
    id 642
    label "porada"
  ]
  node [
    id 643
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 644
    label "regulation"
  ]
  node [
    id 645
    label "recepta"
  ]
  node [
    id 646
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 647
    label "kodeks"
  ]
  node [
    id 648
    label "wskaz&#243;wka"
  ]
  node [
    id 649
    label "model"
  ]
  node [
    id 650
    label "narz&#281;dzie"
  ]
  node [
    id 651
    label "zbi&#243;r"
  ]
  node [
    id 652
    label "tryb"
  ]
  node [
    id 653
    label "nature"
  ]
  node [
    id 654
    label "zlecenie"
  ]
  node [
    id 655
    label "receipt"
  ]
  node [
    id 656
    label "receptariusz"
  ]
  node [
    id 657
    label "obwiniony"
  ]
  node [
    id 658
    label "r&#281;kopis"
  ]
  node [
    id 659
    label "kodeks_pracy"
  ]
  node [
    id 660
    label "kodeks_morski"
  ]
  node [
    id 661
    label "Justynian"
  ]
  node [
    id 662
    label "code"
  ]
  node [
    id 663
    label "kodeks_drogowy"
  ]
  node [
    id 664
    label "zasada"
  ]
  node [
    id 665
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 666
    label "kodeks_rodzinny"
  ]
  node [
    id 667
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 668
    label "kodeks_cywilny"
  ]
  node [
    id 669
    label "kodeks_karny"
  ]
  node [
    id 670
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 671
    label "umocowa&#263;"
  ]
  node [
    id 672
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 673
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 674
    label "procesualistyka"
  ]
  node [
    id 675
    label "regu&#322;a_Allena"
  ]
  node [
    id 676
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 677
    label "kryminalistyka"
  ]
  node [
    id 678
    label "szko&#322;a"
  ]
  node [
    id 679
    label "kierunek"
  ]
  node [
    id 680
    label "zasada_d'Alemberta"
  ]
  node [
    id 681
    label "obserwacja"
  ]
  node [
    id 682
    label "normatywizm"
  ]
  node [
    id 683
    label "jurisprudence"
  ]
  node [
    id 684
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 685
    label "kultura_duchowa"
  ]
  node [
    id 686
    label "prawo_karne_procesowe"
  ]
  node [
    id 687
    label "criterion"
  ]
  node [
    id 688
    label "kazuistyka"
  ]
  node [
    id 689
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 690
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 691
    label "kryminologia"
  ]
  node [
    id 692
    label "opis"
  ]
  node [
    id 693
    label "regu&#322;a_Glogera"
  ]
  node [
    id 694
    label "prawo_Mendla"
  ]
  node [
    id 695
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 696
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 697
    label "prawo_karne"
  ]
  node [
    id 698
    label "legislacyjnie"
  ]
  node [
    id 699
    label "twierdzenie"
  ]
  node [
    id 700
    label "cywilistyka"
  ]
  node [
    id 701
    label "judykatura"
  ]
  node [
    id 702
    label "kanonistyka"
  ]
  node [
    id 703
    label "nauka_prawa"
  ]
  node [
    id 704
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 705
    label "podmiot"
  ]
  node [
    id 706
    label "law"
  ]
  node [
    id 707
    label "qualification"
  ]
  node [
    id 708
    label "dominion"
  ]
  node [
    id 709
    label "wykonawczy"
  ]
  node [
    id 710
    label "normalizacja"
  ]
  node [
    id 711
    label "gauge"
  ]
  node [
    id 712
    label "strike"
  ]
  node [
    id 713
    label "okre&#347;la&#263;"
  ]
  node [
    id 714
    label "s&#261;dzi&#263;"
  ]
  node [
    id 715
    label "znajdowa&#263;"
  ]
  node [
    id 716
    label "award"
  ]
  node [
    id 717
    label "wystawia&#263;"
  ]
  node [
    id 718
    label "budowa&#263;"
  ]
  node [
    id 719
    label "wyjmowa&#263;"
  ]
  node [
    id 720
    label "proponowa&#263;"
  ]
  node [
    id 721
    label "wynosi&#263;"
  ]
  node [
    id 722
    label "wypisywa&#263;"
  ]
  node [
    id 723
    label "wskazywa&#263;"
  ]
  node [
    id 724
    label "dopuszcza&#263;"
  ]
  node [
    id 725
    label "wyra&#380;a&#263;"
  ]
  node [
    id 726
    label "wysuwa&#263;"
  ]
  node [
    id 727
    label "eksponowa&#263;"
  ]
  node [
    id 728
    label "pies_my&#347;liwski"
  ]
  node [
    id 729
    label "represent"
  ]
  node [
    id 730
    label "typify"
  ]
  node [
    id 731
    label "wychyla&#263;"
  ]
  node [
    id 732
    label "przedstawia&#263;"
  ]
  node [
    id 733
    label "wsp&#243;&#322;rz&#261;dzi&#263;"
  ]
  node [
    id 734
    label "robi&#263;"
  ]
  node [
    id 735
    label "my&#347;le&#263;"
  ]
  node [
    id 736
    label "deliver"
  ]
  node [
    id 737
    label "powodowa&#263;"
  ]
  node [
    id 738
    label "hold"
  ]
  node [
    id 739
    label "sprawowa&#263;"
  ]
  node [
    id 740
    label "os&#261;dza&#263;"
  ]
  node [
    id 741
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 742
    label "zas&#261;dza&#263;"
  ]
  node [
    id 743
    label "decydowa&#263;"
  ]
  node [
    id 744
    label "signify"
  ]
  node [
    id 745
    label "style"
  ]
  node [
    id 746
    label "odzyskiwa&#263;"
  ]
  node [
    id 747
    label "znachodzi&#263;"
  ]
  node [
    id 748
    label "pozyskiwa&#263;"
  ]
  node [
    id 749
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 750
    label "detect"
  ]
  node [
    id 751
    label "unwrap"
  ]
  node [
    id 752
    label "wykrywa&#263;"
  ]
  node [
    id 753
    label "wymy&#347;la&#263;"
  ]
  node [
    id 754
    label "wyspecjalizowanie_si&#281;"
  ]
  node [
    id 755
    label "lekarz"
  ]
  node [
    id 756
    label "spec"
  ]
  node [
    id 757
    label "Mesmer"
  ]
  node [
    id 758
    label "Galen"
  ]
  node [
    id 759
    label "zbada&#263;"
  ]
  node [
    id 760
    label "medyk"
  ]
  node [
    id 761
    label "eskulap"
  ]
  node [
    id 762
    label "lekarze"
  ]
  node [
    id 763
    label "Hipokrates"
  ]
  node [
    id 764
    label "dokt&#243;r"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 18
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 39
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 639
  ]
  edge [
    source 14
    target 640
  ]
  edge [
    source 14
    target 366
  ]
  edge [
    source 14
    target 641
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 643
  ]
  edge [
    source 14
    target 644
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 647
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 14
    target 650
  ]
  edge [
    source 14
    target 651
  ]
  edge [
    source 14
    target 652
  ]
  edge [
    source 14
    target 653
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 657
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 17
    target 254
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 319
  ]
  edge [
    source 17
    target 320
  ]
  edge [
    source 17
    target 260
  ]
  edge [
    source 17
    target 321
  ]
  edge [
    source 17
    target 322
  ]
  edge [
    source 17
    target 323
  ]
  edge [
    source 17
    target 324
  ]
  edge [
    source 17
    target 325
  ]
  edge [
    source 17
    target 326
  ]
  edge [
    source 17
    target 327
  ]
  edge [
    source 17
    target 328
  ]
  edge [
    source 17
    target 329
  ]
  edge [
    source 17
    target 330
  ]
  edge [
    source 17
    target 331
  ]
  edge [
    source 17
    target 332
  ]
  edge [
    source 17
    target 333
  ]
  edge [
    source 17
    target 334
  ]
  edge [
    source 17
    target 335
  ]
  edge [
    source 17
    target 336
  ]
  edge [
    source 17
    target 337
  ]
  edge [
    source 17
    target 258
  ]
  edge [
    source 17
    target 338
  ]
  edge [
    source 17
    target 339
  ]
  edge [
    source 17
    target 340
  ]
  edge [
    source 17
    target 341
  ]
]
