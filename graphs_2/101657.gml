graph [
  node [
    id 0
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ogl&#261;da&#263;"
  ]
  node [
    id 2
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 3
    label "notice"
  ]
  node [
    id 4
    label "perceive"
  ]
  node [
    id 5
    label "punkt_widzenia"
  ]
  node [
    id 6
    label "zmale&#263;"
  ]
  node [
    id 7
    label "male&#263;"
  ]
  node [
    id 8
    label "go_steady"
  ]
  node [
    id 9
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 10
    label "postrzega&#263;"
  ]
  node [
    id 11
    label "dostrzega&#263;"
  ]
  node [
    id 12
    label "aprobowa&#263;"
  ]
  node [
    id 13
    label "spowodowa&#263;"
  ]
  node [
    id 14
    label "os&#261;dza&#263;"
  ]
  node [
    id 15
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 16
    label "spotka&#263;"
  ]
  node [
    id 17
    label "reagowa&#263;"
  ]
  node [
    id 18
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 19
    label "wzrok"
  ]
  node [
    id 20
    label "react"
  ]
  node [
    id 21
    label "answer"
  ]
  node [
    id 22
    label "odpowiada&#263;"
  ]
  node [
    id 23
    label "uczestniczy&#263;"
  ]
  node [
    id 24
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 25
    label "dochodzi&#263;"
  ]
  node [
    id 26
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 27
    label "doj&#347;&#263;"
  ]
  node [
    id 28
    label "obacza&#263;"
  ]
  node [
    id 29
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 30
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 31
    label "styka&#263;_si&#281;"
  ]
  node [
    id 32
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 33
    label "visualize"
  ]
  node [
    id 34
    label "pozna&#263;"
  ]
  node [
    id 35
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 36
    label "insert"
  ]
  node [
    id 37
    label "znale&#378;&#263;"
  ]
  node [
    id 38
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 39
    label "befall"
  ]
  node [
    id 40
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "uznawa&#263;"
  ]
  node [
    id 42
    label "approbate"
  ]
  node [
    id 43
    label "act"
  ]
  node [
    id 44
    label "robi&#263;"
  ]
  node [
    id 45
    label "hold"
  ]
  node [
    id 46
    label "strike"
  ]
  node [
    id 47
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 48
    label "s&#261;dzi&#263;"
  ]
  node [
    id 49
    label "powodowa&#263;"
  ]
  node [
    id 50
    label "znajdowa&#263;"
  ]
  node [
    id 51
    label "traktowa&#263;"
  ]
  node [
    id 52
    label "nagradza&#263;"
  ]
  node [
    id 53
    label "sign"
  ]
  node [
    id 54
    label "forytowa&#263;"
  ]
  node [
    id 55
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 56
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 57
    label "okulista"
  ]
  node [
    id 58
    label "zmys&#322;"
  ]
  node [
    id 59
    label "oko"
  ]
  node [
    id 60
    label "widzenie"
  ]
  node [
    id 61
    label "m&#281;tnienie"
  ]
  node [
    id 62
    label "m&#281;tnie&#263;"
  ]
  node [
    id 63
    label "expression"
  ]
  node [
    id 64
    label "kontakt"
  ]
  node [
    id 65
    label "relax"
  ]
  node [
    id 66
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 67
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 68
    label "slack"
  ]
  node [
    id 69
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 70
    label "worsen"
  ]
  node [
    id 71
    label "zmniejszy&#263;_si&#281;"
  ]
  node [
    id 72
    label "reduce"
  ]
  node [
    id 73
    label "sta&#263;_si&#281;"
  ]
  node [
    id 74
    label "arkada"
  ]
  node [
    id 75
    label "Fiedler"
  ]
  node [
    id 76
    label "platforma"
  ]
  node [
    id 77
    label "obywatelski"
  ]
  node [
    id 78
    label "ameryk"
  ]
  node [
    id 79
    label "p&#243;&#322;nocny"
  ]
  node [
    id 80
    label "stan"
  ]
  node [
    id 81
    label "zjednoczy&#263;"
  ]
  node [
    id 82
    label "drugi"
  ]
  node [
    id 83
    label "wojna"
  ]
  node [
    id 84
    label "&#347;wiatowy"
  ]
  node [
    id 85
    label "Tomasz"
  ]
  node [
    id 86
    label "mannowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 80
  ]
  edge [
    source 78
    target 81
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 81
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 84
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 85
    target 86
  ]
]
