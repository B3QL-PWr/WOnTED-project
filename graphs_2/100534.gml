graph [
  node [
    id 0
    label "pal"
    origin "text"
  ]
  node [
    id 1
    label "pra&#269;a"
    origin "text"
  ]
  node [
    id 2
    label "picket"
  ]
  node [
    id 3
    label "s&#322;up"
  ]
  node [
    id 4
    label "dalba"
  ]
  node [
    id 5
    label "paliszcze"
  ]
  node [
    id 6
    label "palisada"
  ]
  node [
    id 7
    label "morze"
  ]
  node [
    id 8
    label "grobla"
  ]
  node [
    id 9
    label "pa&#322;anka"
  ]
  node [
    id 10
    label "umocnienie"
  ]
  node [
    id 11
    label "budowla"
  ]
  node [
    id 12
    label "fortyfikacja"
  ]
  node [
    id 13
    label "fence"
  ]
  node [
    id 14
    label "ogrodzenie"
  ]
  node [
    id 15
    label "rz&#261;d"
  ]
  node [
    id 16
    label "chmura"
  ]
  node [
    id 17
    label "przedmiot"
  ]
  node [
    id 18
    label "wska&#378;nik"
  ]
  node [
    id 19
    label "pas"
  ]
  node [
    id 20
    label "figura_heraldyczna"
  ]
  node [
    id 21
    label "oszustwo"
  ]
  node [
    id 22
    label "formacja_geologiczna"
  ]
  node [
    id 23
    label "plume"
  ]
  node [
    id 24
    label "heraldyka"
  ]
  node [
    id 25
    label "tarcza_herbowa"
  ]
  node [
    id 26
    label "upright"
  ]
  node [
    id 27
    label "osoba_fizyczna"
  ]
  node [
    id 28
    label "konstrukcja"
  ]
  node [
    id 29
    label "pa&#322;a"
  ]
  node [
    id 30
    label "Pra&#269;a"
  ]
  node [
    id 31
    label "Bo&#347;nia"
  ]
  node [
    id 32
    label "Podrinje"
  ]
  node [
    id 33
    label "i"
  ]
  node [
    id 34
    label "Hercegowina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 33
    target 34
  ]
]
