graph [
  node [
    id 0
    label "iskra"
    origin "text"
  ]
  node [
    id 1
    label "cz&#322;owiek"
  ]
  node [
    id 2
    label "blask"
  ]
  node [
    id 3
    label "odrobina"
  ]
  node [
    id 4
    label "b&#322;ysk"
  ]
  node [
    id 5
    label "glint"
  ]
  node [
    id 6
    label "odblask"
  ]
  node [
    id 7
    label "flicker"
  ]
  node [
    id 8
    label "przyczyna"
  ]
  node [
    id 9
    label "pocz&#261;tek"
  ]
  node [
    id 10
    label "freshness"
  ]
  node [
    id 11
    label "discharge"
  ]
  node [
    id 12
    label "&#380;agiew"
  ]
  node [
    id 13
    label "miejsce"
  ]
  node [
    id 14
    label "upgrade"
  ]
  node [
    id 15
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 16
    label "pierworodztwo"
  ]
  node [
    id 17
    label "faza"
  ]
  node [
    id 18
    label "nast&#281;pstwo"
  ]
  node [
    id 19
    label "czynnik"
  ]
  node [
    id 20
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 21
    label "subject"
  ]
  node [
    id 22
    label "rezultat"
  ]
  node [
    id 23
    label "poci&#261;ganie"
  ]
  node [
    id 24
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 25
    label "geneza"
  ]
  node [
    id 26
    label "matuszka"
  ]
  node [
    id 27
    label "asymilowa&#263;"
  ]
  node [
    id 28
    label "nasada"
  ]
  node [
    id 29
    label "profanum"
  ]
  node [
    id 30
    label "wz&#243;r"
  ]
  node [
    id 31
    label "senior"
  ]
  node [
    id 32
    label "asymilowanie"
  ]
  node [
    id 33
    label "os&#322;abia&#263;"
  ]
  node [
    id 34
    label "homo_sapiens"
  ]
  node [
    id 35
    label "osoba"
  ]
  node [
    id 36
    label "ludzko&#347;&#263;"
  ]
  node [
    id 37
    label "Adam"
  ]
  node [
    id 38
    label "hominid"
  ]
  node [
    id 39
    label "posta&#263;"
  ]
  node [
    id 40
    label "portrecista"
  ]
  node [
    id 41
    label "polifag"
  ]
  node [
    id 42
    label "podw&#322;adny"
  ]
  node [
    id 43
    label "dwun&#243;g"
  ]
  node [
    id 44
    label "wapniak"
  ]
  node [
    id 45
    label "duch"
  ]
  node [
    id 46
    label "os&#322;abianie"
  ]
  node [
    id 47
    label "antropochoria"
  ]
  node [
    id 48
    label "figura"
  ]
  node [
    id 49
    label "g&#322;owa"
  ]
  node [
    id 50
    label "mikrokosmos"
  ]
  node [
    id 51
    label "oddzia&#322;ywanie"
  ]
  node [
    id 52
    label "&#322;ysk"
  ]
  node [
    id 53
    label "oznaka"
  ]
  node [
    id 54
    label "wyraz"
  ]
  node [
    id 55
    label "b&#322;ystka"
  ]
  node [
    id 56
    label "ostentation"
  ]
  node [
    id 57
    label "radiance"
  ]
  node [
    id 58
    label "porz&#261;dek"
  ]
  node [
    id 59
    label "chwila"
  ]
  node [
    id 60
    label "&#347;wiat&#322;o"
  ]
  node [
    id 61
    label "wspania&#322;o&#347;&#263;"
  ]
  node [
    id 62
    label "luminosity"
  ]
  node [
    id 63
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 64
    label "light"
  ]
  node [
    id 65
    label "reflection"
  ]
  node [
    id 66
    label "znaczek"
  ]
  node [
    id 67
    label "grain"
  ]
  node [
    id 68
    label "ilo&#347;&#263;"
  ]
  node [
    id 69
    label "dash"
  ]
  node [
    id 70
    label "intensywno&#347;&#263;"
  ]
  node [
    id 71
    label "polano"
  ]
  node [
    id 72
    label "paso&#380;yt"
  ]
  node [
    id 73
    label "grzyb"
  ]
  node [
    id 74
    label "grzyb_nadrzewny"
  ]
  node [
    id 75
    label "&#380;agwiowate"
  ]
  node [
    id 76
    label "&#380;agnica"
  ]
  node [
    id 77
    label "brand"
  ]
  node [
    id 78
    label "pieczarniak"
  ]
  node [
    id 79
    label "saprotrof"
  ]
  node [
    id 80
    label "zarzewie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
]
