graph [
  node [
    id 0
    label "cerkiew"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 2
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 3
    label "brze&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 5
    label "Ko&#347;ci&#243;&#322;_prawos&#322;awny"
  ]
  node [
    id 6
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 7
    label "babiniec"
  ]
  node [
    id 8
    label "ikonostas"
  ]
  node [
    id 9
    label "kliros"
  ]
  node [
    id 10
    label "Ko&#347;ci&#243;&#322;_greckokatolicki"
  ]
  node [
    id 11
    label "carskie_wrota"
  ]
  node [
    id 12
    label "kult"
  ]
  node [
    id 13
    label "ub&#322;agalnia"
  ]
  node [
    id 14
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 15
    label "nawa"
  ]
  node [
    id 16
    label "wsp&#243;lnota"
  ]
  node [
    id 17
    label "Ska&#322;ka"
  ]
  node [
    id 18
    label "zakrystia"
  ]
  node [
    id 19
    label "prezbiterium"
  ]
  node [
    id 20
    label "kropielnica"
  ]
  node [
    id 21
    label "organizacja_religijna"
  ]
  node [
    id 22
    label "nerwica_eklezjogenna"
  ]
  node [
    id 23
    label "church"
  ]
  node [
    id 24
    label "kruchta"
  ]
  node [
    id 25
    label "dom"
  ]
  node [
    id 26
    label "podium"
  ]
  node [
    id 27
    label "synagoga"
  ]
  node [
    id 28
    label "izba"
  ]
  node [
    id 29
    label "przedsionek"
  ]
  node [
    id 30
    label "pomieszczenie"
  ]
  node [
    id 31
    label "grupa"
  ]
  node [
    id 32
    label "przegroda"
  ]
  node [
    id 33
    label "ikona"
  ]
  node [
    id 34
    label "s&#261;d"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "uczestnik"
  ]
  node [
    id 37
    label "dru&#380;ba"
  ]
  node [
    id 38
    label "obserwator"
  ]
  node [
    id 39
    label "osoba_fizyczna"
  ]
  node [
    id 40
    label "ludzko&#347;&#263;"
  ]
  node [
    id 41
    label "asymilowanie"
  ]
  node [
    id 42
    label "wapniak"
  ]
  node [
    id 43
    label "asymilowa&#263;"
  ]
  node [
    id 44
    label "os&#322;abia&#263;"
  ]
  node [
    id 45
    label "posta&#263;"
  ]
  node [
    id 46
    label "hominid"
  ]
  node [
    id 47
    label "podw&#322;adny"
  ]
  node [
    id 48
    label "os&#322;abianie"
  ]
  node [
    id 49
    label "g&#322;owa"
  ]
  node [
    id 50
    label "figura"
  ]
  node [
    id 51
    label "portrecista"
  ]
  node [
    id 52
    label "dwun&#243;g"
  ]
  node [
    id 53
    label "profanum"
  ]
  node [
    id 54
    label "mikrokosmos"
  ]
  node [
    id 55
    label "nasada"
  ]
  node [
    id 56
    label "duch"
  ]
  node [
    id 57
    label "antropochoria"
  ]
  node [
    id 58
    label "osoba"
  ]
  node [
    id 59
    label "wz&#243;r"
  ]
  node [
    id 60
    label "senior"
  ]
  node [
    id 61
    label "oddzia&#322;ywanie"
  ]
  node [
    id 62
    label "Adam"
  ]
  node [
    id 63
    label "homo_sapiens"
  ]
  node [
    id 64
    label "polifag"
  ]
  node [
    id 65
    label "ogl&#261;dacz"
  ]
  node [
    id 66
    label "widownia"
  ]
  node [
    id 67
    label "wys&#322;annik"
  ]
  node [
    id 68
    label "za&#380;y&#322;o&#347;&#263;"
  ]
  node [
    id 69
    label "&#347;lub"
  ]
  node [
    id 70
    label "zesp&#243;&#322;"
  ]
  node [
    id 71
    label "podejrzany"
  ]
  node [
    id 72
    label "s&#261;downictwo"
  ]
  node [
    id 73
    label "system"
  ]
  node [
    id 74
    label "biuro"
  ]
  node [
    id 75
    label "wytw&#243;r"
  ]
  node [
    id 76
    label "court"
  ]
  node [
    id 77
    label "forum"
  ]
  node [
    id 78
    label "bronienie"
  ]
  node [
    id 79
    label "urz&#261;d"
  ]
  node [
    id 80
    label "wydarzenie"
  ]
  node [
    id 81
    label "oskar&#380;yciel"
  ]
  node [
    id 82
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 83
    label "skazany"
  ]
  node [
    id 84
    label "post&#281;powanie"
  ]
  node [
    id 85
    label "broni&#263;"
  ]
  node [
    id 86
    label "my&#347;l"
  ]
  node [
    id 87
    label "pods&#261;dny"
  ]
  node [
    id 88
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 89
    label "obrona"
  ]
  node [
    id 90
    label "wypowied&#378;"
  ]
  node [
    id 91
    label "instytucja"
  ]
  node [
    id 92
    label "antylogizm"
  ]
  node [
    id 93
    label "konektyw"
  ]
  node [
    id 94
    label "procesowicz"
  ]
  node [
    id 95
    label "strona"
  ]
  node [
    id 96
    label "stand"
  ]
  node [
    id 97
    label "&#347;wi&#281;ty"
  ]
  node [
    id 98
    label "wielki"
  ]
  node [
    id 99
    label "ksi&#281;stwo"
  ]
  node [
    id 100
    label "litewski"
  ]
  node [
    id 101
    label "twierdza"
  ]
  node [
    id 102
    label "brzeski"
  ]
  node [
    id 103
    label "sob&#243;r"
  ]
  node [
    id 104
    label "unia"
  ]
  node [
    id 105
    label "ziemia"
  ]
  node [
    id 106
    label "zabra&#263;"
  ]
  node [
    id 107
    label "greckokatolicki"
  ]
  node [
    id 108
    label "rosyjski"
  ]
  node [
    id 109
    label "prawos&#322;awny"
  ]
  node [
    id 110
    label "stary"
  ]
  node [
    id 111
    label "miasto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 98
    target 100
  ]
  edge [
    source 99
    target 100
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 104
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 110
    target 111
  ]
]
