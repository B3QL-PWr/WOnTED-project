graph [
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kolejny"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "zabawa"
    origin "text"
  ]
  node [
    id 4
    label "mirkoangielski"
    origin "text"
  ]
  node [
    id 5
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 6
    label "kto"
    origin "text"
  ]
  node [
    id 7
    label "zaplusuje"
    origin "text"
  ]
  node [
    id 8
    label "ten"
    origin "text"
  ]
  node [
    id 9
    label "wpis"
    origin "text"
  ]
  node [
    id 10
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "nast&#281;pny"
    origin "text"
  ]
  node [
    id 12
    label "nowa"
    origin "text"
  ]
  node [
    id 13
    label "s&#322;&#243;wko"
    origin "text"
  ]
  node [
    id 14
    label "nauka"
    origin "text"
  ]
  node [
    id 15
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 16
    label "invite"
  ]
  node [
    id 17
    label "ask"
  ]
  node [
    id 18
    label "oferowa&#263;"
  ]
  node [
    id 19
    label "zach&#281;ca&#263;"
  ]
  node [
    id 20
    label "volunteer"
  ]
  node [
    id 21
    label "nast&#281;pnie"
  ]
  node [
    id 22
    label "inny"
  ]
  node [
    id 23
    label "nastopny"
  ]
  node [
    id 24
    label "kolejno"
  ]
  node [
    id 25
    label "kt&#243;ry&#347;"
  ]
  node [
    id 26
    label "osobno"
  ]
  node [
    id 27
    label "r&#243;&#380;ny"
  ]
  node [
    id 28
    label "inszy"
  ]
  node [
    id 29
    label "inaczej"
  ]
  node [
    id 30
    label "ranek"
  ]
  node [
    id 31
    label "doba"
  ]
  node [
    id 32
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 33
    label "noc"
  ]
  node [
    id 34
    label "podwiecz&#243;r"
  ]
  node [
    id 35
    label "po&#322;udnie"
  ]
  node [
    id 36
    label "godzina"
  ]
  node [
    id 37
    label "przedpo&#322;udnie"
  ]
  node [
    id 38
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 39
    label "long_time"
  ]
  node [
    id 40
    label "wiecz&#243;r"
  ]
  node [
    id 41
    label "t&#322;usty_czwartek"
  ]
  node [
    id 42
    label "popo&#322;udnie"
  ]
  node [
    id 43
    label "walentynki"
  ]
  node [
    id 44
    label "czynienie_si&#281;"
  ]
  node [
    id 45
    label "s&#322;o&#324;ce"
  ]
  node [
    id 46
    label "rano"
  ]
  node [
    id 47
    label "tydzie&#324;"
  ]
  node [
    id 48
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 49
    label "wzej&#347;cie"
  ]
  node [
    id 50
    label "czas"
  ]
  node [
    id 51
    label "wsta&#263;"
  ]
  node [
    id 52
    label "day"
  ]
  node [
    id 53
    label "termin"
  ]
  node [
    id 54
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 55
    label "wstanie"
  ]
  node [
    id 56
    label "przedwiecz&#243;r"
  ]
  node [
    id 57
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 58
    label "Sylwester"
  ]
  node [
    id 59
    label "poprzedzanie"
  ]
  node [
    id 60
    label "czasoprzestrze&#324;"
  ]
  node [
    id 61
    label "laba"
  ]
  node [
    id 62
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 63
    label "chronometria"
  ]
  node [
    id 64
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 65
    label "rachuba_czasu"
  ]
  node [
    id 66
    label "przep&#322;ywanie"
  ]
  node [
    id 67
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 68
    label "czasokres"
  ]
  node [
    id 69
    label "odczyt"
  ]
  node [
    id 70
    label "chwila"
  ]
  node [
    id 71
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 72
    label "dzieje"
  ]
  node [
    id 73
    label "kategoria_gramatyczna"
  ]
  node [
    id 74
    label "poprzedzenie"
  ]
  node [
    id 75
    label "trawienie"
  ]
  node [
    id 76
    label "pochodzi&#263;"
  ]
  node [
    id 77
    label "period"
  ]
  node [
    id 78
    label "okres_czasu"
  ]
  node [
    id 79
    label "poprzedza&#263;"
  ]
  node [
    id 80
    label "schy&#322;ek"
  ]
  node [
    id 81
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 82
    label "odwlekanie_si&#281;"
  ]
  node [
    id 83
    label "zegar"
  ]
  node [
    id 84
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 85
    label "czwarty_wymiar"
  ]
  node [
    id 86
    label "pochodzenie"
  ]
  node [
    id 87
    label "koniugacja"
  ]
  node [
    id 88
    label "Zeitgeist"
  ]
  node [
    id 89
    label "trawi&#263;"
  ]
  node [
    id 90
    label "pogoda"
  ]
  node [
    id 91
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 92
    label "poprzedzi&#263;"
  ]
  node [
    id 93
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 94
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 95
    label "time_period"
  ]
  node [
    id 96
    label "nazewnictwo"
  ]
  node [
    id 97
    label "term"
  ]
  node [
    id 98
    label "przypadni&#281;cie"
  ]
  node [
    id 99
    label "ekspiracja"
  ]
  node [
    id 100
    label "przypa&#347;&#263;"
  ]
  node [
    id 101
    label "chronogram"
  ]
  node [
    id 102
    label "praktyka"
  ]
  node [
    id 103
    label "nazwa"
  ]
  node [
    id 104
    label "odwieczerz"
  ]
  node [
    id 105
    label "pora"
  ]
  node [
    id 106
    label "przyj&#281;cie"
  ]
  node [
    id 107
    label "spotkanie"
  ]
  node [
    id 108
    label "night"
  ]
  node [
    id 109
    label "zach&#243;d"
  ]
  node [
    id 110
    label "vesper"
  ]
  node [
    id 111
    label "aurora"
  ]
  node [
    id 112
    label "wsch&#243;d"
  ]
  node [
    id 113
    label "zjawisko"
  ]
  node [
    id 114
    label "&#347;rodek"
  ]
  node [
    id 115
    label "obszar"
  ]
  node [
    id 116
    label "Ziemia"
  ]
  node [
    id 117
    label "dwunasta"
  ]
  node [
    id 118
    label "strona_&#347;wiata"
  ]
  node [
    id 119
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 120
    label "dopo&#322;udnie"
  ]
  node [
    id 121
    label "blady_&#347;wit"
  ]
  node [
    id 122
    label "podkurek"
  ]
  node [
    id 123
    label "time"
  ]
  node [
    id 124
    label "p&#243;&#322;godzina"
  ]
  node [
    id 125
    label "jednostka_czasu"
  ]
  node [
    id 126
    label "minuta"
  ]
  node [
    id 127
    label "kwadrans"
  ]
  node [
    id 128
    label "p&#243;&#322;noc"
  ]
  node [
    id 129
    label "nokturn"
  ]
  node [
    id 130
    label "jednostka_geologiczna"
  ]
  node [
    id 131
    label "weekend"
  ]
  node [
    id 132
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 133
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 134
    label "miesi&#261;c"
  ]
  node [
    id 135
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 136
    label "mount"
  ]
  node [
    id 137
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 138
    label "wzej&#347;&#263;"
  ]
  node [
    id 139
    label "ascend"
  ]
  node [
    id 140
    label "kuca&#263;"
  ]
  node [
    id 141
    label "wyzdrowie&#263;"
  ]
  node [
    id 142
    label "opu&#347;ci&#263;"
  ]
  node [
    id 143
    label "rise"
  ]
  node [
    id 144
    label "arise"
  ]
  node [
    id 145
    label "stan&#261;&#263;"
  ]
  node [
    id 146
    label "przesta&#263;"
  ]
  node [
    id 147
    label "wyzdrowienie"
  ]
  node [
    id 148
    label "le&#380;enie"
  ]
  node [
    id 149
    label "kl&#281;czenie"
  ]
  node [
    id 150
    label "opuszczenie"
  ]
  node [
    id 151
    label "uniesienie_si&#281;"
  ]
  node [
    id 152
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 153
    label "siedzenie"
  ]
  node [
    id 154
    label "beginning"
  ]
  node [
    id 155
    label "przestanie"
  ]
  node [
    id 156
    label "S&#322;o&#324;ce"
  ]
  node [
    id 157
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 158
    label "&#347;wiat&#322;o"
  ]
  node [
    id 159
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 160
    label "kochanie"
  ]
  node [
    id 161
    label "sunlight"
  ]
  node [
    id 162
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 163
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 164
    label "grudzie&#324;"
  ]
  node [
    id 165
    label "luty"
  ]
  node [
    id 166
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 167
    label "rozrywka"
  ]
  node [
    id 168
    label "impreza"
  ]
  node [
    id 169
    label "igraszka"
  ]
  node [
    id 170
    label "taniec"
  ]
  node [
    id 171
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 172
    label "gambling"
  ]
  node [
    id 173
    label "chwyt"
  ]
  node [
    id 174
    label "game"
  ]
  node [
    id 175
    label "igra"
  ]
  node [
    id 176
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 177
    label "cecha"
  ]
  node [
    id 178
    label "nabawienie_si&#281;"
  ]
  node [
    id 179
    label "ubaw"
  ]
  node [
    id 180
    label "wodzirej"
  ]
  node [
    id 181
    label "charakterystyka"
  ]
  node [
    id 182
    label "m&#322;ot"
  ]
  node [
    id 183
    label "znak"
  ]
  node [
    id 184
    label "drzewo"
  ]
  node [
    id 185
    label "pr&#243;ba"
  ]
  node [
    id 186
    label "attribute"
  ]
  node [
    id 187
    label "marka"
  ]
  node [
    id 188
    label "impra"
  ]
  node [
    id 189
    label "okazja"
  ]
  node [
    id 190
    label "party"
  ]
  node [
    id 191
    label "czasoumilacz"
  ]
  node [
    id 192
    label "odpoczynek"
  ]
  node [
    id 193
    label "spos&#243;b"
  ]
  node [
    id 194
    label "kompozycja"
  ]
  node [
    id 195
    label "zacisk"
  ]
  node [
    id 196
    label "strategia"
  ]
  node [
    id 197
    label "zabieg"
  ]
  node [
    id 198
    label "ruch"
  ]
  node [
    id 199
    label "uchwyt"
  ]
  node [
    id 200
    label "uj&#281;cie"
  ]
  node [
    id 201
    label "zbi&#243;r"
  ]
  node [
    id 202
    label "karnet"
  ]
  node [
    id 203
    label "posuwisto&#347;&#263;"
  ]
  node [
    id 204
    label "utw&#243;r"
  ]
  node [
    id 205
    label "parkiet"
  ]
  node [
    id 206
    label "choreologia"
  ]
  node [
    id 207
    label "czynno&#347;&#263;"
  ]
  node [
    id 208
    label "krok_taneczny"
  ]
  node [
    id 209
    label "twardzioszek_przydro&#380;ny"
  ]
  node [
    id 210
    label "rado&#347;&#263;"
  ]
  node [
    id 211
    label "narz&#281;dzie"
  ]
  node [
    id 212
    label "Fidel_Castro"
  ]
  node [
    id 213
    label "Anders"
  ]
  node [
    id 214
    label "Ko&#347;ciuszko"
  ]
  node [
    id 215
    label "Tito"
  ]
  node [
    id 216
    label "Miko&#322;ajczyk"
  ]
  node [
    id 217
    label "lider"
  ]
  node [
    id 218
    label "Mao"
  ]
  node [
    id 219
    label "Sabataj_Cwi"
  ]
  node [
    id 220
    label "mistrz_ceremonii"
  ]
  node [
    id 221
    label "wesele"
  ]
  node [
    id 222
    label "jaki&#347;"
  ]
  node [
    id 223
    label "przyzwoity"
  ]
  node [
    id 224
    label "ciekawy"
  ]
  node [
    id 225
    label "jako&#347;"
  ]
  node [
    id 226
    label "jako_tako"
  ]
  node [
    id 227
    label "niez&#322;y"
  ]
  node [
    id 228
    label "dziwny"
  ]
  node [
    id 229
    label "charakterystyczny"
  ]
  node [
    id 230
    label "okre&#347;lony"
  ]
  node [
    id 231
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 232
    label "wiadomy"
  ]
  node [
    id 233
    label "inscription"
  ]
  node [
    id 234
    label "op&#322;ata"
  ]
  node [
    id 235
    label "akt"
  ]
  node [
    id 236
    label "tekst"
  ]
  node [
    id 237
    label "entrance"
  ]
  node [
    id 238
    label "ekscerpcja"
  ]
  node [
    id 239
    label "j&#281;zykowo"
  ]
  node [
    id 240
    label "wypowied&#378;"
  ]
  node [
    id 241
    label "redakcja"
  ]
  node [
    id 242
    label "wytw&#243;r"
  ]
  node [
    id 243
    label "pomini&#281;cie"
  ]
  node [
    id 244
    label "dzie&#322;o"
  ]
  node [
    id 245
    label "preparacja"
  ]
  node [
    id 246
    label "odmianka"
  ]
  node [
    id 247
    label "koniektura"
  ]
  node [
    id 248
    label "pisa&#263;"
  ]
  node [
    id 249
    label "obelga"
  ]
  node [
    id 250
    label "kwota"
  ]
  node [
    id 251
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 252
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 253
    label "podnieci&#263;"
  ]
  node [
    id 254
    label "scena"
  ]
  node [
    id 255
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 256
    label "numer"
  ]
  node [
    id 257
    label "po&#380;ycie"
  ]
  node [
    id 258
    label "poj&#281;cie"
  ]
  node [
    id 259
    label "podniecenie"
  ]
  node [
    id 260
    label "nago&#347;&#263;"
  ]
  node [
    id 261
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 262
    label "fascyku&#322;"
  ]
  node [
    id 263
    label "seks"
  ]
  node [
    id 264
    label "podniecanie"
  ]
  node [
    id 265
    label "imisja"
  ]
  node [
    id 266
    label "zwyczaj"
  ]
  node [
    id 267
    label "rozmna&#380;anie"
  ]
  node [
    id 268
    label "ruch_frykcyjny"
  ]
  node [
    id 269
    label "ontologia"
  ]
  node [
    id 270
    label "wydarzenie"
  ]
  node [
    id 271
    label "na_pieska"
  ]
  node [
    id 272
    label "pozycja_misjonarska"
  ]
  node [
    id 273
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 274
    label "fragment"
  ]
  node [
    id 275
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 276
    label "z&#322;&#261;czenie"
  ]
  node [
    id 277
    label "gra_wst&#281;pna"
  ]
  node [
    id 278
    label "erotyka"
  ]
  node [
    id 279
    label "urzeczywistnienie"
  ]
  node [
    id 280
    label "baraszki"
  ]
  node [
    id 281
    label "certificate"
  ]
  node [
    id 282
    label "po&#380;&#261;danie"
  ]
  node [
    id 283
    label "wzw&#243;d"
  ]
  node [
    id 284
    label "funkcja"
  ]
  node [
    id 285
    label "act"
  ]
  node [
    id 286
    label "dokument"
  ]
  node [
    id 287
    label "arystotelizm"
  ]
  node [
    id 288
    label "podnieca&#263;"
  ]
  node [
    id 289
    label "activity"
  ]
  node [
    id 290
    label "bezproblemowy"
  ]
  node [
    id 291
    label "zapanowa&#263;"
  ]
  node [
    id 292
    label "develop"
  ]
  node [
    id 293
    label "schorzenie"
  ]
  node [
    id 294
    label "obskoczy&#263;"
  ]
  node [
    id 295
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 296
    label "catch"
  ]
  node [
    id 297
    label "zrobi&#263;"
  ]
  node [
    id 298
    label "get"
  ]
  node [
    id 299
    label "zwiastun"
  ]
  node [
    id 300
    label "doczeka&#263;"
  ]
  node [
    id 301
    label "kupi&#263;"
  ]
  node [
    id 302
    label "wysta&#263;"
  ]
  node [
    id 303
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 304
    label "wystarczy&#263;"
  ]
  node [
    id 305
    label "wzi&#261;&#263;"
  ]
  node [
    id 306
    label "naby&#263;"
  ]
  node [
    id 307
    label "nabawianie_si&#281;"
  ]
  node [
    id 308
    label "range"
  ]
  node [
    id 309
    label "uzyska&#263;"
  ]
  node [
    id 310
    label "suffice"
  ]
  node [
    id 311
    label "spowodowa&#263;"
  ]
  node [
    id 312
    label "zaspokoi&#263;"
  ]
  node [
    id 313
    label "odziedziczy&#263;"
  ]
  node [
    id 314
    label "ruszy&#263;"
  ]
  node [
    id 315
    label "take"
  ]
  node [
    id 316
    label "zaatakowa&#263;"
  ]
  node [
    id 317
    label "skorzysta&#263;"
  ]
  node [
    id 318
    label "uciec"
  ]
  node [
    id 319
    label "receive"
  ]
  node [
    id 320
    label "nakaza&#263;"
  ]
  node [
    id 321
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 322
    label "bra&#263;"
  ]
  node [
    id 323
    label "u&#380;y&#263;"
  ]
  node [
    id 324
    label "wyrucha&#263;"
  ]
  node [
    id 325
    label "World_Health_Organization"
  ]
  node [
    id 326
    label "wyciupcia&#263;"
  ]
  node [
    id 327
    label "wygra&#263;"
  ]
  node [
    id 328
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 329
    label "withdraw"
  ]
  node [
    id 330
    label "wzi&#281;cie"
  ]
  node [
    id 331
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 332
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 333
    label "poczyta&#263;"
  ]
  node [
    id 334
    label "obj&#261;&#263;"
  ]
  node [
    id 335
    label "seize"
  ]
  node [
    id 336
    label "aim"
  ]
  node [
    id 337
    label "chwyci&#263;"
  ]
  node [
    id 338
    label "przyj&#261;&#263;"
  ]
  node [
    id 339
    label "pokona&#263;"
  ]
  node [
    id 340
    label "uda&#263;_si&#281;"
  ]
  node [
    id 341
    label "zacz&#261;&#263;"
  ]
  node [
    id 342
    label "otrzyma&#263;"
  ]
  node [
    id 343
    label "wej&#347;&#263;"
  ]
  node [
    id 344
    label "poruszy&#263;"
  ]
  node [
    id 345
    label "poradzi&#263;_sobie"
  ]
  node [
    id 346
    label "osaczy&#263;"
  ]
  node [
    id 347
    label "okra&#347;&#263;"
  ]
  node [
    id 348
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 349
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 350
    label "obiec"
  ]
  node [
    id 351
    label "powstrzyma&#263;"
  ]
  node [
    id 352
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 353
    label "manipulate"
  ]
  node [
    id 354
    label "rule"
  ]
  node [
    id 355
    label "cope"
  ]
  node [
    id 356
    label "post&#261;pi&#263;"
  ]
  node [
    id 357
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 358
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 359
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 360
    label "zorganizowa&#263;"
  ]
  node [
    id 361
    label "appoint"
  ]
  node [
    id 362
    label "wystylizowa&#263;"
  ]
  node [
    id 363
    label "cause"
  ]
  node [
    id 364
    label "przerobi&#263;"
  ]
  node [
    id 365
    label "nabra&#263;"
  ]
  node [
    id 366
    label "make"
  ]
  node [
    id 367
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 368
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 369
    label "wydali&#263;"
  ]
  node [
    id 370
    label "pozyska&#263;"
  ]
  node [
    id 371
    label "ustawi&#263;"
  ]
  node [
    id 372
    label "uwierzy&#263;"
  ]
  node [
    id 373
    label "zagra&#263;"
  ]
  node [
    id 374
    label "beget"
  ]
  node [
    id 375
    label "uzna&#263;"
  ]
  node [
    id 376
    label "draw"
  ]
  node [
    id 377
    label "pozosta&#263;"
  ]
  node [
    id 378
    label "poczeka&#263;"
  ]
  node [
    id 379
    label "wytrwa&#263;"
  ]
  node [
    id 380
    label "realize"
  ]
  node [
    id 381
    label "promocja"
  ]
  node [
    id 382
    label "wytworzy&#263;"
  ]
  node [
    id 383
    label "give_birth"
  ]
  node [
    id 384
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 385
    label "appreciation"
  ]
  node [
    id 386
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 387
    label "allude"
  ]
  node [
    id 388
    label "dotrze&#263;"
  ]
  node [
    id 389
    label "fall_upon"
  ]
  node [
    id 390
    label "przewidywanie"
  ]
  node [
    id 391
    label "oznaka"
  ]
  node [
    id 392
    label "harbinger"
  ]
  node [
    id 393
    label "obwie&#347;ciciel"
  ]
  node [
    id 394
    label "zapowied&#378;"
  ]
  node [
    id 395
    label "declaration"
  ]
  node [
    id 396
    label "reklama"
  ]
  node [
    id 397
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 398
    label "ognisko"
  ]
  node [
    id 399
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 400
    label "powalenie"
  ]
  node [
    id 401
    label "odezwanie_si&#281;"
  ]
  node [
    id 402
    label "atakowanie"
  ]
  node [
    id 403
    label "grupa_ryzyka"
  ]
  node [
    id 404
    label "przypadek"
  ]
  node [
    id 405
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 406
    label "inkubacja"
  ]
  node [
    id 407
    label "kryzys"
  ]
  node [
    id 408
    label "powali&#263;"
  ]
  node [
    id 409
    label "remisja"
  ]
  node [
    id 410
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 411
    label "zajmowa&#263;"
  ]
  node [
    id 412
    label "zaburzenie"
  ]
  node [
    id 413
    label "badanie_histopatologiczne"
  ]
  node [
    id 414
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 415
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 416
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 417
    label "odzywanie_si&#281;"
  ]
  node [
    id 418
    label "diagnoza"
  ]
  node [
    id 419
    label "atakowa&#263;"
  ]
  node [
    id 420
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 421
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 422
    label "zajmowanie"
  ]
  node [
    id 423
    label "gwiazda"
  ]
  node [
    id 424
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 425
    label "Arktur"
  ]
  node [
    id 426
    label "kszta&#322;t"
  ]
  node [
    id 427
    label "Gwiazda_Polarna"
  ]
  node [
    id 428
    label "agregatka"
  ]
  node [
    id 429
    label "gromada"
  ]
  node [
    id 430
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 431
    label "Nibiru"
  ]
  node [
    id 432
    label "konstelacja"
  ]
  node [
    id 433
    label "ornament"
  ]
  node [
    id 434
    label "delta_Scuti"
  ]
  node [
    id 435
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 436
    label "obiekt"
  ]
  node [
    id 437
    label "s&#322;awa"
  ]
  node [
    id 438
    label "promie&#324;"
  ]
  node [
    id 439
    label "star"
  ]
  node [
    id 440
    label "gwiazdosz"
  ]
  node [
    id 441
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 442
    label "asocjacja_gwiazd"
  ]
  node [
    id 443
    label "supergrupa"
  ]
  node [
    id 444
    label "wiedza"
  ]
  node [
    id 445
    label "miasteczko_rowerowe"
  ]
  node [
    id 446
    label "porada"
  ]
  node [
    id 447
    label "fotowoltaika"
  ]
  node [
    id 448
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 449
    label "przem&#243;wienie"
  ]
  node [
    id 450
    label "nauki_o_poznaniu"
  ]
  node [
    id 451
    label "nomotetyczny"
  ]
  node [
    id 452
    label "systematyka"
  ]
  node [
    id 453
    label "proces"
  ]
  node [
    id 454
    label "typologia"
  ]
  node [
    id 455
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 456
    label "kultura_duchowa"
  ]
  node [
    id 457
    label "&#322;awa_szkolna"
  ]
  node [
    id 458
    label "nauki_penalne"
  ]
  node [
    id 459
    label "dziedzina"
  ]
  node [
    id 460
    label "imagineskopia"
  ]
  node [
    id 461
    label "teoria_naukowa"
  ]
  node [
    id 462
    label "inwentyka"
  ]
  node [
    id 463
    label "metodologia"
  ]
  node [
    id 464
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 465
    label "nauki_o_Ziemi"
  ]
  node [
    id 466
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 467
    label "sfera"
  ]
  node [
    id 468
    label "zakres"
  ]
  node [
    id 469
    label "bezdro&#380;e"
  ]
  node [
    id 470
    label "poddzia&#322;"
  ]
  node [
    id 471
    label "kognicja"
  ]
  node [
    id 472
    label "przebieg"
  ]
  node [
    id 473
    label "rozprawa"
  ]
  node [
    id 474
    label "legislacyjnie"
  ]
  node [
    id 475
    label "przes&#322;anka"
  ]
  node [
    id 476
    label "nast&#281;pstwo"
  ]
  node [
    id 477
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 478
    label "zrozumienie"
  ]
  node [
    id 479
    label "obronienie"
  ]
  node [
    id 480
    label "wydanie"
  ]
  node [
    id 481
    label "wyg&#322;oszenie"
  ]
  node [
    id 482
    label "oddzia&#322;anie"
  ]
  node [
    id 483
    label "address"
  ]
  node [
    id 484
    label "wydobycie"
  ]
  node [
    id 485
    label "wyst&#261;pienie"
  ]
  node [
    id 486
    label "talk"
  ]
  node [
    id 487
    label "odzyskanie"
  ]
  node [
    id 488
    label "sermon"
  ]
  node [
    id 489
    label "cognition"
  ]
  node [
    id 490
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 491
    label "intelekt"
  ]
  node [
    id 492
    label "pozwolenie"
  ]
  node [
    id 493
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 494
    label "zaawansowanie"
  ]
  node [
    id 495
    label "wykszta&#322;cenie"
  ]
  node [
    id 496
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 497
    label "wskaz&#243;wka"
  ]
  node [
    id 498
    label "technika"
  ]
  node [
    id 499
    label "typology"
  ]
  node [
    id 500
    label "podzia&#322;"
  ]
  node [
    id 501
    label "kwantyfikacja"
  ]
  node [
    id 502
    label "aparat_krytyczny"
  ]
  node [
    id 503
    label "funkcjonalizm"
  ]
  node [
    id 504
    label "taksonomia"
  ]
  node [
    id 505
    label "biosystematyka"
  ]
  node [
    id 506
    label "biologia"
  ]
  node [
    id 507
    label "kohorta"
  ]
  node [
    id 508
    label "kladystyka"
  ]
  node [
    id 509
    label "wyobra&#378;nia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 445
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 447
  ]
  edge [
    source 14
    target 448
  ]
  edge [
    source 14
    target 449
  ]
  edge [
    source 14
    target 450
  ]
  edge [
    source 14
    target 451
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 453
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 455
  ]
  edge [
    source 14
    target 456
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 458
  ]
  edge [
    source 14
    target 459
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 284
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 270
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 240
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 229
  ]
]
