graph [
  node [
    id 0
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 1
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 2
    label "wyja&#347;ni&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "dzieje"
    origin "text"
  ]
  node [
    id 5
    label "programista"
    origin "text"
  ]
  node [
    id 6
    label "wykop"
    origin "text"
  ]
  node [
    id 7
    label "cz&#322;owiek"
  ]
  node [
    id 8
    label "posta&#263;"
  ]
  node [
    id 9
    label "osoba"
  ]
  node [
    id 10
    label "znaczenie"
  ]
  node [
    id 11
    label "go&#347;&#263;"
  ]
  node [
    id 12
    label "ludzko&#347;&#263;"
  ]
  node [
    id 13
    label "asymilowanie"
  ]
  node [
    id 14
    label "wapniak"
  ]
  node [
    id 15
    label "asymilowa&#263;"
  ]
  node [
    id 16
    label "os&#322;abia&#263;"
  ]
  node [
    id 17
    label "hominid"
  ]
  node [
    id 18
    label "podw&#322;adny"
  ]
  node [
    id 19
    label "os&#322;abianie"
  ]
  node [
    id 20
    label "g&#322;owa"
  ]
  node [
    id 21
    label "figura"
  ]
  node [
    id 22
    label "portrecista"
  ]
  node [
    id 23
    label "dwun&#243;g"
  ]
  node [
    id 24
    label "profanum"
  ]
  node [
    id 25
    label "mikrokosmos"
  ]
  node [
    id 26
    label "nasada"
  ]
  node [
    id 27
    label "duch"
  ]
  node [
    id 28
    label "antropochoria"
  ]
  node [
    id 29
    label "wz&#243;r"
  ]
  node [
    id 30
    label "senior"
  ]
  node [
    id 31
    label "oddzia&#322;ywanie"
  ]
  node [
    id 32
    label "Adam"
  ]
  node [
    id 33
    label "homo_sapiens"
  ]
  node [
    id 34
    label "polifag"
  ]
  node [
    id 35
    label "odwiedziny"
  ]
  node [
    id 36
    label "klient"
  ]
  node [
    id 37
    label "restauracja"
  ]
  node [
    id 38
    label "przybysz"
  ]
  node [
    id 39
    label "uczestnik"
  ]
  node [
    id 40
    label "hotel"
  ]
  node [
    id 41
    label "bratek"
  ]
  node [
    id 42
    label "sztuka"
  ]
  node [
    id 43
    label "facet"
  ]
  node [
    id 44
    label "Chocho&#322;"
  ]
  node [
    id 45
    label "Herkules_Poirot"
  ]
  node [
    id 46
    label "Edyp"
  ]
  node [
    id 47
    label "parali&#380;owa&#263;"
  ]
  node [
    id 48
    label "Harry_Potter"
  ]
  node [
    id 49
    label "Casanova"
  ]
  node [
    id 50
    label "Gargantua"
  ]
  node [
    id 51
    label "Zgredek"
  ]
  node [
    id 52
    label "Winnetou"
  ]
  node [
    id 53
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 54
    label "Dulcynea"
  ]
  node [
    id 55
    label "kategoria_gramatyczna"
  ]
  node [
    id 56
    label "person"
  ]
  node [
    id 57
    label "Sherlock_Holmes"
  ]
  node [
    id 58
    label "Quasimodo"
  ]
  node [
    id 59
    label "Plastu&#347;"
  ]
  node [
    id 60
    label "Faust"
  ]
  node [
    id 61
    label "Wallenrod"
  ]
  node [
    id 62
    label "Dwukwiat"
  ]
  node [
    id 63
    label "koniugacja"
  ]
  node [
    id 64
    label "Don_Juan"
  ]
  node [
    id 65
    label "Don_Kiszot"
  ]
  node [
    id 66
    label "Hamlet"
  ]
  node [
    id 67
    label "Werter"
  ]
  node [
    id 68
    label "istota"
  ]
  node [
    id 69
    label "Szwejk"
  ]
  node [
    id 70
    label "odk&#322;adanie"
  ]
  node [
    id 71
    label "condition"
  ]
  node [
    id 72
    label "liczenie"
  ]
  node [
    id 73
    label "stawianie"
  ]
  node [
    id 74
    label "bycie"
  ]
  node [
    id 75
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 76
    label "assay"
  ]
  node [
    id 77
    label "wskazywanie"
  ]
  node [
    id 78
    label "wyraz"
  ]
  node [
    id 79
    label "gravity"
  ]
  node [
    id 80
    label "weight"
  ]
  node [
    id 81
    label "command"
  ]
  node [
    id 82
    label "odgrywanie_roli"
  ]
  node [
    id 83
    label "informacja"
  ]
  node [
    id 84
    label "cecha"
  ]
  node [
    id 85
    label "okre&#347;lanie"
  ]
  node [
    id 86
    label "wyra&#380;enie"
  ]
  node [
    id 87
    label "charakterystyka"
  ]
  node [
    id 88
    label "zaistnie&#263;"
  ]
  node [
    id 89
    label "Osjan"
  ]
  node [
    id 90
    label "wygl&#261;d"
  ]
  node [
    id 91
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 92
    label "osobowo&#347;&#263;"
  ]
  node [
    id 93
    label "wytw&#243;r"
  ]
  node [
    id 94
    label "trim"
  ]
  node [
    id 95
    label "poby&#263;"
  ]
  node [
    id 96
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 97
    label "Aspazja"
  ]
  node [
    id 98
    label "punkt_widzenia"
  ]
  node [
    id 99
    label "kompleksja"
  ]
  node [
    id 100
    label "wytrzyma&#263;"
  ]
  node [
    id 101
    label "budowa"
  ]
  node [
    id 102
    label "formacja"
  ]
  node [
    id 103
    label "pozosta&#263;"
  ]
  node [
    id 104
    label "point"
  ]
  node [
    id 105
    label "przedstawienie"
  ]
  node [
    id 106
    label "clear"
  ]
  node [
    id 107
    label "przedstawi&#263;"
  ]
  node [
    id 108
    label "explain"
  ]
  node [
    id 109
    label "poja&#347;ni&#263;"
  ]
  node [
    id 110
    label "ukaza&#263;"
  ]
  node [
    id 111
    label "pokaza&#263;"
  ]
  node [
    id 112
    label "poda&#263;"
  ]
  node [
    id 113
    label "zapozna&#263;"
  ]
  node [
    id 114
    label "express"
  ]
  node [
    id 115
    label "represent"
  ]
  node [
    id 116
    label "zaproponowa&#263;"
  ]
  node [
    id 117
    label "zademonstrowa&#263;"
  ]
  node [
    id 118
    label "typify"
  ]
  node [
    id 119
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 120
    label "opisa&#263;"
  ]
  node [
    id 121
    label "rozja&#347;ni&#263;"
  ]
  node [
    id 122
    label "epoka"
  ]
  node [
    id 123
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 124
    label "koleje_losu"
  ]
  node [
    id 125
    label "&#380;ycie"
  ]
  node [
    id 126
    label "czas"
  ]
  node [
    id 127
    label "aalen"
  ]
  node [
    id 128
    label "jura_wczesna"
  ]
  node [
    id 129
    label "holocen"
  ]
  node [
    id 130
    label "pliocen"
  ]
  node [
    id 131
    label "plejstocen"
  ]
  node [
    id 132
    label "paleocen"
  ]
  node [
    id 133
    label "bajos"
  ]
  node [
    id 134
    label "kelowej"
  ]
  node [
    id 135
    label "eocen"
  ]
  node [
    id 136
    label "jednostka_geologiczna"
  ]
  node [
    id 137
    label "okres"
  ]
  node [
    id 138
    label "schy&#322;ek"
  ]
  node [
    id 139
    label "miocen"
  ]
  node [
    id 140
    label "&#347;rodkowy_trias"
  ]
  node [
    id 141
    label "term"
  ]
  node [
    id 142
    label "Zeitgeist"
  ]
  node [
    id 143
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 144
    label "wczesny_trias"
  ]
  node [
    id 145
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 146
    label "jura_&#347;rodkowa"
  ]
  node [
    id 147
    label "oligocen"
  ]
  node [
    id 148
    label "informatyk"
  ]
  node [
    id 149
    label "nauczyciel"
  ]
  node [
    id 150
    label "specjalista"
  ]
  node [
    id 151
    label "zrzutowy"
  ]
  node [
    id 152
    label "odk&#322;ad"
  ]
  node [
    id 153
    label "chody"
  ]
  node [
    id 154
    label "szaniec"
  ]
  node [
    id 155
    label "wyrobisko"
  ]
  node [
    id 156
    label "kopniak"
  ]
  node [
    id 157
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 158
    label "odwa&#322;"
  ]
  node [
    id 159
    label "grodzisko"
  ]
  node [
    id 160
    label "cios"
  ]
  node [
    id 161
    label "kick"
  ]
  node [
    id 162
    label "kopni&#281;cie"
  ]
  node [
    id 163
    label "&#347;rodkowiec"
  ]
  node [
    id 164
    label "podsadzka"
  ]
  node [
    id 165
    label "obudowa"
  ]
  node [
    id 166
    label "sp&#261;g"
  ]
  node [
    id 167
    label "strop"
  ]
  node [
    id 168
    label "rabowarka"
  ]
  node [
    id 169
    label "opinka"
  ]
  node [
    id 170
    label "stojak_cierny"
  ]
  node [
    id 171
    label "kopalnia"
  ]
  node [
    id 172
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 173
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 174
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 175
    label "immersion"
  ]
  node [
    id 176
    label "umieszczenie"
  ]
  node [
    id 177
    label "las"
  ]
  node [
    id 178
    label "nora"
  ]
  node [
    id 179
    label "pies_my&#347;liwski"
  ]
  node [
    id 180
    label "miejsce"
  ]
  node [
    id 181
    label "trasa"
  ]
  node [
    id 182
    label "doj&#347;cie"
  ]
  node [
    id 183
    label "zesp&#243;&#322;"
  ]
  node [
    id 184
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 185
    label "horodyszcze"
  ]
  node [
    id 186
    label "Wyszogr&#243;d"
  ]
  node [
    id 187
    label "usypisko"
  ]
  node [
    id 188
    label "r&#243;w"
  ]
  node [
    id 189
    label "wa&#322;"
  ]
  node [
    id 190
    label "redoubt"
  ]
  node [
    id 191
    label "fortyfikacja"
  ]
  node [
    id 192
    label "mechanika"
  ]
  node [
    id 193
    label "struktura"
  ]
  node [
    id 194
    label "miejsce_pracy"
  ]
  node [
    id 195
    label "organ"
  ]
  node [
    id 196
    label "kreacja"
  ]
  node [
    id 197
    label "zwierz&#281;"
  ]
  node [
    id 198
    label "posesja"
  ]
  node [
    id 199
    label "konstrukcja"
  ]
  node [
    id 200
    label "wjazd"
  ]
  node [
    id 201
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 202
    label "praca"
  ]
  node [
    id 203
    label "constitution"
  ]
  node [
    id 204
    label "gleba"
  ]
  node [
    id 205
    label "p&#281;d"
  ]
  node [
    id 206
    label "zbi&#243;r"
  ]
  node [
    id 207
    label "ablegier"
  ]
  node [
    id 208
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 209
    label "layer"
  ]
  node [
    id 210
    label "r&#243;j"
  ]
  node [
    id 211
    label "mrowisko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
]
