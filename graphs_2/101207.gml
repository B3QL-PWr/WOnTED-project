graph [
  node [
    id 0
    label "feliks"
    origin "text"
  ]
  node [
    id 1
    label "krzemi&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "Feliksa"
  ]
  node [
    id 3
    label "Krzemi&#324;ski"
  ]
  node [
    id 4
    label "komisja"
  ]
  node [
    id 5
    label "komunikacja"
  ]
  node [
    id 6
    label "i"
  ]
  node [
    id 7
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 8
    label "obrona"
  ]
  node [
    id 9
    label "narodowy"
  ]
  node [
    id 10
    label "plan"
  ]
  node [
    id 11
    label "gospodarczy"
  ]
  node [
    id 12
    label "bud&#380;et"
  ]
  node [
    id 13
    label "finanse"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
]
