graph [
  node [
    id 0
    label "airbus"
    origin "text"
  ]
  node [
    id 1
    label "helicopters"
    origin "text"
  ]
  node [
    id 2
    label "wycofywa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "przetarg"
    origin "text"
  ]
  node [
    id 5
    label "&#347;mig&#322;owiec"
    origin "text"
  ]
  node [
    id 6
    label "dla"
    origin "text"
  ]
  node [
    id 7
    label "marynarka"
    origin "text"
  ]
  node [
    id 8
    label "wojenny"
    origin "text"
  ]
  node [
    id 9
    label "samolot_pasa&#380;erski"
  ]
  node [
    id 10
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 11
    label "seclude"
  ]
  node [
    id 12
    label "zrywa&#263;"
  ]
  node [
    id 13
    label "retract"
  ]
  node [
    id 14
    label "sale"
  ]
  node [
    id 15
    label "licytacja"
  ]
  node [
    id 16
    label "konkurs"
  ]
  node [
    id 17
    label "przybitka"
  ]
  node [
    id 18
    label "sprzeda&#380;"
  ]
  node [
    id 19
    label "sp&#243;r"
  ]
  node [
    id 20
    label "auction"
  ]
  node [
    id 21
    label "konflikt"
  ]
  node [
    id 22
    label "clash"
  ]
  node [
    id 23
    label "wsp&#243;r"
  ]
  node [
    id 24
    label "obrona"
  ]
  node [
    id 25
    label "przeniesienie_praw"
  ]
  node [
    id 26
    label "przeda&#380;"
  ]
  node [
    id 27
    label "transakcja"
  ]
  node [
    id 28
    label "sprzedaj&#261;cy"
  ]
  node [
    id 29
    label "rabat"
  ]
  node [
    id 30
    label "casting"
  ]
  node [
    id 31
    label "nab&#243;r"
  ]
  node [
    id 32
    label "Eurowizja"
  ]
  node [
    id 33
    label "eliminacje"
  ]
  node [
    id 34
    label "impreza"
  ]
  node [
    id 35
    label "emulation"
  ]
  node [
    id 36
    label "Interwizja"
  ]
  node [
    id 37
    label "pokrywka"
  ]
  node [
    id 38
    label "wype&#322;nienie"
  ]
  node [
    id 39
    label "zatyczka"
  ]
  node [
    id 40
    label "batch"
  ]
  node [
    id 41
    label "rozdanie"
  ]
  node [
    id 42
    label "faza"
  ]
  node [
    id 43
    label "pas"
  ]
  node [
    id 44
    label "bryd&#380;"
  ]
  node [
    id 45
    label "tysi&#261;c"
  ]
  node [
    id 46
    label "skat"
  ]
  node [
    id 47
    label "gra_w_karty"
  ]
  node [
    id 48
    label "wirop&#322;at"
  ]
  node [
    id 49
    label "&#347;mig&#322;o"
  ]
  node [
    id 50
    label "p&#322;at_no&#347;ny"
  ]
  node [
    id 51
    label "aerodyna"
  ]
  node [
    id 52
    label "&#347;mig&#322;y"
  ]
  node [
    id 53
    label "d&#322;ugo"
  ]
  node [
    id 54
    label "zgrabnie"
  ]
  node [
    id 55
    label "bystro"
  ]
  node [
    id 56
    label "si&#322;ownia_wiatrowa"
  ]
  node [
    id 57
    label "bombowiec"
  ]
  node [
    id 58
    label "&#322;opatka"
  ]
  node [
    id 59
    label "statek_powietrzny"
  ]
  node [
    id 60
    label "aerosanie"
  ]
  node [
    id 61
    label "mieszad&#322;o"
  ]
  node [
    id 62
    label "wiatrak"
  ]
  node [
    id 63
    label "urz&#261;dzenie"
  ]
  node [
    id 64
    label "g&#243;ra"
  ]
  node [
    id 65
    label "United_States_Navy"
  ]
  node [
    id 66
    label "wojsko"
  ]
  node [
    id 67
    label "zbi&#243;r"
  ]
  node [
    id 68
    label "klapa"
  ]
  node [
    id 69
    label "r&#281;kaw"
  ]
  node [
    id 70
    label "guzik"
  ]
  node [
    id 71
    label "zrejterowanie"
  ]
  node [
    id 72
    label "zmobilizowa&#263;"
  ]
  node [
    id 73
    label "przedmiot"
  ]
  node [
    id 74
    label "dezerter"
  ]
  node [
    id 75
    label "oddzia&#322;_karny"
  ]
  node [
    id 76
    label "rezerwa"
  ]
  node [
    id 77
    label "tabor"
  ]
  node [
    id 78
    label "wermacht"
  ]
  node [
    id 79
    label "cofni&#281;cie"
  ]
  node [
    id 80
    label "potencja"
  ]
  node [
    id 81
    label "fala"
  ]
  node [
    id 82
    label "struktura"
  ]
  node [
    id 83
    label "szko&#322;a"
  ]
  node [
    id 84
    label "korpus"
  ]
  node [
    id 85
    label "soldateska"
  ]
  node [
    id 86
    label "ods&#322;ugiwanie"
  ]
  node [
    id 87
    label "werbowanie_si&#281;"
  ]
  node [
    id 88
    label "zdemobilizowanie"
  ]
  node [
    id 89
    label "oddzia&#322;"
  ]
  node [
    id 90
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 91
    label "s&#322;u&#380;ba"
  ]
  node [
    id 92
    label "or&#281;&#380;"
  ]
  node [
    id 93
    label "Legia_Cudzoziemska"
  ]
  node [
    id 94
    label "Armia_Czerwona"
  ]
  node [
    id 95
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 96
    label "rejterowanie"
  ]
  node [
    id 97
    label "Czerwona_Gwardia"
  ]
  node [
    id 98
    label "si&#322;a"
  ]
  node [
    id 99
    label "zrejterowa&#263;"
  ]
  node [
    id 100
    label "sztabslekarz"
  ]
  node [
    id 101
    label "zmobilizowanie"
  ]
  node [
    id 102
    label "wojo"
  ]
  node [
    id 103
    label "pospolite_ruszenie"
  ]
  node [
    id 104
    label "Eurokorpus"
  ]
  node [
    id 105
    label "mobilizowanie"
  ]
  node [
    id 106
    label "rejterowa&#263;"
  ]
  node [
    id 107
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 108
    label "mobilizowa&#263;"
  ]
  node [
    id 109
    label "Armia_Krajowa"
  ]
  node [
    id 110
    label "dryl"
  ]
  node [
    id 111
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 112
    label "petarda"
  ]
  node [
    id 113
    label "pozycja"
  ]
  node [
    id 114
    label "zdemobilizowa&#263;"
  ]
  node [
    id 115
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 116
    label "egzemplarz"
  ]
  node [
    id 117
    label "series"
  ]
  node [
    id 118
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 119
    label "uprawianie"
  ]
  node [
    id 120
    label "praca_rolnicza"
  ]
  node [
    id 121
    label "collection"
  ]
  node [
    id 122
    label "dane"
  ]
  node [
    id 123
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 124
    label "pakiet_klimatyczny"
  ]
  node [
    id 125
    label "poj&#281;cie"
  ]
  node [
    id 126
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 127
    label "sum"
  ]
  node [
    id 128
    label "gathering"
  ]
  node [
    id 129
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 130
    label "album"
  ]
  node [
    id 131
    label "przelezienie"
  ]
  node [
    id 132
    label "&#347;piew"
  ]
  node [
    id 133
    label "Synaj"
  ]
  node [
    id 134
    label "Kreml"
  ]
  node [
    id 135
    label "d&#378;wi&#281;k"
  ]
  node [
    id 136
    label "kierunek"
  ]
  node [
    id 137
    label "wysoki"
  ]
  node [
    id 138
    label "element"
  ]
  node [
    id 139
    label "wzniesienie"
  ]
  node [
    id 140
    label "grupa"
  ]
  node [
    id 141
    label "pi&#281;tro"
  ]
  node [
    id 142
    label "Ropa"
  ]
  node [
    id 143
    label "kupa"
  ]
  node [
    id 144
    label "przele&#378;&#263;"
  ]
  node [
    id 145
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 146
    label "karczek"
  ]
  node [
    id 147
    label "rami&#261;czko"
  ]
  node [
    id 148
    label "Jaworze"
  ]
  node [
    id 149
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 150
    label "zaw&#243;r"
  ]
  node [
    id 151
    label "visitation"
  ]
  node [
    id 152
    label "pokrywa"
  ]
  node [
    id 153
    label "wy&#322;az"
  ]
  node [
    id 154
    label "wydarzenie"
  ]
  node [
    id 155
    label "instrument_d&#281;ty"
  ]
  node [
    id 156
    label "powierzchnia_sterowa"
  ]
  node [
    id 157
    label "skrzyd&#322;o"
  ]
  node [
    id 158
    label "butonierka"
  ]
  node [
    id 159
    label "tent-fly"
  ]
  node [
    id 160
    label "&#380;akiet"
  ]
  node [
    id 161
    label "zapi&#281;cie"
  ]
  node [
    id 162
    label "filobutonista"
  ]
  node [
    id 163
    label "przycisk"
  ]
  node [
    id 164
    label "zero"
  ]
  node [
    id 165
    label "filobutonistyka"
  ]
  node [
    id 166
    label "knob"
  ]
  node [
    id 167
    label "guzikarz"
  ]
  node [
    id 168
    label "pasmanteria"
  ]
  node [
    id 169
    label "r&#243;g"
  ]
  node [
    id 170
    label "przej&#347;cie"
  ]
  node [
    id 171
    label "narz&#281;dzie"
  ]
  node [
    id 172
    label "pomost"
  ]
  node [
    id 173
    label "zar&#281;kawek"
  ]
  node [
    id 174
    label "wiatrowskaz"
  ]
  node [
    id 175
    label "tunel"
  ]
  node [
    id 176
    label "bojowo"
  ]
  node [
    id 177
    label "typowy"
  ]
  node [
    id 178
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 179
    label "zwyczajny"
  ]
  node [
    id 180
    label "typowo"
  ]
  node [
    id 181
    label "cz&#281;sty"
  ]
  node [
    id 182
    label "zwyk&#322;y"
  ]
  node [
    id 183
    label "&#347;mia&#322;o"
  ]
  node [
    id 184
    label "wojennie"
  ]
  node [
    id 185
    label "walecznie"
  ]
  node [
    id 186
    label "pewnie"
  ]
  node [
    id 187
    label "zadziornie"
  ]
  node [
    id 188
    label "bojowy"
  ]
  node [
    id 189
    label "Helicopters"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
]
