graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "taka"
    origin "text"
  ]
  node [
    id 2
    label "lista"
    origin "text"
  ]
  node [
    id 3
    label "wa&#380;ki"
    origin "text"
  ]
  node [
    id 4
    label "temat"
    origin "text"
  ]
  node [
    id 5
    label "poruszenie"
    origin "text"
  ]
  node [
    id 6
    label "ale"
    origin "text"
  ]
  node [
    id 7
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 8
    label "przeczyta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "co&#347;"
    origin "text"
  ]
  node [
    id 11
    label "tak"
    origin "text"
  ]
  node [
    id 12
    label "nieistotny"
    origin "text"
  ]
  node [
    id 13
    label "por&#243;wnanie"
    origin "text"
  ]
  node [
    id 14
    label "chrystus"
    origin "text"
  ]
  node [
    id 15
    label "kr&#243;l"
    origin "text"
  ]
  node [
    id 16
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 17
    label "interesuj&#261;cy"
    origin "text"
  ]
  node [
    id 18
    label "przeciwie&#324;stwo"
    origin "text"
  ]
  node [
    id 19
    label "daleki"
    origin "text"
  ]
  node [
    id 20
    label "los"
    origin "text"
  ]
  node [
    id 21
    label "zawodowy"
    origin "text"
  ]
  node [
    id 22
    label "karier"
    origin "text"
  ]
  node [
    id 23
    label "atrakcyjny"
    origin "text"
  ]
  node [
    id 24
    label "kazimierz"
    origin "text"
  ]
  node [
    id 25
    label "musza"
    origin "text"
  ]
  node [
    id 26
    label "si&#281;"
    origin "text"
  ]
  node [
    id 27
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 28
    label "tym"
    origin "text"
  ]
  node [
    id 29
    label "podzieli&#263;"
    origin "text"
  ]
  node [
    id 30
    label "przodkini"
  ]
  node [
    id 31
    label "matka_zast&#281;pcza"
  ]
  node [
    id 32
    label "matczysko"
  ]
  node [
    id 33
    label "rodzice"
  ]
  node [
    id 34
    label "stara"
  ]
  node [
    id 35
    label "macierz"
  ]
  node [
    id 36
    label "rodzic"
  ]
  node [
    id 37
    label "Matka_Boska"
  ]
  node [
    id 38
    label "macocha"
  ]
  node [
    id 39
    label "starzy"
  ]
  node [
    id 40
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 41
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 42
    label "pokolenie"
  ]
  node [
    id 43
    label "wapniaki"
  ]
  node [
    id 44
    label "krewna"
  ]
  node [
    id 45
    label "opiekun"
  ]
  node [
    id 46
    label "wapniak"
  ]
  node [
    id 47
    label "rodzic_chrzestny"
  ]
  node [
    id 48
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 49
    label "matka"
  ]
  node [
    id 50
    label "&#380;ona"
  ]
  node [
    id 51
    label "kobieta"
  ]
  node [
    id 52
    label "partnerka"
  ]
  node [
    id 53
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 54
    label "matuszka"
  ]
  node [
    id 55
    label "parametryzacja"
  ]
  node [
    id 56
    label "poj&#281;cie"
  ]
  node [
    id 57
    label "mod"
  ]
  node [
    id 58
    label "patriota"
  ]
  node [
    id 59
    label "m&#281;&#380;atka"
  ]
  node [
    id 60
    label "Bangladesz"
  ]
  node [
    id 61
    label "jednostka_monetarna"
  ]
  node [
    id 62
    label "Bengal"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "catalog"
  ]
  node [
    id 65
    label "pozycja"
  ]
  node [
    id 66
    label "tekst"
  ]
  node [
    id 67
    label "sumariusz"
  ]
  node [
    id 68
    label "book"
  ]
  node [
    id 69
    label "stock"
  ]
  node [
    id 70
    label "figurowa&#263;"
  ]
  node [
    id 71
    label "wyliczanka"
  ]
  node [
    id 72
    label "ekscerpcja"
  ]
  node [
    id 73
    label "j&#281;zykowo"
  ]
  node [
    id 74
    label "wypowied&#378;"
  ]
  node [
    id 75
    label "redakcja"
  ]
  node [
    id 76
    label "wytw&#243;r"
  ]
  node [
    id 77
    label "pomini&#281;cie"
  ]
  node [
    id 78
    label "dzie&#322;o"
  ]
  node [
    id 79
    label "preparacja"
  ]
  node [
    id 80
    label "odmianka"
  ]
  node [
    id 81
    label "opu&#347;ci&#263;"
  ]
  node [
    id 82
    label "koniektura"
  ]
  node [
    id 83
    label "pisa&#263;"
  ]
  node [
    id 84
    label "obelga"
  ]
  node [
    id 85
    label "egzemplarz"
  ]
  node [
    id 86
    label "series"
  ]
  node [
    id 87
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 88
    label "uprawianie"
  ]
  node [
    id 89
    label "praca_rolnicza"
  ]
  node [
    id 90
    label "collection"
  ]
  node [
    id 91
    label "dane"
  ]
  node [
    id 92
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 93
    label "pakiet_klimatyczny"
  ]
  node [
    id 94
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 95
    label "sum"
  ]
  node [
    id 96
    label "gathering"
  ]
  node [
    id 97
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 98
    label "album"
  ]
  node [
    id 99
    label "po&#322;o&#380;enie"
  ]
  node [
    id 100
    label "debit"
  ]
  node [
    id 101
    label "druk"
  ]
  node [
    id 102
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 103
    label "szata_graficzna"
  ]
  node [
    id 104
    label "wydawa&#263;"
  ]
  node [
    id 105
    label "szermierka"
  ]
  node [
    id 106
    label "spis"
  ]
  node [
    id 107
    label "wyda&#263;"
  ]
  node [
    id 108
    label "ustawienie"
  ]
  node [
    id 109
    label "publikacja"
  ]
  node [
    id 110
    label "status"
  ]
  node [
    id 111
    label "miejsce"
  ]
  node [
    id 112
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 113
    label "adres"
  ]
  node [
    id 114
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 115
    label "rozmieszczenie"
  ]
  node [
    id 116
    label "sytuacja"
  ]
  node [
    id 117
    label "rz&#261;d"
  ]
  node [
    id 118
    label "redaktor"
  ]
  node [
    id 119
    label "awansowa&#263;"
  ]
  node [
    id 120
    label "wojsko"
  ]
  node [
    id 121
    label "bearing"
  ]
  node [
    id 122
    label "znaczenie"
  ]
  node [
    id 123
    label "awans"
  ]
  node [
    id 124
    label "awansowanie"
  ]
  node [
    id 125
    label "poster"
  ]
  node [
    id 126
    label "le&#380;e&#263;"
  ]
  node [
    id 127
    label "entliczek"
  ]
  node [
    id 128
    label "zabawa"
  ]
  node [
    id 129
    label "wiersz"
  ]
  node [
    id 130
    label "pentliczek"
  ]
  node [
    id 131
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 132
    label "owady_uskrzydlone"
  ]
  node [
    id 133
    label "wa&#380;ny"
  ]
  node [
    id 134
    label "wynios&#322;y"
  ]
  node [
    id 135
    label "dono&#347;ny"
  ]
  node [
    id 136
    label "silny"
  ]
  node [
    id 137
    label "wa&#380;nie"
  ]
  node [
    id 138
    label "istotnie"
  ]
  node [
    id 139
    label "znaczny"
  ]
  node [
    id 140
    label "eksponowany"
  ]
  node [
    id 141
    label "dobry"
  ]
  node [
    id 142
    label "sprawa"
  ]
  node [
    id 143
    label "wyraz_pochodny"
  ]
  node [
    id 144
    label "zboczenie"
  ]
  node [
    id 145
    label "om&#243;wienie"
  ]
  node [
    id 146
    label "cecha"
  ]
  node [
    id 147
    label "rzecz"
  ]
  node [
    id 148
    label "omawia&#263;"
  ]
  node [
    id 149
    label "fraza"
  ]
  node [
    id 150
    label "tre&#347;&#263;"
  ]
  node [
    id 151
    label "entity"
  ]
  node [
    id 152
    label "forum"
  ]
  node [
    id 153
    label "topik"
  ]
  node [
    id 154
    label "tematyka"
  ]
  node [
    id 155
    label "w&#261;tek"
  ]
  node [
    id 156
    label "zbaczanie"
  ]
  node [
    id 157
    label "forma"
  ]
  node [
    id 158
    label "om&#243;wi&#263;"
  ]
  node [
    id 159
    label "omawianie"
  ]
  node [
    id 160
    label "melodia"
  ]
  node [
    id 161
    label "otoczka"
  ]
  node [
    id 162
    label "istota"
  ]
  node [
    id 163
    label "zbacza&#263;"
  ]
  node [
    id 164
    label "zboczy&#263;"
  ]
  node [
    id 165
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 166
    label "wypowiedzenie"
  ]
  node [
    id 167
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 168
    label "zdanie"
  ]
  node [
    id 169
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 170
    label "motyw"
  ]
  node [
    id 171
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 172
    label "informacja"
  ]
  node [
    id 173
    label "zawarto&#347;&#263;"
  ]
  node [
    id 174
    label "kszta&#322;t"
  ]
  node [
    id 175
    label "jednostka_systematyczna"
  ]
  node [
    id 176
    label "poznanie"
  ]
  node [
    id 177
    label "leksem"
  ]
  node [
    id 178
    label "stan"
  ]
  node [
    id 179
    label "blaszka"
  ]
  node [
    id 180
    label "kantyzm"
  ]
  node [
    id 181
    label "zdolno&#347;&#263;"
  ]
  node [
    id 182
    label "do&#322;ek"
  ]
  node [
    id 183
    label "gwiazda"
  ]
  node [
    id 184
    label "formality"
  ]
  node [
    id 185
    label "struktura"
  ]
  node [
    id 186
    label "wygl&#261;d"
  ]
  node [
    id 187
    label "mode"
  ]
  node [
    id 188
    label "morfem"
  ]
  node [
    id 189
    label "rdze&#324;"
  ]
  node [
    id 190
    label "posta&#263;"
  ]
  node [
    id 191
    label "kielich"
  ]
  node [
    id 192
    label "ornamentyka"
  ]
  node [
    id 193
    label "pasmo"
  ]
  node [
    id 194
    label "zwyczaj"
  ]
  node [
    id 195
    label "punkt_widzenia"
  ]
  node [
    id 196
    label "g&#322;owa"
  ]
  node [
    id 197
    label "naczynie"
  ]
  node [
    id 198
    label "p&#322;at"
  ]
  node [
    id 199
    label "maszyna_drukarska"
  ]
  node [
    id 200
    label "obiekt"
  ]
  node [
    id 201
    label "style"
  ]
  node [
    id 202
    label "linearno&#347;&#263;"
  ]
  node [
    id 203
    label "wyra&#380;enie"
  ]
  node [
    id 204
    label "formacja"
  ]
  node [
    id 205
    label "spirala"
  ]
  node [
    id 206
    label "dyspozycja"
  ]
  node [
    id 207
    label "odmiana"
  ]
  node [
    id 208
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 209
    label "wz&#243;r"
  ]
  node [
    id 210
    label "October"
  ]
  node [
    id 211
    label "creation"
  ]
  node [
    id 212
    label "p&#281;tla"
  ]
  node [
    id 213
    label "arystotelizm"
  ]
  node [
    id 214
    label "szablon"
  ]
  node [
    id 215
    label "miniatura"
  ]
  node [
    id 216
    label "zanucenie"
  ]
  node [
    id 217
    label "nuta"
  ]
  node [
    id 218
    label "zakosztowa&#263;"
  ]
  node [
    id 219
    label "zajawka"
  ]
  node [
    id 220
    label "zanuci&#263;"
  ]
  node [
    id 221
    label "emocja"
  ]
  node [
    id 222
    label "oskoma"
  ]
  node [
    id 223
    label "melika"
  ]
  node [
    id 224
    label "nucenie"
  ]
  node [
    id 225
    label "nuci&#263;"
  ]
  node [
    id 226
    label "brzmienie"
  ]
  node [
    id 227
    label "zjawisko"
  ]
  node [
    id 228
    label "taste"
  ]
  node [
    id 229
    label "muzyka"
  ]
  node [
    id 230
    label "inclination"
  ]
  node [
    id 231
    label "charakterystyka"
  ]
  node [
    id 232
    label "m&#322;ot"
  ]
  node [
    id 233
    label "znak"
  ]
  node [
    id 234
    label "drzewo"
  ]
  node [
    id 235
    label "pr&#243;ba"
  ]
  node [
    id 236
    label "attribute"
  ]
  node [
    id 237
    label "marka"
  ]
  node [
    id 238
    label "matter"
  ]
  node [
    id 239
    label "splot"
  ]
  node [
    id 240
    label "ceg&#322;a"
  ]
  node [
    id 241
    label "socket"
  ]
  node [
    id 242
    label "fabu&#322;a"
  ]
  node [
    id 243
    label "mentalno&#347;&#263;"
  ]
  node [
    id 244
    label "superego"
  ]
  node [
    id 245
    label "psychika"
  ]
  node [
    id 246
    label "wn&#281;trze"
  ]
  node [
    id 247
    label "charakter"
  ]
  node [
    id 248
    label "okrywa"
  ]
  node [
    id 249
    label "kontekst"
  ]
  node [
    id 250
    label "object"
  ]
  node [
    id 251
    label "przedmiot"
  ]
  node [
    id 252
    label "wpadni&#281;cie"
  ]
  node [
    id 253
    label "mienie"
  ]
  node [
    id 254
    label "przyroda"
  ]
  node [
    id 255
    label "kultura"
  ]
  node [
    id 256
    label "wpa&#347;&#263;"
  ]
  node [
    id 257
    label "wpadanie"
  ]
  node [
    id 258
    label "wpada&#263;"
  ]
  node [
    id 259
    label "discussion"
  ]
  node [
    id 260
    label "rozpatrywanie"
  ]
  node [
    id 261
    label "dyskutowanie"
  ]
  node [
    id 262
    label "swerve"
  ]
  node [
    id 263
    label "kierunek"
  ]
  node [
    id 264
    label "digress"
  ]
  node [
    id 265
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 266
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 267
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 268
    label "odchodzi&#263;"
  ]
  node [
    id 269
    label "twist"
  ]
  node [
    id 270
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 271
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 272
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 273
    label "omowny"
  ]
  node [
    id 274
    label "figura_stylistyczna"
  ]
  node [
    id 275
    label "sformu&#322;owanie"
  ]
  node [
    id 276
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 277
    label "odchodzenie"
  ]
  node [
    id 278
    label "aberrance"
  ]
  node [
    id 279
    label "dyskutowa&#263;"
  ]
  node [
    id 280
    label "formu&#322;owa&#263;"
  ]
  node [
    id 281
    label "discourse"
  ]
  node [
    id 282
    label "perversion"
  ]
  node [
    id 283
    label "death"
  ]
  node [
    id 284
    label "odej&#347;cie"
  ]
  node [
    id 285
    label "turn"
  ]
  node [
    id 286
    label "k&#261;t"
  ]
  node [
    id 287
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 288
    label "odchylenie_si&#281;"
  ]
  node [
    id 289
    label "deviation"
  ]
  node [
    id 290
    label "patologia"
  ]
  node [
    id 291
    label "przedyskutowa&#263;"
  ]
  node [
    id 292
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 293
    label "publicize"
  ]
  node [
    id 294
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 295
    label "distract"
  ]
  node [
    id 296
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 297
    label "odej&#347;&#263;"
  ]
  node [
    id 298
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 299
    label "zmieni&#263;"
  ]
  node [
    id 300
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 301
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 302
    label "kognicja"
  ]
  node [
    id 303
    label "rozprawa"
  ]
  node [
    id 304
    label "wydarzenie"
  ]
  node [
    id 305
    label "szczeg&#243;&#322;"
  ]
  node [
    id 306
    label "proposition"
  ]
  node [
    id 307
    label "przes&#322;anka"
  ]
  node [
    id 308
    label "idea"
  ]
  node [
    id 309
    label "paj&#261;k"
  ]
  node [
    id 310
    label "przewodnik"
  ]
  node [
    id 311
    label "odcinek"
  ]
  node [
    id 312
    label "topikowate"
  ]
  node [
    id 313
    label "grupa_dyskusyjna"
  ]
  node [
    id 314
    label "s&#261;d"
  ]
  node [
    id 315
    label "plac"
  ]
  node [
    id 316
    label "bazylika"
  ]
  node [
    id 317
    label "przestrze&#324;"
  ]
  node [
    id 318
    label "portal"
  ]
  node [
    id 319
    label "konferencja"
  ]
  node [
    id 320
    label "agora"
  ]
  node [
    id 321
    label "grupa"
  ]
  node [
    id 322
    label "strona"
  ]
  node [
    id 323
    label "wzbudzenie"
  ]
  node [
    id 324
    label "gesture"
  ]
  node [
    id 325
    label "spowodowanie"
  ]
  node [
    id 326
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 327
    label "movement"
  ]
  node [
    id 328
    label "poruszanie_si&#281;"
  ]
  node [
    id 329
    label "zdarzenie_si&#281;"
  ]
  node [
    id 330
    label "ruch"
  ]
  node [
    id 331
    label "zrobienie"
  ]
  node [
    id 332
    label "mechanika"
  ]
  node [
    id 333
    label "utrzymywanie"
  ]
  node [
    id 334
    label "move"
  ]
  node [
    id 335
    label "myk"
  ]
  node [
    id 336
    label "utrzyma&#263;"
  ]
  node [
    id 337
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 338
    label "utrzymanie"
  ]
  node [
    id 339
    label "travel"
  ]
  node [
    id 340
    label "kanciasty"
  ]
  node [
    id 341
    label "commercial_enterprise"
  ]
  node [
    id 342
    label "model"
  ]
  node [
    id 343
    label "strumie&#324;"
  ]
  node [
    id 344
    label "proces"
  ]
  node [
    id 345
    label "aktywno&#347;&#263;"
  ]
  node [
    id 346
    label "kr&#243;tki"
  ]
  node [
    id 347
    label "taktyka"
  ]
  node [
    id 348
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 349
    label "apraksja"
  ]
  node [
    id 350
    label "natural_process"
  ]
  node [
    id 351
    label "utrzymywa&#263;"
  ]
  node [
    id 352
    label "d&#322;ugi"
  ]
  node [
    id 353
    label "dyssypacja_energii"
  ]
  node [
    id 354
    label "tumult"
  ]
  node [
    id 355
    label "stopek"
  ]
  node [
    id 356
    label "czynno&#347;&#263;"
  ]
  node [
    id 357
    label "zmiana"
  ]
  node [
    id 358
    label "manewr"
  ]
  node [
    id 359
    label "lokomocja"
  ]
  node [
    id 360
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 361
    label "komunikacja"
  ]
  node [
    id 362
    label "drift"
  ]
  node [
    id 363
    label "arousal"
  ]
  node [
    id 364
    label "wywo&#322;anie"
  ]
  node [
    id 365
    label "rozbudzenie"
  ]
  node [
    id 366
    label "campaign"
  ]
  node [
    id 367
    label "causing"
  ]
  node [
    id 368
    label "narobienie"
  ]
  node [
    id 369
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 370
    label "porobienie"
  ]
  node [
    id 371
    label "piwo"
  ]
  node [
    id 372
    label "uwarzenie"
  ]
  node [
    id 373
    label "warzenie"
  ]
  node [
    id 374
    label "alkohol"
  ]
  node [
    id 375
    label "nap&#243;j"
  ]
  node [
    id 376
    label "bacik"
  ]
  node [
    id 377
    label "wyj&#347;cie"
  ]
  node [
    id 378
    label "uwarzy&#263;"
  ]
  node [
    id 379
    label "birofilia"
  ]
  node [
    id 380
    label "warzy&#263;"
  ]
  node [
    id 381
    label "nawarzy&#263;"
  ]
  node [
    id 382
    label "browarnia"
  ]
  node [
    id 383
    label "nawarzenie"
  ]
  node [
    id 384
    label "anta&#322;"
  ]
  node [
    id 385
    label "dok&#322;adnie"
  ]
  node [
    id 386
    label "punctiliously"
  ]
  node [
    id 387
    label "meticulously"
  ]
  node [
    id 388
    label "precyzyjnie"
  ]
  node [
    id 389
    label "dok&#322;adny"
  ]
  node [
    id 390
    label "rzetelnie"
  ]
  node [
    id 391
    label "znoszenie"
  ]
  node [
    id 392
    label "nap&#322;ywanie"
  ]
  node [
    id 393
    label "communication"
  ]
  node [
    id 394
    label "signal"
  ]
  node [
    id 395
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 396
    label "znosi&#263;"
  ]
  node [
    id 397
    label "znie&#347;&#263;"
  ]
  node [
    id 398
    label "zniesienie"
  ]
  node [
    id 399
    label "zarys"
  ]
  node [
    id 400
    label "komunikat"
  ]
  node [
    id 401
    label "depesza_emska"
  ]
  node [
    id 402
    label "opracowanie"
  ]
  node [
    id 403
    label "pomys&#322;"
  ]
  node [
    id 404
    label "podstawy"
  ]
  node [
    id 405
    label "shape"
  ]
  node [
    id 406
    label "kreacjonista"
  ]
  node [
    id 407
    label "roi&#263;_si&#281;"
  ]
  node [
    id 408
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 409
    label "punkt"
  ]
  node [
    id 410
    label "wiedza"
  ]
  node [
    id 411
    label "doj&#347;cie"
  ]
  node [
    id 412
    label "obiega&#263;"
  ]
  node [
    id 413
    label "powzi&#281;cie"
  ]
  node [
    id 414
    label "obiegni&#281;cie"
  ]
  node [
    id 415
    label "sygna&#322;"
  ]
  node [
    id 416
    label "obieganie"
  ]
  node [
    id 417
    label "powzi&#261;&#263;"
  ]
  node [
    id 418
    label "obiec"
  ]
  node [
    id 419
    label "doj&#347;&#263;"
  ]
  node [
    id 420
    label "shoot"
  ]
  node [
    id 421
    label "pour"
  ]
  node [
    id 422
    label "zasila&#263;"
  ]
  node [
    id 423
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 424
    label "kapita&#322;"
  ]
  node [
    id 425
    label "meet"
  ]
  node [
    id 426
    label "dociera&#263;"
  ]
  node [
    id 427
    label "zbiera&#263;_si&#281;"
  ]
  node [
    id 428
    label "wzbiera&#263;"
  ]
  node [
    id 429
    label "ogarnia&#263;"
  ]
  node [
    id 430
    label "wype&#322;nia&#263;"
  ]
  node [
    id 431
    label "gromadzenie_si&#281;"
  ]
  node [
    id 432
    label "zbieranie_si&#281;"
  ]
  node [
    id 433
    label "zasilanie"
  ]
  node [
    id 434
    label "docieranie"
  ]
  node [
    id 435
    label "t&#281;&#380;enie"
  ]
  node [
    id 436
    label "nawiewanie"
  ]
  node [
    id 437
    label "nadmuchanie"
  ]
  node [
    id 438
    label "ogarnianie"
  ]
  node [
    id 439
    label "gromadzi&#263;"
  ]
  node [
    id 440
    label "usuwa&#263;"
  ]
  node [
    id 441
    label "porywa&#263;"
  ]
  node [
    id 442
    label "sk&#322;ada&#263;"
  ]
  node [
    id 443
    label "ranny"
  ]
  node [
    id 444
    label "zbiera&#263;"
  ]
  node [
    id 445
    label "behave"
  ]
  node [
    id 446
    label "carry"
  ]
  node [
    id 447
    label "represent"
  ]
  node [
    id 448
    label "podrze&#263;"
  ]
  node [
    id 449
    label "przenosi&#263;"
  ]
  node [
    id 450
    label "str&#243;j"
  ]
  node [
    id 451
    label "wytrzymywa&#263;"
  ]
  node [
    id 452
    label "seclude"
  ]
  node [
    id 453
    label "wygrywa&#263;"
  ]
  node [
    id 454
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 455
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 456
    label "set"
  ]
  node [
    id 457
    label "zu&#380;y&#263;"
  ]
  node [
    id 458
    label "niszczy&#263;"
  ]
  node [
    id 459
    label "tolerowa&#263;"
  ]
  node [
    id 460
    label "jajko"
  ]
  node [
    id 461
    label "zgromadzenie"
  ]
  node [
    id 462
    label "urodzenie"
  ]
  node [
    id 463
    label "suspension"
  ]
  node [
    id 464
    label "poddanie_si&#281;"
  ]
  node [
    id 465
    label "extinction"
  ]
  node [
    id 466
    label "coitus_interruptus"
  ]
  node [
    id 467
    label "przetrwanie"
  ]
  node [
    id 468
    label "&#347;cierpienie"
  ]
  node [
    id 469
    label "abolicjonista"
  ]
  node [
    id 470
    label "zniszczenie"
  ]
  node [
    id 471
    label "posk&#322;adanie"
  ]
  node [
    id 472
    label "zebranie"
  ]
  node [
    id 473
    label "przeniesienie"
  ]
  node [
    id 474
    label "removal"
  ]
  node [
    id 475
    label "withdrawal"
  ]
  node [
    id 476
    label "revocation"
  ]
  node [
    id 477
    label "usuni&#281;cie"
  ]
  node [
    id 478
    label "wygranie"
  ]
  node [
    id 479
    label "porwanie"
  ]
  node [
    id 480
    label "uniewa&#380;nienie"
  ]
  node [
    id 481
    label "toleration"
  ]
  node [
    id 482
    label "wytrzymywanie"
  ]
  node [
    id 483
    label "take"
  ]
  node [
    id 484
    label "usuwanie"
  ]
  node [
    id 485
    label "porywanie"
  ]
  node [
    id 486
    label "wygrywanie"
  ]
  node [
    id 487
    label "abrogation"
  ]
  node [
    id 488
    label "gromadzenie"
  ]
  node [
    id 489
    label "przenoszenie"
  ]
  node [
    id 490
    label "poddawanie_si&#281;"
  ]
  node [
    id 491
    label "wear"
  ]
  node [
    id 492
    label "uniewa&#380;nianie"
  ]
  node [
    id 493
    label "rodzenie"
  ]
  node [
    id 494
    label "tolerowanie"
  ]
  node [
    id 495
    label "niszczenie"
  ]
  node [
    id 496
    label "stand"
  ]
  node [
    id 497
    label "zgromadzi&#263;"
  ]
  node [
    id 498
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 499
    label "float"
  ]
  node [
    id 500
    label "revoke"
  ]
  node [
    id 501
    label "usun&#261;&#263;"
  ]
  node [
    id 502
    label "zebra&#263;"
  ]
  node [
    id 503
    label "wytrzyma&#263;"
  ]
  node [
    id 504
    label "digest"
  ]
  node [
    id 505
    label "lift"
  ]
  node [
    id 506
    label "podda&#263;_si&#281;"
  ]
  node [
    id 507
    label "przenie&#347;&#263;"
  ]
  node [
    id 508
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 509
    label "&#347;cierpie&#263;"
  ]
  node [
    id 510
    label "porwa&#263;"
  ]
  node [
    id 511
    label "wygra&#263;"
  ]
  node [
    id 512
    label "raise"
  ]
  node [
    id 513
    label "zniszczy&#263;"
  ]
  node [
    id 514
    label "thing"
  ]
  node [
    id 515
    label "cosik"
  ]
  node [
    id 516
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 517
    label "nieznaczny"
  ]
  node [
    id 518
    label "nieistotnie"
  ]
  node [
    id 519
    label "s&#322;aby"
  ]
  node [
    id 520
    label "nieznacznie"
  ]
  node [
    id 521
    label "drobnostkowy"
  ]
  node [
    id 522
    label "niewa&#380;ny"
  ]
  node [
    id 523
    label "ma&#322;y"
  ]
  node [
    id 524
    label "nietrwa&#322;y"
  ]
  node [
    id 525
    label "mizerny"
  ]
  node [
    id 526
    label "marnie"
  ]
  node [
    id 527
    label "delikatny"
  ]
  node [
    id 528
    label "po&#347;ledni"
  ]
  node [
    id 529
    label "niezdrowy"
  ]
  node [
    id 530
    label "z&#322;y"
  ]
  node [
    id 531
    label "nieumiej&#281;tny"
  ]
  node [
    id 532
    label "s&#322;abo"
  ]
  node [
    id 533
    label "lura"
  ]
  node [
    id 534
    label "nieudany"
  ]
  node [
    id 535
    label "s&#322;abowity"
  ]
  node [
    id 536
    label "zawodny"
  ]
  node [
    id 537
    label "&#322;agodny"
  ]
  node [
    id 538
    label "md&#322;y"
  ]
  node [
    id 539
    label "niedoskona&#322;y"
  ]
  node [
    id 540
    label "przemijaj&#261;cy"
  ]
  node [
    id 541
    label "niemocny"
  ]
  node [
    id 542
    label "niefajny"
  ]
  node [
    id 543
    label "kiepsko"
  ]
  node [
    id 544
    label "niepowa&#380;nie"
  ]
  node [
    id 545
    label "simile"
  ]
  node [
    id 546
    label "comparison"
  ]
  node [
    id 547
    label "zanalizowanie"
  ]
  node [
    id 548
    label "zestawienie"
  ]
  node [
    id 549
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 550
    label "rozwa&#380;enie"
  ]
  node [
    id 551
    label "udowodnienie"
  ]
  node [
    id 552
    label "przebadanie"
  ]
  node [
    id 553
    label "z&#322;amanie"
  ]
  node [
    id 554
    label "kompozycja"
  ]
  node [
    id 555
    label "strata"
  ]
  node [
    id 556
    label "composition"
  ]
  node [
    id 557
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 558
    label "z&#322;o&#380;enie"
  ]
  node [
    id 559
    label "sprawozdanie_finansowe"
  ]
  node [
    id 560
    label "z&#322;&#261;czenie"
  ]
  node [
    id 561
    label "count"
  ]
  node [
    id 562
    label "analiza"
  ]
  node [
    id 563
    label "deficyt"
  ]
  node [
    id 564
    label "obrot&#243;wka"
  ]
  node [
    id 565
    label "przedstawienie"
  ]
  node [
    id 566
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 567
    label "discrimination"
  ]
  node [
    id 568
    label "diverseness"
  ]
  node [
    id 569
    label "eklektyk"
  ]
  node [
    id 570
    label "rozproszenie_si&#281;"
  ]
  node [
    id 571
    label "differentiation"
  ]
  node [
    id 572
    label "bogactwo"
  ]
  node [
    id 573
    label "multikulturalizm"
  ]
  node [
    id 574
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 575
    label "rozdzielenie"
  ]
  node [
    id 576
    label "nadanie"
  ]
  node [
    id 577
    label "podzielenie"
  ]
  node [
    id 578
    label "Augiasz"
  ]
  node [
    id 579
    label "Stanis&#322;aw_August_Poniatowski"
  ]
  node [
    id 580
    label "koronowanie"
  ]
  node [
    id 581
    label "Henryk_IV"
  ]
  node [
    id 582
    label "kicaj"
  ]
  node [
    id 583
    label "trusia"
  ]
  node [
    id 584
    label "Edward_VII"
  ]
  node [
    id 585
    label "gruba_ryba"
  ]
  node [
    id 586
    label "Herod"
  ]
  node [
    id 587
    label "baron"
  ]
  node [
    id 588
    label "Artur"
  ]
  node [
    id 589
    label "Boles&#322;aw_Chrobry"
  ]
  node [
    id 590
    label "Manuel_I_Szcz&#281;&#347;liwy"
  ]
  node [
    id 591
    label "Aleksander_Wielki"
  ]
  node [
    id 592
    label "August_III_Sas"
  ]
  node [
    id 593
    label "Zygmunt_I_Stary"
  ]
  node [
    id 594
    label "Jugurta"
  ]
  node [
    id 595
    label "Ludwik_XVI"
  ]
  node [
    id 596
    label "turzyca"
  ]
  node [
    id 597
    label "Salomon"
  ]
  node [
    id 598
    label "figura_karciana"
  ]
  node [
    id 599
    label "Zygmunt_II_August"
  ]
  node [
    id 600
    label "figura"
  ]
  node [
    id 601
    label "monarchista"
  ]
  node [
    id 602
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 603
    label "zwierz&#281;_futerkowe"
  ]
  node [
    id 604
    label "koronowa&#263;"
  ]
  node [
    id 605
    label "monarcha"
  ]
  node [
    id 606
    label "Syzyf"
  ]
  node [
    id 607
    label "HP"
  ]
  node [
    id 608
    label "Otton_III"
  ]
  node [
    id 609
    label "Jan_Kazimierz"
  ]
  node [
    id 610
    label "kr&#243;lowa_matka"
  ]
  node [
    id 611
    label "tytu&#322;"
  ]
  node [
    id 612
    label "Karol_Albert"
  ]
  node [
    id 613
    label "Tantal"
  ]
  node [
    id 614
    label "zaj&#261;cowate"
  ]
  node [
    id 615
    label "Dawid"
  ]
  node [
    id 616
    label "Fryderyk_II_Wielki"
  ]
  node [
    id 617
    label "omyk"
  ]
  node [
    id 618
    label "basileus"
  ]
  node [
    id 619
    label "Kazimierz_Wielki"
  ]
  node [
    id 620
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 621
    label "Zygmunt_III_Waza"
  ]
  node [
    id 622
    label "p&#322;aszczyzna"
  ]
  node [
    id 623
    label "cz&#322;owiek"
  ]
  node [
    id 624
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 625
    label "bierka_szachowa"
  ]
  node [
    id 626
    label "obiekt_matematyczny"
  ]
  node [
    id 627
    label "gestaltyzm"
  ]
  node [
    id 628
    label "styl"
  ]
  node [
    id 629
    label "obraz"
  ]
  node [
    id 630
    label "Osjan"
  ]
  node [
    id 631
    label "d&#378;wi&#281;k"
  ]
  node [
    id 632
    label "character"
  ]
  node [
    id 633
    label "kto&#347;"
  ]
  node [
    id 634
    label "rze&#378;ba"
  ]
  node [
    id 635
    label "stylistyka"
  ]
  node [
    id 636
    label "figure"
  ]
  node [
    id 637
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 638
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 639
    label "antycypacja"
  ]
  node [
    id 640
    label "sztuka"
  ]
  node [
    id 641
    label "Aspazja"
  ]
  node [
    id 642
    label "facet"
  ]
  node [
    id 643
    label "popis"
  ]
  node [
    id 644
    label "kompleksja"
  ]
  node [
    id 645
    label "budowa"
  ]
  node [
    id 646
    label "symetria"
  ]
  node [
    id 647
    label "lingwistyka_kognitywna"
  ]
  node [
    id 648
    label "karta"
  ]
  node [
    id 649
    label "podzbi&#243;r"
  ]
  node [
    id 650
    label "point"
  ]
  node [
    id 651
    label "perspektywa"
  ]
  node [
    id 652
    label "sakra"
  ]
  node [
    id 653
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 654
    label "w&#322;adca"
  ]
  node [
    id 655
    label "fitofag"
  ]
  node [
    id 656
    label "herbivore"
  ]
  node [
    id 657
    label "zwierz&#281;"
  ]
  node [
    id 658
    label "ssak_&#380;yworodny"
  ]
  node [
    id 659
    label "ssaki_wy&#380;sze"
  ]
  node [
    id 660
    label "nadtytu&#322;"
  ]
  node [
    id 661
    label "tytulatura"
  ]
  node [
    id 662
    label "elevation"
  ]
  node [
    id 663
    label "mianowaniec"
  ]
  node [
    id 664
    label "nazwa"
  ]
  node [
    id 665
    label "podtytu&#322;"
  ]
  node [
    id 666
    label "arystokrata"
  ]
  node [
    id 667
    label "potentat"
  ]
  node [
    id 668
    label "lennik"
  ]
  node [
    id 669
    label "kr&#243;lik"
  ]
  node [
    id 670
    label "zaj&#261;c"
  ]
  node [
    id 671
    label "czujny"
  ]
  node [
    id 672
    label "strachliwy"
  ]
  node [
    id 673
    label "potulny"
  ]
  node [
    id 674
    label "cichy"
  ]
  node [
    id 675
    label "informatyka"
  ]
  node [
    id 676
    label "biblizm"
  ]
  node [
    id 677
    label "posta&#263;_biblijna"
  ]
  node [
    id 678
    label "stajnia_Augiasza"
  ]
  node [
    id 679
    label "Augeas"
  ]
  node [
    id 680
    label "koronowanie_si&#281;"
  ]
  node [
    id 681
    label "zwie&#324;czenie"
  ]
  node [
    id 682
    label "coronation"
  ]
  node [
    id 683
    label "nagradzanie"
  ]
  node [
    id 684
    label "wie&#324;czenie"
  ]
  node [
    id 685
    label "nagrodzenie"
  ]
  node [
    id 686
    label "mianowanie"
  ]
  node [
    id 687
    label "zwolennik"
  ]
  node [
    id 688
    label "Korwin"
  ]
  node [
    id 689
    label "wie&#324;czy&#263;"
  ]
  node [
    id 690
    label "nagrodzi&#263;"
  ]
  node [
    id 691
    label "nagradza&#263;"
  ]
  node [
    id 692
    label "zwie&#324;czy&#263;"
  ]
  node [
    id 693
    label "Crown"
  ]
  node [
    id 694
    label "mianowa&#263;"
  ]
  node [
    id 695
    label "kosmopolita"
  ]
  node [
    id 696
    label "ciborowate"
  ]
  node [
    id 697
    label "samica"
  ]
  node [
    id 698
    label "szuwar_turzycowy"
  ]
  node [
    id 699
    label "bylina"
  ]
  node [
    id 700
    label "trawa"
  ]
  node [
    id 701
    label "tur"
  ]
  node [
    id 702
    label "sier&#347;&#263;"
  ]
  node [
    id 703
    label "ogon"
  ]
  node [
    id 704
    label "zaj&#281;czaki"
  ]
  node [
    id 705
    label "pomiernie"
  ]
  node [
    id 706
    label "kr&#243;tko"
  ]
  node [
    id 707
    label "mikroskopijnie"
  ]
  node [
    id 708
    label "nieliczny"
  ]
  node [
    id 709
    label "mo&#380;liwie"
  ]
  node [
    id 710
    label "mo&#380;liwy"
  ]
  node [
    id 711
    label "zno&#347;nie"
  ]
  node [
    id 712
    label "malusie&#324;ko"
  ]
  node [
    id 713
    label "mikroskopijny"
  ]
  node [
    id 714
    label "bardzo"
  ]
  node [
    id 715
    label "szybki"
  ]
  node [
    id 716
    label "przeci&#281;tny"
  ]
  node [
    id 717
    label "wstydliwy"
  ]
  node [
    id 718
    label "ch&#322;opiec"
  ]
  node [
    id 719
    label "m&#322;ody"
  ]
  node [
    id 720
    label "marny"
  ]
  node [
    id 721
    label "n&#281;dznie"
  ]
  node [
    id 722
    label "nielicznie"
  ]
  node [
    id 723
    label "licho"
  ]
  node [
    id 724
    label "proporcjonalnie"
  ]
  node [
    id 725
    label "pomierny"
  ]
  node [
    id 726
    label "miernie"
  ]
  node [
    id 727
    label "interesuj&#261;co"
  ]
  node [
    id 728
    label "swoisty"
  ]
  node [
    id 729
    label "dziwny"
  ]
  node [
    id 730
    label "ciekawie"
  ]
  node [
    id 731
    label "g&#322;adki"
  ]
  node [
    id 732
    label "uatrakcyjnianie"
  ]
  node [
    id 733
    label "atrakcyjnie"
  ]
  node [
    id 734
    label "poci&#261;gaj&#261;co"
  ]
  node [
    id 735
    label "po&#380;&#261;dany"
  ]
  node [
    id 736
    label "uatrakcyjnienie"
  ]
  node [
    id 737
    label "dziwnie"
  ]
  node [
    id 738
    label "dziwy"
  ]
  node [
    id 739
    label "inny"
  ]
  node [
    id 740
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 741
    label "odr&#281;bny"
  ]
  node [
    id 742
    label "swoi&#347;cie"
  ]
  node [
    id 743
    label "ciekawy"
  ]
  node [
    id 744
    label "dobrze"
  ]
  node [
    id 745
    label "odmienno&#347;&#263;"
  ]
  node [
    id 746
    label "reverse"
  ]
  node [
    id 747
    label "obstruction"
  ]
  node [
    id 748
    label "trudno&#347;&#263;"
  ]
  node [
    id 749
    label "odwrotny"
  ]
  node [
    id 750
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 751
    label "napotka&#263;"
  ]
  node [
    id 752
    label "subiekcja"
  ]
  node [
    id 753
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 754
    label "k&#322;opotliwy"
  ]
  node [
    id 755
    label "napotkanie"
  ]
  node [
    id 756
    label "poziom"
  ]
  node [
    id 757
    label "difficulty"
  ]
  node [
    id 758
    label "obstacle"
  ]
  node [
    id 759
    label "kategoria_gramatyczna"
  ]
  node [
    id 760
    label "r&#243;&#380;nica"
  ]
  node [
    id 761
    label "dziwno&#347;&#263;"
  ]
  node [
    id 762
    label "wyj&#261;tkowo&#347;&#263;"
  ]
  node [
    id 763
    label "przeciwny"
  ]
  node [
    id 764
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 765
    label "odmienny"
  ]
  node [
    id 766
    label "odwrotnie"
  ]
  node [
    id 767
    label "przeciwnie"
  ]
  node [
    id 768
    label "dawny"
  ]
  node [
    id 769
    label "ogl&#281;dny"
  ]
  node [
    id 770
    label "du&#380;y"
  ]
  node [
    id 771
    label "daleko"
  ]
  node [
    id 772
    label "odleg&#322;y"
  ]
  node [
    id 773
    label "zwi&#261;zany"
  ]
  node [
    id 774
    label "r&#243;&#380;ny"
  ]
  node [
    id 775
    label "odlegle"
  ]
  node [
    id 776
    label "oddalony"
  ]
  node [
    id 777
    label "g&#322;&#281;boki"
  ]
  node [
    id 778
    label "obcy"
  ]
  node [
    id 779
    label "nieobecny"
  ]
  node [
    id 780
    label "przysz&#322;y"
  ]
  node [
    id 781
    label "nadprzyrodzony"
  ]
  node [
    id 782
    label "nieznany"
  ]
  node [
    id 783
    label "pozaludzki"
  ]
  node [
    id 784
    label "obco"
  ]
  node [
    id 785
    label "tameczny"
  ]
  node [
    id 786
    label "osoba"
  ]
  node [
    id 787
    label "nieznajomo"
  ]
  node [
    id 788
    label "cudzy"
  ]
  node [
    id 789
    label "istota_&#380;ywa"
  ]
  node [
    id 790
    label "zaziemsko"
  ]
  node [
    id 791
    label "doros&#322;y"
  ]
  node [
    id 792
    label "niema&#322;o"
  ]
  node [
    id 793
    label "wiele"
  ]
  node [
    id 794
    label "rozwini&#281;ty"
  ]
  node [
    id 795
    label "dorodny"
  ]
  node [
    id 796
    label "prawdziwy"
  ]
  node [
    id 797
    label "du&#380;o"
  ]
  node [
    id 798
    label "kolejny"
  ]
  node [
    id 799
    label "nieprzytomny"
  ]
  node [
    id 800
    label "opuszczenie"
  ]
  node [
    id 801
    label "opuszczanie"
  ]
  node [
    id 802
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 803
    label "po&#322;&#261;czenie"
  ]
  node [
    id 804
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 805
    label "przestarza&#322;y"
  ]
  node [
    id 806
    label "przesz&#322;y"
  ]
  node [
    id 807
    label "od_dawna"
  ]
  node [
    id 808
    label "poprzedni"
  ]
  node [
    id 809
    label "dawno"
  ]
  node [
    id 810
    label "d&#322;ugoletni"
  ]
  node [
    id 811
    label "anachroniczny"
  ]
  node [
    id 812
    label "dawniej"
  ]
  node [
    id 813
    label "niegdysiejszy"
  ]
  node [
    id 814
    label "wcze&#347;niejszy"
  ]
  node [
    id 815
    label "kombatant"
  ]
  node [
    id 816
    label "stary"
  ]
  node [
    id 817
    label "ogl&#281;dnie"
  ]
  node [
    id 818
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 819
    label "stosowny"
  ]
  node [
    id 820
    label "ch&#322;odny"
  ]
  node [
    id 821
    label "og&#243;lny"
  ]
  node [
    id 822
    label "okr&#261;g&#322;y"
  ]
  node [
    id 823
    label "byle_jaki"
  ]
  node [
    id 824
    label "niedok&#322;adny"
  ]
  node [
    id 825
    label "cnotliwy"
  ]
  node [
    id 826
    label "intensywny"
  ]
  node [
    id 827
    label "gruntowny"
  ]
  node [
    id 828
    label "mocny"
  ]
  node [
    id 829
    label "szczery"
  ]
  node [
    id 830
    label "ukryty"
  ]
  node [
    id 831
    label "wyrazisty"
  ]
  node [
    id 832
    label "dog&#322;&#281;bny"
  ]
  node [
    id 833
    label "g&#322;&#281;boko"
  ]
  node [
    id 834
    label "niezrozumia&#322;y"
  ]
  node [
    id 835
    label "niski"
  ]
  node [
    id 836
    label "m&#261;dry"
  ]
  node [
    id 837
    label "oderwany"
  ]
  node [
    id 838
    label "jaki&#347;"
  ]
  node [
    id 839
    label "r&#243;&#380;nie"
  ]
  node [
    id 840
    label "nisko"
  ]
  node [
    id 841
    label "znacznie"
  ]
  node [
    id 842
    label "het"
  ]
  node [
    id 843
    label "nieobecnie"
  ]
  node [
    id 844
    label "wysoko"
  ]
  node [
    id 845
    label "d&#322;ugo"
  ]
  node [
    id 846
    label "rzuci&#263;"
  ]
  node [
    id 847
    label "destiny"
  ]
  node [
    id 848
    label "si&#322;a"
  ]
  node [
    id 849
    label "przymus"
  ]
  node [
    id 850
    label "&#380;ycie"
  ]
  node [
    id 851
    label "hazard"
  ]
  node [
    id 852
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 853
    label "rzucenie"
  ]
  node [
    id 854
    label "przebieg_&#380;ycia"
  ]
  node [
    id 855
    label "bilet"
  ]
  node [
    id 856
    label "karta_wst&#281;pu"
  ]
  node [
    id 857
    label "konik"
  ]
  node [
    id 858
    label "passe-partout"
  ]
  node [
    id 859
    label "cedu&#322;a"
  ]
  node [
    id 860
    label "energia"
  ]
  node [
    id 861
    label "parametr"
  ]
  node [
    id 862
    label "rozwi&#261;zanie"
  ]
  node [
    id 863
    label "wuchta"
  ]
  node [
    id 864
    label "zaleta"
  ]
  node [
    id 865
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 866
    label "moment_si&#322;y"
  ]
  node [
    id 867
    label "mn&#243;stwo"
  ]
  node [
    id 868
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 869
    label "capacity"
  ]
  node [
    id 870
    label "magnitude"
  ]
  node [
    id 871
    label "potencja"
  ]
  node [
    id 872
    label "przemoc"
  ]
  node [
    id 873
    label "potrzeba"
  ]
  node [
    id 874
    label "presja"
  ]
  node [
    id 875
    label "raj_utracony"
  ]
  node [
    id 876
    label "umieranie"
  ]
  node [
    id 877
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 878
    label "prze&#380;ywanie"
  ]
  node [
    id 879
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 880
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 881
    label "po&#322;&#243;g"
  ]
  node [
    id 882
    label "umarcie"
  ]
  node [
    id 883
    label "subsistence"
  ]
  node [
    id 884
    label "power"
  ]
  node [
    id 885
    label "okres_noworodkowy"
  ]
  node [
    id 886
    label "prze&#380;ycie"
  ]
  node [
    id 887
    label "wiek_matuzalemowy"
  ]
  node [
    id 888
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 889
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 890
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 891
    label "do&#380;ywanie"
  ]
  node [
    id 892
    label "byt"
  ]
  node [
    id 893
    label "dzieci&#324;stwo"
  ]
  node [
    id 894
    label "andropauza"
  ]
  node [
    id 895
    label "rozw&#243;j"
  ]
  node [
    id 896
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 897
    label "czas"
  ]
  node [
    id 898
    label "menopauza"
  ]
  node [
    id 899
    label "&#347;mier&#263;"
  ]
  node [
    id 900
    label "koleje_losu"
  ]
  node [
    id 901
    label "bycie"
  ]
  node [
    id 902
    label "zegar_biologiczny"
  ]
  node [
    id 903
    label "szwung"
  ]
  node [
    id 904
    label "przebywanie"
  ]
  node [
    id 905
    label "warunki"
  ]
  node [
    id 906
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 907
    label "niemowl&#281;ctwo"
  ]
  node [
    id 908
    label "&#380;ywy"
  ]
  node [
    id 909
    label "life"
  ]
  node [
    id 910
    label "staro&#347;&#263;"
  ]
  node [
    id 911
    label "energy"
  ]
  node [
    id 912
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 913
    label "konwulsja"
  ]
  node [
    id 914
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 915
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 916
    label "ruszy&#263;"
  ]
  node [
    id 917
    label "powiedzie&#263;"
  ]
  node [
    id 918
    label "majdn&#261;&#263;"
  ]
  node [
    id 919
    label "most"
  ]
  node [
    id 920
    label "poruszy&#263;"
  ]
  node [
    id 921
    label "wyzwanie"
  ]
  node [
    id 922
    label "da&#263;"
  ]
  node [
    id 923
    label "peddle"
  ]
  node [
    id 924
    label "rush"
  ]
  node [
    id 925
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 926
    label "bewilder"
  ]
  node [
    id 927
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 928
    label "przeznaczenie"
  ]
  node [
    id 929
    label "skonstruowa&#263;"
  ]
  node [
    id 930
    label "sygn&#261;&#263;"
  ]
  node [
    id 931
    label "&#347;wiat&#322;o"
  ]
  node [
    id 932
    label "spowodowa&#263;"
  ]
  node [
    id 933
    label "wywo&#322;a&#263;"
  ]
  node [
    id 934
    label "frame"
  ]
  node [
    id 935
    label "podejrzenie"
  ]
  node [
    id 936
    label "czar"
  ]
  node [
    id 937
    label "project"
  ]
  node [
    id 938
    label "zdecydowa&#263;"
  ]
  node [
    id 939
    label "cie&#324;"
  ]
  node [
    id 940
    label "atak"
  ]
  node [
    id 941
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 942
    label "towar"
  ]
  node [
    id 943
    label "ruszenie"
  ]
  node [
    id 944
    label "pierdolni&#281;cie"
  ]
  node [
    id 945
    label "przewr&#243;cenie"
  ]
  node [
    id 946
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 947
    label "skonstruowanie"
  ]
  node [
    id 948
    label "grzmotni&#281;cie"
  ]
  node [
    id 949
    label "zdecydowanie"
  ]
  node [
    id 950
    label "przemieszczenie"
  ]
  node [
    id 951
    label "wyposa&#380;enie"
  ]
  node [
    id 952
    label "shy"
  ]
  node [
    id 953
    label "oddzia&#322;anie"
  ]
  node [
    id 954
    label "zrezygnowanie"
  ]
  node [
    id 955
    label "porzucenie"
  ]
  node [
    id 956
    label "powiedzenie"
  ]
  node [
    id 957
    label "rzucanie"
  ]
  node [
    id 958
    label "play"
  ]
  node [
    id 959
    label "prawdopodobie&#324;stwo"
  ]
  node [
    id 960
    label "rozrywka"
  ]
  node [
    id 961
    label "wideoloteria"
  ]
  node [
    id 962
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 963
    label "czadowy"
  ]
  node [
    id 964
    label "fachowy"
  ]
  node [
    id 965
    label "fajny"
  ]
  node [
    id 966
    label "klawy"
  ]
  node [
    id 967
    label "zawodowo"
  ]
  node [
    id 968
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 969
    label "formalny"
  ]
  node [
    id 970
    label "zawo&#322;any"
  ]
  node [
    id 971
    label "profesjonalny"
  ]
  node [
    id 972
    label "formalizowanie"
  ]
  node [
    id 973
    label "formalnie"
  ]
  node [
    id 974
    label "pozorny"
  ]
  node [
    id 975
    label "kompletny"
  ]
  node [
    id 976
    label "oficjalny"
  ]
  node [
    id 977
    label "sformalizowanie"
  ]
  node [
    id 978
    label "prawomocny"
  ]
  node [
    id 979
    label "trained"
  ]
  node [
    id 980
    label "porz&#261;dny"
  ]
  node [
    id 981
    label "profesjonalnie"
  ]
  node [
    id 982
    label "rzetelny"
  ]
  node [
    id 983
    label "kompetentny"
  ]
  node [
    id 984
    label "specjalny"
  ]
  node [
    id 985
    label "co_si&#281;_zowie"
  ]
  node [
    id 986
    label "umiej&#281;tny"
  ]
  node [
    id 987
    label "fachowo"
  ]
  node [
    id 988
    label "specjalistyczny"
  ]
  node [
    id 989
    label "czadowo"
  ]
  node [
    id 990
    label "dynamiczny"
  ]
  node [
    id 991
    label "odjazdowy"
  ]
  node [
    id 992
    label "ostry"
  ]
  node [
    id 993
    label "byczy"
  ]
  node [
    id 994
    label "fajnie"
  ]
  node [
    id 995
    label "na_schwa&#322;"
  ]
  node [
    id 996
    label "klawo"
  ]
  node [
    id 997
    label "go&#322;&#261;b"
  ]
  node [
    id 998
    label "ptak"
  ]
  node [
    id 999
    label "grucha&#263;"
  ]
  node [
    id 1000
    label "zagrucha&#263;"
  ]
  node [
    id 1001
    label "go&#322;&#281;bie"
  ]
  node [
    id 1002
    label "gruchanie"
  ]
  node [
    id 1003
    label "siwy"
  ]
  node [
    id 1004
    label "pigeon"
  ]
  node [
    id 1005
    label "przesy&#322;ka"
  ]
  node [
    id 1006
    label "dobroczynny"
  ]
  node [
    id 1007
    label "czw&#243;rka"
  ]
  node [
    id 1008
    label "spokojny"
  ]
  node [
    id 1009
    label "skuteczny"
  ]
  node [
    id 1010
    label "&#347;mieszny"
  ]
  node [
    id 1011
    label "mi&#322;y"
  ]
  node [
    id 1012
    label "grzeczny"
  ]
  node [
    id 1013
    label "powitanie"
  ]
  node [
    id 1014
    label "ca&#322;y"
  ]
  node [
    id 1015
    label "zwrot"
  ]
  node [
    id 1016
    label "pomy&#347;lny"
  ]
  node [
    id 1017
    label "moralny"
  ]
  node [
    id 1018
    label "drogi"
  ]
  node [
    id 1019
    label "pozytywny"
  ]
  node [
    id 1020
    label "odpowiedni"
  ]
  node [
    id 1021
    label "korzystny"
  ]
  node [
    id 1022
    label "pos&#322;uszny"
  ]
  node [
    id 1023
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 1024
    label "powodowanie"
  ]
  node [
    id 1025
    label "bezproblemowy"
  ]
  node [
    id 1026
    label "elegancki"
  ]
  node [
    id 1027
    label "og&#243;lnikowy"
  ]
  node [
    id 1028
    label "g&#322;adzenie"
  ]
  node [
    id 1029
    label "nieruchomy"
  ]
  node [
    id 1030
    label "&#322;atwy"
  ]
  node [
    id 1031
    label "r&#243;wny"
  ]
  node [
    id 1032
    label "wyg&#322;adzanie_si&#281;"
  ]
  node [
    id 1033
    label "jednobarwny"
  ]
  node [
    id 1034
    label "przyg&#322;adzenie"
  ]
  node [
    id 1035
    label "&#322;adny"
  ]
  node [
    id 1036
    label "obtaczanie"
  ]
  node [
    id 1037
    label "g&#322;adko"
  ]
  node [
    id 1038
    label "kulturalny"
  ]
  node [
    id 1039
    label "prosty"
  ]
  node [
    id 1040
    label "przyg&#322;adzanie"
  ]
  node [
    id 1041
    label "cisza"
  ]
  node [
    id 1042
    label "wyg&#322;adzenie_si&#281;"
  ]
  node [
    id 1043
    label "wyg&#322;adzenie"
  ]
  node [
    id 1044
    label "wyr&#243;wnanie"
  ]
  node [
    id 1045
    label "Katar"
  ]
  node [
    id 1046
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1047
    label "Libia"
  ]
  node [
    id 1048
    label "Gwatemala"
  ]
  node [
    id 1049
    label "Afganistan"
  ]
  node [
    id 1050
    label "Ekwador"
  ]
  node [
    id 1051
    label "Tad&#380;ykistan"
  ]
  node [
    id 1052
    label "Bhutan"
  ]
  node [
    id 1053
    label "Argentyna"
  ]
  node [
    id 1054
    label "D&#380;ibuti"
  ]
  node [
    id 1055
    label "Wenezuela"
  ]
  node [
    id 1056
    label "Ukraina"
  ]
  node [
    id 1057
    label "Gabon"
  ]
  node [
    id 1058
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1059
    label "Rwanda"
  ]
  node [
    id 1060
    label "Liechtenstein"
  ]
  node [
    id 1061
    label "organizacja"
  ]
  node [
    id 1062
    label "Sri_Lanka"
  ]
  node [
    id 1063
    label "Madagaskar"
  ]
  node [
    id 1064
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1065
    label "Tonga"
  ]
  node [
    id 1066
    label "Kongo"
  ]
  node [
    id 1067
    label "Kanada"
  ]
  node [
    id 1068
    label "Wehrlen"
  ]
  node [
    id 1069
    label "Algieria"
  ]
  node [
    id 1070
    label "Surinam"
  ]
  node [
    id 1071
    label "Chile"
  ]
  node [
    id 1072
    label "Sahara_Zachodnia"
  ]
  node [
    id 1073
    label "Uganda"
  ]
  node [
    id 1074
    label "W&#281;gry"
  ]
  node [
    id 1075
    label "Birma"
  ]
  node [
    id 1076
    label "Kazachstan"
  ]
  node [
    id 1077
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1078
    label "Armenia"
  ]
  node [
    id 1079
    label "Tuwalu"
  ]
  node [
    id 1080
    label "Timor_Wschodni"
  ]
  node [
    id 1081
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1082
    label "Izrael"
  ]
  node [
    id 1083
    label "Estonia"
  ]
  node [
    id 1084
    label "Komory"
  ]
  node [
    id 1085
    label "Kamerun"
  ]
  node [
    id 1086
    label "Haiti"
  ]
  node [
    id 1087
    label "Belize"
  ]
  node [
    id 1088
    label "Sierra_Leone"
  ]
  node [
    id 1089
    label "Luksemburg"
  ]
  node [
    id 1090
    label "USA"
  ]
  node [
    id 1091
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1092
    label "Barbados"
  ]
  node [
    id 1093
    label "San_Marino"
  ]
  node [
    id 1094
    label "Bu&#322;garia"
  ]
  node [
    id 1095
    label "Wietnam"
  ]
  node [
    id 1096
    label "Indonezja"
  ]
  node [
    id 1097
    label "Malawi"
  ]
  node [
    id 1098
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1099
    label "Francja"
  ]
  node [
    id 1100
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1101
    label "partia"
  ]
  node [
    id 1102
    label "Zambia"
  ]
  node [
    id 1103
    label "Angola"
  ]
  node [
    id 1104
    label "Grenada"
  ]
  node [
    id 1105
    label "Nepal"
  ]
  node [
    id 1106
    label "Panama"
  ]
  node [
    id 1107
    label "Rumunia"
  ]
  node [
    id 1108
    label "Czarnog&#243;ra"
  ]
  node [
    id 1109
    label "Malediwy"
  ]
  node [
    id 1110
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1111
    label "S&#322;owacja"
  ]
  node [
    id 1112
    label "para"
  ]
  node [
    id 1113
    label "Egipt"
  ]
  node [
    id 1114
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1115
    label "Kolumbia"
  ]
  node [
    id 1116
    label "Mozambik"
  ]
  node [
    id 1117
    label "Laos"
  ]
  node [
    id 1118
    label "Burundi"
  ]
  node [
    id 1119
    label "Suazi"
  ]
  node [
    id 1120
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1121
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1122
    label "Czechy"
  ]
  node [
    id 1123
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1124
    label "Wyspy_Marshalla"
  ]
  node [
    id 1125
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1126
    label "Dominika"
  ]
  node [
    id 1127
    label "Palau"
  ]
  node [
    id 1128
    label "Syria"
  ]
  node [
    id 1129
    label "Gwinea_Bissau"
  ]
  node [
    id 1130
    label "Liberia"
  ]
  node [
    id 1131
    label "Zimbabwe"
  ]
  node [
    id 1132
    label "Polska"
  ]
  node [
    id 1133
    label "Jamajka"
  ]
  node [
    id 1134
    label "Dominikana"
  ]
  node [
    id 1135
    label "Senegal"
  ]
  node [
    id 1136
    label "Gruzja"
  ]
  node [
    id 1137
    label "Togo"
  ]
  node [
    id 1138
    label "Chorwacja"
  ]
  node [
    id 1139
    label "Meksyk"
  ]
  node [
    id 1140
    label "Macedonia"
  ]
  node [
    id 1141
    label "Gujana"
  ]
  node [
    id 1142
    label "Zair"
  ]
  node [
    id 1143
    label "Albania"
  ]
  node [
    id 1144
    label "Kambod&#380;a"
  ]
  node [
    id 1145
    label "Mauritius"
  ]
  node [
    id 1146
    label "Monako"
  ]
  node [
    id 1147
    label "Gwinea"
  ]
  node [
    id 1148
    label "Mali"
  ]
  node [
    id 1149
    label "Nigeria"
  ]
  node [
    id 1150
    label "Kostaryka"
  ]
  node [
    id 1151
    label "Hanower"
  ]
  node [
    id 1152
    label "Paragwaj"
  ]
  node [
    id 1153
    label "W&#322;ochy"
  ]
  node [
    id 1154
    label "Wyspy_Salomona"
  ]
  node [
    id 1155
    label "Seszele"
  ]
  node [
    id 1156
    label "Hiszpania"
  ]
  node [
    id 1157
    label "Boliwia"
  ]
  node [
    id 1158
    label "Kirgistan"
  ]
  node [
    id 1159
    label "Irlandia"
  ]
  node [
    id 1160
    label "Czad"
  ]
  node [
    id 1161
    label "Irak"
  ]
  node [
    id 1162
    label "Lesoto"
  ]
  node [
    id 1163
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1164
    label "Malta"
  ]
  node [
    id 1165
    label "Andora"
  ]
  node [
    id 1166
    label "Chiny"
  ]
  node [
    id 1167
    label "Filipiny"
  ]
  node [
    id 1168
    label "Antarktis"
  ]
  node [
    id 1169
    label "Niemcy"
  ]
  node [
    id 1170
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1171
    label "Brazylia"
  ]
  node [
    id 1172
    label "terytorium"
  ]
  node [
    id 1173
    label "Nikaragua"
  ]
  node [
    id 1174
    label "Pakistan"
  ]
  node [
    id 1175
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1176
    label "Kenia"
  ]
  node [
    id 1177
    label "Niger"
  ]
  node [
    id 1178
    label "Tunezja"
  ]
  node [
    id 1179
    label "Portugalia"
  ]
  node [
    id 1180
    label "Fid&#380;i"
  ]
  node [
    id 1181
    label "Maroko"
  ]
  node [
    id 1182
    label "Botswana"
  ]
  node [
    id 1183
    label "Tajlandia"
  ]
  node [
    id 1184
    label "Australia"
  ]
  node [
    id 1185
    label "Burkina_Faso"
  ]
  node [
    id 1186
    label "interior"
  ]
  node [
    id 1187
    label "Benin"
  ]
  node [
    id 1188
    label "Tanzania"
  ]
  node [
    id 1189
    label "Indie"
  ]
  node [
    id 1190
    label "&#321;otwa"
  ]
  node [
    id 1191
    label "Kiribati"
  ]
  node [
    id 1192
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1193
    label "Rodezja"
  ]
  node [
    id 1194
    label "Cypr"
  ]
  node [
    id 1195
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1196
    label "Peru"
  ]
  node [
    id 1197
    label "Austria"
  ]
  node [
    id 1198
    label "Urugwaj"
  ]
  node [
    id 1199
    label "Jordania"
  ]
  node [
    id 1200
    label "Grecja"
  ]
  node [
    id 1201
    label "Azerbejd&#380;an"
  ]
  node [
    id 1202
    label "Turcja"
  ]
  node [
    id 1203
    label "Samoa"
  ]
  node [
    id 1204
    label "Sudan"
  ]
  node [
    id 1205
    label "Oman"
  ]
  node [
    id 1206
    label "ziemia"
  ]
  node [
    id 1207
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1208
    label "Uzbekistan"
  ]
  node [
    id 1209
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1210
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1211
    label "Honduras"
  ]
  node [
    id 1212
    label "Mongolia"
  ]
  node [
    id 1213
    label "Portoryko"
  ]
  node [
    id 1214
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1215
    label "Serbia"
  ]
  node [
    id 1216
    label "Tajwan"
  ]
  node [
    id 1217
    label "Wielka_Brytania"
  ]
  node [
    id 1218
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1219
    label "Liban"
  ]
  node [
    id 1220
    label "Japonia"
  ]
  node [
    id 1221
    label "Ghana"
  ]
  node [
    id 1222
    label "Bahrajn"
  ]
  node [
    id 1223
    label "Belgia"
  ]
  node [
    id 1224
    label "Etiopia"
  ]
  node [
    id 1225
    label "Mikronezja"
  ]
  node [
    id 1226
    label "Kuwejt"
  ]
  node [
    id 1227
    label "Bahamy"
  ]
  node [
    id 1228
    label "Rosja"
  ]
  node [
    id 1229
    label "Mo&#322;dawia"
  ]
  node [
    id 1230
    label "Litwa"
  ]
  node [
    id 1231
    label "S&#322;owenia"
  ]
  node [
    id 1232
    label "Szwajcaria"
  ]
  node [
    id 1233
    label "Erytrea"
  ]
  node [
    id 1234
    label "Kuba"
  ]
  node [
    id 1235
    label "Arabia_Saudyjska"
  ]
  node [
    id 1236
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1237
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1238
    label "Malezja"
  ]
  node [
    id 1239
    label "Korea"
  ]
  node [
    id 1240
    label "Jemen"
  ]
  node [
    id 1241
    label "Nowa_Zelandia"
  ]
  node [
    id 1242
    label "Namibia"
  ]
  node [
    id 1243
    label "Nauru"
  ]
  node [
    id 1244
    label "holoarktyka"
  ]
  node [
    id 1245
    label "Brunei"
  ]
  node [
    id 1246
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1247
    label "Khitai"
  ]
  node [
    id 1248
    label "Mauretania"
  ]
  node [
    id 1249
    label "Iran"
  ]
  node [
    id 1250
    label "Gambia"
  ]
  node [
    id 1251
    label "Somalia"
  ]
  node [
    id 1252
    label "Holandia"
  ]
  node [
    id 1253
    label "Turkmenistan"
  ]
  node [
    id 1254
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1255
    label "Salwador"
  ]
  node [
    id 1256
    label "pair"
  ]
  node [
    id 1257
    label "zesp&#243;&#322;"
  ]
  node [
    id 1258
    label "odparowywanie"
  ]
  node [
    id 1259
    label "gaz_cieplarniany"
  ]
  node [
    id 1260
    label "chodzi&#263;"
  ]
  node [
    id 1261
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1262
    label "poker"
  ]
  node [
    id 1263
    label "moneta"
  ]
  node [
    id 1264
    label "parowanie"
  ]
  node [
    id 1265
    label "damp"
  ]
  node [
    id 1266
    label "nale&#380;e&#263;"
  ]
  node [
    id 1267
    label "odparowanie"
  ]
  node [
    id 1268
    label "odparowa&#263;"
  ]
  node [
    id 1269
    label "dodatek"
  ]
  node [
    id 1270
    label "smoke"
  ]
  node [
    id 1271
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1272
    label "odparowywa&#263;"
  ]
  node [
    id 1273
    label "uk&#322;ad"
  ]
  node [
    id 1274
    label "gaz"
  ]
  node [
    id 1275
    label "wyparowanie"
  ]
  node [
    id 1276
    label "obszar"
  ]
  node [
    id 1277
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1278
    label "jednostka_administracyjna"
  ]
  node [
    id 1279
    label "Jukon"
  ]
  node [
    id 1280
    label "podmiot"
  ]
  node [
    id 1281
    label "jednostka_organizacyjna"
  ]
  node [
    id 1282
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1283
    label "TOPR"
  ]
  node [
    id 1284
    label "endecki"
  ]
  node [
    id 1285
    label "przedstawicielstwo"
  ]
  node [
    id 1286
    label "od&#322;am"
  ]
  node [
    id 1287
    label "Cepelia"
  ]
  node [
    id 1288
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1289
    label "ZBoWiD"
  ]
  node [
    id 1290
    label "organization"
  ]
  node [
    id 1291
    label "centrala"
  ]
  node [
    id 1292
    label "GOPR"
  ]
  node [
    id 1293
    label "ZOMO"
  ]
  node [
    id 1294
    label "ZMP"
  ]
  node [
    id 1295
    label "komitet_koordynacyjny"
  ]
  node [
    id 1296
    label "przybud&#243;wka"
  ]
  node [
    id 1297
    label "boj&#243;wka"
  ]
  node [
    id 1298
    label "turning"
  ]
  node [
    id 1299
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1300
    label "skr&#281;t"
  ]
  node [
    id 1301
    label "obr&#243;t"
  ]
  node [
    id 1302
    label "fraza_czasownikowa"
  ]
  node [
    id 1303
    label "jednostka_leksykalna"
  ]
  node [
    id 1304
    label "odm&#322;adzanie"
  ]
  node [
    id 1305
    label "liga"
  ]
  node [
    id 1306
    label "asymilowanie"
  ]
  node [
    id 1307
    label "gromada"
  ]
  node [
    id 1308
    label "asymilowa&#263;"
  ]
  node [
    id 1309
    label "Entuzjastki"
  ]
  node [
    id 1310
    label "Terranie"
  ]
  node [
    id 1311
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1312
    label "category"
  ]
  node [
    id 1313
    label "oddzia&#322;"
  ]
  node [
    id 1314
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1315
    label "cz&#261;steczka"
  ]
  node [
    id 1316
    label "stage_set"
  ]
  node [
    id 1317
    label "type"
  ]
  node [
    id 1318
    label "specgrupa"
  ]
  node [
    id 1319
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1320
    label "&#346;wietliki"
  ]
  node [
    id 1321
    label "odm&#322;odzenie"
  ]
  node [
    id 1322
    label "Eurogrupa"
  ]
  node [
    id 1323
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1324
    label "formacja_geologiczna"
  ]
  node [
    id 1325
    label "harcerze_starsi"
  ]
  node [
    id 1326
    label "Bund"
  ]
  node [
    id 1327
    label "PPR"
  ]
  node [
    id 1328
    label "wybranek"
  ]
  node [
    id 1329
    label "Jakobici"
  ]
  node [
    id 1330
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 1331
    label "SLD"
  ]
  node [
    id 1332
    label "Razem"
  ]
  node [
    id 1333
    label "PiS"
  ]
  node [
    id 1334
    label "package"
  ]
  node [
    id 1335
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 1336
    label "Kuomintang"
  ]
  node [
    id 1337
    label "ZSL"
  ]
  node [
    id 1338
    label "AWS"
  ]
  node [
    id 1339
    label "gra"
  ]
  node [
    id 1340
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 1341
    label "game"
  ]
  node [
    id 1342
    label "blok"
  ]
  node [
    id 1343
    label "materia&#322;"
  ]
  node [
    id 1344
    label "PO"
  ]
  node [
    id 1345
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 1346
    label "niedoczas"
  ]
  node [
    id 1347
    label "Federali&#347;ci"
  ]
  node [
    id 1348
    label "PSL"
  ]
  node [
    id 1349
    label "Wigowie"
  ]
  node [
    id 1350
    label "ZChN"
  ]
  node [
    id 1351
    label "egzekutywa"
  ]
  node [
    id 1352
    label "aktyw"
  ]
  node [
    id 1353
    label "wybranka"
  ]
  node [
    id 1354
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 1355
    label "unit"
  ]
  node [
    id 1356
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 1357
    label "biom"
  ]
  node [
    id 1358
    label "szata_ro&#347;linna"
  ]
  node [
    id 1359
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1360
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1361
    label "zielono&#347;&#263;"
  ]
  node [
    id 1362
    label "pi&#281;tro"
  ]
  node [
    id 1363
    label "plant"
  ]
  node [
    id 1364
    label "ro&#347;lina"
  ]
  node [
    id 1365
    label "geosystem"
  ]
  node [
    id 1366
    label "teren"
  ]
  node [
    id 1367
    label "inti"
  ]
  node [
    id 1368
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1369
    label "sol"
  ]
  node [
    id 1370
    label "Afryka_Zachodnia"
  ]
  node [
    id 1371
    label "baht"
  ]
  node [
    id 1372
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1373
    label "boliviano"
  ]
  node [
    id 1374
    label "dong"
  ]
  node [
    id 1375
    label "Annam"
  ]
  node [
    id 1376
    label "Tonkin"
  ]
  node [
    id 1377
    label "colon"
  ]
  node [
    id 1378
    label "Ameryka_Centralna"
  ]
  node [
    id 1379
    label "Piemont"
  ]
  node [
    id 1380
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1381
    label "NATO"
  ]
  node [
    id 1382
    label "Italia"
  ]
  node [
    id 1383
    label "Kalabria"
  ]
  node [
    id 1384
    label "Sardynia"
  ]
  node [
    id 1385
    label "Apulia"
  ]
  node [
    id 1386
    label "strefa_euro"
  ]
  node [
    id 1387
    label "Ok&#281;cie"
  ]
  node [
    id 1388
    label "Karyntia"
  ]
  node [
    id 1389
    label "Umbria"
  ]
  node [
    id 1390
    label "Romania"
  ]
  node [
    id 1391
    label "Sycylia"
  ]
  node [
    id 1392
    label "Warszawa"
  ]
  node [
    id 1393
    label "lir"
  ]
  node [
    id 1394
    label "Toskania"
  ]
  node [
    id 1395
    label "Lombardia"
  ]
  node [
    id 1396
    label "Liguria"
  ]
  node [
    id 1397
    label "Kampania"
  ]
  node [
    id 1398
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1399
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1400
    label "Ad&#380;aria"
  ]
  node [
    id 1401
    label "lari"
  ]
  node [
    id 1402
    label "Jukatan"
  ]
  node [
    id 1403
    label "dolar_Belize"
  ]
  node [
    id 1404
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1405
    label "dolar"
  ]
  node [
    id 1406
    label "Ohio"
  ]
  node [
    id 1407
    label "P&#243;&#322;noc"
  ]
  node [
    id 1408
    label "Nowy_York"
  ]
  node [
    id 1409
    label "Illinois"
  ]
  node [
    id 1410
    label "Po&#322;udnie"
  ]
  node [
    id 1411
    label "Kalifornia"
  ]
  node [
    id 1412
    label "Wirginia"
  ]
  node [
    id 1413
    label "Teksas"
  ]
  node [
    id 1414
    label "Waszyngton"
  ]
  node [
    id 1415
    label "zielona_karta"
  ]
  node [
    id 1416
    label "Alaska"
  ]
  node [
    id 1417
    label "Massachusetts"
  ]
  node [
    id 1418
    label "Hawaje"
  ]
  node [
    id 1419
    label "Maryland"
  ]
  node [
    id 1420
    label "Michigan"
  ]
  node [
    id 1421
    label "Arizona"
  ]
  node [
    id 1422
    label "Georgia"
  ]
  node [
    id 1423
    label "stan_wolny"
  ]
  node [
    id 1424
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1425
    label "Pensylwania"
  ]
  node [
    id 1426
    label "Luizjana"
  ]
  node [
    id 1427
    label "Nowy_Meksyk"
  ]
  node [
    id 1428
    label "Wuj_Sam"
  ]
  node [
    id 1429
    label "Alabama"
  ]
  node [
    id 1430
    label "Kansas"
  ]
  node [
    id 1431
    label "Oregon"
  ]
  node [
    id 1432
    label "Zach&#243;d"
  ]
  node [
    id 1433
    label "Oklahoma"
  ]
  node [
    id 1434
    label "Floryda"
  ]
  node [
    id 1435
    label "Hudson"
  ]
  node [
    id 1436
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1437
    label "somoni"
  ]
  node [
    id 1438
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1439
    label "perper"
  ]
  node [
    id 1440
    label "Sand&#380;ak"
  ]
  node [
    id 1441
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1442
    label "euro"
  ]
  node [
    id 1443
    label "Karelia"
  ]
  node [
    id 1444
    label "Mari_El"
  ]
  node [
    id 1445
    label "Inguszetia"
  ]
  node [
    id 1446
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1447
    label "Udmurcja"
  ]
  node [
    id 1448
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1449
    label "Newa"
  ]
  node [
    id 1450
    label "&#321;adoga"
  ]
  node [
    id 1451
    label "Czeczenia"
  ]
  node [
    id 1452
    label "Anadyr"
  ]
  node [
    id 1453
    label "Syberia"
  ]
  node [
    id 1454
    label "Tatarstan"
  ]
  node [
    id 1455
    label "Wszechrosja"
  ]
  node [
    id 1456
    label "Azja"
  ]
  node [
    id 1457
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1458
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1459
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1460
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1461
    label "Europa_Wschodnia"
  ]
  node [
    id 1462
    label "Baszkiria"
  ]
  node [
    id 1463
    label "Kamczatka"
  ]
  node [
    id 1464
    label "Jama&#322;"
  ]
  node [
    id 1465
    label "Dagestan"
  ]
  node [
    id 1466
    label "Witim"
  ]
  node [
    id 1467
    label "Tuwa"
  ]
  node [
    id 1468
    label "car"
  ]
  node [
    id 1469
    label "Komi"
  ]
  node [
    id 1470
    label "Czuwaszja"
  ]
  node [
    id 1471
    label "Chakasja"
  ]
  node [
    id 1472
    label "Perm"
  ]
  node [
    id 1473
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1474
    label "Ajon"
  ]
  node [
    id 1475
    label "Adygeja"
  ]
  node [
    id 1476
    label "Dniepr"
  ]
  node [
    id 1477
    label "rubel_rosyjski"
  ]
  node [
    id 1478
    label "Don"
  ]
  node [
    id 1479
    label "Mordowia"
  ]
  node [
    id 1480
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1481
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1482
    label "gourde"
  ]
  node [
    id 1483
    label "Karaiby"
  ]
  node [
    id 1484
    label "escudo_angolskie"
  ]
  node [
    id 1485
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1486
    label "kwanza"
  ]
  node [
    id 1487
    label "ariary"
  ]
  node [
    id 1488
    label "Ocean_Indyjski"
  ]
  node [
    id 1489
    label "Afryka_Wschodnia"
  ]
  node [
    id 1490
    label "frank_malgaski"
  ]
  node [
    id 1491
    label "Unia_Europejska"
  ]
  node [
    id 1492
    label "Windawa"
  ]
  node [
    id 1493
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1494
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1495
    label "&#379;mud&#378;"
  ]
  node [
    id 1496
    label "lit"
  ]
  node [
    id 1497
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1498
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1499
    label "Synaj"
  ]
  node [
    id 1500
    label "paraszyt"
  ]
  node [
    id 1501
    label "funt_egipski"
  ]
  node [
    id 1502
    label "Amhara"
  ]
  node [
    id 1503
    label "birr"
  ]
  node [
    id 1504
    label "Syjon"
  ]
  node [
    id 1505
    label "negus"
  ]
  node [
    id 1506
    label "peso_kolumbijskie"
  ]
  node [
    id 1507
    label "Orinoko"
  ]
  node [
    id 1508
    label "rial_katarski"
  ]
  node [
    id 1509
    label "dram"
  ]
  node [
    id 1510
    label "Limburgia"
  ]
  node [
    id 1511
    label "gulden"
  ]
  node [
    id 1512
    label "Zelandia"
  ]
  node [
    id 1513
    label "Niderlandy"
  ]
  node [
    id 1514
    label "Brabancja"
  ]
  node [
    id 1515
    label "cedi"
  ]
  node [
    id 1516
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1517
    label "milrejs"
  ]
  node [
    id 1518
    label "cruzado"
  ]
  node [
    id 1519
    label "real"
  ]
  node [
    id 1520
    label "frank_monakijski"
  ]
  node [
    id 1521
    label "Fryburg"
  ]
  node [
    id 1522
    label "Bazylea"
  ]
  node [
    id 1523
    label "Alpy"
  ]
  node [
    id 1524
    label "frank_szwajcarski"
  ]
  node [
    id 1525
    label "Helwecja"
  ]
  node [
    id 1526
    label "Europa_Zachodnia"
  ]
  node [
    id 1527
    label "Berno"
  ]
  node [
    id 1528
    label "Ba&#322;kany"
  ]
  node [
    id 1529
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1530
    label "Naddniestrze"
  ]
  node [
    id 1531
    label "Dniestr"
  ]
  node [
    id 1532
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1533
    label "Gagauzja"
  ]
  node [
    id 1534
    label "Indie_Zachodnie"
  ]
  node [
    id 1535
    label "Sikkim"
  ]
  node [
    id 1536
    label "Asam"
  ]
  node [
    id 1537
    label "Kaszmir"
  ]
  node [
    id 1538
    label "rupia_indyjska"
  ]
  node [
    id 1539
    label "Indie_Portugalskie"
  ]
  node [
    id 1540
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1541
    label "Indie_Wschodnie"
  ]
  node [
    id 1542
    label "Kerala"
  ]
  node [
    id 1543
    label "Bollywood"
  ]
  node [
    id 1544
    label "Pend&#380;ab"
  ]
  node [
    id 1545
    label "boliwar"
  ]
  node [
    id 1546
    label "naira"
  ]
  node [
    id 1547
    label "frank_gwinejski"
  ]
  node [
    id 1548
    label "Karaka&#322;pacja"
  ]
  node [
    id 1549
    label "dolar_liberyjski"
  ]
  node [
    id 1550
    label "Dacja"
  ]
  node [
    id 1551
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1552
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1553
    label "Dobrud&#380;a"
  ]
  node [
    id 1554
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1555
    label "dolar_namibijski"
  ]
  node [
    id 1556
    label "kuna"
  ]
  node [
    id 1557
    label "Rugia"
  ]
  node [
    id 1558
    label "Saksonia"
  ]
  node [
    id 1559
    label "Dolna_Saksonia"
  ]
  node [
    id 1560
    label "Anglosas"
  ]
  node [
    id 1561
    label "Hesja"
  ]
  node [
    id 1562
    label "Szlezwik"
  ]
  node [
    id 1563
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1564
    label "Wirtembergia"
  ]
  node [
    id 1565
    label "Po&#322;abie"
  ]
  node [
    id 1566
    label "Germania"
  ]
  node [
    id 1567
    label "Frankonia"
  ]
  node [
    id 1568
    label "Badenia"
  ]
  node [
    id 1569
    label "Holsztyn"
  ]
  node [
    id 1570
    label "Bawaria"
  ]
  node [
    id 1571
    label "Brandenburgia"
  ]
  node [
    id 1572
    label "Szwabia"
  ]
  node [
    id 1573
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1574
    label "Nadrenia"
  ]
  node [
    id 1575
    label "Westfalia"
  ]
  node [
    id 1576
    label "Turyngia"
  ]
  node [
    id 1577
    label "Helgoland"
  ]
  node [
    id 1578
    label "Karlsbad"
  ]
  node [
    id 1579
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1580
    label "korona_w&#281;gierska"
  ]
  node [
    id 1581
    label "forint"
  ]
  node [
    id 1582
    label "Lipt&#243;w"
  ]
  node [
    id 1583
    label "tenge"
  ]
  node [
    id 1584
    label "szach"
  ]
  node [
    id 1585
    label "Baktria"
  ]
  node [
    id 1586
    label "afgani"
  ]
  node [
    id 1587
    label "kip"
  ]
  node [
    id 1588
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1589
    label "Salzburg"
  ]
  node [
    id 1590
    label "Rakuzy"
  ]
  node [
    id 1591
    label "Dyja"
  ]
  node [
    id 1592
    label "Tyrol"
  ]
  node [
    id 1593
    label "konsulent"
  ]
  node [
    id 1594
    label "szyling_austryjacki"
  ]
  node [
    id 1595
    label "peso_urugwajskie"
  ]
  node [
    id 1596
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1597
    label "korona_esto&#324;ska"
  ]
  node [
    id 1598
    label "Skandynawia"
  ]
  node [
    id 1599
    label "Inflanty"
  ]
  node [
    id 1600
    label "marka_esto&#324;ska"
  ]
  node [
    id 1601
    label "Polinezja"
  ]
  node [
    id 1602
    label "tala"
  ]
  node [
    id 1603
    label "Oceania"
  ]
  node [
    id 1604
    label "Podole"
  ]
  node [
    id 1605
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1606
    label "Wsch&#243;d"
  ]
  node [
    id 1607
    label "Zakarpacie"
  ]
  node [
    id 1608
    label "Naddnieprze"
  ]
  node [
    id 1609
    label "Ma&#322;orosja"
  ]
  node [
    id 1610
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1611
    label "Nadbu&#380;e"
  ]
  node [
    id 1612
    label "hrywna"
  ]
  node [
    id 1613
    label "Zaporo&#380;e"
  ]
  node [
    id 1614
    label "Krym"
  ]
  node [
    id 1615
    label "Przykarpacie"
  ]
  node [
    id 1616
    label "Kozaczyzna"
  ]
  node [
    id 1617
    label "karbowaniec"
  ]
  node [
    id 1618
    label "riel"
  ]
  node [
    id 1619
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1620
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1621
    label "kyat"
  ]
  node [
    id 1622
    label "Arakan"
  ]
  node [
    id 1623
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1624
    label "funt_liba&#324;ski"
  ]
  node [
    id 1625
    label "Mariany"
  ]
  node [
    id 1626
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1627
    label "Maghreb"
  ]
  node [
    id 1628
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1629
    label "dinar_algierski"
  ]
  node [
    id 1630
    label "Kabylia"
  ]
  node [
    id 1631
    label "ringgit"
  ]
  node [
    id 1632
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1633
    label "Borneo"
  ]
  node [
    id 1634
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1635
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1636
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1637
    label "lira_izraelska"
  ]
  node [
    id 1638
    label "szekel"
  ]
  node [
    id 1639
    label "Galilea"
  ]
  node [
    id 1640
    label "Judea"
  ]
  node [
    id 1641
    label "tolar"
  ]
  node [
    id 1642
    label "frank_luksemburski"
  ]
  node [
    id 1643
    label "lempira"
  ]
  node [
    id 1644
    label "Pozna&#324;"
  ]
  node [
    id 1645
    label "lira_malta&#324;ska"
  ]
  node [
    id 1646
    label "Gozo"
  ]
  node [
    id 1647
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1648
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1649
    label "Paros"
  ]
  node [
    id 1650
    label "Epir"
  ]
  node [
    id 1651
    label "panhellenizm"
  ]
  node [
    id 1652
    label "Eubea"
  ]
  node [
    id 1653
    label "Rodos"
  ]
  node [
    id 1654
    label "Achaja"
  ]
  node [
    id 1655
    label "Termopile"
  ]
  node [
    id 1656
    label "Attyka"
  ]
  node [
    id 1657
    label "Hellada"
  ]
  node [
    id 1658
    label "Etolia"
  ]
  node [
    id 1659
    label "palestra"
  ]
  node [
    id 1660
    label "Kreta"
  ]
  node [
    id 1661
    label "drachma"
  ]
  node [
    id 1662
    label "Olimp"
  ]
  node [
    id 1663
    label "Tesalia"
  ]
  node [
    id 1664
    label "Peloponez"
  ]
  node [
    id 1665
    label "Eolia"
  ]
  node [
    id 1666
    label "Beocja"
  ]
  node [
    id 1667
    label "Parnas"
  ]
  node [
    id 1668
    label "Lesbos"
  ]
  node [
    id 1669
    label "Atlantyk"
  ]
  node [
    id 1670
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1671
    label "Ulster"
  ]
  node [
    id 1672
    label "funt_irlandzki"
  ]
  node [
    id 1673
    label "Buriaci"
  ]
  node [
    id 1674
    label "Azja_Wschodnia"
  ]
  node [
    id 1675
    label "tugrik"
  ]
  node [
    id 1676
    label "ajmak"
  ]
  node [
    id 1677
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1678
    label "Lotaryngia"
  ]
  node [
    id 1679
    label "Bordeaux"
  ]
  node [
    id 1680
    label "Pikardia"
  ]
  node [
    id 1681
    label "Masyw_Centralny"
  ]
  node [
    id 1682
    label "Akwitania"
  ]
  node [
    id 1683
    label "Alzacja"
  ]
  node [
    id 1684
    label "Sekwana"
  ]
  node [
    id 1685
    label "Langwedocja"
  ]
  node [
    id 1686
    label "Armagnac"
  ]
  node [
    id 1687
    label "Martynika"
  ]
  node [
    id 1688
    label "Bretania"
  ]
  node [
    id 1689
    label "Sabaudia"
  ]
  node [
    id 1690
    label "Korsyka"
  ]
  node [
    id 1691
    label "Normandia"
  ]
  node [
    id 1692
    label "Gaskonia"
  ]
  node [
    id 1693
    label "Burgundia"
  ]
  node [
    id 1694
    label "frank_francuski"
  ]
  node [
    id 1695
    label "Wandea"
  ]
  node [
    id 1696
    label "Prowansja"
  ]
  node [
    id 1697
    label "Gwadelupa"
  ]
  node [
    id 1698
    label "lew"
  ]
  node [
    id 1699
    label "c&#243;rdoba"
  ]
  node [
    id 1700
    label "dolar_Zimbabwe"
  ]
  node [
    id 1701
    label "frank_rwandyjski"
  ]
  node [
    id 1702
    label "kwacha_zambijska"
  ]
  node [
    id 1703
    label "Kurlandia"
  ]
  node [
    id 1704
    label "&#322;at"
  ]
  node [
    id 1705
    label "Liwonia"
  ]
  node [
    id 1706
    label "rubel_&#322;otewski"
  ]
  node [
    id 1707
    label "Himalaje"
  ]
  node [
    id 1708
    label "rupia_nepalska"
  ]
  node [
    id 1709
    label "funt_suda&#324;ski"
  ]
  node [
    id 1710
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1711
    label "dolar_bahamski"
  ]
  node [
    id 1712
    label "Wielka_Bahama"
  ]
  node [
    id 1713
    label "Mazowsze"
  ]
  node [
    id 1714
    label "Pa&#322;uki"
  ]
  node [
    id 1715
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1716
    label "Powi&#347;le"
  ]
  node [
    id 1717
    label "Wolin"
  ]
  node [
    id 1718
    label "z&#322;oty"
  ]
  node [
    id 1719
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1720
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1721
    label "So&#322;a"
  ]
  node [
    id 1722
    label "Krajna"
  ]
  node [
    id 1723
    label "Opolskie"
  ]
  node [
    id 1724
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1725
    label "Suwalszczyzna"
  ]
  node [
    id 1726
    label "barwy_polskie"
  ]
  node [
    id 1727
    label "Podlasie"
  ]
  node [
    id 1728
    label "Izera"
  ]
  node [
    id 1729
    label "Ma&#322;opolska"
  ]
  node [
    id 1730
    label "Warmia"
  ]
  node [
    id 1731
    label "Mazury"
  ]
  node [
    id 1732
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1733
    label "Kaczawa"
  ]
  node [
    id 1734
    label "Lubelszczyzna"
  ]
  node [
    id 1735
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1736
    label "Kielecczyzna"
  ]
  node [
    id 1737
    label "Lubuskie"
  ]
  node [
    id 1738
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1739
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1740
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1741
    label "Kujawy"
  ]
  node [
    id 1742
    label "Podkarpacie"
  ]
  node [
    id 1743
    label "Wielkopolska"
  ]
  node [
    id 1744
    label "Wis&#322;a"
  ]
  node [
    id 1745
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1746
    label "Bory_Tucholskie"
  ]
  node [
    id 1747
    label "Antyle"
  ]
  node [
    id 1748
    label "dolar_Tuvalu"
  ]
  node [
    id 1749
    label "dinar_iracki"
  ]
  node [
    id 1750
    label "korona_s&#322;owacka"
  ]
  node [
    id 1751
    label "Turiec"
  ]
  node [
    id 1752
    label "jen"
  ]
  node [
    id 1753
    label "jinja"
  ]
  node [
    id 1754
    label "Okinawa"
  ]
  node [
    id 1755
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1756
    label "Japonica"
  ]
  node [
    id 1757
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1758
    label "szyling_kenijski"
  ]
  node [
    id 1759
    label "peso_chilijskie"
  ]
  node [
    id 1760
    label "Zanzibar"
  ]
  node [
    id 1761
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1762
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1763
    label "Cebu"
  ]
  node [
    id 1764
    label "Sahara"
  ]
  node [
    id 1765
    label "Tasmania"
  ]
  node [
    id 1766
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1767
    label "dolar_australijski"
  ]
  node [
    id 1768
    label "Quebec"
  ]
  node [
    id 1769
    label "dolar_kanadyjski"
  ]
  node [
    id 1770
    label "Nowa_Fundlandia"
  ]
  node [
    id 1771
    label "quetzal"
  ]
  node [
    id 1772
    label "Manica"
  ]
  node [
    id 1773
    label "escudo_mozambickie"
  ]
  node [
    id 1774
    label "Cabo_Delgado"
  ]
  node [
    id 1775
    label "Inhambane"
  ]
  node [
    id 1776
    label "Maputo"
  ]
  node [
    id 1777
    label "Gaza"
  ]
  node [
    id 1778
    label "Niasa"
  ]
  node [
    id 1779
    label "Nampula"
  ]
  node [
    id 1780
    label "metical"
  ]
  node [
    id 1781
    label "frank_tunezyjski"
  ]
  node [
    id 1782
    label "dinar_tunezyjski"
  ]
  node [
    id 1783
    label "lud"
  ]
  node [
    id 1784
    label "frank_kongijski"
  ]
  node [
    id 1785
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1786
    label "dinar_Bahrajnu"
  ]
  node [
    id 1787
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1788
    label "escudo_portugalskie"
  ]
  node [
    id 1789
    label "Melanezja"
  ]
  node [
    id 1790
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1791
    label "d&#380;amahirijja"
  ]
  node [
    id 1792
    label "dinar_libijski"
  ]
  node [
    id 1793
    label "balboa"
  ]
  node [
    id 1794
    label "dolar_surinamski"
  ]
  node [
    id 1795
    label "dolar_Brunei"
  ]
  node [
    id 1796
    label "Estremadura"
  ]
  node [
    id 1797
    label "Andaluzja"
  ]
  node [
    id 1798
    label "Kastylia"
  ]
  node [
    id 1799
    label "Galicja"
  ]
  node [
    id 1800
    label "Rzym_Zachodni"
  ]
  node [
    id 1801
    label "Aragonia"
  ]
  node [
    id 1802
    label "hacjender"
  ]
  node [
    id 1803
    label "Asturia"
  ]
  node [
    id 1804
    label "Baskonia"
  ]
  node [
    id 1805
    label "Majorka"
  ]
  node [
    id 1806
    label "Walencja"
  ]
  node [
    id 1807
    label "peseta"
  ]
  node [
    id 1808
    label "Katalonia"
  ]
  node [
    id 1809
    label "Luksemburgia"
  ]
  node [
    id 1810
    label "frank_belgijski"
  ]
  node [
    id 1811
    label "Walonia"
  ]
  node [
    id 1812
    label "Flandria"
  ]
  node [
    id 1813
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1814
    label "dolar_Barbadosu"
  ]
  node [
    id 1815
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1816
    label "korona_czeska"
  ]
  node [
    id 1817
    label "Lasko"
  ]
  node [
    id 1818
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1819
    label "Wojwodina"
  ]
  node [
    id 1820
    label "dinar_serbski"
  ]
  node [
    id 1821
    label "funt_syryjski"
  ]
  node [
    id 1822
    label "alawizm"
  ]
  node [
    id 1823
    label "Szantung"
  ]
  node [
    id 1824
    label "Chiny_Zachodnie"
  ]
  node [
    id 1825
    label "Kuantung"
  ]
  node [
    id 1826
    label "D&#380;ungaria"
  ]
  node [
    id 1827
    label "yuan"
  ]
  node [
    id 1828
    label "Hongkong"
  ]
  node [
    id 1829
    label "Chiny_Wschodnie"
  ]
  node [
    id 1830
    label "Guangdong"
  ]
  node [
    id 1831
    label "Junnan"
  ]
  node [
    id 1832
    label "Mand&#380;uria"
  ]
  node [
    id 1833
    label "Syczuan"
  ]
  node [
    id 1834
    label "zair"
  ]
  node [
    id 1835
    label "Katanga"
  ]
  node [
    id 1836
    label "ugija"
  ]
  node [
    id 1837
    label "dalasi"
  ]
  node [
    id 1838
    label "funt_cypryjski"
  ]
  node [
    id 1839
    label "Afrodyzje"
  ]
  node [
    id 1840
    label "lek"
  ]
  node [
    id 1841
    label "frank_alba&#324;ski"
  ]
  node [
    id 1842
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1843
    label "kafar"
  ]
  node [
    id 1844
    label "dolar_jamajski"
  ]
  node [
    id 1845
    label "Ocean_Spokojny"
  ]
  node [
    id 1846
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1847
    label "som"
  ]
  node [
    id 1848
    label "guarani"
  ]
  node [
    id 1849
    label "rial_ira&#324;ski"
  ]
  node [
    id 1850
    label "mu&#322;&#322;a"
  ]
  node [
    id 1851
    label "Persja"
  ]
  node [
    id 1852
    label "Jawa"
  ]
  node [
    id 1853
    label "Sumatra"
  ]
  node [
    id 1854
    label "rupia_indonezyjska"
  ]
  node [
    id 1855
    label "Nowa_Gwinea"
  ]
  node [
    id 1856
    label "Moluki"
  ]
  node [
    id 1857
    label "szyling_somalijski"
  ]
  node [
    id 1858
    label "szyling_ugandyjski"
  ]
  node [
    id 1859
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1860
    label "Ujgur"
  ]
  node [
    id 1861
    label "Azja_Mniejsza"
  ]
  node [
    id 1862
    label "lira_turecka"
  ]
  node [
    id 1863
    label "Pireneje"
  ]
  node [
    id 1864
    label "nakfa"
  ]
  node [
    id 1865
    label "won"
  ]
  node [
    id 1866
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1867
    label "&#346;wite&#378;"
  ]
  node [
    id 1868
    label "dinar_kuwejcki"
  ]
  node [
    id 1869
    label "Nachiczewan"
  ]
  node [
    id 1870
    label "manat_azerski"
  ]
  node [
    id 1871
    label "Karabach"
  ]
  node [
    id 1872
    label "dolar_Kiribati"
  ]
  node [
    id 1873
    label "Anglia"
  ]
  node [
    id 1874
    label "Amazonia"
  ]
  node [
    id 1875
    label "plantowa&#263;"
  ]
  node [
    id 1876
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1877
    label "zapadnia"
  ]
  node [
    id 1878
    label "Zamojszczyzna"
  ]
  node [
    id 1879
    label "budynek"
  ]
  node [
    id 1880
    label "skorupa_ziemska"
  ]
  node [
    id 1881
    label "Turkiestan"
  ]
  node [
    id 1882
    label "Noworosja"
  ]
  node [
    id 1883
    label "Mezoameryka"
  ]
  node [
    id 1884
    label "glinowanie"
  ]
  node [
    id 1885
    label "Kurdystan"
  ]
  node [
    id 1886
    label "martwica"
  ]
  node [
    id 1887
    label "Szkocja"
  ]
  node [
    id 1888
    label "litosfera"
  ]
  node [
    id 1889
    label "penetrator"
  ]
  node [
    id 1890
    label "glinowa&#263;"
  ]
  node [
    id 1891
    label "Zabajkale"
  ]
  node [
    id 1892
    label "domain"
  ]
  node [
    id 1893
    label "Bojkowszczyzna"
  ]
  node [
    id 1894
    label "podglebie"
  ]
  node [
    id 1895
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1896
    label "Pamir"
  ]
  node [
    id 1897
    label "Indochiny"
  ]
  node [
    id 1898
    label "Kurpie"
  ]
  node [
    id 1899
    label "S&#261;decczyzna"
  ]
  node [
    id 1900
    label "kort"
  ]
  node [
    id 1901
    label "czynnik_produkcji"
  ]
  node [
    id 1902
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1903
    label "Huculszczyzna"
  ]
  node [
    id 1904
    label "pojazd"
  ]
  node [
    id 1905
    label "powierzchnia"
  ]
  node [
    id 1906
    label "Podhale"
  ]
  node [
    id 1907
    label "pr&#243;chnica"
  ]
  node [
    id 1908
    label "Hercegowina"
  ]
  node [
    id 1909
    label "Walia"
  ]
  node [
    id 1910
    label "pomieszczenie"
  ]
  node [
    id 1911
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1912
    label "ryzosfera"
  ]
  node [
    id 1913
    label "Kaukaz"
  ]
  node [
    id 1914
    label "Biskupizna"
  ]
  node [
    id 1915
    label "Bo&#347;nia"
  ]
  node [
    id 1916
    label "dotleni&#263;"
  ]
  node [
    id 1917
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1918
    label "Podbeskidzie"
  ]
  node [
    id 1919
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1920
    label "Opolszczyzna"
  ]
  node [
    id 1921
    label "Kaszuby"
  ]
  node [
    id 1922
    label "Ko&#322;yma"
  ]
  node [
    id 1923
    label "glej"
  ]
  node [
    id 1924
    label "posadzka"
  ]
  node [
    id 1925
    label "Polesie"
  ]
  node [
    id 1926
    label "Palestyna"
  ]
  node [
    id 1927
    label "Lauda"
  ]
  node [
    id 1928
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1929
    label "Laponia"
  ]
  node [
    id 1930
    label "Yorkshire"
  ]
  node [
    id 1931
    label "Zag&#243;rze"
  ]
  node [
    id 1932
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1933
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1934
    label "Oksytania"
  ]
  node [
    id 1935
    label "Kociewie"
  ]
  node [
    id 1936
    label "divide"
  ]
  node [
    id 1937
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1938
    label "exchange"
  ]
  node [
    id 1939
    label "przyzna&#263;"
  ]
  node [
    id 1940
    label "change"
  ]
  node [
    id 1941
    label "rozda&#263;"
  ]
  node [
    id 1942
    label "policzy&#263;"
  ]
  node [
    id 1943
    label "distribute"
  ]
  node [
    id 1944
    label "zrobi&#263;"
  ]
  node [
    id 1945
    label "wydzieli&#263;"
  ]
  node [
    id 1946
    label "transgress"
  ]
  node [
    id 1947
    label "pigeonhole"
  ]
  node [
    id 1948
    label "sk&#322;&#243;ci&#263;"
  ]
  node [
    id 1949
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 1950
    label "impart"
  ]
  node [
    id 1951
    label "powa&#347;ni&#263;"
  ]
  node [
    id 1952
    label "zm&#261;ci&#263;"
  ]
  node [
    id 1953
    label "roztrzepa&#263;"
  ]
  node [
    id 1954
    label "nada&#263;"
  ]
  node [
    id 1955
    label "give"
  ]
  node [
    id 1956
    label "pozwoli&#263;"
  ]
  node [
    id 1957
    label "stwierdzi&#263;"
  ]
  node [
    id 1958
    label "wyrachowa&#263;"
  ]
  node [
    id 1959
    label "wyceni&#263;"
  ]
  node [
    id 1960
    label "wzi&#261;&#263;"
  ]
  node [
    id 1961
    label "wynagrodzenie"
  ]
  node [
    id 1962
    label "okre&#347;li&#263;"
  ]
  node [
    id 1963
    label "charge"
  ]
  node [
    id 1964
    label "zakwalifikowa&#263;"
  ]
  node [
    id 1965
    label "wyznaczy&#263;"
  ]
  node [
    id 1966
    label "rozdzieli&#263;"
  ]
  node [
    id 1967
    label "allocate"
  ]
  node [
    id 1968
    label "oddzieli&#263;"
  ]
  node [
    id 1969
    label "przydzieli&#263;"
  ]
  node [
    id 1970
    label "wykroi&#263;"
  ]
  node [
    id 1971
    label "evolve"
  ]
  node [
    id 1972
    label "wytworzy&#263;"
  ]
  node [
    id 1973
    label "signalize"
  ]
  node [
    id 1974
    label "act"
  ]
  node [
    id 1975
    label "post&#261;pi&#263;"
  ]
  node [
    id 1976
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1977
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1978
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1979
    label "zorganizowa&#263;"
  ]
  node [
    id 1980
    label "appoint"
  ]
  node [
    id 1981
    label "wystylizowa&#263;"
  ]
  node [
    id 1982
    label "cause"
  ]
  node [
    id 1983
    label "przerobi&#263;"
  ]
  node [
    id 1984
    label "nabra&#263;"
  ]
  node [
    id 1985
    label "make"
  ]
  node [
    id 1986
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1987
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1988
    label "wydali&#263;"
  ]
  node [
    id 1989
    label "Chrystus"
  ]
  node [
    id 1990
    label "Kazimierz"
  ]
  node [
    id 1991
    label "po&#322;udniowy"
  ]
  node [
    id 1992
    label "sony"
  ]
  node [
    id 1993
    label "ericsson"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 274
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 578
  ]
  edge [
    source 15
    target 579
  ]
  edge [
    source 15
    target 580
  ]
  edge [
    source 15
    target 581
  ]
  edge [
    source 15
    target 582
  ]
  edge [
    source 15
    target 583
  ]
  edge [
    source 15
    target 584
  ]
  edge [
    source 15
    target 585
  ]
  edge [
    source 15
    target 586
  ]
  edge [
    source 15
    target 587
  ]
  edge [
    source 15
    target 588
  ]
  edge [
    source 15
    target 589
  ]
  edge [
    source 15
    target 590
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 231
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 251
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 100
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 101
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 103
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 1989
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 517
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 518
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 544
  ]
  edge [
    source 16
    target 522
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 346
  ]
  edge [
    source 16
    target 520
  ]
  edge [
    source 16
    target 521
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 519
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 18
    target 750
  ]
  edge [
    source 18
    target 751
  ]
  edge [
    source 18
    target 752
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 754
  ]
  edge [
    source 18
    target 755
  ]
  edge [
    source 18
    target 756
  ]
  edge [
    source 18
    target 757
  ]
  edge [
    source 18
    target 758
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 759
  ]
  edge [
    source 18
    target 760
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 762
  ]
  edge [
    source 18
    target 763
  ]
  edge [
    source 18
    target 764
  ]
  edge [
    source 18
    target 765
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 766
  ]
  edge [
    source 18
    target 767
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 768
  ]
  edge [
    source 19
    target 769
  ]
  edge [
    source 19
    target 352
  ]
  edge [
    source 19
    target 770
  ]
  edge [
    source 19
    target 771
  ]
  edge [
    source 19
    target 772
  ]
  edge [
    source 19
    target 773
  ]
  edge [
    source 19
    target 774
  ]
  edge [
    source 19
    target 519
  ]
  edge [
    source 19
    target 775
  ]
  edge [
    source 19
    target 776
  ]
  edge [
    source 19
    target 777
  ]
  edge [
    source 19
    target 778
  ]
  edge [
    source 19
    target 779
  ]
  edge [
    source 19
    target 780
  ]
  edge [
    source 19
    target 781
  ]
  edge [
    source 19
    target 782
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 623
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 739
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 795
  ]
  edge [
    source 19
    target 133
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 797
  ]
  edge [
    source 19
    target 527
  ]
  edge [
    source 19
    target 798
  ]
  edge [
    source 19
    target 799
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 801
  ]
  edge [
    source 19
    target 802
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 804
  ]
  edge [
    source 19
    target 524
  ]
  edge [
    source 19
    target 525
  ]
  edge [
    source 19
    target 526
  ]
  edge [
    source 19
    target 528
  ]
  edge [
    source 19
    target 529
  ]
  edge [
    source 19
    target 530
  ]
  edge [
    source 19
    target 531
  ]
  edge [
    source 19
    target 532
  ]
  edge [
    source 19
    target 517
  ]
  edge [
    source 19
    target 533
  ]
  edge [
    source 19
    target 534
  ]
  edge [
    source 19
    target 535
  ]
  edge [
    source 19
    target 536
  ]
  edge [
    source 19
    target 537
  ]
  edge [
    source 19
    target 538
  ]
  edge [
    source 19
    target 539
  ]
  edge [
    source 19
    target 540
  ]
  edge [
    source 19
    target 541
  ]
  edge [
    source 19
    target 542
  ]
  edge [
    source 19
    target 543
  ]
  edge [
    source 19
    target 805
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 812
  ]
  edge [
    source 19
    target 813
  ]
  edge [
    source 19
    target 814
  ]
  edge [
    source 19
    target 815
  ]
  edge [
    source 19
    target 816
  ]
  edge [
    source 19
    target 817
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 19
    target 829
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 136
  ]
  edge [
    source 19
    target 831
  ]
  edge [
    source 19
    target 832
  ]
  edge [
    source 19
    target 833
  ]
  edge [
    source 19
    target 834
  ]
  edge [
    source 19
    target 835
  ]
  edge [
    source 19
    target 836
  ]
  edge [
    source 19
    target 837
  ]
  edge [
    source 19
    target 838
  ]
  edge [
    source 19
    target 839
  ]
  edge [
    source 19
    target 840
  ]
  edge [
    source 19
    target 841
  ]
  edge [
    source 19
    target 842
  ]
  edge [
    source 19
    target 843
  ]
  edge [
    source 19
    target 844
  ]
  edge [
    source 19
    target 330
  ]
  edge [
    source 19
    target 845
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 846
  ]
  edge [
    source 20
    target 847
  ]
  edge [
    source 20
    target 848
  ]
  edge [
    source 20
    target 849
  ]
  edge [
    source 20
    target 850
  ]
  edge [
    source 20
    target 851
  ]
  edge [
    source 20
    target 852
  ]
  edge [
    source 20
    target 853
  ]
  edge [
    source 20
    target 854
  ]
  edge [
    source 20
    target 855
  ]
  edge [
    source 20
    target 856
  ]
  edge [
    source 20
    target 857
  ]
  edge [
    source 20
    target 858
  ]
  edge [
    source 20
    target 859
  ]
  edge [
    source 20
    target 860
  ]
  edge [
    source 20
    target 861
  ]
  edge [
    source 20
    target 862
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 863
  ]
  edge [
    source 20
    target 864
  ]
  edge [
    source 20
    target 865
  ]
  edge [
    source 20
    target 866
  ]
  edge [
    source 20
    target 867
  ]
  edge [
    source 20
    target 868
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 869
  ]
  edge [
    source 20
    target 870
  ]
  edge [
    source 20
    target 871
  ]
  edge [
    source 20
    target 872
  ]
  edge [
    source 20
    target 873
  ]
  edge [
    source 20
    target 874
  ]
  edge [
    source 20
    target 875
  ]
  edge [
    source 20
    target 876
  ]
  edge [
    source 20
    target 877
  ]
  edge [
    source 20
    target 878
  ]
  edge [
    source 20
    target 879
  ]
  edge [
    source 20
    target 880
  ]
  edge [
    source 20
    target 881
  ]
  edge [
    source 20
    target 882
  ]
  edge [
    source 20
    target 348
  ]
  edge [
    source 20
    target 883
  ]
  edge [
    source 20
    target 884
  ]
  edge [
    source 20
    target 885
  ]
  edge [
    source 20
    target 886
  ]
  edge [
    source 20
    target 887
  ]
  edge [
    source 20
    target 888
  ]
  edge [
    source 20
    target 151
  ]
  edge [
    source 20
    target 889
  ]
  edge [
    source 20
    target 890
  ]
  edge [
    source 20
    target 891
  ]
  edge [
    source 20
    target 892
  ]
  edge [
    source 20
    target 893
  ]
  edge [
    source 20
    target 894
  ]
  edge [
    source 20
    target 895
  ]
  edge [
    source 20
    target 896
  ]
  edge [
    source 20
    target 897
  ]
  edge [
    source 20
    target 898
  ]
  edge [
    source 20
    target 899
  ]
  edge [
    source 20
    target 900
  ]
  edge [
    source 20
    target 901
  ]
  edge [
    source 20
    target 902
  ]
  edge [
    source 20
    target 903
  ]
  edge [
    source 20
    target 904
  ]
  edge [
    source 20
    target 905
  ]
  edge [
    source 20
    target 906
  ]
  edge [
    source 20
    target 907
  ]
  edge [
    source 20
    target 908
  ]
  edge [
    source 20
    target 909
  ]
  edge [
    source 20
    target 910
  ]
  edge [
    source 20
    target 911
  ]
  edge [
    source 20
    target 912
  ]
  edge [
    source 20
    target 913
  ]
  edge [
    source 20
    target 914
  ]
  edge [
    source 20
    target 915
  ]
  edge [
    source 20
    target 916
  ]
  edge [
    source 20
    target 917
  ]
  edge [
    source 20
    target 918
  ]
  edge [
    source 20
    target 919
  ]
  edge [
    source 20
    target 920
  ]
  edge [
    source 20
    target 921
  ]
  edge [
    source 20
    target 922
  ]
  edge [
    source 20
    target 923
  ]
  edge [
    source 20
    target 924
  ]
  edge [
    source 20
    target 925
  ]
  edge [
    source 20
    target 299
  ]
  edge [
    source 20
    target 926
  ]
  edge [
    source 20
    target 927
  ]
  edge [
    source 20
    target 928
  ]
  edge [
    source 20
    target 929
  ]
  edge [
    source 20
    target 930
  ]
  edge [
    source 20
    target 931
  ]
  edge [
    source 20
    target 932
  ]
  edge [
    source 20
    target 933
  ]
  edge [
    source 20
    target 934
  ]
  edge [
    source 20
    target 935
  ]
  edge [
    source 20
    target 936
  ]
  edge [
    source 20
    target 937
  ]
  edge [
    source 20
    target 297
  ]
  edge [
    source 20
    target 938
  ]
  edge [
    source 20
    target 939
  ]
  edge [
    source 20
    target 81
  ]
  edge [
    source 20
    target 940
  ]
  edge [
    source 20
    target 941
  ]
  edge [
    source 20
    target 942
  ]
  edge [
    source 20
    target 943
  ]
  edge [
    source 20
    target 944
  ]
  edge [
    source 20
    target 800
  ]
  edge [
    source 20
    target 364
  ]
  edge [
    source 20
    target 284
  ]
  edge [
    source 20
    target 945
  ]
  edge [
    source 20
    target 946
  ]
  edge [
    source 20
    target 947
  ]
  edge [
    source 20
    target 325
  ]
  edge [
    source 20
    target 948
  ]
  edge [
    source 20
    target 949
  ]
  edge [
    source 20
    target 950
  ]
  edge [
    source 20
    target 951
  ]
  edge [
    source 20
    target 356
  ]
  edge [
    source 20
    target 952
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 954
  ]
  edge [
    source 20
    target 955
  ]
  edge [
    source 20
    target 956
  ]
  edge [
    source 20
    target 957
  ]
  edge [
    source 20
    target 958
  ]
  edge [
    source 20
    target 959
  ]
  edge [
    source 20
    target 960
  ]
  edge [
    source 20
    target 961
  ]
  edge [
    source 20
    target 56
  ]
  edge [
    source 20
    target 962
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 963
  ]
  edge [
    source 21
    target 964
  ]
  edge [
    source 21
    target 965
  ]
  edge [
    source 21
    target 966
  ]
  edge [
    source 21
    target 967
  ]
  edge [
    source 21
    target 968
  ]
  edge [
    source 21
    target 969
  ]
  edge [
    source 21
    target 970
  ]
  edge [
    source 21
    target 971
  ]
  edge [
    source 21
    target 972
  ]
  edge [
    source 21
    target 973
  ]
  edge [
    source 21
    target 974
  ]
  edge [
    source 21
    target 975
  ]
  edge [
    source 21
    target 976
  ]
  edge [
    source 21
    target 796
  ]
  edge [
    source 21
    target 977
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 979
  ]
  edge [
    source 21
    target 980
  ]
  edge [
    source 21
    target 740
  ]
  edge [
    source 21
    target 981
  ]
  edge [
    source 21
    target 820
  ]
  edge [
    source 21
    target 982
  ]
  edge [
    source 21
    target 983
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 21
    target 985
  ]
  edge [
    source 21
    target 141
  ]
  edge [
    source 21
    target 986
  ]
  edge [
    source 21
    target 987
  ]
  edge [
    source 21
    target 988
  ]
  edge [
    source 21
    target 989
  ]
  edge [
    source 21
    target 990
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 991
  ]
  edge [
    source 21
    target 992
  ]
  edge [
    source 21
    target 993
  ]
  edge [
    source 21
    target 994
  ]
  edge [
    source 21
    target 995
  ]
  edge [
    source 21
    target 996
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 997
  ]
  edge [
    source 22
    target 998
  ]
  edge [
    source 22
    target 623
  ]
  edge [
    source 22
    target 999
  ]
  edge [
    source 22
    target 1000
  ]
  edge [
    source 22
    target 1001
  ]
  edge [
    source 22
    target 1002
  ]
  edge [
    source 22
    target 1003
  ]
  edge [
    source 22
    target 1004
  ]
  edge [
    source 22
    target 1005
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 734
  ]
  edge [
    source 23
    target 735
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 736
  ]
  edge [
    source 23
    target 1006
  ]
  edge [
    source 23
    target 1007
  ]
  edge [
    source 23
    target 1008
  ]
  edge [
    source 23
    target 1009
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 1011
  ]
  edge [
    source 23
    target 1012
  ]
  edge [
    source 23
    target 740
  ]
  edge [
    source 23
    target 1013
  ]
  edge [
    source 23
    target 744
  ]
  edge [
    source 23
    target 1014
  ]
  edge [
    source 23
    target 1015
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1022
  ]
  edge [
    source 23
    target 727
  ]
  edge [
    source 23
    target 728
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 730
  ]
  edge [
    source 23
    target 1023
  ]
  edge [
    source 23
    target 1024
  ]
  edge [
    source 23
    target 356
  ]
  edge [
    source 23
    target 325
  ]
  edge [
    source 23
    target 1025
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 1027
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 1029
  ]
  edge [
    source 23
    target 1030
  ]
  edge [
    source 23
    target 1031
  ]
  edge [
    source 23
    target 1032
  ]
  edge [
    source 23
    target 1033
  ]
  edge [
    source 23
    target 1034
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 822
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1990
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1045
  ]
  edge [
    source 27
    target 1046
  ]
  edge [
    source 27
    target 1047
  ]
  edge [
    source 27
    target 1048
  ]
  edge [
    source 27
    target 1049
  ]
  edge [
    source 27
    target 1050
  ]
  edge [
    source 27
    target 1051
  ]
  edge [
    source 27
    target 1052
  ]
  edge [
    source 27
    target 1053
  ]
  edge [
    source 27
    target 1054
  ]
  edge [
    source 27
    target 1055
  ]
  edge [
    source 27
    target 1056
  ]
  edge [
    source 27
    target 1057
  ]
  edge [
    source 27
    target 1058
  ]
  edge [
    source 27
    target 1059
  ]
  edge [
    source 27
    target 1060
  ]
  edge [
    source 27
    target 1061
  ]
  edge [
    source 27
    target 1062
  ]
  edge [
    source 27
    target 1063
  ]
  edge [
    source 27
    target 1064
  ]
  edge [
    source 27
    target 1065
  ]
  edge [
    source 27
    target 1066
  ]
  edge [
    source 27
    target 60
  ]
  edge [
    source 27
    target 1067
  ]
  edge [
    source 27
    target 1068
  ]
  edge [
    source 27
    target 1069
  ]
  edge [
    source 27
    target 1070
  ]
  edge [
    source 27
    target 1071
  ]
  edge [
    source 27
    target 1072
  ]
  edge [
    source 27
    target 1073
  ]
  edge [
    source 27
    target 1074
  ]
  edge [
    source 27
    target 1075
  ]
  edge [
    source 27
    target 1076
  ]
  edge [
    source 27
    target 1077
  ]
  edge [
    source 27
    target 1078
  ]
  edge [
    source 27
    target 1079
  ]
  edge [
    source 27
    target 1080
  ]
  edge [
    source 27
    target 1081
  ]
  edge [
    source 27
    target 1082
  ]
  edge [
    source 27
    target 1083
  ]
  edge [
    source 27
    target 1084
  ]
  edge [
    source 27
    target 1085
  ]
  edge [
    source 27
    target 1086
  ]
  edge [
    source 27
    target 1087
  ]
  edge [
    source 27
    target 1088
  ]
  edge [
    source 27
    target 1089
  ]
  edge [
    source 27
    target 1090
  ]
  edge [
    source 27
    target 1091
  ]
  edge [
    source 27
    target 1092
  ]
  edge [
    source 27
    target 1093
  ]
  edge [
    source 27
    target 1094
  ]
  edge [
    source 27
    target 1095
  ]
  edge [
    source 27
    target 1096
  ]
  edge [
    source 27
    target 1097
  ]
  edge [
    source 27
    target 1098
  ]
  edge [
    source 27
    target 1099
  ]
  edge [
    source 27
    target 1100
  ]
  edge [
    source 27
    target 1101
  ]
  edge [
    source 27
    target 1102
  ]
  edge [
    source 27
    target 1103
  ]
  edge [
    source 27
    target 1104
  ]
  edge [
    source 27
    target 1105
  ]
  edge [
    source 27
    target 1106
  ]
  edge [
    source 27
    target 1107
  ]
  edge [
    source 27
    target 1108
  ]
  edge [
    source 27
    target 1109
  ]
  edge [
    source 27
    target 1110
  ]
  edge [
    source 27
    target 1111
  ]
  edge [
    source 27
    target 1112
  ]
  edge [
    source 27
    target 1113
  ]
  edge [
    source 27
    target 1015
  ]
  edge [
    source 27
    target 1114
  ]
  edge [
    source 27
    target 41
  ]
  edge [
    source 27
    target 1115
  ]
  edge [
    source 27
    target 1116
  ]
  edge [
    source 27
    target 1117
  ]
  edge [
    source 27
    target 1118
  ]
  edge [
    source 27
    target 1119
  ]
  edge [
    source 27
    target 1120
  ]
  edge [
    source 27
    target 1121
  ]
  edge [
    source 27
    target 1122
  ]
  edge [
    source 27
    target 1123
  ]
  edge [
    source 27
    target 1124
  ]
  edge [
    source 27
    target 1125
  ]
  edge [
    source 27
    target 1126
  ]
  edge [
    source 27
    target 1127
  ]
  edge [
    source 27
    target 1128
  ]
  edge [
    source 27
    target 1129
  ]
  edge [
    source 27
    target 1130
  ]
  edge [
    source 27
    target 1131
  ]
  edge [
    source 27
    target 1132
  ]
  edge [
    source 27
    target 1133
  ]
  edge [
    source 27
    target 1134
  ]
  edge [
    source 27
    target 1135
  ]
  edge [
    source 27
    target 1136
  ]
  edge [
    source 27
    target 1137
  ]
  edge [
    source 27
    target 1138
  ]
  edge [
    source 27
    target 1139
  ]
  edge [
    source 27
    target 1140
  ]
  edge [
    source 27
    target 1141
  ]
  edge [
    source 27
    target 1142
  ]
  edge [
    source 27
    target 1143
  ]
  edge [
    source 27
    target 1144
  ]
  edge [
    source 27
    target 1145
  ]
  edge [
    source 27
    target 1146
  ]
  edge [
    source 27
    target 1147
  ]
  edge [
    source 27
    target 1148
  ]
  edge [
    source 27
    target 1149
  ]
  edge [
    source 27
    target 1150
  ]
  edge [
    source 27
    target 1151
  ]
  edge [
    source 27
    target 1152
  ]
  edge [
    source 27
    target 1153
  ]
  edge [
    source 27
    target 1154
  ]
  edge [
    source 27
    target 1155
  ]
  edge [
    source 27
    target 1156
  ]
  edge [
    source 27
    target 1157
  ]
  edge [
    source 27
    target 1158
  ]
  edge [
    source 27
    target 1159
  ]
  edge [
    source 27
    target 1160
  ]
  edge [
    source 27
    target 1161
  ]
  edge [
    source 27
    target 1162
  ]
  edge [
    source 27
    target 1163
  ]
  edge [
    source 27
    target 1164
  ]
  edge [
    source 27
    target 1165
  ]
  edge [
    source 27
    target 1166
  ]
  edge [
    source 27
    target 1167
  ]
  edge [
    source 27
    target 1168
  ]
  edge [
    source 27
    target 1169
  ]
  edge [
    source 27
    target 1170
  ]
  edge [
    source 27
    target 1171
  ]
  edge [
    source 27
    target 1172
  ]
  edge [
    source 27
    target 1173
  ]
  edge [
    source 27
    target 1174
  ]
  edge [
    source 27
    target 1175
  ]
  edge [
    source 27
    target 1176
  ]
  edge [
    source 27
    target 1177
  ]
  edge [
    source 27
    target 1178
  ]
  edge [
    source 27
    target 1179
  ]
  edge [
    source 27
    target 1180
  ]
  edge [
    source 27
    target 1181
  ]
  edge [
    source 27
    target 1182
  ]
  edge [
    source 27
    target 1183
  ]
  edge [
    source 27
    target 1184
  ]
  edge [
    source 27
    target 1185
  ]
  edge [
    source 27
    target 1186
  ]
  edge [
    source 27
    target 1187
  ]
  edge [
    source 27
    target 1188
  ]
  edge [
    source 27
    target 1189
  ]
  edge [
    source 27
    target 1190
  ]
  edge [
    source 27
    target 1191
  ]
  edge [
    source 27
    target 1192
  ]
  edge [
    source 27
    target 1193
  ]
  edge [
    source 27
    target 1194
  ]
  edge [
    source 27
    target 1195
  ]
  edge [
    source 27
    target 1196
  ]
  edge [
    source 27
    target 1197
  ]
  edge [
    source 27
    target 1198
  ]
  edge [
    source 27
    target 1199
  ]
  edge [
    source 27
    target 1200
  ]
  edge [
    source 27
    target 1201
  ]
  edge [
    source 27
    target 1202
  ]
  edge [
    source 27
    target 1203
  ]
  edge [
    source 27
    target 1204
  ]
  edge [
    source 27
    target 1205
  ]
  edge [
    source 27
    target 1206
  ]
  edge [
    source 27
    target 1207
  ]
  edge [
    source 27
    target 1208
  ]
  edge [
    source 27
    target 1209
  ]
  edge [
    source 27
    target 1210
  ]
  edge [
    source 27
    target 1211
  ]
  edge [
    source 27
    target 1212
  ]
  edge [
    source 27
    target 1213
  ]
  edge [
    source 27
    target 1214
  ]
  edge [
    source 27
    target 1215
  ]
  edge [
    source 27
    target 1216
  ]
  edge [
    source 27
    target 1217
  ]
  edge [
    source 27
    target 1218
  ]
  edge [
    source 27
    target 1219
  ]
  edge [
    source 27
    target 1220
  ]
  edge [
    source 27
    target 1221
  ]
  edge [
    source 27
    target 1222
  ]
  edge [
    source 27
    target 1223
  ]
  edge [
    source 27
    target 1224
  ]
  edge [
    source 27
    target 1225
  ]
  edge [
    source 27
    target 1226
  ]
  edge [
    source 27
    target 321
  ]
  edge [
    source 27
    target 1227
  ]
  edge [
    source 27
    target 1228
  ]
  edge [
    source 27
    target 1229
  ]
  edge [
    source 27
    target 1230
  ]
  edge [
    source 27
    target 1231
  ]
  edge [
    source 27
    target 1232
  ]
  edge [
    source 27
    target 1233
  ]
  edge [
    source 27
    target 1234
  ]
  edge [
    source 27
    target 1235
  ]
  edge [
    source 27
    target 1236
  ]
  edge [
    source 27
    target 1237
  ]
  edge [
    source 27
    target 1238
  ]
  edge [
    source 27
    target 1239
  ]
  edge [
    source 27
    target 1240
  ]
  edge [
    source 27
    target 1241
  ]
  edge [
    source 27
    target 1242
  ]
  edge [
    source 27
    target 1243
  ]
  edge [
    source 27
    target 1244
  ]
  edge [
    source 27
    target 1245
  ]
  edge [
    source 27
    target 1246
  ]
  edge [
    source 27
    target 1247
  ]
  edge [
    source 27
    target 1248
  ]
  edge [
    source 27
    target 1249
  ]
  edge [
    source 27
    target 1250
  ]
  edge [
    source 27
    target 1251
  ]
  edge [
    source 27
    target 1252
  ]
  edge [
    source 27
    target 1253
  ]
  edge [
    source 27
    target 1254
  ]
  edge [
    source 27
    target 1255
  ]
  edge [
    source 27
    target 1256
  ]
  edge [
    source 27
    target 1257
  ]
  edge [
    source 27
    target 1258
  ]
  edge [
    source 27
    target 1259
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 1261
  ]
  edge [
    source 27
    target 1262
  ]
  edge [
    source 27
    target 1263
  ]
  edge [
    source 27
    target 1264
  ]
  edge [
    source 27
    target 63
  ]
  edge [
    source 27
    target 1265
  ]
  edge [
    source 27
    target 1266
  ]
  edge [
    source 27
    target 640
  ]
  edge [
    source 27
    target 1267
  ]
  edge [
    source 27
    target 1268
  ]
  edge [
    source 27
    target 1269
  ]
  edge [
    source 27
    target 61
  ]
  edge [
    source 27
    target 1270
  ]
  edge [
    source 27
    target 1271
  ]
  edge [
    source 27
    target 1272
  ]
  edge [
    source 27
    target 1273
  ]
  edge [
    source 27
    target 1274
  ]
  edge [
    source 27
    target 1275
  ]
  edge [
    source 27
    target 1276
  ]
  edge [
    source 27
    target 1277
  ]
  edge [
    source 27
    target 1278
  ]
  edge [
    source 27
    target 1279
  ]
  edge [
    source 27
    target 1280
  ]
  edge [
    source 27
    target 1281
  ]
  edge [
    source 27
    target 185
  ]
  edge [
    source 27
    target 1282
  ]
  edge [
    source 27
    target 1283
  ]
  edge [
    source 27
    target 1284
  ]
  edge [
    source 27
    target 1285
  ]
  edge [
    source 27
    target 1286
  ]
  edge [
    source 27
    target 1287
  ]
  edge [
    source 27
    target 1288
  ]
  edge [
    source 27
    target 1289
  ]
  edge [
    source 27
    target 1290
  ]
  edge [
    source 27
    target 1291
  ]
  edge [
    source 27
    target 1292
  ]
  edge [
    source 27
    target 1293
  ]
  edge [
    source 27
    target 1294
  ]
  edge [
    source 27
    target 1295
  ]
  edge [
    source 27
    target 1296
  ]
  edge [
    source 27
    target 1297
  ]
  edge [
    source 27
    target 409
  ]
  edge [
    source 27
    target 285
  ]
  edge [
    source 27
    target 1298
  ]
  edge [
    source 27
    target 167
  ]
  edge [
    source 27
    target 1299
  ]
  edge [
    source 27
    target 1300
  ]
  edge [
    source 27
    target 1301
  ]
  edge [
    source 27
    target 1302
  ]
  edge [
    source 27
    target 1303
  ]
  edge [
    source 27
    target 357
  ]
  edge [
    source 27
    target 203
  ]
  edge [
    source 27
    target 1304
  ]
  edge [
    source 27
    target 1305
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 27
    target 1306
  ]
  edge [
    source 27
    target 1307
  ]
  edge [
    source 27
    target 97
  ]
  edge [
    source 27
    target 1308
  ]
  edge [
    source 27
    target 85
  ]
  edge [
    source 27
    target 1309
  ]
  edge [
    source 27
    target 554
  ]
  edge [
    source 27
    target 1310
  ]
  edge [
    source 27
    target 1311
  ]
  edge [
    source 27
    target 1312
  ]
  edge [
    source 27
    target 93
  ]
  edge [
    source 27
    target 1313
  ]
  edge [
    source 27
    target 1314
  ]
  edge [
    source 27
    target 1315
  ]
  edge [
    source 27
    target 1316
  ]
  edge [
    source 27
    target 1317
  ]
  edge [
    source 27
    target 1318
  ]
  edge [
    source 27
    target 1319
  ]
  edge [
    source 27
    target 1320
  ]
  edge [
    source 27
    target 1321
  ]
  edge [
    source 27
    target 1322
  ]
  edge [
    source 27
    target 1323
  ]
  edge [
    source 27
    target 1324
  ]
  edge [
    source 27
    target 1325
  ]
  edge [
    source 27
    target 1326
  ]
  edge [
    source 27
    target 1327
  ]
  edge [
    source 27
    target 40
  ]
  edge [
    source 27
    target 1328
  ]
  edge [
    source 27
    target 1329
  ]
  edge [
    source 27
    target 1330
  ]
  edge [
    source 27
    target 1331
  ]
  edge [
    source 27
    target 1332
  ]
  edge [
    source 27
    target 1333
  ]
  edge [
    source 27
    target 1334
  ]
  edge [
    source 27
    target 1335
  ]
  edge [
    source 27
    target 1336
  ]
  edge [
    source 27
    target 1337
  ]
  edge [
    source 27
    target 1338
  ]
  edge [
    source 27
    target 1339
  ]
  edge [
    source 27
    target 1340
  ]
  edge [
    source 27
    target 1341
  ]
  edge [
    source 27
    target 1342
  ]
  edge [
    source 27
    target 1343
  ]
  edge [
    source 27
    target 1344
  ]
  edge [
    source 27
    target 848
  ]
  edge [
    source 27
    target 1345
  ]
  edge [
    source 27
    target 1346
  ]
  edge [
    source 27
    target 1347
  ]
  edge [
    source 27
    target 1348
  ]
  edge [
    source 27
    target 1349
  ]
  edge [
    source 27
    target 1350
  ]
  edge [
    source 27
    target 1351
  ]
  edge [
    source 27
    target 1352
  ]
  edge [
    source 27
    target 1353
  ]
  edge [
    source 27
    target 1354
  ]
  edge [
    source 27
    target 1355
  ]
  edge [
    source 27
    target 1356
  ]
  edge [
    source 27
    target 1357
  ]
  edge [
    source 27
    target 1358
  ]
  edge [
    source 27
    target 1359
  ]
  edge [
    source 27
    target 1360
  ]
  edge [
    source 27
    target 254
  ]
  edge [
    source 27
    target 1361
  ]
  edge [
    source 27
    target 1362
  ]
  edge [
    source 27
    target 1363
  ]
  edge [
    source 27
    target 1364
  ]
  edge [
    source 27
    target 1365
  ]
  edge [
    source 27
    target 1366
  ]
  edge [
    source 27
    target 1367
  ]
  edge [
    source 27
    target 1368
  ]
  edge [
    source 27
    target 1369
  ]
  edge [
    source 27
    target 1370
  ]
  edge [
    source 27
    target 1371
  ]
  edge [
    source 27
    target 1372
  ]
  edge [
    source 27
    target 1373
  ]
  edge [
    source 27
    target 1374
  ]
  edge [
    source 27
    target 1375
  ]
  edge [
    source 27
    target 1376
  ]
  edge [
    source 27
    target 1377
  ]
  edge [
    source 27
    target 1378
  ]
  edge [
    source 27
    target 1379
  ]
  edge [
    source 27
    target 1380
  ]
  edge [
    source 27
    target 1381
  ]
  edge [
    source 27
    target 1382
  ]
  edge [
    source 27
    target 1383
  ]
  edge [
    source 27
    target 1384
  ]
  edge [
    source 27
    target 1385
  ]
  edge [
    source 27
    target 1386
  ]
  edge [
    source 27
    target 1387
  ]
  edge [
    source 27
    target 1388
  ]
  edge [
    source 27
    target 1389
  ]
  edge [
    source 27
    target 1390
  ]
  edge [
    source 27
    target 1391
  ]
  edge [
    source 27
    target 1392
  ]
  edge [
    source 27
    target 1393
  ]
  edge [
    source 27
    target 1394
  ]
  edge [
    source 27
    target 1395
  ]
  edge [
    source 27
    target 1396
  ]
  edge [
    source 27
    target 1397
  ]
  edge [
    source 27
    target 1398
  ]
  edge [
    source 27
    target 1399
  ]
  edge [
    source 27
    target 1400
  ]
  edge [
    source 27
    target 1401
  ]
  edge [
    source 27
    target 1402
  ]
  edge [
    source 27
    target 1403
  ]
  edge [
    source 27
    target 1404
  ]
  edge [
    source 27
    target 1405
  ]
  edge [
    source 27
    target 1406
  ]
  edge [
    source 27
    target 1407
  ]
  edge [
    source 27
    target 1408
  ]
  edge [
    source 27
    target 1409
  ]
  edge [
    source 27
    target 1410
  ]
  edge [
    source 27
    target 1411
  ]
  edge [
    source 27
    target 1412
  ]
  edge [
    source 27
    target 1413
  ]
  edge [
    source 27
    target 1414
  ]
  edge [
    source 27
    target 1415
  ]
  edge [
    source 27
    target 1416
  ]
  edge [
    source 27
    target 1417
  ]
  edge [
    source 27
    target 1418
  ]
  edge [
    source 27
    target 1419
  ]
  edge [
    source 27
    target 1420
  ]
  edge [
    source 27
    target 1421
  ]
  edge [
    source 27
    target 1422
  ]
  edge [
    source 27
    target 1423
  ]
  edge [
    source 27
    target 1424
  ]
  edge [
    source 27
    target 1425
  ]
  edge [
    source 27
    target 1426
  ]
  edge [
    source 27
    target 1427
  ]
  edge [
    source 27
    target 1428
  ]
  edge [
    source 27
    target 1429
  ]
  edge [
    source 27
    target 1430
  ]
  edge [
    source 27
    target 1431
  ]
  edge [
    source 27
    target 1432
  ]
  edge [
    source 27
    target 1433
  ]
  edge [
    source 27
    target 1434
  ]
  edge [
    source 27
    target 1435
  ]
  edge [
    source 27
    target 1436
  ]
  edge [
    source 27
    target 1437
  ]
  edge [
    source 27
    target 1438
  ]
  edge [
    source 27
    target 1439
  ]
  edge [
    source 27
    target 1440
  ]
  edge [
    source 27
    target 1441
  ]
  edge [
    source 27
    target 1442
  ]
  edge [
    source 27
    target 62
  ]
  edge [
    source 27
    target 1443
  ]
  edge [
    source 27
    target 1444
  ]
  edge [
    source 27
    target 1445
  ]
  edge [
    source 27
    target 1446
  ]
  edge [
    source 27
    target 1447
  ]
  edge [
    source 27
    target 1448
  ]
  edge [
    source 27
    target 1449
  ]
  edge [
    source 27
    target 1450
  ]
  edge [
    source 27
    target 1451
  ]
  edge [
    source 27
    target 1452
  ]
  edge [
    source 27
    target 1453
  ]
  edge [
    source 27
    target 1454
  ]
  edge [
    source 27
    target 1455
  ]
  edge [
    source 27
    target 1456
  ]
  edge [
    source 27
    target 1457
  ]
  edge [
    source 27
    target 1458
  ]
  edge [
    source 27
    target 1459
  ]
  edge [
    source 27
    target 1460
  ]
  edge [
    source 27
    target 1461
  ]
  edge [
    source 27
    target 1462
  ]
  edge [
    source 27
    target 1463
  ]
  edge [
    source 27
    target 1464
  ]
  edge [
    source 27
    target 1465
  ]
  edge [
    source 27
    target 1466
  ]
  edge [
    source 27
    target 1467
  ]
  edge [
    source 27
    target 1468
  ]
  edge [
    source 27
    target 1469
  ]
  edge [
    source 27
    target 1470
  ]
  edge [
    source 27
    target 1471
  ]
  edge [
    source 27
    target 1472
  ]
  edge [
    source 27
    target 1473
  ]
  edge [
    source 27
    target 1474
  ]
  edge [
    source 27
    target 1475
  ]
  edge [
    source 27
    target 1476
  ]
  edge [
    source 27
    target 1477
  ]
  edge [
    source 27
    target 1478
  ]
  edge [
    source 27
    target 1479
  ]
  edge [
    source 27
    target 1480
  ]
  edge [
    source 27
    target 1481
  ]
  edge [
    source 27
    target 1482
  ]
  edge [
    source 27
    target 1483
  ]
  edge [
    source 27
    target 1484
  ]
  edge [
    source 27
    target 1485
  ]
  edge [
    source 27
    target 1486
  ]
  edge [
    source 27
    target 1487
  ]
  edge [
    source 27
    target 1488
  ]
  edge [
    source 27
    target 1489
  ]
  edge [
    source 27
    target 1490
  ]
  edge [
    source 27
    target 1491
  ]
  edge [
    source 27
    target 1492
  ]
  edge [
    source 27
    target 1493
  ]
  edge [
    source 27
    target 1494
  ]
  edge [
    source 27
    target 1495
  ]
  edge [
    source 27
    target 1496
  ]
  edge [
    source 27
    target 1497
  ]
  edge [
    source 27
    target 1498
  ]
  edge [
    source 27
    target 1499
  ]
  edge [
    source 27
    target 1500
  ]
  edge [
    source 27
    target 1501
  ]
  edge [
    source 27
    target 1502
  ]
  edge [
    source 27
    target 1503
  ]
  edge [
    source 27
    target 1504
  ]
  edge [
    source 27
    target 1505
  ]
  edge [
    source 27
    target 1506
  ]
  edge [
    source 27
    target 1507
  ]
  edge [
    source 27
    target 1508
  ]
  edge [
    source 27
    target 1509
  ]
  edge [
    source 27
    target 1510
  ]
  edge [
    source 27
    target 1511
  ]
  edge [
    source 27
    target 1512
  ]
  edge [
    source 27
    target 1513
  ]
  edge [
    source 27
    target 1514
  ]
  edge [
    source 27
    target 1515
  ]
  edge [
    source 27
    target 1516
  ]
  edge [
    source 27
    target 1517
  ]
  edge [
    source 27
    target 1518
  ]
  edge [
    source 27
    target 1519
  ]
  edge [
    source 27
    target 1520
  ]
  edge [
    source 27
    target 1521
  ]
  edge [
    source 27
    target 1522
  ]
  edge [
    source 27
    target 1523
  ]
  edge [
    source 27
    target 1524
  ]
  edge [
    source 27
    target 1525
  ]
  edge [
    source 27
    target 1526
  ]
  edge [
    source 27
    target 1527
  ]
  edge [
    source 27
    target 1528
  ]
  edge [
    source 27
    target 1529
  ]
  edge [
    source 27
    target 1530
  ]
  edge [
    source 27
    target 1531
  ]
  edge [
    source 27
    target 1532
  ]
  edge [
    source 27
    target 1533
  ]
  edge [
    source 27
    target 1534
  ]
  edge [
    source 27
    target 1535
  ]
  edge [
    source 27
    target 1536
  ]
  edge [
    source 27
    target 1537
  ]
  edge [
    source 27
    target 1538
  ]
  edge [
    source 27
    target 1539
  ]
  edge [
    source 27
    target 1540
  ]
  edge [
    source 27
    target 1541
  ]
  edge [
    source 27
    target 1542
  ]
  edge [
    source 27
    target 1543
  ]
  edge [
    source 27
    target 1544
  ]
  edge [
    source 27
    target 1545
  ]
  edge [
    source 27
    target 1546
  ]
  edge [
    source 27
    target 1547
  ]
  edge [
    source 27
    target 95
  ]
  edge [
    source 27
    target 1548
  ]
  edge [
    source 27
    target 1549
  ]
  edge [
    source 27
    target 1550
  ]
  edge [
    source 27
    target 1551
  ]
  edge [
    source 27
    target 1552
  ]
  edge [
    source 27
    target 1553
  ]
  edge [
    source 27
    target 1554
  ]
  edge [
    source 27
    target 1555
  ]
  edge [
    source 27
    target 1556
  ]
  edge [
    source 27
    target 1557
  ]
  edge [
    source 27
    target 1558
  ]
  edge [
    source 27
    target 1559
  ]
  edge [
    source 27
    target 1560
  ]
  edge [
    source 27
    target 1561
  ]
  edge [
    source 27
    target 1562
  ]
  edge [
    source 27
    target 1563
  ]
  edge [
    source 27
    target 1564
  ]
  edge [
    source 27
    target 1565
  ]
  edge [
    source 27
    target 1566
  ]
  edge [
    source 27
    target 1567
  ]
  edge [
    source 27
    target 1568
  ]
  edge [
    source 27
    target 1569
  ]
  edge [
    source 27
    target 1570
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 1571
  ]
  edge [
    source 27
    target 1572
  ]
  edge [
    source 27
    target 1573
  ]
  edge [
    source 27
    target 1574
  ]
  edge [
    source 27
    target 1575
  ]
  edge [
    source 27
    target 1576
  ]
  edge [
    source 27
    target 1577
  ]
  edge [
    source 27
    target 1578
  ]
  edge [
    source 27
    target 1579
  ]
  edge [
    source 27
    target 1580
  ]
  edge [
    source 27
    target 1581
  ]
  edge [
    source 27
    target 1582
  ]
  edge [
    source 27
    target 1583
  ]
  edge [
    source 27
    target 1584
  ]
  edge [
    source 27
    target 1585
  ]
  edge [
    source 27
    target 1586
  ]
  edge [
    source 27
    target 1587
  ]
  edge [
    source 27
    target 1588
  ]
  edge [
    source 27
    target 1589
  ]
  edge [
    source 27
    target 1590
  ]
  edge [
    source 27
    target 1591
  ]
  edge [
    source 27
    target 1592
  ]
  edge [
    source 27
    target 1593
  ]
  edge [
    source 27
    target 1594
  ]
  edge [
    source 27
    target 1595
  ]
  edge [
    source 27
    target 1596
  ]
  edge [
    source 27
    target 1597
  ]
  edge [
    source 27
    target 1598
  ]
  edge [
    source 27
    target 1599
  ]
  edge [
    source 27
    target 1600
  ]
  edge [
    source 27
    target 1601
  ]
  edge [
    source 27
    target 1602
  ]
  edge [
    source 27
    target 1603
  ]
  edge [
    source 27
    target 1604
  ]
  edge [
    source 27
    target 1605
  ]
  edge [
    source 27
    target 1606
  ]
  edge [
    source 27
    target 1607
  ]
  edge [
    source 27
    target 1608
  ]
  edge [
    source 27
    target 1609
  ]
  edge [
    source 27
    target 1610
  ]
  edge [
    source 27
    target 1611
  ]
  edge [
    source 27
    target 1612
  ]
  edge [
    source 27
    target 1613
  ]
  edge [
    source 27
    target 1614
  ]
  edge [
    source 27
    target 1615
  ]
  edge [
    source 27
    target 1616
  ]
  edge [
    source 27
    target 1617
  ]
  edge [
    source 27
    target 1618
  ]
  edge [
    source 27
    target 1619
  ]
  edge [
    source 27
    target 1620
  ]
  edge [
    source 27
    target 1621
  ]
  edge [
    source 27
    target 1622
  ]
  edge [
    source 27
    target 1623
  ]
  edge [
    source 27
    target 1624
  ]
  edge [
    source 27
    target 1625
  ]
  edge [
    source 27
    target 1626
  ]
  edge [
    source 27
    target 1627
  ]
  edge [
    source 27
    target 1628
  ]
  edge [
    source 27
    target 1629
  ]
  edge [
    source 27
    target 1630
  ]
  edge [
    source 27
    target 1631
  ]
  edge [
    source 27
    target 1632
  ]
  edge [
    source 27
    target 1633
  ]
  edge [
    source 27
    target 1634
  ]
  edge [
    source 27
    target 1635
  ]
  edge [
    source 27
    target 1636
  ]
  edge [
    source 27
    target 1637
  ]
  edge [
    source 27
    target 1638
  ]
  edge [
    source 27
    target 1639
  ]
  edge [
    source 27
    target 1640
  ]
  edge [
    source 27
    target 1641
  ]
  edge [
    source 27
    target 1642
  ]
  edge [
    source 27
    target 1643
  ]
  edge [
    source 27
    target 1644
  ]
  edge [
    source 27
    target 1645
  ]
  edge [
    source 27
    target 1646
  ]
  edge [
    source 27
    target 1647
  ]
  edge [
    source 27
    target 1648
  ]
  edge [
    source 27
    target 1649
  ]
  edge [
    source 27
    target 1650
  ]
  edge [
    source 27
    target 1651
  ]
  edge [
    source 27
    target 1652
  ]
  edge [
    source 27
    target 1653
  ]
  edge [
    source 27
    target 1654
  ]
  edge [
    source 27
    target 1655
  ]
  edge [
    source 27
    target 1656
  ]
  edge [
    source 27
    target 1657
  ]
  edge [
    source 27
    target 1658
  ]
  edge [
    source 27
    target 1659
  ]
  edge [
    source 27
    target 1660
  ]
  edge [
    source 27
    target 1661
  ]
  edge [
    source 27
    target 1662
  ]
  edge [
    source 27
    target 1663
  ]
  edge [
    source 27
    target 1664
  ]
  edge [
    source 27
    target 1665
  ]
  edge [
    source 27
    target 1666
  ]
  edge [
    source 27
    target 1667
  ]
  edge [
    source 27
    target 1668
  ]
  edge [
    source 27
    target 1669
  ]
  edge [
    source 27
    target 1670
  ]
  edge [
    source 27
    target 1671
  ]
  edge [
    source 27
    target 1672
  ]
  edge [
    source 27
    target 1673
  ]
  edge [
    source 27
    target 1674
  ]
  edge [
    source 27
    target 1675
  ]
  edge [
    source 27
    target 1676
  ]
  edge [
    source 27
    target 1677
  ]
  edge [
    source 27
    target 1678
  ]
  edge [
    source 27
    target 1679
  ]
  edge [
    source 27
    target 1680
  ]
  edge [
    source 27
    target 1681
  ]
  edge [
    source 27
    target 1682
  ]
  edge [
    source 27
    target 1683
  ]
  edge [
    source 27
    target 1684
  ]
  edge [
    source 27
    target 1685
  ]
  edge [
    source 27
    target 1686
  ]
  edge [
    source 27
    target 1687
  ]
  edge [
    source 27
    target 1688
  ]
  edge [
    source 27
    target 1689
  ]
  edge [
    source 27
    target 1690
  ]
  edge [
    source 27
    target 1691
  ]
  edge [
    source 27
    target 1692
  ]
  edge [
    source 27
    target 1693
  ]
  edge [
    source 27
    target 1694
  ]
  edge [
    source 27
    target 1695
  ]
  edge [
    source 27
    target 1696
  ]
  edge [
    source 27
    target 1697
  ]
  edge [
    source 27
    target 1698
  ]
  edge [
    source 27
    target 1699
  ]
  edge [
    source 27
    target 1700
  ]
  edge [
    source 27
    target 1701
  ]
  edge [
    source 27
    target 1702
  ]
  edge [
    source 27
    target 1703
  ]
  edge [
    source 27
    target 1704
  ]
  edge [
    source 27
    target 1705
  ]
  edge [
    source 27
    target 1706
  ]
  edge [
    source 27
    target 1707
  ]
  edge [
    source 27
    target 1708
  ]
  edge [
    source 27
    target 1709
  ]
  edge [
    source 27
    target 1710
  ]
  edge [
    source 27
    target 1711
  ]
  edge [
    source 27
    target 1712
  ]
  edge [
    source 27
    target 1713
  ]
  edge [
    source 27
    target 1714
  ]
  edge [
    source 27
    target 1715
  ]
  edge [
    source 27
    target 1716
  ]
  edge [
    source 27
    target 1717
  ]
  edge [
    source 27
    target 1718
  ]
  edge [
    source 27
    target 1719
  ]
  edge [
    source 27
    target 1720
  ]
  edge [
    source 27
    target 1721
  ]
  edge [
    source 27
    target 1722
  ]
  edge [
    source 27
    target 1723
  ]
  edge [
    source 27
    target 1724
  ]
  edge [
    source 27
    target 1725
  ]
  edge [
    source 27
    target 1726
  ]
  edge [
    source 27
    target 1727
  ]
  edge [
    source 27
    target 1728
  ]
  edge [
    source 27
    target 1729
  ]
  edge [
    source 27
    target 1730
  ]
  edge [
    source 27
    target 1731
  ]
  edge [
    source 27
    target 1732
  ]
  edge [
    source 27
    target 1733
  ]
  edge [
    source 27
    target 1734
  ]
  edge [
    source 27
    target 1735
  ]
  edge [
    source 27
    target 1736
  ]
  edge [
    source 27
    target 1737
  ]
  edge [
    source 27
    target 1738
  ]
  edge [
    source 27
    target 1739
  ]
  edge [
    source 27
    target 1740
  ]
  edge [
    source 27
    target 1741
  ]
  edge [
    source 27
    target 1742
  ]
  edge [
    source 27
    target 1743
  ]
  edge [
    source 27
    target 1744
  ]
  edge [
    source 27
    target 1745
  ]
  edge [
    source 27
    target 1746
  ]
  edge [
    source 27
    target 1747
  ]
  edge [
    source 27
    target 1748
  ]
  edge [
    source 27
    target 1749
  ]
  edge [
    source 27
    target 1750
  ]
  edge [
    source 27
    target 1751
  ]
  edge [
    source 27
    target 1752
  ]
  edge [
    source 27
    target 1753
  ]
  edge [
    source 27
    target 1754
  ]
  edge [
    source 27
    target 1755
  ]
  edge [
    source 27
    target 1756
  ]
  edge [
    source 27
    target 1757
  ]
  edge [
    source 27
    target 1758
  ]
  edge [
    source 27
    target 1759
  ]
  edge [
    source 27
    target 1760
  ]
  edge [
    source 27
    target 1761
  ]
  edge [
    source 27
    target 1762
  ]
  edge [
    source 27
    target 1763
  ]
  edge [
    source 27
    target 1764
  ]
  edge [
    source 27
    target 1765
  ]
  edge [
    source 27
    target 1766
  ]
  edge [
    source 27
    target 1767
  ]
  edge [
    source 27
    target 1768
  ]
  edge [
    source 27
    target 1769
  ]
  edge [
    source 27
    target 1770
  ]
  edge [
    source 27
    target 1771
  ]
  edge [
    source 27
    target 1772
  ]
  edge [
    source 27
    target 1773
  ]
  edge [
    source 27
    target 1774
  ]
  edge [
    source 27
    target 1775
  ]
  edge [
    source 27
    target 1776
  ]
  edge [
    source 27
    target 1777
  ]
  edge [
    source 27
    target 1778
  ]
  edge [
    source 27
    target 1779
  ]
  edge [
    source 27
    target 1780
  ]
  edge [
    source 27
    target 1781
  ]
  edge [
    source 27
    target 1782
  ]
  edge [
    source 27
    target 1783
  ]
  edge [
    source 27
    target 1784
  ]
  edge [
    source 27
    target 1785
  ]
  edge [
    source 27
    target 1786
  ]
  edge [
    source 27
    target 1787
  ]
  edge [
    source 27
    target 1788
  ]
  edge [
    source 27
    target 1789
  ]
  edge [
    source 27
    target 1790
  ]
  edge [
    source 27
    target 1791
  ]
  edge [
    source 27
    target 1792
  ]
  edge [
    source 27
    target 1793
  ]
  edge [
    source 27
    target 1794
  ]
  edge [
    source 27
    target 1795
  ]
  edge [
    source 27
    target 1796
  ]
  edge [
    source 27
    target 1797
  ]
  edge [
    source 27
    target 1798
  ]
  edge [
    source 27
    target 1799
  ]
  edge [
    source 27
    target 1800
  ]
  edge [
    source 27
    target 1801
  ]
  edge [
    source 27
    target 1802
  ]
  edge [
    source 27
    target 1803
  ]
  edge [
    source 27
    target 1804
  ]
  edge [
    source 27
    target 1805
  ]
  edge [
    source 27
    target 1806
  ]
  edge [
    source 27
    target 1807
  ]
  edge [
    source 27
    target 1808
  ]
  edge [
    source 27
    target 1809
  ]
  edge [
    source 27
    target 1810
  ]
  edge [
    source 27
    target 1811
  ]
  edge [
    source 27
    target 1812
  ]
  edge [
    source 27
    target 1813
  ]
  edge [
    source 27
    target 1814
  ]
  edge [
    source 27
    target 1815
  ]
  edge [
    source 27
    target 1816
  ]
  edge [
    source 27
    target 1817
  ]
  edge [
    source 27
    target 1818
  ]
  edge [
    source 27
    target 1819
  ]
  edge [
    source 27
    target 1820
  ]
  edge [
    source 27
    target 1821
  ]
  edge [
    source 27
    target 1822
  ]
  edge [
    source 27
    target 1823
  ]
  edge [
    source 27
    target 1824
  ]
  edge [
    source 27
    target 1825
  ]
  edge [
    source 27
    target 1826
  ]
  edge [
    source 27
    target 1827
  ]
  edge [
    source 27
    target 1828
  ]
  edge [
    source 27
    target 1829
  ]
  edge [
    source 27
    target 1830
  ]
  edge [
    source 27
    target 1831
  ]
  edge [
    source 27
    target 1832
  ]
  edge [
    source 27
    target 1833
  ]
  edge [
    source 27
    target 1834
  ]
  edge [
    source 27
    target 1835
  ]
  edge [
    source 27
    target 1836
  ]
  edge [
    source 27
    target 1837
  ]
  edge [
    source 27
    target 1838
  ]
  edge [
    source 27
    target 1839
  ]
  edge [
    source 27
    target 1840
  ]
  edge [
    source 27
    target 1841
  ]
  edge [
    source 27
    target 1842
  ]
  edge [
    source 27
    target 1843
  ]
  edge [
    source 27
    target 1844
  ]
  edge [
    source 27
    target 1845
  ]
  edge [
    source 27
    target 1846
  ]
  edge [
    source 27
    target 1847
  ]
  edge [
    source 27
    target 1848
  ]
  edge [
    source 27
    target 1849
  ]
  edge [
    source 27
    target 1850
  ]
  edge [
    source 27
    target 1851
  ]
  edge [
    source 27
    target 1852
  ]
  edge [
    source 27
    target 1853
  ]
  edge [
    source 27
    target 1854
  ]
  edge [
    source 27
    target 1855
  ]
  edge [
    source 27
    target 1856
  ]
  edge [
    source 27
    target 1857
  ]
  edge [
    source 27
    target 1858
  ]
  edge [
    source 27
    target 1859
  ]
  edge [
    source 27
    target 1860
  ]
  edge [
    source 27
    target 1861
  ]
  edge [
    source 27
    target 1862
  ]
  edge [
    source 27
    target 1863
  ]
  edge [
    source 27
    target 1864
  ]
  edge [
    source 27
    target 1865
  ]
  edge [
    source 27
    target 1866
  ]
  edge [
    source 27
    target 1867
  ]
  edge [
    source 27
    target 1868
  ]
  edge [
    source 27
    target 1869
  ]
  edge [
    source 27
    target 1870
  ]
  edge [
    source 27
    target 1871
  ]
  edge [
    source 27
    target 1872
  ]
  edge [
    source 27
    target 1873
  ]
  edge [
    source 27
    target 1874
  ]
  edge [
    source 27
    target 1875
  ]
  edge [
    source 27
    target 1876
  ]
  edge [
    source 27
    target 1877
  ]
  edge [
    source 27
    target 1878
  ]
  edge [
    source 27
    target 1879
  ]
  edge [
    source 27
    target 1880
  ]
  edge [
    source 27
    target 1881
  ]
  edge [
    source 27
    target 1882
  ]
  edge [
    source 27
    target 1883
  ]
  edge [
    source 27
    target 1884
  ]
  edge [
    source 27
    target 1885
  ]
  edge [
    source 27
    target 1886
  ]
  edge [
    source 27
    target 1887
  ]
  edge [
    source 27
    target 1888
  ]
  edge [
    source 27
    target 1889
  ]
  edge [
    source 27
    target 1890
  ]
  edge [
    source 27
    target 1891
  ]
  edge [
    source 27
    target 1892
  ]
  edge [
    source 27
    target 1893
  ]
  edge [
    source 27
    target 1894
  ]
  edge [
    source 27
    target 1895
  ]
  edge [
    source 27
    target 1896
  ]
  edge [
    source 27
    target 1897
  ]
  edge [
    source 27
    target 111
  ]
  edge [
    source 27
    target 1898
  ]
  edge [
    source 27
    target 1899
  ]
  edge [
    source 27
    target 1900
  ]
  edge [
    source 27
    target 1901
  ]
  edge [
    source 27
    target 1902
  ]
  edge [
    source 27
    target 1903
  ]
  edge [
    source 27
    target 1904
  ]
  edge [
    source 27
    target 1905
  ]
  edge [
    source 27
    target 1906
  ]
  edge [
    source 27
    target 1907
  ]
  edge [
    source 27
    target 1908
  ]
  edge [
    source 27
    target 1909
  ]
  edge [
    source 27
    target 1910
  ]
  edge [
    source 27
    target 1911
  ]
  edge [
    source 27
    target 1912
  ]
  edge [
    source 27
    target 1913
  ]
  edge [
    source 27
    target 1914
  ]
  edge [
    source 27
    target 1915
  ]
  edge [
    source 27
    target 622
  ]
  edge [
    source 27
    target 1916
  ]
  edge [
    source 27
    target 1917
  ]
  edge [
    source 27
    target 1918
  ]
  edge [
    source 27
    target 1919
  ]
  edge [
    source 27
    target 1920
  ]
  edge [
    source 27
    target 1921
  ]
  edge [
    source 27
    target 1922
  ]
  edge [
    source 27
    target 1923
  ]
  edge [
    source 27
    target 1924
  ]
  edge [
    source 27
    target 1925
  ]
  edge [
    source 27
    target 1926
  ]
  edge [
    source 27
    target 1927
  ]
  edge [
    source 27
    target 1928
  ]
  edge [
    source 27
    target 1929
  ]
  edge [
    source 27
    target 1930
  ]
  edge [
    source 27
    target 1931
  ]
  edge [
    source 27
    target 1932
  ]
  edge [
    source 27
    target 1933
  ]
  edge [
    source 27
    target 1934
  ]
  edge [
    source 27
    target 317
  ]
  edge [
    source 27
    target 1935
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 1936
  ]
  edge [
    source 29
    target 1937
  ]
  edge [
    source 29
    target 1938
  ]
  edge [
    source 29
    target 1939
  ]
  edge [
    source 29
    target 1940
  ]
  edge [
    source 29
    target 1941
  ]
  edge [
    source 29
    target 1942
  ]
  edge [
    source 29
    target 1943
  ]
  edge [
    source 29
    target 1944
  ]
  edge [
    source 29
    target 1945
  ]
  edge [
    source 29
    target 1946
  ]
  edge [
    source 29
    target 932
  ]
  edge [
    source 29
    target 1947
  ]
  edge [
    source 29
    target 1948
  ]
  edge [
    source 29
    target 1949
  ]
  edge [
    source 29
    target 1950
  ]
  edge [
    source 29
    target 1951
  ]
  edge [
    source 29
    target 1952
  ]
  edge [
    source 29
    target 1953
  ]
  edge [
    source 29
    target 1954
  ]
  edge [
    source 29
    target 922
  ]
  edge [
    source 29
    target 1955
  ]
  edge [
    source 29
    target 1956
  ]
  edge [
    source 29
    target 1957
  ]
  edge [
    source 29
    target 1958
  ]
  edge [
    source 29
    target 1959
  ]
  edge [
    source 29
    target 1960
  ]
  edge [
    source 29
    target 1961
  ]
  edge [
    source 29
    target 1962
  ]
  edge [
    source 29
    target 1963
  ]
  edge [
    source 29
    target 1964
  ]
  edge [
    source 29
    target 934
  ]
  edge [
    source 29
    target 1965
  ]
  edge [
    source 29
    target 1966
  ]
  edge [
    source 29
    target 1967
  ]
  edge [
    source 29
    target 1968
  ]
  edge [
    source 29
    target 1969
  ]
  edge [
    source 29
    target 1970
  ]
  edge [
    source 29
    target 1971
  ]
  edge [
    source 29
    target 1972
  ]
  edge [
    source 29
    target 1973
  ]
  edge [
    source 29
    target 1974
  ]
  edge [
    source 29
    target 1975
  ]
  edge [
    source 29
    target 1976
  ]
  edge [
    source 29
    target 1977
  ]
  edge [
    source 29
    target 1978
  ]
  edge [
    source 29
    target 1979
  ]
  edge [
    source 29
    target 1980
  ]
  edge [
    source 29
    target 1981
  ]
  edge [
    source 29
    target 1982
  ]
  edge [
    source 29
    target 1983
  ]
  edge [
    source 29
    target 1984
  ]
  edge [
    source 29
    target 1985
  ]
  edge [
    source 29
    target 1986
  ]
  edge [
    source 29
    target 1987
  ]
  edge [
    source 29
    target 1988
  ]
  edge [
    source 1239
    target 1991
  ]
  edge [
    source 1992
    target 1993
  ]
]
