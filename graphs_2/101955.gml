graph [
  node [
    id 0
    label "emanowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pozytywnyi"
    origin "text"
  ]
  node [
    id 2
    label "emocja"
    origin "text"
  ]
  node [
    id 3
    label "ciep&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "optymizm"
    origin "text"
  ]
  node [
    id 5
    label "reklama"
    origin "text"
  ]
  node [
    id 6
    label "zach&#281;caj&#261;cy"
    origin "text"
  ]
  node [
    id 7
    label "odwiedzanie"
    origin "text"
  ]
  node [
    id 8
    label "islandia"
    origin "text"
  ]
  node [
    id 9
    label "przyda&#263;by"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "polska"
    origin "text"
  ]
  node [
    id 12
    label "air"
  ]
  node [
    id 13
    label "pokazywa&#263;"
  ]
  node [
    id 14
    label "radio_beam"
  ]
  node [
    id 15
    label "wydziela&#263;"
  ]
  node [
    id 16
    label "wydziela&#263;_si&#281;"
  ]
  node [
    id 17
    label "emit"
  ]
  node [
    id 18
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 19
    label "warto&#347;&#263;"
  ]
  node [
    id 20
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 21
    label "by&#263;"
  ]
  node [
    id 22
    label "exhibit"
  ]
  node [
    id 23
    label "podawa&#263;"
  ]
  node [
    id 24
    label "wyraz"
  ]
  node [
    id 25
    label "wyra&#380;a&#263;"
  ]
  node [
    id 26
    label "represent"
  ]
  node [
    id 27
    label "przedstawia&#263;"
  ]
  node [
    id 28
    label "przeszkala&#263;"
  ]
  node [
    id 29
    label "powodowa&#263;"
  ]
  node [
    id 30
    label "introduce"
  ]
  node [
    id 31
    label "exsert"
  ]
  node [
    id 32
    label "bespeak"
  ]
  node [
    id 33
    label "informowa&#263;"
  ]
  node [
    id 34
    label "indicate"
  ]
  node [
    id 35
    label "allocate"
  ]
  node [
    id 36
    label "wytwarza&#263;"
  ]
  node [
    id 37
    label "rozdziela&#263;"
  ]
  node [
    id 38
    label "exhaust"
  ]
  node [
    id 39
    label "przydziela&#263;"
  ]
  node [
    id 40
    label "wyznacza&#263;"
  ]
  node [
    id 41
    label "stagger"
  ]
  node [
    id 42
    label "oddziela&#263;"
  ]
  node [
    id 43
    label "wykrawa&#263;"
  ]
  node [
    id 44
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 45
    label "ogrom"
  ]
  node [
    id 46
    label "iskrzy&#263;"
  ]
  node [
    id 47
    label "d&#322;awi&#263;"
  ]
  node [
    id 48
    label "ostygn&#261;&#263;"
  ]
  node [
    id 49
    label "stygn&#261;&#263;"
  ]
  node [
    id 50
    label "stan"
  ]
  node [
    id 51
    label "temperatura"
  ]
  node [
    id 52
    label "wpa&#347;&#263;"
  ]
  node [
    id 53
    label "afekt"
  ]
  node [
    id 54
    label "wpada&#263;"
  ]
  node [
    id 55
    label "Ohio"
  ]
  node [
    id 56
    label "wci&#281;cie"
  ]
  node [
    id 57
    label "Nowy_York"
  ]
  node [
    id 58
    label "warstwa"
  ]
  node [
    id 59
    label "samopoczucie"
  ]
  node [
    id 60
    label "Illinois"
  ]
  node [
    id 61
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 62
    label "state"
  ]
  node [
    id 63
    label "Jukatan"
  ]
  node [
    id 64
    label "Kalifornia"
  ]
  node [
    id 65
    label "Wirginia"
  ]
  node [
    id 66
    label "wektor"
  ]
  node [
    id 67
    label "Goa"
  ]
  node [
    id 68
    label "Teksas"
  ]
  node [
    id 69
    label "Waszyngton"
  ]
  node [
    id 70
    label "miejsce"
  ]
  node [
    id 71
    label "Massachusetts"
  ]
  node [
    id 72
    label "Alaska"
  ]
  node [
    id 73
    label "Arakan"
  ]
  node [
    id 74
    label "Hawaje"
  ]
  node [
    id 75
    label "Maryland"
  ]
  node [
    id 76
    label "punkt"
  ]
  node [
    id 77
    label "Michigan"
  ]
  node [
    id 78
    label "Arizona"
  ]
  node [
    id 79
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 80
    label "Georgia"
  ]
  node [
    id 81
    label "poziom"
  ]
  node [
    id 82
    label "Pensylwania"
  ]
  node [
    id 83
    label "shape"
  ]
  node [
    id 84
    label "Luizjana"
  ]
  node [
    id 85
    label "Nowy_Meksyk"
  ]
  node [
    id 86
    label "Alabama"
  ]
  node [
    id 87
    label "ilo&#347;&#263;"
  ]
  node [
    id 88
    label "Kansas"
  ]
  node [
    id 89
    label "Oregon"
  ]
  node [
    id 90
    label "Oklahoma"
  ]
  node [
    id 91
    label "Floryda"
  ]
  node [
    id 92
    label "jednostka_administracyjna"
  ]
  node [
    id 93
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 94
    label "afekcja"
  ]
  node [
    id 95
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 96
    label "gasi&#263;"
  ]
  node [
    id 97
    label "oddech"
  ]
  node [
    id 98
    label "mute"
  ]
  node [
    id 99
    label "uciska&#263;"
  ]
  node [
    id 100
    label "accelerator"
  ]
  node [
    id 101
    label "urge"
  ]
  node [
    id 102
    label "zmniejsza&#263;"
  ]
  node [
    id 103
    label "restrict"
  ]
  node [
    id 104
    label "hamowa&#263;"
  ]
  node [
    id 105
    label "hesitate"
  ]
  node [
    id 106
    label "dusi&#263;"
  ]
  node [
    id 107
    label "rozleg&#322;o&#347;&#263;"
  ]
  node [
    id 108
    label "wielko&#347;&#263;"
  ]
  node [
    id 109
    label "clutter"
  ]
  node [
    id 110
    label "mn&#243;stwo"
  ]
  node [
    id 111
    label "intensywno&#347;&#263;"
  ]
  node [
    id 112
    label "zanikn&#261;&#263;"
  ]
  node [
    id 113
    label "och&#322;odzi&#263;_si&#281;"
  ]
  node [
    id 114
    label "uspokoi&#263;_si&#281;"
  ]
  node [
    id 115
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 116
    label "tautochrona"
  ]
  node [
    id 117
    label "denga"
  ]
  node [
    id 118
    label "termoelektryczno&#347;&#263;"
  ]
  node [
    id 119
    label "zapalenie_opon_m&#243;zgowo-rdzeniowych"
  ]
  node [
    id 120
    label "oznaka"
  ]
  node [
    id 121
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 122
    label "hotness"
  ]
  node [
    id 123
    label "choroba_bosto&#324;ska"
  ]
  node [
    id 124
    label "atmosfera"
  ]
  node [
    id 125
    label "rozpalony"
  ]
  node [
    id 126
    label "cia&#322;o"
  ]
  node [
    id 127
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 128
    label "zagrza&#263;"
  ]
  node [
    id 129
    label "termoczu&#322;y"
  ]
  node [
    id 130
    label "strike"
  ]
  node [
    id 131
    label "ulec"
  ]
  node [
    id 132
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 133
    label "collapse"
  ]
  node [
    id 134
    label "rzecz"
  ]
  node [
    id 135
    label "d&#378;wi&#281;k"
  ]
  node [
    id 136
    label "fall_upon"
  ]
  node [
    id 137
    label "ponie&#347;&#263;"
  ]
  node [
    id 138
    label "zapach"
  ]
  node [
    id 139
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 140
    label "uderzy&#263;"
  ]
  node [
    id 141
    label "wymy&#347;li&#263;"
  ]
  node [
    id 142
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 143
    label "decline"
  ]
  node [
    id 144
    label "&#347;wiat&#322;o"
  ]
  node [
    id 145
    label "fall"
  ]
  node [
    id 146
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 147
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 148
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 149
    label "spotka&#263;"
  ]
  node [
    id 150
    label "odwiedzi&#263;"
  ]
  node [
    id 151
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 152
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 153
    label "odczucia"
  ]
  node [
    id 154
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 155
    label "zaziera&#263;"
  ]
  node [
    id 156
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 157
    label "czu&#263;"
  ]
  node [
    id 158
    label "spotyka&#263;"
  ]
  node [
    id 159
    label "drop"
  ]
  node [
    id 160
    label "pogo"
  ]
  node [
    id 161
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 162
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 163
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 164
    label "popada&#263;"
  ]
  node [
    id 165
    label "odwiedza&#263;"
  ]
  node [
    id 166
    label "wymy&#347;la&#263;"
  ]
  node [
    id 167
    label "przypomina&#263;"
  ]
  node [
    id 168
    label "ujmowa&#263;"
  ]
  node [
    id 169
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 170
    label "chowa&#263;"
  ]
  node [
    id 171
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 172
    label "demaskowa&#263;"
  ]
  node [
    id 173
    label "ulega&#263;"
  ]
  node [
    id 174
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 175
    label "flatten"
  ]
  node [
    id 176
    label "cool"
  ]
  node [
    id 177
    label "ch&#322;odzi&#263;_si&#281;"
  ]
  node [
    id 178
    label "zanika&#263;"
  ]
  node [
    id 179
    label "&#347;wieci&#263;"
  ]
  node [
    id 180
    label "mie&#263;_miejsce"
  ]
  node [
    id 181
    label "dawa&#263;"
  ]
  node [
    id 182
    label "flash"
  ]
  node [
    id 183
    label "twinkle"
  ]
  node [
    id 184
    label "mieni&#263;_si&#281;"
  ]
  node [
    id 185
    label "wyr&#243;&#380;nia&#263;_si&#281;"
  ]
  node [
    id 186
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 187
    label "b&#322;yszcze&#263;"
  ]
  node [
    id 188
    label "glitter"
  ]
  node [
    id 189
    label "tryska&#263;"
  ]
  node [
    id 190
    label "geotermia"
  ]
  node [
    id 191
    label "przyjemnie"
  ]
  node [
    id 192
    label "pogoda"
  ]
  node [
    id 193
    label "emisyjno&#347;&#263;"
  ]
  node [
    id 194
    label "mi&#322;o"
  ]
  node [
    id 195
    label "ciep&#322;y"
  ]
  node [
    id 196
    label "zjawisko"
  ]
  node [
    id 197
    label "cecha"
  ]
  node [
    id 198
    label "heat"
  ]
  node [
    id 199
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 200
    label "potrzyma&#263;"
  ]
  node [
    id 201
    label "warunki"
  ]
  node [
    id 202
    label "pok&#243;j"
  ]
  node [
    id 203
    label "atak"
  ]
  node [
    id 204
    label "program"
  ]
  node [
    id 205
    label "meteorology"
  ]
  node [
    id 206
    label "weather"
  ]
  node [
    id 207
    label "czas"
  ]
  node [
    id 208
    label "prognoza_meteorologiczna"
  ]
  node [
    id 209
    label "charakterystyka"
  ]
  node [
    id 210
    label "m&#322;ot"
  ]
  node [
    id 211
    label "znak"
  ]
  node [
    id 212
    label "drzewo"
  ]
  node [
    id 213
    label "pr&#243;ba"
  ]
  node [
    id 214
    label "attribute"
  ]
  node [
    id 215
    label "marka"
  ]
  node [
    id 216
    label "dobrze"
  ]
  node [
    id 217
    label "pleasantly"
  ]
  node [
    id 218
    label "deliciously"
  ]
  node [
    id 219
    label "przyjemny"
  ]
  node [
    id 220
    label "gratifyingly"
  ]
  node [
    id 221
    label "przyja&#378;nie"
  ]
  node [
    id 222
    label "przychylnie"
  ]
  node [
    id 223
    label "mi&#322;y"
  ]
  node [
    id 224
    label "favor"
  ]
  node [
    id 225
    label "dobro&#263;"
  ]
  node [
    id 226
    label "nastawienie"
  ]
  node [
    id 227
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 228
    label "wola"
  ]
  node [
    id 229
    label "proces"
  ]
  node [
    id 230
    label "boski"
  ]
  node [
    id 231
    label "krajobraz"
  ]
  node [
    id 232
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 233
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 234
    label "przywidzenie"
  ]
  node [
    id 235
    label "presence"
  ]
  node [
    id 236
    label "charakter"
  ]
  node [
    id 237
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 238
    label "ocieplanie_si&#281;"
  ]
  node [
    id 239
    label "ocieplanie"
  ]
  node [
    id 240
    label "grzanie"
  ]
  node [
    id 241
    label "ocieplenie_si&#281;"
  ]
  node [
    id 242
    label "zagrzanie"
  ]
  node [
    id 243
    label "ocieplenie"
  ]
  node [
    id 244
    label "korzystny"
  ]
  node [
    id 245
    label "dobry"
  ]
  node [
    id 246
    label "parametr"
  ]
  node [
    id 247
    label "dwutlenek_w&#281;gla"
  ]
  node [
    id 248
    label "Ziemia"
  ]
  node [
    id 249
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 250
    label "energia_termiczna"
  ]
  node [
    id 251
    label "buoyancy"
  ]
  node [
    id 252
    label "postawa"
  ]
  node [
    id 253
    label "pozycja"
  ]
  node [
    id 254
    label "attitude"
  ]
  node [
    id 255
    label "copywriting"
  ]
  node [
    id 256
    label "wypromowa&#263;"
  ]
  node [
    id 257
    label "brief"
  ]
  node [
    id 258
    label "samplowanie"
  ]
  node [
    id 259
    label "akcja"
  ]
  node [
    id 260
    label "promowa&#263;"
  ]
  node [
    id 261
    label "bran&#380;a"
  ]
  node [
    id 262
    label "tekst"
  ]
  node [
    id 263
    label "informacja"
  ]
  node [
    id 264
    label "dywidenda"
  ]
  node [
    id 265
    label "przebieg"
  ]
  node [
    id 266
    label "operacja"
  ]
  node [
    id 267
    label "zagrywka"
  ]
  node [
    id 268
    label "wydarzenie"
  ]
  node [
    id 269
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 270
    label "udzia&#322;"
  ]
  node [
    id 271
    label "commotion"
  ]
  node [
    id 272
    label "occupation"
  ]
  node [
    id 273
    label "gra"
  ]
  node [
    id 274
    label "jazda"
  ]
  node [
    id 275
    label "czyn"
  ]
  node [
    id 276
    label "stock"
  ]
  node [
    id 277
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 278
    label "w&#281;ze&#322;"
  ]
  node [
    id 279
    label "wysoko&#347;&#263;"
  ]
  node [
    id 280
    label "czynno&#347;&#263;"
  ]
  node [
    id 281
    label "instrument_strunowy"
  ]
  node [
    id 282
    label "publikacja"
  ]
  node [
    id 283
    label "wiedza"
  ]
  node [
    id 284
    label "doj&#347;cie"
  ]
  node [
    id 285
    label "obiega&#263;"
  ]
  node [
    id 286
    label "powzi&#281;cie"
  ]
  node [
    id 287
    label "dane"
  ]
  node [
    id 288
    label "obiegni&#281;cie"
  ]
  node [
    id 289
    label "sygna&#322;"
  ]
  node [
    id 290
    label "obieganie"
  ]
  node [
    id 291
    label "powzi&#261;&#263;"
  ]
  node [
    id 292
    label "obiec"
  ]
  node [
    id 293
    label "doj&#347;&#263;"
  ]
  node [
    id 294
    label "ekscerpcja"
  ]
  node [
    id 295
    label "j&#281;zykowo"
  ]
  node [
    id 296
    label "wypowied&#378;"
  ]
  node [
    id 297
    label "redakcja"
  ]
  node [
    id 298
    label "wytw&#243;r"
  ]
  node [
    id 299
    label "pomini&#281;cie"
  ]
  node [
    id 300
    label "dzie&#322;o"
  ]
  node [
    id 301
    label "preparacja"
  ]
  node [
    id 302
    label "odmianka"
  ]
  node [
    id 303
    label "opu&#347;ci&#263;"
  ]
  node [
    id 304
    label "koniektura"
  ]
  node [
    id 305
    label "pisa&#263;"
  ]
  node [
    id 306
    label "obelga"
  ]
  node [
    id 307
    label "dziedzina"
  ]
  node [
    id 308
    label "doprowadzi&#263;"
  ]
  node [
    id 309
    label "nada&#263;"
  ]
  node [
    id 310
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 311
    label "rozpowszechni&#263;"
  ]
  node [
    id 312
    label "zach&#281;ci&#263;"
  ]
  node [
    id 313
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 314
    label "promocja"
  ]
  node [
    id 315
    label "udzieli&#263;"
  ]
  node [
    id 316
    label "pom&#243;c"
  ]
  node [
    id 317
    label "nadawa&#263;"
  ]
  node [
    id 318
    label "wypromowywa&#263;"
  ]
  node [
    id 319
    label "rozpowszechnia&#263;"
  ]
  node [
    id 320
    label "zach&#281;ca&#263;"
  ]
  node [
    id 321
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 322
    label "advance"
  ]
  node [
    id 323
    label "udziela&#263;"
  ]
  node [
    id 324
    label "doprowadza&#263;"
  ]
  node [
    id 325
    label "pomaga&#263;"
  ]
  node [
    id 326
    label "dokument"
  ]
  node [
    id 327
    label "pisanie"
  ]
  node [
    id 328
    label "miksowa&#263;"
  ]
  node [
    id 329
    label "przer&#243;bka"
  ]
  node [
    id 330
    label "miks"
  ]
  node [
    id 331
    label "sampling"
  ]
  node [
    id 332
    label "emitowanie"
  ]
  node [
    id 333
    label "zach&#281;caj&#261;co"
  ]
  node [
    id 334
    label "przyjazny"
  ]
  node [
    id 335
    label "mobilizuj&#261;cy"
  ]
  node [
    id 336
    label "motywuj&#261;co"
  ]
  node [
    id 337
    label "stymuluj&#261;cy"
  ]
  node [
    id 338
    label "mobilizuj&#261;co"
  ]
  node [
    id 339
    label "pozytywny"
  ]
  node [
    id 340
    label "kochanek"
  ]
  node [
    id 341
    label "sk&#322;onny"
  ]
  node [
    id 342
    label "wybranek"
  ]
  node [
    id 343
    label "umi&#322;owany"
  ]
  node [
    id 344
    label "drogi"
  ]
  node [
    id 345
    label "kochanie"
  ]
  node [
    id 346
    label "dyplomata"
  ]
  node [
    id 347
    label "&#322;agodny"
  ]
  node [
    id 348
    label "bezpieczny"
  ]
  node [
    id 349
    label "nieszkodliwy"
  ]
  node [
    id 350
    label "przyja&#378;ny"
  ]
  node [
    id 351
    label "przyst&#281;pny"
  ]
  node [
    id 352
    label "sympatyczny"
  ]
  node [
    id 353
    label "dobroczynny"
  ]
  node [
    id 354
    label "czw&#243;rka"
  ]
  node [
    id 355
    label "spokojny"
  ]
  node [
    id 356
    label "skuteczny"
  ]
  node [
    id 357
    label "&#347;mieszny"
  ]
  node [
    id 358
    label "grzeczny"
  ]
  node [
    id 359
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 360
    label "powitanie"
  ]
  node [
    id 361
    label "ca&#322;y"
  ]
  node [
    id 362
    label "zwrot"
  ]
  node [
    id 363
    label "pomy&#347;lny"
  ]
  node [
    id 364
    label "moralny"
  ]
  node [
    id 365
    label "odpowiedni"
  ]
  node [
    id 366
    label "pos&#322;uszny"
  ]
  node [
    id 367
    label "przybywanie"
  ]
  node [
    id 368
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 369
    label "znoszenie"
  ]
  node [
    id 370
    label "odprowadzanie"
  ]
  node [
    id 371
    label "powodowanie"
  ]
  node [
    id 372
    label "cribbage"
  ]
  node [
    id 373
    label "zdejmowanie"
  ]
  node [
    id 374
    label "zmuszanie"
  ]
  node [
    id 375
    label "przepisywanie"
  ]
  node [
    id 376
    label "constriction"
  ]
  node [
    id 377
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 378
    label "gromadzenie_si&#281;"
  ]
  node [
    id 379
    label "levy"
  ]
  node [
    id 380
    label "ciecz"
  ]
  node [
    id 381
    label "gromadzenie"
  ]
  node [
    id 382
    label "kradzenie"
  ]
  node [
    id 383
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 384
    label "przebranie"
  ]
  node [
    id 385
    label "ripple"
  ]
  node [
    id 386
    label "dostawanie_si&#281;"
  ]
  node [
    id 387
    label "kurczenie"
  ]
  node [
    id 388
    label "przewi&#261;zywanie"
  ]
  node [
    id 389
    label "zwi&#281;kszanie_si&#281;"
  ]
  node [
    id 390
    label "docieranie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
]
