graph [
  node [
    id 0
    label "kanter"
    origin "text"
  ]
  node [
    id 1
    label "uorkiestra"
    origin "text"
  ]
  node [
    id 2
    label "wina"
    origin "text"
  ]
  node [
    id 3
    label "lutnia"
  ]
  node [
    id 4
    label "konsekwencja"
  ]
  node [
    id 5
    label "wstyd"
  ]
  node [
    id 6
    label "guilt"
  ]
  node [
    id 7
    label "odczuwa&#263;"
  ]
  node [
    id 8
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 9
    label "skrupienie_si&#281;"
  ]
  node [
    id 10
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 11
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 12
    label "odczucie"
  ]
  node [
    id 13
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 14
    label "koszula_Dejaniry"
  ]
  node [
    id 15
    label "odczuwanie"
  ]
  node [
    id 16
    label "event"
  ]
  node [
    id 17
    label "rezultat"
  ]
  node [
    id 18
    label "skrupianie_si&#281;"
  ]
  node [
    id 19
    label "odczu&#263;"
  ]
  node [
    id 20
    label "chordofon_szarpany"
  ]
  node [
    id 21
    label "srom"
  ]
  node [
    id 22
    label "emocja"
  ]
  node [
    id 23
    label "dishonor"
  ]
  node [
    id 24
    label "konfuzja"
  ]
  node [
    id 25
    label "shame"
  ]
  node [
    id 26
    label "g&#322;upio"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
]
