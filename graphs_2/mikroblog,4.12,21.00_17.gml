graph [
  node [
    id 0
    label "wiedzieliscie"
    origin "text"
  ]
  node [
    id 1
    label "wa&#322;&#281;sa"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 4
    label "doktor"
    origin "text"
  ]
  node [
    id 5
    label "czyj&#347;"
  ]
  node [
    id 6
    label "m&#261;&#380;"
  ]
  node [
    id 7
    label "prywatny"
  ]
  node [
    id 8
    label "ma&#322;&#380;onek"
  ]
  node [
    id 9
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 10
    label "ch&#322;op"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "pan_m&#322;ody"
  ]
  node [
    id 13
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 14
    label "&#347;lubny"
  ]
  node [
    id 15
    label "pan_domu"
  ]
  node [
    id 16
    label "pan_i_w&#322;adca"
  ]
  node [
    id 17
    label "stary"
  ]
  node [
    id 18
    label "debit"
  ]
  node [
    id 19
    label "redaktor"
  ]
  node [
    id 20
    label "druk"
  ]
  node [
    id 21
    label "publikacja"
  ]
  node [
    id 22
    label "nadtytu&#322;"
  ]
  node [
    id 23
    label "szata_graficzna"
  ]
  node [
    id 24
    label "tytulatura"
  ]
  node [
    id 25
    label "wydawa&#263;"
  ]
  node [
    id 26
    label "elevation"
  ]
  node [
    id 27
    label "wyda&#263;"
  ]
  node [
    id 28
    label "mianowaniec"
  ]
  node [
    id 29
    label "poster"
  ]
  node [
    id 30
    label "nazwa"
  ]
  node [
    id 31
    label "podtytu&#322;"
  ]
  node [
    id 32
    label "technika"
  ]
  node [
    id 33
    label "impression"
  ]
  node [
    id 34
    label "pismo"
  ]
  node [
    id 35
    label "glif"
  ]
  node [
    id 36
    label "dese&#324;"
  ]
  node [
    id 37
    label "prohibita"
  ]
  node [
    id 38
    label "cymelium"
  ]
  node [
    id 39
    label "wytw&#243;r"
  ]
  node [
    id 40
    label "tkanina"
  ]
  node [
    id 41
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 42
    label "zaproszenie"
  ]
  node [
    id 43
    label "tekst"
  ]
  node [
    id 44
    label "formatowanie"
  ]
  node [
    id 45
    label "formatowa&#263;"
  ]
  node [
    id 46
    label "zdobnik"
  ]
  node [
    id 47
    label "character"
  ]
  node [
    id 48
    label "printing"
  ]
  node [
    id 49
    label "produkcja"
  ]
  node [
    id 50
    label "notification"
  ]
  node [
    id 51
    label "term"
  ]
  node [
    id 52
    label "wezwanie"
  ]
  node [
    id 53
    label "patron"
  ]
  node [
    id 54
    label "leksem"
  ]
  node [
    id 55
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 56
    label "redakcja"
  ]
  node [
    id 57
    label "wydawnictwo"
  ]
  node [
    id 58
    label "bran&#380;owiec"
  ]
  node [
    id 59
    label "edytor"
  ]
  node [
    id 60
    label "powierzy&#263;"
  ]
  node [
    id 61
    label "pieni&#261;dze"
  ]
  node [
    id 62
    label "plon"
  ]
  node [
    id 63
    label "give"
  ]
  node [
    id 64
    label "skojarzy&#263;"
  ]
  node [
    id 65
    label "d&#378;wi&#281;k"
  ]
  node [
    id 66
    label "zadenuncjowa&#263;"
  ]
  node [
    id 67
    label "impart"
  ]
  node [
    id 68
    label "da&#263;"
  ]
  node [
    id 69
    label "reszta"
  ]
  node [
    id 70
    label "zapach"
  ]
  node [
    id 71
    label "zrobi&#263;"
  ]
  node [
    id 72
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 73
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 74
    label "wiano"
  ]
  node [
    id 75
    label "translate"
  ]
  node [
    id 76
    label "picture"
  ]
  node [
    id 77
    label "poda&#263;"
  ]
  node [
    id 78
    label "wprowadzi&#263;"
  ]
  node [
    id 79
    label "wytworzy&#263;"
  ]
  node [
    id 80
    label "dress"
  ]
  node [
    id 81
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 82
    label "tajemnica"
  ]
  node [
    id 83
    label "panna_na_wydaniu"
  ]
  node [
    id 84
    label "supply"
  ]
  node [
    id 85
    label "ujawni&#263;"
  ]
  node [
    id 86
    label "prawo"
  ]
  node [
    id 87
    label "robi&#263;"
  ]
  node [
    id 88
    label "mie&#263;_miejsce"
  ]
  node [
    id 89
    label "surrender"
  ]
  node [
    id 90
    label "kojarzy&#263;"
  ]
  node [
    id 91
    label "dawa&#263;"
  ]
  node [
    id 92
    label "wprowadza&#263;"
  ]
  node [
    id 93
    label "podawa&#263;"
  ]
  node [
    id 94
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 95
    label "ujawnia&#263;"
  ]
  node [
    id 96
    label "placard"
  ]
  node [
    id 97
    label "powierza&#263;"
  ]
  node [
    id 98
    label "denuncjowa&#263;"
  ]
  node [
    id 99
    label "wytwarza&#263;"
  ]
  node [
    id 100
    label "train"
  ]
  node [
    id 101
    label "urz&#261;d"
  ]
  node [
    id 102
    label "stanowisko"
  ]
  node [
    id 103
    label "mandatariusz"
  ]
  node [
    id 104
    label "afisz"
  ]
  node [
    id 105
    label "dane"
  ]
  node [
    id 106
    label "zbi&#243;r"
  ]
  node [
    id 107
    label "doktoryzowanie_si&#281;"
  ]
  node [
    id 108
    label "pracownik"
  ]
  node [
    id 109
    label "stopie&#324;_naukowy"
  ]
  node [
    id 110
    label "pracownik_naukowy"
  ]
  node [
    id 111
    label "doktorant"
  ]
  node [
    id 112
    label "salariat"
  ]
  node [
    id 113
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 114
    label "delegowanie"
  ]
  node [
    id 115
    label "pracu&#347;"
  ]
  node [
    id 116
    label "r&#281;ka"
  ]
  node [
    id 117
    label "delegowa&#263;"
  ]
  node [
    id 118
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 119
    label "s&#322;uchacz"
  ]
  node [
    id 120
    label "kandydat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
]
