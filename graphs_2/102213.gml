graph [
  node [
    id 0
    label "przyst&#261;pienie"
    origin "text"
  ]
  node [
    id 1
    label "nowa"
    origin "text"
  ]
  node [
    id 2
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 3
    label "cz&#322;onkowski"
    origin "text"
  ]
  node [
    id 4
    label "konwencja"
    origin "text"
  ]
  node [
    id 5
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 6
    label "mowa"
    origin "text"
  ]
  node [
    id 7
    label "ust&#281;p"
    origin "text"
  ]
  node [
    id 8
    label "poni&#380;ej"
    origin "text"
  ]
  node [
    id 9
    label "jak"
    origin "text"
  ]
  node [
    id 10
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 11
    label "bia&#322;oru&#347;"
    origin "text"
  ]
  node [
    id 12
    label "chiny"
    origin "text"
  ]
  node [
    id 13
    label "chile"
    origin "text"
  ]
  node [
    id 14
    label "mercosur"
    origin "text"
  ]
  node [
    id 15
    label "szwajcaria"
    origin "text"
  ]
  node [
    id 16
    label "zawrze&#263;"
    origin "text"
  ]
  node [
    id 17
    label "lub"
    origin "text"
  ]
  node [
    id 18
    label "podpisa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "wsp&#243;lnie"
    origin "text"
  ]
  node [
    id 20
    label "przez"
    origin "text"
  ]
  node [
    id 21
    label "wsp&#243;lnota"
    origin "text"
  ]
  node [
    id 22
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 23
    label "poprzez"
    origin "text"
  ]
  node [
    id 24
    label "zawarcie"
    origin "text"
  ]
  node [
    id 25
    label "protok&#243;&#322;"
    origin "text"
  ]
  node [
    id 26
    label "taki"
    origin "text"
  ]
  node [
    id 27
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 28
    label "rada"
    origin "text"
  ]
  node [
    id 29
    label "unia"
    origin "text"
  ]
  node [
    id 30
    label "europejski"
    origin "text"
  ]
  node [
    id 31
    label "stanowi&#263;"
    origin "text"
  ]
  node [
    id 32
    label "jednomy&#347;lnie"
    origin "text"
  ]
  node [
    id 33
    label "imienie"
    origin "text"
  ]
  node [
    id 34
    label "trzeci"
    origin "text"
  ]
  node [
    id 35
    label "organizacja"
    origin "text"
  ]
  node [
    id 36
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 37
    label "procedura"
    origin "text"
  ]
  node [
    id 38
    label "pozostawa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "bez"
    origin "text"
  ]
  node [
    id 40
    label "uszczerbek"
    origin "text"
  ]
  node [
    id 41
    label "dla"
    origin "text"
  ]
  node [
    id 42
    label "kompetencja"
    origin "text"
  ]
  node [
    id 43
    label "w&#322;asne"
    origin "text"
  ]
  node [
    id 44
    label "wp&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "podzia&#322;"
    origin "text"
  ]
  node [
    id 46
    label "pomi&#281;dzy"
    origin "text"
  ]
  node [
    id 47
    label "odniesienie"
    origin "text"
  ]
  node [
    id 48
    label "zawiera&#263;"
    origin "text"
  ]
  node [
    id 49
    label "przysz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 50
    label "jakikolwiek"
    origin "text"
  ]
  node [
    id 51
    label "inny"
    origin "text"
  ]
  node [
    id 52
    label "zmiana"
    origin "text"
  ]
  node [
    id 53
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 54
    label "komisja"
    origin "text"
  ]
  node [
    id 55
    label "prowadzi&#263;"
    origin "text"
  ]
  node [
    id 56
    label "negocjacja"
    origin "text"
  ]
  node [
    id 57
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 58
    label "tychy"
    origin "text"
  ]
  node [
    id 59
    label "podstawa"
    origin "text"
  ]
  node [
    id 60
    label "dyrektywa"
    origin "text"
  ]
  node [
    id 61
    label "negocjacyjny"
    origin "text"
  ]
  node [
    id 62
    label "zatwierdzi&#263;"
    origin "text"
  ]
  node [
    id 63
    label "konsultacja"
    origin "text"
  ]
  node [
    id 64
    label "komitet"
    origin "text"
  ]
  node [
    id 65
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 66
    label "si&#281;"
    origin "text"
  ]
  node [
    id 67
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 68
    label "przedk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 69
    label "rad"
    origin "text"
  ]
  node [
    id 70
    label "projekt"
    origin "text"
  ]
  node [
    id 71
    label "maja"
    origin "text"
  ]
  node [
    id 72
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 73
    label "entree"
  ]
  node [
    id 74
    label "wej&#347;cie"
  ]
  node [
    id 75
    label "zacz&#281;cie"
  ]
  node [
    id 76
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 77
    label "adhezja"
  ]
  node [
    id 78
    label "ploy"
  ]
  node [
    id 79
    label "pojawienie_si&#281;"
  ]
  node [
    id 80
    label "wnij&#347;cie"
  ]
  node [
    id 81
    label "podw&#243;rze"
  ]
  node [
    id 82
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 83
    label "wzi&#281;cie"
  ]
  node [
    id 84
    label "stimulation"
  ]
  node [
    id 85
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 86
    label "approach"
  ]
  node [
    id 87
    label "urz&#261;dzenie"
  ]
  node [
    id 88
    label "dom"
  ]
  node [
    id 89
    label "wnikni&#281;cie"
  ]
  node [
    id 90
    label "release"
  ]
  node [
    id 91
    label "trespass"
  ]
  node [
    id 92
    label "spotkanie"
  ]
  node [
    id 93
    label "poznanie"
  ]
  node [
    id 94
    label "wzniesienie_si&#281;"
  ]
  node [
    id 95
    label "pocz&#261;tek"
  ]
  node [
    id 96
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 97
    label "wch&#243;d"
  ]
  node [
    id 98
    label "stanie_si&#281;"
  ]
  node [
    id 99
    label "bramka"
  ]
  node [
    id 100
    label "vent"
  ]
  node [
    id 101
    label "zaatakowanie"
  ]
  node [
    id 102
    label "przenikni&#281;cie"
  ]
  node [
    id 103
    label "cz&#322;onek"
  ]
  node [
    id 104
    label "zdarzenie_si&#281;"
  ]
  node [
    id 105
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 106
    label "doj&#347;cie"
  ]
  node [
    id 107
    label "otw&#243;r"
  ]
  node [
    id 108
    label "dostanie_si&#281;"
  ]
  node [
    id 109
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 110
    label "nast&#261;pienie"
  ]
  node [
    id 111
    label "dost&#281;p"
  ]
  node [
    id 112
    label "wpuszczenie"
  ]
  node [
    id 113
    label "przekroczenie"
  ]
  node [
    id 114
    label "opening"
  ]
  node [
    id 115
    label "odj&#281;cie"
  ]
  node [
    id 116
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 117
    label "wyra&#380;enie"
  ]
  node [
    id 118
    label "discourtesy"
  ]
  node [
    id 119
    label "zrobienie"
  ]
  node [
    id 120
    label "post&#261;pienie"
  ]
  node [
    id 121
    label "czynno&#347;&#263;"
  ]
  node [
    id 122
    label "akces"
  ]
  node [
    id 123
    label "uczestnictwo"
  ]
  node [
    id 124
    label "zjawisko"
  ]
  node [
    id 125
    label "wst&#261;pienie"
  ]
  node [
    id 126
    label "gwiazda"
  ]
  node [
    id 127
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 128
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 129
    label "Nibiru"
  ]
  node [
    id 130
    label "supergrupa"
  ]
  node [
    id 131
    label "obiekt"
  ]
  node [
    id 132
    label "konstelacja"
  ]
  node [
    id 133
    label "gromada"
  ]
  node [
    id 134
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 135
    label "ornament"
  ]
  node [
    id 136
    label "promie&#324;"
  ]
  node [
    id 137
    label "agregatka"
  ]
  node [
    id 138
    label "Gwiazda_Polarna"
  ]
  node [
    id 139
    label "Arktur"
  ]
  node [
    id 140
    label "delta_Scuti"
  ]
  node [
    id 141
    label "s&#322;awa"
  ]
  node [
    id 142
    label "S&#322;o&#324;ce"
  ]
  node [
    id 143
    label "gwiazdosz"
  ]
  node [
    id 144
    label "asocjacja_gwiazd"
  ]
  node [
    id 145
    label "star"
  ]
  node [
    id 146
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 147
    label "kszta&#322;t"
  ]
  node [
    id 148
    label "&#347;wiat&#322;o"
  ]
  node [
    id 149
    label "Japonia"
  ]
  node [
    id 150
    label "Zair"
  ]
  node [
    id 151
    label "Belize"
  ]
  node [
    id 152
    label "San_Marino"
  ]
  node [
    id 153
    label "Tanzania"
  ]
  node [
    id 154
    label "Antigua_i_Barbuda"
  ]
  node [
    id 155
    label "granica_pa&#324;stwa"
  ]
  node [
    id 156
    label "Senegal"
  ]
  node [
    id 157
    label "Seszele"
  ]
  node [
    id 158
    label "Mauretania"
  ]
  node [
    id 159
    label "Indie"
  ]
  node [
    id 160
    label "Filipiny"
  ]
  node [
    id 161
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 162
    label "Zimbabwe"
  ]
  node [
    id 163
    label "Malezja"
  ]
  node [
    id 164
    label "Rumunia"
  ]
  node [
    id 165
    label "Surinam"
  ]
  node [
    id 166
    label "Ukraina"
  ]
  node [
    id 167
    label "Syria"
  ]
  node [
    id 168
    label "Wyspy_Marshalla"
  ]
  node [
    id 169
    label "Burkina_Faso"
  ]
  node [
    id 170
    label "Grecja"
  ]
  node [
    id 171
    label "Polska"
  ]
  node [
    id 172
    label "Wenezuela"
  ]
  node [
    id 173
    label "Suazi"
  ]
  node [
    id 174
    label "Nepal"
  ]
  node [
    id 175
    label "S&#322;owacja"
  ]
  node [
    id 176
    label "Algieria"
  ]
  node [
    id 177
    label "Chiny"
  ]
  node [
    id 178
    label "Grenada"
  ]
  node [
    id 179
    label "Barbados"
  ]
  node [
    id 180
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 181
    label "Pakistan"
  ]
  node [
    id 182
    label "Niemcy"
  ]
  node [
    id 183
    label "Bahrajn"
  ]
  node [
    id 184
    label "Komory"
  ]
  node [
    id 185
    label "Australia"
  ]
  node [
    id 186
    label "Rodezja"
  ]
  node [
    id 187
    label "Malawi"
  ]
  node [
    id 188
    label "Gwinea"
  ]
  node [
    id 189
    label "Wehrlen"
  ]
  node [
    id 190
    label "Meksyk"
  ]
  node [
    id 191
    label "Liechtenstein"
  ]
  node [
    id 192
    label "Czarnog&#243;ra"
  ]
  node [
    id 193
    label "Wielka_Brytania"
  ]
  node [
    id 194
    label "Kuwejt"
  ]
  node [
    id 195
    label "Monako"
  ]
  node [
    id 196
    label "Angola"
  ]
  node [
    id 197
    label "Jemen"
  ]
  node [
    id 198
    label "Etiopia"
  ]
  node [
    id 199
    label "Madagaskar"
  ]
  node [
    id 200
    label "terytorium"
  ]
  node [
    id 201
    label "Kolumbia"
  ]
  node [
    id 202
    label "Portoryko"
  ]
  node [
    id 203
    label "Mauritius"
  ]
  node [
    id 204
    label "Kostaryka"
  ]
  node [
    id 205
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 206
    label "Tajlandia"
  ]
  node [
    id 207
    label "Argentyna"
  ]
  node [
    id 208
    label "Zambia"
  ]
  node [
    id 209
    label "Sri_Lanka"
  ]
  node [
    id 210
    label "Gwatemala"
  ]
  node [
    id 211
    label "Kirgistan"
  ]
  node [
    id 212
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 213
    label "Hiszpania"
  ]
  node [
    id 214
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 215
    label "Salwador"
  ]
  node [
    id 216
    label "Korea"
  ]
  node [
    id 217
    label "Macedonia"
  ]
  node [
    id 218
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 219
    label "Brunei"
  ]
  node [
    id 220
    label "Mozambik"
  ]
  node [
    id 221
    label "Turcja"
  ]
  node [
    id 222
    label "Kambod&#380;a"
  ]
  node [
    id 223
    label "Benin"
  ]
  node [
    id 224
    label "Bhutan"
  ]
  node [
    id 225
    label "Tunezja"
  ]
  node [
    id 226
    label "Austria"
  ]
  node [
    id 227
    label "Izrael"
  ]
  node [
    id 228
    label "Sierra_Leone"
  ]
  node [
    id 229
    label "Jamajka"
  ]
  node [
    id 230
    label "Rosja"
  ]
  node [
    id 231
    label "Rwanda"
  ]
  node [
    id 232
    label "holoarktyka"
  ]
  node [
    id 233
    label "Nigeria"
  ]
  node [
    id 234
    label "USA"
  ]
  node [
    id 235
    label "Oman"
  ]
  node [
    id 236
    label "Luksemburg"
  ]
  node [
    id 237
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 238
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 239
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 240
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 241
    label "Dominikana"
  ]
  node [
    id 242
    label "Irlandia"
  ]
  node [
    id 243
    label "Liban"
  ]
  node [
    id 244
    label "Hanower"
  ]
  node [
    id 245
    label "Estonia"
  ]
  node [
    id 246
    label "Iran"
  ]
  node [
    id 247
    label "Nowa_Zelandia"
  ]
  node [
    id 248
    label "Gabon"
  ]
  node [
    id 249
    label "Samoa"
  ]
  node [
    id 250
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 251
    label "S&#322;owenia"
  ]
  node [
    id 252
    label "Kiribati"
  ]
  node [
    id 253
    label "Egipt"
  ]
  node [
    id 254
    label "Togo"
  ]
  node [
    id 255
    label "Mongolia"
  ]
  node [
    id 256
    label "Sudan"
  ]
  node [
    id 257
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 258
    label "Bahamy"
  ]
  node [
    id 259
    label "Bangladesz"
  ]
  node [
    id 260
    label "partia"
  ]
  node [
    id 261
    label "Serbia"
  ]
  node [
    id 262
    label "Czechy"
  ]
  node [
    id 263
    label "Holandia"
  ]
  node [
    id 264
    label "Birma"
  ]
  node [
    id 265
    label "Albania"
  ]
  node [
    id 266
    label "Mikronezja"
  ]
  node [
    id 267
    label "Gambia"
  ]
  node [
    id 268
    label "Kazachstan"
  ]
  node [
    id 269
    label "interior"
  ]
  node [
    id 270
    label "Uzbekistan"
  ]
  node [
    id 271
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 272
    label "Malta"
  ]
  node [
    id 273
    label "Lesoto"
  ]
  node [
    id 274
    label "para"
  ]
  node [
    id 275
    label "Antarktis"
  ]
  node [
    id 276
    label "Andora"
  ]
  node [
    id 277
    label "Nauru"
  ]
  node [
    id 278
    label "Kuba"
  ]
  node [
    id 279
    label "Wietnam"
  ]
  node [
    id 280
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 281
    label "ziemia"
  ]
  node [
    id 282
    label "Kamerun"
  ]
  node [
    id 283
    label "Chorwacja"
  ]
  node [
    id 284
    label "Urugwaj"
  ]
  node [
    id 285
    label "Niger"
  ]
  node [
    id 286
    label "Turkmenistan"
  ]
  node [
    id 287
    label "Szwajcaria"
  ]
  node [
    id 288
    label "zwrot"
  ]
  node [
    id 289
    label "grupa"
  ]
  node [
    id 290
    label "Palau"
  ]
  node [
    id 291
    label "Litwa"
  ]
  node [
    id 292
    label "Gruzja"
  ]
  node [
    id 293
    label "Tajwan"
  ]
  node [
    id 294
    label "Kongo"
  ]
  node [
    id 295
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 296
    label "Honduras"
  ]
  node [
    id 297
    label "Boliwia"
  ]
  node [
    id 298
    label "Uganda"
  ]
  node [
    id 299
    label "Namibia"
  ]
  node [
    id 300
    label "Azerbejd&#380;an"
  ]
  node [
    id 301
    label "Erytrea"
  ]
  node [
    id 302
    label "Gujana"
  ]
  node [
    id 303
    label "Panama"
  ]
  node [
    id 304
    label "Somalia"
  ]
  node [
    id 305
    label "Burundi"
  ]
  node [
    id 306
    label "Tuwalu"
  ]
  node [
    id 307
    label "Libia"
  ]
  node [
    id 308
    label "Katar"
  ]
  node [
    id 309
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 310
    label "Sahara_Zachodnia"
  ]
  node [
    id 311
    label "Trynidad_i_Tobago"
  ]
  node [
    id 312
    label "Gwinea_Bissau"
  ]
  node [
    id 313
    label "Bu&#322;garia"
  ]
  node [
    id 314
    label "Fid&#380;i"
  ]
  node [
    id 315
    label "Nikaragua"
  ]
  node [
    id 316
    label "Tonga"
  ]
  node [
    id 317
    label "Timor_Wschodni"
  ]
  node [
    id 318
    label "Laos"
  ]
  node [
    id 319
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 320
    label "Ghana"
  ]
  node [
    id 321
    label "Brazylia"
  ]
  node [
    id 322
    label "Belgia"
  ]
  node [
    id 323
    label "Irak"
  ]
  node [
    id 324
    label "Peru"
  ]
  node [
    id 325
    label "Arabia_Saudyjska"
  ]
  node [
    id 326
    label "Indonezja"
  ]
  node [
    id 327
    label "Malediwy"
  ]
  node [
    id 328
    label "Afganistan"
  ]
  node [
    id 329
    label "Jordania"
  ]
  node [
    id 330
    label "Kenia"
  ]
  node [
    id 331
    label "Czad"
  ]
  node [
    id 332
    label "Liberia"
  ]
  node [
    id 333
    label "W&#281;gry"
  ]
  node [
    id 334
    label "Chile"
  ]
  node [
    id 335
    label "Mali"
  ]
  node [
    id 336
    label "Armenia"
  ]
  node [
    id 337
    label "Kanada"
  ]
  node [
    id 338
    label "Cypr"
  ]
  node [
    id 339
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 340
    label "Ekwador"
  ]
  node [
    id 341
    label "Mo&#322;dawia"
  ]
  node [
    id 342
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 343
    label "W&#322;ochy"
  ]
  node [
    id 344
    label "Wyspy_Salomona"
  ]
  node [
    id 345
    label "&#321;otwa"
  ]
  node [
    id 346
    label "D&#380;ibuti"
  ]
  node [
    id 347
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 348
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 349
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 350
    label "Portugalia"
  ]
  node [
    id 351
    label "Botswana"
  ]
  node [
    id 352
    label "Maroko"
  ]
  node [
    id 353
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 354
    label "Francja"
  ]
  node [
    id 355
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 356
    label "Dominika"
  ]
  node [
    id 357
    label "Paragwaj"
  ]
  node [
    id 358
    label "Tad&#380;ykistan"
  ]
  node [
    id 359
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 360
    label "Haiti"
  ]
  node [
    id 361
    label "Khitai"
  ]
  node [
    id 362
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 363
    label "poker"
  ]
  node [
    id 364
    label "nale&#380;e&#263;"
  ]
  node [
    id 365
    label "odparowanie"
  ]
  node [
    id 366
    label "sztuka"
  ]
  node [
    id 367
    label "smoke"
  ]
  node [
    id 368
    label "odparowa&#263;"
  ]
  node [
    id 369
    label "parowanie"
  ]
  node [
    id 370
    label "chodzi&#263;"
  ]
  node [
    id 371
    label "pair"
  ]
  node [
    id 372
    label "zbi&#243;r"
  ]
  node [
    id 373
    label "uk&#322;ad"
  ]
  node [
    id 374
    label "odparowywa&#263;"
  ]
  node [
    id 375
    label "dodatek"
  ]
  node [
    id 376
    label "odparowywanie"
  ]
  node [
    id 377
    label "jednostka_monetarna"
  ]
  node [
    id 378
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 379
    label "moneta"
  ]
  node [
    id 380
    label "damp"
  ]
  node [
    id 381
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 382
    label "wyparowanie"
  ]
  node [
    id 383
    label "gaz_cieplarniany"
  ]
  node [
    id 384
    label "gaz"
  ]
  node [
    id 385
    label "zesp&#243;&#322;"
  ]
  node [
    id 386
    label "jednostka_administracyjna"
  ]
  node [
    id 387
    label "Wile&#324;szczyzna"
  ]
  node [
    id 388
    label "obszar"
  ]
  node [
    id 389
    label "Jukon"
  ]
  node [
    id 390
    label "przybud&#243;wka"
  ]
  node [
    id 391
    label "struktura"
  ]
  node [
    id 392
    label "organization"
  ]
  node [
    id 393
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 394
    label "od&#322;am"
  ]
  node [
    id 395
    label "TOPR"
  ]
  node [
    id 396
    label "komitet_koordynacyjny"
  ]
  node [
    id 397
    label "przedstawicielstwo"
  ]
  node [
    id 398
    label "ZMP"
  ]
  node [
    id 399
    label "Cepelia"
  ]
  node [
    id 400
    label "GOPR"
  ]
  node [
    id 401
    label "endecki"
  ]
  node [
    id 402
    label "ZBoWiD"
  ]
  node [
    id 403
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 404
    label "podmiot"
  ]
  node [
    id 405
    label "boj&#243;wka"
  ]
  node [
    id 406
    label "ZOMO"
  ]
  node [
    id 407
    label "jednostka_organizacyjna"
  ]
  node [
    id 408
    label "centrala"
  ]
  node [
    id 409
    label "punkt"
  ]
  node [
    id 410
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 411
    label "turn"
  ]
  node [
    id 412
    label "fraza_czasownikowa"
  ]
  node [
    id 413
    label "turning"
  ]
  node [
    id 414
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 415
    label "skr&#281;t"
  ]
  node [
    id 416
    label "jednostka_leksykalna"
  ]
  node [
    id 417
    label "obr&#243;t"
  ]
  node [
    id 418
    label "asymilowa&#263;"
  ]
  node [
    id 419
    label "kompozycja"
  ]
  node [
    id 420
    label "pakiet_klimatyczny"
  ]
  node [
    id 421
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 422
    label "type"
  ]
  node [
    id 423
    label "cz&#261;steczka"
  ]
  node [
    id 424
    label "specgrupa"
  ]
  node [
    id 425
    label "egzemplarz"
  ]
  node [
    id 426
    label "stage_set"
  ]
  node [
    id 427
    label "asymilowanie"
  ]
  node [
    id 428
    label "odm&#322;odzenie"
  ]
  node [
    id 429
    label "odm&#322;adza&#263;"
  ]
  node [
    id 430
    label "harcerze_starsi"
  ]
  node [
    id 431
    label "jednostka_systematyczna"
  ]
  node [
    id 432
    label "oddzia&#322;"
  ]
  node [
    id 433
    label "category"
  ]
  node [
    id 434
    label "liga"
  ]
  node [
    id 435
    label "&#346;wietliki"
  ]
  node [
    id 436
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 437
    label "formacja_geologiczna"
  ]
  node [
    id 438
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 439
    label "Eurogrupa"
  ]
  node [
    id 440
    label "Terranie"
  ]
  node [
    id 441
    label "odm&#322;adzanie"
  ]
  node [
    id 442
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 443
    label "Entuzjastki"
  ]
  node [
    id 444
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 445
    label "AWS"
  ]
  node [
    id 446
    label "ZChN"
  ]
  node [
    id 447
    label "Bund"
  ]
  node [
    id 448
    label "PPR"
  ]
  node [
    id 449
    label "blok"
  ]
  node [
    id 450
    label "egzekutywa"
  ]
  node [
    id 451
    label "Wigowie"
  ]
  node [
    id 452
    label "aktyw"
  ]
  node [
    id 453
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 454
    label "Razem"
  ]
  node [
    id 455
    label "unit"
  ]
  node [
    id 456
    label "wybranka"
  ]
  node [
    id 457
    label "SLD"
  ]
  node [
    id 458
    label "ZSL"
  ]
  node [
    id 459
    label "Kuomintang"
  ]
  node [
    id 460
    label "si&#322;a"
  ]
  node [
    id 461
    label "PiS"
  ]
  node [
    id 462
    label "gra"
  ]
  node [
    id 463
    label "Jakobici"
  ]
  node [
    id 464
    label "materia&#322;"
  ]
  node [
    id 465
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 466
    label "package"
  ]
  node [
    id 467
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 468
    label "PO"
  ]
  node [
    id 469
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 470
    label "game"
  ]
  node [
    id 471
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 472
    label "wybranek"
  ]
  node [
    id 473
    label "niedoczas"
  ]
  node [
    id 474
    label "Federali&#347;ci"
  ]
  node [
    id 475
    label "PSL"
  ]
  node [
    id 476
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 477
    label "plant"
  ]
  node [
    id 478
    label "przyroda"
  ]
  node [
    id 479
    label "ro&#347;lina"
  ]
  node [
    id 480
    label "formacja_ro&#347;linna"
  ]
  node [
    id 481
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 482
    label "biom"
  ]
  node [
    id 483
    label "geosystem"
  ]
  node [
    id 484
    label "szata_ro&#347;linna"
  ]
  node [
    id 485
    label "zielono&#347;&#263;"
  ]
  node [
    id 486
    label "pi&#281;tro"
  ]
  node [
    id 487
    label "teren"
  ]
  node [
    id 488
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 489
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 490
    label "Kaszmir"
  ]
  node [
    id 491
    label "Pend&#380;ab"
  ]
  node [
    id 492
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 493
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 494
    label "funt_liba&#324;ski"
  ]
  node [
    id 495
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 496
    label "Pozna&#324;"
  ]
  node [
    id 497
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 498
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 499
    label "Gozo"
  ]
  node [
    id 500
    label "lira_malta&#324;ska"
  ]
  node [
    id 501
    label "strefa_euro"
  ]
  node [
    id 502
    label "Afryka_Zachodnia"
  ]
  node [
    id 503
    label "Afryka_Wschodnia"
  ]
  node [
    id 504
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 505
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 506
    label "dolar_namibijski"
  ]
  node [
    id 507
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 508
    label "NATO"
  ]
  node [
    id 509
    label "escudo_portugalskie"
  ]
  node [
    id 510
    label "milrejs"
  ]
  node [
    id 511
    label "Wielka_Bahama"
  ]
  node [
    id 512
    label "dolar_bahamski"
  ]
  node [
    id 513
    label "Karaiby"
  ]
  node [
    id 514
    label "dolar_liberyjski"
  ]
  node [
    id 515
    label "riel"
  ]
  node [
    id 516
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 517
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 518
    label "&#321;adoga"
  ]
  node [
    id 519
    label "Dniepr"
  ]
  node [
    id 520
    label "Kamczatka"
  ]
  node [
    id 521
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 522
    label "Witim"
  ]
  node [
    id 523
    label "Tuwa"
  ]
  node [
    id 524
    label "Czeczenia"
  ]
  node [
    id 525
    label "Ajon"
  ]
  node [
    id 526
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 527
    label "car"
  ]
  node [
    id 528
    label "Karelia"
  ]
  node [
    id 529
    label "Don"
  ]
  node [
    id 530
    label "Mordowia"
  ]
  node [
    id 531
    label "Czuwaszja"
  ]
  node [
    id 532
    label "Udmurcja"
  ]
  node [
    id 533
    label "Jama&#322;"
  ]
  node [
    id 534
    label "Azja"
  ]
  node [
    id 535
    label "Newa"
  ]
  node [
    id 536
    label "Adygeja"
  ]
  node [
    id 537
    label "Inguszetia"
  ]
  node [
    id 538
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 539
    label "Mari_El"
  ]
  node [
    id 540
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 541
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 542
    label "Wszechrosja"
  ]
  node [
    id 543
    label "rubel_rosyjski"
  ]
  node [
    id 544
    label "s&#322;owianofilstwo"
  ]
  node [
    id 545
    label "Dagestan"
  ]
  node [
    id 546
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 547
    label "Komi"
  ]
  node [
    id 548
    label "Tatarstan"
  ]
  node [
    id 549
    label "Baszkiria"
  ]
  node [
    id 550
    label "Perm"
  ]
  node [
    id 551
    label "Chakasja"
  ]
  node [
    id 552
    label "Syberia"
  ]
  node [
    id 553
    label "Europa_Wschodnia"
  ]
  node [
    id 554
    label "Anadyr"
  ]
  node [
    id 555
    label "lew"
  ]
  node [
    id 556
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 557
    label "Unia_Europejska"
  ]
  node [
    id 558
    label "Dobrud&#380;a"
  ]
  node [
    id 559
    label "Judea"
  ]
  node [
    id 560
    label "lira_izraelska"
  ]
  node [
    id 561
    label "Galilea"
  ]
  node [
    id 562
    label "szekel"
  ]
  node [
    id 563
    label "Luksemburgia"
  ]
  node [
    id 564
    label "Flandria"
  ]
  node [
    id 565
    label "Brabancja"
  ]
  node [
    id 566
    label "Limburgia"
  ]
  node [
    id 567
    label "Walonia"
  ]
  node [
    id 568
    label "frank_belgijski"
  ]
  node [
    id 569
    label "Niderlandy"
  ]
  node [
    id 570
    label "dinar_iracki"
  ]
  node [
    id 571
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 572
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 573
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 574
    label "Maghreb"
  ]
  node [
    id 575
    label "szyling_ugandyjski"
  ]
  node [
    id 576
    label "kafar"
  ]
  node [
    id 577
    label "dolar_jamajski"
  ]
  node [
    id 578
    label "Borneo"
  ]
  node [
    id 579
    label "ringgit"
  ]
  node [
    id 580
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 581
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 582
    label "dolar_surinamski"
  ]
  node [
    id 583
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 584
    label "funt_suda&#324;ski"
  ]
  node [
    id 585
    label "dolar_guja&#324;ski"
  ]
  node [
    id 586
    label "Manica"
  ]
  node [
    id 587
    label "Inhambane"
  ]
  node [
    id 588
    label "Maputo"
  ]
  node [
    id 589
    label "Nampula"
  ]
  node [
    id 590
    label "metical"
  ]
  node [
    id 591
    label "escudo_mozambickie"
  ]
  node [
    id 592
    label "Gaza"
  ]
  node [
    id 593
    label "Niasa"
  ]
  node [
    id 594
    label "Cabo_Delgado"
  ]
  node [
    id 595
    label "Sahara"
  ]
  node [
    id 596
    label "sol"
  ]
  node [
    id 597
    label "inti"
  ]
  node [
    id 598
    label "kip"
  ]
  node [
    id 599
    label "Pireneje"
  ]
  node [
    id 600
    label "euro"
  ]
  node [
    id 601
    label "kwacha_zambijska"
  ]
  node [
    id 602
    label "tugrik"
  ]
  node [
    id 603
    label "Azja_Wschodnia"
  ]
  node [
    id 604
    label "ajmak"
  ]
  node [
    id 605
    label "Buriaci"
  ]
  node [
    id 606
    label "Ameryka_Centralna"
  ]
  node [
    id 607
    label "balboa"
  ]
  node [
    id 608
    label "dolar"
  ]
  node [
    id 609
    label "Zelandia"
  ]
  node [
    id 610
    label "gulden"
  ]
  node [
    id 611
    label "manat_turkme&#324;ski"
  ]
  node [
    id 612
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 613
    label "dolar_Tuvalu"
  ]
  node [
    id 614
    label "Polinezja"
  ]
  node [
    id 615
    label "Katanga"
  ]
  node [
    id 616
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 617
    label "zair"
  ]
  node [
    id 618
    label "frank_szwajcarski"
  ]
  node [
    id 619
    label "Europa_Zachodnia"
  ]
  node [
    id 620
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 621
    label "dolar_Belize"
  ]
  node [
    id 622
    label "Jukatan"
  ]
  node [
    id 623
    label "colon"
  ]
  node [
    id 624
    label "Dyja"
  ]
  node [
    id 625
    label "Izera"
  ]
  node [
    id 626
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 627
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 628
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 629
    label "Lasko"
  ]
  node [
    id 630
    label "korona_czeska"
  ]
  node [
    id 631
    label "ugija"
  ]
  node [
    id 632
    label "szyling_kenijski"
  ]
  node [
    id 633
    label "Karabach"
  ]
  node [
    id 634
    label "manat_azerski"
  ]
  node [
    id 635
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 636
    label "Nachiczewan"
  ]
  node [
    id 637
    label "Bengal"
  ]
  node [
    id 638
    label "taka"
  ]
  node [
    id 639
    label "dolar_Kiribati"
  ]
  node [
    id 640
    label "Ocean_Spokojny"
  ]
  node [
    id 641
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 642
    label "Cebu"
  ]
  node [
    id 643
    label "peso_filipi&#324;skie"
  ]
  node [
    id 644
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 645
    label "Ulster"
  ]
  node [
    id 646
    label "Atlantyk"
  ]
  node [
    id 647
    label "funt_irlandzki"
  ]
  node [
    id 648
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 649
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 650
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 651
    label "cedi"
  ]
  node [
    id 652
    label "ariary"
  ]
  node [
    id 653
    label "Ocean_Indyjski"
  ]
  node [
    id 654
    label "frank_malgaski"
  ]
  node [
    id 655
    label "Walencja"
  ]
  node [
    id 656
    label "Galicja"
  ]
  node [
    id 657
    label "Aragonia"
  ]
  node [
    id 658
    label "Estremadura"
  ]
  node [
    id 659
    label "Rzym_Zachodni"
  ]
  node [
    id 660
    label "Baskonia"
  ]
  node [
    id 661
    label "Andaluzja"
  ]
  node [
    id 662
    label "Katalonia"
  ]
  node [
    id 663
    label "Majorka"
  ]
  node [
    id 664
    label "Asturia"
  ]
  node [
    id 665
    label "Kastylia"
  ]
  node [
    id 666
    label "peseta"
  ]
  node [
    id 667
    label "hacjender"
  ]
  node [
    id 668
    label "peso_chilijskie"
  ]
  node [
    id 669
    label "rupia_indyjska"
  ]
  node [
    id 670
    label "Kerala"
  ]
  node [
    id 671
    label "Indie_Zachodnie"
  ]
  node [
    id 672
    label "Indie_Wschodnie"
  ]
  node [
    id 673
    label "Asam"
  ]
  node [
    id 674
    label "Indie_Portugalskie"
  ]
  node [
    id 675
    label "Bollywood"
  ]
  node [
    id 676
    label "Sikkim"
  ]
  node [
    id 677
    label "jen"
  ]
  node [
    id 678
    label "Okinawa"
  ]
  node [
    id 679
    label "jinja"
  ]
  node [
    id 680
    label "Japonica"
  ]
  node [
    id 681
    label "Karlsbad"
  ]
  node [
    id 682
    label "Turyngia"
  ]
  node [
    id 683
    label "Brandenburgia"
  ]
  node [
    id 684
    label "marka"
  ]
  node [
    id 685
    label "Saksonia"
  ]
  node [
    id 686
    label "Szlezwik"
  ]
  node [
    id 687
    label "Niemcy_Wschodnie"
  ]
  node [
    id 688
    label "Frankonia"
  ]
  node [
    id 689
    label "Rugia"
  ]
  node [
    id 690
    label "Helgoland"
  ]
  node [
    id 691
    label "Bawaria"
  ]
  node [
    id 692
    label "Holsztyn"
  ]
  node [
    id 693
    label "Badenia"
  ]
  node [
    id 694
    label "Wirtembergia"
  ]
  node [
    id 695
    label "Nadrenia"
  ]
  node [
    id 696
    label "Anglosas"
  ]
  node [
    id 697
    label "Hesja"
  ]
  node [
    id 698
    label "Dolna_Saksonia"
  ]
  node [
    id 699
    label "Niemcy_Zachodnie"
  ]
  node [
    id 700
    label "Germania"
  ]
  node [
    id 701
    label "Po&#322;abie"
  ]
  node [
    id 702
    label "Szwabia"
  ]
  node [
    id 703
    label "Westfalia"
  ]
  node [
    id 704
    label "Romania"
  ]
  node [
    id 705
    label "Ok&#281;cie"
  ]
  node [
    id 706
    label "Kalabria"
  ]
  node [
    id 707
    label "Liguria"
  ]
  node [
    id 708
    label "Apulia"
  ]
  node [
    id 709
    label "Piemont"
  ]
  node [
    id 710
    label "Umbria"
  ]
  node [
    id 711
    label "lir"
  ]
  node [
    id 712
    label "Lombardia"
  ]
  node [
    id 713
    label "Warszawa"
  ]
  node [
    id 714
    label "Karyntia"
  ]
  node [
    id 715
    label "Sardynia"
  ]
  node [
    id 716
    label "Toskania"
  ]
  node [
    id 717
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 718
    label "Italia"
  ]
  node [
    id 719
    label "Kampania"
  ]
  node [
    id 720
    label "Sycylia"
  ]
  node [
    id 721
    label "Dacja"
  ]
  node [
    id 722
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 723
    label "lej_rumu&#324;ski"
  ]
  node [
    id 724
    label "Ba&#322;kany"
  ]
  node [
    id 725
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 726
    label "Siedmiogr&#243;d"
  ]
  node [
    id 727
    label "alawizm"
  ]
  node [
    id 728
    label "funt_syryjski"
  ]
  node [
    id 729
    label "frank_rwandyjski"
  ]
  node [
    id 730
    label "dinar_Bahrajnu"
  ]
  node [
    id 731
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 732
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 733
    label "frank_luksemburski"
  ]
  node [
    id 734
    label "peso_kuba&#324;skie"
  ]
  node [
    id 735
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 736
    label "frank_monakijski"
  ]
  node [
    id 737
    label "dinar_algierski"
  ]
  node [
    id 738
    label "Kabylia"
  ]
  node [
    id 739
    label "Oceania"
  ]
  node [
    id 740
    label "Sand&#380;ak"
  ]
  node [
    id 741
    label "Wojwodina"
  ]
  node [
    id 742
    label "dinar_serbski"
  ]
  node [
    id 743
    label "boliwar"
  ]
  node [
    id 744
    label "Orinoko"
  ]
  node [
    id 745
    label "tenge"
  ]
  node [
    id 746
    label "lek"
  ]
  node [
    id 747
    label "frank_alba&#324;ski"
  ]
  node [
    id 748
    label "dolar_Barbadosu"
  ]
  node [
    id 749
    label "Antyle"
  ]
  node [
    id 750
    label "Arakan"
  ]
  node [
    id 751
    label "kyat"
  ]
  node [
    id 752
    label "c&#243;rdoba"
  ]
  node [
    id 753
    label "Lesbos"
  ]
  node [
    id 754
    label "Tesalia"
  ]
  node [
    id 755
    label "Eolia"
  ]
  node [
    id 756
    label "panhellenizm"
  ]
  node [
    id 757
    label "Achaja"
  ]
  node [
    id 758
    label "Kreta"
  ]
  node [
    id 759
    label "Peloponez"
  ]
  node [
    id 760
    label "Olimp"
  ]
  node [
    id 761
    label "drachma"
  ]
  node [
    id 762
    label "Rodos"
  ]
  node [
    id 763
    label "Termopile"
  ]
  node [
    id 764
    label "palestra"
  ]
  node [
    id 765
    label "Eubea"
  ]
  node [
    id 766
    label "Paros"
  ]
  node [
    id 767
    label "Hellada"
  ]
  node [
    id 768
    label "Beocja"
  ]
  node [
    id 769
    label "Parnas"
  ]
  node [
    id 770
    label "Etolia"
  ]
  node [
    id 771
    label "Attyka"
  ]
  node [
    id 772
    label "Epir"
  ]
  node [
    id 773
    label "Mariany"
  ]
  node [
    id 774
    label "Tyrol"
  ]
  node [
    id 775
    label "Salzburg"
  ]
  node [
    id 776
    label "konsulent"
  ]
  node [
    id 777
    label "Rakuzy"
  ]
  node [
    id 778
    label "szyling_austryjacki"
  ]
  node [
    id 779
    label "Amhara"
  ]
  node [
    id 780
    label "negus"
  ]
  node [
    id 781
    label "birr"
  ]
  node [
    id 782
    label "Syjon"
  ]
  node [
    id 783
    label "rupia_indonezyjska"
  ]
  node [
    id 784
    label "Jawa"
  ]
  node [
    id 785
    label "Moluki"
  ]
  node [
    id 786
    label "Nowa_Gwinea"
  ]
  node [
    id 787
    label "Sumatra"
  ]
  node [
    id 788
    label "boliviano"
  ]
  node [
    id 789
    label "frank_francuski"
  ]
  node [
    id 790
    label "Lotaryngia"
  ]
  node [
    id 791
    label "Alzacja"
  ]
  node [
    id 792
    label "Gwadelupa"
  ]
  node [
    id 793
    label "Bordeaux"
  ]
  node [
    id 794
    label "Pikardia"
  ]
  node [
    id 795
    label "Sabaudia"
  ]
  node [
    id 796
    label "Korsyka"
  ]
  node [
    id 797
    label "Bretania"
  ]
  node [
    id 798
    label "Masyw_Centralny"
  ]
  node [
    id 799
    label "Armagnac"
  ]
  node [
    id 800
    label "Akwitania"
  ]
  node [
    id 801
    label "Wandea"
  ]
  node [
    id 802
    label "Martynika"
  ]
  node [
    id 803
    label "Prowansja"
  ]
  node [
    id 804
    label "Sekwana"
  ]
  node [
    id 805
    label "Normandia"
  ]
  node [
    id 806
    label "Burgundia"
  ]
  node [
    id 807
    label "Gaskonia"
  ]
  node [
    id 808
    label "Langwedocja"
  ]
  node [
    id 809
    label "somoni"
  ]
  node [
    id 810
    label "Melanezja"
  ]
  node [
    id 811
    label "dolar_Fid&#380;i"
  ]
  node [
    id 812
    label "Afrodyzje"
  ]
  node [
    id 813
    label "funt_cypryjski"
  ]
  node [
    id 814
    label "peso_dominika&#324;skie"
  ]
  node [
    id 815
    label "Fryburg"
  ]
  node [
    id 816
    label "Bazylea"
  ]
  node [
    id 817
    label "Helwecja"
  ]
  node [
    id 818
    label "Alpy"
  ]
  node [
    id 819
    label "Berno"
  ]
  node [
    id 820
    label "sum"
  ]
  node [
    id 821
    label "Karaka&#322;pacja"
  ]
  node [
    id 822
    label "Liwonia"
  ]
  node [
    id 823
    label "Kurlandia"
  ]
  node [
    id 824
    label "rubel_&#322;otewski"
  ]
  node [
    id 825
    label "&#322;at"
  ]
  node [
    id 826
    label "Windawa"
  ]
  node [
    id 827
    label "Inflanty"
  ]
  node [
    id 828
    label "&#379;mud&#378;"
  ]
  node [
    id 829
    label "lit"
  ]
  node [
    id 830
    label "dinar_tunezyjski"
  ]
  node [
    id 831
    label "frank_tunezyjski"
  ]
  node [
    id 832
    label "lempira"
  ]
  node [
    id 833
    label "Lipt&#243;w"
  ]
  node [
    id 834
    label "korona_w&#281;gierska"
  ]
  node [
    id 835
    label "forint"
  ]
  node [
    id 836
    label "dong"
  ]
  node [
    id 837
    label "Annam"
  ]
  node [
    id 838
    label "Tonkin"
  ]
  node [
    id 839
    label "lud"
  ]
  node [
    id 840
    label "frank_kongijski"
  ]
  node [
    id 841
    label "szyling_somalijski"
  ]
  node [
    id 842
    label "real"
  ]
  node [
    id 843
    label "cruzado"
  ]
  node [
    id 844
    label "Ma&#322;orosja"
  ]
  node [
    id 845
    label "Podole"
  ]
  node [
    id 846
    label "Nadbu&#380;e"
  ]
  node [
    id 847
    label "Przykarpacie"
  ]
  node [
    id 848
    label "Zaporo&#380;e"
  ]
  node [
    id 849
    label "Kozaczyzna"
  ]
  node [
    id 850
    label "Naddnieprze"
  ]
  node [
    id 851
    label "Wsch&#243;d"
  ]
  node [
    id 852
    label "karbowaniec"
  ]
  node [
    id 853
    label "hrywna"
  ]
  node [
    id 854
    label "Ukraina_Zachodnia"
  ]
  node [
    id 855
    label "Dniestr"
  ]
  node [
    id 856
    label "Krym"
  ]
  node [
    id 857
    label "Zakarpacie"
  ]
  node [
    id 858
    label "Wo&#322;y&#324;"
  ]
  node [
    id 859
    label "Tasmania"
  ]
  node [
    id 860
    label "dolar_australijski"
  ]
  node [
    id 861
    label "Nowy_&#346;wiat"
  ]
  node [
    id 862
    label "gourde"
  ]
  node [
    id 863
    label "kwanza"
  ]
  node [
    id 864
    label "escudo_angolskie"
  ]
  node [
    id 865
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 866
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 867
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 868
    label "lari"
  ]
  node [
    id 869
    label "Ad&#380;aria"
  ]
  node [
    id 870
    label "naira"
  ]
  node [
    id 871
    label "Hudson"
  ]
  node [
    id 872
    label "Teksas"
  ]
  node [
    id 873
    label "Georgia"
  ]
  node [
    id 874
    label "Maryland"
  ]
  node [
    id 875
    label "Michigan"
  ]
  node [
    id 876
    label "Massachusetts"
  ]
  node [
    id 877
    label "Luizjana"
  ]
  node [
    id 878
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 879
    label "stan_wolny"
  ]
  node [
    id 880
    label "Floryda"
  ]
  node [
    id 881
    label "Po&#322;udnie"
  ]
  node [
    id 882
    label "Ohio"
  ]
  node [
    id 883
    label "Alaska"
  ]
  node [
    id 884
    label "Nowy_Meksyk"
  ]
  node [
    id 885
    label "Wuj_Sam"
  ]
  node [
    id 886
    label "Kansas"
  ]
  node [
    id 887
    label "Alabama"
  ]
  node [
    id 888
    label "Kalifornia"
  ]
  node [
    id 889
    label "Wirginia"
  ]
  node [
    id 890
    label "Nowy_York"
  ]
  node [
    id 891
    label "Waszyngton"
  ]
  node [
    id 892
    label "Pensylwania"
  ]
  node [
    id 893
    label "zielona_karta"
  ]
  node [
    id 894
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 895
    label "P&#243;&#322;noc"
  ]
  node [
    id 896
    label "Hawaje"
  ]
  node [
    id 897
    label "Zach&#243;d"
  ]
  node [
    id 898
    label "Illinois"
  ]
  node [
    id 899
    label "Oklahoma"
  ]
  node [
    id 900
    label "Oregon"
  ]
  node [
    id 901
    label "Arizona"
  ]
  node [
    id 902
    label "som"
  ]
  node [
    id 903
    label "peso_urugwajskie"
  ]
  node [
    id 904
    label "denar_macedo&#324;ski"
  ]
  node [
    id 905
    label "dolar_Brunei"
  ]
  node [
    id 906
    label "Persja"
  ]
  node [
    id 907
    label "rial_ira&#324;ski"
  ]
  node [
    id 908
    label "mu&#322;&#322;a"
  ]
  node [
    id 909
    label "dinar_libijski"
  ]
  node [
    id 910
    label "d&#380;amahirijja"
  ]
  node [
    id 911
    label "nakfa"
  ]
  node [
    id 912
    label "rial_katarski"
  ]
  node [
    id 913
    label "quetzal"
  ]
  node [
    id 914
    label "won"
  ]
  node [
    id 915
    label "rial_jeme&#324;ski"
  ]
  node [
    id 916
    label "peso_argenty&#324;skie"
  ]
  node [
    id 917
    label "guarani"
  ]
  node [
    id 918
    label "perper"
  ]
  node [
    id 919
    label "dinar_kuwejcki"
  ]
  node [
    id 920
    label "dalasi"
  ]
  node [
    id 921
    label "dolar_Zimbabwe"
  ]
  node [
    id 922
    label "Szantung"
  ]
  node [
    id 923
    label "Mand&#380;uria"
  ]
  node [
    id 924
    label "Hongkong"
  ]
  node [
    id 925
    label "Guangdong"
  ]
  node [
    id 926
    label "yuan"
  ]
  node [
    id 927
    label "D&#380;ungaria"
  ]
  node [
    id 928
    label "Chiny_Zachodnie"
  ]
  node [
    id 929
    label "Junnan"
  ]
  node [
    id 930
    label "Kuantung"
  ]
  node [
    id 931
    label "Chiny_Wschodnie"
  ]
  node [
    id 932
    label "Syczuan"
  ]
  node [
    id 933
    label "Krajna"
  ]
  node [
    id 934
    label "Kielecczyzna"
  ]
  node [
    id 935
    label "Lubuskie"
  ]
  node [
    id 936
    label "Opolskie"
  ]
  node [
    id 937
    label "Mazowsze"
  ]
  node [
    id 938
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 939
    label "Podlasie"
  ]
  node [
    id 940
    label "Kaczawa"
  ]
  node [
    id 941
    label "Wolin"
  ]
  node [
    id 942
    label "Wielkopolska"
  ]
  node [
    id 943
    label "Lubelszczyzna"
  ]
  node [
    id 944
    label "Wis&#322;a"
  ]
  node [
    id 945
    label "So&#322;a"
  ]
  node [
    id 946
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 947
    label "Pa&#322;uki"
  ]
  node [
    id 948
    label "Pomorze_Zachodnie"
  ]
  node [
    id 949
    label "Podkarpacie"
  ]
  node [
    id 950
    label "barwy_polskie"
  ]
  node [
    id 951
    label "Kujawy"
  ]
  node [
    id 952
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 953
    label "Warmia"
  ]
  node [
    id 954
    label "&#321;&#243;dzkie"
  ]
  node [
    id 955
    label "Bory_Tucholskie"
  ]
  node [
    id 956
    label "Suwalszczyzna"
  ]
  node [
    id 957
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 958
    label "z&#322;oty"
  ]
  node [
    id 959
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 960
    label "Ma&#322;opolska"
  ]
  node [
    id 961
    label "Mazury"
  ]
  node [
    id 962
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 963
    label "Powi&#347;le"
  ]
  node [
    id 964
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 965
    label "Azja_Mniejsza"
  ]
  node [
    id 966
    label "lira_turecka"
  ]
  node [
    id 967
    label "Ujgur"
  ]
  node [
    id 968
    label "kuna"
  ]
  node [
    id 969
    label "dram"
  ]
  node [
    id 970
    label "tala"
  ]
  node [
    id 971
    label "korona_s&#322;owacka"
  ]
  node [
    id 972
    label "Turiec"
  ]
  node [
    id 973
    label "rupia_nepalska"
  ]
  node [
    id 974
    label "Himalaje"
  ]
  node [
    id 975
    label "frank_gwinejski"
  ]
  node [
    id 976
    label "marka_esto&#324;ska"
  ]
  node [
    id 977
    label "korona_esto&#324;ska"
  ]
  node [
    id 978
    label "Skandynawia"
  ]
  node [
    id 979
    label "Nowa_Fundlandia"
  ]
  node [
    id 980
    label "Quebec"
  ]
  node [
    id 981
    label "dolar_kanadyjski"
  ]
  node [
    id 982
    label "Zanzibar"
  ]
  node [
    id 983
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 984
    label "&#346;wite&#378;"
  ]
  node [
    id 985
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 986
    label "peso_kolumbijskie"
  ]
  node [
    id 987
    label "funt_egipski"
  ]
  node [
    id 988
    label "Synaj"
  ]
  node [
    id 989
    label "paraszyt"
  ]
  node [
    id 990
    label "afgani"
  ]
  node [
    id 991
    label "Baktria"
  ]
  node [
    id 992
    label "szach"
  ]
  node [
    id 993
    label "baht"
  ]
  node [
    id 994
    label "tolar"
  ]
  node [
    id 995
    label "Naddniestrze"
  ]
  node [
    id 996
    label "lej_mo&#322;dawski"
  ]
  node [
    id 997
    label "Gagauzja"
  ]
  node [
    id 998
    label "posadzka"
  ]
  node [
    id 999
    label "podglebie"
  ]
  node [
    id 1000
    label "Ko&#322;yma"
  ]
  node [
    id 1001
    label "Indochiny"
  ]
  node [
    id 1002
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1003
    label "Bo&#347;nia"
  ]
  node [
    id 1004
    label "Kaukaz"
  ]
  node [
    id 1005
    label "Opolszczyzna"
  ]
  node [
    id 1006
    label "czynnik_produkcji"
  ]
  node [
    id 1007
    label "kort"
  ]
  node [
    id 1008
    label "Polesie"
  ]
  node [
    id 1009
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1010
    label "Yorkshire"
  ]
  node [
    id 1011
    label "zapadnia"
  ]
  node [
    id 1012
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1013
    label "Noworosja"
  ]
  node [
    id 1014
    label "glinowa&#263;"
  ]
  node [
    id 1015
    label "litosfera"
  ]
  node [
    id 1016
    label "Kurpie"
  ]
  node [
    id 1017
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1018
    label "Kociewie"
  ]
  node [
    id 1019
    label "Anglia"
  ]
  node [
    id 1020
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1021
    label "Laponia"
  ]
  node [
    id 1022
    label "Amazonia"
  ]
  node [
    id 1023
    label "Hercegowina"
  ]
  node [
    id 1024
    label "przestrze&#324;"
  ]
  node [
    id 1025
    label "Pamir"
  ]
  node [
    id 1026
    label "powierzchnia"
  ]
  node [
    id 1027
    label "p&#322;aszczyzna"
  ]
  node [
    id 1028
    label "Podhale"
  ]
  node [
    id 1029
    label "pomieszczenie"
  ]
  node [
    id 1030
    label "plantowa&#263;"
  ]
  node [
    id 1031
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1032
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1033
    label "dotleni&#263;"
  ]
  node [
    id 1034
    label "Zabajkale"
  ]
  node [
    id 1035
    label "skorupa_ziemska"
  ]
  node [
    id 1036
    label "glinowanie"
  ]
  node [
    id 1037
    label "Kaszuby"
  ]
  node [
    id 1038
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1039
    label "Oksytania"
  ]
  node [
    id 1040
    label "Mezoameryka"
  ]
  node [
    id 1041
    label "Turkiestan"
  ]
  node [
    id 1042
    label "Kurdystan"
  ]
  node [
    id 1043
    label "glej"
  ]
  node [
    id 1044
    label "Biskupizna"
  ]
  node [
    id 1045
    label "Podbeskidzie"
  ]
  node [
    id 1046
    label "Zag&#243;rze"
  ]
  node [
    id 1047
    label "Szkocja"
  ]
  node [
    id 1048
    label "domain"
  ]
  node [
    id 1049
    label "Huculszczyzna"
  ]
  node [
    id 1050
    label "pojazd"
  ]
  node [
    id 1051
    label "budynek"
  ]
  node [
    id 1052
    label "S&#261;decczyzna"
  ]
  node [
    id 1053
    label "Palestyna"
  ]
  node [
    id 1054
    label "miejsce"
  ]
  node [
    id 1055
    label "Lauda"
  ]
  node [
    id 1056
    label "penetrator"
  ]
  node [
    id 1057
    label "Bojkowszczyzna"
  ]
  node [
    id 1058
    label "ryzosfera"
  ]
  node [
    id 1059
    label "Zamojszczyzna"
  ]
  node [
    id 1060
    label "Walia"
  ]
  node [
    id 1061
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1062
    label "martwica"
  ]
  node [
    id 1063
    label "pr&#243;chnica"
  ]
  node [
    id 1064
    label "line"
  ]
  node [
    id 1065
    label "zwyczaj"
  ]
  node [
    id 1066
    label "kanon"
  ]
  node [
    id 1067
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 1068
    label "zjazd"
  ]
  node [
    id 1069
    label "styl"
  ]
  node [
    id 1070
    label "poj&#281;cie"
  ]
  node [
    id 1071
    label "uprawianie"
  ]
  node [
    id 1072
    label "collection"
  ]
  node [
    id 1073
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1074
    label "gathering"
  ]
  node [
    id 1075
    label "album"
  ]
  node [
    id 1076
    label "praca_rolnicza"
  ]
  node [
    id 1077
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1078
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1079
    label "series"
  ]
  node [
    id 1080
    label "dane"
  ]
  node [
    id 1081
    label "ONZ"
  ]
  node [
    id 1082
    label "podsystem"
  ]
  node [
    id 1083
    label "systemat"
  ]
  node [
    id 1084
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1085
    label "traktat_wersalski"
  ]
  node [
    id 1086
    label "przestawi&#263;"
  ]
  node [
    id 1087
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1088
    label "organ"
  ]
  node [
    id 1089
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 1090
    label "rozprz&#261;c"
  ]
  node [
    id 1091
    label "usenet"
  ]
  node [
    id 1092
    label "wi&#281;&#378;"
  ]
  node [
    id 1093
    label "treaty"
  ]
  node [
    id 1094
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 1095
    label "o&#347;"
  ]
  node [
    id 1096
    label "zachowanie"
  ]
  node [
    id 1097
    label "umowa"
  ]
  node [
    id 1098
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1099
    label "cybernetyk"
  ]
  node [
    id 1100
    label "system"
  ]
  node [
    id 1101
    label "cia&#322;o"
  ]
  node [
    id 1102
    label "alliance"
  ]
  node [
    id 1103
    label "sk&#322;ad"
  ]
  node [
    id 1104
    label "odjazd"
  ]
  node [
    id 1105
    label "manewr"
  ]
  node [
    id 1106
    label "wy&#347;cig"
  ]
  node [
    id 1107
    label "rally"
  ]
  node [
    id 1108
    label "przyjazd"
  ]
  node [
    id 1109
    label "pochy&#322;o&#347;&#263;"
  ]
  node [
    id 1110
    label "kombinacja_alpejska"
  ]
  node [
    id 1111
    label "dojazd"
  ]
  node [
    id 1112
    label "jazda"
  ]
  node [
    id 1113
    label "meeting"
  ]
  node [
    id 1114
    label "kultura_duchowa"
  ]
  node [
    id 1115
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 1116
    label "kultura"
  ]
  node [
    id 1117
    label "ceremony"
  ]
  node [
    id 1118
    label "stopie&#324;_pisma"
  ]
  node [
    id 1119
    label "model"
  ]
  node [
    id 1120
    label "utw&#243;r"
  ]
  node [
    id 1121
    label "criterion"
  ]
  node [
    id 1122
    label "&#347;wi&#281;to&#347;&#263;"
  ]
  node [
    id 1123
    label "msza"
  ]
  node [
    id 1124
    label "zasada"
  ]
  node [
    id 1125
    label "prawo"
  ]
  node [
    id 1126
    label "dekalog"
  ]
  node [
    id 1127
    label "pisa&#263;"
  ]
  node [
    id 1128
    label "spos&#243;b"
  ]
  node [
    id 1129
    label "charakter"
  ]
  node [
    id 1130
    label "dyscyplina_sportowa"
  ]
  node [
    id 1131
    label "natural_language"
  ]
  node [
    id 1132
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1133
    label "handle"
  ]
  node [
    id 1134
    label "reakcja"
  ]
  node [
    id 1135
    label "napisa&#263;"
  ]
  node [
    id 1136
    label "stylik"
  ]
  node [
    id 1137
    label "stroke"
  ]
  node [
    id 1138
    label "trzonek"
  ]
  node [
    id 1139
    label "narz&#281;dzie"
  ]
  node [
    id 1140
    label "behawior"
  ]
  node [
    id 1141
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1142
    label "kod"
  ]
  node [
    id 1143
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1144
    label "gramatyka"
  ]
  node [
    id 1145
    label "fonetyka"
  ]
  node [
    id 1146
    label "t&#322;umaczenie"
  ]
  node [
    id 1147
    label "rozumienie"
  ]
  node [
    id 1148
    label "address"
  ]
  node [
    id 1149
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1150
    label "m&#243;wienie"
  ]
  node [
    id 1151
    label "s&#322;ownictwo"
  ]
  node [
    id 1152
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1153
    label "konsonantyzm"
  ]
  node [
    id 1154
    label "wokalizm"
  ]
  node [
    id 1155
    label "komunikacja"
  ]
  node [
    id 1156
    label "m&#243;wi&#263;"
  ]
  node [
    id 1157
    label "po_koroniarsku"
  ]
  node [
    id 1158
    label "rozumie&#263;"
  ]
  node [
    id 1159
    label "przet&#322;umaczenie"
  ]
  node [
    id 1160
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1161
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1162
    label "tongue"
  ]
  node [
    id 1163
    label "wypowied&#378;"
  ]
  node [
    id 1164
    label "pismo"
  ]
  node [
    id 1165
    label "parafrazowanie"
  ]
  node [
    id 1166
    label "komunikat"
  ]
  node [
    id 1167
    label "stylizacja"
  ]
  node [
    id 1168
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1169
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1170
    label "strawestowanie"
  ]
  node [
    id 1171
    label "sparafrazowanie"
  ]
  node [
    id 1172
    label "sformu&#322;owanie"
  ]
  node [
    id 1173
    label "pos&#322;uchanie"
  ]
  node [
    id 1174
    label "strawestowa&#263;"
  ]
  node [
    id 1175
    label "parafrazowa&#263;"
  ]
  node [
    id 1176
    label "delimitacja"
  ]
  node [
    id 1177
    label "rezultat"
  ]
  node [
    id 1178
    label "ozdobnik"
  ]
  node [
    id 1179
    label "trawestowa&#263;"
  ]
  node [
    id 1180
    label "s&#261;d"
  ]
  node [
    id 1181
    label "sparafrazowa&#263;"
  ]
  node [
    id 1182
    label "trawestowanie"
  ]
  node [
    id 1183
    label "transportation_system"
  ]
  node [
    id 1184
    label "implicite"
  ]
  node [
    id 1185
    label "explicite"
  ]
  node [
    id 1186
    label "wydeptanie"
  ]
  node [
    id 1187
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 1188
    label "wydeptywanie"
  ]
  node [
    id 1189
    label "ekspedytor"
  ]
  node [
    id 1190
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 1191
    label "zapomnie&#263;"
  ]
  node [
    id 1192
    label "cecha"
  ]
  node [
    id 1193
    label "zapominanie"
  ]
  node [
    id 1194
    label "zapomnienie"
  ]
  node [
    id 1195
    label "potencja&#322;"
  ]
  node [
    id 1196
    label "obliczeniowo"
  ]
  node [
    id 1197
    label "ability"
  ]
  node [
    id 1198
    label "posiada&#263;"
  ]
  node [
    id 1199
    label "zapomina&#263;"
  ]
  node [
    id 1200
    label "bezproblemowy"
  ]
  node [
    id 1201
    label "wydarzenie"
  ]
  node [
    id 1202
    label "activity"
  ]
  node [
    id 1203
    label "ci&#261;g"
  ]
  node [
    id 1204
    label "language"
  ]
  node [
    id 1205
    label "szyfrowanie"
  ]
  node [
    id 1206
    label "szablon"
  ]
  node [
    id 1207
    label "code"
  ]
  node [
    id 1208
    label "cover"
  ]
  node [
    id 1209
    label "j&#281;zyk"
  ]
  node [
    id 1210
    label "opowiedzenie"
  ]
  node [
    id 1211
    label "zwracanie_si&#281;"
  ]
  node [
    id 1212
    label "zapeszanie"
  ]
  node [
    id 1213
    label "formu&#322;owanie"
  ]
  node [
    id 1214
    label "wypowiadanie"
  ]
  node [
    id 1215
    label "powiadanie"
  ]
  node [
    id 1216
    label "dysfonia"
  ]
  node [
    id 1217
    label "ozywanie_si&#281;"
  ]
  node [
    id 1218
    label "public_speaking"
  ]
  node [
    id 1219
    label "wyznawanie"
  ]
  node [
    id 1220
    label "dodawanie"
  ]
  node [
    id 1221
    label "wydawanie"
  ]
  node [
    id 1222
    label "wygadanie"
  ]
  node [
    id 1223
    label "informowanie"
  ]
  node [
    id 1224
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1225
    label "dowalenie"
  ]
  node [
    id 1226
    label "prawienie"
  ]
  node [
    id 1227
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 1228
    label "przerywanie"
  ]
  node [
    id 1229
    label "dogadanie_si&#281;"
  ]
  node [
    id 1230
    label "wydobywanie"
  ]
  node [
    id 1231
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1232
    label "wyra&#380;anie"
  ]
  node [
    id 1233
    label "opowiadanie"
  ]
  node [
    id 1234
    label "mawianie"
  ]
  node [
    id 1235
    label "stosowanie"
  ]
  node [
    id 1236
    label "zauwa&#380;enie"
  ]
  node [
    id 1237
    label "speaking"
  ]
  node [
    id 1238
    label "gaworzenie"
  ]
  node [
    id 1239
    label "przepowiadanie"
  ]
  node [
    id 1240
    label "dogadywanie_si&#281;"
  ]
  node [
    id 1241
    label "termin"
  ]
  node [
    id 1242
    label "terminology"
  ]
  node [
    id 1243
    label "zasymilowa&#263;"
  ]
  node [
    id 1244
    label "transkrypcja"
  ]
  node [
    id 1245
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 1246
    label "g&#322;osownia"
  ]
  node [
    id 1247
    label "phonetics"
  ]
  node [
    id 1248
    label "zasymilowanie"
  ]
  node [
    id 1249
    label "palatogram"
  ]
  node [
    id 1250
    label "ortografia"
  ]
  node [
    id 1251
    label "dzia&#322;"
  ]
  node [
    id 1252
    label "zajawka"
  ]
  node [
    id 1253
    label "paleografia"
  ]
  node [
    id 1254
    label "dzie&#322;o"
  ]
  node [
    id 1255
    label "dokument"
  ]
  node [
    id 1256
    label "wk&#322;ad"
  ]
  node [
    id 1257
    label "ok&#322;adka"
  ]
  node [
    id 1258
    label "letter"
  ]
  node [
    id 1259
    label "prasa"
  ]
  node [
    id 1260
    label "psychotest"
  ]
  node [
    id 1261
    label "communication"
  ]
  node [
    id 1262
    label "Zwrotnica"
  ]
  node [
    id 1263
    label "script"
  ]
  node [
    id 1264
    label "czasopismo"
  ]
  node [
    id 1265
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1266
    label "adres"
  ]
  node [
    id 1267
    label "przekaz"
  ]
  node [
    id 1268
    label "grafia"
  ]
  node [
    id 1269
    label "interpunkcja"
  ]
  node [
    id 1270
    label "handwriting"
  ]
  node [
    id 1271
    label "paleograf"
  ]
  node [
    id 1272
    label "list"
  ]
  node [
    id 1273
    label "sk&#322;adnia"
  ]
  node [
    id 1274
    label "morfologia"
  ]
  node [
    id 1275
    label "kategoria_gramatyczna"
  ]
  node [
    id 1276
    label "fleksja"
  ]
  node [
    id 1277
    label "rendition"
  ]
  node [
    id 1278
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 1279
    label "przek&#322;adanie"
  ]
  node [
    id 1280
    label "zrozumia&#322;y"
  ]
  node [
    id 1281
    label "tekst"
  ]
  node [
    id 1282
    label "remark"
  ]
  node [
    id 1283
    label "uzasadnianie"
  ]
  node [
    id 1284
    label "rozwianie"
  ]
  node [
    id 1285
    label "explanation"
  ]
  node [
    id 1286
    label "kr&#281;ty"
  ]
  node [
    id 1287
    label "bronienie"
  ]
  node [
    id 1288
    label "robienie"
  ]
  node [
    id 1289
    label "gossip"
  ]
  node [
    id 1290
    label "przekonywanie"
  ]
  node [
    id 1291
    label "rozwiewanie"
  ]
  node [
    id 1292
    label "przedstawianie"
  ]
  node [
    id 1293
    label "robi&#263;"
  ]
  node [
    id 1294
    label "przekonywa&#263;"
  ]
  node [
    id 1295
    label "u&#322;atwia&#263;"
  ]
  node [
    id 1296
    label "sprawowa&#263;"
  ]
  node [
    id 1297
    label "suplikowa&#263;"
  ]
  node [
    id 1298
    label "interpretowa&#263;"
  ]
  node [
    id 1299
    label "uzasadnia&#263;"
  ]
  node [
    id 1300
    label "give"
  ]
  node [
    id 1301
    label "przedstawia&#263;"
  ]
  node [
    id 1302
    label "explain"
  ]
  node [
    id 1303
    label "poja&#347;nia&#263;"
  ]
  node [
    id 1304
    label "broni&#263;"
  ]
  node [
    id 1305
    label "przek&#322;ada&#263;"
  ]
  node [
    id 1306
    label "elaborate"
  ]
  node [
    id 1307
    label "przekona&#263;"
  ]
  node [
    id 1308
    label "zrobi&#263;"
  ]
  node [
    id 1309
    label "put"
  ]
  node [
    id 1310
    label "frame"
  ]
  node [
    id 1311
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 1312
    label "zinterpretowa&#263;"
  ]
  node [
    id 1313
    label "match"
  ]
  node [
    id 1314
    label "see"
  ]
  node [
    id 1315
    label "kuma&#263;"
  ]
  node [
    id 1316
    label "zna&#263;"
  ]
  node [
    id 1317
    label "empatia"
  ]
  node [
    id 1318
    label "dziama&#263;"
  ]
  node [
    id 1319
    label "czu&#263;"
  ]
  node [
    id 1320
    label "wiedzie&#263;"
  ]
  node [
    id 1321
    label "odbiera&#263;"
  ]
  node [
    id 1322
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1323
    label "prawi&#263;"
  ]
  node [
    id 1324
    label "express"
  ]
  node [
    id 1325
    label "chew_the_fat"
  ]
  node [
    id 1326
    label "talk"
  ]
  node [
    id 1327
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1328
    label "say"
  ]
  node [
    id 1329
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1330
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1331
    label "tell"
  ]
  node [
    id 1332
    label "informowa&#263;"
  ]
  node [
    id 1333
    label "rozmawia&#263;"
  ]
  node [
    id 1334
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1335
    label "powiada&#263;"
  ]
  node [
    id 1336
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1337
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1338
    label "okre&#347;la&#263;"
  ]
  node [
    id 1339
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1340
    label "gaworzy&#263;"
  ]
  node [
    id 1341
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1342
    label "umie&#263;"
  ]
  node [
    id 1343
    label "wydobywa&#263;"
  ]
  node [
    id 1344
    label "czucie"
  ]
  node [
    id 1345
    label "bycie"
  ]
  node [
    id 1346
    label "interpretation"
  ]
  node [
    id 1347
    label "realization"
  ]
  node [
    id 1348
    label "hermeneutyka"
  ]
  node [
    id 1349
    label "kumanie"
  ]
  node [
    id 1350
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 1351
    label "wytw&#243;r"
  ]
  node [
    id 1352
    label "apprehension"
  ]
  node [
    id 1353
    label "wnioskowanie"
  ]
  node [
    id 1354
    label "obja&#347;nienie"
  ]
  node [
    id 1355
    label "kontekst"
  ]
  node [
    id 1356
    label "passage"
  ]
  node [
    id 1357
    label "fragment"
  ]
  node [
    id 1358
    label "artyku&#322;"
  ]
  node [
    id 1359
    label "toaleta"
  ]
  node [
    id 1360
    label "urywek"
  ]
  node [
    id 1361
    label "klozetka"
  ]
  node [
    id 1362
    label "kosmetyka"
  ]
  node [
    id 1363
    label "dressing"
  ]
  node [
    id 1364
    label "sracz"
  ]
  node [
    id 1365
    label "sypialnia"
  ]
  node [
    id 1366
    label "kibel"
  ]
  node [
    id 1367
    label "mycie"
  ]
  node [
    id 1368
    label "prewet"
  ]
  node [
    id 1369
    label "mebel"
  ]
  node [
    id 1370
    label "podw&#322;o&#347;nik"
  ]
  node [
    id 1371
    label "kreacja"
  ]
  node [
    id 1372
    label "redakcja"
  ]
  node [
    id 1373
    label "j&#281;zykowo"
  ]
  node [
    id 1374
    label "preparacja"
  ]
  node [
    id 1375
    label "obelga"
  ]
  node [
    id 1376
    label "odmianka"
  ]
  node [
    id 1377
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1378
    label "pomini&#281;cie"
  ]
  node [
    id 1379
    label "koniektura"
  ]
  node [
    id 1380
    label "ekscerpcja"
  ]
  node [
    id 1381
    label "prawda"
  ]
  node [
    id 1382
    label "wyr&#243;b"
  ]
  node [
    id 1383
    label "nag&#322;&#243;wek"
  ]
  node [
    id 1384
    label "szkic"
  ]
  node [
    id 1385
    label "rodzajnik"
  ]
  node [
    id 1386
    label "towar"
  ]
  node [
    id 1387
    label "paragraf"
  ]
  node [
    id 1388
    label "znak_j&#281;zykowy"
  ]
  node [
    id 1389
    label "nast&#281;pnie"
  ]
  node [
    id 1390
    label "poni&#380;szy"
  ]
  node [
    id 1391
    label "kolejny"
  ]
  node [
    id 1392
    label "ten"
  ]
  node [
    id 1393
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 1394
    label "zobo"
  ]
  node [
    id 1395
    label "byd&#322;o"
  ]
  node [
    id 1396
    label "dzo"
  ]
  node [
    id 1397
    label "yakalo"
  ]
  node [
    id 1398
    label "kr&#281;torogie"
  ]
  node [
    id 1399
    label "g&#322;owa"
  ]
  node [
    id 1400
    label "livestock"
  ]
  node [
    id 1401
    label "posp&#243;lstwo"
  ]
  node [
    id 1402
    label "kraal"
  ]
  node [
    id 1403
    label "czochrad&#322;o"
  ]
  node [
    id 1404
    label "prze&#380;uwacz"
  ]
  node [
    id 1405
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 1406
    label "bizon"
  ]
  node [
    id 1407
    label "zebu"
  ]
  node [
    id 1408
    label "byd&#322;o_domowe"
  ]
  node [
    id 1409
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 1410
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 1411
    label "zamkn&#261;&#263;"
  ]
  node [
    id 1412
    label "admit"
  ]
  node [
    id 1413
    label "incorporate"
  ]
  node [
    id 1414
    label "wezbra&#263;"
  ]
  node [
    id 1415
    label "boil"
  ]
  node [
    id 1416
    label "raptowny"
  ]
  node [
    id 1417
    label "embrace"
  ]
  node [
    id 1418
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1419
    label "pozna&#263;"
  ]
  node [
    id 1420
    label "insert"
  ]
  node [
    id 1421
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 1422
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1423
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 1424
    label "ustali&#263;"
  ]
  node [
    id 1425
    label "kill"
  ]
  node [
    id 1426
    label "rozwi&#261;za&#263;"
  ]
  node [
    id 1427
    label "zako&#324;czy&#263;"
  ]
  node [
    id 1428
    label "uj&#261;&#263;"
  ]
  node [
    id 1429
    label "close"
  ]
  node [
    id 1430
    label "ukry&#263;"
  ]
  node [
    id 1431
    label "sko&#324;czy&#263;"
  ]
  node [
    id 1432
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1433
    label "zablokowa&#263;"
  ]
  node [
    id 1434
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 1435
    label "spowodowa&#263;"
  ]
  node [
    id 1436
    label "zatrzyma&#263;"
  ]
  node [
    id 1437
    label "lock"
  ]
  node [
    id 1438
    label "hold"
  ]
  node [
    id 1439
    label "obj&#261;&#263;"
  ]
  node [
    id 1440
    label "clasp"
  ]
  node [
    id 1441
    label "udost&#281;pni&#263;"
  ]
  node [
    id 1442
    label "zdecydowa&#263;"
  ]
  node [
    id 1443
    label "unwrap"
  ]
  node [
    id 1444
    label "bind"
  ]
  node [
    id 1445
    label "umocni&#263;"
  ]
  node [
    id 1446
    label "przyswoi&#263;"
  ]
  node [
    id 1447
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1448
    label "teach"
  ]
  node [
    id 1449
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 1450
    label "zrozumie&#263;"
  ]
  node [
    id 1451
    label "visualize"
  ]
  node [
    id 1452
    label "experience"
  ]
  node [
    id 1453
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 1454
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1455
    label "topographic_point"
  ]
  node [
    id 1456
    label "feel"
  ]
  node [
    id 1457
    label "zebra&#263;"
  ]
  node [
    id 1458
    label "wzm&#243;c_si&#281;"
  ]
  node [
    id 1459
    label "nape&#322;ni&#263;_si&#281;"
  ]
  node [
    id 1460
    label "podda&#263;_si&#281;"
  ]
  node [
    id 1461
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 1462
    label "rise"
  ]
  node [
    id 1463
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 1464
    label "arise"
  ]
  node [
    id 1465
    label "czyn"
  ]
  node [
    id 1466
    label "warunek"
  ]
  node [
    id 1467
    label "contract"
  ]
  node [
    id 1468
    label "porozumienie"
  ]
  node [
    id 1469
    label "gestia_transportowa"
  ]
  node [
    id 1470
    label "klauzula"
  ]
  node [
    id 1471
    label "nieoczekiwany"
  ]
  node [
    id 1472
    label "raptownie"
  ]
  node [
    id 1473
    label "zawrzenie"
  ]
  node [
    id 1474
    label "gwa&#322;towny"
  ]
  node [
    id 1475
    label "szybki"
  ]
  node [
    id 1476
    label "postawi&#263;"
  ]
  node [
    id 1477
    label "opatrzy&#263;"
  ]
  node [
    id 1478
    label "sign"
  ]
  node [
    id 1479
    label "za&#347;wiadczy&#263;"
  ]
  node [
    id 1480
    label "po&#347;wiadczy&#263;"
  ]
  node [
    id 1481
    label "leave"
  ]
  node [
    id 1482
    label "attest"
  ]
  node [
    id 1483
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 1484
    label "dopowiedzie&#263;"
  ]
  node [
    id 1485
    label "oznaczy&#263;"
  ]
  node [
    id 1486
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 1487
    label "bandage"
  ]
  node [
    id 1488
    label "amend"
  ]
  node [
    id 1489
    label "zabezpieczy&#263;"
  ]
  node [
    id 1490
    label "dress"
  ]
  node [
    id 1491
    label "post"
  ]
  node [
    id 1492
    label "zmieni&#263;"
  ]
  node [
    id 1493
    label "oceni&#263;"
  ]
  node [
    id 1494
    label "wydoby&#263;"
  ]
  node [
    id 1495
    label "pozostawi&#263;"
  ]
  node [
    id 1496
    label "establish"
  ]
  node [
    id 1497
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 1498
    label "zafundowa&#263;"
  ]
  node [
    id 1499
    label "budowla"
  ]
  node [
    id 1500
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 1501
    label "obra&#263;"
  ]
  node [
    id 1502
    label "uczyni&#263;"
  ]
  node [
    id 1503
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 1504
    label "stanowisko"
  ]
  node [
    id 1505
    label "wskaza&#263;"
  ]
  node [
    id 1506
    label "peddle"
  ]
  node [
    id 1507
    label "obstawi&#263;"
  ]
  node [
    id 1508
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 1509
    label "znak"
  ]
  node [
    id 1510
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 1511
    label "wytworzy&#263;"
  ]
  node [
    id 1512
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 1513
    label "set"
  ]
  node [
    id 1514
    label "uruchomi&#263;"
  ]
  node [
    id 1515
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 1516
    label "wyda&#263;"
  ]
  node [
    id 1517
    label "przyzna&#263;"
  ]
  node [
    id 1518
    label "stawi&#263;"
  ]
  node [
    id 1519
    label "wyznaczy&#263;"
  ]
  node [
    id 1520
    label "przedstawi&#263;"
  ]
  node [
    id 1521
    label "wsp&#243;lny"
  ]
  node [
    id 1522
    label "sp&#243;lnie"
  ]
  node [
    id 1523
    label "jeden"
  ]
  node [
    id 1524
    label "uwsp&#243;lnienie"
  ]
  node [
    id 1525
    label "sp&#243;lny"
  ]
  node [
    id 1526
    label "spolny"
  ]
  node [
    id 1527
    label "uwsp&#243;lnianie"
  ]
  node [
    id 1528
    label "zwi&#261;zanie"
  ]
  node [
    id 1529
    label "society"
  ]
  node [
    id 1530
    label "partnership"
  ]
  node [
    id 1531
    label "zwi&#261;zek"
  ]
  node [
    id 1532
    label "wi&#261;zanie"
  ]
  node [
    id 1533
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 1534
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1535
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1536
    label "marriage"
  ]
  node [
    id 1537
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1538
    label "bratnia_dusza"
  ]
  node [
    id 1539
    label "podobie&#324;stwo"
  ]
  node [
    id 1540
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1541
    label "Fremeni"
  ]
  node [
    id 1542
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1543
    label "odwadnianie"
  ]
  node [
    id 1544
    label "azeotrop"
  ]
  node [
    id 1545
    label "odwodni&#263;"
  ]
  node [
    id 1546
    label "lokant"
  ]
  node [
    id 1547
    label "koligacja"
  ]
  node [
    id 1548
    label "odwodnienie"
  ]
  node [
    id 1549
    label "marketing_afiliacyjny"
  ]
  node [
    id 1550
    label "substancja_chemiczna"
  ]
  node [
    id 1551
    label "powi&#261;zanie"
  ]
  node [
    id 1552
    label "odwadnia&#263;"
  ]
  node [
    id 1553
    label "bearing"
  ]
  node [
    id 1554
    label "konstytucja"
  ]
  node [
    id 1555
    label "relacja"
  ]
  node [
    id 1556
    label "podobno&#347;&#263;"
  ]
  node [
    id 1557
    label "Rumelia"
  ]
  node [
    id 1558
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1559
    label "twardnienie"
  ]
  node [
    id 1560
    label "przywi&#261;zanie"
  ]
  node [
    id 1561
    label "narta"
  ]
  node [
    id 1562
    label "pakowanie"
  ]
  node [
    id 1563
    label "uchwyt"
  ]
  node [
    id 1564
    label "szcz&#281;ka"
  ]
  node [
    id 1565
    label "anga&#380;owanie"
  ]
  node [
    id 1566
    label "podwi&#261;zywanie"
  ]
  node [
    id 1567
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 1568
    label "socket"
  ]
  node [
    id 1569
    label "wi&#261;za&#263;"
  ]
  node [
    id 1570
    label "zawi&#261;zek"
  ]
  node [
    id 1571
    label "my&#347;lenie"
  ]
  node [
    id 1572
    label "przedmiot"
  ]
  node [
    id 1573
    label "wytwarzanie"
  ]
  node [
    id 1574
    label "scalanie"
  ]
  node [
    id 1575
    label "do&#322;&#261;czanie"
  ]
  node [
    id 1576
    label "fusion"
  ]
  node [
    id 1577
    label "rozmieszczenie"
  ]
  node [
    id 1578
    label "obwi&#261;zanie"
  ]
  node [
    id 1579
    label "element_konstrukcyjny"
  ]
  node [
    id 1580
    label "mezomeria"
  ]
  node [
    id 1581
    label "combination"
  ]
  node [
    id 1582
    label "szermierka"
  ]
  node [
    id 1583
    label "proces_chemiczny"
  ]
  node [
    id 1584
    label "obezw&#322;adnianie"
  ]
  node [
    id 1585
    label "podwi&#261;zanie"
  ]
  node [
    id 1586
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 1587
    label "tobo&#322;ek"
  ]
  node [
    id 1588
    label "przywi&#261;zywanie"
  ]
  node [
    id 1589
    label "zobowi&#261;zywanie"
  ]
  node [
    id 1590
    label "cement"
  ]
  node [
    id 1591
    label "obwi&#261;zywanie"
  ]
  node [
    id 1592
    label "ceg&#322;a"
  ]
  node [
    id 1593
    label "przymocowywanie"
  ]
  node [
    id 1594
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1595
    label "kojarzenie_si&#281;"
  ]
  node [
    id 1596
    label "miecz"
  ]
  node [
    id 1597
    label "&#322;&#261;czenie"
  ]
  node [
    id 1598
    label "w&#281;ze&#322;"
  ]
  node [
    id 1599
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 1600
    label "opakowa&#263;"
  ]
  node [
    id 1601
    label "scali&#263;"
  ]
  node [
    id 1602
    label "unify"
  ]
  node [
    id 1603
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 1604
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 1605
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 1606
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1607
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 1608
    label "zawi&#261;za&#263;"
  ]
  node [
    id 1609
    label "zaprawa"
  ]
  node [
    id 1610
    label "powi&#261;za&#263;"
  ]
  node [
    id 1611
    label "relate"
  ]
  node [
    id 1612
    label "consort"
  ]
  node [
    id 1613
    label "form"
  ]
  node [
    id 1614
    label "fastening"
  ]
  node [
    id 1615
    label "affiliation"
  ]
  node [
    id 1616
    label "attachment"
  ]
  node [
    id 1617
    label "obezw&#322;adnienie"
  ]
  node [
    id 1618
    label "opakowanie"
  ]
  node [
    id 1619
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1620
    label "do&#322;&#261;czenie"
  ]
  node [
    id 1621
    label "tying"
  ]
  node [
    id 1622
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1623
    label "st&#281;&#380;enie"
  ]
  node [
    id 1624
    label "zobowi&#261;zanie"
  ]
  node [
    id 1625
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 1626
    label "ograniczenie"
  ]
  node [
    id 1627
    label "zawi&#261;zanie"
  ]
  node [
    id 1628
    label "supervene"
  ]
  node [
    id 1629
    label "gamble"
  ]
  node [
    id 1630
    label "nacisn&#261;&#263;"
  ]
  node [
    id 1631
    label "zaatakowa&#263;"
  ]
  node [
    id 1632
    label "nak&#322;oni&#263;"
  ]
  node [
    id 1633
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1634
    label "tug"
  ]
  node [
    id 1635
    label "cram"
  ]
  node [
    id 1636
    label "nadusi&#263;"
  ]
  node [
    id 1637
    label "zach&#281;ci&#263;"
  ]
  node [
    id 1638
    label "anoint"
  ]
  node [
    id 1639
    label "sport"
  ]
  node [
    id 1640
    label "skrytykowa&#263;"
  ]
  node [
    id 1641
    label "postara&#263;_si&#281;"
  ]
  node [
    id 1642
    label "rozegra&#263;"
  ]
  node [
    id 1643
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1644
    label "spell"
  ]
  node [
    id 1645
    label "attack"
  ]
  node [
    id 1646
    label "przeby&#263;"
  ]
  node [
    id 1647
    label "powiedzie&#263;"
  ]
  node [
    id 1648
    label "inclusion"
  ]
  node [
    id 1649
    label "zawieranie"
  ]
  node [
    id 1650
    label "spowodowanie"
  ]
  node [
    id 1651
    label "przyskrzynienie"
  ]
  node [
    id 1652
    label "dissolution"
  ]
  node [
    id 1653
    label "zapoznanie_si&#281;"
  ]
  node [
    id 1654
    label "uchwalenie"
  ]
  node [
    id 1655
    label "umawianie_si&#281;"
  ]
  node [
    id 1656
    label "zapoznanie"
  ]
  node [
    id 1657
    label "pozamykanie"
  ]
  node [
    id 1658
    label "zmieszczenie"
  ]
  node [
    id 1659
    label "ustalenie"
  ]
  node [
    id 1660
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 1661
    label "znajomy"
  ]
  node [
    id 1662
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 1663
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1664
    label "narobienie"
  ]
  node [
    id 1665
    label "porobienie"
  ]
  node [
    id 1666
    label "creation"
  ]
  node [
    id 1667
    label "campaign"
  ]
  node [
    id 1668
    label "causing"
  ]
  node [
    id 1669
    label "ustanowienie"
  ]
  node [
    id 1670
    label "resoluteness"
  ]
  node [
    id 1671
    label "obj&#281;cie"
  ]
  node [
    id 1672
    label "apartment"
  ]
  node [
    id 1673
    label "udost&#281;pnienie"
  ]
  node [
    id 1674
    label "umieszczenie"
  ]
  node [
    id 1675
    label "umocnienie"
  ]
  node [
    id 1676
    label "zdecydowanie"
  ]
  node [
    id 1677
    label "appointment"
  ]
  node [
    id 1678
    label "informacja"
  ]
  node [
    id 1679
    label "decyzja"
  ]
  node [
    id 1680
    label "localization"
  ]
  node [
    id 1681
    label "umo&#380;liwienie"
  ]
  node [
    id 1682
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 1683
    label "knowing"
  ]
  node [
    id 1684
    label "poinformowanie"
  ]
  node [
    id 1685
    label "obznajomienie"
  ]
  node [
    id 1686
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1687
    label "representation"
  ]
  node [
    id 1688
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 1689
    label "pewien"
  ]
  node [
    id 1690
    label "sw&#243;j"
  ]
  node [
    id 1691
    label "znajomek"
  ]
  node [
    id 1692
    label "znany"
  ]
  node [
    id 1693
    label "znajomo"
  ]
  node [
    id 1694
    label "zapoznawanie"
  ]
  node [
    id 1695
    label "przyj&#281;ty"
  ]
  node [
    id 1696
    label "za_pan_brat"
  ]
  node [
    id 1697
    label "powodowanie"
  ]
  node [
    id 1698
    label "przytrzaskiwanie"
  ]
  node [
    id 1699
    label "bycie_w_posiadaniu"
  ]
  node [
    id 1700
    label "okre&#347;lanie"
  ]
  node [
    id 1701
    label "obejmowanie"
  ]
  node [
    id 1702
    label "zamykanie_si&#281;"
  ]
  node [
    id 1703
    label "przygniecenie"
  ]
  node [
    id 1704
    label "z&#322;apanie"
  ]
  node [
    id 1705
    label "zgarni&#281;cie"
  ]
  node [
    id 1706
    label "commitment"
  ]
  node [
    id 1707
    label "akt"
  ]
  node [
    id 1708
    label "komunikacja_zintegrowana"
  ]
  node [
    id 1709
    label "etykieta"
  ]
  node [
    id 1710
    label "prawid&#322;o"
  ]
  node [
    id 1711
    label "zasada_d'Alemberta"
  ]
  node [
    id 1712
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 1713
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1714
    label "opis"
  ]
  node [
    id 1715
    label "base"
  ]
  node [
    id 1716
    label "moralno&#347;&#263;"
  ]
  node [
    id 1717
    label "regu&#322;a_Allena"
  ]
  node [
    id 1718
    label "prawo_Mendla"
  ]
  node [
    id 1719
    label "standard"
  ]
  node [
    id 1720
    label "obserwacja"
  ]
  node [
    id 1721
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1722
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1723
    label "qualification"
  ]
  node [
    id 1724
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1725
    label "normalizacja"
  ]
  node [
    id 1726
    label "dominion"
  ]
  node [
    id 1727
    label "twierdzenie"
  ]
  node [
    id 1728
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1729
    label "substancja"
  ]
  node [
    id 1730
    label "occupation"
  ]
  node [
    id 1731
    label "formality"
  ]
  node [
    id 1732
    label "naklejka"
  ]
  node [
    id 1733
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1734
    label "tab"
  ]
  node [
    id 1735
    label "tabliczka"
  ]
  node [
    id 1736
    label "ustosunkowywa&#263;"
  ]
  node [
    id 1737
    label "podzbi&#243;r"
  ]
  node [
    id 1738
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 1739
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 1740
    label "ustosunkowywanie"
  ]
  node [
    id 1741
    label "sprawko"
  ]
  node [
    id 1742
    label "korespondent"
  ]
  node [
    id 1743
    label "message"
  ]
  node [
    id 1744
    label "trasa"
  ]
  node [
    id 1745
    label "ustosunkowanie"
  ]
  node [
    id 1746
    label "ustosunkowa&#263;"
  ]
  node [
    id 1747
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1748
    label "erotyka"
  ]
  node [
    id 1749
    label "podniecanie"
  ]
  node [
    id 1750
    label "po&#380;ycie"
  ]
  node [
    id 1751
    label "baraszki"
  ]
  node [
    id 1752
    label "numer"
  ]
  node [
    id 1753
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 1754
    label "certificate"
  ]
  node [
    id 1755
    label "ruch_frykcyjny"
  ]
  node [
    id 1756
    label "ontologia"
  ]
  node [
    id 1757
    label "wzw&#243;d"
  ]
  node [
    id 1758
    label "scena"
  ]
  node [
    id 1759
    label "seks"
  ]
  node [
    id 1760
    label "pozycja_misjonarska"
  ]
  node [
    id 1761
    label "rozmna&#380;anie"
  ]
  node [
    id 1762
    label "arystotelizm"
  ]
  node [
    id 1763
    label "urzeczywistnienie"
  ]
  node [
    id 1764
    label "funkcja"
  ]
  node [
    id 1765
    label "act"
  ]
  node [
    id 1766
    label "imisja"
  ]
  node [
    id 1767
    label "podniecenie"
  ]
  node [
    id 1768
    label "podnieca&#263;"
  ]
  node [
    id 1769
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1770
    label "fascyku&#322;"
  ]
  node [
    id 1771
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1772
    label "nago&#347;&#263;"
  ]
  node [
    id 1773
    label "gra_wst&#281;pna"
  ]
  node [
    id 1774
    label "po&#380;&#261;danie"
  ]
  node [
    id 1775
    label "podnieci&#263;"
  ]
  node [
    id 1776
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1777
    label "na_pieska"
  ]
  node [
    id 1778
    label "jaki&#347;"
  ]
  node [
    id 1779
    label "okre&#347;lony"
  ]
  node [
    id 1780
    label "jako&#347;"
  ]
  node [
    id 1781
    label "niez&#322;y"
  ]
  node [
    id 1782
    label "charakterystyczny"
  ]
  node [
    id 1783
    label "jako_tako"
  ]
  node [
    id 1784
    label "ciekawy"
  ]
  node [
    id 1785
    label "dziwny"
  ]
  node [
    id 1786
    label "przyzwoity"
  ]
  node [
    id 1787
    label "wiadomy"
  ]
  node [
    id 1788
    label "konsylium"
  ]
  node [
    id 1789
    label "Rada_Europejska"
  ]
  node [
    id 1790
    label "dyskusja"
  ]
  node [
    id 1791
    label "posiedzenie"
  ]
  node [
    id 1792
    label "Rada_Europy"
  ]
  node [
    id 1793
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 1794
    label "wskaz&#243;wka"
  ]
  node [
    id 1795
    label "conference"
  ]
  node [
    id 1796
    label "zgromadzenie"
  ]
  node [
    id 1797
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 1798
    label "convention"
  ]
  node [
    id 1799
    label "adjustment"
  ]
  node [
    id 1800
    label "pobycie"
  ]
  node [
    id 1801
    label "odsiedzenie"
  ]
  node [
    id 1802
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 1803
    label "pozyskanie"
  ]
  node [
    id 1804
    label "kongregacja"
  ]
  node [
    id 1805
    label "templum"
  ]
  node [
    id 1806
    label "gromadzenie"
  ]
  node [
    id 1807
    label "konwentykiel"
  ]
  node [
    id 1808
    label "klasztor"
  ]
  node [
    id 1809
    label "caucus"
  ]
  node [
    id 1810
    label "skupienie"
  ]
  node [
    id 1811
    label "concourse"
  ]
  node [
    id 1812
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1813
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1814
    label "struktura_anatomiczna"
  ]
  node [
    id 1815
    label "organogeneza"
  ]
  node [
    id 1816
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1817
    label "tw&#243;r"
  ]
  node [
    id 1818
    label "tkanka"
  ]
  node [
    id 1819
    label "stomia"
  ]
  node [
    id 1820
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1821
    label "budowa"
  ]
  node [
    id 1822
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 1823
    label "okolica"
  ]
  node [
    id 1824
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 1825
    label "dekortykacja"
  ]
  node [
    id 1826
    label "Izba_Konsyliarska"
  ]
  node [
    id 1827
    label "sympozjon"
  ]
  node [
    id 1828
    label "rozmowa"
  ]
  node [
    id 1829
    label "tarcza"
  ]
  node [
    id 1830
    label "solucja"
  ]
  node [
    id 1831
    label "implikowa&#263;"
  ]
  node [
    id 1832
    label "zegar"
  ]
  node [
    id 1833
    label "fakt"
  ]
  node [
    id 1834
    label "obrady"
  ]
  node [
    id 1835
    label "Unia_Wolno&#347;ci"
  ]
  node [
    id 1836
    label "Unia"
  ]
  node [
    id 1837
    label "union"
  ]
  node [
    id 1838
    label "eurorealizm"
  ]
  node [
    id 1839
    label "euroentuzjazm"
  ]
  node [
    id 1840
    label "euroko&#322;choz"
  ]
  node [
    id 1841
    label "eurosceptyczny"
  ]
  node [
    id 1842
    label "p&#322;atnik_netto"
  ]
  node [
    id 1843
    label "euroentuzjasta"
  ]
  node [
    id 1844
    label "Bruksela"
  ]
  node [
    id 1845
    label "eurosceptyk"
  ]
  node [
    id 1846
    label "eurorealista"
  ]
  node [
    id 1847
    label "eurosceptycyzm"
  ]
  node [
    id 1848
    label "Fundusze_Unijne"
  ]
  node [
    id 1849
    label "Wsp&#243;lnota_Europejska"
  ]
  node [
    id 1850
    label "prawo_unijne"
  ]
  node [
    id 1851
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1852
    label "europejsko"
  ]
  node [
    id 1853
    label "typowy"
  ]
  node [
    id 1854
    label "po_europejsku"
  ]
  node [
    id 1855
    label "European"
  ]
  node [
    id 1856
    label "typowo"
  ]
  node [
    id 1857
    label "zwyk&#322;y"
  ]
  node [
    id 1858
    label "zwyczajny"
  ]
  node [
    id 1859
    label "cz&#281;sty"
  ]
  node [
    id 1860
    label "wyj&#261;tkowy"
  ]
  node [
    id 1861
    label "podobny"
  ]
  node [
    id 1862
    label "charakterystycznie"
  ]
  node [
    id 1863
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1864
    label "szczeg&#243;lny"
  ]
  node [
    id 1865
    label "stosownie"
  ]
  node [
    id 1866
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1867
    label "prawdziwy"
  ]
  node [
    id 1868
    label "zasadniczy"
  ]
  node [
    id 1869
    label "uprawniony"
  ]
  node [
    id 1870
    label "nale&#380;yty"
  ]
  node [
    id 1871
    label "dobry"
  ]
  node [
    id 1872
    label "nale&#380;ny"
  ]
  node [
    id 1873
    label "pies_my&#347;liwski"
  ]
  node [
    id 1874
    label "decydowa&#263;"
  ]
  node [
    id 1875
    label "decide"
  ]
  node [
    id 1876
    label "by&#263;"
  ]
  node [
    id 1877
    label "typify"
  ]
  node [
    id 1878
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1879
    label "represent"
  ]
  node [
    id 1880
    label "zatrzymywa&#263;"
  ]
  node [
    id 1881
    label "przerywa&#263;"
  ]
  node [
    id 1882
    label "przechowywa&#263;"
  ]
  node [
    id 1883
    label "&#322;apa&#263;"
  ]
  node [
    id 1884
    label "komornik"
  ]
  node [
    id 1885
    label "zaczepia&#263;"
  ]
  node [
    id 1886
    label "zamyka&#263;"
  ]
  node [
    id 1887
    label "zabiera&#263;"
  ]
  node [
    id 1888
    label "powodowa&#263;"
  ]
  node [
    id 1889
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1890
    label "throng"
  ]
  node [
    id 1891
    label "unieruchamia&#263;"
  ]
  node [
    id 1892
    label "allude"
  ]
  node [
    id 1893
    label "zgarnia&#263;"
  ]
  node [
    id 1894
    label "przetrzymywa&#263;"
  ]
  node [
    id 1895
    label "suspend"
  ]
  node [
    id 1896
    label "prosecute"
  ]
  node [
    id 1897
    label "reakcja_chemiczna"
  ]
  node [
    id 1898
    label "determine"
  ]
  node [
    id 1899
    label "work"
  ]
  node [
    id 1900
    label "mean"
  ]
  node [
    id 1901
    label "klasyfikator"
  ]
  node [
    id 1902
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1903
    label "stan"
  ]
  node [
    id 1904
    label "stand"
  ]
  node [
    id 1905
    label "trwa&#263;"
  ]
  node [
    id 1906
    label "equal"
  ]
  node [
    id 1907
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1908
    label "uczestniczy&#263;"
  ]
  node [
    id 1909
    label "obecno&#347;&#263;"
  ]
  node [
    id 1910
    label "si&#281;ga&#263;"
  ]
  node [
    id 1911
    label "mie&#263;_miejsce"
  ]
  node [
    id 1912
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 1913
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1914
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 1915
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 1916
    label "zgodnie"
  ]
  node [
    id 1917
    label "jednomy&#347;lny"
  ]
  node [
    id 1918
    label "unanimously"
  ]
  node [
    id 1919
    label "zgodny"
  ]
  node [
    id 1920
    label "spokojnie"
  ]
  node [
    id 1921
    label "zbie&#380;nie"
  ]
  node [
    id 1922
    label "jednakowo"
  ]
  node [
    id 1923
    label "dobrze"
  ]
  node [
    id 1924
    label "dobieranie_si&#281;"
  ]
  node [
    id 1925
    label "cz&#322;owiek"
  ]
  node [
    id 1926
    label "postronnie"
  ]
  node [
    id 1927
    label "przypadkowy"
  ]
  node [
    id 1928
    label "neutralny"
  ]
  node [
    id 1929
    label "dzie&#324;"
  ]
  node [
    id 1930
    label "long_time"
  ]
  node [
    id 1931
    label "czynienie_si&#281;"
  ]
  node [
    id 1932
    label "noc"
  ]
  node [
    id 1933
    label "wiecz&#243;r"
  ]
  node [
    id 1934
    label "t&#322;usty_czwartek"
  ]
  node [
    id 1935
    label "podwiecz&#243;r"
  ]
  node [
    id 1936
    label "ranek"
  ]
  node [
    id 1937
    label "po&#322;udnie"
  ]
  node [
    id 1938
    label "s&#322;o&#324;ce"
  ]
  node [
    id 1939
    label "Sylwester"
  ]
  node [
    id 1940
    label "godzina"
  ]
  node [
    id 1941
    label "popo&#322;udnie"
  ]
  node [
    id 1942
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 1943
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 1944
    label "walentynki"
  ]
  node [
    id 1945
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 1946
    label "przedpo&#322;udnie"
  ]
  node [
    id 1947
    label "wzej&#347;cie"
  ]
  node [
    id 1948
    label "wstanie"
  ]
  node [
    id 1949
    label "przedwiecz&#243;r"
  ]
  node [
    id 1950
    label "rano"
  ]
  node [
    id 1951
    label "tydzie&#324;"
  ]
  node [
    id 1952
    label "day"
  ]
  node [
    id 1953
    label "doba"
  ]
  node [
    id 1954
    label "wsta&#263;"
  ]
  node [
    id 1955
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 1956
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 1957
    label "czas"
  ]
  node [
    id 1958
    label "nasada"
  ]
  node [
    id 1959
    label "profanum"
  ]
  node [
    id 1960
    label "wz&#243;r"
  ]
  node [
    id 1961
    label "senior"
  ]
  node [
    id 1962
    label "os&#322;abia&#263;"
  ]
  node [
    id 1963
    label "homo_sapiens"
  ]
  node [
    id 1964
    label "osoba"
  ]
  node [
    id 1965
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1966
    label "Adam"
  ]
  node [
    id 1967
    label "hominid"
  ]
  node [
    id 1968
    label "posta&#263;"
  ]
  node [
    id 1969
    label "portrecista"
  ]
  node [
    id 1970
    label "polifag"
  ]
  node [
    id 1971
    label "podw&#322;adny"
  ]
  node [
    id 1972
    label "dwun&#243;g"
  ]
  node [
    id 1973
    label "wapniak"
  ]
  node [
    id 1974
    label "duch"
  ]
  node [
    id 1975
    label "os&#322;abianie"
  ]
  node [
    id 1976
    label "antropochoria"
  ]
  node [
    id 1977
    label "figura"
  ]
  node [
    id 1978
    label "mikrokosmos"
  ]
  node [
    id 1979
    label "przypadkowo"
  ]
  node [
    id 1980
    label "nieuzasadniony"
  ]
  node [
    id 1981
    label "neutralizowanie"
  ]
  node [
    id 1982
    label "niestronniczy"
  ]
  node [
    id 1983
    label "zneutralizowanie"
  ]
  node [
    id 1984
    label "swobodny"
  ]
  node [
    id 1985
    label "bezstronnie"
  ]
  node [
    id 1986
    label "uczciwy"
  ]
  node [
    id 1987
    label "neutralnie"
  ]
  node [
    id 1988
    label "naturalny"
  ]
  node [
    id 1989
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 1990
    label "bierny"
  ]
  node [
    id 1991
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 1992
    label "postronny"
  ]
  node [
    id 1993
    label "konstrukcja"
  ]
  node [
    id 1994
    label "mechanika"
  ]
  node [
    id 1995
    label "group"
  ]
  node [
    id 1996
    label "The_Beatles"
  ]
  node [
    id 1997
    label "Depeche_Mode"
  ]
  node [
    id 1998
    label "zespolik"
  ]
  node [
    id 1999
    label "whole"
  ]
  node [
    id 2000
    label "schorzenie"
  ]
  node [
    id 2001
    label "batch"
  ]
  node [
    id 2002
    label "zabudowania"
  ]
  node [
    id 2003
    label "struktura_geologiczna"
  ]
  node [
    id 2004
    label "kawa&#322;"
  ]
  node [
    id 2005
    label "bry&#322;a"
  ]
  node [
    id 2006
    label "section"
  ]
  node [
    id 2007
    label "siedziba"
  ]
  node [
    id 2008
    label "filia"
  ]
  node [
    id 2009
    label "bank"
  ]
  node [
    id 2010
    label "agencja"
  ]
  node [
    id 2011
    label "ajencja"
  ]
  node [
    id 2012
    label "b&#281;ben_wielki"
  ]
  node [
    id 2013
    label "zarz&#261;d"
  ]
  node [
    id 2014
    label "o&#347;rodek"
  ]
  node [
    id 2015
    label "w&#322;adza"
  ]
  node [
    id 2016
    label "stopa"
  ]
  node [
    id 2017
    label "administration"
  ]
  node [
    id 2018
    label "milicja_obywatelska"
  ]
  node [
    id 2019
    label "ratownictwo"
  ]
  node [
    id 2020
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 2021
    label "byt"
  ]
  node [
    id 2022
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 2023
    label "nauka_prawa"
  ]
  node [
    id 2024
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 2025
    label "osobowo&#347;&#263;"
  ]
  node [
    id 2026
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 2027
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 2028
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 2029
    label "internationalization"
  ]
  node [
    id 2030
    label "transgraniczny"
  ]
  node [
    id 2031
    label "zbiorowo"
  ]
  node [
    id 2032
    label "udost&#281;pnianie"
  ]
  node [
    id 2033
    label "przebieg"
  ]
  node [
    id 2034
    label "facylitator"
  ]
  node [
    id 2035
    label "metodyka"
  ]
  node [
    id 2036
    label "legislacyjnie"
  ]
  node [
    id 2037
    label "tryb"
  ]
  node [
    id 2038
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 2039
    label "modalno&#347;&#263;"
  ]
  node [
    id 2040
    label "z&#261;b"
  ]
  node [
    id 2041
    label "koniugacja"
  ]
  node [
    id 2042
    label "funkcjonowa&#263;"
  ]
  node [
    id 2043
    label "skala"
  ]
  node [
    id 2044
    label "ko&#322;o"
  ]
  node [
    id 2045
    label "proces"
  ]
  node [
    id 2046
    label "cycle"
  ]
  node [
    id 2047
    label "linia"
  ]
  node [
    id 2048
    label "praca"
  ]
  node [
    id 2049
    label "ilo&#347;&#263;"
  ]
  node [
    id 2050
    label "sequence"
  ]
  node [
    id 2051
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 2052
    label "room"
  ]
  node [
    id 2053
    label "wdra&#380;a&#263;"
  ]
  node [
    id 2054
    label "ekspert"
  ]
  node [
    id 2055
    label "mediator"
  ]
  node [
    id 2056
    label "pedagogika"
  ]
  node [
    id 2057
    label "porada"
  ]
  node [
    id 2058
    label "metoda"
  ]
  node [
    id 2059
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 2060
    label "forum"
  ]
  node [
    id 2061
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 2062
    label "s&#261;downictwo"
  ]
  node [
    id 2063
    label "podejrzany"
  ]
  node [
    id 2064
    label "&#347;wiadek"
  ]
  node [
    id 2065
    label "instytucja"
  ]
  node [
    id 2066
    label "biuro"
  ]
  node [
    id 2067
    label "post&#281;powanie"
  ]
  node [
    id 2068
    label "court"
  ]
  node [
    id 2069
    label "my&#347;l"
  ]
  node [
    id 2070
    label "obrona"
  ]
  node [
    id 2071
    label "antylogizm"
  ]
  node [
    id 2072
    label "strona"
  ]
  node [
    id 2073
    label "oskar&#380;yciel"
  ]
  node [
    id 2074
    label "urz&#261;d"
  ]
  node [
    id 2075
    label "skazany"
  ]
  node [
    id 2076
    label "konektyw"
  ]
  node [
    id 2077
    label "pods&#261;dny"
  ]
  node [
    id 2078
    label "procesowicz"
  ]
  node [
    id 2079
    label "blend"
  ]
  node [
    id 2080
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 2081
    label "support"
  ]
  node [
    id 2082
    label "przebywa&#263;"
  ]
  node [
    id 2083
    label "stop"
  ]
  node [
    id 2084
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 2085
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 2086
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 2087
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 2088
    label "&#380;y&#263;"
  ]
  node [
    id 2089
    label "doznawa&#263;"
  ]
  node [
    id 2090
    label "przechodzi&#263;"
  ]
  node [
    id 2091
    label "wytrzymywa&#263;"
  ]
  node [
    id 2092
    label "hesitate"
  ]
  node [
    id 2093
    label "pause"
  ]
  node [
    id 2094
    label "przestawa&#263;"
  ]
  node [
    id 2095
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 2096
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 2097
    label "istnie&#263;"
  ]
  node [
    id 2098
    label "tkwi&#263;"
  ]
  node [
    id 2099
    label "wykonawca"
  ]
  node [
    id 2100
    label "interpretator"
  ]
  node [
    id 2101
    label "przesycanie"
  ]
  node [
    id 2102
    label "reflektor"
  ]
  node [
    id 2103
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 2104
    label "przesyca&#263;"
  ]
  node [
    id 2105
    label "przesyci&#263;"
  ]
  node [
    id 2106
    label "mieszanina"
  ]
  node [
    id 2107
    label "przesycenie"
  ]
  node [
    id 2108
    label "struktura_metalu"
  ]
  node [
    id 2109
    label "alia&#380;"
  ]
  node [
    id 2110
    label "znak_nakazu"
  ]
  node [
    id 2111
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 2112
    label "krzew"
  ]
  node [
    id 2113
    label "pestkowiec"
  ]
  node [
    id 2114
    label "oliwkowate"
  ]
  node [
    id 2115
    label "delfinidyna"
  ]
  node [
    id 2116
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 2117
    label "kwiat"
  ]
  node [
    id 2118
    label "owoc"
  ]
  node [
    id 2119
    label "pi&#380;maczkowate"
  ]
  node [
    id 2120
    label "lilac"
  ]
  node [
    id 2121
    label "hy&#263;ka"
  ]
  node [
    id 2122
    label "ki&#347;&#263;"
  ]
  node [
    id 2123
    label "zako&#324;czenie"
  ]
  node [
    id 2124
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 2125
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 2126
    label "&#380;ubr"
  ]
  node [
    id 2127
    label "kita"
  ]
  node [
    id 2128
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 2129
    label "p&#281;k"
  ]
  node [
    id 2130
    label "powerball"
  ]
  node [
    id 2131
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 2132
    label "r&#281;ka"
  ]
  node [
    id 2133
    label "kostka"
  ]
  node [
    id 2134
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 2135
    label "d&#322;o&#324;"
  ]
  node [
    id 2136
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 2137
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 2138
    label "ogon"
  ]
  node [
    id 2139
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 2140
    label "rurka"
  ]
  node [
    id 2141
    label "kielich"
  ]
  node [
    id 2142
    label "flakon"
  ]
  node [
    id 2143
    label "ozdoba"
  ]
  node [
    id 2144
    label "organ_ro&#347;linny"
  ]
  node [
    id 2145
    label "dno_kwiatowe"
  ]
  node [
    id 2146
    label "warga"
  ]
  node [
    id 2147
    label "przykoronek"
  ]
  node [
    id 2148
    label "korona"
  ]
  node [
    id 2149
    label "skupina"
  ]
  node [
    id 2150
    label "wykarczowanie"
  ]
  node [
    id 2151
    label "&#322;yko"
  ]
  node [
    id 2152
    label "fanerofit"
  ]
  node [
    id 2153
    label "karczowa&#263;"
  ]
  node [
    id 2154
    label "wykarczowa&#263;"
  ]
  node [
    id 2155
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 2156
    label "karczowanie"
  ]
  node [
    id 2157
    label "wypotnik"
  ]
  node [
    id 2158
    label "pochewka"
  ]
  node [
    id 2159
    label "strzyc"
  ]
  node [
    id 2160
    label "wegetacja"
  ]
  node [
    id 2161
    label "zadziorek"
  ]
  node [
    id 2162
    label "flawonoid"
  ]
  node [
    id 2163
    label "fitotron"
  ]
  node [
    id 2164
    label "w&#322;&#243;kno"
  ]
  node [
    id 2165
    label "pora&#380;a&#263;"
  ]
  node [
    id 2166
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 2167
    label "zbiorowisko"
  ]
  node [
    id 2168
    label "do&#322;owa&#263;"
  ]
  node [
    id 2169
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 2170
    label "hodowla"
  ]
  node [
    id 2171
    label "wegetowa&#263;"
  ]
  node [
    id 2172
    label "bulwka"
  ]
  node [
    id 2173
    label "sok"
  ]
  node [
    id 2174
    label "epiderma"
  ]
  node [
    id 2175
    label "g&#322;uszy&#263;"
  ]
  node [
    id 2176
    label "system_korzeniowy"
  ]
  node [
    id 2177
    label "g&#322;uszenie"
  ]
  node [
    id 2178
    label "strzy&#380;enie"
  ]
  node [
    id 2179
    label "p&#281;d"
  ]
  node [
    id 2180
    label "wegetowanie"
  ]
  node [
    id 2181
    label "fotoautotrof"
  ]
  node [
    id 2182
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 2183
    label "gumoza"
  ]
  node [
    id 2184
    label "wyro&#347;le"
  ]
  node [
    id 2185
    label "fitocenoza"
  ]
  node [
    id 2186
    label "ro&#347;liny"
  ]
  node [
    id 2187
    label "odn&#243;&#380;ka"
  ]
  node [
    id 2188
    label "do&#322;owanie"
  ]
  node [
    id 2189
    label "nieuleczalnie_chory"
  ]
  node [
    id 2190
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 2191
    label "pestka"
  ]
  node [
    id 2192
    label "fruktoza"
  ]
  node [
    id 2193
    label "produkt"
  ]
  node [
    id 2194
    label "glukoza"
  ]
  node [
    id 2195
    label "mi&#261;&#380;sz"
  ]
  node [
    id 2196
    label "owocnia"
  ]
  node [
    id 2197
    label "drylowanie"
  ]
  node [
    id 2198
    label "gniazdo_nasienne"
  ]
  node [
    id 2199
    label "frukt"
  ]
  node [
    id 2200
    label "antocyjanidyn"
  ]
  node [
    id 2201
    label "szczeciowce"
  ]
  node [
    id 2202
    label "jasnotowce"
  ]
  node [
    id 2203
    label "Oleaceae"
  ]
  node [
    id 2204
    label "bez_czarny"
  ]
  node [
    id 2205
    label "wielkopolski"
  ]
  node [
    id 2206
    label "ubytek"
  ]
  node [
    id 2207
    label "zniszczenie"
  ]
  node [
    id 2208
    label "szwank"
  ]
  node [
    id 2209
    label "niepowodzenie"
  ]
  node [
    id 2210
    label "visitation"
  ]
  node [
    id 2211
    label "spadek"
  ]
  node [
    id 2212
    label "r&#243;&#380;nica"
  ]
  node [
    id 2213
    label "brak"
  ]
  node [
    id 2214
    label "strata"
  ]
  node [
    id 2215
    label "poniszczenie"
  ]
  node [
    id 2216
    label "podpalenie"
  ]
  node [
    id 2217
    label "kondycja_fizyczna"
  ]
  node [
    id 2218
    label "spl&#261;drowanie"
  ]
  node [
    id 2219
    label "zaszkodzenie"
  ]
  node [
    id 2220
    label "ruin"
  ]
  node [
    id 2221
    label "wear"
  ]
  node [
    id 2222
    label "destruction"
  ]
  node [
    id 2223
    label "zu&#380;ycie"
  ]
  node [
    id 2224
    label "os&#322;abienie"
  ]
  node [
    id 2225
    label "attrition"
  ]
  node [
    id 2226
    label "poniszczenie_si&#281;"
  ]
  node [
    id 2227
    label "zdrowie"
  ]
  node [
    id 2228
    label "gestia"
  ]
  node [
    id 2229
    label "znawstwo"
  ]
  node [
    id 2230
    label "authority"
  ]
  node [
    id 2231
    label "sprawno&#347;&#263;"
  ]
  node [
    id 2232
    label "kiperstwo"
  ]
  node [
    id 2233
    label "information"
  ]
  node [
    id 2234
    label "praktyka"
  ]
  node [
    id 2235
    label "znajomo&#347;&#263;"
  ]
  node [
    id 2236
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 2237
    label "szybko&#347;&#263;"
  ]
  node [
    id 2238
    label "jako&#347;&#263;"
  ]
  node [
    id 2239
    label "harcerski"
  ]
  node [
    id 2240
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 2241
    label "odznaka"
  ]
  node [
    id 2242
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 2243
    label "kanonistyka"
  ]
  node [
    id 2244
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 2245
    label "kazuistyka"
  ]
  node [
    id 2246
    label "procesualistyka"
  ]
  node [
    id 2247
    label "kryminalistyka"
  ]
  node [
    id 2248
    label "prawo_karne"
  ]
  node [
    id 2249
    label "szko&#322;a"
  ]
  node [
    id 2250
    label "normatywizm"
  ]
  node [
    id 2251
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 2252
    label "umocowa&#263;"
  ]
  node [
    id 2253
    label "cywilistyka"
  ]
  node [
    id 2254
    label "jurisprudence"
  ]
  node [
    id 2255
    label "kryminologia"
  ]
  node [
    id 2256
    label "law"
  ]
  node [
    id 2257
    label "judykatura"
  ]
  node [
    id 2258
    label "przepis"
  ]
  node [
    id 2259
    label "prawo_karne_procesowe"
  ]
  node [
    id 2260
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 2261
    label "wykonawczy"
  ]
  node [
    id 2262
    label "kierunek"
  ]
  node [
    id 2263
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 2264
    label "dochodzi&#263;"
  ]
  node [
    id 2265
    label "pour"
  ]
  node [
    id 2266
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 2267
    label "przyp&#322;ywa&#263;"
  ]
  node [
    id 2268
    label "zasila&#263;"
  ]
  node [
    id 2269
    label "kapita&#322;"
  ]
  node [
    id 2270
    label "ciek_wodny"
  ]
  node [
    id 2271
    label "statek"
  ]
  node [
    id 2272
    label "flow"
  ]
  node [
    id 2273
    label "przybywa&#263;"
  ]
  node [
    id 2274
    label "nail"
  ]
  node [
    id 2275
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 2276
    label "dociera&#263;"
  ]
  node [
    id 2277
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 2278
    label "dolatywa&#263;"
  ]
  node [
    id 2279
    label "przesy&#322;ka"
  ]
  node [
    id 2280
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 2281
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 2282
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 2283
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 2284
    label "postrzega&#263;"
  ]
  node [
    id 2285
    label "zachodzi&#263;"
  ]
  node [
    id 2286
    label "orgazm"
  ]
  node [
    id 2287
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 2288
    label "doczeka&#263;"
  ]
  node [
    id 2289
    label "osi&#261;ga&#263;"
  ]
  node [
    id 2290
    label "ripen"
  ]
  node [
    id 2291
    label "submit"
  ]
  node [
    id 2292
    label "uzyskiwa&#263;"
  ]
  node [
    id 2293
    label "claim"
  ]
  node [
    id 2294
    label "dokoptowywa&#263;"
  ]
  node [
    id 2295
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 2296
    label "reach"
  ]
  node [
    id 2297
    label "dostarcza&#263;"
  ]
  node [
    id 2298
    label "digest"
  ]
  node [
    id 2299
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 2300
    label "zas&#243;b"
  ]
  node [
    id 2301
    label "zaleta"
  ]
  node [
    id 2302
    label "&#347;rodowisko"
  ]
  node [
    id 2303
    label "absolutorium"
  ]
  node [
    id 2304
    label "podupadanie"
  ]
  node [
    id 2305
    label "nap&#322;ywanie"
  ]
  node [
    id 2306
    label "podupada&#263;"
  ]
  node [
    id 2307
    label "uruchamia&#263;"
  ]
  node [
    id 2308
    label "uruchamianie"
  ]
  node [
    id 2309
    label "mienie"
  ]
  node [
    id 2310
    label "kapitalista"
  ]
  node [
    id 2311
    label "supernadz&#243;r"
  ]
  node [
    id 2312
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 2313
    label "kwestor"
  ]
  node [
    id 2314
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 2315
    label "uruchomienie"
  ]
  node [
    id 2316
    label "distribution"
  ]
  node [
    id 2317
    label "stopie&#324;"
  ]
  node [
    id 2318
    label "competence"
  ]
  node [
    id 2319
    label "fission"
  ]
  node [
    id 2320
    label "blastogeneza"
  ]
  node [
    id 2321
    label "eksdywizja"
  ]
  node [
    id 2322
    label "przebiegni&#281;cie"
  ]
  node [
    id 2323
    label "przebiec"
  ]
  node [
    id 2324
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 2325
    label "motyw"
  ]
  node [
    id 2326
    label "fabu&#322;a"
  ]
  node [
    id 2327
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 2328
    label "p&#322;&#243;d"
  ]
  node [
    id 2329
    label "szczebel"
  ]
  node [
    id 2330
    label "d&#378;wi&#281;k"
  ]
  node [
    id 2331
    label "podstopie&#324;"
  ]
  node [
    id 2332
    label "forma"
  ]
  node [
    id 2333
    label "minuta"
  ]
  node [
    id 2334
    label "podn&#243;&#380;ek"
  ]
  node [
    id 2335
    label "przymiotnik"
  ]
  node [
    id 2336
    label "gama"
  ]
  node [
    id 2337
    label "element"
  ]
  node [
    id 2338
    label "znaczenie"
  ]
  node [
    id 2339
    label "wschodek"
  ]
  node [
    id 2340
    label "ocena"
  ]
  node [
    id 2341
    label "degree"
  ]
  node [
    id 2342
    label "poziom"
  ]
  node [
    id 2343
    label "wielko&#347;&#263;"
  ]
  node [
    id 2344
    label "jednostka"
  ]
  node [
    id 2345
    label "rank"
  ]
  node [
    id 2346
    label "przys&#322;&#243;wek"
  ]
  node [
    id 2347
    label "schody"
  ]
  node [
    id 2348
    label "oddanie"
  ]
  node [
    id 2349
    label "mention"
  ]
  node [
    id 2350
    label "skill"
  ]
  node [
    id 2351
    label "po&#380;yczenie"
  ]
  node [
    id 2352
    label "dochrapanie_si&#281;"
  ]
  node [
    id 2353
    label "doznanie"
  ]
  node [
    id 2354
    label "gaze"
  ]
  node [
    id 2355
    label "cel"
  ]
  node [
    id 2356
    label "deference"
  ]
  node [
    id 2357
    label "od&#322;o&#380;enie"
  ]
  node [
    id 2358
    label "uzyskanie"
  ]
  node [
    id 2359
    label "dostarczenie"
  ]
  node [
    id 2360
    label "prototype"
  ]
  node [
    id 2361
    label "ofiarno&#347;&#263;"
  ]
  node [
    id 2362
    label "odej&#347;cie"
  ]
  node [
    id 2363
    label "powr&#243;cenie"
  ]
  node [
    id 2364
    label "danie"
  ]
  node [
    id 2365
    label "za&#322;atwienie_si&#281;"
  ]
  node [
    id 2366
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 2367
    label "sprzedanie"
  ]
  node [
    id 2368
    label "prohibition"
  ]
  node [
    id 2369
    label "reciprocation"
  ]
  node [
    id 2370
    label "przedstawienie"
  ]
  node [
    id 2371
    label "pass"
  ]
  node [
    id 2372
    label "render"
  ]
  node [
    id 2373
    label "przekazanie"
  ]
  node [
    id 2374
    label "odpowiedzenie"
  ]
  node [
    id 2375
    label "wierno&#347;&#263;"
  ]
  node [
    id 2376
    label "odst&#261;pienie"
  ]
  node [
    id 2377
    label "obtainment"
  ]
  node [
    id 2378
    label "wykonanie"
  ]
  node [
    id 2379
    label "pragnienie"
  ]
  node [
    id 2380
    label "przes&#322;anie"
  ]
  node [
    id 2381
    label "wytworzenie"
  ]
  node [
    id 2382
    label "delivery"
  ]
  node [
    id 2383
    label "nawodnienie"
  ]
  node [
    id 2384
    label "przeczulica"
  ]
  node [
    id 2385
    label "zmys&#322;"
  ]
  node [
    id 2386
    label "poczucie"
  ]
  node [
    id 2387
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 2388
    label "wy&#347;wiadczenie"
  ]
  node [
    id 2389
    label "rzecz"
  ]
  node [
    id 2390
    label "thing"
  ]
  node [
    id 2391
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 2392
    label "relatywizowanie"
  ]
  node [
    id 2393
    label "zrelatywizowa&#263;"
  ]
  node [
    id 2394
    label "zrelatywizowanie"
  ]
  node [
    id 2395
    label "pomy&#347;lenie"
  ]
  node [
    id 2396
    label "relatywizowa&#263;"
  ]
  node [
    id 2397
    label "kontakt"
  ]
  node [
    id 2398
    label "prorogation"
  ]
  node [
    id 2399
    label "op&#243;&#378;nienie"
  ]
  node [
    id 2400
    label "departure"
  ]
  node [
    id 2401
    label "pozostawienie"
  ]
  node [
    id 2402
    label "zniesienie"
  ]
  node [
    id 2403
    label "wyznaczenie"
  ]
  node [
    id 2404
    label "wstrzymanie_si&#281;"
  ]
  node [
    id 2405
    label "po&#322;o&#380;enie"
  ]
  node [
    id 2406
    label "continuance"
  ]
  node [
    id 2407
    label "rozpo&#380;yczenie"
  ]
  node [
    id 2408
    label "congratulation"
  ]
  node [
    id 2409
    label "z&#322;o&#380;enie"
  ]
  node [
    id 2410
    label "fold"
  ]
  node [
    id 2411
    label "obejmowa&#263;"
  ]
  node [
    id 2412
    label "ustala&#263;"
  ]
  node [
    id 2413
    label "mie&#263;"
  ]
  node [
    id 2414
    label "make"
  ]
  node [
    id 2415
    label "poznawa&#263;"
  ]
  node [
    id 2416
    label "ukrywa&#263;"
  ]
  node [
    id 2417
    label "exsert"
  ]
  node [
    id 2418
    label "ko&#324;czy&#263;"
  ]
  node [
    id 2419
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 2420
    label "umieszcza&#263;"
  ]
  node [
    id 2421
    label "blokowa&#263;"
  ]
  node [
    id 2422
    label "ujmowa&#263;"
  ]
  node [
    id 2423
    label "need"
  ]
  node [
    id 2424
    label "hide"
  ]
  node [
    id 2425
    label "styka&#263;_si&#281;"
  ]
  node [
    id 2426
    label "go_steady"
  ]
  node [
    id 2427
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 2428
    label "detect"
  ]
  node [
    id 2429
    label "hurt"
  ]
  node [
    id 2430
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 2431
    label "cognizance"
  ]
  node [
    id 2432
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 2433
    label "zagarnia&#263;"
  ]
  node [
    id 2434
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 2435
    label "meet"
  ]
  node [
    id 2436
    label "involve"
  ]
  node [
    id 2437
    label "zaskakiwa&#263;"
  ]
  node [
    id 2438
    label "senator"
  ]
  node [
    id 2439
    label "dotyka&#263;"
  ]
  node [
    id 2440
    label "podejmowa&#263;"
  ]
  node [
    id 2441
    label "zmienia&#263;"
  ]
  node [
    id 2442
    label "arrange"
  ]
  node [
    id 2443
    label "umacnia&#263;"
  ]
  node [
    id 2444
    label "jutro"
  ]
  node [
    id 2445
    label "chronometria"
  ]
  node [
    id 2446
    label "odczyt"
  ]
  node [
    id 2447
    label "laba"
  ]
  node [
    id 2448
    label "czasoprzestrze&#324;"
  ]
  node [
    id 2449
    label "time_period"
  ]
  node [
    id 2450
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 2451
    label "Zeitgeist"
  ]
  node [
    id 2452
    label "pochodzenie"
  ]
  node [
    id 2453
    label "przep&#322;ywanie"
  ]
  node [
    id 2454
    label "schy&#322;ek"
  ]
  node [
    id 2455
    label "czwarty_wymiar"
  ]
  node [
    id 2456
    label "poprzedzi&#263;"
  ]
  node [
    id 2457
    label "pogoda"
  ]
  node [
    id 2458
    label "czasokres"
  ]
  node [
    id 2459
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 2460
    label "poprzedzenie"
  ]
  node [
    id 2461
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 2462
    label "dzieje"
  ]
  node [
    id 2463
    label "trawi&#263;"
  ]
  node [
    id 2464
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 2465
    label "poprzedza&#263;"
  ]
  node [
    id 2466
    label "trawienie"
  ]
  node [
    id 2467
    label "chwila"
  ]
  node [
    id 2468
    label "rachuba_czasu"
  ]
  node [
    id 2469
    label "poprzedzanie"
  ]
  node [
    id 2470
    label "okres_czasu"
  ]
  node [
    id 2471
    label "period"
  ]
  node [
    id 2472
    label "odwlekanie_si&#281;"
  ]
  node [
    id 2473
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 2474
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 2475
    label "pochodzi&#263;"
  ]
  node [
    id 2476
    label "dzie&#324;_jutrzejszy"
  ]
  node [
    id 2477
    label "jutrzejszy"
  ]
  node [
    id 2478
    label "blisko"
  ]
  node [
    id 2479
    label "inszy"
  ]
  node [
    id 2480
    label "inaczej"
  ]
  node [
    id 2481
    label "osobno"
  ]
  node [
    id 2482
    label "r&#243;&#380;ny"
  ]
  node [
    id 2483
    label "odr&#281;bny"
  ]
  node [
    id 2484
    label "kolejno"
  ]
  node [
    id 2485
    label "kt&#243;ry&#347;"
  ]
  node [
    id 2486
    label "nastopny"
  ]
  node [
    id 2487
    label "r&#243;&#380;nie"
  ]
  node [
    id 2488
    label "niestandardowo"
  ]
  node [
    id 2489
    label "osobny"
  ]
  node [
    id 2490
    label "osobnie"
  ]
  node [
    id 2491
    label "odr&#281;bnie"
  ]
  node [
    id 2492
    label "udzielnie"
  ]
  node [
    id 2493
    label "individually"
  ]
  node [
    id 2494
    label "oznaka"
  ]
  node [
    id 2495
    label "odmienianie"
  ]
  node [
    id 2496
    label "zmianka"
  ]
  node [
    id 2497
    label "amendment"
  ]
  node [
    id 2498
    label "rewizja"
  ]
  node [
    id 2499
    label "komplet"
  ]
  node [
    id 2500
    label "tura"
  ]
  node [
    id 2501
    label "change"
  ]
  node [
    id 2502
    label "ferment"
  ]
  node [
    id 2503
    label "anatomopatolog"
  ]
  node [
    id 2504
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 2505
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 2506
    label "przywidzenie"
  ]
  node [
    id 2507
    label "boski"
  ]
  node [
    id 2508
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 2509
    label "krajobraz"
  ]
  node [
    id 2510
    label "presence"
  ]
  node [
    id 2511
    label "lekcja"
  ]
  node [
    id 2512
    label "ensemble"
  ]
  node [
    id 2513
    label "klasa"
  ]
  node [
    id 2514
    label "zestaw"
  ]
  node [
    id 2515
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 2516
    label "signal"
  ]
  node [
    id 2517
    label "symbol"
  ]
  node [
    id 2518
    label "biokatalizator"
  ]
  node [
    id 2519
    label "bia&#322;ko"
  ]
  node [
    id 2520
    label "zymaza"
  ]
  node [
    id 2521
    label "poruszenie"
  ]
  node [
    id 2522
    label "immobilizowa&#263;"
  ]
  node [
    id 2523
    label "immobilizacja"
  ]
  node [
    id 2524
    label "apoenzym"
  ]
  node [
    id 2525
    label "immobilizowanie"
  ]
  node [
    id 2526
    label "enzyme"
  ]
  node [
    id 2527
    label "proces_my&#347;lowy"
  ]
  node [
    id 2528
    label "odwo&#322;anie"
  ]
  node [
    id 2529
    label "checkup"
  ]
  node [
    id 2530
    label "krytyka"
  ]
  node [
    id 2531
    label "correction"
  ]
  node [
    id 2532
    label "kipisz"
  ]
  node [
    id 2533
    label "przegl&#261;d"
  ]
  node [
    id 2534
    label "korekta"
  ]
  node [
    id 2535
    label "dow&#243;d"
  ]
  node [
    id 2536
    label "kontrola"
  ]
  node [
    id 2537
    label "rekurs"
  ]
  node [
    id 2538
    label "zaw&#243;d"
  ]
  node [
    id 2539
    label "pracowanie"
  ]
  node [
    id 2540
    label "pracowa&#263;"
  ]
  node [
    id 2541
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 2542
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 2543
    label "stosunek_pracy"
  ]
  node [
    id 2544
    label "kierownictwo"
  ]
  node [
    id 2545
    label "najem"
  ]
  node [
    id 2546
    label "zak&#322;ad"
  ]
  node [
    id 2547
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 2548
    label "tynkarski"
  ]
  node [
    id 2549
    label "tyrka"
  ]
  node [
    id 2550
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 2551
    label "benedykty&#324;ski"
  ]
  node [
    id 2552
    label "poda&#380;_pracy"
  ]
  node [
    id 2553
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 2554
    label "patolog"
  ]
  node [
    id 2555
    label "anatom"
  ]
  node [
    id 2556
    label "Transfiguration"
  ]
  node [
    id 2557
    label "zmienianie"
  ]
  node [
    id 2558
    label "wymienianie"
  ]
  node [
    id 2559
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 2560
    label "zamiana"
  ]
  node [
    id 2561
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 2562
    label "zaaresztowa&#263;"
  ]
  node [
    id 2563
    label "zaczepi&#263;"
  ]
  node [
    id 2564
    label "unieruchomi&#263;"
  ]
  node [
    id 2565
    label "przechowa&#263;"
  ]
  node [
    id 2566
    label "przerwa&#263;"
  ]
  node [
    id 2567
    label "zabra&#263;"
  ]
  node [
    id 2568
    label "continue"
  ]
  node [
    id 2569
    label "bankrupt"
  ]
  node [
    id 2570
    label "anticipate"
  ]
  node [
    id 2571
    label "bury"
  ]
  node [
    id 2572
    label "zacz&#261;&#263;"
  ]
  node [
    id 2573
    label "p&#281;tla"
  ]
  node [
    id 2574
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 2575
    label "zjednoczy&#263;"
  ]
  node [
    id 2576
    label "ally"
  ]
  node [
    id 2577
    label "connect"
  ]
  node [
    id 2578
    label "perpetrate"
  ]
  node [
    id 2579
    label "obowi&#261;za&#263;"
  ]
  node [
    id 2580
    label "articulation"
  ]
  node [
    id 2581
    label "catch"
  ]
  node [
    id 2582
    label "dokoptowa&#263;"
  ]
  node [
    id 2583
    label "stworzy&#263;"
  ]
  node [
    id 2584
    label "owin&#261;&#263;"
  ]
  node [
    id 2585
    label "pack"
  ]
  node [
    id 2586
    label "narosn&#261;&#263;"
  ]
  node [
    id 2587
    label "znieruchomie&#263;"
  ]
  node [
    id 2588
    label "zg&#281;stnie&#263;"
  ]
  node [
    id 2589
    label "stwardnie&#263;"
  ]
  node [
    id 2590
    label "clot"
  ]
  node [
    id 2591
    label "przybra&#263;_na_sile"
  ]
  node [
    id 2592
    label "solidify"
  ]
  node [
    id 2593
    label "zwielokrotni&#263;_si&#281;"
  ]
  node [
    id 2594
    label "os&#322;abi&#263;"
  ]
  node [
    id 2595
    label "overwhelm"
  ]
  node [
    id 2596
    label "porazi&#263;"
  ]
  node [
    id 2597
    label "przetw&#243;r"
  ]
  node [
    id 2598
    label "kastra"
  ]
  node [
    id 2599
    label "przyprawa"
  ]
  node [
    id 2600
    label "zgrupowanie"
  ]
  node [
    id 2601
    label "podk&#322;ad"
  ]
  node [
    id 2602
    label "training"
  ]
  node [
    id 2603
    label "s&#322;oik"
  ]
  node [
    id 2604
    label "&#263;wiczenie"
  ]
  node [
    id 2605
    label "ruch"
  ]
  node [
    id 2606
    label "obw&#243;d"
  ]
  node [
    id 2607
    label "materia&#322;_budowlany"
  ]
  node [
    id 2608
    label "mortar"
  ]
  node [
    id 2609
    label "graf"
  ]
  node [
    id 2610
    label "band"
  ]
  node [
    id 2611
    label "argument"
  ]
  node [
    id 2612
    label "&#378;d&#378;b&#322;o"
  ]
  node [
    id 2613
    label "fala_stoj&#261;ca"
  ]
  node [
    id 2614
    label "mila_morska"
  ]
  node [
    id 2615
    label "uczesanie"
  ]
  node [
    id 2616
    label "jednostka_pr&#281;dko&#347;ci"
  ]
  node [
    id 2617
    label "zgrubienie"
  ]
  node [
    id 2618
    label "pismo_klinowe"
  ]
  node [
    id 2619
    label "problem"
  ]
  node [
    id 2620
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 2621
    label "akcja"
  ]
  node [
    id 2622
    label "przeci&#281;cie"
  ]
  node [
    id 2623
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 2624
    label "hitch"
  ]
  node [
    id 2625
    label "orbita"
  ]
  node [
    id 2626
    label "kryszta&#322;"
  ]
  node [
    id 2627
    label "ekliptyka"
  ]
  node [
    id 2628
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 2629
    label "wiciowiec"
  ]
  node [
    id 2630
    label "tobo&#322;ki"
  ]
  node [
    id 2631
    label "alga"
  ]
  node [
    id 2632
    label "tob&#243;&#322;"
  ]
  node [
    id 2633
    label "tworzywo"
  ]
  node [
    id 2634
    label "tkanka_kostna"
  ]
  node [
    id 2635
    label "wertebroplastyka"
  ]
  node [
    id 2636
    label "wype&#322;nienie"
  ]
  node [
    id 2637
    label "spoiwo"
  ]
  node [
    id 2638
    label "podkomisja"
  ]
  node [
    id 2639
    label "Komisja_Europejska"
  ]
  node [
    id 2640
    label "subcommittee"
  ]
  node [
    id 2641
    label "eksponowa&#263;"
  ]
  node [
    id 2642
    label "g&#243;rowa&#263;"
  ]
  node [
    id 2643
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 2644
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 2645
    label "sterowa&#263;"
  ]
  node [
    id 2646
    label "kierowa&#263;"
  ]
  node [
    id 2647
    label "string"
  ]
  node [
    id 2648
    label "control"
  ]
  node [
    id 2649
    label "kre&#347;li&#263;"
  ]
  node [
    id 2650
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 2651
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 2652
    label "partner"
  ]
  node [
    id 2653
    label "linia_melodyczna"
  ]
  node [
    id 2654
    label "prowadzenie"
  ]
  node [
    id 2655
    label "ukierunkowywa&#263;"
  ]
  node [
    id 2656
    label "przesuwa&#263;"
  ]
  node [
    id 2657
    label "tworzy&#263;"
  ]
  node [
    id 2658
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 2659
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 2660
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 2661
    label "navigate"
  ]
  node [
    id 2662
    label "krzywa"
  ]
  node [
    id 2663
    label "manipulate"
  ]
  node [
    id 2664
    label "motywowa&#263;"
  ]
  node [
    id 2665
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 2666
    label "oszukiwa&#263;"
  ]
  node [
    id 2667
    label "tentegowa&#263;"
  ]
  node [
    id 2668
    label "urz&#261;dza&#263;"
  ]
  node [
    id 2669
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 2670
    label "czyni&#263;"
  ]
  node [
    id 2671
    label "przerabia&#263;"
  ]
  node [
    id 2672
    label "post&#281;powa&#263;"
  ]
  node [
    id 2673
    label "organizowa&#263;"
  ]
  node [
    id 2674
    label "falowa&#263;"
  ]
  node [
    id 2675
    label "stylizowa&#263;"
  ]
  node [
    id 2676
    label "wydala&#263;"
  ]
  node [
    id 2677
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 2678
    label "ukazywa&#263;"
  ]
  node [
    id 2679
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 2680
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 2681
    label "delineate"
  ]
  node [
    id 2682
    label "rysowa&#263;"
  ]
  node [
    id 2683
    label "report"
  ]
  node [
    id 2684
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 2685
    label "usuwa&#263;"
  ]
  node [
    id 2686
    label "draw"
  ]
  node [
    id 2687
    label "describe"
  ]
  node [
    id 2688
    label "clear"
  ]
  node [
    id 2689
    label "get"
  ]
  node [
    id 2690
    label "wytwarza&#263;"
  ]
  node [
    id 2691
    label "consist"
  ]
  node [
    id 2692
    label "raise"
  ]
  node [
    id 2693
    label "pope&#322;nia&#263;"
  ]
  node [
    id 2694
    label "napromieniowywa&#263;"
  ]
  node [
    id 2695
    label "demonstrowa&#263;"
  ]
  node [
    id 2696
    label "podkre&#347;la&#263;"
  ]
  node [
    id 2697
    label "manipulowa&#263;"
  ]
  node [
    id 2698
    label "trzyma&#263;"
  ]
  node [
    id 2699
    label "przeznacza&#263;"
  ]
  node [
    id 2700
    label "administrowa&#263;"
  ]
  node [
    id 2701
    label "order"
  ]
  node [
    id 2702
    label "ustawia&#263;"
  ]
  node [
    id 2703
    label "zwierzchnik"
  ]
  node [
    id 2704
    label "wysy&#322;a&#263;"
  ]
  node [
    id 2705
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 2706
    label "indicate"
  ]
  node [
    id 2707
    label "przekracza&#263;"
  ]
  node [
    id 2708
    label "undertaking"
  ]
  node [
    id 2709
    label "base_on_balls"
  ]
  node [
    id 2710
    label "wyprzedza&#263;"
  ]
  node [
    id 2711
    label "chop"
  ]
  node [
    id 2712
    label "wygrywa&#263;"
  ]
  node [
    id 2713
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 2714
    label "suffice"
  ]
  node [
    id 2715
    label "uprawia&#263;_seks"
  ]
  node [
    id 2716
    label "treat"
  ]
  node [
    id 2717
    label "zaspokaja&#263;"
  ]
  node [
    id 2718
    label "serve"
  ]
  node [
    id 2719
    label "zaspakaja&#263;"
  ]
  node [
    id 2720
    label "dostosowywa&#263;"
  ]
  node [
    id 2721
    label "rusza&#263;"
  ]
  node [
    id 2722
    label "translate"
  ]
  node [
    id 2723
    label "przenosi&#263;"
  ]
  node [
    id 2724
    label "go"
  ]
  node [
    id 2725
    label "postpone"
  ]
  node [
    id 2726
    label "transfer"
  ]
  node [
    id 2727
    label "estrange"
  ]
  node [
    id 2728
    label "przestawia&#263;"
  ]
  node [
    id 2729
    label "wyznacza&#263;"
  ]
  node [
    id 2730
    label "marshal"
  ]
  node [
    id 2731
    label "nadawa&#263;"
  ]
  node [
    id 2732
    label "shape"
  ]
  node [
    id 2733
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 2734
    label "stay"
  ]
  node [
    id 2735
    label "klawisz"
  ]
  node [
    id 2736
    label "curve"
  ]
  node [
    id 2737
    label "curvature"
  ]
  node [
    id 2738
    label "figura_geometryczna"
  ]
  node [
    id 2739
    label "poprowadzi&#263;"
  ]
  node [
    id 2740
    label "sprout"
  ]
  node [
    id 2741
    label "wystawa&#263;"
  ]
  node [
    id 2742
    label "wprowadzanie"
  ]
  node [
    id 2743
    label "g&#243;rowanie"
  ]
  node [
    id 2744
    label "ukierunkowywanie"
  ]
  node [
    id 2745
    label "poprowadzenie"
  ]
  node [
    id 2746
    label "eksponowanie"
  ]
  node [
    id 2747
    label "doprowadzenie"
  ]
  node [
    id 2748
    label "przeci&#261;ganie"
  ]
  node [
    id 2749
    label "sterowanie"
  ]
  node [
    id 2750
    label "dysponowanie"
  ]
  node [
    id 2751
    label "kszta&#322;towanie"
  ]
  node [
    id 2752
    label "management"
  ]
  node [
    id 2753
    label "trzymanie"
  ]
  node [
    id 2754
    label "dawanie"
  ]
  node [
    id 2755
    label "drive"
  ]
  node [
    id 2756
    label "przywodzenie"
  ]
  node [
    id 2757
    label "wprowadzenie"
  ]
  node [
    id 2758
    label "aim"
  ]
  node [
    id 2759
    label "lead"
  ]
  node [
    id 2760
    label "oprowadzenie"
  ]
  node [
    id 2761
    label "prowadzanie"
  ]
  node [
    id 2762
    label "oprowadzanie"
  ]
  node [
    id 2763
    label "przewy&#380;szanie"
  ]
  node [
    id 2764
    label "kierowanie"
  ]
  node [
    id 2765
    label "zwracanie"
  ]
  node [
    id 2766
    label "granie"
  ]
  node [
    id 2767
    label "ta&#324;czenie"
  ]
  node [
    id 2768
    label "kre&#347;lenie"
  ]
  node [
    id 2769
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 2770
    label "zaprowadzanie"
  ]
  node [
    id 2771
    label "pozarz&#261;dzanie"
  ]
  node [
    id 2772
    label "przecinanie"
  ]
  node [
    id 2773
    label "doprowadzanie"
  ]
  node [
    id 2774
    label "aktor"
  ]
  node [
    id 2775
    label "sp&#243;lnik"
  ]
  node [
    id 2776
    label "kolaborator"
  ]
  node [
    id 2777
    label "uczestniczenie"
  ]
  node [
    id 2778
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 2779
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 2780
    label "pracownik"
  ]
  node [
    id 2781
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 2782
    label "przedsi&#281;biorca"
  ]
  node [
    id 2783
    label "nature"
  ]
  node [
    id 2784
    label "tycze&#263;"
  ]
  node [
    id 2785
    label "bargain"
  ]
  node [
    id 2786
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 2787
    label "za&#322;o&#380;enie"
  ]
  node [
    id 2788
    label "strategia"
  ]
  node [
    id 2789
    label "background"
  ]
  node [
    id 2790
    label "punkt_odniesienia"
  ]
  node [
    id 2791
    label "zasadzenie"
  ]
  node [
    id 2792
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 2793
    label "&#347;ciana"
  ]
  node [
    id 2794
    label "podstawowy"
  ]
  node [
    id 2795
    label "dzieci&#281;ctwo"
  ]
  node [
    id 2796
    label "d&#243;&#322;"
  ]
  node [
    id 2797
    label "documentation"
  ]
  node [
    id 2798
    label "bok"
  ]
  node [
    id 2799
    label "pomys&#322;"
  ]
  node [
    id 2800
    label "zasadzi&#263;"
  ]
  node [
    id 2801
    label "column"
  ]
  node [
    id 2802
    label "pot&#281;ga"
  ]
  node [
    id 2803
    label "infliction"
  ]
  node [
    id 2804
    label "proposition"
  ]
  node [
    id 2805
    label "przygotowanie"
  ]
  node [
    id 2806
    label "pozak&#322;adanie"
  ]
  node [
    id 2807
    label "point"
  ]
  node [
    id 2808
    label "program"
  ]
  node [
    id 2809
    label "poubieranie"
  ]
  node [
    id 2810
    label "rozebranie"
  ]
  node [
    id 2811
    label "str&#243;j"
  ]
  node [
    id 2812
    label "przewidzenie"
  ]
  node [
    id 2813
    label "zak&#322;adka"
  ]
  node [
    id 2814
    label "przygotowywanie"
  ]
  node [
    id 2815
    label "podwini&#281;cie"
  ]
  node [
    id 2816
    label "zap&#322;acenie"
  ]
  node [
    id 2817
    label "wyko&#324;czenie"
  ]
  node [
    id 2818
    label "utworzenie"
  ]
  node [
    id 2819
    label "przebranie"
  ]
  node [
    id 2820
    label "obleczenie"
  ]
  node [
    id 2821
    label "przymierzenie"
  ]
  node [
    id 2822
    label "obleczenie_si&#281;"
  ]
  node [
    id 2823
    label "przywdzianie"
  ]
  node [
    id 2824
    label "przyodzianie"
  ]
  node [
    id 2825
    label "pokrycie"
  ]
  node [
    id 2826
    label "odcinek"
  ]
  node [
    id 2827
    label "strzelba"
  ]
  node [
    id 2828
    label "wielok&#261;t"
  ]
  node [
    id 2829
    label "tu&#322;&#243;w"
  ]
  node [
    id 2830
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 2831
    label "lufa"
  ]
  node [
    id 2832
    label "wyrobisko"
  ]
  node [
    id 2833
    label "trudno&#347;&#263;"
  ]
  node [
    id 2834
    label "kres"
  ]
  node [
    id 2835
    label "bariera"
  ]
  node [
    id 2836
    label "przegroda"
  ]
  node [
    id 2837
    label "profil"
  ]
  node [
    id 2838
    label "facebook"
  ]
  node [
    id 2839
    label "zbocze"
  ]
  node [
    id 2840
    label "obstruction"
  ]
  node [
    id 2841
    label "pow&#322;oka"
  ]
  node [
    id 2842
    label "wielo&#347;cian"
  ]
  node [
    id 2843
    label "discipline"
  ]
  node [
    id 2844
    label "zboczy&#263;"
  ]
  node [
    id 2845
    label "w&#261;tek"
  ]
  node [
    id 2846
    label "entity"
  ]
  node [
    id 2847
    label "sponiewiera&#263;"
  ]
  node [
    id 2848
    label "zboczenie"
  ]
  node [
    id 2849
    label "zbaczanie"
  ]
  node [
    id 2850
    label "om&#243;wi&#263;"
  ]
  node [
    id 2851
    label "tre&#347;&#263;"
  ]
  node [
    id 2852
    label "kr&#261;&#380;enie"
  ]
  node [
    id 2853
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 2854
    label "istota"
  ]
  node [
    id 2855
    label "zbacza&#263;"
  ]
  node [
    id 2856
    label "om&#243;wienie"
  ]
  node [
    id 2857
    label "tematyka"
  ]
  node [
    id 2858
    label "omawianie"
  ]
  node [
    id 2859
    label "omawia&#263;"
  ]
  node [
    id 2860
    label "program_nauczania"
  ]
  node [
    id 2861
    label "sponiewieranie"
  ]
  node [
    id 2862
    label "low"
  ]
  node [
    id 2863
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 2864
    label "wykopanie"
  ]
  node [
    id 2865
    label "hole"
  ]
  node [
    id 2866
    label "wykopywa&#263;"
  ]
  node [
    id 2867
    label "za&#322;amanie"
  ]
  node [
    id 2868
    label "wykopa&#263;"
  ]
  node [
    id 2869
    label "&#347;piew"
  ]
  node [
    id 2870
    label "wykopywanie"
  ]
  node [
    id 2871
    label "depressive_disorder"
  ]
  node [
    id 2872
    label "niski"
  ]
  node [
    id 2873
    label "pocz&#261;tkowy"
  ]
  node [
    id 2874
    label "najwa&#380;niejszy"
  ]
  node [
    id 2875
    label "podstawowo"
  ]
  node [
    id 2876
    label "niezaawansowany"
  ]
  node [
    id 2877
    label "przetkanie"
  ]
  node [
    id 2878
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 2879
    label "zaczerpni&#281;cie"
  ]
  node [
    id 2880
    label "anchor"
  ]
  node [
    id 2881
    label "wetkni&#281;cie"
  ]
  node [
    id 2882
    label "interposition"
  ]
  node [
    id 2883
    label "przymocowanie"
  ]
  node [
    id 2884
    label "przymocowa&#263;"
  ]
  node [
    id 2885
    label "osnowa&#263;"
  ]
  node [
    id 2886
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 2887
    label "wetkn&#261;&#263;"
  ]
  node [
    id 2888
    label "pocz&#261;tki"
  ]
  node [
    id 2889
    label "dzieci&#324;stwo"
  ]
  node [
    id 2890
    label "ukradzenie"
  ]
  node [
    id 2891
    label "ukra&#347;&#263;"
  ]
  node [
    id 2892
    label "idea"
  ]
  node [
    id 2893
    label "operacja"
  ]
  node [
    id 2894
    label "doktryna"
  ]
  node [
    id 2895
    label "plan"
  ]
  node [
    id 2896
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 2897
    label "dziedzina"
  ]
  node [
    id 2898
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 2899
    label "wrinkle"
  ]
  node [
    id 2900
    label "wzorzec_projektowy"
  ]
  node [
    id 2901
    label "iloczyn"
  ]
  node [
    id 2902
    label "violence"
  ]
  node [
    id 2903
    label "wojsko"
  ]
  node [
    id 2904
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 2905
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 2906
    label "potencja"
  ]
  node [
    id 2907
    label "guidance"
  ]
  node [
    id 2908
    label "polecenie"
  ]
  node [
    id 2909
    label "consign"
  ]
  node [
    id 2910
    label "pognanie"
  ]
  node [
    id 2911
    label "recommendation"
  ]
  node [
    id 2912
    label "pobiegni&#281;cie"
  ]
  node [
    id 2913
    label "powierzenie"
  ]
  node [
    id 2914
    label "doradzenie"
  ]
  node [
    id 2915
    label "education"
  ]
  node [
    id 2916
    label "zaordynowanie"
  ]
  node [
    id 2917
    label "przesadzenie"
  ]
  node [
    id 2918
    label "ukaz"
  ]
  node [
    id 2919
    label "rekomendacja"
  ]
  node [
    id 2920
    label "statement"
  ]
  node [
    id 2921
    label "zadanie"
  ]
  node [
    id 2922
    label "pozwoli&#263;"
  ]
  node [
    id 2923
    label "nada&#263;"
  ]
  node [
    id 2924
    label "authorize"
  ]
  node [
    id 2925
    label "dramatize"
  ]
  node [
    id 2926
    label "donie&#347;&#263;"
  ]
  node [
    id 2927
    label "da&#263;"
  ]
  node [
    id 2928
    label "za&#322;atwi&#263;"
  ]
  node [
    id 2929
    label "zarekomendowa&#263;"
  ]
  node [
    id 2930
    label "przes&#322;a&#263;"
  ]
  node [
    id 2931
    label "pofolgowa&#263;"
  ]
  node [
    id 2932
    label "uzna&#263;"
  ]
  node [
    id 2933
    label "assent"
  ]
  node [
    id 2934
    label "narada"
  ]
  node [
    id 2935
    label "kryterium"
  ]
  node [
    id 2936
    label "sofcik"
  ]
  node [
    id 2937
    label "pogl&#261;d"
  ]
  node [
    id 2938
    label "appraisal"
  ]
  node [
    id 2939
    label "Komitet_Obrony_Robotnik&#243;w"
  ]
  node [
    id 2940
    label "zbiera&#263;"
  ]
  node [
    id 2941
    label "opracowywa&#263;"
  ]
  node [
    id 2942
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 2943
    label "oddawa&#263;"
  ]
  node [
    id 2944
    label "dawa&#263;"
  ]
  node [
    id 2945
    label "uk&#322;ada&#263;"
  ]
  node [
    id 2946
    label "convey"
  ]
  node [
    id 2947
    label "publicize"
  ]
  node [
    id 2948
    label "dzieli&#263;"
  ]
  node [
    id 2949
    label "przekazywa&#263;"
  ]
  node [
    id 2950
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 2951
    label "przywraca&#263;"
  ]
  node [
    id 2952
    label "scala&#263;"
  ]
  node [
    id 2953
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 2954
    label "train"
  ]
  node [
    id 2955
    label "liczy&#263;"
  ]
  node [
    id 2956
    label "divide"
  ]
  node [
    id 2957
    label "rozdawa&#263;"
  ]
  node [
    id 2958
    label "korzysta&#263;"
  ]
  node [
    id 2959
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 2960
    label "share"
  ]
  node [
    id 2961
    label "sk&#322;&#243;ca&#263;"
  ]
  node [
    id 2962
    label "iloraz"
  ]
  node [
    id 2963
    label "assign"
  ]
  node [
    id 2964
    label "deal"
  ]
  node [
    id 2965
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 2966
    label "odst&#281;powa&#263;"
  ]
  node [
    id 2967
    label "odpowiada&#263;"
  ]
  node [
    id 2968
    label "sprzedawa&#263;"
  ]
  node [
    id 2969
    label "reflect"
  ]
  node [
    id 2970
    label "blurt_out"
  ]
  node [
    id 2971
    label "impart"
  ]
  node [
    id 2972
    label "surrender"
  ]
  node [
    id 2973
    label "sacrifice"
  ]
  node [
    id 2974
    label "deliver"
  ]
  node [
    id 2975
    label "pozyskiwa&#263;"
  ]
  node [
    id 2976
    label "dostawa&#263;"
  ]
  node [
    id 2977
    label "poci&#261;ga&#263;"
  ]
  node [
    id 2978
    label "przejmowa&#263;"
  ]
  node [
    id 2979
    label "congregate"
  ]
  node [
    id 2980
    label "bra&#263;"
  ]
  node [
    id 2981
    label "gromadzi&#263;"
  ]
  node [
    id 2982
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 2983
    label "consolidate"
  ]
  node [
    id 2984
    label "wzbiera&#263;"
  ]
  node [
    id 2985
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 2986
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 2987
    label "zachowywa&#263;"
  ]
  node [
    id 2988
    label "chroni&#263;"
  ]
  node [
    id 2989
    label "gospodarowa&#263;"
  ]
  node [
    id 2990
    label "preserve"
  ]
  node [
    id 2991
    label "darowywa&#263;"
  ]
  node [
    id 2992
    label "znosi&#263;"
  ]
  node [
    id 2993
    label "przygotowywa&#263;"
  ]
  node [
    id 2994
    label "treser"
  ]
  node [
    id 2995
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 2996
    label "porz&#261;dkowa&#263;"
  ]
  node [
    id 2997
    label "dispose"
  ]
  node [
    id 2998
    label "kszta&#322;ci&#263;"
  ]
  node [
    id 2999
    label "uczy&#263;"
  ]
  node [
    id 3000
    label "seat"
  ]
  node [
    id 3001
    label "pokrywa&#263;"
  ]
  node [
    id 3002
    label "pozostawia&#263;"
  ]
  node [
    id 3003
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 3004
    label "wk&#322;ada&#263;"
  ]
  node [
    id 3005
    label "inspirowa&#263;"
  ]
  node [
    id 3006
    label "zaczyna&#263;"
  ]
  node [
    id 3007
    label "wpaja&#263;"
  ]
  node [
    id 3008
    label "psu&#263;"
  ]
  node [
    id 3009
    label "wzbudza&#263;"
  ]
  node [
    id 3010
    label "go&#347;ci&#263;"
  ]
  node [
    id 3011
    label "zyskiwa&#263;"
  ]
  node [
    id 3012
    label "alternate"
  ]
  node [
    id 3013
    label "traci&#263;"
  ]
  node [
    id 3014
    label "zast&#281;powa&#263;"
  ]
  node [
    id 3015
    label "sprawia&#263;"
  ]
  node [
    id 3016
    label "reengineering"
  ]
  node [
    id 3017
    label "jednoczy&#263;"
  ]
  node [
    id 3018
    label "wp&#322;aca&#263;"
  ]
  node [
    id 3019
    label "sygna&#322;"
  ]
  node [
    id 3020
    label "podawa&#263;"
  ]
  node [
    id 3021
    label "doprowadza&#263;"
  ]
  node [
    id 3022
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 3023
    label "powierza&#263;"
  ]
  node [
    id 3024
    label "wpiernicza&#263;"
  ]
  node [
    id 3025
    label "tender"
  ]
  node [
    id 3026
    label "hold_out"
  ]
  node [
    id 3027
    label "rap"
  ]
  node [
    id 3028
    label "obiecywa&#263;"
  ]
  node [
    id 3029
    label "&#322;adowa&#263;"
  ]
  node [
    id 3030
    label "t&#322;uc"
  ]
  node [
    id 3031
    label "nalewa&#263;"
  ]
  node [
    id 3032
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 3033
    label "zezwala&#263;"
  ]
  node [
    id 3034
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 3035
    label "p&#322;aci&#263;"
  ]
  node [
    id 3036
    label "traktowa&#263;"
  ]
  node [
    id 3037
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 3038
    label "przykrochmala&#263;"
  ]
  node [
    id 3039
    label "zbli&#380;a&#263;"
  ]
  node [
    id 3040
    label "urge"
  ]
  node [
    id 3041
    label "dopieprza&#263;"
  ]
  node [
    id 3042
    label "press"
  ]
  node [
    id 3043
    label "uderza&#263;"
  ]
  node [
    id 3044
    label "gem"
  ]
  node [
    id 3045
    label "muzyka"
  ]
  node [
    id 3046
    label "runda"
  ]
  node [
    id 3047
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 3048
    label "substytuowanie"
  ]
  node [
    id 3049
    label "zast&#281;pca"
  ]
  node [
    id 3050
    label "substytuowa&#263;"
  ]
  node [
    id 3051
    label "przyk&#322;ad"
  ]
  node [
    id 3052
    label "shaft"
  ]
  node [
    id 3053
    label "ptaszek"
  ]
  node [
    id 3054
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 3055
    label "przyrodzenie"
  ]
  node [
    id 3056
    label "fiut"
  ]
  node [
    id 3057
    label "element_anatomiczny"
  ]
  node [
    id 3058
    label "wchodzenie"
  ]
  node [
    id 3059
    label "podstawienie"
  ]
  node [
    id 3060
    label "wskazanie"
  ]
  node [
    id 3061
    label "podstawianie"
  ]
  node [
    id 3062
    label "wskazywanie"
  ]
  node [
    id 3063
    label "pe&#322;nomocnik"
  ]
  node [
    id 3064
    label "protezowa&#263;"
  ]
  node [
    id 3065
    label "podstawi&#263;"
  ]
  node [
    id 3066
    label "podstawia&#263;"
  ]
  node [
    id 3067
    label "zast&#261;pi&#263;"
  ]
  node [
    id 3068
    label "wskazywa&#263;"
  ]
  node [
    id 3069
    label "ilustracja"
  ]
  node [
    id 3070
    label "zg&#322;asza&#263;"
  ]
  node [
    id 3071
    label "favor"
  ]
  node [
    id 3072
    label "write"
  ]
  node [
    id 3073
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 3074
    label "wychodzi&#263;"
  ]
  node [
    id 3075
    label "seclude"
  ]
  node [
    id 3076
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 3077
    label "overture"
  ]
  node [
    id 3078
    label "perform"
  ]
  node [
    id 3079
    label "dzia&#322;a&#263;"
  ]
  node [
    id 3080
    label "appear"
  ]
  node [
    id 3081
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 3082
    label "rezygnowa&#263;"
  ]
  node [
    id 3083
    label "nak&#322;ania&#263;"
  ]
  node [
    id 3084
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 3085
    label "wole&#263;"
  ]
  node [
    id 3086
    label "uznawa&#263;"
  ]
  node [
    id 3087
    label "prym"
  ]
  node [
    id 3088
    label "content"
  ]
  node [
    id 3089
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 3090
    label "miliradian"
  ]
  node [
    id 3091
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 3092
    label "berylowiec"
  ]
  node [
    id 3093
    label "zadowolenie_si&#281;"
  ]
  node [
    id 3094
    label "mikroradian"
  ]
  node [
    id 3095
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 3096
    label "jednostka_promieniowania"
  ]
  node [
    id 3097
    label "wyewoluowanie"
  ]
  node [
    id 3098
    label "przyswojenie"
  ]
  node [
    id 3099
    label "one"
  ]
  node [
    id 3100
    label "przelicza&#263;"
  ]
  node [
    id 3101
    label "starzenie_si&#281;"
  ]
  node [
    id 3102
    label "przyswajanie"
  ]
  node [
    id 3103
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 3104
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 3105
    label "przeliczanie"
  ]
  node [
    id 3106
    label "przeliczy&#263;"
  ]
  node [
    id 3107
    label "matematyka"
  ]
  node [
    id 3108
    label "ewoluowanie"
  ]
  node [
    id 3109
    label "ewoluowa&#263;"
  ]
  node [
    id 3110
    label "czynnik_biotyczny"
  ]
  node [
    id 3111
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 3112
    label "przyswaja&#263;"
  ]
  node [
    id 3113
    label "rzut"
  ]
  node [
    id 3114
    label "przeliczenie"
  ]
  node [
    id 3115
    label "wyewoluowa&#263;"
  ]
  node [
    id 3116
    label "supremum"
  ]
  node [
    id 3117
    label "individual"
  ]
  node [
    id 3118
    label "infimum"
  ]
  node [
    id 3119
    label "liczba_naturalna"
  ]
  node [
    id 3120
    label "metal"
  ]
  node [
    id 3121
    label "radian"
  ]
  node [
    id 3122
    label "nanoradian"
  ]
  node [
    id 3123
    label "zadowolony"
  ]
  node [
    id 3124
    label "weso&#322;y"
  ]
  node [
    id 3125
    label "intencja"
  ]
  node [
    id 3126
    label "dokumentacja"
  ]
  node [
    id 3127
    label "agreement"
  ]
  node [
    id 3128
    label "device"
  ]
  node [
    id 3129
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 3130
    label "program_u&#380;ytkowy"
  ]
  node [
    id 3131
    label "thinking"
  ]
  node [
    id 3132
    label "sygnatariusz"
  ]
  node [
    id 3133
    label "writing"
  ]
  node [
    id 3134
    label "&#347;wiadectwo"
  ]
  node [
    id 3135
    label "zapis"
  ]
  node [
    id 3136
    label "record"
  ]
  node [
    id 3137
    label "raport&#243;wka"
  ]
  node [
    id 3138
    label "registratura"
  ]
  node [
    id 3139
    label "parafa"
  ]
  node [
    id 3140
    label "plik"
  ]
  node [
    id 3141
    label "reprezentacja"
  ]
  node [
    id 3142
    label "perspektywa"
  ]
  node [
    id 3143
    label "miejsce_pracy"
  ]
  node [
    id 3144
    label "obraz"
  ]
  node [
    id 3145
    label "rysunek"
  ]
  node [
    id 3146
    label "dekoracja"
  ]
  node [
    id 3147
    label "operat"
  ]
  node [
    id 3148
    label "kosztorys"
  ]
  node [
    id 3149
    label "energia"
  ]
  node [
    id 3150
    label "wedyzm"
  ]
  node [
    id 3151
    label "buddyzm"
  ]
  node [
    id 3152
    label "egzergia"
  ]
  node [
    id 3153
    label "emitowanie"
  ]
  node [
    id 3154
    label "power"
  ]
  node [
    id 3155
    label "emitowa&#263;"
  ]
  node [
    id 3156
    label "szwung"
  ]
  node [
    id 3157
    label "energy"
  ]
  node [
    id 3158
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 3159
    label "kwant_energii"
  ]
  node [
    id 3160
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 3161
    label "hinduizm"
  ]
  node [
    id 3162
    label "Buddhism"
  ]
  node [
    id 3163
    label "wad&#378;rajana"
  ]
  node [
    id 3164
    label "arahant"
  ]
  node [
    id 3165
    label "tantryzm"
  ]
  node [
    id 3166
    label "therawada"
  ]
  node [
    id 3167
    label "mahajana"
  ]
  node [
    id 3168
    label "kalpa"
  ]
  node [
    id 3169
    label "li"
  ]
  node [
    id 3170
    label "bardo"
  ]
  node [
    id 3171
    label "dana"
  ]
  node [
    id 3172
    label "ahinsa"
  ]
  node [
    id 3173
    label "religia"
  ]
  node [
    id 3174
    label "lampka_ma&#347;lana"
  ]
  node [
    id 3175
    label "asura"
  ]
  node [
    id 3176
    label "hinajana"
  ]
  node [
    id 3177
    label "bonzo"
  ]
  node [
    id 3178
    label "proceed"
  ]
  node [
    id 3179
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 3180
    label "pozosta&#263;"
  ]
  node [
    id 3181
    label "osta&#263;_si&#281;"
  ]
  node [
    id 3182
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 3183
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 3184
    label "prze&#380;y&#263;"
  ]
  node [
    id 3185
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 3186
    label "umowy"
  ]
  node [
    id 3187
    label "ojciec"
  ]
  node [
    id 3188
    label "partnerstwo"
  ]
  node [
    id 3189
    label "Afryka"
  ]
  node [
    id 3190
    label "Karaib"
  ]
  node [
    id 3191
    label "i"
  ]
  node [
    id 3192
    label "Pacyfik"
  ]
  node [
    id 3193
    label "porozumie&#263;"
  ]
  node [
    id 3194
    label "gospodarczy"
  ]
  node [
    id 3195
    label "san"
  ]
  node [
    id 3196
    label "marina"
  ]
  node [
    id 3197
    label "republika"
  ]
  node [
    id 3198
    label "po&#322;udniowy"
  ]
  node [
    id 3199
    label "federacja"
  ]
  node [
    id 3200
    label "rosyjski"
  ]
  node [
    id 3201
    label "jugos&#322;owia&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 2
    target 557
  ]
  edge [
    source 2
    target 558
  ]
  edge [
    source 2
    target 559
  ]
  edge [
    source 2
    target 560
  ]
  edge [
    source 2
    target 561
  ]
  edge [
    source 2
    target 562
  ]
  edge [
    source 2
    target 563
  ]
  edge [
    source 2
    target 564
  ]
  edge [
    source 2
    target 565
  ]
  edge [
    source 2
    target 566
  ]
  edge [
    source 2
    target 567
  ]
  edge [
    source 2
    target 568
  ]
  edge [
    source 2
    target 569
  ]
  edge [
    source 2
    target 570
  ]
  edge [
    source 2
    target 571
  ]
  edge [
    source 2
    target 572
  ]
  edge [
    source 2
    target 573
  ]
  edge [
    source 2
    target 574
  ]
  edge [
    source 2
    target 575
  ]
  edge [
    source 2
    target 576
  ]
  edge [
    source 2
    target 577
  ]
  edge [
    source 2
    target 578
  ]
  edge [
    source 2
    target 579
  ]
  edge [
    source 2
    target 580
  ]
  edge [
    source 2
    target 581
  ]
  edge [
    source 2
    target 582
  ]
  edge [
    source 2
    target 583
  ]
  edge [
    source 2
    target 584
  ]
  edge [
    source 2
    target 585
  ]
  edge [
    source 2
    target 586
  ]
  edge [
    source 2
    target 587
  ]
  edge [
    source 2
    target 588
  ]
  edge [
    source 2
    target 589
  ]
  edge [
    source 2
    target 590
  ]
  edge [
    source 2
    target 591
  ]
  edge [
    source 2
    target 592
  ]
  edge [
    source 2
    target 593
  ]
  edge [
    source 2
    target 594
  ]
  edge [
    source 2
    target 595
  ]
  edge [
    source 2
    target 596
  ]
  edge [
    source 2
    target 597
  ]
  edge [
    source 2
    target 598
  ]
  edge [
    source 2
    target 599
  ]
  edge [
    source 2
    target 600
  ]
  edge [
    source 2
    target 601
  ]
  edge [
    source 2
    target 602
  ]
  edge [
    source 2
    target 603
  ]
  edge [
    source 2
    target 604
  ]
  edge [
    source 2
    target 605
  ]
  edge [
    source 2
    target 606
  ]
  edge [
    source 2
    target 607
  ]
  edge [
    source 2
    target 608
  ]
  edge [
    source 2
    target 609
  ]
  edge [
    source 2
    target 610
  ]
  edge [
    source 2
    target 611
  ]
  edge [
    source 2
    target 612
  ]
  edge [
    source 2
    target 613
  ]
  edge [
    source 2
    target 614
  ]
  edge [
    source 2
    target 615
  ]
  edge [
    source 2
    target 616
  ]
  edge [
    source 2
    target 617
  ]
  edge [
    source 2
    target 618
  ]
  edge [
    source 2
    target 619
  ]
  edge [
    source 2
    target 620
  ]
  edge [
    source 2
    target 621
  ]
  edge [
    source 2
    target 622
  ]
  edge [
    source 2
    target 623
  ]
  edge [
    source 2
    target 624
  ]
  edge [
    source 2
    target 625
  ]
  edge [
    source 2
    target 626
  ]
  edge [
    source 2
    target 627
  ]
  edge [
    source 2
    target 628
  ]
  edge [
    source 2
    target 629
  ]
  edge [
    source 2
    target 630
  ]
  edge [
    source 2
    target 631
  ]
  edge [
    source 2
    target 632
  ]
  edge [
    source 2
    target 633
  ]
  edge [
    source 2
    target 634
  ]
  edge [
    source 2
    target 635
  ]
  edge [
    source 2
    target 636
  ]
  edge [
    source 2
    target 637
  ]
  edge [
    source 2
    target 638
  ]
  edge [
    source 2
    target 639
  ]
  edge [
    source 2
    target 640
  ]
  edge [
    source 2
    target 641
  ]
  edge [
    source 2
    target 642
  ]
  edge [
    source 2
    target 643
  ]
  edge [
    source 2
    target 644
  ]
  edge [
    source 2
    target 645
  ]
  edge [
    source 2
    target 646
  ]
  edge [
    source 2
    target 647
  ]
  edge [
    source 2
    target 648
  ]
  edge [
    source 2
    target 649
  ]
  edge [
    source 2
    target 650
  ]
  edge [
    source 2
    target 651
  ]
  edge [
    source 2
    target 652
  ]
  edge [
    source 2
    target 653
  ]
  edge [
    source 2
    target 654
  ]
  edge [
    source 2
    target 655
  ]
  edge [
    source 2
    target 656
  ]
  edge [
    source 2
    target 657
  ]
  edge [
    source 2
    target 658
  ]
  edge [
    source 2
    target 659
  ]
  edge [
    source 2
    target 660
  ]
  edge [
    source 2
    target 661
  ]
  edge [
    source 2
    target 662
  ]
  edge [
    source 2
    target 663
  ]
  edge [
    source 2
    target 664
  ]
  edge [
    source 2
    target 665
  ]
  edge [
    source 2
    target 666
  ]
  edge [
    source 2
    target 667
  ]
  edge [
    source 2
    target 668
  ]
  edge [
    source 2
    target 669
  ]
  edge [
    source 2
    target 670
  ]
  edge [
    source 2
    target 671
  ]
  edge [
    source 2
    target 672
  ]
  edge [
    source 2
    target 673
  ]
  edge [
    source 2
    target 674
  ]
  edge [
    source 2
    target 675
  ]
  edge [
    source 2
    target 676
  ]
  edge [
    source 2
    target 677
  ]
  edge [
    source 2
    target 678
  ]
  edge [
    source 2
    target 679
  ]
  edge [
    source 2
    target 680
  ]
  edge [
    source 2
    target 681
  ]
  edge [
    source 2
    target 682
  ]
  edge [
    source 2
    target 683
  ]
  edge [
    source 2
    target 684
  ]
  edge [
    source 2
    target 685
  ]
  edge [
    source 2
    target 686
  ]
  edge [
    source 2
    target 687
  ]
  edge [
    source 2
    target 688
  ]
  edge [
    source 2
    target 689
  ]
  edge [
    source 2
    target 690
  ]
  edge [
    source 2
    target 691
  ]
  edge [
    source 2
    target 692
  ]
  edge [
    source 2
    target 693
  ]
  edge [
    source 2
    target 694
  ]
  edge [
    source 2
    target 695
  ]
  edge [
    source 2
    target 696
  ]
  edge [
    source 2
    target 697
  ]
  edge [
    source 2
    target 698
  ]
  edge [
    source 2
    target 699
  ]
  edge [
    source 2
    target 700
  ]
  edge [
    source 2
    target 701
  ]
  edge [
    source 2
    target 702
  ]
  edge [
    source 2
    target 703
  ]
  edge [
    source 2
    target 704
  ]
  edge [
    source 2
    target 705
  ]
  edge [
    source 2
    target 706
  ]
  edge [
    source 2
    target 707
  ]
  edge [
    source 2
    target 708
  ]
  edge [
    source 2
    target 709
  ]
  edge [
    source 2
    target 710
  ]
  edge [
    source 2
    target 711
  ]
  edge [
    source 2
    target 712
  ]
  edge [
    source 2
    target 713
  ]
  edge [
    source 2
    target 714
  ]
  edge [
    source 2
    target 715
  ]
  edge [
    source 2
    target 716
  ]
  edge [
    source 2
    target 717
  ]
  edge [
    source 2
    target 718
  ]
  edge [
    source 2
    target 719
  ]
  edge [
    source 2
    target 720
  ]
  edge [
    source 2
    target 721
  ]
  edge [
    source 2
    target 722
  ]
  edge [
    source 2
    target 723
  ]
  edge [
    source 2
    target 724
  ]
  edge [
    source 2
    target 725
  ]
  edge [
    source 2
    target 726
  ]
  edge [
    source 2
    target 727
  ]
  edge [
    source 2
    target 728
  ]
  edge [
    source 2
    target 729
  ]
  edge [
    source 2
    target 730
  ]
  edge [
    source 2
    target 731
  ]
  edge [
    source 2
    target 732
  ]
  edge [
    source 2
    target 733
  ]
  edge [
    source 2
    target 734
  ]
  edge [
    source 2
    target 735
  ]
  edge [
    source 2
    target 736
  ]
  edge [
    source 2
    target 737
  ]
  edge [
    source 2
    target 738
  ]
  edge [
    source 2
    target 739
  ]
  edge [
    source 2
    target 740
  ]
  edge [
    source 2
    target 741
  ]
  edge [
    source 2
    target 742
  ]
  edge [
    source 2
    target 743
  ]
  edge [
    source 2
    target 744
  ]
  edge [
    source 2
    target 745
  ]
  edge [
    source 2
    target 746
  ]
  edge [
    source 2
    target 747
  ]
  edge [
    source 2
    target 748
  ]
  edge [
    source 2
    target 749
  ]
  edge [
    source 2
    target 750
  ]
  edge [
    source 2
    target 751
  ]
  edge [
    source 2
    target 752
  ]
  edge [
    source 2
    target 753
  ]
  edge [
    source 2
    target 754
  ]
  edge [
    source 2
    target 755
  ]
  edge [
    source 2
    target 756
  ]
  edge [
    source 2
    target 757
  ]
  edge [
    source 2
    target 758
  ]
  edge [
    source 2
    target 759
  ]
  edge [
    source 2
    target 760
  ]
  edge [
    source 2
    target 761
  ]
  edge [
    source 2
    target 762
  ]
  edge [
    source 2
    target 763
  ]
  edge [
    source 2
    target 764
  ]
  edge [
    source 2
    target 765
  ]
  edge [
    source 2
    target 766
  ]
  edge [
    source 2
    target 767
  ]
  edge [
    source 2
    target 768
  ]
  edge [
    source 2
    target 769
  ]
  edge [
    source 2
    target 770
  ]
  edge [
    source 2
    target 771
  ]
  edge [
    source 2
    target 772
  ]
  edge [
    source 2
    target 773
  ]
  edge [
    source 2
    target 774
  ]
  edge [
    source 2
    target 775
  ]
  edge [
    source 2
    target 776
  ]
  edge [
    source 2
    target 777
  ]
  edge [
    source 2
    target 778
  ]
  edge [
    source 2
    target 779
  ]
  edge [
    source 2
    target 780
  ]
  edge [
    source 2
    target 781
  ]
  edge [
    source 2
    target 782
  ]
  edge [
    source 2
    target 783
  ]
  edge [
    source 2
    target 784
  ]
  edge [
    source 2
    target 785
  ]
  edge [
    source 2
    target 786
  ]
  edge [
    source 2
    target 787
  ]
  edge [
    source 2
    target 788
  ]
  edge [
    source 2
    target 789
  ]
  edge [
    source 2
    target 790
  ]
  edge [
    source 2
    target 791
  ]
  edge [
    source 2
    target 792
  ]
  edge [
    source 2
    target 793
  ]
  edge [
    source 2
    target 794
  ]
  edge [
    source 2
    target 795
  ]
  edge [
    source 2
    target 796
  ]
  edge [
    source 2
    target 797
  ]
  edge [
    source 2
    target 798
  ]
  edge [
    source 2
    target 799
  ]
  edge [
    source 2
    target 800
  ]
  edge [
    source 2
    target 801
  ]
  edge [
    source 2
    target 802
  ]
  edge [
    source 2
    target 803
  ]
  edge [
    source 2
    target 804
  ]
  edge [
    source 2
    target 805
  ]
  edge [
    source 2
    target 806
  ]
  edge [
    source 2
    target 807
  ]
  edge [
    source 2
    target 808
  ]
  edge [
    source 2
    target 809
  ]
  edge [
    source 2
    target 810
  ]
  edge [
    source 2
    target 811
  ]
  edge [
    source 2
    target 812
  ]
  edge [
    source 2
    target 813
  ]
  edge [
    source 2
    target 814
  ]
  edge [
    source 2
    target 815
  ]
  edge [
    source 2
    target 816
  ]
  edge [
    source 2
    target 817
  ]
  edge [
    source 2
    target 818
  ]
  edge [
    source 2
    target 819
  ]
  edge [
    source 2
    target 820
  ]
  edge [
    source 2
    target 821
  ]
  edge [
    source 2
    target 822
  ]
  edge [
    source 2
    target 823
  ]
  edge [
    source 2
    target 824
  ]
  edge [
    source 2
    target 825
  ]
  edge [
    source 2
    target 826
  ]
  edge [
    source 2
    target 827
  ]
  edge [
    source 2
    target 828
  ]
  edge [
    source 2
    target 829
  ]
  edge [
    source 2
    target 830
  ]
  edge [
    source 2
    target 831
  ]
  edge [
    source 2
    target 832
  ]
  edge [
    source 2
    target 833
  ]
  edge [
    source 2
    target 834
  ]
  edge [
    source 2
    target 835
  ]
  edge [
    source 2
    target 836
  ]
  edge [
    source 2
    target 837
  ]
  edge [
    source 2
    target 838
  ]
  edge [
    source 2
    target 839
  ]
  edge [
    source 2
    target 840
  ]
  edge [
    source 2
    target 841
  ]
  edge [
    source 2
    target 842
  ]
  edge [
    source 2
    target 843
  ]
  edge [
    source 2
    target 844
  ]
  edge [
    source 2
    target 845
  ]
  edge [
    source 2
    target 846
  ]
  edge [
    source 2
    target 847
  ]
  edge [
    source 2
    target 848
  ]
  edge [
    source 2
    target 849
  ]
  edge [
    source 2
    target 850
  ]
  edge [
    source 2
    target 851
  ]
  edge [
    source 2
    target 852
  ]
  edge [
    source 2
    target 853
  ]
  edge [
    source 2
    target 854
  ]
  edge [
    source 2
    target 855
  ]
  edge [
    source 2
    target 856
  ]
  edge [
    source 2
    target 857
  ]
  edge [
    source 2
    target 858
  ]
  edge [
    source 2
    target 859
  ]
  edge [
    source 2
    target 860
  ]
  edge [
    source 2
    target 861
  ]
  edge [
    source 2
    target 862
  ]
  edge [
    source 2
    target 863
  ]
  edge [
    source 2
    target 864
  ]
  edge [
    source 2
    target 865
  ]
  edge [
    source 2
    target 866
  ]
  edge [
    source 2
    target 867
  ]
  edge [
    source 2
    target 868
  ]
  edge [
    source 2
    target 869
  ]
  edge [
    source 2
    target 870
  ]
  edge [
    source 2
    target 871
  ]
  edge [
    source 2
    target 872
  ]
  edge [
    source 2
    target 873
  ]
  edge [
    source 2
    target 874
  ]
  edge [
    source 2
    target 875
  ]
  edge [
    source 2
    target 876
  ]
  edge [
    source 2
    target 877
  ]
  edge [
    source 2
    target 878
  ]
  edge [
    source 2
    target 879
  ]
  edge [
    source 2
    target 880
  ]
  edge [
    source 2
    target 881
  ]
  edge [
    source 2
    target 882
  ]
  edge [
    source 2
    target 883
  ]
  edge [
    source 2
    target 884
  ]
  edge [
    source 2
    target 885
  ]
  edge [
    source 2
    target 886
  ]
  edge [
    source 2
    target 887
  ]
  edge [
    source 2
    target 888
  ]
  edge [
    source 2
    target 889
  ]
  edge [
    source 2
    target 890
  ]
  edge [
    source 2
    target 891
  ]
  edge [
    source 2
    target 892
  ]
  edge [
    source 2
    target 893
  ]
  edge [
    source 2
    target 894
  ]
  edge [
    source 2
    target 895
  ]
  edge [
    source 2
    target 896
  ]
  edge [
    source 2
    target 897
  ]
  edge [
    source 2
    target 898
  ]
  edge [
    source 2
    target 899
  ]
  edge [
    source 2
    target 900
  ]
  edge [
    source 2
    target 901
  ]
  edge [
    source 2
    target 902
  ]
  edge [
    source 2
    target 903
  ]
  edge [
    source 2
    target 904
  ]
  edge [
    source 2
    target 905
  ]
  edge [
    source 2
    target 906
  ]
  edge [
    source 2
    target 907
  ]
  edge [
    source 2
    target 908
  ]
  edge [
    source 2
    target 909
  ]
  edge [
    source 2
    target 910
  ]
  edge [
    source 2
    target 911
  ]
  edge [
    source 2
    target 912
  ]
  edge [
    source 2
    target 913
  ]
  edge [
    source 2
    target 914
  ]
  edge [
    source 2
    target 915
  ]
  edge [
    source 2
    target 916
  ]
  edge [
    source 2
    target 917
  ]
  edge [
    source 2
    target 918
  ]
  edge [
    source 2
    target 919
  ]
  edge [
    source 2
    target 920
  ]
  edge [
    source 2
    target 921
  ]
  edge [
    source 2
    target 922
  ]
  edge [
    source 2
    target 923
  ]
  edge [
    source 2
    target 924
  ]
  edge [
    source 2
    target 925
  ]
  edge [
    source 2
    target 926
  ]
  edge [
    source 2
    target 927
  ]
  edge [
    source 2
    target 928
  ]
  edge [
    source 2
    target 929
  ]
  edge [
    source 2
    target 930
  ]
  edge [
    source 2
    target 931
  ]
  edge [
    source 2
    target 932
  ]
  edge [
    source 2
    target 933
  ]
  edge [
    source 2
    target 934
  ]
  edge [
    source 2
    target 935
  ]
  edge [
    source 2
    target 936
  ]
  edge [
    source 2
    target 937
  ]
  edge [
    source 2
    target 938
  ]
  edge [
    source 2
    target 939
  ]
  edge [
    source 2
    target 940
  ]
  edge [
    source 2
    target 941
  ]
  edge [
    source 2
    target 942
  ]
  edge [
    source 2
    target 943
  ]
  edge [
    source 2
    target 944
  ]
  edge [
    source 2
    target 945
  ]
  edge [
    source 2
    target 946
  ]
  edge [
    source 2
    target 947
  ]
  edge [
    source 2
    target 948
  ]
  edge [
    source 2
    target 949
  ]
  edge [
    source 2
    target 950
  ]
  edge [
    source 2
    target 951
  ]
  edge [
    source 2
    target 952
  ]
  edge [
    source 2
    target 953
  ]
  edge [
    source 2
    target 954
  ]
  edge [
    source 2
    target 955
  ]
  edge [
    source 2
    target 956
  ]
  edge [
    source 2
    target 957
  ]
  edge [
    source 2
    target 958
  ]
  edge [
    source 2
    target 959
  ]
  edge [
    source 2
    target 960
  ]
  edge [
    source 2
    target 961
  ]
  edge [
    source 2
    target 962
  ]
  edge [
    source 2
    target 963
  ]
  edge [
    source 2
    target 964
  ]
  edge [
    source 2
    target 965
  ]
  edge [
    source 2
    target 966
  ]
  edge [
    source 2
    target 967
  ]
  edge [
    source 2
    target 968
  ]
  edge [
    source 2
    target 969
  ]
  edge [
    source 2
    target 970
  ]
  edge [
    source 2
    target 971
  ]
  edge [
    source 2
    target 972
  ]
  edge [
    source 2
    target 973
  ]
  edge [
    source 2
    target 974
  ]
  edge [
    source 2
    target 975
  ]
  edge [
    source 2
    target 976
  ]
  edge [
    source 2
    target 977
  ]
  edge [
    source 2
    target 978
  ]
  edge [
    source 2
    target 979
  ]
  edge [
    source 2
    target 980
  ]
  edge [
    source 2
    target 981
  ]
  edge [
    source 2
    target 982
  ]
  edge [
    source 2
    target 983
  ]
  edge [
    source 2
    target 984
  ]
  edge [
    source 2
    target 985
  ]
  edge [
    source 2
    target 986
  ]
  edge [
    source 2
    target 987
  ]
  edge [
    source 2
    target 988
  ]
  edge [
    source 2
    target 989
  ]
  edge [
    source 2
    target 990
  ]
  edge [
    source 2
    target 991
  ]
  edge [
    source 2
    target 992
  ]
  edge [
    source 2
    target 993
  ]
  edge [
    source 2
    target 994
  ]
  edge [
    source 2
    target 995
  ]
  edge [
    source 2
    target 996
  ]
  edge [
    source 2
    target 997
  ]
  edge [
    source 2
    target 998
  ]
  edge [
    source 2
    target 999
  ]
  edge [
    source 2
    target 1000
  ]
  edge [
    source 2
    target 1001
  ]
  edge [
    source 2
    target 1002
  ]
  edge [
    source 2
    target 1003
  ]
  edge [
    source 2
    target 1004
  ]
  edge [
    source 2
    target 1005
  ]
  edge [
    source 2
    target 1006
  ]
  edge [
    source 2
    target 1007
  ]
  edge [
    source 2
    target 1008
  ]
  edge [
    source 2
    target 1009
  ]
  edge [
    source 2
    target 1010
  ]
  edge [
    source 2
    target 1011
  ]
  edge [
    source 2
    target 1012
  ]
  edge [
    source 2
    target 1013
  ]
  edge [
    source 2
    target 1014
  ]
  edge [
    source 2
    target 1015
  ]
  edge [
    source 2
    target 1016
  ]
  edge [
    source 2
    target 1017
  ]
  edge [
    source 2
    target 1018
  ]
  edge [
    source 2
    target 1019
  ]
  edge [
    source 2
    target 1020
  ]
  edge [
    source 2
    target 1021
  ]
  edge [
    source 2
    target 1022
  ]
  edge [
    source 2
    target 1023
  ]
  edge [
    source 2
    target 1024
  ]
  edge [
    source 2
    target 1025
  ]
  edge [
    source 2
    target 1026
  ]
  edge [
    source 2
    target 1027
  ]
  edge [
    source 2
    target 1028
  ]
  edge [
    source 2
    target 1029
  ]
  edge [
    source 2
    target 1030
  ]
  edge [
    source 2
    target 1031
  ]
  edge [
    source 2
    target 1032
  ]
  edge [
    source 2
    target 1033
  ]
  edge [
    source 2
    target 1034
  ]
  edge [
    source 2
    target 1035
  ]
  edge [
    source 2
    target 1036
  ]
  edge [
    source 2
    target 1037
  ]
  edge [
    source 2
    target 1038
  ]
  edge [
    source 2
    target 1039
  ]
  edge [
    source 2
    target 1040
  ]
  edge [
    source 2
    target 1041
  ]
  edge [
    source 2
    target 1042
  ]
  edge [
    source 2
    target 1043
  ]
  edge [
    source 2
    target 1044
  ]
  edge [
    source 2
    target 1045
  ]
  edge [
    source 2
    target 1046
  ]
  edge [
    source 2
    target 1047
  ]
  edge [
    source 2
    target 1048
  ]
  edge [
    source 2
    target 1049
  ]
  edge [
    source 2
    target 1050
  ]
  edge [
    source 2
    target 1051
  ]
  edge [
    source 2
    target 1052
  ]
  edge [
    source 2
    target 1053
  ]
  edge [
    source 2
    target 1054
  ]
  edge [
    source 2
    target 1055
  ]
  edge [
    source 2
    target 1056
  ]
  edge [
    source 2
    target 1057
  ]
  edge [
    source 2
    target 1058
  ]
  edge [
    source 2
    target 1059
  ]
  edge [
    source 2
    target 1060
  ]
  edge [
    source 2
    target 1061
  ]
  edge [
    source 2
    target 1062
  ]
  edge [
    source 2
    target 1063
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 3189
  ]
  edge [
    source 2
    target 3190
  ]
  edge [
    source 2
    target 3191
  ]
  edge [
    source 2
    target 3192
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 1064
  ]
  edge [
    source 4
    target 1065
  ]
  edge [
    source 4
    target 1066
  ]
  edge [
    source 4
    target 1067
  ]
  edge [
    source 4
    target 1068
  ]
  edge [
    source 4
    target 1069
  ]
  edge [
    source 4
    target 1070
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 1071
  ]
  edge [
    source 4
    target 1072
  ]
  edge [
    source 4
    target 1073
  ]
  edge [
    source 4
    target 1074
  ]
  edge [
    source 4
    target 1075
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 1076
  ]
  edge [
    source 4
    target 1077
  ]
  edge [
    source 4
    target 820
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 1078
  ]
  edge [
    source 4
    target 1079
  ]
  edge [
    source 4
    target 1080
  ]
  edge [
    source 4
    target 1081
  ]
  edge [
    source 4
    target 1082
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 1083
  ]
  edge [
    source 4
    target 1084
  ]
  edge [
    source 4
    target 1085
  ]
  edge [
    source 4
    target 1086
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 1087
  ]
  edge [
    source 4
    target 1088
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 1089
  ]
  edge [
    source 4
    target 1090
  ]
  edge [
    source 4
    target 1091
  ]
  edge [
    source 4
    target 1092
  ]
  edge [
    source 4
    target 1093
  ]
  edge [
    source 4
    target 1094
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 1095
  ]
  edge [
    source 4
    target 1096
  ]
  edge [
    source 4
    target 1097
  ]
  edge [
    source 4
    target 1098
  ]
  edge [
    source 4
    target 1099
  ]
  edge [
    source 4
    target 1100
  ]
  edge [
    source 4
    target 1101
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 1102
  ]
  edge [
    source 4
    target 1103
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 1104
  ]
  edge [
    source 4
    target 1105
  ]
  edge [
    source 4
    target 1106
  ]
  edge [
    source 4
    target 1107
  ]
  edge [
    source 4
    target 1108
  ]
  edge [
    source 4
    target 1109
  ]
  edge [
    source 4
    target 1110
  ]
  edge [
    source 4
    target 1111
  ]
  edge [
    source 4
    target 1112
  ]
  edge [
    source 4
    target 1113
  ]
  edge [
    source 4
    target 1114
  ]
  edge [
    source 4
    target 1115
  ]
  edge [
    source 4
    target 1116
  ]
  edge [
    source 4
    target 1117
  ]
  edge [
    source 4
    target 1118
  ]
  edge [
    source 4
    target 1119
  ]
  edge [
    source 4
    target 1120
  ]
  edge [
    source 4
    target 1121
  ]
  edge [
    source 4
    target 1122
  ]
  edge [
    source 4
    target 1123
  ]
  edge [
    source 4
    target 1124
  ]
  edge [
    source 4
    target 1125
  ]
  edge [
    source 4
    target 1126
  ]
  edge [
    source 4
    target 1127
  ]
  edge [
    source 4
    target 1128
  ]
  edge [
    source 4
    target 1129
  ]
  edge [
    source 4
    target 1130
  ]
  edge [
    source 4
    target 1131
  ]
  edge [
    source 4
    target 1132
  ]
  edge [
    source 4
    target 1133
  ]
  edge [
    source 4
    target 1134
  ]
  edge [
    source 4
    target 1135
  ]
  edge [
    source 4
    target 1136
  ]
  edge [
    source 4
    target 1137
  ]
  edge [
    source 4
    target 1138
  ]
  edge [
    source 4
    target 1139
  ]
  edge [
    source 4
    target 1140
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 1141
  ]
  edge [
    source 6
    target 1142
  ]
  edge [
    source 6
    target 1143
  ]
  edge [
    source 6
    target 1144
  ]
  edge [
    source 6
    target 1145
  ]
  edge [
    source 6
    target 1146
  ]
  edge [
    source 6
    target 1147
  ]
  edge [
    source 6
    target 1148
  ]
  edge [
    source 6
    target 1149
  ]
  edge [
    source 6
    target 1150
  ]
  edge [
    source 6
    target 1151
  ]
  edge [
    source 6
    target 1152
  ]
  edge [
    source 6
    target 1153
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 1154
  ]
  edge [
    source 6
    target 1155
  ]
  edge [
    source 6
    target 1156
  ]
  edge [
    source 6
    target 1157
  ]
  edge [
    source 6
    target 1158
  ]
  edge [
    source 6
    target 1159
  ]
  edge [
    source 6
    target 1160
  ]
  edge [
    source 6
    target 1161
  ]
  edge [
    source 6
    target 1162
  ]
  edge [
    source 6
    target 1163
  ]
  edge [
    source 6
    target 1164
  ]
  edge [
    source 6
    target 1165
  ]
  edge [
    source 6
    target 1166
  ]
  edge [
    source 6
    target 1167
  ]
  edge [
    source 6
    target 1168
  ]
  edge [
    source 6
    target 1169
  ]
  edge [
    source 6
    target 1170
  ]
  edge [
    source 6
    target 1171
  ]
  edge [
    source 6
    target 1172
  ]
  edge [
    source 6
    target 1173
  ]
  edge [
    source 6
    target 1174
  ]
  edge [
    source 6
    target 1175
  ]
  edge [
    source 6
    target 1176
  ]
  edge [
    source 6
    target 1177
  ]
  edge [
    source 6
    target 1178
  ]
  edge [
    source 6
    target 1179
  ]
  edge [
    source 6
    target 1180
  ]
  edge [
    source 6
    target 1181
  ]
  edge [
    source 6
    target 1182
  ]
  edge [
    source 6
    target 1183
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 1184
  ]
  edge [
    source 6
    target 1185
  ]
  edge [
    source 6
    target 1186
  ]
  edge [
    source 6
    target 1187
  ]
  edge [
    source 6
    target 1054
  ]
  edge [
    source 6
    target 1188
  ]
  edge [
    source 6
    target 1189
  ]
  edge [
    source 6
    target 1190
  ]
  edge [
    source 6
    target 1191
  ]
  edge [
    source 6
    target 1192
  ]
  edge [
    source 6
    target 1193
  ]
  edge [
    source 6
    target 1194
  ]
  edge [
    source 6
    target 1195
  ]
  edge [
    source 6
    target 1196
  ]
  edge [
    source 6
    target 1197
  ]
  edge [
    source 6
    target 1198
  ]
  edge [
    source 6
    target 1199
  ]
  edge [
    source 6
    target 1200
  ]
  edge [
    source 6
    target 1201
  ]
  edge [
    source 6
    target 1202
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 1203
  ]
  edge [
    source 6
    target 1204
  ]
  edge [
    source 6
    target 1205
  ]
  edge [
    source 6
    target 1206
  ]
  edge [
    source 6
    target 1207
  ]
  edge [
    source 6
    target 1208
  ]
  edge [
    source 6
    target 1209
  ]
  edge [
    source 6
    target 1210
  ]
  edge [
    source 6
    target 1211
  ]
  edge [
    source 6
    target 1212
  ]
  edge [
    source 6
    target 1213
  ]
  edge [
    source 6
    target 1214
  ]
  edge [
    source 6
    target 1215
  ]
  edge [
    source 6
    target 1216
  ]
  edge [
    source 6
    target 1217
  ]
  edge [
    source 6
    target 1218
  ]
  edge [
    source 6
    target 1219
  ]
  edge [
    source 6
    target 1220
  ]
  edge [
    source 6
    target 1221
  ]
  edge [
    source 6
    target 1222
  ]
  edge [
    source 6
    target 1223
  ]
  edge [
    source 6
    target 1224
  ]
  edge [
    source 6
    target 1225
  ]
  edge [
    source 6
    target 1226
  ]
  edge [
    source 6
    target 1227
  ]
  edge [
    source 6
    target 1228
  ]
  edge [
    source 6
    target 1229
  ]
  edge [
    source 6
    target 1230
  ]
  edge [
    source 6
    target 1231
  ]
  edge [
    source 6
    target 1232
  ]
  edge [
    source 6
    target 1233
  ]
  edge [
    source 6
    target 1234
  ]
  edge [
    source 6
    target 1235
  ]
  edge [
    source 6
    target 1236
  ]
  edge [
    source 6
    target 1237
  ]
  edge [
    source 6
    target 1238
  ]
  edge [
    source 6
    target 1239
  ]
  edge [
    source 6
    target 1240
  ]
  edge [
    source 6
    target 1241
  ]
  edge [
    source 6
    target 1242
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 1243
  ]
  edge [
    source 6
    target 1244
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 1245
  ]
  edge [
    source 6
    target 1246
  ]
  edge [
    source 6
    target 1247
  ]
  edge [
    source 6
    target 1248
  ]
  edge [
    source 6
    target 1249
  ]
  edge [
    source 6
    target 1250
  ]
  edge [
    source 6
    target 1251
  ]
  edge [
    source 6
    target 1252
  ]
  edge [
    source 6
    target 1253
  ]
  edge [
    source 6
    target 1254
  ]
  edge [
    source 6
    target 1255
  ]
  edge [
    source 6
    target 1256
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 1257
  ]
  edge [
    source 6
    target 1258
  ]
  edge [
    source 6
    target 1259
  ]
  edge [
    source 6
    target 1260
  ]
  edge [
    source 6
    target 1261
  ]
  edge [
    source 6
    target 1262
  ]
  edge [
    source 6
    target 1263
  ]
  edge [
    source 6
    target 1264
  ]
  edge [
    source 6
    target 1265
  ]
  edge [
    source 6
    target 1266
  ]
  edge [
    source 6
    target 1267
  ]
  edge [
    source 6
    target 1268
  ]
  edge [
    source 6
    target 1269
  ]
  edge [
    source 6
    target 1270
  ]
  edge [
    source 6
    target 1271
  ]
  edge [
    source 6
    target 1272
  ]
  edge [
    source 6
    target 1273
  ]
  edge [
    source 6
    target 1274
  ]
  edge [
    source 6
    target 1275
  ]
  edge [
    source 6
    target 1276
  ]
  edge [
    source 6
    target 1277
  ]
  edge [
    source 6
    target 1278
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 1279
  ]
  edge [
    source 6
    target 1280
  ]
  edge [
    source 6
    target 1281
  ]
  edge [
    source 6
    target 1282
  ]
  edge [
    source 6
    target 1283
  ]
  edge [
    source 6
    target 1284
  ]
  edge [
    source 6
    target 1285
  ]
  edge [
    source 6
    target 1286
  ]
  edge [
    source 6
    target 1287
  ]
  edge [
    source 6
    target 1288
  ]
  edge [
    source 6
    target 1289
  ]
  edge [
    source 6
    target 1290
  ]
  edge [
    source 6
    target 1291
  ]
  edge [
    source 6
    target 1292
  ]
  edge [
    source 6
    target 1293
  ]
  edge [
    source 6
    target 1294
  ]
  edge [
    source 6
    target 1295
  ]
  edge [
    source 6
    target 1296
  ]
  edge [
    source 6
    target 1297
  ]
  edge [
    source 6
    target 1298
  ]
  edge [
    source 6
    target 1299
  ]
  edge [
    source 6
    target 1300
  ]
  edge [
    source 6
    target 1301
  ]
  edge [
    source 6
    target 1302
  ]
  edge [
    source 6
    target 1303
  ]
  edge [
    source 6
    target 1304
  ]
  edge [
    source 6
    target 1305
  ]
  edge [
    source 6
    target 1306
  ]
  edge [
    source 6
    target 1307
  ]
  edge [
    source 6
    target 1308
  ]
  edge [
    source 6
    target 1309
  ]
  edge [
    source 6
    target 1310
  ]
  edge [
    source 6
    target 1311
  ]
  edge [
    source 6
    target 1312
  ]
  edge [
    source 6
    target 1313
  ]
  edge [
    source 6
    target 1314
  ]
  edge [
    source 6
    target 1315
  ]
  edge [
    source 6
    target 1316
  ]
  edge [
    source 6
    target 1317
  ]
  edge [
    source 6
    target 1318
  ]
  edge [
    source 6
    target 1319
  ]
  edge [
    source 6
    target 1320
  ]
  edge [
    source 6
    target 1321
  ]
  edge [
    source 6
    target 1322
  ]
  edge [
    source 6
    target 1323
  ]
  edge [
    source 6
    target 1324
  ]
  edge [
    source 6
    target 1325
  ]
  edge [
    source 6
    target 1326
  ]
  edge [
    source 6
    target 1327
  ]
  edge [
    source 6
    target 1328
  ]
  edge [
    source 6
    target 1329
  ]
  edge [
    source 6
    target 1330
  ]
  edge [
    source 6
    target 1331
  ]
  edge [
    source 6
    target 1332
  ]
  edge [
    source 6
    target 1333
  ]
  edge [
    source 6
    target 1334
  ]
  edge [
    source 6
    target 1335
  ]
  edge [
    source 6
    target 1336
  ]
  edge [
    source 6
    target 1337
  ]
  edge [
    source 6
    target 1338
  ]
  edge [
    source 6
    target 1339
  ]
  edge [
    source 6
    target 1340
  ]
  edge [
    source 6
    target 1341
  ]
  edge [
    source 6
    target 1342
  ]
  edge [
    source 6
    target 1343
  ]
  edge [
    source 6
    target 1344
  ]
  edge [
    source 6
    target 1345
  ]
  edge [
    source 6
    target 1346
  ]
  edge [
    source 6
    target 1347
  ]
  edge [
    source 6
    target 1348
  ]
  edge [
    source 6
    target 1349
  ]
  edge [
    source 6
    target 1350
  ]
  edge [
    source 6
    target 1351
  ]
  edge [
    source 6
    target 1352
  ]
  edge [
    source 6
    target 1353
  ]
  edge [
    source 6
    target 1354
  ]
  edge [
    source 6
    target 1355
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 1281
  ]
  edge [
    source 7
    target 1356
  ]
  edge [
    source 7
    target 1357
  ]
  edge [
    source 7
    target 1358
  ]
  edge [
    source 7
    target 1359
  ]
  edge [
    source 7
    target 1360
  ]
  edge [
    source 7
    target 1361
  ]
  edge [
    source 7
    target 1362
  ]
  edge [
    source 7
    target 1363
  ]
  edge [
    source 7
    target 1364
  ]
  edge [
    source 7
    target 1365
  ]
  edge [
    source 7
    target 1366
  ]
  edge [
    source 7
    target 1367
  ]
  edge [
    source 7
    target 1368
  ]
  edge [
    source 7
    target 1369
  ]
  edge [
    source 7
    target 1370
  ]
  edge [
    source 7
    target 1371
  ]
  edge [
    source 7
    target 1029
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 1120
  ]
  edge [
    source 7
    target 1127
  ]
  edge [
    source 7
    target 1372
  ]
  edge [
    source 7
    target 1373
  ]
  edge [
    source 7
    target 1374
  ]
  edge [
    source 7
    target 1254
  ]
  edge [
    source 7
    target 1163
  ]
  edge [
    source 7
    target 1375
  ]
  edge [
    source 7
    target 1351
  ]
  edge [
    source 7
    target 1376
  ]
  edge [
    source 7
    target 1377
  ]
  edge [
    source 7
    target 1378
  ]
  edge [
    source 7
    target 1379
  ]
  edge [
    source 7
    target 1380
  ]
  edge [
    source 7
    target 1381
  ]
  edge [
    source 7
    target 1382
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 1383
  ]
  edge [
    source 7
    target 1384
  ]
  edge [
    source 7
    target 1064
  ]
  edge [
    source 7
    target 1385
  ]
  edge [
    source 7
    target 1386
  ]
  edge [
    source 7
    target 1255
  ]
  edge [
    source 7
    target 1387
  ]
  edge [
    source 7
    target 1388
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1389
  ]
  edge [
    source 8
    target 1390
  ]
  edge [
    source 8
    target 1391
  ]
  edge [
    source 8
    target 1392
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1393
  ]
  edge [
    source 9
    target 1394
  ]
  edge [
    source 9
    target 1395
  ]
  edge [
    source 9
    target 1396
  ]
  edge [
    source 9
    target 1397
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 1398
  ]
  edge [
    source 9
    target 1399
  ]
  edge [
    source 9
    target 1400
  ]
  edge [
    source 9
    target 1401
  ]
  edge [
    source 9
    target 1402
  ]
  edge [
    source 9
    target 1403
  ]
  edge [
    source 9
    target 1404
  ]
  edge [
    source 9
    target 1405
  ]
  edge [
    source 9
    target 1406
  ]
  edge [
    source 9
    target 1407
  ]
  edge [
    source 9
    target 1408
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 72
  ]
  edge [
    source 16
    target 1409
  ]
  edge [
    source 16
    target 1410
  ]
  edge [
    source 16
    target 1411
  ]
  edge [
    source 16
    target 1412
  ]
  edge [
    source 16
    target 1097
  ]
  edge [
    source 16
    target 1413
  ]
  edge [
    source 16
    target 1414
  ]
  edge [
    source 16
    target 1415
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 1416
  ]
  edge [
    source 16
    target 1417
  ]
  edge [
    source 16
    target 1418
  ]
  edge [
    source 16
    target 1419
  ]
  edge [
    source 16
    target 1420
  ]
  edge [
    source 16
    target 1421
  ]
  edge [
    source 16
    target 1422
  ]
  edge [
    source 16
    target 1423
  ]
  edge [
    source 16
    target 1424
  ]
  edge [
    source 16
    target 1425
  ]
  edge [
    source 16
    target 1426
  ]
  edge [
    source 16
    target 1427
  ]
  edge [
    source 16
    target 1428
  ]
  edge [
    source 16
    target 1429
  ]
  edge [
    source 16
    target 1430
  ]
  edge [
    source 16
    target 1431
  ]
  edge [
    source 16
    target 1309
  ]
  edge [
    source 16
    target 1432
  ]
  edge [
    source 16
    target 1433
  ]
  edge [
    source 16
    target 1434
  ]
  edge [
    source 16
    target 1435
  ]
  edge [
    source 16
    target 1436
  ]
  edge [
    source 16
    target 1437
  ]
  edge [
    source 16
    target 1438
  ]
  edge [
    source 16
    target 1439
  ]
  edge [
    source 16
    target 1440
  ]
  edge [
    source 16
    target 1441
  ]
  edge [
    source 16
    target 1308
  ]
  edge [
    source 16
    target 1442
  ]
  edge [
    source 16
    target 1443
  ]
  edge [
    source 16
    target 1444
  ]
  edge [
    source 16
    target 1445
  ]
  edge [
    source 16
    target 1446
  ]
  edge [
    source 16
    target 1447
  ]
  edge [
    source 16
    target 1448
  ]
  edge [
    source 16
    target 1449
  ]
  edge [
    source 16
    target 1450
  ]
  edge [
    source 16
    target 1451
  ]
  edge [
    source 16
    target 1452
  ]
  edge [
    source 16
    target 1453
  ]
  edge [
    source 16
    target 1454
  ]
  edge [
    source 16
    target 1455
  ]
  edge [
    source 16
    target 1456
  ]
  edge [
    source 16
    target 1457
  ]
  edge [
    source 16
    target 1458
  ]
  edge [
    source 16
    target 1459
  ]
  edge [
    source 16
    target 1460
  ]
  edge [
    source 16
    target 1461
  ]
  edge [
    source 16
    target 1462
  ]
  edge [
    source 16
    target 1463
  ]
  edge [
    source 16
    target 1464
  ]
  edge [
    source 16
    target 1208
  ]
  edge [
    source 16
    target 1465
  ]
  edge [
    source 16
    target 1466
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 1467
  ]
  edge [
    source 16
    target 1468
  ]
  edge [
    source 16
    target 1469
  ]
  edge [
    source 16
    target 1470
  ]
  edge [
    source 16
    target 1081
  ]
  edge [
    source 16
    target 1082
  ]
  edge [
    source 16
    target 508
  ]
  edge [
    source 16
    target 1083
  ]
  edge [
    source 16
    target 1084
  ]
  edge [
    source 16
    target 1085
  ]
  edge [
    source 16
    target 1086
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 1087
  ]
  edge [
    source 16
    target 1088
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 1089
  ]
  edge [
    source 16
    target 1090
  ]
  edge [
    source 16
    target 1091
  ]
  edge [
    source 16
    target 1092
  ]
  edge [
    source 16
    target 1093
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 1094
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 1095
  ]
  edge [
    source 16
    target 1096
  ]
  edge [
    source 16
    target 1098
  ]
  edge [
    source 16
    target 1099
  ]
  edge [
    source 16
    target 1100
  ]
  edge [
    source 16
    target 1101
  ]
  edge [
    source 16
    target 1102
  ]
  edge [
    source 16
    target 1103
  ]
  edge [
    source 16
    target 1471
  ]
  edge [
    source 16
    target 1472
  ]
  edge [
    source 16
    target 1473
  ]
  edge [
    source 16
    target 1474
  ]
  edge [
    source 16
    target 1475
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 49
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1476
  ]
  edge [
    source 18
    target 1477
  ]
  edge [
    source 18
    target 1478
  ]
  edge [
    source 18
    target 1479
  ]
  edge [
    source 18
    target 1480
  ]
  edge [
    source 18
    target 1481
  ]
  edge [
    source 18
    target 1482
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1483
  ]
  edge [
    source 18
    target 1484
  ]
  edge [
    source 18
    target 1485
  ]
  edge [
    source 18
    target 1486
  ]
  edge [
    source 18
    target 1487
  ]
  edge [
    source 18
    target 1488
  ]
  edge [
    source 18
    target 1489
  ]
  edge [
    source 18
    target 1490
  ]
  edge [
    source 18
    target 1491
  ]
  edge [
    source 18
    target 1492
  ]
  edge [
    source 18
    target 1493
  ]
  edge [
    source 18
    target 1494
  ]
  edge [
    source 18
    target 1495
  ]
  edge [
    source 18
    target 1496
  ]
  edge [
    source 18
    target 1432
  ]
  edge [
    source 18
    target 1497
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 1498
  ]
  edge [
    source 18
    target 1499
  ]
  edge [
    source 18
    target 1500
  ]
  edge [
    source 18
    target 1501
  ]
  edge [
    source 18
    target 1502
  ]
  edge [
    source 18
    target 1503
  ]
  edge [
    source 18
    target 1504
  ]
  edge [
    source 18
    target 1435
  ]
  edge [
    source 18
    target 1505
  ]
  edge [
    source 18
    target 1506
  ]
  edge [
    source 18
    target 1507
  ]
  edge [
    source 18
    target 1508
  ]
  edge [
    source 18
    target 1509
  ]
  edge [
    source 18
    target 1510
  ]
  edge [
    source 18
    target 1511
  ]
  edge [
    source 18
    target 1512
  ]
  edge [
    source 18
    target 1513
  ]
  edge [
    source 18
    target 1514
  ]
  edge [
    source 18
    target 1515
  ]
  edge [
    source 18
    target 1516
  ]
  edge [
    source 18
    target 1517
  ]
  edge [
    source 18
    target 1518
  ]
  edge [
    source 18
    target 1519
  ]
  edge [
    source 18
    target 1520
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1521
  ]
  edge [
    source 19
    target 1522
  ]
  edge [
    source 19
    target 1523
  ]
  edge [
    source 19
    target 1524
  ]
  edge [
    source 19
    target 1525
  ]
  edge [
    source 19
    target 1526
  ]
  edge [
    source 19
    target 1527
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 21
    target 43
  ]
  edge [
    source 21
    target 44
  ]
  edge [
    source 21
    target 46
  ]
  edge [
    source 21
    target 1528
  ]
  edge [
    source 21
    target 655
  ]
  edge [
    source 21
    target 1529
  ]
  edge [
    source 21
    target 488
  ]
  edge [
    source 21
    target 1530
  ]
  edge [
    source 21
    target 724
  ]
  edge [
    source 21
    target 1531
  ]
  edge [
    source 21
    target 1532
  ]
  edge [
    source 21
    target 1533
  ]
  edge [
    source 21
    target 495
  ]
  edge [
    source 21
    target 53
  ]
  edge [
    source 21
    target 1534
  ]
  edge [
    source 21
    target 1535
  ]
  edge [
    source 21
    target 1536
  ]
  edge [
    source 21
    target 1537
  ]
  edge [
    source 21
    target 1538
  ]
  edge [
    source 21
    target 978
  ]
  edge [
    source 21
    target 1539
  ]
  edge [
    source 21
    target 1540
  ]
  edge [
    source 21
    target 1541
  ]
  edge [
    source 21
    target 1542
  ]
  edge [
    source 21
    target 1543
  ]
  edge [
    source 21
    target 1544
  ]
  edge [
    source 21
    target 1545
  ]
  edge [
    source 21
    target 1546
  ]
  edge [
    source 21
    target 1547
  ]
  edge [
    source 21
    target 1548
  ]
  edge [
    source 21
    target 1549
  ]
  edge [
    source 21
    target 1550
  ]
  edge [
    source 21
    target 1551
  ]
  edge [
    source 21
    target 1552
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 21
    target 1553
  ]
  edge [
    source 21
    target 1554
  ]
  edge [
    source 21
    target 1555
  ]
  edge [
    source 21
    target 1192
  ]
  edge [
    source 21
    target 1556
  ]
  edge [
    source 21
    target 1557
  ]
  edge [
    source 21
    target 722
  ]
  edge [
    source 21
    target 556
  ]
  edge [
    source 21
    target 648
  ]
  edge [
    source 21
    target 696
  ]
  edge [
    source 21
    target 1558
  ]
  edge [
    source 21
    target 213
  ]
  edge [
    source 21
    target 1559
  ]
  edge [
    source 21
    target 52
  ]
  edge [
    source 21
    target 1560
  ]
  edge [
    source 21
    target 1561
  ]
  edge [
    source 21
    target 1562
  ]
  edge [
    source 21
    target 1563
  ]
  edge [
    source 21
    target 1564
  ]
  edge [
    source 21
    target 1565
  ]
  edge [
    source 21
    target 1566
  ]
  edge [
    source 21
    target 1567
  ]
  edge [
    source 21
    target 1568
  ]
  edge [
    source 21
    target 1569
  ]
  edge [
    source 21
    target 1570
  ]
  edge [
    source 21
    target 1571
  ]
  edge [
    source 21
    target 1572
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1573
  ]
  edge [
    source 21
    target 1574
  ]
  edge [
    source 21
    target 1575
  ]
  edge [
    source 21
    target 1576
  ]
  edge [
    source 21
    target 1577
  ]
  edge [
    source 21
    target 1261
  ]
  edge [
    source 21
    target 1578
  ]
  edge [
    source 21
    target 1579
  ]
  edge [
    source 21
    target 1580
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1581
  ]
  edge [
    source 21
    target 1582
  ]
  edge [
    source 21
    target 1583
  ]
  edge [
    source 21
    target 1584
  ]
  edge [
    source 21
    target 1585
  ]
  edge [
    source 21
    target 1586
  ]
  edge [
    source 21
    target 1587
  ]
  edge [
    source 21
    target 1588
  ]
  edge [
    source 21
    target 1589
  ]
  edge [
    source 21
    target 1590
  ]
  edge [
    source 21
    target 1363
  ]
  edge [
    source 21
    target 1591
  ]
  edge [
    source 21
    target 1592
  ]
  edge [
    source 21
    target 1593
  ]
  edge [
    source 21
    target 1594
  ]
  edge [
    source 21
    target 1595
  ]
  edge [
    source 21
    target 1596
  ]
  edge [
    source 21
    target 1597
  ]
  edge [
    source 21
    target 1413
  ]
  edge [
    source 21
    target 1598
  ]
  edge [
    source 21
    target 1599
  ]
  edge [
    source 21
    target 1444
  ]
  edge [
    source 21
    target 1600
  ]
  edge [
    source 21
    target 1601
  ]
  edge [
    source 21
    target 1602
  ]
  edge [
    source 21
    target 1603
  ]
  edge [
    source 21
    target 1604
  ]
  edge [
    source 21
    target 1436
  ]
  edge [
    source 21
    target 1605
  ]
  edge [
    source 21
    target 1606
  ]
  edge [
    source 21
    target 1607
  ]
  edge [
    source 21
    target 1608
  ]
  edge [
    source 21
    target 1609
  ]
  edge [
    source 21
    target 1610
  ]
  edge [
    source 21
    target 1611
  ]
  edge [
    source 21
    target 1612
  ]
  edge [
    source 21
    target 1613
  ]
  edge [
    source 21
    target 1614
  ]
  edge [
    source 21
    target 1615
  ]
  edge [
    source 21
    target 1616
  ]
  edge [
    source 21
    target 1617
  ]
  edge [
    source 21
    target 1618
  ]
  edge [
    source 21
    target 1619
  ]
  edge [
    source 21
    target 1620
  ]
  edge [
    source 21
    target 1621
  ]
  edge [
    source 21
    target 1622
  ]
  edge [
    source 21
    target 1623
  ]
  edge [
    source 21
    target 1624
  ]
  edge [
    source 21
    target 1625
  ]
  edge [
    source 21
    target 1626
  ]
  edge [
    source 21
    target 1627
  ]
  edge [
    source 21
    target 28
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 45
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 38
  ]
  edge [
    source 21
    target 42
  ]
  edge [
    source 21
    target 57
  ]
  edge [
    source 21
    target 54
  ]
  edge [
    source 21
    target 30
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1628
  ]
  edge [
    source 22
    target 1629
  ]
  edge [
    source 22
    target 1630
  ]
  edge [
    source 22
    target 1422
  ]
  edge [
    source 22
    target 1631
  ]
  edge [
    source 22
    target 1632
  ]
  edge [
    source 22
    target 1633
  ]
  edge [
    source 22
    target 1435
  ]
  edge [
    source 22
    target 1634
  ]
  edge [
    source 22
    target 1635
  ]
  edge [
    source 22
    target 1636
  ]
  edge [
    source 22
    target 1637
  ]
  edge [
    source 22
    target 1638
  ]
  edge [
    source 22
    target 1639
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 22
    target 1640
  ]
  edge [
    source 22
    target 1641
  ]
  edge [
    source 22
    target 1642
  ]
  edge [
    source 22
    target 1643
  ]
  edge [
    source 22
    target 1644
  ]
  edge [
    source 22
    target 1645
  ]
  edge [
    source 22
    target 1515
  ]
  edge [
    source 22
    target 1646
  ]
  edge [
    source 22
    target 1647
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 52
  ]
  edge [
    source 23
    target 56
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 57
  ]
  edge [
    source 24
    target 58
  ]
  edge [
    source 24
    target 1648
  ]
  edge [
    source 24
    target 1649
  ]
  edge [
    source 24
    target 1650
  ]
  edge [
    source 24
    target 1097
  ]
  edge [
    source 24
    target 373
  ]
  edge [
    source 24
    target 1651
  ]
  edge [
    source 24
    target 1652
  ]
  edge [
    source 24
    target 1653
  ]
  edge [
    source 24
    target 1654
  ]
  edge [
    source 24
    target 1655
  ]
  edge [
    source 24
    target 1656
  ]
  edge [
    source 24
    target 1657
  ]
  edge [
    source 24
    target 1658
  ]
  edge [
    source 24
    target 119
  ]
  edge [
    source 24
    target 1659
  ]
  edge [
    source 24
    target 1660
  ]
  edge [
    source 24
    target 1661
  ]
  edge [
    source 24
    target 1662
  ]
  edge [
    source 24
    target 1663
  ]
  edge [
    source 24
    target 1664
  ]
  edge [
    source 24
    target 1665
  ]
  edge [
    source 24
    target 1666
  ]
  edge [
    source 24
    target 121
  ]
  edge [
    source 24
    target 1667
  ]
  edge [
    source 24
    target 1668
  ]
  edge [
    source 24
    target 1669
  ]
  edge [
    source 24
    target 1670
  ]
  edge [
    source 24
    target 104
  ]
  edge [
    source 24
    target 1671
  ]
  edge [
    source 24
    target 1672
  ]
  edge [
    source 24
    target 1673
  ]
  edge [
    source 24
    target 1674
  ]
  edge [
    source 24
    target 1675
  ]
  edge [
    source 24
    target 1676
  ]
  edge [
    source 24
    target 1677
  ]
  edge [
    source 24
    target 1678
  ]
  edge [
    source 24
    target 1679
  ]
  edge [
    source 24
    target 1680
  ]
  edge [
    source 24
    target 1681
  ]
  edge [
    source 24
    target 1682
  ]
  edge [
    source 24
    target 1683
  ]
  edge [
    source 24
    target 1684
  ]
  edge [
    source 24
    target 1685
  ]
  edge [
    source 24
    target 1686
  ]
  edge [
    source 24
    target 1074
  ]
  edge [
    source 24
    target 1687
  ]
  edge [
    source 24
    target 1688
  ]
  edge [
    source 24
    target 1689
  ]
  edge [
    source 24
    target 1690
  ]
  edge [
    source 24
    target 1691
  ]
  edge [
    source 24
    target 1692
  ]
  edge [
    source 24
    target 1693
  ]
  edge [
    source 24
    target 1694
  ]
  edge [
    source 24
    target 1695
  ]
  edge [
    source 24
    target 1696
  ]
  edge [
    source 24
    target 1697
  ]
  edge [
    source 24
    target 1698
  ]
  edge [
    source 24
    target 1699
  ]
  edge [
    source 24
    target 1700
  ]
  edge [
    source 24
    target 1288
  ]
  edge [
    source 24
    target 1701
  ]
  edge [
    source 24
    target 1437
  ]
  edge [
    source 24
    target 1702
  ]
  edge [
    source 24
    target 1465
  ]
  edge [
    source 24
    target 1466
  ]
  edge [
    source 24
    target 1467
  ]
  edge [
    source 24
    target 1468
  ]
  edge [
    source 24
    target 1469
  ]
  edge [
    source 24
    target 1470
  ]
  edge [
    source 24
    target 1081
  ]
  edge [
    source 24
    target 1082
  ]
  edge [
    source 24
    target 508
  ]
  edge [
    source 24
    target 1083
  ]
  edge [
    source 24
    target 1084
  ]
  edge [
    source 24
    target 1085
  ]
  edge [
    source 24
    target 1086
  ]
  edge [
    source 24
    target 132
  ]
  edge [
    source 24
    target 1087
  ]
  edge [
    source 24
    target 1088
  ]
  edge [
    source 24
    target 372
  ]
  edge [
    source 24
    target 1089
  ]
  edge [
    source 24
    target 1090
  ]
  edge [
    source 24
    target 1091
  ]
  edge [
    source 24
    target 1092
  ]
  edge [
    source 24
    target 1093
  ]
  edge [
    source 24
    target 436
  ]
  edge [
    source 24
    target 1094
  ]
  edge [
    source 24
    target 391
  ]
  edge [
    source 24
    target 1095
  ]
  edge [
    source 24
    target 1096
  ]
  edge [
    source 24
    target 1098
  ]
  edge [
    source 24
    target 1099
  ]
  edge [
    source 24
    target 1100
  ]
  edge [
    source 24
    target 1101
  ]
  edge [
    source 24
    target 1102
  ]
  edge [
    source 24
    target 1103
  ]
  edge [
    source 24
    target 1703
  ]
  edge [
    source 24
    target 1704
  ]
  edge [
    source 24
    target 1705
  ]
  edge [
    source 24
    target 1706
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 38
  ]
  edge [
    source 24
    target 42
  ]
  edge [
    source 24
    target 54
  ]
  edge [
    source 24
    target 40
  ]
  edge [
    source 24
    target 68
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 58
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 70
  ]
  edge [
    source 25
    target 1707
  ]
  edge [
    source 25
    target 1555
  ]
  edge [
    source 25
    target 1124
  ]
  edge [
    source 25
    target 1708
  ]
  edge [
    source 25
    target 1709
  ]
  edge [
    source 25
    target 1710
  ]
  edge [
    source 25
    target 1711
  ]
  edge [
    source 25
    target 1712
  ]
  edge [
    source 25
    target 1713
  ]
  edge [
    source 25
    target 1714
  ]
  edge [
    source 25
    target 1715
  ]
  edge [
    source 25
    target 1716
  ]
  edge [
    source 25
    target 1717
  ]
  edge [
    source 25
    target 1718
  ]
  edge [
    source 25
    target 1121
  ]
  edge [
    source 25
    target 1719
  ]
  edge [
    source 25
    target 1720
  ]
  edge [
    source 25
    target 59
  ]
  edge [
    source 25
    target 1721
  ]
  edge [
    source 25
    target 1722
  ]
  edge [
    source 25
    target 1128
  ]
  edge [
    source 25
    target 1723
  ]
  edge [
    source 25
    target 1724
  ]
  edge [
    source 25
    target 1097
  ]
  edge [
    source 25
    target 1725
  ]
  edge [
    source 25
    target 1726
  ]
  edge [
    source 25
    target 1727
  ]
  edge [
    source 25
    target 1728
  ]
  edge [
    source 25
    target 1125
  ]
  edge [
    source 25
    target 1729
  ]
  edge [
    source 25
    target 1730
  ]
  edge [
    source 25
    target 1731
  ]
  edge [
    source 25
    target 1732
  ]
  edge [
    source 25
    target 1065
  ]
  edge [
    source 25
    target 1733
  ]
  edge [
    source 25
    target 1734
  ]
  edge [
    source 25
    target 1735
  ]
  edge [
    source 25
    target 1528
  ]
  edge [
    source 25
    target 1736
  ]
  edge [
    source 25
    target 1737
  ]
  edge [
    source 25
    target 1531
  ]
  edge [
    source 25
    target 1738
  ]
  edge [
    source 25
    target 1739
  ]
  edge [
    source 25
    target 1535
  ]
  edge [
    source 25
    target 1536
  ]
  edge [
    source 25
    target 1538
  ]
  edge [
    source 25
    target 1740
  ]
  edge [
    source 25
    target 53
  ]
  edge [
    source 25
    target 1741
  ]
  edge [
    source 25
    target 1742
  ]
  edge [
    source 25
    target 1534
  ]
  edge [
    source 25
    target 1532
  ]
  edge [
    source 25
    target 1743
  ]
  edge [
    source 25
    target 1744
  ]
  edge [
    source 25
    target 1094
  ]
  edge [
    source 25
    target 1745
  ]
  edge [
    source 25
    target 1746
  ]
  edge [
    source 25
    target 1163
  ]
  edge [
    source 25
    target 1537
  ]
  edge [
    source 25
    target 1070
  ]
  edge [
    source 25
    target 1747
  ]
  edge [
    source 25
    target 1748
  ]
  edge [
    source 25
    target 1357
  ]
  edge [
    source 25
    target 1749
  ]
  edge [
    source 25
    target 1750
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 1751
  ]
  edge [
    source 25
    target 1752
  ]
  edge [
    source 25
    target 1753
  ]
  edge [
    source 25
    target 1754
  ]
  edge [
    source 25
    target 1755
  ]
  edge [
    source 25
    target 1201
  ]
  edge [
    source 25
    target 1756
  ]
  edge [
    source 25
    target 1757
  ]
  edge [
    source 25
    target 121
  ]
  edge [
    source 25
    target 1758
  ]
  edge [
    source 25
    target 1759
  ]
  edge [
    source 25
    target 1760
  ]
  edge [
    source 25
    target 1761
  ]
  edge [
    source 25
    target 1762
  ]
  edge [
    source 25
    target 1763
  ]
  edge [
    source 25
    target 1619
  ]
  edge [
    source 25
    target 1764
  ]
  edge [
    source 25
    target 1765
  ]
  edge [
    source 25
    target 1766
  ]
  edge [
    source 25
    target 1767
  ]
  edge [
    source 25
    target 1768
  ]
  edge [
    source 25
    target 1769
  ]
  edge [
    source 25
    target 1770
  ]
  edge [
    source 25
    target 1771
  ]
  edge [
    source 25
    target 1772
  ]
  edge [
    source 25
    target 1773
  ]
  edge [
    source 25
    target 1774
  ]
  edge [
    source 25
    target 1775
  ]
  edge [
    source 25
    target 1776
  ]
  edge [
    source 25
    target 1777
  ]
  edge [
    source 25
    target 40
  ]
  edge [
    source 25
    target 68
  ]
  edge [
    source 26
    target 48
  ]
  edge [
    source 26
    target 49
  ]
  edge [
    source 26
    target 1778
  ]
  edge [
    source 26
    target 1779
  ]
  edge [
    source 26
    target 1780
  ]
  edge [
    source 26
    target 1781
  ]
  edge [
    source 26
    target 1782
  ]
  edge [
    source 26
    target 1783
  ]
  edge [
    source 26
    target 1784
  ]
  edge [
    source 26
    target 1785
  ]
  edge [
    source 26
    target 1786
  ]
  edge [
    source 26
    target 1787
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 1788
  ]
  edge [
    source 28
    target 1789
  ]
  edge [
    source 28
    target 1790
  ]
  edge [
    source 28
    target 1791
  ]
  edge [
    source 28
    target 1792
  ]
  edge [
    source 28
    target 1793
  ]
  edge [
    source 28
    target 1794
  ]
  edge [
    source 28
    target 1795
  ]
  edge [
    source 28
    target 1796
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1797
  ]
  edge [
    source 28
    target 1798
  ]
  edge [
    source 28
    target 1799
  ]
  edge [
    source 28
    target 1800
  ]
  edge [
    source 28
    target 1665
  ]
  edge [
    source 28
    target 1801
  ]
  edge [
    source 28
    target 1802
  ]
  edge [
    source 28
    target 92
  ]
  edge [
    source 28
    target 1650
  ]
  edge [
    source 28
    target 1803
  ]
  edge [
    source 28
    target 1804
  ]
  edge [
    source 28
    target 1805
  ]
  edge [
    source 28
    target 1806
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 1807
  ]
  edge [
    source 28
    target 1808
  ]
  edge [
    source 28
    target 1809
  ]
  edge [
    source 28
    target 1810
  ]
  edge [
    source 28
    target 1811
  ]
  edge [
    source 28
    target 121
  ]
  edge [
    source 28
    target 1265
  ]
  edge [
    source 28
    target 373
  ]
  edge [
    source 28
    target 1812
  ]
  edge [
    source 28
    target 1813
  ]
  edge [
    source 28
    target 1814
  ]
  edge [
    source 28
    target 1815
  ]
  edge [
    source 28
    target 1816
  ]
  edge [
    source 28
    target 1817
  ]
  edge [
    source 28
    target 1818
  ]
  edge [
    source 28
    target 1819
  ]
  edge [
    source 28
    target 1820
  ]
  edge [
    source 28
    target 1821
  ]
  edge [
    source 28
    target 1822
  ]
  edge [
    source 28
    target 1823
  ]
  edge [
    source 28
    target 1824
  ]
  edge [
    source 28
    target 476
  ]
  edge [
    source 28
    target 1825
  ]
  edge [
    source 28
    target 1826
  ]
  edge [
    source 28
    target 385
  ]
  edge [
    source 28
    target 407
  ]
  edge [
    source 28
    target 418
  ]
  edge [
    source 28
    target 419
  ]
  edge [
    source 28
    target 420
  ]
  edge [
    source 28
    target 421
  ]
  edge [
    source 28
    target 422
  ]
  edge [
    source 28
    target 423
  ]
  edge [
    source 28
    target 133
  ]
  edge [
    source 28
    target 424
  ]
  edge [
    source 28
    target 425
  ]
  edge [
    source 28
    target 426
  ]
  edge [
    source 28
    target 427
  ]
  edge [
    source 28
    target 372
  ]
  edge [
    source 28
    target 428
  ]
  edge [
    source 28
    target 429
  ]
  edge [
    source 28
    target 430
  ]
  edge [
    source 28
    target 431
  ]
  edge [
    source 28
    target 432
  ]
  edge [
    source 28
    target 433
  ]
  edge [
    source 28
    target 434
  ]
  edge [
    source 28
    target 435
  ]
  edge [
    source 28
    target 436
  ]
  edge [
    source 28
    target 437
  ]
  edge [
    source 28
    target 438
  ]
  edge [
    source 28
    target 439
  ]
  edge [
    source 28
    target 440
  ]
  edge [
    source 28
    target 441
  ]
  edge [
    source 28
    target 442
  ]
  edge [
    source 28
    target 443
  ]
  edge [
    source 28
    target 1827
  ]
  edge [
    source 28
    target 1828
  ]
  edge [
    source 28
    target 1829
  ]
  edge [
    source 28
    target 1830
  ]
  edge [
    source 28
    target 1831
  ]
  edge [
    source 28
    target 1678
  ]
  edge [
    source 28
    target 1832
  ]
  edge [
    source 28
    target 1833
  ]
  edge [
    source 28
    target 1834
  ]
  edge [
    source 28
    target 63
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1581
  ]
  edge [
    source 29
    target 1835
  ]
  edge [
    source 29
    target 373
  ]
  edge [
    source 29
    target 557
  ]
  edge [
    source 29
    target 1836
  ]
  edge [
    source 29
    target 260
  ]
  edge [
    source 29
    target 1837
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 390
  ]
  edge [
    source 29
    target 391
  ]
  edge [
    source 29
    target 392
  ]
  edge [
    source 29
    target 393
  ]
  edge [
    source 29
    target 394
  ]
  edge [
    source 29
    target 395
  ]
  edge [
    source 29
    target 396
  ]
  edge [
    source 29
    target 397
  ]
  edge [
    source 29
    target 398
  ]
  edge [
    source 29
    target 399
  ]
  edge [
    source 29
    target 400
  ]
  edge [
    source 29
    target 401
  ]
  edge [
    source 29
    target 402
  ]
  edge [
    source 29
    target 403
  ]
  edge [
    source 29
    target 404
  ]
  edge [
    source 29
    target 405
  ]
  edge [
    source 29
    target 406
  ]
  edge [
    source 29
    target 385
  ]
  edge [
    source 29
    target 407
  ]
  edge [
    source 29
    target 408
  ]
  edge [
    source 29
    target 444
  ]
  edge [
    source 29
    target 445
  ]
  edge [
    source 29
    target 446
  ]
  edge [
    source 29
    target 447
  ]
  edge [
    source 29
    target 448
  ]
  edge [
    source 29
    target 449
  ]
  edge [
    source 29
    target 450
  ]
  edge [
    source 29
    target 451
  ]
  edge [
    source 29
    target 452
  ]
  edge [
    source 29
    target 453
  ]
  edge [
    source 29
    target 454
  ]
  edge [
    source 29
    target 455
  ]
  edge [
    source 29
    target 456
  ]
  edge [
    source 29
    target 457
  ]
  edge [
    source 29
    target 458
  ]
  edge [
    source 29
    target 459
  ]
  edge [
    source 29
    target 460
  ]
  edge [
    source 29
    target 461
  ]
  edge [
    source 29
    target 462
  ]
  edge [
    source 29
    target 463
  ]
  edge [
    source 29
    target 464
  ]
  edge [
    source 29
    target 465
  ]
  edge [
    source 29
    target 466
  ]
  edge [
    source 29
    target 289
  ]
  edge [
    source 29
    target 467
  ]
  edge [
    source 29
    target 468
  ]
  edge [
    source 29
    target 469
  ]
  edge [
    source 29
    target 470
  ]
  edge [
    source 29
    target 471
  ]
  edge [
    source 29
    target 472
  ]
  edge [
    source 29
    target 473
  ]
  edge [
    source 29
    target 474
  ]
  edge [
    source 29
    target 475
  ]
  edge [
    source 29
    target 476
  ]
  edge [
    source 29
    target 1081
  ]
  edge [
    source 29
    target 1082
  ]
  edge [
    source 29
    target 508
  ]
  edge [
    source 29
    target 1083
  ]
  edge [
    source 29
    target 1084
  ]
  edge [
    source 29
    target 1085
  ]
  edge [
    source 29
    target 1086
  ]
  edge [
    source 29
    target 132
  ]
  edge [
    source 29
    target 1087
  ]
  edge [
    source 29
    target 1088
  ]
  edge [
    source 29
    target 372
  ]
  edge [
    source 29
    target 1089
  ]
  edge [
    source 29
    target 1090
  ]
  edge [
    source 29
    target 1091
  ]
  edge [
    source 29
    target 1092
  ]
  edge [
    source 29
    target 1093
  ]
  edge [
    source 29
    target 436
  ]
  edge [
    source 29
    target 1094
  ]
  edge [
    source 29
    target 1095
  ]
  edge [
    source 29
    target 1096
  ]
  edge [
    source 29
    target 1097
  ]
  edge [
    source 29
    target 1098
  ]
  edge [
    source 29
    target 1099
  ]
  edge [
    source 29
    target 1100
  ]
  edge [
    source 29
    target 1101
  ]
  edge [
    source 29
    target 1102
  ]
  edge [
    source 29
    target 1103
  ]
  edge [
    source 29
    target 1838
  ]
  edge [
    source 29
    target 1839
  ]
  edge [
    source 29
    target 1840
  ]
  edge [
    source 29
    target 439
  ]
  edge [
    source 29
    target 1841
  ]
  edge [
    source 29
    target 1842
  ]
  edge [
    source 29
    target 1843
  ]
  edge [
    source 29
    target 1844
  ]
  edge [
    source 29
    target 1845
  ]
  edge [
    source 29
    target 1846
  ]
  edge [
    source 29
    target 1847
  ]
  edge [
    source 29
    target 1848
  ]
  edge [
    source 29
    target 1849
  ]
  edge [
    source 29
    target 501
  ]
  edge [
    source 29
    target 1850
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1851
  ]
  edge [
    source 30
    target 1852
  ]
  edge [
    source 30
    target 1853
  ]
  edge [
    source 30
    target 1782
  ]
  edge [
    source 30
    target 1854
  ]
  edge [
    source 30
    target 1855
  ]
  edge [
    source 30
    target 1856
  ]
  edge [
    source 30
    target 1857
  ]
  edge [
    source 30
    target 1858
  ]
  edge [
    source 30
    target 1859
  ]
  edge [
    source 30
    target 1860
  ]
  edge [
    source 30
    target 1861
  ]
  edge [
    source 30
    target 1862
  ]
  edge [
    source 30
    target 1863
  ]
  edge [
    source 30
    target 1864
  ]
  edge [
    source 30
    target 1865
  ]
  edge [
    source 30
    target 1866
  ]
  edge [
    source 30
    target 1867
  ]
  edge [
    source 30
    target 1868
  ]
  edge [
    source 30
    target 1869
  ]
  edge [
    source 30
    target 1870
  ]
  edge [
    source 30
    target 1392
  ]
  edge [
    source 30
    target 1871
  ]
  edge [
    source 30
    target 1872
  ]
  edge [
    source 30
    target 65
  ]
  edge [
    source 30
    target 3193
  ]
  edge [
    source 30
    target 3187
  ]
  edge [
    source 30
    target 388
  ]
  edge [
    source 30
    target 3194
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1873
  ]
  edge [
    source 31
    target 1322
  ]
  edge [
    source 31
    target 1874
  ]
  edge [
    source 31
    target 1875
  ]
  edge [
    source 31
    target 1876
  ]
  edge [
    source 31
    target 1877
  ]
  edge [
    source 31
    target 1878
  ]
  edge [
    source 31
    target 1879
  ]
  edge [
    source 31
    target 1880
  ]
  edge [
    source 31
    target 1881
  ]
  edge [
    source 31
    target 1293
  ]
  edge [
    source 31
    target 1882
  ]
  edge [
    source 31
    target 1883
  ]
  edge [
    source 31
    target 1438
  ]
  edge [
    source 31
    target 1884
  ]
  edge [
    source 31
    target 1885
  ]
  edge [
    source 31
    target 1886
  ]
  edge [
    source 31
    target 1887
  ]
  edge [
    source 31
    target 1888
  ]
  edge [
    source 31
    target 1889
  ]
  edge [
    source 31
    target 1890
  ]
  edge [
    source 31
    target 1891
  ]
  edge [
    source 31
    target 1892
  ]
  edge [
    source 31
    target 1893
  ]
  edge [
    source 31
    target 1894
  ]
  edge [
    source 31
    target 1895
  ]
  edge [
    source 31
    target 1896
  ]
  edge [
    source 31
    target 1897
  ]
  edge [
    source 31
    target 1898
  ]
  edge [
    source 31
    target 1899
  ]
  edge [
    source 31
    target 1900
  ]
  edge [
    source 31
    target 1901
  ]
  edge [
    source 31
    target 1902
  ]
  edge [
    source 31
    target 1903
  ]
  edge [
    source 31
    target 1904
  ]
  edge [
    source 31
    target 1905
  ]
  edge [
    source 31
    target 1906
  ]
  edge [
    source 31
    target 1907
  ]
  edge [
    source 31
    target 370
  ]
  edge [
    source 31
    target 1908
  ]
  edge [
    source 31
    target 1909
  ]
  edge [
    source 31
    target 1910
  ]
  edge [
    source 31
    target 1911
  ]
  edge [
    source 31
    target 1912
  ]
  edge [
    source 31
    target 1913
  ]
  edge [
    source 31
    target 1611
  ]
  edge [
    source 31
    target 1914
  ]
  edge [
    source 31
    target 1915
  ]
  edge [
    source 31
    target 55
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 63
  ]
  edge [
    source 32
    target 1916
  ]
  edge [
    source 32
    target 1917
  ]
  edge [
    source 32
    target 1918
  ]
  edge [
    source 32
    target 1919
  ]
  edge [
    source 32
    target 1920
  ]
  edge [
    source 32
    target 1921
  ]
  edge [
    source 32
    target 1922
  ]
  edge [
    source 32
    target 1923
  ]
  edge [
    source 32
    target 1924
  ]
  edge [
    source 32
    target 60
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 33
    target 45
  ]
  edge [
    source 33
    target 60
  ]
  edge [
    source 34
    target 1925
  ]
  edge [
    source 34
    target 1926
  ]
  edge [
    source 34
    target 1927
  ]
  edge [
    source 34
    target 1928
  ]
  edge [
    source 34
    target 1929
  ]
  edge [
    source 34
    target 1930
  ]
  edge [
    source 34
    target 1931
  ]
  edge [
    source 34
    target 1932
  ]
  edge [
    source 34
    target 1933
  ]
  edge [
    source 34
    target 1934
  ]
  edge [
    source 34
    target 1935
  ]
  edge [
    source 34
    target 1936
  ]
  edge [
    source 34
    target 1937
  ]
  edge [
    source 34
    target 1938
  ]
  edge [
    source 34
    target 1939
  ]
  edge [
    source 34
    target 1940
  ]
  edge [
    source 34
    target 1941
  ]
  edge [
    source 34
    target 1942
  ]
  edge [
    source 34
    target 1943
  ]
  edge [
    source 34
    target 1944
  ]
  edge [
    source 34
    target 1945
  ]
  edge [
    source 34
    target 1946
  ]
  edge [
    source 34
    target 1947
  ]
  edge [
    source 34
    target 1948
  ]
  edge [
    source 34
    target 1949
  ]
  edge [
    source 34
    target 1950
  ]
  edge [
    source 34
    target 1241
  ]
  edge [
    source 34
    target 1951
  ]
  edge [
    source 34
    target 1952
  ]
  edge [
    source 34
    target 1953
  ]
  edge [
    source 34
    target 1954
  ]
  edge [
    source 34
    target 1955
  ]
  edge [
    source 34
    target 1956
  ]
  edge [
    source 34
    target 1957
  ]
  edge [
    source 34
    target 418
  ]
  edge [
    source 34
    target 1958
  ]
  edge [
    source 34
    target 1959
  ]
  edge [
    source 34
    target 1960
  ]
  edge [
    source 34
    target 1961
  ]
  edge [
    source 34
    target 427
  ]
  edge [
    source 34
    target 1962
  ]
  edge [
    source 34
    target 1963
  ]
  edge [
    source 34
    target 1964
  ]
  edge [
    source 34
    target 1965
  ]
  edge [
    source 34
    target 1966
  ]
  edge [
    source 34
    target 1967
  ]
  edge [
    source 34
    target 1968
  ]
  edge [
    source 34
    target 1969
  ]
  edge [
    source 34
    target 1970
  ]
  edge [
    source 34
    target 1971
  ]
  edge [
    source 34
    target 1972
  ]
  edge [
    source 34
    target 1973
  ]
  edge [
    source 34
    target 1974
  ]
  edge [
    source 34
    target 1975
  ]
  edge [
    source 34
    target 1976
  ]
  edge [
    source 34
    target 1977
  ]
  edge [
    source 34
    target 1399
  ]
  edge [
    source 34
    target 1978
  ]
  edge [
    source 34
    target 1594
  ]
  edge [
    source 34
    target 1979
  ]
  edge [
    source 34
    target 1980
  ]
  edge [
    source 34
    target 1981
  ]
  edge [
    source 34
    target 1982
  ]
  edge [
    source 34
    target 1983
  ]
  edge [
    source 34
    target 1984
  ]
  edge [
    source 34
    target 1985
  ]
  edge [
    source 34
    target 1986
  ]
  edge [
    source 34
    target 1987
  ]
  edge [
    source 34
    target 1988
  ]
  edge [
    source 34
    target 1989
  ]
  edge [
    source 34
    target 1990
  ]
  edge [
    source 34
    target 1991
  ]
  edge [
    source 34
    target 1992
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 390
  ]
  edge [
    source 35
    target 391
  ]
  edge [
    source 35
    target 392
  ]
  edge [
    source 35
    target 393
  ]
  edge [
    source 35
    target 394
  ]
  edge [
    source 35
    target 395
  ]
  edge [
    source 35
    target 396
  ]
  edge [
    source 35
    target 397
  ]
  edge [
    source 35
    target 398
  ]
  edge [
    source 35
    target 399
  ]
  edge [
    source 35
    target 400
  ]
  edge [
    source 35
    target 401
  ]
  edge [
    source 35
    target 402
  ]
  edge [
    source 35
    target 403
  ]
  edge [
    source 35
    target 404
  ]
  edge [
    source 35
    target 405
  ]
  edge [
    source 35
    target 406
  ]
  edge [
    source 35
    target 385
  ]
  edge [
    source 35
    target 407
  ]
  edge [
    source 35
    target 408
  ]
  edge [
    source 35
    target 1095
  ]
  edge [
    source 35
    target 1096
  ]
  edge [
    source 35
    target 1082
  ]
  edge [
    source 35
    target 1083
  ]
  edge [
    source 35
    target 1192
  ]
  edge [
    source 35
    target 1084
  ]
  edge [
    source 35
    target 1100
  ]
  edge [
    source 35
    target 1090
  ]
  edge [
    source 35
    target 1993
  ]
  edge [
    source 35
    target 1099
  ]
  edge [
    source 35
    target 1994
  ]
  edge [
    source 35
    target 436
  ]
  edge [
    source 35
    target 1087
  ]
  edge [
    source 35
    target 132
  ]
  edge [
    source 35
    target 1091
  ]
  edge [
    source 35
    target 1103
  ]
  edge [
    source 35
    target 1995
  ]
  edge [
    source 35
    target 421
  ]
  edge [
    source 35
    target 372
  ]
  edge [
    source 35
    target 1996
  ]
  edge [
    source 35
    target 428
  ]
  edge [
    source 35
    target 289
  ]
  edge [
    source 35
    target 479
  ]
  edge [
    source 35
    target 441
  ]
  edge [
    source 35
    target 1997
  ]
  edge [
    source 35
    target 429
  ]
  edge [
    source 35
    target 435
  ]
  edge [
    source 35
    target 1998
  ]
  edge [
    source 35
    target 1999
  ]
  edge [
    source 35
    target 937
  ]
  edge [
    source 35
    target 2000
  ]
  edge [
    source 35
    target 1810
  ]
  edge [
    source 35
    target 2001
  ]
  edge [
    source 35
    target 2002
  ]
  edge [
    source 35
    target 1251
  ]
  edge [
    source 35
    target 2003
  ]
  edge [
    source 35
    target 1357
  ]
  edge [
    source 35
    target 2004
  ]
  edge [
    source 35
    target 2005
  ]
  edge [
    source 35
    target 2006
  ]
  edge [
    source 35
    target 2007
  ]
  edge [
    source 35
    target 2008
  ]
  edge [
    source 35
    target 2009
  ]
  edge [
    source 35
    target 2010
  ]
  edge [
    source 35
    target 2011
  ]
  edge [
    source 35
    target 2012
  ]
  edge [
    source 35
    target 2013
  ]
  edge [
    source 35
    target 2014
  ]
  edge [
    source 35
    target 1844
  ]
  edge [
    source 35
    target 1054
  ]
  edge [
    source 35
    target 2015
  ]
  edge [
    source 35
    target 2016
  ]
  edge [
    source 35
    target 2017
  ]
  edge [
    source 35
    target 87
  ]
  edge [
    source 35
    target 1051
  ]
  edge [
    source 35
    target 2018
  ]
  edge [
    source 35
    target 2019
  ]
  edge [
    source 35
    target 2020
  ]
  edge [
    source 35
    target 1925
  ]
  edge [
    source 35
    target 2021
  ]
  edge [
    source 35
    target 2022
  ]
  edge [
    source 35
    target 2023
  ]
  edge [
    source 35
    target 1125
  ]
  edge [
    source 35
    target 2024
  ]
  edge [
    source 35
    target 2025
  ]
  edge [
    source 35
    target 59
  ]
  edge [
    source 35
    target 67
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 2026
  ]
  edge [
    source 36
    target 2027
  ]
  edge [
    source 36
    target 2028
  ]
  edge [
    source 36
    target 1673
  ]
  edge [
    source 36
    target 2029
  ]
  edge [
    source 36
    target 2030
  ]
  edge [
    source 36
    target 1524
  ]
  edge [
    source 36
    target 2031
  ]
  edge [
    source 36
    target 2032
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 393
  ]
  edge [
    source 37
    target 2033
  ]
  edge [
    source 37
    target 2034
  ]
  edge [
    source 37
    target 2035
  ]
  edge [
    source 37
    target 1180
  ]
  edge [
    source 37
    target 2036
  ]
  edge [
    source 37
    target 2037
  ]
  edge [
    source 37
    target 1128
  ]
  edge [
    source 37
    target 2038
  ]
  edge [
    source 37
    target 2039
  ]
  edge [
    source 37
    target 1192
  ]
  edge [
    source 37
    target 2040
  ]
  edge [
    source 37
    target 2041
  ]
  edge [
    source 37
    target 1275
  ]
  edge [
    source 37
    target 2042
  ]
  edge [
    source 37
    target 2043
  ]
  edge [
    source 37
    target 2044
  ]
  edge [
    source 37
    target 1125
  ]
  edge [
    source 37
    target 2045
  ]
  edge [
    source 37
    target 2046
  ]
  edge [
    source 37
    target 372
  ]
  edge [
    source 37
    target 2047
  ]
  edge [
    source 37
    target 2048
  ]
  edge [
    source 37
    target 2049
  ]
  edge [
    source 37
    target 2050
  ]
  edge [
    source 37
    target 2051
  ]
  edge [
    source 37
    target 2052
  ]
  edge [
    source 37
    target 2053
  ]
  edge [
    source 37
    target 2054
  ]
  edge [
    source 37
    target 2055
  ]
  edge [
    source 37
    target 2056
  ]
  edge [
    source 37
    target 2057
  ]
  edge [
    source 37
    target 2058
  ]
  edge [
    source 37
    target 1124
  ]
  edge [
    source 37
    target 2059
  ]
  edge [
    source 37
    target 2060
  ]
  edge [
    source 37
    target 2061
  ]
  edge [
    source 37
    target 2062
  ]
  edge [
    source 37
    target 1201
  ]
  edge [
    source 37
    target 2063
  ]
  edge [
    source 37
    target 2064
  ]
  edge [
    source 37
    target 2065
  ]
  edge [
    source 37
    target 2066
  ]
  edge [
    source 37
    target 2067
  ]
  edge [
    source 37
    target 2068
  ]
  edge [
    source 37
    target 2069
  ]
  edge [
    source 37
    target 2070
  ]
  edge [
    source 37
    target 1100
  ]
  edge [
    source 37
    target 1304
  ]
  edge [
    source 37
    target 2071
  ]
  edge [
    source 37
    target 2072
  ]
  edge [
    source 37
    target 2073
  ]
  edge [
    source 37
    target 2074
  ]
  edge [
    source 37
    target 2075
  ]
  edge [
    source 37
    target 2076
  ]
  edge [
    source 37
    target 1163
  ]
  edge [
    source 37
    target 1287
  ]
  edge [
    source 37
    target 1351
  ]
  edge [
    source 37
    target 2077
  ]
  edge [
    source 37
    target 385
  ]
  edge [
    source 37
    target 2078
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 2079
  ]
  edge [
    source 38
    target 2080
  ]
  edge [
    source 38
    target 2081
  ]
  edge [
    source 38
    target 2082
  ]
  edge [
    source 38
    target 2083
  ]
  edge [
    source 38
    target 2084
  ]
  edge [
    source 38
    target 2085
  ]
  edge [
    source 38
    target 2086
  ]
  edge [
    source 38
    target 2087
  ]
  edge [
    source 38
    target 2088
  ]
  edge [
    source 38
    target 1905
  ]
  edge [
    source 38
    target 2089
  ]
  edge [
    source 38
    target 2090
  ]
  edge [
    source 38
    target 1452
  ]
  edge [
    source 38
    target 2091
  ]
  edge [
    source 38
    target 2092
  ]
  edge [
    source 38
    target 2093
  ]
  edge [
    source 38
    target 2094
  ]
  edge [
    source 38
    target 2095
  ]
  edge [
    source 38
    target 2096
  ]
  edge [
    source 38
    target 2097
  ]
  edge [
    source 38
    target 2098
  ]
  edge [
    source 38
    target 2099
  ]
  edge [
    source 38
    target 2100
  ]
  edge [
    source 38
    target 2101
  ]
  edge [
    source 38
    target 2102
  ]
  edge [
    source 38
    target 2103
  ]
  edge [
    source 38
    target 2104
  ]
  edge [
    source 38
    target 2105
  ]
  edge [
    source 38
    target 2106
  ]
  edge [
    source 38
    target 2107
  ]
  edge [
    source 38
    target 2108
  ]
  edge [
    source 38
    target 2109
  ]
  edge [
    source 38
    target 2110
  ]
  edge [
    source 38
    target 2111
  ]
  edge [
    source 38
    target 42
  ]
  edge [
    source 38
    target 57
  ]
  edge [
    source 38
    target 54
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 2112
  ]
  edge [
    source 39
    target 2113
  ]
  edge [
    source 39
    target 479
  ]
  edge [
    source 39
    target 2114
  ]
  edge [
    source 39
    target 2115
  ]
  edge [
    source 39
    target 2116
  ]
  edge [
    source 39
    target 2117
  ]
  edge [
    source 39
    target 2118
  ]
  edge [
    source 39
    target 2119
  ]
  edge [
    source 39
    target 2120
  ]
  edge [
    source 39
    target 2121
  ]
  edge [
    source 39
    target 2122
  ]
  edge [
    source 39
    target 2123
  ]
  edge [
    source 39
    target 2124
  ]
  edge [
    source 39
    target 2125
  ]
  edge [
    source 39
    target 2126
  ]
  edge [
    source 39
    target 2127
  ]
  edge [
    source 39
    target 2128
  ]
  edge [
    source 39
    target 2129
  ]
  edge [
    source 39
    target 2130
  ]
  edge [
    source 39
    target 2131
  ]
  edge [
    source 39
    target 2132
  ]
  edge [
    source 39
    target 2133
  ]
  edge [
    source 39
    target 2134
  ]
  edge [
    source 39
    target 2135
  ]
  edge [
    source 39
    target 2136
  ]
  edge [
    source 39
    target 2137
  ]
  edge [
    source 39
    target 2138
  ]
  edge [
    source 39
    target 2139
  ]
  edge [
    source 39
    target 2140
  ]
  edge [
    source 39
    target 2141
  ]
  edge [
    source 39
    target 2142
  ]
  edge [
    source 39
    target 2143
  ]
  edge [
    source 39
    target 2144
  ]
  edge [
    source 39
    target 2145
  ]
  edge [
    source 39
    target 2146
  ]
  edge [
    source 39
    target 2147
  ]
  edge [
    source 39
    target 2148
  ]
  edge [
    source 39
    target 2149
  ]
  edge [
    source 39
    target 2150
  ]
  edge [
    source 39
    target 2151
  ]
  edge [
    source 39
    target 2152
  ]
  edge [
    source 39
    target 2153
  ]
  edge [
    source 39
    target 2154
  ]
  edge [
    source 39
    target 2155
  ]
  edge [
    source 39
    target 2156
  ]
  edge [
    source 39
    target 2157
  ]
  edge [
    source 39
    target 2158
  ]
  edge [
    source 39
    target 2159
  ]
  edge [
    source 39
    target 2160
  ]
  edge [
    source 39
    target 2161
  ]
  edge [
    source 39
    target 2162
  ]
  edge [
    source 39
    target 2163
  ]
  edge [
    source 39
    target 2164
  ]
  edge [
    source 39
    target 1570
  ]
  edge [
    source 39
    target 271
  ]
  edge [
    source 39
    target 2165
  ]
  edge [
    source 39
    target 2166
  ]
  edge [
    source 39
    target 2167
  ]
  edge [
    source 39
    target 2168
  ]
  edge [
    source 39
    target 2169
  ]
  edge [
    source 39
    target 2170
  ]
  edge [
    source 39
    target 2171
  ]
  edge [
    source 39
    target 2172
  ]
  edge [
    source 39
    target 2173
  ]
  edge [
    source 39
    target 2174
  ]
  edge [
    source 39
    target 2175
  ]
  edge [
    source 39
    target 2176
  ]
  edge [
    source 39
    target 2177
  ]
  edge [
    source 39
    target 2178
  ]
  edge [
    source 39
    target 2179
  ]
  edge [
    source 39
    target 2180
  ]
  edge [
    source 39
    target 2181
  ]
  edge [
    source 39
    target 2182
  ]
  edge [
    source 39
    target 2183
  ]
  edge [
    source 39
    target 2184
  ]
  edge [
    source 39
    target 2185
  ]
  edge [
    source 39
    target 2186
  ]
  edge [
    source 39
    target 2187
  ]
  edge [
    source 39
    target 2188
  ]
  edge [
    source 39
    target 2189
  ]
  edge [
    source 39
    target 2190
  ]
  edge [
    source 39
    target 2191
  ]
  edge [
    source 39
    target 2192
  ]
  edge [
    source 39
    target 2193
  ]
  edge [
    source 39
    target 2194
  ]
  edge [
    source 39
    target 2195
  ]
  edge [
    source 39
    target 1177
  ]
  edge [
    source 39
    target 131
  ]
  edge [
    source 39
    target 2196
  ]
  edge [
    source 39
    target 2197
  ]
  edge [
    source 39
    target 2198
  ]
  edge [
    source 39
    target 2199
  ]
  edge [
    source 39
    target 2200
  ]
  edge [
    source 39
    target 2201
  ]
  edge [
    source 39
    target 2202
  ]
  edge [
    source 39
    target 2203
  ]
  edge [
    source 39
    target 2204
  ]
  edge [
    source 39
    target 2205
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 2206
  ]
  edge [
    source 40
    target 2207
  ]
  edge [
    source 40
    target 2208
  ]
  edge [
    source 40
    target 2209
  ]
  edge [
    source 40
    target 1201
  ]
  edge [
    source 40
    target 2210
  ]
  edge [
    source 40
    target 2211
  ]
  edge [
    source 40
    target 2040
  ]
  edge [
    source 40
    target 2212
  ]
  edge [
    source 40
    target 1063
  ]
  edge [
    source 40
    target 107
  ]
  edge [
    source 40
    target 2213
  ]
  edge [
    source 40
    target 2214
  ]
  edge [
    source 40
    target 2215
  ]
  edge [
    source 40
    target 2216
  ]
  edge [
    source 40
    target 1650
  ]
  edge [
    source 40
    target 2217
  ]
  edge [
    source 40
    target 2218
  ]
  edge [
    source 40
    target 2219
  ]
  edge [
    source 40
    target 1177
  ]
  edge [
    source 40
    target 2220
  ]
  edge [
    source 40
    target 2221
  ]
  edge [
    source 40
    target 98
  ]
  edge [
    source 40
    target 2222
  ]
  edge [
    source 40
    target 2223
  ]
  edge [
    source 40
    target 2224
  ]
  edge [
    source 40
    target 2225
  ]
  edge [
    source 40
    target 2226
  ]
  edge [
    source 40
    target 2227
  ]
  edge [
    source 40
    target 121
  ]
  edge [
    source 40
    target 68
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 42
    target 1141
  ]
  edge [
    source 42
    target 2228
  ]
  edge [
    source 42
    target 2229
  ]
  edge [
    source 42
    target 1197
  ]
  edge [
    source 42
    target 1125
  ]
  edge [
    source 42
    target 2230
  ]
  edge [
    source 42
    target 2231
  ]
  edge [
    source 42
    target 2232
  ]
  edge [
    source 42
    target 2233
  ]
  edge [
    source 42
    target 2234
  ]
  edge [
    source 42
    target 2235
  ]
  edge [
    source 42
    target 1070
  ]
  edge [
    source 42
    target 1903
  ]
  edge [
    source 42
    target 2236
  ]
  edge [
    source 42
    target 2237
  ]
  edge [
    source 42
    target 1192
  ]
  edge [
    source 42
    target 2217
  ]
  edge [
    source 42
    target 2238
  ]
  edge [
    source 42
    target 2239
  ]
  edge [
    source 42
    target 2227
  ]
  edge [
    source 42
    target 2240
  ]
  edge [
    source 42
    target 2241
  ]
  edge [
    source 42
    target 2242
  ]
  edge [
    source 42
    target 2243
  ]
  edge [
    source 42
    target 2244
  ]
  edge [
    source 42
    target 2245
  ]
  edge [
    source 42
    target 404
  ]
  edge [
    source 42
    target 2036
  ]
  edge [
    source 42
    target 1711
  ]
  edge [
    source 42
    target 1712
  ]
  edge [
    source 42
    target 2246
  ]
  edge [
    source 42
    target 1713
  ]
  edge [
    source 42
    target 2247
  ]
  edge [
    source 42
    target 1714
  ]
  edge [
    source 42
    target 1717
  ]
  edge [
    source 42
    target 2248
  ]
  edge [
    source 42
    target 2022
  ]
  edge [
    source 42
    target 1718
  ]
  edge [
    source 42
    target 1121
  ]
  edge [
    source 42
    target 1719
  ]
  edge [
    source 42
    target 1720
  ]
  edge [
    source 42
    target 2249
  ]
  edge [
    source 42
    target 1114
  ]
  edge [
    source 42
    target 2250
  ]
  edge [
    source 42
    target 1728
  ]
  edge [
    source 42
    target 2251
  ]
  edge [
    source 42
    target 2252
  ]
  edge [
    source 42
    target 2253
  ]
  edge [
    source 42
    target 2023
  ]
  edge [
    source 42
    target 2254
  ]
  edge [
    source 42
    target 1721
  ]
  edge [
    source 42
    target 2255
  ]
  edge [
    source 42
    target 1124
  ]
  edge [
    source 42
    target 2256
  ]
  edge [
    source 42
    target 391
  ]
  edge [
    source 42
    target 1723
  ]
  edge [
    source 42
    target 2257
  ]
  edge [
    source 42
    target 2258
  ]
  edge [
    source 42
    target 2259
  ]
  edge [
    source 42
    target 1725
  ]
  edge [
    source 42
    target 2260
  ]
  edge [
    source 42
    target 2261
  ]
  edge [
    source 42
    target 1726
  ]
  edge [
    source 42
    target 1727
  ]
  edge [
    source 42
    target 1722
  ]
  edge [
    source 42
    target 2262
  ]
  edge [
    source 42
    target 2263
  ]
  edge [
    source 42
    target 1193
  ]
  edge [
    source 42
    target 1191
  ]
  edge [
    source 42
    target 1194
  ]
  edge [
    source 42
    target 1195
  ]
  edge [
    source 42
    target 1196
  ]
  edge [
    source 42
    target 1198
  ]
  edge [
    source 42
    target 1199
  ]
  edge [
    source 42
    target 57
  ]
  edge [
    source 42
    target 54
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 2264
  ]
  edge [
    source 44
    target 2265
  ]
  edge [
    source 44
    target 2266
  ]
  edge [
    source 44
    target 2267
  ]
  edge [
    source 44
    target 2268
  ]
  edge [
    source 44
    target 2269
  ]
  edge [
    source 44
    target 1898
  ]
  edge [
    source 44
    target 2270
  ]
  edge [
    source 44
    target 1899
  ]
  edge [
    source 44
    target 2271
  ]
  edge [
    source 44
    target 2272
  ]
  edge [
    source 44
    target 2273
  ]
  edge [
    source 44
    target 2274
  ]
  edge [
    source 44
    target 2275
  ]
  edge [
    source 44
    target 2276
  ]
  edge [
    source 44
    target 2277
  ]
  edge [
    source 44
    target 2278
  ]
  edge [
    source 44
    target 2279
  ]
  edge [
    source 44
    target 2089
  ]
  edge [
    source 44
    target 2280
  ]
  edge [
    source 44
    target 2281
  ]
  edge [
    source 44
    target 2282
  ]
  edge [
    source 44
    target 2283
  ]
  edge [
    source 44
    target 2284
  ]
  edge [
    source 44
    target 2285
  ]
  edge [
    source 44
    target 2286
  ]
  edge [
    source 44
    target 2287
  ]
  edge [
    source 44
    target 2288
  ]
  edge [
    source 44
    target 2289
  ]
  edge [
    source 44
    target 1534
  ]
  edge [
    source 44
    target 1888
  ]
  edge [
    source 44
    target 2290
  ]
  edge [
    source 44
    target 2291
  ]
  edge [
    source 44
    target 2292
  ]
  edge [
    source 44
    target 2293
  ]
  edge [
    source 44
    target 1293
  ]
  edge [
    source 44
    target 1628
  ]
  edge [
    source 44
    target 2294
  ]
  edge [
    source 44
    target 2295
  ]
  edge [
    source 44
    target 2296
  ]
  edge [
    source 44
    target 2297
  ]
  edge [
    source 44
    target 2298
  ]
  edge [
    source 44
    target 2299
  ]
  edge [
    source 44
    target 2300
  ]
  edge [
    source 44
    target 2301
  ]
  edge [
    source 44
    target 2302
  ]
  edge [
    source 44
    target 2303
  ]
  edge [
    source 44
    target 2304
  ]
  edge [
    source 44
    target 2305
  ]
  edge [
    source 44
    target 2306
  ]
  edge [
    source 44
    target 2307
  ]
  edge [
    source 44
    target 2308
  ]
  edge [
    source 44
    target 2309
  ]
  edge [
    source 44
    target 1006
  ]
  edge [
    source 44
    target 2310
  ]
  edge [
    source 44
    target 2311
  ]
  edge [
    source 44
    target 2312
  ]
  edge [
    source 44
    target 1463
  ]
  edge [
    source 44
    target 2313
  ]
  edge [
    source 44
    target 2314
  ]
  edge [
    source 44
    target 2315
  ]
  edge [
    source 45
    target 2316
  ]
  edge [
    source 45
    target 1201
  ]
  edge [
    source 45
    target 2317
  ]
  edge [
    source 45
    target 1351
  ]
  edge [
    source 45
    target 2318
  ]
  edge [
    source 45
    target 2319
  ]
  edge [
    source 45
    target 2320
  ]
  edge [
    source 45
    target 2321
  ]
  edge [
    source 45
    target 1129
  ]
  edge [
    source 45
    target 2322
  ]
  edge [
    source 45
    target 2323
  ]
  edge [
    source 45
    target 2324
  ]
  edge [
    source 45
    target 2325
  ]
  edge [
    source 45
    target 2326
  ]
  edge [
    source 45
    target 121
  ]
  edge [
    source 45
    target 2327
  ]
  edge [
    source 45
    target 1572
  ]
  edge [
    source 45
    target 1177
  ]
  edge [
    source 45
    target 2328
  ]
  edge [
    source 45
    target 1899
  ]
  edge [
    source 45
    target 2329
  ]
  edge [
    source 45
    target 2330
  ]
  edge [
    source 45
    target 2331
  ]
  edge [
    source 45
    target 2332
  ]
  edge [
    source 45
    target 2333
  ]
  edge [
    source 45
    target 2334
  ]
  edge [
    source 45
    target 2335
  ]
  edge [
    source 45
    target 2336
  ]
  edge [
    source 45
    target 2337
  ]
  edge [
    source 45
    target 2338
  ]
  edge [
    source 45
    target 1054
  ]
  edge [
    source 45
    target 1275
  ]
  edge [
    source 45
    target 2339
  ]
  edge [
    source 45
    target 2340
  ]
  edge [
    source 45
    target 2341
  ]
  edge [
    source 45
    target 2342
  ]
  edge [
    source 45
    target 2343
  ]
  edge [
    source 45
    target 2344
  ]
  edge [
    source 45
    target 2345
  ]
  edge [
    source 45
    target 2346
  ]
  edge [
    source 45
    target 2347
  ]
  edge [
    source 45
    target 147
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 2348
  ]
  edge [
    source 47
    target 1553
  ]
  edge [
    source 47
    target 2349
  ]
  edge [
    source 47
    target 2350
  ]
  edge [
    source 47
    target 2351
  ]
  edge [
    source 47
    target 2352
  ]
  edge [
    source 47
    target 1163
  ]
  edge [
    source 47
    target 2353
  ]
  edge [
    source 47
    target 2354
  ]
  edge [
    source 47
    target 1551
  ]
  edge [
    source 47
    target 2355
  ]
  edge [
    source 47
    target 2356
  ]
  edge [
    source 47
    target 2357
  ]
  edge [
    source 47
    target 2358
  ]
  edge [
    source 47
    target 2359
  ]
  edge [
    source 47
    target 2360
  ]
  edge [
    source 47
    target 2361
  ]
  edge [
    source 47
    target 2362
  ]
  edge [
    source 47
    target 2363
  ]
  edge [
    source 47
    target 2364
  ]
  edge [
    source 47
    target 2365
  ]
  edge [
    source 47
    target 2366
  ]
  edge [
    source 47
    target 2367
  ]
  edge [
    source 47
    target 2368
  ]
  edge [
    source 47
    target 2369
  ]
  edge [
    source 47
    target 2370
  ]
  edge [
    source 47
    target 2371
  ]
  edge [
    source 47
    target 106
  ]
  edge [
    source 47
    target 2372
  ]
  edge [
    source 47
    target 2373
  ]
  edge [
    source 47
    target 2374
  ]
  edge [
    source 47
    target 1706
  ]
  edge [
    source 47
    target 2375
  ]
  edge [
    source 47
    target 1674
  ]
  edge [
    source 47
    target 119
  ]
  edge [
    source 47
    target 2376
  ]
  edge [
    source 47
    target 1165
  ]
  edge [
    source 47
    target 1166
  ]
  edge [
    source 47
    target 1167
  ]
  edge [
    source 47
    target 1171
  ]
  edge [
    source 47
    target 1168
  ]
  edge [
    source 47
    target 1170
  ]
  edge [
    source 47
    target 1169
  ]
  edge [
    source 47
    target 1172
  ]
  edge [
    source 47
    target 1173
  ]
  edge [
    source 47
    target 1174
  ]
  edge [
    source 47
    target 1175
  ]
  edge [
    source 47
    target 1176
  ]
  edge [
    source 47
    target 1177
  ]
  edge [
    source 47
    target 1178
  ]
  edge [
    source 47
    target 1181
  ]
  edge [
    source 47
    target 1180
  ]
  edge [
    source 47
    target 1179
  ]
  edge [
    source 47
    target 1182
  ]
  edge [
    source 47
    target 2377
  ]
  edge [
    source 47
    target 2378
  ]
  edge [
    source 47
    target 2379
  ]
  edge [
    source 47
    target 1650
  ]
  edge [
    source 47
    target 2380
  ]
  edge [
    source 47
    target 2381
  ]
  edge [
    source 47
    target 2382
  ]
  edge [
    source 47
    target 2383
  ]
  edge [
    source 47
    target 121
  ]
  edge [
    source 47
    target 2384
  ]
  edge [
    source 47
    target 92
  ]
  edge [
    source 47
    target 1344
  ]
  edge [
    source 47
    target 2385
  ]
  edge [
    source 47
    target 2386
  ]
  edge [
    source 47
    target 2387
  ]
  edge [
    source 47
    target 2388
  ]
  edge [
    source 47
    target 2389
  ]
  edge [
    source 47
    target 409
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 2390
  ]
  edge [
    source 47
    target 2391
  ]
  edge [
    source 47
    target 1054
  ]
  edge [
    source 47
    target 2392
  ]
  edge [
    source 47
    target 2393
  ]
  edge [
    source 47
    target 2394
  ]
  edge [
    source 47
    target 2395
  ]
  edge [
    source 47
    target 1531
  ]
  edge [
    source 47
    target 1621
  ]
  edge [
    source 47
    target 1622
  ]
  edge [
    source 47
    target 2396
  ]
  edge [
    source 47
    target 1092
  ]
  edge [
    source 47
    target 2397
  ]
  edge [
    source 47
    target 2398
  ]
  edge [
    source 47
    target 1096
  ]
  edge [
    source 47
    target 2399
  ]
  edge [
    source 47
    target 2400
  ]
  edge [
    source 47
    target 2401
  ]
  edge [
    source 47
    target 2402
  ]
  edge [
    source 47
    target 2403
  ]
  edge [
    source 47
    target 2404
  ]
  edge [
    source 47
    target 2405
  ]
  edge [
    source 47
    target 2406
  ]
  edge [
    source 47
    target 1796
  ]
  edge [
    source 47
    target 1278
  ]
  edge [
    source 47
    target 83
  ]
  edge [
    source 47
    target 2407
  ]
  edge [
    source 47
    target 2408
  ]
  edge [
    source 47
    target 2409
  ]
  edge [
    source 48
    target 2410
  ]
  edge [
    source 48
    target 1886
  ]
  edge [
    source 48
    target 2411
  ]
  edge [
    source 48
    target 2412
  ]
  edge [
    source 48
    target 2413
  ]
  edge [
    source 48
    target 2414
  ]
  edge [
    source 48
    target 1437
  ]
  edge [
    source 48
    target 2415
  ]
  edge [
    source 48
    target 2416
  ]
  edge [
    source 48
    target 2417
  ]
  edge [
    source 48
    target 2418
  ]
  edge [
    source 48
    target 2065
  ]
  edge [
    source 48
    target 1891
  ]
  edge [
    source 48
    target 2419
  ]
  edge [
    source 48
    target 2420
  ]
  edge [
    source 48
    target 2421
  ]
  edge [
    source 48
    target 2422
  ]
  edge [
    source 48
    target 65
  ]
  edge [
    source 48
    target 1895
  ]
  edge [
    source 48
    target 2423
  ]
  edge [
    source 48
    target 2081
  ]
  edge [
    source 48
    target 2424
  ]
  edge [
    source 48
    target 1907
  ]
  edge [
    source 48
    target 1319
  ]
  edge [
    source 48
    target 2425
  ]
  edge [
    source 48
    target 1913
  ]
  edge [
    source 48
    target 2426
  ]
  edge [
    source 48
    target 2427
  ]
  edge [
    source 48
    target 2428
  ]
  edge [
    source 48
    target 2429
  ]
  edge [
    source 48
    target 2430
  ]
  edge [
    source 48
    target 2431
  ]
  edge [
    source 48
    target 2287
  ]
  edge [
    source 48
    target 1439
  ]
  edge [
    source 48
    target 1208
  ]
  edge [
    source 48
    target 2432
  ]
  edge [
    source 48
    target 2433
  ]
  edge [
    source 48
    target 1888
  ]
  edge [
    source 48
    target 2434
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 48
    target 2435
  ]
  edge [
    source 48
    target 1158
  ]
  edge [
    source 48
    target 1417
  ]
  edge [
    source 48
    target 2436
  ]
  edge [
    source 48
    target 2437
  ]
  edge [
    source 48
    target 1701
  ]
  edge [
    source 48
    target 2438
  ]
  edge [
    source 48
    target 2439
  ]
  edge [
    source 48
    target 2440
  ]
  edge [
    source 48
    target 1293
  ]
  edge [
    source 48
    target 2441
  ]
  edge [
    source 48
    target 2442
  ]
  edge [
    source 48
    target 1874
  ]
  edge [
    source 48
    target 1443
  ]
  edge [
    source 48
    target 2443
  ]
  edge [
    source 48
    target 1506
  ]
  edge [
    source 49
    target 1957
  ]
  edge [
    source 49
    target 2355
  ]
  edge [
    source 49
    target 2444
  ]
  edge [
    source 49
    target 2445
  ]
  edge [
    source 49
    target 2446
  ]
  edge [
    source 49
    target 2447
  ]
  edge [
    source 49
    target 2448
  ]
  edge [
    source 49
    target 2449
  ]
  edge [
    source 49
    target 2277
  ]
  edge [
    source 49
    target 2450
  ]
  edge [
    source 49
    target 2451
  ]
  edge [
    source 49
    target 2452
  ]
  edge [
    source 49
    target 2453
  ]
  edge [
    source 49
    target 2454
  ]
  edge [
    source 49
    target 2455
  ]
  edge [
    source 49
    target 1275
  ]
  edge [
    source 49
    target 2456
  ]
  edge [
    source 49
    target 2457
  ]
  edge [
    source 49
    target 2458
  ]
  edge [
    source 49
    target 2459
  ]
  edge [
    source 49
    target 2460
  ]
  edge [
    source 49
    target 76
  ]
  edge [
    source 49
    target 2461
  ]
  edge [
    source 49
    target 2462
  ]
  edge [
    source 49
    target 1832
  ]
  edge [
    source 49
    target 2041
  ]
  edge [
    source 49
    target 2463
  ]
  edge [
    source 49
    target 2464
  ]
  edge [
    source 49
    target 2465
  ]
  edge [
    source 49
    target 2387
  ]
  edge [
    source 49
    target 2466
  ]
  edge [
    source 49
    target 2467
  ]
  edge [
    source 49
    target 2468
  ]
  edge [
    source 49
    target 2469
  ]
  edge [
    source 49
    target 2470
  ]
  edge [
    source 49
    target 2471
  ]
  edge [
    source 49
    target 2472
  ]
  edge [
    source 49
    target 2473
  ]
  edge [
    source 49
    target 2474
  ]
  edge [
    source 49
    target 2475
  ]
  edge [
    source 49
    target 2476
  ]
  edge [
    source 49
    target 2477
  ]
  edge [
    source 49
    target 1929
  ]
  edge [
    source 49
    target 2478
  ]
  edge [
    source 49
    target 2389
  ]
  edge [
    source 49
    target 409
  ]
  edge [
    source 49
    target 2390
  ]
  edge [
    source 49
    target 2391
  ]
  edge [
    source 49
    target 1177
  ]
  edge [
    source 49
    target 1054
  ]
  edge [
    source 49
    target 54
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 1778
  ]
  edge [
    source 50
    target 1780
  ]
  edge [
    source 50
    target 1781
  ]
  edge [
    source 50
    target 1782
  ]
  edge [
    source 50
    target 1783
  ]
  edge [
    source 50
    target 1784
  ]
  edge [
    source 50
    target 1785
  ]
  edge [
    source 50
    target 1786
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 2479
  ]
  edge [
    source 51
    target 2480
  ]
  edge [
    source 51
    target 2481
  ]
  edge [
    source 51
    target 2482
  ]
  edge [
    source 51
    target 1391
  ]
  edge [
    source 51
    target 2483
  ]
  edge [
    source 51
    target 1389
  ]
  edge [
    source 51
    target 2484
  ]
  edge [
    source 51
    target 2485
  ]
  edge [
    source 51
    target 2486
  ]
  edge [
    source 51
    target 1778
  ]
  edge [
    source 51
    target 2487
  ]
  edge [
    source 51
    target 2488
  ]
  edge [
    source 51
    target 2489
  ]
  edge [
    source 51
    target 2490
  ]
  edge [
    source 51
    target 2491
  ]
  edge [
    source 51
    target 2492
  ]
  edge [
    source 51
    target 2493
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 2494
  ]
  edge [
    source 52
    target 2495
  ]
  edge [
    source 52
    target 2496
  ]
  edge [
    source 52
    target 2497
  ]
  edge [
    source 52
    target 1356
  ]
  edge [
    source 52
    target 2048
  ]
  edge [
    source 52
    target 2498
  ]
  edge [
    source 52
    target 124
  ]
  edge [
    source 52
    target 2499
  ]
  edge [
    source 52
    target 2500
  ]
  edge [
    source 52
    target 2501
  ]
  edge [
    source 52
    target 2502
  ]
  edge [
    source 52
    target 1957
  ]
  edge [
    source 52
    target 2503
  ]
  edge [
    source 52
    target 1129
  ]
  edge [
    source 52
    target 2504
  ]
  edge [
    source 52
    target 2045
  ]
  edge [
    source 52
    target 2505
  ]
  edge [
    source 52
    target 2506
  ]
  edge [
    source 52
    target 2507
  ]
  edge [
    source 52
    target 2508
  ]
  edge [
    source 52
    target 2509
  ]
  edge [
    source 52
    target 2510
  ]
  edge [
    source 52
    target 2511
  ]
  edge [
    source 52
    target 2512
  ]
  edge [
    source 52
    target 289
  ]
  edge [
    source 52
    target 2513
  ]
  edge [
    source 52
    target 2514
  ]
  edge [
    source 52
    target 2445
  ]
  edge [
    source 52
    target 2446
  ]
  edge [
    source 52
    target 2447
  ]
  edge [
    source 52
    target 2448
  ]
  edge [
    source 52
    target 2449
  ]
  edge [
    source 52
    target 2277
  ]
  edge [
    source 52
    target 2450
  ]
  edge [
    source 52
    target 2451
  ]
  edge [
    source 52
    target 2452
  ]
  edge [
    source 52
    target 2453
  ]
  edge [
    source 52
    target 2454
  ]
  edge [
    source 52
    target 2455
  ]
  edge [
    source 52
    target 1275
  ]
  edge [
    source 52
    target 2456
  ]
  edge [
    source 52
    target 2457
  ]
  edge [
    source 52
    target 2458
  ]
  edge [
    source 52
    target 2459
  ]
  edge [
    source 52
    target 2460
  ]
  edge [
    source 52
    target 76
  ]
  edge [
    source 52
    target 2461
  ]
  edge [
    source 52
    target 2462
  ]
  edge [
    source 52
    target 1832
  ]
  edge [
    source 52
    target 2041
  ]
  edge [
    source 52
    target 2463
  ]
  edge [
    source 52
    target 2464
  ]
  edge [
    source 52
    target 2465
  ]
  edge [
    source 52
    target 2387
  ]
  edge [
    source 52
    target 2466
  ]
  edge [
    source 52
    target 2467
  ]
  edge [
    source 52
    target 2468
  ]
  edge [
    source 52
    target 2469
  ]
  edge [
    source 52
    target 2470
  ]
  edge [
    source 52
    target 2471
  ]
  edge [
    source 52
    target 2472
  ]
  edge [
    source 52
    target 2473
  ]
  edge [
    source 52
    target 2474
  ]
  edge [
    source 52
    target 2475
  ]
  edge [
    source 52
    target 2515
  ]
  edge [
    source 52
    target 2516
  ]
  edge [
    source 52
    target 1831
  ]
  edge [
    source 52
    target 1833
  ]
  edge [
    source 52
    target 2517
  ]
  edge [
    source 52
    target 2518
  ]
  edge [
    source 52
    target 2519
  ]
  edge [
    source 52
    target 2520
  ]
  edge [
    source 52
    target 2521
  ]
  edge [
    source 52
    target 2522
  ]
  edge [
    source 52
    target 2523
  ]
  edge [
    source 52
    target 2524
  ]
  edge [
    source 52
    target 2525
  ]
  edge [
    source 52
    target 2526
  ]
  edge [
    source 52
    target 2527
  ]
  edge [
    source 52
    target 2528
  ]
  edge [
    source 52
    target 2529
  ]
  edge [
    source 52
    target 2530
  ]
  edge [
    source 52
    target 2531
  ]
  edge [
    source 52
    target 2532
  ]
  edge [
    source 52
    target 2533
  ]
  edge [
    source 52
    target 2534
  ]
  edge [
    source 52
    target 2535
  ]
  edge [
    source 52
    target 2536
  ]
  edge [
    source 52
    target 2537
  ]
  edge [
    source 52
    target 2538
  ]
  edge [
    source 52
    target 2539
  ]
  edge [
    source 52
    target 2540
  ]
  edge [
    source 52
    target 2541
  ]
  edge [
    source 52
    target 2542
  ]
  edge [
    source 52
    target 1006
  ]
  edge [
    source 52
    target 1054
  ]
  edge [
    source 52
    target 2543
  ]
  edge [
    source 52
    target 2544
  ]
  edge [
    source 52
    target 2545
  ]
  edge [
    source 52
    target 2240
  ]
  edge [
    source 52
    target 121
  ]
  edge [
    source 52
    target 2007
  ]
  edge [
    source 52
    target 2546
  ]
  edge [
    source 52
    target 2547
  ]
  edge [
    source 52
    target 2548
  ]
  edge [
    source 52
    target 2549
  ]
  edge [
    source 52
    target 2550
  ]
  edge [
    source 52
    target 2551
  ]
  edge [
    source 52
    target 2552
  ]
  edge [
    source 52
    target 1351
  ]
  edge [
    source 52
    target 1624
  ]
  edge [
    source 52
    target 2553
  ]
  edge [
    source 52
    target 2554
  ]
  edge [
    source 52
    target 2555
  ]
  edge [
    source 52
    target 1165
  ]
  edge [
    source 52
    target 1171
  ]
  edge [
    source 52
    target 2556
  ]
  edge [
    source 52
    target 2557
  ]
  edge [
    source 52
    target 2558
  ]
  edge [
    source 52
    target 2559
  ]
  edge [
    source 52
    target 2560
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 53
    target 1413
  ]
  edge [
    source 53
    target 1598
  ]
  edge [
    source 53
    target 1599
  ]
  edge [
    source 53
    target 1444
  ]
  edge [
    source 53
    target 1567
  ]
  edge [
    source 53
    target 1600
  ]
  edge [
    source 53
    target 1601
  ]
  edge [
    source 53
    target 1602
  ]
  edge [
    source 53
    target 1603
  ]
  edge [
    source 53
    target 1604
  ]
  edge [
    source 53
    target 1436
  ]
  edge [
    source 53
    target 1092
  ]
  edge [
    source 53
    target 1605
  ]
  edge [
    source 53
    target 1606
  ]
  edge [
    source 53
    target 1587
  ]
  edge [
    source 53
    target 1607
  ]
  edge [
    source 53
    target 1608
  ]
  edge [
    source 53
    target 1590
  ]
  edge [
    source 53
    target 1609
  ]
  edge [
    source 53
    target 1610
  ]
  edge [
    source 53
    target 1611
  ]
  edge [
    source 53
    target 1612
  ]
  edge [
    source 53
    target 1613
  ]
  edge [
    source 53
    target 2561
  ]
  edge [
    source 53
    target 2562
  ]
  edge [
    source 53
    target 1884
  ]
  edge [
    source 53
    target 2563
  ]
  edge [
    source 53
    target 2564
  ]
  edge [
    source 53
    target 2565
  ]
  edge [
    source 53
    target 1411
  ]
  edge [
    source 53
    target 2566
  ]
  edge [
    source 53
    target 1643
  ]
  edge [
    source 53
    target 2567
  ]
  edge [
    source 53
    target 2568
  ]
  edge [
    source 53
    target 2569
  ]
  edge [
    source 53
    target 2570
  ]
  edge [
    source 53
    target 1300
  ]
  edge [
    source 53
    target 1435
  ]
  edge [
    source 53
    target 2571
  ]
  edge [
    source 53
    target 1895
  ]
  edge [
    source 53
    target 1511
  ]
  edge [
    source 53
    target 2572
  ]
  edge [
    source 53
    target 2573
  ]
  edge [
    source 53
    target 1570
  ]
  edge [
    source 53
    target 2574
  ]
  edge [
    source 53
    target 2575
  ]
  edge [
    source 53
    target 2576
  ]
  edge [
    source 53
    target 2577
  ]
  edge [
    source 53
    target 2578
  ]
  edge [
    source 53
    target 2579
  ]
  edge [
    source 53
    target 2580
  ]
  edge [
    source 53
    target 1308
  ]
  edge [
    source 53
    target 2581
  ]
  edge [
    source 53
    target 1537
  ]
  edge [
    source 53
    target 2582
  ]
  edge [
    source 53
    target 1447
  ]
  edge [
    source 53
    target 1622
  ]
  edge [
    source 53
    target 2583
  ]
  edge [
    source 53
    target 2584
  ]
  edge [
    source 53
    target 2585
  ]
  edge [
    source 53
    target 2586
  ]
  edge [
    source 53
    target 2587
  ]
  edge [
    source 53
    target 2588
  ]
  edge [
    source 53
    target 2589
  ]
  edge [
    source 53
    target 2590
  ]
  edge [
    source 53
    target 1418
  ]
  edge [
    source 53
    target 2591
  ]
  edge [
    source 53
    target 2592
  ]
  edge [
    source 53
    target 2593
  ]
  edge [
    source 53
    target 2594
  ]
  edge [
    source 53
    target 2595
  ]
  edge [
    source 53
    target 2596
  ]
  edge [
    source 53
    target 2597
  ]
  edge [
    source 53
    target 1528
  ]
  edge [
    source 53
    target 2598
  ]
  edge [
    source 53
    target 2599
  ]
  edge [
    source 53
    target 2600
  ]
  edge [
    source 53
    target 2601
  ]
  edge [
    source 53
    target 2602
  ]
  edge [
    source 53
    target 2234
  ]
  edge [
    source 53
    target 2106
  ]
  edge [
    source 53
    target 2603
  ]
  edge [
    source 53
    target 2604
  ]
  edge [
    source 53
    target 2605
  ]
  edge [
    source 53
    target 2606
  ]
  edge [
    source 53
    target 2607
  ]
  edge [
    source 53
    target 2608
  ]
  edge [
    source 53
    target 1569
  ]
  edge [
    source 53
    target 1070
  ]
  edge [
    source 53
    target 2609
  ]
  edge [
    source 53
    target 1814
  ]
  edge [
    source 53
    target 2610
  ]
  edge [
    source 53
    target 1531
  ]
  edge [
    source 53
    target 2611
  ]
  edge [
    source 53
    target 1535
  ]
  edge [
    source 53
    target 1536
  ]
  edge [
    source 53
    target 1538
  ]
  edge [
    source 53
    target 2612
  ]
  edge [
    source 53
    target 2613
  ]
  edge [
    source 53
    target 2614
  ]
  edge [
    source 53
    target 2615
  ]
  edge [
    source 53
    target 409
  ]
  edge [
    source 53
    target 2616
  ]
  edge [
    source 53
    target 2617
  ]
  edge [
    source 53
    target 2618
  ]
  edge [
    source 53
    target 1534
  ]
  edge [
    source 53
    target 2014
  ]
  edge [
    source 53
    target 2619
  ]
  edge [
    source 53
    target 2620
  ]
  edge [
    source 53
    target 1532
  ]
  edge [
    source 53
    target 2621
  ]
  edge [
    source 53
    target 2622
  ]
  edge [
    source 53
    target 1810
  ]
  edge [
    source 53
    target 1744
  ]
  edge [
    source 53
    target 1621
  ]
  edge [
    source 53
    target 2623
  ]
  edge [
    source 53
    target 1098
  ]
  edge [
    source 53
    target 2624
  ]
  edge [
    source 53
    target 2625
  ]
  edge [
    source 53
    target 2626
  ]
  edge [
    source 53
    target 2627
  ]
  edge [
    source 53
    target 476
  ]
  edge [
    source 53
    target 2628
  ]
  edge [
    source 53
    target 2326
  ]
  edge [
    source 53
    target 1549
  ]
  edge [
    source 53
    target 2629
  ]
  edge [
    source 53
    target 2630
  ]
  edge [
    source 53
    target 2631
  ]
  edge [
    source 53
    target 2632
  ]
  edge [
    source 53
    target 2633
  ]
  edge [
    source 53
    target 2634
  ]
  edge [
    source 53
    target 2635
  ]
  edge [
    source 53
    target 2040
  ]
  edge [
    source 53
    target 2636
  ]
  edge [
    source 53
    target 2637
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 68
  ]
  edge [
    source 54
    target 1834
  ]
  edge [
    source 54
    target 2638
  ]
  edge [
    source 54
    target 2639
  ]
  edge [
    source 54
    target 385
  ]
  edge [
    source 54
    target 1088
  ]
  edge [
    source 54
    target 1788
  ]
  edge [
    source 54
    target 1795
  ]
  edge [
    source 54
    target 1790
  ]
  edge [
    source 54
    target 1995
  ]
  edge [
    source 54
    target 421
  ]
  edge [
    source 54
    target 372
  ]
  edge [
    source 54
    target 1996
  ]
  edge [
    source 54
    target 428
  ]
  edge [
    source 54
    target 289
  ]
  edge [
    source 54
    target 479
  ]
  edge [
    source 54
    target 441
  ]
  edge [
    source 54
    target 1997
  ]
  edge [
    source 54
    target 429
  ]
  edge [
    source 54
    target 435
  ]
  edge [
    source 54
    target 1998
  ]
  edge [
    source 54
    target 1999
  ]
  edge [
    source 54
    target 937
  ]
  edge [
    source 54
    target 2000
  ]
  edge [
    source 54
    target 1810
  ]
  edge [
    source 54
    target 2001
  ]
  edge [
    source 54
    target 2002
  ]
  edge [
    source 54
    target 1265
  ]
  edge [
    source 54
    target 373
  ]
  edge [
    source 54
    target 1812
  ]
  edge [
    source 54
    target 1813
  ]
  edge [
    source 54
    target 1814
  ]
  edge [
    source 54
    target 1815
  ]
  edge [
    source 54
    target 1816
  ]
  edge [
    source 54
    target 1817
  ]
  edge [
    source 54
    target 1818
  ]
  edge [
    source 54
    target 1819
  ]
  edge [
    source 54
    target 1820
  ]
  edge [
    source 54
    target 1821
  ]
  edge [
    source 54
    target 1822
  ]
  edge [
    source 54
    target 1823
  ]
  edge [
    source 54
    target 1824
  ]
  edge [
    source 54
    target 476
  ]
  edge [
    source 54
    target 1825
  ]
  edge [
    source 54
    target 1826
  ]
  edge [
    source 54
    target 407
  ]
  edge [
    source 54
    target 2640
  ]
  edge [
    source 54
    target 57
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 2641
  ]
  edge [
    source 55
    target 2642
  ]
  edge [
    source 55
    target 2643
  ]
  edge [
    source 55
    target 2644
  ]
  edge [
    source 55
    target 2645
  ]
  edge [
    source 55
    target 2646
  ]
  edge [
    source 55
    target 2647
  ]
  edge [
    source 55
    target 2648
  ]
  edge [
    source 55
    target 2649
  ]
  edge [
    source 55
    target 2650
  ]
  edge [
    source 55
    target 2651
  ]
  edge [
    source 55
    target 2088
  ]
  edge [
    source 55
    target 2652
  ]
  edge [
    source 55
    target 2653
  ]
  edge [
    source 55
    target 2654
  ]
  edge [
    source 55
    target 2655
  ]
  edge [
    source 55
    target 2656
  ]
  edge [
    source 55
    target 2657
  ]
  edge [
    source 55
    target 1888
  ]
  edge [
    source 55
    target 2658
  ]
  edge [
    source 55
    target 2659
  ]
  edge [
    source 55
    target 1743
  ]
  edge [
    source 55
    target 2660
  ]
  edge [
    source 55
    target 1293
  ]
  edge [
    source 55
    target 2661
  ]
  edge [
    source 55
    target 2662
  ]
  edge [
    source 55
    target 2663
  ]
  edge [
    source 55
    target 1322
  ]
  edge [
    source 55
    target 2664
  ]
  edge [
    source 55
    target 1765
  ]
  edge [
    source 55
    target 1911
  ]
  edge [
    source 55
    target 2665
  ]
  edge [
    source 55
    target 2666
  ]
  edge [
    source 55
    target 2667
  ]
  edge [
    source 55
    target 2668
  ]
  edge [
    source 55
    target 2048
  ]
  edge [
    source 55
    target 2669
  ]
  edge [
    source 55
    target 2670
  ]
  edge [
    source 55
    target 1899
  ]
  edge [
    source 55
    target 2671
  ]
  edge [
    source 55
    target 1300
  ]
  edge [
    source 55
    target 2672
  ]
  edge [
    source 55
    target 1506
  ]
  edge [
    source 55
    target 2673
  ]
  edge [
    source 55
    target 2674
  ]
  edge [
    source 55
    target 2675
  ]
  edge [
    source 55
    target 2676
  ]
  edge [
    source 55
    target 2677
  ]
  edge [
    source 55
    target 2678
  ]
  edge [
    source 55
    target 2679
  ]
  edge [
    source 55
    target 2680
  ]
  edge [
    source 55
    target 2681
  ]
  edge [
    source 55
    target 2682
  ]
  edge [
    source 55
    target 2683
  ]
  edge [
    source 55
    target 2684
  ]
  edge [
    source 55
    target 1301
  ]
  edge [
    source 55
    target 2685
  ]
  edge [
    source 55
    target 2686
  ]
  edge [
    source 55
    target 2687
  ]
  edge [
    source 55
    target 2688
  ]
  edge [
    source 55
    target 2689
  ]
  edge [
    source 55
    target 2690
  ]
  edge [
    source 55
    target 2691
  ]
  edge [
    source 55
    target 2692
  ]
  edge [
    source 55
    target 2693
  ]
  edge [
    source 55
    target 2694
  ]
  edge [
    source 55
    target 2695
  ]
  edge [
    source 55
    target 1443
  ]
  edge [
    source 55
    target 2696
  ]
  edge [
    source 55
    target 2697
  ]
  edge [
    source 55
    target 2698
  ]
  edge [
    source 55
    target 1313
  ]
  edge [
    source 55
    target 2699
  ]
  edge [
    source 55
    target 2700
  ]
  edge [
    source 55
    target 2701
  ]
  edge [
    source 55
    target 2702
  ]
  edge [
    source 55
    target 2703
  ]
  edge [
    source 55
    target 2704
  ]
  edge [
    source 55
    target 2705
  ]
  edge [
    source 55
    target 2706
  ]
  edge [
    source 55
    target 2707
  ]
  edge [
    source 55
    target 2708
  ]
  edge [
    source 55
    target 2709
  ]
  edge [
    source 55
    target 2710
  ]
  edge [
    source 55
    target 2711
  ]
  edge [
    source 55
    target 2712
  ]
  edge [
    source 55
    target 2713
  ]
  edge [
    source 55
    target 2714
  ]
  edge [
    source 55
    target 2715
  ]
  edge [
    source 55
    target 2716
  ]
  edge [
    source 55
    target 2717
  ]
  edge [
    source 55
    target 2718
  ]
  edge [
    source 55
    target 2719
  ]
  edge [
    source 55
    target 2720
  ]
  edge [
    source 55
    target 2441
  ]
  edge [
    source 55
    target 2721
  ]
  edge [
    source 55
    target 2722
  ]
  edge [
    source 55
    target 2723
  ]
  edge [
    source 55
    target 2724
  ]
  edge [
    source 55
    target 2725
  ]
  edge [
    source 55
    target 2726
  ]
  edge [
    source 55
    target 2727
  ]
  edge [
    source 55
    target 2728
  ]
  edge [
    source 55
    target 2729
  ]
  edge [
    source 55
    target 2730
  ]
  edge [
    source 55
    target 2731
  ]
  edge [
    source 55
    target 2732
  ]
  edge [
    source 55
    target 2733
  ]
  edge [
    source 55
    target 2093
  ]
  edge [
    source 55
    target 2085
  ]
  edge [
    source 55
    target 2097
  ]
  edge [
    source 55
    target 2734
  ]
  edge [
    source 55
    target 1080
  ]
  edge [
    source 55
    target 2735
  ]
  edge [
    source 55
    target 2047
  ]
  edge [
    source 55
    target 2736
  ]
  edge [
    source 55
    target 2737
  ]
  edge [
    source 55
    target 2738
  ]
  edge [
    source 55
    target 2739
  ]
  edge [
    source 55
    target 2740
  ]
  edge [
    source 55
    target 2741
  ]
  edge [
    source 55
    target 2742
  ]
  edge [
    source 55
    target 2743
  ]
  edge [
    source 55
    target 2744
  ]
  edge [
    source 55
    target 2745
  ]
  edge [
    source 55
    target 2746
  ]
  edge [
    source 55
    target 2747
  ]
  edge [
    source 55
    target 2748
  ]
  edge [
    source 55
    target 2749
  ]
  edge [
    source 55
    target 2750
  ]
  edge [
    source 55
    target 2751
  ]
  edge [
    source 55
    target 2752
  ]
  edge [
    source 55
    target 1697
  ]
  edge [
    source 55
    target 2753
  ]
  edge [
    source 55
    target 2754
  ]
  edge [
    source 55
    target 2755
  ]
  edge [
    source 55
    target 2756
  ]
  edge [
    source 55
    target 2757
  ]
  edge [
    source 55
    target 2758
  ]
  edge [
    source 55
    target 121
  ]
  edge [
    source 55
    target 2759
  ]
  edge [
    source 55
    target 2760
  ]
  edge [
    source 55
    target 2761
  ]
  edge [
    source 55
    target 2762
  ]
  edge [
    source 55
    target 2763
  ]
  edge [
    source 55
    target 2764
  ]
  edge [
    source 55
    target 2765
  ]
  edge [
    source 55
    target 2622
  ]
  edge [
    source 55
    target 2766
  ]
  edge [
    source 55
    target 2767
  ]
  edge [
    source 55
    target 2768
  ]
  edge [
    source 55
    target 2769
  ]
  edge [
    source 55
    target 2770
  ]
  edge [
    source 55
    target 2771
  ]
  edge [
    source 55
    target 1288
  ]
  edge [
    source 55
    target 2772
  ]
  edge [
    source 55
    target 2773
  ]
  edge [
    source 55
    target 1925
  ]
  edge [
    source 55
    target 2774
  ]
  edge [
    source 55
    target 2775
  ]
  edge [
    source 55
    target 2776
  ]
  edge [
    source 55
    target 2777
  ]
  edge [
    source 55
    target 2778
  ]
  edge [
    source 55
    target 2779
  ]
  edge [
    source 55
    target 2780
  ]
  edge [
    source 55
    target 2781
  ]
  edge [
    source 55
    target 2782
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 1128
  ]
  edge [
    source 56
    target 372
  ]
  edge [
    source 56
    target 1119
  ]
  edge [
    source 56
    target 1139
  ]
  edge [
    source 56
    target 2783
  ]
  edge [
    source 56
    target 2037
  ]
  edge [
    source 57
    target 2784
  ]
  edge [
    source 57
    target 2785
  ]
  edge [
    source 57
    target 2786
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 2787
  ]
  edge [
    source 59
    target 2788
  ]
  edge [
    source 59
    target 2789
  ]
  edge [
    source 59
    target 1572
  ]
  edge [
    source 59
    target 2790
  ]
  edge [
    source 59
    target 2791
  ]
  edge [
    source 59
    target 2792
  ]
  edge [
    source 59
    target 2793
  ]
  edge [
    source 59
    target 2794
  ]
  edge [
    source 59
    target 2795
  ]
  edge [
    source 59
    target 2796
  ]
  edge [
    source 59
    target 2797
  ]
  edge [
    source 59
    target 2798
  ]
  edge [
    source 59
    target 2799
  ]
  edge [
    source 59
    target 2800
  ]
  edge [
    source 59
    target 2801
  ]
  edge [
    source 59
    target 2802
  ]
  edge [
    source 59
    target 2803
  ]
  edge [
    source 59
    target 1650
  ]
  edge [
    source 59
    target 2804
  ]
  edge [
    source 59
    target 2805
  ]
  edge [
    source 59
    target 2806
  ]
  edge [
    source 59
    target 2807
  ]
  edge [
    source 59
    target 2808
  ]
  edge [
    source 59
    target 2809
  ]
  edge [
    source 59
    target 2810
  ]
  edge [
    source 59
    target 2811
  ]
  edge [
    source 59
    target 1499
  ]
  edge [
    source 59
    target 2812
  ]
  edge [
    source 59
    target 2813
  ]
  edge [
    source 59
    target 121
  ]
  edge [
    source 59
    target 1727
  ]
  edge [
    source 59
    target 2814
  ]
  edge [
    source 59
    target 2815
  ]
  edge [
    source 59
    target 2816
  ]
  edge [
    source 59
    target 2817
  ]
  edge [
    source 59
    target 391
  ]
  edge [
    source 59
    target 2818
  ]
  edge [
    source 59
    target 2819
  ]
  edge [
    source 59
    target 2820
  ]
  edge [
    source 59
    target 2821
  ]
  edge [
    source 59
    target 2822
  ]
  edge [
    source 59
    target 2823
  ]
  edge [
    source 59
    target 1674
  ]
  edge [
    source 59
    target 119
  ]
  edge [
    source 59
    target 2824
  ]
  edge [
    source 59
    target 2825
  ]
  edge [
    source 59
    target 2826
  ]
  edge [
    source 59
    target 2827
  ]
  edge [
    source 59
    target 2828
  ]
  edge [
    source 59
    target 2829
  ]
  edge [
    source 59
    target 2830
  ]
  edge [
    source 59
    target 2072
  ]
  edge [
    source 59
    target 2831
  ]
  edge [
    source 59
    target 2262
  ]
  edge [
    source 59
    target 2832
  ]
  edge [
    source 59
    target 2833
  ]
  edge [
    source 59
    target 2834
  ]
  edge [
    source 59
    target 2835
  ]
  edge [
    source 59
    target 2836
  ]
  edge [
    source 59
    target 1027
  ]
  edge [
    source 59
    target 2837
  ]
  edge [
    source 59
    target 2838
  ]
  edge [
    source 59
    target 2839
  ]
  edge [
    source 59
    target 1054
  ]
  edge [
    source 59
    target 476
  ]
  edge [
    source 59
    target 2840
  ]
  edge [
    source 59
    target 147
  ]
  edge [
    source 59
    target 2841
  ]
  edge [
    source 59
    target 2842
  ]
  edge [
    source 59
    target 2843
  ]
  edge [
    source 59
    target 2844
  ]
  edge [
    source 59
    target 2845
  ]
  edge [
    source 59
    target 1116
  ]
  edge [
    source 59
    target 2846
  ]
  edge [
    source 59
    target 2847
  ]
  edge [
    source 59
    target 2848
  ]
  edge [
    source 59
    target 2849
  ]
  edge [
    source 59
    target 1129
  ]
  edge [
    source 59
    target 2390
  ]
  edge [
    source 59
    target 2850
  ]
  edge [
    source 59
    target 2851
  ]
  edge [
    source 59
    target 2337
  ]
  edge [
    source 59
    target 2852
  ]
  edge [
    source 59
    target 2853
  ]
  edge [
    source 59
    target 2854
  ]
  edge [
    source 59
    target 2855
  ]
  edge [
    source 59
    target 2856
  ]
  edge [
    source 59
    target 2389
  ]
  edge [
    source 59
    target 2857
  ]
  edge [
    source 59
    target 2858
  ]
  edge [
    source 59
    target 2859
  ]
  edge [
    source 59
    target 1288
  ]
  edge [
    source 59
    target 2860
  ]
  edge [
    source 59
    target 2861
  ]
  edge [
    source 59
    target 2862
  ]
  edge [
    source 59
    target 2863
  ]
  edge [
    source 59
    target 2330
  ]
  edge [
    source 59
    target 2864
  ]
  edge [
    source 59
    target 2865
  ]
  edge [
    source 59
    target 2866
  ]
  edge [
    source 59
    target 2867
  ]
  edge [
    source 59
    target 2868
  ]
  edge [
    source 59
    target 2869
  ]
  edge [
    source 59
    target 2870
  ]
  edge [
    source 59
    target 2871
  ]
  edge [
    source 59
    target 2872
  ]
  edge [
    source 59
    target 2873
  ]
  edge [
    source 59
    target 2874
  ]
  edge [
    source 59
    target 2875
  ]
  edge [
    source 59
    target 2876
  ]
  edge [
    source 59
    target 2877
  ]
  edge [
    source 59
    target 2878
  ]
  edge [
    source 59
    target 428
  ]
  edge [
    source 59
    target 2879
  ]
  edge [
    source 59
    target 2880
  ]
  edge [
    source 59
    target 2881
  ]
  edge [
    source 59
    target 2882
  ]
  edge [
    source 59
    target 2883
  ]
  edge [
    source 59
    target 477
  ]
  edge [
    source 59
    target 2884
  ]
  edge [
    source 59
    target 1496
  ]
  edge [
    source 59
    target 2885
  ]
  edge [
    source 59
    target 2886
  ]
  edge [
    source 59
    target 1432
  ]
  edge [
    source 59
    target 2887
  ]
  edge [
    source 59
    target 2888
  ]
  edge [
    source 59
    target 2889
  ]
  edge [
    source 59
    target 2452
  ]
  edge [
    source 59
    target 1355
  ]
  edge [
    source 59
    target 2890
  ]
  edge [
    source 59
    target 2891
  ]
  edge [
    source 59
    target 2892
  ]
  edge [
    source 59
    target 1351
  ]
  edge [
    source 59
    target 1100
  ]
  edge [
    source 59
    target 2893
  ]
  edge [
    source 59
    target 2894
  ]
  edge [
    source 59
    target 2895
  ]
  edge [
    source 59
    target 462
  ]
  edge [
    source 59
    target 2896
  ]
  edge [
    source 59
    target 1255
  ]
  edge [
    source 59
    target 2897
  ]
  edge [
    source 59
    target 2058
  ]
  edge [
    source 59
    target 2898
  ]
  edge [
    source 59
    target 2899
  ]
  edge [
    source 59
    target 2900
  ]
  edge [
    source 59
    target 1141
  ]
  edge [
    source 59
    target 2901
  ]
  edge [
    source 59
    target 2902
  ]
  edge [
    source 59
    target 2903
  ]
  edge [
    source 59
    target 2904
  ]
  edge [
    source 59
    target 2905
  ]
  edge [
    source 59
    target 2906
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 2907
  ]
  edge [
    source 60
    target 2908
  ]
  edge [
    source 60
    target 2909
  ]
  edge [
    source 60
    target 2910
  ]
  edge [
    source 60
    target 2911
  ]
  edge [
    source 60
    target 2912
  ]
  edge [
    source 60
    target 2913
  ]
  edge [
    source 60
    target 2914
  ]
  edge [
    source 60
    target 2915
  ]
  edge [
    source 60
    target 1163
  ]
  edge [
    source 60
    target 2916
  ]
  edge [
    source 60
    target 2917
  ]
  edge [
    source 60
    target 2918
  ]
  edge [
    source 60
    target 2919
  ]
  edge [
    source 60
    target 2920
  ]
  edge [
    source 60
    target 2921
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 2922
  ]
  edge [
    source 62
    target 2923
  ]
  edge [
    source 62
    target 2924
  ]
  edge [
    source 62
    target 1435
  ]
  edge [
    source 62
    target 2925
  ]
  edge [
    source 62
    target 1422
  ]
  edge [
    source 62
    target 1765
  ]
  edge [
    source 62
    target 2926
  ]
  edge [
    source 62
    target 1300
  ]
  edge [
    source 62
    target 2927
  ]
  edge [
    source 62
    target 2928
  ]
  edge [
    source 62
    target 2929
  ]
  edge [
    source 62
    target 2930
  ]
  edge [
    source 62
    target 1447
  ]
  edge [
    source 62
    target 1481
  ]
  edge [
    source 62
    target 2931
  ]
  edge [
    source 62
    target 2932
  ]
  edge [
    source 62
    target 2933
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 2340
  ]
  edge [
    source 63
    target 2057
  ]
  edge [
    source 63
    target 2934
  ]
  edge [
    source 63
    target 2935
  ]
  edge [
    source 63
    target 2936
  ]
  edge [
    source 63
    target 1678
  ]
  edge [
    source 63
    target 2937
  ]
  edge [
    source 63
    target 1679
  ]
  edge [
    source 63
    target 2938
  ]
  edge [
    source 63
    target 1794
  ]
  edge [
    source 63
    target 1788
  ]
  edge [
    source 63
    target 1796
  ]
  edge [
    source 63
    target 1790
  ]
  edge [
    source 63
    target 1795
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 385
  ]
  edge [
    source 64
    target 1088
  ]
  edge [
    source 64
    target 2939
  ]
  edge [
    source 64
    target 1995
  ]
  edge [
    source 64
    target 421
  ]
  edge [
    source 64
    target 372
  ]
  edge [
    source 64
    target 1996
  ]
  edge [
    source 64
    target 428
  ]
  edge [
    source 64
    target 289
  ]
  edge [
    source 64
    target 479
  ]
  edge [
    source 64
    target 441
  ]
  edge [
    source 64
    target 1997
  ]
  edge [
    source 64
    target 429
  ]
  edge [
    source 64
    target 435
  ]
  edge [
    source 64
    target 1998
  ]
  edge [
    source 64
    target 1999
  ]
  edge [
    source 64
    target 937
  ]
  edge [
    source 64
    target 2000
  ]
  edge [
    source 64
    target 1810
  ]
  edge [
    source 64
    target 2001
  ]
  edge [
    source 64
    target 2002
  ]
  edge [
    source 64
    target 1265
  ]
  edge [
    source 64
    target 373
  ]
  edge [
    source 64
    target 1812
  ]
  edge [
    source 64
    target 1813
  ]
  edge [
    source 64
    target 1814
  ]
  edge [
    source 64
    target 1815
  ]
  edge [
    source 64
    target 1816
  ]
  edge [
    source 64
    target 1817
  ]
  edge [
    source 64
    target 1818
  ]
  edge [
    source 64
    target 1819
  ]
  edge [
    source 64
    target 1820
  ]
  edge [
    source 64
    target 1821
  ]
  edge [
    source 64
    target 1822
  ]
  edge [
    source 64
    target 1823
  ]
  edge [
    source 64
    target 1824
  ]
  edge [
    source 64
    target 476
  ]
  edge [
    source 64
    target 1825
  ]
  edge [
    source 64
    target 1826
  ]
  edge [
    source 64
    target 407
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 2940
  ]
  edge [
    source 65
    target 2941
  ]
  edge [
    source 65
    target 2942
  ]
  edge [
    source 65
    target 2943
  ]
  edge [
    source 65
    target 2944
  ]
  edge [
    source 65
    target 2945
  ]
  edge [
    source 65
    target 2946
  ]
  edge [
    source 65
    target 2441
  ]
  edge [
    source 65
    target 2947
  ]
  edge [
    source 65
    target 2948
  ]
  edge [
    source 65
    target 2949
  ]
  edge [
    source 65
    target 2950
  ]
  edge [
    source 65
    target 1878
  ]
  edge [
    source 65
    target 2514
  ]
  edge [
    source 65
    target 2951
  ]
  edge [
    source 65
    target 2372
  ]
  edge [
    source 65
    target 2952
  ]
  edge [
    source 65
    target 2953
  ]
  edge [
    source 65
    target 1513
  ]
  edge [
    source 65
    target 2954
  ]
  edge [
    source 65
    target 1293
  ]
  edge [
    source 65
    target 1208
  ]
  edge [
    source 65
    target 2955
  ]
  edge [
    source 65
    target 2956
  ]
  edge [
    source 65
    target 2957
  ]
  edge [
    source 65
    target 1296
  ]
  edge [
    source 65
    target 2958
  ]
  edge [
    source 65
    target 1888
  ]
  edge [
    source 65
    target 2959
  ]
  edge [
    source 65
    target 2960
  ]
  edge [
    source 65
    target 2298
  ]
  edge [
    source 65
    target 1198
  ]
  edge [
    source 65
    target 2961
  ]
  edge [
    source 65
    target 2962
  ]
  edge [
    source 65
    target 2963
  ]
  edge [
    source 65
    target 2964
  ]
  edge [
    source 65
    target 2297
  ]
  edge [
    source 65
    target 2965
  ]
  edge [
    source 65
    target 2966
  ]
  edge [
    source 65
    target 2967
  ]
  edge [
    source 65
    target 1300
  ]
  edge [
    source 65
    target 1301
  ]
  edge [
    source 65
    target 2968
  ]
  edge [
    source 65
    target 2969
  ]
  edge [
    source 65
    target 2970
  ]
  edge [
    source 65
    target 2971
  ]
  edge [
    source 65
    target 2972
  ]
  edge [
    source 65
    target 2420
  ]
  edge [
    source 65
    target 2973
  ]
  edge [
    source 65
    target 2974
  ]
  edge [
    source 65
    target 2975
  ]
  edge [
    source 65
    target 2976
  ]
  edge [
    source 65
    target 2435
  ]
  edge [
    source 65
    target 2977
  ]
  edge [
    source 65
    target 2978
  ]
  edge [
    source 65
    target 2979
  ]
  edge [
    source 65
    target 2980
  ]
  edge [
    source 65
    target 2981
  ]
  edge [
    source 65
    target 2982
  ]
  edge [
    source 65
    target 2983
  ]
  edge [
    source 65
    target 2984
  ]
  edge [
    source 65
    target 2985
  ]
  edge [
    source 65
    target 1911
  ]
  edge [
    source 65
    target 2986
  ]
  edge [
    source 65
    target 2987
  ]
  edge [
    source 65
    target 2988
  ]
  edge [
    source 65
    target 2989
  ]
  edge [
    source 65
    target 2990
  ]
  edge [
    source 65
    target 2725
  ]
  edge [
    source 65
    target 2991
  ]
  edge [
    source 65
    target 2992
  ]
  edge [
    source 65
    target 2993
  ]
  edge [
    source 65
    target 2994
  ]
  edge [
    source 65
    target 2657
  ]
  edge [
    source 65
    target 2995
  ]
  edge [
    source 65
    target 2996
  ]
  edge [
    source 65
    target 2997
  ]
  edge [
    source 65
    target 2998
  ]
  edge [
    source 65
    target 2999
  ]
  edge [
    source 65
    target 2692
  ]
  edge [
    source 65
    target 2650
  ]
  edge [
    source 65
    target 3000
  ]
  edge [
    source 65
    target 1322
  ]
  edge [
    source 65
    target 3001
  ]
  edge [
    source 65
    target 3002
  ]
  edge [
    source 65
    target 2724
  ]
  edge [
    source 65
    target 3003
  ]
  edge [
    source 65
    target 3004
  ]
  edge [
    source 65
    target 3005
  ]
  edge [
    source 65
    target 3006
  ]
  edge [
    source 65
    target 2265
  ]
  edge [
    source 65
    target 1889
  ]
  edge [
    source 65
    target 2712
  ]
  edge [
    source 65
    target 3007
  ]
  edge [
    source 65
    target 3008
  ]
  edge [
    source 65
    target 1306
  ]
  edge [
    source 65
    target 1509
  ]
  edge [
    source 65
    target 3009
  ]
  edge [
    source 65
    target 3010
  ]
  edge [
    source 65
    target 3011
  ]
  edge [
    source 65
    target 3012
  ]
  edge [
    source 65
    target 3013
  ]
  edge [
    source 65
    target 3014
  ]
  edge [
    source 65
    target 2090
  ]
  edge [
    source 65
    target 2501
  ]
  edge [
    source 65
    target 3015
  ]
  edge [
    source 65
    target 3016
  ]
  edge [
    source 65
    target 1612
  ]
  edge [
    source 65
    target 3017
  ]
  edge [
    source 65
    target 1899
  ]
  edge [
    source 65
    target 3018
  ]
  edge [
    source 65
    target 3019
  ]
  edge [
    source 65
    target 2704
  ]
  edge [
    source 65
    target 3020
  ]
  edge [
    source 65
    target 3021
  ]
  edge [
    source 65
    target 2417
  ]
  edge [
    source 65
    target 3022
  ]
  edge [
    source 65
    target 3023
  ]
  edge [
    source 65
    target 3024
  ]
  edge [
    source 65
    target 1913
  ]
  edge [
    source 65
    target 3025
  ]
  edge [
    source 65
    target 3026
  ]
  edge [
    source 65
    target 3027
  ]
  edge [
    source 65
    target 3028
  ]
  edge [
    source 65
    target 3029
  ]
  edge [
    source 65
    target 3030
  ]
  edge [
    source 65
    target 3031
  ]
  edge [
    source 65
    target 2733
  ]
  edge [
    source 65
    target 1438
  ]
  edge [
    source 65
    target 3032
  ]
  edge [
    source 65
    target 3033
  ]
  edge [
    source 65
    target 2699
  ]
  edge [
    source 65
    target 3034
  ]
  edge [
    source 65
    target 3035
  ]
  edge [
    source 65
    target 3036
  ]
  edge [
    source 65
    target 3037
  ]
  edge [
    source 65
    target 1912
  ]
  edge [
    source 65
    target 1611
  ]
  edge [
    source 65
    target 1914
  ]
  edge [
    source 65
    target 1915
  ]
  edge [
    source 65
    target 3038
  ]
  edge [
    source 65
    target 3039
  ]
  edge [
    source 65
    target 3040
  ]
  edge [
    source 65
    target 3041
  ]
  edge [
    source 65
    target 3042
  ]
  edge [
    source 65
    target 3043
  ]
  edge [
    source 65
    target 419
  ]
  edge [
    source 65
    target 3044
  ]
  edge [
    source 65
    target 3045
  ]
  edge [
    source 65
    target 3046
  ]
  edge [
    source 65
    target 391
  ]
  edge [
    source 65
    target 372
  ]
  edge [
    source 65
    target 1098
  ]
  edge [
    source 65
    target 1434
  ]
  edge [
    source 65
    target 426
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 67
    target 3047
  ]
  edge [
    source 67
    target 1925
  ]
  edge [
    source 67
    target 103
  ]
  edge [
    source 67
    target 3048
  ]
  edge [
    source 67
    target 3049
  ]
  edge [
    source 67
    target 3050
  ]
  edge [
    source 67
    target 3051
  ]
  edge [
    source 67
    target 418
  ]
  edge [
    source 67
    target 1958
  ]
  edge [
    source 67
    target 1959
  ]
  edge [
    source 67
    target 1960
  ]
  edge [
    source 67
    target 1961
  ]
  edge [
    source 67
    target 427
  ]
  edge [
    source 67
    target 1962
  ]
  edge [
    source 67
    target 1963
  ]
  edge [
    source 67
    target 1964
  ]
  edge [
    source 67
    target 1965
  ]
  edge [
    source 67
    target 1966
  ]
  edge [
    source 67
    target 1967
  ]
  edge [
    source 67
    target 1968
  ]
  edge [
    source 67
    target 1969
  ]
  edge [
    source 67
    target 1970
  ]
  edge [
    source 67
    target 1971
  ]
  edge [
    source 67
    target 1972
  ]
  edge [
    source 67
    target 1973
  ]
  edge [
    source 67
    target 1974
  ]
  edge [
    source 67
    target 1975
  ]
  edge [
    source 67
    target 1976
  ]
  edge [
    source 67
    target 1977
  ]
  edge [
    source 67
    target 1399
  ]
  edge [
    source 67
    target 1978
  ]
  edge [
    source 67
    target 1594
  ]
  edge [
    source 67
    target 74
  ]
  edge [
    source 67
    target 1101
  ]
  edge [
    source 67
    target 3052
  ]
  edge [
    source 67
    target 3053
  ]
  edge [
    source 67
    target 3054
  ]
  edge [
    source 67
    target 3055
  ]
  edge [
    source 67
    target 3056
  ]
  edge [
    source 67
    target 3057
  ]
  edge [
    source 67
    target 404
  ]
  edge [
    source 67
    target 289
  ]
  edge [
    source 67
    target 1088
  ]
  edge [
    source 67
    target 3058
  ]
  edge [
    source 67
    target 3059
  ]
  edge [
    source 67
    target 3060
  ]
  edge [
    source 67
    target 3061
  ]
  edge [
    source 67
    target 3062
  ]
  edge [
    source 67
    target 3063
  ]
  edge [
    source 67
    target 3064
  ]
  edge [
    source 67
    target 3065
  ]
  edge [
    source 67
    target 3066
  ]
  edge [
    source 67
    target 3014
  ]
  edge [
    source 67
    target 1505
  ]
  edge [
    source 67
    target 3067
  ]
  edge [
    source 67
    target 3068
  ]
  edge [
    source 67
    target 1465
  ]
  edge [
    source 67
    target 3069
  ]
  edge [
    source 67
    target 1833
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 2733
  ]
  edge [
    source 68
    target 3070
  ]
  edge [
    source 68
    target 2722
  ]
  edge [
    source 68
    target 3071
  ]
  edge [
    source 68
    target 1877
  ]
  edge [
    source 68
    target 1305
  ]
  edge [
    source 68
    target 1152
  ]
  edge [
    source 68
    target 1293
  ]
  edge [
    source 68
    target 1294
  ]
  edge [
    source 68
    target 1295
  ]
  edge [
    source 68
    target 1296
  ]
  edge [
    source 68
    target 1297
  ]
  edge [
    source 68
    target 1298
  ]
  edge [
    source 68
    target 1299
  ]
  edge [
    source 68
    target 1300
  ]
  edge [
    source 68
    target 1301
  ]
  edge [
    source 68
    target 1302
  ]
  edge [
    source 68
    target 1303
  ]
  edge [
    source 68
    target 1304
  ]
  edge [
    source 68
    target 1209
  ]
  edge [
    source 68
    target 1306
  ]
  edge [
    source 68
    target 1332
  ]
  edge [
    source 68
    target 3072
  ]
  edge [
    source 68
    target 2683
  ]
  edge [
    source 68
    target 3073
  ]
  edge [
    source 68
    target 3074
  ]
  edge [
    source 68
    target 3075
  ]
  edge [
    source 68
    target 3076
  ]
  edge [
    source 68
    target 3077
  ]
  edge [
    source 68
    target 3078
  ]
  edge [
    source 68
    target 3079
  ]
  edge [
    source 68
    target 2966
  ]
  edge [
    source 68
    target 1765
  ]
  edge [
    source 68
    target 3080
  ]
  edge [
    source 68
    target 3081
  ]
  edge [
    source 68
    target 1443
  ]
  edge [
    source 68
    target 2096
  ]
  edge [
    source 68
    target 3082
  ]
  edge [
    source 68
    target 1908
  ]
  edge [
    source 68
    target 3083
  ]
  edge [
    source 68
    target 1911
  ]
  edge [
    source 68
    target 3037
  ]
  edge [
    source 68
    target 3084
  ]
  edge [
    source 68
    target 3085
  ]
  edge [
    source 68
    target 2441
  ]
  edge [
    source 68
    target 3004
  ]
  edge [
    source 68
    target 3086
  ]
  edge [
    source 68
    target 3087
  ]
  edge [
    source 68
    target 2723
  ]
  edge [
    source 68
    target 2725
  ]
  edge [
    source 68
    target 2942
  ]
  edge [
    source 68
    target 2727
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 3088
  ]
  edge [
    source 69
    target 3089
  ]
  edge [
    source 69
    target 2344
  ]
  edge [
    source 69
    target 3090
  ]
  edge [
    source 69
    target 3091
  ]
  edge [
    source 69
    target 3092
  ]
  edge [
    source 69
    target 3093
  ]
  edge [
    source 69
    target 3094
  ]
  edge [
    source 69
    target 3095
  ]
  edge [
    source 69
    target 3096
  ]
  edge [
    source 69
    target 1070
  ]
  edge [
    source 69
    target 476
  ]
  edge [
    source 69
    target 3097
  ]
  edge [
    source 69
    target 3098
  ]
  edge [
    source 69
    target 3099
  ]
  edge [
    source 69
    target 3100
  ]
  edge [
    source 69
    target 3101
  ]
  edge [
    source 69
    target 131
  ]
  edge [
    source 69
    target 1959
  ]
  edge [
    source 69
    target 2043
  ]
  edge [
    source 69
    target 3102
  ]
  edge [
    source 69
    target 3103
  ]
  edge [
    source 69
    target 3104
  ]
  edge [
    source 69
    target 3105
  ]
  edge [
    source 69
    target 1963
  ]
  edge [
    source 69
    target 3106
  ]
  edge [
    source 69
    target 1964
  ]
  edge [
    source 69
    target 1965
  ]
  edge [
    source 69
    target 3107
  ]
  edge [
    source 69
    target 3108
  ]
  edge [
    source 69
    target 3109
  ]
  edge [
    source 69
    target 3110
  ]
  edge [
    source 69
    target 3111
  ]
  edge [
    source 69
    target 1969
  ]
  edge [
    source 69
    target 1764
  ]
  edge [
    source 69
    target 3112
  ]
  edge [
    source 69
    target 3113
  ]
  edge [
    source 69
    target 1134
  ]
  edge [
    source 69
    target 3114
  ]
  edge [
    source 69
    target 436
  ]
  edge [
    source 69
    target 1974
  ]
  edge [
    source 69
    target 3115
  ]
  edge [
    source 69
    target 1976
  ]
  edge [
    source 69
    target 1977
  ]
  edge [
    source 69
    target 1978
  ]
  edge [
    source 69
    target 1399
  ]
  edge [
    source 69
    target 1446
  ]
  edge [
    source 69
    target 3116
  ]
  edge [
    source 69
    target 3117
  ]
  edge [
    source 69
    target 1594
  ]
  edge [
    source 69
    target 3118
  ]
  edge [
    source 69
    target 3119
  ]
  edge [
    source 69
    target 3120
  ]
  edge [
    source 69
    target 3121
  ]
  edge [
    source 69
    target 3122
  ]
  edge [
    source 69
    target 3123
  ]
  edge [
    source 69
    target 3124
  ]
  edge [
    source 70
    target 3125
  ]
  edge [
    source 70
    target 3126
  ]
  edge [
    source 70
    target 2895
  ]
  edge [
    source 70
    target 3127
  ]
  edge [
    source 70
    target 3128
  ]
  edge [
    source 70
    target 3129
  ]
  edge [
    source 70
    target 1255
  ]
  edge [
    source 70
    target 3130
  ]
  edge [
    source 70
    target 2799
  ]
  edge [
    source 70
    target 3131
  ]
  edge [
    source 70
    target 1351
  ]
  edge [
    source 70
    target 3132
  ]
  edge [
    source 70
    target 1753
  ]
  edge [
    source 70
    target 3133
  ]
  edge [
    source 70
    target 3134
  ]
  edge [
    source 70
    target 3135
  ]
  edge [
    source 70
    target 1358
  ]
  edge [
    source 70
    target 1120
  ]
  edge [
    source 70
    target 3136
  ]
  edge [
    source 70
    target 3137
  ]
  edge [
    source 70
    target 3138
  ]
  edge [
    source 70
    target 1770
  ]
  edge [
    source 70
    target 3139
  ]
  edge [
    source 70
    target 3140
  ]
  edge [
    source 70
    target 3141
  ]
  edge [
    source 70
    target 1024
  ]
  edge [
    source 70
    target 409
  ]
  edge [
    source 70
    target 3142
  ]
  edge [
    source 70
    target 1119
  ]
  edge [
    source 70
    target 3143
  ]
  edge [
    source 70
    target 3144
  ]
  edge [
    source 70
    target 3145
  ]
  edge [
    source 70
    target 3146
  ]
  edge [
    source 70
    target 3147
  ]
  edge [
    source 70
    target 1380
  ]
  edge [
    source 70
    target 3148
  ]
  edge [
    source 70
    target 464
  ]
  edge [
    source 70
    target 2890
  ]
  edge [
    source 70
    target 2888
  ]
  edge [
    source 70
    target 2892
  ]
  edge [
    source 70
    target 2891
  ]
  edge [
    source 70
    target 1100
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 3149
  ]
  edge [
    source 71
    target 3150
  ]
  edge [
    source 71
    target 3151
  ]
  edge [
    source 71
    target 3152
  ]
  edge [
    source 71
    target 3153
  ]
  edge [
    source 71
    target 1192
  ]
  edge [
    source 71
    target 3154
  ]
  edge [
    source 71
    target 124
  ]
  edge [
    source 71
    target 3155
  ]
  edge [
    source 71
    target 3156
  ]
  edge [
    source 71
    target 3157
  ]
  edge [
    source 71
    target 3158
  ]
  edge [
    source 71
    target 3159
  ]
  edge [
    source 71
    target 2240
  ]
  edge [
    source 71
    target 3160
  ]
  edge [
    source 71
    target 3161
  ]
  edge [
    source 71
    target 3162
  ]
  edge [
    source 71
    target 3163
  ]
  edge [
    source 71
    target 3164
  ]
  edge [
    source 71
    target 3165
  ]
  edge [
    source 71
    target 3166
  ]
  edge [
    source 71
    target 3167
  ]
  edge [
    source 71
    target 3168
  ]
  edge [
    source 71
    target 3169
  ]
  edge [
    source 71
    target 3170
  ]
  edge [
    source 71
    target 3171
  ]
  edge [
    source 71
    target 3172
  ]
  edge [
    source 71
    target 3173
  ]
  edge [
    source 71
    target 3174
  ]
  edge [
    source 71
    target 3175
  ]
  edge [
    source 71
    target 3176
  ]
  edge [
    source 71
    target 3177
  ]
  edge [
    source 72
    target 3178
  ]
  edge [
    source 72
    target 3179
  ]
  edge [
    source 72
    target 3180
  ]
  edge [
    source 72
    target 2501
  ]
  edge [
    source 72
    target 3181
  ]
  edge [
    source 72
    target 2581
  ]
  edge [
    source 72
    target 1422
  ]
  edge [
    source 72
    target 3182
  ]
  edge [
    source 72
    target 3183
  ]
  edge [
    source 72
    target 2081
  ]
  edge [
    source 72
    target 3184
  ]
  edge [
    source 72
    target 3185
  ]
  edge [
    source 216
    target 3198
  ]
  edge [
    source 217
    target 3201
  ]
  edge [
    source 217
    target 3197
  ]
  edge [
    source 289
    target 3189
  ]
  edge [
    source 289
    target 3190
  ]
  edge [
    source 289
    target 3191
  ]
  edge [
    source 289
    target 3192
  ]
  edge [
    source 388
    target 3193
  ]
  edge [
    source 388
    target 3187
  ]
  edge [
    source 388
    target 3194
  ]
  edge [
    source 3186
    target 3187
  ]
  edge [
    source 3186
    target 3188
  ]
  edge [
    source 3187
    target 3188
  ]
  edge [
    source 3187
    target 3193
  ]
  edge [
    source 3187
    target 3194
  ]
  edge [
    source 3189
    target 3190
  ]
  edge [
    source 3189
    target 3191
  ]
  edge [
    source 3189
    target 3192
  ]
  edge [
    source 3189
    target 3197
  ]
  edge [
    source 3189
    target 3198
  ]
  edge [
    source 3190
    target 3191
  ]
  edge [
    source 3190
    target 3192
  ]
  edge [
    source 3191
    target 3192
  ]
  edge [
    source 3193
    target 3194
  ]
  edge [
    source 3195
    target 3196
  ]
  edge [
    source 3197
    target 3198
  ]
  edge [
    source 3197
    target 3201
  ]
  edge [
    source 3199
    target 3200
  ]
]
