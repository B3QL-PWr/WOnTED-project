graph [
  node [
    id 0
    label "antonio"
    origin "text"
  ]
  node [
    id 1
    label "imi&#281;"
    origin "text"
  ]
  node [
    id 2
    label "wielko&#347;&#263;"
  ]
  node [
    id 3
    label "leksem"
  ]
  node [
    id 4
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 5
    label "term"
  ]
  node [
    id 6
    label "wezwanie"
  ]
  node [
    id 7
    label "personalia"
  ]
  node [
    id 8
    label "reputacja"
  ]
  node [
    id 9
    label "imiennictwo"
  ]
  node [
    id 10
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 11
    label "deklinacja"
  ]
  node [
    id 12
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 13
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 14
    label "nazwa_w&#322;asna"
  ]
  node [
    id 15
    label "patron"
  ]
  node [
    id 16
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 17
    label "wykrzyknik"
  ]
  node [
    id 18
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 19
    label "wordnet"
  ]
  node [
    id 20
    label "wypowiedzenie"
  ]
  node [
    id 21
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 22
    label "nag&#322;os"
  ]
  node [
    id 23
    label "morfem"
  ]
  node [
    id 24
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 25
    label "wyg&#322;os"
  ]
  node [
    id 26
    label "s&#322;ownictwo"
  ]
  node [
    id 27
    label "jednostka_leksykalna"
  ]
  node [
    id 28
    label "pole_semantyczne"
  ]
  node [
    id 29
    label "pisanie_si&#281;"
  ]
  node [
    id 30
    label "znaczenie"
  ]
  node [
    id 31
    label "opinia"
  ]
  node [
    id 32
    label "poj&#281;cie"
  ]
  node [
    id 33
    label "perversion"
  ]
  node [
    id 34
    label "fleksja"
  ]
  node [
    id 35
    label "k&#261;t"
  ]
  node [
    id 36
    label "&#347;wi&#281;ty"
  ]
  node [
    id 37
    label "patrycjusz"
  ]
  node [
    id 38
    label "opiekun"
  ]
  node [
    id 39
    label "&#347;w"
  ]
  node [
    id 40
    label "zmar&#322;y"
  ]
  node [
    id 41
    label "&#322;uska"
  ]
  node [
    id 42
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 43
    label "nazwa"
  ]
  node [
    id 44
    label "prawnik"
  ]
  node [
    id 45
    label "nab&#243;j"
  ]
  node [
    id 46
    label "szablon"
  ]
  node [
    id 47
    label "zdolno&#347;&#263;"
  ]
  node [
    id 48
    label "liczba"
  ]
  node [
    id 49
    label "zaleta"
  ]
  node [
    id 50
    label "property"
  ]
  node [
    id 51
    label "dymensja"
  ]
  node [
    id 52
    label "measure"
  ]
  node [
    id 53
    label "cecha"
  ]
  node [
    id 54
    label "rozmiar"
  ]
  node [
    id 55
    label "ilo&#347;&#263;"
  ]
  node [
    id 56
    label "potencja"
  ]
  node [
    id 57
    label "rzadko&#347;&#263;"
  ]
  node [
    id 58
    label "warunek_lokalowy"
  ]
  node [
    id 59
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 60
    label "bid"
  ]
  node [
    id 61
    label "pismo_urz&#281;dowe"
  ]
  node [
    id 62
    label "nakaz"
  ]
  node [
    id 63
    label "apostrofa"
  ]
  node [
    id 64
    label "poinformowanie"
  ]
  node [
    id 65
    label "admonition"
  ]
  node [
    id 66
    label "zwo&#322;anie_si&#281;"
  ]
  node [
    id 67
    label "pro&#347;ba"
  ]
  node [
    id 68
    label "zach&#281;cenie"
  ]
  node [
    id 69
    label "nakazanie"
  ]
  node [
    id 70
    label "poproszenie"
  ]
  node [
    id 71
    label "summons"
  ]
  node [
    id 72
    label "wst&#281;p"
  ]
  node [
    id 73
    label "ideonimia"
  ]
  node [
    id 74
    label "zas&#243;b"
  ]
  node [
    id 75
    label "fitonimia"
  ]
  node [
    id 76
    label "chrematonimia"
  ]
  node [
    id 77
    label "leksykologia"
  ]
  node [
    id 78
    label "etnonimia"
  ]
  node [
    id 79
    label "toponimia"
  ]
  node [
    id 80
    label "antroponimia"
  ]
  node [
    id 81
    label "NN"
  ]
  node [
    id 82
    label "pesel"
  ]
  node [
    id 83
    label "nazwisko"
  ]
  node [
    id 84
    label "to&#380;samo&#347;&#263;_osobista"
  ]
  node [
    id 85
    label "adres"
  ]
  node [
    id 86
    label "dane"
  ]
  node [
    id 87
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 88
    label "katolicki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 87
    target 88
  ]
]
