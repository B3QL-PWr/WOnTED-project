graph [
  node [
    id 0
    label "maj"
    origin "text"
  ]
  node [
    id 1
    label "jeszcze"
    origin "text"
  ]
  node [
    id 2
    label "nigdy"
    origin "text"
  ]
  node [
    id 3
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "dentysta"
    origin "text"
  ]
  node [
    id 5
    label "tata"
    origin "text"
  ]
  node [
    id 6
    label "wojtek"
    origin "text"
  ]
  node [
    id 7
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 10
    label "&#380;eby"
    origin "text"
  ]
  node [
    id 11
    label "przekona&#263;"
    origin "text"
  ]
  node [
    id 12
    label "si&#281;"
    origin "text"
  ]
  node [
    id 13
    label "chodzenie"
    origin "text"
  ]
  node [
    id 14
    label "nic"
    origin "text"
  ]
  node [
    id 15
    label "straszny"
    origin "text"
  ]
  node [
    id 16
    label "zarejestrowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "przychodnia"
    origin "text"
  ]
  node [
    id 18
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 19
    label "te&#380;"
    origin "text"
  ]
  node [
    id 20
    label "by&#263;"
    origin "text"
  ]
  node [
    id 21
    label "pacjent"
    origin "text"
  ]
  node [
    id 22
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 23
    label "miesi&#261;c"
  ]
  node [
    id 24
    label "tydzie&#324;"
  ]
  node [
    id 25
    label "miech"
  ]
  node [
    id 26
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 27
    label "czas"
  ]
  node [
    id 28
    label "rok"
  ]
  node [
    id 29
    label "kalendy"
  ]
  node [
    id 30
    label "ci&#261;gle"
  ]
  node [
    id 31
    label "stale"
  ]
  node [
    id 32
    label "ci&#261;g&#322;y"
  ]
  node [
    id 33
    label "nieprzerwanie"
  ]
  node [
    id 34
    label "kompletnie"
  ]
  node [
    id 35
    label "kompletny"
  ]
  node [
    id 36
    label "zupe&#322;nie"
  ]
  node [
    id 37
    label "partnerka"
  ]
  node [
    id 38
    label "cz&#322;owiek"
  ]
  node [
    id 39
    label "aktorka"
  ]
  node [
    id 40
    label "kobieta"
  ]
  node [
    id 41
    label "partner"
  ]
  node [
    id 42
    label "kobita"
  ]
  node [
    id 43
    label "lekarz"
  ]
  node [
    id 44
    label "borowanie"
  ]
  node [
    id 45
    label "borowa&#263;"
  ]
  node [
    id 46
    label "wyrwiz&#261;b"
  ]
  node [
    id 47
    label "Mesmer"
  ]
  node [
    id 48
    label "pracownik"
  ]
  node [
    id 49
    label "Galen"
  ]
  node [
    id 50
    label "zbada&#263;"
  ]
  node [
    id 51
    label "medyk"
  ]
  node [
    id 52
    label "eskulap"
  ]
  node [
    id 53
    label "lekarze"
  ]
  node [
    id 54
    label "Hipokrates"
  ]
  node [
    id 55
    label "dokt&#243;r"
  ]
  node [
    id 56
    label "wiertarka"
  ]
  node [
    id 57
    label "przewiercanie"
  ]
  node [
    id 58
    label "leczenie"
  ]
  node [
    id 59
    label "robienie"
  ]
  node [
    id 60
    label "przewiercenie"
  ]
  node [
    id 61
    label "z&#261;b"
  ]
  node [
    id 62
    label "czyszczenie"
  ]
  node [
    id 63
    label "wwiercanie_si&#281;"
  ]
  node [
    id 64
    label "exercise"
  ]
  node [
    id 65
    label "otw&#243;r"
  ]
  node [
    id 66
    label "wwiercenie_si&#281;"
  ]
  node [
    id 67
    label "leczy&#263;"
  ]
  node [
    id 68
    label "robi&#263;"
  ]
  node [
    id 69
    label "oczyszcza&#263;"
  ]
  node [
    id 70
    label "drill"
  ]
  node [
    id 71
    label "tug"
  ]
  node [
    id 72
    label "kuwada"
  ]
  node [
    id 73
    label "ojciec"
  ]
  node [
    id 74
    label "rodzice"
  ]
  node [
    id 75
    label "ojczym"
  ]
  node [
    id 76
    label "rodzic"
  ]
  node [
    id 77
    label "przodek"
  ]
  node [
    id 78
    label "papa"
  ]
  node [
    id 79
    label "stary"
  ]
  node [
    id 80
    label "starzy"
  ]
  node [
    id 81
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 82
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 83
    label "pokolenie"
  ]
  node [
    id 84
    label "wapniaki"
  ]
  node [
    id 85
    label "opiekun"
  ]
  node [
    id 86
    label "wapniak"
  ]
  node [
    id 87
    label "rodzic_chrzestny"
  ]
  node [
    id 88
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 89
    label "ojcowie"
  ]
  node [
    id 90
    label "linea&#380;"
  ]
  node [
    id 91
    label "krewny"
  ]
  node [
    id 92
    label "chodnik"
  ]
  node [
    id 93
    label "w&#243;z"
  ]
  node [
    id 94
    label "p&#322;ug"
  ]
  node [
    id 95
    label "wyrobisko"
  ]
  node [
    id 96
    label "dziad"
  ]
  node [
    id 97
    label "antecesor"
  ]
  node [
    id 98
    label "post&#281;p"
  ]
  node [
    id 99
    label "kszta&#322;ciciel"
  ]
  node [
    id 100
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 101
    label "tworzyciel"
  ]
  node [
    id 102
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 103
    label "&#347;w"
  ]
  node [
    id 104
    label "pomys&#322;odawca"
  ]
  node [
    id 105
    label "wykonawca"
  ]
  node [
    id 106
    label "samiec"
  ]
  node [
    id 107
    label "zakonnik"
  ]
  node [
    id 108
    label "materia&#322;_budowlany"
  ]
  node [
    id 109
    label "twarz"
  ]
  node [
    id 110
    label "gun_muzzle"
  ]
  node [
    id 111
    label "izolacja"
  ]
  node [
    id 112
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 113
    label "nienowoczesny"
  ]
  node [
    id 114
    label "gruba_ryba"
  ]
  node [
    id 115
    label "zestarzenie_si&#281;"
  ]
  node [
    id 116
    label "poprzedni"
  ]
  node [
    id 117
    label "dawno"
  ]
  node [
    id 118
    label "staro"
  ]
  node [
    id 119
    label "m&#261;&#380;"
  ]
  node [
    id 120
    label "dotychczasowy"
  ]
  node [
    id 121
    label "p&#243;&#378;ny"
  ]
  node [
    id 122
    label "d&#322;ugoletni"
  ]
  node [
    id 123
    label "charakterystyczny"
  ]
  node [
    id 124
    label "brat"
  ]
  node [
    id 125
    label "po_staro&#347;wiecku"
  ]
  node [
    id 126
    label "zwierzchnik"
  ]
  node [
    id 127
    label "znajomy"
  ]
  node [
    id 128
    label "odleg&#322;y"
  ]
  node [
    id 129
    label "starzenie_si&#281;"
  ]
  node [
    id 130
    label "starczo"
  ]
  node [
    id 131
    label "dawniej"
  ]
  node [
    id 132
    label "niegdysiejszy"
  ]
  node [
    id 133
    label "dojrza&#322;y"
  ]
  node [
    id 134
    label "syndrom_kuwady"
  ]
  node [
    id 135
    label "na&#347;ladownictwo"
  ]
  node [
    id 136
    label "zwyczaj"
  ]
  node [
    id 137
    label "ci&#261;&#380;a"
  ]
  node [
    id 138
    label "&#380;onaty"
  ]
  node [
    id 139
    label "podj&#261;&#263;"
  ]
  node [
    id 140
    label "sta&#263;_si&#281;"
  ]
  node [
    id 141
    label "determine"
  ]
  node [
    id 142
    label "zrobi&#263;"
  ]
  node [
    id 143
    label "zareagowa&#263;"
  ]
  node [
    id 144
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 145
    label "draw"
  ]
  node [
    id 146
    label "allude"
  ]
  node [
    id 147
    label "zmieni&#263;"
  ]
  node [
    id 148
    label "zacz&#261;&#263;"
  ]
  node [
    id 149
    label "raise"
  ]
  node [
    id 150
    label "post&#261;pi&#263;"
  ]
  node [
    id 151
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 152
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 153
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 154
    label "zorganizowa&#263;"
  ]
  node [
    id 155
    label "appoint"
  ]
  node [
    id 156
    label "wystylizowa&#263;"
  ]
  node [
    id 157
    label "cause"
  ]
  node [
    id 158
    label "przerobi&#263;"
  ]
  node [
    id 159
    label "nabra&#263;"
  ]
  node [
    id 160
    label "make"
  ]
  node [
    id 161
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 162
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 163
    label "wydali&#263;"
  ]
  node [
    id 164
    label "sail"
  ]
  node [
    id 165
    label "leave"
  ]
  node [
    id 166
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 167
    label "travel"
  ]
  node [
    id 168
    label "proceed"
  ]
  node [
    id 169
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 170
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 171
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 172
    label "zosta&#263;"
  ]
  node [
    id 173
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 174
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 175
    label "przyj&#261;&#263;"
  ]
  node [
    id 176
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 177
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 178
    label "uda&#263;_si&#281;"
  ]
  node [
    id 179
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 180
    label "play_along"
  ]
  node [
    id 181
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 182
    label "opu&#347;ci&#263;"
  ]
  node [
    id 183
    label "become"
  ]
  node [
    id 184
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 185
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 186
    label "odj&#261;&#263;"
  ]
  node [
    id 187
    label "introduce"
  ]
  node [
    id 188
    label "begin"
  ]
  node [
    id 189
    label "do"
  ]
  node [
    id 190
    label "przybra&#263;"
  ]
  node [
    id 191
    label "strike"
  ]
  node [
    id 192
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 193
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 194
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 195
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 196
    label "receive"
  ]
  node [
    id 197
    label "obra&#263;"
  ]
  node [
    id 198
    label "uzna&#263;"
  ]
  node [
    id 199
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 200
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 201
    label "przyj&#281;cie"
  ]
  node [
    id 202
    label "fall"
  ]
  node [
    id 203
    label "swallow"
  ]
  node [
    id 204
    label "odebra&#263;"
  ]
  node [
    id 205
    label "dostarczy&#263;"
  ]
  node [
    id 206
    label "umie&#347;ci&#263;"
  ]
  node [
    id 207
    label "wzi&#261;&#263;"
  ]
  node [
    id 208
    label "absorb"
  ]
  node [
    id 209
    label "undertake"
  ]
  node [
    id 210
    label "sprawi&#263;"
  ]
  node [
    id 211
    label "change"
  ]
  node [
    id 212
    label "zast&#261;pi&#263;"
  ]
  node [
    id 213
    label "come_up"
  ]
  node [
    id 214
    label "przej&#347;&#263;"
  ]
  node [
    id 215
    label "straci&#263;"
  ]
  node [
    id 216
    label "zyska&#263;"
  ]
  node [
    id 217
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 218
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 219
    label "osta&#263;_si&#281;"
  ]
  node [
    id 220
    label "pozosta&#263;"
  ]
  node [
    id 221
    label "catch"
  ]
  node [
    id 222
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 223
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 224
    label "pozostawi&#263;"
  ]
  node [
    id 225
    label "obni&#380;y&#263;"
  ]
  node [
    id 226
    label "zostawi&#263;"
  ]
  node [
    id 227
    label "przesta&#263;"
  ]
  node [
    id 228
    label "potani&#263;"
  ]
  node [
    id 229
    label "drop"
  ]
  node [
    id 230
    label "evacuate"
  ]
  node [
    id 231
    label "humiliate"
  ]
  node [
    id 232
    label "tekst"
  ]
  node [
    id 233
    label "authorize"
  ]
  node [
    id 234
    label "omin&#261;&#263;"
  ]
  node [
    id 235
    label "loom"
  ]
  node [
    id 236
    label "result"
  ]
  node [
    id 237
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 238
    label "wybi&#263;_si&#281;"
  ]
  node [
    id 239
    label "wyj&#347;&#263;_na_jaw"
  ]
  node [
    id 240
    label "appear"
  ]
  node [
    id 241
    label "zgin&#261;&#263;"
  ]
  node [
    id 242
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 243
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 244
    label "rise"
  ]
  node [
    id 245
    label "dziewka"
  ]
  node [
    id 246
    label "sikorka"
  ]
  node [
    id 247
    label "kora"
  ]
  node [
    id 248
    label "dziewcz&#281;"
  ]
  node [
    id 249
    label "dziewoja"
  ]
  node [
    id 250
    label "m&#322;&#243;dka"
  ]
  node [
    id 251
    label "dziecina"
  ]
  node [
    id 252
    label "dziecko"
  ]
  node [
    id 253
    label "dziunia"
  ]
  node [
    id 254
    label "dziewczynina"
  ]
  node [
    id 255
    label "siksa"
  ]
  node [
    id 256
    label "potomkini"
  ]
  node [
    id 257
    label "utulenie"
  ]
  node [
    id 258
    label "pediatra"
  ]
  node [
    id 259
    label "dzieciak"
  ]
  node [
    id 260
    label "utulanie"
  ]
  node [
    id 261
    label "dzieciarnia"
  ]
  node [
    id 262
    label "niepe&#322;noletni"
  ]
  node [
    id 263
    label "organizm"
  ]
  node [
    id 264
    label "utula&#263;"
  ]
  node [
    id 265
    label "cz&#322;owieczek"
  ]
  node [
    id 266
    label "fledgling"
  ]
  node [
    id 267
    label "zwierz&#281;"
  ]
  node [
    id 268
    label "utuli&#263;"
  ]
  node [
    id 269
    label "m&#322;odzik"
  ]
  node [
    id 270
    label "pedofil"
  ]
  node [
    id 271
    label "m&#322;odziak"
  ]
  node [
    id 272
    label "potomek"
  ]
  node [
    id 273
    label "entliczek-pentliczek"
  ]
  node [
    id 274
    label "potomstwo"
  ]
  node [
    id 275
    label "sraluch"
  ]
  node [
    id 276
    label "krewna"
  ]
  node [
    id 277
    label "ludzko&#347;&#263;"
  ]
  node [
    id 278
    label "asymilowanie"
  ]
  node [
    id 279
    label "asymilowa&#263;"
  ]
  node [
    id 280
    label "os&#322;abia&#263;"
  ]
  node [
    id 281
    label "posta&#263;"
  ]
  node [
    id 282
    label "hominid"
  ]
  node [
    id 283
    label "podw&#322;adny"
  ]
  node [
    id 284
    label "os&#322;abianie"
  ]
  node [
    id 285
    label "g&#322;owa"
  ]
  node [
    id 286
    label "figura"
  ]
  node [
    id 287
    label "portrecista"
  ]
  node [
    id 288
    label "dwun&#243;g"
  ]
  node [
    id 289
    label "profanum"
  ]
  node [
    id 290
    label "mikrokosmos"
  ]
  node [
    id 291
    label "nasada"
  ]
  node [
    id 292
    label "duch"
  ]
  node [
    id 293
    label "antropochoria"
  ]
  node [
    id 294
    label "osoba"
  ]
  node [
    id 295
    label "wz&#243;r"
  ]
  node [
    id 296
    label "senior"
  ]
  node [
    id 297
    label "oddzia&#322;ywanie"
  ]
  node [
    id 298
    label "Adam"
  ]
  node [
    id 299
    label "homo_sapiens"
  ]
  node [
    id 300
    label "polifag"
  ]
  node [
    id 301
    label "laska"
  ]
  node [
    id 302
    label "dziewczyna"
  ]
  node [
    id 303
    label "sikora"
  ]
  node [
    id 304
    label "panna"
  ]
  node [
    id 305
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 306
    label "prostytutka"
  ]
  node [
    id 307
    label "ma&#322;olata"
  ]
  node [
    id 308
    label "crust"
  ]
  node [
    id 309
    label "ciasto"
  ]
  node [
    id 310
    label "szabla"
  ]
  node [
    id 311
    label "drzewko"
  ]
  node [
    id 312
    label "drzewo"
  ]
  node [
    id 313
    label "po&#347;ciel&#243;wka"
  ]
  node [
    id 314
    label "harfa"
  ]
  node [
    id 315
    label "bawe&#322;na"
  ]
  node [
    id 316
    label "tkanka_sta&#322;a"
  ]
  node [
    id 317
    label "piskl&#281;"
  ]
  node [
    id 318
    label "samica"
  ]
  node [
    id 319
    label "ptak"
  ]
  node [
    id 320
    label "upierzenie"
  ]
  node [
    id 321
    label "m&#322;odzie&#380;"
  ]
  node [
    id 322
    label "mo&#322;odyca"
  ]
  node [
    id 323
    label "zwrot"
  ]
  node [
    id 324
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 325
    label "get"
  ]
  node [
    id 326
    label "nak&#322;oni&#263;"
  ]
  node [
    id 327
    label "work"
  ]
  node [
    id 328
    label "chemia"
  ]
  node [
    id 329
    label "spowodowa&#263;"
  ]
  node [
    id 330
    label "reakcja_chemiczna"
  ]
  node [
    id 331
    label "act"
  ]
  node [
    id 332
    label "zach&#281;ci&#263;"
  ]
  node [
    id 333
    label "sk&#322;oni&#263;"
  ]
  node [
    id 334
    label "zwerbowa&#263;"
  ]
  node [
    id 335
    label "nacisn&#261;&#263;"
  ]
  node [
    id 336
    label "dochodzenie"
  ]
  node [
    id 337
    label "uganianie_si&#281;"
  ]
  node [
    id 338
    label "ko&#322;atanie_si&#281;"
  ]
  node [
    id 339
    label "rozdeptywanie"
  ]
  node [
    id 340
    label "donoszenie"
  ]
  node [
    id 341
    label "obchodzenie"
  ]
  node [
    id 342
    label "staranie_si&#281;"
  ]
  node [
    id 343
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 344
    label "uruchamianie"
  ]
  node [
    id 345
    label "znoszenie"
  ]
  node [
    id 346
    label "zdeptanie"
  ]
  node [
    id 347
    label "deptanie"
  ]
  node [
    id 348
    label "rozdeptanie"
  ]
  node [
    id 349
    label "ubieranie_si&#281;"
  ]
  node [
    id 350
    label "wydeptywanie"
  ]
  node [
    id 351
    label "uruchomienie"
  ]
  node [
    id 352
    label "nakr&#281;canie"
  ]
  node [
    id 353
    label "wydeptanie"
  ]
  node [
    id 354
    label "noszenie"
  ]
  node [
    id 355
    label "przenoszenie"
  ]
  node [
    id 356
    label "bywanie"
  ]
  node [
    id 357
    label "tr&#243;jstronny"
  ]
  node [
    id 358
    label "uchodzenie_si&#281;"
  ]
  node [
    id 359
    label "nakr&#281;cenie"
  ]
  node [
    id 360
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 361
    label "zatrzymanie"
  ]
  node [
    id 362
    label "wear"
  ]
  node [
    id 363
    label "donaszanie"
  ]
  node [
    id 364
    label "podtrzymywanie"
  ]
  node [
    id 365
    label "w&#322;&#261;czanie"
  ]
  node [
    id 366
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 367
    label "zmierzanie"
  ]
  node [
    id 368
    label "ubieranie"
  ]
  node [
    id 369
    label "dzianie_si&#281;"
  ]
  node [
    id 370
    label "jitter"
  ]
  node [
    id 371
    label "pochodzenie"
  ]
  node [
    id 372
    label "ucz&#281;szczanie"
  ]
  node [
    id 373
    label "przechodzenie"
  ]
  node [
    id 374
    label "p&#322;ywanie"
  ]
  node [
    id 375
    label "poruszanie_si&#281;"
  ]
  node [
    id 376
    label "funkcja"
  ]
  node [
    id 377
    label "impact"
  ]
  node [
    id 378
    label "w&#322;&#261;czenie"
  ]
  node [
    id 379
    label "overlap"
  ]
  node [
    id 380
    label "pokazywanie_si&#281;"
  ]
  node [
    id 381
    label "czynno&#347;&#263;"
  ]
  node [
    id 382
    label "recourse"
  ]
  node [
    id 383
    label "crawl"
  ]
  node [
    id 384
    label "zadryfowanie"
  ]
  node [
    id 385
    label "zagruntowanie"
  ]
  node [
    id 386
    label "gruntowanie"
  ]
  node [
    id 387
    label "obfitowanie"
  ]
  node [
    id 388
    label "podr&#243;&#380;owanie"
  ]
  node [
    id 389
    label "sport_wodny"
  ]
  node [
    id 390
    label "swimming"
  ]
  node [
    id 391
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 392
    label "unoszenie_si&#281;"
  ]
  node [
    id 393
    label "p&#322;ywactwo"
  ]
  node [
    id 394
    label "m&#243;wienie"
  ]
  node [
    id 395
    label "pracowanie"
  ]
  node [
    id 396
    label "pop&#322;ywanie"
  ]
  node [
    id 397
    label "czyn"
  ]
  node [
    id 398
    label "supremum"
  ]
  node [
    id 399
    label "addytywno&#347;&#263;"
  ]
  node [
    id 400
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 401
    label "jednostka"
  ]
  node [
    id 402
    label "function"
  ]
  node [
    id 403
    label "zastosowanie"
  ]
  node [
    id 404
    label "matematyka"
  ]
  node [
    id 405
    label "funkcjonowanie"
  ]
  node [
    id 406
    label "praca"
  ]
  node [
    id 407
    label "rzut"
  ]
  node [
    id 408
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 409
    label "powierzanie"
  ]
  node [
    id 410
    label "cel"
  ]
  node [
    id 411
    label "dziedzina"
  ]
  node [
    id 412
    label "przeciwdziedzina"
  ]
  node [
    id 413
    label "awansowa&#263;"
  ]
  node [
    id 414
    label "stawia&#263;"
  ]
  node [
    id 415
    label "wakowa&#263;"
  ]
  node [
    id 416
    label "znaczenie"
  ]
  node [
    id 417
    label "postawi&#263;"
  ]
  node [
    id 418
    label "awansowanie"
  ]
  node [
    id 419
    label "infimum"
  ]
  node [
    id 420
    label "widzenie"
  ]
  node [
    id 421
    label "wy&#322;&#261;czanie"
  ]
  node [
    id 422
    label "incorporation"
  ]
  node [
    id 423
    label "attachment"
  ]
  node [
    id 424
    label "zaczynanie"
  ]
  node [
    id 425
    label "nastawianie"
  ]
  node [
    id 426
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 427
    label "zapalanie"
  ]
  node [
    id 428
    label "inclusion"
  ]
  node [
    id 429
    label "przes&#322;uchiwanie"
  ]
  node [
    id 430
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 431
    label "uczestniczenie"
  ]
  node [
    id 432
    label "powodowanie"
  ]
  node [
    id 433
    label "przefiltrowanie"
  ]
  node [
    id 434
    label "zamkni&#281;cie"
  ]
  node [
    id 435
    label "career"
  ]
  node [
    id 436
    label "zaaresztowanie"
  ]
  node [
    id 437
    label "przechowanie"
  ]
  node [
    id 438
    label "spowodowanie"
  ]
  node [
    id 439
    label "closure"
  ]
  node [
    id 440
    label "observation"
  ]
  node [
    id 441
    label "uniemo&#380;liwienie"
  ]
  node [
    id 442
    label "pochowanie"
  ]
  node [
    id 443
    label "discontinuance"
  ]
  node [
    id 444
    label "przerwanie"
  ]
  node [
    id 445
    label "zaczepienie"
  ]
  node [
    id 446
    label "pozajmowanie"
  ]
  node [
    id 447
    label "hipostaza"
  ]
  node [
    id 448
    label "capture"
  ]
  node [
    id 449
    label "przetrzymanie"
  ]
  node [
    id 450
    label "oddzia&#322;anie"
  ]
  node [
    id 451
    label "&#322;apanie"
  ]
  node [
    id 452
    label "z&#322;apanie"
  ]
  node [
    id 453
    label "check"
  ]
  node [
    id 454
    label "unieruchomienie"
  ]
  node [
    id 455
    label "zabranie"
  ]
  node [
    id 456
    label "przestanie"
  ]
  node [
    id 457
    label "kapita&#322;"
  ]
  node [
    id 458
    label "zacz&#281;cie"
  ]
  node [
    id 459
    label "propulsion"
  ]
  node [
    id 460
    label "zrobienie"
  ]
  node [
    id 461
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 462
    label "pos&#322;uchanie"
  ]
  node [
    id 463
    label "obejrzenie"
  ]
  node [
    id 464
    label "involvement"
  ]
  node [
    id 465
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 466
    label "za&#347;wiecenie"
  ]
  node [
    id 467
    label "nastawienie"
  ]
  node [
    id 468
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 469
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 470
    label "ukr&#281;cenie"
  ]
  node [
    id 471
    label "gyration"
  ]
  node [
    id 472
    label "ruszanie"
  ]
  node [
    id 473
    label "dokr&#281;cenie"
  ]
  node [
    id 474
    label "kr&#281;cenie"
  ]
  node [
    id 475
    label "zakr&#281;canie"
  ]
  node [
    id 476
    label "tworzenie"
  ]
  node [
    id 477
    label "nagrywanie"
  ]
  node [
    id 478
    label "wind"
  ]
  node [
    id 479
    label "nak&#322;adanie"
  ]
  node [
    id 480
    label "okr&#281;canie"
  ]
  node [
    id 481
    label "wzmaganie"
  ]
  node [
    id 482
    label "wzbudzanie"
  ]
  node [
    id 483
    label "dokr&#281;canie"
  ]
  node [
    id 484
    label "pozawijanie"
  ]
  node [
    id 485
    label "stworzenie"
  ]
  node [
    id 486
    label "nagranie"
  ]
  node [
    id 487
    label "wzbudzenie"
  ]
  node [
    id 488
    label "ruszenie"
  ]
  node [
    id 489
    label "zakr&#281;cenie"
  ]
  node [
    id 490
    label "naniesienie"
  ]
  node [
    id 491
    label "suppression"
  ]
  node [
    id 492
    label "wzmo&#380;enie"
  ]
  node [
    id 493
    label "okr&#281;cenie"
  ]
  node [
    id 494
    label "nak&#322;amanie"
  ]
  node [
    id 495
    label "utrzymywanie"
  ]
  node [
    id 496
    label "obstawanie"
  ]
  node [
    id 497
    label "bycie"
  ]
  node [
    id 498
    label "preservation"
  ]
  node [
    id 499
    label "boost"
  ]
  node [
    id 500
    label "continuance"
  ]
  node [
    id 501
    label "pocieszanie"
  ]
  node [
    id 502
    label "umieszczanie"
  ]
  node [
    id 503
    label "upi&#281;kszanie"
  ]
  node [
    id 504
    label "odziewanie"
  ]
  node [
    id 505
    label "dressing"
  ]
  node [
    id 506
    label "przebieranie"
  ]
  node [
    id 507
    label "k&#322;adzenie"
  ]
  node [
    id 508
    label "pi&#281;kniejszy"
  ]
  node [
    id 509
    label "przymierzanie"
  ]
  node [
    id 510
    label "adornment"
  ]
  node [
    id 511
    label "oblekanie"
  ]
  node [
    id 512
    label "bacteriophage"
  ]
  node [
    id 513
    label "infection"
  ]
  node [
    id 514
    label "zmienianie"
  ]
  node [
    id 515
    label "strzelanie"
  ]
  node [
    id 516
    label "move"
  ]
  node [
    id 517
    label "transportation"
  ]
  node [
    id 518
    label "przesadzanie"
  ]
  node [
    id 519
    label "kopiowanie"
  ]
  node [
    id 520
    label "transmission"
  ]
  node [
    id 521
    label "pocisk"
  ]
  node [
    id 522
    label "dostosowywanie"
  ]
  node [
    id 523
    label "drift"
  ]
  node [
    id 524
    label "przetrwanie"
  ]
  node [
    id 525
    label "przesuwanie_si&#281;"
  ]
  node [
    id 526
    label "przemieszczanie"
  ]
  node [
    id 527
    label "przelatywanie"
  ]
  node [
    id 528
    label "translation"
  ]
  node [
    id 529
    label "ponoszenie"
  ]
  node [
    id 530
    label "rozpowszechnianie"
  ]
  node [
    id 531
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 532
    label "zaczynanie_si&#281;"
  ]
  node [
    id 533
    label "str&#243;j"
  ]
  node [
    id 534
    label "wynikanie"
  ]
  node [
    id 535
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 536
    label "origin"
  ]
  node [
    id 537
    label "background"
  ]
  node [
    id 538
    label "geneza"
  ]
  node [
    id 539
    label "beginning"
  ]
  node [
    id 540
    label "zu&#380;ywanie"
  ]
  node [
    id 541
    label "toleration"
  ]
  node [
    id 542
    label "collection"
  ]
  node [
    id 543
    label "wytrzymywanie"
  ]
  node [
    id 544
    label "take"
  ]
  node [
    id 545
    label "ranny"
  ]
  node [
    id 546
    label "jajko"
  ]
  node [
    id 547
    label "usuwanie"
  ]
  node [
    id 548
    label "porywanie"
  ]
  node [
    id 549
    label "wygrywanie"
  ]
  node [
    id 550
    label "abrogation"
  ]
  node [
    id 551
    label "gromadzenie"
  ]
  node [
    id 552
    label "poddawanie_si&#281;"
  ]
  node [
    id 553
    label "zniszczenie"
  ]
  node [
    id 554
    label "uniewa&#380;nianie"
  ]
  node [
    id 555
    label "rodzenie"
  ]
  node [
    id 556
    label "tolerowanie"
  ]
  node [
    id 557
    label "niszczenie"
  ]
  node [
    id 558
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 559
    label "stand"
  ]
  node [
    id 560
    label "do&#322;&#261;czanie"
  ]
  node [
    id 561
    label "zu&#380;ycie"
  ]
  node [
    id 562
    label "dosi&#281;ganie"
  ]
  node [
    id 563
    label "zanoszenie"
  ]
  node [
    id 564
    label "przebycie"
  ]
  node [
    id 565
    label "sk&#322;adanie"
  ]
  node [
    id 566
    label "informowanie"
  ]
  node [
    id 567
    label "urodzenie"
  ]
  node [
    id 568
    label "naci&#347;ni&#281;cie"
  ]
  node [
    id 569
    label "zniewa&#380;enie"
  ]
  node [
    id 570
    label "rozci&#261;gni&#281;cie"
  ]
  node [
    id 571
    label "rozprowadzenie"
  ]
  node [
    id 572
    label "lu&#378;ny"
  ]
  node [
    id 573
    label "niesienie"
  ]
  node [
    id 574
    label "przepajanie"
  ]
  node [
    id 575
    label "miewanie"
  ]
  node [
    id 576
    label "nazywanie_si&#281;"
  ]
  node [
    id 577
    label "branie"
  ]
  node [
    id 578
    label "bycie_w_posiadaniu"
  ]
  node [
    id 579
    label "enjoyment"
  ]
  node [
    id 580
    label "przej&#347;cie"
  ]
  node [
    id 581
    label "kszta&#322;towanie"
  ]
  node [
    id 582
    label "pozyskiwanie"
  ]
  node [
    id 583
    label "odwiedzanie"
  ]
  node [
    id 584
    label "&#347;wi&#281;towanie"
  ]
  node [
    id 585
    label "okr&#261;&#380;anie"
  ]
  node [
    id 586
    label "oborywanie"
  ]
  node [
    id 587
    label "przychodzenie"
  ]
  node [
    id 588
    label "campaign"
  ]
  node [
    id 589
    label "wyprzedzenie"
  ]
  node [
    id 590
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 591
    label "podchodzenie"
  ]
  node [
    id 592
    label "spotykanie"
  ]
  node [
    id 593
    label "przemierzanie"
  ]
  node [
    id 594
    label "udawanie_si&#281;"
  ]
  node [
    id 595
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 596
    label "przyj&#347;cie"
  ]
  node [
    id 597
    label "post&#281;powanie"
  ]
  node [
    id 598
    label "odchodzenie"
  ]
  node [
    id 599
    label "wyprzedzanie"
  ]
  node [
    id 600
    label "przemierzenie"
  ]
  node [
    id 601
    label "podej&#347;cie"
  ]
  node [
    id 602
    label "wodzenie"
  ]
  node [
    id 603
    label "egress"
  ]
  node [
    id 604
    label "ukszta&#322;towanie"
  ]
  node [
    id 605
    label "skombinowanie"
  ]
  node [
    id 606
    label "zniewa&#380;anie"
  ]
  node [
    id 607
    label "wdeptywanie"
  ]
  node [
    id 608
    label "naruszanie"
  ]
  node [
    id 609
    label "p&#322;odzenie"
  ]
  node [
    id 610
    label "wdeptanie"
  ]
  node [
    id 611
    label "udeptywanie"
  ]
  node [
    id 612
    label "brudzenie"
  ]
  node [
    id 613
    label "tratowanie"
  ]
  node [
    id 614
    label "nast&#281;powanie"
  ]
  node [
    id 615
    label "trample"
  ]
  node [
    id 616
    label "stratowanie"
  ]
  node [
    id 617
    label "pace"
  ]
  node [
    id 618
    label "przemakanie"
  ]
  node [
    id 619
    label "przestawanie"
  ]
  node [
    id 620
    label "nasycanie_si&#281;"
  ]
  node [
    id 621
    label "popychanie"
  ]
  node [
    id 622
    label "dostawanie_si&#281;"
  ]
  node [
    id 623
    label "stawanie_si&#281;"
  ]
  node [
    id 624
    label "przep&#322;ywanie"
  ]
  node [
    id 625
    label "przepuszczanie"
  ]
  node [
    id 626
    label "zaliczanie"
  ]
  node [
    id 627
    label "nas&#261;czanie"
  ]
  node [
    id 628
    label "impregnation"
  ]
  node [
    id 629
    label "uznanie"
  ]
  node [
    id 630
    label "passage"
  ]
  node [
    id 631
    label "trwanie"
  ]
  node [
    id 632
    label "przedostawanie_si&#281;"
  ]
  node [
    id 633
    label "wytyczenie"
  ]
  node [
    id 634
    label "popchni&#281;cie"
  ]
  node [
    id 635
    label "zaznawanie"
  ]
  node [
    id 636
    label "pass"
  ]
  node [
    id 637
    label "nale&#380;enie"
  ]
  node [
    id 638
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 639
    label "potr&#261;canie"
  ]
  node [
    id 640
    label "przebywanie"
  ]
  node [
    id 641
    label "zast&#281;powanie"
  ]
  node [
    id 642
    label "test"
  ]
  node [
    id 643
    label "passing"
  ]
  node [
    id 644
    label "mijanie"
  ]
  node [
    id 645
    label "przerabianie"
  ]
  node [
    id 646
    label "odmienianie"
  ]
  node [
    id 647
    label "rozci&#261;ganie"
  ]
  node [
    id 648
    label "rozprowadzanie"
  ]
  node [
    id 649
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 650
    label "inquest"
  ]
  node [
    id 651
    label "maturation"
  ]
  node [
    id 652
    label "dop&#322;ywanie"
  ]
  node [
    id 653
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 654
    label "inquisition"
  ]
  node [
    id 655
    label "dor&#281;czanie"
  ]
  node [
    id 656
    label "trial"
  ]
  node [
    id 657
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 658
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 659
    label "przesy&#322;ka"
  ]
  node [
    id 660
    label "postrzeganie"
  ]
  node [
    id 661
    label "dodatek"
  ]
  node [
    id 662
    label "wydarzenie"
  ]
  node [
    id 663
    label "assay"
  ]
  node [
    id 664
    label "doje&#380;d&#380;anie"
  ]
  node [
    id 665
    label "dochrapywanie_si&#281;"
  ]
  node [
    id 666
    label "ro&#347;ni&#281;cie"
  ]
  node [
    id 667
    label "Inquisition"
  ]
  node [
    id 668
    label "roszczenie"
  ]
  node [
    id 669
    label "dolatywanie"
  ]
  node [
    id 670
    label "strzelenie"
  ]
  node [
    id 671
    label "orgazm"
  ]
  node [
    id 672
    label "detektyw"
  ]
  node [
    id 673
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 674
    label "doczekanie"
  ]
  node [
    id 675
    label "rozwijanie_si&#281;"
  ]
  node [
    id 676
    label "uzyskiwanie"
  ]
  node [
    id 677
    label "docieranie"
  ]
  node [
    id 678
    label "osi&#261;ganie"
  ]
  node [
    id 679
    label "dop&#322;ata"
  ]
  node [
    id 680
    label "examination"
  ]
  node [
    id 681
    label "ilo&#347;&#263;"
  ]
  node [
    id 682
    label "ciura"
  ]
  node [
    id 683
    label "miernota"
  ]
  node [
    id 684
    label "g&#243;wno"
  ]
  node [
    id 685
    label "love"
  ]
  node [
    id 686
    label "brak"
  ]
  node [
    id 687
    label "nieistnienie"
  ]
  node [
    id 688
    label "odej&#347;cie"
  ]
  node [
    id 689
    label "defect"
  ]
  node [
    id 690
    label "gap"
  ]
  node [
    id 691
    label "odej&#347;&#263;"
  ]
  node [
    id 692
    label "kr&#243;tki"
  ]
  node [
    id 693
    label "wada"
  ]
  node [
    id 694
    label "odchodzi&#263;"
  ]
  node [
    id 695
    label "wyr&#243;b"
  ]
  node [
    id 696
    label "prywatywny"
  ]
  node [
    id 697
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 698
    label "rozmiar"
  ]
  node [
    id 699
    label "part"
  ]
  node [
    id 700
    label "jako&#347;&#263;"
  ]
  node [
    id 701
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 702
    label "tandetno&#347;&#263;"
  ]
  node [
    id 703
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 704
    label "ka&#322;"
  ]
  node [
    id 705
    label "tandeta"
  ]
  node [
    id 706
    label "zero"
  ]
  node [
    id 707
    label "drobiazg"
  ]
  node [
    id 708
    label "chor&#261;&#380;y"
  ]
  node [
    id 709
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 710
    label "niegrzeczny"
  ]
  node [
    id 711
    label "olbrzymi"
  ]
  node [
    id 712
    label "niemoralny"
  ]
  node [
    id 713
    label "kurewski"
  ]
  node [
    id 714
    label "strasznie"
  ]
  node [
    id 715
    label "jebitny"
  ]
  node [
    id 716
    label "olbrzymio"
  ]
  node [
    id 717
    label "ogromnie"
  ]
  node [
    id 718
    label "niezno&#347;ny"
  ]
  node [
    id 719
    label "niegrzecznie"
  ]
  node [
    id 720
    label "trudny"
  ]
  node [
    id 721
    label "niestosowny"
  ]
  node [
    id 722
    label "brzydal"
  ]
  node [
    id 723
    label "niepos&#322;uszny"
  ]
  node [
    id 724
    label "z&#322;y"
  ]
  node [
    id 725
    label "nagannie"
  ]
  node [
    id 726
    label "niemoralnie"
  ]
  node [
    id 727
    label "nieprzyzwoity"
  ]
  node [
    id 728
    label "wulgarny"
  ]
  node [
    id 729
    label "kurewsko"
  ]
  node [
    id 730
    label "zdzirowaty"
  ]
  node [
    id 731
    label "przekl&#281;ty"
  ]
  node [
    id 732
    label "niesamowity"
  ]
  node [
    id 733
    label "okropno"
  ]
  node [
    id 734
    label "jak_cholera"
  ]
  node [
    id 735
    label "wpisa&#263;"
  ]
  node [
    id 736
    label "notice"
  ]
  node [
    id 737
    label "logarithm"
  ]
  node [
    id 738
    label "napisa&#263;"
  ]
  node [
    id 739
    label "write"
  ]
  node [
    id 740
    label "wprowadzi&#263;"
  ]
  node [
    id 741
    label "instytucja"
  ]
  node [
    id 742
    label "lekarz_rodzinny"
  ]
  node [
    id 743
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 744
    label "osoba_prawna"
  ]
  node [
    id 745
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 746
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 747
    label "poj&#281;cie"
  ]
  node [
    id 748
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 749
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 750
    label "biuro"
  ]
  node [
    id 751
    label "organizacja"
  ]
  node [
    id 752
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 753
    label "Fundusze_Unijne"
  ]
  node [
    id 754
    label "zamyka&#263;"
  ]
  node [
    id 755
    label "establishment"
  ]
  node [
    id 756
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 757
    label "urz&#261;d"
  ]
  node [
    id 758
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 759
    label "afiliowa&#263;"
  ]
  node [
    id 760
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 761
    label "standard"
  ]
  node [
    id 762
    label "zamykanie"
  ]
  node [
    id 763
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 764
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 765
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 766
    label "mie&#263;_miejsce"
  ]
  node [
    id 767
    label "equal"
  ]
  node [
    id 768
    label "trwa&#263;"
  ]
  node [
    id 769
    label "chodzi&#263;"
  ]
  node [
    id 770
    label "si&#281;ga&#263;"
  ]
  node [
    id 771
    label "stan"
  ]
  node [
    id 772
    label "obecno&#347;&#263;"
  ]
  node [
    id 773
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 774
    label "uczestniczy&#263;"
  ]
  node [
    id 775
    label "participate"
  ]
  node [
    id 776
    label "istnie&#263;"
  ]
  node [
    id 777
    label "pozostawa&#263;"
  ]
  node [
    id 778
    label "zostawa&#263;"
  ]
  node [
    id 779
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 780
    label "adhere"
  ]
  node [
    id 781
    label "compass"
  ]
  node [
    id 782
    label "korzysta&#263;"
  ]
  node [
    id 783
    label "appreciation"
  ]
  node [
    id 784
    label "osi&#261;ga&#263;"
  ]
  node [
    id 785
    label "dociera&#263;"
  ]
  node [
    id 786
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 787
    label "mierzy&#263;"
  ]
  node [
    id 788
    label "u&#380;ywa&#263;"
  ]
  node [
    id 789
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 790
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 791
    label "exsert"
  ]
  node [
    id 792
    label "being"
  ]
  node [
    id 793
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 794
    label "cecha"
  ]
  node [
    id 795
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 796
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 797
    label "p&#322;ywa&#263;"
  ]
  node [
    id 798
    label "run"
  ]
  node [
    id 799
    label "bangla&#263;"
  ]
  node [
    id 800
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 801
    label "przebiega&#263;"
  ]
  node [
    id 802
    label "wk&#322;ada&#263;"
  ]
  node [
    id 803
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 804
    label "carry"
  ]
  node [
    id 805
    label "bywa&#263;"
  ]
  node [
    id 806
    label "dziama&#263;"
  ]
  node [
    id 807
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 808
    label "stara&#263;_si&#281;"
  ]
  node [
    id 809
    label "para"
  ]
  node [
    id 810
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 811
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 812
    label "krok"
  ]
  node [
    id 813
    label "tryb"
  ]
  node [
    id 814
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 815
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 816
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 817
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 818
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 819
    label "continue"
  ]
  node [
    id 820
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 821
    label "Ohio"
  ]
  node [
    id 822
    label "wci&#281;cie"
  ]
  node [
    id 823
    label "Nowy_York"
  ]
  node [
    id 824
    label "warstwa"
  ]
  node [
    id 825
    label "samopoczucie"
  ]
  node [
    id 826
    label "Illinois"
  ]
  node [
    id 827
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 828
    label "state"
  ]
  node [
    id 829
    label "Jukatan"
  ]
  node [
    id 830
    label "Kalifornia"
  ]
  node [
    id 831
    label "Wirginia"
  ]
  node [
    id 832
    label "wektor"
  ]
  node [
    id 833
    label "Teksas"
  ]
  node [
    id 834
    label "Goa"
  ]
  node [
    id 835
    label "Waszyngton"
  ]
  node [
    id 836
    label "miejsce"
  ]
  node [
    id 837
    label "Massachusetts"
  ]
  node [
    id 838
    label "Alaska"
  ]
  node [
    id 839
    label "Arakan"
  ]
  node [
    id 840
    label "Hawaje"
  ]
  node [
    id 841
    label "Maryland"
  ]
  node [
    id 842
    label "punkt"
  ]
  node [
    id 843
    label "Michigan"
  ]
  node [
    id 844
    label "Arizona"
  ]
  node [
    id 845
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 846
    label "Georgia"
  ]
  node [
    id 847
    label "poziom"
  ]
  node [
    id 848
    label "Pensylwania"
  ]
  node [
    id 849
    label "shape"
  ]
  node [
    id 850
    label "Luizjana"
  ]
  node [
    id 851
    label "Nowy_Meksyk"
  ]
  node [
    id 852
    label "Alabama"
  ]
  node [
    id 853
    label "Kansas"
  ]
  node [
    id 854
    label "Oregon"
  ]
  node [
    id 855
    label "Floryda"
  ]
  node [
    id 856
    label "Oklahoma"
  ]
  node [
    id 857
    label "jednostka_administracyjna"
  ]
  node [
    id 858
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 859
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 860
    label "klient"
  ]
  node [
    id 861
    label "przypadek"
  ]
  node [
    id 862
    label "piel&#281;gniarz"
  ]
  node [
    id 863
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 864
    label "od&#322;&#261;czanie"
  ]
  node [
    id 865
    label "chory"
  ]
  node [
    id 866
    label "od&#322;&#261;czenie"
  ]
  node [
    id 867
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 868
    label "szpitalnik"
  ]
  node [
    id 869
    label "agent_rozliczeniowy"
  ]
  node [
    id 870
    label "komputer_cyfrowy"
  ]
  node [
    id 871
    label "us&#322;ugobiorca"
  ]
  node [
    id 872
    label "Rzymianin"
  ]
  node [
    id 873
    label "szlachcic"
  ]
  node [
    id 874
    label "obywatel"
  ]
  node [
    id 875
    label "klientela"
  ]
  node [
    id 876
    label "bratek"
  ]
  node [
    id 877
    label "program"
  ]
  node [
    id 878
    label "krzy&#380;owiec"
  ]
  node [
    id 879
    label "tytu&#322;"
  ]
  node [
    id 880
    label "szpitalnicy"
  ]
  node [
    id 881
    label "kawaler"
  ]
  node [
    id 882
    label "happening"
  ]
  node [
    id 883
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 884
    label "schorzenie"
  ]
  node [
    id 885
    label "przyk&#322;ad"
  ]
  node [
    id 886
    label "kategoria_gramatyczna"
  ]
  node [
    id 887
    label "przeznaczenie"
  ]
  node [
    id 888
    label "cutoff"
  ]
  node [
    id 889
    label "od&#322;&#261;czony"
  ]
  node [
    id 890
    label "odbicie"
  ]
  node [
    id 891
    label "odci&#281;cie"
  ]
  node [
    id 892
    label "release"
  ]
  node [
    id 893
    label "ablation"
  ]
  node [
    id 894
    label "oddzielenie"
  ]
  node [
    id 895
    label "severance"
  ]
  node [
    id 896
    label "odcinanie"
  ]
  node [
    id 897
    label "oddzielanie"
  ]
  node [
    id 898
    label "odbijanie"
  ]
  node [
    id 899
    label "przerywanie"
  ]
  node [
    id 900
    label "rupture"
  ]
  node [
    id 901
    label "dissociation"
  ]
  node [
    id 902
    label "oddziela&#263;_si&#281;"
  ]
  node [
    id 903
    label "przerywa&#263;"
  ]
  node [
    id 904
    label "odcina&#263;"
  ]
  node [
    id 905
    label "abstract"
  ]
  node [
    id 906
    label "oddziela&#263;"
  ]
  node [
    id 907
    label "challenge"
  ]
  node [
    id 908
    label "publish"
  ]
  node [
    id 909
    label "separate"
  ]
  node [
    id 910
    label "oddzieli&#263;"
  ]
  node [
    id 911
    label "odci&#261;&#263;"
  ]
  node [
    id 912
    label "amputate"
  ]
  node [
    id 913
    label "przerwa&#263;"
  ]
  node [
    id 914
    label "oddzieli&#263;_si&#281;"
  ]
  node [
    id 915
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 916
    label "choro"
  ]
  node [
    id 917
    label "nieprzytomny"
  ]
  node [
    id 918
    label "skandaliczny"
  ]
  node [
    id 919
    label "chor&#243;bka"
  ]
  node [
    id 920
    label "nienormalny"
  ]
  node [
    id 921
    label "niezdrowy"
  ]
  node [
    id 922
    label "w&#347;ciek&#322;y"
  ]
  node [
    id 923
    label "niezrozumia&#322;y"
  ]
  node [
    id 924
    label "chorowanie"
  ]
  node [
    id 925
    label "le&#380;alnia"
  ]
  node [
    id 926
    label "psychiczny"
  ]
  node [
    id 927
    label "zachorowanie"
  ]
  node [
    id 928
    label "rozchorowywanie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 166
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 148
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 150
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 157
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 153
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 155
  ]
  edge [
    source 8
    target 156
  ]
  edge [
    source 8
    target 158
  ]
  edge [
    source 8
    target 159
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 162
  ]
  edge [
    source 8
    target 163
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 9
    target 313
  ]
  edge [
    source 9
    target 314
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 316
  ]
  edge [
    source 9
    target 317
  ]
  edge [
    source 9
    target 318
  ]
  edge [
    source 9
    target 319
  ]
  edge [
    source 9
    target 320
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 322
  ]
  edge [
    source 9
    target 323
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 368
  ]
  edge [
    source 13
    target 369
  ]
  edge [
    source 13
    target 370
  ]
  edge [
    source 13
    target 371
  ]
  edge [
    source 13
    target 372
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 375
  ]
  edge [
    source 13
    target 376
  ]
  edge [
    source 13
    target 377
  ]
  edge [
    source 13
    target 378
  ]
  edge [
    source 13
    target 379
  ]
  edge [
    source 13
    target 380
  ]
  edge [
    source 13
    target 381
  ]
  edge [
    source 13
    target 59
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 38
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 199
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 765
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 768
  ]
  edge [
    source 20
    target 769
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 20
    target 772
  ]
  edge [
    source 20
    target 559
  ]
  edge [
    source 20
    target 773
  ]
  edge [
    source 20
    target 774
  ]
  edge [
    source 20
    target 775
  ]
  edge [
    source 20
    target 68
  ]
  edge [
    source 20
    target 776
  ]
  edge [
    source 20
    target 777
  ]
  edge [
    source 20
    target 778
  ]
  edge [
    source 20
    target 779
  ]
  edge [
    source 20
    target 780
  ]
  edge [
    source 20
    target 781
  ]
  edge [
    source 20
    target 782
  ]
  edge [
    source 20
    target 783
  ]
  edge [
    source 20
    target 784
  ]
  edge [
    source 20
    target 785
  ]
  edge [
    source 20
    target 325
  ]
  edge [
    source 20
    target 786
  ]
  edge [
    source 20
    target 787
  ]
  edge [
    source 20
    target 788
  ]
  edge [
    source 20
    target 789
  ]
  edge [
    source 20
    target 790
  ]
  edge [
    source 20
    target 791
  ]
  edge [
    source 20
    target 792
  ]
  edge [
    source 20
    target 793
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 795
  ]
  edge [
    source 20
    target 796
  ]
  edge [
    source 20
    target 797
  ]
  edge [
    source 20
    target 798
  ]
  edge [
    source 20
    target 799
  ]
  edge [
    source 20
    target 800
  ]
  edge [
    source 20
    target 801
  ]
  edge [
    source 20
    target 802
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 803
  ]
  edge [
    source 20
    target 804
  ]
  edge [
    source 20
    target 805
  ]
  edge [
    source 20
    target 806
  ]
  edge [
    source 20
    target 807
  ]
  edge [
    source 20
    target 808
  ]
  edge [
    source 20
    target 809
  ]
  edge [
    source 20
    target 810
  ]
  edge [
    source 20
    target 533
  ]
  edge [
    source 20
    target 366
  ]
  edge [
    source 20
    target 811
  ]
  edge [
    source 20
    target 812
  ]
  edge [
    source 20
    target 813
  ]
  edge [
    source 20
    target 814
  ]
  edge [
    source 20
    target 815
  ]
  edge [
    source 20
    target 816
  ]
  edge [
    source 20
    target 817
  ]
  edge [
    source 20
    target 818
  ]
  edge [
    source 20
    target 819
  ]
  edge [
    source 20
    target 820
  ]
  edge [
    source 20
    target 821
  ]
  edge [
    source 20
    target 822
  ]
  edge [
    source 20
    target 823
  ]
  edge [
    source 20
    target 824
  ]
  edge [
    source 20
    target 825
  ]
  edge [
    source 20
    target 826
  ]
  edge [
    source 20
    target 827
  ]
  edge [
    source 20
    target 828
  ]
  edge [
    source 20
    target 829
  ]
  edge [
    source 20
    target 830
  ]
  edge [
    source 20
    target 831
  ]
  edge [
    source 20
    target 832
  ]
  edge [
    source 20
    target 833
  ]
  edge [
    source 20
    target 834
  ]
  edge [
    source 20
    target 835
  ]
  edge [
    source 20
    target 836
  ]
  edge [
    source 20
    target 837
  ]
  edge [
    source 20
    target 838
  ]
  edge [
    source 20
    target 839
  ]
  edge [
    source 20
    target 840
  ]
  edge [
    source 20
    target 841
  ]
  edge [
    source 20
    target 842
  ]
  edge [
    source 20
    target 843
  ]
  edge [
    source 20
    target 844
  ]
  edge [
    source 20
    target 845
  ]
  edge [
    source 20
    target 846
  ]
  edge [
    source 20
    target 847
  ]
  edge [
    source 20
    target 848
  ]
  edge [
    source 20
    target 849
  ]
  edge [
    source 20
    target 850
  ]
  edge [
    source 20
    target 851
  ]
  edge [
    source 20
    target 852
  ]
  edge [
    source 20
    target 681
  ]
  edge [
    source 20
    target 853
  ]
  edge [
    source 20
    target 854
  ]
  edge [
    source 20
    target 855
  ]
  edge [
    source 20
    target 856
  ]
  edge [
    source 20
    target 857
  ]
  edge [
    source 20
    target 858
  ]
  edge [
    source 21
    target 38
  ]
  edge [
    source 21
    target 859
  ]
  edge [
    source 21
    target 860
  ]
  edge [
    source 21
    target 861
  ]
  edge [
    source 21
    target 862
  ]
  edge [
    source 21
    target 863
  ]
  edge [
    source 21
    target 864
  ]
  edge [
    source 21
    target 865
  ]
  edge [
    source 21
    target 866
  ]
  edge [
    source 21
    target 867
  ]
  edge [
    source 21
    target 868
  ]
  edge [
    source 21
    target 277
  ]
  edge [
    source 21
    target 278
  ]
  edge [
    source 21
    target 86
  ]
  edge [
    source 21
    target 279
  ]
  edge [
    source 21
    target 280
  ]
  edge [
    source 21
    target 281
  ]
  edge [
    source 21
    target 282
  ]
  edge [
    source 21
    target 283
  ]
  edge [
    source 21
    target 284
  ]
  edge [
    source 21
    target 285
  ]
  edge [
    source 21
    target 286
  ]
  edge [
    source 21
    target 287
  ]
  edge [
    source 21
    target 288
  ]
  edge [
    source 21
    target 289
  ]
  edge [
    source 21
    target 290
  ]
  edge [
    source 21
    target 291
  ]
  edge [
    source 21
    target 292
  ]
  edge [
    source 21
    target 293
  ]
  edge [
    source 21
    target 294
  ]
  edge [
    source 21
    target 295
  ]
  edge [
    source 21
    target 296
  ]
  edge [
    source 21
    target 297
  ]
  edge [
    source 21
    target 298
  ]
  edge [
    source 21
    target 299
  ]
  edge [
    source 21
    target 300
  ]
  edge [
    source 21
    target 869
  ]
  edge [
    source 21
    target 870
  ]
  edge [
    source 21
    target 871
  ]
  edge [
    source 21
    target 872
  ]
  edge [
    source 21
    target 873
  ]
  edge [
    source 21
    target 874
  ]
  edge [
    source 21
    target 875
  ]
  edge [
    source 21
    target 876
  ]
  edge [
    source 21
    target 877
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 879
  ]
  edge [
    source 21
    target 880
  ]
  edge [
    source 21
    target 881
  ]
  edge [
    source 21
    target 107
  ]
  edge [
    source 21
    target 662
  ]
  edge [
    source 21
    target 882
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 21
    target 884
  ]
  edge [
    source 21
    target 885
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 21
    target 888
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 444
  ]
  edge [
    source 21
    target 891
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 893
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 21
    target 895
  ]
  edge [
    source 21
    target 896
  ]
  edge [
    source 21
    target 897
  ]
  edge [
    source 21
    target 898
  ]
  edge [
    source 21
    target 899
  ]
  edge [
    source 21
    target 900
  ]
  edge [
    source 21
    target 901
  ]
  edge [
    source 21
    target 902
  ]
  edge [
    source 21
    target 544
  ]
  edge [
    source 21
    target 903
  ]
  edge [
    source 21
    target 904
  ]
  edge [
    source 21
    target 905
  ]
  edge [
    source 21
    target 906
  ]
  edge [
    source 21
    target 907
  ]
  edge [
    source 21
    target 908
  ]
  edge [
    source 21
    target 909
  ]
  edge [
    source 21
    target 910
  ]
  edge [
    source 21
    target 911
  ]
  edge [
    source 21
    target 912
  ]
  edge [
    source 21
    target 913
  ]
  edge [
    source 21
    target 914
  ]
  edge [
    source 21
    target 915
  ]
  edge [
    source 21
    target 916
  ]
  edge [
    source 21
    target 917
  ]
  edge [
    source 21
    target 918
  ]
  edge [
    source 21
    target 919
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 921
  ]
  edge [
    source 21
    target 922
  ]
  edge [
    source 21
    target 923
  ]
  edge [
    source 21
    target 924
  ]
  edge [
    source 21
    target 925
  ]
  edge [
    source 21
    target 926
  ]
  edge [
    source 21
    target 927
  ]
  edge [
    source 21
    target 928
  ]
]
