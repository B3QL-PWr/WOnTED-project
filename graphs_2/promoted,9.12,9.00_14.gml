graph [
  node [
    id 0
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "szok"
    origin "text"
  ]
  node [
    id 2
    label "przyjecha&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ukraina"
    origin "text"
  ]
  node [
    id 4
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 5
    label "pobyt"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "prze&#380;ycie"
  ]
  node [
    id 8
    label "wstrz&#261;s"
  ]
  node [
    id 9
    label "shock_absorber"
  ]
  node [
    id 10
    label "zaskoczenie"
  ]
  node [
    id 11
    label "zrozumienie"
  ]
  node [
    id 12
    label "siurpryza"
  ]
  node [
    id 13
    label "wzbudzenie"
  ]
  node [
    id 14
    label "wydarzenie"
  ]
  node [
    id 15
    label "zaskok"
  ]
  node [
    id 16
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 17
    label "surprise"
  ]
  node [
    id 18
    label "zdziwienie"
  ]
  node [
    id 19
    label "wra&#380;enie"
  ]
  node [
    id 20
    label "przej&#347;cie"
  ]
  node [
    id 21
    label "doznanie"
  ]
  node [
    id 22
    label "poradzenie_sobie"
  ]
  node [
    id 23
    label "przetrwanie"
  ]
  node [
    id 24
    label "survival"
  ]
  node [
    id 25
    label "zaburzenie"
  ]
  node [
    id 26
    label "oznaka"
  ]
  node [
    id 27
    label "impact"
  ]
  node [
    id 28
    label "ruch"
  ]
  node [
    id 29
    label "zmiana"
  ]
  node [
    id 30
    label "daleki"
  ]
  node [
    id 31
    label "d&#322;ugo"
  ]
  node [
    id 32
    label "mechanika"
  ]
  node [
    id 33
    label "utrzymywanie"
  ]
  node [
    id 34
    label "move"
  ]
  node [
    id 35
    label "poruszenie"
  ]
  node [
    id 36
    label "movement"
  ]
  node [
    id 37
    label "myk"
  ]
  node [
    id 38
    label "utrzyma&#263;"
  ]
  node [
    id 39
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 40
    label "zjawisko"
  ]
  node [
    id 41
    label "utrzymanie"
  ]
  node [
    id 42
    label "travel"
  ]
  node [
    id 43
    label "kanciasty"
  ]
  node [
    id 44
    label "commercial_enterprise"
  ]
  node [
    id 45
    label "model"
  ]
  node [
    id 46
    label "strumie&#324;"
  ]
  node [
    id 47
    label "proces"
  ]
  node [
    id 48
    label "aktywno&#347;&#263;"
  ]
  node [
    id 49
    label "kr&#243;tki"
  ]
  node [
    id 50
    label "taktyka"
  ]
  node [
    id 51
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 52
    label "apraksja"
  ]
  node [
    id 53
    label "natural_process"
  ]
  node [
    id 54
    label "utrzymywa&#263;"
  ]
  node [
    id 55
    label "dyssypacja_energii"
  ]
  node [
    id 56
    label "tumult"
  ]
  node [
    id 57
    label "stopek"
  ]
  node [
    id 58
    label "czynno&#347;&#263;"
  ]
  node [
    id 59
    label "manewr"
  ]
  node [
    id 60
    label "lokomocja"
  ]
  node [
    id 61
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 62
    label "komunikacja"
  ]
  node [
    id 63
    label "drift"
  ]
  node [
    id 64
    label "dawny"
  ]
  node [
    id 65
    label "ogl&#281;dny"
  ]
  node [
    id 66
    label "du&#380;y"
  ]
  node [
    id 67
    label "daleko"
  ]
  node [
    id 68
    label "odleg&#322;y"
  ]
  node [
    id 69
    label "zwi&#261;zany"
  ]
  node [
    id 70
    label "r&#243;&#380;ny"
  ]
  node [
    id 71
    label "s&#322;aby"
  ]
  node [
    id 72
    label "odlegle"
  ]
  node [
    id 73
    label "oddalony"
  ]
  node [
    id 74
    label "g&#322;&#281;boki"
  ]
  node [
    id 75
    label "obcy"
  ]
  node [
    id 76
    label "nieobecny"
  ]
  node [
    id 77
    label "przysz&#322;y"
  ]
  node [
    id 78
    label "obecno&#347;&#263;"
  ]
  node [
    id 79
    label "stan"
  ]
  node [
    id 80
    label "being"
  ]
  node [
    id 81
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 82
    label "cecha"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
]
