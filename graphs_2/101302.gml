graph [
  node [
    id 0
    label "zbigniew"
    origin "text"
  ]
  node [
    id 1
    label "zaremba"
    origin "text"
  ]
  node [
    id 2
    label "Zbigniew"
  ]
  node [
    id 3
    label "Zaremba"
  ]
  node [
    id 4
    label "teatr"
  ]
  node [
    id 5
    label "on"
  ]
  node [
    id 6
    label "Wanda"
  ]
  node [
    id 7
    label "Siemaszkowa"
  ]
  node [
    id 8
    label "przekl&#281;ty"
  ]
  node [
    id 9
    label "oko"
  ]
  node [
    id 10
    label "prorok"
  ]
  node [
    id 11
    label "pawe&#322;"
  ]
  node [
    id 12
    label "Komorowski"
  ]
  node [
    id 13
    label "Stanis&#322;awa"
  ]
  node [
    id 14
    label "J&#281;dryki"
  ]
  node [
    id 15
    label "kino"
  ]
  node [
    id 16
    label "objazdowy"
  ]
  node [
    id 17
    label "zas&#322;u&#380;y&#263;"
  ]
  node [
    id 18
    label "dla"
  ]
  node [
    id 19
    label "wojew&#243;dztwo"
  ]
  node [
    id 20
    label "rzeszowski"
  ]
  node [
    id 21
    label "beat"
  ]
  node [
    id 22
    label "Zarembianki"
  ]
  node [
    id 23
    label "order"
  ]
  node [
    id 24
    label "odrodzi&#263;"
  ]
  node [
    id 25
    label "polski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
]
