graph [
  node [
    id 0
    label "widma"
    origin "text"
  ]
  node [
    id 1
    label "&#380;&#243;&#322;ty"
    origin "text"
  ]
  node [
    id 2
    label "kamizelka"
    origin "text"
  ]
  node [
    id 3
    label "kr&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 4
    label "europa"
    origin "text"
  ]
  node [
    id 5
    label "typ_mongoloidalny"
  ]
  node [
    id 6
    label "kolorowy"
  ]
  node [
    id 7
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 8
    label "ciep&#322;y"
  ]
  node [
    id 9
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 10
    label "jasny"
  ]
  node [
    id 11
    label "zabarwienie_si&#281;"
  ]
  node [
    id 12
    label "ciekawy"
  ]
  node [
    id 13
    label "r&#243;&#380;nofarbny"
  ]
  node [
    id 14
    label "cz&#322;owiek"
  ]
  node [
    id 15
    label "r&#243;&#380;nokolorowo"
  ]
  node [
    id 16
    label "weso&#322;y"
  ]
  node [
    id 17
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 18
    label "barwienie"
  ]
  node [
    id 19
    label "kolorowo"
  ]
  node [
    id 20
    label "barwnie"
  ]
  node [
    id 21
    label "kolorowanie"
  ]
  node [
    id 22
    label "barwisty"
  ]
  node [
    id 23
    label "przyjemny"
  ]
  node [
    id 24
    label "barwienie_si&#281;"
  ]
  node [
    id 25
    label "pi&#281;kny"
  ]
  node [
    id 26
    label "ubarwienie"
  ]
  node [
    id 27
    label "mi&#322;y"
  ]
  node [
    id 28
    label "ocieplanie_si&#281;"
  ]
  node [
    id 29
    label "ocieplanie"
  ]
  node [
    id 30
    label "grzanie"
  ]
  node [
    id 31
    label "ocieplenie_si&#281;"
  ]
  node [
    id 32
    label "zagrzanie"
  ]
  node [
    id 33
    label "ocieplenie"
  ]
  node [
    id 34
    label "korzystny"
  ]
  node [
    id 35
    label "ciep&#322;o"
  ]
  node [
    id 36
    label "dobry"
  ]
  node [
    id 37
    label "o&#347;wietlenie"
  ]
  node [
    id 38
    label "szczery"
  ]
  node [
    id 39
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 40
    label "jasno"
  ]
  node [
    id 41
    label "o&#347;wietlanie"
  ]
  node [
    id 42
    label "przytomny"
  ]
  node [
    id 43
    label "zrozumia&#322;y"
  ]
  node [
    id 44
    label "niezm&#261;cony"
  ]
  node [
    id 45
    label "bia&#322;y"
  ]
  node [
    id 46
    label "klarowny"
  ]
  node [
    id 47
    label "jednoznaczny"
  ]
  node [
    id 48
    label "pogodny"
  ]
  node [
    id 49
    label "odcinanie_si&#281;"
  ]
  node [
    id 50
    label "g&#243;ra"
  ]
  node [
    id 51
    label "westa"
  ]
  node [
    id 52
    label "przedmiot"
  ]
  node [
    id 53
    label "przelezienie"
  ]
  node [
    id 54
    label "&#347;piew"
  ]
  node [
    id 55
    label "Synaj"
  ]
  node [
    id 56
    label "Kreml"
  ]
  node [
    id 57
    label "d&#378;wi&#281;k"
  ]
  node [
    id 58
    label "kierunek"
  ]
  node [
    id 59
    label "wysoki"
  ]
  node [
    id 60
    label "element"
  ]
  node [
    id 61
    label "wzniesienie"
  ]
  node [
    id 62
    label "grupa"
  ]
  node [
    id 63
    label "pi&#281;tro"
  ]
  node [
    id 64
    label "Ropa"
  ]
  node [
    id 65
    label "kupa"
  ]
  node [
    id 66
    label "przele&#378;&#263;"
  ]
  node [
    id 67
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 68
    label "karczek"
  ]
  node [
    id 69
    label "rami&#261;czko"
  ]
  node [
    id 70
    label "Jaworze"
  ]
  node [
    id 71
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 72
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 73
    label "kontrolowa&#263;"
  ]
  node [
    id 74
    label "carry"
  ]
  node [
    id 75
    label "sok"
  ]
  node [
    id 76
    label "krew"
  ]
  node [
    id 77
    label "wheel"
  ]
  node [
    id 78
    label "manipulate"
  ]
  node [
    id 79
    label "robi&#263;"
  ]
  node [
    id 80
    label "pracowa&#263;"
  ]
  node [
    id 81
    label "match"
  ]
  node [
    id 82
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 83
    label "examine"
  ]
  node [
    id 84
    label "wykrwawia&#263;_si&#281;"
  ]
  node [
    id 85
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 86
    label "wykrwawi&#263;"
  ]
  node [
    id 87
    label "hematokryt"
  ]
  node [
    id 88
    label "cecha"
  ]
  node [
    id 89
    label "kr&#261;&#380;enie"
  ]
  node [
    id 90
    label "wykrwawienie_si&#281;"
  ]
  node [
    id 91
    label "farba"
  ]
  node [
    id 92
    label "wykrwawianie_si&#281;"
  ]
  node [
    id 93
    label "wykrwawianie"
  ]
  node [
    id 94
    label "pokrewie&#324;stwo"
  ]
  node [
    id 95
    label "krwinka"
  ]
  node [
    id 96
    label "lifeblood"
  ]
  node [
    id 97
    label "osocze_krwi"
  ]
  node [
    id 98
    label "tkanka_&#322;&#261;czna"
  ]
  node [
    id 99
    label "wykrwawia&#263;"
  ]
  node [
    id 100
    label "charakter"
  ]
  node [
    id 101
    label "wykrwawienie"
  ]
  node [
    id 102
    label "dializowa&#263;"
  ]
  node [
    id 103
    label "&#347;mier&#263;"
  ]
  node [
    id 104
    label "wykrwawi&#263;_si&#281;"
  ]
  node [
    id 105
    label "dializowanie"
  ]
  node [
    id 106
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 107
    label "krwioplucie"
  ]
  node [
    id 108
    label "marker_nowotworowy"
  ]
  node [
    id 109
    label "ciecz"
  ]
  node [
    id 110
    label "nap&#243;j"
  ]
  node [
    id 111
    label "ro&#347;lina"
  ]
  node [
    id 112
    label "zboczenie"
  ]
  node [
    id 113
    label "om&#243;wienie"
  ]
  node [
    id 114
    label "sponiewieranie"
  ]
  node [
    id 115
    label "discipline"
  ]
  node [
    id 116
    label "rzecz"
  ]
  node [
    id 117
    label "omawia&#263;"
  ]
  node [
    id 118
    label "tre&#347;&#263;"
  ]
  node [
    id 119
    label "robienie"
  ]
  node [
    id 120
    label "sponiewiera&#263;"
  ]
  node [
    id 121
    label "entity"
  ]
  node [
    id 122
    label "tematyka"
  ]
  node [
    id 123
    label "w&#261;tek"
  ]
  node [
    id 124
    label "zbaczanie"
  ]
  node [
    id 125
    label "program_nauczania"
  ]
  node [
    id 126
    label "om&#243;wi&#263;"
  ]
  node [
    id 127
    label "omawianie"
  ]
  node [
    id 128
    label "thing"
  ]
  node [
    id 129
    label "kultura"
  ]
  node [
    id 130
    label "istota"
  ]
  node [
    id 131
    label "zbacza&#263;"
  ]
  node [
    id 132
    label "zboczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
]
