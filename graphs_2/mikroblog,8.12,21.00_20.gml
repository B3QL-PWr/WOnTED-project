graph [
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "snookerowy"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wir"
    origin "text"
  ]
  node [
    id 3
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 4
    label "jednostka_monetarna"
  ]
  node [
    id 5
    label "centym"
  ]
  node [
    id 6
    label "Wilko"
  ]
  node [
    id 7
    label "mienie"
  ]
  node [
    id 8
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 9
    label "frymark"
  ]
  node [
    id 10
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 11
    label "commodity"
  ]
  node [
    id 12
    label "integer"
  ]
  node [
    id 13
    label "liczba"
  ]
  node [
    id 14
    label "zlewanie_si&#281;"
  ]
  node [
    id 15
    label "ilo&#347;&#263;"
  ]
  node [
    id 16
    label "uk&#322;ad"
  ]
  node [
    id 17
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 18
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 19
    label "pe&#322;ny"
  ]
  node [
    id 20
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 21
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 22
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 23
    label "stan"
  ]
  node [
    id 24
    label "rzecz"
  ]
  node [
    id 25
    label "immoblizacja"
  ]
  node [
    id 26
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 27
    label "przej&#347;cie"
  ]
  node [
    id 28
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 29
    label "rodowo&#347;&#263;"
  ]
  node [
    id 30
    label "patent"
  ]
  node [
    id 31
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 32
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 33
    label "przej&#347;&#263;"
  ]
  node [
    id 34
    label "possession"
  ]
  node [
    id 35
    label "zamiana"
  ]
  node [
    id 36
    label "maj&#261;tek"
  ]
  node [
    id 37
    label "Iwaszkiewicz"
  ]
  node [
    id 38
    label "zakr&#281;t"
  ]
  node [
    id 39
    label "cz&#322;owiek"
  ]
  node [
    id 40
    label "odchylenie"
  ]
  node [
    id 41
    label "nienormalny"
  ]
  node [
    id 42
    label "&#347;wiergot"
  ]
  node [
    id 43
    label "orygina&#322;"
  ]
  node [
    id 44
    label "krejzol"
  ]
  node [
    id 45
    label "model"
  ]
  node [
    id 46
    label "ludzko&#347;&#263;"
  ]
  node [
    id 47
    label "asymilowanie"
  ]
  node [
    id 48
    label "wapniak"
  ]
  node [
    id 49
    label "asymilowa&#263;"
  ]
  node [
    id 50
    label "os&#322;abia&#263;"
  ]
  node [
    id 51
    label "posta&#263;"
  ]
  node [
    id 52
    label "hominid"
  ]
  node [
    id 53
    label "podw&#322;adny"
  ]
  node [
    id 54
    label "os&#322;abianie"
  ]
  node [
    id 55
    label "g&#322;owa"
  ]
  node [
    id 56
    label "figura"
  ]
  node [
    id 57
    label "portrecista"
  ]
  node [
    id 58
    label "dwun&#243;g"
  ]
  node [
    id 59
    label "profanum"
  ]
  node [
    id 60
    label "mikrokosmos"
  ]
  node [
    id 61
    label "nasada"
  ]
  node [
    id 62
    label "duch"
  ]
  node [
    id 63
    label "antropochoria"
  ]
  node [
    id 64
    label "osoba"
  ]
  node [
    id 65
    label "wz&#243;r"
  ]
  node [
    id 66
    label "senior"
  ]
  node [
    id 67
    label "oddzia&#322;ywanie"
  ]
  node [
    id 68
    label "Adam"
  ]
  node [
    id 69
    label "homo_sapiens"
  ]
  node [
    id 70
    label "polifag"
  ]
  node [
    id 71
    label "r&#243;&#380;nica"
  ]
  node [
    id 72
    label "pochylenie"
  ]
  node [
    id 73
    label "przesuni&#281;cie"
  ]
  node [
    id 74
    label "transposition"
  ]
  node [
    id 75
    label "inno&#347;&#263;"
  ]
  node [
    id 76
    label "statystyka"
  ]
  node [
    id 77
    label "deviation"
  ]
  node [
    id 78
    label "szalona_g&#322;owa"
  ]
  node [
    id 79
    label "nienormalnie"
  ]
  node [
    id 80
    label "anormalnie"
  ]
  node [
    id 81
    label "schizol"
  ]
  node [
    id 82
    label "pochytany"
  ]
  node [
    id 83
    label "popaprany"
  ]
  node [
    id 84
    label "niestandardowy"
  ]
  node [
    id 85
    label "chory_psychicznie"
  ]
  node [
    id 86
    label "nieprawid&#322;owy"
  ]
  node [
    id 87
    label "dziwny"
  ]
  node [
    id 88
    label "psychol"
  ]
  node [
    id 89
    label "powalony"
  ]
  node [
    id 90
    label "stracenie_rozumu"
  ]
  node [
    id 91
    label "chory"
  ]
  node [
    id 92
    label "z&#322;y"
  ]
  node [
    id 93
    label "&#347;piew"
  ]
  node [
    id 94
    label "pipe"
  ]
  node [
    id 95
    label "skwierk"
  ]
  node [
    id 96
    label "mowa"
  ]
  node [
    id 97
    label "ekscentryk"
  ]
  node [
    id 98
    label "zajob"
  ]
  node [
    id 99
    label "nadsterowno&#347;&#263;"
  ]
  node [
    id 100
    label "odcinek"
  ]
  node [
    id 101
    label "zwrot"
  ]
  node [
    id 102
    label "miejsce"
  ]
  node [
    id 103
    label "serpentyna"
  ]
  node [
    id 104
    label "zapaleniec"
  ]
  node [
    id 105
    label "czas"
  ]
  node [
    id 106
    label "podsterowno&#347;&#263;"
  ]
  node [
    id 107
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 108
    label "trasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
]
