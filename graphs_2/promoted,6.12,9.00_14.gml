graph [
  node [
    id 0
    label "mandat"
    origin "text"
  ]
  node [
    id 1
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 2
    label "kara_pieni&#281;&#380;na"
  ]
  node [
    id 3
    label "dokument"
  ]
  node [
    id 4
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 5
    label "commission"
  ]
  node [
    id 6
    label "za&#347;wiadczenie"
  ]
  node [
    id 7
    label "umocowa&#263;"
  ]
  node [
    id 8
    label "authority"
  ]
  node [
    id 9
    label "prawo"
  ]
  node [
    id 10
    label "power_of_attorney"
  ]
  node [
    id 11
    label "zapis"
  ]
  node [
    id 12
    label "&#347;wiadectwo"
  ]
  node [
    id 13
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 14
    label "wytw&#243;r"
  ]
  node [
    id 15
    label "parafa"
  ]
  node [
    id 16
    label "plik"
  ]
  node [
    id 17
    label "raport&#243;wka"
  ]
  node [
    id 18
    label "utw&#243;r"
  ]
  node [
    id 19
    label "record"
  ]
  node [
    id 20
    label "registratura"
  ]
  node [
    id 21
    label "dokumentacja"
  ]
  node [
    id 22
    label "fascyku&#322;"
  ]
  node [
    id 23
    label "artyku&#322;"
  ]
  node [
    id 24
    label "writing"
  ]
  node [
    id 25
    label "sygnatariusz"
  ]
  node [
    id 26
    label "jednostka_monetarna"
  ]
  node [
    id 27
    label "wspania&#322;y"
  ]
  node [
    id 28
    label "metaliczny"
  ]
  node [
    id 29
    label "Polska"
  ]
  node [
    id 30
    label "szlachetny"
  ]
  node [
    id 31
    label "kochany"
  ]
  node [
    id 32
    label "doskona&#322;y"
  ]
  node [
    id 33
    label "grosz"
  ]
  node [
    id 34
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 35
    label "poz&#322;ocenie"
  ]
  node [
    id 36
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 37
    label "utytu&#322;owany"
  ]
  node [
    id 38
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 39
    label "z&#322;ocenie"
  ]
  node [
    id 40
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 41
    label "prominentny"
  ]
  node [
    id 42
    label "znany"
  ]
  node [
    id 43
    label "wybitny"
  ]
  node [
    id 44
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 45
    label "naj"
  ]
  node [
    id 46
    label "&#347;wietny"
  ]
  node [
    id 47
    label "pe&#322;ny"
  ]
  node [
    id 48
    label "doskonale"
  ]
  node [
    id 49
    label "szlachetnie"
  ]
  node [
    id 50
    label "uczciwy"
  ]
  node [
    id 51
    label "zacny"
  ]
  node [
    id 52
    label "harmonijny"
  ]
  node [
    id 53
    label "gatunkowy"
  ]
  node [
    id 54
    label "pi&#281;kny"
  ]
  node [
    id 55
    label "dobry"
  ]
  node [
    id 56
    label "typowy"
  ]
  node [
    id 57
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 58
    label "metaloplastyczny"
  ]
  node [
    id 59
    label "metalicznie"
  ]
  node [
    id 60
    label "kochanek"
  ]
  node [
    id 61
    label "wybranek"
  ]
  node [
    id 62
    label "umi&#322;owany"
  ]
  node [
    id 63
    label "drogi"
  ]
  node [
    id 64
    label "kochanie"
  ]
  node [
    id 65
    label "wspaniale"
  ]
  node [
    id 66
    label "pomy&#347;lny"
  ]
  node [
    id 67
    label "pozytywny"
  ]
  node [
    id 68
    label "&#347;wietnie"
  ]
  node [
    id 69
    label "spania&#322;y"
  ]
  node [
    id 70
    label "och&#281;do&#380;ny"
  ]
  node [
    id 71
    label "warto&#347;ciowy"
  ]
  node [
    id 72
    label "zajebisty"
  ]
  node [
    id 73
    label "bogato"
  ]
  node [
    id 74
    label "typ_mongoloidalny"
  ]
  node [
    id 75
    label "kolorowy"
  ]
  node [
    id 76
    label "&#380;&#243;&#322;to"
  ]
  node [
    id 77
    label "ciep&#322;y"
  ]
  node [
    id 78
    label "&#380;&#243;&#322;cenie"
  ]
  node [
    id 79
    label "jasny"
  ]
  node [
    id 80
    label "kwota"
  ]
  node [
    id 81
    label "groszak"
  ]
  node [
    id 82
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 83
    label "szyling_austryjacki"
  ]
  node [
    id 84
    label "moneta"
  ]
  node [
    id 85
    label "Mazowsze"
  ]
  node [
    id 86
    label "Pa&#322;uki"
  ]
  node [
    id 87
    label "Pomorze_Zachodnie"
  ]
  node [
    id 88
    label "Powi&#347;le"
  ]
  node [
    id 89
    label "Wolin"
  ]
  node [
    id 90
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 91
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 92
    label "So&#322;a"
  ]
  node [
    id 93
    label "Unia_Europejska"
  ]
  node [
    id 94
    label "Krajna"
  ]
  node [
    id 95
    label "Opolskie"
  ]
  node [
    id 96
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 97
    label "Suwalszczyzna"
  ]
  node [
    id 98
    label "barwy_polskie"
  ]
  node [
    id 99
    label "Nadbu&#380;e"
  ]
  node [
    id 100
    label "Podlasie"
  ]
  node [
    id 101
    label "Izera"
  ]
  node [
    id 102
    label "Ma&#322;opolska"
  ]
  node [
    id 103
    label "Warmia"
  ]
  node [
    id 104
    label "Mazury"
  ]
  node [
    id 105
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 106
    label "NATO"
  ]
  node [
    id 107
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 108
    label "Kaczawa"
  ]
  node [
    id 109
    label "Lubelszczyzna"
  ]
  node [
    id 110
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 111
    label "Kielecczyzna"
  ]
  node [
    id 112
    label "Lubuskie"
  ]
  node [
    id 113
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 114
    label "&#321;&#243;dzkie"
  ]
  node [
    id 115
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 116
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 117
    label "Kujawy"
  ]
  node [
    id 118
    label "Podkarpacie"
  ]
  node [
    id 119
    label "Wielkopolska"
  ]
  node [
    id 120
    label "Wis&#322;a"
  ]
  node [
    id 121
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 122
    label "Bory_Tucholskie"
  ]
  node [
    id 123
    label "platerowanie"
  ]
  node [
    id 124
    label "z&#322;ocisty"
  ]
  node [
    id 125
    label "barwienie"
  ]
  node [
    id 126
    label "gilt"
  ]
  node [
    id 127
    label "plating"
  ]
  node [
    id 128
    label "zdobienie"
  ]
  node [
    id 129
    label "club"
  ]
  node [
    id 130
    label "powleczenie"
  ]
  node [
    id 131
    label "zabarwienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
]
