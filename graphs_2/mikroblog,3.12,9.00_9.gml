graph [
  node [
    id 0
    label "reda"
    origin "text"
  ]
  node [
    id 1
    label "dead"
    origin "text"
  ]
  node [
    id 2
    label "redemption"
    origin "text"
  ]
  node [
    id 3
    label "koloryzowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "falochron"
  ]
  node [
    id 5
    label "akwatorium"
  ]
  node [
    id 6
    label "morze"
  ]
  node [
    id 7
    label "zbiornik_wodny"
  ]
  node [
    id 8
    label "obszar"
  ]
  node [
    id 9
    label "ochrona"
  ]
  node [
    id 10
    label "nabrze&#380;e"
  ]
  node [
    id 11
    label "budowla_hydrotechniczna"
  ]
  node [
    id 12
    label "przymorze"
  ]
  node [
    id 13
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 14
    label "bezmiar"
  ]
  node [
    id 15
    label "pe&#322;ne_morze"
  ]
  node [
    id 16
    label "latarnia_morska"
  ]
  node [
    id 17
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 18
    label "nereida"
  ]
  node [
    id 19
    label "okeanida"
  ]
  node [
    id 20
    label "marina"
  ]
  node [
    id 21
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 22
    label "Morze_Czerwone"
  ]
  node [
    id 23
    label "talasoterapia"
  ]
  node [
    id 24
    label "Morze_Bia&#322;e"
  ]
  node [
    id 25
    label "paliszcze"
  ]
  node [
    id 26
    label "Neptun"
  ]
  node [
    id 27
    label "Morze_Czarne"
  ]
  node [
    id 28
    label "laguna"
  ]
  node [
    id 29
    label "Morze_Egejskie"
  ]
  node [
    id 30
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 31
    label "Ziemia"
  ]
  node [
    id 32
    label "Morze_Adriatyckie"
  ]
  node [
    id 33
    label "ubarwia&#263;"
  ]
  node [
    id 34
    label "color"
  ]
  node [
    id 35
    label "barwi&#263;"
  ]
  node [
    id 36
    label "przesadza&#263;"
  ]
  node [
    id 37
    label "shade"
  ]
  node [
    id 38
    label "sprawia&#263;"
  ]
  node [
    id 39
    label "okrasza&#263;"
  ]
  node [
    id 40
    label "umila&#263;"
  ]
  node [
    id 41
    label "sprzyja&#263;"
  ]
  node [
    id 42
    label "tint"
  ]
  node [
    id 43
    label "dodawa&#263;"
  ]
  node [
    id 44
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 45
    label "nadawa&#263;"
  ]
  node [
    id 46
    label "przetwarza&#263;"
  ]
  node [
    id 47
    label "overstate"
  ]
  node [
    id 48
    label "przeskakiwa&#263;"
  ]
  node [
    id 49
    label "reach"
  ]
  node [
    id 50
    label "przenosi&#263;"
  ]
  node [
    id 51
    label "sadzi&#263;"
  ]
  node [
    id 52
    label "sadza&#263;"
  ]
  node [
    id 53
    label "przekracza&#263;"
  ]
  node [
    id 54
    label "przegina&#263;_pa&#322;&#281;"
  ]
  node [
    id 55
    label "shed_blood"
  ]
  node [
    id 56
    label "krwawi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
]
