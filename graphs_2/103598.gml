graph [
  node [
    id 0
    label "kasztan"
    origin "text"
  ]
  node [
    id 1
    label "kwitn&#261;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "chyba"
    origin "text"
  ]
  node [
    id 3
    label "obrodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "r&#243;&#380;ny"
    origin "text"
  ]
  node [
    id 5
    label "tekst"
    origin "text"
  ]
  node [
    id 6
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "matura"
    origin "text"
  ]
  node [
    id 8
    label "zasada"
    origin "text"
  ]
  node [
    id 9
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "media"
    origin "text"
  ]
  node [
    id 11
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 12
    label "przedrukowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zesz&#322;y"
    origin "text"
  ]
  node [
    id 14
    label "rok"
    origin "text"
  ]
  node [
    id 15
    label "zamiast"
    origin "text"
  ]
  node [
    id 16
    label "nowa"
    origin "text"
  ]
  node [
    id 17
    label "czas"
    origin "text"
  ]
  node [
    id 18
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "wysili&#263;"
    origin "text"
  ]
  node [
    id 21
    label "bardzo"
    origin "text"
  ]
  node [
    id 22
    label "pofantazjowa&#263;"
    origin "text"
  ]
  node [
    id 23
    label "temat"
    origin "text"
  ]
  node [
    id 24
    label "idealny"
    origin "text"
  ]
  node [
    id 25
    label "system"
    origin "text"
  ]
  node [
    id 26
    label "szkolnictwo"
    origin "text"
  ]
  node [
    id 27
    label "gdzie"
    origin "text"
  ]
  node [
    id 28
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 29
    label "rywalizacja"
    origin "text"
  ]
  node [
    id 30
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 31
    label "uczy&#263;"
    origin "text"
  ]
  node [
    id 32
    label "pasja"
    origin "text"
  ]
  node [
    id 33
    label "ucze&#324;"
    origin "text"
  ]
  node [
    id 34
    label "rozwija&#263;"
    origin "text"
  ]
  node [
    id 35
    label "swoje"
    origin "text"
  ]
  node [
    id 36
    label "zainteresowania"
    origin "text"
  ]
  node [
    id 37
    label "og&#243;&#322;"
    origin "text"
  ]
  node [
    id 38
    label "sielanka"
    origin "text"
  ]
  node [
    id 39
    label "niczym"
    origin "text"
  ]
  node [
    id 40
    label "pod"
    origin "text"
  ]
  node [
    id 41
    label "um&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "jawor"
    origin "text"
  ]
  node [
    id 43
    label "wystarczy&#263;by"
    origin "text"
  ]
  node [
    id 44
    label "zlikwidowa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "egzamin"
    origin "text"
  ]
  node [
    id 46
    label "og&#243;lnopolski"
    origin "text"
  ]
  node [
    id 47
    label "ten"
    origin "text"
  ]
  node [
    id 48
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 49
    label "skomentowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 50
    label "tablica"
    origin "text"
  ]
  node [
    id 51
    label "pewne"
    origin "text"
  ]
  node [
    id 52
    label "d&#380;entelmen"
    origin "text"
  ]
  node [
    id 53
    label "zna&#263;"
    origin "text"
  ]
  node [
    id 54
    label "niemal"
    origin "text"
  ]
  node [
    id 55
    label "wszyscy"
    origin "text"
  ]
  node [
    id 56
    label "czytelnik"
    origin "text"
  ]
  node [
    id 57
    label "moje"
    origin "text"
  ]
  node [
    id 58
    label "blog"
    origin "text"
  ]
  node [
    id 59
    label "powtarza&#263;"
    origin "text"
  ]
  node [
    id 60
    label "pom&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 61
    label "matematyka"
    origin "text"
  ]
  node [
    id 62
    label "szrot&#243;wek_kasztanowcowiaczek"
  ]
  node [
    id 63
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 64
    label "ro&#347;lina_truj&#261;ca"
  ]
  node [
    id 65
    label "orzech"
  ]
  node [
    id 66
    label "kasztanowcowate"
  ]
  node [
    id 67
    label "mydle&#324;cowate"
  ]
  node [
    id 68
    label "nasiono"
  ]
  node [
    id 69
    label "drzewo"
  ]
  node [
    id 70
    label "ko&#324;"
  ]
  node [
    id 71
    label "drewno"
  ]
  node [
    id 72
    label "chestnut"
  ]
  node [
    id 73
    label "torebka"
  ]
  node [
    id 74
    label "bukowate"
  ]
  node [
    id 75
    label "br&#261;z"
  ]
  node [
    id 76
    label "k&#322;usowa&#263;"
  ]
  node [
    id 77
    label "nar&#243;w"
  ]
  node [
    id 78
    label "&#322;ykawo&#347;&#263;"
  ]
  node [
    id 79
    label "galopowa&#263;"
  ]
  node [
    id 80
    label "koniowate"
  ]
  node [
    id 81
    label "pogalopowanie"
  ]
  node [
    id 82
    label "zaci&#281;cie"
  ]
  node [
    id 83
    label "galopowanie"
  ]
  node [
    id 84
    label "osadza&#263;_si&#281;"
  ]
  node [
    id 85
    label "zar&#380;e&#263;"
  ]
  node [
    id 86
    label "k&#322;usowanie"
  ]
  node [
    id 87
    label "narowienie"
  ]
  node [
    id 88
    label "znarowi&#263;"
  ]
  node [
    id 89
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 90
    label "kawalerzysta"
  ]
  node [
    id 91
    label "pok&#322;usowa&#263;"
  ]
  node [
    id 92
    label "hipoterapia"
  ]
  node [
    id 93
    label "hipoterapeuta"
  ]
  node [
    id 94
    label "zebrula"
  ]
  node [
    id 95
    label "zaci&#261;&#263;"
  ]
  node [
    id 96
    label "lansada"
  ]
  node [
    id 97
    label "uje&#380;d&#380;anie"
  ]
  node [
    id 98
    label "narowi&#263;"
  ]
  node [
    id 99
    label "osadzi&#263;_si&#281;"
  ]
  node [
    id 100
    label "r&#380;enie"
  ]
  node [
    id 101
    label "figura"
  ]
  node [
    id 102
    label "osadzanie_si&#281;"
  ]
  node [
    id 103
    label "zebroid"
  ]
  node [
    id 104
    label "os&#322;omu&#322;"
  ]
  node [
    id 105
    label "r&#380;e&#263;"
  ]
  node [
    id 106
    label "przegalopowa&#263;"
  ]
  node [
    id 107
    label "podkuwanie"
  ]
  node [
    id 108
    label "karmiak"
  ]
  node [
    id 109
    label "podkuwa&#263;"
  ]
  node [
    id 110
    label "penis"
  ]
  node [
    id 111
    label "znarowienie"
  ]
  node [
    id 112
    label "czo&#322;dar"
  ]
  node [
    id 113
    label "zwierz&#281;_wierzchowe"
  ]
  node [
    id 114
    label "osadzenie_si&#281;"
  ]
  node [
    id 115
    label "remuda"
  ]
  node [
    id 116
    label "pogalopowa&#263;"
  ]
  node [
    id 117
    label "pok&#322;usowanie"
  ]
  node [
    id 118
    label "przegalopowanie"
  ]
  node [
    id 119
    label "ko&#324;_dziki"
  ]
  node [
    id 120
    label "dosiad"
  ]
  node [
    id 121
    label "pier&#347;nica"
  ]
  node [
    id 122
    label "parzelnia"
  ]
  node [
    id 123
    label "zadrzewienie"
  ]
  node [
    id 124
    label "&#380;ywica"
  ]
  node [
    id 125
    label "cecha"
  ]
  node [
    id 126
    label "fanerofit"
  ]
  node [
    id 127
    label "zacios"
  ]
  node [
    id 128
    label "graf"
  ]
  node [
    id 129
    label "las"
  ]
  node [
    id 130
    label "karczowa&#263;"
  ]
  node [
    id 131
    label "wykarczowa&#263;"
  ]
  node [
    id 132
    label "karczowanie"
  ]
  node [
    id 133
    label "surowiec"
  ]
  node [
    id 134
    label "&#322;yko"
  ]
  node [
    id 135
    label "szpaler"
  ]
  node [
    id 136
    label "chodnik"
  ]
  node [
    id 137
    label "wykarczowanie"
  ]
  node [
    id 138
    label "skupina"
  ]
  node [
    id 139
    label "pie&#324;"
  ]
  node [
    id 140
    label "kora"
  ]
  node [
    id 141
    label "drzewostan"
  ]
  node [
    id 142
    label "brodaczka"
  ]
  node [
    id 143
    label "korona"
  ]
  node [
    id 144
    label "dodatek"
  ]
  node [
    id 145
    label "torba"
  ]
  node [
    id 146
    label "otoczka"
  ]
  node [
    id 147
    label "paczka"
  ]
  node [
    id 148
    label "owoc"
  ]
  node [
    id 149
    label "bielmo"
  ]
  node [
    id 150
    label "&#322;upina"
  ]
  node [
    id 151
    label "seed"
  ]
  node [
    id 152
    label "elajosom"
  ]
  node [
    id 153
    label "organ"
  ]
  node [
    id 154
    label "zarodek"
  ]
  node [
    id 155
    label "przyczepka"
  ]
  node [
    id 156
    label "drewniany"
  ]
  node [
    id 157
    label "trachej"
  ]
  node [
    id 158
    label "aktorzyna"
  ]
  node [
    id 159
    label "ksylofag"
  ]
  node [
    id 160
    label "tkanka_sta&#322;a"
  ]
  node [
    id 161
    label "mi&#281;kisz_drzewny"
  ]
  node [
    id 162
    label "kolor"
  ]
  node [
    id 163
    label "stop"
  ]
  node [
    id 164
    label "medal"
  ]
  node [
    id 165
    label "metal_kolorowy"
  ]
  node [
    id 166
    label "tangent"
  ]
  node [
    id 167
    label "w&#281;giel"
  ]
  node [
    id 168
    label "produkt"
  ]
  node [
    id 169
    label "jedzenie"
  ]
  node [
    id 170
    label "orzechowate"
  ]
  node [
    id 171
    label "pestkowiec"
  ]
  node [
    id 172
    label "kusza"
  ]
  node [
    id 173
    label "bakalie"
  ]
  node [
    id 174
    label "bukowce"
  ]
  node [
    id 175
    label "Fagaceae"
  ]
  node [
    id 176
    label "mydle&#324;cowce"
  ]
  node [
    id 177
    label "tkwi&#263;"
  ]
  node [
    id 178
    label "zarasta&#263;"
  ]
  node [
    id 179
    label "prosperowa&#263;"
  ]
  node [
    id 180
    label "wygl&#261;da&#263;"
  ]
  node [
    id 181
    label "blow"
  ]
  node [
    id 182
    label "b&#322;yszcze&#263;"
  ]
  node [
    id 183
    label "flower"
  ]
  node [
    id 184
    label "pogr&#261;&#380;a&#263;_si&#281;"
  ]
  node [
    id 185
    label "porasta&#263;"
  ]
  node [
    id 186
    label "znika&#263;"
  ]
  node [
    id 187
    label "wype&#322;nia&#263;_si&#281;"
  ]
  node [
    id 188
    label "lookout"
  ]
  node [
    id 189
    label "peep"
  ]
  node [
    id 190
    label "patrze&#263;"
  ]
  node [
    id 191
    label "by&#263;"
  ]
  node [
    id 192
    label "wyziera&#263;"
  ]
  node [
    id 193
    label "look"
  ]
  node [
    id 194
    label "czeka&#263;"
  ]
  node [
    id 195
    label "&#347;wieci&#263;"
  ]
  node [
    id 196
    label "radiance"
  ]
  node [
    id 197
    label "wyr&#243;&#380;nia&#263;_si&#281;"
  ]
  node [
    id 198
    label "demonstrowa&#263;"
  ]
  node [
    id 199
    label "gra&#263;"
  ]
  node [
    id 200
    label "emanowa&#263;"
  ]
  node [
    id 201
    label "zarysowywa&#263;_si&#281;"
  ]
  node [
    id 202
    label "tryska&#263;"
  ]
  node [
    id 203
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 204
    label "pozostawa&#263;"
  ]
  node [
    id 205
    label "przebywa&#263;"
  ]
  node [
    id 206
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 207
    label "fix"
  ]
  node [
    id 208
    label "polega&#263;"
  ]
  node [
    id 209
    label "dzia&#322;a&#263;"
  ]
  node [
    id 210
    label "karabinek_sportowy"
  ]
  node [
    id 211
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 212
    label "wyda&#263;"
  ]
  node [
    id 213
    label "powierzy&#263;"
  ]
  node [
    id 214
    label "pieni&#261;dze"
  ]
  node [
    id 215
    label "plon"
  ]
  node [
    id 216
    label "give"
  ]
  node [
    id 217
    label "skojarzy&#263;"
  ]
  node [
    id 218
    label "d&#378;wi&#281;k"
  ]
  node [
    id 219
    label "zadenuncjowa&#263;"
  ]
  node [
    id 220
    label "impart"
  ]
  node [
    id 221
    label "da&#263;"
  ]
  node [
    id 222
    label "reszta"
  ]
  node [
    id 223
    label "zapach"
  ]
  node [
    id 224
    label "wydawnictwo"
  ]
  node [
    id 225
    label "zrobi&#263;"
  ]
  node [
    id 226
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 227
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 228
    label "wiano"
  ]
  node [
    id 229
    label "produkcja"
  ]
  node [
    id 230
    label "translate"
  ]
  node [
    id 231
    label "picture"
  ]
  node [
    id 232
    label "poda&#263;"
  ]
  node [
    id 233
    label "wprowadzi&#263;"
  ]
  node [
    id 234
    label "wytworzy&#263;"
  ]
  node [
    id 235
    label "dress"
  ]
  node [
    id 236
    label "tajemnica"
  ]
  node [
    id 237
    label "panna_na_wydaniu"
  ]
  node [
    id 238
    label "supply"
  ]
  node [
    id 239
    label "ujawni&#263;"
  ]
  node [
    id 240
    label "inny"
  ]
  node [
    id 241
    label "jaki&#347;"
  ]
  node [
    id 242
    label "r&#243;&#380;nie"
  ]
  node [
    id 243
    label "przyzwoity"
  ]
  node [
    id 244
    label "ciekawy"
  ]
  node [
    id 245
    label "jako&#347;"
  ]
  node [
    id 246
    label "jako_tako"
  ]
  node [
    id 247
    label "niez&#322;y"
  ]
  node [
    id 248
    label "dziwny"
  ]
  node [
    id 249
    label "charakterystyczny"
  ]
  node [
    id 250
    label "kolejny"
  ]
  node [
    id 251
    label "osobno"
  ]
  node [
    id 252
    label "inszy"
  ]
  node [
    id 253
    label "inaczej"
  ]
  node [
    id 254
    label "osobnie"
  ]
  node [
    id 255
    label "ekscerpcja"
  ]
  node [
    id 256
    label "j&#281;zykowo"
  ]
  node [
    id 257
    label "wypowied&#378;"
  ]
  node [
    id 258
    label "redakcja"
  ]
  node [
    id 259
    label "wytw&#243;r"
  ]
  node [
    id 260
    label "pomini&#281;cie"
  ]
  node [
    id 261
    label "dzie&#322;o"
  ]
  node [
    id 262
    label "preparacja"
  ]
  node [
    id 263
    label "odmianka"
  ]
  node [
    id 264
    label "opu&#347;ci&#263;"
  ]
  node [
    id 265
    label "koniektura"
  ]
  node [
    id 266
    label "pisa&#263;"
  ]
  node [
    id 267
    label "obelga"
  ]
  node [
    id 268
    label "przedmiot"
  ]
  node [
    id 269
    label "p&#322;&#243;d"
  ]
  node [
    id 270
    label "work"
  ]
  node [
    id 271
    label "rezultat"
  ]
  node [
    id 272
    label "obrazowanie"
  ]
  node [
    id 273
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 274
    label "dorobek"
  ]
  node [
    id 275
    label "forma"
  ]
  node [
    id 276
    label "tre&#347;&#263;"
  ]
  node [
    id 277
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 278
    label "retrospektywa"
  ]
  node [
    id 279
    label "works"
  ]
  node [
    id 280
    label "creation"
  ]
  node [
    id 281
    label "tetralogia"
  ]
  node [
    id 282
    label "komunikat"
  ]
  node [
    id 283
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 284
    label "praca"
  ]
  node [
    id 285
    label "pos&#322;uchanie"
  ]
  node [
    id 286
    label "s&#261;d"
  ]
  node [
    id 287
    label "sparafrazowanie"
  ]
  node [
    id 288
    label "strawestowa&#263;"
  ]
  node [
    id 289
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 290
    label "trawestowa&#263;"
  ]
  node [
    id 291
    label "sparafrazowa&#263;"
  ]
  node [
    id 292
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 293
    label "sformu&#322;owanie"
  ]
  node [
    id 294
    label "parafrazowanie"
  ]
  node [
    id 295
    label "ozdobnik"
  ]
  node [
    id 296
    label "delimitacja"
  ]
  node [
    id 297
    label "parafrazowa&#263;"
  ]
  node [
    id 298
    label "stylizacja"
  ]
  node [
    id 299
    label "trawestowanie"
  ]
  node [
    id 300
    label "strawestowanie"
  ]
  node [
    id 301
    label "cholera"
  ]
  node [
    id 302
    label "ubliga"
  ]
  node [
    id 303
    label "niedorobek"
  ]
  node [
    id 304
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 305
    label "chuj"
  ]
  node [
    id 306
    label "bluzg"
  ]
  node [
    id 307
    label "wyzwisko"
  ]
  node [
    id 308
    label "indignation"
  ]
  node [
    id 309
    label "pies"
  ]
  node [
    id 310
    label "wrzuta"
  ]
  node [
    id 311
    label "chujowy"
  ]
  node [
    id 312
    label "krzywda"
  ]
  node [
    id 313
    label "szmata"
  ]
  node [
    id 314
    label "formu&#322;owa&#263;"
  ]
  node [
    id 315
    label "ozdabia&#263;"
  ]
  node [
    id 316
    label "stawia&#263;"
  ]
  node [
    id 317
    label "spell"
  ]
  node [
    id 318
    label "styl"
  ]
  node [
    id 319
    label "skryba"
  ]
  node [
    id 320
    label "read"
  ]
  node [
    id 321
    label "donosi&#263;"
  ]
  node [
    id 322
    label "code"
  ]
  node [
    id 323
    label "dysgrafia"
  ]
  node [
    id 324
    label "dysortografia"
  ]
  node [
    id 325
    label "tworzy&#263;"
  ]
  node [
    id 326
    label "prasa"
  ]
  node [
    id 327
    label "odmiana"
  ]
  node [
    id 328
    label "preparation"
  ]
  node [
    id 329
    label "proces_technologiczny"
  ]
  node [
    id 330
    label "uj&#281;cie"
  ]
  node [
    id 331
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 332
    label "pozostawi&#263;"
  ]
  node [
    id 333
    label "obni&#380;y&#263;"
  ]
  node [
    id 334
    label "zostawi&#263;"
  ]
  node [
    id 335
    label "przesta&#263;"
  ]
  node [
    id 336
    label "potani&#263;"
  ]
  node [
    id 337
    label "drop"
  ]
  node [
    id 338
    label "evacuate"
  ]
  node [
    id 339
    label "humiliate"
  ]
  node [
    id 340
    label "leave"
  ]
  node [
    id 341
    label "straci&#263;"
  ]
  node [
    id 342
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 343
    label "authorize"
  ]
  node [
    id 344
    label "omin&#261;&#263;"
  ]
  node [
    id 345
    label "u&#380;ytkownik"
  ]
  node [
    id 346
    label "komunikacyjnie"
  ]
  node [
    id 347
    label "redaktor"
  ]
  node [
    id 348
    label "radio"
  ]
  node [
    id 349
    label "zesp&#243;&#322;"
  ]
  node [
    id 350
    label "siedziba"
  ]
  node [
    id 351
    label "composition"
  ]
  node [
    id 352
    label "redaction"
  ]
  node [
    id 353
    label "telewizja"
  ]
  node [
    id 354
    label "obr&#243;bka"
  ]
  node [
    id 355
    label "przypuszczenie"
  ]
  node [
    id 356
    label "conjecture"
  ]
  node [
    id 357
    label "wniosek"
  ]
  node [
    id 358
    label "wyb&#243;r"
  ]
  node [
    id 359
    label "dokumentacja"
  ]
  node [
    id 360
    label "ellipsis"
  ]
  node [
    id 361
    label "wykluczenie"
  ]
  node [
    id 362
    label "figura_my&#347;li"
  ]
  node [
    id 363
    label "zrobienie"
  ]
  node [
    id 364
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 365
    label "bargain"
  ]
  node [
    id 366
    label "tycze&#263;"
  ]
  node [
    id 367
    label "&#347;wiadectwo"
  ]
  node [
    id 368
    label "studni&#243;wka"
  ]
  node [
    id 369
    label "matriculation"
  ]
  node [
    id 370
    label "oblewanie"
  ]
  node [
    id 371
    label "faza"
  ]
  node [
    id 372
    label "sesja_egzaminacyjna"
  ]
  node [
    id 373
    label "oblewa&#263;"
  ]
  node [
    id 374
    label "praca_pisemna"
  ]
  node [
    id 375
    label "sprawdzian"
  ]
  node [
    id 376
    label "magiel"
  ]
  node [
    id 377
    label "pr&#243;ba"
  ]
  node [
    id 378
    label "arkusz"
  ]
  node [
    id 379
    label "examination"
  ]
  node [
    id 380
    label "dow&#243;d"
  ]
  node [
    id 381
    label "o&#347;wiadczenie"
  ]
  node [
    id 382
    label "za&#347;wiadczenie"
  ]
  node [
    id 383
    label "certificate"
  ]
  node [
    id 384
    label "promocja"
  ]
  node [
    id 385
    label "dokument"
  ]
  node [
    id 386
    label "zabawa"
  ]
  node [
    id 387
    label "maturzysta"
  ]
  node [
    id 388
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 389
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 390
    label "regu&#322;a_Allena"
  ]
  node [
    id 391
    label "base"
  ]
  node [
    id 392
    label "umowa"
  ]
  node [
    id 393
    label "obserwacja"
  ]
  node [
    id 394
    label "zasada_d'Alemberta"
  ]
  node [
    id 395
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 396
    label "normalizacja"
  ]
  node [
    id 397
    label "moralno&#347;&#263;"
  ]
  node [
    id 398
    label "criterion"
  ]
  node [
    id 399
    label "opis"
  ]
  node [
    id 400
    label "regu&#322;a_Glogera"
  ]
  node [
    id 401
    label "prawo_Mendla"
  ]
  node [
    id 402
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 403
    label "twierdzenie"
  ]
  node [
    id 404
    label "prawo"
  ]
  node [
    id 405
    label "standard"
  ]
  node [
    id 406
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 407
    label "spos&#243;b"
  ]
  node [
    id 408
    label "dominion"
  ]
  node [
    id 409
    label "qualification"
  ]
  node [
    id 410
    label "occupation"
  ]
  node [
    id 411
    label "podstawa"
  ]
  node [
    id 412
    label "substancja"
  ]
  node [
    id 413
    label "prawid&#322;o"
  ]
  node [
    id 414
    label "dobro&#263;"
  ]
  node [
    id 415
    label "aretologia"
  ]
  node [
    id 416
    label "morality"
  ]
  node [
    id 417
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 418
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 419
    label "honesty"
  ]
  node [
    id 420
    label "model"
  ]
  node [
    id 421
    label "organizowa&#263;"
  ]
  node [
    id 422
    label "ordinariness"
  ]
  node [
    id 423
    label "instytucja"
  ]
  node [
    id 424
    label "zorganizowa&#263;"
  ]
  node [
    id 425
    label "taniec_towarzyski"
  ]
  node [
    id 426
    label "organizowanie"
  ]
  node [
    id 427
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 428
    label "zorganizowanie"
  ]
  node [
    id 429
    label "exposition"
  ]
  node [
    id 430
    label "czynno&#347;&#263;"
  ]
  node [
    id 431
    label "obja&#347;nienie"
  ]
  node [
    id 432
    label "zawarcie"
  ]
  node [
    id 433
    label "zawrze&#263;"
  ]
  node [
    id 434
    label "czyn"
  ]
  node [
    id 435
    label "warunek"
  ]
  node [
    id 436
    label "gestia_transportowa"
  ]
  node [
    id 437
    label "contract"
  ]
  node [
    id 438
    label "porozumienie"
  ]
  node [
    id 439
    label "klauzula"
  ]
  node [
    id 440
    label "przenikanie"
  ]
  node [
    id 441
    label "byt"
  ]
  node [
    id 442
    label "materia"
  ]
  node [
    id 443
    label "cz&#261;steczka"
  ]
  node [
    id 444
    label "temperatura_krytyczna"
  ]
  node [
    id 445
    label "przenika&#263;"
  ]
  node [
    id 446
    label "smolisty"
  ]
  node [
    id 447
    label "pot&#281;ga"
  ]
  node [
    id 448
    label "documentation"
  ]
  node [
    id 449
    label "column"
  ]
  node [
    id 450
    label "zasadzenie"
  ]
  node [
    id 451
    label "za&#322;o&#380;enie"
  ]
  node [
    id 452
    label "punkt_odniesienia"
  ]
  node [
    id 453
    label "zasadzi&#263;"
  ]
  node [
    id 454
    label "bok"
  ]
  node [
    id 455
    label "d&#243;&#322;"
  ]
  node [
    id 456
    label "dzieci&#281;ctwo"
  ]
  node [
    id 457
    label "background"
  ]
  node [
    id 458
    label "podstawowy"
  ]
  node [
    id 459
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 460
    label "strategia"
  ]
  node [
    id 461
    label "pomys&#322;"
  ]
  node [
    id 462
    label "&#347;ciana"
  ]
  node [
    id 463
    label "narz&#281;dzie"
  ]
  node [
    id 464
    label "zbi&#243;r"
  ]
  node [
    id 465
    label "tryb"
  ]
  node [
    id 466
    label "nature"
  ]
  node [
    id 467
    label "shoetree"
  ]
  node [
    id 468
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 469
    label "alternatywa_Fredholma"
  ]
  node [
    id 470
    label "oznajmianie"
  ]
  node [
    id 471
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 472
    label "teoria"
  ]
  node [
    id 473
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 474
    label "paradoks_Leontiefa"
  ]
  node [
    id 475
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 476
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 477
    label "teza"
  ]
  node [
    id 478
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 479
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 480
    label "twierdzenie_Pettisa"
  ]
  node [
    id 481
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 482
    label "twierdzenie_Maya"
  ]
  node [
    id 483
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 484
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 485
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 486
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 487
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 488
    label "zapewnianie"
  ]
  node [
    id 489
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 490
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 491
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 492
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 493
    label "twierdzenie_Stokesa"
  ]
  node [
    id 494
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 495
    label "twierdzenie_Cevy"
  ]
  node [
    id 496
    label "twierdzenie_Pascala"
  ]
  node [
    id 497
    label "proposition"
  ]
  node [
    id 498
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 499
    label "komunikowanie"
  ]
  node [
    id 500
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 501
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 502
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 503
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 504
    label "relacja"
  ]
  node [
    id 505
    label "badanie"
  ]
  node [
    id 506
    label "proces_my&#347;lowy"
  ]
  node [
    id 507
    label "remark"
  ]
  node [
    id 508
    label "metoda"
  ]
  node [
    id 509
    label "stwierdzenie"
  ]
  node [
    id 510
    label "observation"
  ]
  node [
    id 511
    label "calibration"
  ]
  node [
    id 512
    label "operacja"
  ]
  node [
    id 513
    label "proces"
  ]
  node [
    id 514
    label "porz&#261;dek"
  ]
  node [
    id 515
    label "dominance"
  ]
  node [
    id 516
    label "zabieg"
  ]
  node [
    id 517
    label "standardization"
  ]
  node [
    id 518
    label "zmiana"
  ]
  node [
    id 519
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 520
    label "umocowa&#263;"
  ]
  node [
    id 521
    label "procesualistyka"
  ]
  node [
    id 522
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 523
    label "kryminalistyka"
  ]
  node [
    id 524
    label "struktura"
  ]
  node [
    id 525
    label "szko&#322;a"
  ]
  node [
    id 526
    label "kierunek"
  ]
  node [
    id 527
    label "normatywizm"
  ]
  node [
    id 528
    label "jurisprudence"
  ]
  node [
    id 529
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 530
    label "kultura_duchowa"
  ]
  node [
    id 531
    label "przepis"
  ]
  node [
    id 532
    label "prawo_karne_procesowe"
  ]
  node [
    id 533
    label "kazuistyka"
  ]
  node [
    id 534
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 535
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 536
    label "kryminologia"
  ]
  node [
    id 537
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 538
    label "prawo_karne"
  ]
  node [
    id 539
    label "legislacyjnie"
  ]
  node [
    id 540
    label "cywilistyka"
  ]
  node [
    id 541
    label "judykatura"
  ]
  node [
    id 542
    label "kanonistyka"
  ]
  node [
    id 543
    label "nauka_prawa"
  ]
  node [
    id 544
    label "podmiot"
  ]
  node [
    id 545
    label "law"
  ]
  node [
    id 546
    label "wykonawczy"
  ]
  node [
    id 547
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 548
    label "majority"
  ]
  node [
    id 549
    label "Rzym_Zachodni"
  ]
  node [
    id 550
    label "whole"
  ]
  node [
    id 551
    label "ilo&#347;&#263;"
  ]
  node [
    id 552
    label "element"
  ]
  node [
    id 553
    label "Rzym_Wschodni"
  ]
  node [
    id 554
    label "urz&#261;dzenie"
  ]
  node [
    id 555
    label "mass-media"
  ]
  node [
    id 556
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 557
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 558
    label "przekazior"
  ]
  node [
    id 559
    label "uzbrajanie"
  ]
  node [
    id 560
    label "medium"
  ]
  node [
    id 561
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 562
    label "&#347;rodek"
  ]
  node [
    id 563
    label "jasnowidz"
  ]
  node [
    id 564
    label "hipnoza"
  ]
  node [
    id 565
    label "cz&#322;owiek"
  ]
  node [
    id 566
    label "spirytysta"
  ]
  node [
    id 567
    label "otoczenie"
  ]
  node [
    id 568
    label "publikator"
  ]
  node [
    id 569
    label "warunki"
  ]
  node [
    id 570
    label "strona"
  ]
  node [
    id 571
    label "przeka&#378;nik"
  ]
  node [
    id 572
    label "&#347;rodek_przekazu"
  ]
  node [
    id 573
    label "armament"
  ]
  node [
    id 574
    label "arming"
  ]
  node [
    id 575
    label "instalacja"
  ]
  node [
    id 576
    label "wyposa&#380;anie"
  ]
  node [
    id 577
    label "dozbrajanie"
  ]
  node [
    id 578
    label "dozbrojenie"
  ]
  node [
    id 579
    label "montowanie"
  ]
  node [
    id 580
    label "skopiowa&#263;"
  ]
  node [
    id 581
    label "reissue"
  ]
  node [
    id 582
    label "og&#322;osi&#263;_drukiem"
  ]
  node [
    id 583
    label "imitate"
  ]
  node [
    id 584
    label "ostatni"
  ]
  node [
    id 585
    label "niedawno"
  ]
  node [
    id 586
    label "poprzedni"
  ]
  node [
    id 587
    label "pozosta&#322;y"
  ]
  node [
    id 588
    label "ostatnio"
  ]
  node [
    id 589
    label "sko&#324;czony"
  ]
  node [
    id 590
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 591
    label "aktualny"
  ]
  node [
    id 592
    label "najgorszy"
  ]
  node [
    id 593
    label "istota_&#380;ywa"
  ]
  node [
    id 594
    label "w&#261;tpliwy"
  ]
  node [
    id 595
    label "p&#243;&#322;rocze"
  ]
  node [
    id 596
    label "martwy_sezon"
  ]
  node [
    id 597
    label "kalendarz"
  ]
  node [
    id 598
    label "cykl_astronomiczny"
  ]
  node [
    id 599
    label "lata"
  ]
  node [
    id 600
    label "pora_roku"
  ]
  node [
    id 601
    label "stulecie"
  ]
  node [
    id 602
    label "kurs"
  ]
  node [
    id 603
    label "jubileusz"
  ]
  node [
    id 604
    label "grupa"
  ]
  node [
    id 605
    label "kwarta&#322;"
  ]
  node [
    id 606
    label "miesi&#261;c"
  ]
  node [
    id 607
    label "summer"
  ]
  node [
    id 608
    label "odm&#322;adzanie"
  ]
  node [
    id 609
    label "liga"
  ]
  node [
    id 610
    label "jednostka_systematyczna"
  ]
  node [
    id 611
    label "asymilowanie"
  ]
  node [
    id 612
    label "gromada"
  ]
  node [
    id 613
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 614
    label "asymilowa&#263;"
  ]
  node [
    id 615
    label "egzemplarz"
  ]
  node [
    id 616
    label "Entuzjastki"
  ]
  node [
    id 617
    label "kompozycja"
  ]
  node [
    id 618
    label "Terranie"
  ]
  node [
    id 619
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 620
    label "category"
  ]
  node [
    id 621
    label "pakiet_klimatyczny"
  ]
  node [
    id 622
    label "oddzia&#322;"
  ]
  node [
    id 623
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 624
    label "stage_set"
  ]
  node [
    id 625
    label "type"
  ]
  node [
    id 626
    label "specgrupa"
  ]
  node [
    id 627
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 628
    label "&#346;wietliki"
  ]
  node [
    id 629
    label "odm&#322;odzenie"
  ]
  node [
    id 630
    label "Eurogrupa"
  ]
  node [
    id 631
    label "odm&#322;adza&#263;"
  ]
  node [
    id 632
    label "formacja_geologiczna"
  ]
  node [
    id 633
    label "harcerze_starsi"
  ]
  node [
    id 634
    label "poprzedzanie"
  ]
  node [
    id 635
    label "czasoprzestrze&#324;"
  ]
  node [
    id 636
    label "laba"
  ]
  node [
    id 637
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 638
    label "chronometria"
  ]
  node [
    id 639
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 640
    label "rachuba_czasu"
  ]
  node [
    id 641
    label "przep&#322;ywanie"
  ]
  node [
    id 642
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 643
    label "czasokres"
  ]
  node [
    id 644
    label "odczyt"
  ]
  node [
    id 645
    label "chwila"
  ]
  node [
    id 646
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 647
    label "dzieje"
  ]
  node [
    id 648
    label "kategoria_gramatyczna"
  ]
  node [
    id 649
    label "poprzedzenie"
  ]
  node [
    id 650
    label "trawienie"
  ]
  node [
    id 651
    label "pochodzi&#263;"
  ]
  node [
    id 652
    label "period"
  ]
  node [
    id 653
    label "okres_czasu"
  ]
  node [
    id 654
    label "poprzedza&#263;"
  ]
  node [
    id 655
    label "schy&#322;ek"
  ]
  node [
    id 656
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 657
    label "odwlekanie_si&#281;"
  ]
  node [
    id 658
    label "zegar"
  ]
  node [
    id 659
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 660
    label "czwarty_wymiar"
  ]
  node [
    id 661
    label "pochodzenie"
  ]
  node [
    id 662
    label "koniugacja"
  ]
  node [
    id 663
    label "Zeitgeist"
  ]
  node [
    id 664
    label "trawi&#263;"
  ]
  node [
    id 665
    label "pogoda"
  ]
  node [
    id 666
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 667
    label "poprzedzi&#263;"
  ]
  node [
    id 668
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 669
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 670
    label "time_period"
  ]
  node [
    id 671
    label "term"
  ]
  node [
    id 672
    label "rok_akademicki"
  ]
  node [
    id 673
    label "rok_szkolny"
  ]
  node [
    id 674
    label "semester"
  ]
  node [
    id 675
    label "anniwersarz"
  ]
  node [
    id 676
    label "rocznica"
  ]
  node [
    id 677
    label "obszar"
  ]
  node [
    id 678
    label "tydzie&#324;"
  ]
  node [
    id 679
    label "miech"
  ]
  node [
    id 680
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 681
    label "kalendy"
  ]
  node [
    id 682
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 683
    label "long_time"
  ]
  node [
    id 684
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 685
    label "almanac"
  ]
  node [
    id 686
    label "rozk&#322;ad"
  ]
  node [
    id 687
    label "Juliusz_Cezar"
  ]
  node [
    id 688
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 689
    label "zwy&#380;kowanie"
  ]
  node [
    id 690
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 691
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 692
    label "zaj&#281;cia"
  ]
  node [
    id 693
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 694
    label "trasa"
  ]
  node [
    id 695
    label "przeorientowywanie"
  ]
  node [
    id 696
    label "przejazd"
  ]
  node [
    id 697
    label "przeorientowywa&#263;"
  ]
  node [
    id 698
    label "nauka"
  ]
  node [
    id 699
    label "przeorientowanie"
  ]
  node [
    id 700
    label "klasa"
  ]
  node [
    id 701
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 702
    label "przeorientowa&#263;"
  ]
  node [
    id 703
    label "manner"
  ]
  node [
    id 704
    label "course"
  ]
  node [
    id 705
    label "passage"
  ]
  node [
    id 706
    label "zni&#380;kowanie"
  ]
  node [
    id 707
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 708
    label "seria"
  ]
  node [
    id 709
    label "stawka"
  ]
  node [
    id 710
    label "way"
  ]
  node [
    id 711
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 712
    label "deprecjacja"
  ]
  node [
    id 713
    label "cedu&#322;a"
  ]
  node [
    id 714
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 715
    label "drive"
  ]
  node [
    id 716
    label "bearing"
  ]
  node [
    id 717
    label "Lira"
  ]
  node [
    id 718
    label "gwiazda"
  ]
  node [
    id 719
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 720
    label "Arktur"
  ]
  node [
    id 721
    label "kszta&#322;t"
  ]
  node [
    id 722
    label "Gwiazda_Polarna"
  ]
  node [
    id 723
    label "agregatka"
  ]
  node [
    id 724
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 725
    label "S&#322;o&#324;ce"
  ]
  node [
    id 726
    label "Nibiru"
  ]
  node [
    id 727
    label "konstelacja"
  ]
  node [
    id 728
    label "ornament"
  ]
  node [
    id 729
    label "delta_Scuti"
  ]
  node [
    id 730
    label "&#347;wiat&#322;o"
  ]
  node [
    id 731
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 732
    label "obiekt"
  ]
  node [
    id 733
    label "s&#322;awa"
  ]
  node [
    id 734
    label "promie&#324;"
  ]
  node [
    id 735
    label "star"
  ]
  node [
    id 736
    label "gwiazdosz"
  ]
  node [
    id 737
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 738
    label "asocjacja_gwiazd"
  ]
  node [
    id 739
    label "supergrupa"
  ]
  node [
    id 740
    label "time"
  ]
  node [
    id 741
    label "blok"
  ]
  node [
    id 742
    label "handout"
  ]
  node [
    id 743
    label "pomiar"
  ]
  node [
    id 744
    label "lecture"
  ]
  node [
    id 745
    label "reading"
  ]
  node [
    id 746
    label "podawanie"
  ]
  node [
    id 747
    label "wyk&#322;ad"
  ]
  node [
    id 748
    label "potrzyma&#263;"
  ]
  node [
    id 749
    label "pok&#243;j"
  ]
  node [
    id 750
    label "atak"
  ]
  node [
    id 751
    label "program"
  ]
  node [
    id 752
    label "zjawisko"
  ]
  node [
    id 753
    label "meteorology"
  ]
  node [
    id 754
    label "weather"
  ]
  node [
    id 755
    label "prognoza_meteorologiczna"
  ]
  node [
    id 756
    label "czas_wolny"
  ]
  node [
    id 757
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 758
    label "metrologia"
  ]
  node [
    id 759
    label "godzinnik"
  ]
  node [
    id 760
    label "bicie"
  ]
  node [
    id 761
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 762
    label "wahad&#322;o"
  ]
  node [
    id 763
    label "kurant"
  ]
  node [
    id 764
    label "cyferblat"
  ]
  node [
    id 765
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 766
    label "nabicie"
  ]
  node [
    id 767
    label "werk"
  ]
  node [
    id 768
    label "czasomierz"
  ]
  node [
    id 769
    label "tyka&#263;"
  ]
  node [
    id 770
    label "tykn&#261;&#263;"
  ]
  node [
    id 771
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 772
    label "kotwica"
  ]
  node [
    id 773
    label "fleksja"
  ]
  node [
    id 774
    label "liczba"
  ]
  node [
    id 775
    label "coupling"
  ]
  node [
    id 776
    label "osoba"
  ]
  node [
    id 777
    label "czasownik"
  ]
  node [
    id 778
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 779
    label "orz&#281;sek"
  ]
  node [
    id 780
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 781
    label "zaczynanie_si&#281;"
  ]
  node [
    id 782
    label "str&#243;j"
  ]
  node [
    id 783
    label "wynikanie"
  ]
  node [
    id 784
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 785
    label "origin"
  ]
  node [
    id 786
    label "geneza"
  ]
  node [
    id 787
    label "beginning"
  ]
  node [
    id 788
    label "digestion"
  ]
  node [
    id 789
    label "unicestwianie"
  ]
  node [
    id 790
    label "sp&#281;dzanie"
  ]
  node [
    id 791
    label "contemplation"
  ]
  node [
    id 792
    label "rozk&#322;adanie"
  ]
  node [
    id 793
    label "marnowanie"
  ]
  node [
    id 794
    label "proces_fizjologiczny"
  ]
  node [
    id 795
    label "przetrawianie"
  ]
  node [
    id 796
    label "perystaltyka"
  ]
  node [
    id 797
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 798
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 799
    label "pour"
  ]
  node [
    id 800
    label "carry"
  ]
  node [
    id 801
    label "sail"
  ]
  node [
    id 802
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 803
    label "go&#347;ci&#263;"
  ]
  node [
    id 804
    label "mija&#263;"
  ]
  node [
    id 805
    label "proceed"
  ]
  node [
    id 806
    label "odej&#347;cie"
  ]
  node [
    id 807
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 808
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 809
    label "zanikni&#281;cie"
  ]
  node [
    id 810
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 811
    label "ciecz"
  ]
  node [
    id 812
    label "opuszczenie"
  ]
  node [
    id 813
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 814
    label "departure"
  ]
  node [
    id 815
    label "oddalenie_si&#281;"
  ]
  node [
    id 816
    label "przeby&#263;"
  ]
  node [
    id 817
    label "min&#261;&#263;"
  ]
  node [
    id 818
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 819
    label "swimming"
  ]
  node [
    id 820
    label "zago&#347;ci&#263;"
  ]
  node [
    id 821
    label "cross"
  ]
  node [
    id 822
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 823
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 824
    label "opatrzy&#263;"
  ]
  node [
    id 825
    label "overwhelm"
  ]
  node [
    id 826
    label "opatrywa&#263;"
  ]
  node [
    id 827
    label "date"
  ]
  node [
    id 828
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 829
    label "wynika&#263;"
  ]
  node [
    id 830
    label "fall"
  ]
  node [
    id 831
    label "poby&#263;"
  ]
  node [
    id 832
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 833
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 834
    label "bolt"
  ]
  node [
    id 835
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 836
    label "spowodowa&#263;"
  ]
  node [
    id 837
    label "uda&#263;_si&#281;"
  ]
  node [
    id 838
    label "opatrzenie"
  ]
  node [
    id 839
    label "zdarzenie_si&#281;"
  ]
  node [
    id 840
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 841
    label "progress"
  ]
  node [
    id 842
    label "opatrywanie"
  ]
  node [
    id 843
    label "mini&#281;cie"
  ]
  node [
    id 844
    label "doznanie"
  ]
  node [
    id 845
    label "zaistnienie"
  ]
  node [
    id 846
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 847
    label "przebycie"
  ]
  node [
    id 848
    label "cruise"
  ]
  node [
    id 849
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 850
    label "usuwa&#263;"
  ]
  node [
    id 851
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 852
    label "lutowa&#263;"
  ]
  node [
    id 853
    label "marnowa&#263;"
  ]
  node [
    id 854
    label "przetrawia&#263;"
  ]
  node [
    id 855
    label "poch&#322;ania&#263;"
  ]
  node [
    id 856
    label "digest"
  ]
  node [
    id 857
    label "metal"
  ]
  node [
    id 858
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 859
    label "sp&#281;dza&#263;"
  ]
  node [
    id 860
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 861
    label "zjawianie_si&#281;"
  ]
  node [
    id 862
    label "przebywanie"
  ]
  node [
    id 863
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 864
    label "mijanie"
  ]
  node [
    id 865
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 866
    label "zaznawanie"
  ]
  node [
    id 867
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 868
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 869
    label "flux"
  ]
  node [
    id 870
    label "epoka"
  ]
  node [
    id 871
    label "charakter"
  ]
  node [
    id 872
    label "flow"
  ]
  node [
    id 873
    label "choroba_przyrodzona"
  ]
  node [
    id 874
    label "ciota"
  ]
  node [
    id 875
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 876
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 877
    label "kres"
  ]
  node [
    id 878
    label "przestrze&#324;"
  ]
  node [
    id 879
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 880
    label "posta&#263;"
  ]
  node [
    id 881
    label "znaczenie"
  ]
  node [
    id 882
    label "go&#347;&#263;"
  ]
  node [
    id 883
    label "ludzko&#347;&#263;"
  ]
  node [
    id 884
    label "wapniak"
  ]
  node [
    id 885
    label "os&#322;abia&#263;"
  ]
  node [
    id 886
    label "hominid"
  ]
  node [
    id 887
    label "podw&#322;adny"
  ]
  node [
    id 888
    label "os&#322;abianie"
  ]
  node [
    id 889
    label "g&#322;owa"
  ]
  node [
    id 890
    label "portrecista"
  ]
  node [
    id 891
    label "dwun&#243;g"
  ]
  node [
    id 892
    label "profanum"
  ]
  node [
    id 893
    label "mikrokosmos"
  ]
  node [
    id 894
    label "nasada"
  ]
  node [
    id 895
    label "duch"
  ]
  node [
    id 896
    label "antropochoria"
  ]
  node [
    id 897
    label "wz&#243;r"
  ]
  node [
    id 898
    label "senior"
  ]
  node [
    id 899
    label "oddzia&#322;ywanie"
  ]
  node [
    id 900
    label "Adam"
  ]
  node [
    id 901
    label "homo_sapiens"
  ]
  node [
    id 902
    label "polifag"
  ]
  node [
    id 903
    label "odwiedziny"
  ]
  node [
    id 904
    label "klient"
  ]
  node [
    id 905
    label "restauracja"
  ]
  node [
    id 906
    label "przybysz"
  ]
  node [
    id 907
    label "uczestnik"
  ]
  node [
    id 908
    label "hotel"
  ]
  node [
    id 909
    label "bratek"
  ]
  node [
    id 910
    label "sztuka"
  ]
  node [
    id 911
    label "facet"
  ]
  node [
    id 912
    label "Chocho&#322;"
  ]
  node [
    id 913
    label "Herkules_Poirot"
  ]
  node [
    id 914
    label "Edyp"
  ]
  node [
    id 915
    label "parali&#380;owa&#263;"
  ]
  node [
    id 916
    label "Harry_Potter"
  ]
  node [
    id 917
    label "Casanova"
  ]
  node [
    id 918
    label "Zgredek"
  ]
  node [
    id 919
    label "Gargantua"
  ]
  node [
    id 920
    label "Winnetou"
  ]
  node [
    id 921
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 922
    label "Dulcynea"
  ]
  node [
    id 923
    label "person"
  ]
  node [
    id 924
    label "Plastu&#347;"
  ]
  node [
    id 925
    label "Quasimodo"
  ]
  node [
    id 926
    label "Sherlock_Holmes"
  ]
  node [
    id 927
    label "Faust"
  ]
  node [
    id 928
    label "Wallenrod"
  ]
  node [
    id 929
    label "Dwukwiat"
  ]
  node [
    id 930
    label "Don_Juan"
  ]
  node [
    id 931
    label "Don_Kiszot"
  ]
  node [
    id 932
    label "Hamlet"
  ]
  node [
    id 933
    label "Werter"
  ]
  node [
    id 934
    label "istota"
  ]
  node [
    id 935
    label "Szwejk"
  ]
  node [
    id 936
    label "charakterystyka"
  ]
  node [
    id 937
    label "zaistnie&#263;"
  ]
  node [
    id 938
    label "Osjan"
  ]
  node [
    id 939
    label "wygl&#261;d"
  ]
  node [
    id 940
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 941
    label "osobowo&#347;&#263;"
  ]
  node [
    id 942
    label "trim"
  ]
  node [
    id 943
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 944
    label "Aspazja"
  ]
  node [
    id 945
    label "punkt_widzenia"
  ]
  node [
    id 946
    label "kompleksja"
  ]
  node [
    id 947
    label "wytrzyma&#263;"
  ]
  node [
    id 948
    label "budowa"
  ]
  node [
    id 949
    label "formacja"
  ]
  node [
    id 950
    label "pozosta&#263;"
  ]
  node [
    id 951
    label "point"
  ]
  node [
    id 952
    label "przedstawienie"
  ]
  node [
    id 953
    label "odk&#322;adanie"
  ]
  node [
    id 954
    label "condition"
  ]
  node [
    id 955
    label "liczenie"
  ]
  node [
    id 956
    label "stawianie"
  ]
  node [
    id 957
    label "bycie"
  ]
  node [
    id 958
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 959
    label "assay"
  ]
  node [
    id 960
    label "wskazywanie"
  ]
  node [
    id 961
    label "wyraz"
  ]
  node [
    id 962
    label "gravity"
  ]
  node [
    id 963
    label "weight"
  ]
  node [
    id 964
    label "command"
  ]
  node [
    id 965
    label "odgrywanie_roli"
  ]
  node [
    id 966
    label "informacja"
  ]
  node [
    id 967
    label "okre&#347;lanie"
  ]
  node [
    id 968
    label "wyra&#380;enie"
  ]
  node [
    id 969
    label "strive"
  ]
  node [
    id 970
    label "wzmocni&#263;"
  ]
  node [
    id 971
    label "mocny"
  ]
  node [
    id 972
    label "uskuteczni&#263;"
  ]
  node [
    id 973
    label "umocnienie"
  ]
  node [
    id 974
    label "wzm&#243;c"
  ]
  node [
    id 975
    label "utrwali&#263;"
  ]
  node [
    id 976
    label "fixate"
  ]
  node [
    id 977
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 978
    label "reinforce"
  ]
  node [
    id 979
    label "zmieni&#263;"
  ]
  node [
    id 980
    label "consolidate"
  ]
  node [
    id 981
    label "podnie&#347;&#263;"
  ]
  node [
    id 982
    label "wyregulowa&#263;"
  ]
  node [
    id 983
    label "zabezpieczy&#263;"
  ]
  node [
    id 984
    label "w_chuj"
  ]
  node [
    id 985
    label "sprawa"
  ]
  node [
    id 986
    label "wyraz_pochodny"
  ]
  node [
    id 987
    label "zboczenie"
  ]
  node [
    id 988
    label "om&#243;wienie"
  ]
  node [
    id 989
    label "rzecz"
  ]
  node [
    id 990
    label "omawia&#263;"
  ]
  node [
    id 991
    label "fraza"
  ]
  node [
    id 992
    label "entity"
  ]
  node [
    id 993
    label "forum"
  ]
  node [
    id 994
    label "topik"
  ]
  node [
    id 995
    label "tematyka"
  ]
  node [
    id 996
    label "w&#261;tek"
  ]
  node [
    id 997
    label "zbaczanie"
  ]
  node [
    id 998
    label "om&#243;wi&#263;"
  ]
  node [
    id 999
    label "omawianie"
  ]
  node [
    id 1000
    label "melodia"
  ]
  node [
    id 1001
    label "zbacza&#263;"
  ]
  node [
    id 1002
    label "zboczy&#263;"
  ]
  node [
    id 1003
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 1004
    label "wypowiedzenie"
  ]
  node [
    id 1005
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1006
    label "zdanie"
  ]
  node [
    id 1007
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 1008
    label "motyw"
  ]
  node [
    id 1009
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1010
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1011
    label "poznanie"
  ]
  node [
    id 1012
    label "leksem"
  ]
  node [
    id 1013
    label "stan"
  ]
  node [
    id 1014
    label "blaszka"
  ]
  node [
    id 1015
    label "poj&#281;cie"
  ]
  node [
    id 1016
    label "kantyzm"
  ]
  node [
    id 1017
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1018
    label "do&#322;ek"
  ]
  node [
    id 1019
    label "formality"
  ]
  node [
    id 1020
    label "mode"
  ]
  node [
    id 1021
    label "morfem"
  ]
  node [
    id 1022
    label "rdze&#324;"
  ]
  node [
    id 1023
    label "kielich"
  ]
  node [
    id 1024
    label "ornamentyka"
  ]
  node [
    id 1025
    label "pasmo"
  ]
  node [
    id 1026
    label "zwyczaj"
  ]
  node [
    id 1027
    label "naczynie"
  ]
  node [
    id 1028
    label "p&#322;at"
  ]
  node [
    id 1029
    label "maszyna_drukarska"
  ]
  node [
    id 1030
    label "style"
  ]
  node [
    id 1031
    label "linearno&#347;&#263;"
  ]
  node [
    id 1032
    label "spirala"
  ]
  node [
    id 1033
    label "dyspozycja"
  ]
  node [
    id 1034
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 1035
    label "October"
  ]
  node [
    id 1036
    label "p&#281;tla"
  ]
  node [
    id 1037
    label "arystotelizm"
  ]
  node [
    id 1038
    label "szablon"
  ]
  node [
    id 1039
    label "miniatura"
  ]
  node [
    id 1040
    label "zanucenie"
  ]
  node [
    id 1041
    label "nuta"
  ]
  node [
    id 1042
    label "zakosztowa&#263;"
  ]
  node [
    id 1043
    label "zajawka"
  ]
  node [
    id 1044
    label "zanuci&#263;"
  ]
  node [
    id 1045
    label "emocja"
  ]
  node [
    id 1046
    label "oskoma"
  ]
  node [
    id 1047
    label "melika"
  ]
  node [
    id 1048
    label "nucenie"
  ]
  node [
    id 1049
    label "nuci&#263;"
  ]
  node [
    id 1050
    label "brzmienie"
  ]
  node [
    id 1051
    label "taste"
  ]
  node [
    id 1052
    label "muzyka"
  ]
  node [
    id 1053
    label "inclination"
  ]
  node [
    id 1054
    label "m&#322;ot"
  ]
  node [
    id 1055
    label "znak"
  ]
  node [
    id 1056
    label "attribute"
  ]
  node [
    id 1057
    label "marka"
  ]
  node [
    id 1058
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1059
    label "superego"
  ]
  node [
    id 1060
    label "psychika"
  ]
  node [
    id 1061
    label "wn&#281;trze"
  ]
  node [
    id 1062
    label "matter"
  ]
  node [
    id 1063
    label "splot"
  ]
  node [
    id 1064
    label "ceg&#322;a"
  ]
  node [
    id 1065
    label "socket"
  ]
  node [
    id 1066
    label "rozmieszczenie"
  ]
  node [
    id 1067
    label "fabu&#322;a"
  ]
  node [
    id 1068
    label "okrywa"
  ]
  node [
    id 1069
    label "kontekst"
  ]
  node [
    id 1070
    label "object"
  ]
  node [
    id 1071
    label "wpadni&#281;cie"
  ]
  node [
    id 1072
    label "mienie"
  ]
  node [
    id 1073
    label "przyroda"
  ]
  node [
    id 1074
    label "kultura"
  ]
  node [
    id 1075
    label "wpa&#347;&#263;"
  ]
  node [
    id 1076
    label "wpadanie"
  ]
  node [
    id 1077
    label "wpada&#263;"
  ]
  node [
    id 1078
    label "discussion"
  ]
  node [
    id 1079
    label "rozpatrywanie"
  ]
  node [
    id 1080
    label "dyskutowanie"
  ]
  node [
    id 1081
    label "omowny"
  ]
  node [
    id 1082
    label "figura_stylistyczna"
  ]
  node [
    id 1083
    label "odchodzenie"
  ]
  node [
    id 1084
    label "aberrance"
  ]
  node [
    id 1085
    label "swerve"
  ]
  node [
    id 1086
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 1087
    label "distract"
  ]
  node [
    id 1088
    label "odej&#347;&#263;"
  ]
  node [
    id 1089
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 1090
    label "twist"
  ]
  node [
    id 1091
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 1092
    label "przedyskutowa&#263;"
  ]
  node [
    id 1093
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1094
    label "publicize"
  ]
  node [
    id 1095
    label "digress"
  ]
  node [
    id 1096
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 1097
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1098
    label "odchodzi&#263;"
  ]
  node [
    id 1099
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 1100
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 1101
    label "perversion"
  ]
  node [
    id 1102
    label "death"
  ]
  node [
    id 1103
    label "turn"
  ]
  node [
    id 1104
    label "k&#261;t"
  ]
  node [
    id 1105
    label "odchylenie_si&#281;"
  ]
  node [
    id 1106
    label "deviation"
  ]
  node [
    id 1107
    label "patologia"
  ]
  node [
    id 1108
    label "dyskutowa&#263;"
  ]
  node [
    id 1109
    label "discourse"
  ]
  node [
    id 1110
    label "kognicja"
  ]
  node [
    id 1111
    label "rozprawa"
  ]
  node [
    id 1112
    label "wydarzenie"
  ]
  node [
    id 1113
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1114
    label "przes&#322;anka"
  ]
  node [
    id 1115
    label "idea"
  ]
  node [
    id 1116
    label "paj&#261;k"
  ]
  node [
    id 1117
    label "przewodnik"
  ]
  node [
    id 1118
    label "odcinek"
  ]
  node [
    id 1119
    label "topikowate"
  ]
  node [
    id 1120
    label "grupa_dyskusyjna"
  ]
  node [
    id 1121
    label "plac"
  ]
  node [
    id 1122
    label "bazylika"
  ]
  node [
    id 1123
    label "miejsce"
  ]
  node [
    id 1124
    label "portal"
  ]
  node [
    id 1125
    label "konferencja"
  ]
  node [
    id 1126
    label "agora"
  ]
  node [
    id 1127
    label "skuteczny"
  ]
  node [
    id 1128
    label "wspania&#322;y"
  ]
  node [
    id 1129
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1130
    label "wznios&#322;y"
  ]
  node [
    id 1131
    label "abstrakcyjny"
  ]
  node [
    id 1132
    label "idealnie"
  ]
  node [
    id 1133
    label "pe&#322;ny"
  ]
  node [
    id 1134
    label "doskonale"
  ]
  node [
    id 1135
    label "czysty"
  ]
  node [
    id 1136
    label "idealistyczny"
  ]
  node [
    id 1137
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1138
    label "nale&#380;ny"
  ]
  node [
    id 1139
    label "nale&#380;yty"
  ]
  node [
    id 1140
    label "typowy"
  ]
  node [
    id 1141
    label "uprawniony"
  ]
  node [
    id 1142
    label "zasadniczy"
  ]
  node [
    id 1143
    label "stosownie"
  ]
  node [
    id 1144
    label "taki"
  ]
  node [
    id 1145
    label "prawdziwy"
  ]
  node [
    id 1146
    label "dobry"
  ]
  node [
    id 1147
    label "my&#347;lowy"
  ]
  node [
    id 1148
    label "niekonwencjonalny"
  ]
  node [
    id 1149
    label "nierealistyczny"
  ]
  node [
    id 1150
    label "teoretyczny"
  ]
  node [
    id 1151
    label "oryginalny"
  ]
  node [
    id 1152
    label "abstrakcyjnie"
  ]
  node [
    id 1153
    label "wspaniale"
  ]
  node [
    id 1154
    label "pomy&#347;lny"
  ]
  node [
    id 1155
    label "pozytywny"
  ]
  node [
    id 1156
    label "&#347;wietnie"
  ]
  node [
    id 1157
    label "spania&#322;y"
  ]
  node [
    id 1158
    label "och&#281;do&#380;ny"
  ]
  node [
    id 1159
    label "warto&#347;ciowy"
  ]
  node [
    id 1160
    label "zajebisty"
  ]
  node [
    id 1161
    label "bogato"
  ]
  node [
    id 1162
    label "nieograniczony"
  ]
  node [
    id 1163
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 1164
    label "satysfakcja"
  ]
  node [
    id 1165
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1166
    label "ca&#322;y"
  ]
  node [
    id 1167
    label "otwarty"
  ]
  node [
    id 1168
    label "wype&#322;nienie"
  ]
  node [
    id 1169
    label "kompletny"
  ]
  node [
    id 1170
    label "pe&#322;no"
  ]
  node [
    id 1171
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 1172
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1173
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 1174
    label "zupe&#322;ny"
  ]
  node [
    id 1175
    label "r&#243;wny"
  ]
  node [
    id 1176
    label "poskutkowanie"
  ]
  node [
    id 1177
    label "sprawny"
  ]
  node [
    id 1178
    label "skutecznie"
  ]
  node [
    id 1179
    label "skutkowanie"
  ]
  node [
    id 1180
    label "idealistycznie"
  ]
  node [
    id 1181
    label "szlachetny"
  ]
  node [
    id 1182
    label "powa&#380;ny"
  ]
  node [
    id 1183
    label "podnios&#322;y"
  ]
  node [
    id 1184
    label "wznio&#347;le"
  ]
  node [
    id 1185
    label "oderwany"
  ]
  node [
    id 1186
    label "pi&#281;kny"
  ]
  node [
    id 1187
    label "pewny"
  ]
  node [
    id 1188
    label "przezroczy&#347;cie"
  ]
  node [
    id 1189
    label "nieemisyjny"
  ]
  node [
    id 1190
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 1191
    label "umycie"
  ]
  node [
    id 1192
    label "ekologiczny"
  ]
  node [
    id 1193
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 1194
    label "bezpieczny"
  ]
  node [
    id 1195
    label "dopuszczalny"
  ]
  node [
    id 1196
    label "mycie"
  ]
  node [
    id 1197
    label "jednolity"
  ]
  node [
    id 1198
    label "udany"
  ]
  node [
    id 1199
    label "czysto"
  ]
  node [
    id 1200
    label "klarowanie"
  ]
  node [
    id 1201
    label "bezchmurny"
  ]
  node [
    id 1202
    label "ostry"
  ]
  node [
    id 1203
    label "legalny"
  ]
  node [
    id 1204
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 1205
    label "prze&#378;roczy"
  ]
  node [
    id 1206
    label "wolny"
  ]
  node [
    id 1207
    label "czyszczenie_si&#281;"
  ]
  node [
    id 1208
    label "uczciwy"
  ]
  node [
    id 1209
    label "do_czysta"
  ]
  node [
    id 1210
    label "klarowanie_si&#281;"
  ]
  node [
    id 1211
    label "sklarowanie"
  ]
  node [
    id 1212
    label "zdrowy"
  ]
  node [
    id 1213
    label "przyjemny"
  ]
  node [
    id 1214
    label "klarowny"
  ]
  node [
    id 1215
    label "cnotliwy"
  ]
  node [
    id 1216
    label "ewidentny"
  ]
  node [
    id 1217
    label "wspinaczka"
  ]
  node [
    id 1218
    label "porz&#261;dny"
  ]
  node [
    id 1219
    label "schludny"
  ]
  node [
    id 1220
    label "doskona&#322;y"
  ]
  node [
    id 1221
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 1222
    label "nieodrodny"
  ]
  node [
    id 1223
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 1224
    label "kompletnie"
  ]
  node [
    id 1225
    label "my&#347;lowo"
  ]
  node [
    id 1226
    label "j&#261;dro"
  ]
  node [
    id 1227
    label "systemik"
  ]
  node [
    id 1228
    label "rozprz&#261;c"
  ]
  node [
    id 1229
    label "oprogramowanie"
  ]
  node [
    id 1230
    label "systemat"
  ]
  node [
    id 1231
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1232
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1233
    label "usenet"
  ]
  node [
    id 1234
    label "przyn&#281;ta"
  ]
  node [
    id 1235
    label "net"
  ]
  node [
    id 1236
    label "w&#281;dkarstwo"
  ]
  node [
    id 1237
    label "eratem"
  ]
  node [
    id 1238
    label "doktryna"
  ]
  node [
    id 1239
    label "pulpit"
  ]
  node [
    id 1240
    label "jednostka_geologiczna"
  ]
  node [
    id 1241
    label "o&#347;"
  ]
  node [
    id 1242
    label "podsystem"
  ]
  node [
    id 1243
    label "ryba"
  ]
  node [
    id 1244
    label "Leopard"
  ]
  node [
    id 1245
    label "Android"
  ]
  node [
    id 1246
    label "zachowanie"
  ]
  node [
    id 1247
    label "cybernetyk"
  ]
  node [
    id 1248
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1249
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1250
    label "method"
  ]
  node [
    id 1251
    label "sk&#322;ad"
  ]
  node [
    id 1252
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1253
    label "uk&#322;ad"
  ]
  node [
    id 1254
    label "styl_architektoniczny"
  ]
  node [
    id 1255
    label "skumanie"
  ]
  node [
    id 1256
    label "orientacja"
  ]
  node [
    id 1257
    label "zorientowanie"
  ]
  node [
    id 1258
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1259
    label "clasp"
  ]
  node [
    id 1260
    label "przem&#243;wienie"
  ]
  node [
    id 1261
    label "series"
  ]
  node [
    id 1262
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1263
    label "uprawianie"
  ]
  node [
    id 1264
    label "praca_rolnicza"
  ]
  node [
    id 1265
    label "collection"
  ]
  node [
    id 1266
    label "dane"
  ]
  node [
    id 1267
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1268
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1269
    label "sum"
  ]
  node [
    id 1270
    label "gathering"
  ]
  node [
    id 1271
    label "album"
  ]
  node [
    id 1272
    label "system_komputerowy"
  ]
  node [
    id 1273
    label "sprz&#281;t"
  ]
  node [
    id 1274
    label "mechanika"
  ]
  node [
    id 1275
    label "konstrukcja"
  ]
  node [
    id 1276
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 1277
    label "moczownik"
  ]
  node [
    id 1278
    label "embryo"
  ]
  node [
    id 1279
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 1280
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 1281
    label "latawiec"
  ]
  node [
    id 1282
    label "reengineering"
  ]
  node [
    id 1283
    label "integer"
  ]
  node [
    id 1284
    label "zlewanie_si&#281;"
  ]
  node [
    id 1285
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1286
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1287
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1288
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 1289
    label "doctrine"
  ]
  node [
    id 1290
    label "urozmaicenie"
  ]
  node [
    id 1291
    label "pu&#322;apka"
  ]
  node [
    id 1292
    label "pon&#281;ta"
  ]
  node [
    id 1293
    label "wabik"
  ]
  node [
    id 1294
    label "sport"
  ]
  node [
    id 1295
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 1296
    label "kr&#281;gowiec"
  ]
  node [
    id 1297
    label "doniczkowiec"
  ]
  node [
    id 1298
    label "mi&#281;so"
  ]
  node [
    id 1299
    label "patroszy&#263;"
  ]
  node [
    id 1300
    label "rakowato&#347;&#263;"
  ]
  node [
    id 1301
    label "ryby"
  ]
  node [
    id 1302
    label "fish"
  ]
  node [
    id 1303
    label "linia_boczna"
  ]
  node [
    id 1304
    label "tar&#322;o"
  ]
  node [
    id 1305
    label "wyrostek_filtracyjny"
  ]
  node [
    id 1306
    label "m&#281;tnooki"
  ]
  node [
    id 1307
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 1308
    label "pokrywa_skrzelowa"
  ]
  node [
    id 1309
    label "ikra"
  ]
  node [
    id 1310
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 1311
    label "szczelina_skrzelowa"
  ]
  node [
    id 1312
    label "blat"
  ]
  node [
    id 1313
    label "interfejs"
  ]
  node [
    id 1314
    label "okno"
  ]
  node [
    id 1315
    label "ikona"
  ]
  node [
    id 1316
    label "system_operacyjny"
  ]
  node [
    id 1317
    label "mebel"
  ]
  node [
    id 1318
    label "relaxation"
  ]
  node [
    id 1319
    label "os&#322;abienie"
  ]
  node [
    id 1320
    label "oswobodzenie"
  ]
  node [
    id 1321
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1322
    label "zdezorganizowanie"
  ]
  node [
    id 1323
    label "reakcja"
  ]
  node [
    id 1324
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 1325
    label "pochowanie"
  ]
  node [
    id 1326
    label "zdyscyplinowanie"
  ]
  node [
    id 1327
    label "post&#261;pienie"
  ]
  node [
    id 1328
    label "post"
  ]
  node [
    id 1329
    label "zwierz&#281;"
  ]
  node [
    id 1330
    label "behawior"
  ]
  node [
    id 1331
    label "dieta"
  ]
  node [
    id 1332
    label "podtrzymanie"
  ]
  node [
    id 1333
    label "etolog"
  ]
  node [
    id 1334
    label "przechowanie"
  ]
  node [
    id 1335
    label "oswobodzi&#263;"
  ]
  node [
    id 1336
    label "os&#322;abi&#263;"
  ]
  node [
    id 1337
    label "disengage"
  ]
  node [
    id 1338
    label "zdezorganizowa&#263;"
  ]
  node [
    id 1339
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1340
    label "naukowiec"
  ]
  node [
    id 1341
    label "provider"
  ]
  node [
    id 1342
    label "b&#322;&#261;d"
  ]
  node [
    id 1343
    label "hipertekst"
  ]
  node [
    id 1344
    label "cyberprzestrze&#324;"
  ]
  node [
    id 1345
    label "mem"
  ]
  node [
    id 1346
    label "grooming"
  ]
  node [
    id 1347
    label "gra_sieciowa"
  ]
  node [
    id 1348
    label "biznes_elektroniczny"
  ]
  node [
    id 1349
    label "sie&#263;_komputerowa"
  ]
  node [
    id 1350
    label "punkt_dost&#281;pu"
  ]
  node [
    id 1351
    label "us&#322;uga_internetowa"
  ]
  node [
    id 1352
    label "netbook"
  ]
  node [
    id 1353
    label "e-hazard"
  ]
  node [
    id 1354
    label "podcast"
  ]
  node [
    id 1355
    label "prezenter"
  ]
  node [
    id 1356
    label "typ"
  ]
  node [
    id 1357
    label "mildew"
  ]
  node [
    id 1358
    label "zi&#243;&#322;ko"
  ]
  node [
    id 1359
    label "motif"
  ]
  node [
    id 1360
    label "pozowanie"
  ]
  node [
    id 1361
    label "ideal"
  ]
  node [
    id 1362
    label "matryca"
  ]
  node [
    id 1363
    label "adaptation"
  ]
  node [
    id 1364
    label "ruch"
  ]
  node [
    id 1365
    label "pozowa&#263;"
  ]
  node [
    id 1366
    label "imitacja"
  ]
  node [
    id 1367
    label "orygina&#322;"
  ]
  node [
    id 1368
    label "podejrzany"
  ]
  node [
    id 1369
    label "s&#261;downictwo"
  ]
  node [
    id 1370
    label "biuro"
  ]
  node [
    id 1371
    label "court"
  ]
  node [
    id 1372
    label "bronienie"
  ]
  node [
    id 1373
    label "urz&#261;d"
  ]
  node [
    id 1374
    label "oskar&#380;yciel"
  ]
  node [
    id 1375
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1376
    label "skazany"
  ]
  node [
    id 1377
    label "post&#281;powanie"
  ]
  node [
    id 1378
    label "broni&#263;"
  ]
  node [
    id 1379
    label "my&#347;l"
  ]
  node [
    id 1380
    label "pods&#261;dny"
  ]
  node [
    id 1381
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1382
    label "obrona"
  ]
  node [
    id 1383
    label "antylogizm"
  ]
  node [
    id 1384
    label "konektyw"
  ]
  node [
    id 1385
    label "&#347;wiadek"
  ]
  node [
    id 1386
    label "procesowicz"
  ]
  node [
    id 1387
    label "lias"
  ]
  node [
    id 1388
    label "dzia&#322;"
  ]
  node [
    id 1389
    label "jednostka"
  ]
  node [
    id 1390
    label "pi&#281;tro"
  ]
  node [
    id 1391
    label "filia"
  ]
  node [
    id 1392
    label "malm"
  ]
  node [
    id 1393
    label "dogger"
  ]
  node [
    id 1394
    label "poziom"
  ]
  node [
    id 1395
    label "bank"
  ]
  node [
    id 1396
    label "ajencja"
  ]
  node [
    id 1397
    label "wojsko"
  ]
  node [
    id 1398
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 1399
    label "agencja"
  ]
  node [
    id 1400
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 1401
    label "szpital"
  ]
  node [
    id 1402
    label "algebra_liniowa"
  ]
  node [
    id 1403
    label "macierz_j&#261;drowa"
  ]
  node [
    id 1404
    label "atom"
  ]
  node [
    id 1405
    label "nukleon"
  ]
  node [
    id 1406
    label "kariokineza"
  ]
  node [
    id 1407
    label "core"
  ]
  node [
    id 1408
    label "chemia_j&#261;drowa"
  ]
  node [
    id 1409
    label "anorchizm"
  ]
  node [
    id 1410
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 1411
    label "nasieniak"
  ]
  node [
    id 1412
    label "wn&#281;trostwo"
  ]
  node [
    id 1413
    label "ziarno"
  ]
  node [
    id 1414
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 1415
    label "j&#261;derko"
  ]
  node [
    id 1416
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 1417
    label "jajo"
  ]
  node [
    id 1418
    label "chromosom"
  ]
  node [
    id 1419
    label "organellum"
  ]
  node [
    id 1420
    label "moszna"
  ]
  node [
    id 1421
    label "przeciwobraz"
  ]
  node [
    id 1422
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 1423
    label "protoplazma"
  ]
  node [
    id 1424
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 1425
    label "nukleosynteza"
  ]
  node [
    id 1426
    label "subsystem"
  ]
  node [
    id 1427
    label "ko&#322;o"
  ]
  node [
    id 1428
    label "granica"
  ]
  node [
    id 1429
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 1430
    label "suport"
  ]
  node [
    id 1431
    label "prosta"
  ]
  node [
    id 1432
    label "o&#347;rodek"
  ]
  node [
    id 1433
    label "eonotem"
  ]
  node [
    id 1434
    label "constellation"
  ]
  node [
    id 1435
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 1436
    label "Ptak_Rajski"
  ]
  node [
    id 1437
    label "W&#281;&#380;ownik"
  ]
  node [
    id 1438
    label "Panna"
  ]
  node [
    id 1439
    label "W&#261;&#380;"
  ]
  node [
    id 1440
    label "blokada"
  ]
  node [
    id 1441
    label "hurtownia"
  ]
  node [
    id 1442
    label "pomieszczenie"
  ]
  node [
    id 1443
    label "pole"
  ]
  node [
    id 1444
    label "pas"
  ]
  node [
    id 1445
    label "basic"
  ]
  node [
    id 1446
    label "sk&#322;adnik"
  ]
  node [
    id 1447
    label "sklep"
  ]
  node [
    id 1448
    label "constitution"
  ]
  node [
    id 1449
    label "fabryka"
  ]
  node [
    id 1450
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1451
    label "syf"
  ]
  node [
    id 1452
    label "rank_and_file"
  ]
  node [
    id 1453
    label "set"
  ]
  node [
    id 1454
    label "tabulacja"
  ]
  node [
    id 1455
    label "Karta_Nauczyciela"
  ]
  node [
    id 1456
    label "program_nauczania"
  ]
  node [
    id 1457
    label "gospodarka"
  ]
  node [
    id 1458
    label "inwentarz"
  ]
  node [
    id 1459
    label "rynek"
  ]
  node [
    id 1460
    label "mieszkalnictwo"
  ]
  node [
    id 1461
    label "agregat_ekonomiczny"
  ]
  node [
    id 1462
    label "miejsce_pracy"
  ]
  node [
    id 1463
    label "produkowanie"
  ]
  node [
    id 1464
    label "farmaceutyka"
  ]
  node [
    id 1465
    label "rolnictwo"
  ]
  node [
    id 1466
    label "transport"
  ]
  node [
    id 1467
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 1468
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 1469
    label "obronno&#347;&#263;"
  ]
  node [
    id 1470
    label "sektor_prywatny"
  ]
  node [
    id 1471
    label "sch&#322;adza&#263;"
  ]
  node [
    id 1472
    label "czerwona_strefa"
  ]
  node [
    id 1473
    label "sektor_publiczny"
  ]
  node [
    id 1474
    label "bankowo&#347;&#263;"
  ]
  node [
    id 1475
    label "gospodarowanie"
  ]
  node [
    id 1476
    label "obora"
  ]
  node [
    id 1477
    label "gospodarka_wodna"
  ]
  node [
    id 1478
    label "gospodarka_le&#347;na"
  ]
  node [
    id 1479
    label "gospodarowa&#263;"
  ]
  node [
    id 1480
    label "wytw&#243;rnia"
  ]
  node [
    id 1481
    label "stodo&#322;a"
  ]
  node [
    id 1482
    label "przemys&#322;"
  ]
  node [
    id 1483
    label "spichlerz"
  ]
  node [
    id 1484
    label "sch&#322;adzanie"
  ]
  node [
    id 1485
    label "administracja"
  ]
  node [
    id 1486
    label "sch&#322;odzenie"
  ]
  node [
    id 1487
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 1488
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 1489
    label "regulacja_cen"
  ]
  node [
    id 1490
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 1491
    label "czyj&#347;"
  ]
  node [
    id 1492
    label "m&#261;&#380;"
  ]
  node [
    id 1493
    label "prywatny"
  ]
  node [
    id 1494
    label "ma&#322;&#380;onek"
  ]
  node [
    id 1495
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1496
    label "ch&#322;op"
  ]
  node [
    id 1497
    label "pan_m&#322;ody"
  ]
  node [
    id 1498
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1499
    label "&#347;lubny"
  ]
  node [
    id 1500
    label "pan_domu"
  ]
  node [
    id 1501
    label "pan_i_w&#322;adca"
  ]
  node [
    id 1502
    label "stary"
  ]
  node [
    id 1503
    label "contest"
  ]
  node [
    id 1504
    label "przebiec"
  ]
  node [
    id 1505
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1506
    label "przebiegni&#281;cie"
  ]
  node [
    id 1507
    label "belfer"
  ]
  node [
    id 1508
    label "kszta&#322;ciciel"
  ]
  node [
    id 1509
    label "preceptor"
  ]
  node [
    id 1510
    label "pedagog"
  ]
  node [
    id 1511
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1512
    label "szkolnik"
  ]
  node [
    id 1513
    label "profesor"
  ]
  node [
    id 1514
    label "popularyzator"
  ]
  node [
    id 1515
    label "rozszerzyciel"
  ]
  node [
    id 1516
    label "stopie&#324;_naukowy"
  ]
  node [
    id 1517
    label "nauczyciel_akademicki"
  ]
  node [
    id 1518
    label "tytu&#322;"
  ]
  node [
    id 1519
    label "profesura"
  ]
  node [
    id 1520
    label "konsulent"
  ]
  node [
    id 1521
    label "wirtuoz"
  ]
  node [
    id 1522
    label "autor"
  ]
  node [
    id 1523
    label "wyprawka"
  ]
  node [
    id 1524
    label "mundurek"
  ]
  node [
    id 1525
    label "tarcza"
  ]
  node [
    id 1526
    label "elew"
  ]
  node [
    id 1527
    label "absolwent"
  ]
  node [
    id 1528
    label "zwierzchnik"
  ]
  node [
    id 1529
    label "ekspert"
  ]
  node [
    id 1530
    label "ochotnik"
  ]
  node [
    id 1531
    label "pomocnik"
  ]
  node [
    id 1532
    label "student"
  ]
  node [
    id 1533
    label "nauczyciel_muzyki"
  ]
  node [
    id 1534
    label "zakonnik"
  ]
  node [
    id 1535
    label "J&#281;drzejewicz"
  ]
  node [
    id 1536
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 1537
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 1538
    label "John_Dewey"
  ]
  node [
    id 1539
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1540
    label "train"
  ]
  node [
    id 1541
    label "szkoli&#263;"
  ]
  node [
    id 1542
    label "zapoznawa&#263;"
  ]
  node [
    id 1543
    label "pracowa&#263;"
  ]
  node [
    id 1544
    label "teach"
  ]
  node [
    id 1545
    label "endeavor"
  ]
  node [
    id 1546
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1547
    label "mie&#263;_miejsce"
  ]
  node [
    id 1548
    label "podejmowa&#263;"
  ]
  node [
    id 1549
    label "dziama&#263;"
  ]
  node [
    id 1550
    label "do"
  ]
  node [
    id 1551
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 1552
    label "bangla&#263;"
  ]
  node [
    id 1553
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 1554
    label "maszyna"
  ]
  node [
    id 1555
    label "funkcjonowa&#263;"
  ]
  node [
    id 1556
    label "robi&#263;"
  ]
  node [
    id 1557
    label "determine"
  ]
  node [
    id 1558
    label "powodowa&#263;"
  ]
  node [
    id 1559
    label "reakcja_chemiczna"
  ]
  node [
    id 1560
    label "puszcza&#263;"
  ]
  node [
    id 1561
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1562
    label "rozpakowywa&#263;"
  ]
  node [
    id 1563
    label "rozstawia&#263;"
  ]
  node [
    id 1564
    label "dopowiada&#263;"
  ]
  node [
    id 1565
    label "inflate"
  ]
  node [
    id 1566
    label "zawiera&#263;"
  ]
  node [
    id 1567
    label "poznawa&#263;"
  ]
  node [
    id 1568
    label "obznajamia&#263;"
  ]
  node [
    id 1569
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1570
    label "go_steady"
  ]
  node [
    id 1571
    label "informowa&#263;"
  ]
  node [
    id 1572
    label "prowadzi&#263;"
  ]
  node [
    id 1573
    label "doskonali&#263;"
  ]
  node [
    id 1574
    label "o&#347;wieca&#263;"
  ]
  node [
    id 1575
    label "pomaga&#263;"
  ]
  node [
    id 1576
    label "zaj&#281;cie"
  ]
  node [
    id 1577
    label "Jezus_Chrystus"
  ]
  node [
    id 1578
    label "lunacy"
  ]
  node [
    id 1579
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 1580
    label "kurwica"
  ]
  node [
    id 1581
    label "tendency"
  ]
  node [
    id 1582
    label "feblik"
  ]
  node [
    id 1583
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 1584
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 1585
    label "pie&#347;&#324;_pasyjna"
  ]
  node [
    id 1586
    label "utw&#243;r"
  ]
  node [
    id 1587
    label "dedication"
  ]
  node [
    id 1588
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1589
    label "nami&#281;tny"
  ]
  node [
    id 1590
    label "fondness"
  ]
  node [
    id 1591
    label "z&#322;o&#347;&#263;"
  ]
  node [
    id 1592
    label "temper"
  ]
  node [
    id 1593
    label "poryw"
  ]
  node [
    id 1594
    label "gniew"
  ]
  node [
    id 1595
    label "wkurw"
  ]
  node [
    id 1596
    label "fury"
  ]
  node [
    id 1597
    label "sympatia"
  ]
  node [
    id 1598
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1599
    label "podatno&#347;&#263;"
  ]
  node [
    id 1600
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1601
    label "care"
  ]
  node [
    id 1602
    label "benedykty&#324;ski"
  ]
  node [
    id 1603
    label "career"
  ]
  node [
    id 1604
    label "anektowanie"
  ]
  node [
    id 1605
    label "dostarczenie"
  ]
  node [
    id 1606
    label "u&#380;ycie"
  ]
  node [
    id 1607
    label "spowodowanie"
  ]
  node [
    id 1608
    label "klasyfikacja"
  ]
  node [
    id 1609
    label "zadanie"
  ]
  node [
    id 1610
    label "wzi&#281;cie"
  ]
  node [
    id 1611
    label "wzbudzenie"
  ]
  node [
    id 1612
    label "tynkarski"
  ]
  node [
    id 1613
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 1614
    label "zapanowanie"
  ]
  node [
    id 1615
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 1616
    label "czynnik_produkcji"
  ]
  node [
    id 1617
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 1618
    label "pozajmowanie"
  ]
  node [
    id 1619
    label "activity"
  ]
  node [
    id 1620
    label "ulokowanie_si&#281;"
  ]
  node [
    id 1621
    label "usytuowanie_si&#281;"
  ]
  node [
    id 1622
    label "obj&#281;cie"
  ]
  node [
    id 1623
    label "zabranie"
  ]
  node [
    id 1624
    label "devotion"
  ]
  node [
    id 1625
    label "powa&#380;anie"
  ]
  node [
    id 1626
    label "dba&#322;o&#347;&#263;"
  ]
  node [
    id 1627
    label "obrz&#281;d"
  ]
  node [
    id 1628
    label "part"
  ]
  node [
    id 1629
    label "element_anatomiczny"
  ]
  node [
    id 1630
    label "w&#347;ciek&#322;o&#347;&#263;"
  ]
  node [
    id 1631
    label "streszczenie"
  ]
  node [
    id 1632
    label "harbinger"
  ]
  node [
    id 1633
    label "ch&#281;&#263;"
  ]
  node [
    id 1634
    label "zapowied&#378;"
  ]
  node [
    id 1635
    label "zami&#322;owanie"
  ]
  node [
    id 1636
    label "czasopismo"
  ]
  node [
    id 1637
    label "reklama"
  ]
  node [
    id 1638
    label "gadka"
  ]
  node [
    id 1639
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 1640
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 1641
    label "gor&#261;cy"
  ]
  node [
    id 1642
    label "nami&#281;tnie"
  ]
  node [
    id 1643
    label "g&#322;&#281;boki"
  ]
  node [
    id 1644
    label "ciep&#322;y"
  ]
  node [
    id 1645
    label "zmys&#322;owy"
  ]
  node [
    id 1646
    label "&#380;ywy"
  ]
  node [
    id 1647
    label "gorliwy"
  ]
  node [
    id 1648
    label "czu&#322;y"
  ]
  node [
    id 1649
    label "kusz&#261;cy"
  ]
  node [
    id 1650
    label "pieprzn&#261;&#263;"
  ]
  node [
    id 1651
    label "wyci&#261;&#263;"
  ]
  node [
    id 1652
    label "spa&#347;&#263;"
  ]
  node [
    id 1653
    label "wybi&#263;"
  ]
  node [
    id 1654
    label "uderzy&#263;"
  ]
  node [
    id 1655
    label "slaughter"
  ]
  node [
    id 1656
    label "wyrze&#378;bi&#263;"
  ]
  node [
    id 1657
    label "przewr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1658
    label "przyrzn&#261;&#263;"
  ]
  node [
    id 1659
    label "rzemie&#347;lnik"
  ]
  node [
    id 1660
    label "kontynuator"
  ]
  node [
    id 1661
    label "zwolennik"
  ]
  node [
    id 1662
    label "praktykant"
  ]
  node [
    id 1663
    label "czeladnik"
  ]
  node [
    id 1664
    label "wagon"
  ]
  node [
    id 1665
    label "mecz_mistrzowski"
  ]
  node [
    id 1666
    label "arrangement"
  ]
  node [
    id 1667
    label "class"
  ]
  node [
    id 1668
    label "&#322;awka"
  ]
  node [
    id 1669
    label "wykrzyknik"
  ]
  node [
    id 1670
    label "zaleta"
  ]
  node [
    id 1671
    label "programowanie_obiektowe"
  ]
  node [
    id 1672
    label "warstwa"
  ]
  node [
    id 1673
    label "rezerwa"
  ]
  node [
    id 1674
    label "Ekwici"
  ]
  node [
    id 1675
    label "&#347;rodowisko"
  ]
  node [
    id 1676
    label "organizacja"
  ]
  node [
    id 1677
    label "sala"
  ]
  node [
    id 1678
    label "pomoc"
  ]
  node [
    id 1679
    label "form"
  ]
  node [
    id 1680
    label "przepisa&#263;"
  ]
  node [
    id 1681
    label "jako&#347;&#263;"
  ]
  node [
    id 1682
    label "znak_jako&#347;ci"
  ]
  node [
    id 1683
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1684
    label "przepisanie"
  ]
  node [
    id 1685
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1686
    label "dziennik_lekcyjny"
  ]
  node [
    id 1687
    label "fakcja"
  ]
  node [
    id 1688
    label "botanika"
  ]
  node [
    id 1689
    label "do&#347;wiadczenie"
  ]
  node [
    id 1690
    label "teren_szko&#322;y"
  ]
  node [
    id 1691
    label "wiedza"
  ]
  node [
    id 1692
    label "Mickiewicz"
  ]
  node [
    id 1693
    label "kwalifikacje"
  ]
  node [
    id 1694
    label "podr&#281;cznik"
  ]
  node [
    id 1695
    label "praktyka"
  ]
  node [
    id 1696
    label "school"
  ]
  node [
    id 1697
    label "zda&#263;"
  ]
  node [
    id 1698
    label "gabinet"
  ]
  node [
    id 1699
    label "urszulanki"
  ]
  node [
    id 1700
    label "sztuba"
  ]
  node [
    id 1701
    label "&#322;awa_szkolna"
  ]
  node [
    id 1702
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1703
    label "lekcja"
  ]
  node [
    id 1704
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1705
    label "skolaryzacja"
  ]
  node [
    id 1706
    label "stopek"
  ]
  node [
    id 1707
    label "sekretariat"
  ]
  node [
    id 1708
    label "ideologia"
  ]
  node [
    id 1709
    label "lesson"
  ]
  node [
    id 1710
    label "niepokalanki"
  ]
  node [
    id 1711
    label "szkolenie"
  ]
  node [
    id 1712
    label "kara"
  ]
  node [
    id 1713
    label "nowicjusz"
  ]
  node [
    id 1714
    label "remiecha"
  ]
  node [
    id 1715
    label "nast&#281;pca"
  ]
  node [
    id 1716
    label "na&#347;ladowca"
  ]
  node [
    id 1717
    label "&#380;o&#322;nierz"
  ]
  node [
    id 1718
    label "zapomoga"
  ]
  node [
    id 1719
    label "komplet"
  ]
  node [
    id 1720
    label "layette"
  ]
  node [
    id 1721
    label "wyprawa"
  ]
  node [
    id 1722
    label "niemowl&#281;"
  ]
  node [
    id 1723
    label "naszywka"
  ]
  node [
    id 1724
    label "wskaz&#243;wka"
  ]
  node [
    id 1725
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 1726
    label "obro&#324;ca"
  ]
  node [
    id 1727
    label "bro&#324;_ochronna"
  ]
  node [
    id 1728
    label "odznaka"
  ]
  node [
    id 1729
    label "bro&#324;"
  ]
  node [
    id 1730
    label "denture"
  ]
  node [
    id 1731
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 1732
    label "telefon"
  ]
  node [
    id 1733
    label "or&#281;&#380;"
  ]
  node [
    id 1734
    label "ochrona"
  ]
  node [
    id 1735
    label "target"
  ]
  node [
    id 1736
    label "cel"
  ]
  node [
    id 1737
    label "powierzchnia"
  ]
  node [
    id 1738
    label "harcerz"
  ]
  node [
    id 1739
    label "uniform"
  ]
  node [
    id 1740
    label "rzemie&#347;lniczek"
  ]
  node [
    id 1741
    label "wydawa&#263;"
  ]
  node [
    id 1742
    label "dissolve"
  ]
  node [
    id 1743
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1744
    label "zwalnia&#263;"
  ]
  node [
    id 1745
    label "nadawa&#263;"
  ]
  node [
    id 1746
    label "go"
  ]
  node [
    id 1747
    label "dematerializowa&#263;_si&#281;"
  ]
  node [
    id 1748
    label "float"
  ]
  node [
    id 1749
    label "lease"
  ]
  node [
    id 1750
    label "dzier&#380;awi&#263;"
  ]
  node [
    id 1751
    label "przestawa&#263;"
  ]
  node [
    id 1752
    label "odbarwia&#263;_si&#281;"
  ]
  node [
    id 1753
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 1754
    label "oddawa&#263;"
  ]
  node [
    id 1755
    label "ust&#281;powa&#263;"
  ]
  node [
    id 1756
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1757
    label "wypuszcza&#263;"
  ]
  node [
    id 1758
    label "permit"
  ]
  node [
    id 1759
    label "zezwala&#263;"
  ]
  node [
    id 1760
    label "zmienia&#263;"
  ]
  node [
    id 1761
    label "increase"
  ]
  node [
    id 1762
    label "wt&#243;rowa&#263;"
  ]
  node [
    id 1763
    label "dorabia&#263;"
  ]
  node [
    id 1764
    label "ko&#324;czy&#263;"
  ]
  node [
    id 1765
    label "m&#243;wi&#263;"
  ]
  node [
    id 1766
    label "bind"
  ]
  node [
    id 1767
    label "ujawnia&#263;"
  ]
  node [
    id 1768
    label "dodawa&#263;"
  ]
  node [
    id 1769
    label "pozostawia&#263;"
  ]
  node [
    id 1770
    label "czyni&#263;"
  ]
  node [
    id 1771
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 1772
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1773
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 1774
    label "raise"
  ]
  node [
    id 1775
    label "przewidywa&#263;"
  ]
  node [
    id 1776
    label "przyznawa&#263;"
  ]
  node [
    id 1777
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1778
    label "obstawia&#263;"
  ]
  node [
    id 1779
    label "umieszcza&#263;"
  ]
  node [
    id 1780
    label "ocenia&#263;"
  ]
  node [
    id 1781
    label "zastawia&#263;"
  ]
  node [
    id 1782
    label "stanowisko"
  ]
  node [
    id 1783
    label "wskazywa&#263;"
  ]
  node [
    id 1784
    label "introduce"
  ]
  node [
    id 1785
    label "uruchamia&#263;"
  ]
  node [
    id 1786
    label "wytwarza&#263;"
  ]
  node [
    id 1787
    label "fundowa&#263;"
  ]
  node [
    id 1788
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 1789
    label "deliver"
  ]
  node [
    id 1790
    label "wyznacza&#263;"
  ]
  node [
    id 1791
    label "przedstawia&#263;"
  ]
  node [
    id 1792
    label "wydobywa&#263;"
  ]
  node [
    id 1793
    label "rozsuwa&#263;"
  ]
  node [
    id 1794
    label "order"
  ]
  node [
    id 1795
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 1796
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 1797
    label "rozmieszcza&#263;"
  ]
  node [
    id 1798
    label "wyjmowa&#263;"
  ]
  node [
    id 1799
    label "opr&#243;&#380;nia&#263;"
  ]
  node [
    id 1800
    label "unwrap"
  ]
  node [
    id 1801
    label "przywraca&#263;"
  ]
  node [
    id 1802
    label "psu&#263;"
  ]
  node [
    id 1803
    label "wygrywa&#263;"
  ]
  node [
    id 1804
    label "exsert"
  ]
  node [
    id 1805
    label "dzieli&#263;"
  ]
  node [
    id 1806
    label "oddala&#263;"
  ]
  node [
    id 1807
    label "cywilizacja"
  ]
  node [
    id 1808
    label "elita"
  ]
  node [
    id 1809
    label "status"
  ]
  node [
    id 1810
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1811
    label "aspo&#322;eczny"
  ]
  node [
    id 1812
    label "ludzie_pracy"
  ]
  node [
    id 1813
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 1814
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 1815
    label "pozaklasowy"
  ]
  node [
    id 1816
    label "Fremeni"
  ]
  node [
    id 1817
    label "uwarstwienie"
  ]
  node [
    id 1818
    label "community"
  ]
  node [
    id 1819
    label "kastowo&#347;&#263;"
  ]
  node [
    id 1820
    label "facylitacja"
  ]
  node [
    id 1821
    label "zatoni&#281;cie"
  ]
  node [
    id 1822
    label "toni&#281;cie"
  ]
  node [
    id 1823
    label "rozmiar"
  ]
  node [
    id 1824
    label "niskogatunkowy"
  ]
  node [
    id 1825
    label "awansowa&#263;"
  ]
  node [
    id 1826
    label "awans"
  ]
  node [
    id 1827
    label "podmiotowo"
  ]
  node [
    id 1828
    label "awansowanie"
  ]
  node [
    id 1829
    label "sytuacja"
  ]
  node [
    id 1830
    label "niekorzystny"
  ]
  node [
    id 1831
    label "aspo&#322;ecznie"
  ]
  node [
    id 1832
    label "niech&#281;tny"
  ]
  node [
    id 1833
    label "asymilowanie_si&#281;"
  ]
  node [
    id 1834
    label "Wsch&#243;d"
  ]
  node [
    id 1835
    label "przejmowanie"
  ]
  node [
    id 1836
    label "makrokosmos"
  ]
  node [
    id 1837
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 1838
    label "civilization"
  ]
  node [
    id 1839
    label "przejmowa&#263;"
  ]
  node [
    id 1840
    label "technika"
  ]
  node [
    id 1841
    label "kuchnia"
  ]
  node [
    id 1842
    label "rozw&#243;j"
  ]
  node [
    id 1843
    label "populace"
  ]
  node [
    id 1844
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 1845
    label "przej&#281;cie"
  ]
  node [
    id 1846
    label "przej&#261;&#263;"
  ]
  node [
    id 1847
    label "cywilizowanie"
  ]
  node [
    id 1848
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 1849
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 1850
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 1851
    label "stratification"
  ]
  node [
    id 1852
    label "lamination"
  ]
  node [
    id 1853
    label "podzia&#322;"
  ]
  node [
    id 1854
    label "elite"
  ]
  node [
    id 1855
    label "uprawienie"
  ]
  node [
    id 1856
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1857
    label "p&#322;osa"
  ]
  node [
    id 1858
    label "ziemia"
  ]
  node [
    id 1859
    label "t&#322;o"
  ]
  node [
    id 1860
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 1861
    label "gospodarstwo"
  ]
  node [
    id 1862
    label "uprawi&#263;"
  ]
  node [
    id 1863
    label "room"
  ]
  node [
    id 1864
    label "dw&#243;r"
  ]
  node [
    id 1865
    label "okazja"
  ]
  node [
    id 1866
    label "irygowanie"
  ]
  node [
    id 1867
    label "compass"
  ]
  node [
    id 1868
    label "square"
  ]
  node [
    id 1869
    label "zmienna"
  ]
  node [
    id 1870
    label "irygowa&#263;"
  ]
  node [
    id 1871
    label "socjologia"
  ]
  node [
    id 1872
    label "boisko"
  ]
  node [
    id 1873
    label "dziedzina"
  ]
  node [
    id 1874
    label "baza_danych"
  ]
  node [
    id 1875
    label "region"
  ]
  node [
    id 1876
    label "zagon"
  ]
  node [
    id 1877
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 1878
    label "plane"
  ]
  node [
    id 1879
    label "radlina"
  ]
  node [
    id 1880
    label "wiersz"
  ]
  node [
    id 1881
    label "szcz&#281;&#347;cie"
  ]
  node [
    id 1882
    label "liryka"
  ]
  node [
    id 1883
    label "ber&#380;eretka"
  ]
  node [
    id 1884
    label "wra&#380;enie"
  ]
  node [
    id 1885
    label "dobrodziejstwo"
  ]
  node [
    id 1886
    label "przypadek"
  ]
  node [
    id 1887
    label "dobro"
  ]
  node [
    id 1888
    label "przeznaczenie"
  ]
  node [
    id 1889
    label "strofoida"
  ]
  node [
    id 1890
    label "podmiot_liryczny"
  ]
  node [
    id 1891
    label "cezura"
  ]
  node [
    id 1892
    label "zwrotka"
  ]
  node [
    id 1893
    label "fragment"
  ]
  node [
    id 1894
    label "metr"
  ]
  node [
    id 1895
    label "refren"
  ]
  node [
    id 1896
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 1897
    label "rodzaj_literacki"
  ]
  node [
    id 1898
    label "wra&#380;liwo&#347;&#263;"
  ]
  node [
    id 1899
    label "nastrojowo&#347;&#263;"
  ]
  node [
    id 1900
    label "uczuciowo&#347;&#263;"
  ]
  node [
    id 1901
    label "waka"
  ]
  node [
    id 1902
    label "literatura"
  ]
  node [
    id 1903
    label "szesnastowieczny"
  ]
  node [
    id 1904
    label "piosenka"
  ]
  node [
    id 1905
    label "taniec_dworski"
  ]
  node [
    id 1906
    label "porozumie&#263;_si&#281;"
  ]
  node [
    id 1907
    label "skontaktowa&#263;"
  ]
  node [
    id 1908
    label "appoint"
  ]
  node [
    id 1909
    label "stage"
  ]
  node [
    id 1910
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1911
    label "reach"
  ]
  node [
    id 1912
    label "maple"
  ]
  node [
    id 1913
    label "czeczotka"
  ]
  node [
    id 1914
    label "klon"
  ]
  node [
    id 1915
    label "jaworzyna"
  ]
  node [
    id 1916
    label "klonowate"
  ]
  node [
    id 1917
    label "podr&#243;bka"
  ]
  node [
    id 1918
    label "knockoff"
  ]
  node [
    id 1919
    label "organizm"
  ]
  node [
    id 1920
    label "konto"
  ]
  node [
    id 1921
    label "kopia"
  ]
  node [
    id 1922
    label "skrzydlak"
  ]
  node [
    id 1923
    label "mikrokomputer"
  ]
  node [
    id 1924
    label "ptak_w&#281;drowny"
  ]
  node [
    id 1925
    label "wi&#261;z"
  ]
  node [
    id 1926
    label "ptak_&#347;piewaj&#261;cy"
  ]
  node [
    id 1927
    label "okleina"
  ]
  node [
    id 1928
    label "&#322;uskacze"
  ]
  node [
    id 1929
    label "topola"
  ]
  node [
    id 1930
    label "brzoza"
  ]
  node [
    id 1931
    label "usun&#261;&#263;"
  ]
  node [
    id 1932
    label "za&#322;atwi&#263;"
  ]
  node [
    id 1933
    label "withdraw"
  ]
  node [
    id 1934
    label "motivate"
  ]
  node [
    id 1935
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 1936
    label "wyrugowa&#263;"
  ]
  node [
    id 1937
    label "undo"
  ]
  node [
    id 1938
    label "zabi&#263;"
  ]
  node [
    id 1939
    label "przenie&#347;&#263;"
  ]
  node [
    id 1940
    label "przesun&#261;&#263;"
  ]
  node [
    id 1941
    label "doprowadzi&#263;"
  ]
  node [
    id 1942
    label "wystarczy&#263;"
  ]
  node [
    id 1943
    label "pozyska&#263;"
  ]
  node [
    id 1944
    label "plan"
  ]
  node [
    id 1945
    label "uzyska&#263;"
  ]
  node [
    id 1946
    label "serve"
  ]
  node [
    id 1947
    label "coil"
  ]
  node [
    id 1948
    label "fotoelement"
  ]
  node [
    id 1949
    label "komutowanie"
  ]
  node [
    id 1950
    label "stan_skupienia"
  ]
  node [
    id 1951
    label "nastr&#243;j"
  ]
  node [
    id 1952
    label "przerywacz"
  ]
  node [
    id 1953
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 1954
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 1955
    label "kraw&#281;d&#378;"
  ]
  node [
    id 1956
    label "obsesja"
  ]
  node [
    id 1957
    label "dw&#243;jnik"
  ]
  node [
    id 1958
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 1959
    label "okres"
  ]
  node [
    id 1960
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 1961
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1962
    label "przew&#243;d"
  ]
  node [
    id 1963
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 1964
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 1965
    label "obw&#243;d"
  ]
  node [
    id 1966
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1967
    label "degree"
  ]
  node [
    id 1968
    label "komutowa&#263;"
  ]
  node [
    id 1969
    label "podchodzi&#263;"
  ]
  node [
    id 1970
    label "&#263;wiczenie"
  ]
  node [
    id 1971
    label "pytanie"
  ]
  node [
    id 1972
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 1973
    label "kontrola"
  ]
  node [
    id 1974
    label "dydaktyka"
  ]
  node [
    id 1975
    label "spotkanie"
  ]
  node [
    id 1976
    label "pobiera&#263;"
  ]
  node [
    id 1977
    label "metal_szlachetny"
  ]
  node [
    id 1978
    label "pobranie"
  ]
  node [
    id 1979
    label "usi&#322;owanie"
  ]
  node [
    id 1980
    label "pobra&#263;"
  ]
  node [
    id 1981
    label "pobieranie"
  ]
  node [
    id 1982
    label "effort"
  ]
  node [
    id 1983
    label "analiza_chemiczna"
  ]
  node [
    id 1984
    label "item"
  ]
  node [
    id 1985
    label "probiernictwo"
  ]
  node [
    id 1986
    label "test"
  ]
  node [
    id 1987
    label "p&#243;&#322;arkusz"
  ]
  node [
    id 1988
    label "spill"
  ]
  node [
    id 1989
    label "moczy&#263;"
  ]
  node [
    id 1990
    label "zalewa&#263;"
  ]
  node [
    id 1991
    label "op&#322;ywa&#263;"
  ]
  node [
    id 1992
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 1993
    label "przegrywa&#263;"
  ]
  node [
    id 1994
    label "powleka&#263;"
  ]
  node [
    id 1995
    label "egzaminowa&#263;"
  ]
  node [
    id 1996
    label "glaze"
  ]
  node [
    id 1997
    label "la&#263;"
  ]
  node [
    id 1998
    label "barwi&#263;"
  ]
  node [
    id 1999
    label "punkt"
  ]
  node [
    id 2000
    label "maglownik"
  ]
  node [
    id 2001
    label "rozmowa"
  ]
  node [
    id 2002
    label "zak&#322;ad"
  ]
  node [
    id 2003
    label "t&#322;ok"
  ]
  node [
    id 2004
    label "przes&#322;uchanie"
  ]
  node [
    id 2005
    label "plotka"
  ]
  node [
    id 2006
    label "p&#322;ukanie"
  ]
  node [
    id 2007
    label "egzaminator"
  ]
  node [
    id 2008
    label "przegrywanie"
  ]
  node [
    id 2009
    label "pokrywanie"
  ]
  node [
    id 2010
    label "otaczanie"
  ]
  node [
    id 2011
    label "zdawanie"
  ]
  node [
    id 2012
    label "&#347;wi&#281;towanie"
  ]
  node [
    id 2013
    label "polew"
  ]
  node [
    id 2014
    label "powlekanie"
  ]
  node [
    id 2015
    label "perfusion"
  ]
  node [
    id 2016
    label "lanie"
  ]
  node [
    id 2017
    label "egzaminowanie"
  ]
  node [
    id 2018
    label "og&#243;lnokrajowy"
  ]
  node [
    id 2019
    label "og&#243;lnopolsko"
  ]
  node [
    id 2020
    label "og&#243;lnokrajowo"
  ]
  node [
    id 2021
    label "generalny"
  ]
  node [
    id 2022
    label "okre&#347;lony"
  ]
  node [
    id 2023
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 2024
    label "wiadomy"
  ]
  node [
    id 2025
    label "rozmiar&#243;wka"
  ]
  node [
    id 2026
    label "p&#322;aszczyzna"
  ]
  node [
    id 2027
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 2028
    label "kosz"
  ]
  node [
    id 2029
    label "transparent"
  ]
  node [
    id 2030
    label "rubryka"
  ]
  node [
    id 2031
    label "kontener"
  ]
  node [
    id 2032
    label "spis"
  ]
  node [
    id 2033
    label "plate"
  ]
  node [
    id 2034
    label "szachownica_Punnetta"
  ]
  node [
    id 2035
    label "chart"
  ]
  node [
    id 2036
    label "wymiar"
  ]
  node [
    id 2037
    label "surface"
  ]
  node [
    id 2038
    label "zakres"
  ]
  node [
    id 2039
    label "kwadrant"
  ]
  node [
    id 2040
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 2041
    label "ukszta&#322;towanie"
  ]
  node [
    id 2042
    label "cia&#322;o"
  ]
  node [
    id 2043
    label "p&#322;aszczak"
  ]
  node [
    id 2044
    label "sponiewieranie"
  ]
  node [
    id 2045
    label "discipline"
  ]
  node [
    id 2046
    label "kr&#261;&#380;enie"
  ]
  node [
    id 2047
    label "robienie"
  ]
  node [
    id 2048
    label "sponiewiera&#263;"
  ]
  node [
    id 2049
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 2050
    label "thing"
  ]
  node [
    id 2051
    label "catalog"
  ]
  node [
    id 2052
    label "pozycja"
  ]
  node [
    id 2053
    label "akt"
  ]
  node [
    id 2054
    label "sumariusz"
  ]
  node [
    id 2055
    label "book"
  ]
  node [
    id 2056
    label "stock"
  ]
  node [
    id 2057
    label "figurowa&#263;"
  ]
  node [
    id 2058
    label "wyliczanka"
  ]
  node [
    id 2059
    label "bin"
  ]
  node [
    id 2060
    label "pojemnik"
  ]
  node [
    id 2061
    label "kontenerowiec"
  ]
  node [
    id 2062
    label "practice"
  ]
  node [
    id 2063
    label "wykre&#347;lanie"
  ]
  node [
    id 2064
    label "element_konstrukcyjny"
  ]
  node [
    id 2065
    label "treaty"
  ]
  node [
    id 2066
    label "przestawi&#263;"
  ]
  node [
    id 2067
    label "alliance"
  ]
  node [
    id 2068
    label "ONZ"
  ]
  node [
    id 2069
    label "NATO"
  ]
  node [
    id 2070
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 2071
    label "wi&#281;&#378;"
  ]
  node [
    id 2072
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 2073
    label "traktat_wersalski"
  ]
  node [
    id 2074
    label "oznaka"
  ]
  node [
    id 2075
    label "flaga"
  ]
  node [
    id 2076
    label "tabela"
  ]
  node [
    id 2077
    label "trafienie"
  ]
  node [
    id 2078
    label "basket"
  ]
  node [
    id 2079
    label "cage"
  ]
  node [
    id 2080
    label "koszyk&#243;wka"
  ]
  node [
    id 2081
    label "fotel"
  ]
  node [
    id 2082
    label "strefa_podkoszowa"
  ]
  node [
    id 2083
    label "balon"
  ]
  node [
    id 2084
    label "obr&#281;cz"
  ]
  node [
    id 2085
    label "sicz"
  ]
  node [
    id 2086
    label "&#347;mietnik"
  ]
  node [
    id 2087
    label "pla&#380;a"
  ]
  node [
    id 2088
    label "esau&#322;"
  ]
  node [
    id 2089
    label "pannier"
  ]
  node [
    id 2090
    label "pi&#322;ka"
  ]
  node [
    id 2091
    label "przelobowa&#263;"
  ]
  node [
    id 2092
    label "zbi&#243;rka"
  ]
  node [
    id 2093
    label "koz&#322;owanie"
  ]
  node [
    id 2094
    label "ob&#243;z"
  ]
  node [
    id 2095
    label "przyczepa"
  ]
  node [
    id 2096
    label "koz&#322;owa&#263;"
  ]
  node [
    id 2097
    label "motor"
  ]
  node [
    id 2098
    label "kroki"
  ]
  node [
    id 2099
    label "ataman_koszowy"
  ]
  node [
    id 2100
    label "przelobowanie"
  ]
  node [
    id 2101
    label "wiklina"
  ]
  node [
    id 2102
    label "wsad"
  ]
  node [
    id 2103
    label "dwutakt"
  ]
  node [
    id 2104
    label "sala_gimnastyczna"
  ]
  node [
    id 2105
    label "pies_go&#324;czy"
  ]
  node [
    id 2106
    label "polownik"
  ]
  node [
    id 2107
    label "greyhound"
  ]
  node [
    id 2108
    label "pies_wy&#347;cigowy"
  ]
  node [
    id 2109
    label "szczwacz"
  ]
  node [
    id 2110
    label "wype&#322;nianie"
  ]
  node [
    id 2111
    label "heading"
  ]
  node [
    id 2112
    label "artyku&#322;"
  ]
  node [
    id 2113
    label "gentelman"
  ]
  node [
    id 2114
    label "elegant"
  ]
  node [
    id 2115
    label "narcyz"
  ]
  node [
    id 2116
    label "cognizance"
  ]
  node [
    id 2117
    label "wiedzie&#263;"
  ]
  node [
    id 2118
    label "odbiorca"
  ]
  node [
    id 2119
    label "biblioteka"
  ]
  node [
    id 2120
    label "agent_rozliczeniowy"
  ]
  node [
    id 2121
    label "komputer_cyfrowy"
  ]
  node [
    id 2122
    label "us&#322;ugobiorca"
  ]
  node [
    id 2123
    label "Rzymianin"
  ]
  node [
    id 2124
    label "szlachcic"
  ]
  node [
    id 2125
    label "obywatel"
  ]
  node [
    id 2126
    label "klientela"
  ]
  node [
    id 2127
    label "recipient_role"
  ]
  node [
    id 2128
    label "otrzymanie"
  ]
  node [
    id 2129
    label "otrzymywanie"
  ]
  node [
    id 2130
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 2131
    label "czytelnia"
  ]
  node [
    id 2132
    label "kolekcja"
  ]
  node [
    id 2133
    label "rewers"
  ]
  node [
    id 2134
    label "library"
  ]
  node [
    id 2135
    label "budynek"
  ]
  node [
    id 2136
    label "programowanie"
  ]
  node [
    id 2137
    label "informatorium"
  ]
  node [
    id 2138
    label "komcio"
  ]
  node [
    id 2139
    label "blogosfera"
  ]
  node [
    id 2140
    label "pami&#281;tnik"
  ]
  node [
    id 2141
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 2142
    label "pami&#261;tka"
  ]
  node [
    id 2143
    label "notes"
  ]
  node [
    id 2144
    label "zapiski"
  ]
  node [
    id 2145
    label "raptularz"
  ]
  node [
    id 2146
    label "utw&#243;r_epicki"
  ]
  node [
    id 2147
    label "kartka"
  ]
  node [
    id 2148
    label "logowanie"
  ]
  node [
    id 2149
    label "plik"
  ]
  node [
    id 2150
    label "adres_internetowy"
  ]
  node [
    id 2151
    label "linia"
  ]
  node [
    id 2152
    label "serwis_internetowy"
  ]
  node [
    id 2153
    label "skr&#281;canie"
  ]
  node [
    id 2154
    label "skr&#281;ca&#263;"
  ]
  node [
    id 2155
    label "orientowanie"
  ]
  node [
    id 2156
    label "skr&#281;ci&#263;"
  ]
  node [
    id 2157
    label "ty&#322;"
  ]
  node [
    id 2158
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 2159
    label "layout"
  ]
  node [
    id 2160
    label "zorientowa&#263;"
  ]
  node [
    id 2161
    label "pagina"
  ]
  node [
    id 2162
    label "g&#243;ra"
  ]
  node [
    id 2163
    label "orientowa&#263;"
  ]
  node [
    id 2164
    label "voice"
  ]
  node [
    id 2165
    label "prz&#243;d"
  ]
  node [
    id 2166
    label "internet"
  ]
  node [
    id 2167
    label "skr&#281;cenie"
  ]
  node [
    id 2168
    label "komentarz"
  ]
  node [
    id 2169
    label "podawa&#263;"
  ]
  node [
    id 2170
    label "repeat"
  ]
  node [
    id 2171
    label "podszkala&#263;_si&#281;"
  ]
  node [
    id 2172
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 2173
    label "stylizowa&#263;"
  ]
  node [
    id 2174
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 2175
    label "falowa&#263;"
  ]
  node [
    id 2176
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 2177
    label "peddle"
  ]
  node [
    id 2178
    label "wydala&#263;"
  ]
  node [
    id 2179
    label "tentegowa&#263;"
  ]
  node [
    id 2180
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 2181
    label "urz&#261;dza&#263;"
  ]
  node [
    id 2182
    label "oszukiwa&#263;"
  ]
  node [
    id 2183
    label "ukazywa&#263;"
  ]
  node [
    id 2184
    label "przerabia&#263;"
  ]
  node [
    id 2185
    label "act"
  ]
  node [
    id 2186
    label "post&#281;powa&#263;"
  ]
  node [
    id 2187
    label "tenis"
  ]
  node [
    id 2188
    label "deal"
  ]
  node [
    id 2189
    label "dawa&#263;"
  ]
  node [
    id 2190
    label "rozgrywa&#263;"
  ]
  node [
    id 2191
    label "kelner"
  ]
  node [
    id 2192
    label "siatk&#243;wka"
  ]
  node [
    id 2193
    label "cover"
  ]
  node [
    id 2194
    label "tender"
  ]
  node [
    id 2195
    label "faszerowa&#263;"
  ]
  node [
    id 2196
    label "serwowa&#263;"
  ]
  node [
    id 2197
    label "impute"
  ]
  node [
    id 2198
    label "oskar&#380;y&#263;"
  ]
  node [
    id 2199
    label "prate"
  ]
  node [
    id 2200
    label "disapprove"
  ]
  node [
    id 2201
    label "zakomunikowa&#263;"
  ]
  node [
    id 2202
    label "rachunek_operatorowy"
  ]
  node [
    id 2203
    label "kryptologia"
  ]
  node [
    id 2204
    label "logicyzm"
  ]
  node [
    id 2205
    label "logika"
  ]
  node [
    id 2206
    label "matematyka_czysta"
  ]
  node [
    id 2207
    label "forsing"
  ]
  node [
    id 2208
    label "supremum"
  ]
  node [
    id 2209
    label "modelowanie_matematyczne"
  ]
  node [
    id 2210
    label "matma"
  ]
  node [
    id 2211
    label "teoria_katastrof"
  ]
  node [
    id 2212
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 2213
    label "fizyka_matematyczna"
  ]
  node [
    id 2214
    label "teoria_graf&#243;w"
  ]
  node [
    id 2215
    label "rzut"
  ]
  node [
    id 2216
    label "rachunki"
  ]
  node [
    id 2217
    label "topologia_algebraiczna"
  ]
  node [
    id 2218
    label "matematyka_stosowana"
  ]
  node [
    id 2219
    label "funkcja"
  ]
  node [
    id 2220
    label "infimum"
  ]
  node [
    id 2221
    label "przebieg"
  ]
  node [
    id 2222
    label "studia"
  ]
  node [
    id 2223
    label "arithmetic"
  ]
  node [
    id 2224
    label "logicism"
  ]
  node [
    id 2225
    label "doktryna_filozoficzna"
  ]
  node [
    id 2226
    label "filozofia_matematyki"
  ]
  node [
    id 2227
    label "ograniczenie"
  ]
  node [
    id 2228
    label "addytywno&#347;&#263;"
  ]
  node [
    id 2229
    label "function"
  ]
  node [
    id 2230
    label "zastosowanie"
  ]
  node [
    id 2231
    label "funkcjonowanie"
  ]
  node [
    id 2232
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 2233
    label "powierzanie"
  ]
  node [
    id 2234
    label "przeciwdziedzina"
  ]
  node [
    id 2235
    label "wakowa&#263;"
  ]
  node [
    id 2236
    label "postawi&#263;"
  ]
  node [
    id 2237
    label "armia"
  ]
  node [
    id 2238
    label "nawr&#243;t_choroby"
  ]
  node [
    id 2239
    label "potomstwo"
  ]
  node [
    id 2240
    label "odwzorowanie"
  ]
  node [
    id 2241
    label "rysunek"
  ]
  node [
    id 2242
    label "scene"
  ]
  node [
    id 2243
    label "throw"
  ]
  node [
    id 2244
    label "projection"
  ]
  node [
    id 2245
    label "injection"
  ]
  node [
    id 2246
    label "k&#322;ad"
  ]
  node [
    id 2247
    label "mold"
  ]
  node [
    id 2248
    label "przyswoi&#263;"
  ]
  node [
    id 2249
    label "one"
  ]
  node [
    id 2250
    label "ewoluowanie"
  ]
  node [
    id 2251
    label "skala"
  ]
  node [
    id 2252
    label "przyswajanie"
  ]
  node [
    id 2253
    label "wyewoluowanie"
  ]
  node [
    id 2254
    label "przeliczy&#263;"
  ]
  node [
    id 2255
    label "wyewoluowa&#263;"
  ]
  node [
    id 2256
    label "ewoluowa&#263;"
  ]
  node [
    id 2257
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 2258
    label "liczba_naturalna"
  ]
  node [
    id 2259
    label "czynnik_biotyczny"
  ]
  node [
    id 2260
    label "individual"
  ]
  node [
    id 2261
    label "przyswaja&#263;"
  ]
  node [
    id 2262
    label "przyswojenie"
  ]
  node [
    id 2263
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 2264
    label "starzenie_si&#281;"
  ]
  node [
    id 2265
    label "przeliczanie"
  ]
  node [
    id 2266
    label "przelicza&#263;"
  ]
  node [
    id 2267
    label "przeliczenie"
  ]
  node [
    id 2268
    label "izomorfizm"
  ]
  node [
    id 2269
    label "teoremat"
  ]
  node [
    id 2270
    label "logizacja"
  ]
  node [
    id 2271
    label "m&#261;dro&#347;&#263;"
  ]
  node [
    id 2272
    label "analityka"
  ]
  node [
    id 2273
    label "predykat"
  ]
  node [
    id 2274
    label "rozumno&#347;&#263;"
  ]
  node [
    id 2275
    label "metamatematyka"
  ]
  node [
    id 2276
    label "filozofia"
  ]
  node [
    id 2277
    label "operacja_logiczna"
  ]
  node [
    id 2278
    label "sylogistyka"
  ]
  node [
    id 2279
    label "dialektyka"
  ]
  node [
    id 2280
    label "podzia&#322;_logiczny"
  ]
  node [
    id 2281
    label "logistics"
  ]
  node [
    id 2282
    label "informatyka"
  ]
  node [
    id 2283
    label "kryptografia"
  ]
  node [
    id 2284
    label "kryptoanaliza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 250
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 17
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 627
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 631
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 633
  ]
  edge [
    source 14
    target 634
  ]
  edge [
    source 14
    target 635
  ]
  edge [
    source 14
    target 636
  ]
  edge [
    source 14
    target 637
  ]
  edge [
    source 14
    target 638
  ]
  edge [
    source 14
    target 639
  ]
  edge [
    source 14
    target 640
  ]
  edge [
    source 14
    target 641
  ]
  edge [
    source 14
    target 642
  ]
  edge [
    source 14
    target 643
  ]
  edge [
    source 14
    target 644
  ]
  edge [
    source 14
    target 645
  ]
  edge [
    source 14
    target 646
  ]
  edge [
    source 14
    target 647
  ]
  edge [
    source 14
    target 648
  ]
  edge [
    source 14
    target 649
  ]
  edge [
    source 14
    target 650
  ]
  edge [
    source 14
    target 651
  ]
  edge [
    source 14
    target 652
  ]
  edge [
    source 14
    target 653
  ]
  edge [
    source 14
    target 654
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 656
  ]
  edge [
    source 14
    target 657
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 668
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 671
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 673
  ]
  edge [
    source 14
    target 674
  ]
  edge [
    source 14
    target 675
  ]
  edge [
    source 14
    target 676
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 678
  ]
  edge [
    source 14
    target 679
  ]
  edge [
    source 14
    target 680
  ]
  edge [
    source 14
    target 681
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 684
  ]
  edge [
    source 14
    target 685
  ]
  edge [
    source 14
    target 686
  ]
  edge [
    source 14
    target 224
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 689
  ]
  edge [
    source 14
    target 690
  ]
  edge [
    source 14
    target 691
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 694
  ]
  edge [
    source 14
    target 695
  ]
  edge [
    source 14
    target 696
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 697
  ]
  edge [
    source 14
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 407
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 635
  ]
  edge [
    source 17
    target 636
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 638
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 640
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 649
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 569
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 554
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 465
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 33
  ]
  edge [
    source 17
    target 45
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 776
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 101
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 58
  ]
  edge [
    source 19
    target 59
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 969
  ]
  edge [
    source 20
    target 970
  ]
  edge [
    source 20
    target 971
  ]
  edge [
    source 20
    target 972
  ]
  edge [
    source 20
    target 973
  ]
  edge [
    source 20
    target 974
  ]
  edge [
    source 20
    target 975
  ]
  edge [
    source 20
    target 976
  ]
  edge [
    source 20
    target 977
  ]
  edge [
    source 20
    target 978
  ]
  edge [
    source 20
    target 979
  ]
  edge [
    source 20
    target 836
  ]
  edge [
    source 20
    target 980
  ]
  edge [
    source 20
    target 981
  ]
  edge [
    source 20
    target 982
  ]
  edge [
    source 20
    target 983
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 984
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 985
  ]
  edge [
    source 23
    target 986
  ]
  edge [
    source 23
    target 987
  ]
  edge [
    source 23
    target 988
  ]
  edge [
    source 23
    target 125
  ]
  edge [
    source 23
    target 989
  ]
  edge [
    source 23
    target 990
  ]
  edge [
    source 23
    target 991
  ]
  edge [
    source 23
    target 276
  ]
  edge [
    source 23
    target 992
  ]
  edge [
    source 23
    target 993
  ]
  edge [
    source 23
    target 994
  ]
  edge [
    source 23
    target 995
  ]
  edge [
    source 23
    target 996
  ]
  edge [
    source 23
    target 997
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 998
  ]
  edge [
    source 23
    target 999
  ]
  edge [
    source 23
    target 1000
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 934
  ]
  edge [
    source 23
    target 1001
  ]
  edge [
    source 23
    target 1002
  ]
  edge [
    source 23
    target 1003
  ]
  edge [
    source 23
    target 464
  ]
  edge [
    source 23
    target 1004
  ]
  edge [
    source 23
    target 1005
  ]
  edge [
    source 23
    target 1006
  ]
  edge [
    source 23
    target 1007
  ]
  edge [
    source 23
    target 1008
  ]
  edge [
    source 23
    target 613
  ]
  edge [
    source 23
    target 958
  ]
  edge [
    source 23
    target 966
  ]
  edge [
    source 23
    target 1009
  ]
  edge [
    source 23
    target 721
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 610
  ]
  edge [
    source 23
    target 1011
  ]
  edge [
    source 23
    target 1012
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 1013
  ]
  edge [
    source 23
    target 1014
  ]
  edge [
    source 23
    target 1015
  ]
  edge [
    source 23
    target 1016
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 1018
  ]
  edge [
    source 23
    target 718
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 524
  ]
  edge [
    source 23
    target 939
  ]
  edge [
    source 23
    target 1020
  ]
  edge [
    source 23
    target 1021
  ]
  edge [
    source 23
    target 1022
  ]
  edge [
    source 23
    target 880
  ]
  edge [
    source 23
    target 1023
  ]
  edge [
    source 23
    target 1024
  ]
  edge [
    source 23
    target 1025
  ]
  edge [
    source 23
    target 1026
  ]
  edge [
    source 23
    target 945
  ]
  edge [
    source 23
    target 889
  ]
  edge [
    source 23
    target 1027
  ]
  edge [
    source 23
    target 1028
  ]
  edge [
    source 23
    target 1029
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 1030
  ]
  edge [
    source 23
    target 1031
  ]
  edge [
    source 23
    target 968
  ]
  edge [
    source 23
    target 949
  ]
  edge [
    source 23
    target 1032
  ]
  edge [
    source 23
    target 1033
  ]
  edge [
    source 23
    target 327
  ]
  edge [
    source 23
    target 1034
  ]
  edge [
    source 23
    target 897
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 280
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 1042
  ]
  edge [
    source 23
    target 1043
  ]
  edge [
    source 23
    target 1044
  ]
  edge [
    source 23
    target 1045
  ]
  edge [
    source 23
    target 1046
  ]
  edge [
    source 23
    target 1047
  ]
  edge [
    source 23
    target 1048
  ]
  edge [
    source 23
    target 1049
  ]
  edge [
    source 23
    target 1050
  ]
  edge [
    source 23
    target 752
  ]
  edge [
    source 23
    target 1051
  ]
  edge [
    source 23
    target 1052
  ]
  edge [
    source 23
    target 1053
  ]
  edge [
    source 23
    target 936
  ]
  edge [
    source 23
    target 1054
  ]
  edge [
    source 23
    target 1055
  ]
  edge [
    source 23
    target 69
  ]
  edge [
    source 23
    target 377
  ]
  edge [
    source 23
    target 1056
  ]
  edge [
    source 23
    target 1057
  ]
  edge [
    source 23
    target 1058
  ]
  edge [
    source 23
    target 1059
  ]
  edge [
    source 23
    target 1060
  ]
  edge [
    source 23
    target 881
  ]
  edge [
    source 23
    target 1061
  ]
  edge [
    source 23
    target 871
  ]
  edge [
    source 23
    target 1062
  ]
  edge [
    source 23
    target 1063
  ]
  edge [
    source 23
    target 259
  ]
  edge [
    source 23
    target 1064
  ]
  edge [
    source 23
    target 1065
  ]
  edge [
    source 23
    target 1066
  ]
  edge [
    source 23
    target 1067
  ]
  edge [
    source 23
    target 1068
  ]
  edge [
    source 23
    target 1069
  ]
  edge [
    source 23
    target 1070
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 1071
  ]
  edge [
    source 23
    target 1072
  ]
  edge [
    source 23
    target 1073
  ]
  edge [
    source 23
    target 1074
  ]
  edge [
    source 23
    target 1075
  ]
  edge [
    source 23
    target 1076
  ]
  edge [
    source 23
    target 1077
  ]
  edge [
    source 23
    target 1078
  ]
  edge [
    source 23
    target 1079
  ]
  edge [
    source 23
    target 1080
  ]
  edge [
    source 23
    target 1081
  ]
  edge [
    source 23
    target 1082
  ]
  edge [
    source 23
    target 293
  ]
  edge [
    source 23
    target 867
  ]
  edge [
    source 23
    target 1083
  ]
  edge [
    source 23
    target 1084
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 526
  ]
  edge [
    source 23
    target 1086
  ]
  edge [
    source 23
    target 1087
  ]
  edge [
    source 23
    target 823
  ]
  edge [
    source 23
    target 1088
  ]
  edge [
    source 23
    target 1089
  ]
  edge [
    source 23
    target 1090
  ]
  edge [
    source 23
    target 979
  ]
  edge [
    source 23
    target 659
  ]
  edge [
    source 23
    target 1091
  ]
  edge [
    source 23
    target 1092
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 1094
  ]
  edge [
    source 23
    target 1095
  ]
  edge [
    source 23
    target 797
  ]
  edge [
    source 23
    target 1096
  ]
  edge [
    source 23
    target 1097
  ]
  edge [
    source 23
    target 1098
  ]
  edge [
    source 23
    target 639
  ]
  edge [
    source 23
    target 1099
  ]
  edge [
    source 23
    target 1100
  ]
  edge [
    source 23
    target 1101
  ]
  edge [
    source 23
    target 1102
  ]
  edge [
    source 23
    target 806
  ]
  edge [
    source 23
    target 1103
  ]
  edge [
    source 23
    target 1104
  ]
  edge [
    source 23
    target 840
  ]
  edge [
    source 23
    target 1105
  ]
  edge [
    source 23
    target 1106
  ]
  edge [
    source 23
    target 1107
  ]
  edge [
    source 23
    target 1108
  ]
  edge [
    source 23
    target 314
  ]
  edge [
    source 23
    target 1109
  ]
  edge [
    source 23
    target 1110
  ]
  edge [
    source 23
    target 1111
  ]
  edge [
    source 23
    target 1112
  ]
  edge [
    source 23
    target 1113
  ]
  edge [
    source 23
    target 497
  ]
  edge [
    source 23
    target 1114
  ]
  edge [
    source 23
    target 1115
  ]
  edge [
    source 23
    target 1116
  ]
  edge [
    source 23
    target 1117
  ]
  edge [
    source 23
    target 1118
  ]
  edge [
    source 23
    target 1119
  ]
  edge [
    source 23
    target 1120
  ]
  edge [
    source 23
    target 286
  ]
  edge [
    source 23
    target 1121
  ]
  edge [
    source 23
    target 1122
  ]
  edge [
    source 23
    target 878
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 604
  ]
  edge [
    source 23
    target 570
  ]
  edge [
    source 23
    target 34
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 1130
  ]
  edge [
    source 24
    target 1131
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 24
    target 1143
  ]
  edge [
    source 24
    target 1144
  ]
  edge [
    source 24
    target 249
  ]
  edge [
    source 24
    target 1145
  ]
  edge [
    source 24
    target 47
  ]
  edge [
    source 24
    target 1146
  ]
  edge [
    source 24
    target 1147
  ]
  edge [
    source 24
    target 1148
  ]
  edge [
    source 24
    target 1149
  ]
  edge [
    source 24
    target 1150
  ]
  edge [
    source 24
    target 1151
  ]
  edge [
    source 24
    target 1152
  ]
  edge [
    source 24
    target 1153
  ]
  edge [
    source 24
    target 1154
  ]
  edge [
    source 24
    target 1155
  ]
  edge [
    source 24
    target 1156
  ]
  edge [
    source 24
    target 1157
  ]
  edge [
    source 24
    target 1158
  ]
  edge [
    source 24
    target 1159
  ]
  edge [
    source 24
    target 1160
  ]
  edge [
    source 24
    target 1161
  ]
  edge [
    source 24
    target 1162
  ]
  edge [
    source 24
    target 1163
  ]
  edge [
    source 24
    target 1164
  ]
  edge [
    source 24
    target 1165
  ]
  edge [
    source 24
    target 1166
  ]
  edge [
    source 24
    target 1167
  ]
  edge [
    source 24
    target 1168
  ]
  edge [
    source 24
    target 1169
  ]
  edge [
    source 24
    target 613
  ]
  edge [
    source 24
    target 1170
  ]
  edge [
    source 24
    target 1171
  ]
  edge [
    source 24
    target 1172
  ]
  edge [
    source 24
    target 1173
  ]
  edge [
    source 24
    target 1174
  ]
  edge [
    source 24
    target 1175
  ]
  edge [
    source 24
    target 1176
  ]
  edge [
    source 24
    target 1177
  ]
  edge [
    source 24
    target 1178
  ]
  edge [
    source 24
    target 1179
  ]
  edge [
    source 24
    target 1180
  ]
  edge [
    source 24
    target 1181
  ]
  edge [
    source 24
    target 1182
  ]
  edge [
    source 24
    target 1183
  ]
  edge [
    source 24
    target 1184
  ]
  edge [
    source 24
    target 1185
  ]
  edge [
    source 24
    target 1186
  ]
  edge [
    source 24
    target 1187
  ]
  edge [
    source 24
    target 1188
  ]
  edge [
    source 24
    target 1189
  ]
  edge [
    source 24
    target 1190
  ]
  edge [
    source 24
    target 1191
  ]
  edge [
    source 24
    target 1192
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 1201
  ]
  edge [
    source 24
    target 1202
  ]
  edge [
    source 24
    target 1203
  ]
  edge [
    source 24
    target 1204
  ]
  edge [
    source 24
    target 1205
  ]
  edge [
    source 24
    target 1206
  ]
  edge [
    source 24
    target 1207
  ]
  edge [
    source 24
    target 1208
  ]
  edge [
    source 24
    target 1209
  ]
  edge [
    source 24
    target 1210
  ]
  edge [
    source 24
    target 1211
  ]
  edge [
    source 24
    target 1212
  ]
  edge [
    source 24
    target 1213
  ]
  edge [
    source 24
    target 1214
  ]
  edge [
    source 24
    target 1215
  ]
  edge [
    source 24
    target 1216
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 24
    target 1219
  ]
  edge [
    source 24
    target 1220
  ]
  edge [
    source 24
    target 1221
  ]
  edge [
    source 24
    target 1222
  ]
  edge [
    source 24
    target 1223
  ]
  edge [
    source 24
    target 1224
  ]
  edge [
    source 24
    target 1225
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1226
  ]
  edge [
    source 25
    target 1227
  ]
  edge [
    source 25
    target 1228
  ]
  edge [
    source 25
    target 1229
  ]
  edge [
    source 25
    target 1015
  ]
  edge [
    source 25
    target 1230
  ]
  edge [
    source 25
    target 1231
  ]
  edge [
    source 25
    target 613
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 420
  ]
  edge [
    source 25
    target 524
  ]
  edge [
    source 25
    target 1233
  ]
  edge [
    source 25
    target 286
  ]
  edge [
    source 25
    target 464
  ]
  edge [
    source 25
    target 514
  ]
  edge [
    source 25
    target 395
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 269
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 622
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 727
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 508
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 407
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 411
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 463
  ]
  edge [
    source 25
    target 465
  ]
  edge [
    source 25
    target 466
  ]
  edge [
    source 25
    target 447
  ]
  edge [
    source 25
    target 448
  ]
  edge [
    source 25
    target 268
  ]
  edge [
    source 25
    target 449
  ]
  edge [
    source 25
    target 450
  ]
  edge [
    source 25
    target 451
  ]
  edge [
    source 25
    target 452
  ]
  edge [
    source 25
    target 453
  ]
  edge [
    source 25
    target 454
  ]
  edge [
    source 25
    target 455
  ]
  edge [
    source 25
    target 456
  ]
  edge [
    source 25
    target 457
  ]
  edge [
    source 25
    target 458
  ]
  edge [
    source 25
    target 459
  ]
  edge [
    source 25
    target 460
  ]
  edge [
    source 25
    target 461
  ]
  edge [
    source 25
    target 462
  ]
  edge [
    source 25
    target 504
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 1013
  ]
  edge [
    source 25
    target 125
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 396
  ]
  edge [
    source 25
    target 285
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 25
    target 259
  ]
  edge [
    source 25
    target 1257
  ]
  edge [
    source 25
    target 472
  ]
  edge [
    source 25
    target 1258
  ]
  edge [
    source 25
    target 1259
  ]
  edge [
    source 25
    target 275
  ]
  edge [
    source 25
    target 1260
  ]
  edge [
    source 25
    target 615
  ]
  edge [
    source 25
    target 1261
  ]
  edge [
    source 25
    target 1262
  ]
  edge [
    source 25
    target 1263
  ]
  edge [
    source 25
    target 1264
  ]
  edge [
    source 25
    target 1265
  ]
  edge [
    source 25
    target 1266
  ]
  edge [
    source 25
    target 1267
  ]
  edge [
    source 25
    target 621
  ]
  edge [
    source 25
    target 1268
  ]
  edge [
    source 25
    target 1269
  ]
  edge [
    source 25
    target 1270
  ]
  edge [
    source 25
    target 1271
  ]
  edge [
    source 25
    target 1272
  ]
  edge [
    source 25
    target 1273
  ]
  edge [
    source 25
    target 1274
  ]
  edge [
    source 25
    target 1275
  ]
  edge [
    source 25
    target 1276
  ]
  edge [
    source 25
    target 1277
  ]
  edge [
    source 25
    target 1278
  ]
  edge [
    source 25
    target 1279
  ]
  edge [
    source 25
    target 154
  ]
  edge [
    source 25
    target 1280
  ]
  edge [
    source 25
    target 1281
  ]
  edge [
    source 25
    target 1282
  ]
  edge [
    source 25
    target 751
  ]
  edge [
    source 25
    target 1283
  ]
  edge [
    source 25
    target 774
  ]
  edge [
    source 25
    target 1284
  ]
  edge [
    source 25
    target 551
  ]
  edge [
    source 25
    target 1285
  ]
  edge [
    source 25
    target 1286
  ]
  edge [
    source 25
    target 1133
  ]
  edge [
    source 25
    target 1287
  ]
  edge [
    source 25
    target 1288
  ]
  edge [
    source 25
    target 1120
  ]
  edge [
    source 25
    target 1289
  ]
  edge [
    source 25
    target 1290
  ]
  edge [
    source 25
    target 1291
  ]
  edge [
    source 25
    target 1292
  ]
  edge [
    source 25
    target 1293
  ]
  edge [
    source 25
    target 1294
  ]
  edge [
    source 25
    target 1295
  ]
  edge [
    source 25
    target 1296
  ]
  edge [
    source 25
    target 565
  ]
  edge [
    source 25
    target 1297
  ]
  edge [
    source 25
    target 1298
  ]
  edge [
    source 25
    target 1299
  ]
  edge [
    source 25
    target 1300
  ]
  edge [
    source 25
    target 1301
  ]
  edge [
    source 25
    target 1302
  ]
  edge [
    source 25
    target 1303
  ]
  edge [
    source 25
    target 1304
  ]
  edge [
    source 25
    target 1305
  ]
  edge [
    source 25
    target 1306
  ]
  edge [
    source 25
    target 1307
  ]
  edge [
    source 25
    target 1308
  ]
  edge [
    source 25
    target 1309
  ]
  edge [
    source 25
    target 1310
  ]
  edge [
    source 25
    target 1311
  ]
  edge [
    source 25
    target 1312
  ]
  edge [
    source 25
    target 1313
  ]
  edge [
    source 25
    target 1314
  ]
  edge [
    source 25
    target 677
  ]
  edge [
    source 25
    target 1315
  ]
  edge [
    source 25
    target 1316
  ]
  edge [
    source 25
    target 1317
  ]
  edge [
    source 25
    target 1017
  ]
  edge [
    source 25
    target 1318
  ]
  edge [
    source 25
    target 1319
  ]
  edge [
    source 25
    target 1320
  ]
  edge [
    source 25
    target 1321
  ]
  edge [
    source 25
    target 1322
  ]
  edge [
    source 25
    target 1323
  ]
  edge [
    source 25
    target 1324
  ]
  edge [
    source 25
    target 236
  ]
  edge [
    source 25
    target 1112
  ]
  edge [
    source 25
    target 1325
  ]
  edge [
    source 25
    target 1326
  ]
  edge [
    source 25
    target 1327
  ]
  edge [
    source 25
    target 1328
  ]
  edge [
    source 25
    target 716
  ]
  edge [
    source 25
    target 1329
  ]
  edge [
    source 25
    target 1330
  ]
  edge [
    source 25
    target 510
  ]
  edge [
    source 25
    target 1331
  ]
  edge [
    source 25
    target 1332
  ]
  edge [
    source 25
    target 1333
  ]
  edge [
    source 25
    target 1334
  ]
  edge [
    source 25
    target 363
  ]
  edge [
    source 25
    target 1335
  ]
  edge [
    source 25
    target 1336
  ]
  edge [
    source 25
    target 1337
  ]
  edge [
    source 25
    target 1338
  ]
  edge [
    source 25
    target 1339
  ]
  edge [
    source 25
    target 1340
  ]
  edge [
    source 25
    target 1341
  ]
  edge [
    source 25
    target 1342
  ]
  edge [
    source 25
    target 1343
  ]
  edge [
    source 25
    target 1344
  ]
  edge [
    source 25
    target 1345
  ]
  edge [
    source 25
    target 1346
  ]
  edge [
    source 25
    target 1347
  ]
  edge [
    source 25
    target 1348
  ]
  edge [
    source 25
    target 1349
  ]
  edge [
    source 25
    target 1350
  ]
  edge [
    source 25
    target 1351
  ]
  edge [
    source 25
    target 1352
  ]
  edge [
    source 25
    target 1353
  ]
  edge [
    source 25
    target 1354
  ]
  edge [
    source 25
    target 570
  ]
  edge [
    source 25
    target 1355
  ]
  edge [
    source 25
    target 1356
  ]
  edge [
    source 25
    target 1357
  ]
  edge [
    source 25
    target 1358
  ]
  edge [
    source 25
    target 1359
  ]
  edge [
    source 25
    target 1360
  ]
  edge [
    source 25
    target 1361
  ]
  edge [
    source 25
    target 897
  ]
  edge [
    source 25
    target 1362
  ]
  edge [
    source 25
    target 1363
  ]
  edge [
    source 25
    target 1364
  ]
  edge [
    source 25
    target 1365
  ]
  edge [
    source 25
    target 1366
  ]
  edge [
    source 25
    target 1367
  ]
  edge [
    source 25
    target 911
  ]
  edge [
    source 25
    target 1039
  ]
  edge [
    source 25
    target 349
  ]
  edge [
    source 25
    target 1368
  ]
  edge [
    source 25
    target 1369
  ]
  edge [
    source 25
    target 1370
  ]
  edge [
    source 25
    target 1371
  ]
  edge [
    source 25
    target 993
  ]
  edge [
    source 25
    target 1372
  ]
  edge [
    source 25
    target 1373
  ]
  edge [
    source 25
    target 1374
  ]
  edge [
    source 25
    target 1375
  ]
  edge [
    source 25
    target 1376
  ]
  edge [
    source 25
    target 1377
  ]
  edge [
    source 25
    target 1378
  ]
  edge [
    source 25
    target 1379
  ]
  edge [
    source 25
    target 1380
  ]
  edge [
    source 25
    target 1381
  ]
  edge [
    source 25
    target 1382
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 423
  ]
  edge [
    source 25
    target 1383
  ]
  edge [
    source 25
    target 1384
  ]
  edge [
    source 25
    target 1385
  ]
  edge [
    source 25
    target 1386
  ]
  edge [
    source 25
    target 1387
  ]
  edge [
    source 25
    target 1388
  ]
  edge [
    source 25
    target 1389
  ]
  edge [
    source 25
    target 1390
  ]
  edge [
    source 25
    target 700
  ]
  edge [
    source 25
    target 1391
  ]
  edge [
    source 25
    target 1392
  ]
  edge [
    source 25
    target 550
  ]
  edge [
    source 25
    target 1393
  ]
  edge [
    source 25
    target 1394
  ]
  edge [
    source 25
    target 384
  ]
  edge [
    source 25
    target 602
  ]
  edge [
    source 25
    target 1395
  ]
  edge [
    source 25
    target 949
  ]
  edge [
    source 25
    target 1396
  ]
  edge [
    source 25
    target 1397
  ]
  edge [
    source 25
    target 350
  ]
  edge [
    source 25
    target 1398
  ]
  edge [
    source 25
    target 1399
  ]
  edge [
    source 25
    target 1400
  ]
  edge [
    source 25
    target 1401
  ]
  edge [
    source 25
    target 1402
  ]
  edge [
    source 25
    target 1403
  ]
  edge [
    source 25
    target 1404
  ]
  edge [
    source 25
    target 1405
  ]
  edge [
    source 25
    target 1406
  ]
  edge [
    source 25
    target 1407
  ]
  edge [
    source 25
    target 1408
  ]
  edge [
    source 25
    target 1409
  ]
  edge [
    source 25
    target 1410
  ]
  edge [
    source 25
    target 1411
  ]
  edge [
    source 25
    target 1412
  ]
  edge [
    source 25
    target 1413
  ]
  edge [
    source 25
    target 1414
  ]
  edge [
    source 25
    target 1415
  ]
  edge [
    source 25
    target 1416
  ]
  edge [
    source 25
    target 1417
  ]
  edge [
    source 25
    target 731
  ]
  edge [
    source 25
    target 1418
  ]
  edge [
    source 25
    target 1419
  ]
  edge [
    source 25
    target 1420
  ]
  edge [
    source 25
    target 1421
  ]
  edge [
    source 25
    target 1422
  ]
  edge [
    source 25
    target 562
  ]
  edge [
    source 25
    target 1423
  ]
  edge [
    source 25
    target 881
  ]
  edge [
    source 25
    target 1424
  ]
  edge [
    source 25
    target 547
  ]
  edge [
    source 25
    target 1425
  ]
  edge [
    source 25
    target 1426
  ]
  edge [
    source 25
    target 1427
  ]
  edge [
    source 25
    target 1428
  ]
  edge [
    source 25
    target 1429
  ]
  edge [
    source 25
    target 1430
  ]
  edge [
    source 25
    target 1431
  ]
  edge [
    source 25
    target 1432
  ]
  edge [
    source 25
    target 1433
  ]
  edge [
    source 25
    target 1434
  ]
  edge [
    source 25
    target 1435
  ]
  edge [
    source 25
    target 1436
  ]
  edge [
    source 25
    target 1437
  ]
  edge [
    source 25
    target 1438
  ]
  edge [
    source 25
    target 1439
  ]
  edge [
    source 25
    target 1440
  ]
  edge [
    source 25
    target 1441
  ]
  edge [
    source 25
    target 1442
  ]
  edge [
    source 25
    target 1443
  ]
  edge [
    source 25
    target 1444
  ]
  edge [
    source 25
    target 1123
  ]
  edge [
    source 25
    target 1445
  ]
  edge [
    source 25
    target 1446
  ]
  edge [
    source 25
    target 1447
  ]
  edge [
    source 25
    target 354
  ]
  edge [
    source 25
    target 1448
  ]
  edge [
    source 25
    target 1449
  ]
  edge [
    source 25
    target 730
  ]
  edge [
    source 25
    target 1450
  ]
  edge [
    source 25
    target 1451
  ]
  edge [
    source 25
    target 1452
  ]
  edge [
    source 25
    target 1453
  ]
  edge [
    source 25
    target 1454
  ]
  edge [
    source 25
    target 33
  ]
  edge [
    source 25
    target 50
  ]
  edge [
    source 25
    target 61
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1455
  ]
  edge [
    source 26
    target 1456
  ]
  edge [
    source 26
    target 1457
  ]
  edge [
    source 26
    target 1458
  ]
  edge [
    source 26
    target 1459
  ]
  edge [
    source 26
    target 1460
  ]
  edge [
    source 26
    target 1461
  ]
  edge [
    source 26
    target 1462
  ]
  edge [
    source 26
    target 1463
  ]
  edge [
    source 26
    target 1464
  ]
  edge [
    source 26
    target 1465
  ]
  edge [
    source 26
    target 1466
  ]
  edge [
    source 26
    target 1467
  ]
  edge [
    source 26
    target 1468
  ]
  edge [
    source 26
    target 1469
  ]
  edge [
    source 26
    target 1470
  ]
  edge [
    source 26
    target 1471
  ]
  edge [
    source 26
    target 1472
  ]
  edge [
    source 26
    target 524
  ]
  edge [
    source 26
    target 1443
  ]
  edge [
    source 26
    target 1473
  ]
  edge [
    source 26
    target 1474
  ]
  edge [
    source 26
    target 1475
  ]
  edge [
    source 26
    target 1476
  ]
  edge [
    source 26
    target 1477
  ]
  edge [
    source 26
    target 1478
  ]
  edge [
    source 26
    target 1479
  ]
  edge [
    source 26
    target 1449
  ]
  edge [
    source 26
    target 1480
  ]
  edge [
    source 26
    target 1481
  ]
  edge [
    source 26
    target 1482
  ]
  edge [
    source 26
    target 1483
  ]
  edge [
    source 26
    target 1484
  ]
  edge [
    source 26
    target 1485
  ]
  edge [
    source 26
    target 1486
  ]
  edge [
    source 26
    target 1487
  ]
  edge [
    source 26
    target 1488
  ]
  edge [
    source 26
    target 1489
  ]
  edge [
    source 26
    target 1490
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1491
  ]
  edge [
    source 28
    target 1492
  ]
  edge [
    source 28
    target 1493
  ]
  edge [
    source 28
    target 1494
  ]
  edge [
    source 28
    target 1495
  ]
  edge [
    source 28
    target 1496
  ]
  edge [
    source 28
    target 565
  ]
  edge [
    source 28
    target 1497
  ]
  edge [
    source 28
    target 1498
  ]
  edge [
    source 28
    target 1499
  ]
  edge [
    source 28
    target 1500
  ]
  edge [
    source 28
    target 1501
  ]
  edge [
    source 28
    target 1502
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1503
  ]
  edge [
    source 29
    target 1112
  ]
  edge [
    source 29
    target 1504
  ]
  edge [
    source 29
    target 871
  ]
  edge [
    source 29
    target 430
  ]
  edge [
    source 29
    target 1505
  ]
  edge [
    source 29
    target 1008
  ]
  edge [
    source 29
    target 1506
  ]
  edge [
    source 29
    target 1067
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1507
  ]
  edge [
    source 30
    target 1508
  ]
  edge [
    source 30
    target 1509
  ]
  edge [
    source 30
    target 1510
  ]
  edge [
    source 30
    target 1511
  ]
  edge [
    source 30
    target 1512
  ]
  edge [
    source 30
    target 1513
  ]
  edge [
    source 30
    target 1514
  ]
  edge [
    source 30
    target 1515
  ]
  edge [
    source 30
    target 565
  ]
  edge [
    source 30
    target 1516
  ]
  edge [
    source 30
    target 1517
  ]
  edge [
    source 30
    target 1518
  ]
  edge [
    source 30
    target 1519
  ]
  edge [
    source 30
    target 1520
  ]
  edge [
    source 30
    target 1521
  ]
  edge [
    source 30
    target 1522
  ]
  edge [
    source 30
    target 1523
  ]
  edge [
    source 30
    target 1524
  ]
  edge [
    source 30
    target 525
  ]
  edge [
    source 30
    target 1525
  ]
  edge [
    source 30
    target 1526
  ]
  edge [
    source 30
    target 1527
  ]
  edge [
    source 30
    target 700
  ]
  edge [
    source 30
    target 1528
  ]
  edge [
    source 30
    target 1529
  ]
  edge [
    source 30
    target 1530
  ]
  edge [
    source 30
    target 1531
  ]
  edge [
    source 30
    target 1532
  ]
  edge [
    source 30
    target 1533
  ]
  edge [
    source 30
    target 1534
  ]
  edge [
    source 30
    target 1535
  ]
  edge [
    source 30
    target 1536
  ]
  edge [
    source 30
    target 1537
  ]
  edge [
    source 30
    target 1538
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 1539
  ]
  edge [
    source 31
    target 1540
  ]
  edge [
    source 31
    target 1541
  ]
  edge [
    source 31
    target 1542
  ]
  edge [
    source 31
    target 1543
  ]
  edge [
    source 31
    target 1544
  ]
  edge [
    source 31
    target 1545
  ]
  edge [
    source 31
    target 1546
  ]
  edge [
    source 31
    target 1547
  ]
  edge [
    source 31
    target 1548
  ]
  edge [
    source 31
    target 1549
  ]
  edge [
    source 31
    target 1550
  ]
  edge [
    source 31
    target 1551
  ]
  edge [
    source 31
    target 1552
  ]
  edge [
    source 31
    target 1553
  ]
  edge [
    source 31
    target 270
  ]
  edge [
    source 31
    target 1554
  ]
  edge [
    source 31
    target 209
  ]
  edge [
    source 31
    target 833
  ]
  edge [
    source 31
    target 465
  ]
  edge [
    source 31
    target 1555
  ]
  edge [
    source 31
    target 284
  ]
  edge [
    source 31
    target 1556
  ]
  edge [
    source 31
    target 1557
  ]
  edge [
    source 31
    target 1558
  ]
  edge [
    source 31
    target 1559
  ]
  edge [
    source 31
    target 990
  ]
  edge [
    source 31
    target 1560
  ]
  edge [
    source 31
    target 1561
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 1562
  ]
  edge [
    source 31
    target 1563
  ]
  edge [
    source 31
    target 1564
  ]
  edge [
    source 31
    target 1565
  ]
  edge [
    source 31
    target 858
  ]
  edge [
    source 31
    target 1566
  ]
  edge [
    source 31
    target 1567
  ]
  edge [
    source 31
    target 1568
  ]
  edge [
    source 31
    target 1569
  ]
  edge [
    source 31
    target 1570
  ]
  edge [
    source 31
    target 1571
  ]
  edge [
    source 31
    target 1572
  ]
  edge [
    source 31
    target 1573
  ]
  edge [
    source 31
    target 1574
  ]
  edge [
    source 31
    target 1575
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1576
  ]
  edge [
    source 32
    target 1577
  ]
  edge [
    source 32
    target 1043
  ]
  edge [
    source 32
    target 1578
  ]
  edge [
    source 32
    target 1579
  ]
  edge [
    source 32
    target 1580
  ]
  edge [
    source 32
    target 1581
  ]
  edge [
    source 32
    target 1582
  ]
  edge [
    source 32
    target 1583
  ]
  edge [
    source 32
    target 261
  ]
  edge [
    source 32
    target 1584
  ]
  edge [
    source 32
    target 1585
  ]
  edge [
    source 32
    target 1586
  ]
  edge [
    source 32
    target 1587
  ]
  edge [
    source 32
    target 1588
  ]
  edge [
    source 32
    target 1589
  ]
  edge [
    source 32
    target 1590
  ]
  edge [
    source 32
    target 1591
  ]
  edge [
    source 32
    target 1592
  ]
  edge [
    source 32
    target 1593
  ]
  edge [
    source 32
    target 1594
  ]
  edge [
    source 32
    target 1595
  ]
  edge [
    source 32
    target 1596
  ]
  edge [
    source 32
    target 1597
  ]
  edge [
    source 32
    target 1598
  ]
  edge [
    source 32
    target 1599
  ]
  edge [
    source 32
    target 1600
  ]
  edge [
    source 32
    target 1601
  ]
  edge [
    source 32
    target 839
  ]
  edge [
    source 32
    target 1602
  ]
  edge [
    source 32
    target 1603
  ]
  edge [
    source 32
    target 1604
  ]
  edge [
    source 32
    target 1605
  ]
  edge [
    source 32
    target 1606
  ]
  edge [
    source 32
    target 1607
  ]
  edge [
    source 32
    target 1608
  ]
  edge [
    source 32
    target 1609
  ]
  edge [
    source 32
    target 1610
  ]
  edge [
    source 32
    target 1611
  ]
  edge [
    source 32
    target 1612
  ]
  edge [
    source 32
    target 1168
  ]
  edge [
    source 32
    target 1613
  ]
  edge [
    source 32
    target 1614
  ]
  edge [
    source 32
    target 430
  ]
  edge [
    source 32
    target 1615
  ]
  edge [
    source 32
    target 518
  ]
  edge [
    source 32
    target 1616
  ]
  edge [
    source 32
    target 1617
  ]
  edge [
    source 32
    target 1618
  ]
  edge [
    source 32
    target 1619
  ]
  edge [
    source 32
    target 1620
  ]
  edge [
    source 32
    target 1621
  ]
  edge [
    source 32
    target 1622
  ]
  edge [
    source 32
    target 1623
  ]
  edge [
    source 32
    target 1624
  ]
  edge [
    source 32
    target 1625
  ]
  edge [
    source 32
    target 1626
  ]
  edge [
    source 32
    target 1627
  ]
  edge [
    source 32
    target 272
  ]
  edge [
    source 32
    target 273
  ]
  edge [
    source 32
    target 274
  ]
  edge [
    source 32
    target 275
  ]
  edge [
    source 32
    target 276
  ]
  edge [
    source 32
    target 277
  ]
  edge [
    source 32
    target 278
  ]
  edge [
    source 32
    target 279
  ]
  edge [
    source 32
    target 280
  ]
  edge [
    source 32
    target 281
  ]
  edge [
    source 32
    target 282
  ]
  edge [
    source 32
    target 283
  ]
  edge [
    source 32
    target 284
  ]
  edge [
    source 32
    target 153
  ]
  edge [
    source 32
    target 1628
  ]
  edge [
    source 32
    target 1629
  ]
  edge [
    source 32
    target 1630
  ]
  edge [
    source 32
    target 1631
  ]
  edge [
    source 32
    target 1632
  ]
  edge [
    source 32
    target 1633
  ]
  edge [
    source 32
    target 1634
  ]
  edge [
    source 32
    target 1635
  ]
  edge [
    source 32
    target 1636
  ]
  edge [
    source 32
    target 1637
  ]
  edge [
    source 32
    target 1638
  ]
  edge [
    source 32
    target 1639
  ]
  edge [
    source 32
    target 1640
  ]
  edge [
    source 32
    target 1641
  ]
  edge [
    source 32
    target 1642
  ]
  edge [
    source 32
    target 1643
  ]
  edge [
    source 32
    target 1644
  ]
  edge [
    source 32
    target 1645
  ]
  edge [
    source 32
    target 1646
  ]
  edge [
    source 32
    target 1647
  ]
  edge [
    source 32
    target 1648
  ]
  edge [
    source 32
    target 1649
  ]
  edge [
    source 32
    target 1650
  ]
  edge [
    source 32
    target 1651
  ]
  edge [
    source 32
    target 1652
  ]
  edge [
    source 32
    target 830
  ]
  edge [
    source 32
    target 1653
  ]
  edge [
    source 32
    target 1654
  ]
  edge [
    source 32
    target 225
  ]
  edge [
    source 32
    target 1655
  ]
  edge [
    source 32
    target 825
  ]
  edge [
    source 32
    target 1656
  ]
  edge [
    source 32
    target 1657
  ]
  edge [
    source 32
    target 1658
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1659
  ]
  edge [
    source 33
    target 1523
  ]
  edge [
    source 33
    target 1524
  ]
  edge [
    source 33
    target 525
  ]
  edge [
    source 33
    target 565
  ]
  edge [
    source 33
    target 1525
  ]
  edge [
    source 33
    target 1526
  ]
  edge [
    source 33
    target 1660
  ]
  edge [
    source 33
    target 1527
  ]
  edge [
    source 33
    target 1661
  ]
  edge [
    source 33
    target 1662
  ]
  edge [
    source 33
    target 700
  ]
  edge [
    source 33
    target 1663
  ]
  edge [
    source 33
    target 1664
  ]
  edge [
    source 33
    target 1665
  ]
  edge [
    source 33
    target 268
  ]
  edge [
    source 33
    target 1666
  ]
  edge [
    source 33
    target 1667
  ]
  edge [
    source 33
    target 1668
  ]
  edge [
    source 33
    target 1669
  ]
  edge [
    source 33
    target 1670
  ]
  edge [
    source 33
    target 610
  ]
  edge [
    source 33
    target 1671
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 33
    target 1672
  ]
  edge [
    source 33
    target 1673
  ]
  edge [
    source 33
    target 612
  ]
  edge [
    source 33
    target 1674
  ]
  edge [
    source 33
    target 1675
  ]
  edge [
    source 33
    target 464
  ]
  edge [
    source 33
    target 1676
  ]
  edge [
    source 33
    target 1677
  ]
  edge [
    source 33
    target 1678
  ]
  edge [
    source 33
    target 1679
  ]
  edge [
    source 33
    target 604
  ]
  edge [
    source 33
    target 1680
  ]
  edge [
    source 33
    target 1681
  ]
  edge [
    source 33
    target 1682
  ]
  edge [
    source 33
    target 1683
  ]
  edge [
    source 33
    target 1394
  ]
  edge [
    source 33
    target 625
  ]
  edge [
    source 33
    target 384
  ]
  edge [
    source 33
    target 1684
  ]
  edge [
    source 33
    target 602
  ]
  edge [
    source 33
    target 732
  ]
  edge [
    source 33
    target 1685
  ]
  edge [
    source 33
    target 1686
  ]
  edge [
    source 33
    target 1356
  ]
  edge [
    source 33
    target 1687
  ]
  edge [
    source 33
    target 1382
  ]
  edge [
    source 33
    target 750
  ]
  edge [
    source 33
    target 1688
  ]
  edge [
    source 33
    target 1689
  ]
  edge [
    source 33
    target 1690
  ]
  edge [
    source 33
    target 1691
  ]
  edge [
    source 33
    target 1692
  ]
  edge [
    source 33
    target 1693
  ]
  edge [
    source 33
    target 1694
  ]
  edge [
    source 33
    target 1695
  ]
  edge [
    source 33
    target 1696
  ]
  edge [
    source 33
    target 1697
  ]
  edge [
    source 33
    target 1698
  ]
  edge [
    source 33
    target 1699
  ]
  edge [
    source 33
    target 1700
  ]
  edge [
    source 33
    target 1701
  ]
  edge [
    source 33
    target 698
  ]
  edge [
    source 33
    target 1702
  ]
  edge [
    source 33
    target 1052
  ]
  edge [
    source 33
    target 1703
  ]
  edge [
    source 33
    target 508
  ]
  edge [
    source 33
    target 1704
  ]
  edge [
    source 33
    target 1705
  ]
  edge [
    source 33
    target 1006
  ]
  edge [
    source 33
    target 1706
  ]
  edge [
    source 33
    target 1707
  ]
  edge [
    source 33
    target 1708
  ]
  edge [
    source 33
    target 1709
  ]
  edge [
    source 33
    target 423
  ]
  edge [
    source 33
    target 1710
  ]
  edge [
    source 33
    target 350
  ]
  edge [
    source 33
    target 1711
  ]
  edge [
    source 33
    target 1712
  ]
  edge [
    source 33
    target 883
  ]
  edge [
    source 33
    target 611
  ]
  edge [
    source 33
    target 884
  ]
  edge [
    source 33
    target 614
  ]
  edge [
    source 33
    target 885
  ]
  edge [
    source 33
    target 880
  ]
  edge [
    source 33
    target 886
  ]
  edge [
    source 33
    target 887
  ]
  edge [
    source 33
    target 888
  ]
  edge [
    source 33
    target 889
  ]
  edge [
    source 33
    target 101
  ]
  edge [
    source 33
    target 890
  ]
  edge [
    source 33
    target 891
  ]
  edge [
    source 33
    target 892
  ]
  edge [
    source 33
    target 893
  ]
  edge [
    source 33
    target 894
  ]
  edge [
    source 33
    target 895
  ]
  edge [
    source 33
    target 896
  ]
  edge [
    source 33
    target 776
  ]
  edge [
    source 33
    target 897
  ]
  edge [
    source 33
    target 898
  ]
  edge [
    source 33
    target 899
  ]
  edge [
    source 33
    target 900
  ]
  edge [
    source 33
    target 901
  ]
  edge [
    source 33
    target 902
  ]
  edge [
    source 33
    target 1713
  ]
  edge [
    source 33
    target 1714
  ]
  edge [
    source 33
    target 1715
  ]
  edge [
    source 33
    target 1716
  ]
  edge [
    source 33
    target 1717
  ]
  edge [
    source 33
    target 1718
  ]
  edge [
    source 33
    target 1719
  ]
  edge [
    source 33
    target 1720
  ]
  edge [
    source 33
    target 1721
  ]
  edge [
    source 33
    target 1722
  ]
  edge [
    source 33
    target 228
  ]
  edge [
    source 33
    target 1723
  ]
  edge [
    source 33
    target 721
  ]
  edge [
    source 33
    target 1724
  ]
  edge [
    source 33
    target 1725
  ]
  edge [
    source 33
    target 1726
  ]
  edge [
    source 33
    target 1727
  ]
  edge [
    source 33
    target 1728
  ]
  edge [
    source 33
    target 1729
  ]
  edge [
    source 33
    target 1730
  ]
  edge [
    source 33
    target 1731
  ]
  edge [
    source 33
    target 1732
  ]
  edge [
    source 33
    target 1733
  ]
  edge [
    source 33
    target 1734
  ]
  edge [
    source 33
    target 1735
  ]
  edge [
    source 33
    target 1736
  ]
  edge [
    source 33
    target 731
  ]
  edge [
    source 33
    target 1554
  ]
  edge [
    source 33
    target 677
  ]
  edge [
    source 33
    target 1737
  ]
  edge [
    source 33
    target 547
  ]
  edge [
    source 33
    target 1532
  ]
  edge [
    source 33
    target 1738
  ]
  edge [
    source 33
    target 1739
  ]
  edge [
    source 33
    target 1740
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 990
  ]
  edge [
    source 34
    target 1560
  ]
  edge [
    source 34
    target 1561
  ]
  edge [
    source 34
    target 316
  ]
  edge [
    source 34
    target 1562
  ]
  edge [
    source 34
    target 1540
  ]
  edge [
    source 34
    target 1563
  ]
  edge [
    source 34
    target 1564
  ]
  edge [
    source 34
    target 1565
  ]
  edge [
    source 34
    target 1539
  ]
  edge [
    source 34
    target 858
  ]
  edge [
    source 34
    target 1556
  ]
  edge [
    source 34
    target 1741
  ]
  edge [
    source 34
    target 1742
  ]
  edge [
    source 34
    target 1743
  ]
  edge [
    source 34
    target 1744
  ]
  edge [
    source 34
    target 1745
  ]
  edge [
    source 34
    target 1746
  ]
  edge [
    source 34
    target 1572
  ]
  edge [
    source 34
    target 1747
  ]
  edge [
    source 34
    target 1748
  ]
  edge [
    source 34
    target 1749
  ]
  edge [
    source 34
    target 1750
  ]
  edge [
    source 34
    target 1751
  ]
  edge [
    source 34
    target 1752
  ]
  edge [
    source 34
    target 1753
  ]
  edge [
    source 34
    target 1754
  ]
  edge [
    source 34
    target 1755
  ]
  edge [
    source 34
    target 1756
  ]
  edge [
    source 34
    target 1757
  ]
  edge [
    source 34
    target 1758
  ]
  edge [
    source 34
    target 1558
  ]
  edge [
    source 34
    target 1759
  ]
  edge [
    source 34
    target 1557
  ]
  edge [
    source 34
    target 270
  ]
  edge [
    source 34
    target 1559
  ]
  edge [
    source 34
    target 1760
  ]
  edge [
    source 34
    target 1761
  ]
  edge [
    source 34
    target 1108
  ]
  edge [
    source 34
    target 314
  ]
  edge [
    source 34
    target 1109
  ]
  edge [
    source 34
    target 1762
  ]
  edge [
    source 34
    target 1763
  ]
  edge [
    source 34
    target 1764
  ]
  edge [
    source 34
    target 1765
  ]
  edge [
    source 34
    target 1766
  ]
  edge [
    source 34
    target 1767
  ]
  edge [
    source 34
    target 1768
  ]
  edge [
    source 34
    target 1769
  ]
  edge [
    source 34
    target 1770
  ]
  edge [
    source 34
    target 1771
  ]
  edge [
    source 34
    target 1772
  ]
  edge [
    source 34
    target 1773
  ]
  edge [
    source 34
    target 1774
  ]
  edge [
    source 34
    target 1775
  ]
  edge [
    source 34
    target 1776
  ]
  edge [
    source 34
    target 1777
  ]
  edge [
    source 34
    target 1778
  ]
  edge [
    source 34
    target 1779
  ]
  edge [
    source 34
    target 1780
  ]
  edge [
    source 34
    target 1781
  ]
  edge [
    source 34
    target 1782
  ]
  edge [
    source 34
    target 1055
  ]
  edge [
    source 34
    target 1783
  ]
  edge [
    source 34
    target 1784
  ]
  edge [
    source 34
    target 1785
  ]
  edge [
    source 34
    target 1786
  ]
  edge [
    source 34
    target 1787
  ]
  edge [
    source 34
    target 1788
  ]
  edge [
    source 34
    target 1789
  ]
  edge [
    source 34
    target 1790
  ]
  edge [
    source 34
    target 1791
  ]
  edge [
    source 34
    target 1792
  ]
  edge [
    source 34
    target 1793
  ]
  edge [
    source 34
    target 1794
  ]
  edge [
    source 34
    target 1795
  ]
  edge [
    source 34
    target 1796
  ]
  edge [
    source 34
    target 1797
  ]
  edge [
    source 34
    target 1798
  ]
  edge [
    source 34
    target 1266
  ]
  edge [
    source 34
    target 1799
  ]
  edge [
    source 34
    target 1800
  ]
  edge [
    source 34
    target 1801
  ]
  edge [
    source 34
    target 1453
  ]
  edge [
    source 34
    target 885
  ]
  edge [
    source 34
    target 1094
  ]
  edge [
    source 34
    target 1802
  ]
  edge [
    source 34
    target 1803
  ]
  edge [
    source 34
    target 1804
  ]
  edge [
    source 34
    target 1805
  ]
  edge [
    source 34
    target 1806
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1043
  ]
  edge [
    source 36
    target 1583
  ]
  edge [
    source 36
    target 1582
  ]
  edge [
    source 36
    target 1588
  ]
  edge [
    source 36
    target 1581
  ]
  edge [
    source 36
    target 1597
  ]
  edge [
    source 36
    target 1598
  ]
  edge [
    source 36
    target 1599
  ]
  edge [
    source 36
    target 1631
  ]
  edge [
    source 36
    target 1632
  ]
  edge [
    source 36
    target 1633
  ]
  edge [
    source 36
    target 1634
  ]
  edge [
    source 36
    target 1635
  ]
  edge [
    source 36
    target 1636
  ]
  edge [
    source 36
    target 1637
  ]
  edge [
    source 36
    target 1638
  ]
  edge [
    source 36
    target 1639
  ]
  edge [
    source 36
    target 1650
  ]
  edge [
    source 36
    target 1651
  ]
  edge [
    source 36
    target 1652
  ]
  edge [
    source 36
    target 830
  ]
  edge [
    source 36
    target 1653
  ]
  edge [
    source 36
    target 1654
  ]
  edge [
    source 36
    target 225
  ]
  edge [
    source 36
    target 1655
  ]
  edge [
    source 36
    target 825
  ]
  edge [
    source 36
    target 1656
  ]
  edge [
    source 36
    target 1657
  ]
  edge [
    source 36
    target 1658
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1807
  ]
  edge [
    source 37
    target 1443
  ]
  edge [
    source 37
    target 1284
  ]
  edge [
    source 37
    target 1808
  ]
  edge [
    source 37
    target 1809
  ]
  edge [
    source 37
    target 1810
  ]
  edge [
    source 37
    target 1683
  ]
  edge [
    source 37
    target 551
  ]
  edge [
    source 37
    target 1811
  ]
  edge [
    source 37
    target 1812
  ]
  edge [
    source 37
    target 1813
  ]
  edge [
    source 37
    target 1814
  ]
  edge [
    source 37
    target 1815
  ]
  edge [
    source 37
    target 1816
  ]
  edge [
    source 37
    target 1133
  ]
  edge [
    source 37
    target 1817
  ]
  edge [
    source 37
    target 1287
  ]
  edge [
    source 37
    target 1818
  ]
  edge [
    source 37
    target 700
  ]
  edge [
    source 37
    target 1819
  ]
  edge [
    source 37
    target 1820
  ]
  edge [
    source 37
    target 464
  ]
  edge [
    source 37
    target 1162
  ]
  edge [
    source 37
    target 1163
  ]
  edge [
    source 37
    target 1164
  ]
  edge [
    source 37
    target 1165
  ]
  edge [
    source 37
    target 1166
  ]
  edge [
    source 37
    target 1167
  ]
  edge [
    source 37
    target 1168
  ]
  edge [
    source 37
    target 1169
  ]
  edge [
    source 37
    target 613
  ]
  edge [
    source 37
    target 1170
  ]
  edge [
    source 37
    target 1171
  ]
  edge [
    source 37
    target 1172
  ]
  edge [
    source 37
    target 1173
  ]
  edge [
    source 37
    target 1174
  ]
  edge [
    source 37
    target 1175
  ]
  edge [
    source 37
    target 1821
  ]
  edge [
    source 37
    target 1822
  ]
  edge [
    source 37
    target 125
  ]
  edge [
    source 37
    target 1823
  ]
  edge [
    source 37
    target 1628
  ]
  edge [
    source 37
    target 1824
  ]
  edge [
    source 37
    target 954
  ]
  edge [
    source 37
    target 1825
  ]
  edge [
    source 37
    target 881
  ]
  edge [
    source 37
    target 1013
  ]
  edge [
    source 37
    target 1826
  ]
  edge [
    source 37
    target 1827
  ]
  edge [
    source 37
    target 1828
  ]
  edge [
    source 37
    target 1829
  ]
  edge [
    source 37
    target 1830
  ]
  edge [
    source 37
    target 1831
  ]
  edge [
    source 37
    target 1140
  ]
  edge [
    source 37
    target 1832
  ]
  edge [
    source 37
    target 1833
  ]
  edge [
    source 37
    target 1834
  ]
  edge [
    source 37
    target 268
  ]
  edge [
    source 37
    target 1835
  ]
  edge [
    source 37
    target 752
  ]
  edge [
    source 37
    target 989
  ]
  edge [
    source 37
    target 1836
  ]
  edge [
    source 37
    target 1837
  ]
  edge [
    source 37
    target 1838
  ]
  edge [
    source 37
    target 1839
  ]
  edge [
    source 37
    target 371
  ]
  edge [
    source 37
    target 1840
  ]
  edge [
    source 37
    target 1841
  ]
  edge [
    source 37
    target 1842
  ]
  edge [
    source 37
    target 1843
  ]
  edge [
    source 37
    target 1844
  ]
  edge [
    source 37
    target 1845
  ]
  edge [
    source 37
    target 1846
  ]
  edge [
    source 37
    target 1847
  ]
  edge [
    source 37
    target 1848
  ]
  edge [
    source 37
    target 1849
  ]
  edge [
    source 37
    target 1850
  ]
  edge [
    source 37
    target 524
  ]
  edge [
    source 37
    target 1851
  ]
  edge [
    source 37
    target 1852
  ]
  edge [
    source 37
    target 1853
  ]
  edge [
    source 37
    target 1854
  ]
  edge [
    source 37
    target 1675
  ]
  edge [
    source 37
    target 1664
  ]
  edge [
    source 37
    target 1665
  ]
  edge [
    source 37
    target 1666
  ]
  edge [
    source 37
    target 1667
  ]
  edge [
    source 37
    target 1668
  ]
  edge [
    source 37
    target 1669
  ]
  edge [
    source 37
    target 1670
  ]
  edge [
    source 37
    target 610
  ]
  edge [
    source 37
    target 1671
  ]
  edge [
    source 37
    target 50
  ]
  edge [
    source 37
    target 1672
  ]
  edge [
    source 37
    target 1673
  ]
  edge [
    source 37
    target 612
  ]
  edge [
    source 37
    target 1674
  ]
  edge [
    source 37
    target 525
  ]
  edge [
    source 37
    target 1676
  ]
  edge [
    source 37
    target 1677
  ]
  edge [
    source 37
    target 1678
  ]
  edge [
    source 37
    target 1679
  ]
  edge [
    source 37
    target 604
  ]
  edge [
    source 37
    target 1680
  ]
  edge [
    source 37
    target 1681
  ]
  edge [
    source 37
    target 1682
  ]
  edge [
    source 37
    target 1394
  ]
  edge [
    source 37
    target 625
  ]
  edge [
    source 37
    target 384
  ]
  edge [
    source 37
    target 1684
  ]
  edge [
    source 37
    target 602
  ]
  edge [
    source 37
    target 732
  ]
  edge [
    source 37
    target 1685
  ]
  edge [
    source 37
    target 1686
  ]
  edge [
    source 37
    target 1356
  ]
  edge [
    source 37
    target 1687
  ]
  edge [
    source 37
    target 1382
  ]
  edge [
    source 37
    target 750
  ]
  edge [
    source 37
    target 1688
  ]
  edge [
    source 37
    target 1855
  ]
  edge [
    source 37
    target 1856
  ]
  edge [
    source 37
    target 1857
  ]
  edge [
    source 37
    target 1858
  ]
  edge [
    source 37
    target 1859
  ]
  edge [
    source 37
    target 1860
  ]
  edge [
    source 37
    target 1861
  ]
  edge [
    source 37
    target 1862
  ]
  edge [
    source 37
    target 1863
  ]
  edge [
    source 37
    target 1864
  ]
  edge [
    source 37
    target 1865
  ]
  edge [
    source 37
    target 1866
  ]
  edge [
    source 37
    target 1867
  ]
  edge [
    source 37
    target 1868
  ]
  edge [
    source 37
    target 1869
  ]
  edge [
    source 37
    target 1870
  ]
  edge [
    source 37
    target 1871
  ]
  edge [
    source 37
    target 1872
  ]
  edge [
    source 37
    target 1873
  ]
  edge [
    source 37
    target 1874
  ]
  edge [
    source 37
    target 1875
  ]
  edge [
    source 37
    target 878
  ]
  edge [
    source 37
    target 1876
  ]
  edge [
    source 37
    target 677
  ]
  edge [
    source 37
    target 1251
  ]
  edge [
    source 37
    target 1737
  ]
  edge [
    source 37
    target 1877
  ]
  edge [
    source 37
    target 1878
  ]
  edge [
    source 37
    target 1879
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1880
  ]
  edge [
    source 38
    target 1881
  ]
  edge [
    source 38
    target 1882
  ]
  edge [
    source 38
    target 1883
  ]
  edge [
    source 38
    target 1884
  ]
  edge [
    source 38
    target 1885
  ]
  edge [
    source 38
    target 1886
  ]
  edge [
    source 38
    target 1887
  ]
  edge [
    source 38
    target 1888
  ]
  edge [
    source 38
    target 1889
  ]
  edge [
    source 38
    target 1082
  ]
  edge [
    source 38
    target 257
  ]
  edge [
    source 38
    target 1890
  ]
  edge [
    source 38
    target 1891
  ]
  edge [
    source 38
    target 1892
  ]
  edge [
    source 38
    target 1893
  ]
  edge [
    source 38
    target 1894
  ]
  edge [
    source 38
    target 1895
  ]
  edge [
    source 38
    target 1896
  ]
  edge [
    source 38
    target 1897
  ]
  edge [
    source 38
    target 1898
  ]
  edge [
    source 38
    target 1899
  ]
  edge [
    source 38
    target 1900
  ]
  edge [
    source 38
    target 1901
  ]
  edge [
    source 38
    target 1902
  ]
  edge [
    source 38
    target 1903
  ]
  edge [
    source 38
    target 1904
  ]
  edge [
    source 38
    target 1905
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1906
  ]
  edge [
    source 41
    target 1907
  ]
  edge [
    source 41
    target 1908
  ]
  edge [
    source 41
    target 1909
  ]
  edge [
    source 41
    target 1910
  ]
  edge [
    source 41
    target 1911
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1912
  ]
  edge [
    source 42
    target 1913
  ]
  edge [
    source 42
    target 69
  ]
  edge [
    source 42
    target 71
  ]
  edge [
    source 42
    target 1914
  ]
  edge [
    source 42
    target 1915
  ]
  edge [
    source 42
    target 1916
  ]
  edge [
    source 42
    target 63
  ]
  edge [
    source 42
    target 1917
  ]
  edge [
    source 42
    target 1918
  ]
  edge [
    source 42
    target 1919
  ]
  edge [
    source 42
    target 1920
  ]
  edge [
    source 42
    target 67
  ]
  edge [
    source 42
    target 1921
  ]
  edge [
    source 42
    target 1922
  ]
  edge [
    source 42
    target 1923
  ]
  edge [
    source 42
    target 751
  ]
  edge [
    source 42
    target 121
  ]
  edge [
    source 42
    target 122
  ]
  edge [
    source 42
    target 123
  ]
  edge [
    source 42
    target 124
  ]
  edge [
    source 42
    target 125
  ]
  edge [
    source 42
    target 126
  ]
  edge [
    source 42
    target 127
  ]
  edge [
    source 42
    target 128
  ]
  edge [
    source 42
    target 129
  ]
  edge [
    source 42
    target 130
  ]
  edge [
    source 42
    target 131
  ]
  edge [
    source 42
    target 132
  ]
  edge [
    source 42
    target 133
  ]
  edge [
    source 42
    target 134
  ]
  edge [
    source 42
    target 135
  ]
  edge [
    source 42
    target 136
  ]
  edge [
    source 42
    target 137
  ]
  edge [
    source 42
    target 138
  ]
  edge [
    source 42
    target 139
  ]
  edge [
    source 42
    target 140
  ]
  edge [
    source 42
    target 141
  ]
  edge [
    source 42
    target 142
  ]
  edge [
    source 42
    target 143
  ]
  edge [
    source 42
    target 156
  ]
  edge [
    source 42
    target 157
  ]
  edge [
    source 42
    target 158
  ]
  edge [
    source 42
    target 159
  ]
  edge [
    source 42
    target 160
  ]
  edge [
    source 42
    target 161
  ]
  edge [
    source 42
    target 1924
  ]
  edge [
    source 42
    target 1925
  ]
  edge [
    source 42
    target 1926
  ]
  edge [
    source 42
    target 1927
  ]
  edge [
    source 42
    target 1928
  ]
  edge [
    source 42
    target 1929
  ]
  edge [
    source 42
    target 1930
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1931
  ]
  edge [
    source 44
    target 1932
  ]
  edge [
    source 44
    target 1933
  ]
  edge [
    source 44
    target 1934
  ]
  edge [
    source 44
    target 1935
  ]
  edge [
    source 44
    target 1936
  ]
  edge [
    source 44
    target 1746
  ]
  edge [
    source 44
    target 1937
  ]
  edge [
    source 44
    target 1938
  ]
  edge [
    source 44
    target 836
  ]
  edge [
    source 44
    target 1939
  ]
  edge [
    source 44
    target 1940
  ]
  edge [
    source 44
    target 1941
  ]
  edge [
    source 44
    target 1942
  ]
  edge [
    source 44
    target 1943
  ]
  edge [
    source 44
    target 1944
  ]
  edge [
    source 44
    target 1909
  ]
  edge [
    source 44
    target 1945
  ]
  edge [
    source 44
    target 1946
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 370
  ]
  edge [
    source 45
    target 371
  ]
  edge [
    source 45
    target 372
  ]
  edge [
    source 45
    target 373
  ]
  edge [
    source 45
    target 374
  ]
  edge [
    source 45
    target 375
  ]
  edge [
    source 45
    target 376
  ]
  edge [
    source 45
    target 377
  ]
  edge [
    source 45
    target 378
  ]
  edge [
    source 45
    target 379
  ]
  edge [
    source 45
    target 598
  ]
  edge [
    source 45
    target 1947
  ]
  edge [
    source 45
    target 752
  ]
  edge [
    source 45
    target 1948
  ]
  edge [
    source 45
    target 1949
  ]
  edge [
    source 45
    target 1950
  ]
  edge [
    source 45
    target 1951
  ]
  edge [
    source 45
    target 1952
  ]
  edge [
    source 45
    target 1953
  ]
  edge [
    source 45
    target 1954
  ]
  edge [
    source 45
    target 1955
  ]
  edge [
    source 45
    target 1956
  ]
  edge [
    source 45
    target 1957
  ]
  edge [
    source 45
    target 1958
  ]
  edge [
    source 45
    target 1959
  ]
  edge [
    source 45
    target 1960
  ]
  edge [
    source 45
    target 1961
  ]
  edge [
    source 45
    target 1962
  ]
  edge [
    source 45
    target 1963
  ]
  edge [
    source 45
    target 1964
  ]
  edge [
    source 45
    target 1965
  ]
  edge [
    source 45
    target 1966
  ]
  edge [
    source 45
    target 1967
  ]
  edge [
    source 45
    target 1968
  ]
  edge [
    source 45
    target 1969
  ]
  edge [
    source 45
    target 1970
  ]
  edge [
    source 45
    target 1971
  ]
  edge [
    source 45
    target 1725
  ]
  edge [
    source 45
    target 1972
  ]
  edge [
    source 45
    target 1973
  ]
  edge [
    source 45
    target 1974
  ]
  edge [
    source 45
    target 1689
  ]
  edge [
    source 45
    target 1975
  ]
  edge [
    source 45
    target 1976
  ]
  edge [
    source 45
    target 1977
  ]
  edge [
    source 45
    target 1978
  ]
  edge [
    source 45
    target 464
  ]
  edge [
    source 45
    target 1979
  ]
  edge [
    source 45
    target 1980
  ]
  edge [
    source 45
    target 1981
  ]
  edge [
    source 45
    target 1055
  ]
  edge [
    source 45
    target 271
  ]
  edge [
    source 45
    target 1982
  ]
  edge [
    source 45
    target 1983
  ]
  edge [
    source 45
    target 1984
  ]
  edge [
    source 45
    target 430
  ]
  edge [
    source 45
    target 1829
  ]
  edge [
    source 45
    target 1985
  ]
  edge [
    source 45
    target 551
  ]
  edge [
    source 45
    target 1986
  ]
  edge [
    source 45
    target 547
  ]
  edge [
    source 45
    target 1028
  ]
  edge [
    source 45
    target 1987
  ]
  edge [
    source 45
    target 1988
  ]
  edge [
    source 45
    target 1780
  ]
  edge [
    source 45
    target 1989
  ]
  edge [
    source 45
    target 1990
  ]
  edge [
    source 45
    target 1991
  ]
  edge [
    source 45
    target 1992
  ]
  edge [
    source 45
    target 1993
  ]
  edge [
    source 45
    target 1994
  ]
  edge [
    source 45
    target 1995
  ]
  edge [
    source 45
    target 1996
  ]
  edge [
    source 45
    target 1997
  ]
  edge [
    source 45
    target 1998
  ]
  edge [
    source 45
    target 1999
  ]
  edge [
    source 45
    target 1112
  ]
  edge [
    source 45
    target 2000
  ]
  edge [
    source 45
    target 2001
  ]
  edge [
    source 45
    target 2002
  ]
  edge [
    source 45
    target 2003
  ]
  edge [
    source 45
    target 2004
  ]
  edge [
    source 45
    target 554
  ]
  edge [
    source 45
    target 2005
  ]
  edge [
    source 45
    target 2006
  ]
  edge [
    source 45
    target 2007
  ]
  edge [
    source 45
    target 2008
  ]
  edge [
    source 45
    target 2009
  ]
  edge [
    source 45
    target 2010
  ]
  edge [
    source 45
    target 2011
  ]
  edge [
    source 45
    target 2012
  ]
  edge [
    source 45
    target 2013
  ]
  edge [
    source 45
    target 2014
  ]
  edge [
    source 45
    target 2015
  ]
  edge [
    source 45
    target 2016
  ]
  edge [
    source 45
    target 2017
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 2018
  ]
  edge [
    source 46
    target 2019
  ]
  edge [
    source 46
    target 2020
  ]
  edge [
    source 46
    target 2021
  ]
  edge [
    source 47
    target 2022
  ]
  edge [
    source 47
    target 2023
  ]
  edge [
    source 47
    target 2024
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 2025
  ]
  edge [
    source 50
    target 2026
  ]
  edge [
    source 50
    target 525
  ]
  edge [
    source 50
    target 268
  ]
  edge [
    source 50
    target 2027
  ]
  edge [
    source 50
    target 1525
  ]
  edge [
    source 50
    target 2028
  ]
  edge [
    source 50
    target 2029
  ]
  edge [
    source 50
    target 1253
  ]
  edge [
    source 50
    target 2030
  ]
  edge [
    source 50
    target 2031
  ]
  edge [
    source 50
    target 2032
  ]
  edge [
    source 50
    target 2033
  ]
  edge [
    source 50
    target 1275
  ]
  edge [
    source 50
    target 2034
  ]
  edge [
    source 50
    target 2035
  ]
  edge [
    source 50
    target 700
  ]
  edge [
    source 50
    target 2036
  ]
  edge [
    source 50
    target 462
  ]
  edge [
    source 50
    target 2037
  ]
  edge [
    source 50
    target 2038
  ]
  edge [
    source 50
    target 2039
  ]
  edge [
    source 50
    target 1967
  ]
  edge [
    source 50
    target 2040
  ]
  edge [
    source 50
    target 1737
  ]
  edge [
    source 50
    target 2041
  ]
  edge [
    source 50
    target 2042
  ]
  edge [
    source 50
    target 613
  ]
  edge [
    source 50
    target 2043
  ]
  edge [
    source 50
    target 987
  ]
  edge [
    source 50
    target 988
  ]
  edge [
    source 50
    target 2044
  ]
  edge [
    source 50
    target 2045
  ]
  edge [
    source 50
    target 989
  ]
  edge [
    source 50
    target 990
  ]
  edge [
    source 50
    target 2046
  ]
  edge [
    source 50
    target 276
  ]
  edge [
    source 50
    target 2047
  ]
  edge [
    source 50
    target 2048
  ]
  edge [
    source 50
    target 552
  ]
  edge [
    source 50
    target 992
  ]
  edge [
    source 50
    target 2049
  ]
  edge [
    source 50
    target 995
  ]
  edge [
    source 50
    target 996
  ]
  edge [
    source 50
    target 871
  ]
  edge [
    source 50
    target 997
  ]
  edge [
    source 50
    target 1456
  ]
  edge [
    source 50
    target 998
  ]
  edge [
    source 50
    target 999
  ]
  edge [
    source 50
    target 2050
  ]
  edge [
    source 50
    target 1074
  ]
  edge [
    source 50
    target 934
  ]
  edge [
    source 50
    target 1001
  ]
  edge [
    source 50
    target 1002
  ]
  edge [
    source 50
    target 464
  ]
  edge [
    source 50
    target 2051
  ]
  edge [
    source 50
    target 2052
  ]
  edge [
    source 50
    target 2053
  ]
  edge [
    source 50
    target 2054
  ]
  edge [
    source 50
    target 2055
  ]
  edge [
    source 50
    target 2056
  ]
  edge [
    source 50
    target 2057
  ]
  edge [
    source 50
    target 430
  ]
  edge [
    source 50
    target 2058
  ]
  edge [
    source 50
    target 524
  ]
  edge [
    source 50
    target 2059
  ]
  edge [
    source 50
    target 2060
  ]
  edge [
    source 50
    target 1009
  ]
  edge [
    source 50
    target 2061
  ]
  edge [
    source 50
    target 2062
  ]
  edge [
    source 50
    target 259
  ]
  edge [
    source 50
    target 2063
  ]
  edge [
    source 50
    target 948
  ]
  edge [
    source 50
    target 2064
  ]
  edge [
    source 50
    target 1228
  ]
  edge [
    source 50
    target 2065
  ]
  edge [
    source 50
    target 1230
  ]
  edge [
    source 50
    target 392
  ]
  edge [
    source 50
    target 561
  ]
  edge [
    source 50
    target 1233
  ]
  edge [
    source 50
    target 2066
  ]
  edge [
    source 50
    target 2067
  ]
  edge [
    source 50
    target 2068
  ]
  edge [
    source 50
    target 2069
  ]
  edge [
    source 50
    target 727
  ]
  edge [
    source 50
    target 1241
  ]
  edge [
    source 50
    target 1242
  ]
  edge [
    source 50
    target 432
  ]
  edge [
    source 50
    target 433
  ]
  edge [
    source 50
    target 153
  ]
  edge [
    source 50
    target 2070
  ]
  edge [
    source 50
    target 2071
  ]
  edge [
    source 50
    target 2072
  ]
  edge [
    source 50
    target 1246
  ]
  edge [
    source 50
    target 1247
  ]
  edge [
    source 50
    target 1248
  ]
  edge [
    source 50
    target 1249
  ]
  edge [
    source 50
    target 1251
  ]
  edge [
    source 50
    target 2073
  ]
  edge [
    source 50
    target 1723
  ]
  edge [
    source 50
    target 721
  ]
  edge [
    source 50
    target 1724
  ]
  edge [
    source 50
    target 1725
  ]
  edge [
    source 50
    target 1726
  ]
  edge [
    source 50
    target 1727
  ]
  edge [
    source 50
    target 1728
  ]
  edge [
    source 50
    target 1729
  ]
  edge [
    source 50
    target 1730
  ]
  edge [
    source 50
    target 1731
  ]
  edge [
    source 50
    target 1732
  ]
  edge [
    source 50
    target 1733
  ]
  edge [
    source 50
    target 1734
  ]
  edge [
    source 50
    target 1735
  ]
  edge [
    source 50
    target 1736
  ]
  edge [
    source 50
    target 731
  ]
  edge [
    source 50
    target 1554
  ]
  edge [
    source 50
    target 677
  ]
  edge [
    source 50
    target 547
  ]
  edge [
    source 50
    target 2074
  ]
  edge [
    source 50
    target 2075
  ]
  edge [
    source 50
    target 2076
  ]
  edge [
    source 50
    target 966
  ]
  edge [
    source 50
    target 2077
  ]
  edge [
    source 50
    target 2078
  ]
  edge [
    source 50
    target 2079
  ]
  edge [
    source 50
    target 2080
  ]
  edge [
    source 50
    target 2081
  ]
  edge [
    source 50
    target 2082
  ]
  edge [
    source 50
    target 2083
  ]
  edge [
    source 50
    target 2084
  ]
  edge [
    source 50
    target 2085
  ]
  edge [
    source 50
    target 2086
  ]
  edge [
    source 50
    target 2087
  ]
  edge [
    source 50
    target 2088
  ]
  edge [
    source 50
    target 2089
  ]
  edge [
    source 50
    target 2090
  ]
  edge [
    source 50
    target 2091
  ]
  edge [
    source 50
    target 2092
  ]
  edge [
    source 50
    target 2093
  ]
  edge [
    source 50
    target 2094
  ]
  edge [
    source 50
    target 2095
  ]
  edge [
    source 50
    target 2096
  ]
  edge [
    source 50
    target 2097
  ]
  edge [
    source 50
    target 2098
  ]
  edge [
    source 50
    target 2099
  ]
  edge [
    source 50
    target 2100
  ]
  edge [
    source 50
    target 2101
  ]
  edge [
    source 50
    target 2102
  ]
  edge [
    source 50
    target 2103
  ]
  edge [
    source 50
    target 2104
  ]
  edge [
    source 50
    target 1689
  ]
  edge [
    source 50
    target 1690
  ]
  edge [
    source 50
    target 1691
  ]
  edge [
    source 50
    target 1692
  ]
  edge [
    source 50
    target 1693
  ]
  edge [
    source 50
    target 1694
  ]
  edge [
    source 50
    target 1527
  ]
  edge [
    source 50
    target 1695
  ]
  edge [
    source 50
    target 1696
  ]
  edge [
    source 50
    target 1697
  ]
  edge [
    source 50
    target 1698
  ]
  edge [
    source 50
    target 1699
  ]
  edge [
    source 50
    target 1700
  ]
  edge [
    source 50
    target 1701
  ]
  edge [
    source 50
    target 698
  ]
  edge [
    source 50
    target 1702
  ]
  edge [
    source 50
    target 1680
  ]
  edge [
    source 50
    target 1052
  ]
  edge [
    source 50
    target 604
  ]
  edge [
    source 50
    target 1679
  ]
  edge [
    source 50
    target 1703
  ]
  edge [
    source 50
    target 508
  ]
  edge [
    source 50
    target 1704
  ]
  edge [
    source 50
    target 1684
  ]
  edge [
    source 50
    target 1705
  ]
  edge [
    source 50
    target 1006
  ]
  edge [
    source 50
    target 1706
  ]
  edge [
    source 50
    target 1707
  ]
  edge [
    source 50
    target 1708
  ]
  edge [
    source 50
    target 1709
  ]
  edge [
    source 50
    target 423
  ]
  edge [
    source 50
    target 1710
  ]
  edge [
    source 50
    target 350
  ]
  edge [
    source 50
    target 1711
  ]
  edge [
    source 50
    target 1712
  ]
  edge [
    source 50
    target 2105
  ]
  edge [
    source 50
    target 2106
  ]
  edge [
    source 50
    target 2107
  ]
  edge [
    source 50
    target 2108
  ]
  edge [
    source 50
    target 2109
  ]
  edge [
    source 50
    target 2110
  ]
  edge [
    source 50
    target 1168
  ]
  edge [
    source 50
    target 2111
  ]
  edge [
    source 50
    target 2112
  ]
  edge [
    source 50
    target 1388
  ]
  edge [
    source 50
    target 1664
  ]
  edge [
    source 50
    target 1665
  ]
  edge [
    source 50
    target 1666
  ]
  edge [
    source 50
    target 1667
  ]
  edge [
    source 50
    target 1668
  ]
  edge [
    source 50
    target 1669
  ]
  edge [
    source 50
    target 1670
  ]
  edge [
    source 50
    target 610
  ]
  edge [
    source 50
    target 1671
  ]
  edge [
    source 50
    target 1672
  ]
  edge [
    source 50
    target 1673
  ]
  edge [
    source 50
    target 612
  ]
  edge [
    source 50
    target 1674
  ]
  edge [
    source 50
    target 1675
  ]
  edge [
    source 50
    target 1676
  ]
  edge [
    source 50
    target 1677
  ]
  edge [
    source 50
    target 1678
  ]
  edge [
    source 50
    target 1681
  ]
  edge [
    source 50
    target 1682
  ]
  edge [
    source 50
    target 1683
  ]
  edge [
    source 50
    target 1394
  ]
  edge [
    source 50
    target 625
  ]
  edge [
    source 50
    target 384
  ]
  edge [
    source 50
    target 602
  ]
  edge [
    source 50
    target 732
  ]
  edge [
    source 50
    target 1685
  ]
  edge [
    source 50
    target 1686
  ]
  edge [
    source 50
    target 1356
  ]
  edge [
    source 50
    target 1687
  ]
  edge [
    source 50
    target 1382
  ]
  edge [
    source 50
    target 750
  ]
  edge [
    source 50
    target 1688
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 2113
  ]
  edge [
    source 52
    target 2114
  ]
  edge [
    source 52
    target 565
  ]
  edge [
    source 52
    target 2115
  ]
  edge [
    source 52
    target 883
  ]
  edge [
    source 52
    target 611
  ]
  edge [
    source 52
    target 884
  ]
  edge [
    source 52
    target 614
  ]
  edge [
    source 52
    target 885
  ]
  edge [
    source 52
    target 880
  ]
  edge [
    source 52
    target 886
  ]
  edge [
    source 52
    target 887
  ]
  edge [
    source 52
    target 888
  ]
  edge [
    source 52
    target 889
  ]
  edge [
    source 52
    target 101
  ]
  edge [
    source 52
    target 890
  ]
  edge [
    source 52
    target 891
  ]
  edge [
    source 52
    target 892
  ]
  edge [
    source 52
    target 893
  ]
  edge [
    source 52
    target 894
  ]
  edge [
    source 52
    target 895
  ]
  edge [
    source 52
    target 896
  ]
  edge [
    source 52
    target 776
  ]
  edge [
    source 52
    target 897
  ]
  edge [
    source 52
    target 898
  ]
  edge [
    source 52
    target 899
  ]
  edge [
    source 52
    target 900
  ]
  edge [
    source 52
    target 901
  ]
  edge [
    source 52
    target 902
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 2116
  ]
  edge [
    source 53
    target 2117
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 904
  ]
  edge [
    source 56
    target 2118
  ]
  edge [
    source 56
    target 2119
  ]
  edge [
    source 56
    target 2120
  ]
  edge [
    source 56
    target 2121
  ]
  edge [
    source 56
    target 2122
  ]
  edge [
    source 56
    target 565
  ]
  edge [
    source 56
    target 2123
  ]
  edge [
    source 56
    target 2124
  ]
  edge [
    source 56
    target 2125
  ]
  edge [
    source 56
    target 2126
  ]
  edge [
    source 56
    target 909
  ]
  edge [
    source 56
    target 751
  ]
  edge [
    source 56
    target 2127
  ]
  edge [
    source 56
    target 268
  ]
  edge [
    source 56
    target 2128
  ]
  edge [
    source 56
    target 2129
  ]
  edge [
    source 56
    target 2130
  ]
  edge [
    source 56
    target 464
  ]
  edge [
    source 56
    target 2131
  ]
  edge [
    source 56
    target 2132
  ]
  edge [
    source 56
    target 423
  ]
  edge [
    source 56
    target 2133
  ]
  edge [
    source 56
    target 2134
  ]
  edge [
    source 56
    target 2135
  ]
  edge [
    source 56
    target 2136
  ]
  edge [
    source 56
    target 749
  ]
  edge [
    source 56
    target 2137
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 58
    target 2138
  ]
  edge [
    source 58
    target 2139
  ]
  edge [
    source 58
    target 2140
  ]
  edge [
    source 58
    target 570
  ]
  edge [
    source 58
    target 2141
  ]
  edge [
    source 58
    target 2142
  ]
  edge [
    source 58
    target 2143
  ]
  edge [
    source 58
    target 2144
  ]
  edge [
    source 58
    target 2145
  ]
  edge [
    source 58
    target 1271
  ]
  edge [
    source 58
    target 2146
  ]
  edge [
    source 58
    target 2147
  ]
  edge [
    source 58
    target 1010
  ]
  edge [
    source 58
    target 2148
  ]
  edge [
    source 58
    target 2149
  ]
  edge [
    source 58
    target 286
  ]
  edge [
    source 58
    target 2150
  ]
  edge [
    source 58
    target 2151
  ]
  edge [
    source 58
    target 2152
  ]
  edge [
    source 58
    target 880
  ]
  edge [
    source 58
    target 454
  ]
  edge [
    source 58
    target 2153
  ]
  edge [
    source 58
    target 2154
  ]
  edge [
    source 58
    target 2155
  ]
  edge [
    source 58
    target 2156
  ]
  edge [
    source 58
    target 330
  ]
  edge [
    source 58
    target 1257
  ]
  edge [
    source 58
    target 2157
  ]
  edge [
    source 58
    target 2158
  ]
  edge [
    source 58
    target 1893
  ]
  edge [
    source 58
    target 2159
  ]
  edge [
    source 58
    target 732
  ]
  edge [
    source 58
    target 2160
  ]
  edge [
    source 58
    target 2161
  ]
  edge [
    source 58
    target 544
  ]
  edge [
    source 58
    target 2162
  ]
  edge [
    source 58
    target 2163
  ]
  edge [
    source 58
    target 2164
  ]
  edge [
    source 58
    target 1256
  ]
  edge [
    source 58
    target 2165
  ]
  edge [
    source 58
    target 2166
  ]
  edge [
    source 58
    target 1737
  ]
  edge [
    source 58
    target 547
  ]
  edge [
    source 58
    target 275
  ]
  edge [
    source 58
    target 2167
  ]
  edge [
    source 58
    target 464
  ]
  edge [
    source 58
    target 2168
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 1556
  ]
  edge [
    source 59
    target 2169
  ]
  edge [
    source 59
    target 2170
  ]
  edge [
    source 59
    target 2171
  ]
  edge [
    source 59
    target 220
  ]
  edge [
    source 59
    target 421
  ]
  edge [
    source 59
    target 2172
  ]
  edge [
    source 59
    target 1770
  ]
  edge [
    source 59
    target 216
  ]
  edge [
    source 59
    target 2173
  ]
  edge [
    source 59
    target 2174
  ]
  edge [
    source 59
    target 2175
  ]
  edge [
    source 59
    target 2176
  ]
  edge [
    source 59
    target 2177
  ]
  edge [
    source 59
    target 284
  ]
  edge [
    source 59
    target 2178
  ]
  edge [
    source 59
    target 1546
  ]
  edge [
    source 59
    target 2179
  ]
  edge [
    source 59
    target 2180
  ]
  edge [
    source 59
    target 2181
  ]
  edge [
    source 59
    target 2182
  ]
  edge [
    source 59
    target 270
  ]
  edge [
    source 59
    target 2183
  ]
  edge [
    source 59
    target 2184
  ]
  edge [
    source 59
    target 2185
  ]
  edge [
    source 59
    target 2186
  ]
  edge [
    source 59
    target 2187
  ]
  edge [
    source 59
    target 2188
  ]
  edge [
    source 59
    target 2189
  ]
  edge [
    source 59
    target 316
  ]
  edge [
    source 59
    target 2190
  ]
  edge [
    source 59
    target 2191
  ]
  edge [
    source 59
    target 2192
  ]
  edge [
    source 59
    target 2193
  ]
  edge [
    source 59
    target 2194
  ]
  edge [
    source 59
    target 169
  ]
  edge [
    source 59
    target 2195
  ]
  edge [
    source 59
    target 1784
  ]
  edge [
    source 59
    target 1571
  ]
  edge [
    source 59
    target 2196
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 2197
  ]
  edge [
    source 60
    target 2198
  ]
  edge [
    source 60
    target 832
  ]
  edge [
    source 60
    target 2199
  ]
  edge [
    source 60
    target 2200
  ]
  edge [
    source 60
    target 2201
  ]
  edge [
    source 61
    target 2202
  ]
  edge [
    source 61
    target 268
  ]
  edge [
    source 61
    target 757
  ]
  edge [
    source 61
    target 2203
  ]
  edge [
    source 61
    target 2204
  ]
  edge [
    source 61
    target 2205
  ]
  edge [
    source 61
    target 2206
  ]
  edge [
    source 61
    target 2207
  ]
  edge [
    source 61
    target 2208
  ]
  edge [
    source 61
    target 2209
  ]
  edge [
    source 61
    target 2210
  ]
  edge [
    source 61
    target 2211
  ]
  edge [
    source 61
    target 2212
  ]
  edge [
    source 61
    target 526
  ]
  edge [
    source 61
    target 1389
  ]
  edge [
    source 61
    target 2213
  ]
  edge [
    source 61
    target 2214
  ]
  edge [
    source 61
    target 2215
  ]
  edge [
    source 61
    target 2216
  ]
  edge [
    source 61
    target 2217
  ]
  edge [
    source 61
    target 2218
  ]
  edge [
    source 61
    target 2219
  ]
  edge [
    source 61
    target 2220
  ]
  edge [
    source 61
    target 987
  ]
  edge [
    source 61
    target 988
  ]
  edge [
    source 61
    target 2044
  ]
  edge [
    source 61
    target 2045
  ]
  edge [
    source 61
    target 989
  ]
  edge [
    source 61
    target 990
  ]
  edge [
    source 61
    target 2046
  ]
  edge [
    source 61
    target 276
  ]
  edge [
    source 61
    target 2047
  ]
  edge [
    source 61
    target 2048
  ]
  edge [
    source 61
    target 552
  ]
  edge [
    source 61
    target 992
  ]
  edge [
    source 61
    target 2049
  ]
  edge [
    source 61
    target 995
  ]
  edge [
    source 61
    target 996
  ]
  edge [
    source 61
    target 871
  ]
  edge [
    source 61
    target 997
  ]
  edge [
    source 61
    target 1456
  ]
  edge [
    source 61
    target 998
  ]
  edge [
    source 61
    target 999
  ]
  edge [
    source 61
    target 2050
  ]
  edge [
    source 61
    target 1074
  ]
  edge [
    source 61
    target 934
  ]
  edge [
    source 61
    target 1001
  ]
  edge [
    source 61
    target 1002
  ]
  edge [
    source 61
    target 2221
  ]
  edge [
    source 61
    target 688
  ]
  edge [
    source 61
    target 691
  ]
  edge [
    source 61
    target 693
  ]
  edge [
    source 61
    target 1695
  ]
  edge [
    source 61
    target 695
  ]
  edge [
    source 61
    target 2222
  ]
  edge [
    source 61
    target 2151
  ]
  edge [
    source 61
    target 454
  ]
  edge [
    source 61
    target 2153
  ]
  edge [
    source 61
    target 2154
  ]
  edge [
    source 61
    target 697
  ]
  edge [
    source 61
    target 2155
  ]
  edge [
    source 61
    target 2156
  ]
  edge [
    source 61
    target 699
  ]
  edge [
    source 61
    target 1257
  ]
  edge [
    source 61
    target 702
  ]
  edge [
    source 61
    target 707
  ]
  edge [
    source 61
    target 508
  ]
  edge [
    source 61
    target 2157
  ]
  edge [
    source 61
    target 2160
  ]
  edge [
    source 61
    target 2162
  ]
  edge [
    source 61
    target 2163
  ]
  edge [
    source 61
    target 407
  ]
  edge [
    source 61
    target 1708
  ]
  edge [
    source 61
    target 1256
  ]
  edge [
    source 61
    target 2165
  ]
  edge [
    source 61
    target 716
  ]
  edge [
    source 61
    target 2167
  ]
  edge [
    source 61
    target 2223
  ]
  edge [
    source 61
    target 2224
  ]
  edge [
    source 61
    target 2225
  ]
  edge [
    source 61
    target 2226
  ]
  edge [
    source 61
    target 2227
  ]
  edge [
    source 61
    target 434
  ]
  edge [
    source 61
    target 2228
  ]
  edge [
    source 61
    target 2229
  ]
  edge [
    source 61
    target 2230
  ]
  edge [
    source 61
    target 2231
  ]
  edge [
    source 61
    target 284
  ]
  edge [
    source 61
    target 2232
  ]
  edge [
    source 61
    target 2233
  ]
  edge [
    source 61
    target 1736
  ]
  edge [
    source 61
    target 1873
  ]
  edge [
    source 61
    target 2234
  ]
  edge [
    source 61
    target 1825
  ]
  edge [
    source 61
    target 316
  ]
  edge [
    source 61
    target 2235
  ]
  edge [
    source 61
    target 881
  ]
  edge [
    source 61
    target 2236
  ]
  edge [
    source 61
    target 1828
  ]
  edge [
    source 61
    target 380
  ]
  edge [
    source 61
    target 2237
  ]
  edge [
    source 61
    target 2238
  ]
  edge [
    source 61
    target 2239
  ]
  edge [
    source 61
    target 2240
  ]
  edge [
    source 61
    target 2241
  ]
  edge [
    source 61
    target 2242
  ]
  edge [
    source 61
    target 2243
  ]
  edge [
    source 61
    target 1748
  ]
  edge [
    source 61
    target 1999
  ]
  edge [
    source 61
    target 2244
  ]
  edge [
    source 61
    target 2245
  ]
  edge [
    source 61
    target 181
  ]
  edge [
    source 61
    target 461
  ]
  edge [
    source 61
    target 1364
  ]
  edge [
    source 61
    target 2246
  ]
  edge [
    source 61
    target 547
  ]
  edge [
    source 61
    target 2247
  ]
  edge [
    source 61
    target 2248
  ]
  edge [
    source 61
    target 883
  ]
  edge [
    source 61
    target 2249
  ]
  edge [
    source 61
    target 1015
  ]
  edge [
    source 61
    target 2250
  ]
  edge [
    source 61
    target 2251
  ]
  edge [
    source 61
    target 613
  ]
  edge [
    source 61
    target 2252
  ]
  edge [
    source 61
    target 2253
  ]
  edge [
    source 61
    target 1323
  ]
  edge [
    source 61
    target 2254
  ]
  edge [
    source 61
    target 2255
  ]
  edge [
    source 61
    target 2256
  ]
  edge [
    source 61
    target 2257
  ]
  edge [
    source 61
    target 2258
  ]
  edge [
    source 61
    target 2259
  ]
  edge [
    source 61
    target 889
  ]
  edge [
    source 61
    target 101
  ]
  edge [
    source 61
    target 2260
  ]
  edge [
    source 61
    target 890
  ]
  edge [
    source 61
    target 732
  ]
  edge [
    source 61
    target 2261
  ]
  edge [
    source 61
    target 2262
  ]
  edge [
    source 61
    target 2263
  ]
  edge [
    source 61
    target 892
  ]
  edge [
    source 61
    target 893
  ]
  edge [
    source 61
    target 2264
  ]
  edge [
    source 61
    target 895
  ]
  edge [
    source 61
    target 2265
  ]
  edge [
    source 61
    target 776
  ]
  edge [
    source 61
    target 899
  ]
  edge [
    source 61
    target 896
  ]
  edge [
    source 61
    target 901
  ]
  edge [
    source 61
    target 2266
  ]
  edge [
    source 61
    target 2267
  ]
  edge [
    source 61
    target 2268
  ]
  edge [
    source 61
    target 2269
  ]
  edge [
    source 61
    target 2270
  ]
  edge [
    source 61
    target 2271
  ]
  edge [
    source 61
    target 2272
  ]
  edge [
    source 61
    target 2273
  ]
  edge [
    source 61
    target 2274
  ]
  edge [
    source 61
    target 2275
  ]
  edge [
    source 61
    target 2276
  ]
  edge [
    source 61
    target 2277
  ]
  edge [
    source 61
    target 2278
  ]
  edge [
    source 61
    target 2279
  ]
  edge [
    source 61
    target 2280
  ]
  edge [
    source 61
    target 2281
  ]
  edge [
    source 61
    target 2282
  ]
  edge [
    source 61
    target 2283
  ]
  edge [
    source 61
    target 698
  ]
  edge [
    source 61
    target 2284
  ]
]
