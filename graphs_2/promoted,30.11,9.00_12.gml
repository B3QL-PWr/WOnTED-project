graph [
  node [
    id 0
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 1
    label "szef"
    origin "text"
  ]
  node [
    id 2
    label "jednostka"
    origin "text"
  ]
  node [
    id 3
    label "wojskowy"
    origin "text"
  ]
  node [
    id 4
    label "wojsko"
    origin "text"
  ]
  node [
    id 5
    label "specjalny"
    origin "text"
  ]
  node [
    id 6
    label "gra"
    origin "text"
  ]
  node [
    id 7
    label "genera&#322;"
    origin "text"
  ]
  node [
    id 8
    label "roman"
    origin "text"
  ]
  node [
    id 9
    label "polka"
    origin "text"
  ]
  node [
    id 10
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 11
    label "pozwala&#263;"
    origin "text"
  ]
  node [
    id 12
    label "narzuca&#263;"
    origin "text"
  ]
  node [
    id 13
    label "siebie"
    origin "text"
  ]
  node [
    id 14
    label "rosyjski"
    origin "text"
  ]
  node [
    id 15
    label "narracja"
    origin "text"
  ]
  node [
    id 16
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 17
    label "powodowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "wierzch"
    origin "text"
  ]
  node [
    id 19
    label "wychodzi&#263;"
    origin "text"
  ]
  node [
    id 20
    label "wr&#281;cz"
    origin "text"
  ]
  node [
    id 21
    label "wrogo&#347;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 23
    label "polska"
    origin "text"
  ]
  node [
    id 24
    label "ukraina"
    origin "text"
  ]
  node [
    id 25
    label "dawny"
  ]
  node [
    id 26
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 27
    label "eksprezydent"
  ]
  node [
    id 28
    label "partner"
  ]
  node [
    id 29
    label "rozw&#243;d"
  ]
  node [
    id 30
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 31
    label "wcze&#347;niejszy"
  ]
  node [
    id 32
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 33
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 34
    label "pracownik"
  ]
  node [
    id 35
    label "przedsi&#281;biorca"
  ]
  node [
    id 36
    label "cz&#322;owiek"
  ]
  node [
    id 37
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 38
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 39
    label "kolaborator"
  ]
  node [
    id 40
    label "prowadzi&#263;"
  ]
  node [
    id 41
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 42
    label "sp&#243;lnik"
  ]
  node [
    id 43
    label "aktor"
  ]
  node [
    id 44
    label "uczestniczenie"
  ]
  node [
    id 45
    label "wcze&#347;niej"
  ]
  node [
    id 46
    label "przestarza&#322;y"
  ]
  node [
    id 47
    label "odleg&#322;y"
  ]
  node [
    id 48
    label "przesz&#322;y"
  ]
  node [
    id 49
    label "od_dawna"
  ]
  node [
    id 50
    label "poprzedni"
  ]
  node [
    id 51
    label "dawno"
  ]
  node [
    id 52
    label "d&#322;ugoletni"
  ]
  node [
    id 53
    label "anachroniczny"
  ]
  node [
    id 54
    label "dawniej"
  ]
  node [
    id 55
    label "niegdysiejszy"
  ]
  node [
    id 56
    label "kombatant"
  ]
  node [
    id 57
    label "stary"
  ]
  node [
    id 58
    label "rozstanie"
  ]
  node [
    id 59
    label "ekspartner"
  ]
  node [
    id 60
    label "rozbita_rodzina"
  ]
  node [
    id 61
    label "uniewa&#380;nienie"
  ]
  node [
    id 62
    label "separation"
  ]
  node [
    id 63
    label "prezydent"
  ]
  node [
    id 64
    label "pryncypa&#322;"
  ]
  node [
    id 65
    label "kierownictwo"
  ]
  node [
    id 66
    label "kierowa&#263;"
  ]
  node [
    id 67
    label "zwrot"
  ]
  node [
    id 68
    label "punkt"
  ]
  node [
    id 69
    label "turn"
  ]
  node [
    id 70
    label "turning"
  ]
  node [
    id 71
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 72
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 73
    label "skr&#281;t"
  ]
  node [
    id 74
    label "obr&#243;t"
  ]
  node [
    id 75
    label "fraza_czasownikowa"
  ]
  node [
    id 76
    label "jednostka_leksykalna"
  ]
  node [
    id 77
    label "zmiana"
  ]
  node [
    id 78
    label "wyra&#380;enie"
  ]
  node [
    id 79
    label "ludzko&#347;&#263;"
  ]
  node [
    id 80
    label "asymilowanie"
  ]
  node [
    id 81
    label "wapniak"
  ]
  node [
    id 82
    label "asymilowa&#263;"
  ]
  node [
    id 83
    label "os&#322;abia&#263;"
  ]
  node [
    id 84
    label "posta&#263;"
  ]
  node [
    id 85
    label "hominid"
  ]
  node [
    id 86
    label "podw&#322;adny"
  ]
  node [
    id 87
    label "os&#322;abianie"
  ]
  node [
    id 88
    label "g&#322;owa"
  ]
  node [
    id 89
    label "figura"
  ]
  node [
    id 90
    label "portrecista"
  ]
  node [
    id 91
    label "dwun&#243;g"
  ]
  node [
    id 92
    label "profanum"
  ]
  node [
    id 93
    label "mikrokosmos"
  ]
  node [
    id 94
    label "nasada"
  ]
  node [
    id 95
    label "duch"
  ]
  node [
    id 96
    label "antropochoria"
  ]
  node [
    id 97
    label "osoba"
  ]
  node [
    id 98
    label "wz&#243;r"
  ]
  node [
    id 99
    label "senior"
  ]
  node [
    id 100
    label "oddzia&#322;ywanie"
  ]
  node [
    id 101
    label "Adam"
  ]
  node [
    id 102
    label "homo_sapiens"
  ]
  node [
    id 103
    label "polifag"
  ]
  node [
    id 104
    label "biuro"
  ]
  node [
    id 105
    label "lead"
  ]
  node [
    id 106
    label "zesp&#243;&#322;"
  ]
  node [
    id 107
    label "siedziba"
  ]
  node [
    id 108
    label "praca"
  ]
  node [
    id 109
    label "w&#322;adza"
  ]
  node [
    id 110
    label "g&#322;os"
  ]
  node [
    id 111
    label "zwierzchnik"
  ]
  node [
    id 112
    label "sterowa&#263;"
  ]
  node [
    id 113
    label "wysy&#322;a&#263;"
  ]
  node [
    id 114
    label "manipulate"
  ]
  node [
    id 115
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 116
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 117
    label "ustawia&#263;"
  ]
  node [
    id 118
    label "give"
  ]
  node [
    id 119
    label "przeznacza&#263;"
  ]
  node [
    id 120
    label "control"
  ]
  node [
    id 121
    label "match"
  ]
  node [
    id 122
    label "motywowa&#263;"
  ]
  node [
    id 123
    label "administrowa&#263;"
  ]
  node [
    id 124
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 125
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 126
    label "order"
  ]
  node [
    id 127
    label "indicate"
  ]
  node [
    id 128
    label "przyswoi&#263;"
  ]
  node [
    id 129
    label "one"
  ]
  node [
    id 130
    label "poj&#281;cie"
  ]
  node [
    id 131
    label "ewoluowanie"
  ]
  node [
    id 132
    label "supremum"
  ]
  node [
    id 133
    label "skala"
  ]
  node [
    id 134
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 135
    label "przyswajanie"
  ]
  node [
    id 136
    label "wyewoluowanie"
  ]
  node [
    id 137
    label "reakcja"
  ]
  node [
    id 138
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 139
    label "przeliczy&#263;"
  ]
  node [
    id 140
    label "wyewoluowa&#263;"
  ]
  node [
    id 141
    label "ewoluowa&#263;"
  ]
  node [
    id 142
    label "matematyka"
  ]
  node [
    id 143
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 144
    label "rzut"
  ]
  node [
    id 145
    label "liczba_naturalna"
  ]
  node [
    id 146
    label "czynnik_biotyczny"
  ]
  node [
    id 147
    label "individual"
  ]
  node [
    id 148
    label "obiekt"
  ]
  node [
    id 149
    label "przyswaja&#263;"
  ]
  node [
    id 150
    label "przyswojenie"
  ]
  node [
    id 151
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 152
    label "starzenie_si&#281;"
  ]
  node [
    id 153
    label "przeliczanie"
  ]
  node [
    id 154
    label "funkcja"
  ]
  node [
    id 155
    label "przelicza&#263;"
  ]
  node [
    id 156
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 157
    label "infimum"
  ]
  node [
    id 158
    label "przeliczenie"
  ]
  node [
    id 159
    label "pos&#322;uchanie"
  ]
  node [
    id 160
    label "skumanie"
  ]
  node [
    id 161
    label "orientacja"
  ]
  node [
    id 162
    label "wytw&#243;r"
  ]
  node [
    id 163
    label "zorientowanie"
  ]
  node [
    id 164
    label "teoria"
  ]
  node [
    id 165
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 166
    label "clasp"
  ]
  node [
    id 167
    label "forma"
  ]
  node [
    id 168
    label "przem&#243;wienie"
  ]
  node [
    id 169
    label "co&#347;"
  ]
  node [
    id 170
    label "budynek"
  ]
  node [
    id 171
    label "thing"
  ]
  node [
    id 172
    label "program"
  ]
  node [
    id 173
    label "rzecz"
  ]
  node [
    id 174
    label "strona"
  ]
  node [
    id 175
    label "Chocho&#322;"
  ]
  node [
    id 176
    label "Herkules_Poirot"
  ]
  node [
    id 177
    label "Edyp"
  ]
  node [
    id 178
    label "parali&#380;owa&#263;"
  ]
  node [
    id 179
    label "Harry_Potter"
  ]
  node [
    id 180
    label "Casanova"
  ]
  node [
    id 181
    label "Zgredek"
  ]
  node [
    id 182
    label "Gargantua"
  ]
  node [
    id 183
    label "Winnetou"
  ]
  node [
    id 184
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 185
    label "Dulcynea"
  ]
  node [
    id 186
    label "kategoria_gramatyczna"
  ]
  node [
    id 187
    label "person"
  ]
  node [
    id 188
    label "Plastu&#347;"
  ]
  node [
    id 189
    label "Quasimodo"
  ]
  node [
    id 190
    label "Sherlock_Holmes"
  ]
  node [
    id 191
    label "Faust"
  ]
  node [
    id 192
    label "Wallenrod"
  ]
  node [
    id 193
    label "Dwukwiat"
  ]
  node [
    id 194
    label "Don_Juan"
  ]
  node [
    id 195
    label "koniugacja"
  ]
  node [
    id 196
    label "Don_Kiszot"
  ]
  node [
    id 197
    label "Hamlet"
  ]
  node [
    id 198
    label "Werter"
  ]
  node [
    id 199
    label "istota"
  ]
  node [
    id 200
    label "Szwejk"
  ]
  node [
    id 201
    label "integer"
  ]
  node [
    id 202
    label "liczba"
  ]
  node [
    id 203
    label "zlewanie_si&#281;"
  ]
  node [
    id 204
    label "ilo&#347;&#263;"
  ]
  node [
    id 205
    label "uk&#322;ad"
  ]
  node [
    id 206
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 207
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 208
    label "pe&#322;ny"
  ]
  node [
    id 209
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 210
    label "Rzym_Zachodni"
  ]
  node [
    id 211
    label "whole"
  ]
  node [
    id 212
    label "element"
  ]
  node [
    id 213
    label "Rzym_Wschodni"
  ]
  node [
    id 214
    label "urz&#261;dzenie"
  ]
  node [
    id 215
    label "masztab"
  ]
  node [
    id 216
    label "kreska"
  ]
  node [
    id 217
    label "podzia&#322;ka"
  ]
  node [
    id 218
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 219
    label "wielko&#347;&#263;"
  ]
  node [
    id 220
    label "zero"
  ]
  node [
    id 221
    label "interwa&#322;"
  ]
  node [
    id 222
    label "przymiar"
  ]
  node [
    id 223
    label "struktura"
  ]
  node [
    id 224
    label "sfera"
  ]
  node [
    id 225
    label "zbi&#243;r"
  ]
  node [
    id 226
    label "dominanta"
  ]
  node [
    id 227
    label "tetrachord"
  ]
  node [
    id 228
    label "scale"
  ]
  node [
    id 229
    label "przedzia&#322;"
  ]
  node [
    id 230
    label "podzakres"
  ]
  node [
    id 231
    label "proporcja"
  ]
  node [
    id 232
    label "dziedzina"
  ]
  node [
    id 233
    label "part"
  ]
  node [
    id 234
    label "rejestr"
  ]
  node [
    id 235
    label "subdominanta"
  ]
  node [
    id 236
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 237
    label "atom"
  ]
  node [
    id 238
    label "odbicie"
  ]
  node [
    id 239
    label "przyroda"
  ]
  node [
    id 240
    label "Ziemia"
  ]
  node [
    id 241
    label "kosmos"
  ]
  node [
    id 242
    label "miniatura"
  ]
  node [
    id 243
    label "czyn"
  ]
  node [
    id 244
    label "addytywno&#347;&#263;"
  ]
  node [
    id 245
    label "function"
  ]
  node [
    id 246
    label "zastosowanie"
  ]
  node [
    id 247
    label "funkcjonowanie"
  ]
  node [
    id 248
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 249
    label "powierzanie"
  ]
  node [
    id 250
    label "cel"
  ]
  node [
    id 251
    label "przeciwdziedzina"
  ]
  node [
    id 252
    label "awansowa&#263;"
  ]
  node [
    id 253
    label "stawia&#263;"
  ]
  node [
    id 254
    label "wakowa&#263;"
  ]
  node [
    id 255
    label "znaczenie"
  ]
  node [
    id 256
    label "postawi&#263;"
  ]
  node [
    id 257
    label "awansowanie"
  ]
  node [
    id 258
    label "wymienienie"
  ]
  node [
    id 259
    label "przerachowanie"
  ]
  node [
    id 260
    label "skontrolowanie"
  ]
  node [
    id 261
    label "count"
  ]
  node [
    id 262
    label "sprawdza&#263;"
  ]
  node [
    id 263
    label "zmienia&#263;"
  ]
  node [
    id 264
    label "przerachowywa&#263;"
  ]
  node [
    id 265
    label "ograniczenie"
  ]
  node [
    id 266
    label "armia"
  ]
  node [
    id 267
    label "nawr&#243;t_choroby"
  ]
  node [
    id 268
    label "potomstwo"
  ]
  node [
    id 269
    label "odwzorowanie"
  ]
  node [
    id 270
    label "rysunek"
  ]
  node [
    id 271
    label "scene"
  ]
  node [
    id 272
    label "throw"
  ]
  node [
    id 273
    label "float"
  ]
  node [
    id 274
    label "projection"
  ]
  node [
    id 275
    label "injection"
  ]
  node [
    id 276
    label "blow"
  ]
  node [
    id 277
    label "pomys&#322;"
  ]
  node [
    id 278
    label "ruch"
  ]
  node [
    id 279
    label "k&#322;ad"
  ]
  node [
    id 280
    label "mold"
  ]
  node [
    id 281
    label "sprawdzanie"
  ]
  node [
    id 282
    label "zast&#281;powanie"
  ]
  node [
    id 283
    label "przerachowywanie"
  ]
  node [
    id 284
    label "rachunek_operatorowy"
  ]
  node [
    id 285
    label "przedmiot"
  ]
  node [
    id 286
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 287
    label "kryptologia"
  ]
  node [
    id 288
    label "logicyzm"
  ]
  node [
    id 289
    label "logika"
  ]
  node [
    id 290
    label "matematyka_czysta"
  ]
  node [
    id 291
    label "forsing"
  ]
  node [
    id 292
    label "modelowanie_matematyczne"
  ]
  node [
    id 293
    label "matma"
  ]
  node [
    id 294
    label "teoria_katastrof"
  ]
  node [
    id 295
    label "kierunek"
  ]
  node [
    id 296
    label "fizyka_matematyczna"
  ]
  node [
    id 297
    label "teoria_graf&#243;w"
  ]
  node [
    id 298
    label "rachunki"
  ]
  node [
    id 299
    label "topologia_algebraiczna"
  ]
  node [
    id 300
    label "matematyka_stosowana"
  ]
  node [
    id 301
    label "sprawdzi&#263;"
  ]
  node [
    id 302
    label "change"
  ]
  node [
    id 303
    label "przerachowa&#263;"
  ]
  node [
    id 304
    label "zmieni&#263;"
  ]
  node [
    id 305
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 306
    label "organizm"
  ]
  node [
    id 307
    label "translate"
  ]
  node [
    id 308
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 309
    label "kultura"
  ]
  node [
    id 310
    label "pobra&#263;"
  ]
  node [
    id 311
    label "thrill"
  ]
  node [
    id 312
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 313
    label "przeobra&#380;a&#263;_si&#281;"
  ]
  node [
    id 314
    label "pobranie"
  ]
  node [
    id 315
    label "wyniesienie"
  ]
  node [
    id 316
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 317
    label "assimilation"
  ]
  node [
    id 318
    label "emotion"
  ]
  node [
    id 319
    label "nauczenie_si&#281;"
  ]
  node [
    id 320
    label "zaczerpni&#281;cie"
  ]
  node [
    id 321
    label "mechanizm_obronny"
  ]
  node [
    id 322
    label "convention"
  ]
  node [
    id 323
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 324
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 325
    label "czerpanie"
  ]
  node [
    id 326
    label "uczenie_si&#281;"
  ]
  node [
    id 327
    label "pobieranie"
  ]
  node [
    id 328
    label "acquisition"
  ]
  node [
    id 329
    label "od&#380;ywianie"
  ]
  node [
    id 330
    label "wynoszenie"
  ]
  node [
    id 331
    label "absorption"
  ]
  node [
    id 332
    label "wdra&#380;anie_si&#281;"
  ]
  node [
    id 333
    label "react"
  ]
  node [
    id 334
    label "zachowanie"
  ]
  node [
    id 335
    label "reaction"
  ]
  node [
    id 336
    label "rozmowa"
  ]
  node [
    id 337
    label "response"
  ]
  node [
    id 338
    label "rezultat"
  ]
  node [
    id 339
    label "respondent"
  ]
  node [
    id 340
    label "przeobrazi&#263;_si&#281;"
  ]
  node [
    id 341
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 342
    label "treat"
  ]
  node [
    id 343
    label "czerpa&#263;"
  ]
  node [
    id 344
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 345
    label "pobiera&#263;"
  ]
  node [
    id 346
    label "rede"
  ]
  node [
    id 347
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 348
    label "kszta&#322;t"
  ]
  node [
    id 349
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 350
    label "wiedza"
  ]
  node [
    id 351
    label "alkohol"
  ]
  node [
    id 352
    label "zdolno&#347;&#263;"
  ]
  node [
    id 353
    label "cecha"
  ]
  node [
    id 354
    label "&#380;ycie"
  ]
  node [
    id 355
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 356
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 357
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 358
    label "sztuka"
  ]
  node [
    id 359
    label "dekiel"
  ]
  node [
    id 360
    label "ro&#347;lina"
  ]
  node [
    id 361
    label "&#347;ci&#281;cie"
  ]
  node [
    id 362
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 363
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 364
    label "&#347;ci&#281;gno"
  ]
  node [
    id 365
    label "noosfera"
  ]
  node [
    id 366
    label "byd&#322;o"
  ]
  node [
    id 367
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 368
    label "makrocefalia"
  ]
  node [
    id 369
    label "ucho"
  ]
  node [
    id 370
    label "g&#243;ra"
  ]
  node [
    id 371
    label "m&#243;zg"
  ]
  node [
    id 372
    label "fryzura"
  ]
  node [
    id 373
    label "umys&#322;"
  ]
  node [
    id 374
    label "cia&#322;o"
  ]
  node [
    id 375
    label "cz&#322;onek"
  ]
  node [
    id 376
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 377
    label "czaszka"
  ]
  node [
    id 378
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 379
    label "powodowanie"
  ]
  node [
    id 380
    label "hipnotyzowanie"
  ]
  node [
    id 381
    label "&#347;lad"
  ]
  node [
    id 382
    label "docieranie"
  ]
  node [
    id 383
    label "natural_process"
  ]
  node [
    id 384
    label "reakcja_chemiczna"
  ]
  node [
    id 385
    label "wdzieranie_si&#281;"
  ]
  node [
    id 386
    label "zjawisko"
  ]
  node [
    id 387
    label "act"
  ]
  node [
    id 388
    label "lobbysta"
  ]
  node [
    id 389
    label "allochoria"
  ]
  node [
    id 390
    label "wygl&#261;d"
  ]
  node [
    id 391
    label "fotograf"
  ]
  node [
    id 392
    label "malarz"
  ]
  node [
    id 393
    label "artysta"
  ]
  node [
    id 394
    label "charakterystyka"
  ]
  node [
    id 395
    label "p&#322;aszczyzna"
  ]
  node [
    id 396
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 397
    label "bierka_szachowa"
  ]
  node [
    id 398
    label "obiekt_matematyczny"
  ]
  node [
    id 399
    label "gestaltyzm"
  ]
  node [
    id 400
    label "styl"
  ]
  node [
    id 401
    label "obraz"
  ]
  node [
    id 402
    label "Osjan"
  ]
  node [
    id 403
    label "d&#378;wi&#281;k"
  ]
  node [
    id 404
    label "character"
  ]
  node [
    id 405
    label "kto&#347;"
  ]
  node [
    id 406
    label "rze&#378;ba"
  ]
  node [
    id 407
    label "stylistyka"
  ]
  node [
    id 408
    label "figure"
  ]
  node [
    id 409
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 410
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 411
    label "miejsce"
  ]
  node [
    id 412
    label "antycypacja"
  ]
  node [
    id 413
    label "ornamentyka"
  ]
  node [
    id 414
    label "informacja"
  ]
  node [
    id 415
    label "Aspazja"
  ]
  node [
    id 416
    label "facet"
  ]
  node [
    id 417
    label "popis"
  ]
  node [
    id 418
    label "wiersz"
  ]
  node [
    id 419
    label "kompleksja"
  ]
  node [
    id 420
    label "budowa"
  ]
  node [
    id 421
    label "symetria"
  ]
  node [
    id 422
    label "lingwistyka_kognitywna"
  ]
  node [
    id 423
    label "karta"
  ]
  node [
    id 424
    label "shape"
  ]
  node [
    id 425
    label "podzbi&#243;r"
  ]
  node [
    id 426
    label "przedstawienie"
  ]
  node [
    id 427
    label "point"
  ]
  node [
    id 428
    label "perspektywa"
  ]
  node [
    id 429
    label "piek&#322;o"
  ]
  node [
    id 430
    label "human_body"
  ]
  node [
    id 431
    label "ofiarowywanie"
  ]
  node [
    id 432
    label "sfera_afektywna"
  ]
  node [
    id 433
    label "nekromancja"
  ]
  node [
    id 434
    label "Po&#347;wist"
  ]
  node [
    id 435
    label "podekscytowanie"
  ]
  node [
    id 436
    label "deformowanie"
  ]
  node [
    id 437
    label "sumienie"
  ]
  node [
    id 438
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 439
    label "deformowa&#263;"
  ]
  node [
    id 440
    label "osobowo&#347;&#263;"
  ]
  node [
    id 441
    label "psychika"
  ]
  node [
    id 442
    label "zjawa"
  ]
  node [
    id 443
    label "zmar&#322;y"
  ]
  node [
    id 444
    label "istota_nadprzyrodzona"
  ]
  node [
    id 445
    label "power"
  ]
  node [
    id 446
    label "entity"
  ]
  node [
    id 447
    label "ofiarowywa&#263;"
  ]
  node [
    id 448
    label "oddech"
  ]
  node [
    id 449
    label "seksualno&#347;&#263;"
  ]
  node [
    id 450
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 451
    label "byt"
  ]
  node [
    id 452
    label "si&#322;a"
  ]
  node [
    id 453
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 454
    label "ego"
  ]
  node [
    id 455
    label "ofiarowanie"
  ]
  node [
    id 456
    label "charakter"
  ]
  node [
    id 457
    label "fizjonomia"
  ]
  node [
    id 458
    label "kompleks"
  ]
  node [
    id 459
    label "zapalno&#347;&#263;"
  ]
  node [
    id 460
    label "T&#281;sknica"
  ]
  node [
    id 461
    label "ofiarowa&#263;"
  ]
  node [
    id 462
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 463
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 464
    label "passion"
  ]
  node [
    id 465
    label "so&#322;dat"
  ]
  node [
    id 466
    label "rota"
  ]
  node [
    id 467
    label "militarnie"
  ]
  node [
    id 468
    label "elew"
  ]
  node [
    id 469
    label "wojskowo"
  ]
  node [
    id 470
    label "zdemobilizowanie"
  ]
  node [
    id 471
    label "Gurkha"
  ]
  node [
    id 472
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 473
    label "walcz&#261;cy"
  ]
  node [
    id 474
    label "&#380;o&#322;dowy"
  ]
  node [
    id 475
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 476
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 477
    label "antybalistyczny"
  ]
  node [
    id 478
    label "demobilizowa&#263;"
  ]
  node [
    id 479
    label "podleg&#322;y"
  ]
  node [
    id 480
    label "harcap"
  ]
  node [
    id 481
    label "demobilizowanie"
  ]
  node [
    id 482
    label "typowy"
  ]
  node [
    id 483
    label "mundurowy"
  ]
  node [
    id 484
    label "zdemobilizowa&#263;"
  ]
  node [
    id 485
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 486
    label "zwyczajny"
  ]
  node [
    id 487
    label "typowo"
  ]
  node [
    id 488
    label "cz&#281;sty"
  ]
  node [
    id 489
    label "zwyk&#322;y"
  ]
  node [
    id 490
    label "intencjonalny"
  ]
  node [
    id 491
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 492
    label "niedorozw&#243;j"
  ]
  node [
    id 493
    label "szczeg&#243;lny"
  ]
  node [
    id 494
    label "specjalnie"
  ]
  node [
    id 495
    label "nieetatowy"
  ]
  node [
    id 496
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 497
    label "nienormalny"
  ]
  node [
    id 498
    label "umy&#347;lnie"
  ]
  node [
    id 499
    label "odpowiedni"
  ]
  node [
    id 500
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 501
    label "zale&#380;ny"
  ]
  node [
    id 502
    label "podlegle"
  ]
  node [
    id 503
    label "militarny"
  ]
  node [
    id 504
    label "antyrakietowy"
  ]
  node [
    id 505
    label "ucze&#324;"
  ]
  node [
    id 506
    label "&#380;o&#322;nierz"
  ]
  node [
    id 507
    label "odstr&#281;czenie"
  ]
  node [
    id 508
    label "zreorganizowanie"
  ]
  node [
    id 509
    label "odprawienie"
  ]
  node [
    id 510
    label "zniech&#281;cenie_si&#281;"
  ]
  node [
    id 511
    label "zniech&#281;ca&#263;"
  ]
  node [
    id 512
    label "zwalnia&#263;"
  ]
  node [
    id 513
    label "przebudowywa&#263;"
  ]
  node [
    id 514
    label "Nepal"
  ]
  node [
    id 515
    label "peruka"
  ]
  node [
    id 516
    label "warkocz"
  ]
  node [
    id 517
    label "zwalnianie"
  ]
  node [
    id 518
    label "zniech&#281;canie"
  ]
  node [
    id 519
    label "przebudowywanie"
  ]
  node [
    id 520
    label "zniech&#281;canie_si&#281;"
  ]
  node [
    id 521
    label "funkcjonariusz"
  ]
  node [
    id 522
    label "nosiciel"
  ]
  node [
    id 523
    label "wojownik"
  ]
  node [
    id 524
    label "zniech&#281;ci&#263;"
  ]
  node [
    id 525
    label "zreorganizowa&#263;"
  ]
  node [
    id 526
    label "odprawi&#263;"
  ]
  node [
    id 527
    label "piecz&#261;tka"
  ]
  node [
    id 528
    label "s&#261;d_ko&#347;cielny"
  ]
  node [
    id 529
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 530
    label "&#322;ama&#263;"
  ]
  node [
    id 531
    label "stra&#380;_po&#380;arna"
  ]
  node [
    id 532
    label "tortury"
  ]
  node [
    id 533
    label "papie&#380;"
  ]
  node [
    id 534
    label "chordofon_szarpany"
  ]
  node [
    id 535
    label "przysi&#281;ga"
  ]
  node [
    id 536
    label "&#322;amanie"
  ]
  node [
    id 537
    label "szyk"
  ]
  node [
    id 538
    label "s&#261;d_apelacyjny"
  ]
  node [
    id 539
    label "whip"
  ]
  node [
    id 540
    label "Rota"
  ]
  node [
    id 541
    label "instrument_strunowy"
  ]
  node [
    id 542
    label "formu&#322;a"
  ]
  node [
    id 543
    label "zrejterowanie"
  ]
  node [
    id 544
    label "zmobilizowa&#263;"
  ]
  node [
    id 545
    label "dezerter"
  ]
  node [
    id 546
    label "oddzia&#322;_karny"
  ]
  node [
    id 547
    label "rezerwa"
  ]
  node [
    id 548
    label "tabor"
  ]
  node [
    id 549
    label "wermacht"
  ]
  node [
    id 550
    label "cofni&#281;cie"
  ]
  node [
    id 551
    label "potencja"
  ]
  node [
    id 552
    label "fala"
  ]
  node [
    id 553
    label "szko&#322;a"
  ]
  node [
    id 554
    label "korpus"
  ]
  node [
    id 555
    label "soldateska"
  ]
  node [
    id 556
    label "ods&#322;ugiwanie"
  ]
  node [
    id 557
    label "werbowanie_si&#281;"
  ]
  node [
    id 558
    label "oddzia&#322;"
  ]
  node [
    id 559
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 560
    label "s&#322;u&#380;ba"
  ]
  node [
    id 561
    label "or&#281;&#380;"
  ]
  node [
    id 562
    label "Legia_Cudzoziemska"
  ]
  node [
    id 563
    label "Armia_Czerwona"
  ]
  node [
    id 564
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 565
    label "rejterowanie"
  ]
  node [
    id 566
    label "Czerwona_Gwardia"
  ]
  node [
    id 567
    label "zrejterowa&#263;"
  ]
  node [
    id 568
    label "sztabslekarz"
  ]
  node [
    id 569
    label "zmobilizowanie"
  ]
  node [
    id 570
    label "wojo"
  ]
  node [
    id 571
    label "pospolite_ruszenie"
  ]
  node [
    id 572
    label "Eurokorpus"
  ]
  node [
    id 573
    label "mobilizowanie"
  ]
  node [
    id 574
    label "rejterowa&#263;"
  ]
  node [
    id 575
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 576
    label "mobilizowa&#263;"
  ]
  node [
    id 577
    label "Armia_Krajowa"
  ]
  node [
    id 578
    label "obrona"
  ]
  node [
    id 579
    label "dryl"
  ]
  node [
    id 580
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 581
    label "petarda"
  ]
  node [
    id 582
    label "pozycja"
  ]
  node [
    id 583
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 584
    label "grupa"
  ]
  node [
    id 585
    label "zaw&#243;d"
  ]
  node [
    id 586
    label "mechanika"
  ]
  node [
    id 587
    label "o&#347;"
  ]
  node [
    id 588
    label "usenet"
  ]
  node [
    id 589
    label "rozprz&#261;c"
  ]
  node [
    id 590
    label "cybernetyk"
  ]
  node [
    id 591
    label "podsystem"
  ]
  node [
    id 592
    label "system"
  ]
  node [
    id 593
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 594
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 595
    label "sk&#322;ad"
  ]
  node [
    id 596
    label "systemat"
  ]
  node [
    id 597
    label "konstrukcja"
  ]
  node [
    id 598
    label "konstelacja"
  ]
  node [
    id 599
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 600
    label "instytucja"
  ]
  node [
    id 601
    label "wys&#322;uga"
  ]
  node [
    id 602
    label "service"
  ]
  node [
    id 603
    label "czworak"
  ]
  node [
    id 604
    label "ZOMO"
  ]
  node [
    id 605
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 606
    label "ma&#322;pa_ogoniasta"
  ]
  node [
    id 607
    label "zdyscyplinowanie"
  ]
  node [
    id 608
    label "&#263;wiczenie"
  ]
  node [
    id 609
    label "metoda"
  ]
  node [
    id 610
    label "wszystko&#380;erca"
  ]
  node [
    id 611
    label "do&#347;wiadczenie"
  ]
  node [
    id 612
    label "teren_szko&#322;y"
  ]
  node [
    id 613
    label "Mickiewicz"
  ]
  node [
    id 614
    label "kwalifikacje"
  ]
  node [
    id 615
    label "podr&#281;cznik"
  ]
  node [
    id 616
    label "absolwent"
  ]
  node [
    id 617
    label "praktyka"
  ]
  node [
    id 618
    label "school"
  ]
  node [
    id 619
    label "zda&#263;"
  ]
  node [
    id 620
    label "gabinet"
  ]
  node [
    id 621
    label "urszulanki"
  ]
  node [
    id 622
    label "sztuba"
  ]
  node [
    id 623
    label "&#322;awa_szkolna"
  ]
  node [
    id 624
    label "nauka"
  ]
  node [
    id 625
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 626
    label "przepisa&#263;"
  ]
  node [
    id 627
    label "muzyka"
  ]
  node [
    id 628
    label "form"
  ]
  node [
    id 629
    label "klasa"
  ]
  node [
    id 630
    label "lekcja"
  ]
  node [
    id 631
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 632
    label "przepisanie"
  ]
  node [
    id 633
    label "czas"
  ]
  node [
    id 634
    label "skolaryzacja"
  ]
  node [
    id 635
    label "zdanie"
  ]
  node [
    id 636
    label "stopek"
  ]
  node [
    id 637
    label "sekretariat"
  ]
  node [
    id 638
    label "ideologia"
  ]
  node [
    id 639
    label "lesson"
  ]
  node [
    id 640
    label "niepokalanki"
  ]
  node [
    id 641
    label "szkolenie"
  ]
  node [
    id 642
    label "kara"
  ]
  node [
    id 643
    label "tablica"
  ]
  node [
    id 644
    label "zboczenie"
  ]
  node [
    id 645
    label "om&#243;wienie"
  ]
  node [
    id 646
    label "sponiewieranie"
  ]
  node [
    id 647
    label "discipline"
  ]
  node [
    id 648
    label "omawia&#263;"
  ]
  node [
    id 649
    label "kr&#261;&#380;enie"
  ]
  node [
    id 650
    label "tre&#347;&#263;"
  ]
  node [
    id 651
    label "robienie"
  ]
  node [
    id 652
    label "sponiewiera&#263;"
  ]
  node [
    id 653
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 654
    label "tematyka"
  ]
  node [
    id 655
    label "w&#261;tek"
  ]
  node [
    id 656
    label "zbaczanie"
  ]
  node [
    id 657
    label "program_nauczania"
  ]
  node [
    id 658
    label "om&#243;wi&#263;"
  ]
  node [
    id 659
    label "omawianie"
  ]
  node [
    id 660
    label "zbacza&#263;"
  ]
  node [
    id 661
    label "zboczy&#263;"
  ]
  node [
    id 662
    label "pachwina"
  ]
  node [
    id 663
    label "obudowa"
  ]
  node [
    id 664
    label "corpus"
  ]
  node [
    id 665
    label "brzuch"
  ]
  node [
    id 666
    label "budowla"
  ]
  node [
    id 667
    label "sto&#380;ek_wzrostu"
  ]
  node [
    id 668
    label "dywizja"
  ]
  node [
    id 669
    label "mi&#281;so"
  ]
  node [
    id 670
    label "dekolt"
  ]
  node [
    id 671
    label "zad"
  ]
  node [
    id 672
    label "documentation"
  ]
  node [
    id 673
    label "zasadzi&#263;"
  ]
  node [
    id 674
    label "zasadzenie"
  ]
  node [
    id 675
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 676
    label "struktura_anatomiczna"
  ]
  node [
    id 677
    label "bok"
  ]
  node [
    id 678
    label "podstawowy"
  ]
  node [
    id 679
    label "pupa"
  ]
  node [
    id 680
    label "krocze"
  ]
  node [
    id 681
    label "pier&#347;"
  ]
  node [
    id 682
    label "za&#322;o&#380;enie"
  ]
  node [
    id 683
    label "tuszka"
  ]
  node [
    id 684
    label "punkt_odniesienia"
  ]
  node [
    id 685
    label "konkordancja"
  ]
  node [
    id 686
    label "plecy"
  ]
  node [
    id 687
    label "klatka_piersiowa"
  ]
  node [
    id 688
    label "dr&#243;b"
  ]
  node [
    id 689
    label "biodro"
  ]
  node [
    id 690
    label "pacha"
  ]
  node [
    id 691
    label "despotyzm"
  ]
  node [
    id 692
    label "lias"
  ]
  node [
    id 693
    label "dzia&#322;"
  ]
  node [
    id 694
    label "pi&#281;tro"
  ]
  node [
    id 695
    label "jednostka_geologiczna"
  ]
  node [
    id 696
    label "filia"
  ]
  node [
    id 697
    label "malm"
  ]
  node [
    id 698
    label "dogger"
  ]
  node [
    id 699
    label "poziom"
  ]
  node [
    id 700
    label "promocja"
  ]
  node [
    id 701
    label "kurs"
  ]
  node [
    id 702
    label "bank"
  ]
  node [
    id 703
    label "formacja"
  ]
  node [
    id 704
    label "ajencja"
  ]
  node [
    id 705
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 706
    label "agencja"
  ]
  node [
    id 707
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 708
    label "szpital"
  ]
  node [
    id 709
    label "egzamin"
  ]
  node [
    id 710
    label "walka"
  ]
  node [
    id 711
    label "liga"
  ]
  node [
    id 712
    label "gracz"
  ]
  node [
    id 713
    label "protection"
  ]
  node [
    id 714
    label "poparcie"
  ]
  node [
    id 715
    label "mecz"
  ]
  node [
    id 716
    label "defense"
  ]
  node [
    id 717
    label "s&#261;d"
  ]
  node [
    id 718
    label "auspices"
  ]
  node [
    id 719
    label "ochrona"
  ]
  node [
    id 720
    label "sp&#243;r"
  ]
  node [
    id 721
    label "post&#281;powanie"
  ]
  node [
    id 722
    label "manewr"
  ]
  node [
    id 723
    label "defensive_structure"
  ]
  node [
    id 724
    label "guard_duty"
  ]
  node [
    id 725
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 726
    label "zas&#243;b"
  ]
  node [
    id 727
    label "nieufno&#347;&#263;"
  ]
  node [
    id 728
    label "zapasy"
  ]
  node [
    id 729
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 730
    label "resource"
  ]
  node [
    id 731
    label "potency"
  ]
  node [
    id 732
    label "tomizm"
  ]
  node [
    id 733
    label "wydolno&#347;&#263;"
  ]
  node [
    id 734
    label "arystotelizm"
  ]
  node [
    id 735
    label "gotowo&#347;&#263;"
  ]
  node [
    id 736
    label "element_anatomiczny"
  ]
  node [
    id 737
    label "poro&#380;e"
  ]
  node [
    id 738
    label "heraldyka"
  ]
  node [
    id 739
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 740
    label "bro&#324;"
  ]
  node [
    id 741
    label "call"
  ]
  node [
    id 742
    label "stawia&#263;_w_stan_pogotowia"
  ]
  node [
    id 743
    label "usposabia&#263;"
  ]
  node [
    id 744
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 745
    label "pobudza&#263;"
  ]
  node [
    id 746
    label "boost"
  ]
  node [
    id 747
    label "przygotowywa&#263;"
  ]
  node [
    id 748
    label "revocation"
  ]
  node [
    id 749
    label "cofni&#281;cie_si&#281;"
  ]
  node [
    id 750
    label "spowodowanie"
  ]
  node [
    id 751
    label "przemieszczenie"
  ]
  node [
    id 752
    label "coitus_interruptus"
  ]
  node [
    id 753
    label "retraction"
  ]
  node [
    id 754
    label "wycofywanie_si&#281;"
  ]
  node [
    id 755
    label "uciekanie"
  ]
  node [
    id 756
    label "rezygnowanie"
  ]
  node [
    id 757
    label "unikanie"
  ]
  node [
    id 758
    label "energia"
  ]
  node [
    id 759
    label "parametr"
  ]
  node [
    id 760
    label "rozwi&#261;zanie"
  ]
  node [
    id 761
    label "wuchta"
  ]
  node [
    id 762
    label "zaleta"
  ]
  node [
    id 763
    label "moment_si&#322;y"
  ]
  node [
    id 764
    label "mn&#243;stwo"
  ]
  node [
    id 765
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 766
    label "capacity"
  ]
  node [
    id 767
    label "magnitude"
  ]
  node [
    id 768
    label "przemoc"
  ]
  node [
    id 769
    label "nastawi&#263;"
  ]
  node [
    id 770
    label "wake_up"
  ]
  node [
    id 771
    label "powo&#322;a&#263;"
  ]
  node [
    id 772
    label "postawi&#263;_w_stan_pogotowia"
  ]
  node [
    id 773
    label "pool"
  ]
  node [
    id 774
    label "pobudzi&#263;"
  ]
  node [
    id 775
    label "przygotowa&#263;"
  ]
  node [
    id 776
    label "zrezygnowanie"
  ]
  node [
    id 777
    label "wycofanie_si&#281;"
  ]
  node [
    id 778
    label "przestraszenie_si&#281;"
  ]
  node [
    id 779
    label "uciekni&#281;cie"
  ]
  node [
    id 780
    label "powo&#322;anie"
  ]
  node [
    id 781
    label "pobudzenie"
  ]
  node [
    id 782
    label "postawienie_"
  ]
  node [
    id 783
    label "zebranie_si&#322;"
  ]
  node [
    id 784
    label "przygotowanie"
  ]
  node [
    id 785
    label "nastawienie"
  ]
  node [
    id 786
    label "vivification"
  ]
  node [
    id 787
    label "powo&#322;ywanie"
  ]
  node [
    id 788
    label "vitalization"
  ]
  node [
    id 789
    label "usposabianie"
  ]
  node [
    id 790
    label "stawianie_"
  ]
  node [
    id 791
    label "pobudzanie"
  ]
  node [
    id 792
    label "przygotowywanie"
  ]
  node [
    id 793
    label "uciec"
  ]
  node [
    id 794
    label "stch&#243;rzy&#263;"
  ]
  node [
    id 795
    label "wycofa&#263;_si&#281;"
  ]
  node [
    id 796
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 797
    label "retreat"
  ]
  node [
    id 798
    label "ucieka&#263;"
  ]
  node [
    id 799
    label "rezygnowa&#263;"
  ]
  node [
    id 800
    label "po&#322;o&#380;enie"
  ]
  node [
    id 801
    label "debit"
  ]
  node [
    id 802
    label "druk"
  ]
  node [
    id 803
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 804
    label "szata_graficzna"
  ]
  node [
    id 805
    label "wydawa&#263;"
  ]
  node [
    id 806
    label "szermierka"
  ]
  node [
    id 807
    label "spis"
  ]
  node [
    id 808
    label "wyda&#263;"
  ]
  node [
    id 809
    label "ustawienie"
  ]
  node [
    id 810
    label "publikacja"
  ]
  node [
    id 811
    label "status"
  ]
  node [
    id 812
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 813
    label "adres"
  ]
  node [
    id 814
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 815
    label "rozmieszczenie"
  ]
  node [
    id 816
    label "sytuacja"
  ]
  node [
    id 817
    label "rz&#261;d"
  ]
  node [
    id 818
    label "redaktor"
  ]
  node [
    id 819
    label "bearing"
  ]
  node [
    id 820
    label "awans"
  ]
  node [
    id 821
    label "poster"
  ]
  node [
    id 822
    label "le&#380;e&#263;"
  ]
  node [
    id 823
    label "Cygan"
  ]
  node [
    id 824
    label "dostawa"
  ]
  node [
    id 825
    label "transport"
  ]
  node [
    id 826
    label "pojazd"
  ]
  node [
    id 827
    label "ob&#243;z"
  ]
  node [
    id 828
    label "park"
  ]
  node [
    id 829
    label "odbywa&#263;"
  ]
  node [
    id 830
    label "robi&#263;"
  ]
  node [
    id 831
    label "serve"
  ]
  node [
    id 832
    label "pasemko"
  ]
  node [
    id 833
    label "znak_diakrytyczny"
  ]
  node [
    id 834
    label "zafalowanie"
  ]
  node [
    id 835
    label "kot"
  ]
  node [
    id 836
    label "strumie&#324;"
  ]
  node [
    id 837
    label "karb"
  ]
  node [
    id 838
    label "fit"
  ]
  node [
    id 839
    label "grzywa_fali"
  ]
  node [
    id 840
    label "woda"
  ]
  node [
    id 841
    label "efekt_Dopplera"
  ]
  node [
    id 842
    label "obcinka"
  ]
  node [
    id 843
    label "t&#322;um"
  ]
  node [
    id 844
    label "okres"
  ]
  node [
    id 845
    label "stream"
  ]
  node [
    id 846
    label "zafalowa&#263;"
  ]
  node [
    id 847
    label "rozbicie_si&#281;"
  ]
  node [
    id 848
    label "clutter"
  ]
  node [
    id 849
    label "rozbijanie_si&#281;"
  ]
  node [
    id 850
    label "czo&#322;o_fali"
  ]
  node [
    id 851
    label "pirotechnika"
  ]
  node [
    id 852
    label "cios"
  ]
  node [
    id 853
    label "sztuczne_ognie"
  ]
  node [
    id 854
    label "pocisk"
  ]
  node [
    id 855
    label "bomba"
  ]
  node [
    id 856
    label "cizia"
  ]
  node [
    id 857
    label "odjazd"
  ]
  node [
    id 858
    label "wyr&#243;b"
  ]
  node [
    id 859
    label "przest&#281;pca"
  ]
  node [
    id 860
    label "zdezerterowanie"
  ]
  node [
    id 861
    label "uciekinier"
  ]
  node [
    id 862
    label "kapitulant"
  ]
  node [
    id 863
    label "odreagowanie"
  ]
  node [
    id 864
    label "odreagowywanie"
  ]
  node [
    id 865
    label "odbywanie"
  ]
  node [
    id 866
    label "lekarz"
  ]
  node [
    id 867
    label "nienormalnie"
  ]
  node [
    id 868
    label "anormalnie"
  ]
  node [
    id 869
    label "schizol"
  ]
  node [
    id 870
    label "pochytany"
  ]
  node [
    id 871
    label "popaprany"
  ]
  node [
    id 872
    label "niestandardowy"
  ]
  node [
    id 873
    label "chory_psychicznie"
  ]
  node [
    id 874
    label "nieprawid&#322;owy"
  ]
  node [
    id 875
    label "dziwny"
  ]
  node [
    id 876
    label "psychol"
  ]
  node [
    id 877
    label "powalony"
  ]
  node [
    id 878
    label "stracenie_rozumu"
  ]
  node [
    id 879
    label "chory"
  ]
  node [
    id 880
    label "z&#322;y"
  ]
  node [
    id 881
    label "nieprzypadkowy"
  ]
  node [
    id 882
    label "intencjonalnie"
  ]
  node [
    id 883
    label "szczeg&#243;lnie"
  ]
  node [
    id 884
    label "wyj&#261;tkowy"
  ]
  node [
    id 885
    label "zaburzenie"
  ]
  node [
    id 886
    label "niedoskona&#322;o&#347;&#263;"
  ]
  node [
    id 887
    label "wada"
  ]
  node [
    id 888
    label "zacofanie"
  ]
  node [
    id 889
    label "g&#322;upek"
  ]
  node [
    id 890
    label "zesp&#243;&#322;_Downa"
  ]
  node [
    id 891
    label "idiotyzm"
  ]
  node [
    id 892
    label "zdarzony"
  ]
  node [
    id 893
    label "odpowiednio"
  ]
  node [
    id 894
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 895
    label "nale&#380;ny"
  ]
  node [
    id 896
    label "nale&#380;yty"
  ]
  node [
    id 897
    label "stosownie"
  ]
  node [
    id 898
    label "odpowiadanie"
  ]
  node [
    id 899
    label "umy&#347;lny"
  ]
  node [
    id 900
    label "niesprawno&#347;&#263;"
  ]
  node [
    id 901
    label "nieetatowo"
  ]
  node [
    id 902
    label "nieoficjalny"
  ]
  node [
    id 903
    label "inny"
  ]
  node [
    id 904
    label "zatrudniony"
  ]
  node [
    id 905
    label "zmienno&#347;&#263;"
  ]
  node [
    id 906
    label "play"
  ]
  node [
    id 907
    label "rozgrywka"
  ]
  node [
    id 908
    label "apparent_motion"
  ]
  node [
    id 909
    label "wydarzenie"
  ]
  node [
    id 910
    label "contest"
  ]
  node [
    id 911
    label "akcja"
  ]
  node [
    id 912
    label "komplet"
  ]
  node [
    id 913
    label "zabawa"
  ]
  node [
    id 914
    label "zasada"
  ]
  node [
    id 915
    label "rywalizacja"
  ]
  node [
    id 916
    label "zbijany"
  ]
  node [
    id 917
    label "game"
  ]
  node [
    id 918
    label "odg&#322;os"
  ]
  node [
    id 919
    label "Pok&#233;mon"
  ]
  node [
    id 920
    label "czynno&#347;&#263;"
  ]
  node [
    id 921
    label "synteza"
  ]
  node [
    id 922
    label "odtworzenie"
  ]
  node [
    id 923
    label "rekwizyt_do_gry"
  ]
  node [
    id 924
    label "resonance"
  ]
  node [
    id 925
    label "wydanie"
  ]
  node [
    id 926
    label "wpadni&#281;cie"
  ]
  node [
    id 927
    label "wpadanie"
  ]
  node [
    id 928
    label "sound"
  ]
  node [
    id 929
    label "brzmienie"
  ]
  node [
    id 930
    label "wpa&#347;&#263;"
  ]
  node [
    id 931
    label "note"
  ]
  node [
    id 932
    label "onomatopeja"
  ]
  node [
    id 933
    label "wpada&#263;"
  ]
  node [
    id 934
    label "kognicja"
  ]
  node [
    id 935
    label "campaign"
  ]
  node [
    id 936
    label "rozprawa"
  ]
  node [
    id 937
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 938
    label "fashion"
  ]
  node [
    id 939
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 940
    label "zmierzanie"
  ]
  node [
    id 941
    label "przes&#322;anka"
  ]
  node [
    id 942
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 943
    label "kazanie"
  ]
  node [
    id 944
    label "trafienie"
  ]
  node [
    id 945
    label "rewan&#380;owy"
  ]
  node [
    id 946
    label "zagrywka"
  ]
  node [
    id 947
    label "faza"
  ]
  node [
    id 948
    label "euroliga"
  ]
  node [
    id 949
    label "interliga"
  ]
  node [
    id 950
    label "runda"
  ]
  node [
    id 951
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 952
    label "rozrywka"
  ]
  node [
    id 953
    label "impreza"
  ]
  node [
    id 954
    label "igraszka"
  ]
  node [
    id 955
    label "taniec"
  ]
  node [
    id 956
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 957
    label "gambling"
  ]
  node [
    id 958
    label "chwyt"
  ]
  node [
    id 959
    label "igra"
  ]
  node [
    id 960
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 961
    label "nabawienie_si&#281;"
  ]
  node [
    id 962
    label "ubaw"
  ]
  node [
    id 963
    label "wodzirej"
  ]
  node [
    id 964
    label "activity"
  ]
  node [
    id 965
    label "bezproblemowy"
  ]
  node [
    id 966
    label "przebiec"
  ]
  node [
    id 967
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 968
    label "motyw"
  ]
  node [
    id 969
    label "przebiegni&#281;cie"
  ]
  node [
    id 970
    label "fabu&#322;a"
  ]
  node [
    id 971
    label "proces_technologiczny"
  ]
  node [
    id 972
    label "mieszanina"
  ]
  node [
    id 973
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 974
    label "fusion"
  ]
  node [
    id 975
    label "zestawienie"
  ]
  node [
    id 976
    label "uog&#243;lnienie"
  ]
  node [
    id 977
    label "puszczenie"
  ]
  node [
    id 978
    label "ustalenie"
  ]
  node [
    id 979
    label "wyst&#281;p"
  ]
  node [
    id 980
    label "reproduction"
  ]
  node [
    id 981
    label "przywr&#243;cenie"
  ]
  node [
    id 982
    label "w&#322;&#261;czenie"
  ]
  node [
    id 983
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 984
    label "restoration"
  ]
  node [
    id 985
    label "odbudowanie"
  ]
  node [
    id 986
    label "ensemble"
  ]
  node [
    id 987
    label "zestaw"
  ]
  node [
    id 988
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 989
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 990
    label "regu&#322;a_Allena"
  ]
  node [
    id 991
    label "base"
  ]
  node [
    id 992
    label "umowa"
  ]
  node [
    id 993
    label "obserwacja"
  ]
  node [
    id 994
    label "zasada_d'Alemberta"
  ]
  node [
    id 995
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 996
    label "normalizacja"
  ]
  node [
    id 997
    label "moralno&#347;&#263;"
  ]
  node [
    id 998
    label "criterion"
  ]
  node [
    id 999
    label "opis"
  ]
  node [
    id 1000
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1001
    label "prawo_Mendla"
  ]
  node [
    id 1002
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1003
    label "twierdzenie"
  ]
  node [
    id 1004
    label "prawo"
  ]
  node [
    id 1005
    label "standard"
  ]
  node [
    id 1006
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1007
    label "spos&#243;b"
  ]
  node [
    id 1008
    label "dominion"
  ]
  node [
    id 1009
    label "qualification"
  ]
  node [
    id 1010
    label "occupation"
  ]
  node [
    id 1011
    label "podstawa"
  ]
  node [
    id 1012
    label "substancja"
  ]
  node [
    id 1013
    label "prawid&#322;o"
  ]
  node [
    id 1014
    label "dywidenda"
  ]
  node [
    id 1015
    label "przebieg"
  ]
  node [
    id 1016
    label "operacja"
  ]
  node [
    id 1017
    label "udzia&#322;"
  ]
  node [
    id 1018
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1019
    label "commotion"
  ]
  node [
    id 1020
    label "jazda"
  ]
  node [
    id 1021
    label "stock"
  ]
  node [
    id 1022
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 1023
    label "w&#281;ze&#322;"
  ]
  node [
    id 1024
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1025
    label "pi&#322;ka"
  ]
  node [
    id 1026
    label "Anders"
  ]
  node [
    id 1027
    label "Ko&#347;ciuszko"
  ]
  node [
    id 1028
    label "Moczar"
  ]
  node [
    id 1029
    label "sejmik"
  ]
  node [
    id 1030
    label "starosta"
  ]
  node [
    id 1031
    label "Franco"
  ]
  node [
    id 1032
    label "jenera&#322;"
  ]
  node [
    id 1033
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 1034
    label "oficer"
  ]
  node [
    id 1035
    label "zakonnik"
  ]
  node [
    id 1036
    label "Maczek"
  ]
  node [
    id 1037
    label "organ"
  ]
  node [
    id 1038
    label "zgromadzenie"
  ]
  node [
    id 1039
    label "podchor&#261;&#380;y"
  ]
  node [
    id 1040
    label "podoficer"
  ]
  node [
    id 1041
    label "urz&#281;dnik"
  ]
  node [
    id 1042
    label "podstaro&#347;ci"
  ]
  node [
    id 1043
    label "burgrabia"
  ]
  node [
    id 1044
    label "w&#322;odarz"
  ]
  node [
    id 1045
    label "samorz&#261;dowiec"
  ]
  node [
    id 1046
    label "przedstawiciel"
  ]
  node [
    id 1047
    label "br"
  ]
  node [
    id 1048
    label "mnich"
  ]
  node [
    id 1049
    label "zakon"
  ]
  node [
    id 1050
    label "wyznawca"
  ]
  node [
    id 1051
    label "&#347;w"
  ]
  node [
    id 1052
    label "powstanie"
  ]
  node [
    id 1053
    label "taniec_ludowy"
  ]
  node [
    id 1054
    label "melodia"
  ]
  node [
    id 1055
    label "karnet"
  ]
  node [
    id 1056
    label "posuwisto&#347;&#263;"
  ]
  node [
    id 1057
    label "utw&#243;r"
  ]
  node [
    id 1058
    label "parkiet"
  ]
  node [
    id 1059
    label "choreologia"
  ]
  node [
    id 1060
    label "krok_taneczny"
  ]
  node [
    id 1061
    label "zanucenie"
  ]
  node [
    id 1062
    label "nuta"
  ]
  node [
    id 1063
    label "zakosztowa&#263;"
  ]
  node [
    id 1064
    label "zajawka"
  ]
  node [
    id 1065
    label "zanuci&#263;"
  ]
  node [
    id 1066
    label "emocja"
  ]
  node [
    id 1067
    label "oskoma"
  ]
  node [
    id 1068
    label "melika"
  ]
  node [
    id 1069
    label "nucenie"
  ]
  node [
    id 1070
    label "nuci&#263;"
  ]
  node [
    id 1071
    label "taste"
  ]
  node [
    id 1072
    label "inclination"
  ]
  node [
    id 1073
    label "discover"
  ]
  node [
    id 1074
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 1075
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 1076
    label "wydoby&#263;"
  ]
  node [
    id 1077
    label "poda&#263;"
  ]
  node [
    id 1078
    label "okre&#347;li&#263;"
  ]
  node [
    id 1079
    label "express"
  ]
  node [
    id 1080
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1081
    label "wyrazi&#263;"
  ]
  node [
    id 1082
    label "rzekn&#261;&#263;"
  ]
  node [
    id 1083
    label "unwrap"
  ]
  node [
    id 1084
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1085
    label "convey"
  ]
  node [
    id 1086
    label "tenis"
  ]
  node [
    id 1087
    label "supply"
  ]
  node [
    id 1088
    label "da&#263;"
  ]
  node [
    id 1089
    label "ustawi&#263;"
  ]
  node [
    id 1090
    label "siatk&#243;wka"
  ]
  node [
    id 1091
    label "zagra&#263;"
  ]
  node [
    id 1092
    label "jedzenie"
  ]
  node [
    id 1093
    label "poinformowa&#263;"
  ]
  node [
    id 1094
    label "introduce"
  ]
  node [
    id 1095
    label "nafaszerowa&#263;"
  ]
  node [
    id 1096
    label "zaserwowa&#263;"
  ]
  node [
    id 1097
    label "draw"
  ]
  node [
    id 1098
    label "doby&#263;"
  ]
  node [
    id 1099
    label "g&#243;rnictwo"
  ]
  node [
    id 1100
    label "wyeksploatowa&#263;"
  ]
  node [
    id 1101
    label "extract"
  ]
  node [
    id 1102
    label "obtain"
  ]
  node [
    id 1103
    label "wyj&#261;&#263;"
  ]
  node [
    id 1104
    label "ocali&#263;"
  ]
  node [
    id 1105
    label "uzyska&#263;"
  ]
  node [
    id 1106
    label "wydosta&#263;"
  ]
  node [
    id 1107
    label "uwydatni&#263;"
  ]
  node [
    id 1108
    label "distill"
  ]
  node [
    id 1109
    label "raise"
  ]
  node [
    id 1110
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1111
    label "testify"
  ]
  node [
    id 1112
    label "zakomunikowa&#263;"
  ]
  node [
    id 1113
    label "oznaczy&#263;"
  ]
  node [
    id 1114
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 1115
    label "vent"
  ]
  node [
    id 1116
    label "zdecydowa&#263;"
  ]
  node [
    id 1117
    label "zrobi&#263;"
  ]
  node [
    id 1118
    label "spowodowa&#263;"
  ]
  node [
    id 1119
    label "situate"
  ]
  node [
    id 1120
    label "nominate"
  ]
  node [
    id 1121
    label "uznawa&#263;"
  ]
  node [
    id 1122
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1123
    label "consent"
  ]
  node [
    id 1124
    label "authorize"
  ]
  node [
    id 1125
    label "os&#261;dza&#263;"
  ]
  node [
    id 1126
    label "consider"
  ]
  node [
    id 1127
    label "notice"
  ]
  node [
    id 1128
    label "stwierdza&#263;"
  ]
  node [
    id 1129
    label "przyznawa&#263;"
  ]
  node [
    id 1130
    label "aplikowa&#263;"
  ]
  node [
    id 1131
    label "intrude"
  ]
  node [
    id 1132
    label "trespass"
  ]
  node [
    id 1133
    label "zmusza&#263;"
  ]
  node [
    id 1134
    label "umieszcza&#263;"
  ]
  node [
    id 1135
    label "force"
  ]
  node [
    id 1136
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 1137
    label "sandbag"
  ]
  node [
    id 1138
    label "plasowa&#263;"
  ]
  node [
    id 1139
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1140
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 1141
    label "pomieszcza&#263;"
  ]
  node [
    id 1142
    label "accommodate"
  ]
  node [
    id 1143
    label "venture"
  ]
  node [
    id 1144
    label "wpiernicza&#263;"
  ]
  node [
    id 1145
    label "okre&#347;la&#263;"
  ]
  node [
    id 1146
    label "naszywa&#263;"
  ]
  node [
    id 1147
    label "praktykowa&#263;"
  ]
  node [
    id 1148
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1149
    label "applique"
  ]
  node [
    id 1150
    label "zaleca&#263;"
  ]
  node [
    id 1151
    label "use"
  ]
  node [
    id 1152
    label "save"
  ]
  node [
    id 1153
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 1154
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 1155
    label "kacapski"
  ]
  node [
    id 1156
    label "po_rosyjsku"
  ]
  node [
    id 1157
    label "wielkoruski"
  ]
  node [
    id 1158
    label "Russian"
  ]
  node [
    id 1159
    label "j&#281;zyk"
  ]
  node [
    id 1160
    label "rusek"
  ]
  node [
    id 1161
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1162
    label "artykulator"
  ]
  node [
    id 1163
    label "kod"
  ]
  node [
    id 1164
    label "kawa&#322;ek"
  ]
  node [
    id 1165
    label "gramatyka"
  ]
  node [
    id 1166
    label "stylik"
  ]
  node [
    id 1167
    label "przet&#322;umaczenie"
  ]
  node [
    id 1168
    label "formalizowanie"
  ]
  node [
    id 1169
    label "ssanie"
  ]
  node [
    id 1170
    label "ssa&#263;"
  ]
  node [
    id 1171
    label "language"
  ]
  node [
    id 1172
    label "liza&#263;"
  ]
  node [
    id 1173
    label "napisa&#263;"
  ]
  node [
    id 1174
    label "konsonantyzm"
  ]
  node [
    id 1175
    label "wokalizm"
  ]
  node [
    id 1176
    label "pisa&#263;"
  ]
  node [
    id 1177
    label "fonetyka"
  ]
  node [
    id 1178
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1179
    label "jeniec"
  ]
  node [
    id 1180
    label "but"
  ]
  node [
    id 1181
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1182
    label "po_koroniarsku"
  ]
  node [
    id 1183
    label "kultura_duchowa"
  ]
  node [
    id 1184
    label "t&#322;umaczenie"
  ]
  node [
    id 1185
    label "m&#243;wienie"
  ]
  node [
    id 1186
    label "pype&#263;"
  ]
  node [
    id 1187
    label "lizanie"
  ]
  node [
    id 1188
    label "pismo"
  ]
  node [
    id 1189
    label "formalizowa&#263;"
  ]
  node [
    id 1190
    label "rozumie&#263;"
  ]
  node [
    id 1191
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1192
    label "rozumienie"
  ]
  node [
    id 1193
    label "makroglosja"
  ]
  node [
    id 1194
    label "m&#243;wi&#263;"
  ]
  node [
    id 1195
    label "jama_ustna"
  ]
  node [
    id 1196
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1197
    label "formacja_geologiczna"
  ]
  node [
    id 1198
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1199
    label "natural_language"
  ]
  node [
    id 1200
    label "s&#322;ownictwo"
  ]
  node [
    id 1201
    label "wschodnioeuropejski"
  ]
  node [
    id 1202
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 1203
    label "poga&#324;ski"
  ]
  node [
    id 1204
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 1205
    label "topielec"
  ]
  node [
    id 1206
    label "europejski"
  ]
  node [
    id 1207
    label "j&#281;zyk_rosyjski"
  ]
  node [
    id 1208
    label "po_kacapsku"
  ]
  node [
    id 1209
    label "ruski"
  ]
  node [
    id 1210
    label "imperialny"
  ]
  node [
    id 1211
    label "po_wielkorusku"
  ]
  node [
    id 1212
    label "narrative"
  ]
  node [
    id 1213
    label "wypowied&#378;"
  ]
  node [
    id 1214
    label "retardacja"
  ]
  node [
    id 1215
    label "plot"
  ]
  node [
    id 1216
    label "mowa_zale&#380;na"
  ]
  node [
    id 1217
    label "mowa_pozornie_zale&#380;na"
  ]
  node [
    id 1218
    label "mowa_niezale&#380;na"
  ]
  node [
    id 1219
    label "wielog&#322;os"
  ]
  node [
    id 1220
    label "prolog"
  ]
  node [
    id 1221
    label "sparafrazowanie"
  ]
  node [
    id 1222
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1223
    label "strawestowa&#263;"
  ]
  node [
    id 1224
    label "sparafrazowa&#263;"
  ]
  node [
    id 1225
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1226
    label "trawestowa&#263;"
  ]
  node [
    id 1227
    label "sformu&#322;owanie"
  ]
  node [
    id 1228
    label "parafrazowanie"
  ]
  node [
    id 1229
    label "ozdobnik"
  ]
  node [
    id 1230
    label "delimitacja"
  ]
  node [
    id 1231
    label "parafrazowa&#263;"
  ]
  node [
    id 1232
    label "stylizacja"
  ]
  node [
    id 1233
    label "komunikat"
  ]
  node [
    id 1234
    label "trawestowanie"
  ]
  node [
    id 1235
    label "strawestowanie"
  ]
  node [
    id 1236
    label "kompozycja"
  ]
  node [
    id 1237
    label "faktura"
  ]
  node [
    id 1238
    label "chwyt_kompozycyjny"
  ]
  node [
    id 1239
    label "op&#243;&#378;nienie"
  ]
  node [
    id 1240
    label "wst&#281;p"
  ]
  node [
    id 1241
    label "wy&#347;cig"
  ]
  node [
    id 1242
    label "mie&#263;_miejsce"
  ]
  node [
    id 1243
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1244
    label "explain"
  ]
  node [
    id 1245
    label "determine"
  ]
  node [
    id 1246
    label "work"
  ]
  node [
    id 1247
    label "wierzchnica"
  ]
  node [
    id 1248
    label "kartka"
  ]
  node [
    id 1249
    label "logowanie"
  ]
  node [
    id 1250
    label "plik"
  ]
  node [
    id 1251
    label "adres_internetowy"
  ]
  node [
    id 1252
    label "linia"
  ]
  node [
    id 1253
    label "serwis_internetowy"
  ]
  node [
    id 1254
    label "skr&#281;canie"
  ]
  node [
    id 1255
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1256
    label "orientowanie"
  ]
  node [
    id 1257
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1258
    label "uj&#281;cie"
  ]
  node [
    id 1259
    label "ty&#322;"
  ]
  node [
    id 1260
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1261
    label "fragment"
  ]
  node [
    id 1262
    label "layout"
  ]
  node [
    id 1263
    label "zorientowa&#263;"
  ]
  node [
    id 1264
    label "pagina"
  ]
  node [
    id 1265
    label "podmiot"
  ]
  node [
    id 1266
    label "orientowa&#263;"
  ]
  node [
    id 1267
    label "voice"
  ]
  node [
    id 1268
    label "prz&#243;d"
  ]
  node [
    id 1269
    label "internet"
  ]
  node [
    id 1270
    label "powierzchnia"
  ]
  node [
    id 1271
    label "skr&#281;cenie"
  ]
  node [
    id 1272
    label "Wielka_Racza"
  ]
  node [
    id 1273
    label "&#346;winica"
  ]
  node [
    id 1274
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 1275
    label "Che&#322;miec"
  ]
  node [
    id 1276
    label "wierzcho&#322;"
  ]
  node [
    id 1277
    label "wierzcho&#322;ek"
  ]
  node [
    id 1278
    label "Radunia"
  ]
  node [
    id 1279
    label "warstwa"
  ]
  node [
    id 1280
    label "Barania_G&#243;ra"
  ]
  node [
    id 1281
    label "Groniczki"
  ]
  node [
    id 1282
    label "wierch"
  ]
  node [
    id 1283
    label "Czupel"
  ]
  node [
    id 1284
    label "Jaworz"
  ]
  node [
    id 1285
    label "Okr&#261;glica"
  ]
  node [
    id 1286
    label "Walig&#243;ra"
  ]
  node [
    id 1287
    label "l&#261;d"
  ]
  node [
    id 1288
    label "Wielka_Sowa"
  ]
  node [
    id 1289
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 1290
    label "&#321;omnica"
  ]
  node [
    id 1291
    label "wzniesienie"
  ]
  node [
    id 1292
    label "Beskid"
  ]
  node [
    id 1293
    label "Wo&#322;ek"
  ]
  node [
    id 1294
    label "Rysianka"
  ]
  node [
    id 1295
    label "Mody&#324;"
  ]
  node [
    id 1296
    label "Jura"
  ]
  node [
    id 1297
    label "Obidowa"
  ]
  node [
    id 1298
    label "Jaworzyna"
  ]
  node [
    id 1299
    label "Turbacz"
  ]
  node [
    id 1300
    label "Czarna_G&#243;ra"
  ]
  node [
    id 1301
    label "Rudawiec"
  ]
  node [
    id 1302
    label "torfowisko"
  ]
  node [
    id 1303
    label "Ja&#322;owiec"
  ]
  node [
    id 1304
    label "Wielki_Chocz"
  ]
  node [
    id 1305
    label "Orlica"
  ]
  node [
    id 1306
    label "obszar"
  ]
  node [
    id 1307
    label "Szrenica"
  ]
  node [
    id 1308
    label "&#346;nie&#380;nik"
  ]
  node [
    id 1309
    label "Cubryna"
  ]
  node [
    id 1310
    label "Wielki_Bukowiec"
  ]
  node [
    id 1311
    label "Magura"
  ]
  node [
    id 1312
    label "korona"
  ]
  node [
    id 1313
    label "wieczko"
  ]
  node [
    id 1314
    label "Lubogoszcz"
  ]
  node [
    id 1315
    label "Dekan"
  ]
  node [
    id 1316
    label "opuszcza&#263;"
  ]
  node [
    id 1317
    label "uzyskiwa&#263;"
  ]
  node [
    id 1318
    label "appear"
  ]
  node [
    id 1319
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1320
    label "impart"
  ]
  node [
    id 1321
    label "proceed"
  ]
  node [
    id 1322
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 1323
    label "publish"
  ]
  node [
    id 1324
    label "za&#322;atwi&#263;"
  ]
  node [
    id 1325
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 1326
    label "blend"
  ]
  node [
    id 1327
    label "pochodzi&#263;"
  ]
  node [
    id 1328
    label "wygl&#261;da&#263;"
  ]
  node [
    id 1329
    label "wyrusza&#263;"
  ]
  node [
    id 1330
    label "seclude"
  ]
  node [
    id 1331
    label "heighten"
  ]
  node [
    id 1332
    label "strona_&#347;wiata"
  ]
  node [
    id 1333
    label "ukazywa&#263;_si&#281;_drukiem"
  ]
  node [
    id 1334
    label "wystarcza&#263;"
  ]
  node [
    id 1335
    label "schodzi&#263;"
  ]
  node [
    id 1336
    label "gra&#263;"
  ]
  node [
    id 1337
    label "perform"
  ]
  node [
    id 1338
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1339
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 1340
    label "ko&#324;czy&#263;"
  ]
  node [
    id 1341
    label "przedk&#322;ada&#263;"
  ]
  node [
    id 1342
    label "wypada&#263;"
  ]
  node [
    id 1343
    label "przedstawia&#263;"
  ]
  node [
    id 1344
    label "rusza&#263;"
  ]
  node [
    id 1345
    label "&#347;wieci&#263;"
  ]
  node [
    id 1346
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 1347
    label "muzykowa&#263;"
  ]
  node [
    id 1348
    label "majaczy&#263;"
  ]
  node [
    id 1349
    label "szczeka&#263;"
  ]
  node [
    id 1350
    label "wykonywa&#263;"
  ]
  node [
    id 1351
    label "napierdziela&#263;"
  ]
  node [
    id 1352
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1353
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 1354
    label "instrument_muzyczny"
  ]
  node [
    id 1355
    label "pasowa&#263;"
  ]
  node [
    id 1356
    label "dally"
  ]
  node [
    id 1357
    label "i&#347;&#263;"
  ]
  node [
    id 1358
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1359
    label "tokowa&#263;"
  ]
  node [
    id 1360
    label "wida&#263;"
  ]
  node [
    id 1361
    label "prezentowa&#263;"
  ]
  node [
    id 1362
    label "rozgrywa&#263;"
  ]
  node [
    id 1363
    label "do"
  ]
  node [
    id 1364
    label "brzmie&#263;"
  ]
  node [
    id 1365
    label "wykorzystywa&#263;"
  ]
  node [
    id 1366
    label "cope"
  ]
  node [
    id 1367
    label "otwarcie"
  ]
  node [
    id 1368
    label "typify"
  ]
  node [
    id 1369
    label "rola"
  ]
  node [
    id 1370
    label "satisfy"
  ]
  node [
    id 1371
    label "close"
  ]
  node [
    id 1372
    label "przestawa&#263;"
  ]
  node [
    id 1373
    label "zako&#324;cza&#263;"
  ]
  node [
    id 1374
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 1375
    label "stanowi&#263;"
  ]
  node [
    id 1376
    label "doprowadzi&#263;"
  ]
  node [
    id 1377
    label "wystarczy&#263;"
  ]
  node [
    id 1378
    label "pozyska&#263;"
  ]
  node [
    id 1379
    label "plan"
  ]
  node [
    id 1380
    label "stage"
  ]
  node [
    id 1381
    label "zabi&#263;"
  ]
  node [
    id 1382
    label "pozostawia&#263;"
  ]
  node [
    id 1383
    label "traci&#263;"
  ]
  node [
    id 1384
    label "obni&#380;a&#263;"
  ]
  node [
    id 1385
    label "abort"
  ]
  node [
    id 1386
    label "omija&#263;"
  ]
  node [
    id 1387
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 1388
    label "forowa&#263;_si&#281;"
  ]
  node [
    id 1389
    label "potania&#263;"
  ]
  node [
    id 1390
    label "teatr"
  ]
  node [
    id 1391
    label "exhibit"
  ]
  node [
    id 1392
    label "podawa&#263;"
  ]
  node [
    id 1393
    label "display"
  ]
  node [
    id 1394
    label "pokazywa&#263;"
  ]
  node [
    id 1395
    label "demonstrowa&#263;"
  ]
  node [
    id 1396
    label "zapoznawa&#263;"
  ]
  node [
    id 1397
    label "opisywa&#263;"
  ]
  node [
    id 1398
    label "ukazywa&#263;"
  ]
  node [
    id 1399
    label "represent"
  ]
  node [
    id 1400
    label "zg&#322;asza&#263;"
  ]
  node [
    id 1401
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1402
    label "attest"
  ]
  node [
    id 1403
    label "favor"
  ]
  node [
    id 1404
    label "przek&#322;ada&#263;"
  ]
  node [
    id 1405
    label "wytwarza&#263;"
  ]
  node [
    id 1406
    label "take"
  ]
  node [
    id 1407
    label "get"
  ]
  node [
    id 1408
    label "mark"
  ]
  node [
    id 1409
    label "lookout"
  ]
  node [
    id 1410
    label "peep"
  ]
  node [
    id 1411
    label "patrze&#263;"
  ]
  node [
    id 1412
    label "by&#263;"
  ]
  node [
    id 1413
    label "wyziera&#263;"
  ]
  node [
    id 1414
    label "look"
  ]
  node [
    id 1415
    label "czeka&#263;"
  ]
  node [
    id 1416
    label "lecie&#263;"
  ]
  node [
    id 1417
    label "wynika&#263;"
  ]
  node [
    id 1418
    label "necessity"
  ]
  node [
    id 1419
    label "fall"
  ]
  node [
    id 1420
    label "fall_out"
  ]
  node [
    id 1421
    label "trza"
  ]
  node [
    id 1422
    label "temat"
  ]
  node [
    id 1423
    label "digress"
  ]
  node [
    id 1424
    label "przenosi&#263;_si&#281;"
  ]
  node [
    id 1425
    label "wschodzi&#263;"
  ]
  node [
    id 1426
    label "ubywa&#263;"
  ]
  node [
    id 1427
    label "mija&#263;"
  ]
  node [
    id 1428
    label "odpada&#263;"
  ]
  node [
    id 1429
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1430
    label "przej&#347;&#263;"
  ]
  node [
    id 1431
    label "podrze&#263;"
  ]
  node [
    id 1432
    label "umiera&#263;"
  ]
  node [
    id 1433
    label "wprowadza&#263;"
  ]
  node [
    id 1434
    label "&#347;piewa&#263;"
  ]
  node [
    id 1435
    label "obni&#380;a&#263;_si&#281;"
  ]
  node [
    id 1436
    label "str&#243;j"
  ]
  node [
    id 1437
    label "gin&#261;&#263;"
  ]
  node [
    id 1438
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 1439
    label "set"
  ]
  node [
    id 1440
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1441
    label "odpuszcza&#263;"
  ]
  node [
    id 1442
    label "zu&#380;y&#263;"
  ]
  node [
    id 1443
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1444
    label "refuse"
  ]
  node [
    id 1445
    label "zaspokaja&#263;"
  ]
  node [
    id 1446
    label "suffice"
  ]
  node [
    id 1447
    label "dostawa&#263;"
  ]
  node [
    id 1448
    label "stawa&#263;"
  ]
  node [
    id 1449
    label "date"
  ]
  node [
    id 1450
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 1451
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1452
    label "poby&#263;"
  ]
  node [
    id 1453
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1454
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1455
    label "bolt"
  ]
  node [
    id 1456
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 1457
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1458
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 1459
    label "dociera&#263;"
  ]
  node [
    id 1460
    label "g&#322;upstwo"
  ]
  node [
    id 1461
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 1462
    label "prevention"
  ]
  node [
    id 1463
    label "pomiarkowanie"
  ]
  node [
    id 1464
    label "przeszkoda"
  ]
  node [
    id 1465
    label "intelekt"
  ]
  node [
    id 1466
    label "zmniejszenie"
  ]
  node [
    id 1467
    label "reservation"
  ]
  node [
    id 1468
    label "przekroczenie"
  ]
  node [
    id 1469
    label "finlandyzacja"
  ]
  node [
    id 1470
    label "otoczenie"
  ]
  node [
    id 1471
    label "osielstwo"
  ]
  node [
    id 1472
    label "zdyskryminowanie"
  ]
  node [
    id 1473
    label "warunek"
  ]
  node [
    id 1474
    label "limitation"
  ]
  node [
    id 1475
    label "przekroczy&#263;"
  ]
  node [
    id 1476
    label "przekraczanie"
  ]
  node [
    id 1477
    label "przekracza&#263;"
  ]
  node [
    id 1478
    label "barrier"
  ]
  node [
    id 1479
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 1480
    label "ogrom"
  ]
  node [
    id 1481
    label "iskrzy&#263;"
  ]
  node [
    id 1482
    label "d&#322;awi&#263;"
  ]
  node [
    id 1483
    label "ostygn&#261;&#263;"
  ]
  node [
    id 1484
    label "stygn&#261;&#263;"
  ]
  node [
    id 1485
    label "stan"
  ]
  node [
    id 1486
    label "temperatura"
  ]
  node [
    id 1487
    label "afekt"
  ]
  node [
    id 1488
    label "Roman"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 755
  ]
  edge [
    source 4
    target 756
  ]
  edge [
    source 4
    target 757
  ]
  edge [
    source 4
    target 758
  ]
  edge [
    source 4
    target 759
  ]
  edge [
    source 4
    target 760
  ]
  edge [
    source 4
    target 761
  ]
  edge [
    source 4
    target 762
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 763
  ]
  edge [
    source 4
    target 764
  ]
  edge [
    source 4
    target 765
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 766
  ]
  edge [
    source 4
    target 767
  ]
  edge [
    source 4
    target 768
  ]
  edge [
    source 4
    target 769
  ]
  edge [
    source 4
    target 770
  ]
  edge [
    source 4
    target 771
  ]
  edge [
    source 4
    target 772
  ]
  edge [
    source 4
    target 773
  ]
  edge [
    source 4
    target 774
  ]
  edge [
    source 4
    target 775
  ]
  edge [
    source 4
    target 776
  ]
  edge [
    source 4
    target 777
  ]
  edge [
    source 4
    target 778
  ]
  edge [
    source 4
    target 779
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 780
  ]
  edge [
    source 4
    target 781
  ]
  edge [
    source 4
    target 782
  ]
  edge [
    source 4
    target 783
  ]
  edge [
    source 4
    target 784
  ]
  edge [
    source 4
    target 785
  ]
  edge [
    source 4
    target 786
  ]
  edge [
    source 4
    target 787
  ]
  edge [
    source 4
    target 788
  ]
  edge [
    source 4
    target 789
  ]
  edge [
    source 4
    target 790
  ]
  edge [
    source 4
    target 791
  ]
  edge [
    source 4
    target 792
  ]
  edge [
    source 4
    target 793
  ]
  edge [
    source 4
    target 794
  ]
  edge [
    source 4
    target 795
  ]
  edge [
    source 4
    target 796
  ]
  edge [
    source 4
    target 797
  ]
  edge [
    source 4
    target 798
  ]
  edge [
    source 4
    target 799
  ]
  edge [
    source 4
    target 800
  ]
  edge [
    source 4
    target 801
  ]
  edge [
    source 4
    target 802
  ]
  edge [
    source 4
    target 803
  ]
  edge [
    source 4
    target 804
  ]
  edge [
    source 4
    target 805
  ]
  edge [
    source 4
    target 806
  ]
  edge [
    source 4
    target 807
  ]
  edge [
    source 4
    target 808
  ]
  edge [
    source 4
    target 809
  ]
  edge [
    source 4
    target 810
  ]
  edge [
    source 4
    target 811
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 812
  ]
  edge [
    source 4
    target 813
  ]
  edge [
    source 4
    target 814
  ]
  edge [
    source 4
    target 815
  ]
  edge [
    source 4
    target 816
  ]
  edge [
    source 4
    target 817
  ]
  edge [
    source 4
    target 818
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 819
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 820
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 821
  ]
  edge [
    source 4
    target 822
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 823
  ]
  edge [
    source 4
    target 824
  ]
  edge [
    source 4
    target 825
  ]
  edge [
    source 4
    target 826
  ]
  edge [
    source 4
    target 827
  ]
  edge [
    source 4
    target 828
  ]
  edge [
    source 4
    target 829
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 830
  ]
  edge [
    source 4
    target 831
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 832
  ]
  edge [
    source 4
    target 833
  ]
  edge [
    source 4
    target 834
  ]
  edge [
    source 4
    target 835
  ]
  edge [
    source 4
    target 836
  ]
  edge [
    source 4
    target 837
  ]
  edge [
    source 4
    target 838
  ]
  edge [
    source 4
    target 839
  ]
  edge [
    source 4
    target 840
  ]
  edge [
    source 4
    target 841
  ]
  edge [
    source 4
    target 842
  ]
  edge [
    source 4
    target 843
  ]
  edge [
    source 4
    target 844
  ]
  edge [
    source 4
    target 845
  ]
  edge [
    source 4
    target 846
  ]
  edge [
    source 4
    target 847
  ]
  edge [
    source 4
    target 848
  ]
  edge [
    source 4
    target 849
  ]
  edge [
    source 4
    target 850
  ]
  edge [
    source 4
    target 851
  ]
  edge [
    source 4
    target 852
  ]
  edge [
    source 4
    target 853
  ]
  edge [
    source 4
    target 854
  ]
  edge [
    source 4
    target 855
  ]
  edge [
    source 4
    target 856
  ]
  edge [
    source 4
    target 857
  ]
  edge [
    source 4
    target 858
  ]
  edge [
    source 4
    target 859
  ]
  edge [
    source 4
    target 860
  ]
  edge [
    source 4
    target 861
  ]
  edge [
    source 4
    target 862
  ]
  edge [
    source 4
    target 863
  ]
  edge [
    source 4
    target 864
  ]
  edge [
    source 4
    target 865
  ]
  edge [
    source 4
    target 866
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 867
  ]
  edge [
    source 5
    target 868
  ]
  edge [
    source 5
    target 869
  ]
  edge [
    source 5
    target 870
  ]
  edge [
    source 5
    target 871
  ]
  edge [
    source 5
    target 872
  ]
  edge [
    source 5
    target 873
  ]
  edge [
    source 5
    target 874
  ]
  edge [
    source 5
    target 875
  ]
  edge [
    source 5
    target 876
  ]
  edge [
    source 5
    target 877
  ]
  edge [
    source 5
    target 878
  ]
  edge [
    source 5
    target 879
  ]
  edge [
    source 5
    target 880
  ]
  edge [
    source 5
    target 881
  ]
  edge [
    source 5
    target 882
  ]
  edge [
    source 5
    target 883
  ]
  edge [
    source 5
    target 884
  ]
  edge [
    source 5
    target 885
  ]
  edge [
    source 5
    target 886
  ]
  edge [
    source 5
    target 887
  ]
  edge [
    source 5
    target 888
  ]
  edge [
    source 5
    target 889
  ]
  edge [
    source 5
    target 890
  ]
  edge [
    source 5
    target 891
  ]
  edge [
    source 5
    target 892
  ]
  edge [
    source 5
    target 893
  ]
  edge [
    source 5
    target 894
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 895
  ]
  edge [
    source 5
    target 896
  ]
  edge [
    source 5
    target 897
  ]
  edge [
    source 5
    target 898
  ]
  edge [
    source 5
    target 899
  ]
  edge [
    source 5
    target 900
  ]
  edge [
    source 5
    target 901
  ]
  edge [
    source 5
    target 902
  ]
  edge [
    source 5
    target 903
  ]
  edge [
    source 5
    target 904
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 918
  ]
  edge [
    source 6
    target 919
  ]
  edge [
    source 6
    target 920
  ]
  edge [
    source 6
    target 921
  ]
  edge [
    source 6
    target 922
  ]
  edge [
    source 6
    target 923
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 925
  ]
  edge [
    source 6
    target 926
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 927
  ]
  edge [
    source 6
    target 805
  ]
  edge [
    source 6
    target 928
  ]
  edge [
    source 6
    target 929
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 808
  ]
  edge [
    source 6
    target 930
  ]
  edge [
    source 6
    target 931
  ]
  edge [
    source 6
    target 932
  ]
  edge [
    source 6
    target 933
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 934
  ]
  edge [
    source 6
    target 935
  ]
  edge [
    source 6
    target 936
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 937
  ]
  edge [
    source 6
    target 938
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 939
  ]
  edge [
    source 6
    target 940
  ]
  edge [
    source 6
    target 941
  ]
  edge [
    source 6
    target 942
  ]
  edge [
    source 6
    target 943
  ]
  edge [
    source 6
    target 944
  ]
  edge [
    source 6
    target 945
  ]
  edge [
    source 6
    target 946
  ]
  edge [
    source 6
    target 947
  ]
  edge [
    source 6
    target 948
  ]
  edge [
    source 6
    target 949
  ]
  edge [
    source 6
    target 950
  ]
  edge [
    source 6
    target 951
  ]
  edge [
    source 6
    target 952
  ]
  edge [
    source 6
    target 953
  ]
  edge [
    source 6
    target 954
  ]
  edge [
    source 6
    target 955
  ]
  edge [
    source 6
    target 956
  ]
  edge [
    source 6
    target 957
  ]
  edge [
    source 6
    target 958
  ]
  edge [
    source 6
    target 959
  ]
  edge [
    source 6
    target 960
  ]
  edge [
    source 6
    target 961
  ]
  edge [
    source 6
    target 962
  ]
  edge [
    source 6
    target 963
  ]
  edge [
    source 6
    target 964
  ]
  edge [
    source 6
    target 965
  ]
  edge [
    source 6
    target 966
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 967
  ]
  edge [
    source 6
    target 968
  ]
  edge [
    source 6
    target 969
  ]
  edge [
    source 6
    target 970
  ]
  edge [
    source 6
    target 971
  ]
  edge [
    source 6
    target 972
  ]
  edge [
    source 6
    target 973
  ]
  edge [
    source 6
    target 974
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 975
  ]
  edge [
    source 6
    target 976
  ]
  edge [
    source 6
    target 977
  ]
  edge [
    source 6
    target 978
  ]
  edge [
    source 6
    target 979
  ]
  edge [
    source 6
    target 980
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 981
  ]
  edge [
    source 6
    target 982
  ]
  edge [
    source 6
    target 983
  ]
  edge [
    source 6
    target 984
  ]
  edge [
    source 6
    target 985
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 986
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 987
  ]
  edge [
    source 6
    target 988
  ]
  edge [
    source 6
    target 989
  ]
  edge [
    source 6
    target 990
  ]
  edge [
    source 6
    target 991
  ]
  edge [
    source 6
    target 992
  ]
  edge [
    source 6
    target 993
  ]
  edge [
    source 6
    target 994
  ]
  edge [
    source 6
    target 995
  ]
  edge [
    source 6
    target 996
  ]
  edge [
    source 6
    target 997
  ]
  edge [
    source 6
    target 998
  ]
  edge [
    source 6
    target 999
  ]
  edge [
    source 6
    target 1000
  ]
  edge [
    source 6
    target 1001
  ]
  edge [
    source 6
    target 1002
  ]
  edge [
    source 6
    target 1003
  ]
  edge [
    source 6
    target 1004
  ]
  edge [
    source 6
    target 1005
  ]
  edge [
    source 6
    target 1006
  ]
  edge [
    source 6
    target 1007
  ]
  edge [
    source 6
    target 1008
  ]
  edge [
    source 6
    target 1009
  ]
  edge [
    source 6
    target 1010
  ]
  edge [
    source 6
    target 1011
  ]
  edge [
    source 6
    target 1012
  ]
  edge [
    source 6
    target 1013
  ]
  edge [
    source 6
    target 1014
  ]
  edge [
    source 6
    target 1015
  ]
  edge [
    source 6
    target 1016
  ]
  edge [
    source 6
    target 1017
  ]
  edge [
    source 6
    target 1018
  ]
  edge [
    source 6
    target 1019
  ]
  edge [
    source 6
    target 1020
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 1021
  ]
  edge [
    source 6
    target 1022
  ]
  edge [
    source 6
    target 1023
  ]
  edge [
    source 6
    target 1024
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 1025
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 1026
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 1027
  ]
  edge [
    source 7
    target 1028
  ]
  edge [
    source 7
    target 1029
  ]
  edge [
    source 7
    target 1030
  ]
  edge [
    source 7
    target 1031
  ]
  edge [
    source 7
    target 1032
  ]
  edge [
    source 7
    target 1033
  ]
  edge [
    source 7
    target 1034
  ]
  edge [
    source 7
    target 1035
  ]
  edge [
    source 7
    target 1036
  ]
  edge [
    source 7
    target 1037
  ]
  edge [
    source 7
    target 1038
  ]
  edge [
    source 7
    target 1039
  ]
  edge [
    source 7
    target 1040
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 1041
  ]
  edge [
    source 7
    target 1042
  ]
  edge [
    source 7
    target 1043
  ]
  edge [
    source 7
    target 1044
  ]
  edge [
    source 7
    target 1045
  ]
  edge [
    source 7
    target 1046
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 1047
  ]
  edge [
    source 7
    target 1048
  ]
  edge [
    source 7
    target 1049
  ]
  edge [
    source 7
    target 1050
  ]
  edge [
    source 7
    target 1051
  ]
  edge [
    source 7
    target 1052
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1053
  ]
  edge [
    source 9
    target 1054
  ]
  edge [
    source 9
    target 955
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 1055
  ]
  edge [
    source 9
    target 1056
  ]
  edge [
    source 9
    target 1057
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 1058
  ]
  edge [
    source 9
    target 1059
  ]
  edge [
    source 9
    target 920
  ]
  edge [
    source 9
    target 1060
  ]
  edge [
    source 9
    target 1061
  ]
  edge [
    source 9
    target 1062
  ]
  edge [
    source 9
    target 1063
  ]
  edge [
    source 9
    target 1064
  ]
  edge [
    source 9
    target 1065
  ]
  edge [
    source 9
    target 1066
  ]
  edge [
    source 9
    target 1067
  ]
  edge [
    source 9
    target 1068
  ]
  edge [
    source 9
    target 1069
  ]
  edge [
    source 9
    target 1070
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 929
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 1071
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 1072
  ]
  edge [
    source 9
    target 1488
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1073
  ]
  edge [
    source 10
    target 1074
  ]
  edge [
    source 10
    target 1075
  ]
  edge [
    source 10
    target 1076
  ]
  edge [
    source 10
    target 1077
  ]
  edge [
    source 10
    target 1078
  ]
  edge [
    source 10
    target 1079
  ]
  edge [
    source 10
    target 1080
  ]
  edge [
    source 10
    target 1081
  ]
  edge [
    source 10
    target 1082
  ]
  edge [
    source 10
    target 1083
  ]
  edge [
    source 10
    target 1084
  ]
  edge [
    source 10
    target 1085
  ]
  edge [
    source 10
    target 1086
  ]
  edge [
    source 10
    target 1087
  ]
  edge [
    source 10
    target 1088
  ]
  edge [
    source 10
    target 1089
  ]
  edge [
    source 10
    target 1090
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 1091
  ]
  edge [
    source 10
    target 1092
  ]
  edge [
    source 10
    target 1093
  ]
  edge [
    source 10
    target 1094
  ]
  edge [
    source 10
    target 1095
  ]
  edge [
    source 10
    target 1096
  ]
  edge [
    source 10
    target 1097
  ]
  edge [
    source 10
    target 1098
  ]
  edge [
    source 10
    target 1099
  ]
  edge [
    source 10
    target 1100
  ]
  edge [
    source 10
    target 1101
  ]
  edge [
    source 10
    target 1102
  ]
  edge [
    source 10
    target 1103
  ]
  edge [
    source 10
    target 1104
  ]
  edge [
    source 10
    target 1105
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 1106
  ]
  edge [
    source 10
    target 1107
  ]
  edge [
    source 10
    target 1108
  ]
  edge [
    source 10
    target 1109
  ]
  edge [
    source 10
    target 1110
  ]
  edge [
    source 10
    target 1111
  ]
  edge [
    source 10
    target 1112
  ]
  edge [
    source 10
    target 1113
  ]
  edge [
    source 10
    target 1114
  ]
  edge [
    source 10
    target 1115
  ]
  edge [
    source 10
    target 1116
  ]
  edge [
    source 10
    target 1117
  ]
  edge [
    source 10
    target 1118
  ]
  edge [
    source 10
    target 1119
  ]
  edge [
    source 10
    target 1120
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1121
  ]
  edge [
    source 11
    target 1122
  ]
  edge [
    source 11
    target 1123
  ]
  edge [
    source 11
    target 1124
  ]
  edge [
    source 11
    target 1125
  ]
  edge [
    source 11
    target 1126
  ]
  edge [
    source 11
    target 1127
  ]
  edge [
    source 11
    target 1128
  ]
  edge [
    source 11
    target 1129
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1130
  ]
  edge [
    source 12
    target 1131
  ]
  edge [
    source 12
    target 1132
  ]
  edge [
    source 12
    target 1133
  ]
  edge [
    source 12
    target 1134
  ]
  edge [
    source 12
    target 1135
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 1136
  ]
  edge [
    source 12
    target 1137
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 1138
  ]
  edge [
    source 12
    target 1139
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 1140
  ]
  edge [
    source 12
    target 1141
  ]
  edge [
    source 12
    target 1142
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 1143
  ]
  edge [
    source 12
    target 1144
  ]
  edge [
    source 12
    target 1145
  ]
  edge [
    source 12
    target 1146
  ]
  edge [
    source 12
    target 1147
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 1148
  ]
  edge [
    source 12
    target 1149
  ]
  edge [
    source 12
    target 1150
  ]
  edge [
    source 12
    target 1151
  ]
  edge [
    source 12
    target 1152
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1153
  ]
  edge [
    source 14
    target 1154
  ]
  edge [
    source 14
    target 1155
  ]
  edge [
    source 14
    target 1156
  ]
  edge [
    source 14
    target 1157
  ]
  edge [
    source 14
    target 1158
  ]
  edge [
    source 14
    target 1159
  ]
  edge [
    source 14
    target 1160
  ]
  edge [
    source 14
    target 1161
  ]
  edge [
    source 14
    target 1162
  ]
  edge [
    source 14
    target 1163
  ]
  edge [
    source 14
    target 1164
  ]
  edge [
    source 14
    target 285
  ]
  edge [
    source 14
    target 396
  ]
  edge [
    source 14
    target 1165
  ]
  edge [
    source 14
    target 1166
  ]
  edge [
    source 14
    target 1167
  ]
  edge [
    source 14
    target 1168
  ]
  edge [
    source 14
    target 1169
  ]
  edge [
    source 14
    target 1170
  ]
  edge [
    source 14
    target 1171
  ]
  edge [
    source 14
    target 1172
  ]
  edge [
    source 14
    target 1173
  ]
  edge [
    source 14
    target 1174
  ]
  edge [
    source 14
    target 1175
  ]
  edge [
    source 14
    target 1176
  ]
  edge [
    source 14
    target 1177
  ]
  edge [
    source 14
    target 1178
  ]
  edge [
    source 14
    target 1179
  ]
  edge [
    source 14
    target 1180
  ]
  edge [
    source 14
    target 1181
  ]
  edge [
    source 14
    target 1182
  ]
  edge [
    source 14
    target 1183
  ]
  edge [
    source 14
    target 1184
  ]
  edge [
    source 14
    target 1185
  ]
  edge [
    source 14
    target 1186
  ]
  edge [
    source 14
    target 1187
  ]
  edge [
    source 14
    target 1188
  ]
  edge [
    source 14
    target 1189
  ]
  edge [
    source 14
    target 1190
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 1191
  ]
  edge [
    source 14
    target 1192
  ]
  edge [
    source 14
    target 1007
  ]
  edge [
    source 14
    target 1193
  ]
  edge [
    source 14
    target 1194
  ]
  edge [
    source 14
    target 1195
  ]
  edge [
    source 14
    target 1196
  ]
  edge [
    source 14
    target 1197
  ]
  edge [
    source 14
    target 1198
  ]
  edge [
    source 14
    target 1199
  ]
  edge [
    source 14
    target 1200
  ]
  edge [
    source 14
    target 214
  ]
  edge [
    source 14
    target 1201
  ]
  edge [
    source 14
    target 1202
  ]
  edge [
    source 14
    target 1203
  ]
  edge [
    source 14
    target 1204
  ]
  edge [
    source 14
    target 1205
  ]
  edge [
    source 14
    target 1206
  ]
  edge [
    source 14
    target 1207
  ]
  edge [
    source 14
    target 1208
  ]
  edge [
    source 14
    target 1209
  ]
  edge [
    source 14
    target 1210
  ]
  edge [
    source 14
    target 1211
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 15
    target 1212
  ]
  edge [
    source 15
    target 1213
  ]
  edge [
    source 15
    target 1214
  ]
  edge [
    source 15
    target 1215
  ]
  edge [
    source 15
    target 1216
  ]
  edge [
    source 15
    target 1217
  ]
  edge [
    source 15
    target 1218
  ]
  edge [
    source 15
    target 1219
  ]
  edge [
    source 15
    target 1220
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 1221
  ]
  edge [
    source 15
    target 1222
  ]
  edge [
    source 15
    target 1223
  ]
  edge [
    source 15
    target 1224
  ]
  edge [
    source 15
    target 1225
  ]
  edge [
    source 15
    target 1226
  ]
  edge [
    source 15
    target 1227
  ]
  edge [
    source 15
    target 1228
  ]
  edge [
    source 15
    target 1229
  ]
  edge [
    source 15
    target 1230
  ]
  edge [
    source 15
    target 1231
  ]
  edge [
    source 15
    target 1232
  ]
  edge [
    source 15
    target 1233
  ]
  edge [
    source 15
    target 1234
  ]
  edge [
    source 15
    target 1235
  ]
  edge [
    source 15
    target 338
  ]
  edge [
    source 15
    target 1236
  ]
  edge [
    source 15
    target 1237
  ]
  edge [
    source 15
    target 1238
  ]
  edge [
    source 15
    target 1239
  ]
  edge [
    source 15
    target 492
  ]
  edge [
    source 15
    target 1240
  ]
  edge [
    source 15
    target 1241
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1242
  ]
  edge [
    source 17
    target 1243
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 387
  ]
  edge [
    source 17
    target 115
  ]
  edge [
    source 17
    target 1178
  ]
  edge [
    source 17
    target 1244
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 1245
  ]
  edge [
    source 17
    target 1246
  ]
  edge [
    source 17
    target 384
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1247
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 1248
  ]
  edge [
    source 18
    target 803
  ]
  edge [
    source 18
    target 1249
  ]
  edge [
    source 18
    target 1250
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 1251
  ]
  edge [
    source 18
    target 1252
  ]
  edge [
    source 18
    target 1253
  ]
  edge [
    source 18
    target 84
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 1254
  ]
  edge [
    source 18
    target 1255
  ]
  edge [
    source 18
    target 1256
  ]
  edge [
    source 18
    target 1257
  ]
  edge [
    source 18
    target 1258
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 1259
  ]
  edge [
    source 18
    target 1260
  ]
  edge [
    source 18
    target 1261
  ]
  edge [
    source 18
    target 1262
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 1263
  ]
  edge [
    source 18
    target 1264
  ]
  edge [
    source 18
    target 1265
  ]
  edge [
    source 18
    target 370
  ]
  edge [
    source 18
    target 1266
  ]
  edge [
    source 18
    target 1267
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 1268
  ]
  edge [
    source 18
    target 1269
  ]
  edge [
    source 18
    target 1270
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 1271
  ]
  edge [
    source 18
    target 1272
  ]
  edge [
    source 18
    target 1273
  ]
  edge [
    source 18
    target 1274
  ]
  edge [
    source 18
    target 1275
  ]
  edge [
    source 18
    target 1276
  ]
  edge [
    source 18
    target 1277
  ]
  edge [
    source 18
    target 1278
  ]
  edge [
    source 18
    target 1279
  ]
  edge [
    source 18
    target 1280
  ]
  edge [
    source 18
    target 1281
  ]
  edge [
    source 18
    target 1282
  ]
  edge [
    source 18
    target 1283
  ]
  edge [
    source 18
    target 1284
  ]
  edge [
    source 18
    target 1285
  ]
  edge [
    source 18
    target 1286
  ]
  edge [
    source 18
    target 1287
  ]
  edge [
    source 18
    target 1288
  ]
  edge [
    source 18
    target 1289
  ]
  edge [
    source 18
    target 1290
  ]
  edge [
    source 18
    target 1291
  ]
  edge [
    source 18
    target 1292
  ]
  edge [
    source 18
    target 1293
  ]
  edge [
    source 18
    target 1294
  ]
  edge [
    source 18
    target 1295
  ]
  edge [
    source 18
    target 1296
  ]
  edge [
    source 18
    target 1297
  ]
  edge [
    source 18
    target 1298
  ]
  edge [
    source 18
    target 1299
  ]
  edge [
    source 18
    target 1300
  ]
  edge [
    source 18
    target 1301
  ]
  edge [
    source 18
    target 1302
  ]
  edge [
    source 18
    target 1303
  ]
  edge [
    source 18
    target 1304
  ]
  edge [
    source 18
    target 1305
  ]
  edge [
    source 18
    target 1306
  ]
  edge [
    source 18
    target 1307
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1309
  ]
  edge [
    source 18
    target 1310
  ]
  edge [
    source 18
    target 1311
  ]
  edge [
    source 18
    target 1312
  ]
  edge [
    source 18
    target 1313
  ]
  edge [
    source 18
    target 1314
  ]
  edge [
    source 18
    target 1315
  ]
  edge [
    source 18
    target 22
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 1316
  ]
  edge [
    source 19
    target 1317
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 1318
  ]
  edge [
    source 19
    target 1319
  ]
  edge [
    source 19
    target 1320
  ]
  edge [
    source 19
    target 1321
  ]
  edge [
    source 19
    target 1322
  ]
  edge [
    source 19
    target 1323
  ]
  edge [
    source 19
    target 1324
  ]
  edge [
    source 19
    target 1325
  ]
  edge [
    source 19
    target 1326
  ]
  edge [
    source 19
    target 1327
  ]
  edge [
    source 19
    target 1328
  ]
  edge [
    source 19
    target 1329
  ]
  edge [
    source 19
    target 1330
  ]
  edge [
    source 19
    target 1331
  ]
  edge [
    source 19
    target 1332
  ]
  edge [
    source 19
    target 1333
  ]
  edge [
    source 19
    target 1334
  ]
  edge [
    source 19
    target 1335
  ]
  edge [
    source 19
    target 1336
  ]
  edge [
    source 19
    target 1337
  ]
  edge [
    source 19
    target 1338
  ]
  edge [
    source 19
    target 1339
  ]
  edge [
    source 19
    target 1340
  ]
  edge [
    source 19
    target 1341
  ]
  edge [
    source 19
    target 1342
  ]
  edge [
    source 19
    target 1343
  ]
  edge [
    source 19
    target 1344
  ]
  edge [
    source 19
    target 1345
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 1346
  ]
  edge [
    source 19
    target 1347
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 1348
  ]
  edge [
    source 19
    target 1349
  ]
  edge [
    source 19
    target 1350
  ]
  edge [
    source 19
    target 1351
  ]
  edge [
    source 19
    target 1352
  ]
  edge [
    source 19
    target 1353
  ]
  edge [
    source 19
    target 1354
  ]
  edge [
    source 19
    target 1355
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 1356
  ]
  edge [
    source 19
    target 1357
  ]
  edge [
    source 19
    target 1358
  ]
  edge [
    source 19
    target 1359
  ]
  edge [
    source 19
    target 1360
  ]
  edge [
    source 19
    target 1361
  ]
  edge [
    source 19
    target 1362
  ]
  edge [
    source 19
    target 1363
  ]
  edge [
    source 19
    target 1364
  ]
  edge [
    source 19
    target 1365
  ]
  edge [
    source 19
    target 1366
  ]
  edge [
    source 19
    target 1367
  ]
  edge [
    source 19
    target 1368
  ]
  edge [
    source 19
    target 1369
  ]
  edge [
    source 19
    target 1370
  ]
  edge [
    source 19
    target 1371
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1372
  ]
  edge [
    source 19
    target 1373
  ]
  edge [
    source 19
    target 1374
  ]
  edge [
    source 19
    target 1375
  ]
  edge [
    source 19
    target 1376
  ]
  edge [
    source 19
    target 1377
  ]
  edge [
    source 19
    target 1378
  ]
  edge [
    source 19
    target 1379
  ]
  edge [
    source 19
    target 1380
  ]
  edge [
    source 19
    target 1381
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 831
  ]
  edge [
    source 19
    target 1382
  ]
  edge [
    source 19
    target 1383
  ]
  edge [
    source 19
    target 1384
  ]
  edge [
    source 19
    target 1385
  ]
  edge [
    source 19
    target 1386
  ]
  edge [
    source 19
    target 1387
  ]
  edge [
    source 19
    target 1388
  ]
  edge [
    source 19
    target 1389
  ]
  edge [
    source 19
    target 1390
  ]
  edge [
    source 19
    target 1391
  ]
  edge [
    source 19
    target 1392
  ]
  edge [
    source 19
    target 1393
  ]
  edge [
    source 19
    target 1394
  ]
  edge [
    source 19
    target 1395
  ]
  edge [
    source 19
    target 426
  ]
  edge [
    source 19
    target 1396
  ]
  edge [
    source 19
    target 1397
  ]
  edge [
    source 19
    target 1398
  ]
  edge [
    source 19
    target 1399
  ]
  edge [
    source 19
    target 1400
  ]
  edge [
    source 19
    target 1401
  ]
  edge [
    source 19
    target 1402
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1403
  ]
  edge [
    source 19
    target 307
  ]
  edge [
    source 19
    target 1404
  ]
  edge [
    source 19
    target 1405
  ]
  edge [
    source 19
    target 1406
  ]
  edge [
    source 19
    target 1407
  ]
  edge [
    source 19
    target 1408
  ]
  edge [
    source 19
    target 1409
  ]
  edge [
    source 19
    target 1410
  ]
  edge [
    source 19
    target 1411
  ]
  edge [
    source 19
    target 1412
  ]
  edge [
    source 19
    target 1413
  ]
  edge [
    source 19
    target 1414
  ]
  edge [
    source 19
    target 1415
  ]
  edge [
    source 19
    target 1416
  ]
  edge [
    source 19
    target 1417
  ]
  edge [
    source 19
    target 1418
  ]
  edge [
    source 19
    target 1419
  ]
  edge [
    source 19
    target 1420
  ]
  edge [
    source 19
    target 1421
  ]
  edge [
    source 19
    target 1422
  ]
  edge [
    source 19
    target 1423
  ]
  edge [
    source 19
    target 1424
  ]
  edge [
    source 19
    target 1425
  ]
  edge [
    source 19
    target 1426
  ]
  edge [
    source 19
    target 1427
  ]
  edge [
    source 19
    target 1428
  ]
  edge [
    source 19
    target 1429
  ]
  edge [
    source 19
    target 1430
  ]
  edge [
    source 19
    target 1431
  ]
  edge [
    source 19
    target 1432
  ]
  edge [
    source 19
    target 1433
  ]
  edge [
    source 19
    target 1434
  ]
  edge [
    source 19
    target 1435
  ]
  edge [
    source 19
    target 1436
  ]
  edge [
    source 19
    target 1437
  ]
  edge [
    source 19
    target 1438
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 1439
  ]
  edge [
    source 19
    target 1440
  ]
  edge [
    source 19
    target 1441
  ]
  edge [
    source 19
    target 1442
  ]
  edge [
    source 19
    target 1443
  ]
  edge [
    source 19
    target 660
  ]
  edge [
    source 19
    target 1444
  ]
  edge [
    source 19
    target 1445
  ]
  edge [
    source 19
    target 1446
  ]
  edge [
    source 19
    target 1447
  ]
  edge [
    source 19
    target 1448
  ]
  edge [
    source 19
    target 1449
  ]
  edge [
    source 19
    target 1450
  ]
  edge [
    source 19
    target 1451
  ]
  edge [
    source 19
    target 1452
  ]
  edge [
    source 19
    target 1453
  ]
  edge [
    source 19
    target 1454
  ]
  edge [
    source 19
    target 1455
  ]
  edge [
    source 19
    target 633
  ]
  edge [
    source 19
    target 1456
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1457
  ]
  edge [
    source 19
    target 1458
  ]
  edge [
    source 19
    target 1459
  ]
  edge [
    source 19
    target 1460
  ]
  edge [
    source 19
    target 1461
  ]
  edge [
    source 19
    target 1462
  ]
  edge [
    source 19
    target 1463
  ]
  edge [
    source 19
    target 1464
  ]
  edge [
    source 19
    target 1465
  ]
  edge [
    source 19
    target 1466
  ]
  edge [
    source 19
    target 1467
  ]
  edge [
    source 19
    target 1468
  ]
  edge [
    source 19
    target 1469
  ]
  edge [
    source 19
    target 1470
  ]
  edge [
    source 19
    target 1471
  ]
  edge [
    source 19
    target 1472
  ]
  edge [
    source 19
    target 1473
  ]
  edge [
    source 19
    target 1474
  ]
  edge [
    source 19
    target 353
  ]
  edge [
    source 19
    target 1475
  ]
  edge [
    source 19
    target 1476
  ]
  edge [
    source 19
    target 1477
  ]
  edge [
    source 19
    target 1478
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1066
  ]
  edge [
    source 21
    target 1479
  ]
  edge [
    source 21
    target 1480
  ]
  edge [
    source 21
    target 1481
  ]
  edge [
    source 21
    target 1482
  ]
  edge [
    source 21
    target 1483
  ]
  edge [
    source 21
    target 1484
  ]
  edge [
    source 21
    target 1485
  ]
  edge [
    source 21
    target 1486
  ]
  edge [
    source 21
    target 930
  ]
  edge [
    source 21
    target 1487
  ]
  edge [
    source 21
    target 933
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
]
