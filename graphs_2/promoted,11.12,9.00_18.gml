graph [
  node [
    id 0
    label "prawdziwy"
    origin "text"
  ]
  node [
    id 1
    label "plaga"
    origin "text"
  ]
  node [
    id 2
    label "&#380;ywny"
  ]
  node [
    id 3
    label "szczery"
  ]
  node [
    id 4
    label "naturalny"
  ]
  node [
    id 5
    label "naprawd&#281;"
  ]
  node [
    id 6
    label "realnie"
  ]
  node [
    id 7
    label "podobny"
  ]
  node [
    id 8
    label "zgodny"
  ]
  node [
    id 9
    label "m&#261;dry"
  ]
  node [
    id 10
    label "prawdziwie"
  ]
  node [
    id 11
    label "prawy"
  ]
  node [
    id 12
    label "zrozumia&#322;y"
  ]
  node [
    id 13
    label "immanentny"
  ]
  node [
    id 14
    label "zwyczajny"
  ]
  node [
    id 15
    label "bezsporny"
  ]
  node [
    id 16
    label "organicznie"
  ]
  node [
    id 17
    label "pierwotny"
  ]
  node [
    id 18
    label "neutralny"
  ]
  node [
    id 19
    label "normalny"
  ]
  node [
    id 20
    label "rzeczywisty"
  ]
  node [
    id 21
    label "naturalnie"
  ]
  node [
    id 22
    label "zgodnie"
  ]
  node [
    id 23
    label "zbie&#380;ny"
  ]
  node [
    id 24
    label "spokojny"
  ]
  node [
    id 25
    label "dobry"
  ]
  node [
    id 26
    label "zm&#261;drzenie"
  ]
  node [
    id 27
    label "m&#261;drzenie"
  ]
  node [
    id 28
    label "m&#261;drze"
  ]
  node [
    id 29
    label "skomplikowany"
  ]
  node [
    id 30
    label "m&#261;drzenie_si&#281;"
  ]
  node [
    id 31
    label "pyszny"
  ]
  node [
    id 32
    label "inteligentny"
  ]
  node [
    id 33
    label "szczodry"
  ]
  node [
    id 34
    label "s&#322;uszny"
  ]
  node [
    id 35
    label "uczciwy"
  ]
  node [
    id 36
    label "przekonuj&#261;cy"
  ]
  node [
    id 37
    label "prostoduszny"
  ]
  node [
    id 38
    label "szczyry"
  ]
  node [
    id 39
    label "szczerze"
  ]
  node [
    id 40
    label "czysty"
  ]
  node [
    id 41
    label "przypominanie"
  ]
  node [
    id 42
    label "podobnie"
  ]
  node [
    id 43
    label "asymilowanie"
  ]
  node [
    id 44
    label "upodabnianie_si&#281;"
  ]
  node [
    id 45
    label "upodobnienie"
  ]
  node [
    id 46
    label "drugi"
  ]
  node [
    id 47
    label "taki"
  ]
  node [
    id 48
    label "charakterystyczny"
  ]
  node [
    id 49
    label "upodobnienie_si&#281;"
  ]
  node [
    id 50
    label "zasymilowanie"
  ]
  node [
    id 51
    label "szczero"
  ]
  node [
    id 52
    label "truly"
  ]
  node [
    id 53
    label "realny"
  ]
  node [
    id 54
    label "mo&#380;liwie"
  ]
  node [
    id 55
    label "&#380;yzny"
  ]
  node [
    id 56
    label "trouble"
  ]
  node [
    id 57
    label "wydarzenie"
  ]
  node [
    id 58
    label "przebiec"
  ]
  node [
    id 59
    label "charakter"
  ]
  node [
    id 60
    label "czynno&#347;&#263;"
  ]
  node [
    id 61
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 62
    label "motyw"
  ]
  node [
    id 63
    label "przebiegni&#281;cie"
  ]
  node [
    id 64
    label "fabu&#322;a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
]
