graph [
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ponownie"
    origin "text"
  ]
  node [
    id 2
    label "greet"
  ]
  node [
    id 3
    label "welcome"
  ]
  node [
    id 4
    label "pozdrawia&#263;"
  ]
  node [
    id 5
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 6
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 7
    label "robi&#263;"
  ]
  node [
    id 8
    label "obchodzi&#263;"
  ]
  node [
    id 9
    label "bless"
  ]
  node [
    id 10
    label "znowu"
  ]
  node [
    id 11
    label "ponowny"
  ]
  node [
    id 12
    label "wznawianie"
  ]
  node [
    id 13
    label "wznowienie_si&#281;"
  ]
  node [
    id 14
    label "drugi"
  ]
  node [
    id 15
    label "wznawianie_si&#281;"
  ]
  node [
    id 16
    label "dalszy"
  ]
  node [
    id 17
    label "nawrotny"
  ]
  node [
    id 18
    label "wznowienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
]
