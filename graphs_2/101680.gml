graph [
  node [
    id 0
    label "o&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 1
    label "poselski"
    origin "text"
  ]
  node [
    id 2
    label "komunikat"
  ]
  node [
    id 3
    label "announcement"
  ]
  node [
    id 4
    label "poinformowanie"
  ]
  node [
    id 5
    label "resolution"
  ]
  node [
    id 6
    label "wypowied&#378;"
  ]
  node [
    id 7
    label "statement"
  ]
  node [
    id 8
    label "zwiastowanie"
  ]
  node [
    id 9
    label "parafrazowanie"
  ]
  node [
    id 10
    label "stylizacja"
  ]
  node [
    id 11
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 12
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 13
    label "strawestowanie"
  ]
  node [
    id 14
    label "sparafrazowanie"
  ]
  node [
    id 15
    label "sformu&#322;owanie"
  ]
  node [
    id 16
    label "pos&#322;uchanie"
  ]
  node [
    id 17
    label "strawestowa&#263;"
  ]
  node [
    id 18
    label "parafrazowa&#263;"
  ]
  node [
    id 19
    label "delimitacja"
  ]
  node [
    id 20
    label "rezultat"
  ]
  node [
    id 21
    label "ozdobnik"
  ]
  node [
    id 22
    label "trawestowa&#263;"
  ]
  node [
    id 23
    label "s&#261;d"
  ]
  node [
    id 24
    label "sparafrazowa&#263;"
  ]
  node [
    id 25
    label "trawestowanie"
  ]
  node [
    id 26
    label "roi&#263;_si&#281;"
  ]
  node [
    id 27
    label "kreacjonista"
  ]
  node [
    id 28
    label "communication"
  ]
  node [
    id 29
    label "wytw&#243;r"
  ]
  node [
    id 30
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 31
    label "telling"
  ]
  node [
    id 32
    label "spowodowanie"
  ]
  node [
    id 33
    label "zrobienie"
  ]
  node [
    id 34
    label "oznajmienie"
  ]
  node [
    id 35
    label "&#347;wiadczenie"
  ]
  node [
    id 36
    label "oznajmianie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
]
