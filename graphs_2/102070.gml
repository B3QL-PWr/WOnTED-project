graph [
  node [
    id 0
    label "jeden"
    origin "text"
  ]
  node [
    id 1
    label "moi"
    origin "text"
  ]
  node [
    id 2
    label "ulubiony"
    origin "text"
  ]
  node [
    id 3
    label "bloger"
    origin "text"
  ]
  node [
    id 4
    label "john"
    origin "text"
  ]
  node [
    id 5
    label "naughton"
    origin "text"
  ]
  node [
    id 6
    label "pisz"
    origin "text"
  ]
  node [
    id 7
    label "dobry"
    origin "text"
  ]
  node [
    id 8
    label "wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "mama"
    origin "text"
  ]
  node [
    id 10
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 11
    label "przez"
    origin "text"
  ]
  node [
    id 12
    label "jak"
    origin "text"
  ]
  node [
    id 13
    label "europa"
    origin "text"
  ]
  node [
    id 14
    label "by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "pora&#380;ka"
    origin "text"
  ]
  node [
    id 16
    label "pis"
    origin "text"
  ]
  node [
    id 17
    label "ostatni"
    origin "text"
  ]
  node [
    id 18
    label "wybory"
    origin "text"
  ]
  node [
    id 19
    label "zlinkowanym"
    origin "text"
  ]
  node [
    id 20
    label "naughtona"
    origin "text"
  ]
  node [
    id 21
    label "tekst"
    origin "text"
  ]
  node [
    id 22
    label "guardiana"
    origin "text"
  ]
  node [
    id 23
    label "zatytu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 24
    label "straszny"
    origin "text"
  ]
  node [
    id 25
    label "bli&#378;niak"
    origin "text"
  ]
  node [
    id 26
    label "shot"
  ]
  node [
    id 27
    label "jednakowy"
  ]
  node [
    id 28
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 29
    label "ujednolicenie"
  ]
  node [
    id 30
    label "jaki&#347;"
  ]
  node [
    id 31
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 32
    label "jednolicie"
  ]
  node [
    id 33
    label "kieliszek"
  ]
  node [
    id 34
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 35
    label "w&#243;dka"
  ]
  node [
    id 36
    label "ten"
  ]
  node [
    id 37
    label "szk&#322;o"
  ]
  node [
    id 38
    label "zawarto&#347;&#263;"
  ]
  node [
    id 39
    label "naczynie"
  ]
  node [
    id 40
    label "alkohol"
  ]
  node [
    id 41
    label "sznaps"
  ]
  node [
    id 42
    label "nap&#243;j"
  ]
  node [
    id 43
    label "gorza&#322;ka"
  ]
  node [
    id 44
    label "mohorycz"
  ]
  node [
    id 45
    label "okre&#347;lony"
  ]
  node [
    id 46
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 47
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 48
    label "zr&#243;wnanie"
  ]
  node [
    id 49
    label "mundurowanie"
  ]
  node [
    id 50
    label "taki&#380;"
  ]
  node [
    id 51
    label "jednakowo"
  ]
  node [
    id 52
    label "mundurowa&#263;"
  ]
  node [
    id 53
    label "zr&#243;wnywanie"
  ]
  node [
    id 54
    label "identyczny"
  ]
  node [
    id 55
    label "z&#322;o&#380;ony"
  ]
  node [
    id 56
    label "przyzwoity"
  ]
  node [
    id 57
    label "ciekawy"
  ]
  node [
    id 58
    label "jako&#347;"
  ]
  node [
    id 59
    label "jako_tako"
  ]
  node [
    id 60
    label "niez&#322;y"
  ]
  node [
    id 61
    label "dziwny"
  ]
  node [
    id 62
    label "charakterystyczny"
  ]
  node [
    id 63
    label "g&#322;&#281;bszy"
  ]
  node [
    id 64
    label "drink"
  ]
  node [
    id 65
    label "upodobnienie"
  ]
  node [
    id 66
    label "jednolity"
  ]
  node [
    id 67
    label "calibration"
  ]
  node [
    id 68
    label "wyj&#261;tkowy"
  ]
  node [
    id 69
    label "faworytny"
  ]
  node [
    id 70
    label "wyj&#261;tkowo"
  ]
  node [
    id 71
    label "inny"
  ]
  node [
    id 72
    label "ukochany"
  ]
  node [
    id 73
    label "internauta"
  ]
  node [
    id 74
    label "autor"
  ]
  node [
    id 75
    label "kszta&#322;ciciel"
  ]
  node [
    id 76
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 77
    label "tworzyciel"
  ]
  node [
    id 78
    label "wykonawca"
  ]
  node [
    id 79
    label "pomys&#322;odawca"
  ]
  node [
    id 80
    label "&#347;w"
  ]
  node [
    id 81
    label "u&#380;ytkownik"
  ]
  node [
    id 82
    label "dobroczynny"
  ]
  node [
    id 83
    label "czw&#243;rka"
  ]
  node [
    id 84
    label "spokojny"
  ]
  node [
    id 85
    label "skuteczny"
  ]
  node [
    id 86
    label "&#347;mieszny"
  ]
  node [
    id 87
    label "mi&#322;y"
  ]
  node [
    id 88
    label "grzeczny"
  ]
  node [
    id 89
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 90
    label "powitanie"
  ]
  node [
    id 91
    label "dobrze"
  ]
  node [
    id 92
    label "ca&#322;y"
  ]
  node [
    id 93
    label "zwrot"
  ]
  node [
    id 94
    label "pomy&#347;lny"
  ]
  node [
    id 95
    label "moralny"
  ]
  node [
    id 96
    label "drogi"
  ]
  node [
    id 97
    label "pozytywny"
  ]
  node [
    id 98
    label "odpowiedni"
  ]
  node [
    id 99
    label "korzystny"
  ]
  node [
    id 100
    label "pos&#322;uszny"
  ]
  node [
    id 101
    label "moralnie"
  ]
  node [
    id 102
    label "warto&#347;ciowy"
  ]
  node [
    id 103
    label "etycznie"
  ]
  node [
    id 104
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 105
    label "nale&#380;ny"
  ]
  node [
    id 106
    label "nale&#380;yty"
  ]
  node [
    id 107
    label "typowy"
  ]
  node [
    id 108
    label "uprawniony"
  ]
  node [
    id 109
    label "zasadniczy"
  ]
  node [
    id 110
    label "stosownie"
  ]
  node [
    id 111
    label "taki"
  ]
  node [
    id 112
    label "prawdziwy"
  ]
  node [
    id 113
    label "pozytywnie"
  ]
  node [
    id 114
    label "fajny"
  ]
  node [
    id 115
    label "dodatnio"
  ]
  node [
    id 116
    label "przyjemny"
  ]
  node [
    id 117
    label "po&#380;&#261;dany"
  ]
  node [
    id 118
    label "niepowa&#380;ny"
  ]
  node [
    id 119
    label "o&#347;mieszanie"
  ]
  node [
    id 120
    label "&#347;miesznie"
  ]
  node [
    id 121
    label "bawny"
  ]
  node [
    id 122
    label "o&#347;mieszenie"
  ]
  node [
    id 123
    label "nieadekwatny"
  ]
  node [
    id 124
    label "wolny"
  ]
  node [
    id 125
    label "uspokajanie_si&#281;"
  ]
  node [
    id 126
    label "bezproblemowy"
  ]
  node [
    id 127
    label "spokojnie"
  ]
  node [
    id 128
    label "uspokojenie_si&#281;"
  ]
  node [
    id 129
    label "cicho"
  ]
  node [
    id 130
    label "uspokojenie"
  ]
  node [
    id 131
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 132
    label "nietrudny"
  ]
  node [
    id 133
    label "uspokajanie"
  ]
  node [
    id 134
    label "zale&#380;ny"
  ]
  node [
    id 135
    label "uleg&#322;y"
  ]
  node [
    id 136
    label "pos&#322;usznie"
  ]
  node [
    id 137
    label "grzecznie"
  ]
  node [
    id 138
    label "stosowny"
  ]
  node [
    id 139
    label "niewinny"
  ]
  node [
    id 140
    label "konserwatywny"
  ]
  node [
    id 141
    label "nijaki"
  ]
  node [
    id 142
    label "korzystnie"
  ]
  node [
    id 143
    label "drogo"
  ]
  node [
    id 144
    label "cz&#322;owiek"
  ]
  node [
    id 145
    label "bliski"
  ]
  node [
    id 146
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 147
    label "przyjaciel"
  ]
  node [
    id 148
    label "jedyny"
  ]
  node [
    id 149
    label "du&#380;y"
  ]
  node [
    id 150
    label "zdr&#243;w"
  ]
  node [
    id 151
    label "calu&#347;ko"
  ]
  node [
    id 152
    label "kompletny"
  ]
  node [
    id 153
    label "&#380;ywy"
  ]
  node [
    id 154
    label "pe&#322;ny"
  ]
  node [
    id 155
    label "podobny"
  ]
  node [
    id 156
    label "ca&#322;o"
  ]
  node [
    id 157
    label "poskutkowanie"
  ]
  node [
    id 158
    label "sprawny"
  ]
  node [
    id 159
    label "skutecznie"
  ]
  node [
    id 160
    label "skutkowanie"
  ]
  node [
    id 161
    label "pomy&#347;lnie"
  ]
  node [
    id 162
    label "toto-lotek"
  ]
  node [
    id 163
    label "trafienie"
  ]
  node [
    id 164
    label "zbi&#243;r"
  ]
  node [
    id 165
    label "arkusz_drukarski"
  ]
  node [
    id 166
    label "&#322;&#243;dka"
  ]
  node [
    id 167
    label "four"
  ]
  node [
    id 168
    label "&#263;wiartka"
  ]
  node [
    id 169
    label "hotel"
  ]
  node [
    id 170
    label "cyfra"
  ]
  node [
    id 171
    label "pok&#243;j"
  ]
  node [
    id 172
    label "stopie&#324;"
  ]
  node [
    id 173
    label "obiekt"
  ]
  node [
    id 174
    label "minialbum"
  ]
  node [
    id 175
    label "osada"
  ]
  node [
    id 176
    label "p&#322;yta_winylowa"
  ]
  node [
    id 177
    label "blotka"
  ]
  node [
    id 178
    label "zaprz&#281;g"
  ]
  node [
    id 179
    label "przedtrzonowiec"
  ]
  node [
    id 180
    label "punkt"
  ]
  node [
    id 181
    label "turn"
  ]
  node [
    id 182
    label "turning"
  ]
  node [
    id 183
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 184
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 185
    label "skr&#281;t"
  ]
  node [
    id 186
    label "obr&#243;t"
  ]
  node [
    id 187
    label "fraza_czasownikowa"
  ]
  node [
    id 188
    label "jednostka_leksykalna"
  ]
  node [
    id 189
    label "zmiana"
  ]
  node [
    id 190
    label "wyra&#380;enie"
  ]
  node [
    id 191
    label "welcome"
  ]
  node [
    id 192
    label "spotkanie"
  ]
  node [
    id 193
    label "pozdrowienie"
  ]
  node [
    id 194
    label "zwyczaj"
  ]
  node [
    id 195
    label "greeting"
  ]
  node [
    id 196
    label "zdarzony"
  ]
  node [
    id 197
    label "odpowiednio"
  ]
  node [
    id 198
    label "odpowiadanie"
  ]
  node [
    id 199
    label "specjalny"
  ]
  node [
    id 200
    label "kochanek"
  ]
  node [
    id 201
    label "sk&#322;onny"
  ]
  node [
    id 202
    label "wybranek"
  ]
  node [
    id 203
    label "umi&#322;owany"
  ]
  node [
    id 204
    label "przyjemnie"
  ]
  node [
    id 205
    label "mi&#322;o"
  ]
  node [
    id 206
    label "kochanie"
  ]
  node [
    id 207
    label "dyplomata"
  ]
  node [
    id 208
    label "dobroczynnie"
  ]
  node [
    id 209
    label "lepiej"
  ]
  node [
    id 210
    label "wiele"
  ]
  node [
    id 211
    label "spo&#322;eczny"
  ]
  node [
    id 212
    label "znoszenie"
  ]
  node [
    id 213
    label "nap&#322;ywanie"
  ]
  node [
    id 214
    label "communication"
  ]
  node [
    id 215
    label "signal"
  ]
  node [
    id 216
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 217
    label "znosi&#263;"
  ]
  node [
    id 218
    label "znie&#347;&#263;"
  ]
  node [
    id 219
    label "zniesienie"
  ]
  node [
    id 220
    label "zarys"
  ]
  node [
    id 221
    label "informacja"
  ]
  node [
    id 222
    label "komunikat"
  ]
  node [
    id 223
    label "depesza_emska"
  ]
  node [
    id 224
    label "kszta&#322;t"
  ]
  node [
    id 225
    label "opracowanie"
  ]
  node [
    id 226
    label "pomys&#322;"
  ]
  node [
    id 227
    label "podstawy"
  ]
  node [
    id 228
    label "shape"
  ]
  node [
    id 229
    label "kreacjonista"
  ]
  node [
    id 230
    label "roi&#263;_si&#281;"
  ]
  node [
    id 231
    label "wytw&#243;r"
  ]
  node [
    id 232
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 233
    label "publikacja"
  ]
  node [
    id 234
    label "wiedza"
  ]
  node [
    id 235
    label "obiega&#263;"
  ]
  node [
    id 236
    label "powzi&#281;cie"
  ]
  node [
    id 237
    label "dane"
  ]
  node [
    id 238
    label "obiegni&#281;cie"
  ]
  node [
    id 239
    label "sygna&#322;"
  ]
  node [
    id 240
    label "obieganie"
  ]
  node [
    id 241
    label "powzi&#261;&#263;"
  ]
  node [
    id 242
    label "obiec"
  ]
  node [
    id 243
    label "doj&#347;cie"
  ]
  node [
    id 244
    label "doj&#347;&#263;"
  ]
  node [
    id 245
    label "shoot"
  ]
  node [
    id 246
    label "pour"
  ]
  node [
    id 247
    label "zasila&#263;"
  ]
  node [
    id 248
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 249
    label "kapita&#322;"
  ]
  node [
    id 250
    label "meet"
  ]
  node [
    id 251
    label "dociera&#263;"
  ]
  node [
    id 252
    label "zbiera&#263;_si&#281;"
  ]
  node [
    id 253
    label "wzbiera&#263;"
  ]
  node [
    id 254
    label "ogarnia&#263;"
  ]
  node [
    id 255
    label "wype&#322;nia&#263;"
  ]
  node [
    id 256
    label "gromadzenie_si&#281;"
  ]
  node [
    id 257
    label "zbieranie_si&#281;"
  ]
  node [
    id 258
    label "zasilanie"
  ]
  node [
    id 259
    label "docieranie"
  ]
  node [
    id 260
    label "t&#281;&#380;enie"
  ]
  node [
    id 261
    label "nawiewanie"
  ]
  node [
    id 262
    label "nadmuchanie"
  ]
  node [
    id 263
    label "ogarnianie"
  ]
  node [
    id 264
    label "gromadzi&#263;"
  ]
  node [
    id 265
    label "usuwa&#263;"
  ]
  node [
    id 266
    label "porywa&#263;"
  ]
  node [
    id 267
    label "sk&#322;ada&#263;"
  ]
  node [
    id 268
    label "ranny"
  ]
  node [
    id 269
    label "zbiera&#263;"
  ]
  node [
    id 270
    label "behave"
  ]
  node [
    id 271
    label "carry"
  ]
  node [
    id 272
    label "represent"
  ]
  node [
    id 273
    label "podrze&#263;"
  ]
  node [
    id 274
    label "przenosi&#263;"
  ]
  node [
    id 275
    label "str&#243;j"
  ]
  node [
    id 276
    label "wytrzymywa&#263;"
  ]
  node [
    id 277
    label "seclude"
  ]
  node [
    id 278
    label "wygrywa&#263;"
  ]
  node [
    id 279
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 280
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 281
    label "set"
  ]
  node [
    id 282
    label "zu&#380;y&#263;"
  ]
  node [
    id 283
    label "niszczy&#263;"
  ]
  node [
    id 284
    label "tolerowa&#263;"
  ]
  node [
    id 285
    label "jajko"
  ]
  node [
    id 286
    label "zgromadzenie"
  ]
  node [
    id 287
    label "urodzenie"
  ]
  node [
    id 288
    label "suspension"
  ]
  node [
    id 289
    label "poddanie_si&#281;"
  ]
  node [
    id 290
    label "extinction"
  ]
  node [
    id 291
    label "coitus_interruptus"
  ]
  node [
    id 292
    label "przetrwanie"
  ]
  node [
    id 293
    label "&#347;cierpienie"
  ]
  node [
    id 294
    label "abolicjonista"
  ]
  node [
    id 295
    label "zniszczenie"
  ]
  node [
    id 296
    label "posk&#322;adanie"
  ]
  node [
    id 297
    label "zebranie"
  ]
  node [
    id 298
    label "przeniesienie"
  ]
  node [
    id 299
    label "removal"
  ]
  node [
    id 300
    label "withdrawal"
  ]
  node [
    id 301
    label "revocation"
  ]
  node [
    id 302
    label "usuni&#281;cie"
  ]
  node [
    id 303
    label "wygranie"
  ]
  node [
    id 304
    label "porwanie"
  ]
  node [
    id 305
    label "uniewa&#380;nienie"
  ]
  node [
    id 306
    label "toleration"
  ]
  node [
    id 307
    label "collection"
  ]
  node [
    id 308
    label "wytrzymywanie"
  ]
  node [
    id 309
    label "take"
  ]
  node [
    id 310
    label "usuwanie"
  ]
  node [
    id 311
    label "porywanie"
  ]
  node [
    id 312
    label "wygrywanie"
  ]
  node [
    id 313
    label "abrogation"
  ]
  node [
    id 314
    label "gromadzenie"
  ]
  node [
    id 315
    label "przenoszenie"
  ]
  node [
    id 316
    label "poddawanie_si&#281;"
  ]
  node [
    id 317
    label "wear"
  ]
  node [
    id 318
    label "uniewa&#380;nianie"
  ]
  node [
    id 319
    label "rodzenie"
  ]
  node [
    id 320
    label "tolerowanie"
  ]
  node [
    id 321
    label "niszczenie"
  ]
  node [
    id 322
    label "stand"
  ]
  node [
    id 323
    label "zgromadzi&#263;"
  ]
  node [
    id 324
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 325
    label "float"
  ]
  node [
    id 326
    label "revoke"
  ]
  node [
    id 327
    label "usun&#261;&#263;"
  ]
  node [
    id 328
    label "zebra&#263;"
  ]
  node [
    id 329
    label "wytrzyma&#263;"
  ]
  node [
    id 330
    label "digest"
  ]
  node [
    id 331
    label "lift"
  ]
  node [
    id 332
    label "podda&#263;_si&#281;"
  ]
  node [
    id 333
    label "przenie&#347;&#263;"
  ]
  node [
    id 334
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 335
    label "&#347;cierpie&#263;"
  ]
  node [
    id 336
    label "porwa&#263;"
  ]
  node [
    id 337
    label "wygra&#263;"
  ]
  node [
    id 338
    label "raise"
  ]
  node [
    id 339
    label "zniszczy&#263;"
  ]
  node [
    id 340
    label "przodkini"
  ]
  node [
    id 341
    label "matka_zast&#281;pcza"
  ]
  node [
    id 342
    label "matczysko"
  ]
  node [
    id 343
    label "rodzice"
  ]
  node [
    id 344
    label "stara"
  ]
  node [
    id 345
    label "macierz"
  ]
  node [
    id 346
    label "rodzic"
  ]
  node [
    id 347
    label "Matka_Boska"
  ]
  node [
    id 348
    label "macocha"
  ]
  node [
    id 349
    label "starzy"
  ]
  node [
    id 350
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 351
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 352
    label "pokolenie"
  ]
  node [
    id 353
    label "wapniaki"
  ]
  node [
    id 354
    label "opiekun"
  ]
  node [
    id 355
    label "wapniak"
  ]
  node [
    id 356
    label "rodzic_chrzestny"
  ]
  node [
    id 357
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 358
    label "krewna"
  ]
  node [
    id 359
    label "matka"
  ]
  node [
    id 360
    label "&#380;ona"
  ]
  node [
    id 361
    label "kobieta"
  ]
  node [
    id 362
    label "partnerka"
  ]
  node [
    id 363
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 364
    label "matuszka"
  ]
  node [
    id 365
    label "parametryzacja"
  ]
  node [
    id 366
    label "pa&#324;stwo"
  ]
  node [
    id 367
    label "poj&#281;cie"
  ]
  node [
    id 368
    label "mod"
  ]
  node [
    id 369
    label "patriota"
  ]
  node [
    id 370
    label "m&#281;&#380;atka"
  ]
  node [
    id 371
    label "tydzie&#324;"
  ]
  node [
    id 372
    label "miech"
  ]
  node [
    id 373
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 374
    label "czas"
  ]
  node [
    id 375
    label "rok"
  ]
  node [
    id 376
    label "kalendy"
  ]
  node [
    id 377
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 378
    label "satelita"
  ]
  node [
    id 379
    label "peryselenium"
  ]
  node [
    id 380
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 381
    label "&#347;wiat&#322;o"
  ]
  node [
    id 382
    label "aposelenium"
  ]
  node [
    id 383
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 384
    label "Tytan"
  ]
  node [
    id 385
    label "moon"
  ]
  node [
    id 386
    label "aparat_fotograficzny"
  ]
  node [
    id 387
    label "bag"
  ]
  node [
    id 388
    label "sakwa"
  ]
  node [
    id 389
    label "torba"
  ]
  node [
    id 390
    label "przyrz&#261;d"
  ]
  node [
    id 391
    label "w&#243;r"
  ]
  node [
    id 392
    label "poprzedzanie"
  ]
  node [
    id 393
    label "czasoprzestrze&#324;"
  ]
  node [
    id 394
    label "laba"
  ]
  node [
    id 395
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 396
    label "chronometria"
  ]
  node [
    id 397
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 398
    label "rachuba_czasu"
  ]
  node [
    id 399
    label "przep&#322;ywanie"
  ]
  node [
    id 400
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 401
    label "czasokres"
  ]
  node [
    id 402
    label "odczyt"
  ]
  node [
    id 403
    label "chwila"
  ]
  node [
    id 404
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 405
    label "dzieje"
  ]
  node [
    id 406
    label "kategoria_gramatyczna"
  ]
  node [
    id 407
    label "poprzedzenie"
  ]
  node [
    id 408
    label "trawienie"
  ]
  node [
    id 409
    label "pochodzi&#263;"
  ]
  node [
    id 410
    label "period"
  ]
  node [
    id 411
    label "okres_czasu"
  ]
  node [
    id 412
    label "poprzedza&#263;"
  ]
  node [
    id 413
    label "schy&#322;ek"
  ]
  node [
    id 414
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 415
    label "odwlekanie_si&#281;"
  ]
  node [
    id 416
    label "zegar"
  ]
  node [
    id 417
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 418
    label "czwarty_wymiar"
  ]
  node [
    id 419
    label "pochodzenie"
  ]
  node [
    id 420
    label "koniugacja"
  ]
  node [
    id 421
    label "Zeitgeist"
  ]
  node [
    id 422
    label "trawi&#263;"
  ]
  node [
    id 423
    label "pogoda"
  ]
  node [
    id 424
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 425
    label "poprzedzi&#263;"
  ]
  node [
    id 426
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 427
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 428
    label "time_period"
  ]
  node [
    id 429
    label "doba"
  ]
  node [
    id 430
    label "weekend"
  ]
  node [
    id 431
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 432
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 433
    label "p&#243;&#322;rocze"
  ]
  node [
    id 434
    label "martwy_sezon"
  ]
  node [
    id 435
    label "kalendarz"
  ]
  node [
    id 436
    label "cykl_astronomiczny"
  ]
  node [
    id 437
    label "lata"
  ]
  node [
    id 438
    label "pora_roku"
  ]
  node [
    id 439
    label "stulecie"
  ]
  node [
    id 440
    label "kurs"
  ]
  node [
    id 441
    label "jubileusz"
  ]
  node [
    id 442
    label "grupa"
  ]
  node [
    id 443
    label "kwarta&#322;"
  ]
  node [
    id 444
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 445
    label "zobo"
  ]
  node [
    id 446
    label "yakalo"
  ]
  node [
    id 447
    label "byd&#322;o"
  ]
  node [
    id 448
    label "dzo"
  ]
  node [
    id 449
    label "kr&#281;torogie"
  ]
  node [
    id 450
    label "g&#322;owa"
  ]
  node [
    id 451
    label "czochrad&#322;o"
  ]
  node [
    id 452
    label "posp&#243;lstwo"
  ]
  node [
    id 453
    label "kraal"
  ]
  node [
    id 454
    label "livestock"
  ]
  node [
    id 455
    label "prze&#380;uwacz"
  ]
  node [
    id 456
    label "bizon"
  ]
  node [
    id 457
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 458
    label "zebu"
  ]
  node [
    id 459
    label "byd&#322;o_domowe"
  ]
  node [
    id 460
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 461
    label "mie&#263;_miejsce"
  ]
  node [
    id 462
    label "equal"
  ]
  node [
    id 463
    label "trwa&#263;"
  ]
  node [
    id 464
    label "chodzi&#263;"
  ]
  node [
    id 465
    label "si&#281;ga&#263;"
  ]
  node [
    id 466
    label "stan"
  ]
  node [
    id 467
    label "obecno&#347;&#263;"
  ]
  node [
    id 468
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 469
    label "uczestniczy&#263;"
  ]
  node [
    id 470
    label "participate"
  ]
  node [
    id 471
    label "robi&#263;"
  ]
  node [
    id 472
    label "istnie&#263;"
  ]
  node [
    id 473
    label "pozostawa&#263;"
  ]
  node [
    id 474
    label "zostawa&#263;"
  ]
  node [
    id 475
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 476
    label "adhere"
  ]
  node [
    id 477
    label "compass"
  ]
  node [
    id 478
    label "korzysta&#263;"
  ]
  node [
    id 479
    label "appreciation"
  ]
  node [
    id 480
    label "osi&#261;ga&#263;"
  ]
  node [
    id 481
    label "get"
  ]
  node [
    id 482
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 483
    label "mierzy&#263;"
  ]
  node [
    id 484
    label "u&#380;ywa&#263;"
  ]
  node [
    id 485
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 486
    label "exsert"
  ]
  node [
    id 487
    label "being"
  ]
  node [
    id 488
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 489
    label "cecha"
  ]
  node [
    id 490
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 491
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 492
    label "p&#322;ywa&#263;"
  ]
  node [
    id 493
    label "run"
  ]
  node [
    id 494
    label "bangla&#263;"
  ]
  node [
    id 495
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 496
    label "przebiega&#263;"
  ]
  node [
    id 497
    label "wk&#322;ada&#263;"
  ]
  node [
    id 498
    label "proceed"
  ]
  node [
    id 499
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 500
    label "bywa&#263;"
  ]
  node [
    id 501
    label "dziama&#263;"
  ]
  node [
    id 502
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 503
    label "stara&#263;_si&#281;"
  ]
  node [
    id 504
    label "para"
  ]
  node [
    id 505
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 506
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 507
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 508
    label "krok"
  ]
  node [
    id 509
    label "tryb"
  ]
  node [
    id 510
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 511
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 512
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 513
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 514
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 515
    label "continue"
  ]
  node [
    id 516
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 517
    label "Ohio"
  ]
  node [
    id 518
    label "wci&#281;cie"
  ]
  node [
    id 519
    label "Nowy_York"
  ]
  node [
    id 520
    label "warstwa"
  ]
  node [
    id 521
    label "samopoczucie"
  ]
  node [
    id 522
    label "Illinois"
  ]
  node [
    id 523
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 524
    label "state"
  ]
  node [
    id 525
    label "Jukatan"
  ]
  node [
    id 526
    label "Kalifornia"
  ]
  node [
    id 527
    label "Wirginia"
  ]
  node [
    id 528
    label "wektor"
  ]
  node [
    id 529
    label "Goa"
  ]
  node [
    id 530
    label "Teksas"
  ]
  node [
    id 531
    label "Waszyngton"
  ]
  node [
    id 532
    label "miejsce"
  ]
  node [
    id 533
    label "Massachusetts"
  ]
  node [
    id 534
    label "Alaska"
  ]
  node [
    id 535
    label "Arakan"
  ]
  node [
    id 536
    label "Hawaje"
  ]
  node [
    id 537
    label "Maryland"
  ]
  node [
    id 538
    label "Michigan"
  ]
  node [
    id 539
    label "Arizona"
  ]
  node [
    id 540
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 541
    label "Georgia"
  ]
  node [
    id 542
    label "poziom"
  ]
  node [
    id 543
    label "Pensylwania"
  ]
  node [
    id 544
    label "Luizjana"
  ]
  node [
    id 545
    label "Nowy_Meksyk"
  ]
  node [
    id 546
    label "Alabama"
  ]
  node [
    id 547
    label "ilo&#347;&#263;"
  ]
  node [
    id 548
    label "Kansas"
  ]
  node [
    id 549
    label "Oregon"
  ]
  node [
    id 550
    label "Oklahoma"
  ]
  node [
    id 551
    label "Floryda"
  ]
  node [
    id 552
    label "jednostka_administracyjna"
  ]
  node [
    id 553
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 554
    label "po&#322;o&#380;enie"
  ]
  node [
    id 555
    label "wysiadka"
  ]
  node [
    id 556
    label "reverse"
  ]
  node [
    id 557
    label "gorzka_pigu&#322;ka"
  ]
  node [
    id 558
    label "przegra"
  ]
  node [
    id 559
    label "k&#322;adzenie"
  ]
  node [
    id 560
    label "niepowodzenie"
  ]
  node [
    id 561
    label "niefortunno&#347;&#263;"
  ]
  node [
    id 562
    label "rezultat"
  ]
  node [
    id 563
    label "lipa"
  ]
  node [
    id 564
    label "passa"
  ]
  node [
    id 565
    label "kuna"
  ]
  node [
    id 566
    label "siaja"
  ]
  node [
    id 567
    label "&#322;ub"
  ]
  node [
    id 568
    label "&#347;lazowate"
  ]
  node [
    id 569
    label "linden"
  ]
  node [
    id 570
    label "orzech"
  ]
  node [
    id 571
    label "nieudany"
  ]
  node [
    id 572
    label "drzewo"
  ]
  node [
    id 573
    label "k&#322;amstwo"
  ]
  node [
    id 574
    label "ro&#347;lina_miododajna"
  ]
  node [
    id 575
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 576
    label "&#347;ciema"
  ]
  node [
    id 577
    label "drewno"
  ]
  node [
    id 578
    label "lipowate"
  ]
  node [
    id 579
    label "ro&#347;lina"
  ]
  node [
    id 580
    label "baloney"
  ]
  node [
    id 581
    label "sytuacja"
  ]
  node [
    id 582
    label "visitation"
  ]
  node [
    id 583
    label "wydarzenie"
  ]
  node [
    id 584
    label "dzia&#322;anie"
  ]
  node [
    id 585
    label "typ"
  ]
  node [
    id 586
    label "event"
  ]
  node [
    id 587
    label "przyczyna"
  ]
  node [
    id 588
    label "kres"
  ]
  node [
    id 589
    label "lot"
  ]
  node [
    id 590
    label "przegraniec"
  ]
  node [
    id 591
    label "tragedia"
  ]
  node [
    id 592
    label "nieudacznik"
  ]
  node [
    id 593
    label "pszczo&#322;a"
  ]
  node [
    id 594
    label "przebieg"
  ]
  node [
    id 595
    label "continuum"
  ]
  node [
    id 596
    label "ci&#261;g&#322;o&#347;&#263;"
  ]
  node [
    id 597
    label "sukces"
  ]
  node [
    id 598
    label "ci&#261;g"
  ]
  node [
    id 599
    label "umieszczanie"
  ]
  node [
    id 600
    label "poszywanie"
  ]
  node [
    id 601
    label "powodowanie"
  ]
  node [
    id 602
    label "przyk&#322;adanie"
  ]
  node [
    id 603
    label "ubieranie"
  ]
  node [
    id 604
    label "disposal"
  ]
  node [
    id 605
    label "sk&#322;adanie"
  ]
  node [
    id 606
    label "psucie"
  ]
  node [
    id 607
    label "obk&#322;adanie"
  ]
  node [
    id 608
    label "zak&#322;adanie"
  ]
  node [
    id 609
    label "montowanie"
  ]
  node [
    id 610
    label "przenocowanie"
  ]
  node [
    id 611
    label "nak&#322;adzenie"
  ]
  node [
    id 612
    label "pouk&#322;adanie"
  ]
  node [
    id 613
    label "pokrycie"
  ]
  node [
    id 614
    label "zepsucie"
  ]
  node [
    id 615
    label "ustawienie"
  ]
  node [
    id 616
    label "spowodowanie"
  ]
  node [
    id 617
    label "trim"
  ]
  node [
    id 618
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 619
    label "ugoszczenie"
  ]
  node [
    id 620
    label "le&#380;enie"
  ]
  node [
    id 621
    label "adres"
  ]
  node [
    id 622
    label "zbudowanie"
  ]
  node [
    id 623
    label "umieszczenie"
  ]
  node [
    id 624
    label "reading"
  ]
  node [
    id 625
    label "czynno&#347;&#263;"
  ]
  node [
    id 626
    label "zabicie"
  ]
  node [
    id 627
    label "presentation"
  ]
  node [
    id 628
    label "le&#380;e&#263;"
  ]
  node [
    id 629
    label "kolejny"
  ]
  node [
    id 630
    label "niedawno"
  ]
  node [
    id 631
    label "poprzedni"
  ]
  node [
    id 632
    label "pozosta&#322;y"
  ]
  node [
    id 633
    label "ostatnio"
  ]
  node [
    id 634
    label "sko&#324;czony"
  ]
  node [
    id 635
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 636
    label "aktualny"
  ]
  node [
    id 637
    label "najgorszy"
  ]
  node [
    id 638
    label "istota_&#380;ywa"
  ]
  node [
    id 639
    label "w&#261;tpliwy"
  ]
  node [
    id 640
    label "nast&#281;pnie"
  ]
  node [
    id 641
    label "nastopny"
  ]
  node [
    id 642
    label "kolejno"
  ]
  node [
    id 643
    label "kt&#243;ry&#347;"
  ]
  node [
    id 644
    label "przesz&#322;y"
  ]
  node [
    id 645
    label "wcze&#347;niejszy"
  ]
  node [
    id 646
    label "poprzednio"
  ]
  node [
    id 647
    label "w&#261;tpliwie"
  ]
  node [
    id 648
    label "pozorny"
  ]
  node [
    id 649
    label "ostateczny"
  ]
  node [
    id 650
    label "wa&#380;ny"
  ]
  node [
    id 651
    label "ludzko&#347;&#263;"
  ]
  node [
    id 652
    label "asymilowanie"
  ]
  node [
    id 653
    label "asymilowa&#263;"
  ]
  node [
    id 654
    label "os&#322;abia&#263;"
  ]
  node [
    id 655
    label "posta&#263;"
  ]
  node [
    id 656
    label "hominid"
  ]
  node [
    id 657
    label "podw&#322;adny"
  ]
  node [
    id 658
    label "os&#322;abianie"
  ]
  node [
    id 659
    label "figura"
  ]
  node [
    id 660
    label "portrecista"
  ]
  node [
    id 661
    label "dwun&#243;g"
  ]
  node [
    id 662
    label "profanum"
  ]
  node [
    id 663
    label "mikrokosmos"
  ]
  node [
    id 664
    label "nasada"
  ]
  node [
    id 665
    label "duch"
  ]
  node [
    id 666
    label "antropochoria"
  ]
  node [
    id 667
    label "osoba"
  ]
  node [
    id 668
    label "wz&#243;r"
  ]
  node [
    id 669
    label "senior"
  ]
  node [
    id 670
    label "oddzia&#322;ywanie"
  ]
  node [
    id 671
    label "Adam"
  ]
  node [
    id 672
    label "homo_sapiens"
  ]
  node [
    id 673
    label "polifag"
  ]
  node [
    id 674
    label "wykszta&#322;cony"
  ]
  node [
    id 675
    label "dyplomowany"
  ]
  node [
    id 676
    label "wykwalifikowany"
  ]
  node [
    id 677
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 678
    label "sko&#324;czenie"
  ]
  node [
    id 679
    label "wielki"
  ]
  node [
    id 680
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 681
    label "aktualnie"
  ]
  node [
    id 682
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 683
    label "aktualizowanie"
  ]
  node [
    id 684
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 685
    label "uaktualnienie"
  ]
  node [
    id 686
    label "skrutator"
  ]
  node [
    id 687
    label "g&#322;osowanie"
  ]
  node [
    id 688
    label "reasumowa&#263;"
  ]
  node [
    id 689
    label "przeg&#322;osowanie"
  ]
  node [
    id 690
    label "reasumowanie"
  ]
  node [
    id 691
    label "akcja"
  ]
  node [
    id 692
    label "wybieranie"
  ]
  node [
    id 693
    label "poll"
  ]
  node [
    id 694
    label "vote"
  ]
  node [
    id 695
    label "decydowanie"
  ]
  node [
    id 696
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 697
    label "przeg&#322;osowywanie"
  ]
  node [
    id 698
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 699
    label "wi&#281;kszo&#347;&#263;_bezwzgl&#281;dna"
  ]
  node [
    id 700
    label "wybranie"
  ]
  node [
    id 701
    label "m&#261;&#380;_zaufania"
  ]
  node [
    id 702
    label "rewident"
  ]
  node [
    id 703
    label "ekscerpcja"
  ]
  node [
    id 704
    label "j&#281;zykowo"
  ]
  node [
    id 705
    label "wypowied&#378;"
  ]
  node [
    id 706
    label "redakcja"
  ]
  node [
    id 707
    label "pomini&#281;cie"
  ]
  node [
    id 708
    label "dzie&#322;o"
  ]
  node [
    id 709
    label "preparacja"
  ]
  node [
    id 710
    label "odmianka"
  ]
  node [
    id 711
    label "opu&#347;ci&#263;"
  ]
  node [
    id 712
    label "koniektura"
  ]
  node [
    id 713
    label "pisa&#263;"
  ]
  node [
    id 714
    label "obelga"
  ]
  node [
    id 715
    label "przedmiot"
  ]
  node [
    id 716
    label "p&#322;&#243;d"
  ]
  node [
    id 717
    label "work"
  ]
  node [
    id 718
    label "obrazowanie"
  ]
  node [
    id 719
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 720
    label "dorobek"
  ]
  node [
    id 721
    label "forma"
  ]
  node [
    id 722
    label "tre&#347;&#263;"
  ]
  node [
    id 723
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 724
    label "retrospektywa"
  ]
  node [
    id 725
    label "works"
  ]
  node [
    id 726
    label "creation"
  ]
  node [
    id 727
    label "tetralogia"
  ]
  node [
    id 728
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 729
    label "praca"
  ]
  node [
    id 730
    label "pos&#322;uchanie"
  ]
  node [
    id 731
    label "s&#261;d"
  ]
  node [
    id 732
    label "sparafrazowanie"
  ]
  node [
    id 733
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 734
    label "strawestowa&#263;"
  ]
  node [
    id 735
    label "sparafrazowa&#263;"
  ]
  node [
    id 736
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 737
    label "trawestowa&#263;"
  ]
  node [
    id 738
    label "sformu&#322;owanie"
  ]
  node [
    id 739
    label "parafrazowanie"
  ]
  node [
    id 740
    label "ozdobnik"
  ]
  node [
    id 741
    label "delimitacja"
  ]
  node [
    id 742
    label "parafrazowa&#263;"
  ]
  node [
    id 743
    label "stylizacja"
  ]
  node [
    id 744
    label "trawestowanie"
  ]
  node [
    id 745
    label "strawestowanie"
  ]
  node [
    id 746
    label "cholera"
  ]
  node [
    id 747
    label "ubliga"
  ]
  node [
    id 748
    label "niedorobek"
  ]
  node [
    id 749
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 750
    label "chuj"
  ]
  node [
    id 751
    label "bluzg"
  ]
  node [
    id 752
    label "wyzwisko"
  ]
  node [
    id 753
    label "indignation"
  ]
  node [
    id 754
    label "pies"
  ]
  node [
    id 755
    label "wrzuta"
  ]
  node [
    id 756
    label "chujowy"
  ]
  node [
    id 757
    label "krzywda"
  ]
  node [
    id 758
    label "szmata"
  ]
  node [
    id 759
    label "odmiana"
  ]
  node [
    id 760
    label "formu&#322;owa&#263;"
  ]
  node [
    id 761
    label "ozdabia&#263;"
  ]
  node [
    id 762
    label "stawia&#263;"
  ]
  node [
    id 763
    label "spell"
  ]
  node [
    id 764
    label "styl"
  ]
  node [
    id 765
    label "skryba"
  ]
  node [
    id 766
    label "read"
  ]
  node [
    id 767
    label "donosi&#263;"
  ]
  node [
    id 768
    label "code"
  ]
  node [
    id 769
    label "dysgrafia"
  ]
  node [
    id 770
    label "dysortografia"
  ]
  node [
    id 771
    label "tworzy&#263;"
  ]
  node [
    id 772
    label "prasa"
  ]
  node [
    id 773
    label "preparation"
  ]
  node [
    id 774
    label "proces_technologiczny"
  ]
  node [
    id 775
    label "uj&#281;cie"
  ]
  node [
    id 776
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 777
    label "pozostawi&#263;"
  ]
  node [
    id 778
    label "obni&#380;y&#263;"
  ]
  node [
    id 779
    label "zostawi&#263;"
  ]
  node [
    id 780
    label "przesta&#263;"
  ]
  node [
    id 781
    label "potani&#263;"
  ]
  node [
    id 782
    label "drop"
  ]
  node [
    id 783
    label "evacuate"
  ]
  node [
    id 784
    label "humiliate"
  ]
  node [
    id 785
    label "leave"
  ]
  node [
    id 786
    label "straci&#263;"
  ]
  node [
    id 787
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 788
    label "authorize"
  ]
  node [
    id 789
    label "omin&#261;&#263;"
  ]
  node [
    id 790
    label "przypuszczenie"
  ]
  node [
    id 791
    label "conjecture"
  ]
  node [
    id 792
    label "obr&#243;bka"
  ]
  node [
    id 793
    label "wniosek"
  ]
  node [
    id 794
    label "redaktor"
  ]
  node [
    id 795
    label "radio"
  ]
  node [
    id 796
    label "zesp&#243;&#322;"
  ]
  node [
    id 797
    label "siedziba"
  ]
  node [
    id 798
    label "composition"
  ]
  node [
    id 799
    label "wydawnictwo"
  ]
  node [
    id 800
    label "redaction"
  ]
  node [
    id 801
    label "telewizja"
  ]
  node [
    id 802
    label "wyb&#243;r"
  ]
  node [
    id 803
    label "dokumentacja"
  ]
  node [
    id 804
    label "komunikacyjnie"
  ]
  node [
    id 805
    label "ellipsis"
  ]
  node [
    id 806
    label "wykluczenie"
  ]
  node [
    id 807
    label "figura_my&#347;li"
  ]
  node [
    id 808
    label "zrobienie"
  ]
  node [
    id 809
    label "title"
  ]
  node [
    id 810
    label "nazwa&#263;"
  ]
  node [
    id 811
    label "broadcast"
  ]
  node [
    id 812
    label "nada&#263;"
  ]
  node [
    id 813
    label "okre&#347;li&#263;"
  ]
  node [
    id 814
    label "niegrzeczny"
  ]
  node [
    id 815
    label "olbrzymi"
  ]
  node [
    id 816
    label "niemoralny"
  ]
  node [
    id 817
    label "kurewski"
  ]
  node [
    id 818
    label "strasznie"
  ]
  node [
    id 819
    label "jebitny"
  ]
  node [
    id 820
    label "olbrzymio"
  ]
  node [
    id 821
    label "ogromnie"
  ]
  node [
    id 822
    label "z&#322;y"
  ]
  node [
    id 823
    label "nagannie"
  ]
  node [
    id 824
    label "niemoralnie"
  ]
  node [
    id 825
    label "nieprzyzwoity"
  ]
  node [
    id 826
    label "niezno&#347;ny"
  ]
  node [
    id 827
    label "niegrzecznie"
  ]
  node [
    id 828
    label "trudny"
  ]
  node [
    id 829
    label "niestosowny"
  ]
  node [
    id 830
    label "brzydal"
  ]
  node [
    id 831
    label "niepos&#322;uszny"
  ]
  node [
    id 832
    label "wulgarny"
  ]
  node [
    id 833
    label "kurewsko"
  ]
  node [
    id 834
    label "zdzirowaty"
  ]
  node [
    id 835
    label "przekl&#281;ty"
  ]
  node [
    id 836
    label "niesamowity"
  ]
  node [
    id 837
    label "okropno"
  ]
  node [
    id 838
    label "jak_cholera"
  ]
  node [
    id 839
    label "g&#243;ra"
  ]
  node [
    id 840
    label "bluzka"
  ]
  node [
    id 841
    label "brat"
  ]
  node [
    id 842
    label "sweter"
  ]
  node [
    id 843
    label "bli&#378;ni&#281;ta"
  ]
  node [
    id 844
    label "komplet"
  ]
  node [
    id 845
    label "semi-detached_house"
  ]
  node [
    id 846
    label "dom"
  ]
  node [
    id 847
    label "bli&#378;ni&#281;"
  ]
  node [
    id 848
    label "przelezienie"
  ]
  node [
    id 849
    label "&#347;piew"
  ]
  node [
    id 850
    label "Synaj"
  ]
  node [
    id 851
    label "Kreml"
  ]
  node [
    id 852
    label "d&#378;wi&#281;k"
  ]
  node [
    id 853
    label "kierunek"
  ]
  node [
    id 854
    label "wysoki"
  ]
  node [
    id 855
    label "element"
  ]
  node [
    id 856
    label "wzniesienie"
  ]
  node [
    id 857
    label "pi&#281;tro"
  ]
  node [
    id 858
    label "Ropa"
  ]
  node [
    id 859
    label "kupa"
  ]
  node [
    id 860
    label "przele&#378;&#263;"
  ]
  node [
    id 861
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 862
    label "karczek"
  ]
  node [
    id 863
    label "rami&#261;czko"
  ]
  node [
    id 864
    label "Jaworze"
  ]
  node [
    id 865
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 866
    label "lekcja"
  ]
  node [
    id 867
    label "ensemble"
  ]
  node [
    id 868
    label "klasa"
  ]
  node [
    id 869
    label "zestaw"
  ]
  node [
    id 870
    label "sw&#243;j"
  ]
  node [
    id 871
    label "r&#243;wniacha"
  ]
  node [
    id 872
    label "krewny"
  ]
  node [
    id 873
    label "bratanie_si&#281;"
  ]
  node [
    id 874
    label "rodze&#324;stwo"
  ]
  node [
    id 875
    label "br"
  ]
  node [
    id 876
    label "pobratymiec"
  ]
  node [
    id 877
    label "mnich"
  ]
  node [
    id 878
    label "zakon"
  ]
  node [
    id 879
    label "cz&#322;onek"
  ]
  node [
    id 880
    label "bractwo"
  ]
  node [
    id 881
    label "zbratanie_si&#281;"
  ]
  node [
    id 882
    label "wyznawca"
  ]
  node [
    id 883
    label "stryj"
  ]
  node [
    id 884
    label "zro&#347;lak"
  ]
  node [
    id 885
    label "dziecko"
  ]
  node [
    id 886
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 887
    label "rodzina"
  ]
  node [
    id 888
    label "substancja_mieszkaniowa"
  ]
  node [
    id 889
    label "instytucja"
  ]
  node [
    id 890
    label "dom_rodzinny"
  ]
  node [
    id 891
    label "budynek"
  ]
  node [
    id 892
    label "stead"
  ]
  node [
    id 893
    label "garderoba"
  ]
  node [
    id 894
    label "wiecha"
  ]
  node [
    id 895
    label "fratria"
  ]
  node [
    id 896
    label "saczek"
  ]
  node [
    id 897
    label "dzianina"
  ]
  node [
    id 898
    label "wieloraczki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 460
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 463
  ]
  edge [
    source 14
    target 464
  ]
  edge [
    source 14
    target 465
  ]
  edge [
    source 14
    target 466
  ]
  edge [
    source 14
    target 467
  ]
  edge [
    source 14
    target 322
  ]
  edge [
    source 14
    target 468
  ]
  edge [
    source 14
    target 469
  ]
  edge [
    source 14
    target 470
  ]
  edge [
    source 14
    target 471
  ]
  edge [
    source 14
    target 472
  ]
  edge [
    source 14
    target 473
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 476
  ]
  edge [
    source 14
    target 477
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 251
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 397
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 271
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 275
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 228
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 570
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 573
  ]
  edge [
    source 15
    target 574
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 576
  ]
  edge [
    source 15
    target 577
  ]
  edge [
    source 15
    target 578
  ]
  edge [
    source 15
    target 579
  ]
  edge [
    source 15
    target 580
  ]
  edge [
    source 15
    target 581
  ]
  edge [
    source 15
    target 582
  ]
  edge [
    source 15
    target 583
  ]
  edge [
    source 15
    target 584
  ]
  edge [
    source 15
    target 585
  ]
  edge [
    source 15
    target 586
  ]
  edge [
    source 15
    target 587
  ]
  edge [
    source 15
    target 489
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 588
  ]
  edge [
    source 15
    target 589
  ]
  edge [
    source 15
    target 590
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 303
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 629
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 632
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 635
  ]
  edge [
    source 17
    target 636
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 638
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 640
  ]
  edge [
    source 17
    target 71
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 17
    target 649
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 355
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 450
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 675
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 45
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 703
  ]
  edge [
    source 21
    target 704
  ]
  edge [
    source 21
    target 705
  ]
  edge [
    source 21
    target 706
  ]
  edge [
    source 21
    target 231
  ]
  edge [
    source 21
    target 707
  ]
  edge [
    source 21
    target 708
  ]
  edge [
    source 21
    target 709
  ]
  edge [
    source 21
    target 710
  ]
  edge [
    source 21
    target 711
  ]
  edge [
    source 21
    target 712
  ]
  edge [
    source 21
    target 713
  ]
  edge [
    source 21
    target 714
  ]
  edge [
    source 21
    target 715
  ]
  edge [
    source 21
    target 716
  ]
  edge [
    source 21
    target 717
  ]
  edge [
    source 21
    target 562
  ]
  edge [
    source 21
    target 718
  ]
  edge [
    source 21
    target 719
  ]
  edge [
    source 21
    target 720
  ]
  edge [
    source 21
    target 721
  ]
  edge [
    source 21
    target 722
  ]
  edge [
    source 21
    target 723
  ]
  edge [
    source 21
    target 724
  ]
  edge [
    source 21
    target 725
  ]
  edge [
    source 21
    target 726
  ]
  edge [
    source 21
    target 727
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 728
  ]
  edge [
    source 21
    target 729
  ]
  edge [
    source 21
    target 730
  ]
  edge [
    source 21
    target 731
  ]
  edge [
    source 21
    target 732
  ]
  edge [
    source 21
    target 733
  ]
  edge [
    source 21
    target 734
  ]
  edge [
    source 21
    target 735
  ]
  edge [
    source 21
    target 736
  ]
  edge [
    source 21
    target 737
  ]
  edge [
    source 21
    target 738
  ]
  edge [
    source 21
    target 739
  ]
  edge [
    source 21
    target 740
  ]
  edge [
    source 21
    target 741
  ]
  edge [
    source 21
    target 742
  ]
  edge [
    source 21
    target 743
  ]
  edge [
    source 21
    target 744
  ]
  edge [
    source 21
    target 745
  ]
  edge [
    source 21
    target 746
  ]
  edge [
    source 21
    target 747
  ]
  edge [
    source 21
    target 748
  ]
  edge [
    source 21
    target 749
  ]
  edge [
    source 21
    target 750
  ]
  edge [
    source 21
    target 751
  ]
  edge [
    source 21
    target 752
  ]
  edge [
    source 21
    target 753
  ]
  edge [
    source 21
    target 754
  ]
  edge [
    source 21
    target 755
  ]
  edge [
    source 21
    target 756
  ]
  edge [
    source 21
    target 757
  ]
  edge [
    source 21
    target 758
  ]
  edge [
    source 21
    target 759
  ]
  edge [
    source 21
    target 760
  ]
  edge [
    source 21
    target 761
  ]
  edge [
    source 21
    target 762
  ]
  edge [
    source 21
    target 763
  ]
  edge [
    source 21
    target 764
  ]
  edge [
    source 21
    target 765
  ]
  edge [
    source 21
    target 766
  ]
  edge [
    source 21
    target 767
  ]
  edge [
    source 21
    target 768
  ]
  edge [
    source 21
    target 769
  ]
  edge [
    source 21
    target 770
  ]
  edge [
    source 21
    target 771
  ]
  edge [
    source 21
    target 772
  ]
  edge [
    source 21
    target 773
  ]
  edge [
    source 21
    target 774
  ]
  edge [
    source 21
    target 775
  ]
  edge [
    source 21
    target 776
  ]
  edge [
    source 21
    target 777
  ]
  edge [
    source 21
    target 778
  ]
  edge [
    source 21
    target 779
  ]
  edge [
    source 21
    target 780
  ]
  edge [
    source 21
    target 781
  ]
  edge [
    source 21
    target 782
  ]
  edge [
    source 21
    target 783
  ]
  edge [
    source 21
    target 784
  ]
  edge [
    source 21
    target 785
  ]
  edge [
    source 21
    target 786
  ]
  edge [
    source 21
    target 787
  ]
  edge [
    source 21
    target 788
  ]
  edge [
    source 21
    target 789
  ]
  edge [
    source 21
    target 790
  ]
  edge [
    source 21
    target 791
  ]
  edge [
    source 21
    target 792
  ]
  edge [
    source 21
    target 793
  ]
  edge [
    source 21
    target 794
  ]
  edge [
    source 21
    target 795
  ]
  edge [
    source 21
    target 796
  ]
  edge [
    source 21
    target 797
  ]
  edge [
    source 21
    target 798
  ]
  edge [
    source 21
    target 799
  ]
  edge [
    source 21
    target 800
  ]
  edge [
    source 21
    target 801
  ]
  edge [
    source 21
    target 802
  ]
  edge [
    source 21
    target 803
  ]
  edge [
    source 21
    target 81
  ]
  edge [
    source 21
    target 804
  ]
  edge [
    source 21
    target 805
  ]
  edge [
    source 21
    target 806
  ]
  edge [
    source 21
    target 807
  ]
  edge [
    source 21
    target 808
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 809
  ]
  edge [
    source 23
    target 810
  ]
  edge [
    source 23
    target 811
  ]
  edge [
    source 23
    target 812
  ]
  edge [
    source 23
    target 813
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 814
  ]
  edge [
    source 24
    target 815
  ]
  edge [
    source 24
    target 816
  ]
  edge [
    source 24
    target 817
  ]
  edge [
    source 24
    target 818
  ]
  edge [
    source 24
    target 819
  ]
  edge [
    source 24
    target 820
  ]
  edge [
    source 24
    target 821
  ]
  edge [
    source 24
    target 822
  ]
  edge [
    source 24
    target 823
  ]
  edge [
    source 24
    target 824
  ]
  edge [
    source 24
    target 825
  ]
  edge [
    source 24
    target 826
  ]
  edge [
    source 24
    target 827
  ]
  edge [
    source 24
    target 828
  ]
  edge [
    source 24
    target 829
  ]
  edge [
    source 24
    target 830
  ]
  edge [
    source 24
    target 831
  ]
  edge [
    source 24
    target 832
  ]
  edge [
    source 24
    target 833
  ]
  edge [
    source 24
    target 834
  ]
  edge [
    source 24
    target 835
  ]
  edge [
    source 24
    target 836
  ]
  edge [
    source 24
    target 837
  ]
  edge [
    source 24
    target 838
  ]
  edge [
    source 25
    target 839
  ]
  edge [
    source 25
    target 840
  ]
  edge [
    source 25
    target 841
  ]
  edge [
    source 25
    target 842
  ]
  edge [
    source 25
    target 843
  ]
  edge [
    source 25
    target 844
  ]
  edge [
    source 25
    target 845
  ]
  edge [
    source 25
    target 846
  ]
  edge [
    source 25
    target 847
  ]
  edge [
    source 25
    target 715
  ]
  edge [
    source 25
    target 848
  ]
  edge [
    source 25
    target 849
  ]
  edge [
    source 25
    target 850
  ]
  edge [
    source 25
    target 851
  ]
  edge [
    source 25
    target 852
  ]
  edge [
    source 25
    target 853
  ]
  edge [
    source 25
    target 854
  ]
  edge [
    source 25
    target 855
  ]
  edge [
    source 25
    target 856
  ]
  edge [
    source 25
    target 442
  ]
  edge [
    source 25
    target 857
  ]
  edge [
    source 25
    target 858
  ]
  edge [
    source 25
    target 859
  ]
  edge [
    source 25
    target 860
  ]
  edge [
    source 25
    target 861
  ]
  edge [
    source 25
    target 862
  ]
  edge [
    source 25
    target 863
  ]
  edge [
    source 25
    target 864
  ]
  edge [
    source 25
    target 865
  ]
  edge [
    source 25
    target 866
  ]
  edge [
    source 25
    target 867
  ]
  edge [
    source 25
    target 868
  ]
  edge [
    source 25
    target 869
  ]
  edge [
    source 25
    target 870
  ]
  edge [
    source 25
    target 871
  ]
  edge [
    source 25
    target 144
  ]
  edge [
    source 25
    target 872
  ]
  edge [
    source 25
    target 873
  ]
  edge [
    source 25
    target 874
  ]
  edge [
    source 25
    target 875
  ]
  edge [
    source 25
    target 876
  ]
  edge [
    source 25
    target 877
  ]
  edge [
    source 25
    target 93
  ]
  edge [
    source 25
    target 80
  ]
  edge [
    source 25
    target 147
  ]
  edge [
    source 25
    target 878
  ]
  edge [
    source 25
    target 879
  ]
  edge [
    source 25
    target 880
  ]
  edge [
    source 25
    target 881
  ]
  edge [
    source 25
    target 882
  ]
  edge [
    source 25
    target 883
  ]
  edge [
    source 25
    target 884
  ]
  edge [
    source 25
    target 885
  ]
  edge [
    source 25
    target 886
  ]
  edge [
    source 25
    target 887
  ]
  edge [
    source 25
    target 888
  ]
  edge [
    source 25
    target 889
  ]
  edge [
    source 25
    target 797
  ]
  edge [
    source 25
    target 890
  ]
  edge [
    source 25
    target 891
  ]
  edge [
    source 25
    target 351
  ]
  edge [
    source 25
    target 367
  ]
  edge [
    source 25
    target 892
  ]
  edge [
    source 25
    target 893
  ]
  edge [
    source 25
    target 894
  ]
  edge [
    source 25
    target 895
  ]
  edge [
    source 25
    target 896
  ]
  edge [
    source 25
    target 897
  ]
  edge [
    source 25
    target 898
  ]
]
