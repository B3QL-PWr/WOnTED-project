graph [
  node [
    id 0
    label "prawilno"
    origin "text"
  ]
  node [
    id 1
    label "przypomina&#263;"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "jeden"
    origin "text"
  ]
  node [
    id 4
    label "argument"
    origin "text"
  ]
  node [
    id 5
    label "zakaz"
    origin "text"
  ]
  node [
    id 6
    label "handel"
    origin "text"
  ]
  node [
    id 7
    label "niedziela"
    origin "text"
  ]
  node [
    id 8
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 9
    label "kto"
    origin "text"
  ]
  node [
    id 10
    label "popiera&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "sprzeciwia&#263;"
    origin "text"
  ]
  node [
    id 13
    label "ryj"
    origin "text"
  ]
  node [
    id 14
    label "za&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 15
    label "obrazek"
    origin "text"
  ]
  node [
    id 16
    label "informowa&#263;"
  ]
  node [
    id 17
    label "recall"
  ]
  node [
    id 18
    label "prompt"
  ]
  node [
    id 19
    label "u&#347;wiadamia&#263;"
  ]
  node [
    id 20
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 21
    label "explain"
  ]
  node [
    id 22
    label "powiada&#263;"
  ]
  node [
    id 23
    label "komunikowa&#263;"
  ]
  node [
    id 24
    label "inform"
  ]
  node [
    id 25
    label "m&#261;&#380;"
  ]
  node [
    id 26
    label "czyj&#347;"
  ]
  node [
    id 27
    label "prywatny"
  ]
  node [
    id 28
    label "pan_i_w&#322;adca"
  ]
  node [
    id 29
    label "cz&#322;owiek"
  ]
  node [
    id 30
    label "pan_m&#322;ody"
  ]
  node [
    id 31
    label "ch&#322;op"
  ]
  node [
    id 32
    label "&#347;lubny"
  ]
  node [
    id 33
    label "pan_domu"
  ]
  node [
    id 34
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 35
    label "ma&#322;&#380;onek"
  ]
  node [
    id 36
    label "stary"
  ]
  node [
    id 37
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 38
    label "kieliszek"
  ]
  node [
    id 39
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 40
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 41
    label "w&#243;dka"
  ]
  node [
    id 42
    label "ujednolicenie"
  ]
  node [
    id 43
    label "ten"
  ]
  node [
    id 44
    label "jaki&#347;"
  ]
  node [
    id 45
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 46
    label "jednakowy"
  ]
  node [
    id 47
    label "jednolicie"
  ]
  node [
    id 48
    label "shot"
  ]
  node [
    id 49
    label "naczynie"
  ]
  node [
    id 50
    label "zawarto&#347;&#263;"
  ]
  node [
    id 51
    label "szk&#322;o"
  ]
  node [
    id 52
    label "mohorycz"
  ]
  node [
    id 53
    label "gorza&#322;ka"
  ]
  node [
    id 54
    label "alkohol"
  ]
  node [
    id 55
    label "sznaps"
  ]
  node [
    id 56
    label "nap&#243;j"
  ]
  node [
    id 57
    label "taki&#380;"
  ]
  node [
    id 58
    label "identyczny"
  ]
  node [
    id 59
    label "zr&#243;wnanie"
  ]
  node [
    id 60
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 61
    label "zr&#243;wnywanie"
  ]
  node [
    id 62
    label "mundurowanie"
  ]
  node [
    id 63
    label "mundurowa&#263;"
  ]
  node [
    id 64
    label "jednakowo"
  ]
  node [
    id 65
    label "okre&#347;lony"
  ]
  node [
    id 66
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 67
    label "z&#322;o&#380;ony"
  ]
  node [
    id 68
    label "jako&#347;"
  ]
  node [
    id 69
    label "niez&#322;y"
  ]
  node [
    id 70
    label "charakterystyczny"
  ]
  node [
    id 71
    label "jako_tako"
  ]
  node [
    id 72
    label "ciekawy"
  ]
  node [
    id 73
    label "dziwny"
  ]
  node [
    id 74
    label "przyzwoity"
  ]
  node [
    id 75
    label "g&#322;&#281;bszy"
  ]
  node [
    id 76
    label "drink"
  ]
  node [
    id 77
    label "jednolity"
  ]
  node [
    id 78
    label "upodobnienie"
  ]
  node [
    id 79
    label "calibration"
  ]
  node [
    id 80
    label "rzecz"
  ]
  node [
    id 81
    label "argumentacja"
  ]
  node [
    id 82
    label "zmienna"
  ]
  node [
    id 83
    label "operand"
  ]
  node [
    id 84
    label "parametr"
  ]
  node [
    id 85
    label "s&#261;d"
  ]
  node [
    id 86
    label "dow&#243;d"
  ]
  node [
    id 87
    label "wielko&#347;&#263;"
  ]
  node [
    id 88
    label "charakterystyka"
  ]
  node [
    id 89
    label "wymiar"
  ]
  node [
    id 90
    label "istota"
  ]
  node [
    id 91
    label "przedmiot"
  ]
  node [
    id 92
    label "wpada&#263;"
  ]
  node [
    id 93
    label "object"
  ]
  node [
    id 94
    label "przyroda"
  ]
  node [
    id 95
    label "wpa&#347;&#263;"
  ]
  node [
    id 96
    label "kultura"
  ]
  node [
    id 97
    label "mienie"
  ]
  node [
    id 98
    label "obiekt"
  ]
  node [
    id 99
    label "temat"
  ]
  node [
    id 100
    label "wpadni&#281;cie"
  ]
  node [
    id 101
    label "wpadanie"
  ]
  node [
    id 102
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 103
    label "forum"
  ]
  node [
    id 104
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 105
    label "s&#261;downictwo"
  ]
  node [
    id 106
    label "wydarzenie"
  ]
  node [
    id 107
    label "podejrzany"
  ]
  node [
    id 108
    label "&#347;wiadek"
  ]
  node [
    id 109
    label "instytucja"
  ]
  node [
    id 110
    label "biuro"
  ]
  node [
    id 111
    label "post&#281;powanie"
  ]
  node [
    id 112
    label "court"
  ]
  node [
    id 113
    label "my&#347;l"
  ]
  node [
    id 114
    label "obrona"
  ]
  node [
    id 115
    label "system"
  ]
  node [
    id 116
    label "broni&#263;"
  ]
  node [
    id 117
    label "antylogizm"
  ]
  node [
    id 118
    label "strona"
  ]
  node [
    id 119
    label "oskar&#380;yciel"
  ]
  node [
    id 120
    label "urz&#261;d"
  ]
  node [
    id 121
    label "skazany"
  ]
  node [
    id 122
    label "konektyw"
  ]
  node [
    id 123
    label "wypowied&#378;"
  ]
  node [
    id 124
    label "bronienie"
  ]
  node [
    id 125
    label "wytw&#243;r"
  ]
  node [
    id 126
    label "pods&#261;dny"
  ]
  node [
    id 127
    label "zesp&#243;&#322;"
  ]
  node [
    id 128
    label "procesowicz"
  ]
  node [
    id 129
    label "warto&#347;&#263;"
  ]
  node [
    id 130
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 131
    label "variable"
  ]
  node [
    id 132
    label "uzasadnienie"
  ]
  node [
    id 133
    label "komunikacja"
  ]
  node [
    id 134
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 135
    label "transformata"
  ]
  node [
    id 136
    label "certificate"
  ]
  node [
    id 137
    label "&#347;rodek"
  ]
  node [
    id 138
    label "act"
  ]
  node [
    id 139
    label "rewizja"
  ]
  node [
    id 140
    label "forsing"
  ]
  node [
    id 141
    label "dokument"
  ]
  node [
    id 142
    label "rozporz&#261;dzenie"
  ]
  node [
    id 143
    label "polecenie"
  ]
  node [
    id 144
    label "consign"
  ]
  node [
    id 145
    label "pognanie"
  ]
  node [
    id 146
    label "recommendation"
  ]
  node [
    id 147
    label "pobiegni&#281;cie"
  ]
  node [
    id 148
    label "powierzenie"
  ]
  node [
    id 149
    label "doradzenie"
  ]
  node [
    id 150
    label "education"
  ]
  node [
    id 151
    label "zaordynowanie"
  ]
  node [
    id 152
    label "przesadzenie"
  ]
  node [
    id 153
    label "ukaz"
  ]
  node [
    id 154
    label "rekomendacja"
  ]
  node [
    id 155
    label "statement"
  ]
  node [
    id 156
    label "zadanie"
  ]
  node [
    id 157
    label "akt"
  ]
  node [
    id 158
    label "ordonans"
  ]
  node [
    id 159
    label "zarz&#261;dzenie"
  ]
  node [
    id 160
    label "rule"
  ]
  node [
    id 161
    label "arrangement"
  ]
  node [
    id 162
    label "commission"
  ]
  node [
    id 163
    label "komercja"
  ]
  node [
    id 164
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 165
    label "popyt"
  ]
  node [
    id 166
    label "business"
  ]
  node [
    id 167
    label "proces_ekonomiczny"
  ]
  node [
    id 168
    label "popyt_zagregowany"
  ]
  node [
    id 169
    label "elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 170
    label "mechanizm_mno&#380;nikowy"
  ]
  node [
    id 171
    label "lepko&#347;&#263;_cen"
  ]
  node [
    id 172
    label "krzywa_popytu"
  ]
  node [
    id 173
    label "cena_r&#243;wnowagi_rynkowej"
  ]
  node [
    id 174
    label "czynnik_niecenowy"
  ]
  node [
    id 175
    label "nawis_inflacyjny"
  ]
  node [
    id 176
    label "szmira"
  ]
  node [
    id 177
    label "commerce"
  ]
  node [
    id 178
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 179
    label "zasoby_ludzkie"
  ]
  node [
    id 180
    label "Orlen"
  ]
  node [
    id 181
    label "MAN_SE"
  ]
  node [
    id 182
    label "Spo&#322;em"
  ]
  node [
    id 183
    label "MAC"
  ]
  node [
    id 184
    label "Baltona"
  ]
  node [
    id 185
    label "podmiot_gospodarczy"
  ]
  node [
    id 186
    label "networking"
  ]
  node [
    id 187
    label "Pewex"
  ]
  node [
    id 188
    label "Orbis"
  ]
  node [
    id 189
    label "Canon"
  ]
  node [
    id 190
    label "HP"
  ]
  node [
    id 191
    label "Google"
  ]
  node [
    id 192
    label "interes"
  ]
  node [
    id 193
    label "Hortex"
  ]
  node [
    id 194
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 195
    label "Apeks"
  ]
  node [
    id 196
    label "reengineering"
  ]
  node [
    id 197
    label "zasoby"
  ]
  node [
    id 198
    label "tydzie&#324;"
  ]
  node [
    id 199
    label "bia&#322;a_niedziela"
  ]
  node [
    id 200
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 201
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 202
    label "Niedziela_Palmowa"
  ]
  node [
    id 203
    label "weekend"
  ]
  node [
    id 204
    label "Wielkanoc"
  ]
  node [
    id 205
    label "niedziela_przewodnia"
  ]
  node [
    id 206
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 207
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 208
    label "doba"
  ]
  node [
    id 209
    label "miesi&#261;c"
  ]
  node [
    id 210
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 211
    label "czas"
  ]
  node [
    id 212
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 213
    label "rezurekcja"
  ]
  node [
    id 214
    label "wiosna"
  ]
  node [
    id 215
    label "&#347;niadanie_wielkanocne"
  ]
  node [
    id 216
    label "Poniedzia&#322;ek_Wielkonocny"
  ]
  node [
    id 217
    label "sobota"
  ]
  node [
    id 218
    label "uzasadnia&#263;"
  ]
  node [
    id 219
    label "unbosom"
  ]
  node [
    id 220
    label "pomaga&#263;"
  ]
  node [
    id 221
    label "robi&#263;"
  ]
  node [
    id 222
    label "u&#322;atwia&#263;"
  ]
  node [
    id 223
    label "back"
  ]
  node [
    id 224
    label "powodowa&#263;"
  ]
  node [
    id 225
    label "digest"
  ]
  node [
    id 226
    label "skutkowa&#263;"
  ]
  node [
    id 227
    label "sprzyja&#263;"
  ]
  node [
    id 228
    label "concur"
  ]
  node [
    id 229
    label "Warszawa"
  ]
  node [
    id 230
    label "mie&#263;_miejsce"
  ]
  node [
    id 231
    label "aid"
  ]
  node [
    id 232
    label "morda"
  ]
  node [
    id 233
    label "dzi&#243;b"
  ]
  node [
    id 234
    label "usta"
  ]
  node [
    id 235
    label "&#347;winia"
  ]
  node [
    id 236
    label "twarz"
  ]
  node [
    id 237
    label "wyraz_twarzy"
  ]
  node [
    id 238
    label "p&#243;&#322;profil"
  ]
  node [
    id 239
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 240
    label "rys"
  ]
  node [
    id 241
    label "brew"
  ]
  node [
    id 242
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 243
    label "micha"
  ]
  node [
    id 244
    label "ucho"
  ]
  node [
    id 245
    label "profil"
  ]
  node [
    id 246
    label "podbr&#243;dek"
  ]
  node [
    id 247
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 248
    label "policzek"
  ]
  node [
    id 249
    label "posta&#263;"
  ]
  node [
    id 250
    label "cera"
  ]
  node [
    id 251
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 252
    label "czo&#322;o"
  ]
  node [
    id 253
    label "oko"
  ]
  node [
    id 254
    label "uj&#281;cie"
  ]
  node [
    id 255
    label "maskowato&#347;&#263;"
  ]
  node [
    id 256
    label "powieka"
  ]
  node [
    id 257
    label "nos"
  ]
  node [
    id 258
    label "prz&#243;d"
  ]
  node [
    id 259
    label "zas&#322;ona"
  ]
  node [
    id 260
    label "twarzyczka"
  ]
  node [
    id 261
    label "pysk"
  ]
  node [
    id 262
    label "reputacja"
  ]
  node [
    id 263
    label "liczko"
  ]
  node [
    id 264
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 265
    label "przedstawiciel"
  ]
  node [
    id 266
    label "p&#322;e&#263;"
  ]
  node [
    id 267
    label "ssa&#263;"
  ]
  node [
    id 268
    label "warga_dolna"
  ]
  node [
    id 269
    label "zaci&#281;cie"
  ]
  node [
    id 270
    label "zacinanie"
  ]
  node [
    id 271
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 272
    label "ryjek"
  ]
  node [
    id 273
    label "warga_g&#243;rna"
  ]
  node [
    id 274
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 275
    label "zacina&#263;"
  ]
  node [
    id 276
    label "jama_ustna"
  ]
  node [
    id 277
    label "jadaczka"
  ]
  node [
    id 278
    label "zaci&#261;&#263;"
  ]
  node [
    id 279
    label "organ"
  ]
  node [
    id 280
    label "ssanie"
  ]
  node [
    id 281
    label "statek"
  ]
  node [
    id 282
    label "zako&#324;czenie"
  ]
  node [
    id 283
    label "bow"
  ]
  node [
    id 284
    label "struktura_anatomiczna"
  ]
  node [
    id 285
    label "ustnik"
  ]
  node [
    id 286
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 287
    label "blizna"
  ]
  node [
    id 288
    label "dziob&#243;wka"
  ]
  node [
    id 289
    label "grzebie&#324;"
  ]
  node [
    id 290
    label "samolot"
  ]
  node [
    id 291
    label "ptak"
  ]
  node [
    id 292
    label "ostry"
  ]
  node [
    id 293
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 294
    label "&#322;eb"
  ]
  node [
    id 295
    label "chrz&#261;kanie"
  ]
  node [
    id 296
    label "kwiczenie"
  ]
  node [
    id 297
    label "szczecina"
  ]
  node [
    id 298
    label "zakwicze&#263;"
  ]
  node [
    id 299
    label "czerwone_mi&#281;so"
  ]
  node [
    id 300
    label "trzoda_chlewna"
  ]
  node [
    id 301
    label "chrz&#261;ka&#263;"
  ]
  node [
    id 302
    label "ssak_parzystokopytny"
  ]
  node [
    id 303
    label "kwicze&#263;"
  ]
  node [
    id 304
    label "wszystko&#380;erca"
  ]
  node [
    id 305
    label "paskudziarz"
  ]
  node [
    id 306
    label "mi&#281;so"
  ]
  node [
    id 307
    label "&#347;winiowate"
  ]
  node [
    id 308
    label "zachrz&#261;ka&#263;"
  ]
  node [
    id 309
    label "zwierz&#281;_gospodarskie"
  ]
  node [
    id 310
    label "kwikni&#281;cie"
  ]
  node [
    id 311
    label "chrz&#261;kni&#281;cie"
  ]
  node [
    id 312
    label "&#347;winiak"
  ]
  node [
    id 313
    label "kaban"
  ]
  node [
    id 314
    label "bydl&#281;"
  ]
  node [
    id 315
    label "annex"
  ]
  node [
    id 316
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 317
    label "zrobi&#263;"
  ]
  node [
    id 318
    label "catch"
  ]
  node [
    id 319
    label "spowodowa&#263;"
  ]
  node [
    id 320
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 321
    label "articulation"
  ]
  node [
    id 322
    label "dokoptowa&#263;"
  ]
  node [
    id 323
    label "picture"
  ]
  node [
    id 324
    label "opowiadanie"
  ]
  node [
    id 325
    label "druk_ulotny"
  ]
  node [
    id 326
    label "rysunek"
  ]
  node [
    id 327
    label "grafika"
  ]
  node [
    id 328
    label "ilustracja"
  ]
  node [
    id 329
    label "plastyka"
  ]
  node [
    id 330
    label "kreska"
  ]
  node [
    id 331
    label "shape"
  ]
  node [
    id 332
    label "teka"
  ]
  node [
    id 333
    label "kszta&#322;t"
  ]
  node [
    id 334
    label "photograph"
  ]
  node [
    id 335
    label "rozpowiedzenie"
  ]
  node [
    id 336
    label "report"
  ]
  node [
    id 337
    label "spalenie"
  ]
  node [
    id 338
    label "story"
  ]
  node [
    id 339
    label "podbarwianie"
  ]
  node [
    id 340
    label "rozpowiadanie"
  ]
  node [
    id 341
    label "utw&#243;r_epicki"
  ]
  node [
    id 342
    label "proza"
  ]
  node [
    id 343
    label "follow-up"
  ]
  node [
    id 344
    label "prawienie"
  ]
  node [
    id 345
    label "przedstawianie"
  ]
  node [
    id 346
    label "fabu&#322;a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 13
    target 233
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 235
  ]
  edge [
    source 13
    target 236
  ]
  edge [
    source 13
    target 237
  ]
  edge [
    source 13
    target 238
  ]
  edge [
    source 13
    target 239
  ]
  edge [
    source 13
    target 240
  ]
  edge [
    source 13
    target 241
  ]
  edge [
    source 13
    target 242
  ]
  edge [
    source 13
    target 243
  ]
  edge [
    source 13
    target 244
  ]
  edge [
    source 13
    target 245
  ]
  edge [
    source 13
    target 246
  ]
  edge [
    source 13
    target 247
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 249
  ]
  edge [
    source 13
    target 250
  ]
  edge [
    source 13
    target 29
  ]
  edge [
    source 13
    target 251
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 13
    target 256
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 259
  ]
  edge [
    source 13
    target 260
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 262
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 264
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 269
  ]
  edge [
    source 13
    target 270
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 274
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 276
  ]
  edge [
    source 13
    target 277
  ]
  edge [
    source 13
    target 278
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 281
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 13
    target 284
  ]
  edge [
    source 13
    target 285
  ]
  edge [
    source 13
    target 286
  ]
  edge [
    source 13
    target 287
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 289
  ]
  edge [
    source 13
    target 290
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 292
  ]
  edge [
    source 13
    target 293
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 295
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 297
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 299
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 301
  ]
  edge [
    source 13
    target 302
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 314
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 315
  ]
  edge [
    source 14
    target 316
  ]
  edge [
    source 14
    target 317
  ]
  edge [
    source 14
    target 318
  ]
  edge [
    source 14
    target 319
  ]
  edge [
    source 14
    target 320
  ]
  edge [
    source 14
    target 321
  ]
  edge [
    source 14
    target 322
  ]
  edge [
    source 15
    target 323
  ]
  edge [
    source 15
    target 324
  ]
  edge [
    source 15
    target 325
  ]
  edge [
    source 15
    target 326
  ]
  edge [
    source 15
    target 327
  ]
  edge [
    source 15
    target 328
  ]
  edge [
    source 15
    target 329
  ]
  edge [
    source 15
    target 330
  ]
  edge [
    source 15
    target 331
  ]
  edge [
    source 15
    target 332
  ]
  edge [
    source 15
    target 333
  ]
  edge [
    source 15
    target 334
  ]
  edge [
    source 15
    target 335
  ]
  edge [
    source 15
    target 336
  ]
  edge [
    source 15
    target 337
  ]
  edge [
    source 15
    target 338
  ]
  edge [
    source 15
    target 339
  ]
  edge [
    source 15
    target 340
  ]
  edge [
    source 15
    target 341
  ]
  edge [
    source 15
    target 342
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 343
  ]
  edge [
    source 15
    target 344
  ]
  edge [
    source 15
    target 345
  ]
  edge [
    source 15
    target 346
  ]
]
