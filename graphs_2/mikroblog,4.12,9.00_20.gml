graph [
  node [
    id 0
    label "twierdza"
    origin "text"
  ]
  node [
    id 1
    label "heheszki"
    origin "text"
  ]
  node [
    id 2
    label "Dyjament"
  ]
  node [
    id 3
    label "Brenna"
  ]
  node [
    id 4
    label "flanka"
  ]
  node [
    id 5
    label "bastion"
  ]
  node [
    id 6
    label "budowla"
  ]
  node [
    id 7
    label "schronienie"
  ]
  node [
    id 8
    label "Szlisselburg"
  ]
  node [
    id 9
    label "bezpieczny"
  ]
  node [
    id 10
    label "ukryty"
  ]
  node [
    id 11
    label "cover"
  ]
  node [
    id 12
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 13
    label "ukrycie"
  ]
  node [
    id 14
    label "miejsce"
  ]
  node [
    id 15
    label "obudowanie"
  ]
  node [
    id 16
    label "obudowywa&#263;"
  ]
  node [
    id 17
    label "zbudowa&#263;"
  ]
  node [
    id 18
    label "obudowa&#263;"
  ]
  node [
    id 19
    label "kolumnada"
  ]
  node [
    id 20
    label "korpus"
  ]
  node [
    id 21
    label "Sukiennice"
  ]
  node [
    id 22
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 23
    label "fundament"
  ]
  node [
    id 24
    label "obudowywanie"
  ]
  node [
    id 25
    label "postanie"
  ]
  node [
    id 26
    label "zbudowanie"
  ]
  node [
    id 27
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 28
    label "stan_surowy"
  ]
  node [
    id 29
    label "konstrukcja"
  ]
  node [
    id 30
    label "rzecz"
  ]
  node [
    id 31
    label "basteja"
  ]
  node [
    id 32
    label "ostoja"
  ]
  node [
    id 33
    label "kurtyna"
  ]
  node [
    id 34
    label "fortyfikacja"
  ]
  node [
    id 35
    label "dzie&#322;o_koronowe"
  ]
  node [
    id 36
    label "narys_bastionowy"
  ]
  node [
    id 37
    label "boisko"
  ]
  node [
    id 38
    label "ugrupowanie"
  ]
  node [
    id 39
    label "bok"
  ]
  node [
    id 40
    label "Brandenburg"
  ]
  node [
    id 41
    label "Ryga"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
]
