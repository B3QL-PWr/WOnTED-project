graph [
  node [
    id 0
    label "raz"
    origin "text"
  ]
  node [
    id 1
    label "konieczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "szpital"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "szpitalny"
    origin "text"
  ]
  node [
    id 7
    label "oddzia&#322;"
    origin "text"
  ]
  node [
    id 8
    label "ratunkowy"
    origin "text"
  ]
  node [
    id 9
    label "lub"
    origin "text"
  ]
  node [
    id 10
    label "jednostka"
    origin "text"
  ]
  node [
    id 11
    label "organizacyjny"
    origin "text"
  ]
  node [
    id 12
    label "wyspecjalizowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zakres"
    origin "text"
  ]
  node [
    id 14
    label "udziela&#263;"
    origin "text"
  ]
  node [
    id 15
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 16
    label "zdrowotny"
    origin "text"
  ]
  node [
    id 17
    label "niezb&#281;dny"
    origin "text"
  ]
  node [
    id 18
    label "dla"
    origin "text"
  ]
  node [
    id 19
    label "ratownictwo"
    origin "text"
  ]
  node [
    id 20
    label "medyczny"
    origin "text"
  ]
  node [
    id 21
    label "zapewnia&#263;"
    origin "text"
  ]
  node [
    id 22
    label "niezw&#322;oczny"
    origin "text"
  ]
  node [
    id 23
    label "transport"
    origin "text"
  ]
  node [
    id 24
    label "sanitarny"
    origin "text"
  ]
  node [
    id 25
    label "osoba"
    origin "text"
  ]
  node [
    id 26
    label "stan"
    origin "text"
  ]
  node [
    id 27
    label "nag&#322;y"
    origin "text"
  ]
  node [
    id 28
    label "zagro&#380;enie"
    origin "text"
  ]
  node [
    id 29
    label "bliski"
    origin "text"
  ]
  node [
    id 30
    label "zak&#322;ad"
    origin "text"
  ]
  node [
    id 31
    label "opieka"
    origin "text"
  ]
  node [
    id 32
    label "odpowiedni"
    origin "text"
  ]
  node [
    id 33
    label "time"
  ]
  node [
    id 34
    label "cios"
  ]
  node [
    id 35
    label "chwila"
  ]
  node [
    id 36
    label "uderzenie"
  ]
  node [
    id 37
    label "blok"
  ]
  node [
    id 38
    label "shot"
  ]
  node [
    id 39
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 40
    label "struktura_geologiczna"
  ]
  node [
    id 41
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 42
    label "pr&#243;ba"
  ]
  node [
    id 43
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 44
    label "coup"
  ]
  node [
    id 45
    label "siekacz"
  ]
  node [
    id 46
    label "instrumentalizacja"
  ]
  node [
    id 47
    label "trafienie"
  ]
  node [
    id 48
    label "walka"
  ]
  node [
    id 49
    label "zdarzenie_si&#281;"
  ]
  node [
    id 50
    label "wdarcie_si&#281;"
  ]
  node [
    id 51
    label "pogorszenie"
  ]
  node [
    id 52
    label "d&#378;wi&#281;k"
  ]
  node [
    id 53
    label "poczucie"
  ]
  node [
    id 54
    label "reakcja"
  ]
  node [
    id 55
    label "contact"
  ]
  node [
    id 56
    label "stukni&#281;cie"
  ]
  node [
    id 57
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 58
    label "bat"
  ]
  node [
    id 59
    label "spowodowanie"
  ]
  node [
    id 60
    label "rush"
  ]
  node [
    id 61
    label "odbicie"
  ]
  node [
    id 62
    label "dawka"
  ]
  node [
    id 63
    label "zadanie"
  ]
  node [
    id 64
    label "&#347;ci&#281;cie"
  ]
  node [
    id 65
    label "st&#322;uczenie"
  ]
  node [
    id 66
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 67
    label "odbicie_si&#281;"
  ]
  node [
    id 68
    label "dotkni&#281;cie"
  ]
  node [
    id 69
    label "charge"
  ]
  node [
    id 70
    label "dostanie"
  ]
  node [
    id 71
    label "skrytykowanie"
  ]
  node [
    id 72
    label "zagrywka"
  ]
  node [
    id 73
    label "manewr"
  ]
  node [
    id 74
    label "nast&#261;pienie"
  ]
  node [
    id 75
    label "uderzanie"
  ]
  node [
    id 76
    label "pogoda"
  ]
  node [
    id 77
    label "stroke"
  ]
  node [
    id 78
    label "pobicie"
  ]
  node [
    id 79
    label "ruch"
  ]
  node [
    id 80
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 81
    label "flap"
  ]
  node [
    id 82
    label "dotyk"
  ]
  node [
    id 83
    label "zrobienie"
  ]
  node [
    id 84
    label "czas"
  ]
  node [
    id 85
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 86
    label "przymus"
  ]
  node [
    id 87
    label "wym&#243;g"
  ]
  node [
    id 88
    label "obligatoryjno&#347;&#263;"
  ]
  node [
    id 89
    label "operator_modalny"
  ]
  node [
    id 90
    label "condition"
  ]
  node [
    id 91
    label "potrzeba"
  ]
  node [
    id 92
    label "need"
  ]
  node [
    id 93
    label "umowa"
  ]
  node [
    id 94
    label "presja"
  ]
  node [
    id 95
    label "obowi&#261;zkowo&#347;&#263;"
  ]
  node [
    id 96
    label "sk&#322;adnik"
  ]
  node [
    id 97
    label "warunki"
  ]
  node [
    id 98
    label "sytuacja"
  ]
  node [
    id 99
    label "wydarzenie"
  ]
  node [
    id 100
    label "centrum_urazowe"
  ]
  node [
    id 101
    label "kostnica"
  ]
  node [
    id 102
    label "izba_chorych"
  ]
  node [
    id 103
    label "instytucja"
  ]
  node [
    id 104
    label "oddzia&#322;_septyczny"
  ]
  node [
    id 105
    label "klinicysta"
  ]
  node [
    id 106
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 107
    label "blok_operacyjny"
  ]
  node [
    id 108
    label "zabieg&#243;wka"
  ]
  node [
    id 109
    label "sala_chorych"
  ]
  node [
    id 110
    label "&#322;&#243;&#380;eczko_nadziei"
  ]
  node [
    id 111
    label "szpitalnictwo"
  ]
  node [
    id 112
    label "osoba_prawna"
  ]
  node [
    id 113
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 114
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 115
    label "poj&#281;cie"
  ]
  node [
    id 116
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 117
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 118
    label "biuro"
  ]
  node [
    id 119
    label "organizacja"
  ]
  node [
    id 120
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 121
    label "Fundusze_Unijne"
  ]
  node [
    id 122
    label "zamyka&#263;"
  ]
  node [
    id 123
    label "establishment"
  ]
  node [
    id 124
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 125
    label "urz&#261;d"
  ]
  node [
    id 126
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 127
    label "afiliowa&#263;"
  ]
  node [
    id 128
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 129
    label "standard"
  ]
  node [
    id 130
    label "zamykanie"
  ]
  node [
    id 131
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 132
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 133
    label "trupiarnia"
  ]
  node [
    id 134
    label "zesp&#243;&#322;"
  ]
  node [
    id 135
    label "dzia&#322;"
  ]
  node [
    id 136
    label "system"
  ]
  node [
    id 137
    label "lias"
  ]
  node [
    id 138
    label "pi&#281;tro"
  ]
  node [
    id 139
    label "klasa"
  ]
  node [
    id 140
    label "jednostka_geologiczna"
  ]
  node [
    id 141
    label "filia"
  ]
  node [
    id 142
    label "malm"
  ]
  node [
    id 143
    label "whole"
  ]
  node [
    id 144
    label "dogger"
  ]
  node [
    id 145
    label "poziom"
  ]
  node [
    id 146
    label "promocja"
  ]
  node [
    id 147
    label "kurs"
  ]
  node [
    id 148
    label "bank"
  ]
  node [
    id 149
    label "formacja"
  ]
  node [
    id 150
    label "ajencja"
  ]
  node [
    id 151
    label "wojsko"
  ]
  node [
    id 152
    label "siedziba"
  ]
  node [
    id 153
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 154
    label "agencja"
  ]
  node [
    id 155
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 156
    label "gabinet"
  ]
  node [
    id 157
    label "opieka_medyczna"
  ]
  node [
    id 158
    label "lekarz"
  ]
  node [
    id 159
    label "odzyskiwa&#263;"
  ]
  node [
    id 160
    label "znachodzi&#263;"
  ]
  node [
    id 161
    label "pozyskiwa&#263;"
  ]
  node [
    id 162
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 163
    label "detect"
  ]
  node [
    id 164
    label "powodowa&#263;"
  ]
  node [
    id 165
    label "unwrap"
  ]
  node [
    id 166
    label "wykrywa&#263;"
  ]
  node [
    id 167
    label "os&#261;dza&#263;"
  ]
  node [
    id 168
    label "doznawa&#263;"
  ]
  node [
    id 169
    label "wymy&#347;la&#263;"
  ]
  node [
    id 170
    label "mistreat"
  ]
  node [
    id 171
    label "obra&#380;a&#263;"
  ]
  node [
    id 172
    label "odkrywa&#263;"
  ]
  node [
    id 173
    label "debunk"
  ]
  node [
    id 174
    label "dostrzega&#263;"
  ]
  node [
    id 175
    label "okre&#347;la&#263;"
  ]
  node [
    id 176
    label "mie&#263;_miejsce"
  ]
  node [
    id 177
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 178
    label "motywowa&#263;"
  ]
  node [
    id 179
    label "act"
  ]
  node [
    id 180
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 181
    label "uzyskiwa&#263;"
  ]
  node [
    id 182
    label "wytwarza&#263;"
  ]
  node [
    id 183
    label "tease"
  ]
  node [
    id 184
    label "take"
  ]
  node [
    id 185
    label "hurt"
  ]
  node [
    id 186
    label "recur"
  ]
  node [
    id 187
    label "przychodzi&#263;"
  ]
  node [
    id 188
    label "sum_up"
  ]
  node [
    id 189
    label "strike"
  ]
  node [
    id 190
    label "robi&#263;"
  ]
  node [
    id 191
    label "s&#261;dzi&#263;"
  ]
  node [
    id 192
    label "hold"
  ]
  node [
    id 193
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 194
    label "typowy"
  ]
  node [
    id 195
    label "szpitalnie"
  ]
  node [
    id 196
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 197
    label "zwyczajny"
  ]
  node [
    id 198
    label "typowo"
  ]
  node [
    id 199
    label "cz&#281;sty"
  ]
  node [
    id 200
    label "zwyk&#322;y"
  ]
  node [
    id 201
    label "leczniczy"
  ]
  node [
    id 202
    label "lekarsko"
  ]
  node [
    id 203
    label "medycznie"
  ]
  node [
    id 204
    label "paramedyczny"
  ]
  node [
    id 205
    label "profilowy"
  ]
  node [
    id 206
    label "bia&#322;y"
  ]
  node [
    id 207
    label "praktyczny"
  ]
  node [
    id 208
    label "specjalistyczny"
  ]
  node [
    id 209
    label "zgodny"
  ]
  node [
    id 210
    label "specjalny"
  ]
  node [
    id 211
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 212
    label "jednostka_organizacyjna"
  ]
  node [
    id 213
    label "sfera"
  ]
  node [
    id 214
    label "miejsce_pracy"
  ]
  node [
    id 215
    label "insourcing"
  ]
  node [
    id 216
    label "wytw&#243;r"
  ]
  node [
    id 217
    label "column"
  ]
  node [
    id 218
    label "distribution"
  ]
  node [
    id 219
    label "stopie&#324;"
  ]
  node [
    id 220
    label "competence"
  ]
  node [
    id 221
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 222
    label "bezdro&#380;e"
  ]
  node [
    id 223
    label "poddzia&#322;"
  ]
  node [
    id 224
    label "przyswoi&#263;"
  ]
  node [
    id 225
    label "ludzko&#347;&#263;"
  ]
  node [
    id 226
    label "one"
  ]
  node [
    id 227
    label "ewoluowanie"
  ]
  node [
    id 228
    label "supremum"
  ]
  node [
    id 229
    label "skala"
  ]
  node [
    id 230
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 231
    label "przyswajanie"
  ]
  node [
    id 232
    label "wyewoluowanie"
  ]
  node [
    id 233
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 234
    label "przeliczy&#263;"
  ]
  node [
    id 235
    label "wyewoluowa&#263;"
  ]
  node [
    id 236
    label "ewoluowa&#263;"
  ]
  node [
    id 237
    label "matematyka"
  ]
  node [
    id 238
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 239
    label "rzut"
  ]
  node [
    id 240
    label "liczba_naturalna"
  ]
  node [
    id 241
    label "czynnik_biotyczny"
  ]
  node [
    id 242
    label "g&#322;owa"
  ]
  node [
    id 243
    label "figura"
  ]
  node [
    id 244
    label "individual"
  ]
  node [
    id 245
    label "portrecista"
  ]
  node [
    id 246
    label "obiekt"
  ]
  node [
    id 247
    label "przyswaja&#263;"
  ]
  node [
    id 248
    label "przyswojenie"
  ]
  node [
    id 249
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 250
    label "profanum"
  ]
  node [
    id 251
    label "mikrokosmos"
  ]
  node [
    id 252
    label "starzenie_si&#281;"
  ]
  node [
    id 253
    label "duch"
  ]
  node [
    id 254
    label "przeliczanie"
  ]
  node [
    id 255
    label "oddzia&#322;ywanie"
  ]
  node [
    id 256
    label "antropochoria"
  ]
  node [
    id 257
    label "funkcja"
  ]
  node [
    id 258
    label "homo_sapiens"
  ]
  node [
    id 259
    label "przelicza&#263;"
  ]
  node [
    id 260
    label "infimum"
  ]
  node [
    id 261
    label "przeliczenie"
  ]
  node [
    id 262
    label "Mazowsze"
  ]
  node [
    id 263
    label "odm&#322;adzanie"
  ]
  node [
    id 264
    label "&#346;wietliki"
  ]
  node [
    id 265
    label "zbi&#243;r"
  ]
  node [
    id 266
    label "skupienie"
  ]
  node [
    id 267
    label "The_Beatles"
  ]
  node [
    id 268
    label "odm&#322;adza&#263;"
  ]
  node [
    id 269
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 270
    label "zabudowania"
  ]
  node [
    id 271
    label "group"
  ]
  node [
    id 272
    label "zespolik"
  ]
  node [
    id 273
    label "schorzenie"
  ]
  node [
    id 274
    label "ro&#347;lina"
  ]
  node [
    id 275
    label "grupa"
  ]
  node [
    id 276
    label "Depeche_Mode"
  ]
  node [
    id 277
    label "batch"
  ]
  node [
    id 278
    label "odm&#322;odzenie"
  ]
  node [
    id 279
    label "po&#322;o&#380;enie"
  ]
  node [
    id 280
    label "jako&#347;&#263;"
  ]
  node [
    id 281
    label "p&#322;aszczyzna"
  ]
  node [
    id 282
    label "punkt_widzenia"
  ]
  node [
    id 283
    label "kierunek"
  ]
  node [
    id 284
    label "wyk&#322;adnik"
  ]
  node [
    id 285
    label "faza"
  ]
  node [
    id 286
    label "szczebel"
  ]
  node [
    id 287
    label "budynek"
  ]
  node [
    id 288
    label "wysoko&#347;&#263;"
  ]
  node [
    id 289
    label "ranga"
  ]
  node [
    id 290
    label "&#321;ubianka"
  ]
  node [
    id 291
    label "dzia&#322;_personalny"
  ]
  node [
    id 292
    label "Kreml"
  ]
  node [
    id 293
    label "Bia&#322;y_Dom"
  ]
  node [
    id 294
    label "miejsce"
  ]
  node [
    id 295
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 296
    label "sadowisko"
  ]
  node [
    id 297
    label "Bund"
  ]
  node [
    id 298
    label "PPR"
  ]
  node [
    id 299
    label "Jakobici"
  ]
  node [
    id 300
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 301
    label "leksem"
  ]
  node [
    id 302
    label "SLD"
  ]
  node [
    id 303
    label "Razem"
  ]
  node [
    id 304
    label "PiS"
  ]
  node [
    id 305
    label "zjawisko"
  ]
  node [
    id 306
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 307
    label "partia"
  ]
  node [
    id 308
    label "Kuomintang"
  ]
  node [
    id 309
    label "ZSL"
  ]
  node [
    id 310
    label "szko&#322;a"
  ]
  node [
    id 311
    label "proces"
  ]
  node [
    id 312
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 313
    label "rugby"
  ]
  node [
    id 314
    label "AWS"
  ]
  node [
    id 315
    label "posta&#263;"
  ]
  node [
    id 316
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 317
    label "PO"
  ]
  node [
    id 318
    label "si&#322;a"
  ]
  node [
    id 319
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 320
    label "Federali&#347;ci"
  ]
  node [
    id 321
    label "PSL"
  ]
  node [
    id 322
    label "czynno&#347;&#263;"
  ]
  node [
    id 323
    label "Wigowie"
  ]
  node [
    id 324
    label "ZChN"
  ]
  node [
    id 325
    label "egzekutywa"
  ]
  node [
    id 326
    label "rocznik"
  ]
  node [
    id 327
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 328
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 329
    label "unit"
  ]
  node [
    id 330
    label "forma"
  ]
  node [
    id 331
    label "przedstawicielstwo"
  ]
  node [
    id 332
    label "firma"
  ]
  node [
    id 333
    label "NASA"
  ]
  node [
    id 334
    label "wagon"
  ]
  node [
    id 335
    label "mecz_mistrzowski"
  ]
  node [
    id 336
    label "przedmiot"
  ]
  node [
    id 337
    label "arrangement"
  ]
  node [
    id 338
    label "class"
  ]
  node [
    id 339
    label "&#322;awka"
  ]
  node [
    id 340
    label "wykrzyknik"
  ]
  node [
    id 341
    label "zaleta"
  ]
  node [
    id 342
    label "jednostka_systematyczna"
  ]
  node [
    id 343
    label "programowanie_obiektowe"
  ]
  node [
    id 344
    label "tablica"
  ]
  node [
    id 345
    label "warstwa"
  ]
  node [
    id 346
    label "rezerwa"
  ]
  node [
    id 347
    label "gromada"
  ]
  node [
    id 348
    label "Ekwici"
  ]
  node [
    id 349
    label "&#347;rodowisko"
  ]
  node [
    id 350
    label "sala"
  ]
  node [
    id 351
    label "pomoc"
  ]
  node [
    id 352
    label "form"
  ]
  node [
    id 353
    label "przepisa&#263;"
  ]
  node [
    id 354
    label "znak_jako&#347;ci"
  ]
  node [
    id 355
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 356
    label "type"
  ]
  node [
    id 357
    label "przepisanie"
  ]
  node [
    id 358
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 359
    label "dziennik_lekcyjny"
  ]
  node [
    id 360
    label "typ"
  ]
  node [
    id 361
    label "fakcja"
  ]
  node [
    id 362
    label "obrona"
  ]
  node [
    id 363
    label "atak"
  ]
  node [
    id 364
    label "botanika"
  ]
  node [
    id 365
    label "damka"
  ]
  node [
    id 366
    label "warcaby"
  ]
  node [
    id 367
    label "promotion"
  ]
  node [
    id 368
    label "impreza"
  ]
  node [
    id 369
    label "sprzeda&#380;"
  ]
  node [
    id 370
    label "zamiana"
  ]
  node [
    id 371
    label "udzieli&#263;"
  ]
  node [
    id 372
    label "brief"
  ]
  node [
    id 373
    label "decyzja"
  ]
  node [
    id 374
    label "&#347;wiadectwo"
  ]
  node [
    id 375
    label "akcja"
  ]
  node [
    id 376
    label "bran&#380;a"
  ]
  node [
    id 377
    label "commencement"
  ]
  node [
    id 378
    label "okazja"
  ]
  node [
    id 379
    label "informacja"
  ]
  node [
    id 380
    label "promowa&#263;"
  ]
  node [
    id 381
    label "graduacja"
  ]
  node [
    id 382
    label "nominacja"
  ]
  node [
    id 383
    label "szachy"
  ]
  node [
    id 384
    label "popularyzacja"
  ]
  node [
    id 385
    label "wypromowa&#263;"
  ]
  node [
    id 386
    label "gradation"
  ]
  node [
    id 387
    label "uzyska&#263;"
  ]
  node [
    id 388
    label "zrejterowanie"
  ]
  node [
    id 389
    label "zmobilizowa&#263;"
  ]
  node [
    id 390
    label "dezerter"
  ]
  node [
    id 391
    label "oddzia&#322;_karny"
  ]
  node [
    id 392
    label "tabor"
  ]
  node [
    id 393
    label "wermacht"
  ]
  node [
    id 394
    label "cofni&#281;cie"
  ]
  node [
    id 395
    label "potencja"
  ]
  node [
    id 396
    label "fala"
  ]
  node [
    id 397
    label "struktura"
  ]
  node [
    id 398
    label "korpus"
  ]
  node [
    id 399
    label "soldateska"
  ]
  node [
    id 400
    label "ods&#322;ugiwanie"
  ]
  node [
    id 401
    label "werbowanie_si&#281;"
  ]
  node [
    id 402
    label "zdemobilizowanie"
  ]
  node [
    id 403
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 404
    label "s&#322;u&#380;ba"
  ]
  node [
    id 405
    label "or&#281;&#380;"
  ]
  node [
    id 406
    label "Legia_Cudzoziemska"
  ]
  node [
    id 407
    label "Armia_Czerwona"
  ]
  node [
    id 408
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 409
    label "rejterowanie"
  ]
  node [
    id 410
    label "Czerwona_Gwardia"
  ]
  node [
    id 411
    label "zrejterowa&#263;"
  ]
  node [
    id 412
    label "sztabslekarz"
  ]
  node [
    id 413
    label "zmobilizowanie"
  ]
  node [
    id 414
    label "wojo"
  ]
  node [
    id 415
    label "pospolite_ruszenie"
  ]
  node [
    id 416
    label "Eurokorpus"
  ]
  node [
    id 417
    label "mobilizowanie"
  ]
  node [
    id 418
    label "rejterowa&#263;"
  ]
  node [
    id 419
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 420
    label "mobilizowa&#263;"
  ]
  node [
    id 421
    label "Armia_Krajowa"
  ]
  node [
    id 422
    label "dryl"
  ]
  node [
    id 423
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 424
    label "petarda"
  ]
  node [
    id 425
    label "pozycja"
  ]
  node [
    id 426
    label "zdemobilizowa&#263;"
  ]
  node [
    id 427
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 428
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 429
    label "zwy&#380;kowanie"
  ]
  node [
    id 430
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 431
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 432
    label "zaj&#281;cia"
  ]
  node [
    id 433
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 434
    label "trasa"
  ]
  node [
    id 435
    label "rok"
  ]
  node [
    id 436
    label "przeorientowywanie"
  ]
  node [
    id 437
    label "przejazd"
  ]
  node [
    id 438
    label "przeorientowywa&#263;"
  ]
  node [
    id 439
    label "nauka"
  ]
  node [
    id 440
    label "przeorientowanie"
  ]
  node [
    id 441
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 442
    label "przeorientowa&#263;"
  ]
  node [
    id 443
    label "manner"
  ]
  node [
    id 444
    label "course"
  ]
  node [
    id 445
    label "passage"
  ]
  node [
    id 446
    label "zni&#380;kowanie"
  ]
  node [
    id 447
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 448
    label "seria"
  ]
  node [
    id 449
    label "stawka"
  ]
  node [
    id 450
    label "way"
  ]
  node [
    id 451
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 452
    label "spos&#243;b"
  ]
  node [
    id 453
    label "deprecjacja"
  ]
  node [
    id 454
    label "cedu&#322;a"
  ]
  node [
    id 455
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 456
    label "drive"
  ]
  node [
    id 457
    label "bearing"
  ]
  node [
    id 458
    label "Lira"
  ]
  node [
    id 459
    label "dzier&#380;awa"
  ]
  node [
    id 460
    label "j&#261;dro"
  ]
  node [
    id 461
    label "systemik"
  ]
  node [
    id 462
    label "rozprz&#261;c"
  ]
  node [
    id 463
    label "oprogramowanie"
  ]
  node [
    id 464
    label "systemat"
  ]
  node [
    id 465
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 466
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 467
    label "model"
  ]
  node [
    id 468
    label "usenet"
  ]
  node [
    id 469
    label "s&#261;d"
  ]
  node [
    id 470
    label "porz&#261;dek"
  ]
  node [
    id 471
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 472
    label "przyn&#281;ta"
  ]
  node [
    id 473
    label "p&#322;&#243;d"
  ]
  node [
    id 474
    label "net"
  ]
  node [
    id 475
    label "w&#281;dkarstwo"
  ]
  node [
    id 476
    label "eratem"
  ]
  node [
    id 477
    label "doktryna"
  ]
  node [
    id 478
    label "pulpit"
  ]
  node [
    id 479
    label "konstelacja"
  ]
  node [
    id 480
    label "o&#347;"
  ]
  node [
    id 481
    label "podsystem"
  ]
  node [
    id 482
    label "metoda"
  ]
  node [
    id 483
    label "ryba"
  ]
  node [
    id 484
    label "Leopard"
  ]
  node [
    id 485
    label "Android"
  ]
  node [
    id 486
    label "zachowanie"
  ]
  node [
    id 487
    label "cybernetyk"
  ]
  node [
    id 488
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 489
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 490
    label "method"
  ]
  node [
    id 491
    label "sk&#322;ad"
  ]
  node [
    id 492
    label "podstawa"
  ]
  node [
    id 493
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 494
    label "agent_rozliczeniowy"
  ]
  node [
    id 495
    label "kwota"
  ]
  node [
    id 496
    label "konto"
  ]
  node [
    id 497
    label "wk&#322;adca"
  ]
  node [
    id 498
    label "eurorynek"
  ]
  node [
    id 499
    label "chronozona"
  ]
  node [
    id 500
    label "kondygnacja"
  ]
  node [
    id 501
    label "eta&#380;"
  ]
  node [
    id 502
    label "floor"
  ]
  node [
    id 503
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 504
    label "formacja_geologiczna"
  ]
  node [
    id 505
    label "jura_g&#243;rna"
  ]
  node [
    id 506
    label "jura"
  ]
  node [
    id 507
    label "pos&#322;uchanie"
  ]
  node [
    id 508
    label "skumanie"
  ]
  node [
    id 509
    label "orientacja"
  ]
  node [
    id 510
    label "teoria"
  ]
  node [
    id 511
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 512
    label "clasp"
  ]
  node [
    id 513
    label "przem&#243;wienie"
  ]
  node [
    id 514
    label "zorientowanie"
  ]
  node [
    id 515
    label "co&#347;"
  ]
  node [
    id 516
    label "thing"
  ]
  node [
    id 517
    label "program"
  ]
  node [
    id 518
    label "rzecz"
  ]
  node [
    id 519
    label "strona"
  ]
  node [
    id 520
    label "Chocho&#322;"
  ]
  node [
    id 521
    label "Herkules_Poirot"
  ]
  node [
    id 522
    label "Edyp"
  ]
  node [
    id 523
    label "parali&#380;owa&#263;"
  ]
  node [
    id 524
    label "Harry_Potter"
  ]
  node [
    id 525
    label "Casanova"
  ]
  node [
    id 526
    label "Gargantua"
  ]
  node [
    id 527
    label "Zgredek"
  ]
  node [
    id 528
    label "Winnetou"
  ]
  node [
    id 529
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 530
    label "Dulcynea"
  ]
  node [
    id 531
    label "kategoria_gramatyczna"
  ]
  node [
    id 532
    label "person"
  ]
  node [
    id 533
    label "Sherlock_Holmes"
  ]
  node [
    id 534
    label "Quasimodo"
  ]
  node [
    id 535
    label "Plastu&#347;"
  ]
  node [
    id 536
    label "Faust"
  ]
  node [
    id 537
    label "Wallenrod"
  ]
  node [
    id 538
    label "Dwukwiat"
  ]
  node [
    id 539
    label "koniugacja"
  ]
  node [
    id 540
    label "Don_Juan"
  ]
  node [
    id 541
    label "Don_Kiszot"
  ]
  node [
    id 542
    label "Hamlet"
  ]
  node [
    id 543
    label "Werter"
  ]
  node [
    id 544
    label "istota"
  ]
  node [
    id 545
    label "Szwejk"
  ]
  node [
    id 546
    label "Rzym_Zachodni"
  ]
  node [
    id 547
    label "ilo&#347;&#263;"
  ]
  node [
    id 548
    label "element"
  ]
  node [
    id 549
    label "Rzym_Wschodni"
  ]
  node [
    id 550
    label "urz&#261;dzenie"
  ]
  node [
    id 551
    label "integer"
  ]
  node [
    id 552
    label "liczba"
  ]
  node [
    id 553
    label "zlewanie_si&#281;"
  ]
  node [
    id 554
    label "uk&#322;ad"
  ]
  node [
    id 555
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 556
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 557
    label "pe&#322;ny"
  ]
  node [
    id 558
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 559
    label "masztab"
  ]
  node [
    id 560
    label "kreska"
  ]
  node [
    id 561
    label "podzia&#322;ka"
  ]
  node [
    id 562
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 563
    label "wielko&#347;&#263;"
  ]
  node [
    id 564
    label "zero"
  ]
  node [
    id 565
    label "interwa&#322;"
  ]
  node [
    id 566
    label "przymiar"
  ]
  node [
    id 567
    label "dominanta"
  ]
  node [
    id 568
    label "tetrachord"
  ]
  node [
    id 569
    label "scale"
  ]
  node [
    id 570
    label "przedzia&#322;"
  ]
  node [
    id 571
    label "podzakres"
  ]
  node [
    id 572
    label "proporcja"
  ]
  node [
    id 573
    label "dziedzina"
  ]
  node [
    id 574
    label "part"
  ]
  node [
    id 575
    label "rejestr"
  ]
  node [
    id 576
    label "subdominanta"
  ]
  node [
    id 577
    label "cz&#322;owiek"
  ]
  node [
    id 578
    label "atom"
  ]
  node [
    id 579
    label "przyroda"
  ]
  node [
    id 580
    label "Ziemia"
  ]
  node [
    id 581
    label "kosmos"
  ]
  node [
    id 582
    label "miniatura"
  ]
  node [
    id 583
    label "wymienienie"
  ]
  node [
    id 584
    label "przerachowanie"
  ]
  node [
    id 585
    label "skontrolowanie"
  ]
  node [
    id 586
    label "count"
  ]
  node [
    id 587
    label "rachunek_operatorowy"
  ]
  node [
    id 588
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 589
    label "kryptologia"
  ]
  node [
    id 590
    label "logicyzm"
  ]
  node [
    id 591
    label "logika"
  ]
  node [
    id 592
    label "matematyka_czysta"
  ]
  node [
    id 593
    label "forsing"
  ]
  node [
    id 594
    label "modelowanie_matematyczne"
  ]
  node [
    id 595
    label "matma"
  ]
  node [
    id 596
    label "teoria_katastrof"
  ]
  node [
    id 597
    label "fizyka_matematyczna"
  ]
  node [
    id 598
    label "teoria_graf&#243;w"
  ]
  node [
    id 599
    label "rachunki"
  ]
  node [
    id 600
    label "topologia_algebraiczna"
  ]
  node [
    id 601
    label "matematyka_stosowana"
  ]
  node [
    id 602
    label "sprawdzanie"
  ]
  node [
    id 603
    label "zast&#281;powanie"
  ]
  node [
    id 604
    label "przerachowywanie"
  ]
  node [
    id 605
    label "ograniczenie"
  ]
  node [
    id 606
    label "czyn"
  ]
  node [
    id 607
    label "addytywno&#347;&#263;"
  ]
  node [
    id 608
    label "function"
  ]
  node [
    id 609
    label "zastosowanie"
  ]
  node [
    id 610
    label "funkcjonowanie"
  ]
  node [
    id 611
    label "praca"
  ]
  node [
    id 612
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 613
    label "powierzanie"
  ]
  node [
    id 614
    label "cel"
  ]
  node [
    id 615
    label "przeciwdziedzina"
  ]
  node [
    id 616
    label "awansowa&#263;"
  ]
  node [
    id 617
    label "stawia&#263;"
  ]
  node [
    id 618
    label "wakowa&#263;"
  ]
  node [
    id 619
    label "znaczenie"
  ]
  node [
    id 620
    label "postawi&#263;"
  ]
  node [
    id 621
    label "awansowanie"
  ]
  node [
    id 622
    label "armia"
  ]
  node [
    id 623
    label "nawr&#243;t_choroby"
  ]
  node [
    id 624
    label "potomstwo"
  ]
  node [
    id 625
    label "odwzorowanie"
  ]
  node [
    id 626
    label "rysunek"
  ]
  node [
    id 627
    label "scene"
  ]
  node [
    id 628
    label "throw"
  ]
  node [
    id 629
    label "float"
  ]
  node [
    id 630
    label "punkt"
  ]
  node [
    id 631
    label "projection"
  ]
  node [
    id 632
    label "injection"
  ]
  node [
    id 633
    label "blow"
  ]
  node [
    id 634
    label "pomys&#322;"
  ]
  node [
    id 635
    label "k&#322;ad"
  ]
  node [
    id 636
    label "mold"
  ]
  node [
    id 637
    label "sprawdza&#263;"
  ]
  node [
    id 638
    label "zmienia&#263;"
  ]
  node [
    id 639
    label "przerachowywa&#263;"
  ]
  node [
    id 640
    label "sprawdzi&#263;"
  ]
  node [
    id 641
    label "change"
  ]
  node [
    id 642
    label "przerachowa&#263;"
  ]
  node [
    id 643
    label "zmieni&#263;"
  ]
  node [
    id 644
    label "organizm"
  ]
  node [
    id 645
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 646
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 647
    label "czerpanie"
  ]
  node [
    id 648
    label "uczenie_si&#281;"
  ]
  node [
    id 649
    label "pobieranie"
  ]
  node [
    id 650
    label "acquisition"
  ]
  node [
    id 651
    label "od&#380;ywianie"
  ]
  node [
    id 652
    label "kultura"
  ]
  node [
    id 653
    label "wynoszenie"
  ]
  node [
    id 654
    label "absorption"
  ]
  node [
    id 655
    label "wdra&#380;anie_si&#281;"
  ]
  node [
    id 656
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 657
    label "translate"
  ]
  node [
    id 658
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 659
    label "pobra&#263;"
  ]
  node [
    id 660
    label "thrill"
  ]
  node [
    id 661
    label "pobranie"
  ]
  node [
    id 662
    label "wyniesienie"
  ]
  node [
    id 663
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 664
    label "assimilation"
  ]
  node [
    id 665
    label "emotion"
  ]
  node [
    id 666
    label "nauczenie_si&#281;"
  ]
  node [
    id 667
    label "zaczerpni&#281;cie"
  ]
  node [
    id 668
    label "mechanizm_obronny"
  ]
  node [
    id 669
    label "convention"
  ]
  node [
    id 670
    label "przeobrazi&#263;_si&#281;"
  ]
  node [
    id 671
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 672
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 673
    label "przeobra&#380;a&#263;_si&#281;"
  ]
  node [
    id 674
    label "react"
  ]
  node [
    id 675
    label "reaction"
  ]
  node [
    id 676
    label "rozmowa"
  ]
  node [
    id 677
    label "response"
  ]
  node [
    id 678
    label "rezultat"
  ]
  node [
    id 679
    label "respondent"
  ]
  node [
    id 680
    label "treat"
  ]
  node [
    id 681
    label "czerpa&#263;"
  ]
  node [
    id 682
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 683
    label "pobiera&#263;"
  ]
  node [
    id 684
    label "rede"
  ]
  node [
    id 685
    label "wygl&#261;d"
  ]
  node [
    id 686
    label "fotograf"
  ]
  node [
    id 687
    label "malarz"
  ]
  node [
    id 688
    label "artysta"
  ]
  node [
    id 689
    label "powodowanie"
  ]
  node [
    id 690
    label "hipnotyzowanie"
  ]
  node [
    id 691
    label "&#347;lad"
  ]
  node [
    id 692
    label "docieranie"
  ]
  node [
    id 693
    label "natural_process"
  ]
  node [
    id 694
    label "reakcja_chemiczna"
  ]
  node [
    id 695
    label "wdzieranie_si&#281;"
  ]
  node [
    id 696
    label "lobbysta"
  ]
  node [
    id 697
    label "pryncypa&#322;"
  ]
  node [
    id 698
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 699
    label "kszta&#322;t"
  ]
  node [
    id 700
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 701
    label "wiedza"
  ]
  node [
    id 702
    label "kierowa&#263;"
  ]
  node [
    id 703
    label "alkohol"
  ]
  node [
    id 704
    label "zdolno&#347;&#263;"
  ]
  node [
    id 705
    label "cecha"
  ]
  node [
    id 706
    label "&#380;ycie"
  ]
  node [
    id 707
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 708
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 709
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 710
    label "sztuka"
  ]
  node [
    id 711
    label "dekiel"
  ]
  node [
    id 712
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 713
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 714
    label "&#347;ci&#281;gno"
  ]
  node [
    id 715
    label "noosfera"
  ]
  node [
    id 716
    label "byd&#322;o"
  ]
  node [
    id 717
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 718
    label "makrocefalia"
  ]
  node [
    id 719
    label "ucho"
  ]
  node [
    id 720
    label "g&#243;ra"
  ]
  node [
    id 721
    label "m&#243;zg"
  ]
  node [
    id 722
    label "kierownictwo"
  ]
  node [
    id 723
    label "fryzura"
  ]
  node [
    id 724
    label "umys&#322;"
  ]
  node [
    id 725
    label "cia&#322;o"
  ]
  node [
    id 726
    label "cz&#322;onek"
  ]
  node [
    id 727
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 728
    label "czaszka"
  ]
  node [
    id 729
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 730
    label "allochoria"
  ]
  node [
    id 731
    label "charakterystyka"
  ]
  node [
    id 732
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 733
    label "bierka_szachowa"
  ]
  node [
    id 734
    label "obiekt_matematyczny"
  ]
  node [
    id 735
    label "gestaltyzm"
  ]
  node [
    id 736
    label "styl"
  ]
  node [
    id 737
    label "obraz"
  ]
  node [
    id 738
    label "Osjan"
  ]
  node [
    id 739
    label "character"
  ]
  node [
    id 740
    label "kto&#347;"
  ]
  node [
    id 741
    label "rze&#378;ba"
  ]
  node [
    id 742
    label "stylistyka"
  ]
  node [
    id 743
    label "figure"
  ]
  node [
    id 744
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 745
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 746
    label "antycypacja"
  ]
  node [
    id 747
    label "ornamentyka"
  ]
  node [
    id 748
    label "Aspazja"
  ]
  node [
    id 749
    label "facet"
  ]
  node [
    id 750
    label "popis"
  ]
  node [
    id 751
    label "wiersz"
  ]
  node [
    id 752
    label "kompleksja"
  ]
  node [
    id 753
    label "budowa"
  ]
  node [
    id 754
    label "symetria"
  ]
  node [
    id 755
    label "lingwistyka_kognitywna"
  ]
  node [
    id 756
    label "karta"
  ]
  node [
    id 757
    label "shape"
  ]
  node [
    id 758
    label "podzbi&#243;r"
  ]
  node [
    id 759
    label "przedstawienie"
  ]
  node [
    id 760
    label "point"
  ]
  node [
    id 761
    label "perspektywa"
  ]
  node [
    id 762
    label "piek&#322;o"
  ]
  node [
    id 763
    label "human_body"
  ]
  node [
    id 764
    label "ofiarowywanie"
  ]
  node [
    id 765
    label "sfera_afektywna"
  ]
  node [
    id 766
    label "nekromancja"
  ]
  node [
    id 767
    label "Po&#347;wist"
  ]
  node [
    id 768
    label "podekscytowanie"
  ]
  node [
    id 769
    label "deformowanie"
  ]
  node [
    id 770
    label "sumienie"
  ]
  node [
    id 771
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 772
    label "deformowa&#263;"
  ]
  node [
    id 773
    label "osobowo&#347;&#263;"
  ]
  node [
    id 774
    label "psychika"
  ]
  node [
    id 775
    label "zjawa"
  ]
  node [
    id 776
    label "zmar&#322;y"
  ]
  node [
    id 777
    label "istota_nadprzyrodzona"
  ]
  node [
    id 778
    label "power"
  ]
  node [
    id 779
    label "entity"
  ]
  node [
    id 780
    label "ofiarowywa&#263;"
  ]
  node [
    id 781
    label "oddech"
  ]
  node [
    id 782
    label "seksualno&#347;&#263;"
  ]
  node [
    id 783
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 784
    label "byt"
  ]
  node [
    id 785
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 786
    label "ego"
  ]
  node [
    id 787
    label "ofiarowanie"
  ]
  node [
    id 788
    label "charakter"
  ]
  node [
    id 789
    label "fizjonomia"
  ]
  node [
    id 790
    label "kompleks"
  ]
  node [
    id 791
    label "zapalno&#347;&#263;"
  ]
  node [
    id 792
    label "T&#281;sknica"
  ]
  node [
    id 793
    label "ofiarowa&#263;"
  ]
  node [
    id 794
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 795
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 796
    label "passion"
  ]
  node [
    id 797
    label "specify"
  ]
  node [
    id 798
    label "wyszkoli&#263;"
  ]
  node [
    id 799
    label "ukierunkowa&#263;"
  ]
  node [
    id 800
    label "pom&#243;c"
  ]
  node [
    id 801
    label "train"
  ]
  node [
    id 802
    label "o&#347;wieci&#263;"
  ]
  node [
    id 803
    label "aim"
  ]
  node [
    id 804
    label "set"
  ]
  node [
    id 805
    label "wyznaczy&#263;"
  ]
  node [
    id 806
    label "granica"
  ]
  node [
    id 807
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 808
    label "desygnat"
  ]
  node [
    id 809
    label "circle"
  ]
  node [
    id 810
    label "rozmiar"
  ]
  node [
    id 811
    label "izochronizm"
  ]
  node [
    id 812
    label "zasi&#261;g"
  ]
  node [
    id 813
    label "bridge"
  ]
  node [
    id 814
    label "warunek_lokalowy"
  ]
  node [
    id 815
    label "rzadko&#347;&#263;"
  ]
  node [
    id 816
    label "measure"
  ]
  node [
    id 817
    label "opinia"
  ]
  node [
    id 818
    label "dymensja"
  ]
  node [
    id 819
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 820
    label "property"
  ]
  node [
    id 821
    label "egzemplarz"
  ]
  node [
    id 822
    label "series"
  ]
  node [
    id 823
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 824
    label "uprawianie"
  ]
  node [
    id 825
    label "praca_rolnicza"
  ]
  node [
    id 826
    label "collection"
  ]
  node [
    id 827
    label "dane"
  ]
  node [
    id 828
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 829
    label "pakiet_klimatyczny"
  ]
  node [
    id 830
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 831
    label "sum"
  ]
  node [
    id 832
    label "gathering"
  ]
  node [
    id 833
    label "album"
  ]
  node [
    id 834
    label "przej&#347;cie"
  ]
  node [
    id 835
    label "kres"
  ]
  node [
    id 836
    label "granica_pa&#324;stwa"
  ]
  node [
    id 837
    label "Ural"
  ]
  node [
    id 838
    label "miara"
  ]
  node [
    id 839
    label "end"
  ]
  node [
    id 840
    label "pu&#322;ap"
  ]
  node [
    id 841
    label "koniec"
  ]
  node [
    id 842
    label "granice"
  ]
  node [
    id 843
    label "frontier"
  ]
  node [
    id 844
    label "wymiar"
  ]
  node [
    id 845
    label "strefa"
  ]
  node [
    id 846
    label "kula"
  ]
  node [
    id 847
    label "sector"
  ]
  node [
    id 848
    label "przestrze&#324;"
  ]
  node [
    id 849
    label "p&#243;&#322;kula"
  ]
  node [
    id 850
    label "huczek"
  ]
  node [
    id 851
    label "p&#243;&#322;sfera"
  ]
  node [
    id 852
    label "powierzchnia"
  ]
  node [
    id 853
    label "kolur"
  ]
  node [
    id 854
    label "odpowiednik"
  ]
  node [
    id 855
    label "designatum"
  ]
  node [
    id 856
    label "nazwa_rzetelna"
  ]
  node [
    id 857
    label "nazwa_pozorna"
  ]
  node [
    id 858
    label "denotacja"
  ]
  node [
    id 859
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 860
    label "odst&#281;powa&#263;"
  ]
  node [
    id 861
    label "dawa&#263;"
  ]
  node [
    id 862
    label "assign"
  ]
  node [
    id 863
    label "render"
  ]
  node [
    id 864
    label "accord"
  ]
  node [
    id 865
    label "zezwala&#263;"
  ]
  node [
    id 866
    label "przyznawa&#263;"
  ]
  node [
    id 867
    label "przekazywa&#263;"
  ]
  node [
    id 868
    label "dostarcza&#263;"
  ]
  node [
    id 869
    label "&#322;adowa&#263;"
  ]
  node [
    id 870
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 871
    label "give"
  ]
  node [
    id 872
    label "przeznacza&#263;"
  ]
  node [
    id 873
    label "surrender"
  ]
  node [
    id 874
    label "traktowa&#263;"
  ]
  node [
    id 875
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 876
    label "obiecywa&#263;"
  ]
  node [
    id 877
    label "tender"
  ]
  node [
    id 878
    label "rap"
  ]
  node [
    id 879
    label "umieszcza&#263;"
  ]
  node [
    id 880
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 881
    label "t&#322;uc"
  ]
  node [
    id 882
    label "powierza&#263;"
  ]
  node [
    id 883
    label "wpiernicza&#263;"
  ]
  node [
    id 884
    label "exsert"
  ]
  node [
    id 885
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 886
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 887
    label "p&#322;aci&#263;"
  ]
  node [
    id 888
    label "hold_out"
  ]
  node [
    id 889
    label "nalewa&#263;"
  ]
  node [
    id 890
    label "confer"
  ]
  node [
    id 891
    label "distribute"
  ]
  node [
    id 892
    label "stwierdza&#263;"
  ]
  node [
    id 893
    label "nadawa&#263;"
  ]
  node [
    id 894
    label "uznawa&#263;"
  ]
  node [
    id 895
    label "authorize"
  ]
  node [
    id 896
    label "odsuwa&#263;_si&#281;"
  ]
  node [
    id 897
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 898
    label "odwr&#243;t"
  ]
  node [
    id 899
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 900
    label "impart"
  ]
  node [
    id 901
    label "czynienie_dobra"
  ]
  node [
    id 902
    label "zobowi&#261;zanie"
  ]
  node [
    id 903
    label "p&#322;acenie"
  ]
  node [
    id 904
    label "wyraz"
  ]
  node [
    id 905
    label "koszt_rodzajowy"
  ]
  node [
    id 906
    label "service"
  ]
  node [
    id 907
    label "us&#322;uga"
  ]
  node [
    id 908
    label "przekonywanie"
  ]
  node [
    id 909
    label "sk&#322;adanie"
  ]
  node [
    id 910
    label "informowanie"
  ]
  node [
    id 911
    label "command"
  ]
  node [
    id 912
    label "performance"
  ]
  node [
    id 913
    label "pracowanie"
  ]
  node [
    id 914
    label "opowiadanie"
  ]
  node [
    id 915
    label "dawanie"
  ]
  node [
    id 916
    label "przyk&#322;adanie"
  ]
  node [
    id 917
    label "gromadzenie"
  ]
  node [
    id 918
    label "zestawianie"
  ]
  node [
    id 919
    label "opracowywanie"
  ]
  node [
    id 920
    label "m&#243;wienie"
  ]
  node [
    id 921
    label "gi&#281;cie"
  ]
  node [
    id 922
    label "follow-up"
  ]
  node [
    id 923
    label "wypowied&#378;"
  ]
  node [
    id 924
    label "report"
  ]
  node [
    id 925
    label "spalenie"
  ]
  node [
    id 926
    label "rozpowiedzenie"
  ]
  node [
    id 927
    label "podbarwianie"
  ]
  node [
    id 928
    label "przedstawianie"
  ]
  node [
    id 929
    label "story"
  ]
  node [
    id 930
    label "rozpowiadanie"
  ]
  node [
    id 931
    label "proza"
  ]
  node [
    id 932
    label "prawienie"
  ]
  node [
    id 933
    label "utw&#243;r_epicki"
  ]
  node [
    id 934
    label "fabu&#322;a"
  ]
  node [
    id 935
    label "wydawanie"
  ]
  node [
    id 936
    label "wage"
  ]
  node [
    id 937
    label "pay"
  ]
  node [
    id 938
    label "wykupywanie"
  ]
  node [
    id 939
    label "osi&#261;ganie"
  ]
  node [
    id 940
    label "przepracowanie_si&#281;"
  ]
  node [
    id 941
    label "zarz&#261;dzanie"
  ]
  node [
    id 942
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 943
    label "podlizanie_si&#281;"
  ]
  node [
    id 944
    label "dopracowanie"
  ]
  node [
    id 945
    label "podlizywanie_si&#281;"
  ]
  node [
    id 946
    label "uruchamianie"
  ]
  node [
    id 947
    label "dzia&#322;anie"
  ]
  node [
    id 948
    label "d&#261;&#380;enie"
  ]
  node [
    id 949
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 950
    label "uruchomienie"
  ]
  node [
    id 951
    label "nakr&#281;canie"
  ]
  node [
    id 952
    label "tr&#243;jstronny"
  ]
  node [
    id 953
    label "postaranie_si&#281;"
  ]
  node [
    id 954
    label "odpocz&#281;cie"
  ]
  node [
    id 955
    label "nakr&#281;cenie"
  ]
  node [
    id 956
    label "zatrzymanie"
  ]
  node [
    id 957
    label "spracowanie_si&#281;"
  ]
  node [
    id 958
    label "skakanie"
  ]
  node [
    id 959
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 960
    label "podtrzymywanie"
  ]
  node [
    id 961
    label "w&#322;&#261;czanie"
  ]
  node [
    id 962
    label "zaprz&#281;ganie"
  ]
  node [
    id 963
    label "podejmowanie"
  ]
  node [
    id 964
    label "maszyna"
  ]
  node [
    id 965
    label "wyrabianie"
  ]
  node [
    id 966
    label "dzianie_si&#281;"
  ]
  node [
    id 967
    label "use"
  ]
  node [
    id 968
    label "przepracowanie"
  ]
  node [
    id 969
    label "poruszanie_si&#281;"
  ]
  node [
    id 970
    label "impact"
  ]
  node [
    id 971
    label "przepracowywanie"
  ]
  node [
    id 972
    label "courtship"
  ]
  node [
    id 973
    label "zapracowanie"
  ]
  node [
    id 974
    label "wyrobienie"
  ]
  node [
    id 975
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 976
    label "w&#322;&#261;czenie"
  ]
  node [
    id 977
    label "communication"
  ]
  node [
    id 978
    label "powiadanie"
  ]
  node [
    id 979
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 980
    label "komunikowanie"
  ]
  node [
    id 981
    label "orientowanie"
  ]
  node [
    id 982
    label "odk&#322;adanie"
  ]
  node [
    id 983
    label "liczenie"
  ]
  node [
    id 984
    label "stawianie"
  ]
  node [
    id 985
    label "bycie"
  ]
  node [
    id 986
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 987
    label "assay"
  ]
  node [
    id 988
    label "wskazywanie"
  ]
  node [
    id 989
    label "gravity"
  ]
  node [
    id 990
    label "weight"
  ]
  node [
    id 991
    label "odgrywanie_roli"
  ]
  node [
    id 992
    label "okre&#347;lanie"
  ]
  node [
    id 993
    label "wyra&#380;enie"
  ]
  node [
    id 994
    label "stosunek_prawny"
  ]
  node [
    id 995
    label "oblig"
  ]
  node [
    id 996
    label "uregulowa&#263;"
  ]
  node [
    id 997
    label "oddzia&#322;anie"
  ]
  node [
    id 998
    label "occupation"
  ]
  node [
    id 999
    label "duty"
  ]
  node [
    id 1000
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 1001
    label "zapowied&#378;"
  ]
  node [
    id 1002
    label "obowi&#261;zek"
  ]
  node [
    id 1003
    label "statement"
  ]
  node [
    id 1004
    label "zapewnienie"
  ]
  node [
    id 1005
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 1006
    label "produkt_gotowy"
  ]
  node [
    id 1007
    label "asortyment"
  ]
  node [
    id 1008
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1009
    label "sk&#322;anianie"
  ]
  node [
    id 1010
    label "przekonywanie_si&#281;"
  ]
  node [
    id 1011
    label "persuasion"
  ]
  node [
    id 1012
    label "term"
  ]
  node [
    id 1013
    label "oznaka"
  ]
  node [
    id 1014
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 1015
    label "zdrowy"
  ]
  node [
    id 1016
    label "zdrowotnie"
  ]
  node [
    id 1017
    label "dobry"
  ]
  node [
    id 1018
    label "prozdrowotny"
  ]
  node [
    id 1019
    label "dobrze"
  ]
  node [
    id 1020
    label "zdrowo"
  ]
  node [
    id 1021
    label "wyzdrowienie"
  ]
  node [
    id 1022
    label "wyleczenie_si&#281;"
  ]
  node [
    id 1023
    label "uzdrowienie"
  ]
  node [
    id 1024
    label "silny"
  ]
  node [
    id 1025
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 1026
    label "normalny"
  ]
  node [
    id 1027
    label "rozs&#261;dny"
  ]
  node [
    id 1028
    label "korzystny"
  ]
  node [
    id 1029
    label "zdrowienie"
  ]
  node [
    id 1030
    label "solidny"
  ]
  node [
    id 1031
    label "uzdrawianie"
  ]
  node [
    id 1032
    label "dobroczynny"
  ]
  node [
    id 1033
    label "czw&#243;rka"
  ]
  node [
    id 1034
    label "spokojny"
  ]
  node [
    id 1035
    label "skuteczny"
  ]
  node [
    id 1036
    label "&#347;mieszny"
  ]
  node [
    id 1037
    label "mi&#322;y"
  ]
  node [
    id 1038
    label "grzeczny"
  ]
  node [
    id 1039
    label "powitanie"
  ]
  node [
    id 1040
    label "ca&#322;y"
  ]
  node [
    id 1041
    label "zwrot"
  ]
  node [
    id 1042
    label "pomy&#347;lny"
  ]
  node [
    id 1043
    label "moralny"
  ]
  node [
    id 1044
    label "drogi"
  ]
  node [
    id 1045
    label "pozytywny"
  ]
  node [
    id 1046
    label "pos&#322;uszny"
  ]
  node [
    id 1047
    label "niezb&#281;dnie"
  ]
  node [
    id 1048
    label "konieczny"
  ]
  node [
    id 1049
    label "necessarily"
  ]
  node [
    id 1050
    label "GOPR"
  ]
  node [
    id 1051
    label "us&#322;uga_spo&#322;eczna"
  ]
  node [
    id 1052
    label "intencjonalny"
  ]
  node [
    id 1053
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1054
    label "niedorozw&#243;j"
  ]
  node [
    id 1055
    label "szczeg&#243;lny"
  ]
  node [
    id 1056
    label "specjalnie"
  ]
  node [
    id 1057
    label "nieetatowy"
  ]
  node [
    id 1058
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1059
    label "nienormalny"
  ]
  node [
    id 1060
    label "umy&#347;lnie"
  ]
  node [
    id 1061
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 1062
    label "leczniczo"
  ]
  node [
    id 1063
    label "specjalistycznie"
  ]
  node [
    id 1064
    label "fachowo"
  ]
  node [
    id 1065
    label "fachowy"
  ]
  node [
    id 1066
    label "zgodnie"
  ]
  node [
    id 1067
    label "zbie&#380;ny"
  ]
  node [
    id 1068
    label "racjonalny"
  ]
  node [
    id 1069
    label "u&#380;yteczny"
  ]
  node [
    id 1070
    label "praktycznie"
  ]
  node [
    id 1071
    label "oficjalnie"
  ]
  node [
    id 1072
    label "lekarski"
  ]
  node [
    id 1073
    label "carat"
  ]
  node [
    id 1074
    label "bia&#322;y_murzyn"
  ]
  node [
    id 1075
    label "Rosjanin"
  ]
  node [
    id 1076
    label "bia&#322;e"
  ]
  node [
    id 1077
    label "jasnosk&#243;ry"
  ]
  node [
    id 1078
    label "bia&#322;y_taniec"
  ]
  node [
    id 1079
    label "dzia&#322;acz"
  ]
  node [
    id 1080
    label "bezbarwny"
  ]
  node [
    id 1081
    label "siwy"
  ]
  node [
    id 1082
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 1083
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1084
    label "Polak"
  ]
  node [
    id 1085
    label "bia&#322;o"
  ]
  node [
    id 1086
    label "typ_orientalny"
  ]
  node [
    id 1087
    label "libera&#322;"
  ]
  node [
    id 1088
    label "czysty"
  ]
  node [
    id 1089
    label "&#347;nie&#380;nie"
  ]
  node [
    id 1090
    label "konserwatysta"
  ]
  node [
    id 1091
    label "&#347;nie&#380;no"
  ]
  node [
    id 1092
    label "bia&#322;as"
  ]
  node [
    id 1093
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 1094
    label "blady"
  ]
  node [
    id 1095
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 1096
    label "nacjonalista"
  ]
  node [
    id 1097
    label "jasny"
  ]
  node [
    id 1098
    label "podobny"
  ]
  node [
    id 1099
    label "paramedycznie"
  ]
  node [
    id 1100
    label "informowa&#263;"
  ]
  node [
    id 1101
    label "deliver"
  ]
  node [
    id 1102
    label "utrzymywa&#263;"
  ]
  node [
    id 1103
    label "argue"
  ]
  node [
    id 1104
    label "podtrzymywa&#263;"
  ]
  node [
    id 1105
    label "twierdzi&#263;"
  ]
  node [
    id 1106
    label "corroborate"
  ]
  node [
    id 1107
    label "trzyma&#263;"
  ]
  node [
    id 1108
    label "panowa&#263;"
  ]
  node [
    id 1109
    label "defy"
  ]
  node [
    id 1110
    label "cope"
  ]
  node [
    id 1111
    label "broni&#263;"
  ]
  node [
    id 1112
    label "sprawowa&#263;"
  ]
  node [
    id 1113
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 1114
    label "zachowywa&#263;"
  ]
  node [
    id 1115
    label "powiada&#263;"
  ]
  node [
    id 1116
    label "komunikowa&#263;"
  ]
  node [
    id 1117
    label "inform"
  ]
  node [
    id 1118
    label "get"
  ]
  node [
    id 1119
    label "niezw&#322;ocznie"
  ]
  node [
    id 1120
    label "natychmiastowy"
  ]
  node [
    id 1121
    label "natychmiast"
  ]
  node [
    id 1122
    label "szybki"
  ]
  node [
    id 1123
    label "roz&#322;adunek"
  ]
  node [
    id 1124
    label "sprz&#281;t"
  ]
  node [
    id 1125
    label "jednoszynowy"
  ]
  node [
    id 1126
    label "unos"
  ]
  node [
    id 1127
    label "traffic"
  ]
  node [
    id 1128
    label "prze&#322;adunek"
  ]
  node [
    id 1129
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 1130
    label "komunikacja"
  ]
  node [
    id 1131
    label "tyfon"
  ]
  node [
    id 1132
    label "zawarto&#347;&#263;"
  ]
  node [
    id 1133
    label "towar"
  ]
  node [
    id 1134
    label "gospodarka"
  ]
  node [
    id 1135
    label "za&#322;adunek"
  ]
  node [
    id 1136
    label "transportation_system"
  ]
  node [
    id 1137
    label "explicite"
  ]
  node [
    id 1138
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 1139
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1140
    label "wydeptywanie"
  ]
  node [
    id 1141
    label "wydeptanie"
  ]
  node [
    id 1142
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 1143
    label "implicite"
  ]
  node [
    id 1144
    label "ekspedytor"
  ]
  node [
    id 1145
    label "liga"
  ]
  node [
    id 1146
    label "asymilowanie"
  ]
  node [
    id 1147
    label "asymilowa&#263;"
  ]
  node [
    id 1148
    label "Entuzjastki"
  ]
  node [
    id 1149
    label "kompozycja"
  ]
  node [
    id 1150
    label "Terranie"
  ]
  node [
    id 1151
    label "category"
  ]
  node [
    id 1152
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1153
    label "cz&#261;steczka"
  ]
  node [
    id 1154
    label "stage_set"
  ]
  node [
    id 1155
    label "specgrupa"
  ]
  node [
    id 1156
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1157
    label "Eurogrupa"
  ]
  node [
    id 1158
    label "harcerze_starsi"
  ]
  node [
    id 1159
    label "temat"
  ]
  node [
    id 1160
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1161
    label "wn&#281;trze"
  ]
  node [
    id 1162
    label "metka"
  ]
  node [
    id 1163
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 1164
    label "szprycowa&#263;"
  ]
  node [
    id 1165
    label "naszprycowa&#263;"
  ]
  node [
    id 1166
    label "rzuca&#263;"
  ]
  node [
    id 1167
    label "tandeta"
  ]
  node [
    id 1168
    label "obr&#243;t_handlowy"
  ]
  node [
    id 1169
    label "wyr&#243;b"
  ]
  node [
    id 1170
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 1171
    label "rzuci&#263;"
  ]
  node [
    id 1172
    label "naszprycowanie"
  ]
  node [
    id 1173
    label "tkanina"
  ]
  node [
    id 1174
    label "szprycowanie"
  ]
  node [
    id 1175
    label "za&#322;adownia"
  ]
  node [
    id 1176
    label "&#322;&#243;dzki"
  ]
  node [
    id 1177
    label "narkobiznes"
  ]
  node [
    id 1178
    label "rzucenie"
  ]
  node [
    id 1179
    label "rzucanie"
  ]
  node [
    id 1180
    label "sprz&#281;cior"
  ]
  node [
    id 1181
    label "penis"
  ]
  node [
    id 1182
    label "kolekcja"
  ]
  node [
    id 1183
    label "furniture"
  ]
  node [
    id 1184
    label "sprz&#281;cik"
  ]
  node [
    id 1185
    label "equipment"
  ]
  node [
    id 1186
    label "relokacja"
  ]
  node [
    id 1187
    label "kolej"
  ]
  node [
    id 1188
    label "raport"
  ]
  node [
    id 1189
    label "spis"
  ]
  node [
    id 1190
    label "bilet"
  ]
  node [
    id 1191
    label "sygnalizator"
  ]
  node [
    id 1192
    label "ci&#281;&#380;ar"
  ]
  node [
    id 1193
    label "&#322;adunek"
  ]
  node [
    id 1194
    label "inwentarz"
  ]
  node [
    id 1195
    label "rynek"
  ]
  node [
    id 1196
    label "mieszkalnictwo"
  ]
  node [
    id 1197
    label "agregat_ekonomiczny"
  ]
  node [
    id 1198
    label "farmaceutyka"
  ]
  node [
    id 1199
    label "produkowanie"
  ]
  node [
    id 1200
    label "rolnictwo"
  ]
  node [
    id 1201
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 1202
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 1203
    label "obronno&#347;&#263;"
  ]
  node [
    id 1204
    label "sektor_prywatny"
  ]
  node [
    id 1205
    label "sch&#322;adza&#263;"
  ]
  node [
    id 1206
    label "czerwona_strefa"
  ]
  node [
    id 1207
    label "pole"
  ]
  node [
    id 1208
    label "sektor_publiczny"
  ]
  node [
    id 1209
    label "bankowo&#347;&#263;"
  ]
  node [
    id 1210
    label "gospodarowanie"
  ]
  node [
    id 1211
    label "obora"
  ]
  node [
    id 1212
    label "gospodarka_wodna"
  ]
  node [
    id 1213
    label "gospodarka_le&#347;na"
  ]
  node [
    id 1214
    label "gospodarowa&#263;"
  ]
  node [
    id 1215
    label "fabryka"
  ]
  node [
    id 1216
    label "wytw&#243;rnia"
  ]
  node [
    id 1217
    label "stodo&#322;a"
  ]
  node [
    id 1218
    label "przemys&#322;"
  ]
  node [
    id 1219
    label "spichlerz"
  ]
  node [
    id 1220
    label "sch&#322;adzanie"
  ]
  node [
    id 1221
    label "administracja"
  ]
  node [
    id 1222
    label "sch&#322;odzenie"
  ]
  node [
    id 1223
    label "zasada"
  ]
  node [
    id 1224
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 1225
    label "regulacja_cen"
  ]
  node [
    id 1226
    label "szkolnictwo"
  ]
  node [
    id 1227
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 1228
    label "mentalno&#347;&#263;"
  ]
  node [
    id 1229
    label "superego"
  ]
  node [
    id 1230
    label "zaistnie&#263;"
  ]
  node [
    id 1231
    label "trim"
  ]
  node [
    id 1232
    label "poby&#263;"
  ]
  node [
    id 1233
    label "wytrzyma&#263;"
  ]
  node [
    id 1234
    label "pozosta&#263;"
  ]
  node [
    id 1235
    label "go&#347;&#263;"
  ]
  node [
    id 1236
    label "hamper"
  ]
  node [
    id 1237
    label "spasm"
  ]
  node [
    id 1238
    label "mrozi&#263;"
  ]
  node [
    id 1239
    label "pora&#380;a&#263;"
  ]
  node [
    id 1240
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 1241
    label "fleksja"
  ]
  node [
    id 1242
    label "coupling"
  ]
  node [
    id 1243
    label "tryb"
  ]
  node [
    id 1244
    label "czasownik"
  ]
  node [
    id 1245
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 1246
    label "orz&#281;sek"
  ]
  node [
    id 1247
    label "Szekspir"
  ]
  node [
    id 1248
    label "Mickiewicz"
  ]
  node [
    id 1249
    label "cierpienie"
  ]
  node [
    id 1250
    label "Ohio"
  ]
  node [
    id 1251
    label "wci&#281;cie"
  ]
  node [
    id 1252
    label "Nowy_York"
  ]
  node [
    id 1253
    label "samopoczucie"
  ]
  node [
    id 1254
    label "Illinois"
  ]
  node [
    id 1255
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1256
    label "state"
  ]
  node [
    id 1257
    label "Jukatan"
  ]
  node [
    id 1258
    label "Kalifornia"
  ]
  node [
    id 1259
    label "Wirginia"
  ]
  node [
    id 1260
    label "wektor"
  ]
  node [
    id 1261
    label "by&#263;"
  ]
  node [
    id 1262
    label "Teksas"
  ]
  node [
    id 1263
    label "Goa"
  ]
  node [
    id 1264
    label "Waszyngton"
  ]
  node [
    id 1265
    label "Massachusetts"
  ]
  node [
    id 1266
    label "Alaska"
  ]
  node [
    id 1267
    label "Arakan"
  ]
  node [
    id 1268
    label "Hawaje"
  ]
  node [
    id 1269
    label "Maryland"
  ]
  node [
    id 1270
    label "Michigan"
  ]
  node [
    id 1271
    label "Arizona"
  ]
  node [
    id 1272
    label "Georgia"
  ]
  node [
    id 1273
    label "Pensylwania"
  ]
  node [
    id 1274
    label "Luizjana"
  ]
  node [
    id 1275
    label "Nowy_Meksyk"
  ]
  node [
    id 1276
    label "Alabama"
  ]
  node [
    id 1277
    label "Kansas"
  ]
  node [
    id 1278
    label "Oregon"
  ]
  node [
    id 1279
    label "Floryda"
  ]
  node [
    id 1280
    label "Oklahoma"
  ]
  node [
    id 1281
    label "jednostka_administracyjna"
  ]
  node [
    id 1282
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1283
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 1284
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 1285
    label "indentation"
  ]
  node [
    id 1286
    label "zjedzenie"
  ]
  node [
    id 1287
    label "snub"
  ]
  node [
    id 1288
    label "plac"
  ]
  node [
    id 1289
    label "location"
  ]
  node [
    id 1290
    label "uwaga"
  ]
  node [
    id 1291
    label "status"
  ]
  node [
    id 1292
    label "rz&#261;d"
  ]
  node [
    id 1293
    label "przek&#322;adaniec"
  ]
  node [
    id 1294
    label "covering"
  ]
  node [
    id 1295
    label "podwarstwa"
  ]
  node [
    id 1296
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1297
    label "dyspozycja"
  ]
  node [
    id 1298
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1299
    label "zwrot_wektora"
  ]
  node [
    id 1300
    label "vector"
  ]
  node [
    id 1301
    label "parametryzacja"
  ]
  node [
    id 1302
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 1303
    label "sprawa"
  ]
  node [
    id 1304
    label "ust&#281;p"
  ]
  node [
    id 1305
    label "plan"
  ]
  node [
    id 1306
    label "problemat"
  ]
  node [
    id 1307
    label "plamka"
  ]
  node [
    id 1308
    label "stopie&#324;_pisma"
  ]
  node [
    id 1309
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1310
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1311
    label "mark"
  ]
  node [
    id 1312
    label "prosta"
  ]
  node [
    id 1313
    label "problematyka"
  ]
  node [
    id 1314
    label "zapunktowa&#263;"
  ]
  node [
    id 1315
    label "podpunkt"
  ]
  node [
    id 1316
    label "USA"
  ]
  node [
    id 1317
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1318
    label "Polinezja"
  ]
  node [
    id 1319
    label "Birma"
  ]
  node [
    id 1320
    label "Indie_Portugalskie"
  ]
  node [
    id 1321
    label "Belize"
  ]
  node [
    id 1322
    label "Meksyk"
  ]
  node [
    id 1323
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1324
    label "Aleuty"
  ]
  node [
    id 1325
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1326
    label "equal"
  ]
  node [
    id 1327
    label "trwa&#263;"
  ]
  node [
    id 1328
    label "chodzi&#263;"
  ]
  node [
    id 1329
    label "si&#281;ga&#263;"
  ]
  node [
    id 1330
    label "obecno&#347;&#263;"
  ]
  node [
    id 1331
    label "stand"
  ]
  node [
    id 1332
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1333
    label "uczestniczy&#263;"
  ]
  node [
    id 1334
    label "zawrze&#263;"
  ]
  node [
    id 1335
    label "zawrzenie"
  ]
  node [
    id 1336
    label "pilnie"
  ]
  node [
    id 1337
    label "nieoczekiwany"
  ]
  node [
    id 1338
    label "raptownie"
  ]
  node [
    id 1339
    label "intensywny"
  ]
  node [
    id 1340
    label "prosty"
  ]
  node [
    id 1341
    label "kr&#243;tki"
  ]
  node [
    id 1342
    label "temperamentny"
  ]
  node [
    id 1343
    label "bystrolotny"
  ]
  node [
    id 1344
    label "dynamiczny"
  ]
  node [
    id 1345
    label "szybko"
  ]
  node [
    id 1346
    label "sprawny"
  ]
  node [
    id 1347
    label "bezpo&#347;redni"
  ]
  node [
    id 1348
    label "energiczny"
  ]
  node [
    id 1349
    label "niespodziewanie"
  ]
  node [
    id 1350
    label "zaskakuj&#261;cy"
  ]
  node [
    id 1351
    label "nieprzewidzianie"
  ]
  node [
    id 1352
    label "raptowny"
  ]
  node [
    id 1353
    label "wykipienie"
  ]
  node [
    id 1354
    label "nasilenie_si&#281;"
  ]
  node [
    id 1355
    label "wrzenie"
  ]
  node [
    id 1356
    label "ugotowanie_si&#281;"
  ]
  node [
    id 1357
    label "kipienie"
  ]
  node [
    id 1358
    label "zareagowanie"
  ]
  node [
    id 1359
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1360
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 1361
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1362
    label "insert"
  ]
  node [
    id 1363
    label "incorporate"
  ]
  node [
    id 1364
    label "pozna&#263;"
  ]
  node [
    id 1365
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 1366
    label "boil"
  ]
  node [
    id 1367
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 1368
    label "zamkn&#261;&#263;"
  ]
  node [
    id 1369
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 1370
    label "ustali&#263;"
  ]
  node [
    id 1371
    label "admit"
  ]
  node [
    id 1372
    label "wezbra&#263;"
  ]
  node [
    id 1373
    label "embrace"
  ]
  node [
    id 1374
    label "pilny"
  ]
  node [
    id 1375
    label "intensywnie"
  ]
  node [
    id 1376
    label "pilno"
  ]
  node [
    id 1377
    label "zawisa&#263;"
  ]
  node [
    id 1378
    label "emergency"
  ]
  node [
    id 1379
    label "szachowanie"
  ]
  node [
    id 1380
    label "zagrozi&#263;"
  ]
  node [
    id 1381
    label "zaistnienie"
  ]
  node [
    id 1382
    label "uprzedzenie"
  ]
  node [
    id 1383
    label "zawisanie"
  ]
  node [
    id 1384
    label "czarny_punkt"
  ]
  node [
    id 1385
    label "zaszachowanie"
  ]
  node [
    id 1386
    label "m&#322;ot"
  ]
  node [
    id 1387
    label "znak"
  ]
  node [
    id 1388
    label "drzewo"
  ]
  node [
    id 1389
    label "attribute"
  ]
  node [
    id 1390
    label "marka"
  ]
  node [
    id 1391
    label "niech&#281;&#263;"
  ]
  node [
    id 1392
    label "bias"
  ]
  node [
    id 1393
    label "anticipation"
  ]
  node [
    id 1394
    label "przygotowanie"
  ]
  node [
    id 1395
    label "progress"
  ]
  node [
    id 1396
    label "poinformowanie"
  ]
  node [
    id 1397
    label "og&#322;oszenie"
  ]
  node [
    id 1398
    label "powstanie"
  ]
  node [
    id 1399
    label "apparition"
  ]
  node [
    id 1400
    label "nap&#281;dzenie"
  ]
  node [
    id 1401
    label "geneza"
  ]
  node [
    id 1402
    label "niebezpiecze&#324;stwo"
  ]
  node [
    id 1403
    label "zaszachowa&#263;"
  ]
  node [
    id 1404
    label "threaten"
  ]
  node [
    id 1405
    label "szachowa&#263;"
  ]
  node [
    id 1406
    label "uprzedzi&#263;"
  ]
  node [
    id 1407
    label "menace"
  ]
  node [
    id 1408
    label "pojawianie_si&#281;"
  ]
  node [
    id 1409
    label "umieranie"
  ]
  node [
    id 1410
    label "nieruchomienie"
  ]
  node [
    id 1411
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1412
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1413
    label "nieruchomie&#263;"
  ]
  node [
    id 1414
    label "ko&#324;czy&#263;"
  ]
  node [
    id 1415
    label "matowanie"
  ]
  node [
    id 1416
    label "atakowanie"
  ]
  node [
    id 1417
    label "ruszenie_si&#281;"
  ]
  node [
    id 1418
    label "zaatakowanie"
  ]
  node [
    id 1419
    label "zamatowanie"
  ]
  node [
    id 1420
    label "blisko"
  ]
  node [
    id 1421
    label "znajomy"
  ]
  node [
    id 1422
    label "zwi&#261;zany"
  ]
  node [
    id 1423
    label "przesz&#322;y"
  ]
  node [
    id 1424
    label "zbli&#380;enie"
  ]
  node [
    id 1425
    label "oddalony"
  ]
  node [
    id 1426
    label "dok&#322;adny"
  ]
  node [
    id 1427
    label "nieodleg&#322;y"
  ]
  node [
    id 1428
    label "przysz&#322;y"
  ]
  node [
    id 1429
    label "gotowy"
  ]
  node [
    id 1430
    label "ma&#322;y"
  ]
  node [
    id 1431
    label "jednowyrazowy"
  ]
  node [
    id 1432
    label "s&#322;aby"
  ]
  node [
    id 1433
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1434
    label "kr&#243;tko"
  ]
  node [
    id 1435
    label "drobny"
  ]
  node [
    id 1436
    label "brak"
  ]
  node [
    id 1437
    label "z&#322;y"
  ]
  node [
    id 1438
    label "nietrze&#378;wy"
  ]
  node [
    id 1439
    label "czekanie"
  ]
  node [
    id 1440
    label "martwy"
  ]
  node [
    id 1441
    label "m&#243;c"
  ]
  node [
    id 1442
    label "gotowo"
  ]
  node [
    id 1443
    label "przygotowywanie"
  ]
  node [
    id 1444
    label "dyspozycyjny"
  ]
  node [
    id 1445
    label "zalany"
  ]
  node [
    id 1446
    label "nieuchronny"
  ]
  node [
    id 1447
    label "doj&#347;cie"
  ]
  node [
    id 1448
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 1449
    label "dok&#322;adnie"
  ]
  node [
    id 1450
    label "silnie"
  ]
  node [
    id 1451
    label "zetkni&#281;cie"
  ]
  node [
    id 1452
    label "po&#380;ycie"
  ]
  node [
    id 1453
    label "podnieci&#263;"
  ]
  node [
    id 1454
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 1455
    label "numer"
  ]
  node [
    id 1456
    label "closeup"
  ]
  node [
    id 1457
    label "podniecenie"
  ]
  node [
    id 1458
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1459
    label "konfidencja"
  ]
  node [
    id 1460
    label "seks"
  ]
  node [
    id 1461
    label "podniecanie"
  ]
  node [
    id 1462
    label "imisja"
  ]
  node [
    id 1463
    label "pobratymstwo"
  ]
  node [
    id 1464
    label "rozmna&#380;anie"
  ]
  node [
    id 1465
    label "proximity"
  ]
  node [
    id 1466
    label "uj&#281;cie"
  ]
  node [
    id 1467
    label "ruch_frykcyjny"
  ]
  node [
    id 1468
    label "na_pieska"
  ]
  node [
    id 1469
    label "pozycja_misjonarska"
  ]
  node [
    id 1470
    label "przemieszczenie"
  ]
  node [
    id 1471
    label "dru&#380;ba"
  ]
  node [
    id 1472
    label "approach"
  ]
  node [
    id 1473
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 1474
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1475
    label "gra_wst&#281;pna"
  ]
  node [
    id 1476
    label "znajomo&#347;&#263;"
  ]
  node [
    id 1477
    label "erotyka"
  ]
  node [
    id 1478
    label "baraszki"
  ]
  node [
    id 1479
    label "po&#380;&#261;danie"
  ]
  node [
    id 1480
    label "wzw&#243;d"
  ]
  node [
    id 1481
    label "podnieca&#263;"
  ]
  node [
    id 1482
    label "zro&#347;ni&#281;cie_si&#281;"
  ]
  node [
    id 1483
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1484
    label "sprecyzowanie"
  ]
  node [
    id 1485
    label "precyzyjny"
  ]
  node [
    id 1486
    label "miliamperomierz"
  ]
  node [
    id 1487
    label "precyzowanie"
  ]
  node [
    id 1488
    label "rzetelny"
  ]
  node [
    id 1489
    label "wapniak"
  ]
  node [
    id 1490
    label "os&#322;abia&#263;"
  ]
  node [
    id 1491
    label "hominid"
  ]
  node [
    id 1492
    label "podw&#322;adny"
  ]
  node [
    id 1493
    label "os&#322;abianie"
  ]
  node [
    id 1494
    label "dwun&#243;g"
  ]
  node [
    id 1495
    label "nasada"
  ]
  node [
    id 1496
    label "wz&#243;r"
  ]
  node [
    id 1497
    label "senior"
  ]
  node [
    id 1498
    label "Adam"
  ]
  node [
    id 1499
    label "polifag"
  ]
  node [
    id 1500
    label "znany"
  ]
  node [
    id 1501
    label "zapoznanie"
  ]
  node [
    id 1502
    label "sw&#243;j"
  ]
  node [
    id 1503
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 1504
    label "zapoznanie_si&#281;"
  ]
  node [
    id 1505
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1506
    label "znajomek"
  ]
  node [
    id 1507
    label "zapoznawanie"
  ]
  node [
    id 1508
    label "przyj&#281;ty"
  ]
  node [
    id 1509
    label "pewien"
  ]
  node [
    id 1510
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 1511
    label "znajomo"
  ]
  node [
    id 1512
    label "za_pan_brat"
  ]
  node [
    id 1513
    label "kolejny"
  ]
  node [
    id 1514
    label "nieznaczny"
  ]
  node [
    id 1515
    label "przeci&#281;tny"
  ]
  node [
    id 1516
    label "wstydliwy"
  ]
  node [
    id 1517
    label "niewa&#380;ny"
  ]
  node [
    id 1518
    label "ch&#322;opiec"
  ]
  node [
    id 1519
    label "m&#322;ody"
  ]
  node [
    id 1520
    label "ma&#322;o"
  ]
  node [
    id 1521
    label "marny"
  ]
  node [
    id 1522
    label "nieliczny"
  ]
  node [
    id 1523
    label "n&#281;dznie"
  ]
  node [
    id 1524
    label "oderwany"
  ]
  node [
    id 1525
    label "daleki"
  ]
  node [
    id 1526
    label "daleko"
  ]
  node [
    id 1527
    label "miniony"
  ]
  node [
    id 1528
    label "ostatni"
  ]
  node [
    id 1529
    label "krzepienie"
  ]
  node [
    id 1530
    label "&#380;ywotny"
  ]
  node [
    id 1531
    label "mocny"
  ]
  node [
    id 1532
    label "pokrzepienie"
  ]
  node [
    id 1533
    label "zdecydowany"
  ]
  node [
    id 1534
    label "niepodwa&#380;alny"
  ]
  node [
    id 1535
    label "du&#380;y"
  ]
  node [
    id 1536
    label "mocno"
  ]
  node [
    id 1537
    label "przekonuj&#261;cy"
  ]
  node [
    id 1538
    label "wytrzyma&#322;y"
  ]
  node [
    id 1539
    label "konkretny"
  ]
  node [
    id 1540
    label "meflochina"
  ]
  node [
    id 1541
    label "zajebisty"
  ]
  node [
    id 1542
    label "zak&#322;adka"
  ]
  node [
    id 1543
    label "wyko&#324;czenie"
  ]
  node [
    id 1544
    label "company"
  ]
  node [
    id 1545
    label "instytut"
  ]
  node [
    id 1546
    label "bookmark"
  ]
  node [
    id 1547
    label "fa&#322;da"
  ]
  node [
    id 1548
    label "znacznik"
  ]
  node [
    id 1549
    label "sp&#243;&#322;g&#322;oska"
  ]
  node [
    id 1550
    label "widok"
  ]
  node [
    id 1551
    label "zu&#380;ycie"
  ]
  node [
    id 1552
    label "skonany"
  ]
  node [
    id 1553
    label "zniszczenie"
  ]
  node [
    id 1554
    label "os&#322;abienie"
  ]
  node [
    id 1555
    label "str&#243;j"
  ]
  node [
    id 1556
    label "wymordowanie"
  ]
  node [
    id 1557
    label "murder"
  ]
  node [
    id 1558
    label "pomordowanie"
  ]
  node [
    id 1559
    label "znu&#380;enie"
  ]
  node [
    id 1560
    label "ukszta&#322;towanie"
  ]
  node [
    id 1561
    label "zm&#281;czenie"
  ]
  node [
    id 1562
    label "adjustment"
  ]
  node [
    id 1563
    label "zabicie"
  ]
  node [
    id 1564
    label "zawarcie"
  ]
  node [
    id 1565
    label "warunek"
  ]
  node [
    id 1566
    label "gestia_transportowa"
  ]
  node [
    id 1567
    label "contract"
  ]
  node [
    id 1568
    label "porozumienie"
  ]
  node [
    id 1569
    label "klauzula"
  ]
  node [
    id 1570
    label "Apeks"
  ]
  node [
    id 1571
    label "zasoby"
  ]
  node [
    id 1572
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 1573
    label "zaufanie"
  ]
  node [
    id 1574
    label "Hortex"
  ]
  node [
    id 1575
    label "reengineering"
  ]
  node [
    id 1576
    label "nazwa_w&#322;asna"
  ]
  node [
    id 1577
    label "podmiot_gospodarczy"
  ]
  node [
    id 1578
    label "paczkarnia"
  ]
  node [
    id 1579
    label "Orlen"
  ]
  node [
    id 1580
    label "interes"
  ]
  node [
    id 1581
    label "Google"
  ]
  node [
    id 1582
    label "Canon"
  ]
  node [
    id 1583
    label "Pewex"
  ]
  node [
    id 1584
    label "MAN_SE"
  ]
  node [
    id 1585
    label "Spo&#322;em"
  ]
  node [
    id 1586
    label "networking"
  ]
  node [
    id 1587
    label "MAC"
  ]
  node [
    id 1588
    label "zasoby_ludzkie"
  ]
  node [
    id 1589
    label "Baltona"
  ]
  node [
    id 1590
    label "Orbis"
  ]
  node [
    id 1591
    label "biurowiec"
  ]
  node [
    id 1592
    label "HP"
  ]
  node [
    id 1593
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 1594
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 1595
    label "Ossolineum"
  ]
  node [
    id 1596
    label "plac&#243;wka"
  ]
  node [
    id 1597
    label "institute"
  ]
  node [
    id 1598
    label "opieku&#324;cze_skrzyd&#322;a"
  ]
  node [
    id 1599
    label "staranie"
  ]
  node [
    id 1600
    label "nadz&#243;r"
  ]
  node [
    id 1601
    label "&#347;rodek"
  ]
  node [
    id 1602
    label "darowizna"
  ]
  node [
    id 1603
    label "doch&#243;d"
  ]
  node [
    id 1604
    label "telefon_zaufania"
  ]
  node [
    id 1605
    label "pomocnik"
  ]
  node [
    id 1606
    label "zgodzi&#263;"
  ]
  node [
    id 1607
    label "examination"
  ]
  node [
    id 1608
    label "usi&#322;owanie"
  ]
  node [
    id 1609
    label "starunek"
  ]
  node [
    id 1610
    label "zdarzony"
  ]
  node [
    id 1611
    label "odpowiednio"
  ]
  node [
    id 1612
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1613
    label "nale&#380;ny"
  ]
  node [
    id 1614
    label "nale&#380;yty"
  ]
  node [
    id 1615
    label "stosownie"
  ]
  node [
    id 1616
    label "odpowiadanie"
  ]
  node [
    id 1617
    label "uprawniony"
  ]
  node [
    id 1618
    label "zasadniczy"
  ]
  node [
    id 1619
    label "taki"
  ]
  node [
    id 1620
    label "charakterystyczny"
  ]
  node [
    id 1621
    label "prawdziwy"
  ]
  node [
    id 1622
    label "ten"
  ]
  node [
    id 1623
    label "powinny"
  ]
  node [
    id 1624
    label "nale&#380;nie"
  ]
  node [
    id 1625
    label "godny"
  ]
  node [
    id 1626
    label "przynale&#380;ny"
  ]
  node [
    id 1627
    label "zadowalaj&#261;cy"
  ]
  node [
    id 1628
    label "nale&#380;ycie"
  ]
  node [
    id 1629
    label "przystojny"
  ]
  node [
    id 1630
    label "reagowanie"
  ]
  node [
    id 1631
    label "pokutowanie"
  ]
  node [
    id 1632
    label "pytanie"
  ]
  node [
    id 1633
    label "odpowiedzialny"
  ]
  node [
    id 1634
    label "winny"
  ]
  node [
    id 1635
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 1636
    label "picie_piwa"
  ]
  node [
    id 1637
    label "parry"
  ]
  node [
    id 1638
    label "fit"
  ]
  node [
    id 1639
    label "rendition"
  ]
  node [
    id 1640
    label "ponoszenie"
  ]
  node [
    id 1641
    label "rozmawianie"
  ]
  node [
    id 1642
    label "stosowny"
  ]
  node [
    id 1643
    label "charakterystycznie"
  ]
  node [
    id 1644
    label "prawdziwie"
  ]
  node [
    id 1645
    label "polskie"
  ]
  node [
    id 1646
    label "norma"
  ]
  node [
    id 1647
    label "prawo"
  ]
  node [
    id 1648
    label "ojciec"
  ]
  node [
    id 1649
    label "rucho"
  ]
  node [
    id 1650
    label "drogowy"
  ]
  node [
    id 1651
    label "dziennik"
  ]
  node [
    id 1652
    label "u"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 52
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 10
    target 757
  ]
  edge [
    source 10
    target 758
  ]
  edge [
    source 10
    target 759
  ]
  edge [
    source 10
    target 760
  ]
  edge [
    source 10
    target 761
  ]
  edge [
    source 10
    target 762
  ]
  edge [
    source 10
    target 763
  ]
  edge [
    source 10
    target 764
  ]
  edge [
    source 10
    target 765
  ]
  edge [
    source 10
    target 766
  ]
  edge [
    source 10
    target 767
  ]
  edge [
    source 10
    target 768
  ]
  edge [
    source 10
    target 769
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 26
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 230
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 222
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 221
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 14
    target 868
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 871
  ]
  edge [
    source 14
    target 872
  ]
  edge [
    source 14
    target 873
  ]
  edge [
    source 14
    target 874
  ]
  edge [
    source 14
    target 875
  ]
  edge [
    source 14
    target 876
  ]
  edge [
    source 14
    target 877
  ]
  edge [
    source 14
    target 878
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 880
  ]
  edge [
    source 14
    target 881
  ]
  edge [
    source 14
    target 882
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 884
  ]
  edge [
    source 14
    target 801
  ]
  edge [
    source 14
    target 885
  ]
  edge [
    source 14
    target 886
  ]
  edge [
    source 14
    target 887
  ]
  edge [
    source 14
    target 888
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 890
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 891
  ]
  edge [
    source 14
    target 892
  ]
  edge [
    source 14
    target 893
  ]
  edge [
    source 14
    target 894
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 906
  ]
  edge [
    source 15
    target 907
  ]
  edge [
    source 15
    target 908
  ]
  edge [
    source 15
    target 909
  ]
  edge [
    source 15
    target 910
  ]
  edge [
    source 15
    target 911
  ]
  edge [
    source 15
    target 912
  ]
  edge [
    source 15
    target 913
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 916
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 918
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 920
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 922
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 940
  ]
  edge [
    source 15
    target 941
  ]
  edge [
    source 15
    target 942
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 944
  ]
  edge [
    source 15
    target 945
  ]
  edge [
    source 15
    target 946
  ]
  edge [
    source 15
    target 947
  ]
  edge [
    source 15
    target 948
  ]
  edge [
    source 15
    target 949
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 257
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 15
    target 971
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 972
  ]
  edge [
    source 15
    target 973
  ]
  edge [
    source 15
    target 974
  ]
  edge [
    source 15
    target 975
  ]
  edge [
    source 15
    target 976
  ]
  edge [
    source 15
    target 977
  ]
  edge [
    source 15
    target 978
  ]
  edge [
    source 15
    target 979
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 981
  ]
  edge [
    source 15
    target 514
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 15
    target 986
  ]
  edge [
    source 15
    target 987
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 989
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 15
    target 544
  ]
  edge [
    source 15
    target 379
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 992
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 993
  ]
  edge [
    source 15
    target 994
  ]
  edge [
    source 15
    target 995
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 15
    target 997
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 15
    target 999
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 15
    target 1002
  ]
  edge [
    source 15
    target 1003
  ]
  edge [
    source 15
    target 1004
  ]
  edge [
    source 15
    target 1005
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 486
  ]
  edge [
    source 15
    target 1006
  ]
  edge [
    source 15
    target 1007
  ]
  edge [
    source 15
    target 322
  ]
  edge [
    source 15
    target 1008
  ]
  edge [
    source 15
    target 255
  ]
  edge [
    source 15
    target 1009
  ]
  edge [
    source 15
    target 1010
  ]
  edge [
    source 15
    target 1011
  ]
  edge [
    source 15
    target 1012
  ]
  edge [
    source 15
    target 1013
  ]
  edge [
    source 15
    target 1014
  ]
  edge [
    source 15
    target 301
  ]
  edge [
    source 15
    target 315
  ]
  edge [
    source 15
    target 548
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 28
  ]
  edge [
    source 16
    target 29
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 1015
  ]
  edge [
    source 16
    target 1016
  ]
  edge [
    source 16
    target 1017
  ]
  edge [
    source 16
    target 1018
  ]
  edge [
    source 16
    target 1019
  ]
  edge [
    source 16
    target 1020
  ]
  edge [
    source 16
    target 1021
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 1022
  ]
  edge [
    source 16
    target 1023
  ]
  edge [
    source 16
    target 1024
  ]
  edge [
    source 16
    target 1025
  ]
  edge [
    source 16
    target 1026
  ]
  edge [
    source 16
    target 1027
  ]
  edge [
    source 16
    target 1028
  ]
  edge [
    source 16
    target 1029
  ]
  edge [
    source 16
    target 1030
  ]
  edge [
    source 16
    target 1031
  ]
  edge [
    source 16
    target 1032
  ]
  edge [
    source 16
    target 1033
  ]
  edge [
    source 16
    target 1034
  ]
  edge [
    source 16
    target 1035
  ]
  edge [
    source 16
    target 1036
  ]
  edge [
    source 16
    target 1037
  ]
  edge [
    source 16
    target 1038
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 16
    target 1039
  ]
  edge [
    source 16
    target 1040
  ]
  edge [
    source 16
    target 1041
  ]
  edge [
    source 16
    target 1042
  ]
  edge [
    source 16
    target 1043
  ]
  edge [
    source 16
    target 1044
  ]
  edge [
    source 16
    target 1045
  ]
  edge [
    source 16
    target 1046
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1053
  ]
  edge [
    source 20
    target 1054
  ]
  edge [
    source 20
    target 1055
  ]
  edge [
    source 20
    target 1056
  ]
  edge [
    source 20
    target 1057
  ]
  edge [
    source 20
    target 1058
  ]
  edge [
    source 20
    target 1059
  ]
  edge [
    source 20
    target 1060
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 20
    target 1061
  ]
  edge [
    source 20
    target 1062
  ]
  edge [
    source 20
    target 1063
  ]
  edge [
    source 20
    target 1064
  ]
  edge [
    source 20
    target 1065
  ]
  edge [
    source 20
    target 1066
  ]
  edge [
    source 20
    target 1067
  ]
  edge [
    source 20
    target 1034
  ]
  edge [
    source 20
    target 1017
  ]
  edge [
    source 20
    target 1068
  ]
  edge [
    source 20
    target 1069
  ]
  edge [
    source 20
    target 1070
  ]
  edge [
    source 20
    target 1071
  ]
  edge [
    source 20
    target 1072
  ]
  edge [
    source 20
    target 1073
  ]
  edge [
    source 20
    target 1074
  ]
  edge [
    source 20
    target 1075
  ]
  edge [
    source 20
    target 577
  ]
  edge [
    source 20
    target 1076
  ]
  edge [
    source 20
    target 1077
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 1078
  ]
  edge [
    source 20
    target 1079
  ]
  edge [
    source 20
    target 1080
  ]
  edge [
    source 20
    target 1081
  ]
  edge [
    source 20
    target 1082
  ]
  edge [
    source 20
    target 1083
  ]
  edge [
    source 20
    target 1084
  ]
  edge [
    source 20
    target 1085
  ]
  edge [
    source 20
    target 1086
  ]
  edge [
    source 20
    target 1087
  ]
  edge [
    source 20
    target 1088
  ]
  edge [
    source 20
    target 1089
  ]
  edge [
    source 20
    target 1090
  ]
  edge [
    source 20
    target 1091
  ]
  edge [
    source 20
    target 1092
  ]
  edge [
    source 20
    target 1093
  ]
  edge [
    source 20
    target 1094
  ]
  edge [
    source 20
    target 1095
  ]
  edge [
    source 20
    target 1096
  ]
  edge [
    source 20
    target 1097
  ]
  edge [
    source 20
    target 1098
  ]
  edge [
    source 20
    target 1099
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 868
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 784
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 73
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 21
    target 1110
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1112
  ]
  edge [
    source 21
    target 1113
  ]
  edge [
    source 21
    target 1114
  ]
  edge [
    source 21
    target 1115
  ]
  edge [
    source 21
    target 1116
  ]
  edge [
    source 21
    target 1117
  ]
  edge [
    source 21
    target 1118
  ]
  edge [
    source 21
    target 164
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1119
  ]
  edge [
    source 22
    target 1120
  ]
  edge [
    source 22
    target 1121
  ]
  edge [
    source 22
    target 1122
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1123
  ]
  edge [
    source 23
    target 1124
  ]
  edge [
    source 23
    target 454
  ]
  edge [
    source 23
    target 1125
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1127
  ]
  edge [
    source 23
    target 1128
  ]
  edge [
    source 23
    target 1129
  ]
  edge [
    source 23
    target 907
  ]
  edge [
    source 23
    target 1130
  ]
  edge [
    source 23
    target 1131
  ]
  edge [
    source 23
    target 1132
  ]
  edge [
    source 23
    target 275
  ]
  edge [
    source 23
    target 1133
  ]
  edge [
    source 23
    target 1134
  ]
  edge [
    source 23
    target 1135
  ]
  edge [
    source 23
    target 1006
  ]
  edge [
    source 23
    target 906
  ]
  edge [
    source 23
    target 1007
  ]
  edge [
    source 23
    target 322
  ]
  edge [
    source 23
    target 1008
  ]
  edge [
    source 23
    target 1136
  ]
  edge [
    source 23
    target 1137
  ]
  edge [
    source 23
    target 1138
  ]
  edge [
    source 23
    target 1139
  ]
  edge [
    source 23
    target 1140
  ]
  edge [
    source 23
    target 294
  ]
  edge [
    source 23
    target 1141
  ]
  edge [
    source 23
    target 1142
  ]
  edge [
    source 23
    target 1143
  ]
  edge [
    source 23
    target 1144
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 1145
  ]
  edge [
    source 23
    target 342
  ]
  edge [
    source 23
    target 1146
  ]
  edge [
    source 23
    target 347
  ]
  edge [
    source 23
    target 230
  ]
  edge [
    source 23
    target 1147
  ]
  edge [
    source 23
    target 821
  ]
  edge [
    source 23
    target 1148
  ]
  edge [
    source 23
    target 265
  ]
  edge [
    source 23
    target 1149
  ]
  edge [
    source 23
    target 1150
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 1151
  ]
  edge [
    source 23
    target 829
  ]
  edge [
    source 23
    target 1152
  ]
  edge [
    source 23
    target 1153
  ]
  edge [
    source 23
    target 1154
  ]
  edge [
    source 23
    target 356
  ]
  edge [
    source 23
    target 1155
  ]
  edge [
    source 23
    target 1156
  ]
  edge [
    source 23
    target 264
  ]
  edge [
    source 23
    target 278
  ]
  edge [
    source 23
    target 1157
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 504
  ]
  edge [
    source 23
    target 1158
  ]
  edge [
    source 23
    target 1159
  ]
  edge [
    source 23
    target 547
  ]
  edge [
    source 23
    target 1160
  ]
  edge [
    source 23
    target 1161
  ]
  edge [
    source 23
    target 379
  ]
  edge [
    source 23
    target 1162
  ]
  edge [
    source 23
    target 1163
  ]
  edge [
    source 23
    target 577
  ]
  edge [
    source 23
    target 1164
  ]
  edge [
    source 23
    target 1165
  ]
  edge [
    source 23
    target 1166
  ]
  edge [
    source 23
    target 1167
  ]
  edge [
    source 23
    target 1168
  ]
  edge [
    source 23
    target 1169
  ]
  edge [
    source 23
    target 1170
  ]
  edge [
    source 23
    target 1171
  ]
  edge [
    source 23
    target 1172
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 1174
  ]
  edge [
    source 23
    target 1175
  ]
  edge [
    source 23
    target 1176
  ]
  edge [
    source 23
    target 1177
  ]
  edge [
    source 23
    target 1178
  ]
  edge [
    source 23
    target 1179
  ]
  edge [
    source 23
    target 1180
  ]
  edge [
    source 23
    target 1181
  ]
  edge [
    source 23
    target 336
  ]
  edge [
    source 23
    target 1182
  ]
  edge [
    source 23
    target 1183
  ]
  edge [
    source 23
    target 1184
  ]
  edge [
    source 23
    target 1185
  ]
  edge [
    source 23
    target 1186
  ]
  edge [
    source 23
    target 1187
  ]
  edge [
    source 23
    target 1188
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 1189
  ]
  edge [
    source 23
    target 1190
  ]
  edge [
    source 23
    target 1191
  ]
  edge [
    source 23
    target 1192
  ]
  edge [
    source 23
    target 806
  ]
  edge [
    source 23
    target 1193
  ]
  edge [
    source 23
    target 1194
  ]
  edge [
    source 23
    target 1195
  ]
  edge [
    source 23
    target 1196
  ]
  edge [
    source 23
    target 1197
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 1198
  ]
  edge [
    source 23
    target 1199
  ]
  edge [
    source 23
    target 1200
  ]
  edge [
    source 23
    target 1201
  ]
  edge [
    source 23
    target 1202
  ]
  edge [
    source 23
    target 1203
  ]
  edge [
    source 23
    target 1204
  ]
  edge [
    source 23
    target 1205
  ]
  edge [
    source 23
    target 1206
  ]
  edge [
    source 23
    target 397
  ]
  edge [
    source 23
    target 1207
  ]
  edge [
    source 23
    target 1208
  ]
  edge [
    source 23
    target 1209
  ]
  edge [
    source 23
    target 1210
  ]
  edge [
    source 23
    target 1211
  ]
  edge [
    source 23
    target 1212
  ]
  edge [
    source 23
    target 1213
  ]
  edge [
    source 23
    target 1214
  ]
  edge [
    source 23
    target 1215
  ]
  edge [
    source 23
    target 1216
  ]
  edge [
    source 23
    target 1217
  ]
  edge [
    source 23
    target 1218
  ]
  edge [
    source 23
    target 1219
  ]
  edge [
    source 23
    target 1220
  ]
  edge [
    source 23
    target 1221
  ]
  edge [
    source 23
    target 1222
  ]
  edge [
    source 23
    target 1093
  ]
  edge [
    source 23
    target 1223
  ]
  edge [
    source 23
    target 1224
  ]
  edge [
    source 23
    target 1225
  ]
  edge [
    source 23
    target 1226
  ]
  edge [
    source 23
    target 1227
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 201
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 24
    target 204
  ]
  edge [
    source 24
    target 205
  ]
  edge [
    source 24
    target 206
  ]
  edge [
    source 24
    target 207
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 24
    target 210
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 520
  ]
  edge [
    source 25
    target 521
  ]
  edge [
    source 25
    target 522
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 523
  ]
  edge [
    source 25
    target 524
  ]
  edge [
    source 25
    target 525
  ]
  edge [
    source 25
    target 526
  ]
  edge [
    source 25
    target 527
  ]
  edge [
    source 25
    target 528
  ]
  edge [
    source 25
    target 529
  ]
  edge [
    source 25
    target 315
  ]
  edge [
    source 25
    target 530
  ]
  edge [
    source 25
    target 531
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 25
    target 245
  ]
  edge [
    source 25
    target 532
  ]
  edge [
    source 25
    target 533
  ]
  edge [
    source 25
    target 534
  ]
  edge [
    source 25
    target 535
  ]
  edge [
    source 25
    target 536
  ]
  edge [
    source 25
    target 537
  ]
  edge [
    source 25
    target 538
  ]
  edge [
    source 25
    target 539
  ]
  edge [
    source 25
    target 250
  ]
  edge [
    source 25
    target 540
  ]
  edge [
    source 25
    target 541
  ]
  edge [
    source 25
    target 251
  ]
  edge [
    source 25
    target 253
  ]
  edge [
    source 25
    target 256
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 25
    target 542
  ]
  edge [
    source 25
    target 543
  ]
  edge [
    source 25
    target 544
  ]
  edge [
    source 25
    target 545
  ]
  edge [
    source 25
    target 258
  ]
  edge [
    source 25
    target 1228
  ]
  edge [
    source 25
    target 1229
  ]
  edge [
    source 25
    target 774
  ]
  edge [
    source 25
    target 619
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 788
  ]
  edge [
    source 25
    target 705
  ]
  edge [
    source 25
    target 731
  ]
  edge [
    source 25
    target 577
  ]
  edge [
    source 25
    target 1230
  ]
  edge [
    source 25
    target 738
  ]
  edge [
    source 25
    target 740
  ]
  edge [
    source 25
    target 685
  ]
  edge [
    source 25
    target 744
  ]
  edge [
    source 25
    target 773
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 1231
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 745
  ]
  edge [
    source 25
    target 748
  ]
  edge [
    source 25
    target 282
  ]
  edge [
    source 25
    target 752
  ]
  edge [
    source 25
    target 1233
  ]
  edge [
    source 25
    target 753
  ]
  edge [
    source 25
    target 149
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 760
  ]
  edge [
    source 25
    target 759
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 552
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 84
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 697
  ]
  edge [
    source 25
    target 698
  ]
  edge [
    source 25
    target 699
  ]
  edge [
    source 25
    target 700
  ]
  edge [
    source 25
    target 701
  ]
  edge [
    source 25
    target 702
  ]
  edge [
    source 25
    target 703
  ]
  edge [
    source 25
    target 704
  ]
  edge [
    source 25
    target 706
  ]
  edge [
    source 25
    target 707
  ]
  edge [
    source 25
    target 708
  ]
  edge [
    source 25
    target 709
  ]
  edge [
    source 25
    target 710
  ]
  edge [
    source 25
    target 711
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 64
  ]
  edge [
    source 25
    target 712
  ]
  edge [
    source 25
    target 713
  ]
  edge [
    source 25
    target 714
  ]
  edge [
    source 25
    target 715
  ]
  edge [
    source 25
    target 716
  ]
  edge [
    source 25
    target 717
  ]
  edge [
    source 25
    target 718
  ]
  edge [
    source 25
    target 246
  ]
  edge [
    source 25
    target 719
  ]
  edge [
    source 25
    target 720
  ]
  edge [
    source 25
    target 721
  ]
  edge [
    source 25
    target 722
  ]
  edge [
    source 25
    target 723
  ]
  edge [
    source 25
    target 724
  ]
  edge [
    source 25
    target 725
  ]
  edge [
    source 25
    target 726
  ]
  edge [
    source 25
    target 727
  ]
  edge [
    source 25
    target 728
  ]
  edge [
    source 25
    target 729
  ]
  edge [
    source 25
    target 573
  ]
  edge [
    source 25
    target 689
  ]
  edge [
    source 25
    target 690
  ]
  edge [
    source 25
    target 691
  ]
  edge [
    source 25
    target 692
  ]
  edge [
    source 25
    target 693
  ]
  edge [
    source 25
    target 694
  ]
  edge [
    source 25
    target 695
  ]
  edge [
    source 25
    target 305
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 678
  ]
  edge [
    source 25
    target 696
  ]
  edge [
    source 25
    target 730
  ]
  edge [
    source 25
    target 686
  ]
  edge [
    source 25
    target 687
  ]
  edge [
    source 25
    target 688
  ]
  edge [
    source 25
    target 281
  ]
  edge [
    source 25
    target 336
  ]
  edge [
    source 25
    target 732
  ]
  edge [
    source 25
    target 733
  ]
  edge [
    source 25
    target 734
  ]
  edge [
    source 25
    target 735
  ]
  edge [
    source 25
    target 736
  ]
  edge [
    source 25
    target 737
  ]
  edge [
    source 25
    target 518
  ]
  edge [
    source 25
    target 52
  ]
  edge [
    source 25
    target 739
  ]
  edge [
    source 25
    target 741
  ]
  edge [
    source 25
    target 742
  ]
  edge [
    source 25
    target 743
  ]
  edge [
    source 25
    target 294
  ]
  edge [
    source 25
    target 746
  ]
  edge [
    source 25
    target 747
  ]
  edge [
    source 25
    target 379
  ]
  edge [
    source 25
    target 749
  ]
  edge [
    source 25
    target 750
  ]
  edge [
    source 25
    target 751
  ]
  edge [
    source 25
    target 754
  ]
  edge [
    source 25
    target 755
  ]
  edge [
    source 25
    target 756
  ]
  edge [
    source 25
    target 757
  ]
  edge [
    source 25
    target 758
  ]
  edge [
    source 25
    target 761
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 762
  ]
  edge [
    source 25
    target 763
  ]
  edge [
    source 25
    target 764
  ]
  edge [
    source 25
    target 765
  ]
  edge [
    source 25
    target 766
  ]
  edge [
    source 25
    target 767
  ]
  edge [
    source 25
    target 768
  ]
  edge [
    source 25
    target 769
  ]
  edge [
    source 25
    target 770
  ]
  edge [
    source 25
    target 771
  ]
  edge [
    source 25
    target 772
  ]
  edge [
    source 25
    target 775
  ]
  edge [
    source 25
    target 776
  ]
  edge [
    source 25
    target 777
  ]
  edge [
    source 25
    target 778
  ]
  edge [
    source 25
    target 779
  ]
  edge [
    source 25
    target 780
  ]
  edge [
    source 25
    target 781
  ]
  edge [
    source 25
    target 782
  ]
  edge [
    source 25
    target 783
  ]
  edge [
    source 25
    target 784
  ]
  edge [
    source 25
    target 318
  ]
  edge [
    source 25
    target 785
  ]
  edge [
    source 25
    target 786
  ]
  edge [
    source 25
    target 787
  ]
  edge [
    source 25
    target 789
  ]
  edge [
    source 25
    target 790
  ]
  edge [
    source 25
    target 791
  ]
  edge [
    source 25
    target 792
  ]
  edge [
    source 25
    target 793
  ]
  edge [
    source 25
    target 794
  ]
  edge [
    source 25
    target 795
  ]
  edge [
    source 25
    target 796
  ]
  edge [
    source 25
    target 312
  ]
  edge [
    source 25
    target 578
  ]
  edge [
    source 25
    target 61
  ]
  edge [
    source 25
    target 579
  ]
  edge [
    source 25
    target 580
  ]
  edge [
    source 25
    target 581
  ]
  edge [
    source 25
    target 582
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1250
  ]
  edge [
    source 26
    target 1251
  ]
  edge [
    source 26
    target 1252
  ]
  edge [
    source 26
    target 345
  ]
  edge [
    source 26
    target 1253
  ]
  edge [
    source 26
    target 1254
  ]
  edge [
    source 26
    target 1255
  ]
  edge [
    source 26
    target 1256
  ]
  edge [
    source 26
    target 1257
  ]
  edge [
    source 26
    target 1258
  ]
  edge [
    source 26
    target 1259
  ]
  edge [
    source 26
    target 1260
  ]
  edge [
    source 26
    target 1261
  ]
  edge [
    source 26
    target 1262
  ]
  edge [
    source 26
    target 1263
  ]
  edge [
    source 26
    target 1264
  ]
  edge [
    source 26
    target 294
  ]
  edge [
    source 26
    target 1265
  ]
  edge [
    source 26
    target 1266
  ]
  edge [
    source 26
    target 1267
  ]
  edge [
    source 26
    target 1268
  ]
  edge [
    source 26
    target 1269
  ]
  edge [
    source 26
    target 630
  ]
  edge [
    source 26
    target 1270
  ]
  edge [
    source 26
    target 1271
  ]
  edge [
    source 26
    target 85
  ]
  edge [
    source 26
    target 1272
  ]
  edge [
    source 26
    target 145
  ]
  edge [
    source 26
    target 1273
  ]
  edge [
    source 26
    target 757
  ]
  edge [
    source 26
    target 1274
  ]
  edge [
    source 26
    target 1275
  ]
  edge [
    source 26
    target 1276
  ]
  edge [
    source 26
    target 547
  ]
  edge [
    source 26
    target 1277
  ]
  edge [
    source 26
    target 1278
  ]
  edge [
    source 26
    target 1279
  ]
  edge [
    source 26
    target 1280
  ]
  edge [
    source 26
    target 1281
  ]
  edge [
    source 26
    target 1282
  ]
  edge [
    source 26
    target 1283
  ]
  edge [
    source 26
    target 1284
  ]
  edge [
    source 26
    target 1285
  ]
  edge [
    source 26
    target 1286
  ]
  edge [
    source 26
    target 1287
  ]
  edge [
    source 26
    target 814
  ]
  edge [
    source 26
    target 1288
  ]
  edge [
    source 26
    target 1289
  ]
  edge [
    source 26
    target 1290
  ]
  edge [
    source 26
    target 848
  ]
  edge [
    source 26
    target 1291
  ]
  edge [
    source 26
    target 1160
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 725
  ]
  edge [
    source 26
    target 705
  ]
  edge [
    source 26
    target 221
  ]
  edge [
    source 26
    target 611
  ]
  edge [
    source 26
    target 1292
  ]
  edge [
    source 26
    target 96
  ]
  edge [
    source 26
    target 97
  ]
  edge [
    source 26
    target 98
  ]
  edge [
    source 26
    target 99
  ]
  edge [
    source 26
    target 281
  ]
  edge [
    source 26
    target 1293
  ]
  edge [
    source 26
    target 265
  ]
  edge [
    source 26
    target 1294
  ]
  edge [
    source 26
    target 312
  ]
  edge [
    source 26
    target 1295
  ]
  edge [
    source 26
    target 1296
  ]
  edge [
    source 26
    target 1297
  ]
  edge [
    source 26
    target 330
  ]
  edge [
    source 26
    target 283
  ]
  edge [
    source 26
    target 644
  ]
  edge [
    source 26
    target 734
  ]
  edge [
    source 26
    target 1298
  ]
  edge [
    source 26
    target 1299
  ]
  edge [
    source 26
    target 1300
  ]
  edge [
    source 26
    target 1301
  ]
  edge [
    source 26
    target 1302
  ]
  edge [
    source 26
    target 279
  ]
  edge [
    source 26
    target 1303
  ]
  edge [
    source 26
    target 1304
  ]
  edge [
    source 26
    target 1305
  ]
  edge [
    source 26
    target 1306
  ]
  edge [
    source 26
    target 1307
  ]
  edge [
    source 26
    target 1308
  ]
  edge [
    source 26
    target 1309
  ]
  edge [
    source 26
    target 1310
  ]
  edge [
    source 26
    target 1311
  ]
  edge [
    source 26
    target 709
  ]
  edge [
    source 26
    target 1312
  ]
  edge [
    source 26
    target 1313
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 1314
  ]
  edge [
    source 26
    target 1315
  ]
  edge [
    source 26
    target 151
  ]
  edge [
    source 26
    target 835
  ]
  edge [
    source 26
    target 760
  ]
  edge [
    source 26
    target 425
  ]
  edge [
    source 26
    target 280
  ]
  edge [
    source 26
    target 282
  ]
  edge [
    source 26
    target 284
  ]
  edge [
    source 26
    target 285
  ]
  edge [
    source 26
    target 286
  ]
  edge [
    source 26
    target 287
  ]
  edge [
    source 26
    target 288
  ]
  edge [
    source 26
    target 289
  ]
  edge [
    source 26
    target 230
  ]
  edge [
    source 26
    target 810
  ]
  edge [
    source 26
    target 574
  ]
  edge [
    source 26
    target 1316
  ]
  edge [
    source 26
    target 1317
  ]
  edge [
    source 26
    target 1318
  ]
  edge [
    source 26
    target 1319
  ]
  edge [
    source 26
    target 1320
  ]
  edge [
    source 26
    target 1321
  ]
  edge [
    source 26
    target 1322
  ]
  edge [
    source 26
    target 1323
  ]
  edge [
    source 26
    target 1324
  ]
  edge [
    source 26
    target 1325
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 1326
  ]
  edge [
    source 26
    target 1327
  ]
  edge [
    source 26
    target 1328
  ]
  edge [
    source 26
    target 1329
  ]
  edge [
    source 26
    target 1330
  ]
  edge [
    source 26
    target 1331
  ]
  edge [
    source 26
    target 1332
  ]
  edge [
    source 26
    target 1333
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1122
  ]
  edge [
    source 27
    target 1334
  ]
  edge [
    source 27
    target 1335
  ]
  edge [
    source 27
    target 1336
  ]
  edge [
    source 27
    target 1337
  ]
  edge [
    source 27
    target 1338
  ]
  edge [
    source 27
    target 1339
  ]
  edge [
    source 27
    target 1340
  ]
  edge [
    source 27
    target 1341
  ]
  edge [
    source 27
    target 1342
  ]
  edge [
    source 27
    target 1343
  ]
  edge [
    source 27
    target 1344
  ]
  edge [
    source 27
    target 1345
  ]
  edge [
    source 27
    target 1346
  ]
  edge [
    source 27
    target 1347
  ]
  edge [
    source 27
    target 1348
  ]
  edge [
    source 27
    target 1349
  ]
  edge [
    source 27
    target 1350
  ]
  edge [
    source 27
    target 1351
  ]
  edge [
    source 27
    target 1352
  ]
  edge [
    source 27
    target 1353
  ]
  edge [
    source 27
    target 49
  ]
  edge [
    source 27
    target 1354
  ]
  edge [
    source 27
    target 1355
  ]
  edge [
    source 27
    target 1356
  ]
  edge [
    source 27
    target 1357
  ]
  edge [
    source 27
    target 1358
  ]
  edge [
    source 27
    target 1359
  ]
  edge [
    source 27
    target 1360
  ]
  edge [
    source 27
    target 1361
  ]
  edge [
    source 27
    target 1362
  ]
  edge [
    source 27
    target 1363
  ]
  edge [
    source 27
    target 1364
  ]
  edge [
    source 27
    target 1365
  ]
  edge [
    source 27
    target 1366
  ]
  edge [
    source 27
    target 554
  ]
  edge [
    source 27
    target 93
  ]
  edge [
    source 27
    target 1367
  ]
  edge [
    source 27
    target 1368
  ]
  edge [
    source 27
    target 1369
  ]
  edge [
    source 27
    target 1370
  ]
  edge [
    source 27
    target 1371
  ]
  edge [
    source 27
    target 1372
  ]
  edge [
    source 27
    target 1373
  ]
  edge [
    source 27
    target 1374
  ]
  edge [
    source 27
    target 1375
  ]
  edge [
    source 27
    target 1376
  ]
  edge [
    source 28
    target 1377
  ]
  edge [
    source 28
    target 1378
  ]
  edge [
    source 28
    target 705
  ]
  edge [
    source 28
    target 1379
  ]
  edge [
    source 28
    target 1380
  ]
  edge [
    source 28
    target 1381
  ]
  edge [
    source 28
    target 1382
  ]
  edge [
    source 28
    target 1383
  ]
  edge [
    source 28
    target 1384
  ]
  edge [
    source 28
    target 1385
  ]
  edge [
    source 28
    target 731
  ]
  edge [
    source 28
    target 1386
  ]
  edge [
    source 28
    target 1387
  ]
  edge [
    source 28
    target 1388
  ]
  edge [
    source 28
    target 42
  ]
  edge [
    source 28
    target 1389
  ]
  edge [
    source 28
    target 1390
  ]
  edge [
    source 28
    target 1391
  ]
  edge [
    source 28
    target 1392
  ]
  edge [
    source 28
    target 1393
  ]
  edge [
    source 28
    target 1394
  ]
  edge [
    source 28
    target 1395
  ]
  edge [
    source 28
    target 1396
  ]
  edge [
    source 28
    target 1397
  ]
  edge [
    source 28
    target 83
  ]
  edge [
    source 28
    target 99
  ]
  edge [
    source 28
    target 1398
  ]
  edge [
    source 28
    target 1399
  ]
  edge [
    source 28
    target 511
  ]
  edge [
    source 28
    target 1400
  ]
  edge [
    source 28
    target 1401
  ]
  edge [
    source 28
    target 1402
  ]
  edge [
    source 28
    target 1403
  ]
  edge [
    source 28
    target 1404
  ]
  edge [
    source 28
    target 1405
  ]
  edge [
    source 28
    target 1230
  ]
  edge [
    source 28
    target 1406
  ]
  edge [
    source 28
    target 1407
  ]
  edge [
    source 28
    target 1408
  ]
  edge [
    source 28
    target 1409
  ]
  edge [
    source 28
    target 1410
  ]
  edge [
    source 28
    target 966
  ]
  edge [
    source 28
    target 1411
  ]
  edge [
    source 28
    target 176
  ]
  edge [
    source 28
    target 1412
  ]
  edge [
    source 28
    target 1413
  ]
  edge [
    source 28
    target 885
  ]
  edge [
    source 28
    target 1414
  ]
  edge [
    source 28
    target 1415
  ]
  edge [
    source 28
    target 1416
  ]
  edge [
    source 28
    target 1417
  ]
  edge [
    source 28
    target 1418
  ]
  edge [
    source 28
    target 1419
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1420
  ]
  edge [
    source 29
    target 577
  ]
  edge [
    source 29
    target 1421
  ]
  edge [
    source 29
    target 1422
  ]
  edge [
    source 29
    target 1423
  ]
  edge [
    source 29
    target 1024
  ]
  edge [
    source 29
    target 1424
  ]
  edge [
    source 29
    target 1341
  ]
  edge [
    source 29
    target 1425
  ]
  edge [
    source 29
    target 1426
  ]
  edge [
    source 29
    target 1427
  ]
  edge [
    source 29
    target 1428
  ]
  edge [
    source 29
    target 1429
  ]
  edge [
    source 29
    target 1430
  ]
  edge [
    source 29
    target 1122
  ]
  edge [
    source 29
    target 1431
  ]
  edge [
    source 29
    target 1432
  ]
  edge [
    source 29
    target 1433
  ]
  edge [
    source 29
    target 1434
  ]
  edge [
    source 29
    target 1435
  ]
  edge [
    source 29
    target 79
  ]
  edge [
    source 29
    target 1436
  ]
  edge [
    source 29
    target 1437
  ]
  edge [
    source 29
    target 1438
  ]
  edge [
    source 29
    target 1439
  ]
  edge [
    source 29
    target 1440
  ]
  edge [
    source 29
    target 1441
  ]
  edge [
    source 29
    target 1442
  ]
  edge [
    source 29
    target 1443
  ]
  edge [
    source 29
    target 1394
  ]
  edge [
    source 29
    target 1444
  ]
  edge [
    source 29
    target 1445
  ]
  edge [
    source 29
    target 1446
  ]
  edge [
    source 29
    target 1447
  ]
  edge [
    source 29
    target 1448
  ]
  edge [
    source 29
    target 1449
  ]
  edge [
    source 29
    target 1450
  ]
  edge [
    source 29
    target 1451
  ]
  edge [
    source 29
    target 1305
  ]
  edge [
    source 29
    target 1452
  ]
  edge [
    source 29
    target 1453
  ]
  edge [
    source 29
    target 1454
  ]
  edge [
    source 29
    target 1455
  ]
  edge [
    source 29
    target 1456
  ]
  edge [
    source 29
    target 1457
  ]
  edge [
    source 29
    target 1458
  ]
  edge [
    source 29
    target 1459
  ]
  edge [
    source 29
    target 1460
  ]
  edge [
    source 29
    target 1461
  ]
  edge [
    source 29
    target 1462
  ]
  edge [
    source 29
    target 1463
  ]
  edge [
    source 29
    target 1464
  ]
  edge [
    source 29
    target 1465
  ]
  edge [
    source 29
    target 1466
  ]
  edge [
    source 29
    target 1467
  ]
  edge [
    source 29
    target 1468
  ]
  edge [
    source 29
    target 1469
  ]
  edge [
    source 29
    target 1470
  ]
  edge [
    source 29
    target 1471
  ]
  edge [
    source 29
    target 1472
  ]
  edge [
    source 29
    target 1473
  ]
  edge [
    source 29
    target 1474
  ]
  edge [
    source 29
    target 322
  ]
  edge [
    source 29
    target 1475
  ]
  edge [
    source 29
    target 1476
  ]
  edge [
    source 29
    target 1477
  ]
  edge [
    source 29
    target 1478
  ]
  edge [
    source 29
    target 1479
  ]
  edge [
    source 29
    target 1480
  ]
  edge [
    source 29
    target 1481
  ]
  edge [
    source 29
    target 1482
  ]
  edge [
    source 29
    target 1483
  ]
  edge [
    source 29
    target 1484
  ]
  edge [
    source 29
    target 1485
  ]
  edge [
    source 29
    target 1486
  ]
  edge [
    source 29
    target 1487
  ]
  edge [
    source 29
    target 1488
  ]
  edge [
    source 29
    target 225
  ]
  edge [
    source 29
    target 1146
  ]
  edge [
    source 29
    target 1489
  ]
  edge [
    source 29
    target 1147
  ]
  edge [
    source 29
    target 1490
  ]
  edge [
    source 29
    target 315
  ]
  edge [
    source 29
    target 1491
  ]
  edge [
    source 29
    target 1492
  ]
  edge [
    source 29
    target 1493
  ]
  edge [
    source 29
    target 242
  ]
  edge [
    source 29
    target 243
  ]
  edge [
    source 29
    target 245
  ]
  edge [
    source 29
    target 1494
  ]
  edge [
    source 29
    target 250
  ]
  edge [
    source 29
    target 251
  ]
  edge [
    source 29
    target 1495
  ]
  edge [
    source 29
    target 253
  ]
  edge [
    source 29
    target 256
  ]
  edge [
    source 29
    target 1496
  ]
  edge [
    source 29
    target 1497
  ]
  edge [
    source 29
    target 255
  ]
  edge [
    source 29
    target 1498
  ]
  edge [
    source 29
    target 258
  ]
  edge [
    source 29
    target 1499
  ]
  edge [
    source 29
    target 1500
  ]
  edge [
    source 29
    target 1501
  ]
  edge [
    source 29
    target 1502
  ]
  edge [
    source 29
    target 1503
  ]
  edge [
    source 29
    target 1504
  ]
  edge [
    source 29
    target 1505
  ]
  edge [
    source 29
    target 1506
  ]
  edge [
    source 29
    target 1507
  ]
  edge [
    source 29
    target 1508
  ]
  edge [
    source 29
    target 1509
  ]
  edge [
    source 29
    target 1510
  ]
  edge [
    source 29
    target 1511
  ]
  edge [
    source 29
    target 1512
  ]
  edge [
    source 29
    target 1513
  ]
  edge [
    source 29
    target 1514
  ]
  edge [
    source 29
    target 1515
  ]
  edge [
    source 29
    target 1516
  ]
  edge [
    source 29
    target 1517
  ]
  edge [
    source 29
    target 1518
  ]
  edge [
    source 29
    target 1519
  ]
  edge [
    source 29
    target 1520
  ]
  edge [
    source 29
    target 1521
  ]
  edge [
    source 29
    target 1522
  ]
  edge [
    source 29
    target 1523
  ]
  edge [
    source 29
    target 1524
  ]
  edge [
    source 29
    target 1525
  ]
  edge [
    source 29
    target 1526
  ]
  edge [
    source 29
    target 1527
  ]
  edge [
    source 29
    target 1528
  ]
  edge [
    source 29
    target 1339
  ]
  edge [
    source 29
    target 1529
  ]
  edge [
    source 29
    target 1530
  ]
  edge [
    source 29
    target 1531
  ]
  edge [
    source 29
    target 1532
  ]
  edge [
    source 29
    target 1533
  ]
  edge [
    source 29
    target 1534
  ]
  edge [
    source 29
    target 1535
  ]
  edge [
    source 29
    target 1536
  ]
  edge [
    source 29
    target 1537
  ]
  edge [
    source 29
    target 1538
  ]
  edge [
    source 29
    target 1539
  ]
  edge [
    source 29
    target 1015
  ]
  edge [
    source 29
    target 1540
  ]
  edge [
    source 29
    target 1541
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1542
  ]
  edge [
    source 30
    target 212
  ]
  edge [
    source 30
    target 214
  ]
  edge [
    source 30
    target 103
  ]
  edge [
    source 30
    target 1543
  ]
  edge [
    source 30
    target 332
  ]
  edge [
    source 30
    target 606
  ]
  edge [
    source 30
    target 1544
  ]
  edge [
    source 30
    target 1545
  ]
  edge [
    source 30
    target 93
  ]
  edge [
    source 30
    target 1546
  ]
  edge [
    source 30
    target 828
  ]
  edge [
    source 30
    target 1547
  ]
  edge [
    source 30
    target 1548
  ]
  edge [
    source 30
    target 1549
  ]
  edge [
    source 30
    target 1550
  ]
  edge [
    source 30
    target 517
  ]
  edge [
    source 30
    target 1474
  ]
  edge [
    source 30
    target 221
  ]
  edge [
    source 30
    target 519
  ]
  edge [
    source 30
    target 1551
  ]
  edge [
    source 30
    target 1552
  ]
  edge [
    source 30
    target 1553
  ]
  edge [
    source 30
    target 1554
  ]
  edge [
    source 30
    target 1555
  ]
  edge [
    source 30
    target 1556
  ]
  edge [
    source 30
    target 1557
  ]
  edge [
    source 30
    target 1558
  ]
  edge [
    source 30
    target 1559
  ]
  edge [
    source 30
    target 83
  ]
  edge [
    source 30
    target 1560
  ]
  edge [
    source 30
    target 1561
  ]
  edge [
    source 30
    target 1562
  ]
  edge [
    source 30
    target 1563
  ]
  edge [
    source 30
    target 1564
  ]
  edge [
    source 30
    target 1334
  ]
  edge [
    source 30
    target 1565
  ]
  edge [
    source 30
    target 1566
  ]
  edge [
    source 30
    target 1567
  ]
  edge [
    source 30
    target 1568
  ]
  edge [
    source 30
    target 1569
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 1570
  ]
  edge [
    source 30
    target 1571
  ]
  edge [
    source 30
    target 577
  ]
  edge [
    source 30
    target 1572
  ]
  edge [
    source 30
    target 1573
  ]
  edge [
    source 30
    target 1574
  ]
  edge [
    source 30
    target 1575
  ]
  edge [
    source 30
    target 1576
  ]
  edge [
    source 30
    target 1577
  ]
  edge [
    source 30
    target 1578
  ]
  edge [
    source 30
    target 1579
  ]
  edge [
    source 30
    target 1580
  ]
  edge [
    source 30
    target 1581
  ]
  edge [
    source 30
    target 1582
  ]
  edge [
    source 30
    target 1583
  ]
  edge [
    source 30
    target 1584
  ]
  edge [
    source 30
    target 1585
  ]
  edge [
    source 30
    target 139
  ]
  edge [
    source 30
    target 1586
  ]
  edge [
    source 30
    target 1587
  ]
  edge [
    source 30
    target 1588
  ]
  edge [
    source 30
    target 1589
  ]
  edge [
    source 30
    target 1590
  ]
  edge [
    source 30
    target 1591
  ]
  edge [
    source 30
    target 1592
  ]
  edge [
    source 30
    target 152
  ]
  edge [
    source 30
    target 112
  ]
  edge [
    source 30
    target 113
  ]
  edge [
    source 30
    target 114
  ]
  edge [
    source 30
    target 115
  ]
  edge [
    source 30
    target 116
  ]
  edge [
    source 30
    target 117
  ]
  edge [
    source 30
    target 118
  ]
  edge [
    source 30
    target 119
  ]
  edge [
    source 30
    target 120
  ]
  edge [
    source 30
    target 121
  ]
  edge [
    source 30
    target 122
  ]
  edge [
    source 30
    target 123
  ]
  edge [
    source 30
    target 124
  ]
  edge [
    source 30
    target 125
  ]
  edge [
    source 30
    target 126
  ]
  edge [
    source 30
    target 127
  ]
  edge [
    source 30
    target 128
  ]
  edge [
    source 30
    target 129
  ]
  edge [
    source 30
    target 130
  ]
  edge [
    source 30
    target 131
  ]
  edge [
    source 30
    target 132
  ]
  edge [
    source 30
    target 1593
  ]
  edge [
    source 30
    target 1594
  ]
  edge [
    source 30
    target 1595
  ]
  edge [
    source 30
    target 1596
  ]
  edge [
    source 30
    target 1597
  ]
  edge [
    source 31
    target 351
  ]
  edge [
    source 31
    target 1598
  ]
  edge [
    source 31
    target 1599
  ]
  edge [
    source 31
    target 1600
  ]
  edge [
    source 31
    target 1601
  ]
  edge [
    source 31
    target 1602
  ]
  edge [
    source 31
    target 336
  ]
  edge [
    source 31
    target 1145
  ]
  edge [
    source 31
    target 1603
  ]
  edge [
    source 31
    target 1604
  ]
  edge [
    source 31
    target 1605
  ]
  edge [
    source 31
    target 1606
  ]
  edge [
    source 31
    target 275
  ]
  edge [
    source 31
    target 820
  ]
  edge [
    source 31
    target 103
  ]
  edge [
    source 31
    target 322
  ]
  edge [
    source 31
    target 1607
  ]
  edge [
    source 31
    target 1608
  ]
  edge [
    source 31
    target 1609
  ]
  edge [
    source 32
    target 1610
  ]
  edge [
    source 32
    target 1611
  ]
  edge [
    source 32
    target 1612
  ]
  edge [
    source 32
    target 196
  ]
  edge [
    source 32
    target 1613
  ]
  edge [
    source 32
    target 1614
  ]
  edge [
    source 32
    target 1615
  ]
  edge [
    source 32
    target 1616
  ]
  edge [
    source 32
    target 210
  ]
  edge [
    source 32
    target 194
  ]
  edge [
    source 32
    target 1617
  ]
  edge [
    source 32
    target 1618
  ]
  edge [
    source 32
    target 1619
  ]
  edge [
    source 32
    target 1620
  ]
  edge [
    source 32
    target 1621
  ]
  edge [
    source 32
    target 1622
  ]
  edge [
    source 32
    target 1017
  ]
  edge [
    source 32
    target 1623
  ]
  edge [
    source 32
    target 1624
  ]
  edge [
    source 32
    target 1625
  ]
  edge [
    source 32
    target 1626
  ]
  edge [
    source 32
    target 1627
  ]
  edge [
    source 32
    target 1628
  ]
  edge [
    source 32
    target 1629
  ]
  edge [
    source 32
    target 1052
  ]
  edge [
    source 32
    target 1053
  ]
  edge [
    source 32
    target 1054
  ]
  edge [
    source 32
    target 1055
  ]
  edge [
    source 32
    target 1056
  ]
  edge [
    source 32
    target 1057
  ]
  edge [
    source 32
    target 1058
  ]
  edge [
    source 32
    target 1059
  ]
  edge [
    source 32
    target 1060
  ]
  edge [
    source 32
    target 1061
  ]
  edge [
    source 32
    target 1630
  ]
  edge [
    source 32
    target 915
  ]
  edge [
    source 32
    target 689
  ]
  edge [
    source 32
    target 985
  ]
  edge [
    source 32
    target 1631
  ]
  edge [
    source 32
    target 1632
  ]
  edge [
    source 32
    target 1633
  ]
  edge [
    source 32
    target 1634
  ]
  edge [
    source 32
    target 1635
  ]
  edge [
    source 32
    target 1636
  ]
  edge [
    source 32
    target 1637
  ]
  edge [
    source 32
    target 1638
  ]
  edge [
    source 32
    target 966
  ]
  edge [
    source 32
    target 1639
  ]
  edge [
    source 32
    target 1640
  ]
  edge [
    source 32
    target 1641
  ]
  edge [
    source 32
    target 1642
  ]
  edge [
    source 32
    target 1643
  ]
  edge [
    source 32
    target 1019
  ]
  edge [
    source 32
    target 1644
  ]
  edge [
    source 1645
    target 1646
  ]
  edge [
    source 1647
    target 1648
  ]
  edge [
    source 1647
    target 1649
  ]
  edge [
    source 1647
    target 1650
  ]
  edge [
    source 1648
    target 1649
  ]
  edge [
    source 1648
    target 1650
  ]
  edge [
    source 1649
    target 1650
  ]
  edge [
    source 1651
    target 1652
  ]
]
