graph [
  node [
    id 0
    label "star"
    origin "text"
  ]
  node [
    id 1
    label "wiesia"
    origin "text"
  ]
  node [
    id 2
    label "gmina"
    origin "text"
  ]
  node [
    id 3
    label "kro&#347;niewice"
    origin "text"
  ]
  node [
    id 4
    label "Biskupice"
  ]
  node [
    id 5
    label "radny"
  ]
  node [
    id 6
    label "urz&#261;d"
  ]
  node [
    id 7
    label "powiat"
  ]
  node [
    id 8
    label "rada_gminy"
  ]
  node [
    id 9
    label "Dobro&#324;"
  ]
  node [
    id 10
    label "organizacja_religijna"
  ]
  node [
    id 11
    label "Karlsbad"
  ]
  node [
    id 12
    label "Wielka_Wie&#347;"
  ]
  node [
    id 13
    label "jednostka_administracyjna"
  ]
  node [
    id 14
    label "stanowisko"
  ]
  node [
    id 15
    label "position"
  ]
  node [
    id 16
    label "instytucja"
  ]
  node [
    id 17
    label "siedziba"
  ]
  node [
    id 18
    label "organ"
  ]
  node [
    id 19
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 20
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 21
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 22
    label "mianowaniec"
  ]
  node [
    id 23
    label "dzia&#322;"
  ]
  node [
    id 24
    label "okienko"
  ]
  node [
    id 25
    label "w&#322;adza"
  ]
  node [
    id 26
    label "Zabrze"
  ]
  node [
    id 27
    label "Ma&#322;opolska"
  ]
  node [
    id 28
    label "Niemcy"
  ]
  node [
    id 29
    label "wojew&#243;dztwo"
  ]
  node [
    id 30
    label "rada"
  ]
  node [
    id 31
    label "samorz&#261;dowiec"
  ]
  node [
    id 32
    label "przedstawiciel"
  ]
  node [
    id 33
    label "rajca"
  ]
  node [
    id 34
    label "stary"
  ]
  node [
    id 35
    label "wie&#347;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 34
    target 35
  ]
]
