graph [
  node [
    id 0
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "serwis"
    origin "text"
  ]
  node [
    id 2
    label "spo&#322;eczno&#347;ciowy"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 4
    label "przyzna&#263;"
    origin "text"
  ]
  node [
    id 5
    label "taki"
    origin "text"
  ]
  node [
    id 6
    label "firma"
    origin "text"
  ]
  node [
    id 7
    label "airbnb"
    origin "text"
  ]
  node [
    id 8
    label "netflix"
    origin "text"
  ]
  node [
    id 9
    label "lyft"
    origin "text"
  ]
  node [
    id 10
    label "uprzywilejowany"
    origin "text"
  ]
  node [
    id 11
    label "dost&#281;p"
    origin "text"
  ]
  node [
    id 12
    label "dana"
    origin "text"
  ]
  node [
    id 13
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 14
    label "wewn&#281;trzny"
    origin "text"
  ]
  node [
    id 15
    label "mail"
    origin "text"
  ]
  node [
    id 16
    label "facebook"
    origin "text"
  ]
  node [
    id 17
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 19
    label "przez"
    origin "text"
  ]
  node [
    id 20
    label "brytyjski"
    origin "text"
  ]
  node [
    id 21
    label "parlament"
    origin "text"
  ]
  node [
    id 22
    label "znaczny"
  ]
  node [
    id 23
    label "du&#380;o"
  ]
  node [
    id 24
    label "wa&#380;ny"
  ]
  node [
    id 25
    label "niema&#322;o"
  ]
  node [
    id 26
    label "wiele"
  ]
  node [
    id 27
    label "prawdziwy"
  ]
  node [
    id 28
    label "rozwini&#281;ty"
  ]
  node [
    id 29
    label "doros&#322;y"
  ]
  node [
    id 30
    label "dorodny"
  ]
  node [
    id 31
    label "zgodny"
  ]
  node [
    id 32
    label "prawdziwie"
  ]
  node [
    id 33
    label "podobny"
  ]
  node [
    id 34
    label "m&#261;dry"
  ]
  node [
    id 35
    label "szczery"
  ]
  node [
    id 36
    label "naprawd&#281;"
  ]
  node [
    id 37
    label "naturalny"
  ]
  node [
    id 38
    label "&#380;ywny"
  ]
  node [
    id 39
    label "realnie"
  ]
  node [
    id 40
    label "zauwa&#380;alny"
  ]
  node [
    id 41
    label "znacznie"
  ]
  node [
    id 42
    label "silny"
  ]
  node [
    id 43
    label "wa&#380;nie"
  ]
  node [
    id 44
    label "eksponowany"
  ]
  node [
    id 45
    label "wynios&#322;y"
  ]
  node [
    id 46
    label "dobry"
  ]
  node [
    id 47
    label "istotnie"
  ]
  node [
    id 48
    label "dono&#347;ny"
  ]
  node [
    id 49
    label "do&#347;cig&#322;y"
  ]
  node [
    id 50
    label "ukszta&#322;towany"
  ]
  node [
    id 51
    label "&#378;ra&#322;y"
  ]
  node [
    id 52
    label "zdr&#243;w"
  ]
  node [
    id 53
    label "dorodnie"
  ]
  node [
    id 54
    label "okaza&#322;y"
  ]
  node [
    id 55
    label "mocno"
  ]
  node [
    id 56
    label "cz&#281;sto"
  ]
  node [
    id 57
    label "bardzo"
  ]
  node [
    id 58
    label "wiela"
  ]
  node [
    id 59
    label "cz&#322;owiek"
  ]
  node [
    id 60
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 61
    label "dojrza&#322;y"
  ]
  node [
    id 62
    label "wapniak"
  ]
  node [
    id 63
    label "senior"
  ]
  node [
    id 64
    label "dojrzale"
  ]
  node [
    id 65
    label "doro&#347;lenie"
  ]
  node [
    id 66
    label "wydoro&#347;lenie"
  ]
  node [
    id 67
    label "doletni"
  ]
  node [
    id 68
    label "doro&#347;le"
  ]
  node [
    id 69
    label "uderzenie"
  ]
  node [
    id 70
    label "punkt"
  ]
  node [
    id 71
    label "porcja"
  ]
  node [
    id 72
    label "mecz"
  ]
  node [
    id 73
    label "YouTube"
  ]
  node [
    id 74
    label "zak&#322;ad"
  ]
  node [
    id 75
    label "zastawa"
  ]
  node [
    id 76
    label "us&#322;uga"
  ]
  node [
    id 77
    label "doniesienie"
  ]
  node [
    id 78
    label "wytw&#243;r"
  ]
  node [
    id 79
    label "service"
  ]
  node [
    id 80
    label "strona"
  ]
  node [
    id 81
    label "pogorszenie"
  ]
  node [
    id 82
    label "time"
  ]
  node [
    id 83
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 84
    label "spowodowanie"
  ]
  node [
    id 85
    label "d&#378;wi&#281;k"
  ]
  node [
    id 86
    label "odbicie"
  ]
  node [
    id 87
    label "odbicie_si&#281;"
  ]
  node [
    id 88
    label "dotkni&#281;cie"
  ]
  node [
    id 89
    label "zadanie"
  ]
  node [
    id 90
    label "pobicie"
  ]
  node [
    id 91
    label "skrytykowanie"
  ]
  node [
    id 92
    label "charge"
  ]
  node [
    id 93
    label "instrumentalizacja"
  ]
  node [
    id 94
    label "manewr"
  ]
  node [
    id 95
    label "st&#322;uczenie"
  ]
  node [
    id 96
    label "rush"
  ]
  node [
    id 97
    label "uderzanie"
  ]
  node [
    id 98
    label "walka"
  ]
  node [
    id 99
    label "pogoda"
  ]
  node [
    id 100
    label "stukni&#281;cie"
  ]
  node [
    id 101
    label "&#347;ci&#281;cie"
  ]
  node [
    id 102
    label "dotyk"
  ]
  node [
    id 103
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 104
    label "trafienie"
  ]
  node [
    id 105
    label "zagrywka"
  ]
  node [
    id 106
    label "zdarzenie_si&#281;"
  ]
  node [
    id 107
    label "dostanie"
  ]
  node [
    id 108
    label "poczucie"
  ]
  node [
    id 109
    label "reakcja"
  ]
  node [
    id 110
    label "cios"
  ]
  node [
    id 111
    label "stroke"
  ]
  node [
    id 112
    label "contact"
  ]
  node [
    id 113
    label "nast&#261;pienie"
  ]
  node [
    id 114
    label "bat"
  ]
  node [
    id 115
    label "flap"
  ]
  node [
    id 116
    label "wdarcie_si&#281;"
  ]
  node [
    id 117
    label "ruch"
  ]
  node [
    id 118
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 119
    label "zrobienie"
  ]
  node [
    id 120
    label "dawka"
  ]
  node [
    id 121
    label "coup"
  ]
  node [
    id 122
    label "produkt_gotowy"
  ]
  node [
    id 123
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 124
    label "asortyment"
  ]
  node [
    id 125
    label "&#347;wiadczenie"
  ]
  node [
    id 126
    label "czynno&#347;&#263;"
  ]
  node [
    id 127
    label "element_wyposa&#380;enia"
  ]
  node [
    id 128
    label "sto&#322;owizna"
  ]
  node [
    id 129
    label "przedmiot"
  ]
  node [
    id 130
    label "rezultat"
  ]
  node [
    id 131
    label "p&#322;&#243;d"
  ]
  node [
    id 132
    label "work"
  ]
  node [
    id 133
    label "&#380;o&#322;d"
  ]
  node [
    id 134
    label "ilo&#347;&#263;"
  ]
  node [
    id 135
    label "zas&#243;b"
  ]
  node [
    id 136
    label "czyn"
  ]
  node [
    id 137
    label "wyko&#324;czenie"
  ]
  node [
    id 138
    label "umowa"
  ]
  node [
    id 139
    label "miejsce_pracy"
  ]
  node [
    id 140
    label "instytut"
  ]
  node [
    id 141
    label "jednostka_organizacyjna"
  ]
  node [
    id 142
    label "instytucja"
  ]
  node [
    id 143
    label "zak&#322;adka"
  ]
  node [
    id 144
    label "company"
  ]
  node [
    id 145
    label "obiekt_matematyczny"
  ]
  node [
    id 146
    label "stopie&#324;_pisma"
  ]
  node [
    id 147
    label "pozycja"
  ]
  node [
    id 148
    label "problemat"
  ]
  node [
    id 149
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 150
    label "obiekt"
  ]
  node [
    id 151
    label "point"
  ]
  node [
    id 152
    label "plamka"
  ]
  node [
    id 153
    label "przestrze&#324;"
  ]
  node [
    id 154
    label "mark"
  ]
  node [
    id 155
    label "ust&#281;p"
  ]
  node [
    id 156
    label "po&#322;o&#380;enie"
  ]
  node [
    id 157
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 158
    label "miejsce"
  ]
  node [
    id 159
    label "kres"
  ]
  node [
    id 160
    label "plan"
  ]
  node [
    id 161
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 162
    label "chwila"
  ]
  node [
    id 163
    label "podpunkt"
  ]
  node [
    id 164
    label "jednostka"
  ]
  node [
    id 165
    label "sprawa"
  ]
  node [
    id 166
    label "problematyka"
  ]
  node [
    id 167
    label "prosta"
  ]
  node [
    id 168
    label "wojsko"
  ]
  node [
    id 169
    label "zapunktowa&#263;"
  ]
  node [
    id 170
    label "game"
  ]
  node [
    id 171
    label "obrona"
  ]
  node [
    id 172
    label "gra"
  ]
  node [
    id 173
    label "serw"
  ]
  node [
    id 174
    label "dwumecz"
  ]
  node [
    id 175
    label "linia"
  ]
  node [
    id 176
    label "zorientowa&#263;"
  ]
  node [
    id 177
    label "orientowa&#263;"
  ]
  node [
    id 178
    label "fragment"
  ]
  node [
    id 179
    label "skr&#281;cenie"
  ]
  node [
    id 180
    label "skr&#281;ci&#263;"
  ]
  node [
    id 181
    label "internet"
  ]
  node [
    id 182
    label "g&#243;ra"
  ]
  node [
    id 183
    label "orientowanie"
  ]
  node [
    id 184
    label "zorientowanie"
  ]
  node [
    id 185
    label "forma"
  ]
  node [
    id 186
    label "podmiot"
  ]
  node [
    id 187
    label "ty&#322;"
  ]
  node [
    id 188
    label "logowanie"
  ]
  node [
    id 189
    label "voice"
  ]
  node [
    id 190
    label "kartka"
  ]
  node [
    id 191
    label "layout"
  ]
  node [
    id 192
    label "bok"
  ]
  node [
    id 193
    label "powierzchnia"
  ]
  node [
    id 194
    label "posta&#263;"
  ]
  node [
    id 195
    label "skr&#281;canie"
  ]
  node [
    id 196
    label "orientacja"
  ]
  node [
    id 197
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 198
    label "pagina"
  ]
  node [
    id 199
    label "uj&#281;cie"
  ]
  node [
    id 200
    label "serwis_internetowy"
  ]
  node [
    id 201
    label "adres_internetowy"
  ]
  node [
    id 202
    label "prz&#243;d"
  ]
  node [
    id 203
    label "s&#261;d"
  ]
  node [
    id 204
    label "skr&#281;ca&#263;"
  ]
  node [
    id 205
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 206
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 207
    label "plik"
  ]
  node [
    id 208
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 209
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 210
    label "announcement"
  ]
  node [
    id 211
    label "message"
  ]
  node [
    id 212
    label "zniesienie"
  ]
  node [
    id 213
    label "poinformowanie"
  ]
  node [
    id 214
    label "zawiadomienie"
  ]
  node [
    id 215
    label "do&#322;&#261;czenie"
  ]
  node [
    id 216
    label "fetch"
  ]
  node [
    id 217
    label "zaniesienie"
  ]
  node [
    id 218
    label "naznoszenie"
  ]
  node [
    id 219
    label "medialny"
  ]
  node [
    id 220
    label "publiczny"
  ]
  node [
    id 221
    label "upublicznienie"
  ]
  node [
    id 222
    label "publicznie"
  ]
  node [
    id 223
    label "upublicznianie"
  ]
  node [
    id 224
    label "jawny"
  ]
  node [
    id 225
    label "popularny"
  ]
  node [
    id 226
    label "&#347;rodkowy"
  ]
  node [
    id 227
    label "medialnie"
  ]
  node [
    id 228
    label "nieprawdziwy"
  ]
  node [
    id 229
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 230
    label "przyroda"
  ]
  node [
    id 231
    label "Wsch&#243;d"
  ]
  node [
    id 232
    label "obszar"
  ]
  node [
    id 233
    label "morze"
  ]
  node [
    id 234
    label "kuchnia"
  ]
  node [
    id 235
    label "biosfera"
  ]
  node [
    id 236
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 237
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 238
    label "geotermia"
  ]
  node [
    id 239
    label "atmosfera"
  ]
  node [
    id 240
    label "ekosystem"
  ]
  node [
    id 241
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 242
    label "p&#243;&#322;kula"
  ]
  node [
    id 243
    label "class"
  ]
  node [
    id 244
    label "huczek"
  ]
  node [
    id 245
    label "planeta"
  ]
  node [
    id 246
    label "makrokosmos"
  ]
  node [
    id 247
    label "przej&#261;&#263;"
  ]
  node [
    id 248
    label "przej&#281;cie"
  ]
  node [
    id 249
    label "universe"
  ]
  node [
    id 250
    label "biegun"
  ]
  node [
    id 251
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 252
    label "wszechstworzenie"
  ]
  node [
    id 253
    label "populace"
  ]
  node [
    id 254
    label "magnetosfera"
  ]
  node [
    id 255
    label "po&#322;udnie"
  ]
  node [
    id 256
    label "przejmowa&#263;"
  ]
  node [
    id 257
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 258
    label "zagranica"
  ]
  node [
    id 259
    label "fauna"
  ]
  node [
    id 260
    label "p&#243;&#322;noc"
  ]
  node [
    id 261
    label "stw&#243;r"
  ]
  node [
    id 262
    label "ekosfera"
  ]
  node [
    id 263
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 264
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 265
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 266
    label "litosfera"
  ]
  node [
    id 267
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 268
    label "zjawisko"
  ]
  node [
    id 269
    label "ciemna_materia"
  ]
  node [
    id 270
    label "teren"
  ]
  node [
    id 271
    label "asymilowanie_si&#281;"
  ]
  node [
    id 272
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 273
    label "Nowy_&#346;wiat"
  ]
  node [
    id 274
    label "barysfera"
  ]
  node [
    id 275
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 276
    label "grupa"
  ]
  node [
    id 277
    label "Ziemia"
  ]
  node [
    id 278
    label "hydrosfera"
  ]
  node [
    id 279
    label "rzecz"
  ]
  node [
    id 280
    label "Stary_&#346;wiat"
  ]
  node [
    id 281
    label "przejmowanie"
  ]
  node [
    id 282
    label "geosfera"
  ]
  node [
    id 283
    label "mikrokosmos"
  ]
  node [
    id 284
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 285
    label "woda"
  ]
  node [
    id 286
    label "biota"
  ]
  node [
    id 287
    label "rze&#378;ba"
  ]
  node [
    id 288
    label "environment"
  ]
  node [
    id 289
    label "czarna_dziura"
  ]
  node [
    id 290
    label "obiekt_naturalny"
  ]
  node [
    id 291
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 292
    label "ozonosfera"
  ]
  node [
    id 293
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 294
    label "geoida"
  ]
  node [
    id 295
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 296
    label "asymilowa&#263;"
  ]
  node [
    id 297
    label "kompozycja"
  ]
  node [
    id 298
    label "pakiet_klimatyczny"
  ]
  node [
    id 299
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 300
    label "type"
  ]
  node [
    id 301
    label "cz&#261;steczka"
  ]
  node [
    id 302
    label "gromada"
  ]
  node [
    id 303
    label "specgrupa"
  ]
  node [
    id 304
    label "egzemplarz"
  ]
  node [
    id 305
    label "stage_set"
  ]
  node [
    id 306
    label "asymilowanie"
  ]
  node [
    id 307
    label "zbi&#243;r"
  ]
  node [
    id 308
    label "odm&#322;odzenie"
  ]
  node [
    id 309
    label "odm&#322;adza&#263;"
  ]
  node [
    id 310
    label "harcerze_starsi"
  ]
  node [
    id 311
    label "jednostka_systematyczna"
  ]
  node [
    id 312
    label "oddzia&#322;"
  ]
  node [
    id 313
    label "category"
  ]
  node [
    id 314
    label "liga"
  ]
  node [
    id 315
    label "&#346;wietliki"
  ]
  node [
    id 316
    label "formacja_geologiczna"
  ]
  node [
    id 317
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 318
    label "Eurogrupa"
  ]
  node [
    id 319
    label "Terranie"
  ]
  node [
    id 320
    label "odm&#322;adzanie"
  ]
  node [
    id 321
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 322
    label "Entuzjastki"
  ]
  node [
    id 323
    label "Kosowo"
  ]
  node [
    id 324
    label "zach&#243;d"
  ]
  node [
    id 325
    label "Zabu&#380;e"
  ]
  node [
    id 326
    label "wymiar"
  ]
  node [
    id 327
    label "antroposfera"
  ]
  node [
    id 328
    label "Arktyka"
  ]
  node [
    id 329
    label "Notogea"
  ]
  node [
    id 330
    label "Piotrowo"
  ]
  node [
    id 331
    label "akrecja"
  ]
  node [
    id 332
    label "zakres"
  ]
  node [
    id 333
    label "Ludwin&#243;w"
  ]
  node [
    id 334
    label "Ruda_Pabianicka"
  ]
  node [
    id 335
    label "wsch&#243;d"
  ]
  node [
    id 336
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 337
    label "Pow&#261;zki"
  ]
  node [
    id 338
    label "&#321;&#281;g"
  ]
  node [
    id 339
    label "Rakowice"
  ]
  node [
    id 340
    label "Syberia_Wschodnia"
  ]
  node [
    id 341
    label "Zab&#322;ocie"
  ]
  node [
    id 342
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 343
    label "Kresy_Zachodnie"
  ]
  node [
    id 344
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 345
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 346
    label "holarktyka"
  ]
  node [
    id 347
    label "terytorium"
  ]
  node [
    id 348
    label "pas_planetoid"
  ]
  node [
    id 349
    label "Antarktyka"
  ]
  node [
    id 350
    label "Syberia_Zachodnia"
  ]
  node [
    id 351
    label "Neogea"
  ]
  node [
    id 352
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 353
    label "Olszanica"
  ]
  node [
    id 354
    label "liczba"
  ]
  node [
    id 355
    label "uk&#322;ad"
  ]
  node [
    id 356
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 357
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 358
    label "integer"
  ]
  node [
    id 359
    label "zlewanie_si&#281;"
  ]
  node [
    id 360
    label "pe&#322;ny"
  ]
  node [
    id 361
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 362
    label "charakter"
  ]
  node [
    id 363
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 364
    label "proces"
  ]
  node [
    id 365
    label "przywidzenie"
  ]
  node [
    id 366
    label "boski"
  ]
  node [
    id 367
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 368
    label "krajobraz"
  ]
  node [
    id 369
    label "presence"
  ]
  node [
    id 370
    label "oktant"
  ]
  node [
    id 371
    label "przedzielenie"
  ]
  node [
    id 372
    label "przedzieli&#263;"
  ]
  node [
    id 373
    label "przestw&#243;r"
  ]
  node [
    id 374
    label "rozdziela&#263;"
  ]
  node [
    id 375
    label "nielito&#347;ciwy"
  ]
  node [
    id 376
    label "czasoprzestrze&#324;"
  ]
  node [
    id 377
    label "niezmierzony"
  ]
  node [
    id 378
    label "bezbrze&#380;e"
  ]
  node [
    id 379
    label "rozdzielanie"
  ]
  node [
    id 380
    label "rura"
  ]
  node [
    id 381
    label "&#347;rodowisko"
  ]
  node [
    id 382
    label "grzebiuszka"
  ]
  node [
    id 383
    label "miniatura"
  ]
  node [
    id 384
    label "atom"
  ]
  node [
    id 385
    label "kosmos"
  ]
  node [
    id 386
    label "potw&#243;r"
  ]
  node [
    id 387
    label "istota_&#380;ywa"
  ]
  node [
    id 388
    label "monster"
  ]
  node [
    id 389
    label "istota_fantastyczna"
  ]
  node [
    id 390
    label "niecz&#322;owiek"
  ]
  node [
    id 391
    label "smok_wawelski"
  ]
  node [
    id 392
    label "kultura"
  ]
  node [
    id 393
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 394
    label "energia_termiczna"
  ]
  node [
    id 395
    label "ciep&#322;o"
  ]
  node [
    id 396
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 397
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 398
    label "aspekt"
  ]
  node [
    id 399
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 400
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 401
    label "powietrznia"
  ]
  node [
    id 402
    label "egzosfera"
  ]
  node [
    id 403
    label "mezopauza"
  ]
  node [
    id 404
    label "mezosfera"
  ]
  node [
    id 405
    label "powietrze"
  ]
  node [
    id 406
    label "pow&#322;oka"
  ]
  node [
    id 407
    label "termosfera"
  ]
  node [
    id 408
    label "stratosfera"
  ]
  node [
    id 409
    label "kwas"
  ]
  node [
    id 410
    label "atmosphere"
  ]
  node [
    id 411
    label "homosfera"
  ]
  node [
    id 412
    label "cecha"
  ]
  node [
    id 413
    label "metasfera"
  ]
  node [
    id 414
    label "tropopauza"
  ]
  node [
    id 415
    label "heterosfera"
  ]
  node [
    id 416
    label "klimat"
  ]
  node [
    id 417
    label "atmosferyki"
  ]
  node [
    id 418
    label "jonosfera"
  ]
  node [
    id 419
    label "troposfera"
  ]
  node [
    id 420
    label "sferoida"
  ]
  node [
    id 421
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 422
    label "istota"
  ]
  node [
    id 423
    label "wpada&#263;"
  ]
  node [
    id 424
    label "object"
  ]
  node [
    id 425
    label "wpa&#347;&#263;"
  ]
  node [
    id 426
    label "mienie"
  ]
  node [
    id 427
    label "temat"
  ]
  node [
    id 428
    label "wpadni&#281;cie"
  ]
  node [
    id 429
    label "wpadanie"
  ]
  node [
    id 430
    label "ogarnia&#263;"
  ]
  node [
    id 431
    label "handle"
  ]
  node [
    id 432
    label "treat"
  ]
  node [
    id 433
    label "czerpa&#263;"
  ]
  node [
    id 434
    label "go"
  ]
  node [
    id 435
    label "bra&#263;"
  ]
  node [
    id 436
    label "wzbudza&#263;"
  ]
  node [
    id 437
    label "thrill"
  ]
  node [
    id 438
    label "ogarn&#261;&#263;"
  ]
  node [
    id 439
    label "bang"
  ]
  node [
    id 440
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 441
    label "wzi&#261;&#263;"
  ]
  node [
    id 442
    label "wzbudzi&#263;"
  ]
  node [
    id 443
    label "stimulate"
  ]
  node [
    id 444
    label "branie"
  ]
  node [
    id 445
    label "acquisition"
  ]
  node [
    id 446
    label "czerpanie"
  ]
  node [
    id 447
    label "ogarnianie"
  ]
  node [
    id 448
    label "wzbudzanie"
  ]
  node [
    id 449
    label "movement"
  ]
  node [
    id 450
    label "caparison"
  ]
  node [
    id 451
    label "interception"
  ]
  node [
    id 452
    label "zaczerpni&#281;cie"
  ]
  node [
    id 453
    label "wzbudzenie"
  ]
  node [
    id 454
    label "wzi&#281;cie"
  ]
  node [
    id 455
    label "wra&#380;enie"
  ]
  node [
    id 456
    label "emotion"
  ]
  node [
    id 457
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 458
    label "discipline"
  ]
  node [
    id 459
    label "zboczy&#263;"
  ]
  node [
    id 460
    label "w&#261;tek"
  ]
  node [
    id 461
    label "entity"
  ]
  node [
    id 462
    label "sponiewiera&#263;"
  ]
  node [
    id 463
    label "zboczenie"
  ]
  node [
    id 464
    label "zbaczanie"
  ]
  node [
    id 465
    label "thing"
  ]
  node [
    id 466
    label "om&#243;wi&#263;"
  ]
  node [
    id 467
    label "tre&#347;&#263;"
  ]
  node [
    id 468
    label "element"
  ]
  node [
    id 469
    label "kr&#261;&#380;enie"
  ]
  node [
    id 470
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 471
    label "zbacza&#263;"
  ]
  node [
    id 472
    label "om&#243;wienie"
  ]
  node [
    id 473
    label "tematyka"
  ]
  node [
    id 474
    label "omawianie"
  ]
  node [
    id 475
    label "omawia&#263;"
  ]
  node [
    id 476
    label "robienie"
  ]
  node [
    id 477
    label "program_nauczania"
  ]
  node [
    id 478
    label "sponiewieranie"
  ]
  node [
    id 479
    label "performance"
  ]
  node [
    id 480
    label "sztuka"
  ]
  node [
    id 481
    label "strona_&#347;wiata"
  ]
  node [
    id 482
    label "p&#243;&#322;nocek"
  ]
  node [
    id 483
    label "noc"
  ]
  node [
    id 484
    label "Boreasz"
  ]
  node [
    id 485
    label "godzina"
  ]
  node [
    id 486
    label "granica_pa&#324;stwa"
  ]
  node [
    id 487
    label "warstwa"
  ]
  node [
    id 488
    label "kriosfera"
  ]
  node [
    id 489
    label "lej_polarny"
  ]
  node [
    id 490
    label "sfera"
  ]
  node [
    id 491
    label "zawiasy"
  ]
  node [
    id 492
    label "brzeg"
  ]
  node [
    id 493
    label "p&#322;oza"
  ]
  node [
    id 494
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 495
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 496
    label "element_anatomiczny"
  ]
  node [
    id 497
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 498
    label "organ"
  ]
  node [
    id 499
    label "marina"
  ]
  node [
    id 500
    label "reda"
  ]
  node [
    id 501
    label "pe&#322;ne_morze"
  ]
  node [
    id 502
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 503
    label "Morze_Bia&#322;e"
  ]
  node [
    id 504
    label "przymorze"
  ]
  node [
    id 505
    label "Morze_Adriatyckie"
  ]
  node [
    id 506
    label "paliszcze"
  ]
  node [
    id 507
    label "talasoterapia"
  ]
  node [
    id 508
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 509
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 510
    label "bezmiar"
  ]
  node [
    id 511
    label "Morze_Egejskie"
  ]
  node [
    id 512
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 513
    label "latarnia_morska"
  ]
  node [
    id 514
    label "Neptun"
  ]
  node [
    id 515
    label "Morze_Czarne"
  ]
  node [
    id 516
    label "nereida"
  ]
  node [
    id 517
    label "laguna"
  ]
  node [
    id 518
    label "okeanida"
  ]
  node [
    id 519
    label "Morze_Czerwone"
  ]
  node [
    id 520
    label "zbiornik_wodny"
  ]
  node [
    id 521
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 522
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 523
    label "rze&#378;biarstwo"
  ]
  node [
    id 524
    label "planacja"
  ]
  node [
    id 525
    label "plastyka"
  ]
  node [
    id 526
    label "relief"
  ]
  node [
    id 527
    label "bozzetto"
  ]
  node [
    id 528
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 529
    label "j&#261;dro"
  ]
  node [
    id 530
    label "&#347;rodek"
  ]
  node [
    id 531
    label "dwunasta"
  ]
  node [
    id 532
    label "dzie&#324;"
  ]
  node [
    id 533
    label "pora"
  ]
  node [
    id 534
    label "ozon"
  ]
  node [
    id 535
    label "warstwa_perydotytowa"
  ]
  node [
    id 536
    label "gleba"
  ]
  node [
    id 537
    label "skorupa_ziemska"
  ]
  node [
    id 538
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 539
    label "warstwa_granitowa"
  ]
  node [
    id 540
    label "sialma"
  ]
  node [
    id 541
    label "kresom&#243;zgowie"
  ]
  node [
    id 542
    label "kula"
  ]
  node [
    id 543
    label "przyra"
  ]
  node [
    id 544
    label "awifauna"
  ]
  node [
    id 545
    label "ichtiofauna"
  ]
  node [
    id 546
    label "biom"
  ]
  node [
    id 547
    label "geosystem"
  ]
  node [
    id 548
    label "przybieranie"
  ]
  node [
    id 549
    label "pustka"
  ]
  node [
    id 550
    label "przybrze&#380;e"
  ]
  node [
    id 551
    label "woda_s&#322;odka"
  ]
  node [
    id 552
    label "utylizator"
  ]
  node [
    id 553
    label "spi&#281;trzenie"
  ]
  node [
    id 554
    label "wodnik"
  ]
  node [
    id 555
    label "water"
  ]
  node [
    id 556
    label "fala"
  ]
  node [
    id 557
    label "kryptodepresja"
  ]
  node [
    id 558
    label "klarownik"
  ]
  node [
    id 559
    label "tlenek"
  ]
  node [
    id 560
    label "l&#243;d"
  ]
  node [
    id 561
    label "nabranie"
  ]
  node [
    id 562
    label "chlastanie"
  ]
  node [
    id 563
    label "zrzut"
  ]
  node [
    id 564
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 565
    label "uci&#261;g"
  ]
  node [
    id 566
    label "nabra&#263;"
  ]
  node [
    id 567
    label "wybrze&#380;e"
  ]
  node [
    id 568
    label "p&#322;ycizna"
  ]
  node [
    id 569
    label "uj&#281;cie_wody"
  ]
  node [
    id 570
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 571
    label "ciecz"
  ]
  node [
    id 572
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 573
    label "Waruna"
  ]
  node [
    id 574
    label "chlasta&#263;"
  ]
  node [
    id 575
    label "bicie"
  ]
  node [
    id 576
    label "deklamacja"
  ]
  node [
    id 577
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 578
    label "spi&#281;trzanie"
  ]
  node [
    id 579
    label "wypowied&#378;"
  ]
  node [
    id 580
    label "spi&#281;trza&#263;"
  ]
  node [
    id 581
    label "wysi&#281;k"
  ]
  node [
    id 582
    label "dotleni&#263;"
  ]
  node [
    id 583
    label "pojazd"
  ]
  node [
    id 584
    label "nap&#243;j"
  ]
  node [
    id 585
    label "bombast"
  ]
  node [
    id 586
    label "biotop"
  ]
  node [
    id 587
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 588
    label "biocenoza"
  ]
  node [
    id 589
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 590
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 591
    label "nation"
  ]
  node [
    id 592
    label "w&#322;adza"
  ]
  node [
    id 593
    label "kontekst"
  ]
  node [
    id 594
    label "plant"
  ]
  node [
    id 595
    label "ro&#347;lina"
  ]
  node [
    id 596
    label "formacja_ro&#347;linna"
  ]
  node [
    id 597
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 598
    label "szata_ro&#347;linna"
  ]
  node [
    id 599
    label "zielono&#347;&#263;"
  ]
  node [
    id 600
    label "pi&#281;tro"
  ]
  node [
    id 601
    label "cyprysowate"
  ]
  node [
    id 602
    label "iglak"
  ]
  node [
    id 603
    label "zlewozmywak"
  ]
  node [
    id 604
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 605
    label "zaplecze"
  ]
  node [
    id 606
    label "gotowa&#263;"
  ]
  node [
    id 607
    label "jedzenie"
  ]
  node [
    id 608
    label "tajniki"
  ]
  node [
    id 609
    label "zaj&#281;cie"
  ]
  node [
    id 610
    label "pomieszczenie"
  ]
  node [
    id 611
    label "strefa"
  ]
  node [
    id 612
    label "Jowisz"
  ]
  node [
    id 613
    label "syzygia"
  ]
  node [
    id 614
    label "Uran"
  ]
  node [
    id 615
    label "Saturn"
  ]
  node [
    id 616
    label "dar"
  ]
  node [
    id 617
    label "real"
  ]
  node [
    id 618
    label "Ukraina"
  ]
  node [
    id 619
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 620
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 621
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 622
    label "blok_wschodni"
  ]
  node [
    id 623
    label "Europa_Wschodnia"
  ]
  node [
    id 624
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 625
    label "stwierdzi&#263;"
  ]
  node [
    id 626
    label "pozwoli&#263;"
  ]
  node [
    id 627
    label "nada&#263;"
  ]
  node [
    id 628
    label "give"
  ]
  node [
    id 629
    label "da&#263;"
  ]
  node [
    id 630
    label "donie&#347;&#263;"
  ]
  node [
    id 631
    label "spowodowa&#263;"
  ]
  node [
    id 632
    label "za&#322;atwi&#263;"
  ]
  node [
    id 633
    label "zarekomendowa&#263;"
  ]
  node [
    id 634
    label "przes&#322;a&#263;"
  ]
  node [
    id 635
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 636
    label "dress"
  ]
  node [
    id 637
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 638
    label "obieca&#263;"
  ]
  node [
    id 639
    label "testify"
  ]
  node [
    id 640
    label "przeznaczy&#263;"
  ]
  node [
    id 641
    label "supply"
  ]
  node [
    id 642
    label "odst&#261;pi&#263;"
  ]
  node [
    id 643
    label "zada&#263;"
  ]
  node [
    id 644
    label "rap"
  ]
  node [
    id 645
    label "feed"
  ]
  node [
    id 646
    label "convey"
  ]
  node [
    id 647
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 648
    label "picture"
  ]
  node [
    id 649
    label "zap&#322;aci&#263;"
  ]
  node [
    id 650
    label "udost&#281;pni&#263;"
  ]
  node [
    id 651
    label "sztachn&#261;&#263;"
  ]
  node [
    id 652
    label "doda&#263;"
  ]
  node [
    id 653
    label "dostarczy&#263;"
  ]
  node [
    id 654
    label "zrobi&#263;"
  ]
  node [
    id 655
    label "przywali&#263;"
  ]
  node [
    id 656
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 657
    label "wyrzec_si&#281;"
  ]
  node [
    id 658
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 659
    label "powierzy&#263;"
  ]
  node [
    id 660
    label "przekaza&#263;"
  ]
  node [
    id 661
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 662
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 663
    label "leave"
  ]
  node [
    id 664
    label "pofolgowa&#263;"
  ]
  node [
    id 665
    label "uzna&#263;"
  ]
  node [
    id 666
    label "assent"
  ]
  node [
    id 667
    label "declare"
  ]
  node [
    id 668
    label "oznajmi&#263;"
  ]
  node [
    id 669
    label "powiedzie&#263;"
  ]
  node [
    id 670
    label "jaki&#347;"
  ]
  node [
    id 671
    label "okre&#347;lony"
  ]
  node [
    id 672
    label "jako&#347;"
  ]
  node [
    id 673
    label "niez&#322;y"
  ]
  node [
    id 674
    label "charakterystyczny"
  ]
  node [
    id 675
    label "jako_tako"
  ]
  node [
    id 676
    label "ciekawy"
  ]
  node [
    id 677
    label "dziwny"
  ]
  node [
    id 678
    label "przyzwoity"
  ]
  node [
    id 679
    label "wiadomy"
  ]
  node [
    id 680
    label "MAN_SE"
  ]
  node [
    id 681
    label "Spo&#322;em"
  ]
  node [
    id 682
    label "Orbis"
  ]
  node [
    id 683
    label "Canon"
  ]
  node [
    id 684
    label "HP"
  ]
  node [
    id 685
    label "nazwa_w&#322;asna"
  ]
  node [
    id 686
    label "zasoby"
  ]
  node [
    id 687
    label "zasoby_ludzkie"
  ]
  node [
    id 688
    label "klasa"
  ]
  node [
    id 689
    label "Baltona"
  ]
  node [
    id 690
    label "zaufanie"
  ]
  node [
    id 691
    label "paczkarnia"
  ]
  node [
    id 692
    label "reengineering"
  ]
  node [
    id 693
    label "siedziba"
  ]
  node [
    id 694
    label "Orlen"
  ]
  node [
    id 695
    label "Pewex"
  ]
  node [
    id 696
    label "biurowiec"
  ]
  node [
    id 697
    label "Apeks"
  ]
  node [
    id 698
    label "MAC"
  ]
  node [
    id 699
    label "networking"
  ]
  node [
    id 700
    label "podmiot_gospodarczy"
  ]
  node [
    id 701
    label "Google"
  ]
  node [
    id 702
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 703
    label "Hortex"
  ]
  node [
    id 704
    label "interes"
  ]
  node [
    id 705
    label "zaleta"
  ]
  node [
    id 706
    label "rezerwa"
  ]
  node [
    id 707
    label "botanika"
  ]
  node [
    id 708
    label "jako&#347;&#263;"
  ]
  node [
    id 709
    label "atak"
  ]
  node [
    id 710
    label "mecz_mistrzowski"
  ]
  node [
    id 711
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 712
    label "Ekwici"
  ]
  node [
    id 713
    label "sala"
  ]
  node [
    id 714
    label "wagon"
  ]
  node [
    id 715
    label "przepisa&#263;"
  ]
  node [
    id 716
    label "promocja"
  ]
  node [
    id 717
    label "arrangement"
  ]
  node [
    id 718
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 719
    label "szko&#322;a"
  ]
  node [
    id 720
    label "programowanie_obiektowe"
  ]
  node [
    id 721
    label "wykrzyknik"
  ]
  node [
    id 722
    label "dziennik_lekcyjny"
  ]
  node [
    id 723
    label "typ"
  ]
  node [
    id 724
    label "znak_jako&#347;ci"
  ]
  node [
    id 725
    label "&#322;awka"
  ]
  node [
    id 726
    label "organizacja"
  ]
  node [
    id 727
    label "poziom"
  ]
  node [
    id 728
    label "tablica"
  ]
  node [
    id 729
    label "przepisanie"
  ]
  node [
    id 730
    label "fakcja"
  ]
  node [
    id 731
    label "pomoc"
  ]
  node [
    id 732
    label "form"
  ]
  node [
    id 733
    label "kurs"
  ]
  node [
    id 734
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 735
    label "budynek"
  ]
  node [
    id 736
    label "&#321;ubianka"
  ]
  node [
    id 737
    label "Bia&#322;y_Dom"
  ]
  node [
    id 738
    label "dzia&#322;_personalny"
  ]
  node [
    id 739
    label "Kreml"
  ]
  node [
    id 740
    label "sadowisko"
  ]
  node [
    id 741
    label "nasada"
  ]
  node [
    id 742
    label "profanum"
  ]
  node [
    id 743
    label "wz&#243;r"
  ]
  node [
    id 744
    label "os&#322;abia&#263;"
  ]
  node [
    id 745
    label "homo_sapiens"
  ]
  node [
    id 746
    label "osoba"
  ]
  node [
    id 747
    label "ludzko&#347;&#263;"
  ]
  node [
    id 748
    label "Adam"
  ]
  node [
    id 749
    label "hominid"
  ]
  node [
    id 750
    label "portrecista"
  ]
  node [
    id 751
    label "polifag"
  ]
  node [
    id 752
    label "podw&#322;adny"
  ]
  node [
    id 753
    label "dwun&#243;g"
  ]
  node [
    id 754
    label "duch"
  ]
  node [
    id 755
    label "os&#322;abianie"
  ]
  node [
    id 756
    label "antropochoria"
  ]
  node [
    id 757
    label "figura"
  ]
  node [
    id 758
    label "g&#322;owa"
  ]
  node [
    id 759
    label "oddzia&#322;ywanie"
  ]
  node [
    id 760
    label "magazyn"
  ]
  node [
    id 761
    label "dzia&#322;"
  ]
  node [
    id 762
    label "zasoby_kopalin"
  ]
  node [
    id 763
    label "z&#322;o&#380;e"
  ]
  node [
    id 764
    label "samodzielno&#347;&#263;"
  ]
  node [
    id 765
    label "driveway"
  ]
  node [
    id 766
    label "informatyka"
  ]
  node [
    id 767
    label "ropa_naftowa"
  ]
  node [
    id 768
    label "paliwo"
  ]
  node [
    id 769
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 770
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 771
    label "strategia"
  ]
  node [
    id 772
    label "oprogramowanie"
  ]
  node [
    id 773
    label "zmienia&#263;"
  ]
  node [
    id 774
    label "odmienienie"
  ]
  node [
    id 775
    label "przer&#243;bka"
  ]
  node [
    id 776
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 777
    label "dobro"
  ]
  node [
    id 778
    label "penis"
  ]
  node [
    id 779
    label "postawa"
  ]
  node [
    id 780
    label "faith"
  ]
  node [
    id 781
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 782
    label "opoka"
  ]
  node [
    id 783
    label "credit"
  ]
  node [
    id 784
    label "zacz&#281;cie"
  ]
  node [
    id 785
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 786
    label "szczeg&#243;lny"
  ]
  node [
    id 787
    label "lekki"
  ]
  node [
    id 788
    label "lekko"
  ]
  node [
    id 789
    label "g&#322;adko"
  ]
  node [
    id 790
    label "piaszczysty"
  ]
  node [
    id 791
    label "beztroskliwy"
  ]
  node [
    id 792
    label "subtelny"
  ]
  node [
    id 793
    label "&#322;acny"
  ]
  node [
    id 794
    label "letki"
  ]
  node [
    id 795
    label "nieznacznie"
  ]
  node [
    id 796
    label "bezpieczny"
  ]
  node [
    id 797
    label "snadny"
  ]
  node [
    id 798
    label "delikatnie"
  ]
  node [
    id 799
    label "zr&#281;czny"
  ]
  node [
    id 800
    label "prosty"
  ]
  node [
    id 801
    label "suchy"
  ]
  node [
    id 802
    label "przyjemny"
  ]
  node [
    id 803
    label "lekkozbrojny"
  ]
  node [
    id 804
    label "przyswajalny"
  ]
  node [
    id 805
    label "p&#322;ynny"
  ]
  node [
    id 806
    label "zgrabny"
  ]
  node [
    id 807
    label "zwinnie"
  ]
  node [
    id 808
    label "przewiewny"
  ]
  node [
    id 809
    label "s&#322;aby"
  ]
  node [
    id 810
    label "&#322;atwo"
  ]
  node [
    id 811
    label "ubogi"
  ]
  node [
    id 812
    label "nierozwa&#380;ny"
  ]
  node [
    id 813
    label "dietetyczny"
  ]
  node [
    id 814
    label "&#322;atwy"
  ]
  node [
    id 815
    label "beztrosko"
  ]
  node [
    id 816
    label "polotny"
  ]
  node [
    id 817
    label "cienki"
  ]
  node [
    id 818
    label "nieg&#322;&#281;boko"
  ]
  node [
    id 819
    label "delikatny"
  ]
  node [
    id 820
    label "szczeg&#243;lnie"
  ]
  node [
    id 821
    label "wyj&#261;tkowy"
  ]
  node [
    id 822
    label "operacja"
  ]
  node [
    id 823
    label "has&#322;o"
  ]
  node [
    id 824
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 825
    label "konto"
  ]
  node [
    id 826
    label "operator_modalny"
  ]
  node [
    id 827
    label "alternatywa"
  ]
  node [
    id 828
    label "wydarzenie"
  ]
  node [
    id 829
    label "wyb&#243;r"
  ]
  node [
    id 830
    label "egzekutywa"
  ]
  node [
    id 831
    label "potencja&#322;"
  ]
  node [
    id 832
    label "obliczeniowo"
  ]
  node [
    id 833
    label "ability"
  ]
  node [
    id 834
    label "posiada&#263;"
  ]
  node [
    id 835
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 836
    label "prospect"
  ]
  node [
    id 837
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 838
    label "proces_my&#347;lowy"
  ]
  node [
    id 839
    label "liczenie"
  ]
  node [
    id 840
    label "laparotomia"
  ]
  node [
    id 841
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 842
    label "liczy&#263;"
  ]
  node [
    id 843
    label "matematyka"
  ]
  node [
    id 844
    label "funkcja"
  ]
  node [
    id 845
    label "rzut"
  ]
  node [
    id 846
    label "zabieg"
  ]
  node [
    id 847
    label "mathematical_process"
  ]
  node [
    id 848
    label "szew"
  ]
  node [
    id 849
    label "torakotomia"
  ]
  node [
    id 850
    label "supremum"
  ]
  node [
    id 851
    label "chirurg"
  ]
  node [
    id 852
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 853
    label "infimum"
  ]
  node [
    id 854
    label "rz&#261;d"
  ]
  node [
    id 855
    label "uwaga"
  ]
  node [
    id 856
    label "praca"
  ]
  node [
    id 857
    label "plac"
  ]
  node [
    id 858
    label "location"
  ]
  node [
    id 859
    label "warunek_lokalowy"
  ]
  node [
    id 860
    label "cia&#322;o"
  ]
  node [
    id 861
    label "status"
  ]
  node [
    id 862
    label "leksem"
  ]
  node [
    id 863
    label "solicitation"
  ]
  node [
    id 864
    label "artyku&#322;_has&#322;owy"
  ]
  node [
    id 865
    label "kod"
  ]
  node [
    id 866
    label "rozwi&#261;zanie"
  ]
  node [
    id 867
    label "guide_word"
  ]
  node [
    id 868
    label "ochrona"
  ]
  node [
    id 869
    label "sygna&#322;"
  ]
  node [
    id 870
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 871
    label "artyku&#322;"
  ]
  node [
    id 872
    label "wyra&#380;enie"
  ]
  node [
    id 873
    label "przes&#322;anie"
  ]
  node [
    id 874
    label "idea"
  ]
  node [
    id 875
    label "definicja"
  ]
  node [
    id 876
    label "powiedzenie"
  ]
  node [
    id 877
    label "sztuka_dla_sztuki"
  ]
  node [
    id 878
    label "kwalifikator"
  ]
  node [
    id 879
    label "reprezentacja"
  ]
  node [
    id 880
    label "kredyt"
  ]
  node [
    id 881
    label "kariera"
  ]
  node [
    id 882
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 883
    label "bank"
  ]
  node [
    id 884
    label "debet"
  ]
  node [
    id 885
    label "dorobek"
  ]
  node [
    id 886
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 887
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 888
    label "rachunek"
  ]
  node [
    id 889
    label "subkonto"
  ]
  node [
    id 890
    label "zamek"
  ]
  node [
    id 891
    label "infa"
  ]
  node [
    id 892
    label "gramatyka_formalna"
  ]
  node [
    id 893
    label "baza_danych"
  ]
  node [
    id 894
    label "kryptologia"
  ]
  node [
    id 895
    label "przetwarzanie_informacji"
  ]
  node [
    id 896
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 897
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 898
    label "dziedzina_informatyki"
  ]
  node [
    id 899
    label "program"
  ]
  node [
    id 900
    label "kierunek"
  ]
  node [
    id 901
    label "sztuczna_inteligencja"
  ]
  node [
    id 902
    label "artefakt"
  ]
  node [
    id 903
    label "cnota"
  ]
  node [
    id 904
    label "buddyzm"
  ]
  node [
    id 905
    label "da&#324;"
  ]
  node [
    id 906
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 907
    label "dyspozycja"
  ]
  node [
    id 908
    label "faculty"
  ]
  node [
    id 909
    label "stygmat"
  ]
  node [
    id 910
    label "stan"
  ]
  node [
    id 911
    label "honesty"
  ]
  node [
    id 912
    label "panie&#324;stwo"
  ]
  node [
    id 913
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 914
    label "aretologia"
  ]
  node [
    id 915
    label "dobro&#263;"
  ]
  node [
    id 916
    label "Buddhism"
  ]
  node [
    id 917
    label "wad&#378;rajana"
  ]
  node [
    id 918
    label "arahant"
  ]
  node [
    id 919
    label "tantryzm"
  ]
  node [
    id 920
    label "therawada"
  ]
  node [
    id 921
    label "mahajana"
  ]
  node [
    id 922
    label "kalpa"
  ]
  node [
    id 923
    label "li"
  ]
  node [
    id 924
    label "maja"
  ]
  node [
    id 925
    label "bardo"
  ]
  node [
    id 926
    label "ahinsa"
  ]
  node [
    id 927
    label "religia"
  ]
  node [
    id 928
    label "lampka_ma&#347;lana"
  ]
  node [
    id 929
    label "asura"
  ]
  node [
    id 930
    label "hinajana"
  ]
  node [
    id 931
    label "bonzo"
  ]
  node [
    id 932
    label "rise"
  ]
  node [
    id 933
    label "appear"
  ]
  node [
    id 934
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 935
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 936
    label "wewn&#281;trznie"
  ]
  node [
    id 937
    label "wn&#281;trzny"
  ]
  node [
    id 938
    label "psychiczny"
  ]
  node [
    id 939
    label "numer"
  ]
  node [
    id 940
    label "filia"
  ]
  node [
    id 941
    label "formacja"
  ]
  node [
    id 942
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 943
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 944
    label "agencja"
  ]
  node [
    id 945
    label "whole"
  ]
  node [
    id 946
    label "malm"
  ]
  node [
    id 947
    label "szpital"
  ]
  node [
    id 948
    label "jednostka_geologiczna"
  ]
  node [
    id 949
    label "dogger"
  ]
  node [
    id 950
    label "ajencja"
  ]
  node [
    id 951
    label "lias"
  ]
  node [
    id 952
    label "system"
  ]
  node [
    id 953
    label "zesp&#243;&#322;"
  ]
  node [
    id 954
    label "nerwowo_chory"
  ]
  node [
    id 955
    label "niematerialny"
  ]
  node [
    id 956
    label "nienormalny"
  ]
  node [
    id 957
    label "odciele&#347;nianie_si&#281;"
  ]
  node [
    id 958
    label "psychiatra"
  ]
  node [
    id 959
    label "odciele&#347;nienie_si&#281;"
  ]
  node [
    id 960
    label "niezr&#243;wnowa&#380;ony"
  ]
  node [
    id 961
    label "psychicznie"
  ]
  node [
    id 962
    label "oznaczenie"
  ]
  node [
    id 963
    label "facet"
  ]
  node [
    id 964
    label "wyst&#281;p"
  ]
  node [
    id 965
    label "turn"
  ]
  node [
    id 966
    label "pok&#243;j"
  ]
  node [
    id 967
    label "akt_p&#322;ciowy"
  ]
  node [
    id 968
    label "&#380;art"
  ]
  node [
    id 969
    label "publikacja"
  ]
  node [
    id 970
    label "czasopismo"
  ]
  node [
    id 971
    label "orygina&#322;"
  ]
  node [
    id 972
    label "zi&#243;&#322;ko"
  ]
  node [
    id 973
    label "impression"
  ]
  node [
    id 974
    label "sztos"
  ]
  node [
    id 975
    label "hotel"
  ]
  node [
    id 976
    label "wn&#281;trzowy"
  ]
  node [
    id 977
    label "skrzynka_odbiorcza"
  ]
  node [
    id 978
    label "mailowa&#263;"
  ]
  node [
    id 979
    label "poczta_elektroniczna"
  ]
  node [
    id 980
    label "skrzynka_mailowa"
  ]
  node [
    id 981
    label "mailowanie"
  ]
  node [
    id 982
    label "identyfikator"
  ]
  node [
    id 983
    label "us&#322;uga_internetowa"
  ]
  node [
    id 984
    label "komunikat"
  ]
  node [
    id 985
    label "zarys"
  ]
  node [
    id 986
    label "nap&#322;ywanie"
  ]
  node [
    id 987
    label "signal"
  ]
  node [
    id 988
    label "znie&#347;&#263;"
  ]
  node [
    id 989
    label "informacja"
  ]
  node [
    id 990
    label "communication"
  ]
  node [
    id 991
    label "depesza_emska"
  ]
  node [
    id 992
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 993
    label "znosi&#263;"
  ]
  node [
    id 994
    label "znoszenie"
  ]
  node [
    id 995
    label "oznaka"
  ]
  node [
    id 996
    label "identifier"
  ]
  node [
    id 997
    label "plakietka"
  ]
  node [
    id 998
    label "urz&#261;dzenie"
  ]
  node [
    id 999
    label "symbol"
  ]
  node [
    id 1000
    label "przesy&#322;a&#263;"
  ]
  node [
    id 1001
    label "przesy&#322;anie"
  ]
  node [
    id 1002
    label "wall"
  ]
  node [
    id 1003
    label "profil"
  ]
  node [
    id 1004
    label "wprowadzi&#263;"
  ]
  node [
    id 1005
    label "wydawnictwo"
  ]
  node [
    id 1006
    label "upubliczni&#263;"
  ]
  node [
    id 1007
    label "wej&#347;&#263;"
  ]
  node [
    id 1008
    label "zacz&#261;&#263;"
  ]
  node [
    id 1009
    label "doprowadzi&#263;"
  ]
  node [
    id 1010
    label "rynek"
  ]
  node [
    id 1011
    label "zej&#347;&#263;"
  ]
  node [
    id 1012
    label "wpisa&#263;"
  ]
  node [
    id 1013
    label "zapozna&#263;"
  ]
  node [
    id 1014
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 1015
    label "insert"
  ]
  node [
    id 1016
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1017
    label "indicate"
  ]
  node [
    id 1018
    label "redakcja"
  ]
  node [
    id 1019
    label "druk"
  ]
  node [
    id 1020
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 1021
    label "poster"
  ]
  node [
    id 1022
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 1023
    label "redaktor"
  ]
  node [
    id 1024
    label "szata_graficzna"
  ]
  node [
    id 1025
    label "debit"
  ]
  node [
    id 1026
    label "wyda&#263;"
  ]
  node [
    id 1027
    label "wydawa&#263;"
  ]
  node [
    id 1028
    label "doba"
  ]
  node [
    id 1029
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 1030
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 1031
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 1032
    label "teraz"
  ]
  node [
    id 1033
    label "czas"
  ]
  node [
    id 1034
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1035
    label "jednocze&#347;nie"
  ]
  node [
    id 1036
    label "long_time"
  ]
  node [
    id 1037
    label "tydzie&#324;"
  ]
  node [
    id 1038
    label "po_brytyjsku"
  ]
  node [
    id 1039
    label "anglosaski"
  ]
  node [
    id 1040
    label "europejski"
  ]
  node [
    id 1041
    label "morris"
  ]
  node [
    id 1042
    label "j&#281;zyk_martwy"
  ]
  node [
    id 1043
    label "angielski"
  ]
  node [
    id 1044
    label "angielsko"
  ]
  node [
    id 1045
    label "brytyjsko"
  ]
  node [
    id 1046
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 1047
    label "zachodnioeuropejski"
  ]
  node [
    id 1048
    label "j&#281;zyk_angielski"
  ]
  node [
    id 1049
    label "anglosasko"
  ]
  node [
    id 1050
    label "po_anglosasku"
  ]
  node [
    id 1051
    label "typowy"
  ]
  node [
    id 1052
    label "europejsko"
  ]
  node [
    id 1053
    label "po_europejsku"
  ]
  node [
    id 1054
    label "European"
  ]
  node [
    id 1055
    label "English"
  ]
  node [
    id 1056
    label "po_angielsku"
  ]
  node [
    id 1057
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 1058
    label "j&#281;zyki_angielskie"
  ]
  node [
    id 1059
    label "anglicki"
  ]
  node [
    id 1060
    label "j&#281;zyk"
  ]
  node [
    id 1061
    label "angol"
  ]
  node [
    id 1062
    label "moreska"
  ]
  node [
    id 1063
    label "zachodni"
  ]
  node [
    id 1064
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 1065
    label "characteristically"
  ]
  node [
    id 1066
    label "taniec_ludowy"
  ]
  node [
    id 1067
    label "urz&#261;d"
  ]
  node [
    id 1068
    label "plankton_polityczny"
  ]
  node [
    id 1069
    label "europarlament"
  ]
  node [
    id 1070
    label "ustawodawca"
  ]
  node [
    id 1071
    label "grupa_bilateralna"
  ]
  node [
    id 1072
    label "autor"
  ]
  node [
    id 1073
    label "legislature"
  ]
  node [
    id 1074
    label "ustanowiciel"
  ]
  node [
    id 1075
    label "mianowaniec"
  ]
  node [
    id 1076
    label "okienko"
  ]
  node [
    id 1077
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1078
    label "position"
  ]
  node [
    id 1079
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1080
    label "stanowisko"
  ]
  node [
    id 1081
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1082
    label "Bruksela"
  ]
  node [
    id 1083
    label "eurowybory"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 3
    target 600
  ]
  edge [
    source 3
    target 601
  ]
  edge [
    source 3
    target 602
  ]
  edge [
    source 3
    target 603
  ]
  edge [
    source 3
    target 604
  ]
  edge [
    source 3
    target 605
  ]
  edge [
    source 3
    target 606
  ]
  edge [
    source 3
    target 607
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 612
  ]
  edge [
    source 3
    target 613
  ]
  edge [
    source 3
    target 614
  ]
  edge [
    source 3
    target 615
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 616
  ]
  edge [
    source 3
    target 617
  ]
  edge [
    source 3
    target 618
  ]
  edge [
    source 3
    target 619
  ]
  edge [
    source 3
    target 620
  ]
  edge [
    source 3
    target 621
  ]
  edge [
    source 3
    target 622
  ]
  edge [
    source 3
    target 623
  ]
  edge [
    source 3
    target 624
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 670
  ]
  edge [
    source 5
    target 671
  ]
  edge [
    source 5
    target 672
  ]
  edge [
    source 5
    target 673
  ]
  edge [
    source 5
    target 674
  ]
  edge [
    source 5
    target 675
  ]
  edge [
    source 5
    target 676
  ]
  edge [
    source 5
    target 677
  ]
  edge [
    source 5
    target 678
  ]
  edge [
    source 5
    target 679
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 769
  ]
  edge [
    source 6
    target 770
  ]
  edge [
    source 6
    target 771
  ]
  edge [
    source 6
    target 772
  ]
  edge [
    source 6
    target 773
  ]
  edge [
    source 6
    target 774
  ]
  edge [
    source 6
    target 775
  ]
  edge [
    source 6
    target 776
  ]
  edge [
    source 6
    target 777
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 784
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 822
  ]
  edge [
    source 11
    target 823
  ]
  edge [
    source 11
    target 824
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 158
  ]
  edge [
    source 11
    target 826
  ]
  edge [
    source 11
    target 827
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 828
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 830
  ]
  edge [
    source 11
    target 831
  ]
  edge [
    source 11
    target 832
  ]
  edge [
    source 11
    target 833
  ]
  edge [
    source 11
    target 834
  ]
  edge [
    source 11
    target 835
  ]
  edge [
    source 11
    target 836
  ]
  edge [
    source 11
    target 837
  ]
  edge [
    source 11
    target 838
  ]
  edge [
    source 11
    target 839
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 164
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 206
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 162
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 312
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 761
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 688
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 693
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 70
  ]
  edge [
    source 14
    target 354
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 977
  ]
  edge [
    source 15
    target 978
  ]
  edge [
    source 15
    target 979
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 981
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 986
  ]
  edge [
    source 15
    target 987
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 989
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 15
    target 992
  ]
  edge [
    source 15
    target 993
  ]
  edge [
    source 15
    target 994
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 426
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 995
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 15
    target 997
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 15
    target 999
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 426
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 532
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1038
  ]
  edge [
    source 20
    target 1039
  ]
  edge [
    source 20
    target 1040
  ]
  edge [
    source 20
    target 1041
  ]
  edge [
    source 20
    target 1042
  ]
  edge [
    source 20
    target 1043
  ]
  edge [
    source 20
    target 1044
  ]
  edge [
    source 20
    target 1045
  ]
  edge [
    source 20
    target 1046
  ]
  edge [
    source 20
    target 1047
  ]
  edge [
    source 20
    target 1048
  ]
  edge [
    source 20
    target 1049
  ]
  edge [
    source 20
    target 1050
  ]
  edge [
    source 20
    target 1051
  ]
  edge [
    source 20
    target 674
  ]
  edge [
    source 20
    target 60
  ]
  edge [
    source 20
    target 1052
  ]
  edge [
    source 20
    target 1053
  ]
  edge [
    source 20
    target 1054
  ]
  edge [
    source 20
    target 1055
  ]
  edge [
    source 20
    target 1056
  ]
  edge [
    source 20
    target 1057
  ]
  edge [
    source 20
    target 1058
  ]
  edge [
    source 20
    target 1059
  ]
  edge [
    source 20
    target 1060
  ]
  edge [
    source 20
    target 1061
  ]
  edge [
    source 20
    target 1062
  ]
  edge [
    source 20
    target 1063
  ]
  edge [
    source 20
    target 1064
  ]
  edge [
    source 20
    target 1065
  ]
  edge [
    source 20
    target 1066
  ]
  edge [
    source 21
    target 1067
  ]
  edge [
    source 21
    target 1068
  ]
  edge [
    source 21
    target 276
  ]
  edge [
    source 21
    target 1069
  ]
  edge [
    source 21
    target 1070
  ]
  edge [
    source 21
    target 1071
  ]
  edge [
    source 21
    target 1072
  ]
  edge [
    source 21
    target 1073
  ]
  edge [
    source 21
    target 498
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 693
  ]
  edge [
    source 21
    target 761
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 592
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 142
  ]
  edge [
    source 21
    target 296
  ]
  edge [
    source 21
    target 297
  ]
  edge [
    source 21
    target 298
  ]
  edge [
    source 21
    target 299
  ]
  edge [
    source 21
    target 300
  ]
  edge [
    source 21
    target 301
  ]
  edge [
    source 21
    target 302
  ]
  edge [
    source 21
    target 303
  ]
  edge [
    source 21
    target 304
  ]
  edge [
    source 21
    target 305
  ]
  edge [
    source 21
    target 306
  ]
  edge [
    source 21
    target 307
  ]
  edge [
    source 21
    target 308
  ]
  edge [
    source 21
    target 309
  ]
  edge [
    source 21
    target 310
  ]
  edge [
    source 21
    target 311
  ]
  edge [
    source 21
    target 312
  ]
  edge [
    source 21
    target 313
  ]
  edge [
    source 21
    target 314
  ]
  edge [
    source 21
    target 315
  ]
  edge [
    source 21
    target 272
  ]
  edge [
    source 21
    target 316
  ]
  edge [
    source 21
    target 317
  ]
  edge [
    source 21
    target 318
  ]
  edge [
    source 21
    target 319
  ]
  edge [
    source 21
    target 320
  ]
  edge [
    source 21
    target 321
  ]
  edge [
    source 21
    target 322
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
]
