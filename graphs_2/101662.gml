graph [
  node [
    id 0
    label "kolej"
    origin "text"
  ]
  node [
    id 1
    label "druga"
    origin "text"
  ]
  node [
    id 2
    label "propozycja"
    origin "text"
  ]
  node [
    id 3
    label "zawrze&#263;"
    origin "text"
  ]
  node [
    id 4
    label "druk"
    origin "text"
  ]
  node [
    id 5
    label "by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pewne"
    origin "text"
  ]
  node [
    id 7
    label "rodzaj"
    origin "text"
  ]
  node [
    id 8
    label "w&#261;tpliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "przyj&#281;ty"
    origin "text"
  ]
  node [
    id 10
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 11
    label "prawny"
    origin "text"
  ]
  node [
    id 12
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "zastosowanie"
    origin "text"
  ]
  node [
    id 14
    label "tylko"
    origin "text"
  ]
  node [
    id 15
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 16
    label "nadp&#322;ata"
    origin "text"
  ]
  node [
    id 17
    label "powsta&#322;y"
    origin "text"
  ]
  node [
    id 18
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 19
    label "wej&#347;cie"
    origin "text"
  ]
  node [
    id 20
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 21
    label "proponowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "zmiana"
    origin "text"
  ]
  node [
    id 23
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 24
    label "obecna"
    origin "text"
  ]
  node [
    id 25
    label "jeszcze"
    origin "text"
  ]
  node [
    id 26
    label "droga"
  ]
  node [
    id 27
    label "wagon"
  ]
  node [
    id 28
    label "lokomotywa"
  ]
  node [
    id 29
    label "trakcja"
  ]
  node [
    id 30
    label "run"
  ]
  node [
    id 31
    label "blokada"
  ]
  node [
    id 32
    label "kolejno&#347;&#263;"
  ]
  node [
    id 33
    label "tor"
  ]
  node [
    id 34
    label "pojazd_kolejowy"
  ]
  node [
    id 35
    label "linia"
  ]
  node [
    id 36
    label "tender"
  ]
  node [
    id 37
    label "proces"
  ]
  node [
    id 38
    label "cug"
  ]
  node [
    id 39
    label "pocz&#261;tek"
  ]
  node [
    id 40
    label "czas"
  ]
  node [
    id 41
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 42
    label "poci&#261;g"
  ]
  node [
    id 43
    label "cedu&#322;a"
  ]
  node [
    id 44
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 45
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 46
    label "nast&#281;pstwo"
  ]
  node [
    id 47
    label "kszta&#322;t"
  ]
  node [
    id 48
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 49
    label "armia"
  ]
  node [
    id 50
    label "cz&#322;owiek"
  ]
  node [
    id 51
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 52
    label "poprowadzi&#263;"
  ]
  node [
    id 53
    label "cord"
  ]
  node [
    id 54
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 55
    label "cecha"
  ]
  node [
    id 56
    label "trasa"
  ]
  node [
    id 57
    label "po&#322;&#261;czenie"
  ]
  node [
    id 58
    label "tract"
  ]
  node [
    id 59
    label "materia&#322;_zecerski"
  ]
  node [
    id 60
    label "przeorientowywanie"
  ]
  node [
    id 61
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 62
    label "curve"
  ]
  node [
    id 63
    label "figura_geometryczna"
  ]
  node [
    id 64
    label "wygl&#261;d"
  ]
  node [
    id 65
    label "zbi&#243;r"
  ]
  node [
    id 66
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 67
    label "jard"
  ]
  node [
    id 68
    label "szczep"
  ]
  node [
    id 69
    label "phreaker"
  ]
  node [
    id 70
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 71
    label "grupa_organizm&#243;w"
  ]
  node [
    id 72
    label "prowadzi&#263;"
  ]
  node [
    id 73
    label "przeorientowywa&#263;"
  ]
  node [
    id 74
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 75
    label "access"
  ]
  node [
    id 76
    label "przeorientowanie"
  ]
  node [
    id 77
    label "przeorientowa&#263;"
  ]
  node [
    id 78
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 79
    label "billing"
  ]
  node [
    id 80
    label "granica"
  ]
  node [
    id 81
    label "szpaler"
  ]
  node [
    id 82
    label "sztrych"
  ]
  node [
    id 83
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 84
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 85
    label "drzewo_genealogiczne"
  ]
  node [
    id 86
    label "transporter"
  ]
  node [
    id 87
    label "line"
  ]
  node [
    id 88
    label "fragment"
  ]
  node [
    id 89
    label "kompleksja"
  ]
  node [
    id 90
    label "przew&#243;d"
  ]
  node [
    id 91
    label "budowa"
  ]
  node [
    id 92
    label "granice"
  ]
  node [
    id 93
    label "kontakt"
  ]
  node [
    id 94
    label "rz&#261;d"
  ]
  node [
    id 95
    label "przewo&#378;nik"
  ]
  node [
    id 96
    label "przystanek"
  ]
  node [
    id 97
    label "linijka"
  ]
  node [
    id 98
    label "spos&#243;b"
  ]
  node [
    id 99
    label "uporz&#261;dkowanie"
  ]
  node [
    id 100
    label "coalescence"
  ]
  node [
    id 101
    label "Ural"
  ]
  node [
    id 102
    label "point"
  ]
  node [
    id 103
    label "bearing"
  ]
  node [
    id 104
    label "prowadzenie"
  ]
  node [
    id 105
    label "tekst"
  ]
  node [
    id 106
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 107
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 108
    label "koniec"
  ]
  node [
    id 109
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 110
    label "poprzedzanie"
  ]
  node [
    id 111
    label "czasoprzestrze&#324;"
  ]
  node [
    id 112
    label "laba"
  ]
  node [
    id 113
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 114
    label "chronometria"
  ]
  node [
    id 115
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 116
    label "rachuba_czasu"
  ]
  node [
    id 117
    label "przep&#322;ywanie"
  ]
  node [
    id 118
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 119
    label "czasokres"
  ]
  node [
    id 120
    label "odczyt"
  ]
  node [
    id 121
    label "chwila"
  ]
  node [
    id 122
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 123
    label "dzieje"
  ]
  node [
    id 124
    label "kategoria_gramatyczna"
  ]
  node [
    id 125
    label "poprzedzenie"
  ]
  node [
    id 126
    label "trawienie"
  ]
  node [
    id 127
    label "pochodzi&#263;"
  ]
  node [
    id 128
    label "period"
  ]
  node [
    id 129
    label "okres_czasu"
  ]
  node [
    id 130
    label "poprzedza&#263;"
  ]
  node [
    id 131
    label "schy&#322;ek"
  ]
  node [
    id 132
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 133
    label "odwlekanie_si&#281;"
  ]
  node [
    id 134
    label "zegar"
  ]
  node [
    id 135
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 136
    label "czwarty_wymiar"
  ]
  node [
    id 137
    label "pochodzenie"
  ]
  node [
    id 138
    label "koniugacja"
  ]
  node [
    id 139
    label "Zeitgeist"
  ]
  node [
    id 140
    label "trawi&#263;"
  ]
  node [
    id 141
    label "pogoda"
  ]
  node [
    id 142
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 143
    label "poprzedzi&#263;"
  ]
  node [
    id 144
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 145
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 146
    label "time_period"
  ]
  node [
    id 147
    label "ekskursja"
  ]
  node [
    id 148
    label "bezsilnikowy"
  ]
  node [
    id 149
    label "budowla"
  ]
  node [
    id 150
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 151
    label "podbieg"
  ]
  node [
    id 152
    label "turystyka"
  ]
  node [
    id 153
    label "nawierzchnia"
  ]
  node [
    id 154
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 155
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 156
    label "rajza"
  ]
  node [
    id 157
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 158
    label "korona_drogi"
  ]
  node [
    id 159
    label "passage"
  ]
  node [
    id 160
    label "wylot"
  ]
  node [
    id 161
    label "ekwipunek"
  ]
  node [
    id 162
    label "zbior&#243;wka"
  ]
  node [
    id 163
    label "marszrutyzacja"
  ]
  node [
    id 164
    label "wyb&#243;j"
  ]
  node [
    id 165
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 166
    label "drogowskaz"
  ]
  node [
    id 167
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 168
    label "pobocze"
  ]
  node [
    id 169
    label "journey"
  ]
  node [
    id 170
    label "ruch"
  ]
  node [
    id 171
    label "uk&#322;ad"
  ]
  node [
    id 172
    label "popyt"
  ]
  node [
    id 173
    label "podbijarka_torowa"
  ]
  node [
    id 174
    label "rozstaw_tr&#243;jstopowy"
  ]
  node [
    id 175
    label "torowisko"
  ]
  node [
    id 176
    label "szyna"
  ]
  node [
    id 177
    label "miejsce"
  ]
  node [
    id 178
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 179
    label "rozstaw_przyl&#261;dkowy"
  ]
  node [
    id 180
    label "linia_kolejowa"
  ]
  node [
    id 181
    label "lane"
  ]
  node [
    id 182
    label "nawierzchnia_kolejowa"
  ]
  node [
    id 183
    label "podk&#322;ad"
  ]
  node [
    id 184
    label "aktynowiec"
  ]
  node [
    id 185
    label "balastowanie"
  ]
  node [
    id 186
    label "rozstaw_bo&#347;niacki"
  ]
  node [
    id 187
    label "pierworodztwo"
  ]
  node [
    id 188
    label "faza"
  ]
  node [
    id 189
    label "upgrade"
  ]
  node [
    id 190
    label "statek"
  ]
  node [
    id 191
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 192
    label "okr&#281;t"
  ]
  node [
    id 193
    label "karton"
  ]
  node [
    id 194
    label "czo&#322;ownica"
  ]
  node [
    id 195
    label "harmonijka"
  ]
  node [
    id 196
    label "tramwaj"
  ]
  node [
    id 197
    label "klasa"
  ]
  node [
    id 198
    label "ciuchcia"
  ]
  node [
    id 199
    label "pojazd_trakcyjny"
  ]
  node [
    id 200
    label "raport"
  ]
  node [
    id 201
    label "transport"
  ]
  node [
    id 202
    label "kurs"
  ]
  node [
    id 203
    label "spis"
  ]
  node [
    id 204
    label "bilet"
  ]
  node [
    id 205
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 206
    label "bloking"
  ]
  node [
    id 207
    label "znieczulenie"
  ]
  node [
    id 208
    label "block"
  ]
  node [
    id 209
    label "utrudnienie"
  ]
  node [
    id 210
    label "arrest"
  ]
  node [
    id 211
    label "anestezja"
  ]
  node [
    id 212
    label "ochrona"
  ]
  node [
    id 213
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 214
    label "izolacja"
  ]
  node [
    id 215
    label "blok"
  ]
  node [
    id 216
    label "zwrotnica"
  ]
  node [
    id 217
    label "siatk&#243;wka"
  ]
  node [
    id 218
    label "sankcja"
  ]
  node [
    id 219
    label "semafor"
  ]
  node [
    id 220
    label "obrona"
  ]
  node [
    id 221
    label "deadlock"
  ]
  node [
    id 222
    label "lock"
  ]
  node [
    id 223
    label "niemo&#380;no&#347;&#263;"
  ]
  node [
    id 224
    label "urz&#261;dzenie"
  ]
  node [
    id 225
    label "kognicja"
  ]
  node [
    id 226
    label "przebieg"
  ]
  node [
    id 227
    label "rozprawa"
  ]
  node [
    id 228
    label "wydarzenie"
  ]
  node [
    id 229
    label "legislacyjnie"
  ]
  node [
    id 230
    label "przes&#322;anka"
  ]
  node [
    id 231
    label "zjawisko"
  ]
  node [
    id 232
    label "odczuwa&#263;"
  ]
  node [
    id 233
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 234
    label "wydziedziczy&#263;"
  ]
  node [
    id 235
    label "skrupienie_si&#281;"
  ]
  node [
    id 236
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 237
    label "wydziedziczenie"
  ]
  node [
    id 238
    label "odczucie"
  ]
  node [
    id 239
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 240
    label "koszula_Dejaniry"
  ]
  node [
    id 241
    label "odczuwanie"
  ]
  node [
    id 242
    label "event"
  ]
  node [
    id 243
    label "rezultat"
  ]
  node [
    id 244
    label "prawo"
  ]
  node [
    id 245
    label "skrupianie_si&#281;"
  ]
  node [
    id 246
    label "odczu&#263;"
  ]
  node [
    id 247
    label "para"
  ]
  node [
    id 248
    label "pr&#261;d"
  ]
  node [
    id 249
    label "draft"
  ]
  node [
    id 250
    label "stan"
  ]
  node [
    id 251
    label "&#347;l&#261;ski"
  ]
  node [
    id 252
    label "ci&#261;g"
  ]
  node [
    id 253
    label "zaprz&#281;g"
  ]
  node [
    id 254
    label "godzina"
  ]
  node [
    id 255
    label "time"
  ]
  node [
    id 256
    label "doba"
  ]
  node [
    id 257
    label "p&#243;&#322;godzina"
  ]
  node [
    id 258
    label "jednostka_czasu"
  ]
  node [
    id 259
    label "minuta"
  ]
  node [
    id 260
    label "kwadrans"
  ]
  node [
    id 261
    label "proposal"
  ]
  node [
    id 262
    label "pomys&#322;"
  ]
  node [
    id 263
    label "wytw&#243;r"
  ]
  node [
    id 264
    label "pocz&#261;tki"
  ]
  node [
    id 265
    label "ukra&#347;&#263;"
  ]
  node [
    id 266
    label "ukradzenie"
  ]
  node [
    id 267
    label "idea"
  ]
  node [
    id 268
    label "system"
  ]
  node [
    id 269
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 270
    label "straci&#263;_g&#322;ow&#281;"
  ]
  node [
    id 271
    label "sta&#263;_si&#281;"
  ]
  node [
    id 272
    label "raptowny"
  ]
  node [
    id 273
    label "insert"
  ]
  node [
    id 274
    label "incorporate"
  ]
  node [
    id 275
    label "pozna&#263;"
  ]
  node [
    id 276
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 277
    label "boil"
  ]
  node [
    id 278
    label "umowa"
  ]
  node [
    id 279
    label "ugotowa&#263;_si&#281;"
  ]
  node [
    id 280
    label "zamkn&#261;&#263;"
  ]
  node [
    id 281
    label "spot&#281;gowa&#263;_si&#281;"
  ]
  node [
    id 282
    label "ustali&#263;"
  ]
  node [
    id 283
    label "admit"
  ]
  node [
    id 284
    label "wezbra&#263;"
  ]
  node [
    id 285
    label "embrace"
  ]
  node [
    id 286
    label "zako&#324;czy&#263;"
  ]
  node [
    id 287
    label "put"
  ]
  node [
    id 288
    label "ukry&#263;"
  ]
  node [
    id 289
    label "zablokowa&#263;"
  ]
  node [
    id 290
    label "sko&#324;czy&#263;"
  ]
  node [
    id 291
    label "uj&#261;&#263;"
  ]
  node [
    id 292
    label "zatrzyma&#263;"
  ]
  node [
    id 293
    label "close"
  ]
  node [
    id 294
    label "rozwi&#261;za&#263;"
  ]
  node [
    id 295
    label "spowodowa&#263;"
  ]
  node [
    id 296
    label "kill"
  ]
  node [
    id 297
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 298
    label "umie&#347;ci&#263;"
  ]
  node [
    id 299
    label "udost&#281;pni&#263;"
  ]
  node [
    id 300
    label "obj&#261;&#263;"
  ]
  node [
    id 301
    label "clasp"
  ]
  node [
    id 302
    label "hold"
  ]
  node [
    id 303
    label "zdecydowa&#263;"
  ]
  node [
    id 304
    label "zrobi&#263;"
  ]
  node [
    id 305
    label "bind"
  ]
  node [
    id 306
    label "umocni&#263;"
  ]
  node [
    id 307
    label "unwrap"
  ]
  node [
    id 308
    label "zrozumie&#263;"
  ]
  node [
    id 309
    label "feel"
  ]
  node [
    id 310
    label "topographic_point"
  ]
  node [
    id 311
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 312
    label "visualize"
  ]
  node [
    id 313
    label "przyswoi&#263;"
  ]
  node [
    id 314
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 315
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 316
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 317
    label "teach"
  ]
  node [
    id 318
    label "experience"
  ]
  node [
    id 319
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 320
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 321
    label "wzm&#243;c_si&#281;"
  ]
  node [
    id 322
    label "nape&#322;ni&#263;_si&#281;"
  ]
  node [
    id 323
    label "zebra&#263;"
  ]
  node [
    id 324
    label "podda&#263;_si&#281;"
  ]
  node [
    id 325
    label "rise"
  ]
  node [
    id 326
    label "arise"
  ]
  node [
    id 327
    label "cover"
  ]
  node [
    id 328
    label "rozprz&#261;c"
  ]
  node [
    id 329
    label "treaty"
  ]
  node [
    id 330
    label "systemat"
  ]
  node [
    id 331
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 332
    label "struktura"
  ]
  node [
    id 333
    label "usenet"
  ]
  node [
    id 334
    label "przestawi&#263;"
  ]
  node [
    id 335
    label "alliance"
  ]
  node [
    id 336
    label "ONZ"
  ]
  node [
    id 337
    label "NATO"
  ]
  node [
    id 338
    label "konstelacja"
  ]
  node [
    id 339
    label "o&#347;"
  ]
  node [
    id 340
    label "podsystem"
  ]
  node [
    id 341
    label "zawarcie"
  ]
  node [
    id 342
    label "organ"
  ]
  node [
    id 343
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 344
    label "wi&#281;&#378;"
  ]
  node [
    id 345
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 346
    label "zachowanie"
  ]
  node [
    id 347
    label "cybernetyk"
  ]
  node [
    id 348
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 349
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 350
    label "sk&#322;ad"
  ]
  node [
    id 351
    label "traktat_wersalski"
  ]
  node [
    id 352
    label "cia&#322;o"
  ]
  node [
    id 353
    label "czyn"
  ]
  node [
    id 354
    label "warunek"
  ]
  node [
    id 355
    label "gestia_transportowa"
  ]
  node [
    id 356
    label "contract"
  ]
  node [
    id 357
    label "porozumienie"
  ]
  node [
    id 358
    label "klauzula"
  ]
  node [
    id 359
    label "szybki"
  ]
  node [
    id 360
    label "gwa&#322;towny"
  ]
  node [
    id 361
    label "zawrzenie"
  ]
  node [
    id 362
    label "nieoczekiwany"
  ]
  node [
    id 363
    label "raptownie"
  ]
  node [
    id 364
    label "technika"
  ]
  node [
    id 365
    label "impression"
  ]
  node [
    id 366
    label "pismo"
  ]
  node [
    id 367
    label "publikacja"
  ]
  node [
    id 368
    label "glif"
  ]
  node [
    id 369
    label "dese&#324;"
  ]
  node [
    id 370
    label "prohibita"
  ]
  node [
    id 371
    label "cymelium"
  ]
  node [
    id 372
    label "tkanina"
  ]
  node [
    id 373
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 374
    label "zaproszenie"
  ]
  node [
    id 375
    label "formatowanie"
  ]
  node [
    id 376
    label "formatowa&#263;"
  ]
  node [
    id 377
    label "zdobnik"
  ]
  node [
    id 378
    label "character"
  ]
  node [
    id 379
    label "printing"
  ]
  node [
    id 380
    label "telekomunikacja"
  ]
  node [
    id 381
    label "cywilizacja"
  ]
  node [
    id 382
    label "przedmiot"
  ]
  node [
    id 383
    label "wiedza"
  ]
  node [
    id 384
    label "sprawno&#347;&#263;"
  ]
  node [
    id 385
    label "engineering"
  ]
  node [
    id 386
    label "fotowoltaika"
  ]
  node [
    id 387
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 388
    label "teletechnika"
  ]
  node [
    id 389
    label "mechanika_precyzyjna"
  ]
  node [
    id 390
    label "technologia"
  ]
  node [
    id 391
    label "p&#322;&#243;d"
  ]
  node [
    id 392
    label "work"
  ]
  node [
    id 393
    label "psychotest"
  ]
  node [
    id 394
    label "wk&#322;ad"
  ]
  node [
    id 395
    label "handwriting"
  ]
  node [
    id 396
    label "przekaz"
  ]
  node [
    id 397
    label "dzie&#322;o"
  ]
  node [
    id 398
    label "paleograf"
  ]
  node [
    id 399
    label "interpunkcja"
  ]
  node [
    id 400
    label "dzia&#322;"
  ]
  node [
    id 401
    label "grafia"
  ]
  node [
    id 402
    label "egzemplarz"
  ]
  node [
    id 403
    label "communication"
  ]
  node [
    id 404
    label "script"
  ]
  node [
    id 405
    label "zajawka"
  ]
  node [
    id 406
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 407
    label "list"
  ]
  node [
    id 408
    label "adres"
  ]
  node [
    id 409
    label "Zwrotnica"
  ]
  node [
    id 410
    label "czasopismo"
  ]
  node [
    id 411
    label "ok&#322;adka"
  ]
  node [
    id 412
    label "ortografia"
  ]
  node [
    id 413
    label "letter"
  ]
  node [
    id 414
    label "komunikacja"
  ]
  node [
    id 415
    label "paleografia"
  ]
  node [
    id 416
    label "j&#281;zyk"
  ]
  node [
    id 417
    label "dokument"
  ]
  node [
    id 418
    label "prasa"
  ]
  node [
    id 419
    label "wz&#243;r"
  ]
  node [
    id 420
    label "design"
  ]
  node [
    id 421
    label "produkcja"
  ]
  node [
    id 422
    label "notification"
  ]
  node [
    id 423
    label "pru&#263;_si&#281;"
  ]
  node [
    id 424
    label "materia&#322;"
  ]
  node [
    id 425
    label "maglownia"
  ]
  node [
    id 426
    label "opalarnia"
  ]
  node [
    id 427
    label "prucie_si&#281;"
  ]
  node [
    id 428
    label "splot"
  ]
  node [
    id 429
    label "karbonizowa&#263;"
  ]
  node [
    id 430
    label "rozpru&#263;_si&#281;"
  ]
  node [
    id 431
    label "karbonizacja"
  ]
  node [
    id 432
    label "rozprucie_si&#281;"
  ]
  node [
    id 433
    label "towar"
  ]
  node [
    id 434
    label "apretura"
  ]
  node [
    id 435
    label "ekscerpcja"
  ]
  node [
    id 436
    label "j&#281;zykowo"
  ]
  node [
    id 437
    label "wypowied&#378;"
  ]
  node [
    id 438
    label "redakcja"
  ]
  node [
    id 439
    label "pomini&#281;cie"
  ]
  node [
    id 440
    label "preparacja"
  ]
  node [
    id 441
    label "odmianka"
  ]
  node [
    id 442
    label "opu&#347;ci&#263;"
  ]
  node [
    id 443
    label "koniektura"
  ]
  node [
    id 444
    label "pisa&#263;"
  ]
  node [
    id 445
    label "obelga"
  ]
  node [
    id 446
    label "czcionka"
  ]
  node [
    id 447
    label "kr&#243;j"
  ]
  node [
    id 448
    label "splay"
  ]
  node [
    id 449
    label "symbol"
  ]
  node [
    id 450
    label "zmienianie"
  ]
  node [
    id 451
    label "przygotowywanie"
  ]
  node [
    id 452
    label "sk&#322;adanie"
  ]
  node [
    id 453
    label "edytowanie"
  ]
  node [
    id 454
    label "format"
  ]
  node [
    id 455
    label "dostosowywanie"
  ]
  node [
    id 456
    label "rarytas"
  ]
  node [
    id 457
    label "zapis"
  ]
  node [
    id 458
    label "r&#281;kopis"
  ]
  node [
    id 459
    label "wzajemno&#347;&#263;"
  ]
  node [
    id 460
    label "podw&#243;jno&#347;&#263;"
  ]
  node [
    id 461
    label "pro&#347;ba"
  ]
  node [
    id 462
    label "invitation"
  ]
  node [
    id 463
    label "karteczka"
  ]
  node [
    id 464
    label "zaproponowanie"
  ]
  node [
    id 465
    label "edytowa&#263;"
  ]
  node [
    id 466
    label "dostosowywa&#263;"
  ]
  node [
    id 467
    label "sk&#322;ada&#263;"
  ]
  node [
    id 468
    label "przygotowywa&#263;"
  ]
  node [
    id 469
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 470
    label "mie&#263;_miejsce"
  ]
  node [
    id 471
    label "equal"
  ]
  node [
    id 472
    label "trwa&#263;"
  ]
  node [
    id 473
    label "chodzi&#263;"
  ]
  node [
    id 474
    label "si&#281;ga&#263;"
  ]
  node [
    id 475
    label "obecno&#347;&#263;"
  ]
  node [
    id 476
    label "stand"
  ]
  node [
    id 477
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 478
    label "uczestniczy&#263;"
  ]
  node [
    id 479
    label "participate"
  ]
  node [
    id 480
    label "robi&#263;"
  ]
  node [
    id 481
    label "istnie&#263;"
  ]
  node [
    id 482
    label "pozostawa&#263;"
  ]
  node [
    id 483
    label "zostawa&#263;"
  ]
  node [
    id 484
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 485
    label "adhere"
  ]
  node [
    id 486
    label "compass"
  ]
  node [
    id 487
    label "korzysta&#263;"
  ]
  node [
    id 488
    label "appreciation"
  ]
  node [
    id 489
    label "osi&#261;ga&#263;"
  ]
  node [
    id 490
    label "dociera&#263;"
  ]
  node [
    id 491
    label "get"
  ]
  node [
    id 492
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 493
    label "mierzy&#263;"
  ]
  node [
    id 494
    label "u&#380;ywa&#263;"
  ]
  node [
    id 495
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 496
    label "exsert"
  ]
  node [
    id 497
    label "being"
  ]
  node [
    id 498
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 499
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 500
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 501
    label "p&#322;ywa&#263;"
  ]
  node [
    id 502
    label "bangla&#263;"
  ]
  node [
    id 503
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 504
    label "przebiega&#263;"
  ]
  node [
    id 505
    label "wk&#322;ada&#263;"
  ]
  node [
    id 506
    label "proceed"
  ]
  node [
    id 507
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 508
    label "carry"
  ]
  node [
    id 509
    label "bywa&#263;"
  ]
  node [
    id 510
    label "dziama&#263;"
  ]
  node [
    id 511
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 512
    label "stara&#263;_si&#281;"
  ]
  node [
    id 513
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 514
    label "str&#243;j"
  ]
  node [
    id 515
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 516
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 517
    label "krok"
  ]
  node [
    id 518
    label "tryb"
  ]
  node [
    id 519
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 520
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 521
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 522
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 523
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 524
    label "continue"
  ]
  node [
    id 525
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 526
    label "Ohio"
  ]
  node [
    id 527
    label "wci&#281;cie"
  ]
  node [
    id 528
    label "Nowy_York"
  ]
  node [
    id 529
    label "warstwa"
  ]
  node [
    id 530
    label "samopoczucie"
  ]
  node [
    id 531
    label "Illinois"
  ]
  node [
    id 532
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 533
    label "state"
  ]
  node [
    id 534
    label "Jukatan"
  ]
  node [
    id 535
    label "Kalifornia"
  ]
  node [
    id 536
    label "Wirginia"
  ]
  node [
    id 537
    label "wektor"
  ]
  node [
    id 538
    label "Goa"
  ]
  node [
    id 539
    label "Teksas"
  ]
  node [
    id 540
    label "Waszyngton"
  ]
  node [
    id 541
    label "Massachusetts"
  ]
  node [
    id 542
    label "Alaska"
  ]
  node [
    id 543
    label "Arakan"
  ]
  node [
    id 544
    label "Hawaje"
  ]
  node [
    id 545
    label "Maryland"
  ]
  node [
    id 546
    label "punkt"
  ]
  node [
    id 547
    label "Michigan"
  ]
  node [
    id 548
    label "Arizona"
  ]
  node [
    id 549
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 550
    label "Georgia"
  ]
  node [
    id 551
    label "poziom"
  ]
  node [
    id 552
    label "Pensylwania"
  ]
  node [
    id 553
    label "shape"
  ]
  node [
    id 554
    label "Luizjana"
  ]
  node [
    id 555
    label "Nowy_Meksyk"
  ]
  node [
    id 556
    label "Alabama"
  ]
  node [
    id 557
    label "ilo&#347;&#263;"
  ]
  node [
    id 558
    label "Kansas"
  ]
  node [
    id 559
    label "Oregon"
  ]
  node [
    id 560
    label "Oklahoma"
  ]
  node [
    id 561
    label "Floryda"
  ]
  node [
    id 562
    label "jednostka_administracyjna"
  ]
  node [
    id 563
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 564
    label "rodzina"
  ]
  node [
    id 565
    label "fashion"
  ]
  node [
    id 566
    label "jednostka_systematyczna"
  ]
  node [
    id 567
    label "autorament"
  ]
  node [
    id 568
    label "variety"
  ]
  node [
    id 569
    label "pob&#243;r"
  ]
  node [
    id 570
    label "wojsko"
  ]
  node [
    id 571
    label "typ"
  ]
  node [
    id 572
    label "type"
  ]
  node [
    id 573
    label "powinowaci"
  ]
  node [
    id 574
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 575
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 576
    label "rodze&#324;stwo"
  ]
  node [
    id 577
    label "krewni"
  ]
  node [
    id 578
    label "Ossoli&#324;scy"
  ]
  node [
    id 579
    label "potomstwo"
  ]
  node [
    id 580
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 581
    label "theater"
  ]
  node [
    id 582
    label "Soplicowie"
  ]
  node [
    id 583
    label "kin"
  ]
  node [
    id 584
    label "family"
  ]
  node [
    id 585
    label "rodzice"
  ]
  node [
    id 586
    label "ordynacja"
  ]
  node [
    id 587
    label "grupa"
  ]
  node [
    id 588
    label "dom_rodzinny"
  ]
  node [
    id 589
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 590
    label "Ostrogscy"
  ]
  node [
    id 591
    label "bliscy"
  ]
  node [
    id 592
    label "przyjaciel_domu"
  ]
  node [
    id 593
    label "dom"
  ]
  node [
    id 594
    label "Firlejowie"
  ]
  node [
    id 595
    label "Kossakowie"
  ]
  node [
    id 596
    label "Czartoryscy"
  ]
  node [
    id 597
    label "Sapiehowie"
  ]
  node [
    id 598
    label "w&#261;tpienie"
  ]
  node [
    id 599
    label "question"
  ]
  node [
    id 600
    label "pos&#322;uchanie"
  ]
  node [
    id 601
    label "s&#261;d"
  ]
  node [
    id 602
    label "sparafrazowanie"
  ]
  node [
    id 603
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 604
    label "strawestowa&#263;"
  ]
  node [
    id 605
    label "sparafrazowa&#263;"
  ]
  node [
    id 606
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 607
    label "trawestowa&#263;"
  ]
  node [
    id 608
    label "sformu&#322;owanie"
  ]
  node [
    id 609
    label "parafrazowanie"
  ]
  node [
    id 610
    label "ozdobnik"
  ]
  node [
    id 611
    label "delimitacja"
  ]
  node [
    id 612
    label "parafrazowa&#263;"
  ]
  node [
    id 613
    label "stylizacja"
  ]
  node [
    id 614
    label "komunikat"
  ]
  node [
    id 615
    label "trawestowanie"
  ]
  node [
    id 616
    label "strawestowanie"
  ]
  node [
    id 617
    label "doubt"
  ]
  node [
    id 618
    label "bycie"
  ]
  node [
    id 619
    label "obowi&#261;zuj&#261;cy"
  ]
  node [
    id 620
    label "znajomy"
  ]
  node [
    id 621
    label "powszechny"
  ]
  node [
    id 622
    label "uprawomocnienie_si&#281;"
  ]
  node [
    id 623
    label "uprawomocnianie_si&#281;"
  ]
  node [
    id 624
    label "obowi&#261;zuj&#261;co"
  ]
  node [
    id 625
    label "aktualny"
  ]
  node [
    id 626
    label "znany"
  ]
  node [
    id 627
    label "zbiorowy"
  ]
  node [
    id 628
    label "cz&#281;sty"
  ]
  node [
    id 629
    label "powszechnie"
  ]
  node [
    id 630
    label "generalny"
  ]
  node [
    id 631
    label "zapoznanie"
  ]
  node [
    id 632
    label "sw&#243;j"
  ]
  node [
    id 633
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 634
    label "zapoznanie_si&#281;"
  ]
  node [
    id 635
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 636
    label "znajomek"
  ]
  node [
    id 637
    label "zapoznawanie"
  ]
  node [
    id 638
    label "znajomo"
  ]
  node [
    id 639
    label "pewien"
  ]
  node [
    id 640
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 641
    label "za_pan_brat"
  ]
  node [
    id 642
    label "po&#322;&#243;g"
  ]
  node [
    id 643
    label "spe&#322;nienie"
  ]
  node [
    id 644
    label "dula"
  ]
  node [
    id 645
    label "usuni&#281;cie"
  ]
  node [
    id 646
    label "wymy&#347;lenie"
  ]
  node [
    id 647
    label "po&#322;o&#380;na"
  ]
  node [
    id 648
    label "wyj&#347;cie"
  ]
  node [
    id 649
    label "uniewa&#380;nienie"
  ]
  node [
    id 650
    label "proces_fizjologiczny"
  ]
  node [
    id 651
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 652
    label "szok_poporodowy"
  ]
  node [
    id 653
    label "marc&#243;wka"
  ]
  node [
    id 654
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 655
    label "birth"
  ]
  node [
    id 656
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 657
    label "wynik"
  ]
  node [
    id 658
    label "przestanie"
  ]
  node [
    id 659
    label "wyniesienie"
  ]
  node [
    id 660
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 661
    label "odej&#347;cie"
  ]
  node [
    id 662
    label "pozabieranie"
  ]
  node [
    id 663
    label "pozbycie_si&#281;"
  ]
  node [
    id 664
    label "pousuwanie"
  ]
  node [
    id 665
    label "przesuni&#281;cie"
  ]
  node [
    id 666
    label "przeniesienie"
  ]
  node [
    id 667
    label "znikni&#281;cie"
  ]
  node [
    id 668
    label "spowodowanie"
  ]
  node [
    id 669
    label "coitus_interruptus"
  ]
  node [
    id 670
    label "abstraction"
  ]
  node [
    id 671
    label "removal"
  ]
  node [
    id 672
    label "czynno&#347;&#263;"
  ]
  node [
    id 673
    label "wyrugowanie"
  ]
  node [
    id 674
    label "realizowa&#263;_si&#281;"
  ]
  node [
    id 675
    label "urzeczywistnienie"
  ]
  node [
    id 676
    label "emocja"
  ]
  node [
    id 677
    label "completion"
  ]
  node [
    id 678
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 679
    label "ziszczenie_si&#281;"
  ]
  node [
    id 680
    label "realization"
  ]
  node [
    id 681
    label "pe&#322;ny"
  ]
  node [
    id 682
    label "realizowanie_si&#281;"
  ]
  node [
    id 683
    label "enjoyment"
  ]
  node [
    id 684
    label "gratyfikacja"
  ]
  node [
    id 685
    label "zrobienie"
  ]
  node [
    id 686
    label "model"
  ]
  node [
    id 687
    label "narz&#281;dzie"
  ]
  node [
    id 688
    label "nature"
  ]
  node [
    id 689
    label "invention"
  ]
  node [
    id 690
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 691
    label "oduczenie"
  ]
  node [
    id 692
    label "disavowal"
  ]
  node [
    id 693
    label "zako&#324;czenie"
  ]
  node [
    id 694
    label "cessation"
  ]
  node [
    id 695
    label "przeczekanie"
  ]
  node [
    id 696
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 697
    label "okazanie_si&#281;"
  ]
  node [
    id 698
    label "ograniczenie"
  ]
  node [
    id 699
    label "uzyskanie"
  ]
  node [
    id 700
    label "ruszenie"
  ]
  node [
    id 701
    label "podzianie_si&#281;"
  ]
  node [
    id 702
    label "spotkanie"
  ]
  node [
    id 703
    label "powychodzenie"
  ]
  node [
    id 704
    label "opuszczenie"
  ]
  node [
    id 705
    label "postrze&#380;enie"
  ]
  node [
    id 706
    label "transgression"
  ]
  node [
    id 707
    label "wyczerpanie_si&#281;"
  ]
  node [
    id 708
    label "wychodzenie"
  ]
  node [
    id 709
    label "uko&#324;czenie"
  ]
  node [
    id 710
    label "powiedzenie_si&#281;"
  ]
  node [
    id 711
    label "policzenie"
  ]
  node [
    id 712
    label "podziewanie_si&#281;"
  ]
  node [
    id 713
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 714
    label "exit"
  ]
  node [
    id 715
    label "vent"
  ]
  node [
    id 716
    label "uwolnienie_si&#281;"
  ]
  node [
    id 717
    label "deviation"
  ]
  node [
    id 718
    label "release"
  ]
  node [
    id 719
    label "wych&#243;d"
  ]
  node [
    id 720
    label "withdrawal"
  ]
  node [
    id 721
    label "wypadni&#281;cie"
  ]
  node [
    id 722
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 723
    label "kres"
  ]
  node [
    id 724
    label "odch&#243;d"
  ]
  node [
    id 725
    label "przebywanie"
  ]
  node [
    id 726
    label "przedstawienie"
  ]
  node [
    id 727
    label "wywiedzenie_si&#281;"
  ]
  node [
    id 728
    label "zagranie"
  ]
  node [
    id 729
    label "ukazanie_si&#281;_drukiem"
  ]
  node [
    id 730
    label "emergence"
  ]
  node [
    id 731
    label "zaokr&#261;glenie"
  ]
  node [
    id 732
    label "dzia&#322;anie"
  ]
  node [
    id 733
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 734
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 735
    label "przyczyna"
  ]
  node [
    id 736
    label "retraction"
  ]
  node [
    id 737
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 738
    label "zerwanie"
  ]
  node [
    id 739
    label "konsekwencja"
  ]
  node [
    id 740
    label "przyjmowa&#263;_por&#243;d"
  ]
  node [
    id 741
    label "przyj&#261;&#263;_por&#243;d"
  ]
  node [
    id 742
    label "babka"
  ]
  node [
    id 743
    label "piel&#281;gniarka"
  ]
  node [
    id 744
    label "zabory"
  ]
  node [
    id 745
    label "ci&#281;&#380;arna"
  ]
  node [
    id 746
    label "asystentka"
  ]
  node [
    id 747
    label "pomoc"
  ]
  node [
    id 748
    label "zlec"
  ]
  node [
    id 749
    label "zlegni&#281;cie"
  ]
  node [
    id 750
    label "konstytucyjnoprawny"
  ]
  node [
    id 751
    label "prawniczo"
  ]
  node [
    id 752
    label "prawnie"
  ]
  node [
    id 753
    label "legalny"
  ]
  node [
    id 754
    label "jurydyczny"
  ]
  node [
    id 755
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 756
    label "urz&#281;dowo"
  ]
  node [
    id 757
    label "prawniczy"
  ]
  node [
    id 758
    label "legalnie"
  ]
  node [
    id 759
    label "gajny"
  ]
  node [
    id 760
    label "hide"
  ]
  node [
    id 761
    label "czu&#263;"
  ]
  node [
    id 762
    label "support"
  ]
  node [
    id 763
    label "need"
  ]
  node [
    id 764
    label "wykonawca"
  ]
  node [
    id 765
    label "interpretator"
  ]
  node [
    id 766
    label "postrzega&#263;"
  ]
  node [
    id 767
    label "przewidywa&#263;"
  ]
  node [
    id 768
    label "smell"
  ]
  node [
    id 769
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 770
    label "uczuwa&#263;"
  ]
  node [
    id 771
    label "spirit"
  ]
  node [
    id 772
    label "doznawa&#263;"
  ]
  node [
    id 773
    label "anticipate"
  ]
  node [
    id 774
    label "stosowanie"
  ]
  node [
    id 775
    label "cel"
  ]
  node [
    id 776
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 777
    label "funkcja"
  ]
  node [
    id 778
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 779
    label "use"
  ]
  node [
    id 780
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 781
    label "thing"
  ]
  node [
    id 782
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 783
    label "rzecz"
  ]
  node [
    id 784
    label "narobienie"
  ]
  node [
    id 785
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 786
    label "creation"
  ]
  node [
    id 787
    label "porobienie"
  ]
  node [
    id 788
    label "supremum"
  ]
  node [
    id 789
    label "addytywno&#347;&#263;"
  ]
  node [
    id 790
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 791
    label "jednostka"
  ]
  node [
    id 792
    label "function"
  ]
  node [
    id 793
    label "matematyka"
  ]
  node [
    id 794
    label "funkcjonowanie"
  ]
  node [
    id 795
    label "praca"
  ]
  node [
    id 796
    label "rzut"
  ]
  node [
    id 797
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 798
    label "powierzanie"
  ]
  node [
    id 799
    label "dziedzina"
  ]
  node [
    id 800
    label "przeciwdziedzina"
  ]
  node [
    id 801
    label "awansowa&#263;"
  ]
  node [
    id 802
    label "stawia&#263;"
  ]
  node [
    id 803
    label "wakowa&#263;"
  ]
  node [
    id 804
    label "znaczenie"
  ]
  node [
    id 805
    label "postawi&#263;"
  ]
  node [
    id 806
    label "awansowanie"
  ]
  node [
    id 807
    label "infimum"
  ]
  node [
    id 808
    label "przejaskrawianie"
  ]
  node [
    id 809
    label "zniszczenie"
  ]
  node [
    id 810
    label "robienie"
  ]
  node [
    id 811
    label "zu&#380;ywanie"
  ]
  node [
    id 812
    label "wy&#322;&#261;czny"
  ]
  node [
    id 813
    label "w&#322;asny"
  ]
  node [
    id 814
    label "unikatowy"
  ]
  node [
    id 815
    label "jedyny"
  ]
  node [
    id 816
    label "nadwy&#380;ka"
  ]
  node [
    id 817
    label "saldo"
  ]
  node [
    id 818
    label "z_nawi&#261;zk&#261;"
  ]
  node [
    id 819
    label "nadmiar"
  ]
  node [
    id 820
    label "warto&#347;&#263;"
  ]
  node [
    id 821
    label "statement"
  ]
  node [
    id 822
    label "relacja"
  ]
  node [
    id 823
    label "r&#243;&#380;nica"
  ]
  node [
    id 824
    label "ranek"
  ]
  node [
    id 825
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 826
    label "noc"
  ]
  node [
    id 827
    label "podwiecz&#243;r"
  ]
  node [
    id 828
    label "po&#322;udnie"
  ]
  node [
    id 829
    label "przedpo&#322;udnie"
  ]
  node [
    id 830
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 831
    label "long_time"
  ]
  node [
    id 832
    label "wiecz&#243;r"
  ]
  node [
    id 833
    label "t&#322;usty_czwartek"
  ]
  node [
    id 834
    label "popo&#322;udnie"
  ]
  node [
    id 835
    label "walentynki"
  ]
  node [
    id 836
    label "czynienie_si&#281;"
  ]
  node [
    id 837
    label "s&#322;o&#324;ce"
  ]
  node [
    id 838
    label "rano"
  ]
  node [
    id 839
    label "tydzie&#324;"
  ]
  node [
    id 840
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 841
    label "wzej&#347;cie"
  ]
  node [
    id 842
    label "wsta&#263;"
  ]
  node [
    id 843
    label "day"
  ]
  node [
    id 844
    label "termin"
  ]
  node [
    id 845
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 846
    label "wstanie"
  ]
  node [
    id 847
    label "przedwiecz&#243;r"
  ]
  node [
    id 848
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 849
    label "Sylwester"
  ]
  node [
    id 850
    label "nazewnictwo"
  ]
  node [
    id 851
    label "term"
  ]
  node [
    id 852
    label "przypadni&#281;cie"
  ]
  node [
    id 853
    label "ekspiracja"
  ]
  node [
    id 854
    label "przypa&#347;&#263;"
  ]
  node [
    id 855
    label "chronogram"
  ]
  node [
    id 856
    label "praktyka"
  ]
  node [
    id 857
    label "nazwa"
  ]
  node [
    id 858
    label "przyj&#281;cie"
  ]
  node [
    id 859
    label "night"
  ]
  node [
    id 860
    label "zach&#243;d"
  ]
  node [
    id 861
    label "vesper"
  ]
  node [
    id 862
    label "pora"
  ]
  node [
    id 863
    label "odwieczerz"
  ]
  node [
    id 864
    label "blady_&#347;wit"
  ]
  node [
    id 865
    label "podkurek"
  ]
  node [
    id 866
    label "aurora"
  ]
  node [
    id 867
    label "wsch&#243;d"
  ]
  node [
    id 868
    label "&#347;rodek"
  ]
  node [
    id 869
    label "obszar"
  ]
  node [
    id 870
    label "Ziemia"
  ]
  node [
    id 871
    label "dwunasta"
  ]
  node [
    id 872
    label "strona_&#347;wiata"
  ]
  node [
    id 873
    label "dopo&#322;udnie"
  ]
  node [
    id 874
    label "p&#243;&#322;noc"
  ]
  node [
    id 875
    label "nokturn"
  ]
  node [
    id 876
    label "jednostka_geologiczna"
  ]
  node [
    id 877
    label "weekend"
  ]
  node [
    id 878
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 879
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 880
    label "miesi&#261;c"
  ]
  node [
    id 881
    label "S&#322;o&#324;ce"
  ]
  node [
    id 882
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 883
    label "&#347;wiat&#322;o"
  ]
  node [
    id 884
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 885
    label "kochanie"
  ]
  node [
    id 886
    label "sunlight"
  ]
  node [
    id 887
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 888
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 889
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 890
    label "mount"
  ]
  node [
    id 891
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 892
    label "wzej&#347;&#263;"
  ]
  node [
    id 893
    label "ascend"
  ]
  node [
    id 894
    label "kuca&#263;"
  ]
  node [
    id 895
    label "wyzdrowie&#263;"
  ]
  node [
    id 896
    label "stan&#261;&#263;"
  ]
  node [
    id 897
    label "przesta&#263;"
  ]
  node [
    id 898
    label "wyzdrowienie"
  ]
  node [
    id 899
    label "le&#380;enie"
  ]
  node [
    id 900
    label "kl&#281;czenie"
  ]
  node [
    id 901
    label "uniesienie_si&#281;"
  ]
  node [
    id 902
    label "siedzenie"
  ]
  node [
    id 903
    label "beginning"
  ]
  node [
    id 904
    label "grudzie&#324;"
  ]
  node [
    id 905
    label "luty"
  ]
  node [
    id 906
    label "wnikni&#281;cie"
  ]
  node [
    id 907
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 908
    label "poznanie"
  ]
  node [
    id 909
    label "pojawienie_si&#281;"
  ]
  node [
    id 910
    label "zdarzenie_si&#281;"
  ]
  node [
    id 911
    label "przenikni&#281;cie"
  ]
  node [
    id 912
    label "wpuszczenie"
  ]
  node [
    id 913
    label "zaatakowanie"
  ]
  node [
    id 914
    label "trespass"
  ]
  node [
    id 915
    label "dost&#281;p"
  ]
  node [
    id 916
    label "doj&#347;cie"
  ]
  node [
    id 917
    label "przekroczenie"
  ]
  node [
    id 918
    label "otw&#243;r"
  ]
  node [
    id 919
    label "wzi&#281;cie"
  ]
  node [
    id 920
    label "stimulation"
  ]
  node [
    id 921
    label "dostanie_si&#281;"
  ]
  node [
    id 922
    label "approach"
  ]
  node [
    id 923
    label "wnij&#347;cie"
  ]
  node [
    id 924
    label "bramka"
  ]
  node [
    id 925
    label "wzniesienie_si&#281;"
  ]
  node [
    id 926
    label "podw&#243;rze"
  ]
  node [
    id 927
    label "wch&#243;d"
  ]
  node [
    id 928
    label "nast&#261;pienie"
  ]
  node [
    id 929
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 930
    label "zacz&#281;cie"
  ]
  node [
    id 931
    label "cz&#322;onek"
  ]
  node [
    id 932
    label "stanie_si&#281;"
  ]
  node [
    id 933
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 934
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 935
    label "discourtesy"
  ]
  node [
    id 936
    label "odj&#281;cie"
  ]
  node [
    id 937
    label "post&#261;pienie"
  ]
  node [
    id 938
    label "opening"
  ]
  node [
    id 939
    label "zrobienie_pierwszego_kroku"
  ]
  node [
    id 940
    label "wyra&#380;enie"
  ]
  node [
    id 941
    label "porobienie_si&#281;"
  ]
  node [
    id 942
    label "naci&#347;ni&#281;cie"
  ]
  node [
    id 943
    label "chodzenie"
  ]
  node [
    id 944
    label "advent"
  ]
  node [
    id 945
    label "dochrapanie_si&#281;"
  ]
  node [
    id 946
    label "skill"
  ]
  node [
    id 947
    label "accomplishment"
  ]
  node [
    id 948
    label "sukces"
  ]
  node [
    id 949
    label "zaawansowanie"
  ]
  node [
    id 950
    label "dotarcie"
  ]
  node [
    id 951
    label "act"
  ]
  node [
    id 952
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 953
    label "doznanie"
  ]
  node [
    id 954
    label "gathering"
  ]
  node [
    id 955
    label "powitanie"
  ]
  node [
    id 956
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 957
    label "znalezienie"
  ]
  node [
    id 958
    label "match"
  ]
  node [
    id 959
    label "employment"
  ]
  node [
    id 960
    label "po&#380;egnanie"
  ]
  node [
    id 961
    label "gather"
  ]
  node [
    id 962
    label "spotykanie"
  ]
  node [
    id 963
    label "spotkanie_si&#281;"
  ]
  node [
    id 964
    label "dochodzenie"
  ]
  node [
    id 965
    label "znajomo&#347;ci"
  ]
  node [
    id 966
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 967
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 968
    label "powi&#261;zanie"
  ]
  node [
    id 969
    label "entrance"
  ]
  node [
    id 970
    label "affiliation"
  ]
  node [
    id 971
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 972
    label "dor&#281;czenie"
  ]
  node [
    id 973
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 974
    label "bodziec"
  ]
  node [
    id 975
    label "informacja"
  ]
  node [
    id 976
    label "przesy&#322;ka"
  ]
  node [
    id 977
    label "gotowy"
  ]
  node [
    id 978
    label "avenue"
  ]
  node [
    id 979
    label "postrzeganie"
  ]
  node [
    id 980
    label "dodatek"
  ]
  node [
    id 981
    label "dojrza&#322;y"
  ]
  node [
    id 982
    label "dojechanie"
  ]
  node [
    id 983
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 984
    label "ingress"
  ]
  node [
    id 985
    label "strzelenie"
  ]
  node [
    id 986
    label "orzekni&#281;cie"
  ]
  node [
    id 987
    label "orgazm"
  ]
  node [
    id 988
    label "dolecenie"
  ]
  node [
    id 989
    label "rozpowszechnienie"
  ]
  node [
    id 990
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 991
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 992
    label "dop&#322;ata"
  ]
  node [
    id 993
    label "informatyka"
  ]
  node [
    id 994
    label "operacja"
  ]
  node [
    id 995
    label "konto"
  ]
  node [
    id 996
    label "has&#322;o"
  ]
  node [
    id 997
    label "skrytykowanie"
  ]
  node [
    id 998
    label "walka"
  ]
  node [
    id 999
    label "manewr"
  ]
  node [
    id 1000
    label "oddzia&#322;anie"
  ]
  node [
    id 1001
    label "przebycie"
  ]
  node [
    id 1002
    label "upolowanie"
  ]
  node [
    id 1003
    label "wdarcie_si&#281;"
  ]
  node [
    id 1004
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 1005
    label "sport"
  ]
  node [
    id 1006
    label "progress"
  ]
  node [
    id 1007
    label "spr&#243;bowanie"
  ]
  node [
    id 1008
    label "powiedzenie"
  ]
  node [
    id 1009
    label "rozegranie"
  ]
  node [
    id 1010
    label "mini&#281;cie"
  ]
  node [
    id 1011
    label "przepuszczenie"
  ]
  node [
    id 1012
    label "transgresja"
  ]
  node [
    id 1013
    label "offense"
  ]
  node [
    id 1014
    label "kom&#243;rka"
  ]
  node [
    id 1015
    label "furnishing"
  ]
  node [
    id 1016
    label "zabezpieczenie"
  ]
  node [
    id 1017
    label "wyrz&#261;dzenie"
  ]
  node [
    id 1018
    label "zagospodarowanie"
  ]
  node [
    id 1019
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 1020
    label "ig&#322;a"
  ]
  node [
    id 1021
    label "wirnik"
  ]
  node [
    id 1022
    label "aparatura"
  ]
  node [
    id 1023
    label "system_energetyczny"
  ]
  node [
    id 1024
    label "impulsator"
  ]
  node [
    id 1025
    label "mechanizm"
  ]
  node [
    id 1026
    label "sprz&#281;t"
  ]
  node [
    id 1027
    label "blokowanie"
  ]
  node [
    id 1028
    label "set"
  ]
  node [
    id 1029
    label "zablokowanie"
  ]
  node [
    id 1030
    label "przygotowanie"
  ]
  node [
    id 1031
    label "komora"
  ]
  node [
    id 1032
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 1033
    label "wsi&#261;kni&#281;cie"
  ]
  node [
    id 1034
    label "infiltration"
  ]
  node [
    id 1035
    label "penetration"
  ]
  node [
    id 1036
    label "przestrze&#324;"
  ]
  node [
    id 1037
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 1038
    label "wybicie"
  ]
  node [
    id 1039
    label "wyd&#322;ubanie"
  ]
  node [
    id 1040
    label "przerwa"
  ]
  node [
    id 1041
    label "powybijanie"
  ]
  node [
    id 1042
    label "wybijanie"
  ]
  node [
    id 1043
    label "wiercenie"
  ]
  node [
    id 1044
    label "acquaintance"
  ]
  node [
    id 1045
    label "nauczenie_si&#281;"
  ]
  node [
    id 1046
    label "poczucie"
  ]
  node [
    id 1047
    label "knowing"
  ]
  node [
    id 1048
    label "wy&#347;wiadczenie"
  ]
  node [
    id 1049
    label "inclusion"
  ]
  node [
    id 1050
    label "zrozumienie"
  ]
  node [
    id 1051
    label "designation"
  ]
  node [
    id 1052
    label "umo&#380;liwienie"
  ]
  node [
    id 1053
    label "sensing"
  ]
  node [
    id 1054
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 1055
    label "forma"
  ]
  node [
    id 1056
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 1057
    label "insekt"
  ]
  node [
    id 1058
    label "puszczenie"
  ]
  node [
    id 1059
    label "entree"
  ]
  node [
    id 1060
    label "wprowadzenie"
  ]
  node [
    id 1061
    label "dmuchni&#281;cie"
  ]
  node [
    id 1062
    label "niesienie"
  ]
  node [
    id 1063
    label "wch&#322;oni&#281;cie"
  ]
  node [
    id 1064
    label "nakazanie"
  ]
  node [
    id 1065
    label "pokonanie"
  ]
  node [
    id 1066
    label "take"
  ]
  node [
    id 1067
    label "wywiezienie"
  ]
  node [
    id 1068
    label "zaopatrzenie_si&#281;"
  ]
  node [
    id 1069
    label "wymienienie_si&#281;"
  ]
  node [
    id 1070
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 1071
    label "uciekni&#281;cie"
  ]
  node [
    id 1072
    label "pobranie"
  ]
  node [
    id 1073
    label "poczytanie"
  ]
  node [
    id 1074
    label "si&#281;gni&#281;cie"
  ]
  node [
    id 1075
    label "u&#380;ycie"
  ]
  node [
    id 1076
    label "powodzenie"
  ]
  node [
    id 1077
    label "pickings"
  ]
  node [
    id 1078
    label "zniesienie"
  ]
  node [
    id 1079
    label "kupienie"
  ]
  node [
    id 1080
    label "bite"
  ]
  node [
    id 1081
    label "dostanie"
  ]
  node [
    id 1082
    label "wyruchanie"
  ]
  node [
    id 1083
    label "odziedziczenie"
  ]
  node [
    id 1084
    label "capture"
  ]
  node [
    id 1085
    label "otrzymanie"
  ]
  node [
    id 1086
    label "branie"
  ]
  node [
    id 1087
    label "wygranie"
  ]
  node [
    id 1088
    label "wzi&#261;&#263;"
  ]
  node [
    id 1089
    label "obj&#281;cie"
  ]
  node [
    id 1090
    label "w&#322;o&#380;enie"
  ]
  node [
    id 1091
    label "udanie_si&#281;"
  ]
  node [
    id 1092
    label "wych&#281;do&#380;enie"
  ]
  node [
    id 1093
    label "podmiot"
  ]
  node [
    id 1094
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 1095
    label "ptaszek"
  ]
  node [
    id 1096
    label "organizacja"
  ]
  node [
    id 1097
    label "element_anatomiczny"
  ]
  node [
    id 1098
    label "przyrodzenie"
  ]
  node [
    id 1099
    label "fiut"
  ]
  node [
    id 1100
    label "shaft"
  ]
  node [
    id 1101
    label "wchodzenie"
  ]
  node [
    id 1102
    label "przedstawiciel"
  ]
  node [
    id 1103
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 1104
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 1105
    label "nas&#261;czenie"
  ]
  node [
    id 1106
    label "strain"
  ]
  node [
    id 1107
    label "przedostanie_si&#281;"
  ]
  node [
    id 1108
    label "control"
  ]
  node [
    id 1109
    label "nasycenie_si&#281;"
  ]
  node [
    id 1110
    label "permeation"
  ]
  node [
    id 1111
    label "nasilenie_si&#281;"
  ]
  node [
    id 1112
    label "przemokni&#281;cie"
  ]
  node [
    id 1113
    label "przepojenie"
  ]
  node [
    id 1114
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 1115
    label "obstawianie"
  ]
  node [
    id 1116
    label "trafienie"
  ]
  node [
    id 1117
    label "obstawienie"
  ]
  node [
    id 1118
    label "przeszkoda"
  ]
  node [
    id 1119
    label "zawiasy"
  ]
  node [
    id 1120
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 1121
    label "s&#322;upek"
  ]
  node [
    id 1122
    label "boisko"
  ]
  node [
    id 1123
    label "siatka"
  ]
  node [
    id 1124
    label "obstawia&#263;"
  ]
  node [
    id 1125
    label "ogrodzenie"
  ]
  node [
    id 1126
    label "zamek"
  ]
  node [
    id 1127
    label "goal"
  ]
  node [
    id 1128
    label "poprzeczka"
  ]
  node [
    id 1129
    label "p&#322;ot"
  ]
  node [
    id 1130
    label "obstawi&#263;"
  ]
  node [
    id 1131
    label "brama"
  ]
  node [
    id 1132
    label "plac"
  ]
  node [
    id 1133
    label "przechowalnia"
  ]
  node [
    id 1134
    label "podjazd"
  ]
  node [
    id 1135
    label "ogr&#243;d"
  ]
  node [
    id 1136
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1137
    label "substancja_mieszkaniowa"
  ]
  node [
    id 1138
    label "instytucja"
  ]
  node [
    id 1139
    label "siedziba"
  ]
  node [
    id 1140
    label "budynek"
  ]
  node [
    id 1141
    label "poj&#281;cie"
  ]
  node [
    id 1142
    label "stead"
  ]
  node [
    id 1143
    label "garderoba"
  ]
  node [
    id 1144
    label "wiecha"
  ]
  node [
    id 1145
    label "fratria"
  ]
  node [
    id 1146
    label "raj_utracony"
  ]
  node [
    id 1147
    label "umieranie"
  ]
  node [
    id 1148
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 1149
    label "prze&#380;ywanie"
  ]
  node [
    id 1150
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1151
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1152
    label "umarcie"
  ]
  node [
    id 1153
    label "subsistence"
  ]
  node [
    id 1154
    label "power"
  ]
  node [
    id 1155
    label "okres_noworodkowy"
  ]
  node [
    id 1156
    label "prze&#380;ycie"
  ]
  node [
    id 1157
    label "wiek_matuzalemowy"
  ]
  node [
    id 1158
    label "entity"
  ]
  node [
    id 1159
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1160
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 1161
    label "do&#380;ywanie"
  ]
  node [
    id 1162
    label "byt"
  ]
  node [
    id 1163
    label "dzieci&#324;stwo"
  ]
  node [
    id 1164
    label "andropauza"
  ]
  node [
    id 1165
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1166
    label "rozw&#243;j"
  ]
  node [
    id 1167
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 1168
    label "menopauza"
  ]
  node [
    id 1169
    label "&#347;mier&#263;"
  ]
  node [
    id 1170
    label "koleje_losu"
  ]
  node [
    id 1171
    label "zegar_biologiczny"
  ]
  node [
    id 1172
    label "szwung"
  ]
  node [
    id 1173
    label "warunki"
  ]
  node [
    id 1174
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1175
    label "niemowl&#281;ctwo"
  ]
  node [
    id 1176
    label "&#380;ywy"
  ]
  node [
    id 1177
    label "life"
  ]
  node [
    id 1178
    label "staro&#347;&#263;"
  ]
  node [
    id 1179
    label "energy"
  ]
  node [
    id 1180
    label "trwanie"
  ]
  node [
    id 1181
    label "wra&#380;enie"
  ]
  node [
    id 1182
    label "przej&#347;cie"
  ]
  node [
    id 1183
    label "poradzenie_sobie"
  ]
  node [
    id 1184
    label "przetrwanie"
  ]
  node [
    id 1185
    label "survival"
  ]
  node [
    id 1186
    label "przechodzenie"
  ]
  node [
    id 1187
    label "wytrzymywanie"
  ]
  node [
    id 1188
    label "zaznawanie"
  ]
  node [
    id 1189
    label "obejrzenie"
  ]
  node [
    id 1190
    label "widzenie"
  ]
  node [
    id 1191
    label "urzeczywistnianie"
  ]
  node [
    id 1192
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1193
    label "przeszkodzenie"
  ]
  node [
    id 1194
    label "produkowanie"
  ]
  node [
    id 1195
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 1196
    label "przeszkadzanie"
  ]
  node [
    id 1197
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1198
    label "wyprodukowanie"
  ]
  node [
    id 1199
    label "utrzymywanie"
  ]
  node [
    id 1200
    label "subsystencja"
  ]
  node [
    id 1201
    label "utrzyma&#263;"
  ]
  node [
    id 1202
    label "egzystencja"
  ]
  node [
    id 1203
    label "wy&#380;ywienie"
  ]
  node [
    id 1204
    label "ontologicznie"
  ]
  node [
    id 1205
    label "utrzymanie"
  ]
  node [
    id 1206
    label "potencja"
  ]
  node [
    id 1207
    label "utrzymywa&#263;"
  ]
  node [
    id 1208
    label "status"
  ]
  node [
    id 1209
    label "sytuacja"
  ]
  node [
    id 1210
    label "ocieranie_si&#281;"
  ]
  node [
    id 1211
    label "otoczenie_si&#281;"
  ]
  node [
    id 1212
    label "posiedzenie"
  ]
  node [
    id 1213
    label "otarcie_si&#281;"
  ]
  node [
    id 1214
    label "atakowanie"
  ]
  node [
    id 1215
    label "otaczanie_si&#281;"
  ]
  node [
    id 1216
    label "zmierzanie"
  ]
  node [
    id 1217
    label "residency"
  ]
  node [
    id 1218
    label "sojourn"
  ]
  node [
    id 1219
    label "tkwienie"
  ]
  node [
    id 1220
    label "absolutorium"
  ]
  node [
    id 1221
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1222
    label "activity"
  ]
  node [
    id 1223
    label "ton"
  ]
  node [
    id 1224
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 1225
    label "korkowanie"
  ]
  node [
    id 1226
    label "death"
  ]
  node [
    id 1227
    label "zabijanie"
  ]
  node [
    id 1228
    label "martwy"
  ]
  node [
    id 1229
    label "przestawanie"
  ]
  node [
    id 1230
    label "odumieranie"
  ]
  node [
    id 1231
    label "zdychanie"
  ]
  node [
    id 1232
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 1233
    label "zanikanie"
  ]
  node [
    id 1234
    label "ko&#324;czenie"
  ]
  node [
    id 1235
    label "nieuleczalnie_chory"
  ]
  node [
    id 1236
    label "ciekawy"
  ]
  node [
    id 1237
    label "&#380;ywotny"
  ]
  node [
    id 1238
    label "naturalny"
  ]
  node [
    id 1239
    label "&#380;ywo"
  ]
  node [
    id 1240
    label "o&#380;ywianie"
  ]
  node [
    id 1241
    label "silny"
  ]
  node [
    id 1242
    label "g&#322;&#281;boki"
  ]
  node [
    id 1243
    label "wyra&#378;ny"
  ]
  node [
    id 1244
    label "czynny"
  ]
  node [
    id 1245
    label "zgrabny"
  ]
  node [
    id 1246
    label "prawdziwy"
  ]
  node [
    id 1247
    label "realistyczny"
  ]
  node [
    id 1248
    label "energiczny"
  ]
  node [
    id 1249
    label "odumarcie"
  ]
  node [
    id 1250
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1251
    label "pomarcie"
  ]
  node [
    id 1252
    label "die"
  ]
  node [
    id 1253
    label "sko&#324;czenie"
  ]
  node [
    id 1254
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 1255
    label "zdechni&#281;cie"
  ]
  node [
    id 1256
    label "zabicie"
  ]
  node [
    id 1257
    label "procedura"
  ]
  node [
    id 1258
    label "proces_biologiczny"
  ]
  node [
    id 1259
    label "z&#322;ote_czasy"
  ]
  node [
    id 1260
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 1261
    label "process"
  ]
  node [
    id 1262
    label "cycle"
  ]
  node [
    id 1263
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 1264
    label "adolescence"
  ]
  node [
    id 1265
    label "wiek"
  ]
  node [
    id 1266
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 1267
    label "zielone_lata"
  ]
  node [
    id 1268
    label "defenestracja"
  ]
  node [
    id 1269
    label "agonia"
  ]
  node [
    id 1270
    label "mogi&#322;a"
  ]
  node [
    id 1271
    label "kres_&#380;ycia"
  ]
  node [
    id 1272
    label "upadek"
  ]
  node [
    id 1273
    label "szeol"
  ]
  node [
    id 1274
    label "pogrzebanie"
  ]
  node [
    id 1275
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1276
    label "&#380;a&#322;oba"
  ]
  node [
    id 1277
    label "pogrzeb"
  ]
  node [
    id 1278
    label "majority"
  ]
  node [
    id 1279
    label "osiemnastoletni"
  ]
  node [
    id 1280
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1281
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1282
    label "age"
  ]
  node [
    id 1283
    label "kobieta"
  ]
  node [
    id 1284
    label "przekwitanie"
  ]
  node [
    id 1285
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 1286
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1287
    label "energia"
  ]
  node [
    id 1288
    label "zapa&#322;"
  ]
  node [
    id 1289
    label "wnioskowa&#263;"
  ]
  node [
    id 1290
    label "report"
  ]
  node [
    id 1291
    label "zach&#281;ca&#263;"
  ]
  node [
    id 1292
    label "volunteer"
  ]
  node [
    id 1293
    label "suggest"
  ]
  node [
    id 1294
    label "informowa&#263;"
  ]
  node [
    id 1295
    label "kandydatura"
  ]
  node [
    id 1296
    label "prosi&#263;"
  ]
  node [
    id 1297
    label "dochodzi&#263;"
  ]
  node [
    id 1298
    label "argue"
  ]
  node [
    id 1299
    label "raise"
  ]
  node [
    id 1300
    label "powiada&#263;"
  ]
  node [
    id 1301
    label "komunikowa&#263;"
  ]
  node [
    id 1302
    label "inform"
  ]
  node [
    id 1303
    label "pozyskiwa&#263;"
  ]
  node [
    id 1304
    label "zaproponowa&#263;"
  ]
  node [
    id 1305
    label "proponowanie"
  ]
  node [
    id 1306
    label "campaigning"
  ]
  node [
    id 1307
    label "wniosek"
  ]
  node [
    id 1308
    label "rewizja"
  ]
  node [
    id 1309
    label "oznaka"
  ]
  node [
    id 1310
    label "change"
  ]
  node [
    id 1311
    label "ferment"
  ]
  node [
    id 1312
    label "komplet"
  ]
  node [
    id 1313
    label "anatomopatolog"
  ]
  node [
    id 1314
    label "zmianka"
  ]
  node [
    id 1315
    label "amendment"
  ]
  node [
    id 1316
    label "odmienianie"
  ]
  node [
    id 1317
    label "tura"
  ]
  node [
    id 1318
    label "boski"
  ]
  node [
    id 1319
    label "krajobraz"
  ]
  node [
    id 1320
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1321
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1322
    label "przywidzenie"
  ]
  node [
    id 1323
    label "presence"
  ]
  node [
    id 1324
    label "charakter"
  ]
  node [
    id 1325
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1326
    label "lekcja"
  ]
  node [
    id 1327
    label "ensemble"
  ]
  node [
    id 1328
    label "zestaw"
  ]
  node [
    id 1329
    label "implikowa&#263;"
  ]
  node [
    id 1330
    label "signal"
  ]
  node [
    id 1331
    label "fakt"
  ]
  node [
    id 1332
    label "proces_my&#347;lowy"
  ]
  node [
    id 1333
    label "dow&#243;d"
  ]
  node [
    id 1334
    label "krytyka"
  ]
  node [
    id 1335
    label "rekurs"
  ]
  node [
    id 1336
    label "checkup"
  ]
  node [
    id 1337
    label "kontrola"
  ]
  node [
    id 1338
    label "odwo&#322;anie"
  ]
  node [
    id 1339
    label "correction"
  ]
  node [
    id 1340
    label "przegl&#261;d"
  ]
  node [
    id 1341
    label "kipisz"
  ]
  node [
    id 1342
    label "korekta"
  ]
  node [
    id 1343
    label "bia&#322;ko"
  ]
  node [
    id 1344
    label "immobilizowa&#263;"
  ]
  node [
    id 1345
    label "poruszenie"
  ]
  node [
    id 1346
    label "immobilizacja"
  ]
  node [
    id 1347
    label "apoenzym"
  ]
  node [
    id 1348
    label "zymaza"
  ]
  node [
    id 1349
    label "enzyme"
  ]
  node [
    id 1350
    label "immobilizowanie"
  ]
  node [
    id 1351
    label "biokatalizator"
  ]
  node [
    id 1352
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 1353
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 1354
    label "najem"
  ]
  node [
    id 1355
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 1356
    label "zak&#322;ad"
  ]
  node [
    id 1357
    label "stosunek_pracy"
  ]
  node [
    id 1358
    label "benedykty&#324;ski"
  ]
  node [
    id 1359
    label "poda&#380;_pracy"
  ]
  node [
    id 1360
    label "pracowanie"
  ]
  node [
    id 1361
    label "tyrka"
  ]
  node [
    id 1362
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 1363
    label "zaw&#243;d"
  ]
  node [
    id 1364
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1365
    label "tynkarski"
  ]
  node [
    id 1366
    label "pracowa&#263;"
  ]
  node [
    id 1367
    label "czynnik_produkcji"
  ]
  node [
    id 1368
    label "zobowi&#261;zanie"
  ]
  node [
    id 1369
    label "kierownictwo"
  ]
  node [
    id 1370
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 1371
    label "patolog"
  ]
  node [
    id 1372
    label "anatom"
  ]
  node [
    id 1373
    label "zamiana"
  ]
  node [
    id 1374
    label "wymienianie"
  ]
  node [
    id 1375
    label "Transfiguration"
  ]
  node [
    id 1376
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 1377
    label "ci&#261;gle"
  ]
  node [
    id 1378
    label "stale"
  ]
  node [
    id 1379
    label "ci&#261;g&#322;y"
  ]
  node [
    id 1380
    label "nieprzerwanie"
  ]
  node [
    id 1381
    label "przyjazny"
  ]
  node [
    id 1382
    label "pa&#324;stwo"
  ]
  node [
    id 1383
    label "komisja"
  ]
  node [
    id 1384
    label "finanse"
  ]
  node [
    id 1385
    label "publiczny"
  ]
  node [
    id 1386
    label "Jerzy"
  ]
  node [
    id 1387
    label "Szmajdzi&#324;ski"
  ]
  node [
    id 1388
    label "Jan"
  ]
  node [
    id 1389
    label "&#322;opata"
  ]
  node [
    id 1390
    label "polskie"
  ]
  node [
    id 1391
    label "stronnictwo"
  ]
  node [
    id 1392
    label "ludowy"
  ]
  node [
    id 1393
    label "podatkowy"
  ]
  node [
    id 1394
    label "narodowy"
  ]
  node [
    id 1395
    label "bank"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 263
  ]
  edge [
    source 10
    target 264
  ]
  edge [
    source 10
    target 266
  ]
  edge [
    source 10
    target 265
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 243
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 256
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 254
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 40
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 231
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 255
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 258
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 320
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 442
  ]
  edge [
    source 18
    target 325
  ]
  edge [
    source 18
    target 326
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 906
  ]
  edge [
    source 19
    target 907
  ]
  edge [
    source 19
    target 702
  ]
  edge [
    source 19
    target 908
  ]
  edge [
    source 19
    target 909
  ]
  edge [
    source 19
    target 910
  ]
  edge [
    source 19
    target 911
  ]
  edge [
    source 19
    target 912
  ]
  edge [
    source 19
    target 913
  ]
  edge [
    source 19
    target 914
  ]
  edge [
    source 19
    target 915
  ]
  edge [
    source 19
    target 916
  ]
  edge [
    source 19
    target 917
  ]
  edge [
    source 19
    target 918
  ]
  edge [
    source 19
    target 919
  ]
  edge [
    source 19
    target 715
  ]
  edge [
    source 19
    target 920
  ]
  edge [
    source 19
    target 921
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 922
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 923
  ]
  edge [
    source 19
    target 924
  ]
  edge [
    source 19
    target 925
  ]
  edge [
    source 19
    target 926
  ]
  edge [
    source 19
    target 722
  ]
  edge [
    source 19
    target 593
  ]
  edge [
    source 19
    target 927
  ]
  edge [
    source 19
    target 928
  ]
  edge [
    source 19
    target 929
  ]
  edge [
    source 19
    target 930
  ]
  edge [
    source 19
    target 931
  ]
  edge [
    source 19
    target 932
  ]
  edge [
    source 19
    target 933
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 672
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 685
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 19
    target 943
  ]
  edge [
    source 19
    target 242
  ]
  edge [
    source 19
    target 944
  ]
  edge [
    source 19
    target 699
  ]
  edge [
    source 19
    target 945
  ]
  edge [
    source 19
    target 946
  ]
  edge [
    source 19
    target 947
  ]
  edge [
    source 19
    target 948
  ]
  edge [
    source 19
    target 949
  ]
  edge [
    source 19
    target 950
  ]
  edge [
    source 19
    target 951
  ]
  edge [
    source 19
    target 952
  ]
  edge [
    source 19
    target 953
  ]
  edge [
    source 19
    target 954
  ]
  edge [
    source 19
    target 341
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 620
  ]
  edge [
    source 19
    target 955
  ]
  edge [
    source 19
    target 635
  ]
  edge [
    source 19
    target 668
  ]
  edge [
    source 19
    target 956
  ]
  edge [
    source 19
    target 957
  ]
  edge [
    source 19
    target 958
  ]
  edge [
    source 19
    target 959
  ]
  edge [
    source 19
    target 960
  ]
  edge [
    source 19
    target 961
  ]
  edge [
    source 19
    target 962
  ]
  edge [
    source 19
    target 963
  ]
  edge [
    source 19
    target 964
  ]
  edge [
    source 19
    target 965
  ]
  edge [
    source 19
    target 966
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 889
  ]
  edge [
    source 19
    target 967
  ]
  edge [
    source 19
    target 968
  ]
  edge [
    source 19
    target 969
  ]
  edge [
    source 19
    target 970
  ]
  edge [
    source 19
    target 971
  ]
  edge [
    source 19
    target 972
  ]
  edge [
    source 19
    target 973
  ]
  edge [
    source 19
    target 974
  ]
  edge [
    source 19
    target 975
  ]
  edge [
    source 19
    target 976
  ]
  edge [
    source 19
    target 977
  ]
  edge [
    source 19
    target 978
  ]
  edge [
    source 19
    target 979
  ]
  edge [
    source 19
    target 980
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 982
  ]
  edge [
    source 19
    target 983
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 46
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 255
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 698
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 730
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 382
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 687
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 416
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 19
    target 1034
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1036
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 633
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 634
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1053
  ]
  edge [
    source 19
    target 1054
  ]
  edge [
    source 19
    target 631
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1059
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 1061
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1063
  ]
  edge [
    source 19
    target 1064
  ]
  edge [
    source 19
    target 700
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 662
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 858
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 19
    target 342
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 352
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 587
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 1110
  ]
  edge [
    source 19
    target 1111
  ]
  edge [
    source 19
    target 1112
  ]
  edge [
    source 19
    target 1113
  ]
  edge [
    source 19
    target 1114
  ]
  edge [
    source 19
    target 1115
  ]
  edge [
    source 19
    target 1116
  ]
  edge [
    source 19
    target 1117
  ]
  edge [
    source 19
    target 1118
  ]
  edge [
    source 19
    target 1119
  ]
  edge [
    source 19
    target 1120
  ]
  edge [
    source 19
    target 1121
  ]
  edge [
    source 19
    target 1122
  ]
  edge [
    source 19
    target 1123
  ]
  edge [
    source 19
    target 1124
  ]
  edge [
    source 19
    target 1125
  ]
  edge [
    source 19
    target 1126
  ]
  edge [
    source 19
    target 1127
  ]
  edge [
    source 19
    target 1128
  ]
  edge [
    source 19
    target 1129
  ]
  edge [
    source 19
    target 1130
  ]
  edge [
    source 19
    target 1131
  ]
  edge [
    source 19
    target 1132
  ]
  edge [
    source 19
    target 1133
  ]
  edge [
    source 19
    target 1134
  ]
  edge [
    source 19
    target 1135
  ]
  edge [
    source 19
    target 1136
  ]
  edge [
    source 19
    target 564
  ]
  edge [
    source 19
    target 1137
  ]
  edge [
    source 19
    target 1138
  ]
  edge [
    source 19
    target 1139
  ]
  edge [
    source 19
    target 588
  ]
  edge [
    source 19
    target 1140
  ]
  edge [
    source 19
    target 589
  ]
  edge [
    source 19
    target 1141
  ]
  edge [
    source 19
    target 1142
  ]
  edge [
    source 19
    target 1143
  ]
  edge [
    source 19
    target 1144
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1146
  ]
  edge [
    source 20
    target 1147
  ]
  edge [
    source 20
    target 1148
  ]
  edge [
    source 20
    target 1149
  ]
  edge [
    source 20
    target 1150
  ]
  edge [
    source 20
    target 1151
  ]
  edge [
    source 20
    target 642
  ]
  edge [
    source 20
    target 1152
  ]
  edge [
    source 20
    target 387
  ]
  edge [
    source 20
    target 1153
  ]
  edge [
    source 20
    target 1154
  ]
  edge [
    source 20
    target 1155
  ]
  edge [
    source 20
    target 1156
  ]
  edge [
    source 20
    target 1157
  ]
  edge [
    source 20
    target 656
  ]
  edge [
    source 20
    target 1158
  ]
  edge [
    source 20
    target 1159
  ]
  edge [
    source 20
    target 1160
  ]
  edge [
    source 20
    target 1161
  ]
  edge [
    source 20
    target 1162
  ]
  edge [
    source 20
    target 1163
  ]
  edge [
    source 20
    target 1164
  ]
  edge [
    source 20
    target 1165
  ]
  edge [
    source 20
    target 1166
  ]
  edge [
    source 20
    target 1167
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 1168
  ]
  edge [
    source 20
    target 1169
  ]
  edge [
    source 20
    target 1170
  ]
  edge [
    source 20
    target 618
  ]
  edge [
    source 20
    target 1171
  ]
  edge [
    source 20
    target 1172
  ]
  edge [
    source 20
    target 725
  ]
  edge [
    source 20
    target 1173
  ]
  edge [
    source 20
    target 1174
  ]
  edge [
    source 20
    target 1175
  ]
  edge [
    source 20
    target 1176
  ]
  edge [
    source 20
    target 1177
  ]
  edge [
    source 20
    target 1178
  ]
  edge [
    source 20
    target 1179
  ]
  edge [
    source 20
    target 1180
  ]
  edge [
    source 20
    target 1181
  ]
  edge [
    source 20
    target 1182
  ]
  edge [
    source 20
    target 953
  ]
  edge [
    source 20
    target 1183
  ]
  edge [
    source 20
    target 1184
  ]
  edge [
    source 20
    target 1185
  ]
  edge [
    source 20
    target 1186
  ]
  edge [
    source 20
    target 1187
  ]
  edge [
    source 20
    target 1188
  ]
  edge [
    source 20
    target 1189
  ]
  edge [
    source 20
    target 1190
  ]
  edge [
    source 20
    target 1191
  ]
  edge [
    source 20
    target 1192
  ]
  edge [
    source 20
    target 1193
  ]
  edge [
    source 20
    target 1194
  ]
  edge [
    source 20
    target 497
  ]
  edge [
    source 20
    target 667
  ]
  edge [
    source 20
    target 810
  ]
  edge [
    source 20
    target 1195
  ]
  edge [
    source 20
    target 1196
  ]
  edge [
    source 20
    target 1197
  ]
  edge [
    source 20
    target 1198
  ]
  edge [
    source 20
    target 1199
  ]
  edge [
    source 20
    target 1200
  ]
  edge [
    source 20
    target 1201
  ]
  edge [
    source 20
    target 1202
  ]
  edge [
    source 20
    target 1203
  ]
  edge [
    source 20
    target 1204
  ]
  edge [
    source 20
    target 1205
  ]
  edge [
    source 20
    target 331
  ]
  edge [
    source 20
    target 1206
  ]
  edge [
    source 20
    target 1207
  ]
  edge [
    source 20
    target 1208
  ]
  edge [
    source 20
    target 1209
  ]
  edge [
    source 20
    target 110
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 20
    target 130
  ]
  edge [
    source 20
    target 131
  ]
  edge [
    source 20
    target 132
  ]
  edge [
    source 20
    target 133
  ]
  edge [
    source 20
    target 134
  ]
  edge [
    source 20
    target 135
  ]
  edge [
    source 20
    target 136
  ]
  edge [
    source 20
    target 137
  ]
  edge [
    source 20
    target 138
  ]
  edge [
    source 20
    target 139
  ]
  edge [
    source 20
    target 140
  ]
  edge [
    source 20
    target 141
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 143
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 145
  ]
  edge [
    source 20
    target 146
  ]
  edge [
    source 20
    target 1210
  ]
  edge [
    source 20
    target 1211
  ]
  edge [
    source 20
    target 1212
  ]
  edge [
    source 20
    target 1213
  ]
  edge [
    source 20
    target 1214
  ]
  edge [
    source 20
    target 1215
  ]
  edge [
    source 20
    target 648
  ]
  edge [
    source 20
    target 1216
  ]
  edge [
    source 20
    target 1217
  ]
  edge [
    source 20
    target 1218
  ]
  edge [
    source 20
    target 708
  ]
  edge [
    source 20
    target 1219
  ]
  edge [
    source 20
    target 713
  ]
  edge [
    source 20
    target 1220
  ]
  edge [
    source 20
    target 1221
  ]
  edge [
    source 20
    target 732
  ]
  edge [
    source 20
    target 1222
  ]
  edge [
    source 20
    target 1223
  ]
  edge [
    source 20
    target 1224
  ]
  edge [
    source 20
    target 55
  ]
  edge [
    source 20
    target 1225
  ]
  edge [
    source 20
    target 1226
  ]
  edge [
    source 20
    target 1227
  ]
  edge [
    source 20
    target 1228
  ]
  edge [
    source 20
    target 1229
  ]
  edge [
    source 20
    target 1230
  ]
  edge [
    source 20
    target 1231
  ]
  edge [
    source 20
    target 250
  ]
  edge [
    source 20
    target 1232
  ]
  edge [
    source 20
    target 1233
  ]
  edge [
    source 20
    target 1234
  ]
  edge [
    source 20
    target 1235
  ]
  edge [
    source 20
    target 1236
  ]
  edge [
    source 20
    target 359
  ]
  edge [
    source 20
    target 1237
  ]
  edge [
    source 20
    target 1238
  ]
  edge [
    source 20
    target 1239
  ]
  edge [
    source 20
    target 50
  ]
  edge [
    source 20
    target 1240
  ]
  edge [
    source 20
    target 1241
  ]
  edge [
    source 20
    target 1242
  ]
  edge [
    source 20
    target 1243
  ]
  edge [
    source 20
    target 1244
  ]
  edge [
    source 20
    target 625
  ]
  edge [
    source 20
    target 1245
  ]
  edge [
    source 20
    target 1246
  ]
  edge [
    source 20
    target 1247
  ]
  edge [
    source 20
    target 1248
  ]
  edge [
    source 20
    target 1249
  ]
  edge [
    source 20
    target 658
  ]
  edge [
    source 20
    target 1250
  ]
  edge [
    source 20
    target 1251
  ]
  edge [
    source 20
    target 1252
  ]
  edge [
    source 20
    target 1253
  ]
  edge [
    source 20
    target 1254
  ]
  edge [
    source 20
    target 1255
  ]
  edge [
    source 20
    target 1256
  ]
  edge [
    source 20
    target 1257
  ]
  edge [
    source 20
    target 37
  ]
  edge [
    source 20
    target 1258
  ]
  edge [
    source 20
    target 1259
  ]
  edge [
    source 20
    target 1260
  ]
  edge [
    source 20
    target 1261
  ]
  edge [
    source 20
    target 1262
  ]
  edge [
    source 20
    target 1263
  ]
  edge [
    source 20
    target 1264
  ]
  edge [
    source 20
    target 1265
  ]
  edge [
    source 20
    target 1266
  ]
  edge [
    source 20
    target 1267
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 1268
  ]
  edge [
    source 20
    target 1269
  ]
  edge [
    source 20
    target 723
  ]
  edge [
    source 20
    target 1270
  ]
  edge [
    source 20
    target 1271
  ]
  edge [
    source 20
    target 1272
  ]
  edge [
    source 20
    target 1273
  ]
  edge [
    source 20
    target 1274
  ]
  edge [
    source 20
    target 1275
  ]
  edge [
    source 20
    target 1276
  ]
  edge [
    source 20
    target 1277
  ]
  edge [
    source 20
    target 1278
  ]
  edge [
    source 20
    target 1279
  ]
  edge [
    source 20
    target 1280
  ]
  edge [
    source 20
    target 1281
  ]
  edge [
    source 20
    target 1282
  ]
  edge [
    source 20
    target 1283
  ]
  edge [
    source 20
    target 1284
  ]
  edge [
    source 20
    target 1285
  ]
  edge [
    source 20
    target 1286
  ]
  edge [
    source 20
    target 1287
  ]
  edge [
    source 20
    target 1288
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1289
  ]
  edge [
    source 21
    target 1290
  ]
  edge [
    source 21
    target 1291
  ]
  edge [
    source 21
    target 1292
  ]
  edge [
    source 21
    target 1293
  ]
  edge [
    source 21
    target 1294
  ]
  edge [
    source 21
    target 1295
  ]
  edge [
    source 21
    target 467
  ]
  edge [
    source 21
    target 1296
  ]
  edge [
    source 21
    target 1297
  ]
  edge [
    source 21
    target 1298
  ]
  edge [
    source 21
    target 1299
  ]
  edge [
    source 21
    target 1300
  ]
  edge [
    source 21
    target 1301
  ]
  edge [
    source 21
    target 1302
  ]
  edge [
    source 21
    target 1303
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 464
  ]
  edge [
    source 21
    target 1304
  ]
  edge [
    source 21
    target 1305
  ]
  edge [
    source 21
    target 1306
  ]
  edge [
    source 21
    target 1307
  ]
  edge [
    source 21
    target 278
  ]
  edge [
    source 21
    target 327
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 1309
  ]
  edge [
    source 22
    target 1310
  ]
  edge [
    source 22
    target 1311
  ]
  edge [
    source 22
    target 1312
  ]
  edge [
    source 22
    target 1313
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 40
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 795
  ]
  edge [
    source 22
    target 1316
  ]
  edge [
    source 22
    target 1317
  ]
  edge [
    source 22
    target 37
  ]
  edge [
    source 22
    target 1318
  ]
  edge [
    source 22
    target 1319
  ]
  edge [
    source 22
    target 1320
  ]
  edge [
    source 22
    target 1321
  ]
  edge [
    source 22
    target 1322
  ]
  edge [
    source 22
    target 1323
  ]
  edge [
    source 22
    target 1324
  ]
  edge [
    source 22
    target 1325
  ]
  edge [
    source 22
    target 1326
  ]
  edge [
    source 22
    target 1327
  ]
  edge [
    source 22
    target 587
  ]
  edge [
    source 22
    target 197
  ]
  edge [
    source 22
    target 1328
  ]
  edge [
    source 22
    target 110
  ]
  edge [
    source 22
    target 111
  ]
  edge [
    source 22
    target 112
  ]
  edge [
    source 22
    target 113
  ]
  edge [
    source 22
    target 114
  ]
  edge [
    source 22
    target 115
  ]
  edge [
    source 22
    target 116
  ]
  edge [
    source 22
    target 117
  ]
  edge [
    source 22
    target 118
  ]
  edge [
    source 22
    target 119
  ]
  edge [
    source 22
    target 120
  ]
  edge [
    source 22
    target 121
  ]
  edge [
    source 22
    target 122
  ]
  edge [
    source 22
    target 123
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 22
    target 125
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 22
    target 127
  ]
  edge [
    source 22
    target 128
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 22
    target 135
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 22
    target 139
  ]
  edge [
    source 22
    target 140
  ]
  edge [
    source 22
    target 141
  ]
  edge [
    source 22
    target 142
  ]
  edge [
    source 22
    target 143
  ]
  edge [
    source 22
    target 144
  ]
  edge [
    source 22
    target 145
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 41
  ]
  edge [
    source 22
    target 1329
  ]
  edge [
    source 22
    target 1330
  ]
  edge [
    source 22
    target 1331
  ]
  edge [
    source 22
    target 449
  ]
  edge [
    source 22
    target 1332
  ]
  edge [
    source 22
    target 1333
  ]
  edge [
    source 22
    target 1334
  ]
  edge [
    source 22
    target 1335
  ]
  edge [
    source 22
    target 1336
  ]
  edge [
    source 22
    target 1337
  ]
  edge [
    source 22
    target 1338
  ]
  edge [
    source 22
    target 1339
  ]
  edge [
    source 22
    target 1340
  ]
  edge [
    source 22
    target 1341
  ]
  edge [
    source 22
    target 1342
  ]
  edge [
    source 22
    target 1343
  ]
  edge [
    source 22
    target 1344
  ]
  edge [
    source 22
    target 1345
  ]
  edge [
    source 22
    target 1346
  ]
  edge [
    source 22
    target 1347
  ]
  edge [
    source 22
    target 1348
  ]
  edge [
    source 22
    target 1349
  ]
  edge [
    source 22
    target 1350
  ]
  edge [
    source 22
    target 1351
  ]
  edge [
    source 22
    target 1352
  ]
  edge [
    source 22
    target 1353
  ]
  edge [
    source 22
    target 1354
  ]
  edge [
    source 22
    target 1355
  ]
  edge [
    source 22
    target 1356
  ]
  edge [
    source 22
    target 1357
  ]
  edge [
    source 22
    target 1358
  ]
  edge [
    source 22
    target 1359
  ]
  edge [
    source 22
    target 1360
  ]
  edge [
    source 22
    target 1361
  ]
  edge [
    source 22
    target 1362
  ]
  edge [
    source 22
    target 263
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 22
    target 1363
  ]
  edge [
    source 22
    target 1364
  ]
  edge [
    source 22
    target 1365
  ]
  edge [
    source 22
    target 1366
  ]
  edge [
    source 22
    target 672
  ]
  edge [
    source 22
    target 1367
  ]
  edge [
    source 22
    target 1368
  ]
  edge [
    source 22
    target 1369
  ]
  edge [
    source 22
    target 1139
  ]
  edge [
    source 22
    target 1370
  ]
  edge [
    source 22
    target 1371
  ]
  edge [
    source 22
    target 1372
  ]
  edge [
    source 22
    target 602
  ]
  edge [
    source 22
    target 450
  ]
  edge [
    source 22
    target 609
  ]
  edge [
    source 22
    target 1373
  ]
  edge [
    source 22
    target 1374
  ]
  edge [
    source 22
    target 1375
  ]
  edge [
    source 22
    target 1376
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 1377
  ]
  edge [
    source 25
    target 1378
  ]
  edge [
    source 25
    target 1379
  ]
  edge [
    source 25
    target 1380
  ]
  edge [
    source 586
    target 1393
  ]
  edge [
    source 1381
    target 1382
  ]
  edge [
    source 1383
    target 1384
  ]
  edge [
    source 1383
    target 1385
  ]
  edge [
    source 1384
    target 1385
  ]
  edge [
    source 1386
    target 1387
  ]
  edge [
    source 1388
    target 1389
  ]
  edge [
    source 1390
    target 1391
  ]
  edge [
    source 1390
    target 1392
  ]
  edge [
    source 1390
    target 1394
  ]
  edge [
    source 1390
    target 1395
  ]
  edge [
    source 1391
    target 1392
  ]
  edge [
    source 1394
    target 1395
  ]
]
