graph [
  node [
    id 0
    label "&#347;nieg"
    origin "text"
  ]
  node [
    id 1
    label "mr&#243;z"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spacer"
    origin "text"
  ]
  node [
    id 4
    label "ale"
    origin "text"
  ]
  node [
    id 5
    label "nie"
    origin "text"
  ]
  node [
    id 6
    label "oznaka"
    origin "text"
  ]
  node [
    id 7
    label "wiosna"
    origin "text"
  ]
  node [
    id 8
    label "musza"
    origin "text"
  ]
  node [
    id 9
    label "uzbroi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "si&#281;"
    origin "text"
  ]
  node [
    id 11
    label "cierpliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "pozdrawia&#263;"
    origin "text"
  ]
  node [
    id 13
    label "cieplutko"
    origin "text"
  ]
  node [
    id 14
    label "opad"
  ]
  node [
    id 15
    label "sypn&#261;&#263;"
  ]
  node [
    id 16
    label "kokaina"
  ]
  node [
    id 17
    label "sypni&#281;cie"
  ]
  node [
    id 18
    label "pow&#322;oka"
  ]
  node [
    id 19
    label "rakieta"
  ]
  node [
    id 20
    label "adrenomimetyk"
  ]
  node [
    id 21
    label "bia&#322;y_proszek"
  ]
  node [
    id 22
    label "metyl"
  ]
  node [
    id 23
    label "narkotyk_twardy"
  ]
  node [
    id 24
    label "karbonyl"
  ]
  node [
    id 25
    label "alkaloid"
  ]
  node [
    id 26
    label "fenyl"
  ]
  node [
    id 27
    label "tynk"
  ]
  node [
    id 28
    label "trucizna"
  ]
  node [
    id 29
    label "warstwa"
  ]
  node [
    id 30
    label "poszwa"
  ]
  node [
    id 31
    label "powierzchnia"
  ]
  node [
    id 32
    label "zawalny"
  ]
  node [
    id 33
    label "&#263;wiczenie"
  ]
  node [
    id 34
    label "fall"
  ]
  node [
    id 35
    label "nimbus"
  ]
  node [
    id 36
    label "pluwia&#322;"
  ]
  node [
    id 37
    label "zjawisko"
  ]
  node [
    id 38
    label "substancja"
  ]
  node [
    id 39
    label "statek_kosmiczny"
  ]
  node [
    id 40
    label "szybki"
  ]
  node [
    id 41
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 42
    label "naci&#261;g"
  ]
  node [
    id 43
    label "silnik_rakietowy"
  ]
  node [
    id 44
    label "but"
  ]
  node [
    id 45
    label "&#322;&#261;cznik"
  ]
  node [
    id 46
    label "przyrz&#261;d"
  ]
  node [
    id 47
    label "pojazd"
  ]
  node [
    id 48
    label "g&#322;&#243;wka"
  ]
  node [
    id 49
    label "pojazd_lataj&#261;cy"
  ]
  node [
    id 50
    label "pocisk_odrzutowy"
  ]
  node [
    id 51
    label "tenisista"
  ]
  node [
    id 52
    label "rzuci&#263;"
  ]
  node [
    id 53
    label "spa&#347;&#263;"
  ]
  node [
    id 54
    label "da&#263;"
  ]
  node [
    id 55
    label "pour"
  ]
  node [
    id 56
    label "carry"
  ]
  node [
    id 57
    label "run"
  ]
  node [
    id 58
    label "powiedzie&#263;"
  ]
  node [
    id 59
    label "wygada&#263;_si&#281;"
  ]
  node [
    id 60
    label "danie"
  ]
  node [
    id 61
    label "spadni&#281;cie"
  ]
  node [
    id 62
    label "wydanie"
  ]
  node [
    id 63
    label "rzucenie"
  ]
  node [
    id 64
    label "powiedzenie"
  ]
  node [
    id 65
    label "k&#261;sa&#263;"
  ]
  node [
    id 66
    label "jednostopniowy"
  ]
  node [
    id 67
    label "przymrozek"
  ]
  node [
    id 68
    label "zimno"
  ]
  node [
    id 69
    label "temperatura"
  ]
  node [
    id 70
    label "mrozi&#263;"
  ]
  node [
    id 71
    label "mro&#380;enie"
  ]
  node [
    id 72
    label "k&#261;sanie"
  ]
  node [
    id 73
    label "niepogoda"
  ]
  node [
    id 74
    label "proces"
  ]
  node [
    id 75
    label "boski"
  ]
  node [
    id 76
    label "krajobraz"
  ]
  node [
    id 77
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 78
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 79
    label "przywidzenie"
  ]
  node [
    id 80
    label "presence"
  ]
  node [
    id 81
    label "charakter"
  ]
  node [
    id 82
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 83
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 84
    label "tautochrona"
  ]
  node [
    id 85
    label "denga"
  ]
  node [
    id 86
    label "termoelektryczno&#347;&#263;"
  ]
  node [
    id 87
    label "zapalenie_opon_m&#243;zgowo-rdzeniowych"
  ]
  node [
    id 88
    label "emocja"
  ]
  node [
    id 89
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 90
    label "hotness"
  ]
  node [
    id 91
    label "choroba_bosto&#324;ska"
  ]
  node [
    id 92
    label "atmosfera"
  ]
  node [
    id 93
    label "rozpalony"
  ]
  node [
    id 94
    label "cia&#322;o"
  ]
  node [
    id 95
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 96
    label "zagrza&#263;"
  ]
  node [
    id 97
    label "termoczu&#322;y"
  ]
  node [
    id 98
    label "sytuacja"
  ]
  node [
    id 99
    label "pogoda"
  ]
  node [
    id 100
    label "opryszczkowe_zapalenie_opon_m&#243;zgowych_i_m&#243;zgu"
  ]
  node [
    id 101
    label "kriofil"
  ]
  node [
    id 102
    label "nieczule"
  ]
  node [
    id 103
    label "p&#281;cherz"
  ]
  node [
    id 104
    label "wirus_opryszczki_pospolitej"
  ]
  node [
    id 105
    label "choroba_wirusowa"
  ]
  node [
    id 106
    label "coldness"
  ]
  node [
    id 107
    label "spokojnie"
  ]
  node [
    id 108
    label "ch&#322;odny"
  ]
  node [
    id 109
    label "zimny"
  ]
  node [
    id 110
    label "szron"
  ]
  node [
    id 111
    label "krionika"
  ]
  node [
    id 112
    label "zi&#281;bienie"
  ]
  node [
    id 113
    label "freeze"
  ]
  node [
    id 114
    label "powodowanie"
  ]
  node [
    id 115
    label "parali&#380;owanie"
  ]
  node [
    id 116
    label "traktowanie"
  ]
  node [
    id 117
    label "dzianie_si&#281;"
  ]
  node [
    id 118
    label "pull"
  ]
  node [
    id 119
    label "doskwiera&#263;"
  ]
  node [
    id 120
    label "k&#322;u&#263;"
  ]
  node [
    id 121
    label "szkodzi&#263;"
  ]
  node [
    id 122
    label "kaganiec"
  ]
  node [
    id 123
    label "kaleczy&#263;"
  ]
  node [
    id 124
    label "write_out"
  ]
  node [
    id 125
    label "etapowy"
  ]
  node [
    id 126
    label "zi&#281;bi&#263;"
  ]
  node [
    id 127
    label "nip"
  ]
  node [
    id 128
    label "parali&#380;owa&#263;"
  ]
  node [
    id 129
    label "powodowa&#263;"
  ]
  node [
    id 130
    label "poddawa&#263;"
  ]
  node [
    id 131
    label "refrigerate"
  ]
  node [
    id 132
    label "zagryzanie"
  ]
  node [
    id 133
    label "kaleczenie"
  ]
  node [
    id 134
    label "bolenie"
  ]
  node [
    id 135
    label "bite"
  ]
  node [
    id 136
    label "zagryzienie"
  ]
  node [
    id 137
    label "czynno&#347;&#263;"
  ]
  node [
    id 138
    label "natural_process"
  ]
  node [
    id 139
    label "prezentacja"
  ]
  node [
    id 140
    label "ruch"
  ]
  node [
    id 141
    label "activity"
  ]
  node [
    id 142
    label "bezproblemowy"
  ]
  node [
    id 143
    label "wydarzenie"
  ]
  node [
    id 144
    label "mechanika"
  ]
  node [
    id 145
    label "utrzymywanie"
  ]
  node [
    id 146
    label "move"
  ]
  node [
    id 147
    label "poruszenie"
  ]
  node [
    id 148
    label "movement"
  ]
  node [
    id 149
    label "myk"
  ]
  node [
    id 150
    label "utrzyma&#263;"
  ]
  node [
    id 151
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 152
    label "utrzymanie"
  ]
  node [
    id 153
    label "travel"
  ]
  node [
    id 154
    label "kanciasty"
  ]
  node [
    id 155
    label "commercial_enterprise"
  ]
  node [
    id 156
    label "model"
  ]
  node [
    id 157
    label "strumie&#324;"
  ]
  node [
    id 158
    label "aktywno&#347;&#263;"
  ]
  node [
    id 159
    label "kr&#243;tki"
  ]
  node [
    id 160
    label "taktyka"
  ]
  node [
    id 161
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 162
    label "apraksja"
  ]
  node [
    id 163
    label "utrzymywa&#263;"
  ]
  node [
    id 164
    label "d&#322;ugi"
  ]
  node [
    id 165
    label "dyssypacja_energii"
  ]
  node [
    id 166
    label "tumult"
  ]
  node [
    id 167
    label "stopek"
  ]
  node [
    id 168
    label "zmiana"
  ]
  node [
    id 169
    label "manewr"
  ]
  node [
    id 170
    label "lokomocja"
  ]
  node [
    id 171
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 172
    label "komunikacja"
  ]
  node [
    id 173
    label "drift"
  ]
  node [
    id 174
    label "pokaz&#243;wka"
  ]
  node [
    id 175
    label "prezenter"
  ]
  node [
    id 176
    label "wypowied&#378;"
  ]
  node [
    id 177
    label "impreza"
  ]
  node [
    id 178
    label "grafika_u&#380;ytkowa"
  ]
  node [
    id 179
    label "show"
  ]
  node [
    id 180
    label "szkolenie"
  ]
  node [
    id 181
    label "komunikat"
  ]
  node [
    id 182
    label "informacja"
  ]
  node [
    id 183
    label "piwo"
  ]
  node [
    id 184
    label "warzenie"
  ]
  node [
    id 185
    label "nawarzy&#263;"
  ]
  node [
    id 186
    label "alkohol"
  ]
  node [
    id 187
    label "nap&#243;j"
  ]
  node [
    id 188
    label "bacik"
  ]
  node [
    id 189
    label "wyj&#347;cie"
  ]
  node [
    id 190
    label "uwarzy&#263;"
  ]
  node [
    id 191
    label "birofilia"
  ]
  node [
    id 192
    label "warzy&#263;"
  ]
  node [
    id 193
    label "uwarzenie"
  ]
  node [
    id 194
    label "browarnia"
  ]
  node [
    id 195
    label "nawarzenie"
  ]
  node [
    id 196
    label "anta&#322;"
  ]
  node [
    id 197
    label "sprzeciw"
  ]
  node [
    id 198
    label "czerwona_kartka"
  ]
  node [
    id 199
    label "protestacja"
  ]
  node [
    id 200
    label "reakcja"
  ]
  node [
    id 201
    label "implikowa&#263;"
  ]
  node [
    id 202
    label "signal"
  ]
  node [
    id 203
    label "fakt"
  ]
  node [
    id 204
    label "symbol"
  ]
  node [
    id 205
    label "znak_pisarski"
  ]
  node [
    id 206
    label "znak"
  ]
  node [
    id 207
    label "notacja"
  ]
  node [
    id 208
    label "wcielenie"
  ]
  node [
    id 209
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 210
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 211
    label "character"
  ]
  node [
    id 212
    label "symbolizowanie"
  ]
  node [
    id 213
    label "bia&#322;e_plamy"
  ]
  node [
    id 214
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 215
    label "imply"
  ]
  node [
    id 216
    label "zwiesna"
  ]
  node [
    id 217
    label "przedn&#243;wek"
  ]
  node [
    id 218
    label "pora_roku"
  ]
  node [
    id 219
    label "pocz&#261;tek"
  ]
  node [
    id 220
    label "nowalijka"
  ]
  node [
    id 221
    label "rok"
  ]
  node [
    id 222
    label "pierworodztwo"
  ]
  node [
    id 223
    label "faza"
  ]
  node [
    id 224
    label "miejsce"
  ]
  node [
    id 225
    label "upgrade"
  ]
  node [
    id 226
    label "nast&#281;pstwo"
  ]
  node [
    id 227
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 228
    label "p&#243;&#322;rocze"
  ]
  node [
    id 229
    label "martwy_sezon"
  ]
  node [
    id 230
    label "kalendarz"
  ]
  node [
    id 231
    label "cykl_astronomiczny"
  ]
  node [
    id 232
    label "lata"
  ]
  node [
    id 233
    label "stulecie"
  ]
  node [
    id 234
    label "kurs"
  ]
  node [
    id 235
    label "czas"
  ]
  node [
    id 236
    label "jubileusz"
  ]
  node [
    id 237
    label "grupa"
  ]
  node [
    id 238
    label "kwarta&#322;"
  ]
  node [
    id 239
    label "miesi&#261;c"
  ]
  node [
    id 240
    label "warzywo"
  ]
  node [
    id 241
    label "zainstalowa&#263;"
  ]
  node [
    id 242
    label "prime"
  ]
  node [
    id 243
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 244
    label "accommodate"
  ]
  node [
    id 245
    label "spowodowa&#263;"
  ]
  node [
    id 246
    label "dostosowa&#263;"
  ]
  node [
    id 247
    label "zrobi&#263;"
  ]
  node [
    id 248
    label "komputer"
  ]
  node [
    id 249
    label "program"
  ]
  node [
    id 250
    label "install"
  ]
  node [
    id 251
    label "umie&#347;ci&#263;"
  ]
  node [
    id 252
    label "patience"
  ]
  node [
    id 253
    label "wytrwa&#322;o&#347;&#263;"
  ]
  node [
    id 254
    label "pacjencja"
  ]
  node [
    id 255
    label "wytrzyma&#322;o&#347;&#263;"
  ]
  node [
    id 256
    label "constancy"
  ]
  node [
    id 257
    label "wola"
  ]
  node [
    id 258
    label "usilno&#347;&#263;"
  ]
  node [
    id 259
    label "greet"
  ]
  node [
    id 260
    label "robi&#263;"
  ]
  node [
    id 261
    label "organizowa&#263;"
  ]
  node [
    id 262
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 263
    label "czyni&#263;"
  ]
  node [
    id 264
    label "give"
  ]
  node [
    id 265
    label "stylizowa&#263;"
  ]
  node [
    id 266
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 267
    label "falowa&#263;"
  ]
  node [
    id 268
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 269
    label "peddle"
  ]
  node [
    id 270
    label "praca"
  ]
  node [
    id 271
    label "wydala&#263;"
  ]
  node [
    id 272
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 273
    label "tentegowa&#263;"
  ]
  node [
    id 274
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 275
    label "urz&#261;dza&#263;"
  ]
  node [
    id 276
    label "oszukiwa&#263;"
  ]
  node [
    id 277
    label "work"
  ]
  node [
    id 278
    label "ukazywa&#263;"
  ]
  node [
    id 279
    label "przerabia&#263;"
  ]
  node [
    id 280
    label "act"
  ]
  node [
    id 281
    label "post&#281;powa&#263;"
  ]
  node [
    id 282
    label "cieplutki"
  ]
  node [
    id 283
    label "przyjemnie"
  ]
  node [
    id 284
    label "milutko"
  ]
  node [
    id 285
    label "heartily"
  ]
  node [
    id 286
    label "milutki"
  ]
  node [
    id 287
    label "uroczy"
  ]
  node [
    id 288
    label "korzystny"
  ]
  node [
    id 289
    label "dobrze"
  ]
  node [
    id 290
    label "pleasantly"
  ]
  node [
    id 291
    label "deliciously"
  ]
  node [
    id 292
    label "przyjemny"
  ]
  node [
    id 293
    label "gratifyingly"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 260
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 262
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 264
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 277
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 13
    target 284
  ]
  edge [
    source 13
    target 285
  ]
  edge [
    source 13
    target 286
  ]
  edge [
    source 13
    target 287
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 289
  ]
  edge [
    source 13
    target 290
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 292
  ]
  edge [
    source 13
    target 293
  ]
]
