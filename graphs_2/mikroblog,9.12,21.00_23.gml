graph [
  node [
    id 0
    label "wczoraj"
    origin "text"
  ]
  node [
    id 1
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 2
    label "impreza"
    origin "text"
  ]
  node [
    id 3
    label "firmowy"
    origin "text"
  ]
  node [
    id 4
    label "wygl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 5
    label "trzeba"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "dawno"
  ]
  node [
    id 8
    label "doba"
  ]
  node [
    id 9
    label "niedawno"
  ]
  node [
    id 10
    label "aktualnie"
  ]
  node [
    id 11
    label "ostatni"
  ]
  node [
    id 12
    label "dawny"
  ]
  node [
    id 13
    label "d&#322;ugotrwale"
  ]
  node [
    id 14
    label "wcze&#347;niej"
  ]
  node [
    id 15
    label "ongi&#347;"
  ]
  node [
    id 16
    label "dawnie"
  ]
  node [
    id 17
    label "tydzie&#324;"
  ]
  node [
    id 18
    label "noc"
  ]
  node [
    id 19
    label "dzie&#324;"
  ]
  node [
    id 20
    label "czas"
  ]
  node [
    id 21
    label "godzina"
  ]
  node [
    id 22
    label "long_time"
  ]
  node [
    id 23
    label "jednostka_geologiczna"
  ]
  node [
    id 24
    label "proszek"
  ]
  node [
    id 25
    label "tablet"
  ]
  node [
    id 26
    label "dawka"
  ]
  node [
    id 27
    label "blister"
  ]
  node [
    id 28
    label "lekarstwo"
  ]
  node [
    id 29
    label "cecha"
  ]
  node [
    id 30
    label "impra"
  ]
  node [
    id 31
    label "rozrywka"
  ]
  node [
    id 32
    label "przyj&#281;cie"
  ]
  node [
    id 33
    label "okazja"
  ]
  node [
    id 34
    label "party"
  ]
  node [
    id 35
    label "podw&#243;zka"
  ]
  node [
    id 36
    label "wydarzenie"
  ]
  node [
    id 37
    label "okazka"
  ]
  node [
    id 38
    label "oferta"
  ]
  node [
    id 39
    label "autostop"
  ]
  node [
    id 40
    label "atrakcyjny"
  ]
  node [
    id 41
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 42
    label "sytuacja"
  ]
  node [
    id 43
    label "adeptness"
  ]
  node [
    id 44
    label "spotkanie"
  ]
  node [
    id 45
    label "wpuszczenie"
  ]
  node [
    id 46
    label "credence"
  ]
  node [
    id 47
    label "zobowi&#261;zanie_si&#281;"
  ]
  node [
    id 48
    label "dopuszczenie"
  ]
  node [
    id 49
    label "zareagowanie"
  ]
  node [
    id 50
    label "uznanie"
  ]
  node [
    id 51
    label "presumption"
  ]
  node [
    id 52
    label "wzi&#281;cie"
  ]
  node [
    id 53
    label "entertainment"
  ]
  node [
    id 54
    label "przyj&#261;&#263;"
  ]
  node [
    id 55
    label "reception"
  ]
  node [
    id 56
    label "umieszczenie"
  ]
  node [
    id 57
    label "poch&#322;oni&#281;cie"
  ]
  node [
    id 58
    label "zgodzenie_si&#281;"
  ]
  node [
    id 59
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 60
    label "stanie_si&#281;"
  ]
  node [
    id 61
    label "w&#322;&#261;czenie"
  ]
  node [
    id 62
    label "zrobienie"
  ]
  node [
    id 63
    label "czasoumilacz"
  ]
  node [
    id 64
    label "odpoczynek"
  ]
  node [
    id 65
    label "game"
  ]
  node [
    id 66
    label "oryginalny"
  ]
  node [
    id 67
    label "firmowo"
  ]
  node [
    id 68
    label "markowy"
  ]
  node [
    id 69
    label "niespotykany"
  ]
  node [
    id 70
    label "o&#380;ywczy"
  ]
  node [
    id 71
    label "ekscentryczny"
  ]
  node [
    id 72
    label "nowy"
  ]
  node [
    id 73
    label "oryginalnie"
  ]
  node [
    id 74
    label "inny"
  ]
  node [
    id 75
    label "pierwotny"
  ]
  node [
    id 76
    label "prawdziwy"
  ]
  node [
    id 77
    label "warto&#347;ciowy"
  ]
  node [
    id 78
    label "rozpoznawalny"
  ]
  node [
    id 79
    label "renomowany"
  ]
  node [
    id 80
    label "lookout"
  ]
  node [
    id 81
    label "peep"
  ]
  node [
    id 82
    label "patrze&#263;"
  ]
  node [
    id 83
    label "wyziera&#263;"
  ]
  node [
    id 84
    label "look"
  ]
  node [
    id 85
    label "czeka&#263;"
  ]
  node [
    id 86
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 87
    label "punkt_widzenia"
  ]
  node [
    id 88
    label "koso"
  ]
  node [
    id 89
    label "robi&#263;"
  ]
  node [
    id 90
    label "pogl&#261;da&#263;"
  ]
  node [
    id 91
    label "dba&#263;"
  ]
  node [
    id 92
    label "szuka&#263;"
  ]
  node [
    id 93
    label "uwa&#380;a&#263;"
  ]
  node [
    id 94
    label "traktowa&#263;"
  ]
  node [
    id 95
    label "go_steady"
  ]
  node [
    id 96
    label "os&#261;dza&#263;"
  ]
  node [
    id 97
    label "pauzowa&#263;"
  ]
  node [
    id 98
    label "oczekiwa&#263;"
  ]
  node [
    id 99
    label "decydowa&#263;"
  ]
  node [
    id 100
    label "sp&#281;dza&#263;"
  ]
  node [
    id 101
    label "hold"
  ]
  node [
    id 102
    label "anticipate"
  ]
  node [
    id 103
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 104
    label "mie&#263;_miejsce"
  ]
  node [
    id 105
    label "equal"
  ]
  node [
    id 106
    label "trwa&#263;"
  ]
  node [
    id 107
    label "chodzi&#263;"
  ]
  node [
    id 108
    label "si&#281;ga&#263;"
  ]
  node [
    id 109
    label "stan"
  ]
  node [
    id 110
    label "obecno&#347;&#263;"
  ]
  node [
    id 111
    label "stand"
  ]
  node [
    id 112
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 113
    label "uczestniczy&#263;"
  ]
  node [
    id 114
    label "stylizacja"
  ]
  node [
    id 115
    label "wygl&#261;d"
  ]
  node [
    id 116
    label "necessity"
  ]
  node [
    id 117
    label "trza"
  ]
  node [
    id 118
    label "participate"
  ]
  node [
    id 119
    label "istnie&#263;"
  ]
  node [
    id 120
    label "pozostawa&#263;"
  ]
  node [
    id 121
    label "zostawa&#263;"
  ]
  node [
    id 122
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 123
    label "adhere"
  ]
  node [
    id 124
    label "compass"
  ]
  node [
    id 125
    label "korzysta&#263;"
  ]
  node [
    id 126
    label "appreciation"
  ]
  node [
    id 127
    label "osi&#261;ga&#263;"
  ]
  node [
    id 128
    label "dociera&#263;"
  ]
  node [
    id 129
    label "get"
  ]
  node [
    id 130
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 131
    label "mierzy&#263;"
  ]
  node [
    id 132
    label "u&#380;ywa&#263;"
  ]
  node [
    id 133
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 134
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 135
    label "exsert"
  ]
  node [
    id 136
    label "being"
  ]
  node [
    id 137
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 138
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 139
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 140
    label "p&#322;ywa&#263;"
  ]
  node [
    id 141
    label "run"
  ]
  node [
    id 142
    label "bangla&#263;"
  ]
  node [
    id 143
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 144
    label "przebiega&#263;"
  ]
  node [
    id 145
    label "wk&#322;ada&#263;"
  ]
  node [
    id 146
    label "proceed"
  ]
  node [
    id 147
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 148
    label "carry"
  ]
  node [
    id 149
    label "bywa&#263;"
  ]
  node [
    id 150
    label "dziama&#263;"
  ]
  node [
    id 151
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 152
    label "stara&#263;_si&#281;"
  ]
  node [
    id 153
    label "para"
  ]
  node [
    id 154
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 155
    label "str&#243;j"
  ]
  node [
    id 156
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 157
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 158
    label "krok"
  ]
  node [
    id 159
    label "tryb"
  ]
  node [
    id 160
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 161
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 162
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 163
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 164
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 165
    label "continue"
  ]
  node [
    id 166
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 167
    label "Ohio"
  ]
  node [
    id 168
    label "wci&#281;cie"
  ]
  node [
    id 169
    label "Nowy_York"
  ]
  node [
    id 170
    label "warstwa"
  ]
  node [
    id 171
    label "samopoczucie"
  ]
  node [
    id 172
    label "Illinois"
  ]
  node [
    id 173
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 174
    label "state"
  ]
  node [
    id 175
    label "Jukatan"
  ]
  node [
    id 176
    label "Kalifornia"
  ]
  node [
    id 177
    label "Wirginia"
  ]
  node [
    id 178
    label "wektor"
  ]
  node [
    id 179
    label "Goa"
  ]
  node [
    id 180
    label "Teksas"
  ]
  node [
    id 181
    label "Waszyngton"
  ]
  node [
    id 182
    label "miejsce"
  ]
  node [
    id 183
    label "Massachusetts"
  ]
  node [
    id 184
    label "Alaska"
  ]
  node [
    id 185
    label "Arakan"
  ]
  node [
    id 186
    label "Hawaje"
  ]
  node [
    id 187
    label "Maryland"
  ]
  node [
    id 188
    label "punkt"
  ]
  node [
    id 189
    label "Michigan"
  ]
  node [
    id 190
    label "Arizona"
  ]
  node [
    id 191
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 192
    label "Georgia"
  ]
  node [
    id 193
    label "poziom"
  ]
  node [
    id 194
    label "Pensylwania"
  ]
  node [
    id 195
    label "shape"
  ]
  node [
    id 196
    label "Luizjana"
  ]
  node [
    id 197
    label "Nowy_Meksyk"
  ]
  node [
    id 198
    label "Alabama"
  ]
  node [
    id 199
    label "ilo&#347;&#263;"
  ]
  node [
    id 200
    label "Kansas"
  ]
  node [
    id 201
    label "Oregon"
  ]
  node [
    id 202
    label "Oklahoma"
  ]
  node [
    id 203
    label "Floryda"
  ]
  node [
    id 204
    label "jednostka_administracyjna"
  ]
  node [
    id 205
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
]
