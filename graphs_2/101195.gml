graph [
  node [
    id 0
    label "kondukt"
    origin "text"
  ]
  node [
    id 1
    label "pogrzebowy"
    origin "text"
  ]
  node [
    id 2
    label "karawaniarz"
  ]
  node [
    id 3
    label "&#380;a&#322;obnik"
  ]
  node [
    id 4
    label "orszak"
  ]
  node [
    id 5
    label "grupa"
  ]
  node [
    id 6
    label "cortege"
  ]
  node [
    id 7
    label "asysta"
  ]
  node [
    id 8
    label "uczestnik"
  ]
  node [
    id 9
    label "rusa&#322;ka"
  ]
  node [
    id 10
    label "typowy"
  ]
  node [
    id 11
    label "kirowy"
  ]
  node [
    id 12
    label "pogrzebowo"
  ]
  node [
    id 13
    label "&#380;a&#322;obny"
  ]
  node [
    id 14
    label "&#380;a&#322;osny"
  ]
  node [
    id 15
    label "smutny"
  ]
  node [
    id 16
    label "&#380;a&#322;obnie"
  ]
  node [
    id 17
    label "ponury"
  ]
  node [
    id 18
    label "czarny"
  ]
  node [
    id 19
    label "somberly"
  ]
  node [
    id 20
    label "typowo"
  ]
  node [
    id 21
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 22
    label "zwyczajny"
  ]
  node [
    id 23
    label "cz&#281;sty"
  ]
  node [
    id 24
    label "zwyk&#322;y"
  ]
  node [
    id 25
    label "mi&#322;owa&#263;"
  ]
  node [
    id 26
    label "pan"
  ]
  node [
    id 27
    label "ja"
  ]
  node [
    id 28
    label "by&#263;"
  ]
  node [
    id 29
    label "zmartwychwsta&#263;"
  ]
  node [
    id 30
    label "i"
  ]
  node [
    id 31
    label "&#380;y&#263;"
  ]
  node [
    id 32
    label "modlitwa"
  ]
  node [
    id 33
    label "pa&#324;ski"
  ]
  node [
    id 34
    label "wita&#263;"
  ]
  node [
    id 35
    label "kr&#243;lowa"
  ]
  node [
    id 36
    label "anio&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 27
    target 31
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 34
    target 35
  ]
]
