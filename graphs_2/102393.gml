graph [
  node [
    id 0
    label "rocznik"
    origin "text"
  ]
  node [
    id 1
    label "formacja"
  ]
  node [
    id 2
    label "yearbook"
  ]
  node [
    id 3
    label "czasopismo"
  ]
  node [
    id 4
    label "kronika"
  ]
  node [
    id 5
    label "Bund"
  ]
  node [
    id 6
    label "Mazowsze"
  ]
  node [
    id 7
    label "PPR"
  ]
  node [
    id 8
    label "Jakobici"
  ]
  node [
    id 9
    label "zesp&#243;&#322;"
  ]
  node [
    id 10
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 11
    label "leksem"
  ]
  node [
    id 12
    label "SLD"
  ]
  node [
    id 13
    label "zespolik"
  ]
  node [
    id 14
    label "Razem"
  ]
  node [
    id 15
    label "PiS"
  ]
  node [
    id 16
    label "zjawisko"
  ]
  node [
    id 17
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 18
    label "partia"
  ]
  node [
    id 19
    label "Kuomintang"
  ]
  node [
    id 20
    label "ZSL"
  ]
  node [
    id 21
    label "szko&#322;a"
  ]
  node [
    id 22
    label "jednostka"
  ]
  node [
    id 23
    label "proces"
  ]
  node [
    id 24
    label "organizacja"
  ]
  node [
    id 25
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 26
    label "rugby"
  ]
  node [
    id 27
    label "AWS"
  ]
  node [
    id 28
    label "posta&#263;"
  ]
  node [
    id 29
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 30
    label "blok"
  ]
  node [
    id 31
    label "PO"
  ]
  node [
    id 32
    label "si&#322;a"
  ]
  node [
    id 33
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 34
    label "Federali&#347;ci"
  ]
  node [
    id 35
    label "PSL"
  ]
  node [
    id 36
    label "czynno&#347;&#263;"
  ]
  node [
    id 37
    label "wojsko"
  ]
  node [
    id 38
    label "Wigowie"
  ]
  node [
    id 39
    label "ZChN"
  ]
  node [
    id 40
    label "egzekutywa"
  ]
  node [
    id 41
    label "The_Beatles"
  ]
  node [
    id 42
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 43
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 44
    label "unit"
  ]
  node [
    id 45
    label "Depeche_Mode"
  ]
  node [
    id 46
    label "forma"
  ]
  node [
    id 47
    label "zapis"
  ]
  node [
    id 48
    label "chronograf"
  ]
  node [
    id 49
    label "latopis"
  ]
  node [
    id 50
    label "ksi&#281;ga"
  ]
  node [
    id 51
    label "egzemplarz"
  ]
  node [
    id 52
    label "psychotest"
  ]
  node [
    id 53
    label "pismo"
  ]
  node [
    id 54
    label "communication"
  ]
  node [
    id 55
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 56
    label "wk&#322;ad"
  ]
  node [
    id 57
    label "zajawka"
  ]
  node [
    id 58
    label "ok&#322;adka"
  ]
  node [
    id 59
    label "Zwrotnica"
  ]
  node [
    id 60
    label "dzia&#322;"
  ]
  node [
    id 61
    label "prasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
]
