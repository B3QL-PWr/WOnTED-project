graph [
  node [
    id 0
    label "kardyna&#322;ek"
    origin "text"
  ]
  node [
    id 1
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "ryba"
  ]
  node [
    id 3
    label "karpiowate"
  ]
  node [
    id 4
    label "systemik"
  ]
  node [
    id 5
    label "tar&#322;o"
  ]
  node [
    id 6
    label "rakowato&#347;&#263;"
  ]
  node [
    id 7
    label "szczelina_skrzelowa"
  ]
  node [
    id 8
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 9
    label "doniczkowiec"
  ]
  node [
    id 10
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 11
    label "cz&#322;owiek"
  ]
  node [
    id 12
    label "mi&#281;so"
  ]
  node [
    id 13
    label "fish"
  ]
  node [
    id 14
    label "patroszy&#263;"
  ]
  node [
    id 15
    label "linia_boczna"
  ]
  node [
    id 16
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 17
    label "pokrywa_skrzelowa"
  ]
  node [
    id 18
    label "kr&#281;gowiec"
  ]
  node [
    id 19
    label "w&#281;dkarstwo"
  ]
  node [
    id 20
    label "ryby"
  ]
  node [
    id 21
    label "m&#281;tnooki"
  ]
  node [
    id 22
    label "ikra"
  ]
  node [
    id 23
    label "system"
  ]
  node [
    id 24
    label "wyrostek_filtracyjny"
  ]
  node [
    id 25
    label "karpiokszta&#322;tne"
  ]
  node [
    id 26
    label "lipny"
  ]
  node [
    id 27
    label "dalekowschodni"
  ]
  node [
    id 28
    label "makroj&#281;zyk"
  ]
  node [
    id 29
    label "niedrogi"
  ]
  node [
    id 30
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 31
    label "kitajski"
  ]
  node [
    id 32
    label "chi&#324;sko"
  ]
  node [
    id 33
    label "go"
  ]
  node [
    id 34
    label "azjatycki"
  ]
  node [
    id 35
    label "tandetny"
  ]
  node [
    id 36
    label "po_chi&#324;sku"
  ]
  node [
    id 37
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 38
    label "j&#281;zyk"
  ]
  node [
    id 39
    label "dziwaczny"
  ]
  node [
    id 40
    label "pisa&#263;"
  ]
  node [
    id 41
    label "kod"
  ]
  node [
    id 42
    label "pype&#263;"
  ]
  node [
    id 43
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 44
    label "gramatyka"
  ]
  node [
    id 45
    label "language"
  ]
  node [
    id 46
    label "fonetyka"
  ]
  node [
    id 47
    label "t&#322;umaczenie"
  ]
  node [
    id 48
    label "artykulator"
  ]
  node [
    id 49
    label "rozumienie"
  ]
  node [
    id 50
    label "jama_ustna"
  ]
  node [
    id 51
    label "urz&#261;dzenie"
  ]
  node [
    id 52
    label "organ"
  ]
  node [
    id 53
    label "ssanie"
  ]
  node [
    id 54
    label "lizanie"
  ]
  node [
    id 55
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 56
    label "przedmiot"
  ]
  node [
    id 57
    label "liza&#263;"
  ]
  node [
    id 58
    label "makroglosja"
  ]
  node [
    id 59
    label "natural_language"
  ]
  node [
    id 60
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 61
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 62
    label "napisa&#263;"
  ]
  node [
    id 63
    label "m&#243;wienie"
  ]
  node [
    id 64
    label "s&#322;ownictwo"
  ]
  node [
    id 65
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 66
    label "konsonantyzm"
  ]
  node [
    id 67
    label "ssa&#263;"
  ]
  node [
    id 68
    label "wokalizm"
  ]
  node [
    id 69
    label "kultura_duchowa"
  ]
  node [
    id 70
    label "formalizowanie"
  ]
  node [
    id 71
    label "jeniec"
  ]
  node [
    id 72
    label "m&#243;wi&#263;"
  ]
  node [
    id 73
    label "kawa&#322;ek"
  ]
  node [
    id 74
    label "po_koroniarsku"
  ]
  node [
    id 75
    label "rozumie&#263;"
  ]
  node [
    id 76
    label "stylik"
  ]
  node [
    id 77
    label "przet&#322;umaczenie"
  ]
  node [
    id 78
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 79
    label "formacja_geologiczna"
  ]
  node [
    id 80
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 81
    label "spos&#243;b"
  ]
  node [
    id 82
    label "but"
  ]
  node [
    id 83
    label "pismo"
  ]
  node [
    id 84
    label "formalizowa&#263;"
  ]
  node [
    id 85
    label "niedrogo"
  ]
  node [
    id 86
    label "tandetnie"
  ]
  node [
    id 87
    label "odra&#380;aj&#261;cy"
  ]
  node [
    id 88
    label "&#380;a&#322;osny"
  ]
  node [
    id 89
    label "banalny"
  ]
  node [
    id 90
    label "kiczowaty"
  ]
  node [
    id 91
    label "nieelegancki"
  ]
  node [
    id 92
    label "kiepski"
  ]
  node [
    id 93
    label "nikczemny"
  ]
  node [
    id 94
    label "tani"
  ]
  node [
    id 95
    label "siajowy"
  ]
  node [
    id 96
    label "z&#322;y"
  ]
  node [
    id 97
    label "nieprawdziwy"
  ]
  node [
    id 98
    label "lipnie"
  ]
  node [
    id 99
    label "po_dalekowschodniemu"
  ]
  node [
    id 100
    label "j&#281;zyk_naturalny"
  ]
  node [
    id 101
    label "ka&#322;mucki"
  ]
  node [
    id 102
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 103
    label "kampong"
  ]
  node [
    id 104
    label "balut"
  ]
  node [
    id 105
    label "typowy"
  ]
  node [
    id 106
    label "charakterystyczny"
  ]
  node [
    id 107
    label "ghaty"
  ]
  node [
    id 108
    label "azjatycko"
  ]
  node [
    id 109
    label "dziwny"
  ]
  node [
    id 110
    label "dziwacznie"
  ]
  node [
    id 111
    label "dziwotworny"
  ]
  node [
    id 112
    label "goban"
  ]
  node [
    id 113
    label "gra_planszowa"
  ]
  node [
    id 114
    label "sport_umys&#322;owy"
  ]
  node [
    id 115
    label "g&#243;ry"
  ]
  node [
    id 116
    label "Po&#322;udniowochi&#324;skie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 115
    target 116
  ]
]
