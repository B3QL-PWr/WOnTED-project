graph [
  node [
    id 0
    label "zlodzieje"
    origin "text"
  ]
  node [
    id 1
    label "kradziez"
    origin "text"
  ]
  node [
    id 2
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 3
    label "wojsko"
    origin "text"
  ]
  node [
    id 4
    label "prokuratura"
    origin "text"
  ]
  node [
    id 5
    label "policja"
    origin "text"
  ]
  node [
    id 6
    label "prawo"
    origin "text"
  ]
  node [
    id 7
    label "pytaniedoeksperta"
    origin "text"
  ]
  node [
    id 8
    label "kradzie&#380;"
    origin "text"
  ]
  node [
    id 9
    label "osobowy"
    origin "text"
  ]
  node [
    id 10
    label "cywilny"
    origin "text"
  ]
  node [
    id 11
    label "osa"
    origin "text"
  ]
  node [
    id 12
    label "fizyczny"
    origin "text"
  ]
  node [
    id 13
    label "jakims"
    origin "text"
  ]
  node [
    id 14
    label "miesiacu"
    origin "text"
  ]
  node [
    id 15
    label "sprawa"
    origin "text"
  ]
  node [
    id 16
    label "le&#380;&#261;ca"
    origin "text"
  ]
  node [
    id 17
    label "trafila"
    origin "text"
  ]
  node [
    id 18
    label "wojskowy"
    origin "text"
  ]
  node [
    id 19
    label "pojazd_drogowy"
  ]
  node [
    id 20
    label "spryskiwacz"
  ]
  node [
    id 21
    label "most"
  ]
  node [
    id 22
    label "baga&#380;nik"
  ]
  node [
    id 23
    label "silnik"
  ]
  node [
    id 24
    label "dachowanie"
  ]
  node [
    id 25
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 26
    label "pompa_wodna"
  ]
  node [
    id 27
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 28
    label "poduszka_powietrzna"
  ]
  node [
    id 29
    label "tempomat"
  ]
  node [
    id 30
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 31
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 32
    label "deska_rozdzielcza"
  ]
  node [
    id 33
    label "immobilizer"
  ]
  node [
    id 34
    label "t&#322;umik"
  ]
  node [
    id 35
    label "kierownica"
  ]
  node [
    id 36
    label "ABS"
  ]
  node [
    id 37
    label "bak"
  ]
  node [
    id 38
    label "dwu&#347;lad"
  ]
  node [
    id 39
    label "poci&#261;g_drogowy"
  ]
  node [
    id 40
    label "wycieraczka"
  ]
  node [
    id 41
    label "pojazd"
  ]
  node [
    id 42
    label "sprinkler"
  ]
  node [
    id 43
    label "urz&#261;dzenie"
  ]
  node [
    id 44
    label "przyrz&#261;d"
  ]
  node [
    id 45
    label "rzuci&#263;"
  ]
  node [
    id 46
    label "prz&#281;s&#322;o"
  ]
  node [
    id 47
    label "m&#243;zg"
  ]
  node [
    id 48
    label "trasa"
  ]
  node [
    id 49
    label "jarzmo_mostowe"
  ]
  node [
    id 50
    label "pylon"
  ]
  node [
    id 51
    label "zam&#243;zgowie"
  ]
  node [
    id 52
    label "obiekt_mostowy"
  ]
  node [
    id 53
    label "szczelina_dylatacyjna"
  ]
  node [
    id 54
    label "rzucenie"
  ]
  node [
    id 55
    label "bridge"
  ]
  node [
    id 56
    label "rzuca&#263;"
  ]
  node [
    id 57
    label "suwnica"
  ]
  node [
    id 58
    label "porozumienie"
  ]
  node [
    id 59
    label "nap&#281;d"
  ]
  node [
    id 60
    label "rzucanie"
  ]
  node [
    id 61
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 62
    label "motor"
  ]
  node [
    id 63
    label "rower"
  ]
  node [
    id 64
    label "stolik_topograficzny"
  ]
  node [
    id 65
    label "kontroler_gier"
  ]
  node [
    id 66
    label "bakenbardy"
  ]
  node [
    id 67
    label "tank"
  ]
  node [
    id 68
    label "fordek"
  ]
  node [
    id 69
    label "zbiornik"
  ]
  node [
    id 70
    label "beard"
  ]
  node [
    id 71
    label "zarost"
  ]
  node [
    id 72
    label "ochrona"
  ]
  node [
    id 73
    label "mata"
  ]
  node [
    id 74
    label "biblioteka"
  ]
  node [
    id 75
    label "radiator"
  ]
  node [
    id 76
    label "wyci&#261;garka"
  ]
  node [
    id 77
    label "gondola_silnikowa"
  ]
  node [
    id 78
    label "aerosanie"
  ]
  node [
    id 79
    label "podgrzewacz"
  ]
  node [
    id 80
    label "motogodzina"
  ]
  node [
    id 81
    label "motoszybowiec"
  ]
  node [
    id 82
    label "program"
  ]
  node [
    id 83
    label "gniazdo_zaworowe"
  ]
  node [
    id 84
    label "mechanizm"
  ]
  node [
    id 85
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 86
    label "dociera&#263;"
  ]
  node [
    id 87
    label "dotarcie"
  ]
  node [
    id 88
    label "motor&#243;wka"
  ]
  node [
    id 89
    label "rz&#281;zi&#263;"
  ]
  node [
    id 90
    label "perpetuum_mobile"
  ]
  node [
    id 91
    label "docieranie"
  ]
  node [
    id 92
    label "bombowiec"
  ]
  node [
    id 93
    label "dotrze&#263;"
  ]
  node [
    id 94
    label "rz&#281;&#380;enie"
  ]
  node [
    id 95
    label "rekwizyt_muzyczny"
  ]
  node [
    id 96
    label "attenuator"
  ]
  node [
    id 97
    label "regulator"
  ]
  node [
    id 98
    label "bro&#324;_palna"
  ]
  node [
    id 99
    label "g&#243;ra_mi&#281;sa"
  ]
  node [
    id 100
    label "cycek"
  ]
  node [
    id 101
    label "biust"
  ]
  node [
    id 102
    label "cz&#322;owiek"
  ]
  node [
    id 103
    label "hamowanie"
  ]
  node [
    id 104
    label "uk&#322;ad"
  ]
  node [
    id 105
    label "acrylonitrile-butadiene-styrene"
  ]
  node [
    id 106
    label "sze&#347;ciopak"
  ]
  node [
    id 107
    label "kulturysta"
  ]
  node [
    id 108
    label "mu&#322;y"
  ]
  node [
    id 109
    label "przewracanie_si&#281;"
  ]
  node [
    id 110
    label "przewr&#243;cenie_si&#281;"
  ]
  node [
    id 111
    label "jechanie"
  ]
  node [
    id 112
    label "zrejterowanie"
  ]
  node [
    id 113
    label "zmobilizowa&#263;"
  ]
  node [
    id 114
    label "przedmiot"
  ]
  node [
    id 115
    label "dezerter"
  ]
  node [
    id 116
    label "oddzia&#322;_karny"
  ]
  node [
    id 117
    label "rezerwa"
  ]
  node [
    id 118
    label "tabor"
  ]
  node [
    id 119
    label "wermacht"
  ]
  node [
    id 120
    label "cofni&#281;cie"
  ]
  node [
    id 121
    label "potencja"
  ]
  node [
    id 122
    label "fala"
  ]
  node [
    id 123
    label "struktura"
  ]
  node [
    id 124
    label "szko&#322;a"
  ]
  node [
    id 125
    label "korpus"
  ]
  node [
    id 126
    label "soldateska"
  ]
  node [
    id 127
    label "ods&#322;ugiwanie"
  ]
  node [
    id 128
    label "werbowanie_si&#281;"
  ]
  node [
    id 129
    label "zdemobilizowanie"
  ]
  node [
    id 130
    label "oddzia&#322;"
  ]
  node [
    id 131
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 132
    label "s&#322;u&#380;ba"
  ]
  node [
    id 133
    label "or&#281;&#380;"
  ]
  node [
    id 134
    label "Legia_Cudzoziemska"
  ]
  node [
    id 135
    label "Armia_Czerwona"
  ]
  node [
    id 136
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 137
    label "rejterowanie"
  ]
  node [
    id 138
    label "Czerwona_Gwardia"
  ]
  node [
    id 139
    label "si&#322;a"
  ]
  node [
    id 140
    label "zrejterowa&#263;"
  ]
  node [
    id 141
    label "sztabslekarz"
  ]
  node [
    id 142
    label "zmobilizowanie"
  ]
  node [
    id 143
    label "wojo"
  ]
  node [
    id 144
    label "pospolite_ruszenie"
  ]
  node [
    id 145
    label "Eurokorpus"
  ]
  node [
    id 146
    label "mobilizowanie"
  ]
  node [
    id 147
    label "rejterowa&#263;"
  ]
  node [
    id 148
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 149
    label "mobilizowa&#263;"
  ]
  node [
    id 150
    label "Armia_Krajowa"
  ]
  node [
    id 151
    label "obrona"
  ]
  node [
    id 152
    label "dryl"
  ]
  node [
    id 153
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 154
    label "petarda"
  ]
  node [
    id 155
    label "pozycja"
  ]
  node [
    id 156
    label "zdemobilizowa&#263;"
  ]
  node [
    id 157
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 158
    label "mechanika"
  ]
  node [
    id 159
    label "o&#347;"
  ]
  node [
    id 160
    label "usenet"
  ]
  node [
    id 161
    label "rozprz&#261;c"
  ]
  node [
    id 162
    label "zachowanie"
  ]
  node [
    id 163
    label "cybernetyk"
  ]
  node [
    id 164
    label "podsystem"
  ]
  node [
    id 165
    label "system"
  ]
  node [
    id 166
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 167
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 168
    label "sk&#322;ad"
  ]
  node [
    id 169
    label "systemat"
  ]
  node [
    id 170
    label "cecha"
  ]
  node [
    id 171
    label "konstrukcja"
  ]
  node [
    id 172
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 173
    label "konstelacja"
  ]
  node [
    id 174
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 175
    label "zesp&#243;&#322;"
  ]
  node [
    id 176
    label "instytucja"
  ]
  node [
    id 177
    label "wys&#322;uga"
  ]
  node [
    id 178
    label "service"
  ]
  node [
    id 179
    label "czworak"
  ]
  node [
    id 180
    label "ZOMO"
  ]
  node [
    id 181
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 182
    label "praca"
  ]
  node [
    id 183
    label "ma&#322;pa_ogoniasta"
  ]
  node [
    id 184
    label "zdyscyplinowanie"
  ]
  node [
    id 185
    label "&#263;wiczenie"
  ]
  node [
    id 186
    label "metoda"
  ]
  node [
    id 187
    label "wszystko&#380;erca"
  ]
  node [
    id 188
    label "do&#347;wiadczenie"
  ]
  node [
    id 189
    label "teren_szko&#322;y"
  ]
  node [
    id 190
    label "wiedza"
  ]
  node [
    id 191
    label "Mickiewicz"
  ]
  node [
    id 192
    label "kwalifikacje"
  ]
  node [
    id 193
    label "podr&#281;cznik"
  ]
  node [
    id 194
    label "absolwent"
  ]
  node [
    id 195
    label "praktyka"
  ]
  node [
    id 196
    label "school"
  ]
  node [
    id 197
    label "zda&#263;"
  ]
  node [
    id 198
    label "gabinet"
  ]
  node [
    id 199
    label "urszulanki"
  ]
  node [
    id 200
    label "sztuba"
  ]
  node [
    id 201
    label "&#322;awa_szkolna"
  ]
  node [
    id 202
    label "nauka"
  ]
  node [
    id 203
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 204
    label "przepisa&#263;"
  ]
  node [
    id 205
    label "muzyka"
  ]
  node [
    id 206
    label "grupa"
  ]
  node [
    id 207
    label "form"
  ]
  node [
    id 208
    label "klasa"
  ]
  node [
    id 209
    label "lekcja"
  ]
  node [
    id 210
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 211
    label "przepisanie"
  ]
  node [
    id 212
    label "czas"
  ]
  node [
    id 213
    label "skolaryzacja"
  ]
  node [
    id 214
    label "zdanie"
  ]
  node [
    id 215
    label "stopek"
  ]
  node [
    id 216
    label "sekretariat"
  ]
  node [
    id 217
    label "ideologia"
  ]
  node [
    id 218
    label "lesson"
  ]
  node [
    id 219
    label "niepokalanki"
  ]
  node [
    id 220
    label "siedziba"
  ]
  node [
    id 221
    label "szkolenie"
  ]
  node [
    id 222
    label "kara"
  ]
  node [
    id 223
    label "tablica"
  ]
  node [
    id 224
    label "zboczenie"
  ]
  node [
    id 225
    label "om&#243;wienie"
  ]
  node [
    id 226
    label "sponiewieranie"
  ]
  node [
    id 227
    label "discipline"
  ]
  node [
    id 228
    label "rzecz"
  ]
  node [
    id 229
    label "omawia&#263;"
  ]
  node [
    id 230
    label "kr&#261;&#380;enie"
  ]
  node [
    id 231
    label "tre&#347;&#263;"
  ]
  node [
    id 232
    label "robienie"
  ]
  node [
    id 233
    label "sponiewiera&#263;"
  ]
  node [
    id 234
    label "element"
  ]
  node [
    id 235
    label "entity"
  ]
  node [
    id 236
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 237
    label "tematyka"
  ]
  node [
    id 238
    label "w&#261;tek"
  ]
  node [
    id 239
    label "charakter"
  ]
  node [
    id 240
    label "zbaczanie"
  ]
  node [
    id 241
    label "program_nauczania"
  ]
  node [
    id 242
    label "om&#243;wi&#263;"
  ]
  node [
    id 243
    label "omawianie"
  ]
  node [
    id 244
    label "thing"
  ]
  node [
    id 245
    label "kultura"
  ]
  node [
    id 246
    label "istota"
  ]
  node [
    id 247
    label "zbacza&#263;"
  ]
  node [
    id 248
    label "zboczy&#263;"
  ]
  node [
    id 249
    label "pachwina"
  ]
  node [
    id 250
    label "obudowa"
  ]
  node [
    id 251
    label "corpus"
  ]
  node [
    id 252
    label "brzuch"
  ]
  node [
    id 253
    label "budowla"
  ]
  node [
    id 254
    label "sto&#380;ek_wzrostu"
  ]
  node [
    id 255
    label "dywizja"
  ]
  node [
    id 256
    label "mi&#281;so"
  ]
  node [
    id 257
    label "dekolt"
  ]
  node [
    id 258
    label "zad"
  ]
  node [
    id 259
    label "documentation"
  ]
  node [
    id 260
    label "zbi&#243;r"
  ]
  node [
    id 261
    label "zasadzi&#263;"
  ]
  node [
    id 262
    label "zasadzenie"
  ]
  node [
    id 263
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 264
    label "struktura_anatomiczna"
  ]
  node [
    id 265
    label "bok"
  ]
  node [
    id 266
    label "podstawowy"
  ]
  node [
    id 267
    label "pupa"
  ]
  node [
    id 268
    label "krocze"
  ]
  node [
    id 269
    label "pier&#347;"
  ]
  node [
    id 270
    label "za&#322;o&#380;enie"
  ]
  node [
    id 271
    label "tuszka"
  ]
  node [
    id 272
    label "punkt_odniesienia"
  ]
  node [
    id 273
    label "konkordancja"
  ]
  node [
    id 274
    label "plecy"
  ]
  node [
    id 275
    label "klatka_piersiowa"
  ]
  node [
    id 276
    label "dr&#243;b"
  ]
  node [
    id 277
    label "biodro"
  ]
  node [
    id 278
    label "pacha"
  ]
  node [
    id 279
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 280
    label "despotyzm"
  ]
  node [
    id 281
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 282
    label "dzia&#322;"
  ]
  node [
    id 283
    label "lias"
  ]
  node [
    id 284
    label "jednostka"
  ]
  node [
    id 285
    label "pi&#281;tro"
  ]
  node [
    id 286
    label "jednostka_geologiczna"
  ]
  node [
    id 287
    label "filia"
  ]
  node [
    id 288
    label "malm"
  ]
  node [
    id 289
    label "whole"
  ]
  node [
    id 290
    label "dogger"
  ]
  node [
    id 291
    label "poziom"
  ]
  node [
    id 292
    label "promocja"
  ]
  node [
    id 293
    label "kurs"
  ]
  node [
    id 294
    label "bank"
  ]
  node [
    id 295
    label "formacja"
  ]
  node [
    id 296
    label "ajencja"
  ]
  node [
    id 297
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 298
    label "agencja"
  ]
  node [
    id 299
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 300
    label "szpital"
  ]
  node [
    id 301
    label "egzamin"
  ]
  node [
    id 302
    label "walka"
  ]
  node [
    id 303
    label "liga"
  ]
  node [
    id 304
    label "gracz"
  ]
  node [
    id 305
    label "poj&#281;cie"
  ]
  node [
    id 306
    label "protection"
  ]
  node [
    id 307
    label "poparcie"
  ]
  node [
    id 308
    label "mecz"
  ]
  node [
    id 309
    label "reakcja"
  ]
  node [
    id 310
    label "defense"
  ]
  node [
    id 311
    label "s&#261;d"
  ]
  node [
    id 312
    label "auspices"
  ]
  node [
    id 313
    label "gra"
  ]
  node [
    id 314
    label "sp&#243;r"
  ]
  node [
    id 315
    label "post&#281;powanie"
  ]
  node [
    id 316
    label "manewr"
  ]
  node [
    id 317
    label "defensive_structure"
  ]
  node [
    id 318
    label "guard_duty"
  ]
  node [
    id 319
    label "strona"
  ]
  node [
    id 320
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 321
    label "zas&#243;b"
  ]
  node [
    id 322
    label "nieufno&#347;&#263;"
  ]
  node [
    id 323
    label "zapasy"
  ]
  node [
    id 324
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 325
    label "resource"
  ]
  node [
    id 326
    label "potency"
  ]
  node [
    id 327
    label "byt"
  ]
  node [
    id 328
    label "tomizm"
  ]
  node [
    id 329
    label "wydolno&#347;&#263;"
  ]
  node [
    id 330
    label "zdolno&#347;&#263;"
  ]
  node [
    id 331
    label "arystotelizm"
  ]
  node [
    id 332
    label "gotowo&#347;&#263;"
  ]
  node [
    id 333
    label "element_anatomiczny"
  ]
  node [
    id 334
    label "poro&#380;e"
  ]
  node [
    id 335
    label "heraldyka"
  ]
  node [
    id 336
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 337
    label "bro&#324;"
  ]
  node [
    id 338
    label "call"
  ]
  node [
    id 339
    label "stawia&#263;_w_stan_pogotowia"
  ]
  node [
    id 340
    label "usposabia&#263;"
  ]
  node [
    id 341
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 342
    label "pobudza&#263;"
  ]
  node [
    id 343
    label "boost"
  ]
  node [
    id 344
    label "przygotowywa&#263;"
  ]
  node [
    id 345
    label "revocation"
  ]
  node [
    id 346
    label "cofni&#281;cie_si&#281;"
  ]
  node [
    id 347
    label "spowodowanie"
  ]
  node [
    id 348
    label "uniewa&#380;nienie"
  ]
  node [
    id 349
    label "przemieszczenie"
  ]
  node [
    id 350
    label "coitus_interruptus"
  ]
  node [
    id 351
    label "retraction"
  ]
  node [
    id 352
    label "wycofywanie_si&#281;"
  ]
  node [
    id 353
    label "uciekanie"
  ]
  node [
    id 354
    label "rezygnowanie"
  ]
  node [
    id 355
    label "unikanie"
  ]
  node [
    id 356
    label "energia"
  ]
  node [
    id 357
    label "parametr"
  ]
  node [
    id 358
    label "rozwi&#261;zanie"
  ]
  node [
    id 359
    label "wuchta"
  ]
  node [
    id 360
    label "zaleta"
  ]
  node [
    id 361
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 362
    label "moment_si&#322;y"
  ]
  node [
    id 363
    label "mn&#243;stwo"
  ]
  node [
    id 364
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 365
    label "zjawisko"
  ]
  node [
    id 366
    label "capacity"
  ]
  node [
    id 367
    label "magnitude"
  ]
  node [
    id 368
    label "przemoc"
  ]
  node [
    id 369
    label "nastawi&#263;"
  ]
  node [
    id 370
    label "wake_up"
  ]
  node [
    id 371
    label "powo&#322;a&#263;"
  ]
  node [
    id 372
    label "postawi&#263;_w_stan_pogotowia"
  ]
  node [
    id 373
    label "pool"
  ]
  node [
    id 374
    label "pobudzi&#263;"
  ]
  node [
    id 375
    label "przygotowa&#263;"
  ]
  node [
    id 376
    label "zrezygnowanie"
  ]
  node [
    id 377
    label "wycofanie_si&#281;"
  ]
  node [
    id 378
    label "przestraszenie_si&#281;"
  ]
  node [
    id 379
    label "uciekni&#281;cie"
  ]
  node [
    id 380
    label "&#380;o&#322;nierz"
  ]
  node [
    id 381
    label "zniech&#281;ci&#263;"
  ]
  node [
    id 382
    label "zreorganizowa&#263;"
  ]
  node [
    id 383
    label "odprawi&#263;"
  ]
  node [
    id 384
    label "powo&#322;anie"
  ]
  node [
    id 385
    label "pobudzenie"
  ]
  node [
    id 386
    label "postawienie_"
  ]
  node [
    id 387
    label "zebranie_si&#322;"
  ]
  node [
    id 388
    label "przygotowanie"
  ]
  node [
    id 389
    label "nastawienie"
  ]
  node [
    id 390
    label "vivification"
  ]
  node [
    id 391
    label "powo&#322;ywanie"
  ]
  node [
    id 392
    label "vitalization"
  ]
  node [
    id 393
    label "usposabianie"
  ]
  node [
    id 394
    label "stawianie_"
  ]
  node [
    id 395
    label "pobudzanie"
  ]
  node [
    id 396
    label "przygotowywanie"
  ]
  node [
    id 397
    label "uciec"
  ]
  node [
    id 398
    label "stch&#243;rzy&#263;"
  ]
  node [
    id 399
    label "wycofa&#263;_si&#281;"
  ]
  node [
    id 400
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 401
    label "retreat"
  ]
  node [
    id 402
    label "ucieka&#263;"
  ]
  node [
    id 403
    label "rezygnowa&#263;"
  ]
  node [
    id 404
    label "po&#322;o&#380;enie"
  ]
  node [
    id 405
    label "debit"
  ]
  node [
    id 406
    label "druk"
  ]
  node [
    id 407
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 408
    label "szata_graficzna"
  ]
  node [
    id 409
    label "wydawa&#263;"
  ]
  node [
    id 410
    label "szermierka"
  ]
  node [
    id 411
    label "spis"
  ]
  node [
    id 412
    label "wyda&#263;"
  ]
  node [
    id 413
    label "ustawienie"
  ]
  node [
    id 414
    label "publikacja"
  ]
  node [
    id 415
    label "status"
  ]
  node [
    id 416
    label "miejsce"
  ]
  node [
    id 417
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 418
    label "adres"
  ]
  node [
    id 419
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 420
    label "rozmieszczenie"
  ]
  node [
    id 421
    label "sytuacja"
  ]
  node [
    id 422
    label "rz&#261;d"
  ]
  node [
    id 423
    label "redaktor"
  ]
  node [
    id 424
    label "awansowa&#263;"
  ]
  node [
    id 425
    label "bearing"
  ]
  node [
    id 426
    label "znaczenie"
  ]
  node [
    id 427
    label "awans"
  ]
  node [
    id 428
    label "awansowanie"
  ]
  node [
    id 429
    label "poster"
  ]
  node [
    id 430
    label "le&#380;e&#263;"
  ]
  node [
    id 431
    label "odstr&#281;czenie"
  ]
  node [
    id 432
    label "zreorganizowanie"
  ]
  node [
    id 433
    label "odprawienie"
  ]
  node [
    id 434
    label "zniech&#281;cenie_si&#281;"
  ]
  node [
    id 435
    label "Cygan"
  ]
  node [
    id 436
    label "dostawa"
  ]
  node [
    id 437
    label "transport"
  ]
  node [
    id 438
    label "ob&#243;z"
  ]
  node [
    id 439
    label "park"
  ]
  node [
    id 440
    label "odbywa&#263;"
  ]
  node [
    id 441
    label "treat"
  ]
  node [
    id 442
    label "robi&#263;"
  ]
  node [
    id 443
    label "serve"
  ]
  node [
    id 444
    label "kszta&#322;t"
  ]
  node [
    id 445
    label "pasemko"
  ]
  node [
    id 446
    label "znak_diakrytyczny"
  ]
  node [
    id 447
    label "zafalowanie"
  ]
  node [
    id 448
    label "kot"
  ]
  node [
    id 449
    label "strumie&#324;"
  ]
  node [
    id 450
    label "karb"
  ]
  node [
    id 451
    label "fit"
  ]
  node [
    id 452
    label "grzywa_fali"
  ]
  node [
    id 453
    label "woda"
  ]
  node [
    id 454
    label "efekt_Dopplera"
  ]
  node [
    id 455
    label "obcinka"
  ]
  node [
    id 456
    label "t&#322;um"
  ]
  node [
    id 457
    label "okres"
  ]
  node [
    id 458
    label "stream"
  ]
  node [
    id 459
    label "zafalowa&#263;"
  ]
  node [
    id 460
    label "rozbicie_si&#281;"
  ]
  node [
    id 461
    label "clutter"
  ]
  node [
    id 462
    label "rozbijanie_si&#281;"
  ]
  node [
    id 463
    label "czo&#322;o_fali"
  ]
  node [
    id 464
    label "pirotechnika"
  ]
  node [
    id 465
    label "cios"
  ]
  node [
    id 466
    label "sztuczne_ognie"
  ]
  node [
    id 467
    label "pocisk"
  ]
  node [
    id 468
    label "bomba"
  ]
  node [
    id 469
    label "cizia"
  ]
  node [
    id 470
    label "odjazd"
  ]
  node [
    id 471
    label "wyr&#243;b"
  ]
  node [
    id 472
    label "przest&#281;pca"
  ]
  node [
    id 473
    label "zdezerterowanie"
  ]
  node [
    id 474
    label "uciekinier"
  ]
  node [
    id 475
    label "kapitulant"
  ]
  node [
    id 476
    label "odreagowanie"
  ]
  node [
    id 477
    label "odreagowywanie"
  ]
  node [
    id 478
    label "odbywanie"
  ]
  node [
    id 479
    label "lekarz"
  ]
  node [
    id 480
    label "urz&#261;d"
  ]
  node [
    id 481
    label "organ"
  ]
  node [
    id 482
    label "&#321;ubianka"
  ]
  node [
    id 483
    label "miejsce_pracy"
  ]
  node [
    id 484
    label "dzia&#322;_personalny"
  ]
  node [
    id 485
    label "Kreml"
  ]
  node [
    id 486
    label "Bia&#322;y_Dom"
  ]
  node [
    id 487
    label "budynek"
  ]
  node [
    id 488
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 489
    label "sadowisko"
  ]
  node [
    id 490
    label "stanowisko"
  ]
  node [
    id 491
    label "position"
  ]
  node [
    id 492
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 493
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 494
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 495
    label "mianowaniec"
  ]
  node [
    id 496
    label "okienko"
  ]
  node [
    id 497
    label "w&#322;adza"
  ]
  node [
    id 498
    label "tkanka"
  ]
  node [
    id 499
    label "jednostka_organizacyjna"
  ]
  node [
    id 500
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 501
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 502
    label "tw&#243;r"
  ]
  node [
    id 503
    label "organogeneza"
  ]
  node [
    id 504
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 505
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 506
    label "dekortykacja"
  ]
  node [
    id 507
    label "Izba_Konsyliarska"
  ]
  node [
    id 508
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 509
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 510
    label "stomia"
  ]
  node [
    id 511
    label "budowa"
  ]
  node [
    id 512
    label "okolica"
  ]
  node [
    id 513
    label "Komitet_Region&#243;w"
  ]
  node [
    id 514
    label "komisariat"
  ]
  node [
    id 515
    label "posterunek"
  ]
  node [
    id 516
    label "psiarnia"
  ]
  node [
    id 517
    label "stawia&#263;"
  ]
  node [
    id 518
    label "wakowa&#263;"
  ]
  node [
    id 519
    label "powierzanie"
  ]
  node [
    id 520
    label "postawi&#263;"
  ]
  node [
    id 521
    label "warta"
  ]
  node [
    id 522
    label "odm&#322;adzanie"
  ]
  node [
    id 523
    label "jednostka_systematyczna"
  ]
  node [
    id 524
    label "asymilowanie"
  ]
  node [
    id 525
    label "gromada"
  ]
  node [
    id 526
    label "asymilowa&#263;"
  ]
  node [
    id 527
    label "egzemplarz"
  ]
  node [
    id 528
    label "Entuzjastki"
  ]
  node [
    id 529
    label "kompozycja"
  ]
  node [
    id 530
    label "Terranie"
  ]
  node [
    id 531
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 532
    label "category"
  ]
  node [
    id 533
    label "pakiet_klimatyczny"
  ]
  node [
    id 534
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 535
    label "cz&#261;steczka"
  ]
  node [
    id 536
    label "stage_set"
  ]
  node [
    id 537
    label "type"
  ]
  node [
    id 538
    label "specgrupa"
  ]
  node [
    id 539
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 540
    label "&#346;wietliki"
  ]
  node [
    id 541
    label "odm&#322;odzenie"
  ]
  node [
    id 542
    label "Eurogrupa"
  ]
  node [
    id 543
    label "odm&#322;adza&#263;"
  ]
  node [
    id 544
    label "formacja_geologiczna"
  ]
  node [
    id 545
    label "harcerze_starsi"
  ]
  node [
    id 546
    label "czasowy"
  ]
  node [
    id 547
    label "commissariat"
  ]
  node [
    id 548
    label "rewir"
  ]
  node [
    id 549
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 550
    label "umocowa&#263;"
  ]
  node [
    id 551
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 552
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 553
    label "procesualistyka"
  ]
  node [
    id 554
    label "regu&#322;a_Allena"
  ]
  node [
    id 555
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 556
    label "kryminalistyka"
  ]
  node [
    id 557
    label "kierunek"
  ]
  node [
    id 558
    label "zasada_d'Alemberta"
  ]
  node [
    id 559
    label "obserwacja"
  ]
  node [
    id 560
    label "normatywizm"
  ]
  node [
    id 561
    label "jurisprudence"
  ]
  node [
    id 562
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 563
    label "kultura_duchowa"
  ]
  node [
    id 564
    label "przepis"
  ]
  node [
    id 565
    label "prawo_karne_procesowe"
  ]
  node [
    id 566
    label "criterion"
  ]
  node [
    id 567
    label "kazuistyka"
  ]
  node [
    id 568
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 569
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 570
    label "kryminologia"
  ]
  node [
    id 571
    label "opis"
  ]
  node [
    id 572
    label "regu&#322;a_Glogera"
  ]
  node [
    id 573
    label "prawo_Mendla"
  ]
  node [
    id 574
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 575
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 576
    label "prawo_karne"
  ]
  node [
    id 577
    label "legislacyjnie"
  ]
  node [
    id 578
    label "twierdzenie"
  ]
  node [
    id 579
    label "cywilistyka"
  ]
  node [
    id 580
    label "judykatura"
  ]
  node [
    id 581
    label "kanonistyka"
  ]
  node [
    id 582
    label "standard"
  ]
  node [
    id 583
    label "nauka_prawa"
  ]
  node [
    id 584
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 585
    label "podmiot"
  ]
  node [
    id 586
    label "law"
  ]
  node [
    id 587
    label "qualification"
  ]
  node [
    id 588
    label "dominion"
  ]
  node [
    id 589
    label "wykonawczy"
  ]
  node [
    id 590
    label "zasada"
  ]
  node [
    id 591
    label "normalizacja"
  ]
  node [
    id 592
    label "wypowied&#378;"
  ]
  node [
    id 593
    label "exposition"
  ]
  node [
    id 594
    label "czynno&#347;&#263;"
  ]
  node [
    id 595
    label "obja&#347;nienie"
  ]
  node [
    id 596
    label "model"
  ]
  node [
    id 597
    label "organizowa&#263;"
  ]
  node [
    id 598
    label "ordinariness"
  ]
  node [
    id 599
    label "zorganizowa&#263;"
  ]
  node [
    id 600
    label "taniec_towarzyski"
  ]
  node [
    id 601
    label "organizowanie"
  ]
  node [
    id 602
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 603
    label "zorganizowanie"
  ]
  node [
    id 604
    label "przebieg"
  ]
  node [
    id 605
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 606
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 607
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 608
    label "przeorientowywanie"
  ]
  node [
    id 609
    label "studia"
  ]
  node [
    id 610
    label "linia"
  ]
  node [
    id 611
    label "skr&#281;canie"
  ]
  node [
    id 612
    label "skr&#281;ca&#263;"
  ]
  node [
    id 613
    label "przeorientowywa&#263;"
  ]
  node [
    id 614
    label "orientowanie"
  ]
  node [
    id 615
    label "skr&#281;ci&#263;"
  ]
  node [
    id 616
    label "przeorientowanie"
  ]
  node [
    id 617
    label "zorientowanie"
  ]
  node [
    id 618
    label "przeorientowa&#263;"
  ]
  node [
    id 619
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 620
    label "ty&#322;"
  ]
  node [
    id 621
    label "zorientowa&#263;"
  ]
  node [
    id 622
    label "g&#243;ra"
  ]
  node [
    id 623
    label "orientowa&#263;"
  ]
  node [
    id 624
    label "spos&#243;b"
  ]
  node [
    id 625
    label "orientacja"
  ]
  node [
    id 626
    label "prz&#243;d"
  ]
  node [
    id 627
    label "skr&#281;cenie"
  ]
  node [
    id 628
    label "posiada&#263;"
  ]
  node [
    id 629
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 630
    label "wydarzenie"
  ]
  node [
    id 631
    label "egzekutywa"
  ]
  node [
    id 632
    label "potencja&#322;"
  ]
  node [
    id 633
    label "wyb&#243;r"
  ]
  node [
    id 634
    label "prospect"
  ]
  node [
    id 635
    label "ability"
  ]
  node [
    id 636
    label "obliczeniowo"
  ]
  node [
    id 637
    label "alternatywa"
  ]
  node [
    id 638
    label "operator_modalny"
  ]
  node [
    id 639
    label "badanie"
  ]
  node [
    id 640
    label "proces_my&#347;lowy"
  ]
  node [
    id 641
    label "remark"
  ]
  node [
    id 642
    label "stwierdzenie"
  ]
  node [
    id 643
    label "observation"
  ]
  node [
    id 644
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 645
    label "alternatywa_Fredholma"
  ]
  node [
    id 646
    label "oznajmianie"
  ]
  node [
    id 647
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 648
    label "teoria"
  ]
  node [
    id 649
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 650
    label "paradoks_Leontiefa"
  ]
  node [
    id 651
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 652
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 653
    label "teza"
  ]
  node [
    id 654
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 655
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 656
    label "twierdzenie_Pettisa"
  ]
  node [
    id 657
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 658
    label "twierdzenie_Maya"
  ]
  node [
    id 659
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 660
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 661
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 662
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 663
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 664
    label "zapewnianie"
  ]
  node [
    id 665
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 666
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 667
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 668
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 669
    label "twierdzenie_Stokesa"
  ]
  node [
    id 670
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 671
    label "twierdzenie_Cevy"
  ]
  node [
    id 672
    label "twierdzenie_Pascala"
  ]
  node [
    id 673
    label "proposition"
  ]
  node [
    id 674
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 675
    label "komunikowanie"
  ]
  node [
    id 676
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 677
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 678
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 679
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 680
    label "relacja"
  ]
  node [
    id 681
    label "calibration"
  ]
  node [
    id 682
    label "operacja"
  ]
  node [
    id 683
    label "proces"
  ]
  node [
    id 684
    label "porz&#261;dek"
  ]
  node [
    id 685
    label "dominance"
  ]
  node [
    id 686
    label "zabieg"
  ]
  node [
    id 687
    label "standardization"
  ]
  node [
    id 688
    label "zmiana"
  ]
  node [
    id 689
    label "orzecznictwo"
  ]
  node [
    id 690
    label "wykonawczo"
  ]
  node [
    id 691
    label "osobowo&#347;&#263;"
  ]
  node [
    id 692
    label "organizacja"
  ]
  node [
    id 693
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 694
    label "set"
  ]
  node [
    id 695
    label "nada&#263;"
  ]
  node [
    id 696
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 697
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 698
    label "cook"
  ]
  node [
    id 699
    label "procedura"
  ]
  node [
    id 700
    label "norma_prawna"
  ]
  node [
    id 701
    label "przedawnienie_si&#281;"
  ]
  node [
    id 702
    label "przedawnianie_si&#281;"
  ]
  node [
    id 703
    label "porada"
  ]
  node [
    id 704
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 705
    label "regulation"
  ]
  node [
    id 706
    label "recepta"
  ]
  node [
    id 707
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 708
    label "kodeks"
  ]
  node [
    id 709
    label "base"
  ]
  node [
    id 710
    label "umowa"
  ]
  node [
    id 711
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 712
    label "moralno&#347;&#263;"
  ]
  node [
    id 713
    label "occupation"
  ]
  node [
    id 714
    label "podstawa"
  ]
  node [
    id 715
    label "substancja"
  ]
  node [
    id 716
    label "prawid&#322;o"
  ]
  node [
    id 717
    label "casuistry"
  ]
  node [
    id 718
    label "manipulacja"
  ]
  node [
    id 719
    label "probabilizm"
  ]
  node [
    id 720
    label "dermatoglifika"
  ]
  node [
    id 721
    label "mikro&#347;lad"
  ]
  node [
    id 722
    label "technika_&#347;ledcza"
  ]
  node [
    id 723
    label "plundering"
  ]
  node [
    id 724
    label "przest&#281;pstwo"
  ]
  node [
    id 725
    label "brudny"
  ]
  node [
    id 726
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 727
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 728
    label "crime"
  ]
  node [
    id 729
    label "sprawstwo"
  ]
  node [
    id 730
    label "przebiec"
  ]
  node [
    id 731
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 732
    label "motyw"
  ]
  node [
    id 733
    label "przebiegni&#281;cie"
  ]
  node [
    id 734
    label "fabu&#322;a"
  ]
  node [
    id 735
    label "osobowo"
  ]
  node [
    id 736
    label "nieoficjalny"
  ]
  node [
    id 737
    label "cywilnie"
  ]
  node [
    id 738
    label "nieoficjalnie"
  ]
  node [
    id 739
    label "nieformalny"
  ]
  node [
    id 740
    label "osy_w&#322;a&#347;ciwe"
  ]
  node [
    id 741
    label "osowate"
  ]
  node [
    id 742
    label "&#380;&#261;d&#322;&#243;wka"
  ]
  node [
    id 743
    label "b&#322;onk&#243;wka"
  ]
  node [
    id 744
    label "&#380;&#261;d&#322;&#243;wki"
  ]
  node [
    id 745
    label "osy"
  ]
  node [
    id 746
    label "pracownik"
  ]
  node [
    id 747
    label "fizykalnie"
  ]
  node [
    id 748
    label "materializowanie"
  ]
  node [
    id 749
    label "fizycznie"
  ]
  node [
    id 750
    label "namacalny"
  ]
  node [
    id 751
    label "widoczny"
  ]
  node [
    id 752
    label "zmaterializowanie"
  ]
  node [
    id 753
    label "organiczny"
  ]
  node [
    id 754
    label "materjalny"
  ]
  node [
    id 755
    label "gimnastyczny"
  ]
  node [
    id 756
    label "po_newtonowsku"
  ]
  node [
    id 757
    label "forcibly"
  ]
  node [
    id 758
    label "fizykalny"
  ]
  node [
    id 759
    label "physically"
  ]
  node [
    id 760
    label "namacalnie"
  ]
  node [
    id 761
    label "powodowanie"
  ]
  node [
    id 762
    label "wyjrzenie"
  ]
  node [
    id 763
    label "wygl&#261;danie"
  ]
  node [
    id 764
    label "widny"
  ]
  node [
    id 765
    label "widomy"
  ]
  node [
    id 766
    label "pojawianie_si&#281;"
  ]
  node [
    id 767
    label "widocznie"
  ]
  node [
    id 768
    label "wyra&#378;ny"
  ]
  node [
    id 769
    label "widzialny"
  ]
  node [
    id 770
    label "wystawienie_si&#281;"
  ]
  node [
    id 771
    label "widnienie"
  ]
  node [
    id 772
    label "ods&#322;oni&#281;cie"
  ]
  node [
    id 773
    label "ods&#322;anianie"
  ]
  node [
    id 774
    label "zarysowanie_si&#281;"
  ]
  node [
    id 775
    label "dostrzegalny"
  ]
  node [
    id 776
    label "wystawianie_si&#281;"
  ]
  node [
    id 777
    label "finansowy"
  ]
  node [
    id 778
    label "materialny"
  ]
  node [
    id 779
    label "naturalny"
  ]
  node [
    id 780
    label "nieodparty"
  ]
  node [
    id 781
    label "na&#347;ladowczy"
  ]
  node [
    id 782
    label "organicznie"
  ]
  node [
    id 783
    label "podobny"
  ]
  node [
    id 784
    label "trwa&#322;y"
  ]
  node [
    id 785
    label "salariat"
  ]
  node [
    id 786
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 787
    label "delegowanie"
  ]
  node [
    id 788
    label "pracu&#347;"
  ]
  node [
    id 789
    label "r&#281;ka"
  ]
  node [
    id 790
    label "delegowa&#263;"
  ]
  node [
    id 791
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 792
    label "postrzegalny"
  ]
  node [
    id 793
    label "konkretny"
  ]
  node [
    id 794
    label "wiarygodny"
  ]
  node [
    id 795
    label "kognicja"
  ]
  node [
    id 796
    label "object"
  ]
  node [
    id 797
    label "rozprawa"
  ]
  node [
    id 798
    label "temat"
  ]
  node [
    id 799
    label "szczeg&#243;&#322;"
  ]
  node [
    id 800
    label "przes&#322;anka"
  ]
  node [
    id 801
    label "idea"
  ]
  node [
    id 802
    label "intelekt"
  ]
  node [
    id 803
    label "Kant"
  ]
  node [
    id 804
    label "p&#322;&#243;d"
  ]
  node [
    id 805
    label "cel"
  ]
  node [
    id 806
    label "pomys&#322;"
  ]
  node [
    id 807
    label "ideacja"
  ]
  node [
    id 808
    label "wpadni&#281;cie"
  ]
  node [
    id 809
    label "mienie"
  ]
  node [
    id 810
    label "przyroda"
  ]
  node [
    id 811
    label "obiekt"
  ]
  node [
    id 812
    label "wpa&#347;&#263;"
  ]
  node [
    id 813
    label "wpadanie"
  ]
  node [
    id 814
    label "wpada&#263;"
  ]
  node [
    id 815
    label "rozumowanie"
  ]
  node [
    id 816
    label "opracowanie"
  ]
  node [
    id 817
    label "obrady"
  ]
  node [
    id 818
    label "cytat"
  ]
  node [
    id 819
    label "tekst"
  ]
  node [
    id 820
    label "s&#261;dzenie"
  ]
  node [
    id 821
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 822
    label "niuansowa&#263;"
  ]
  node [
    id 823
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 824
    label "sk&#322;adnik"
  ]
  node [
    id 825
    label "zniuansowa&#263;"
  ]
  node [
    id 826
    label "fakt"
  ]
  node [
    id 827
    label "przyczyna"
  ]
  node [
    id 828
    label "wnioskowanie"
  ]
  node [
    id 829
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 830
    label "wyraz_pochodny"
  ]
  node [
    id 831
    label "fraza"
  ]
  node [
    id 832
    label "forum"
  ]
  node [
    id 833
    label "topik"
  ]
  node [
    id 834
    label "forma"
  ]
  node [
    id 835
    label "melodia"
  ]
  node [
    id 836
    label "otoczka"
  ]
  node [
    id 837
    label "so&#322;dat"
  ]
  node [
    id 838
    label "rota"
  ]
  node [
    id 839
    label "militarnie"
  ]
  node [
    id 840
    label "elew"
  ]
  node [
    id 841
    label "wojskowo"
  ]
  node [
    id 842
    label "Gurkha"
  ]
  node [
    id 843
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 844
    label "walcz&#261;cy"
  ]
  node [
    id 845
    label "&#380;o&#322;dowy"
  ]
  node [
    id 846
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 847
    label "antybalistyczny"
  ]
  node [
    id 848
    label "demobilizowa&#263;"
  ]
  node [
    id 849
    label "podleg&#322;y"
  ]
  node [
    id 850
    label "specjalny"
  ]
  node [
    id 851
    label "harcap"
  ]
  node [
    id 852
    label "demobilizowanie"
  ]
  node [
    id 853
    label "typowy"
  ]
  node [
    id 854
    label "mundurowy"
  ]
  node [
    id 855
    label "intencjonalny"
  ]
  node [
    id 856
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 857
    label "niedorozw&#243;j"
  ]
  node [
    id 858
    label "szczeg&#243;lny"
  ]
  node [
    id 859
    label "specjalnie"
  ]
  node [
    id 860
    label "nieetatowy"
  ]
  node [
    id 861
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 862
    label "nienormalny"
  ]
  node [
    id 863
    label "umy&#347;lnie"
  ]
  node [
    id 864
    label "odpowiedni"
  ]
  node [
    id 865
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 866
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 867
    label "zwyczajny"
  ]
  node [
    id 868
    label "typowo"
  ]
  node [
    id 869
    label "cz&#281;sty"
  ]
  node [
    id 870
    label "zwyk&#322;y"
  ]
  node [
    id 871
    label "zale&#380;ny"
  ]
  node [
    id 872
    label "podlegle"
  ]
  node [
    id 873
    label "ludzko&#347;&#263;"
  ]
  node [
    id 874
    label "wapniak"
  ]
  node [
    id 875
    label "os&#322;abia&#263;"
  ]
  node [
    id 876
    label "posta&#263;"
  ]
  node [
    id 877
    label "hominid"
  ]
  node [
    id 878
    label "podw&#322;adny"
  ]
  node [
    id 879
    label "os&#322;abianie"
  ]
  node [
    id 880
    label "g&#322;owa"
  ]
  node [
    id 881
    label "figura"
  ]
  node [
    id 882
    label "portrecista"
  ]
  node [
    id 883
    label "dwun&#243;g"
  ]
  node [
    id 884
    label "profanum"
  ]
  node [
    id 885
    label "mikrokosmos"
  ]
  node [
    id 886
    label "nasada"
  ]
  node [
    id 887
    label "duch"
  ]
  node [
    id 888
    label "antropochoria"
  ]
  node [
    id 889
    label "osoba"
  ]
  node [
    id 890
    label "wz&#243;r"
  ]
  node [
    id 891
    label "senior"
  ]
  node [
    id 892
    label "oddzia&#322;ywanie"
  ]
  node [
    id 893
    label "Adam"
  ]
  node [
    id 894
    label "homo_sapiens"
  ]
  node [
    id 895
    label "polifag"
  ]
  node [
    id 896
    label "militarny"
  ]
  node [
    id 897
    label "antyrakietowy"
  ]
  node [
    id 898
    label "wojownik"
  ]
  node [
    id 899
    label "Nepal"
  ]
  node [
    id 900
    label "zwalnianie"
  ]
  node [
    id 901
    label "zniech&#281;canie"
  ]
  node [
    id 902
    label "przebudowywanie"
  ]
  node [
    id 903
    label "zniech&#281;canie_si&#281;"
  ]
  node [
    id 904
    label "zniech&#281;ca&#263;"
  ]
  node [
    id 905
    label "zwalnia&#263;"
  ]
  node [
    id 906
    label "przebudowywa&#263;"
  ]
  node [
    id 907
    label "funkcjonariusz"
  ]
  node [
    id 908
    label "nosiciel"
  ]
  node [
    id 909
    label "ucze&#324;"
  ]
  node [
    id 910
    label "peruka"
  ]
  node [
    id 911
    label "warkocz"
  ]
  node [
    id 912
    label "piecz&#261;tka"
  ]
  node [
    id 913
    label "s&#261;d_ko&#347;cielny"
  ]
  node [
    id 914
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 915
    label "&#322;ama&#263;"
  ]
  node [
    id 916
    label "stra&#380;_po&#380;arna"
  ]
  node [
    id 917
    label "tortury"
  ]
  node [
    id 918
    label "papie&#380;"
  ]
  node [
    id 919
    label "chordofon_szarpany"
  ]
  node [
    id 920
    label "przysi&#281;ga"
  ]
  node [
    id 921
    label "&#322;amanie"
  ]
  node [
    id 922
    label "szyk"
  ]
  node [
    id 923
    label "s&#261;d_apelacyjny"
  ]
  node [
    id 924
    label "whip"
  ]
  node [
    id 925
    label "Rota"
  ]
  node [
    id 926
    label "instrument_strunowy"
  ]
  node [
    id 927
    label "formu&#322;a"
  ]
  node [
    id 928
    label "zaw&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 239
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 327
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 305
  ]
  edge [
    source 15
    target 246
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 245
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 311
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 234
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 225
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 231
  ]
  edge [
    source 15
    target 235
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 237
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 240
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 242
  ]
  edge [
    source 15
    target 243
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 247
  ]
  edge [
    source 15
    target 248
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 102
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 281
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 524
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 380
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 431
  ]
  edge [
    source 18
    target 432
  ]
  edge [
    source 18
    target 433
  ]
  edge [
    source 18
    target 434
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 381
  ]
  edge [
    source 18
    target 382
  ]
  edge [
    source 18
    target 383
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 117
  ]
  edge [
    source 18
    target 118
  ]
  edge [
    source 18
    target 119
  ]
  edge [
    source 18
    target 120
  ]
  edge [
    source 18
    target 121
  ]
  edge [
    source 18
    target 122
  ]
  edge [
    source 18
    target 123
  ]
  edge [
    source 18
    target 124
  ]
  edge [
    source 18
    target 125
  ]
  edge [
    source 18
    target 126
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 18
    target 130
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 928
  ]
]
