graph [
  node [
    id 0
    label "media"
    origin "text"
  ]
  node [
    id 1
    label "spo&#322;eczno&#347;ciowy"
    origin "text"
  ]
  node [
    id 2
    label "zamieszcza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "apel"
    origin "text"
  ]
  node [
    id 4
    label "policja"
    origin "text"
  ]
  node [
    id 5
    label "zaprzestanie"
    origin "text"
  ]
  node [
    id 6
    label "strzelanie"
    origin "text"
  ]
  node [
    id 7
    label "czarna"
    origin "text"
  ]
  node [
    id 8
    label "mass-media"
  ]
  node [
    id 9
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 10
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 11
    label "przekazior"
  ]
  node [
    id 12
    label "uzbrajanie"
  ]
  node [
    id 13
    label "medium"
  ]
  node [
    id 14
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 15
    label "&#347;rodek"
  ]
  node [
    id 16
    label "jasnowidz"
  ]
  node [
    id 17
    label "hipnoza"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "spirytysta"
  ]
  node [
    id 20
    label "otoczenie"
  ]
  node [
    id 21
    label "publikator"
  ]
  node [
    id 22
    label "warunki"
  ]
  node [
    id 23
    label "strona"
  ]
  node [
    id 24
    label "przeka&#378;nik"
  ]
  node [
    id 25
    label "&#347;rodek_przekazu"
  ]
  node [
    id 26
    label "armament"
  ]
  node [
    id 27
    label "arming"
  ]
  node [
    id 28
    label "instalacja"
  ]
  node [
    id 29
    label "wyposa&#380;anie"
  ]
  node [
    id 30
    label "dozbrajanie"
  ]
  node [
    id 31
    label "dozbrojenie"
  ]
  node [
    id 32
    label "montowanie"
  ]
  node [
    id 33
    label "medialny"
  ]
  node [
    id 34
    label "publiczny"
  ]
  node [
    id 35
    label "upublicznianie"
  ]
  node [
    id 36
    label "jawny"
  ]
  node [
    id 37
    label "upublicznienie"
  ]
  node [
    id 38
    label "publicznie"
  ]
  node [
    id 39
    label "popularny"
  ]
  node [
    id 40
    label "&#347;rodkowy"
  ]
  node [
    id 41
    label "medialnie"
  ]
  node [
    id 42
    label "nieprawdziwy"
  ]
  node [
    id 43
    label "publikowa&#263;"
  ]
  node [
    id 44
    label "umieszcza&#263;"
  ]
  node [
    id 45
    label "upublicznia&#263;"
  ]
  node [
    id 46
    label "give"
  ]
  node [
    id 47
    label "wydawnictwo"
  ]
  node [
    id 48
    label "wprowadza&#263;"
  ]
  node [
    id 49
    label "plasowa&#263;"
  ]
  node [
    id 50
    label "umie&#347;ci&#263;"
  ]
  node [
    id 51
    label "robi&#263;"
  ]
  node [
    id 52
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 53
    label "pomieszcza&#263;"
  ]
  node [
    id 54
    label "accommodate"
  ]
  node [
    id 55
    label "zmienia&#263;"
  ]
  node [
    id 56
    label "powodowa&#263;"
  ]
  node [
    id 57
    label "venture"
  ]
  node [
    id 58
    label "wpiernicza&#263;"
  ]
  node [
    id 59
    label "okre&#347;la&#263;"
  ]
  node [
    id 60
    label "pro&#347;ba"
  ]
  node [
    id 61
    label "znak"
  ]
  node [
    id 62
    label "zdyscyplinowanie"
  ]
  node [
    id 63
    label "recoil"
  ]
  node [
    id 64
    label "spotkanie"
  ]
  node [
    id 65
    label "uderzenie"
  ]
  node [
    id 66
    label "pies_my&#347;liwski"
  ]
  node [
    id 67
    label "bid"
  ]
  node [
    id 68
    label "sygna&#322;"
  ]
  node [
    id 69
    label "szermierka"
  ]
  node [
    id 70
    label "zbi&#243;rka"
  ]
  node [
    id 71
    label "doznanie"
  ]
  node [
    id 72
    label "gathering"
  ]
  node [
    id 73
    label "zawarcie"
  ]
  node [
    id 74
    label "wydarzenie"
  ]
  node [
    id 75
    label "znajomy"
  ]
  node [
    id 76
    label "powitanie"
  ]
  node [
    id 77
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 78
    label "spowodowanie"
  ]
  node [
    id 79
    label "zdarzenie_si&#281;"
  ]
  node [
    id 80
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 81
    label "znalezienie"
  ]
  node [
    id 82
    label "match"
  ]
  node [
    id 83
    label "employment"
  ]
  node [
    id 84
    label "po&#380;egnanie"
  ]
  node [
    id 85
    label "gather"
  ]
  node [
    id 86
    label "spotykanie"
  ]
  node [
    id 87
    label "spotkanie_si&#281;"
  ]
  node [
    id 88
    label "instrumentalizacja"
  ]
  node [
    id 89
    label "trafienie"
  ]
  node [
    id 90
    label "walka"
  ]
  node [
    id 91
    label "cios"
  ]
  node [
    id 92
    label "wdarcie_si&#281;"
  ]
  node [
    id 93
    label "pogorszenie"
  ]
  node [
    id 94
    label "d&#378;wi&#281;k"
  ]
  node [
    id 95
    label "poczucie"
  ]
  node [
    id 96
    label "coup"
  ]
  node [
    id 97
    label "reakcja"
  ]
  node [
    id 98
    label "contact"
  ]
  node [
    id 99
    label "stukni&#281;cie"
  ]
  node [
    id 100
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 101
    label "bat"
  ]
  node [
    id 102
    label "rush"
  ]
  node [
    id 103
    label "odbicie"
  ]
  node [
    id 104
    label "dawka"
  ]
  node [
    id 105
    label "zadanie"
  ]
  node [
    id 106
    label "&#347;ci&#281;cie"
  ]
  node [
    id 107
    label "st&#322;uczenie"
  ]
  node [
    id 108
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 109
    label "time"
  ]
  node [
    id 110
    label "odbicie_si&#281;"
  ]
  node [
    id 111
    label "dotkni&#281;cie"
  ]
  node [
    id 112
    label "charge"
  ]
  node [
    id 113
    label "dostanie"
  ]
  node [
    id 114
    label "skrytykowanie"
  ]
  node [
    id 115
    label "zagrywka"
  ]
  node [
    id 116
    label "manewr"
  ]
  node [
    id 117
    label "nast&#261;pienie"
  ]
  node [
    id 118
    label "uderzanie"
  ]
  node [
    id 119
    label "pogoda"
  ]
  node [
    id 120
    label "stroke"
  ]
  node [
    id 121
    label "pobicie"
  ]
  node [
    id 122
    label "ruch"
  ]
  node [
    id 123
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 124
    label "flap"
  ]
  node [
    id 125
    label "dotyk"
  ]
  node [
    id 126
    label "zrobienie"
  ]
  node [
    id 127
    label "podporz&#261;dkowanie"
  ]
  node [
    id 128
    label "zachowanie"
  ]
  node [
    id 129
    label "porz&#261;dek"
  ]
  node [
    id 130
    label "zachowywanie"
  ]
  node [
    id 131
    label "mores"
  ]
  node [
    id 132
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 133
    label "zachowa&#263;"
  ]
  node [
    id 134
    label "zachowywa&#263;"
  ]
  node [
    id 135
    label "nauczenie"
  ]
  node [
    id 136
    label "wypowied&#378;"
  ]
  node [
    id 137
    label "solicitation"
  ]
  node [
    id 138
    label "dow&#243;d"
  ]
  node [
    id 139
    label "oznakowanie"
  ]
  node [
    id 140
    label "fakt"
  ]
  node [
    id 141
    label "stawia&#263;"
  ]
  node [
    id 142
    label "wytw&#243;r"
  ]
  node [
    id 143
    label "point"
  ]
  node [
    id 144
    label "kodzik"
  ]
  node [
    id 145
    label "postawi&#263;"
  ]
  node [
    id 146
    label "mark"
  ]
  node [
    id 147
    label "herb"
  ]
  node [
    id 148
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 149
    label "attribute"
  ]
  node [
    id 150
    label "implikowa&#263;"
  ]
  node [
    id 151
    label "przekazywa&#263;"
  ]
  node [
    id 152
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 153
    label "pulsation"
  ]
  node [
    id 154
    label "przekazywanie"
  ]
  node [
    id 155
    label "przewodzenie"
  ]
  node [
    id 156
    label "po&#322;&#261;czenie"
  ]
  node [
    id 157
    label "fala"
  ]
  node [
    id 158
    label "doj&#347;cie"
  ]
  node [
    id 159
    label "przekazanie"
  ]
  node [
    id 160
    label "przewodzi&#263;"
  ]
  node [
    id 161
    label "zapowied&#378;"
  ]
  node [
    id 162
    label "medium_transmisyjne"
  ]
  node [
    id 163
    label "demodulacja"
  ]
  node [
    id 164
    label "doj&#347;&#263;"
  ]
  node [
    id 165
    label "przekaza&#263;"
  ]
  node [
    id 166
    label "czynnik"
  ]
  node [
    id 167
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 168
    label "aliasing"
  ]
  node [
    id 169
    label "wizja"
  ]
  node [
    id 170
    label "modulacja"
  ]
  node [
    id 171
    label "drift"
  ]
  node [
    id 172
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 173
    label "plansza"
  ]
  node [
    id 174
    label "wi&#261;zanie"
  ]
  node [
    id 175
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 176
    label "ripostowa&#263;"
  ]
  node [
    id 177
    label "czerwona_kartka"
  ]
  node [
    id 178
    label "czarna_kartka"
  ]
  node [
    id 179
    label "pozycja"
  ]
  node [
    id 180
    label "fight"
  ]
  node [
    id 181
    label "sztuka"
  ]
  node [
    id 182
    label "sport_walki"
  ]
  node [
    id 183
    label "manszeta"
  ]
  node [
    id 184
    label "ripostowanie"
  ]
  node [
    id 185
    label "tusz"
  ]
  node [
    id 186
    label "kwestarz"
  ]
  node [
    id 187
    label "kwestowanie"
  ]
  node [
    id 188
    label "collection"
  ]
  node [
    id 189
    label "koszyk&#243;wka"
  ]
  node [
    id 190
    label "chwyt"
  ]
  node [
    id 191
    label "czynno&#347;&#263;"
  ]
  node [
    id 192
    label "organ"
  ]
  node [
    id 193
    label "grupa"
  ]
  node [
    id 194
    label "komisariat"
  ]
  node [
    id 195
    label "s&#322;u&#380;ba"
  ]
  node [
    id 196
    label "posterunek"
  ]
  node [
    id 197
    label "psiarnia"
  ]
  node [
    id 198
    label "awansowa&#263;"
  ]
  node [
    id 199
    label "wakowa&#263;"
  ]
  node [
    id 200
    label "powierzanie"
  ]
  node [
    id 201
    label "agencja"
  ]
  node [
    id 202
    label "awansowanie"
  ]
  node [
    id 203
    label "warta"
  ]
  node [
    id 204
    label "praca"
  ]
  node [
    id 205
    label "tkanka"
  ]
  node [
    id 206
    label "jednostka_organizacyjna"
  ]
  node [
    id 207
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 208
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 209
    label "tw&#243;r"
  ]
  node [
    id 210
    label "organogeneza"
  ]
  node [
    id 211
    label "zesp&#243;&#322;"
  ]
  node [
    id 212
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 213
    label "struktura_anatomiczna"
  ]
  node [
    id 214
    label "uk&#322;ad"
  ]
  node [
    id 215
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 216
    label "dekortykacja"
  ]
  node [
    id 217
    label "Izba_Konsyliarska"
  ]
  node [
    id 218
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 219
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 220
    label "stomia"
  ]
  node [
    id 221
    label "budowa"
  ]
  node [
    id 222
    label "okolica"
  ]
  node [
    id 223
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 224
    label "Komitet_Region&#243;w"
  ]
  node [
    id 225
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 226
    label "instytucja"
  ]
  node [
    id 227
    label "wys&#322;uga"
  ]
  node [
    id 228
    label "service"
  ]
  node [
    id 229
    label "czworak"
  ]
  node [
    id 230
    label "ZOMO"
  ]
  node [
    id 231
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 232
    label "odm&#322;adzanie"
  ]
  node [
    id 233
    label "liga"
  ]
  node [
    id 234
    label "jednostka_systematyczna"
  ]
  node [
    id 235
    label "asymilowanie"
  ]
  node [
    id 236
    label "gromada"
  ]
  node [
    id 237
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 238
    label "asymilowa&#263;"
  ]
  node [
    id 239
    label "egzemplarz"
  ]
  node [
    id 240
    label "Entuzjastki"
  ]
  node [
    id 241
    label "zbi&#243;r"
  ]
  node [
    id 242
    label "kompozycja"
  ]
  node [
    id 243
    label "Terranie"
  ]
  node [
    id 244
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 245
    label "category"
  ]
  node [
    id 246
    label "pakiet_klimatyczny"
  ]
  node [
    id 247
    label "oddzia&#322;"
  ]
  node [
    id 248
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 249
    label "cz&#261;steczka"
  ]
  node [
    id 250
    label "stage_set"
  ]
  node [
    id 251
    label "type"
  ]
  node [
    id 252
    label "specgrupa"
  ]
  node [
    id 253
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 254
    label "&#346;wietliki"
  ]
  node [
    id 255
    label "odm&#322;odzenie"
  ]
  node [
    id 256
    label "Eurogrupa"
  ]
  node [
    id 257
    label "odm&#322;adza&#263;"
  ]
  node [
    id 258
    label "formacja_geologiczna"
  ]
  node [
    id 259
    label "harcerze_starsi"
  ]
  node [
    id 260
    label "urz&#261;d"
  ]
  node [
    id 261
    label "jednostka"
  ]
  node [
    id 262
    label "czasowy"
  ]
  node [
    id 263
    label "commissariat"
  ]
  node [
    id 264
    label "rewir"
  ]
  node [
    id 265
    label "oduczenie"
  ]
  node [
    id 266
    label "zako&#324;czenie"
  ]
  node [
    id 267
    label "disavowal"
  ]
  node [
    id 268
    label "dzia&#322;anie"
  ]
  node [
    id 269
    label "closing"
  ]
  node [
    id 270
    label "termination"
  ]
  node [
    id 271
    label "zrezygnowanie"
  ]
  node [
    id 272
    label "closure"
  ]
  node [
    id 273
    label "ukszta&#322;towanie"
  ]
  node [
    id 274
    label "conclusion"
  ]
  node [
    id 275
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 276
    label "koniec"
  ]
  node [
    id 277
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 278
    label "adjustment"
  ]
  node [
    id 279
    label "oddzia&#322;anie"
  ]
  node [
    id 280
    label "przestanie"
  ]
  node [
    id 281
    label "fire"
  ]
  node [
    id 282
    label "zestrzeliwanie"
  ]
  node [
    id 283
    label "odstrzeliwanie"
  ]
  node [
    id 284
    label "zestrzelenie"
  ]
  node [
    id 285
    label "wystrzelanie"
  ]
  node [
    id 286
    label "wylatywanie"
  ]
  node [
    id 287
    label "chybianie"
  ]
  node [
    id 288
    label "przestrzeliwanie"
  ]
  node [
    id 289
    label "walczenie"
  ]
  node [
    id 290
    label "ostrzelanie"
  ]
  node [
    id 291
    label "ostrzeliwanie"
  ]
  node [
    id 292
    label "trafianie"
  ]
  node [
    id 293
    label "odpalanie"
  ]
  node [
    id 294
    label "odstrzelenie"
  ]
  node [
    id 295
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 296
    label "postrzelanie"
  ]
  node [
    id 297
    label "chybienie"
  ]
  node [
    id 298
    label "grzanie"
  ]
  node [
    id 299
    label "palenie"
  ]
  node [
    id 300
    label "kropni&#281;cie"
  ]
  node [
    id 301
    label "prze&#322;adowywanie"
  ]
  node [
    id 302
    label "plucie"
  ]
  node [
    id 303
    label "przygotowanie"
  ]
  node [
    id 304
    label "zaatakowanie"
  ]
  node [
    id 305
    label "przygotowywanie"
  ]
  node [
    id 306
    label "spr&#243;bowanie"
  ]
  node [
    id 307
    label "opierzenie"
  ]
  node [
    id 308
    label "miss"
  ]
  node [
    id 309
    label "zranienie"
  ]
  node [
    id 310
    label "uszkodzenie"
  ]
  node [
    id 311
    label "porobienie"
  ]
  node [
    id 312
    label "od&#322;upanie"
  ]
  node [
    id 313
    label "urwanie"
  ]
  node [
    id 314
    label "zabicie"
  ]
  node [
    id 315
    label "od&#322;upywanie"
  ]
  node [
    id 316
    label "g&#243;rnictwo"
  ]
  node [
    id 317
    label "urywanie"
  ]
  node [
    id 318
    label "zabijanie"
  ]
  node [
    id 319
    label "pr&#243;bowanie"
  ]
  node [
    id 320
    label "dziurawienie"
  ]
  node [
    id 321
    label "opierzanie"
  ]
  node [
    id 322
    label "dzianie_si&#281;"
  ]
  node [
    id 323
    label "dosi&#281;ganie"
  ]
  node [
    id 324
    label "dopasowywanie_si&#281;"
  ]
  node [
    id 325
    label "zjawianie_si&#281;"
  ]
  node [
    id 326
    label "wpadanie"
  ]
  node [
    id 327
    label "pojawianie_si&#281;"
  ]
  node [
    id 328
    label "dostawanie"
  ]
  node [
    id 329
    label "pocisk"
  ]
  node [
    id 330
    label "docieranie"
  ]
  node [
    id 331
    label "aim"
  ]
  node [
    id 332
    label "dolatywanie"
  ]
  node [
    id 333
    label "znajdowanie"
  ]
  node [
    id 334
    label "dostawanie_si&#281;"
  ]
  node [
    id 335
    label "meeting"
  ]
  node [
    id 336
    label "ukaranie"
  ]
  node [
    id 337
    label "wypowiedzenie"
  ]
  node [
    id 338
    label "prze&#322;adowanie"
  ]
  node [
    id 339
    label "napisanie"
  ]
  node [
    id 340
    label "odpalenie"
  ]
  node [
    id 341
    label "palni&#281;cie"
  ]
  node [
    id 342
    label "wypicie"
  ]
  node [
    id 343
    label "punch"
  ]
  node [
    id 344
    label "spadni&#281;cie"
  ]
  node [
    id 345
    label "wylecenie"
  ]
  node [
    id 346
    label "odbezpieczenie"
  ]
  node [
    id 347
    label "wybuchni&#281;cie"
  ]
  node [
    id 348
    label "pluni&#281;cie"
  ]
  node [
    id 349
    label "dolecenie"
  ]
  node [
    id 350
    label "rzucenie"
  ]
  node [
    id 351
    label "shooting"
  ]
  node [
    id 352
    label "postrzelenie"
  ]
  node [
    id 353
    label "skupienie"
  ]
  node [
    id 354
    label "zrzucenie"
  ]
  node [
    id 355
    label "precipitation"
  ]
  node [
    id 356
    label "zrzucanie"
  ]
  node [
    id 357
    label "skupianie"
  ]
  node [
    id 358
    label "atakowanie"
  ]
  node [
    id 359
    label "sprzeciwianie_si&#281;"
  ]
  node [
    id 360
    label "obstawanie"
  ]
  node [
    id 361
    label "adwokatowanie"
  ]
  node [
    id 362
    label "opieranie_si&#281;"
  ]
  node [
    id 363
    label "powalczenie"
  ]
  node [
    id 364
    label "staranie_si&#281;"
  ]
  node [
    id 365
    label "rywalizowanie"
  ]
  node [
    id 366
    label "my&#347;lenie"
  ]
  node [
    id 367
    label "bojownik"
  ]
  node [
    id 368
    label "staczanie"
  ]
  node [
    id 369
    label "robienie"
  ]
  node [
    id 370
    label "oblegni&#281;cie"
  ]
  node [
    id 371
    label "zwalczenie"
  ]
  node [
    id 372
    label "usi&#322;owanie"
  ]
  node [
    id 373
    label "struggle"
  ]
  node [
    id 374
    label "t&#322;umaczenie"
  ]
  node [
    id 375
    label "obleganie"
  ]
  node [
    id 376
    label "playing"
  ]
  node [
    id 377
    label "ust&#281;powanie"
  ]
  node [
    id 378
    label "zawody"
  ]
  node [
    id 379
    label "zwojowanie"
  ]
  node [
    id 380
    label "wywalczenie"
  ]
  node [
    id 381
    label "nawojowanie_si&#281;"
  ]
  node [
    id 382
    label "or&#281;dowanie"
  ]
  node [
    id 383
    label "powodowanie"
  ]
  node [
    id 384
    label "zwracanie_uwagi"
  ]
  node [
    id 385
    label "&#347;cinanie"
  ]
  node [
    id 386
    label "stukanie"
  ]
  node [
    id 387
    label "grasowanie"
  ]
  node [
    id 388
    label "napadanie"
  ]
  node [
    id 389
    label "judgment"
  ]
  node [
    id 390
    label "taranowanie"
  ]
  node [
    id 391
    label "pouderzanie"
  ]
  node [
    id 392
    label "t&#322;uczenie"
  ]
  node [
    id 393
    label "skontrowanie"
  ]
  node [
    id 394
    label "haratanie"
  ]
  node [
    id 395
    label "bicie"
  ]
  node [
    id 396
    label "kontrowanie"
  ]
  node [
    id 397
    label "dotykanie"
  ]
  node [
    id 398
    label "zwracanie_si&#281;"
  ]
  node [
    id 399
    label "pobijanie"
  ]
  node [
    id 400
    label "staranowanie"
  ]
  node [
    id 401
    label "wytrzepanie"
  ]
  node [
    id 402
    label "trzepanie"
  ]
  node [
    id 403
    label "zamachiwanie_si&#281;"
  ]
  node [
    id 404
    label "krytykowanie"
  ]
  node [
    id 405
    label "torpedowanie"
  ]
  node [
    id 406
    label "friction"
  ]
  node [
    id 407
    label "nast&#281;powanie"
  ]
  node [
    id 408
    label "odbijanie"
  ]
  node [
    id 409
    label "knock"
  ]
  node [
    id 410
    label "zadawanie"
  ]
  node [
    id 411
    label "rozkwaszenie"
  ]
  node [
    id 412
    label "dopalenie"
  ]
  node [
    id 413
    label "burning"
  ]
  node [
    id 414
    label "na&#322;&#243;g"
  ]
  node [
    id 415
    label "emergency"
  ]
  node [
    id 416
    label "burn"
  ]
  node [
    id 417
    label "dokuczanie"
  ]
  node [
    id 418
    label "cygaro"
  ]
  node [
    id 419
    label "napalenie"
  ]
  node [
    id 420
    label "podpalanie"
  ]
  node [
    id 421
    label "rozpalanie"
  ]
  node [
    id 422
    label "pykni&#281;cie"
  ]
  node [
    id 423
    label "zu&#380;ywanie"
  ]
  node [
    id 424
    label "wypalanie"
  ]
  node [
    id 425
    label "fajka"
  ]
  node [
    id 426
    label "podtrzymywanie"
  ]
  node [
    id 427
    label "papieros"
  ]
  node [
    id 428
    label "dra&#380;nienie"
  ]
  node [
    id 429
    label "kadzenie"
  ]
  node [
    id 430
    label "wypalenie"
  ]
  node [
    id 431
    label "ogie&#324;"
  ]
  node [
    id 432
    label "dowcip"
  ]
  node [
    id 433
    label "popalenie"
  ]
  node [
    id 434
    label "niszczenie"
  ]
  node [
    id 435
    label "paliwo"
  ]
  node [
    id 436
    label "bolenie"
  ]
  node [
    id 437
    label "palenie_si&#281;"
  ]
  node [
    id 438
    label "incineration"
  ]
  node [
    id 439
    label "psucie"
  ]
  node [
    id 440
    label "jaranie"
  ]
  node [
    id 441
    label "wyplucie"
  ]
  node [
    id 442
    label "wypluwanie"
  ]
  node [
    id 443
    label "spit"
  ]
  node [
    id 444
    label "lekcewa&#380;enie"
  ]
  node [
    id 445
    label "opluwanie"
  ]
  node [
    id 446
    label "wydzielanie"
  ]
  node [
    id 447
    label "rozdzielanie"
  ]
  node [
    id 448
    label "dawanie"
  ]
  node [
    id 449
    label "detonowanie"
  ]
  node [
    id 450
    label "zapalanie"
  ]
  node [
    id 451
    label "ignition"
  ]
  node [
    id 452
    label "roz&#380;arzanie"
  ]
  node [
    id 453
    label "odpowiadanie"
  ]
  node [
    id 454
    label "gassing"
  ]
  node [
    id 455
    label "fracture"
  ]
  node [
    id 456
    label "roztapianie"
  ]
  node [
    id 457
    label "podnoszenie"
  ]
  node [
    id 458
    label "odpuszczanie"
  ]
  node [
    id 459
    label "wypra&#380;anie"
  ]
  node [
    id 460
    label "zw&#281;glanie"
  ]
  node [
    id 461
    label "zw&#281;glenie"
  ]
  node [
    id 462
    label "spieczenie"
  ]
  node [
    id 463
    label "picie"
  ]
  node [
    id 464
    label "narkotyzowanie_si&#281;"
  ]
  node [
    id 465
    label "zat&#322;uczenie"
  ]
  node [
    id 466
    label "wypra&#380;enie"
  ]
  node [
    id 467
    label "spiekanie"
  ]
  node [
    id 468
    label "pobudzanie"
  ]
  node [
    id 469
    label "rozgrzewanie_si&#281;"
  ]
  node [
    id 470
    label "odwadnianie"
  ]
  node [
    id 471
    label "ciep&#322;y"
  ]
  node [
    id 472
    label "gnanie"
  ]
  node [
    id 473
    label "heating"
  ]
  node [
    id 474
    label "umieszczanie"
  ]
  node [
    id 475
    label "przesadzanie"
  ]
  node [
    id 476
    label "przemieszczanie"
  ]
  node [
    id 477
    label "przeci&#261;&#380;anie"
  ]
  node [
    id 478
    label "&#322;adowanie"
  ]
  node [
    id 479
    label "wystrzeliwanie"
  ]
  node [
    id 480
    label "wznoszenie_si&#281;"
  ]
  node [
    id 481
    label "wybieganie"
  ]
  node [
    id 482
    label "wydostawanie_si&#281;"
  ]
  node [
    id 483
    label "katapultowanie"
  ]
  node [
    id 484
    label "samolot"
  ]
  node [
    id 485
    label "opuszczanie"
  ]
  node [
    id 486
    label "odchodzenie"
  ]
  node [
    id 487
    label "wybijanie"
  ]
  node [
    id 488
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 489
    label "wyczerpanie"
  ]
  node [
    id 490
    label "pozabijanie"
  ]
  node [
    id 491
    label "czarny"
  ]
  node [
    id 492
    label "kawa"
  ]
  node [
    id 493
    label "murzynek"
  ]
  node [
    id 494
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 495
    label "dripper"
  ]
  node [
    id 496
    label "ziarno"
  ]
  node [
    id 497
    label "u&#380;ywka"
  ]
  node [
    id 498
    label "egzotyk"
  ]
  node [
    id 499
    label "marzanowate"
  ]
  node [
    id 500
    label "nap&#243;j"
  ]
  node [
    id 501
    label "jedzenie"
  ]
  node [
    id 502
    label "produkt"
  ]
  node [
    id 503
    label "pestkowiec"
  ]
  node [
    id 504
    label "ro&#347;lina"
  ]
  node [
    id 505
    label "porcja"
  ]
  node [
    id 506
    label "kofeina"
  ]
  node [
    id 507
    label "chemex"
  ]
  node [
    id 508
    label "czarna_kawa"
  ]
  node [
    id 509
    label "ma&#347;lak_pstry"
  ]
  node [
    id 510
    label "ciasto"
  ]
  node [
    id 511
    label "czarne"
  ]
  node [
    id 512
    label "kolorowy"
  ]
  node [
    id 513
    label "bierka_szachowa"
  ]
  node [
    id 514
    label "gorzki"
  ]
  node [
    id 515
    label "murzy&#324;ski"
  ]
  node [
    id 516
    label "ksi&#261;dz"
  ]
  node [
    id 517
    label "kompletny"
  ]
  node [
    id 518
    label "przewrotny"
  ]
  node [
    id 519
    label "ponury"
  ]
  node [
    id 520
    label "beznadziejny"
  ]
  node [
    id 521
    label "z&#322;y"
  ]
  node [
    id 522
    label "wedel"
  ]
  node [
    id 523
    label "czarnuch"
  ]
  node [
    id 524
    label "granatowo"
  ]
  node [
    id 525
    label "ciemny"
  ]
  node [
    id 526
    label "negatywny"
  ]
  node [
    id 527
    label "ciemnienie"
  ]
  node [
    id 528
    label "czernienie"
  ]
  node [
    id 529
    label "zaczernienie"
  ]
  node [
    id 530
    label "pesymistycznie"
  ]
  node [
    id 531
    label "abolicjonista"
  ]
  node [
    id 532
    label "brudny"
  ]
  node [
    id 533
    label "zaczernianie_si&#281;"
  ]
  node [
    id 534
    label "kafar"
  ]
  node [
    id 535
    label "czarnuchowaty"
  ]
  node [
    id 536
    label "pessimistic"
  ]
  node [
    id 537
    label "czarniawy"
  ]
  node [
    id 538
    label "ciemnosk&#243;ry"
  ]
  node [
    id 539
    label "okrutny"
  ]
  node [
    id 540
    label "czarno"
  ]
  node [
    id 541
    label "zaczernienie_si&#281;"
  ]
  node [
    id 542
    label "niepomy&#347;lny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
]
