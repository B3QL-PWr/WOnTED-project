graph [
  node [
    id 0
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "handlowy"
    origin "text"
  ]
  node [
    id 3
    label "rezygnowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sprzeda&#380;"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ywy"
    origin "text"
  ]
  node [
    id 6
    label "karp"
    origin "text"
  ]
  node [
    id 7
    label "doros&#322;y"
  ]
  node [
    id 8
    label "znaczny"
  ]
  node [
    id 9
    label "niema&#322;o"
  ]
  node [
    id 10
    label "wiele"
  ]
  node [
    id 11
    label "rozwini&#281;ty"
  ]
  node [
    id 12
    label "dorodny"
  ]
  node [
    id 13
    label "wa&#380;ny"
  ]
  node [
    id 14
    label "prawdziwy"
  ]
  node [
    id 15
    label "du&#380;o"
  ]
  node [
    id 16
    label "&#380;ywny"
  ]
  node [
    id 17
    label "szczery"
  ]
  node [
    id 18
    label "naturalny"
  ]
  node [
    id 19
    label "naprawd&#281;"
  ]
  node [
    id 20
    label "realnie"
  ]
  node [
    id 21
    label "podobny"
  ]
  node [
    id 22
    label "zgodny"
  ]
  node [
    id 23
    label "m&#261;dry"
  ]
  node [
    id 24
    label "prawdziwie"
  ]
  node [
    id 25
    label "znacznie"
  ]
  node [
    id 26
    label "zauwa&#380;alny"
  ]
  node [
    id 27
    label "wynios&#322;y"
  ]
  node [
    id 28
    label "dono&#347;ny"
  ]
  node [
    id 29
    label "silny"
  ]
  node [
    id 30
    label "wa&#380;nie"
  ]
  node [
    id 31
    label "istotnie"
  ]
  node [
    id 32
    label "eksponowany"
  ]
  node [
    id 33
    label "dobry"
  ]
  node [
    id 34
    label "ukszta&#322;towany"
  ]
  node [
    id 35
    label "do&#347;cig&#322;y"
  ]
  node [
    id 36
    label "&#378;ra&#322;y"
  ]
  node [
    id 37
    label "zdr&#243;w"
  ]
  node [
    id 38
    label "dorodnie"
  ]
  node [
    id 39
    label "okaza&#322;y"
  ]
  node [
    id 40
    label "mocno"
  ]
  node [
    id 41
    label "wiela"
  ]
  node [
    id 42
    label "bardzo"
  ]
  node [
    id 43
    label "cz&#281;sto"
  ]
  node [
    id 44
    label "wydoro&#347;lenie"
  ]
  node [
    id 45
    label "cz&#322;owiek"
  ]
  node [
    id 46
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 47
    label "doro&#347;lenie"
  ]
  node [
    id 48
    label "doro&#347;le"
  ]
  node [
    id 49
    label "senior"
  ]
  node [
    id 50
    label "dojrzale"
  ]
  node [
    id 51
    label "wapniak"
  ]
  node [
    id 52
    label "dojrza&#322;y"
  ]
  node [
    id 53
    label "doletni"
  ]
  node [
    id 54
    label "kszta&#322;t"
  ]
  node [
    id 55
    label "provider"
  ]
  node [
    id 56
    label "biznes_elektroniczny"
  ]
  node [
    id 57
    label "zasadzka"
  ]
  node [
    id 58
    label "mesh"
  ]
  node [
    id 59
    label "plecionka"
  ]
  node [
    id 60
    label "gauze"
  ]
  node [
    id 61
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 62
    label "struktura"
  ]
  node [
    id 63
    label "web"
  ]
  node [
    id 64
    label "organizacja"
  ]
  node [
    id 65
    label "gra_sieciowa"
  ]
  node [
    id 66
    label "net"
  ]
  node [
    id 67
    label "media"
  ]
  node [
    id 68
    label "sie&#263;_komputerowa"
  ]
  node [
    id 69
    label "nitka"
  ]
  node [
    id 70
    label "snu&#263;"
  ]
  node [
    id 71
    label "vane"
  ]
  node [
    id 72
    label "instalacja"
  ]
  node [
    id 73
    label "wysnu&#263;"
  ]
  node [
    id 74
    label "organization"
  ]
  node [
    id 75
    label "obiekt"
  ]
  node [
    id 76
    label "us&#322;uga_internetowa"
  ]
  node [
    id 77
    label "rozmieszczenie"
  ]
  node [
    id 78
    label "podcast"
  ]
  node [
    id 79
    label "hipertekst"
  ]
  node [
    id 80
    label "cyberprzestrze&#324;"
  ]
  node [
    id 81
    label "mem"
  ]
  node [
    id 82
    label "grooming"
  ]
  node [
    id 83
    label "punkt_dost&#281;pu"
  ]
  node [
    id 84
    label "netbook"
  ]
  node [
    id 85
    label "e-hazard"
  ]
  node [
    id 86
    label "strona"
  ]
  node [
    id 87
    label "zastawia&#263;"
  ]
  node [
    id 88
    label "miejsce"
  ]
  node [
    id 89
    label "zastawi&#263;"
  ]
  node [
    id 90
    label "ambush"
  ]
  node [
    id 91
    label "atak"
  ]
  node [
    id 92
    label "podst&#281;p"
  ]
  node [
    id 93
    label "sytuacja"
  ]
  node [
    id 94
    label "formacja"
  ]
  node [
    id 95
    label "punkt_widzenia"
  ]
  node [
    id 96
    label "wygl&#261;d"
  ]
  node [
    id 97
    label "g&#322;owa"
  ]
  node [
    id 98
    label "spirala"
  ]
  node [
    id 99
    label "p&#322;at"
  ]
  node [
    id 100
    label "comeliness"
  ]
  node [
    id 101
    label "kielich"
  ]
  node [
    id 102
    label "face"
  ]
  node [
    id 103
    label "blaszka"
  ]
  node [
    id 104
    label "charakter"
  ]
  node [
    id 105
    label "p&#281;tla"
  ]
  node [
    id 106
    label "pasmo"
  ]
  node [
    id 107
    label "cecha"
  ]
  node [
    id 108
    label "linearno&#347;&#263;"
  ]
  node [
    id 109
    label "gwiazda"
  ]
  node [
    id 110
    label "miniatura"
  ]
  node [
    id 111
    label "integer"
  ]
  node [
    id 112
    label "liczba"
  ]
  node [
    id 113
    label "zlewanie_si&#281;"
  ]
  node [
    id 114
    label "ilo&#347;&#263;"
  ]
  node [
    id 115
    label "uk&#322;ad"
  ]
  node [
    id 116
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 117
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 118
    label "pe&#322;ny"
  ]
  node [
    id 119
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 120
    label "u&#322;o&#380;enie"
  ]
  node [
    id 121
    label "porozmieszczanie"
  ]
  node [
    id 122
    label "wyst&#281;powanie"
  ]
  node [
    id 123
    label "layout"
  ]
  node [
    id 124
    label "umieszczenie"
  ]
  node [
    id 125
    label "mechanika"
  ]
  node [
    id 126
    label "o&#347;"
  ]
  node [
    id 127
    label "usenet"
  ]
  node [
    id 128
    label "rozprz&#261;c"
  ]
  node [
    id 129
    label "zachowanie"
  ]
  node [
    id 130
    label "cybernetyk"
  ]
  node [
    id 131
    label "podsystem"
  ]
  node [
    id 132
    label "system"
  ]
  node [
    id 133
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 134
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 135
    label "sk&#322;ad"
  ]
  node [
    id 136
    label "systemat"
  ]
  node [
    id 137
    label "konstrukcja"
  ]
  node [
    id 138
    label "konstelacja"
  ]
  node [
    id 139
    label "podmiot"
  ]
  node [
    id 140
    label "jednostka_organizacyjna"
  ]
  node [
    id 141
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 142
    label "TOPR"
  ]
  node [
    id 143
    label "endecki"
  ]
  node [
    id 144
    label "zesp&#243;&#322;"
  ]
  node [
    id 145
    label "przedstawicielstwo"
  ]
  node [
    id 146
    label "od&#322;am"
  ]
  node [
    id 147
    label "Cepelia"
  ]
  node [
    id 148
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 149
    label "ZBoWiD"
  ]
  node [
    id 150
    label "centrala"
  ]
  node [
    id 151
    label "GOPR"
  ]
  node [
    id 152
    label "ZOMO"
  ]
  node [
    id 153
    label "ZMP"
  ]
  node [
    id 154
    label "komitet_koordynacyjny"
  ]
  node [
    id 155
    label "przybud&#243;wka"
  ]
  node [
    id 156
    label "boj&#243;wka"
  ]
  node [
    id 157
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 158
    label "proces"
  ]
  node [
    id 159
    label "kompozycja"
  ]
  node [
    id 160
    label "uzbrajanie"
  ]
  node [
    id 161
    label "czynno&#347;&#263;"
  ]
  node [
    id 162
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 163
    label "co&#347;"
  ]
  node [
    id 164
    label "budynek"
  ]
  node [
    id 165
    label "thing"
  ]
  node [
    id 166
    label "poj&#281;cie"
  ]
  node [
    id 167
    label "program"
  ]
  node [
    id 168
    label "rzecz"
  ]
  node [
    id 169
    label "mass-media"
  ]
  node [
    id 170
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 171
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 172
    label "przekazior"
  ]
  node [
    id 173
    label "medium"
  ]
  node [
    id 174
    label "tekst"
  ]
  node [
    id 175
    label "ornament"
  ]
  node [
    id 176
    label "przedmiot"
  ]
  node [
    id 177
    label "splot"
  ]
  node [
    id 178
    label "braid"
  ]
  node [
    id 179
    label "szachulec"
  ]
  node [
    id 180
    label "b&#322;&#261;d"
  ]
  node [
    id 181
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 182
    label "nawijad&#322;o"
  ]
  node [
    id 183
    label "sznur"
  ]
  node [
    id 184
    label "motowid&#322;o"
  ]
  node [
    id 185
    label "makaron"
  ]
  node [
    id 186
    label "internet"
  ]
  node [
    id 187
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 188
    label "kartka"
  ]
  node [
    id 189
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 190
    label "logowanie"
  ]
  node [
    id 191
    label "plik"
  ]
  node [
    id 192
    label "s&#261;d"
  ]
  node [
    id 193
    label "adres_internetowy"
  ]
  node [
    id 194
    label "linia"
  ]
  node [
    id 195
    label "serwis_internetowy"
  ]
  node [
    id 196
    label "posta&#263;"
  ]
  node [
    id 197
    label "bok"
  ]
  node [
    id 198
    label "skr&#281;canie"
  ]
  node [
    id 199
    label "skr&#281;ca&#263;"
  ]
  node [
    id 200
    label "orientowanie"
  ]
  node [
    id 201
    label "skr&#281;ci&#263;"
  ]
  node [
    id 202
    label "uj&#281;cie"
  ]
  node [
    id 203
    label "zorientowanie"
  ]
  node [
    id 204
    label "ty&#322;"
  ]
  node [
    id 205
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 206
    label "fragment"
  ]
  node [
    id 207
    label "zorientowa&#263;"
  ]
  node [
    id 208
    label "pagina"
  ]
  node [
    id 209
    label "g&#243;ra"
  ]
  node [
    id 210
    label "orientowa&#263;"
  ]
  node [
    id 211
    label "voice"
  ]
  node [
    id 212
    label "orientacja"
  ]
  node [
    id 213
    label "prz&#243;d"
  ]
  node [
    id 214
    label "powierzchnia"
  ]
  node [
    id 215
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 216
    label "forma"
  ]
  node [
    id 217
    label "skr&#281;cenie"
  ]
  node [
    id 218
    label "paj&#261;k"
  ]
  node [
    id 219
    label "devise"
  ]
  node [
    id 220
    label "wyjmowa&#263;"
  ]
  node [
    id 221
    label "project"
  ]
  node [
    id 222
    label "my&#347;le&#263;"
  ]
  node [
    id 223
    label "produkowa&#263;"
  ]
  node [
    id 224
    label "uk&#322;ada&#263;"
  ]
  node [
    id 225
    label "tworzy&#263;"
  ]
  node [
    id 226
    label "wyj&#261;&#263;"
  ]
  node [
    id 227
    label "stworzy&#263;"
  ]
  node [
    id 228
    label "zasadzi&#263;"
  ]
  node [
    id 229
    label "dostawca"
  ]
  node [
    id 230
    label "telefonia"
  ]
  node [
    id 231
    label "wydawnictwo"
  ]
  node [
    id 232
    label "meme"
  ]
  node [
    id 233
    label "hazard"
  ]
  node [
    id 234
    label "molestowanie_seksualne"
  ]
  node [
    id 235
    label "piel&#281;gnacja"
  ]
  node [
    id 236
    label "zwierz&#281;_domowe"
  ]
  node [
    id 237
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 238
    label "ma&#322;y"
  ]
  node [
    id 239
    label "handlowo"
  ]
  node [
    id 240
    label "charge"
  ]
  node [
    id 241
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 242
    label "przestawa&#263;"
  ]
  node [
    id 243
    label "&#380;y&#263;"
  ]
  node [
    id 244
    label "coating"
  ]
  node [
    id 245
    label "przebywa&#263;"
  ]
  node [
    id 246
    label "determine"
  ]
  node [
    id 247
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 248
    label "ko&#324;czy&#263;"
  ]
  node [
    id 249
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 250
    label "finish_up"
  ]
  node [
    id 251
    label "przeniesienie_praw"
  ]
  node [
    id 252
    label "przeda&#380;"
  ]
  node [
    id 253
    label "transakcja"
  ]
  node [
    id 254
    label "sprzedaj&#261;cy"
  ]
  node [
    id 255
    label "rabat"
  ]
  node [
    id 256
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 257
    label "arbitra&#380;"
  ]
  node [
    id 258
    label "zam&#243;wienie"
  ]
  node [
    id 259
    label "cena_transferowa"
  ]
  node [
    id 260
    label "kontrakt_terminowy"
  ]
  node [
    id 261
    label "facjenda"
  ]
  node [
    id 262
    label "kupno"
  ]
  node [
    id 263
    label "zni&#380;ka"
  ]
  node [
    id 264
    label "ciekawy"
  ]
  node [
    id 265
    label "szybki"
  ]
  node [
    id 266
    label "&#380;ywotny"
  ]
  node [
    id 267
    label "&#380;ywo"
  ]
  node [
    id 268
    label "o&#380;ywianie"
  ]
  node [
    id 269
    label "&#380;ycie"
  ]
  node [
    id 270
    label "g&#322;&#281;boki"
  ]
  node [
    id 271
    label "wyra&#378;ny"
  ]
  node [
    id 272
    label "czynny"
  ]
  node [
    id 273
    label "aktualny"
  ]
  node [
    id 274
    label "zgrabny"
  ]
  node [
    id 275
    label "realistyczny"
  ]
  node [
    id 276
    label "energiczny"
  ]
  node [
    id 277
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 278
    label "aktualnie"
  ]
  node [
    id 279
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 280
    label "aktualizowanie"
  ]
  node [
    id 281
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 282
    label "uaktualnienie"
  ]
  node [
    id 283
    label "prawy"
  ]
  node [
    id 284
    label "zrozumia&#322;y"
  ]
  node [
    id 285
    label "immanentny"
  ]
  node [
    id 286
    label "zwyczajny"
  ]
  node [
    id 287
    label "bezsporny"
  ]
  node [
    id 288
    label "organicznie"
  ]
  node [
    id 289
    label "pierwotny"
  ]
  node [
    id 290
    label "neutralny"
  ]
  node [
    id 291
    label "normalny"
  ]
  node [
    id 292
    label "rzeczywisty"
  ]
  node [
    id 293
    label "naturalnie"
  ]
  node [
    id 294
    label "ludzko&#347;&#263;"
  ]
  node [
    id 295
    label "asymilowanie"
  ]
  node [
    id 296
    label "asymilowa&#263;"
  ]
  node [
    id 297
    label "os&#322;abia&#263;"
  ]
  node [
    id 298
    label "hominid"
  ]
  node [
    id 299
    label "podw&#322;adny"
  ]
  node [
    id 300
    label "os&#322;abianie"
  ]
  node [
    id 301
    label "figura"
  ]
  node [
    id 302
    label "portrecista"
  ]
  node [
    id 303
    label "dwun&#243;g"
  ]
  node [
    id 304
    label "profanum"
  ]
  node [
    id 305
    label "mikrokosmos"
  ]
  node [
    id 306
    label "nasada"
  ]
  node [
    id 307
    label "duch"
  ]
  node [
    id 308
    label "antropochoria"
  ]
  node [
    id 309
    label "osoba"
  ]
  node [
    id 310
    label "wz&#243;r"
  ]
  node [
    id 311
    label "oddzia&#322;ywanie"
  ]
  node [
    id 312
    label "Adam"
  ]
  node [
    id 313
    label "homo_sapiens"
  ]
  node [
    id 314
    label "polifag"
  ]
  node [
    id 315
    label "wyra&#378;nie"
  ]
  node [
    id 316
    label "nieoboj&#281;tny"
  ]
  node [
    id 317
    label "zdecydowany"
  ]
  node [
    id 318
    label "intensywny"
  ]
  node [
    id 319
    label "gruntowny"
  ]
  node [
    id 320
    label "mocny"
  ]
  node [
    id 321
    label "ukryty"
  ]
  node [
    id 322
    label "wyrazisty"
  ]
  node [
    id 323
    label "daleki"
  ]
  node [
    id 324
    label "dog&#322;&#281;bny"
  ]
  node [
    id 325
    label "g&#322;&#281;boko"
  ]
  node [
    id 326
    label "niezrozumia&#322;y"
  ]
  node [
    id 327
    label "niski"
  ]
  node [
    id 328
    label "realistycznie"
  ]
  node [
    id 329
    label "przytomny"
  ]
  node [
    id 330
    label "zdrowy"
  ]
  node [
    id 331
    label "energicznie"
  ]
  node [
    id 332
    label "ostry"
  ]
  node [
    id 333
    label "jary"
  ]
  node [
    id 334
    label "prosty"
  ]
  node [
    id 335
    label "kr&#243;tki"
  ]
  node [
    id 336
    label "temperamentny"
  ]
  node [
    id 337
    label "bystrolotny"
  ]
  node [
    id 338
    label "dynamiczny"
  ]
  node [
    id 339
    label "szybko"
  ]
  node [
    id 340
    label "sprawny"
  ]
  node [
    id 341
    label "bezpo&#347;redni"
  ]
  node [
    id 342
    label "kszta&#322;tny"
  ]
  node [
    id 343
    label "zr&#281;czny"
  ]
  node [
    id 344
    label "skuteczny"
  ]
  node [
    id 345
    label "p&#322;ynny"
  ]
  node [
    id 346
    label "delikatny"
  ]
  node [
    id 347
    label "polotny"
  ]
  node [
    id 348
    label "zwinny"
  ]
  node [
    id 349
    label "zgrabnie"
  ]
  node [
    id 350
    label "harmonijny"
  ]
  node [
    id 351
    label "zwinnie"
  ]
  node [
    id 352
    label "nietuzinkowy"
  ]
  node [
    id 353
    label "intryguj&#261;cy"
  ]
  node [
    id 354
    label "ch&#281;tny"
  ]
  node [
    id 355
    label "swoisty"
  ]
  node [
    id 356
    label "interesowanie"
  ]
  node [
    id 357
    label "dziwny"
  ]
  node [
    id 358
    label "interesuj&#261;cy"
  ]
  node [
    id 359
    label "ciekawie"
  ]
  node [
    id 360
    label "indagator"
  ]
  node [
    id 361
    label "realny"
  ]
  node [
    id 362
    label "dzia&#322;anie"
  ]
  node [
    id 363
    label "dzia&#322;alny"
  ]
  node [
    id 364
    label "faktyczny"
  ]
  node [
    id 365
    label "zdolny"
  ]
  node [
    id 366
    label "czynnie"
  ]
  node [
    id 367
    label "uczynnianie"
  ]
  node [
    id 368
    label "aktywnie"
  ]
  node [
    id 369
    label "zaanga&#380;owany"
  ]
  node [
    id 370
    label "istotny"
  ]
  node [
    id 371
    label "zaj&#281;ty"
  ]
  node [
    id 372
    label "uczynnienie"
  ]
  node [
    id 373
    label "krzepienie"
  ]
  node [
    id 374
    label "pokrzepienie"
  ]
  node [
    id 375
    label "niepodwa&#380;alny"
  ]
  node [
    id 376
    label "przekonuj&#261;cy"
  ]
  node [
    id 377
    label "wytrzyma&#322;y"
  ]
  node [
    id 378
    label "konkretny"
  ]
  node [
    id 379
    label "silnie"
  ]
  node [
    id 380
    label "meflochina"
  ]
  node [
    id 381
    label "zajebisty"
  ]
  node [
    id 382
    label "biologicznie"
  ]
  node [
    id 383
    label "&#380;ywotnie"
  ]
  node [
    id 384
    label "nasycony"
  ]
  node [
    id 385
    label "vitalization"
  ]
  node [
    id 386
    label "przywracanie"
  ]
  node [
    id 387
    label "ratowanie"
  ]
  node [
    id 388
    label "nadawanie"
  ]
  node [
    id 389
    label "pobudzanie"
  ]
  node [
    id 390
    label "wzbudzanie"
  ]
  node [
    id 391
    label "raj_utracony"
  ]
  node [
    id 392
    label "umieranie"
  ]
  node [
    id 393
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 394
    label "prze&#380;ywanie"
  ]
  node [
    id 395
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 396
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 397
    label "po&#322;&#243;g"
  ]
  node [
    id 398
    label "umarcie"
  ]
  node [
    id 399
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 400
    label "subsistence"
  ]
  node [
    id 401
    label "power"
  ]
  node [
    id 402
    label "okres_noworodkowy"
  ]
  node [
    id 403
    label "prze&#380;ycie"
  ]
  node [
    id 404
    label "wiek_matuzalemowy"
  ]
  node [
    id 405
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 406
    label "entity"
  ]
  node [
    id 407
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 408
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 409
    label "do&#380;ywanie"
  ]
  node [
    id 410
    label "byt"
  ]
  node [
    id 411
    label "andropauza"
  ]
  node [
    id 412
    label "dzieci&#324;stwo"
  ]
  node [
    id 413
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 414
    label "rozw&#243;j"
  ]
  node [
    id 415
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 416
    label "czas"
  ]
  node [
    id 417
    label "menopauza"
  ]
  node [
    id 418
    label "&#347;mier&#263;"
  ]
  node [
    id 419
    label "koleje_losu"
  ]
  node [
    id 420
    label "bycie"
  ]
  node [
    id 421
    label "zegar_biologiczny"
  ]
  node [
    id 422
    label "szwung"
  ]
  node [
    id 423
    label "przebywanie"
  ]
  node [
    id 424
    label "warunki"
  ]
  node [
    id 425
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 426
    label "niemowl&#281;ctwo"
  ]
  node [
    id 427
    label "life"
  ]
  node [
    id 428
    label "staro&#347;&#263;"
  ]
  node [
    id 429
    label "energy"
  ]
  node [
    id 430
    label "ryba"
  ]
  node [
    id 431
    label "karpiowate"
  ]
  node [
    id 432
    label "carp"
  ]
  node [
    id 433
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 434
    label "kr&#281;gowiec"
  ]
  node [
    id 435
    label "systemik"
  ]
  node [
    id 436
    label "doniczkowiec"
  ]
  node [
    id 437
    label "mi&#281;so"
  ]
  node [
    id 438
    label "patroszy&#263;"
  ]
  node [
    id 439
    label "rakowato&#347;&#263;"
  ]
  node [
    id 440
    label "w&#281;dkarstwo"
  ]
  node [
    id 441
    label "ryby"
  ]
  node [
    id 442
    label "fish"
  ]
  node [
    id 443
    label "linia_boczna"
  ]
  node [
    id 444
    label "tar&#322;o"
  ]
  node [
    id 445
    label "wyrostek_filtracyjny"
  ]
  node [
    id 446
    label "m&#281;tnooki"
  ]
  node [
    id 447
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 448
    label "pokrywa_skrzelowa"
  ]
  node [
    id 449
    label "ikra"
  ]
  node [
    id 450
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 451
    label "szczelina_skrzelowa"
  ]
  node [
    id 452
    label "karpiokszta&#322;tne"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
]
