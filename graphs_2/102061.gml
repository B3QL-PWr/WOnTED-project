graph [
  node [
    id 0
    label "forum"
    origin "text"
  ]
  node [
    id 1
    label "pismak"
    origin "text"
  ]
  node [
    id 2
    label "najstarszy"
    origin "text"
  ]
  node [
    id 3
    label "konkurs"
    origin "text"
  ]
  node [
    id 4
    label "gazetka"
    origin "text"
  ]
  node [
    id 5
    label "szkolny"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wa&#322;brzych"
    origin "text"
  ]
  node [
    id 10
    label "rok"
    origin "text"
  ]
  node [
    id 11
    label "pomys&#322;odawczyni"
    origin "text"
  ]
  node [
    id 12
    label "nauczycielka"
    origin "text"
  ]
  node [
    id 13
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 14
    label "polski"
    origin "text"
  ]
  node [
    id 15
    label "wieloletni"
    origin "text"
  ]
  node [
    id 16
    label "animatorka"
    origin "text"
  ]
  node [
    id 17
    label "media"
    origin "text"
  ]
  node [
    id 18
    label "el&#380;bieta"
    origin "text"
  ]
  node [
    id 19
    label "sura"
    origin "text"
  ]
  node [
    id 20
    label "opiekunka"
    origin "text"
  ]
  node [
    id 21
    label "obok"
    origin "text"
  ]
  node [
    id 22
    label "przez"
    origin "text"
  ]
  node [
    id 23
    label "lata"
    origin "text"
  ]
  node [
    id 24
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 25
    label "wymiar"
    origin "text"
  ]
  node [
    id 26
    label "regionalny"
    origin "text"
  ]
  node [
    id 27
    label "najpierw"
    origin "text"
  ]
  node [
    id 28
    label "woj"
    origin "text"
  ]
  node [
    id 29
    label "wa&#322;brzyskie"
    origin "text"
  ]
  node [
    id 30
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 31
    label "dolno&#347;l&#261;ski"
    origin "text"
  ]
  node [
    id 32
    label "si&#243;dma"
    origin "text"
  ]
  node [
    id 33
    label "edycja"
    origin "text"
  ]
  node [
    id 34
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 35
    label "zasi&#281;g"
    origin "text"
  ]
  node [
    id 36
    label "og&#243;lnopolski"
    origin "text"
  ]
  node [
    id 37
    label "jedenasta"
    origin "text"
  ]
  node [
    id 38
    label "poza"
    origin "text"
  ]
  node [
    id 39
    label "gala"
    origin "text"
  ]
  node [
    id 40
    label "poprzedza&#263;"
    origin "text"
  ]
  node [
    id 41
    label "dwudniowy"
    origin "text"
  ]
  node [
    id 42
    label "warsztat"
    origin "text"
  ]
  node [
    id 43
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 44
    label "dla"
    origin "text"
  ]
  node [
    id 45
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 46
    label "dobry"
    origin "text"
  ]
  node [
    id 47
    label "redakcja"
    origin "text"
  ]
  node [
    id 48
    label "czternasta"
    origin "text"
  ]
  node [
    id 49
    label "wsp&#243;&#322;organizowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "fundacja"
    origin "text"
  ]
  node [
    id 51
    label "nowa"
    origin "text"
  ]
  node [
    id 52
    label "raz"
    origin "text"
  ]
  node [
    id 53
    label "kolejny"
    origin "text"
  ]
  node [
    id 54
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 55
    label "patronat"
    origin "text"
  ]
  node [
    id 56
    label "obj&#261;&#263;"
    origin "text"
  ]
  node [
    id 57
    label "ministerstwo"
    origin "text"
  ]
  node [
    id 58
    label "edukacja"
    origin "text"
  ]
  node [
    id 59
    label "narodowy"
    origin "text"
  ]
  node [
    id 60
    label "grupa_dyskusyjna"
  ]
  node [
    id 61
    label "s&#261;d"
  ]
  node [
    id 62
    label "plac"
  ]
  node [
    id 63
    label "bazylika"
  ]
  node [
    id 64
    label "przestrze&#324;"
  ]
  node [
    id 65
    label "miejsce"
  ]
  node [
    id 66
    label "portal"
  ]
  node [
    id 67
    label "konferencja"
  ]
  node [
    id 68
    label "agora"
  ]
  node [
    id 69
    label "grupa"
  ]
  node [
    id 70
    label "strona"
  ]
  node [
    id 71
    label "odm&#322;adzanie"
  ]
  node [
    id 72
    label "liga"
  ]
  node [
    id 73
    label "jednostka_systematyczna"
  ]
  node [
    id 74
    label "asymilowanie"
  ]
  node [
    id 75
    label "gromada"
  ]
  node [
    id 76
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 77
    label "asymilowa&#263;"
  ]
  node [
    id 78
    label "egzemplarz"
  ]
  node [
    id 79
    label "Entuzjastki"
  ]
  node [
    id 80
    label "zbi&#243;r"
  ]
  node [
    id 81
    label "kompozycja"
  ]
  node [
    id 82
    label "Terranie"
  ]
  node [
    id 83
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 84
    label "category"
  ]
  node [
    id 85
    label "pakiet_klimatyczny"
  ]
  node [
    id 86
    label "oddzia&#322;"
  ]
  node [
    id 87
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 88
    label "cz&#261;steczka"
  ]
  node [
    id 89
    label "stage_set"
  ]
  node [
    id 90
    label "type"
  ]
  node [
    id 91
    label "specgrupa"
  ]
  node [
    id 92
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 93
    label "&#346;wietliki"
  ]
  node [
    id 94
    label "odm&#322;odzenie"
  ]
  node [
    id 95
    label "Eurogrupa"
  ]
  node [
    id 96
    label "odm&#322;adza&#263;"
  ]
  node [
    id 97
    label "formacja_geologiczna"
  ]
  node [
    id 98
    label "harcerze_starsi"
  ]
  node [
    id 99
    label "warunek_lokalowy"
  ]
  node [
    id 100
    label "location"
  ]
  node [
    id 101
    label "uwaga"
  ]
  node [
    id 102
    label "status"
  ]
  node [
    id 103
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 104
    label "chwila"
  ]
  node [
    id 105
    label "cia&#322;o"
  ]
  node [
    id 106
    label "cecha"
  ]
  node [
    id 107
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 108
    label "praca"
  ]
  node [
    id 109
    label "rz&#261;d"
  ]
  node [
    id 110
    label "&#321;ubianka"
  ]
  node [
    id 111
    label "area"
  ]
  node [
    id 112
    label "Majdan"
  ]
  node [
    id 113
    label "pole_bitwy"
  ]
  node [
    id 114
    label "stoisko"
  ]
  node [
    id 115
    label "obszar"
  ]
  node [
    id 116
    label "pierzeja"
  ]
  node [
    id 117
    label "obiekt_handlowy"
  ]
  node [
    id 118
    label "zgromadzenie"
  ]
  node [
    id 119
    label "miasto"
  ]
  node [
    id 120
    label "targowica"
  ]
  node [
    id 121
    label "kram"
  ]
  node [
    id 122
    label "rozdzielanie"
  ]
  node [
    id 123
    label "bezbrze&#380;e"
  ]
  node [
    id 124
    label "punkt"
  ]
  node [
    id 125
    label "czasoprzestrze&#324;"
  ]
  node [
    id 126
    label "niezmierzony"
  ]
  node [
    id 127
    label "przedzielenie"
  ]
  node [
    id 128
    label "nielito&#347;ciwy"
  ]
  node [
    id 129
    label "rozdziela&#263;"
  ]
  node [
    id 130
    label "oktant"
  ]
  node [
    id 131
    label "przedzieli&#263;"
  ]
  node [
    id 132
    label "przestw&#243;r"
  ]
  node [
    id 133
    label "kartka"
  ]
  node [
    id 134
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 135
    label "logowanie"
  ]
  node [
    id 136
    label "plik"
  ]
  node [
    id 137
    label "adres_internetowy"
  ]
  node [
    id 138
    label "linia"
  ]
  node [
    id 139
    label "serwis_internetowy"
  ]
  node [
    id 140
    label "posta&#263;"
  ]
  node [
    id 141
    label "bok"
  ]
  node [
    id 142
    label "skr&#281;canie"
  ]
  node [
    id 143
    label "skr&#281;ca&#263;"
  ]
  node [
    id 144
    label "orientowanie"
  ]
  node [
    id 145
    label "skr&#281;ci&#263;"
  ]
  node [
    id 146
    label "uj&#281;cie"
  ]
  node [
    id 147
    label "zorientowanie"
  ]
  node [
    id 148
    label "ty&#322;"
  ]
  node [
    id 149
    label "fragment"
  ]
  node [
    id 150
    label "layout"
  ]
  node [
    id 151
    label "obiekt"
  ]
  node [
    id 152
    label "zorientowa&#263;"
  ]
  node [
    id 153
    label "pagina"
  ]
  node [
    id 154
    label "podmiot"
  ]
  node [
    id 155
    label "g&#243;ra"
  ]
  node [
    id 156
    label "orientowa&#263;"
  ]
  node [
    id 157
    label "voice"
  ]
  node [
    id 158
    label "orientacja"
  ]
  node [
    id 159
    label "prz&#243;d"
  ]
  node [
    id 160
    label "internet"
  ]
  node [
    id 161
    label "powierzchnia"
  ]
  node [
    id 162
    label "forma"
  ]
  node [
    id 163
    label "skr&#281;cenie"
  ]
  node [
    id 164
    label "Ja&#322;ta"
  ]
  node [
    id 165
    label "spotkanie"
  ]
  node [
    id 166
    label "konferencyjka"
  ]
  node [
    id 167
    label "conference"
  ]
  node [
    id 168
    label "grusza_pospolita"
  ]
  node [
    id 169
    label "Poczdam"
  ]
  node [
    id 170
    label "obramienie"
  ]
  node [
    id 171
    label "Onet"
  ]
  node [
    id 172
    label "archiwolta"
  ]
  node [
    id 173
    label "wej&#347;cie"
  ]
  node [
    id 174
    label "hala"
  ]
  node [
    id 175
    label "westwerk"
  ]
  node [
    id 176
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 177
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 178
    label "zesp&#243;&#322;"
  ]
  node [
    id 179
    label "podejrzany"
  ]
  node [
    id 180
    label "s&#261;downictwo"
  ]
  node [
    id 181
    label "system"
  ]
  node [
    id 182
    label "biuro"
  ]
  node [
    id 183
    label "wytw&#243;r"
  ]
  node [
    id 184
    label "court"
  ]
  node [
    id 185
    label "bronienie"
  ]
  node [
    id 186
    label "urz&#261;d"
  ]
  node [
    id 187
    label "wydarzenie"
  ]
  node [
    id 188
    label "oskar&#380;yciel"
  ]
  node [
    id 189
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 190
    label "skazany"
  ]
  node [
    id 191
    label "post&#281;powanie"
  ]
  node [
    id 192
    label "broni&#263;"
  ]
  node [
    id 193
    label "my&#347;l"
  ]
  node [
    id 194
    label "pods&#261;dny"
  ]
  node [
    id 195
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 196
    label "obrona"
  ]
  node [
    id 197
    label "wypowied&#378;"
  ]
  node [
    id 198
    label "instytucja"
  ]
  node [
    id 199
    label "antylogizm"
  ]
  node [
    id 200
    label "konektyw"
  ]
  node [
    id 201
    label "&#347;wiadek"
  ]
  node [
    id 202
    label "procesowicz"
  ]
  node [
    id 203
    label "gryzipi&#243;rek"
  ]
  node [
    id 204
    label "urz&#281;dnik"
  ]
  node [
    id 205
    label "grafoman"
  ]
  node [
    id 206
    label "dziennikarz_prasowy"
  ]
  node [
    id 207
    label "trybik"
  ]
  node [
    id 208
    label "casting"
  ]
  node [
    id 209
    label "nab&#243;r"
  ]
  node [
    id 210
    label "Eurowizja"
  ]
  node [
    id 211
    label "eliminacje"
  ]
  node [
    id 212
    label "impreza"
  ]
  node [
    id 213
    label "emulation"
  ]
  node [
    id 214
    label "Interwizja"
  ]
  node [
    id 215
    label "impra"
  ]
  node [
    id 216
    label "rozrywka"
  ]
  node [
    id 217
    label "przyj&#281;cie"
  ]
  node [
    id 218
    label "okazja"
  ]
  node [
    id 219
    label "party"
  ]
  node [
    id 220
    label "recruitment"
  ]
  node [
    id 221
    label "wyb&#243;r"
  ]
  node [
    id 222
    label "faza"
  ]
  node [
    id 223
    label "runda"
  ]
  node [
    id 224
    label "turniej"
  ]
  node [
    id 225
    label "retirement"
  ]
  node [
    id 226
    label "przes&#322;uchanie"
  ]
  node [
    id 227
    label "w&#281;dkarstwo"
  ]
  node [
    id 228
    label "czasopismo"
  ]
  node [
    id 229
    label "psychotest"
  ]
  node [
    id 230
    label "pismo"
  ]
  node [
    id 231
    label "communication"
  ]
  node [
    id 232
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 233
    label "wk&#322;ad"
  ]
  node [
    id 234
    label "zajawka"
  ]
  node [
    id 235
    label "ok&#322;adka"
  ]
  node [
    id 236
    label "Zwrotnica"
  ]
  node [
    id 237
    label "dzia&#322;"
  ]
  node [
    id 238
    label "prasa"
  ]
  node [
    id 239
    label "szkolnie"
  ]
  node [
    id 240
    label "podstawowy"
  ]
  node [
    id 241
    label "prosty"
  ]
  node [
    id 242
    label "szkoleniowy"
  ]
  node [
    id 243
    label "skromny"
  ]
  node [
    id 244
    label "po_prostu"
  ]
  node [
    id 245
    label "naturalny"
  ]
  node [
    id 246
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 247
    label "rozprostowanie"
  ]
  node [
    id 248
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 249
    label "prosto"
  ]
  node [
    id 250
    label "prostowanie_si&#281;"
  ]
  node [
    id 251
    label "niepozorny"
  ]
  node [
    id 252
    label "cios"
  ]
  node [
    id 253
    label "prostoduszny"
  ]
  node [
    id 254
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 255
    label "naiwny"
  ]
  node [
    id 256
    label "&#322;atwy"
  ]
  node [
    id 257
    label "prostowanie"
  ]
  node [
    id 258
    label "zwyk&#322;y"
  ]
  node [
    id 259
    label "naukowy"
  ]
  node [
    id 260
    label "niezaawansowany"
  ]
  node [
    id 261
    label "najwa&#380;niejszy"
  ]
  node [
    id 262
    label "pocz&#261;tkowy"
  ]
  node [
    id 263
    label "podstawowo"
  ]
  node [
    id 264
    label "planowa&#263;"
  ]
  node [
    id 265
    label "dostosowywa&#263;"
  ]
  node [
    id 266
    label "treat"
  ]
  node [
    id 267
    label "pozyskiwa&#263;"
  ]
  node [
    id 268
    label "ensnare"
  ]
  node [
    id 269
    label "skupia&#263;"
  ]
  node [
    id 270
    label "create"
  ]
  node [
    id 271
    label "przygotowywa&#263;"
  ]
  node [
    id 272
    label "tworzy&#263;"
  ]
  node [
    id 273
    label "standard"
  ]
  node [
    id 274
    label "wprowadza&#263;"
  ]
  node [
    id 275
    label "rynek"
  ]
  node [
    id 276
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 277
    label "robi&#263;"
  ]
  node [
    id 278
    label "wprawia&#263;"
  ]
  node [
    id 279
    label "zaczyna&#263;"
  ]
  node [
    id 280
    label "wpisywa&#263;"
  ]
  node [
    id 281
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 282
    label "wchodzi&#263;"
  ]
  node [
    id 283
    label "take"
  ]
  node [
    id 284
    label "zapoznawa&#263;"
  ]
  node [
    id 285
    label "powodowa&#263;"
  ]
  node [
    id 286
    label "inflict"
  ]
  node [
    id 287
    label "umieszcza&#263;"
  ]
  node [
    id 288
    label "schodzi&#263;"
  ]
  node [
    id 289
    label "induct"
  ]
  node [
    id 290
    label "begin"
  ]
  node [
    id 291
    label "doprowadza&#263;"
  ]
  node [
    id 292
    label "ognisko"
  ]
  node [
    id 293
    label "huddle"
  ]
  node [
    id 294
    label "zbiera&#263;"
  ]
  node [
    id 295
    label "masowa&#263;"
  ]
  node [
    id 296
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 297
    label "uzyskiwa&#263;"
  ]
  node [
    id 298
    label "wytwarza&#263;"
  ]
  node [
    id 299
    label "tease"
  ]
  node [
    id 300
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 301
    label "pope&#322;nia&#263;"
  ]
  node [
    id 302
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 303
    label "get"
  ]
  node [
    id 304
    label "consist"
  ]
  node [
    id 305
    label "stanowi&#263;"
  ]
  node [
    id 306
    label "raise"
  ]
  node [
    id 307
    label "sposobi&#263;"
  ]
  node [
    id 308
    label "usposabia&#263;"
  ]
  node [
    id 309
    label "train"
  ]
  node [
    id 310
    label "arrange"
  ]
  node [
    id 311
    label "szkoli&#263;"
  ]
  node [
    id 312
    label "wykonywa&#263;"
  ]
  node [
    id 313
    label "pryczy&#263;"
  ]
  node [
    id 314
    label "mean"
  ]
  node [
    id 315
    label "lot_&#347;lizgowy"
  ]
  node [
    id 316
    label "organize"
  ]
  node [
    id 317
    label "project"
  ]
  node [
    id 318
    label "my&#347;le&#263;"
  ]
  node [
    id 319
    label "volunteer"
  ]
  node [
    id 320
    label "opracowywa&#263;"
  ]
  node [
    id 321
    label "opuszcza&#263;_si&#281;"
  ]
  node [
    id 322
    label "zmienia&#263;"
  ]
  node [
    id 323
    label "equal"
  ]
  node [
    id 324
    label "model"
  ]
  node [
    id 325
    label "ordinariness"
  ]
  node [
    id 326
    label "zorganizowa&#263;"
  ]
  node [
    id 327
    label "taniec_towarzyski"
  ]
  node [
    id 328
    label "organizowanie"
  ]
  node [
    id 329
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 330
    label "criterion"
  ]
  node [
    id 331
    label "zorganizowanie"
  ]
  node [
    id 332
    label "cover"
  ]
  node [
    id 333
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 334
    label "mie&#263;_miejsce"
  ]
  node [
    id 335
    label "trwa&#263;"
  ]
  node [
    id 336
    label "chodzi&#263;"
  ]
  node [
    id 337
    label "si&#281;ga&#263;"
  ]
  node [
    id 338
    label "stan"
  ]
  node [
    id 339
    label "obecno&#347;&#263;"
  ]
  node [
    id 340
    label "stand"
  ]
  node [
    id 341
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 342
    label "uczestniczy&#263;"
  ]
  node [
    id 343
    label "participate"
  ]
  node [
    id 344
    label "istnie&#263;"
  ]
  node [
    id 345
    label "pozostawa&#263;"
  ]
  node [
    id 346
    label "zostawa&#263;"
  ]
  node [
    id 347
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 348
    label "adhere"
  ]
  node [
    id 349
    label "compass"
  ]
  node [
    id 350
    label "korzysta&#263;"
  ]
  node [
    id 351
    label "appreciation"
  ]
  node [
    id 352
    label "osi&#261;ga&#263;"
  ]
  node [
    id 353
    label "dociera&#263;"
  ]
  node [
    id 354
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 355
    label "mierzy&#263;"
  ]
  node [
    id 356
    label "u&#380;ywa&#263;"
  ]
  node [
    id 357
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 358
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 359
    label "exsert"
  ]
  node [
    id 360
    label "being"
  ]
  node [
    id 361
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 362
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 363
    label "p&#322;ywa&#263;"
  ]
  node [
    id 364
    label "run"
  ]
  node [
    id 365
    label "bangla&#263;"
  ]
  node [
    id 366
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 367
    label "przebiega&#263;"
  ]
  node [
    id 368
    label "wk&#322;ada&#263;"
  ]
  node [
    id 369
    label "proceed"
  ]
  node [
    id 370
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 371
    label "carry"
  ]
  node [
    id 372
    label "bywa&#263;"
  ]
  node [
    id 373
    label "dziama&#263;"
  ]
  node [
    id 374
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 375
    label "stara&#263;_si&#281;"
  ]
  node [
    id 376
    label "para"
  ]
  node [
    id 377
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 378
    label "str&#243;j"
  ]
  node [
    id 379
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 380
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 381
    label "krok"
  ]
  node [
    id 382
    label "tryb"
  ]
  node [
    id 383
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 384
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 385
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 386
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 387
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 388
    label "continue"
  ]
  node [
    id 389
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 390
    label "Ohio"
  ]
  node [
    id 391
    label "wci&#281;cie"
  ]
  node [
    id 392
    label "Nowy_York"
  ]
  node [
    id 393
    label "warstwa"
  ]
  node [
    id 394
    label "samopoczucie"
  ]
  node [
    id 395
    label "Illinois"
  ]
  node [
    id 396
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 397
    label "state"
  ]
  node [
    id 398
    label "Jukatan"
  ]
  node [
    id 399
    label "Kalifornia"
  ]
  node [
    id 400
    label "Wirginia"
  ]
  node [
    id 401
    label "wektor"
  ]
  node [
    id 402
    label "Teksas"
  ]
  node [
    id 403
    label "Goa"
  ]
  node [
    id 404
    label "Waszyngton"
  ]
  node [
    id 405
    label "Massachusetts"
  ]
  node [
    id 406
    label "Alaska"
  ]
  node [
    id 407
    label "Arakan"
  ]
  node [
    id 408
    label "Hawaje"
  ]
  node [
    id 409
    label "Maryland"
  ]
  node [
    id 410
    label "Michigan"
  ]
  node [
    id 411
    label "Arizona"
  ]
  node [
    id 412
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 413
    label "Georgia"
  ]
  node [
    id 414
    label "poziom"
  ]
  node [
    id 415
    label "Pensylwania"
  ]
  node [
    id 416
    label "shape"
  ]
  node [
    id 417
    label "Luizjana"
  ]
  node [
    id 418
    label "Nowy_Meksyk"
  ]
  node [
    id 419
    label "Alabama"
  ]
  node [
    id 420
    label "ilo&#347;&#263;"
  ]
  node [
    id 421
    label "Kansas"
  ]
  node [
    id 422
    label "Oregon"
  ]
  node [
    id 423
    label "Floryda"
  ]
  node [
    id 424
    label "Oklahoma"
  ]
  node [
    id 425
    label "jednostka_administracyjna"
  ]
  node [
    id 426
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 427
    label "p&#243;&#322;rocze"
  ]
  node [
    id 428
    label "martwy_sezon"
  ]
  node [
    id 429
    label "kalendarz"
  ]
  node [
    id 430
    label "cykl_astronomiczny"
  ]
  node [
    id 431
    label "pora_roku"
  ]
  node [
    id 432
    label "stulecie"
  ]
  node [
    id 433
    label "kurs"
  ]
  node [
    id 434
    label "czas"
  ]
  node [
    id 435
    label "jubileusz"
  ]
  node [
    id 436
    label "kwarta&#322;"
  ]
  node [
    id 437
    label "miesi&#261;c"
  ]
  node [
    id 438
    label "summer"
  ]
  node [
    id 439
    label "poprzedzanie"
  ]
  node [
    id 440
    label "laba"
  ]
  node [
    id 441
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 442
    label "chronometria"
  ]
  node [
    id 443
    label "rachuba_czasu"
  ]
  node [
    id 444
    label "przep&#322;ywanie"
  ]
  node [
    id 445
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 446
    label "czasokres"
  ]
  node [
    id 447
    label "odczyt"
  ]
  node [
    id 448
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 449
    label "dzieje"
  ]
  node [
    id 450
    label "kategoria_gramatyczna"
  ]
  node [
    id 451
    label "poprzedzenie"
  ]
  node [
    id 452
    label "trawienie"
  ]
  node [
    id 453
    label "pochodzi&#263;"
  ]
  node [
    id 454
    label "period"
  ]
  node [
    id 455
    label "okres_czasu"
  ]
  node [
    id 456
    label "schy&#322;ek"
  ]
  node [
    id 457
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 458
    label "odwlekanie_si&#281;"
  ]
  node [
    id 459
    label "zegar"
  ]
  node [
    id 460
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 461
    label "czwarty_wymiar"
  ]
  node [
    id 462
    label "pochodzenie"
  ]
  node [
    id 463
    label "koniugacja"
  ]
  node [
    id 464
    label "Zeitgeist"
  ]
  node [
    id 465
    label "trawi&#263;"
  ]
  node [
    id 466
    label "pogoda"
  ]
  node [
    id 467
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 468
    label "poprzedzi&#263;"
  ]
  node [
    id 469
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 470
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 471
    label "time_period"
  ]
  node [
    id 472
    label "tydzie&#324;"
  ]
  node [
    id 473
    label "miech"
  ]
  node [
    id 474
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 475
    label "kalendy"
  ]
  node [
    id 476
    label "term"
  ]
  node [
    id 477
    label "rok_akademicki"
  ]
  node [
    id 478
    label "rok_szkolny"
  ]
  node [
    id 479
    label "semester"
  ]
  node [
    id 480
    label "anniwersarz"
  ]
  node [
    id 481
    label "rocznica"
  ]
  node [
    id 482
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 483
    label "long_time"
  ]
  node [
    id 484
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 485
    label "almanac"
  ]
  node [
    id 486
    label "rozk&#322;ad"
  ]
  node [
    id 487
    label "wydawnictwo"
  ]
  node [
    id 488
    label "Juliusz_Cezar"
  ]
  node [
    id 489
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 490
    label "zwy&#380;kowanie"
  ]
  node [
    id 491
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 492
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 493
    label "zaj&#281;cia"
  ]
  node [
    id 494
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 495
    label "trasa"
  ]
  node [
    id 496
    label "przeorientowywanie"
  ]
  node [
    id 497
    label "przejazd"
  ]
  node [
    id 498
    label "kierunek"
  ]
  node [
    id 499
    label "przeorientowywa&#263;"
  ]
  node [
    id 500
    label "nauka"
  ]
  node [
    id 501
    label "przeorientowanie"
  ]
  node [
    id 502
    label "klasa"
  ]
  node [
    id 503
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 504
    label "przeorientowa&#263;"
  ]
  node [
    id 505
    label "manner"
  ]
  node [
    id 506
    label "course"
  ]
  node [
    id 507
    label "passage"
  ]
  node [
    id 508
    label "zni&#380;kowanie"
  ]
  node [
    id 509
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 510
    label "seria"
  ]
  node [
    id 511
    label "stawka"
  ]
  node [
    id 512
    label "way"
  ]
  node [
    id 513
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 514
    label "spos&#243;b"
  ]
  node [
    id 515
    label "deprecjacja"
  ]
  node [
    id 516
    label "cedu&#322;a"
  ]
  node [
    id 517
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 518
    label "drive"
  ]
  node [
    id 519
    label "bearing"
  ]
  node [
    id 520
    label "Lira"
  ]
  node [
    id 521
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 522
    label "artykulator"
  ]
  node [
    id 523
    label "kod"
  ]
  node [
    id 524
    label "kawa&#322;ek"
  ]
  node [
    id 525
    label "przedmiot"
  ]
  node [
    id 526
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 527
    label "gramatyka"
  ]
  node [
    id 528
    label "stylik"
  ]
  node [
    id 529
    label "przet&#322;umaczenie"
  ]
  node [
    id 530
    label "formalizowanie"
  ]
  node [
    id 531
    label "ssanie"
  ]
  node [
    id 532
    label "ssa&#263;"
  ]
  node [
    id 533
    label "language"
  ]
  node [
    id 534
    label "liza&#263;"
  ]
  node [
    id 535
    label "napisa&#263;"
  ]
  node [
    id 536
    label "konsonantyzm"
  ]
  node [
    id 537
    label "wokalizm"
  ]
  node [
    id 538
    label "pisa&#263;"
  ]
  node [
    id 539
    label "fonetyka"
  ]
  node [
    id 540
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 541
    label "jeniec"
  ]
  node [
    id 542
    label "but"
  ]
  node [
    id 543
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 544
    label "po_koroniarsku"
  ]
  node [
    id 545
    label "kultura_duchowa"
  ]
  node [
    id 546
    label "t&#322;umaczenie"
  ]
  node [
    id 547
    label "m&#243;wienie"
  ]
  node [
    id 548
    label "pype&#263;"
  ]
  node [
    id 549
    label "lizanie"
  ]
  node [
    id 550
    label "formalizowa&#263;"
  ]
  node [
    id 551
    label "rozumie&#263;"
  ]
  node [
    id 552
    label "organ"
  ]
  node [
    id 553
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 554
    label "rozumienie"
  ]
  node [
    id 555
    label "makroglosja"
  ]
  node [
    id 556
    label "m&#243;wi&#263;"
  ]
  node [
    id 557
    label "jama_ustna"
  ]
  node [
    id 558
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 559
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 560
    label "natural_language"
  ]
  node [
    id 561
    label "s&#322;ownictwo"
  ]
  node [
    id 562
    label "urz&#261;dzenie"
  ]
  node [
    id 563
    label "kawa&#322;"
  ]
  node [
    id 564
    label "plot"
  ]
  node [
    id 565
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 566
    label "utw&#243;r"
  ]
  node [
    id 567
    label "piece"
  ]
  node [
    id 568
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 569
    label "podp&#322;ywanie"
  ]
  node [
    id 570
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 571
    label "narz&#281;dzie"
  ]
  node [
    id 572
    label "nature"
  ]
  node [
    id 573
    label "struktura"
  ]
  node [
    id 574
    label "code"
  ]
  node [
    id 575
    label "szyfrowanie"
  ]
  node [
    id 576
    label "ci&#261;g"
  ]
  node [
    id 577
    label "szablon"
  ]
  node [
    id 578
    label "&#380;o&#322;nierz"
  ]
  node [
    id 579
    label "internowanie"
  ]
  node [
    id 580
    label "ojczyc"
  ]
  node [
    id 581
    label "pojmaniec"
  ]
  node [
    id 582
    label "niewolnik"
  ]
  node [
    id 583
    label "internowa&#263;"
  ]
  node [
    id 584
    label "aparat_artykulacyjny"
  ]
  node [
    id 585
    label "tkanka"
  ]
  node [
    id 586
    label "jednostka_organizacyjna"
  ]
  node [
    id 587
    label "budowa"
  ]
  node [
    id 588
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 589
    label "tw&#243;r"
  ]
  node [
    id 590
    label "organogeneza"
  ]
  node [
    id 591
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 592
    label "struktura_anatomiczna"
  ]
  node [
    id 593
    label "uk&#322;ad"
  ]
  node [
    id 594
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 595
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 596
    label "Izba_Konsyliarska"
  ]
  node [
    id 597
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 598
    label "stomia"
  ]
  node [
    id 599
    label "dekortykacja"
  ]
  node [
    id 600
    label "okolica"
  ]
  node [
    id 601
    label "Komitet_Region&#243;w"
  ]
  node [
    id 602
    label "zboczenie"
  ]
  node [
    id 603
    label "om&#243;wienie"
  ]
  node [
    id 604
    label "sponiewieranie"
  ]
  node [
    id 605
    label "discipline"
  ]
  node [
    id 606
    label "rzecz"
  ]
  node [
    id 607
    label "omawia&#263;"
  ]
  node [
    id 608
    label "kr&#261;&#380;enie"
  ]
  node [
    id 609
    label "tre&#347;&#263;"
  ]
  node [
    id 610
    label "robienie"
  ]
  node [
    id 611
    label "sponiewiera&#263;"
  ]
  node [
    id 612
    label "element"
  ]
  node [
    id 613
    label "entity"
  ]
  node [
    id 614
    label "tematyka"
  ]
  node [
    id 615
    label "w&#261;tek"
  ]
  node [
    id 616
    label "charakter"
  ]
  node [
    id 617
    label "zbaczanie"
  ]
  node [
    id 618
    label "program_nauczania"
  ]
  node [
    id 619
    label "om&#243;wi&#263;"
  ]
  node [
    id 620
    label "omawianie"
  ]
  node [
    id 621
    label "thing"
  ]
  node [
    id 622
    label "kultura"
  ]
  node [
    id 623
    label "istota"
  ]
  node [
    id 624
    label "zbacza&#263;"
  ]
  node [
    id 625
    label "zboczy&#263;"
  ]
  node [
    id 626
    label "zapi&#281;tek"
  ]
  node [
    id 627
    label "sznurowad&#322;o"
  ]
  node [
    id 628
    label "rozbijarka"
  ]
  node [
    id 629
    label "podeszwa"
  ]
  node [
    id 630
    label "obcas"
  ]
  node [
    id 631
    label "wzu&#263;"
  ]
  node [
    id 632
    label "wzuwanie"
  ]
  node [
    id 633
    label "przyszwa"
  ]
  node [
    id 634
    label "raki"
  ]
  node [
    id 635
    label "cholewa"
  ]
  node [
    id 636
    label "cholewka"
  ]
  node [
    id 637
    label "zel&#243;wka"
  ]
  node [
    id 638
    label "obuwie"
  ]
  node [
    id 639
    label "napi&#281;tek"
  ]
  node [
    id 640
    label "wzucie"
  ]
  node [
    id 641
    label "kom&#243;rka"
  ]
  node [
    id 642
    label "furnishing"
  ]
  node [
    id 643
    label "zabezpieczenie"
  ]
  node [
    id 644
    label "zrobienie"
  ]
  node [
    id 645
    label "wyrz&#261;dzenie"
  ]
  node [
    id 646
    label "zagospodarowanie"
  ]
  node [
    id 647
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 648
    label "ig&#322;a"
  ]
  node [
    id 649
    label "wirnik"
  ]
  node [
    id 650
    label "aparatura"
  ]
  node [
    id 651
    label "system_energetyczny"
  ]
  node [
    id 652
    label "impulsator"
  ]
  node [
    id 653
    label "mechanizm"
  ]
  node [
    id 654
    label "sprz&#281;t"
  ]
  node [
    id 655
    label "czynno&#347;&#263;"
  ]
  node [
    id 656
    label "blokowanie"
  ]
  node [
    id 657
    label "set"
  ]
  node [
    id 658
    label "zablokowanie"
  ]
  node [
    id 659
    label "przygotowanie"
  ]
  node [
    id 660
    label "komora"
  ]
  node [
    id 661
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 662
    label "public_speaking"
  ]
  node [
    id 663
    label "powiadanie"
  ]
  node [
    id 664
    label "przepowiadanie"
  ]
  node [
    id 665
    label "wyznawanie"
  ]
  node [
    id 666
    label "wypowiadanie"
  ]
  node [
    id 667
    label "wydobywanie"
  ]
  node [
    id 668
    label "gaworzenie"
  ]
  node [
    id 669
    label "stosowanie"
  ]
  node [
    id 670
    label "wyra&#380;anie"
  ]
  node [
    id 671
    label "formu&#322;owanie"
  ]
  node [
    id 672
    label "dowalenie"
  ]
  node [
    id 673
    label "przerywanie"
  ]
  node [
    id 674
    label "wydawanie"
  ]
  node [
    id 675
    label "dogadywanie_si&#281;"
  ]
  node [
    id 676
    label "dodawanie"
  ]
  node [
    id 677
    label "prawienie"
  ]
  node [
    id 678
    label "opowiadanie"
  ]
  node [
    id 679
    label "ozywanie_si&#281;"
  ]
  node [
    id 680
    label "zapeszanie"
  ]
  node [
    id 681
    label "zwracanie_si&#281;"
  ]
  node [
    id 682
    label "dysfonia"
  ]
  node [
    id 683
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 684
    label "speaking"
  ]
  node [
    id 685
    label "zauwa&#380;enie"
  ]
  node [
    id 686
    label "mawianie"
  ]
  node [
    id 687
    label "opowiedzenie"
  ]
  node [
    id 688
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 689
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 690
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 691
    label "informowanie"
  ]
  node [
    id 692
    label "dogadanie_si&#281;"
  ]
  node [
    id 693
    label "wygadanie"
  ]
  node [
    id 694
    label "handwriting"
  ]
  node [
    id 695
    label "przekaz"
  ]
  node [
    id 696
    label "dzie&#322;o"
  ]
  node [
    id 697
    label "paleograf"
  ]
  node [
    id 698
    label "interpunkcja"
  ]
  node [
    id 699
    label "grafia"
  ]
  node [
    id 700
    label "script"
  ]
  node [
    id 701
    label "list"
  ]
  node [
    id 702
    label "adres"
  ]
  node [
    id 703
    label "ortografia"
  ]
  node [
    id 704
    label "letter"
  ]
  node [
    id 705
    label "komunikacja"
  ]
  node [
    id 706
    label "paleografia"
  ]
  node [
    id 707
    label "dokument"
  ]
  node [
    id 708
    label "terminology"
  ]
  node [
    id 709
    label "termin"
  ]
  node [
    id 710
    label "fleksja"
  ]
  node [
    id 711
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 712
    label "sk&#322;adnia"
  ]
  node [
    id 713
    label "morfologia"
  ]
  node [
    id 714
    label "g&#322;osownia"
  ]
  node [
    id 715
    label "zasymilowa&#263;"
  ]
  node [
    id 716
    label "phonetics"
  ]
  node [
    id 717
    label "palatogram"
  ]
  node [
    id 718
    label "transkrypcja"
  ]
  node [
    id 719
    label "zasymilowanie"
  ]
  node [
    id 720
    label "styl"
  ]
  node [
    id 721
    label "formu&#322;owa&#263;"
  ]
  node [
    id 722
    label "ozdabia&#263;"
  ]
  node [
    id 723
    label "stawia&#263;"
  ]
  node [
    id 724
    label "spell"
  ]
  node [
    id 725
    label "skryba"
  ]
  node [
    id 726
    label "read"
  ]
  node [
    id 727
    label "donosi&#263;"
  ]
  node [
    id 728
    label "tekst"
  ]
  node [
    id 729
    label "dysgrafia"
  ]
  node [
    id 730
    label "dysortografia"
  ]
  node [
    id 731
    label "stworzy&#263;"
  ]
  node [
    id 732
    label "postawi&#263;"
  ]
  node [
    id 733
    label "write"
  ]
  node [
    id 734
    label "donie&#347;&#263;"
  ]
  node [
    id 735
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 736
    label "explanation"
  ]
  node [
    id 737
    label "remark"
  ]
  node [
    id 738
    label "przek&#322;adanie"
  ]
  node [
    id 739
    label "zrozumia&#322;y"
  ]
  node [
    id 740
    label "przekonywanie"
  ]
  node [
    id 741
    label "uzasadnianie"
  ]
  node [
    id 742
    label "rozwianie"
  ]
  node [
    id 743
    label "rozwiewanie"
  ]
  node [
    id 744
    label "gossip"
  ]
  node [
    id 745
    label "przedstawianie"
  ]
  node [
    id 746
    label "rendition"
  ]
  node [
    id 747
    label "kr&#281;ty"
  ]
  node [
    id 748
    label "zinterpretowa&#263;"
  ]
  node [
    id 749
    label "put"
  ]
  node [
    id 750
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 751
    label "zrobi&#263;"
  ]
  node [
    id 752
    label "przekona&#263;"
  ]
  node [
    id 753
    label "frame"
  ]
  node [
    id 754
    label "poja&#347;nia&#263;"
  ]
  node [
    id 755
    label "u&#322;atwia&#263;"
  ]
  node [
    id 756
    label "elaborate"
  ]
  node [
    id 757
    label "give"
  ]
  node [
    id 758
    label "suplikowa&#263;"
  ]
  node [
    id 759
    label "przek&#322;ada&#263;"
  ]
  node [
    id 760
    label "przekonywa&#263;"
  ]
  node [
    id 761
    label "interpretowa&#263;"
  ]
  node [
    id 762
    label "explain"
  ]
  node [
    id 763
    label "przedstawia&#263;"
  ]
  node [
    id 764
    label "sprawowa&#263;"
  ]
  node [
    id 765
    label "uzasadnia&#263;"
  ]
  node [
    id 766
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 767
    label "gaworzy&#263;"
  ]
  node [
    id 768
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 769
    label "rozmawia&#263;"
  ]
  node [
    id 770
    label "wyra&#380;a&#263;"
  ]
  node [
    id 771
    label "umie&#263;"
  ]
  node [
    id 772
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 773
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 774
    label "express"
  ]
  node [
    id 775
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 776
    label "talk"
  ]
  node [
    id 777
    label "prawi&#263;"
  ]
  node [
    id 778
    label "powiada&#263;"
  ]
  node [
    id 779
    label "tell"
  ]
  node [
    id 780
    label "chew_the_fat"
  ]
  node [
    id 781
    label "say"
  ]
  node [
    id 782
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 783
    label "informowa&#263;"
  ]
  node [
    id 784
    label "wydobywa&#263;"
  ]
  node [
    id 785
    label "okre&#347;la&#263;"
  ]
  node [
    id 786
    label "hermeneutyka"
  ]
  node [
    id 787
    label "bycie"
  ]
  node [
    id 788
    label "kontekst"
  ]
  node [
    id 789
    label "apprehension"
  ]
  node [
    id 790
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 791
    label "interpretation"
  ]
  node [
    id 792
    label "obja&#347;nienie"
  ]
  node [
    id 793
    label "czucie"
  ]
  node [
    id 794
    label "realization"
  ]
  node [
    id 795
    label "kumanie"
  ]
  node [
    id 796
    label "wnioskowanie"
  ]
  node [
    id 797
    label "wiedzie&#263;"
  ]
  node [
    id 798
    label "kuma&#263;"
  ]
  node [
    id 799
    label "czu&#263;"
  ]
  node [
    id 800
    label "match"
  ]
  node [
    id 801
    label "empatia"
  ]
  node [
    id 802
    label "odbiera&#263;"
  ]
  node [
    id 803
    label "see"
  ]
  node [
    id 804
    label "zna&#263;"
  ]
  node [
    id 805
    label "validate"
  ]
  node [
    id 806
    label "nadawa&#263;"
  ]
  node [
    id 807
    label "precyzowa&#263;"
  ]
  node [
    id 808
    label "nadawanie"
  ]
  node [
    id 809
    label "precyzowanie"
  ]
  node [
    id 810
    label "formalny"
  ]
  node [
    id 811
    label "picie"
  ]
  node [
    id 812
    label "usta"
  ]
  node [
    id 813
    label "ruszanie"
  ]
  node [
    id 814
    label "&#347;lina"
  ]
  node [
    id 815
    label "consumption"
  ]
  node [
    id 816
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 817
    label "rozpuszczanie"
  ]
  node [
    id 818
    label "aspiration"
  ]
  node [
    id 819
    label "wci&#261;ganie"
  ]
  node [
    id 820
    label "odci&#261;ganie"
  ]
  node [
    id 821
    label "wessanie"
  ]
  node [
    id 822
    label "ga&#378;nik"
  ]
  node [
    id 823
    label "wysysanie"
  ]
  node [
    id 824
    label "wyssanie"
  ]
  node [
    id 825
    label "wada_wrodzona"
  ]
  node [
    id 826
    label "znami&#281;"
  ]
  node [
    id 827
    label "krosta"
  ]
  node [
    id 828
    label "spot"
  ]
  node [
    id 829
    label "schorzenie"
  ]
  node [
    id 830
    label "brodawka"
  ]
  node [
    id 831
    label "pip"
  ]
  node [
    id 832
    label "dotykanie"
  ]
  node [
    id 833
    label "przesuwanie"
  ]
  node [
    id 834
    label "zlizanie"
  ]
  node [
    id 835
    label "g&#322;askanie"
  ]
  node [
    id 836
    label "wylizywanie"
  ]
  node [
    id 837
    label "zlizywanie"
  ]
  node [
    id 838
    label "wylizanie"
  ]
  node [
    id 839
    label "pi&#263;"
  ]
  node [
    id 840
    label "sponge"
  ]
  node [
    id 841
    label "mleko"
  ]
  node [
    id 842
    label "rozpuszcza&#263;"
  ]
  node [
    id 843
    label "wci&#261;ga&#263;"
  ]
  node [
    id 844
    label "rusza&#263;"
  ]
  node [
    id 845
    label "sucking"
  ]
  node [
    id 846
    label "smoczek"
  ]
  node [
    id 847
    label "salt_lick"
  ]
  node [
    id 848
    label "dotyka&#263;"
  ]
  node [
    id 849
    label "muska&#263;"
  ]
  node [
    id 850
    label "Polish"
  ]
  node [
    id 851
    label "goniony"
  ]
  node [
    id 852
    label "oberek"
  ]
  node [
    id 853
    label "ryba_po_grecku"
  ]
  node [
    id 854
    label "sztajer"
  ]
  node [
    id 855
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 856
    label "krakowiak"
  ]
  node [
    id 857
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 858
    label "pierogi_ruskie"
  ]
  node [
    id 859
    label "lacki"
  ]
  node [
    id 860
    label "polak"
  ]
  node [
    id 861
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 862
    label "chodzony"
  ]
  node [
    id 863
    label "po_polsku"
  ]
  node [
    id 864
    label "mazur"
  ]
  node [
    id 865
    label "polsko"
  ]
  node [
    id 866
    label "skoczny"
  ]
  node [
    id 867
    label "drabant"
  ]
  node [
    id 868
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 869
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 870
    label "wschodnioeuropejski"
  ]
  node [
    id 871
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 872
    label "poga&#324;ski"
  ]
  node [
    id 873
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 874
    label "topielec"
  ]
  node [
    id 875
    label "europejski"
  ]
  node [
    id 876
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 877
    label "langosz"
  ]
  node [
    id 878
    label "gwardzista"
  ]
  node [
    id 879
    label "melodia"
  ]
  node [
    id 880
    label "taniec"
  ]
  node [
    id 881
    label "taniec_ludowy"
  ]
  node [
    id 882
    label "&#347;redniowieczny"
  ]
  node [
    id 883
    label "europejsko"
  ]
  node [
    id 884
    label "specjalny"
  ]
  node [
    id 885
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 886
    label "weso&#322;y"
  ]
  node [
    id 887
    label "sprawny"
  ]
  node [
    id 888
    label "rytmiczny"
  ]
  node [
    id 889
    label "skocznie"
  ]
  node [
    id 890
    label "energiczny"
  ]
  node [
    id 891
    label "przytup"
  ]
  node [
    id 892
    label "ho&#322;ubiec"
  ]
  node [
    id 893
    label "wodzi&#263;"
  ]
  node [
    id 894
    label "lendler"
  ]
  node [
    id 895
    label "austriacki"
  ]
  node [
    id 896
    label "polka"
  ]
  node [
    id 897
    label "ludowy"
  ]
  node [
    id 898
    label "pie&#347;&#324;"
  ]
  node [
    id 899
    label "mieszkaniec"
  ]
  node [
    id 900
    label "centu&#347;"
  ]
  node [
    id 901
    label "lalka"
  ]
  node [
    id 902
    label "Ma&#322;opolanin"
  ]
  node [
    id 903
    label "krakauer"
  ]
  node [
    id 904
    label "d&#322;ugi"
  ]
  node [
    id 905
    label "d&#322;ugotrwa&#322;y"
  ]
  node [
    id 906
    label "d&#322;ugotrwale"
  ]
  node [
    id 907
    label "daleki"
  ]
  node [
    id 908
    label "ruch"
  ]
  node [
    id 909
    label "d&#322;ugo"
  ]
  node [
    id 910
    label "mass-media"
  ]
  node [
    id 911
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 912
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 913
    label "przekazior"
  ]
  node [
    id 914
    label "uzbrajanie"
  ]
  node [
    id 915
    label "medium"
  ]
  node [
    id 916
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 917
    label "&#347;rodek"
  ]
  node [
    id 918
    label "jasnowidz"
  ]
  node [
    id 919
    label "hipnoza"
  ]
  node [
    id 920
    label "cz&#322;owiek"
  ]
  node [
    id 921
    label "spirytysta"
  ]
  node [
    id 922
    label "otoczenie"
  ]
  node [
    id 923
    label "publikator"
  ]
  node [
    id 924
    label "warunki"
  ]
  node [
    id 925
    label "przeka&#378;nik"
  ]
  node [
    id 926
    label "&#347;rodek_przekazu"
  ]
  node [
    id 927
    label "armament"
  ]
  node [
    id 928
    label "arming"
  ]
  node [
    id 929
    label "instalacja"
  ]
  node [
    id 930
    label "wyposa&#380;anie"
  ]
  node [
    id 931
    label "dozbrajanie"
  ]
  node [
    id 932
    label "dozbrojenie"
  ]
  node [
    id 933
    label "montowanie"
  ]
  node [
    id 934
    label "rozdzia&#322;"
  ]
  node [
    id 935
    label "Koran"
  ]
  node [
    id 936
    label "interruption"
  ]
  node [
    id 937
    label "podzia&#322;"
  ]
  node [
    id 938
    label "podrozdzia&#322;"
  ]
  node [
    id 939
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 940
    label "hurysa"
  ]
  node [
    id 941
    label "hid&#380;ab"
  ]
  node [
    id 942
    label "szariat"
  ]
  node [
    id 943
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 944
    label "bliski"
  ]
  node [
    id 945
    label "blisko"
  ]
  node [
    id 946
    label "znajomy"
  ]
  node [
    id 947
    label "zwi&#261;zany"
  ]
  node [
    id 948
    label "przesz&#322;y"
  ]
  node [
    id 949
    label "silny"
  ]
  node [
    id 950
    label "zbli&#380;enie"
  ]
  node [
    id 951
    label "kr&#243;tki"
  ]
  node [
    id 952
    label "oddalony"
  ]
  node [
    id 953
    label "dok&#322;adny"
  ]
  node [
    id 954
    label "nieodleg&#322;y"
  ]
  node [
    id 955
    label "przysz&#322;y"
  ]
  node [
    id 956
    label "gotowy"
  ]
  node [
    id 957
    label "ma&#322;y"
  ]
  node [
    id 958
    label "proszek"
  ]
  node [
    id 959
    label "tablet"
  ]
  node [
    id 960
    label "dawka"
  ]
  node [
    id 961
    label "blister"
  ]
  node [
    id 962
    label "lekarstwo"
  ]
  node [
    id 963
    label "parametr"
  ]
  node [
    id 964
    label "liczba"
  ]
  node [
    id 965
    label "dane"
  ]
  node [
    id 966
    label "znaczenie"
  ]
  node [
    id 967
    label "wielko&#347;&#263;"
  ]
  node [
    id 968
    label "dymensja"
  ]
  node [
    id 969
    label "charakterystyka"
  ]
  node [
    id 970
    label "m&#322;ot"
  ]
  node [
    id 971
    label "znak"
  ]
  node [
    id 972
    label "drzewo"
  ]
  node [
    id 973
    label "pr&#243;ba"
  ]
  node [
    id 974
    label "attribute"
  ]
  node [
    id 975
    label "marka"
  ]
  node [
    id 976
    label "rozmiar"
  ]
  node [
    id 977
    label "rzadko&#347;&#263;"
  ]
  node [
    id 978
    label "zaleta"
  ]
  node [
    id 979
    label "measure"
  ]
  node [
    id 980
    label "opinia"
  ]
  node [
    id 981
    label "poj&#281;cie"
  ]
  node [
    id 982
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 983
    label "zdolno&#347;&#263;"
  ]
  node [
    id 984
    label "potencja"
  ]
  node [
    id 985
    label "property"
  ]
  node [
    id 986
    label "po&#322;o&#380;enie"
  ]
  node [
    id 987
    label "jako&#347;&#263;"
  ]
  node [
    id 988
    label "p&#322;aszczyzna"
  ]
  node [
    id 989
    label "punkt_widzenia"
  ]
  node [
    id 990
    label "wyk&#322;adnik"
  ]
  node [
    id 991
    label "szczebel"
  ]
  node [
    id 992
    label "budynek"
  ]
  node [
    id 993
    label "wysoko&#347;&#263;"
  ]
  node [
    id 994
    label "ranga"
  ]
  node [
    id 995
    label "odk&#322;adanie"
  ]
  node [
    id 996
    label "condition"
  ]
  node [
    id 997
    label "liczenie"
  ]
  node [
    id 998
    label "stawianie"
  ]
  node [
    id 999
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 1000
    label "assay"
  ]
  node [
    id 1001
    label "wskazywanie"
  ]
  node [
    id 1002
    label "wyraz"
  ]
  node [
    id 1003
    label "gravity"
  ]
  node [
    id 1004
    label "weight"
  ]
  node [
    id 1005
    label "command"
  ]
  node [
    id 1006
    label "odgrywanie_roli"
  ]
  node [
    id 1007
    label "informacja"
  ]
  node [
    id 1008
    label "okre&#347;lanie"
  ]
  node [
    id 1009
    label "kto&#347;"
  ]
  node [
    id 1010
    label "wyra&#380;enie"
  ]
  node [
    id 1011
    label "edytowa&#263;"
  ]
  node [
    id 1012
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1013
    label "spakowanie"
  ]
  node [
    id 1014
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1015
    label "pakowa&#263;"
  ]
  node [
    id 1016
    label "rekord"
  ]
  node [
    id 1017
    label "korelator"
  ]
  node [
    id 1018
    label "wyci&#261;ganie"
  ]
  node [
    id 1019
    label "pakowanie"
  ]
  node [
    id 1020
    label "sekwencjonowa&#263;"
  ]
  node [
    id 1021
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 1022
    label "jednostka_informacji"
  ]
  node [
    id 1023
    label "evidence"
  ]
  node [
    id 1024
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 1025
    label "rozpakowywanie"
  ]
  node [
    id 1026
    label "rozpakowanie"
  ]
  node [
    id 1027
    label "rozpakowywa&#263;"
  ]
  node [
    id 1028
    label "konwersja"
  ]
  node [
    id 1029
    label "nap&#322;ywanie"
  ]
  node [
    id 1030
    label "rozpakowa&#263;"
  ]
  node [
    id 1031
    label "spakowa&#263;"
  ]
  node [
    id 1032
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 1033
    label "edytowanie"
  ]
  node [
    id 1034
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 1035
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 1036
    label "sekwencjonowanie"
  ]
  node [
    id 1037
    label "kategoria"
  ]
  node [
    id 1038
    label "pierwiastek"
  ]
  node [
    id 1039
    label "number"
  ]
  node [
    id 1040
    label "kwadrat_magiczny"
  ]
  node [
    id 1041
    label "part"
  ]
  node [
    id 1042
    label "zmienna"
  ]
  node [
    id 1043
    label "tradycyjny"
  ]
  node [
    id 1044
    label "regionalnie"
  ]
  node [
    id 1045
    label "lokalny"
  ]
  node [
    id 1046
    label "typowy"
  ]
  node [
    id 1047
    label "lokalnie"
  ]
  node [
    id 1048
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1049
    label "zwyczajny"
  ]
  node [
    id 1050
    label "typowo"
  ]
  node [
    id 1051
    label "cz&#281;sty"
  ]
  node [
    id 1052
    label "modelowy"
  ]
  node [
    id 1053
    label "tradycyjnie"
  ]
  node [
    id 1054
    label "surowy"
  ]
  node [
    id 1055
    label "zachowawczy"
  ]
  node [
    id 1056
    label "nienowoczesny"
  ]
  node [
    id 1057
    label "przyj&#281;ty"
  ]
  node [
    id 1058
    label "wierny"
  ]
  node [
    id 1059
    label "zwyczajowy"
  ]
  node [
    id 1060
    label "nasamprz&#243;d"
  ]
  node [
    id 1061
    label "pierw"
  ]
  node [
    id 1062
    label "pocz&#261;tkowo"
  ]
  node [
    id 1063
    label "pierwiej"
  ]
  node [
    id 1064
    label "wcze&#347;niej"
  ]
  node [
    id 1065
    label "dzieci&#281;co"
  ]
  node [
    id 1066
    label "zrazu"
  ]
  node [
    id 1067
    label "wcze&#347;niejszy"
  ]
  node [
    id 1068
    label "priorytetowo"
  ]
  node [
    id 1069
    label "wite&#378;"
  ]
  node [
    id 1070
    label "wojownik"
  ]
  node [
    id 1071
    label "osada"
  ]
  node [
    id 1072
    label "walcz&#261;cy"
  ]
  node [
    id 1073
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 1074
    label "harcap"
  ]
  node [
    id 1075
    label "wojsko"
  ]
  node [
    id 1076
    label "elew"
  ]
  node [
    id 1077
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 1078
    label "demobilizowanie"
  ]
  node [
    id 1079
    label "demobilizowa&#263;"
  ]
  node [
    id 1080
    label "zdemobilizowanie"
  ]
  node [
    id 1081
    label "Gurkha"
  ]
  node [
    id 1082
    label "so&#322;dat"
  ]
  node [
    id 1083
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 1084
    label "mundurowy"
  ]
  node [
    id 1085
    label "rota"
  ]
  node [
    id 1086
    label "zdemobilizowa&#263;"
  ]
  node [
    id 1087
    label "&#380;o&#322;dowy"
  ]
  node [
    id 1088
    label "motyl_dzienny"
  ]
  node [
    id 1089
    label "paziowate"
  ]
  node [
    id 1090
    label "p&#243;&#378;ny"
  ]
  node [
    id 1091
    label "do_p&#243;&#378;na"
  ]
  node [
    id 1092
    label "po_dolno&#347;l&#261;sku"
  ]
  node [
    id 1093
    label "&#347;l&#261;ski"
  ]
  node [
    id 1094
    label "cug"
  ]
  node [
    id 1095
    label "krepel"
  ]
  node [
    id 1096
    label "mietlorz"
  ]
  node [
    id 1097
    label "francuz"
  ]
  node [
    id 1098
    label "etnolekt"
  ]
  node [
    id 1099
    label "sza&#322;ot"
  ]
  node [
    id 1100
    label "halba"
  ]
  node [
    id 1101
    label "ch&#322;opiec"
  ]
  node [
    id 1102
    label "buchta"
  ]
  node [
    id 1103
    label "czarne_kluski"
  ]
  node [
    id 1104
    label "szpajza"
  ]
  node [
    id 1105
    label "szl&#261;ski"
  ]
  node [
    id 1106
    label "&#347;lonski"
  ]
  node [
    id 1107
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 1108
    label "waloszek"
  ]
  node [
    id 1109
    label "godzina"
  ]
  node [
    id 1110
    label "time"
  ]
  node [
    id 1111
    label "doba"
  ]
  node [
    id 1112
    label "p&#243;&#322;godzina"
  ]
  node [
    id 1113
    label "jednostka_czasu"
  ]
  node [
    id 1114
    label "minuta"
  ]
  node [
    id 1115
    label "kwadrans"
  ]
  node [
    id 1116
    label "impression"
  ]
  node [
    id 1117
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1118
    label "odmiana"
  ]
  node [
    id 1119
    label "cykl"
  ]
  node [
    id 1120
    label "notification"
  ]
  node [
    id 1121
    label "zmiana"
  ]
  node [
    id 1122
    label "produkcja"
  ]
  node [
    id 1123
    label "mutant"
  ]
  node [
    id 1124
    label "rewizja"
  ]
  node [
    id 1125
    label "typ"
  ]
  node [
    id 1126
    label "paradygmat"
  ]
  node [
    id 1127
    label "change"
  ]
  node [
    id 1128
    label "podgatunek"
  ]
  node [
    id 1129
    label "ferment"
  ]
  node [
    id 1130
    label "rasa"
  ]
  node [
    id 1131
    label "zjawisko"
  ]
  node [
    id 1132
    label "realizacja"
  ]
  node [
    id 1133
    label "tingel-tangel"
  ]
  node [
    id 1134
    label "wydawa&#263;"
  ]
  node [
    id 1135
    label "numer"
  ]
  node [
    id 1136
    label "monta&#380;"
  ]
  node [
    id 1137
    label "wyda&#263;"
  ]
  node [
    id 1138
    label "postprodukcja"
  ]
  node [
    id 1139
    label "performance"
  ]
  node [
    id 1140
    label "fabrication"
  ]
  node [
    id 1141
    label "product"
  ]
  node [
    id 1142
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1143
    label "uzysk"
  ]
  node [
    id 1144
    label "rozw&#243;j"
  ]
  node [
    id 1145
    label "odtworzenie"
  ]
  node [
    id 1146
    label "dorobek"
  ]
  node [
    id 1147
    label "kreacja"
  ]
  node [
    id 1148
    label "trema"
  ]
  node [
    id 1149
    label "creation"
  ]
  node [
    id 1150
    label "kooperowa&#263;"
  ]
  node [
    id 1151
    label "czynnik_biotyczny"
  ]
  node [
    id 1152
    label "wyewoluowanie"
  ]
  node [
    id 1153
    label "reakcja"
  ]
  node [
    id 1154
    label "individual"
  ]
  node [
    id 1155
    label "przyswoi&#263;"
  ]
  node [
    id 1156
    label "starzenie_si&#281;"
  ]
  node [
    id 1157
    label "wyewoluowa&#263;"
  ]
  node [
    id 1158
    label "okaz"
  ]
  node [
    id 1159
    label "przyswojenie"
  ]
  node [
    id 1160
    label "ewoluowanie"
  ]
  node [
    id 1161
    label "ewoluowa&#263;"
  ]
  node [
    id 1162
    label "sztuka"
  ]
  node [
    id 1163
    label "agent"
  ]
  node [
    id 1164
    label "przyswaja&#263;"
  ]
  node [
    id 1165
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 1166
    label "nicpo&#324;"
  ]
  node [
    id 1167
    label "przyswajanie"
  ]
  node [
    id 1168
    label "oznaka"
  ]
  node [
    id 1169
    label "komplet"
  ]
  node [
    id 1170
    label "anatomopatolog"
  ]
  node [
    id 1171
    label "zmianka"
  ]
  node [
    id 1172
    label "amendment"
  ]
  node [
    id 1173
    label "odmienianie"
  ]
  node [
    id 1174
    label "tura"
  ]
  node [
    id 1175
    label "przebieg"
  ]
  node [
    id 1176
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 1177
    label "miesi&#261;czka"
  ]
  node [
    id 1178
    label "okres"
  ]
  node [
    id 1179
    label "owulacja"
  ]
  node [
    id 1180
    label "sekwencja"
  ]
  node [
    id 1181
    label "cycle"
  ]
  node [
    id 1182
    label "czyj&#347;"
  ]
  node [
    id 1183
    label "m&#261;&#380;"
  ]
  node [
    id 1184
    label "prywatny"
  ]
  node [
    id 1185
    label "ma&#322;&#380;onek"
  ]
  node [
    id 1186
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1187
    label "ch&#322;op"
  ]
  node [
    id 1188
    label "pan_m&#322;ody"
  ]
  node [
    id 1189
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1190
    label "&#347;lubny"
  ]
  node [
    id 1191
    label "pan_domu"
  ]
  node [
    id 1192
    label "pan_i_w&#322;adca"
  ]
  node [
    id 1193
    label "stary"
  ]
  node [
    id 1194
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1195
    label "p&#243;&#322;noc"
  ]
  node [
    id 1196
    label "Kosowo"
  ]
  node [
    id 1197
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1198
    label "Zab&#322;ocie"
  ]
  node [
    id 1199
    label "zach&#243;d"
  ]
  node [
    id 1200
    label "po&#322;udnie"
  ]
  node [
    id 1201
    label "Pow&#261;zki"
  ]
  node [
    id 1202
    label "Piotrowo"
  ]
  node [
    id 1203
    label "Olszanica"
  ]
  node [
    id 1204
    label "holarktyka"
  ]
  node [
    id 1205
    label "Ruda_Pabianicka"
  ]
  node [
    id 1206
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1207
    label "Ludwin&#243;w"
  ]
  node [
    id 1208
    label "Arktyka"
  ]
  node [
    id 1209
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1210
    label "Zabu&#380;e"
  ]
  node [
    id 1211
    label "antroposfera"
  ]
  node [
    id 1212
    label "terytorium"
  ]
  node [
    id 1213
    label "Neogea"
  ]
  node [
    id 1214
    label "Syberia_Zachodnia"
  ]
  node [
    id 1215
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1216
    label "zakres"
  ]
  node [
    id 1217
    label "pas_planetoid"
  ]
  node [
    id 1218
    label "Syberia_Wschodnia"
  ]
  node [
    id 1219
    label "Antarktyka"
  ]
  node [
    id 1220
    label "Rakowice"
  ]
  node [
    id 1221
    label "akrecja"
  ]
  node [
    id 1222
    label "&#321;&#281;g"
  ]
  node [
    id 1223
    label "Kresy_Zachodnie"
  ]
  node [
    id 1224
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1225
    label "wsch&#243;d"
  ]
  node [
    id 1226
    label "Notogea"
  ]
  node [
    id 1227
    label "ton"
  ]
  node [
    id 1228
    label "odcinek"
  ]
  node [
    id 1229
    label "ambitus"
  ]
  node [
    id 1230
    label "skala"
  ]
  node [
    id 1231
    label "og&#243;lnokrajowy"
  ]
  node [
    id 1232
    label "og&#243;lnopolsko"
  ]
  node [
    id 1233
    label "og&#243;lnokrajowo"
  ]
  node [
    id 1234
    label "generalny"
  ]
  node [
    id 1235
    label "ustawienie"
  ]
  node [
    id 1236
    label "mode"
  ]
  node [
    id 1237
    label "przesada"
  ]
  node [
    id 1238
    label "gra"
  ]
  node [
    id 1239
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1240
    label "ustalenie"
  ]
  node [
    id 1241
    label "erection"
  ]
  node [
    id 1242
    label "setup"
  ]
  node [
    id 1243
    label "spowodowanie"
  ]
  node [
    id 1244
    label "erecting"
  ]
  node [
    id 1245
    label "rozmieszczenie"
  ]
  node [
    id 1246
    label "poustawianie"
  ]
  node [
    id 1247
    label "zinterpretowanie"
  ]
  node [
    id 1248
    label "porozstawianie"
  ]
  node [
    id 1249
    label "rola"
  ]
  node [
    id 1250
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 1251
    label "zmienno&#347;&#263;"
  ]
  node [
    id 1252
    label "play"
  ]
  node [
    id 1253
    label "rozgrywka"
  ]
  node [
    id 1254
    label "apparent_motion"
  ]
  node [
    id 1255
    label "contest"
  ]
  node [
    id 1256
    label "akcja"
  ]
  node [
    id 1257
    label "zabawa"
  ]
  node [
    id 1258
    label "zasada"
  ]
  node [
    id 1259
    label "rywalizacja"
  ]
  node [
    id 1260
    label "zbijany"
  ]
  node [
    id 1261
    label "game"
  ]
  node [
    id 1262
    label "odg&#322;os"
  ]
  node [
    id 1263
    label "Pok&#233;mon"
  ]
  node [
    id 1264
    label "synteza"
  ]
  node [
    id 1265
    label "rekwizyt_do_gry"
  ]
  node [
    id 1266
    label "sztuczno&#347;&#263;"
  ]
  node [
    id 1267
    label "nadmiar"
  ]
  node [
    id 1268
    label "jab&#322;ko"
  ]
  node [
    id 1269
    label "ceremonia"
  ]
  node [
    id 1270
    label "jab&#322;o&#324;_domowa"
  ]
  node [
    id 1271
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 1272
    label "dba&#322;o&#347;&#263;"
  ]
  node [
    id 1273
    label "klejnot"
  ]
  node [
    id 1274
    label "wielopestkowiec"
  ]
  node [
    id 1275
    label "regalia"
  ]
  node [
    id 1276
    label "sk&#243;ra"
  ]
  node [
    id 1277
    label "ogonek"
  ]
  node [
    id 1278
    label "owoc"
  ]
  node [
    id 1279
    label "opatrywa&#263;"
  ]
  node [
    id 1280
    label "oznacza&#263;"
  ]
  node [
    id 1281
    label "bandage"
  ]
  node [
    id 1282
    label "dopowiada&#263;"
  ]
  node [
    id 1283
    label "revise"
  ]
  node [
    id 1284
    label "zabezpiecza&#263;"
  ]
  node [
    id 1285
    label "przywraca&#263;"
  ]
  node [
    id 1286
    label "dress"
  ]
  node [
    id 1287
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 1288
    label "kilkudniowy"
  ]
  node [
    id 1289
    label "sprawno&#347;&#263;"
  ]
  node [
    id 1290
    label "st&#243;&#322;_roboczy"
  ]
  node [
    id 1291
    label "wyposa&#380;enie"
  ]
  node [
    id 1292
    label "pracownia"
  ]
  node [
    id 1293
    label "szybko&#347;&#263;"
  ]
  node [
    id 1294
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1295
    label "kondycja_fizyczna"
  ]
  node [
    id 1296
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1297
    label "zdrowie"
  ]
  node [
    id 1298
    label "harcerski"
  ]
  node [
    id 1299
    label "odznaka"
  ]
  node [
    id 1300
    label "danie"
  ]
  node [
    id 1301
    label "fixture"
  ]
  node [
    id 1302
    label "zinformatyzowanie"
  ]
  node [
    id 1303
    label "zainstalowanie"
  ]
  node [
    id 1304
    label "doznanie"
  ]
  node [
    id 1305
    label "gathering"
  ]
  node [
    id 1306
    label "zawarcie"
  ]
  node [
    id 1307
    label "powitanie"
  ]
  node [
    id 1308
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1309
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1310
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 1311
    label "znalezienie"
  ]
  node [
    id 1312
    label "employment"
  ]
  node [
    id 1313
    label "po&#380;egnanie"
  ]
  node [
    id 1314
    label "gather"
  ]
  node [
    id 1315
    label "spotykanie"
  ]
  node [
    id 1316
    label "spotkanie_si&#281;"
  ]
  node [
    id 1317
    label "pomieszczenie"
  ]
  node [
    id 1318
    label "zawodowy"
  ]
  node [
    id 1319
    label "dziennikarsko"
  ]
  node [
    id 1320
    label "tre&#347;ciwy"
  ]
  node [
    id 1321
    label "po_dziennikarsku"
  ]
  node [
    id 1322
    label "wzorowy"
  ]
  node [
    id 1323
    label "obiektywny"
  ]
  node [
    id 1324
    label "rzetelny"
  ]
  node [
    id 1325
    label "doskona&#322;y"
  ]
  node [
    id 1326
    label "przyk&#322;adny"
  ]
  node [
    id 1327
    label "&#322;adny"
  ]
  node [
    id 1328
    label "wzorowo"
  ]
  node [
    id 1329
    label "czadowy"
  ]
  node [
    id 1330
    label "fachowy"
  ]
  node [
    id 1331
    label "fajny"
  ]
  node [
    id 1332
    label "klawy"
  ]
  node [
    id 1333
    label "zawodowo"
  ]
  node [
    id 1334
    label "s&#322;u&#380;bowo"
  ]
  node [
    id 1335
    label "zawo&#322;any"
  ]
  node [
    id 1336
    label "profesjonalny"
  ]
  node [
    id 1337
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1338
    label "nale&#380;ny"
  ]
  node [
    id 1339
    label "nale&#380;yty"
  ]
  node [
    id 1340
    label "uprawniony"
  ]
  node [
    id 1341
    label "zasadniczy"
  ]
  node [
    id 1342
    label "stosownie"
  ]
  node [
    id 1343
    label "taki"
  ]
  node [
    id 1344
    label "charakterystyczny"
  ]
  node [
    id 1345
    label "prawdziwy"
  ]
  node [
    id 1346
    label "ten"
  ]
  node [
    id 1347
    label "syc&#261;cy"
  ]
  node [
    id 1348
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 1349
    label "tre&#347;ciwie"
  ]
  node [
    id 1350
    label "zgrabny"
  ]
  node [
    id 1351
    label "g&#281;sty"
  ]
  node [
    id 1352
    label "rzetelnie"
  ]
  node [
    id 1353
    label "przekonuj&#261;cy"
  ]
  node [
    id 1354
    label "porz&#261;dny"
  ]
  node [
    id 1355
    label "uczciwy"
  ]
  node [
    id 1356
    label "obiektywizowanie"
  ]
  node [
    id 1357
    label "zobiektywizowanie"
  ]
  node [
    id 1358
    label "niezale&#380;ny"
  ]
  node [
    id 1359
    label "bezsporny"
  ]
  node [
    id 1360
    label "obiektywnie"
  ]
  node [
    id 1361
    label "neutralny"
  ]
  node [
    id 1362
    label "faktyczny"
  ]
  node [
    id 1363
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 1364
    label "cz&#322;onek"
  ]
  node [
    id 1365
    label "przyk&#322;ad"
  ]
  node [
    id 1366
    label "substytuowa&#263;"
  ]
  node [
    id 1367
    label "substytuowanie"
  ]
  node [
    id 1368
    label "zast&#281;pca"
  ]
  node [
    id 1369
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1370
    label "wapniak"
  ]
  node [
    id 1371
    label "os&#322;abia&#263;"
  ]
  node [
    id 1372
    label "hominid"
  ]
  node [
    id 1373
    label "podw&#322;adny"
  ]
  node [
    id 1374
    label "os&#322;abianie"
  ]
  node [
    id 1375
    label "g&#322;owa"
  ]
  node [
    id 1376
    label "figura"
  ]
  node [
    id 1377
    label "portrecista"
  ]
  node [
    id 1378
    label "dwun&#243;g"
  ]
  node [
    id 1379
    label "profanum"
  ]
  node [
    id 1380
    label "mikrokosmos"
  ]
  node [
    id 1381
    label "nasada"
  ]
  node [
    id 1382
    label "duch"
  ]
  node [
    id 1383
    label "antropochoria"
  ]
  node [
    id 1384
    label "osoba"
  ]
  node [
    id 1385
    label "wz&#243;r"
  ]
  node [
    id 1386
    label "senior"
  ]
  node [
    id 1387
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1388
    label "Adam"
  ]
  node [
    id 1389
    label "homo_sapiens"
  ]
  node [
    id 1390
    label "polifag"
  ]
  node [
    id 1391
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 1392
    label "ptaszek"
  ]
  node [
    id 1393
    label "organizacja"
  ]
  node [
    id 1394
    label "element_anatomiczny"
  ]
  node [
    id 1395
    label "przyrodzenie"
  ]
  node [
    id 1396
    label "fiut"
  ]
  node [
    id 1397
    label "shaft"
  ]
  node [
    id 1398
    label "wchodzenie"
  ]
  node [
    id 1399
    label "pe&#322;nomocnik"
  ]
  node [
    id 1400
    label "podstawienie"
  ]
  node [
    id 1401
    label "wskazanie"
  ]
  node [
    id 1402
    label "podstawianie"
  ]
  node [
    id 1403
    label "wskaza&#263;"
  ]
  node [
    id 1404
    label "podstawi&#263;"
  ]
  node [
    id 1405
    label "wskazywa&#263;"
  ]
  node [
    id 1406
    label "zast&#261;pi&#263;"
  ]
  node [
    id 1407
    label "zast&#281;powa&#263;"
  ]
  node [
    id 1408
    label "podstawia&#263;"
  ]
  node [
    id 1409
    label "protezowa&#263;"
  ]
  node [
    id 1410
    label "fakt"
  ]
  node [
    id 1411
    label "czyn"
  ]
  node [
    id 1412
    label "ilustracja"
  ]
  node [
    id 1413
    label "dobroczynny"
  ]
  node [
    id 1414
    label "czw&#243;rka"
  ]
  node [
    id 1415
    label "spokojny"
  ]
  node [
    id 1416
    label "skuteczny"
  ]
  node [
    id 1417
    label "&#347;mieszny"
  ]
  node [
    id 1418
    label "mi&#322;y"
  ]
  node [
    id 1419
    label "grzeczny"
  ]
  node [
    id 1420
    label "dobrze"
  ]
  node [
    id 1421
    label "ca&#322;y"
  ]
  node [
    id 1422
    label "zwrot"
  ]
  node [
    id 1423
    label "pomy&#347;lny"
  ]
  node [
    id 1424
    label "moralny"
  ]
  node [
    id 1425
    label "drogi"
  ]
  node [
    id 1426
    label "pozytywny"
  ]
  node [
    id 1427
    label "odpowiedni"
  ]
  node [
    id 1428
    label "korzystny"
  ]
  node [
    id 1429
    label "pos&#322;uszny"
  ]
  node [
    id 1430
    label "moralnie"
  ]
  node [
    id 1431
    label "warto&#347;ciowy"
  ]
  node [
    id 1432
    label "etycznie"
  ]
  node [
    id 1433
    label "pozytywnie"
  ]
  node [
    id 1434
    label "dodatnio"
  ]
  node [
    id 1435
    label "przyjemny"
  ]
  node [
    id 1436
    label "po&#380;&#261;dany"
  ]
  node [
    id 1437
    label "niepowa&#380;ny"
  ]
  node [
    id 1438
    label "o&#347;mieszanie"
  ]
  node [
    id 1439
    label "&#347;miesznie"
  ]
  node [
    id 1440
    label "bawny"
  ]
  node [
    id 1441
    label "o&#347;mieszenie"
  ]
  node [
    id 1442
    label "dziwny"
  ]
  node [
    id 1443
    label "nieadekwatny"
  ]
  node [
    id 1444
    label "zale&#380;ny"
  ]
  node [
    id 1445
    label "uleg&#322;y"
  ]
  node [
    id 1446
    label "pos&#322;usznie"
  ]
  node [
    id 1447
    label "grzecznie"
  ]
  node [
    id 1448
    label "stosowny"
  ]
  node [
    id 1449
    label "niewinny"
  ]
  node [
    id 1450
    label "konserwatywny"
  ]
  node [
    id 1451
    label "nijaki"
  ]
  node [
    id 1452
    label "wolny"
  ]
  node [
    id 1453
    label "uspokajanie_si&#281;"
  ]
  node [
    id 1454
    label "bezproblemowy"
  ]
  node [
    id 1455
    label "spokojnie"
  ]
  node [
    id 1456
    label "uspokojenie_si&#281;"
  ]
  node [
    id 1457
    label "cicho"
  ]
  node [
    id 1458
    label "uspokojenie"
  ]
  node [
    id 1459
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 1460
    label "nietrudny"
  ]
  node [
    id 1461
    label "uspokajanie"
  ]
  node [
    id 1462
    label "korzystnie"
  ]
  node [
    id 1463
    label "drogo"
  ]
  node [
    id 1464
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1465
    label "przyjaciel"
  ]
  node [
    id 1466
    label "jedyny"
  ]
  node [
    id 1467
    label "du&#380;y"
  ]
  node [
    id 1468
    label "zdr&#243;w"
  ]
  node [
    id 1469
    label "calu&#347;ko"
  ]
  node [
    id 1470
    label "kompletny"
  ]
  node [
    id 1471
    label "&#380;ywy"
  ]
  node [
    id 1472
    label "pe&#322;ny"
  ]
  node [
    id 1473
    label "podobny"
  ]
  node [
    id 1474
    label "ca&#322;o"
  ]
  node [
    id 1475
    label "poskutkowanie"
  ]
  node [
    id 1476
    label "skutecznie"
  ]
  node [
    id 1477
    label "skutkowanie"
  ]
  node [
    id 1478
    label "pomy&#347;lnie"
  ]
  node [
    id 1479
    label "toto-lotek"
  ]
  node [
    id 1480
    label "trafienie"
  ]
  node [
    id 1481
    label "arkusz_drukarski"
  ]
  node [
    id 1482
    label "&#322;&#243;dka"
  ]
  node [
    id 1483
    label "four"
  ]
  node [
    id 1484
    label "&#263;wiartka"
  ]
  node [
    id 1485
    label "hotel"
  ]
  node [
    id 1486
    label "cyfra"
  ]
  node [
    id 1487
    label "pok&#243;j"
  ]
  node [
    id 1488
    label "stopie&#324;"
  ]
  node [
    id 1489
    label "minialbum"
  ]
  node [
    id 1490
    label "p&#322;yta_winylowa"
  ]
  node [
    id 1491
    label "blotka"
  ]
  node [
    id 1492
    label "zaprz&#281;g"
  ]
  node [
    id 1493
    label "przedtrzonowiec"
  ]
  node [
    id 1494
    label "turn"
  ]
  node [
    id 1495
    label "turning"
  ]
  node [
    id 1496
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1497
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1498
    label "skr&#281;t"
  ]
  node [
    id 1499
    label "obr&#243;t"
  ]
  node [
    id 1500
    label "fraza_czasownikowa"
  ]
  node [
    id 1501
    label "jednostka_leksykalna"
  ]
  node [
    id 1502
    label "welcome"
  ]
  node [
    id 1503
    label "pozdrowienie"
  ]
  node [
    id 1504
    label "zwyczaj"
  ]
  node [
    id 1505
    label "greeting"
  ]
  node [
    id 1506
    label "zdarzony"
  ]
  node [
    id 1507
    label "odpowiednio"
  ]
  node [
    id 1508
    label "odpowiadanie"
  ]
  node [
    id 1509
    label "kochanek"
  ]
  node [
    id 1510
    label "sk&#322;onny"
  ]
  node [
    id 1511
    label "wybranek"
  ]
  node [
    id 1512
    label "umi&#322;owany"
  ]
  node [
    id 1513
    label "przyjemnie"
  ]
  node [
    id 1514
    label "mi&#322;o"
  ]
  node [
    id 1515
    label "kochanie"
  ]
  node [
    id 1516
    label "dyplomata"
  ]
  node [
    id 1517
    label "dobroczynnie"
  ]
  node [
    id 1518
    label "lepiej"
  ]
  node [
    id 1519
    label "wiele"
  ]
  node [
    id 1520
    label "spo&#322;eczny"
  ]
  node [
    id 1521
    label "redaktor"
  ]
  node [
    id 1522
    label "radio"
  ]
  node [
    id 1523
    label "siedziba"
  ]
  node [
    id 1524
    label "composition"
  ]
  node [
    id 1525
    label "redaction"
  ]
  node [
    id 1526
    label "telewizja"
  ]
  node [
    id 1527
    label "obr&#243;bka"
  ]
  node [
    id 1528
    label "Mazowsze"
  ]
  node [
    id 1529
    label "whole"
  ]
  node [
    id 1530
    label "skupienie"
  ]
  node [
    id 1531
    label "The_Beatles"
  ]
  node [
    id 1532
    label "zabudowania"
  ]
  node [
    id 1533
    label "group"
  ]
  node [
    id 1534
    label "zespolik"
  ]
  node [
    id 1535
    label "ro&#347;lina"
  ]
  node [
    id 1536
    label "Depeche_Mode"
  ]
  node [
    id 1537
    label "batch"
  ]
  node [
    id 1538
    label "miejsce_pracy"
  ]
  node [
    id 1539
    label "dzia&#322;_personalny"
  ]
  node [
    id 1540
    label "Kreml"
  ]
  node [
    id 1541
    label "Bia&#322;y_Dom"
  ]
  node [
    id 1542
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 1543
    label "sadowisko"
  ]
  node [
    id 1544
    label "proces_technologiczny"
  ]
  node [
    id 1545
    label "proces"
  ]
  node [
    id 1546
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 1547
    label "bran&#380;owiec"
  ]
  node [
    id 1548
    label "edytor"
  ]
  node [
    id 1549
    label "debit"
  ]
  node [
    id 1550
    label "druk"
  ]
  node [
    id 1551
    label "publikacja"
  ]
  node [
    id 1552
    label "szata_graficzna"
  ]
  node [
    id 1553
    label "firma"
  ]
  node [
    id 1554
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 1555
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 1556
    label "poster"
  ]
  node [
    id 1557
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 1558
    label "paj&#281;czarz"
  ]
  node [
    id 1559
    label "radiola"
  ]
  node [
    id 1560
    label "programowiec"
  ]
  node [
    id 1561
    label "stacja"
  ]
  node [
    id 1562
    label "odbiornik"
  ]
  node [
    id 1563
    label "eliminator"
  ]
  node [
    id 1564
    label "radiolinia"
  ]
  node [
    id 1565
    label "fala_radiowa"
  ]
  node [
    id 1566
    label "radiofonia"
  ]
  node [
    id 1567
    label "odbieranie"
  ]
  node [
    id 1568
    label "studio"
  ]
  node [
    id 1569
    label "dyskryminator"
  ]
  node [
    id 1570
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 1571
    label "telekomunikacja"
  ]
  node [
    id 1572
    label "ekran"
  ]
  node [
    id 1573
    label "BBC"
  ]
  node [
    id 1574
    label "Polsat"
  ]
  node [
    id 1575
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 1576
    label "muza"
  ]
  node [
    id 1577
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 1578
    label "technologia"
  ]
  node [
    id 1579
    label "ekscerpcja"
  ]
  node [
    id 1580
    label "j&#281;zykowo"
  ]
  node [
    id 1581
    label "pomini&#281;cie"
  ]
  node [
    id 1582
    label "preparacja"
  ]
  node [
    id 1583
    label "odmianka"
  ]
  node [
    id 1584
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1585
    label "koniektura"
  ]
  node [
    id 1586
    label "obelga"
  ]
  node [
    id 1587
    label "darowizna"
  ]
  node [
    id 1588
    label "foundation"
  ]
  node [
    id 1589
    label "dar"
  ]
  node [
    id 1590
    label "pocz&#261;tek"
  ]
  node [
    id 1591
    label "osoba_prawna"
  ]
  node [
    id 1592
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1593
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1594
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1595
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1596
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1597
    label "Fundusze_Unijne"
  ]
  node [
    id 1598
    label "zamyka&#263;"
  ]
  node [
    id 1599
    label "establishment"
  ]
  node [
    id 1600
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1601
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1602
    label "afiliowa&#263;"
  ]
  node [
    id 1603
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1604
    label "zamykanie"
  ]
  node [
    id 1605
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1606
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1607
    label "przeniesienie_praw"
  ]
  node [
    id 1608
    label "zapomoga"
  ]
  node [
    id 1609
    label "transakcja"
  ]
  node [
    id 1610
    label "pierworodztwo"
  ]
  node [
    id 1611
    label "upgrade"
  ]
  node [
    id 1612
    label "nast&#281;pstwo"
  ]
  node [
    id 1613
    label "dyspozycja"
  ]
  node [
    id 1614
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1615
    label "da&#324;"
  ]
  node [
    id 1616
    label "faculty"
  ]
  node [
    id 1617
    label "stygmat"
  ]
  node [
    id 1618
    label "dobro"
  ]
  node [
    id 1619
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 1620
    label "gwiazda"
  ]
  node [
    id 1621
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 1622
    label "Arktur"
  ]
  node [
    id 1623
    label "kszta&#322;t"
  ]
  node [
    id 1624
    label "Gwiazda_Polarna"
  ]
  node [
    id 1625
    label "agregatka"
  ]
  node [
    id 1626
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 1627
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1628
    label "Nibiru"
  ]
  node [
    id 1629
    label "konstelacja"
  ]
  node [
    id 1630
    label "ornament"
  ]
  node [
    id 1631
    label "delta_Scuti"
  ]
  node [
    id 1632
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1633
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1634
    label "s&#322;awa"
  ]
  node [
    id 1635
    label "promie&#324;"
  ]
  node [
    id 1636
    label "star"
  ]
  node [
    id 1637
    label "gwiazdosz"
  ]
  node [
    id 1638
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 1639
    label "asocjacja_gwiazd"
  ]
  node [
    id 1640
    label "supergrupa"
  ]
  node [
    id 1641
    label "uderzenie"
  ]
  node [
    id 1642
    label "blok"
  ]
  node [
    id 1643
    label "shot"
  ]
  node [
    id 1644
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 1645
    label "struktura_geologiczna"
  ]
  node [
    id 1646
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 1647
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 1648
    label "coup"
  ]
  node [
    id 1649
    label "siekacz"
  ]
  node [
    id 1650
    label "instrumentalizacja"
  ]
  node [
    id 1651
    label "walka"
  ]
  node [
    id 1652
    label "wdarcie_si&#281;"
  ]
  node [
    id 1653
    label "pogorszenie"
  ]
  node [
    id 1654
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1655
    label "poczucie"
  ]
  node [
    id 1656
    label "contact"
  ]
  node [
    id 1657
    label "stukni&#281;cie"
  ]
  node [
    id 1658
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 1659
    label "bat"
  ]
  node [
    id 1660
    label "rush"
  ]
  node [
    id 1661
    label "odbicie"
  ]
  node [
    id 1662
    label "zadanie"
  ]
  node [
    id 1663
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1664
    label "st&#322;uczenie"
  ]
  node [
    id 1665
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 1666
    label "odbicie_si&#281;"
  ]
  node [
    id 1667
    label "dotkni&#281;cie"
  ]
  node [
    id 1668
    label "charge"
  ]
  node [
    id 1669
    label "dostanie"
  ]
  node [
    id 1670
    label "skrytykowanie"
  ]
  node [
    id 1671
    label "zagrywka"
  ]
  node [
    id 1672
    label "manewr"
  ]
  node [
    id 1673
    label "nast&#261;pienie"
  ]
  node [
    id 1674
    label "uderzanie"
  ]
  node [
    id 1675
    label "stroke"
  ]
  node [
    id 1676
    label "pobicie"
  ]
  node [
    id 1677
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 1678
    label "flap"
  ]
  node [
    id 1679
    label "dotyk"
  ]
  node [
    id 1680
    label "nast&#281;pnie"
  ]
  node [
    id 1681
    label "inny"
  ]
  node [
    id 1682
    label "nastopny"
  ]
  node [
    id 1683
    label "kolejno"
  ]
  node [
    id 1684
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1685
    label "osobno"
  ]
  node [
    id 1686
    label "r&#243;&#380;ny"
  ]
  node [
    id 1687
    label "inszy"
  ]
  node [
    id 1688
    label "inaczej"
  ]
  node [
    id 1689
    label "licencja"
  ]
  node [
    id 1690
    label "opieka"
  ]
  node [
    id 1691
    label "nadz&#243;r"
  ]
  node [
    id 1692
    label "sponsorship"
  ]
  node [
    id 1693
    label "zezwolenie"
  ]
  node [
    id 1694
    label "za&#347;wiadczenie"
  ]
  node [
    id 1695
    label "licencjonowa&#263;"
  ]
  node [
    id 1696
    label "rasowy"
  ]
  node [
    id 1697
    label "pozwolenie"
  ]
  node [
    id 1698
    label "hodowla"
  ]
  node [
    id 1699
    label "prawo"
  ]
  node [
    id 1700
    label "license"
  ]
  node [
    id 1701
    label "examination"
  ]
  node [
    id 1702
    label "pomoc"
  ]
  node [
    id 1703
    label "opieku&#324;cze_skrzyd&#322;a"
  ]
  node [
    id 1704
    label "staranie"
  ]
  node [
    id 1705
    label "embrace"
  ]
  node [
    id 1706
    label "manipulate"
  ]
  node [
    id 1707
    label "assume"
  ]
  node [
    id 1708
    label "podj&#261;&#263;"
  ]
  node [
    id 1709
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1710
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 1711
    label "skuma&#263;"
  ]
  node [
    id 1712
    label "obejmowa&#263;"
  ]
  node [
    id 1713
    label "zagarn&#261;&#263;"
  ]
  node [
    id 1714
    label "obj&#281;cie"
  ]
  node [
    id 1715
    label "spowodowa&#263;"
  ]
  node [
    id 1716
    label "involve"
  ]
  node [
    id 1717
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 1718
    label "do"
  ]
  node [
    id 1719
    label "zacz&#261;&#263;"
  ]
  node [
    id 1720
    label "dotkn&#261;&#263;"
  ]
  node [
    id 1721
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 1722
    label "udost&#281;pni&#263;"
  ]
  node [
    id 1723
    label "clasp"
  ]
  node [
    id 1724
    label "hold"
  ]
  node [
    id 1725
    label "umie&#347;ci&#263;"
  ]
  node [
    id 1726
    label "act"
  ]
  node [
    id 1727
    label "post&#261;pi&#263;"
  ]
  node [
    id 1728
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1729
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 1730
    label "odj&#261;&#263;"
  ]
  node [
    id 1731
    label "cause"
  ]
  node [
    id 1732
    label "introduce"
  ]
  node [
    id 1733
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 1734
    label "spotka&#263;"
  ]
  node [
    id 1735
    label "dotkn&#261;&#263;_si&#281;"
  ]
  node [
    id 1736
    label "allude"
  ]
  node [
    id 1737
    label "range"
  ]
  node [
    id 1738
    label "diss"
  ]
  node [
    id 1739
    label "pique"
  ]
  node [
    id 1740
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 1741
    label "wzbudzi&#263;"
  ]
  node [
    id 1742
    label "zareagowa&#263;"
  ]
  node [
    id 1743
    label "draw"
  ]
  node [
    id 1744
    label "zmieni&#263;"
  ]
  node [
    id 1745
    label "his"
  ]
  node [
    id 1746
    label "ut"
  ]
  node [
    id 1747
    label "C"
  ]
  node [
    id 1748
    label "zrozumie&#263;"
  ]
  node [
    id 1749
    label "zaskakiwa&#263;"
  ]
  node [
    id 1750
    label "fold"
  ]
  node [
    id 1751
    label "podejmowa&#263;"
  ]
  node [
    id 1752
    label "senator"
  ]
  node [
    id 1753
    label "mie&#263;"
  ]
  node [
    id 1754
    label "meet"
  ]
  node [
    id 1755
    label "obejmowanie"
  ]
  node [
    id 1756
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 1757
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1758
    label "dotyczy&#263;"
  ]
  node [
    id 1759
    label "zagarnia&#263;"
  ]
  node [
    id 1760
    label "zmieszczenie"
  ]
  node [
    id 1761
    label "pos&#322;uchanie"
  ]
  node [
    id 1762
    label "zagarni&#281;cie"
  ]
  node [
    id 1763
    label "skumanie"
  ]
  node [
    id 1764
    label "odnoszenie_si&#281;"
  ]
  node [
    id 1765
    label "przem&#243;wienie"
  ]
  node [
    id 1766
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1767
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 1768
    label "podpasienie"
  ]
  node [
    id 1769
    label "gest"
  ]
  node [
    id 1770
    label "pozyska&#263;"
  ]
  node [
    id 1771
    label "zabra&#263;"
  ]
  node [
    id 1772
    label "nabra&#263;"
  ]
  node [
    id 1773
    label "zaaresztowa&#263;"
  ]
  node [
    id 1774
    label "przysun&#261;&#263;"
  ]
  node [
    id 1775
    label "porwa&#263;"
  ]
  node [
    id 1776
    label "departament"
  ]
  node [
    id 1777
    label "NKWD"
  ]
  node [
    id 1778
    label "ministerium"
  ]
  node [
    id 1779
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 1780
    label "MSW"
  ]
  node [
    id 1781
    label "resort"
  ]
  node [
    id 1782
    label "stanowisko"
  ]
  node [
    id 1783
    label "position"
  ]
  node [
    id 1784
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1785
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1786
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1787
    label "mianowaniec"
  ]
  node [
    id 1788
    label "okienko"
  ]
  node [
    id 1789
    label "w&#322;adza"
  ]
  node [
    id 1790
    label "relation"
  ]
  node [
    id 1791
    label "podsekcja"
  ]
  node [
    id 1792
    label "ministry"
  ]
  node [
    id 1793
    label "Martynika"
  ]
  node [
    id 1794
    label "Gwadelupa"
  ]
  node [
    id 1795
    label "Moza"
  ]
  node [
    id 1796
    label "formation"
  ]
  node [
    id 1797
    label "Karta_Nauczyciela"
  ]
  node [
    id 1798
    label "wiedza"
  ]
  node [
    id 1799
    label "heureza"
  ]
  node [
    id 1800
    label "miasteczko_rowerowe"
  ]
  node [
    id 1801
    label "urszulanki"
  ]
  node [
    id 1802
    label "niepokalanki"
  ]
  node [
    id 1803
    label "kwalifikacje"
  ]
  node [
    id 1804
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 1805
    label "&#322;awa_szkolna"
  ]
  node [
    id 1806
    label "skolaryzacja"
  ]
  node [
    id 1807
    label "szkolnictwo"
  ]
  node [
    id 1808
    label "form"
  ]
  node [
    id 1809
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 1810
    label "gospodarka"
  ]
  node [
    id 1811
    label "porada"
  ]
  node [
    id 1812
    label "fotowoltaika"
  ]
  node [
    id 1813
    label "nauki_o_poznaniu"
  ]
  node [
    id 1814
    label "nomotetyczny"
  ]
  node [
    id 1815
    label "systematyka"
  ]
  node [
    id 1816
    label "typologia"
  ]
  node [
    id 1817
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 1818
    label "nauki_penalne"
  ]
  node [
    id 1819
    label "dziedzina"
  ]
  node [
    id 1820
    label "imagineskopia"
  ]
  node [
    id 1821
    label "teoria_naukowa"
  ]
  node [
    id 1822
    label "inwentyka"
  ]
  node [
    id 1823
    label "metodologia"
  ]
  node [
    id 1824
    label "nauki_o_Ziemi"
  ]
  node [
    id 1825
    label "kognicja"
  ]
  node [
    id 1826
    label "rozprawa"
  ]
  node [
    id 1827
    label "legislacyjnie"
  ]
  node [
    id 1828
    label "przes&#322;anka"
  ]
  node [
    id 1829
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1830
    label "metoda"
  ]
  node [
    id 1831
    label "nauczanie"
  ]
  node [
    id 1832
    label "inwentarz"
  ]
  node [
    id 1833
    label "mieszkalnictwo"
  ]
  node [
    id 1834
    label "agregat_ekonomiczny"
  ]
  node [
    id 1835
    label "farmaceutyka"
  ]
  node [
    id 1836
    label "produkowanie"
  ]
  node [
    id 1837
    label "rolnictwo"
  ]
  node [
    id 1838
    label "transport"
  ]
  node [
    id 1839
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 1840
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 1841
    label "obronno&#347;&#263;"
  ]
  node [
    id 1842
    label "sektor_prywatny"
  ]
  node [
    id 1843
    label "sch&#322;adza&#263;"
  ]
  node [
    id 1844
    label "czerwona_strefa"
  ]
  node [
    id 1845
    label "pole"
  ]
  node [
    id 1846
    label "sektor_publiczny"
  ]
  node [
    id 1847
    label "bankowo&#347;&#263;"
  ]
  node [
    id 1848
    label "gospodarowanie"
  ]
  node [
    id 1849
    label "obora"
  ]
  node [
    id 1850
    label "gospodarka_wodna"
  ]
  node [
    id 1851
    label "gospodarka_le&#347;na"
  ]
  node [
    id 1852
    label "gospodarowa&#263;"
  ]
  node [
    id 1853
    label "fabryka"
  ]
  node [
    id 1854
    label "wytw&#243;rnia"
  ]
  node [
    id 1855
    label "stodo&#322;a"
  ]
  node [
    id 1856
    label "przemys&#322;"
  ]
  node [
    id 1857
    label "spichlerz"
  ]
  node [
    id 1858
    label "sch&#322;adzanie"
  ]
  node [
    id 1859
    label "administracja"
  ]
  node [
    id 1860
    label "sch&#322;odzenie"
  ]
  node [
    id 1861
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 1862
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 1863
    label "regulacja_cen"
  ]
  node [
    id 1864
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 1865
    label "proporcja"
  ]
  node [
    id 1866
    label "wykszta&#322;cenie"
  ]
  node [
    id 1867
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 1868
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 1869
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 1870
    label "urszulanki_szare"
  ]
  node [
    id 1871
    label "cognition"
  ]
  node [
    id 1872
    label "intelekt"
  ]
  node [
    id 1873
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1874
    label "zaawansowanie"
  ]
  node [
    id 1875
    label "nacjonalistyczny"
  ]
  node [
    id 1876
    label "narodowo"
  ]
  node [
    id 1877
    label "wa&#380;ny"
  ]
  node [
    id 1878
    label "wynios&#322;y"
  ]
  node [
    id 1879
    label "dono&#347;ny"
  ]
  node [
    id 1880
    label "wa&#380;nie"
  ]
  node [
    id 1881
    label "istotnie"
  ]
  node [
    id 1882
    label "znaczny"
  ]
  node [
    id 1883
    label "eksponowany"
  ]
  node [
    id 1884
    label "polityczny"
  ]
  node [
    id 1885
    label "nacjonalistycznie"
  ]
  node [
    id 1886
    label "narodowo&#347;ciowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 230
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 374
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 229
  ]
  edge [
    source 13
    target 233
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 237
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 231
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 236
  ]
  edge [
    source 13
    target 228
  ]
  edge [
    source 13
    target 235
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 238
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 277
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 778
  ]
  edge [
    source 13
    target 779
  ]
  edge [
    source 13
    target 780
  ]
  edge [
    source 13
    target 781
  ]
  edge [
    source 13
    target 782
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 850
  ]
  edge [
    source 14
    target 851
  ]
  edge [
    source 14
    target 852
  ]
  edge [
    source 14
    target 853
  ]
  edge [
    source 14
    target 854
  ]
  edge [
    source 14
    target 855
  ]
  edge [
    source 14
    target 856
  ]
  edge [
    source 14
    target 857
  ]
  edge [
    source 14
    target 858
  ]
  edge [
    source 14
    target 859
  ]
  edge [
    source 14
    target 860
  ]
  edge [
    source 14
    target 861
  ]
  edge [
    source 14
    target 862
  ]
  edge [
    source 14
    target 863
  ]
  edge [
    source 14
    target 864
  ]
  edge [
    source 14
    target 865
  ]
  edge [
    source 14
    target 866
  ]
  edge [
    source 14
    target 867
  ]
  edge [
    source 14
    target 868
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 230
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 869
  ]
  edge [
    source 14
    target 870
  ]
  edge [
    source 14
    target 871
  ]
  edge [
    source 14
    target 872
  ]
  edge [
    source 14
    target 873
  ]
  edge [
    source 14
    target 874
  ]
  edge [
    source 14
    target 875
  ]
  edge [
    source 14
    target 876
  ]
  edge [
    source 14
    target 877
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 374
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 878
  ]
  edge [
    source 14
    target 879
  ]
  edge [
    source 14
    target 880
  ]
  edge [
    source 14
    target 881
  ]
  edge [
    source 14
    target 882
  ]
  edge [
    source 14
    target 883
  ]
  edge [
    source 14
    target 884
  ]
  edge [
    source 14
    target 885
  ]
  edge [
    source 14
    target 886
  ]
  edge [
    source 14
    target 887
  ]
  edge [
    source 14
    target 888
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 890
  ]
  edge [
    source 14
    target 891
  ]
  edge [
    source 14
    target 892
  ]
  edge [
    source 14
    target 893
  ]
  edge [
    source 14
    target 894
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 896
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 899
  ]
  edge [
    source 14
    target 900
  ]
  edge [
    source 14
    target 901
  ]
  edge [
    source 14
    target 902
  ]
  edge [
    source 14
    target 903
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 906
  ]
  edge [
    source 15
    target 907
  ]
  edge [
    source 15
    target 908
  ]
  edge [
    source 15
    target 909
  ]
  edge [
    source 15
    target 25
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 51
  ]
  edge [
    source 17
    target 52
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 47
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 25
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 934
  ]
  edge [
    source 19
    target 935
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 936
  ]
  edge [
    source 19
    target 937
  ]
  edge [
    source 19
    target 938
  ]
  edge [
    source 19
    target 939
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 940
  ]
  edge [
    source 19
    target 941
  ]
  edge [
    source 19
    target 942
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 943
  ]
  edge [
    source 21
    target 944
  ]
  edge [
    source 21
    target 945
  ]
  edge [
    source 21
    target 920
  ]
  edge [
    source 21
    target 946
  ]
  edge [
    source 21
    target 947
  ]
  edge [
    source 21
    target 948
  ]
  edge [
    source 21
    target 949
  ]
  edge [
    source 21
    target 950
  ]
  edge [
    source 21
    target 951
  ]
  edge [
    source 21
    target 952
  ]
  edge [
    source 21
    target 953
  ]
  edge [
    source 21
    target 954
  ]
  edge [
    source 21
    target 955
  ]
  edge [
    source 21
    target 956
  ]
  edge [
    source 21
    target 957
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 438
  ]
  edge [
    source 23
    target 434
  ]
  edge [
    source 23
    target 439
  ]
  edge [
    source 23
    target 125
  ]
  edge [
    source 23
    target 440
  ]
  edge [
    source 23
    target 441
  ]
  edge [
    source 23
    target 442
  ]
  edge [
    source 23
    target 357
  ]
  edge [
    source 23
    target 443
  ]
  edge [
    source 23
    target 444
  ]
  edge [
    source 23
    target 445
  ]
  edge [
    source 23
    target 446
  ]
  edge [
    source 23
    target 447
  ]
  edge [
    source 23
    target 104
  ]
  edge [
    source 23
    target 448
  ]
  edge [
    source 23
    target 449
  ]
  edge [
    source 23
    target 450
  ]
  edge [
    source 23
    target 451
  ]
  edge [
    source 23
    target 452
  ]
  edge [
    source 23
    target 453
  ]
  edge [
    source 23
    target 454
  ]
  edge [
    source 23
    target 455
  ]
  edge [
    source 23
    target 40
  ]
  edge [
    source 23
    target 456
  ]
  edge [
    source 23
    target 457
  ]
  edge [
    source 23
    target 458
  ]
  edge [
    source 23
    target 459
  ]
  edge [
    source 23
    target 460
  ]
  edge [
    source 23
    target 461
  ]
  edge [
    source 23
    target 462
  ]
  edge [
    source 23
    target 463
  ]
  edge [
    source 23
    target 464
  ]
  edge [
    source 23
    target 465
  ]
  edge [
    source 23
    target 466
  ]
  edge [
    source 23
    target 467
  ]
  edge [
    source 23
    target 468
  ]
  edge [
    source 23
    target 469
  ]
  edge [
    source 23
    target 470
  ]
  edge [
    source 23
    target 471
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 958
  ]
  edge [
    source 24
    target 959
  ]
  edge [
    source 24
    target 960
  ]
  edge [
    source 24
    target 961
  ]
  edge [
    source 24
    target 962
  ]
  edge [
    source 24
    target 106
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 963
  ]
  edge [
    source 25
    target 99
  ]
  edge [
    source 25
    target 964
  ]
  edge [
    source 25
    target 965
  ]
  edge [
    source 25
    target 420
  ]
  edge [
    source 25
    target 414
  ]
  edge [
    source 25
    target 966
  ]
  edge [
    source 25
    target 967
  ]
  edge [
    source 25
    target 968
  ]
  edge [
    source 25
    target 106
  ]
  edge [
    source 25
    target 70
  ]
  edge [
    source 25
    target 969
  ]
  edge [
    source 25
    target 970
  ]
  edge [
    source 25
    target 971
  ]
  edge [
    source 25
    target 972
  ]
  edge [
    source 25
    target 973
  ]
  edge [
    source 25
    target 974
  ]
  edge [
    source 25
    target 975
  ]
  edge [
    source 25
    target 976
  ]
  edge [
    source 25
    target 977
  ]
  edge [
    source 25
    target 978
  ]
  edge [
    source 25
    target 979
  ]
  edge [
    source 25
    target 980
  ]
  edge [
    source 25
    target 981
  ]
  edge [
    source 25
    target 982
  ]
  edge [
    source 25
    target 983
  ]
  edge [
    source 25
    target 984
  ]
  edge [
    source 25
    target 985
  ]
  edge [
    source 25
    target 133
  ]
  edge [
    source 25
    target 134
  ]
  edge [
    source 25
    target 135
  ]
  edge [
    source 25
    target 136
  ]
  edge [
    source 25
    target 61
  ]
  edge [
    source 25
    target 137
  ]
  edge [
    source 25
    target 138
  ]
  edge [
    source 25
    target 139
  ]
  edge [
    source 25
    target 140
  ]
  edge [
    source 25
    target 141
  ]
  edge [
    source 25
    target 142
  ]
  edge [
    source 25
    target 143
  ]
  edge [
    source 25
    target 144
  ]
  edge [
    source 25
    target 145
  ]
  edge [
    source 25
    target 146
  ]
  edge [
    source 25
    target 147
  ]
  edge [
    source 25
    target 148
  ]
  edge [
    source 25
    target 103
  ]
  edge [
    source 25
    target 149
  ]
  edge [
    source 25
    target 150
  ]
  edge [
    source 25
    target 151
  ]
  edge [
    source 25
    target 152
  ]
  edge [
    source 25
    target 153
  ]
  edge [
    source 25
    target 154
  ]
  edge [
    source 25
    target 155
  ]
  edge [
    source 25
    target 156
  ]
  edge [
    source 25
    target 157
  ]
  edge [
    source 25
    target 158
  ]
  edge [
    source 25
    target 159
  ]
  edge [
    source 25
    target 160
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 107
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 25
    target 986
  ]
  edge [
    source 25
    target 987
  ]
  edge [
    source 25
    target 988
  ]
  edge [
    source 25
    target 989
  ]
  edge [
    source 25
    target 498
  ]
  edge [
    source 25
    target 990
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 991
  ]
  edge [
    source 25
    target 992
  ]
  edge [
    source 25
    target 993
  ]
  edge [
    source 25
    target 994
  ]
  edge [
    source 25
    target 995
  ]
  edge [
    source 25
    target 996
  ]
  edge [
    source 25
    target 997
  ]
  edge [
    source 25
    target 998
  ]
  edge [
    source 25
    target 787
  ]
  edge [
    source 25
    target 999
  ]
  edge [
    source 25
    target 1000
  ]
  edge [
    source 25
    target 1001
  ]
  edge [
    source 25
    target 1002
  ]
  edge [
    source 25
    target 1003
  ]
  edge [
    source 25
    target 1004
  ]
  edge [
    source 25
    target 1005
  ]
  edge [
    source 25
    target 1006
  ]
  edge [
    source 25
    target 623
  ]
  edge [
    source 25
    target 1007
  ]
  edge [
    source 25
    target 1008
  ]
  edge [
    source 25
    target 1009
  ]
  edge [
    source 25
    target 1010
  ]
  edge [
    source 25
    target 1011
  ]
  edge [
    source 25
    target 1012
  ]
  edge [
    source 25
    target 1013
  ]
  edge [
    source 25
    target 1014
  ]
  edge [
    source 25
    target 1015
  ]
  edge [
    source 25
    target 1016
  ]
  edge [
    source 25
    target 1017
  ]
  edge [
    source 25
    target 1018
  ]
  edge [
    source 25
    target 1019
  ]
  edge [
    source 25
    target 1020
  ]
  edge [
    source 25
    target 1021
  ]
  edge [
    source 25
    target 1022
  ]
  edge [
    source 25
    target 80
  ]
  edge [
    source 25
    target 1023
  ]
  edge [
    source 25
    target 1024
  ]
  edge [
    source 25
    target 1025
  ]
  edge [
    source 25
    target 354
  ]
  edge [
    source 25
    target 1026
  ]
  edge [
    source 25
    target 1027
  ]
  edge [
    source 25
    target 1028
  ]
  edge [
    source 25
    target 1029
  ]
  edge [
    source 25
    target 1030
  ]
  edge [
    source 25
    target 1031
  ]
  edge [
    source 25
    target 1032
  ]
  edge [
    source 25
    target 1033
  ]
  edge [
    source 25
    target 1034
  ]
  edge [
    source 25
    target 1035
  ]
  edge [
    source 25
    target 1036
  ]
  edge [
    source 25
    target 1037
  ]
  edge [
    source 25
    target 1038
  ]
  edge [
    source 25
    target 1039
  ]
  edge [
    source 25
    target 450
  ]
  edge [
    source 25
    target 69
  ]
  edge [
    source 25
    target 1040
  ]
  edge [
    source 25
    target 463
  ]
  edge [
    source 25
    target 76
  ]
  edge [
    source 25
    target 1041
  ]
  edge [
    source 25
    target 1042
  ]
  edge [
    source 25
    target 35
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1043
  ]
  edge [
    source 26
    target 1044
  ]
  edge [
    source 26
    target 1045
  ]
  edge [
    source 26
    target 1046
  ]
  edge [
    source 26
    target 1047
  ]
  edge [
    source 26
    target 1048
  ]
  edge [
    source 26
    target 1049
  ]
  edge [
    source 26
    target 1050
  ]
  edge [
    source 26
    target 1051
  ]
  edge [
    source 26
    target 258
  ]
  edge [
    source 26
    target 1052
  ]
  edge [
    source 26
    target 1053
  ]
  edge [
    source 26
    target 1054
  ]
  edge [
    source 26
    target 1055
  ]
  edge [
    source 26
    target 1056
  ]
  edge [
    source 26
    target 1057
  ]
  edge [
    source 26
    target 1058
  ]
  edge [
    source 26
    target 1059
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1060
  ]
  edge [
    source 27
    target 1061
  ]
  edge [
    source 27
    target 1062
  ]
  edge [
    source 27
    target 1063
  ]
  edge [
    source 27
    target 1064
  ]
  edge [
    source 27
    target 1065
  ]
  edge [
    source 27
    target 262
  ]
  edge [
    source 27
    target 1066
  ]
  edge [
    source 27
    target 1067
  ]
  edge [
    source 27
    target 1068
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 578
  ]
  edge [
    source 28
    target 1069
  ]
  edge [
    source 28
    target 1070
  ]
  edge [
    source 28
    target 1071
  ]
  edge [
    source 28
    target 1072
  ]
  edge [
    source 28
    target 920
  ]
  edge [
    source 28
    target 1073
  ]
  edge [
    source 28
    target 1074
  ]
  edge [
    source 28
    target 1075
  ]
  edge [
    source 28
    target 1076
  ]
  edge [
    source 28
    target 1077
  ]
  edge [
    source 28
    target 1078
  ]
  edge [
    source 28
    target 1079
  ]
  edge [
    source 28
    target 1080
  ]
  edge [
    source 28
    target 1081
  ]
  edge [
    source 28
    target 1082
  ]
  edge [
    source 28
    target 1083
  ]
  edge [
    source 28
    target 1084
  ]
  edge [
    source 28
    target 1085
  ]
  edge [
    source 28
    target 1086
  ]
  edge [
    source 28
    target 1087
  ]
  edge [
    source 28
    target 1088
  ]
  edge [
    source 28
    target 1089
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1090
  ]
  edge [
    source 30
    target 1091
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1092
  ]
  edge [
    source 31
    target 1093
  ]
  edge [
    source 31
    target 1094
  ]
  edge [
    source 31
    target 1095
  ]
  edge [
    source 31
    target 1096
  ]
  edge [
    source 31
    target 1097
  ]
  edge [
    source 31
    target 1098
  ]
  edge [
    source 31
    target 1099
  ]
  edge [
    source 31
    target 1048
  ]
  edge [
    source 31
    target 861
  ]
  edge [
    source 31
    target 1100
  ]
  edge [
    source 31
    target 1101
  ]
  edge [
    source 31
    target 1102
  ]
  edge [
    source 31
    target 1103
  ]
  edge [
    source 31
    target 1104
  ]
  edge [
    source 31
    target 1105
  ]
  edge [
    source 31
    target 1106
  ]
  edge [
    source 31
    target 1107
  ]
  edge [
    source 31
    target 1108
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1109
  ]
  edge [
    source 32
    target 1110
  ]
  edge [
    source 32
    target 1111
  ]
  edge [
    source 32
    target 1112
  ]
  edge [
    source 32
    target 1113
  ]
  edge [
    source 32
    target 434
  ]
  edge [
    source 32
    target 1114
  ]
  edge [
    source 32
    target 1115
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 48
  ]
  edge [
    source 33
    target 78
  ]
  edge [
    source 33
    target 1116
  ]
  edge [
    source 33
    target 1117
  ]
  edge [
    source 33
    target 1118
  ]
  edge [
    source 33
    target 1119
  ]
  edge [
    source 33
    target 1120
  ]
  edge [
    source 33
    target 1121
  ]
  edge [
    source 33
    target 1122
  ]
  edge [
    source 33
    target 1123
  ]
  edge [
    source 33
    target 1124
  ]
  edge [
    source 33
    target 527
  ]
  edge [
    source 33
    target 1125
  ]
  edge [
    source 33
    target 1126
  ]
  edge [
    source 33
    target 73
  ]
  edge [
    source 33
    target 1127
  ]
  edge [
    source 33
    target 1128
  ]
  edge [
    source 33
    target 1129
  ]
  edge [
    source 33
    target 140
  ]
  edge [
    source 33
    target 1130
  ]
  edge [
    source 33
    target 1131
  ]
  edge [
    source 33
    target 212
  ]
  edge [
    source 33
    target 1132
  ]
  edge [
    source 33
    target 1133
  ]
  edge [
    source 33
    target 1134
  ]
  edge [
    source 33
    target 1135
  ]
  edge [
    source 33
    target 1136
  ]
  edge [
    source 33
    target 1137
  ]
  edge [
    source 33
    target 1138
  ]
  edge [
    source 33
    target 1139
  ]
  edge [
    source 33
    target 1140
  ]
  edge [
    source 33
    target 80
  ]
  edge [
    source 33
    target 1141
  ]
  edge [
    source 33
    target 1142
  ]
  edge [
    source 33
    target 1143
  ]
  edge [
    source 33
    target 1144
  ]
  edge [
    source 33
    target 1145
  ]
  edge [
    source 33
    target 1146
  ]
  edge [
    source 33
    target 1147
  ]
  edge [
    source 33
    target 1148
  ]
  edge [
    source 33
    target 1149
  ]
  edge [
    source 33
    target 1150
  ]
  edge [
    source 33
    target 1151
  ]
  edge [
    source 33
    target 1152
  ]
  edge [
    source 33
    target 1153
  ]
  edge [
    source 33
    target 1154
  ]
  edge [
    source 33
    target 1155
  ]
  edge [
    source 33
    target 183
  ]
  edge [
    source 33
    target 1156
  ]
  edge [
    source 33
    target 1157
  ]
  edge [
    source 33
    target 1158
  ]
  edge [
    source 33
    target 1041
  ]
  edge [
    source 33
    target 1159
  ]
  edge [
    source 33
    target 1160
  ]
  edge [
    source 33
    target 1161
  ]
  edge [
    source 33
    target 151
  ]
  edge [
    source 33
    target 1162
  ]
  edge [
    source 33
    target 1163
  ]
  edge [
    source 33
    target 1164
  ]
  edge [
    source 33
    target 1165
  ]
  edge [
    source 33
    target 1166
  ]
  edge [
    source 33
    target 1167
  ]
  edge [
    source 33
    target 507
  ]
  edge [
    source 33
    target 1168
  ]
  edge [
    source 33
    target 1169
  ]
  edge [
    source 33
    target 1170
  ]
  edge [
    source 33
    target 1171
  ]
  edge [
    source 33
    target 434
  ]
  edge [
    source 33
    target 1172
  ]
  edge [
    source 33
    target 108
  ]
  edge [
    source 33
    target 1173
  ]
  edge [
    source 33
    target 1174
  ]
  edge [
    source 33
    target 657
  ]
  edge [
    source 33
    target 1175
  ]
  edge [
    source 33
    target 1176
  ]
  edge [
    source 33
    target 1177
  ]
  edge [
    source 33
    target 1178
  ]
  edge [
    source 33
    target 1179
  ]
  edge [
    source 33
    target 1180
  ]
  edge [
    source 33
    target 76
  ]
  edge [
    source 33
    target 1181
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1182
  ]
  edge [
    source 34
    target 1183
  ]
  edge [
    source 34
    target 1184
  ]
  edge [
    source 34
    target 1185
  ]
  edge [
    source 34
    target 1186
  ]
  edge [
    source 34
    target 1187
  ]
  edge [
    source 34
    target 920
  ]
  edge [
    source 34
    target 1188
  ]
  edge [
    source 34
    target 1189
  ]
  edge [
    source 34
    target 1190
  ]
  edge [
    source 34
    target 1191
  ]
  edge [
    source 34
    target 1192
  ]
  edge [
    source 34
    target 1193
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1194
  ]
  edge [
    source 35
    target 115
  ]
  edge [
    source 35
    target 1195
  ]
  edge [
    source 35
    target 1196
  ]
  edge [
    source 35
    target 1197
  ]
  edge [
    source 35
    target 1198
  ]
  edge [
    source 35
    target 1199
  ]
  edge [
    source 35
    target 1200
  ]
  edge [
    source 35
    target 1201
  ]
  edge [
    source 35
    target 76
  ]
  edge [
    source 35
    target 1202
  ]
  edge [
    source 35
    target 1203
  ]
  edge [
    source 35
    target 80
  ]
  edge [
    source 35
    target 1204
  ]
  edge [
    source 35
    target 1205
  ]
  edge [
    source 35
    target 1206
  ]
  edge [
    source 35
    target 1207
  ]
  edge [
    source 35
    target 1208
  ]
  edge [
    source 35
    target 1209
  ]
  edge [
    source 35
    target 1210
  ]
  edge [
    source 35
    target 65
  ]
  edge [
    source 35
    target 1211
  ]
  edge [
    source 35
    target 1212
  ]
  edge [
    source 35
    target 1213
  ]
  edge [
    source 35
    target 1214
  ]
  edge [
    source 35
    target 1215
  ]
  edge [
    source 35
    target 1216
  ]
  edge [
    source 35
    target 1217
  ]
  edge [
    source 35
    target 1218
  ]
  edge [
    source 35
    target 1219
  ]
  edge [
    source 35
    target 1220
  ]
  edge [
    source 35
    target 1221
  ]
  edge [
    source 35
    target 1222
  ]
  edge [
    source 35
    target 1223
  ]
  edge [
    source 35
    target 1224
  ]
  edge [
    source 35
    target 64
  ]
  edge [
    source 35
    target 1225
  ]
  edge [
    source 35
    target 1226
  ]
  edge [
    source 35
    target 1227
  ]
  edge [
    source 35
    target 976
  ]
  edge [
    source 35
    target 1228
  ]
  edge [
    source 35
    target 1229
  ]
  edge [
    source 35
    target 434
  ]
  edge [
    source 35
    target 1230
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1231
  ]
  edge [
    source 36
    target 1232
  ]
  edge [
    source 36
    target 1233
  ]
  edge [
    source 36
    target 1234
  ]
  edge [
    source 37
    target 1109
  ]
  edge [
    source 37
    target 1110
  ]
  edge [
    source 37
    target 1111
  ]
  edge [
    source 37
    target 1112
  ]
  edge [
    source 37
    target 1113
  ]
  edge [
    source 37
    target 434
  ]
  edge [
    source 37
    target 1114
  ]
  edge [
    source 37
    target 1115
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1235
  ]
  edge [
    source 38
    target 1236
  ]
  edge [
    source 38
    target 1237
  ]
  edge [
    source 38
    target 134
  ]
  edge [
    source 38
    target 1238
  ]
  edge [
    source 38
    target 1239
  ]
  edge [
    source 38
    target 1240
  ]
  edge [
    source 38
    target 1241
  ]
  edge [
    source 38
    target 1242
  ]
  edge [
    source 38
    target 1243
  ]
  edge [
    source 38
    target 1244
  ]
  edge [
    source 38
    target 1245
  ]
  edge [
    source 38
    target 1246
  ]
  edge [
    source 38
    target 1247
  ]
  edge [
    source 38
    target 1248
  ]
  edge [
    source 38
    target 655
  ]
  edge [
    source 38
    target 1249
  ]
  edge [
    source 38
    target 1250
  ]
  edge [
    source 38
    target 1251
  ]
  edge [
    source 38
    target 1252
  ]
  edge [
    source 38
    target 1253
  ]
  edge [
    source 38
    target 1254
  ]
  edge [
    source 38
    target 187
  ]
  edge [
    source 38
    target 1255
  ]
  edge [
    source 38
    target 1256
  ]
  edge [
    source 38
    target 1169
  ]
  edge [
    source 38
    target 1257
  ]
  edge [
    source 38
    target 1258
  ]
  edge [
    source 38
    target 1259
  ]
  edge [
    source 38
    target 1260
  ]
  edge [
    source 38
    target 191
  ]
  edge [
    source 38
    target 1261
  ]
  edge [
    source 38
    target 1262
  ]
  edge [
    source 38
    target 1263
  ]
  edge [
    source 38
    target 1264
  ]
  edge [
    source 38
    target 1145
  ]
  edge [
    source 38
    target 1265
  ]
  edge [
    source 38
    target 1266
  ]
  edge [
    source 38
    target 1267
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1268
  ]
  edge [
    source 39
    target 1269
  ]
  edge [
    source 39
    target 1270
  ]
  edge [
    source 39
    target 101
  ]
  edge [
    source 39
    target 1271
  ]
  edge [
    source 39
    target 1272
  ]
  edge [
    source 39
    target 1273
  ]
  edge [
    source 39
    target 1274
  ]
  edge [
    source 39
    target 1275
  ]
  edge [
    source 39
    target 1276
  ]
  edge [
    source 39
    target 1277
  ]
  edge [
    source 39
    target 1278
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1279
  ]
  edge [
    source 40
    target 434
  ]
  edge [
    source 40
    target 439
  ]
  edge [
    source 40
    target 125
  ]
  edge [
    source 40
    target 440
  ]
  edge [
    source 40
    target 441
  ]
  edge [
    source 40
    target 442
  ]
  edge [
    source 40
    target 357
  ]
  edge [
    source 40
    target 443
  ]
  edge [
    source 40
    target 444
  ]
  edge [
    source 40
    target 445
  ]
  edge [
    source 40
    target 446
  ]
  edge [
    source 40
    target 447
  ]
  edge [
    source 40
    target 104
  ]
  edge [
    source 40
    target 448
  ]
  edge [
    source 40
    target 449
  ]
  edge [
    source 40
    target 450
  ]
  edge [
    source 40
    target 451
  ]
  edge [
    source 40
    target 452
  ]
  edge [
    source 40
    target 453
  ]
  edge [
    source 40
    target 454
  ]
  edge [
    source 40
    target 455
  ]
  edge [
    source 40
    target 456
  ]
  edge [
    source 40
    target 457
  ]
  edge [
    source 40
    target 458
  ]
  edge [
    source 40
    target 459
  ]
  edge [
    source 40
    target 460
  ]
  edge [
    source 40
    target 461
  ]
  edge [
    source 40
    target 462
  ]
  edge [
    source 40
    target 463
  ]
  edge [
    source 40
    target 464
  ]
  edge [
    source 40
    target 465
  ]
  edge [
    source 40
    target 466
  ]
  edge [
    source 40
    target 467
  ]
  edge [
    source 40
    target 468
  ]
  edge [
    source 40
    target 469
  ]
  edge [
    source 40
    target 470
  ]
  edge [
    source 40
    target 471
  ]
  edge [
    source 40
    target 1280
  ]
  edge [
    source 40
    target 757
  ]
  edge [
    source 40
    target 1281
  ]
  edge [
    source 40
    target 1282
  ]
  edge [
    source 40
    target 1283
  ]
  edge [
    source 40
    target 1284
  ]
  edge [
    source 40
    target 1285
  ]
  edge [
    source 40
    target 1286
  ]
  edge [
    source 40
    target 1287
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1288
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 58
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1289
  ]
  edge [
    source 42
    target 165
  ]
  edge [
    source 42
    target 1290
  ]
  edge [
    source 42
    target 65
  ]
  edge [
    source 42
    target 1291
  ]
  edge [
    source 42
    target 1292
  ]
  edge [
    source 42
    target 99
  ]
  edge [
    source 42
    target 62
  ]
  edge [
    source 42
    target 100
  ]
  edge [
    source 42
    target 101
  ]
  edge [
    source 42
    target 64
  ]
  edge [
    source 42
    target 102
  ]
  edge [
    source 42
    target 103
  ]
  edge [
    source 42
    target 104
  ]
  edge [
    source 42
    target 105
  ]
  edge [
    source 42
    target 106
  ]
  edge [
    source 42
    target 107
  ]
  edge [
    source 42
    target 108
  ]
  edge [
    source 42
    target 109
  ]
  edge [
    source 42
    target 987
  ]
  edge [
    source 42
    target 1293
  ]
  edge [
    source 42
    target 1294
  ]
  edge [
    source 42
    target 1295
  ]
  edge [
    source 42
    target 1296
  ]
  edge [
    source 42
    target 1297
  ]
  edge [
    source 42
    target 338
  ]
  edge [
    source 42
    target 981
  ]
  edge [
    source 42
    target 1298
  ]
  edge [
    source 42
    target 1299
  ]
  edge [
    source 42
    target 1300
  ]
  edge [
    source 42
    target 1301
  ]
  edge [
    source 42
    target 80
  ]
  edge [
    source 42
    target 1302
  ]
  edge [
    source 42
    target 1243
  ]
  edge [
    source 42
    target 1303
  ]
  edge [
    source 42
    target 562
  ]
  edge [
    source 42
    target 644
  ]
  edge [
    source 42
    target 1304
  ]
  edge [
    source 42
    target 1305
  ]
  edge [
    source 42
    target 1306
  ]
  edge [
    source 42
    target 187
  ]
  edge [
    source 42
    target 946
  ]
  edge [
    source 42
    target 1307
  ]
  edge [
    source 42
    target 1308
  ]
  edge [
    source 42
    target 1309
  ]
  edge [
    source 42
    target 1310
  ]
  edge [
    source 42
    target 1311
  ]
  edge [
    source 42
    target 800
  ]
  edge [
    source 42
    target 1312
  ]
  edge [
    source 42
    target 1313
  ]
  edge [
    source 42
    target 1314
  ]
  edge [
    source 42
    target 1315
  ]
  edge [
    source 42
    target 1316
  ]
  edge [
    source 42
    target 1317
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 42
    target 59
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1318
  ]
  edge [
    source 43
    target 1048
  ]
  edge [
    source 43
    target 1046
  ]
  edge [
    source 43
    target 1319
  ]
  edge [
    source 43
    target 1320
  ]
  edge [
    source 43
    target 1321
  ]
  edge [
    source 43
    target 1322
  ]
  edge [
    source 43
    target 1323
  ]
  edge [
    source 43
    target 1324
  ]
  edge [
    source 43
    target 1325
  ]
  edge [
    source 43
    target 1326
  ]
  edge [
    source 43
    target 1327
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 1328
  ]
  edge [
    source 43
    target 1329
  ]
  edge [
    source 43
    target 1330
  ]
  edge [
    source 43
    target 1331
  ]
  edge [
    source 43
    target 1332
  ]
  edge [
    source 43
    target 1333
  ]
  edge [
    source 43
    target 1334
  ]
  edge [
    source 43
    target 810
  ]
  edge [
    source 43
    target 1335
  ]
  edge [
    source 43
    target 1336
  ]
  edge [
    source 43
    target 1049
  ]
  edge [
    source 43
    target 1050
  ]
  edge [
    source 43
    target 1051
  ]
  edge [
    source 43
    target 258
  ]
  edge [
    source 43
    target 1337
  ]
  edge [
    source 43
    target 1338
  ]
  edge [
    source 43
    target 1339
  ]
  edge [
    source 43
    target 1340
  ]
  edge [
    source 43
    target 1341
  ]
  edge [
    source 43
    target 1342
  ]
  edge [
    source 43
    target 1343
  ]
  edge [
    source 43
    target 1344
  ]
  edge [
    source 43
    target 1345
  ]
  edge [
    source 43
    target 1346
  ]
  edge [
    source 43
    target 1347
  ]
  edge [
    source 43
    target 1348
  ]
  edge [
    source 43
    target 1349
  ]
  edge [
    source 43
    target 1350
  ]
  edge [
    source 43
    target 1351
  ]
  edge [
    source 43
    target 1352
  ]
  edge [
    source 43
    target 1353
  ]
  edge [
    source 43
    target 1354
  ]
  edge [
    source 43
    target 1355
  ]
  edge [
    source 43
    target 1356
  ]
  edge [
    source 43
    target 1357
  ]
  edge [
    source 43
    target 1358
  ]
  edge [
    source 43
    target 1359
  ]
  edge [
    source 43
    target 1360
  ]
  edge [
    source 43
    target 1361
  ]
  edge [
    source 43
    target 1362
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 920
  ]
  edge [
    source 45
    target 1363
  ]
  edge [
    source 45
    target 1364
  ]
  edge [
    source 45
    target 1365
  ]
  edge [
    source 45
    target 1366
  ]
  edge [
    source 45
    target 1367
  ]
  edge [
    source 45
    target 1368
  ]
  edge [
    source 45
    target 1369
  ]
  edge [
    source 45
    target 74
  ]
  edge [
    source 45
    target 1370
  ]
  edge [
    source 45
    target 77
  ]
  edge [
    source 45
    target 1371
  ]
  edge [
    source 45
    target 140
  ]
  edge [
    source 45
    target 1372
  ]
  edge [
    source 45
    target 1373
  ]
  edge [
    source 45
    target 1374
  ]
  edge [
    source 45
    target 1375
  ]
  edge [
    source 45
    target 1376
  ]
  edge [
    source 45
    target 1377
  ]
  edge [
    source 45
    target 1378
  ]
  edge [
    source 45
    target 1379
  ]
  edge [
    source 45
    target 1380
  ]
  edge [
    source 45
    target 1381
  ]
  edge [
    source 45
    target 1382
  ]
  edge [
    source 45
    target 1383
  ]
  edge [
    source 45
    target 1384
  ]
  edge [
    source 45
    target 1385
  ]
  edge [
    source 45
    target 1386
  ]
  edge [
    source 45
    target 1387
  ]
  edge [
    source 45
    target 1388
  ]
  edge [
    source 45
    target 1389
  ]
  edge [
    source 45
    target 1390
  ]
  edge [
    source 45
    target 154
  ]
  edge [
    source 45
    target 1391
  ]
  edge [
    source 45
    target 552
  ]
  edge [
    source 45
    target 1392
  ]
  edge [
    source 45
    target 1393
  ]
  edge [
    source 45
    target 1394
  ]
  edge [
    source 45
    target 105
  ]
  edge [
    source 45
    target 1395
  ]
  edge [
    source 45
    target 1396
  ]
  edge [
    source 45
    target 1397
  ]
  edge [
    source 45
    target 1398
  ]
  edge [
    source 45
    target 69
  ]
  edge [
    source 45
    target 173
  ]
  edge [
    source 45
    target 1001
  ]
  edge [
    source 45
    target 1399
  ]
  edge [
    source 45
    target 1400
  ]
  edge [
    source 45
    target 1401
  ]
  edge [
    source 45
    target 1402
  ]
  edge [
    source 45
    target 1403
  ]
  edge [
    source 45
    target 1404
  ]
  edge [
    source 45
    target 1405
  ]
  edge [
    source 45
    target 1406
  ]
  edge [
    source 45
    target 1407
  ]
  edge [
    source 45
    target 1408
  ]
  edge [
    source 45
    target 1409
  ]
  edge [
    source 45
    target 1410
  ]
  edge [
    source 45
    target 1411
  ]
  edge [
    source 45
    target 1412
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1413
  ]
  edge [
    source 46
    target 1414
  ]
  edge [
    source 46
    target 1415
  ]
  edge [
    source 46
    target 1416
  ]
  edge [
    source 46
    target 1417
  ]
  edge [
    source 46
    target 1418
  ]
  edge [
    source 46
    target 1419
  ]
  edge [
    source 46
    target 1048
  ]
  edge [
    source 46
    target 1307
  ]
  edge [
    source 46
    target 1420
  ]
  edge [
    source 46
    target 1421
  ]
  edge [
    source 46
    target 1422
  ]
  edge [
    source 46
    target 1423
  ]
  edge [
    source 46
    target 1424
  ]
  edge [
    source 46
    target 1425
  ]
  edge [
    source 46
    target 1426
  ]
  edge [
    source 46
    target 1427
  ]
  edge [
    source 46
    target 1428
  ]
  edge [
    source 46
    target 1429
  ]
  edge [
    source 46
    target 1430
  ]
  edge [
    source 46
    target 1431
  ]
  edge [
    source 46
    target 1432
  ]
  edge [
    source 46
    target 1337
  ]
  edge [
    source 46
    target 1338
  ]
  edge [
    source 46
    target 1339
  ]
  edge [
    source 46
    target 1046
  ]
  edge [
    source 46
    target 1340
  ]
  edge [
    source 46
    target 1341
  ]
  edge [
    source 46
    target 1342
  ]
  edge [
    source 46
    target 1343
  ]
  edge [
    source 46
    target 1344
  ]
  edge [
    source 46
    target 1345
  ]
  edge [
    source 46
    target 1346
  ]
  edge [
    source 46
    target 1433
  ]
  edge [
    source 46
    target 1331
  ]
  edge [
    source 46
    target 1434
  ]
  edge [
    source 46
    target 1435
  ]
  edge [
    source 46
    target 1436
  ]
  edge [
    source 46
    target 1437
  ]
  edge [
    source 46
    target 1438
  ]
  edge [
    source 46
    target 1439
  ]
  edge [
    source 46
    target 1440
  ]
  edge [
    source 46
    target 1441
  ]
  edge [
    source 46
    target 1442
  ]
  edge [
    source 46
    target 1443
  ]
  edge [
    source 46
    target 1444
  ]
  edge [
    source 46
    target 1445
  ]
  edge [
    source 46
    target 1446
  ]
  edge [
    source 46
    target 1447
  ]
  edge [
    source 46
    target 1448
  ]
  edge [
    source 46
    target 1449
  ]
  edge [
    source 46
    target 1450
  ]
  edge [
    source 46
    target 1451
  ]
  edge [
    source 46
    target 1452
  ]
  edge [
    source 46
    target 1453
  ]
  edge [
    source 46
    target 1454
  ]
  edge [
    source 46
    target 1455
  ]
  edge [
    source 46
    target 1456
  ]
  edge [
    source 46
    target 1457
  ]
  edge [
    source 46
    target 1458
  ]
  edge [
    source 46
    target 1459
  ]
  edge [
    source 46
    target 1460
  ]
  edge [
    source 46
    target 1461
  ]
  edge [
    source 46
    target 1462
  ]
  edge [
    source 46
    target 1463
  ]
  edge [
    source 46
    target 920
  ]
  edge [
    source 46
    target 944
  ]
  edge [
    source 46
    target 1464
  ]
  edge [
    source 46
    target 1465
  ]
  edge [
    source 46
    target 1466
  ]
  edge [
    source 46
    target 1467
  ]
  edge [
    source 46
    target 1468
  ]
  edge [
    source 46
    target 1469
  ]
  edge [
    source 46
    target 1470
  ]
  edge [
    source 46
    target 1471
  ]
  edge [
    source 46
    target 1472
  ]
  edge [
    source 46
    target 1473
  ]
  edge [
    source 46
    target 1474
  ]
  edge [
    source 46
    target 1475
  ]
  edge [
    source 46
    target 887
  ]
  edge [
    source 46
    target 1476
  ]
  edge [
    source 46
    target 1477
  ]
  edge [
    source 46
    target 1478
  ]
  edge [
    source 46
    target 1479
  ]
  edge [
    source 46
    target 1480
  ]
  edge [
    source 46
    target 80
  ]
  edge [
    source 46
    target 1481
  ]
  edge [
    source 46
    target 1482
  ]
  edge [
    source 46
    target 1483
  ]
  edge [
    source 46
    target 1484
  ]
  edge [
    source 46
    target 1485
  ]
  edge [
    source 46
    target 1486
  ]
  edge [
    source 46
    target 1487
  ]
  edge [
    source 46
    target 1488
  ]
  edge [
    source 46
    target 151
  ]
  edge [
    source 46
    target 1489
  ]
  edge [
    source 46
    target 1071
  ]
  edge [
    source 46
    target 1490
  ]
  edge [
    source 46
    target 1491
  ]
  edge [
    source 46
    target 1492
  ]
  edge [
    source 46
    target 1493
  ]
  edge [
    source 46
    target 124
  ]
  edge [
    source 46
    target 1494
  ]
  edge [
    source 46
    target 1495
  ]
  edge [
    source 46
    target 1496
  ]
  edge [
    source 46
    target 1497
  ]
  edge [
    source 46
    target 1498
  ]
  edge [
    source 46
    target 1499
  ]
  edge [
    source 46
    target 1500
  ]
  edge [
    source 46
    target 1501
  ]
  edge [
    source 46
    target 1121
  ]
  edge [
    source 46
    target 1010
  ]
  edge [
    source 46
    target 1502
  ]
  edge [
    source 46
    target 165
  ]
  edge [
    source 46
    target 1503
  ]
  edge [
    source 46
    target 1504
  ]
  edge [
    source 46
    target 1505
  ]
  edge [
    source 46
    target 1506
  ]
  edge [
    source 46
    target 1507
  ]
  edge [
    source 46
    target 1508
  ]
  edge [
    source 46
    target 884
  ]
  edge [
    source 46
    target 1509
  ]
  edge [
    source 46
    target 1510
  ]
  edge [
    source 46
    target 1511
  ]
  edge [
    source 46
    target 1512
  ]
  edge [
    source 46
    target 1513
  ]
  edge [
    source 46
    target 1514
  ]
  edge [
    source 46
    target 1515
  ]
  edge [
    source 46
    target 1516
  ]
  edge [
    source 46
    target 1517
  ]
  edge [
    source 46
    target 1518
  ]
  edge [
    source 46
    target 1519
  ]
  edge [
    source 46
    target 1520
  ]
  edge [
    source 46
    target 59
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1521
  ]
  edge [
    source 47
    target 1522
  ]
  edge [
    source 47
    target 178
  ]
  edge [
    source 47
    target 1523
  ]
  edge [
    source 47
    target 1524
  ]
  edge [
    source 47
    target 487
  ]
  edge [
    source 47
    target 1525
  ]
  edge [
    source 47
    target 728
  ]
  edge [
    source 47
    target 1526
  ]
  edge [
    source 47
    target 1527
  ]
  edge [
    source 47
    target 1528
  ]
  edge [
    source 47
    target 71
  ]
  edge [
    source 47
    target 93
  ]
  edge [
    source 47
    target 80
  ]
  edge [
    source 47
    target 1529
  ]
  edge [
    source 47
    target 1530
  ]
  edge [
    source 47
    target 1531
  ]
  edge [
    source 47
    target 83
  ]
  edge [
    source 47
    target 96
  ]
  edge [
    source 47
    target 1532
  ]
  edge [
    source 47
    target 1533
  ]
  edge [
    source 47
    target 1534
  ]
  edge [
    source 47
    target 829
  ]
  edge [
    source 47
    target 1535
  ]
  edge [
    source 47
    target 69
  ]
  edge [
    source 47
    target 1536
  ]
  edge [
    source 47
    target 1537
  ]
  edge [
    source 47
    target 94
  ]
  edge [
    source 47
    target 110
  ]
  edge [
    source 47
    target 1538
  ]
  edge [
    source 47
    target 1539
  ]
  edge [
    source 47
    target 1540
  ]
  edge [
    source 47
    target 1541
  ]
  edge [
    source 47
    target 992
  ]
  edge [
    source 47
    target 65
  ]
  edge [
    source 47
    target 1542
  ]
  edge [
    source 47
    target 1543
  ]
  edge [
    source 47
    target 1544
  ]
  edge [
    source 47
    target 655
  ]
  edge [
    source 47
    target 1117
  ]
  edge [
    source 47
    target 1545
  ]
  edge [
    source 47
    target 1546
  ]
  edge [
    source 47
    target 920
  ]
  edge [
    source 47
    target 1547
  ]
  edge [
    source 47
    target 1548
  ]
  edge [
    source 47
    target 1549
  ]
  edge [
    source 47
    target 1550
  ]
  edge [
    source 47
    target 1551
  ]
  edge [
    source 47
    target 1552
  ]
  edge [
    source 47
    target 1553
  ]
  edge [
    source 47
    target 1134
  ]
  edge [
    source 47
    target 1554
  ]
  edge [
    source 47
    target 1137
  ]
  edge [
    source 47
    target 1555
  ]
  edge [
    source 47
    target 1556
  ]
  edge [
    source 47
    target 1557
  ]
  edge [
    source 47
    target 1558
  ]
  edge [
    source 47
    target 1559
  ]
  edge [
    source 47
    target 1560
  ]
  edge [
    source 47
    target 828
  ]
  edge [
    source 47
    target 1561
  ]
  edge [
    source 47
    target 593
  ]
  edge [
    source 47
    target 1562
  ]
  edge [
    source 47
    target 1563
  ]
  edge [
    source 47
    target 1564
  ]
  edge [
    source 47
    target 1565
  ]
  edge [
    source 47
    target 1566
  ]
  edge [
    source 47
    target 1567
  ]
  edge [
    source 47
    target 1568
  ]
  edge [
    source 47
    target 1569
  ]
  edge [
    source 47
    target 1570
  ]
  edge [
    source 47
    target 802
  ]
  edge [
    source 47
    target 1571
  ]
  edge [
    source 47
    target 1572
  ]
  edge [
    source 47
    target 1573
  ]
  edge [
    source 47
    target 214
  ]
  edge [
    source 47
    target 198
  ]
  edge [
    source 47
    target 1574
  ]
  edge [
    source 47
    target 1575
  ]
  edge [
    source 47
    target 1576
  ]
  edge [
    source 47
    target 1577
  ]
  edge [
    source 47
    target 1578
  ]
  edge [
    source 47
    target 1579
  ]
  edge [
    source 47
    target 1580
  ]
  edge [
    source 47
    target 197
  ]
  edge [
    source 47
    target 183
  ]
  edge [
    source 47
    target 1581
  ]
  edge [
    source 47
    target 696
  ]
  edge [
    source 47
    target 1582
  ]
  edge [
    source 47
    target 1583
  ]
  edge [
    source 47
    target 1584
  ]
  edge [
    source 47
    target 1585
  ]
  edge [
    source 47
    target 538
  ]
  edge [
    source 47
    target 1586
  ]
  edge [
    source 47
    target 58
  ]
  edge [
    source 48
    target 1109
  ]
  edge [
    source 48
    target 1110
  ]
  edge [
    source 48
    target 1111
  ]
  edge [
    source 48
    target 1112
  ]
  edge [
    source 48
    target 1113
  ]
  edge [
    source 48
    target 434
  ]
  edge [
    source 48
    target 1114
  ]
  edge [
    source 48
    target 1115
  ]
  edge [
    source 48
    target 59
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 264
  ]
  edge [
    source 49
    target 265
  ]
  edge [
    source 49
    target 266
  ]
  edge [
    source 49
    target 267
  ]
  edge [
    source 49
    target 268
  ]
  edge [
    source 49
    target 269
  ]
  edge [
    source 49
    target 270
  ]
  edge [
    source 49
    target 271
  ]
  edge [
    source 49
    target 272
  ]
  edge [
    source 49
    target 273
  ]
  edge [
    source 49
    target 274
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 1587
  ]
  edge [
    source 50
    target 1588
  ]
  edge [
    source 50
    target 198
  ]
  edge [
    source 50
    target 1589
  ]
  edge [
    source 50
    target 1590
  ]
  edge [
    source 50
    target 1591
  ]
  edge [
    source 50
    target 1592
  ]
  edge [
    source 50
    target 1593
  ]
  edge [
    source 50
    target 981
  ]
  edge [
    source 50
    target 1594
  ]
  edge [
    source 50
    target 1595
  ]
  edge [
    source 50
    target 182
  ]
  edge [
    source 50
    target 1393
  ]
  edge [
    source 50
    target 1596
  ]
  edge [
    source 50
    target 1597
  ]
  edge [
    source 50
    target 1598
  ]
  edge [
    source 50
    target 1599
  ]
  edge [
    source 50
    target 1600
  ]
  edge [
    source 50
    target 186
  ]
  edge [
    source 50
    target 1601
  ]
  edge [
    source 50
    target 1602
  ]
  edge [
    source 50
    target 1603
  ]
  edge [
    source 50
    target 273
  ]
  edge [
    source 50
    target 1604
  ]
  edge [
    source 50
    target 1605
  ]
  edge [
    source 50
    target 1606
  ]
  edge [
    source 50
    target 1607
  ]
  edge [
    source 50
    target 1608
  ]
  edge [
    source 50
    target 1609
  ]
  edge [
    source 50
    target 1610
  ]
  edge [
    source 50
    target 222
  ]
  edge [
    source 50
    target 65
  ]
  edge [
    source 50
    target 1611
  ]
  edge [
    source 50
    target 1612
  ]
  edge [
    source 50
    target 107
  ]
  edge [
    source 50
    target 1613
  ]
  edge [
    source 50
    target 1614
  ]
  edge [
    source 50
    target 1615
  ]
  edge [
    source 50
    target 1616
  ]
  edge [
    source 50
    target 1617
  ]
  edge [
    source 50
    target 1618
  ]
  edge [
    source 50
    target 606
  ]
  edge [
    source 50
    target 1619
  ]
  edge [
    source 51
    target 1620
  ]
  edge [
    source 51
    target 1621
  ]
  edge [
    source 51
    target 1622
  ]
  edge [
    source 51
    target 1623
  ]
  edge [
    source 51
    target 1624
  ]
  edge [
    source 51
    target 1625
  ]
  edge [
    source 51
    target 75
  ]
  edge [
    source 51
    target 1626
  ]
  edge [
    source 51
    target 1627
  ]
  edge [
    source 51
    target 1628
  ]
  edge [
    source 51
    target 1629
  ]
  edge [
    source 51
    target 1630
  ]
  edge [
    source 51
    target 1631
  ]
  edge [
    source 51
    target 1632
  ]
  edge [
    source 51
    target 1633
  ]
  edge [
    source 51
    target 151
  ]
  edge [
    source 51
    target 1634
  ]
  edge [
    source 51
    target 1635
  ]
  edge [
    source 51
    target 1636
  ]
  edge [
    source 51
    target 1637
  ]
  edge [
    source 51
    target 1638
  ]
  edge [
    source 51
    target 1639
  ]
  edge [
    source 51
    target 1640
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1110
  ]
  edge [
    source 52
    target 252
  ]
  edge [
    source 52
    target 104
  ]
  edge [
    source 52
    target 1641
  ]
  edge [
    source 52
    target 1642
  ]
  edge [
    source 52
    target 1643
  ]
  edge [
    source 52
    target 1644
  ]
  edge [
    source 52
    target 1645
  ]
  edge [
    source 52
    target 1646
  ]
  edge [
    source 52
    target 973
  ]
  edge [
    source 52
    target 1647
  ]
  edge [
    source 52
    target 1648
  ]
  edge [
    source 52
    target 1649
  ]
  edge [
    source 52
    target 1650
  ]
  edge [
    source 52
    target 1480
  ]
  edge [
    source 52
    target 1651
  ]
  edge [
    source 52
    target 1309
  ]
  edge [
    source 52
    target 1652
  ]
  edge [
    source 52
    target 1653
  ]
  edge [
    source 52
    target 1654
  ]
  edge [
    source 52
    target 1655
  ]
  edge [
    source 52
    target 1153
  ]
  edge [
    source 52
    target 1656
  ]
  edge [
    source 52
    target 1657
  ]
  edge [
    source 52
    target 1658
  ]
  edge [
    source 52
    target 1659
  ]
  edge [
    source 52
    target 1243
  ]
  edge [
    source 52
    target 1660
  ]
  edge [
    source 52
    target 1661
  ]
  edge [
    source 52
    target 960
  ]
  edge [
    source 52
    target 1662
  ]
  edge [
    source 52
    target 1663
  ]
  edge [
    source 52
    target 1664
  ]
  edge [
    source 52
    target 1665
  ]
  edge [
    source 52
    target 1666
  ]
  edge [
    source 52
    target 1667
  ]
  edge [
    source 52
    target 1668
  ]
  edge [
    source 52
    target 1669
  ]
  edge [
    source 52
    target 1670
  ]
  edge [
    source 52
    target 1671
  ]
  edge [
    source 52
    target 1672
  ]
  edge [
    source 52
    target 1673
  ]
  edge [
    source 52
    target 1674
  ]
  edge [
    source 52
    target 466
  ]
  edge [
    source 52
    target 1675
  ]
  edge [
    source 52
    target 1676
  ]
  edge [
    source 52
    target 908
  ]
  edge [
    source 52
    target 1677
  ]
  edge [
    source 52
    target 1678
  ]
  edge [
    source 52
    target 1679
  ]
  edge [
    source 52
    target 644
  ]
  edge [
    source 52
    target 434
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 1680
  ]
  edge [
    source 53
    target 1681
  ]
  edge [
    source 53
    target 1682
  ]
  edge [
    source 53
    target 1683
  ]
  edge [
    source 53
    target 1684
  ]
  edge [
    source 53
    target 1685
  ]
  edge [
    source 53
    target 1686
  ]
  edge [
    source 53
    target 1687
  ]
  edge [
    source 53
    target 1688
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 1689
  ]
  edge [
    source 55
    target 1690
  ]
  edge [
    source 55
    target 1691
  ]
  edge [
    source 55
    target 1692
  ]
  edge [
    source 55
    target 1693
  ]
  edge [
    source 55
    target 1694
  ]
  edge [
    source 55
    target 1695
  ]
  edge [
    source 55
    target 1696
  ]
  edge [
    source 55
    target 1697
  ]
  edge [
    source 55
    target 1698
  ]
  edge [
    source 55
    target 1699
  ]
  edge [
    source 55
    target 1700
  ]
  edge [
    source 55
    target 198
  ]
  edge [
    source 55
    target 655
  ]
  edge [
    source 55
    target 1701
  ]
  edge [
    source 55
    target 1702
  ]
  edge [
    source 55
    target 1703
  ]
  edge [
    source 55
    target 1704
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 1705
  ]
  edge [
    source 56
    target 1706
  ]
  edge [
    source 56
    target 1707
  ]
  edge [
    source 56
    target 1708
  ]
  edge [
    source 56
    target 1709
  ]
  edge [
    source 56
    target 1710
  ]
  edge [
    source 56
    target 1711
  ]
  edge [
    source 56
    target 1712
  ]
  edge [
    source 56
    target 1713
  ]
  edge [
    source 56
    target 1714
  ]
  edge [
    source 56
    target 1715
  ]
  edge [
    source 56
    target 1716
  ]
  edge [
    source 56
    target 1717
  ]
  edge [
    source 56
    target 1718
  ]
  edge [
    source 56
    target 1719
  ]
  edge [
    source 56
    target 1720
  ]
  edge [
    source 56
    target 1721
  ]
  edge [
    source 56
    target 1722
  ]
  edge [
    source 56
    target 1723
  ]
  edge [
    source 56
    target 1724
  ]
  edge [
    source 56
    target 1725
  ]
  edge [
    source 56
    target 1726
  ]
  edge [
    source 56
    target 1727
  ]
  edge [
    source 56
    target 1728
  ]
  edge [
    source 56
    target 1729
  ]
  edge [
    source 56
    target 1730
  ]
  edge [
    source 56
    target 751
  ]
  edge [
    source 56
    target 1731
  ]
  edge [
    source 56
    target 1732
  ]
  edge [
    source 56
    target 290
  ]
  edge [
    source 56
    target 1733
  ]
  edge [
    source 56
    target 1734
  ]
  edge [
    source 56
    target 1735
  ]
  edge [
    source 56
    target 1736
  ]
  edge [
    source 56
    target 1737
  ]
  edge [
    source 56
    target 1738
  ]
  edge [
    source 56
    target 1739
  ]
  edge [
    source 56
    target 1740
  ]
  edge [
    source 56
    target 1741
  ]
  edge [
    source 56
    target 1742
  ]
  edge [
    source 56
    target 1743
  ]
  edge [
    source 56
    target 1744
  ]
  edge [
    source 56
    target 306
  ]
  edge [
    source 56
    target 332
  ]
  edge [
    source 56
    target 1745
  ]
  edge [
    source 56
    target 1654
  ]
  edge [
    source 56
    target 1746
  ]
  edge [
    source 56
    target 1747
  ]
  edge [
    source 56
    target 1748
  ]
  edge [
    source 56
    target 1749
  ]
  edge [
    source 56
    target 1750
  ]
  edge [
    source 56
    target 1751
  ]
  edge [
    source 56
    target 551
  ]
  edge [
    source 56
    target 1752
  ]
  edge [
    source 56
    target 1753
  ]
  edge [
    source 56
    target 1754
  ]
  edge [
    source 56
    target 1755
  ]
  edge [
    source 56
    target 1756
  ]
  edge [
    source 56
    target 285
  ]
  edge [
    source 56
    target 1757
  ]
  edge [
    source 56
    target 1758
  ]
  edge [
    source 56
    target 1759
  ]
  edge [
    source 56
    target 848
  ]
  edge [
    source 56
    target 1760
  ]
  edge [
    source 56
    target 1761
  ]
  edge [
    source 56
    target 1762
  ]
  edge [
    source 56
    target 1763
  ]
  edge [
    source 56
    target 1764
  ]
  edge [
    source 56
    target 1667
  ]
  edge [
    source 56
    target 1765
  ]
  edge [
    source 56
    target 147
  ]
  edge [
    source 56
    target 1766
  ]
  edge [
    source 56
    target 1767
  ]
  edge [
    source 56
    target 1768
  ]
  edge [
    source 56
    target 1769
  ]
  edge [
    source 56
    target 1770
  ]
  edge [
    source 56
    target 1771
  ]
  edge [
    source 56
    target 283
  ]
  edge [
    source 56
    target 1772
  ]
  edge [
    source 56
    target 1773
  ]
  edge [
    source 56
    target 1774
  ]
  edge [
    source 56
    target 1775
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 1776
  ]
  edge [
    source 57
    target 186
  ]
  edge [
    source 57
    target 1777
  ]
  edge [
    source 57
    target 1778
  ]
  edge [
    source 57
    target 1779
  ]
  edge [
    source 57
    target 1780
  ]
  edge [
    source 57
    target 1781
  ]
  edge [
    source 57
    target 1782
  ]
  edge [
    source 57
    target 1783
  ]
  edge [
    source 57
    target 198
  ]
  edge [
    source 57
    target 1523
  ]
  edge [
    source 57
    target 552
  ]
  edge [
    source 57
    target 1784
  ]
  edge [
    source 57
    target 1785
  ]
  edge [
    source 57
    target 1786
  ]
  edge [
    source 57
    target 1787
  ]
  edge [
    source 57
    target 237
  ]
  edge [
    source 57
    target 1788
  ]
  edge [
    source 57
    target 1789
  ]
  edge [
    source 57
    target 586
  ]
  edge [
    source 57
    target 1790
  ]
  edge [
    source 57
    target 1791
  ]
  edge [
    source 57
    target 1792
  ]
  edge [
    source 57
    target 1793
  ]
  edge [
    source 57
    target 1794
  ]
  edge [
    source 57
    target 1795
  ]
  edge [
    source 57
    target 425
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 1796
  ]
  edge [
    source 58
    target 1797
  ]
  edge [
    source 58
    target 1798
  ]
  edge [
    source 58
    target 1799
  ]
  edge [
    source 58
    target 1545
  ]
  edge [
    source 58
    target 1800
  ]
  edge [
    source 58
    target 1801
  ]
  edge [
    source 58
    target 1802
  ]
  edge [
    source 58
    target 1803
  ]
  edge [
    source 58
    target 1804
  ]
  edge [
    source 58
    target 1805
  ]
  edge [
    source 58
    target 500
  ]
  edge [
    source 58
    target 1806
  ]
  edge [
    source 58
    target 618
  ]
  edge [
    source 58
    target 1807
  ]
  edge [
    source 58
    target 1808
  ]
  edge [
    source 58
    target 1809
  ]
  edge [
    source 58
    target 1810
  ]
  edge [
    source 58
    target 1811
  ]
  edge [
    source 58
    target 1812
  ]
  edge [
    source 58
    target 1765
  ]
  edge [
    source 58
    target 1813
  ]
  edge [
    source 58
    target 1814
  ]
  edge [
    source 58
    target 1815
  ]
  edge [
    source 58
    target 1816
  ]
  edge [
    source 58
    target 1817
  ]
  edge [
    source 58
    target 545
  ]
  edge [
    source 58
    target 1818
  ]
  edge [
    source 58
    target 1819
  ]
  edge [
    source 58
    target 1820
  ]
  edge [
    source 58
    target 1821
  ]
  edge [
    source 58
    target 1822
  ]
  edge [
    source 58
    target 1823
  ]
  edge [
    source 58
    target 1824
  ]
  edge [
    source 58
    target 1825
  ]
  edge [
    source 58
    target 1175
  ]
  edge [
    source 58
    target 1826
  ]
  edge [
    source 58
    target 187
  ]
  edge [
    source 58
    target 1827
  ]
  edge [
    source 58
    target 1828
  ]
  edge [
    source 58
    target 1131
  ]
  edge [
    source 58
    target 1612
  ]
  edge [
    source 58
    target 1829
  ]
  edge [
    source 58
    target 1294
  ]
  edge [
    source 58
    target 211
  ]
  edge [
    source 58
    target 1830
  ]
  edge [
    source 58
    target 1831
  ]
  edge [
    source 58
    target 1832
  ]
  edge [
    source 58
    target 275
  ]
  edge [
    source 58
    target 1833
  ]
  edge [
    source 58
    target 1834
  ]
  edge [
    source 58
    target 1538
  ]
  edge [
    source 58
    target 1835
  ]
  edge [
    source 58
    target 1836
  ]
  edge [
    source 58
    target 1837
  ]
  edge [
    source 58
    target 1838
  ]
  edge [
    source 58
    target 1839
  ]
  edge [
    source 58
    target 1840
  ]
  edge [
    source 58
    target 1841
  ]
  edge [
    source 58
    target 1842
  ]
  edge [
    source 58
    target 1843
  ]
  edge [
    source 58
    target 1844
  ]
  edge [
    source 58
    target 573
  ]
  edge [
    source 58
    target 1845
  ]
  edge [
    source 58
    target 1846
  ]
  edge [
    source 58
    target 1847
  ]
  edge [
    source 58
    target 1848
  ]
  edge [
    source 58
    target 1849
  ]
  edge [
    source 58
    target 1850
  ]
  edge [
    source 58
    target 1851
  ]
  edge [
    source 58
    target 1852
  ]
  edge [
    source 58
    target 1853
  ]
  edge [
    source 58
    target 1854
  ]
  edge [
    source 58
    target 1855
  ]
  edge [
    source 58
    target 1856
  ]
  edge [
    source 58
    target 1857
  ]
  edge [
    source 58
    target 1858
  ]
  edge [
    source 58
    target 1859
  ]
  edge [
    source 58
    target 1860
  ]
  edge [
    source 58
    target 1861
  ]
  edge [
    source 58
    target 1258
  ]
  edge [
    source 58
    target 1862
  ]
  edge [
    source 58
    target 1863
  ]
  edge [
    source 58
    target 1864
  ]
  edge [
    source 58
    target 1865
  ]
  edge [
    source 58
    target 1866
  ]
  edge [
    source 58
    target 1867
  ]
  edge [
    source 58
    target 1868
  ]
  edge [
    source 58
    target 1869
  ]
  edge [
    source 58
    target 1870
  ]
  edge [
    source 58
    target 1871
  ]
  edge [
    source 58
    target 1872
  ]
  edge [
    source 58
    target 1697
  ]
  edge [
    source 58
    target 1873
  ]
  edge [
    source 58
    target 1874
  ]
  edge [
    source 58
    target 76
  ]
  edge [
    source 59
    target 1048
  ]
  edge [
    source 59
    target 1875
  ]
  edge [
    source 59
    target 1876
  ]
  edge [
    source 59
    target 1877
  ]
  edge [
    source 59
    target 1878
  ]
  edge [
    source 59
    target 1879
  ]
  edge [
    source 59
    target 949
  ]
  edge [
    source 59
    target 1880
  ]
  edge [
    source 59
    target 1881
  ]
  edge [
    source 59
    target 1882
  ]
  edge [
    source 59
    target 1883
  ]
  edge [
    source 59
    target 1337
  ]
  edge [
    source 59
    target 1338
  ]
  edge [
    source 59
    target 1339
  ]
  edge [
    source 59
    target 1046
  ]
  edge [
    source 59
    target 1340
  ]
  edge [
    source 59
    target 1341
  ]
  edge [
    source 59
    target 1342
  ]
  edge [
    source 59
    target 1343
  ]
  edge [
    source 59
    target 1344
  ]
  edge [
    source 59
    target 1345
  ]
  edge [
    source 59
    target 1346
  ]
  edge [
    source 59
    target 1884
  ]
  edge [
    source 59
    target 1885
  ]
  edge [
    source 59
    target 1886
  ]
]
