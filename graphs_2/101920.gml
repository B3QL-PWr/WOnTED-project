graph [
  node [
    id 0
    label "obowi&#261;zek"
    origin "text"
  ]
  node [
    id 1
    label "uprawnienie"
    origin "text"
  ]
  node [
    id 2
    label "stra&#380;nik"
    origin "text"
  ]
  node [
    id 3
    label "stra&#380;"
    origin "text"
  ]
  node [
    id 4
    label "miejski"
    origin "text"
  ]
  node [
    id 5
    label "obarczy&#263;"
  ]
  node [
    id 6
    label "powinno&#347;&#263;"
  ]
  node [
    id 7
    label "wym&#243;g"
  ]
  node [
    id 8
    label "zadanie"
  ]
  node [
    id 9
    label "duty"
  ]
  node [
    id 10
    label "za&#322;o&#380;enie"
  ]
  node [
    id 11
    label "zbi&#243;r"
  ]
  node [
    id 12
    label "nakarmienie"
  ]
  node [
    id 13
    label "przepisanie"
  ]
  node [
    id 14
    label "powierzanie"
  ]
  node [
    id 15
    label "przepisa&#263;"
  ]
  node [
    id 16
    label "zaszkodzenie"
  ]
  node [
    id 17
    label "problem"
  ]
  node [
    id 18
    label "zobowi&#261;zanie"
  ]
  node [
    id 19
    label "zaj&#281;cie"
  ]
  node [
    id 20
    label "yield"
  ]
  node [
    id 21
    label "work"
  ]
  node [
    id 22
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 23
    label "czynno&#347;&#263;"
  ]
  node [
    id 24
    label "stan"
  ]
  node [
    id 25
    label "umowa"
  ]
  node [
    id 26
    label "need"
  ]
  node [
    id 27
    label "condition"
  ]
  node [
    id 28
    label "potrzeba"
  ]
  node [
    id 29
    label "load"
  ]
  node [
    id 30
    label "oskar&#380;y&#263;"
  ]
  node [
    id 31
    label "blame"
  ]
  node [
    id 32
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 33
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 34
    label "odpowiedzialno&#347;&#263;"
  ]
  node [
    id 35
    label "charge"
  ]
  node [
    id 36
    label "law"
  ]
  node [
    id 37
    label "spowodowanie"
  ]
  node [
    id 38
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 39
    label "dokument"
  ]
  node [
    id 40
    label "title"
  ]
  node [
    id 41
    label "authorization"
  ]
  node [
    id 42
    label "campaign"
  ]
  node [
    id 43
    label "causing"
  ]
  node [
    id 44
    label "bezproblemowy"
  ]
  node [
    id 45
    label "wydarzenie"
  ]
  node [
    id 46
    label "activity"
  ]
  node [
    id 47
    label "operator_modalny"
  ]
  node [
    id 48
    label "alternatywa"
  ]
  node [
    id 49
    label "cecha"
  ]
  node [
    id 50
    label "wyb&#243;r"
  ]
  node [
    id 51
    label "egzekutywa"
  ]
  node [
    id 52
    label "potencja&#322;"
  ]
  node [
    id 53
    label "obliczeniowo"
  ]
  node [
    id 54
    label "ability"
  ]
  node [
    id 55
    label "posiada&#263;"
  ]
  node [
    id 56
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 57
    label "prospect"
  ]
  node [
    id 58
    label "sygnatariusz"
  ]
  node [
    id 59
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 60
    label "dokumentacja"
  ]
  node [
    id 61
    label "writing"
  ]
  node [
    id 62
    label "&#347;wiadectwo"
  ]
  node [
    id 63
    label "zapis"
  ]
  node [
    id 64
    label "artyku&#322;"
  ]
  node [
    id 65
    label "utw&#243;r"
  ]
  node [
    id 66
    label "record"
  ]
  node [
    id 67
    label "wytw&#243;r"
  ]
  node [
    id 68
    label "raport&#243;wka"
  ]
  node [
    id 69
    label "registratura"
  ]
  node [
    id 70
    label "fascyku&#322;"
  ]
  node [
    id 71
    label "parafa"
  ]
  node [
    id 72
    label "plik"
  ]
  node [
    id 73
    label "Cerber"
  ]
  node [
    id 74
    label "pracownik"
  ]
  node [
    id 75
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 76
    label "cz&#322;owiek"
  ]
  node [
    id 77
    label "delegowa&#263;"
  ]
  node [
    id 78
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 79
    label "salariat"
  ]
  node [
    id 80
    label "pracu&#347;"
  ]
  node [
    id 81
    label "r&#281;ka"
  ]
  node [
    id 82
    label "delegowanie"
  ]
  node [
    id 83
    label "wielog&#322;owy"
  ]
  node [
    id 84
    label "stra&#380;_ogniowa"
  ]
  node [
    id 85
    label "s&#322;u&#380;ba_publiczna"
  ]
  node [
    id 86
    label "ochrona"
  ]
  node [
    id 87
    label "rota"
  ]
  node [
    id 88
    label "s&#322;u&#380;ba"
  ]
  node [
    id 89
    label "wedeta"
  ]
  node [
    id 90
    label "posterunek"
  ]
  node [
    id 91
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 92
    label "czworak"
  ]
  node [
    id 93
    label "praca"
  ]
  node [
    id 94
    label "wys&#322;uga"
  ]
  node [
    id 95
    label "service"
  ]
  node [
    id 96
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 97
    label "instytucja"
  ]
  node [
    id 98
    label "ZOMO"
  ]
  node [
    id 99
    label "zesp&#243;&#322;"
  ]
  node [
    id 100
    label "tarcza"
  ]
  node [
    id 101
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 102
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 103
    label "borowiec"
  ]
  node [
    id 104
    label "obstawienie"
  ]
  node [
    id 105
    label "chemical_bond"
  ]
  node [
    id 106
    label "obiekt"
  ]
  node [
    id 107
    label "formacja"
  ]
  node [
    id 108
    label "obstawianie"
  ]
  node [
    id 109
    label "transportacja"
  ]
  node [
    id 110
    label "obstawia&#263;"
  ]
  node [
    id 111
    label "ubezpieczenie"
  ]
  node [
    id 112
    label "gwiazda"
  ]
  node [
    id 113
    label "aktorka"
  ]
  node [
    id 114
    label "postawi&#263;"
  ]
  node [
    id 115
    label "awansowa&#263;"
  ]
  node [
    id 116
    label "pozycja"
  ]
  node [
    id 117
    label "wakowa&#263;"
  ]
  node [
    id 118
    label "agencja"
  ]
  node [
    id 119
    label "warta"
  ]
  node [
    id 120
    label "awansowanie"
  ]
  node [
    id 121
    label "stawia&#263;"
  ]
  node [
    id 122
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 123
    label "papie&#380;"
  ]
  node [
    id 124
    label "&#322;amanie"
  ]
  node [
    id 125
    label "przysi&#281;ga"
  ]
  node [
    id 126
    label "piecz&#261;tka"
  ]
  node [
    id 127
    label "whip"
  ]
  node [
    id 128
    label "s&#261;d_apelacyjny"
  ]
  node [
    id 129
    label "tortury"
  ]
  node [
    id 130
    label "instrument_strunowy"
  ]
  node [
    id 131
    label "chordofon_szarpany"
  ]
  node [
    id 132
    label "formu&#322;a"
  ]
  node [
    id 133
    label "wojsko"
  ]
  node [
    id 134
    label "s&#261;d_ko&#347;cielny"
  ]
  node [
    id 135
    label "&#322;ama&#263;"
  ]
  node [
    id 136
    label "Rota"
  ]
  node [
    id 137
    label "stra&#380;_po&#380;arna"
  ]
  node [
    id 138
    label "szyk"
  ]
  node [
    id 139
    label "typowy"
  ]
  node [
    id 140
    label "publiczny"
  ]
  node [
    id 141
    label "miejsko"
  ]
  node [
    id 142
    label "miastowy"
  ]
  node [
    id 143
    label "upublicznienie"
  ]
  node [
    id 144
    label "publicznie"
  ]
  node [
    id 145
    label "upublicznianie"
  ]
  node [
    id 146
    label "jawny"
  ]
  node [
    id 147
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 148
    label "typowo"
  ]
  node [
    id 149
    label "zwyk&#322;y"
  ]
  node [
    id 150
    label "zwyczajny"
  ]
  node [
    id 151
    label "cz&#281;sty"
  ]
  node [
    id 152
    label "nowoczesny"
  ]
  node [
    id 153
    label "obywatel"
  ]
  node [
    id 154
    label "mieszczanin"
  ]
  node [
    id 155
    label "mieszcza&#324;stwo"
  ]
  node [
    id 156
    label "charakterystycznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
]
