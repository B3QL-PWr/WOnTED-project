graph [
  node [
    id 0
    label "copyright"
    origin "text"
  ]
  node [
    id 1
    label "alliance"
    origin "text"
  ]
  node [
    id 2
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 3
    label "organizacja"
    origin "text"
  ]
  node [
    id 4
    label "skupia&#263;"
    origin "text"
  ]
  node [
    id 5
    label "taki"
    origin "text"
  ]
  node [
    id 6
    label "podmiot"
    origin "text"
  ]
  node [
    id 7
    label "jak"
    origin "text"
  ]
  node [
    id 8
    label "mpaa"
    origin "text"
  ]
  node [
    id 9
    label "riaa"
    origin "text"
  ]
  node [
    id 10
    label "przedstawiciel"
    origin "text"
  ]
  node [
    id 11
    label "przemys&#322;"
    origin "text"
  ]
  node [
    id 12
    label "kultura"
    origin "text"
  ]
  node [
    id 13
    label "usa"
    origin "text"
  ]
  node [
    id 14
    label "tzw"
    origin "text"
  ]
  node [
    id 15
    label "content"
    origin "text"
  ]
  node [
    id 16
    label "rozes&#322;a&#263;"
    origin "text"
  ]
  node [
    id 17
    label "osoba"
    origin "text"
  ]
  node [
    id 18
    label "ubiega&#263;"
    origin "text"
  ]
  node [
    id 19
    label "si&#281;"
    origin "text"
  ]
  node [
    id 20
    label "kandydowa&#263;"
    origin "text"
  ]
  node [
    id 21
    label "wybory"
    origin "text"
  ]
  node [
    id 22
    label "prezydencki"
    origin "text"
  ]
  node [
    id 23
    label "rocznik"
    origin "text"
  ]
  node [
    id 24
    label "list"
    origin "text"
  ]
  node [
    id 25
    label "po&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 26
    label "kwestionariusz"
    origin "text"
  ]
  node [
    id 27
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 28
    label "sondowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "polityk"
    origin "text"
  ]
  node [
    id 30
    label "sprawa"
    origin "text"
  ]
  node [
    id 31
    label "circa"
    origin "text"
  ]
  node [
    id 32
    label "apelowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "zaostrzenie"
    origin "text"
  ]
  node [
    id 34
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 35
    label "skuteczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 36
    label "walka"
    origin "text"
  ]
  node [
    id 37
    label "piractwo"
    origin "text"
  ]
  node [
    id 38
    label "dla"
    origin "text"
  ]
  node [
    id 39
    label "dobra"
    origin "text"
  ]
  node [
    id 40
    label "tylko"
    origin "text"
  ]
  node [
    id 41
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 42
    label "milion"
    origin "text"
  ]
  node [
    id 43
    label "&#380;yj&#261;cy"
    origin "text"
  ]
  node [
    id 44
    label "prawy"
    origin "text"
  ]
  node [
    id 45
    label "autorski"
    origin "text"
  ]
  node [
    id 46
    label "ale"
    origin "text"
  ]
  node [
    id 47
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 48
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 49
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 50
    label "j&#281;zyk_angielski"
  ]
  node [
    id 51
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 52
    label "fajny"
  ]
  node [
    id 53
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 54
    label "po_ameryka&#324;sku"
  ]
  node [
    id 55
    label "typowy"
  ]
  node [
    id 56
    label "anglosaski"
  ]
  node [
    id 57
    label "boston"
  ]
  node [
    id 58
    label "pepperoni"
  ]
  node [
    id 59
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 60
    label "nowoczesny"
  ]
  node [
    id 61
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 62
    label "zachodni"
  ]
  node [
    id 63
    label "Princeton"
  ]
  node [
    id 64
    label "charakterystyczny"
  ]
  node [
    id 65
    label "cake-walk"
  ]
  node [
    id 66
    label "po_anglosasku"
  ]
  node [
    id 67
    label "anglosasko"
  ]
  node [
    id 68
    label "zachodny"
  ]
  node [
    id 69
    label "nowy"
  ]
  node [
    id 70
    label "nowo&#380;ytny"
  ]
  node [
    id 71
    label "otwarty"
  ]
  node [
    id 72
    label "nowocze&#347;nie"
  ]
  node [
    id 73
    label "byczy"
  ]
  node [
    id 74
    label "fajnie"
  ]
  node [
    id 75
    label "klawy"
  ]
  node [
    id 76
    label "dobry"
  ]
  node [
    id 77
    label "charakterystycznie"
  ]
  node [
    id 78
    label "szczeg&#243;lny"
  ]
  node [
    id 79
    label "wyj&#261;tkowy"
  ]
  node [
    id 80
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 81
    label "podobny"
  ]
  node [
    id 82
    label "zwyczajny"
  ]
  node [
    id 83
    label "typowo"
  ]
  node [
    id 84
    label "cz&#281;sty"
  ]
  node [
    id 85
    label "zwyk&#322;y"
  ]
  node [
    id 86
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 87
    label "nale&#380;ny"
  ]
  node [
    id 88
    label "nale&#380;yty"
  ]
  node [
    id 89
    label "uprawniony"
  ]
  node [
    id 90
    label "zasadniczy"
  ]
  node [
    id 91
    label "stosownie"
  ]
  node [
    id 92
    label "prawdziwy"
  ]
  node [
    id 93
    label "ten"
  ]
  node [
    id 94
    label "taniec_towarzyski"
  ]
  node [
    id 95
    label "melodia"
  ]
  node [
    id 96
    label "taniec"
  ]
  node [
    id 97
    label "tkanina_we&#322;niana"
  ]
  node [
    id 98
    label "walc"
  ]
  node [
    id 99
    label "ubrani&#243;wka"
  ]
  node [
    id 100
    label "salami"
  ]
  node [
    id 101
    label "jednostka_organizacyjna"
  ]
  node [
    id 102
    label "struktura"
  ]
  node [
    id 103
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 104
    label "TOPR"
  ]
  node [
    id 105
    label "endecki"
  ]
  node [
    id 106
    label "zesp&#243;&#322;"
  ]
  node [
    id 107
    label "przedstawicielstwo"
  ]
  node [
    id 108
    label "od&#322;am"
  ]
  node [
    id 109
    label "Cepelia"
  ]
  node [
    id 110
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 111
    label "ZBoWiD"
  ]
  node [
    id 112
    label "organization"
  ]
  node [
    id 113
    label "centrala"
  ]
  node [
    id 114
    label "GOPR"
  ]
  node [
    id 115
    label "ZOMO"
  ]
  node [
    id 116
    label "ZMP"
  ]
  node [
    id 117
    label "komitet_koordynacyjny"
  ]
  node [
    id 118
    label "przybud&#243;wka"
  ]
  node [
    id 119
    label "boj&#243;wka"
  ]
  node [
    id 120
    label "mechanika"
  ]
  node [
    id 121
    label "o&#347;"
  ]
  node [
    id 122
    label "usenet"
  ]
  node [
    id 123
    label "rozprz&#261;c"
  ]
  node [
    id 124
    label "zachowanie"
  ]
  node [
    id 125
    label "cybernetyk"
  ]
  node [
    id 126
    label "podsystem"
  ]
  node [
    id 127
    label "system"
  ]
  node [
    id 128
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 129
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 130
    label "sk&#322;ad"
  ]
  node [
    id 131
    label "systemat"
  ]
  node [
    id 132
    label "cecha"
  ]
  node [
    id 133
    label "konstrukcja"
  ]
  node [
    id 134
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 135
    label "konstelacja"
  ]
  node [
    id 136
    label "Mazowsze"
  ]
  node [
    id 137
    label "odm&#322;adzanie"
  ]
  node [
    id 138
    label "&#346;wietliki"
  ]
  node [
    id 139
    label "zbi&#243;r"
  ]
  node [
    id 140
    label "whole"
  ]
  node [
    id 141
    label "skupienie"
  ]
  node [
    id 142
    label "The_Beatles"
  ]
  node [
    id 143
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 144
    label "odm&#322;adza&#263;"
  ]
  node [
    id 145
    label "zabudowania"
  ]
  node [
    id 146
    label "group"
  ]
  node [
    id 147
    label "zespolik"
  ]
  node [
    id 148
    label "schorzenie"
  ]
  node [
    id 149
    label "ro&#347;lina"
  ]
  node [
    id 150
    label "grupa"
  ]
  node [
    id 151
    label "Depeche_Mode"
  ]
  node [
    id 152
    label "batch"
  ]
  node [
    id 153
    label "odm&#322;odzenie"
  ]
  node [
    id 154
    label "ajencja"
  ]
  node [
    id 155
    label "siedziba"
  ]
  node [
    id 156
    label "agencja"
  ]
  node [
    id 157
    label "bank"
  ]
  node [
    id 158
    label "filia"
  ]
  node [
    id 159
    label "kawa&#322;"
  ]
  node [
    id 160
    label "bry&#322;a"
  ]
  node [
    id 161
    label "fragment"
  ]
  node [
    id 162
    label "struktura_geologiczna"
  ]
  node [
    id 163
    label "dzia&#322;"
  ]
  node [
    id 164
    label "section"
  ]
  node [
    id 165
    label "budynek"
  ]
  node [
    id 166
    label "b&#281;ben_wielki"
  ]
  node [
    id 167
    label "Bruksela"
  ]
  node [
    id 168
    label "administration"
  ]
  node [
    id 169
    label "miejsce"
  ]
  node [
    id 170
    label "zarz&#261;d"
  ]
  node [
    id 171
    label "stopa"
  ]
  node [
    id 172
    label "o&#347;rodek"
  ]
  node [
    id 173
    label "urz&#261;dzenie"
  ]
  node [
    id 174
    label "w&#322;adza"
  ]
  node [
    id 175
    label "ratownictwo"
  ]
  node [
    id 176
    label "milicja_obywatelska"
  ]
  node [
    id 177
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 178
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 179
    label "byt"
  ]
  node [
    id 180
    label "cz&#322;owiek"
  ]
  node [
    id 181
    label "osobowo&#347;&#263;"
  ]
  node [
    id 182
    label "prawo"
  ]
  node [
    id 183
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 184
    label "nauka_prawa"
  ]
  node [
    id 185
    label "ognisko"
  ]
  node [
    id 186
    label "huddle"
  ]
  node [
    id 187
    label "zbiera&#263;"
  ]
  node [
    id 188
    label "masowa&#263;"
  ]
  node [
    id 189
    label "po&#347;wi&#281;ca&#263;"
  ]
  node [
    id 190
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 191
    label "dedicate"
  ]
  node [
    id 192
    label "robi&#263;"
  ]
  node [
    id 193
    label "przeznacza&#263;"
  ]
  node [
    id 194
    label "przejmowa&#263;"
  ]
  node [
    id 195
    label "sprz&#261;ta&#263;"
  ]
  node [
    id 196
    label "gromadzi&#263;"
  ]
  node [
    id 197
    label "mie&#263;_miejsce"
  ]
  node [
    id 198
    label "bra&#263;"
  ]
  node [
    id 199
    label "pozyskiwa&#263;"
  ]
  node [
    id 200
    label "poci&#261;ga&#263;"
  ]
  node [
    id 201
    label "wzbiera&#263;"
  ]
  node [
    id 202
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 203
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 204
    label "meet"
  ]
  node [
    id 205
    label "dostawa&#263;"
  ]
  node [
    id 206
    label "powodowa&#263;"
  ]
  node [
    id 207
    label "consolidate"
  ]
  node [
    id 208
    label "umieszcza&#263;"
  ]
  node [
    id 209
    label "uk&#322;ada&#263;"
  ]
  node [
    id 210
    label "congregate"
  ]
  node [
    id 211
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 212
    label "massage"
  ]
  node [
    id 213
    label "trze&#263;"
  ]
  node [
    id 214
    label "kulturystyka"
  ]
  node [
    id 215
    label "punkt"
  ]
  node [
    id 216
    label "skupisko"
  ]
  node [
    id 217
    label "&#347;wiat&#322;o"
  ]
  node [
    id 218
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 219
    label "impreza"
  ]
  node [
    id 220
    label "Hollywood"
  ]
  node [
    id 221
    label "center"
  ]
  node [
    id 222
    label "palenisko"
  ]
  node [
    id 223
    label "watra"
  ]
  node [
    id 224
    label "hotbed"
  ]
  node [
    id 225
    label "skupi&#263;"
  ]
  node [
    id 226
    label "okre&#347;lony"
  ]
  node [
    id 227
    label "jaki&#347;"
  ]
  node [
    id 228
    label "przyzwoity"
  ]
  node [
    id 229
    label "ciekawy"
  ]
  node [
    id 230
    label "jako&#347;"
  ]
  node [
    id 231
    label "jako_tako"
  ]
  node [
    id 232
    label "niez&#322;y"
  ]
  node [
    id 233
    label "dziwny"
  ]
  node [
    id 234
    label "wiadomy"
  ]
  node [
    id 235
    label "ludzko&#347;&#263;"
  ]
  node [
    id 236
    label "asymilowanie"
  ]
  node [
    id 237
    label "wapniak"
  ]
  node [
    id 238
    label "asymilowa&#263;"
  ]
  node [
    id 239
    label "os&#322;abia&#263;"
  ]
  node [
    id 240
    label "posta&#263;"
  ]
  node [
    id 241
    label "hominid"
  ]
  node [
    id 242
    label "podw&#322;adny"
  ]
  node [
    id 243
    label "os&#322;abianie"
  ]
  node [
    id 244
    label "g&#322;owa"
  ]
  node [
    id 245
    label "figura"
  ]
  node [
    id 246
    label "portrecista"
  ]
  node [
    id 247
    label "dwun&#243;g"
  ]
  node [
    id 248
    label "profanum"
  ]
  node [
    id 249
    label "mikrokosmos"
  ]
  node [
    id 250
    label "nasada"
  ]
  node [
    id 251
    label "duch"
  ]
  node [
    id 252
    label "antropochoria"
  ]
  node [
    id 253
    label "wz&#243;r"
  ]
  node [
    id 254
    label "senior"
  ]
  node [
    id 255
    label "oddzia&#322;ywanie"
  ]
  node [
    id 256
    label "Adam"
  ]
  node [
    id 257
    label "homo_sapiens"
  ]
  node [
    id 258
    label "polifag"
  ]
  node [
    id 259
    label "utrzymywanie"
  ]
  node [
    id 260
    label "bycie"
  ]
  node [
    id 261
    label "entity"
  ]
  node [
    id 262
    label "subsystencja"
  ]
  node [
    id 263
    label "utrzyma&#263;"
  ]
  node [
    id 264
    label "egzystencja"
  ]
  node [
    id 265
    label "wy&#380;ywienie"
  ]
  node [
    id 266
    label "ontologicznie"
  ]
  node [
    id 267
    label "utrzymanie"
  ]
  node [
    id 268
    label "potencja"
  ]
  node [
    id 269
    label "utrzymywa&#263;"
  ]
  node [
    id 270
    label "mentalno&#347;&#263;"
  ]
  node [
    id 271
    label "superego"
  ]
  node [
    id 272
    label "psychika"
  ]
  node [
    id 273
    label "charakter"
  ]
  node [
    id 274
    label "wn&#281;trze"
  ]
  node [
    id 275
    label "self"
  ]
  node [
    id 276
    label "status"
  ]
  node [
    id 277
    label "umocowa&#263;"
  ]
  node [
    id 278
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 279
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 280
    label "procesualistyka"
  ]
  node [
    id 281
    label "regu&#322;a_Allena"
  ]
  node [
    id 282
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 283
    label "kryminalistyka"
  ]
  node [
    id 284
    label "szko&#322;a"
  ]
  node [
    id 285
    label "kierunek"
  ]
  node [
    id 286
    label "zasada_d'Alemberta"
  ]
  node [
    id 287
    label "obserwacja"
  ]
  node [
    id 288
    label "normatywizm"
  ]
  node [
    id 289
    label "jurisprudence"
  ]
  node [
    id 290
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 291
    label "kultura_duchowa"
  ]
  node [
    id 292
    label "przepis"
  ]
  node [
    id 293
    label "prawo_karne_procesowe"
  ]
  node [
    id 294
    label "criterion"
  ]
  node [
    id 295
    label "kazuistyka"
  ]
  node [
    id 296
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 297
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 298
    label "kryminologia"
  ]
  node [
    id 299
    label "opis"
  ]
  node [
    id 300
    label "regu&#322;a_Glogera"
  ]
  node [
    id 301
    label "prawo_Mendla"
  ]
  node [
    id 302
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 303
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 304
    label "prawo_karne"
  ]
  node [
    id 305
    label "legislacyjnie"
  ]
  node [
    id 306
    label "twierdzenie"
  ]
  node [
    id 307
    label "cywilistyka"
  ]
  node [
    id 308
    label "judykatura"
  ]
  node [
    id 309
    label "kanonistyka"
  ]
  node [
    id 310
    label "standard"
  ]
  node [
    id 311
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 312
    label "law"
  ]
  node [
    id 313
    label "qualification"
  ]
  node [
    id 314
    label "dominion"
  ]
  node [
    id 315
    label "wykonawczy"
  ]
  node [
    id 316
    label "zasada"
  ]
  node [
    id 317
    label "normalizacja"
  ]
  node [
    id 318
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 319
    label "zobo"
  ]
  node [
    id 320
    label "yakalo"
  ]
  node [
    id 321
    label "byd&#322;o"
  ]
  node [
    id 322
    label "dzo"
  ]
  node [
    id 323
    label "kr&#281;torogie"
  ]
  node [
    id 324
    label "czochrad&#322;o"
  ]
  node [
    id 325
    label "posp&#243;lstwo"
  ]
  node [
    id 326
    label "kraal"
  ]
  node [
    id 327
    label "livestock"
  ]
  node [
    id 328
    label "prze&#380;uwacz"
  ]
  node [
    id 329
    label "bizon"
  ]
  node [
    id 330
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 331
    label "zebu"
  ]
  node [
    id 332
    label "byd&#322;o_domowe"
  ]
  node [
    id 333
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 334
    label "cz&#322;onek"
  ]
  node [
    id 335
    label "przyk&#322;ad"
  ]
  node [
    id 336
    label "substytuowa&#263;"
  ]
  node [
    id 337
    label "substytuowanie"
  ]
  node [
    id 338
    label "zast&#281;pca"
  ]
  node [
    id 339
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 340
    label "organ"
  ]
  node [
    id 341
    label "ptaszek"
  ]
  node [
    id 342
    label "element_anatomiczny"
  ]
  node [
    id 343
    label "cia&#322;o"
  ]
  node [
    id 344
    label "przyrodzenie"
  ]
  node [
    id 345
    label "fiut"
  ]
  node [
    id 346
    label "shaft"
  ]
  node [
    id 347
    label "wchodzenie"
  ]
  node [
    id 348
    label "wej&#347;cie"
  ]
  node [
    id 349
    label "wskazywanie"
  ]
  node [
    id 350
    label "pe&#322;nomocnik"
  ]
  node [
    id 351
    label "podstawienie"
  ]
  node [
    id 352
    label "wskazanie"
  ]
  node [
    id 353
    label "podstawianie"
  ]
  node [
    id 354
    label "wskaza&#263;"
  ]
  node [
    id 355
    label "podstawi&#263;"
  ]
  node [
    id 356
    label "wskazywa&#263;"
  ]
  node [
    id 357
    label "zast&#261;pi&#263;"
  ]
  node [
    id 358
    label "zast&#281;powa&#263;"
  ]
  node [
    id 359
    label "podstawia&#263;"
  ]
  node [
    id 360
    label "protezowa&#263;"
  ]
  node [
    id 361
    label "fakt"
  ]
  node [
    id 362
    label "czyn"
  ]
  node [
    id 363
    label "ilustracja"
  ]
  node [
    id 364
    label "przemys&#322;&#243;wka"
  ]
  node [
    id 365
    label "uprzemys&#322;awianie_si&#281;"
  ]
  node [
    id 366
    label "uprzemys&#322;owienie"
  ]
  node [
    id 367
    label "uprzemys&#322;owienie_si&#281;"
  ]
  node [
    id 368
    label "przechowalnictwo"
  ]
  node [
    id 369
    label "uprzemys&#322;awianie"
  ]
  node [
    id 370
    label "gospodarka"
  ]
  node [
    id 371
    label "przemys&#322;_spo&#380;ywczy"
  ]
  node [
    id 372
    label "wiedza"
  ]
  node [
    id 373
    label "dobra_konsumpcyjne"
  ]
  node [
    id 374
    label "produkt"
  ]
  node [
    id 375
    label "inwentarz"
  ]
  node [
    id 376
    label "rynek"
  ]
  node [
    id 377
    label "mieszkalnictwo"
  ]
  node [
    id 378
    label "agregat_ekonomiczny"
  ]
  node [
    id 379
    label "miejsce_pracy"
  ]
  node [
    id 380
    label "produkowanie"
  ]
  node [
    id 381
    label "farmaceutyka"
  ]
  node [
    id 382
    label "rolnictwo"
  ]
  node [
    id 383
    label "transport"
  ]
  node [
    id 384
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 385
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 386
    label "obronno&#347;&#263;"
  ]
  node [
    id 387
    label "sektor_prywatny"
  ]
  node [
    id 388
    label "sch&#322;adza&#263;"
  ]
  node [
    id 389
    label "czerwona_strefa"
  ]
  node [
    id 390
    label "pole"
  ]
  node [
    id 391
    label "sektor_publiczny"
  ]
  node [
    id 392
    label "bankowo&#347;&#263;"
  ]
  node [
    id 393
    label "gospodarowanie"
  ]
  node [
    id 394
    label "obora"
  ]
  node [
    id 395
    label "gospodarka_wodna"
  ]
  node [
    id 396
    label "gospodarka_le&#347;na"
  ]
  node [
    id 397
    label "gospodarowa&#263;"
  ]
  node [
    id 398
    label "fabryka"
  ]
  node [
    id 399
    label "wytw&#243;rnia"
  ]
  node [
    id 400
    label "stodo&#322;a"
  ]
  node [
    id 401
    label "spichlerz"
  ]
  node [
    id 402
    label "sch&#322;adzanie"
  ]
  node [
    id 403
    label "administracja"
  ]
  node [
    id 404
    label "sch&#322;odzenie"
  ]
  node [
    id 405
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 406
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 407
    label "regulacja_cen"
  ]
  node [
    id 408
    label "szkolnictwo"
  ]
  node [
    id 409
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 410
    label "rozwini&#281;cie"
  ]
  node [
    id 411
    label "proces_ekonomiczny"
  ]
  node [
    id 412
    label "modernizacja"
  ]
  node [
    id 413
    label "spowodowanie"
  ]
  node [
    id 414
    label "industrialization"
  ]
  node [
    id 415
    label "czynno&#347;&#263;"
  ]
  node [
    id 416
    label "powodowanie"
  ]
  node [
    id 417
    label "asymilowanie_si&#281;"
  ]
  node [
    id 418
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 419
    label "Wsch&#243;d"
  ]
  node [
    id 420
    label "przedmiot"
  ]
  node [
    id 421
    label "praca_rolnicza"
  ]
  node [
    id 422
    label "przejmowanie"
  ]
  node [
    id 423
    label "zjawisko"
  ]
  node [
    id 424
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 425
    label "makrokosmos"
  ]
  node [
    id 426
    label "rzecz"
  ]
  node [
    id 427
    label "konwencja"
  ]
  node [
    id 428
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 429
    label "propriety"
  ]
  node [
    id 430
    label "brzoskwiniarnia"
  ]
  node [
    id 431
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 432
    label "sztuka"
  ]
  node [
    id 433
    label "zwyczaj"
  ]
  node [
    id 434
    label "jako&#347;&#263;"
  ]
  node [
    id 435
    label "kuchnia"
  ]
  node [
    id 436
    label "tradycja"
  ]
  node [
    id 437
    label "populace"
  ]
  node [
    id 438
    label "hodowla"
  ]
  node [
    id 439
    label "religia"
  ]
  node [
    id 440
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 441
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 442
    label "przej&#281;cie"
  ]
  node [
    id 443
    label "przej&#261;&#263;"
  ]
  node [
    id 444
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 445
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 446
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 447
    label "warto&#347;&#263;"
  ]
  node [
    id 448
    label "quality"
  ]
  node [
    id 449
    label "co&#347;"
  ]
  node [
    id 450
    label "state"
  ]
  node [
    id 451
    label "syf"
  ]
  node [
    id 452
    label "absolutorium"
  ]
  node [
    id 453
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 454
    label "dzia&#322;anie"
  ]
  node [
    id 455
    label "activity"
  ]
  node [
    id 456
    label "proces"
  ]
  node [
    id 457
    label "boski"
  ]
  node [
    id 458
    label "krajobraz"
  ]
  node [
    id 459
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 460
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 461
    label "przywidzenie"
  ]
  node [
    id 462
    label "presence"
  ]
  node [
    id 463
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 464
    label "potrzymanie"
  ]
  node [
    id 465
    label "pod&#243;j"
  ]
  node [
    id 466
    label "filiacja"
  ]
  node [
    id 467
    label "licencjonowanie"
  ]
  node [
    id 468
    label "opasa&#263;"
  ]
  node [
    id 469
    label "ch&#243;w"
  ]
  node [
    id 470
    label "licencja"
  ]
  node [
    id 471
    label "sokolarnia"
  ]
  node [
    id 472
    label "potrzyma&#263;"
  ]
  node [
    id 473
    label "rozp&#322;&#243;d"
  ]
  node [
    id 474
    label "grupa_organizm&#243;w"
  ]
  node [
    id 475
    label "wypas"
  ]
  node [
    id 476
    label "wychowalnia"
  ]
  node [
    id 477
    label "pstr&#261;garnia"
  ]
  node [
    id 478
    label "krzy&#380;owanie"
  ]
  node [
    id 479
    label "licencjonowa&#263;"
  ]
  node [
    id 480
    label "odch&#243;w"
  ]
  node [
    id 481
    label "tucz"
  ]
  node [
    id 482
    label "ud&#243;j"
  ]
  node [
    id 483
    label "klatka"
  ]
  node [
    id 484
    label "opasienie"
  ]
  node [
    id 485
    label "wych&#243;w"
  ]
  node [
    id 486
    label "obrz&#261;dek"
  ]
  node [
    id 487
    label "opasanie"
  ]
  node [
    id 488
    label "polish"
  ]
  node [
    id 489
    label "akwarium"
  ]
  node [
    id 490
    label "biotechnika"
  ]
  node [
    id 491
    label "charakterystyka"
  ]
  node [
    id 492
    label "m&#322;ot"
  ]
  node [
    id 493
    label "znak"
  ]
  node [
    id 494
    label "drzewo"
  ]
  node [
    id 495
    label "pr&#243;ba"
  ]
  node [
    id 496
    label "attribute"
  ]
  node [
    id 497
    label "marka"
  ]
  node [
    id 498
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 499
    label "uk&#322;ad"
  ]
  node [
    id 500
    label "styl"
  ]
  node [
    id 501
    label "line"
  ]
  node [
    id 502
    label "kanon"
  ]
  node [
    id 503
    label "zjazd"
  ]
  node [
    id 504
    label "biom"
  ]
  node [
    id 505
    label "szata_ro&#347;linna"
  ]
  node [
    id 506
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 507
    label "formacja_ro&#347;linna"
  ]
  node [
    id 508
    label "przyroda"
  ]
  node [
    id 509
    label "zielono&#347;&#263;"
  ]
  node [
    id 510
    label "pi&#281;tro"
  ]
  node [
    id 511
    label "plant"
  ]
  node [
    id 512
    label "geosystem"
  ]
  node [
    id 513
    label "pr&#243;bowanie"
  ]
  node [
    id 514
    label "rola"
  ]
  node [
    id 515
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 516
    label "realizacja"
  ]
  node [
    id 517
    label "scena"
  ]
  node [
    id 518
    label "didaskalia"
  ]
  node [
    id 519
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 520
    label "environment"
  ]
  node [
    id 521
    label "head"
  ]
  node [
    id 522
    label "scenariusz"
  ]
  node [
    id 523
    label "egzemplarz"
  ]
  node [
    id 524
    label "jednostka"
  ]
  node [
    id 525
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 526
    label "utw&#243;r"
  ]
  node [
    id 527
    label "fortel"
  ]
  node [
    id 528
    label "theatrical_performance"
  ]
  node [
    id 529
    label "ambala&#380;"
  ]
  node [
    id 530
    label "sprawno&#347;&#263;"
  ]
  node [
    id 531
    label "kobieta"
  ]
  node [
    id 532
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 533
    label "Faust"
  ]
  node [
    id 534
    label "scenografia"
  ]
  node [
    id 535
    label "ods&#322;ona"
  ]
  node [
    id 536
    label "turn"
  ]
  node [
    id 537
    label "pokaz"
  ]
  node [
    id 538
    label "ilo&#347;&#263;"
  ]
  node [
    id 539
    label "przedstawienie"
  ]
  node [
    id 540
    label "przedstawi&#263;"
  ]
  node [
    id 541
    label "Apollo"
  ]
  node [
    id 542
    label "przedstawianie"
  ]
  node [
    id 543
    label "przedstawia&#263;"
  ]
  node [
    id 544
    label "towar"
  ]
  node [
    id 545
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 546
    label "ceremony"
  ]
  node [
    id 547
    label "kult"
  ]
  node [
    id 548
    label "mitologia"
  ]
  node [
    id 549
    label "wyznanie"
  ]
  node [
    id 550
    label "ideologia"
  ]
  node [
    id 551
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 552
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 553
    label "nawracanie_si&#281;"
  ]
  node [
    id 554
    label "duchowny"
  ]
  node [
    id 555
    label "rela"
  ]
  node [
    id 556
    label "kosmologia"
  ]
  node [
    id 557
    label "kosmogonia"
  ]
  node [
    id 558
    label "nawraca&#263;"
  ]
  node [
    id 559
    label "mistyka"
  ]
  node [
    id 560
    label "staro&#347;cina_weselna"
  ]
  node [
    id 561
    label "folklor"
  ]
  node [
    id 562
    label "objawienie"
  ]
  node [
    id 563
    label "dorobek"
  ]
  node [
    id 564
    label "tworzenie"
  ]
  node [
    id 565
    label "kreacja"
  ]
  node [
    id 566
    label "creation"
  ]
  node [
    id 567
    label "zaj&#281;cie"
  ]
  node [
    id 568
    label "instytucja"
  ]
  node [
    id 569
    label "tajniki"
  ]
  node [
    id 570
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 571
    label "jedzenie"
  ]
  node [
    id 572
    label "zaplecze"
  ]
  node [
    id 573
    label "pomieszczenie"
  ]
  node [
    id 574
    label "zlewozmywak"
  ]
  node [
    id 575
    label "gotowa&#263;"
  ]
  node [
    id 576
    label "ciemna_materia"
  ]
  node [
    id 577
    label "planeta"
  ]
  node [
    id 578
    label "ekosfera"
  ]
  node [
    id 579
    label "przestrze&#324;"
  ]
  node [
    id 580
    label "czarna_dziura"
  ]
  node [
    id 581
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 582
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 583
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 584
    label "kosmos"
  ]
  node [
    id 585
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 586
    label "poprawno&#347;&#263;"
  ]
  node [
    id 587
    label "og&#322;ada"
  ]
  node [
    id 588
    label "service"
  ]
  node [
    id 589
    label "stosowno&#347;&#263;"
  ]
  node [
    id 590
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 591
    label "Ukraina"
  ]
  node [
    id 592
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 593
    label "blok_wschodni"
  ]
  node [
    id 594
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 595
    label "wsch&#243;d"
  ]
  node [
    id 596
    label "Europa_Wschodnia"
  ]
  node [
    id 597
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 598
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 599
    label "treat"
  ]
  node [
    id 600
    label "czerpa&#263;"
  ]
  node [
    id 601
    label "go"
  ]
  node [
    id 602
    label "handle"
  ]
  node [
    id 603
    label "wzbudza&#263;"
  ]
  node [
    id 604
    label "ogarnia&#263;"
  ]
  node [
    id 605
    label "bang"
  ]
  node [
    id 606
    label "wzi&#261;&#263;"
  ]
  node [
    id 607
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 608
    label "stimulate"
  ]
  node [
    id 609
    label "ogarn&#261;&#263;"
  ]
  node [
    id 610
    label "wzbudzi&#263;"
  ]
  node [
    id 611
    label "thrill"
  ]
  node [
    id 612
    label "czerpanie"
  ]
  node [
    id 613
    label "acquisition"
  ]
  node [
    id 614
    label "branie"
  ]
  node [
    id 615
    label "caparison"
  ]
  node [
    id 616
    label "movement"
  ]
  node [
    id 617
    label "wzbudzanie"
  ]
  node [
    id 618
    label "ogarnianie"
  ]
  node [
    id 619
    label "wra&#380;enie"
  ]
  node [
    id 620
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 621
    label "interception"
  ]
  node [
    id 622
    label "wzbudzenie"
  ]
  node [
    id 623
    label "emotion"
  ]
  node [
    id 624
    label "zaczerpni&#281;cie"
  ]
  node [
    id 625
    label "wzi&#281;cie"
  ]
  node [
    id 626
    label "zboczenie"
  ]
  node [
    id 627
    label "om&#243;wienie"
  ]
  node [
    id 628
    label "sponiewieranie"
  ]
  node [
    id 629
    label "discipline"
  ]
  node [
    id 630
    label "omawia&#263;"
  ]
  node [
    id 631
    label "kr&#261;&#380;enie"
  ]
  node [
    id 632
    label "tre&#347;&#263;"
  ]
  node [
    id 633
    label "robienie"
  ]
  node [
    id 634
    label "sponiewiera&#263;"
  ]
  node [
    id 635
    label "element"
  ]
  node [
    id 636
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 637
    label "tematyka"
  ]
  node [
    id 638
    label "w&#261;tek"
  ]
  node [
    id 639
    label "zbaczanie"
  ]
  node [
    id 640
    label "program_nauczania"
  ]
  node [
    id 641
    label "om&#243;wi&#263;"
  ]
  node [
    id 642
    label "omawianie"
  ]
  node [
    id 643
    label "thing"
  ]
  node [
    id 644
    label "istota"
  ]
  node [
    id 645
    label "zbacza&#263;"
  ]
  node [
    id 646
    label "zboczy&#263;"
  ]
  node [
    id 647
    label "object"
  ]
  node [
    id 648
    label "temat"
  ]
  node [
    id 649
    label "wpadni&#281;cie"
  ]
  node [
    id 650
    label "mienie"
  ]
  node [
    id 651
    label "obiekt"
  ]
  node [
    id 652
    label "wpa&#347;&#263;"
  ]
  node [
    id 653
    label "wpadanie"
  ]
  node [
    id 654
    label "wpada&#263;"
  ]
  node [
    id 655
    label "uprawa"
  ]
  node [
    id 656
    label "ship"
  ]
  node [
    id 657
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 658
    label "&#322;&#243;&#380;ko"
  ]
  node [
    id 659
    label "przes&#322;a&#263;"
  ]
  node [
    id 660
    label "wear"
  ]
  node [
    id 661
    label "rozmie&#347;ci&#263;"
  ]
  node [
    id 662
    label "peddle"
  ]
  node [
    id 663
    label "os&#322;abi&#263;"
  ]
  node [
    id 664
    label "zepsu&#263;"
  ]
  node [
    id 665
    label "zmieni&#263;"
  ]
  node [
    id 666
    label "podzieli&#263;"
  ]
  node [
    id 667
    label "spowodowa&#263;"
  ]
  node [
    id 668
    label "range"
  ]
  node [
    id 669
    label "oddali&#263;"
  ]
  node [
    id 670
    label "stagger"
  ]
  node [
    id 671
    label "note"
  ]
  node [
    id 672
    label "raise"
  ]
  node [
    id 673
    label "wygra&#263;"
  ]
  node [
    id 674
    label "przekaza&#263;"
  ]
  node [
    id 675
    label "convey"
  ]
  node [
    id 676
    label "grant"
  ]
  node [
    id 677
    label "rama_&#322;&#243;&#380;ka"
  ]
  node [
    id 678
    label "wyrko"
  ]
  node [
    id 679
    label "roz&#347;cielenie"
  ]
  node [
    id 680
    label "materac"
  ]
  node [
    id 681
    label "roz&#347;cieli&#263;"
  ]
  node [
    id 682
    label "roz&#347;cie&#322;a&#263;"
  ]
  node [
    id 683
    label "zas&#322;a&#263;"
  ]
  node [
    id 684
    label "promiskuityzm"
  ]
  node [
    id 685
    label "mebel"
  ]
  node [
    id 686
    label "wezg&#322;owie"
  ]
  node [
    id 687
    label "dopasowanie_seksualne"
  ]
  node [
    id 688
    label "s&#322;anie"
  ]
  node [
    id 689
    label "sexual_activity"
  ]
  node [
    id 690
    label "s&#322;a&#263;"
  ]
  node [
    id 691
    label "niedopasowanie_seksualne"
  ]
  node [
    id 692
    label "roz&#347;cie&#322;anie"
  ]
  node [
    id 693
    label "zas&#322;anie"
  ]
  node [
    id 694
    label "petting"
  ]
  node [
    id 695
    label "zag&#322;&#243;wek"
  ]
  node [
    id 696
    label "Chocho&#322;"
  ]
  node [
    id 697
    label "Herkules_Poirot"
  ]
  node [
    id 698
    label "Edyp"
  ]
  node [
    id 699
    label "parali&#380;owa&#263;"
  ]
  node [
    id 700
    label "Harry_Potter"
  ]
  node [
    id 701
    label "Casanova"
  ]
  node [
    id 702
    label "Gargantua"
  ]
  node [
    id 703
    label "Zgredek"
  ]
  node [
    id 704
    label "Winnetou"
  ]
  node [
    id 705
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 706
    label "Dulcynea"
  ]
  node [
    id 707
    label "kategoria_gramatyczna"
  ]
  node [
    id 708
    label "person"
  ]
  node [
    id 709
    label "Sherlock_Holmes"
  ]
  node [
    id 710
    label "Quasimodo"
  ]
  node [
    id 711
    label "Plastu&#347;"
  ]
  node [
    id 712
    label "Wallenrod"
  ]
  node [
    id 713
    label "Dwukwiat"
  ]
  node [
    id 714
    label "koniugacja"
  ]
  node [
    id 715
    label "Don_Juan"
  ]
  node [
    id 716
    label "Don_Kiszot"
  ]
  node [
    id 717
    label "Hamlet"
  ]
  node [
    id 718
    label "Werter"
  ]
  node [
    id 719
    label "Szwejk"
  ]
  node [
    id 720
    label "znaczenie"
  ]
  node [
    id 721
    label "zaistnie&#263;"
  ]
  node [
    id 722
    label "Osjan"
  ]
  node [
    id 723
    label "kto&#347;"
  ]
  node [
    id 724
    label "wygl&#261;d"
  ]
  node [
    id 725
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 726
    label "wytw&#243;r"
  ]
  node [
    id 727
    label "trim"
  ]
  node [
    id 728
    label "poby&#263;"
  ]
  node [
    id 729
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 730
    label "Aspazja"
  ]
  node [
    id 731
    label "punkt_widzenia"
  ]
  node [
    id 732
    label "kompleksja"
  ]
  node [
    id 733
    label "wytrzyma&#263;"
  ]
  node [
    id 734
    label "budowa"
  ]
  node [
    id 735
    label "formacja"
  ]
  node [
    id 736
    label "pozosta&#263;"
  ]
  node [
    id 737
    label "point"
  ]
  node [
    id 738
    label "go&#347;&#263;"
  ]
  node [
    id 739
    label "hamper"
  ]
  node [
    id 740
    label "spasm"
  ]
  node [
    id 741
    label "mrozi&#263;"
  ]
  node [
    id 742
    label "pora&#380;a&#263;"
  ]
  node [
    id 743
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 744
    label "fleksja"
  ]
  node [
    id 745
    label "liczba"
  ]
  node [
    id 746
    label "coupling"
  ]
  node [
    id 747
    label "tryb"
  ]
  node [
    id 748
    label "czas"
  ]
  node [
    id 749
    label "czasownik"
  ]
  node [
    id 750
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 751
    label "orz&#281;sek"
  ]
  node [
    id 752
    label "fotograf"
  ]
  node [
    id 753
    label "malarz"
  ]
  node [
    id 754
    label "artysta"
  ]
  node [
    id 755
    label "hipnotyzowanie"
  ]
  node [
    id 756
    label "&#347;lad"
  ]
  node [
    id 757
    label "docieranie"
  ]
  node [
    id 758
    label "natural_process"
  ]
  node [
    id 759
    label "reakcja_chemiczna"
  ]
  node [
    id 760
    label "wdzieranie_si&#281;"
  ]
  node [
    id 761
    label "act"
  ]
  node [
    id 762
    label "rezultat"
  ]
  node [
    id 763
    label "lobbysta"
  ]
  node [
    id 764
    label "pryncypa&#322;"
  ]
  node [
    id 765
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 766
    label "kszta&#322;t"
  ]
  node [
    id 767
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 768
    label "kierowa&#263;"
  ]
  node [
    id 769
    label "alkohol"
  ]
  node [
    id 770
    label "zdolno&#347;&#263;"
  ]
  node [
    id 771
    label "&#380;ycie"
  ]
  node [
    id 772
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 773
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 774
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 775
    label "dekiel"
  ]
  node [
    id 776
    label "&#347;ci&#281;cie"
  ]
  node [
    id 777
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 778
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 779
    label "&#347;ci&#281;gno"
  ]
  node [
    id 780
    label "noosfera"
  ]
  node [
    id 781
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 782
    label "makrocefalia"
  ]
  node [
    id 783
    label "ucho"
  ]
  node [
    id 784
    label "g&#243;ra"
  ]
  node [
    id 785
    label "m&#243;zg"
  ]
  node [
    id 786
    label "kierownictwo"
  ]
  node [
    id 787
    label "fryzura"
  ]
  node [
    id 788
    label "umys&#322;"
  ]
  node [
    id 789
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 790
    label "czaszka"
  ]
  node [
    id 791
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 792
    label "allochoria"
  ]
  node [
    id 793
    label "p&#322;aszczyzna"
  ]
  node [
    id 794
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 795
    label "bierka_szachowa"
  ]
  node [
    id 796
    label "obiekt_matematyczny"
  ]
  node [
    id 797
    label "gestaltyzm"
  ]
  node [
    id 798
    label "obraz"
  ]
  node [
    id 799
    label "d&#378;wi&#281;k"
  ]
  node [
    id 800
    label "character"
  ]
  node [
    id 801
    label "rze&#378;ba"
  ]
  node [
    id 802
    label "stylistyka"
  ]
  node [
    id 803
    label "figure"
  ]
  node [
    id 804
    label "antycypacja"
  ]
  node [
    id 805
    label "ornamentyka"
  ]
  node [
    id 806
    label "informacja"
  ]
  node [
    id 807
    label "facet"
  ]
  node [
    id 808
    label "popis"
  ]
  node [
    id 809
    label "wiersz"
  ]
  node [
    id 810
    label "symetria"
  ]
  node [
    id 811
    label "lingwistyka_kognitywna"
  ]
  node [
    id 812
    label "karta"
  ]
  node [
    id 813
    label "shape"
  ]
  node [
    id 814
    label "podzbi&#243;r"
  ]
  node [
    id 815
    label "perspektywa"
  ]
  node [
    id 816
    label "dziedzina"
  ]
  node [
    id 817
    label "Szekspir"
  ]
  node [
    id 818
    label "Mickiewicz"
  ]
  node [
    id 819
    label "cierpienie"
  ]
  node [
    id 820
    label "piek&#322;o"
  ]
  node [
    id 821
    label "human_body"
  ]
  node [
    id 822
    label "ofiarowywanie"
  ]
  node [
    id 823
    label "sfera_afektywna"
  ]
  node [
    id 824
    label "nekromancja"
  ]
  node [
    id 825
    label "Po&#347;wist"
  ]
  node [
    id 826
    label "podekscytowanie"
  ]
  node [
    id 827
    label "deformowanie"
  ]
  node [
    id 828
    label "sumienie"
  ]
  node [
    id 829
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 830
    label "deformowa&#263;"
  ]
  node [
    id 831
    label "zjawa"
  ]
  node [
    id 832
    label "zmar&#322;y"
  ]
  node [
    id 833
    label "istota_nadprzyrodzona"
  ]
  node [
    id 834
    label "power"
  ]
  node [
    id 835
    label "ofiarowywa&#263;"
  ]
  node [
    id 836
    label "oddech"
  ]
  node [
    id 837
    label "seksualno&#347;&#263;"
  ]
  node [
    id 838
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 839
    label "si&#322;a"
  ]
  node [
    id 840
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 841
    label "ego"
  ]
  node [
    id 842
    label "ofiarowanie"
  ]
  node [
    id 843
    label "fizjonomia"
  ]
  node [
    id 844
    label "kompleks"
  ]
  node [
    id 845
    label "zapalno&#347;&#263;"
  ]
  node [
    id 846
    label "T&#281;sknica"
  ]
  node [
    id 847
    label "ofiarowa&#263;"
  ]
  node [
    id 848
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 849
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 850
    label "passion"
  ]
  node [
    id 851
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 852
    label "atom"
  ]
  node [
    id 853
    label "odbicie"
  ]
  node [
    id 854
    label "Ziemia"
  ]
  node [
    id 855
    label "miniatura"
  ]
  node [
    id 856
    label "anticipate"
  ]
  node [
    id 857
    label "organizowa&#263;"
  ]
  node [
    id 858
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 859
    label "czyni&#263;"
  ]
  node [
    id 860
    label "give"
  ]
  node [
    id 861
    label "stylizowa&#263;"
  ]
  node [
    id 862
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 863
    label "falowa&#263;"
  ]
  node [
    id 864
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 865
    label "praca"
  ]
  node [
    id 866
    label "wydala&#263;"
  ]
  node [
    id 867
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 868
    label "tentegowa&#263;"
  ]
  node [
    id 869
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 870
    label "urz&#261;dza&#263;"
  ]
  node [
    id 871
    label "oszukiwa&#263;"
  ]
  node [
    id 872
    label "work"
  ]
  node [
    id 873
    label "ukazywa&#263;"
  ]
  node [
    id 874
    label "przerabia&#263;"
  ]
  node [
    id 875
    label "post&#281;powa&#263;"
  ]
  node [
    id 876
    label "stara&#263;_si&#281;"
  ]
  node [
    id 877
    label "draw_a_bead_on"
  ]
  node [
    id 878
    label "skrutator"
  ]
  node [
    id 879
    label "g&#322;osowanie"
  ]
  node [
    id 880
    label "reasumowa&#263;"
  ]
  node [
    id 881
    label "przeg&#322;osowanie"
  ]
  node [
    id 882
    label "reasumowanie"
  ]
  node [
    id 883
    label "akcja"
  ]
  node [
    id 884
    label "wybieranie"
  ]
  node [
    id 885
    label "poll"
  ]
  node [
    id 886
    label "vote"
  ]
  node [
    id 887
    label "decydowanie"
  ]
  node [
    id 888
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 889
    label "przeg&#322;osowywanie"
  ]
  node [
    id 890
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 891
    label "wi&#281;kszo&#347;&#263;_bezwzgl&#281;dna"
  ]
  node [
    id 892
    label "wybranie"
  ]
  node [
    id 893
    label "m&#261;&#380;_zaufania"
  ]
  node [
    id 894
    label "rewident"
  ]
  node [
    id 895
    label "yearbook"
  ]
  node [
    id 896
    label "czasopismo"
  ]
  node [
    id 897
    label "kronika"
  ]
  node [
    id 898
    label "Bund"
  ]
  node [
    id 899
    label "PPR"
  ]
  node [
    id 900
    label "Jakobici"
  ]
  node [
    id 901
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 902
    label "leksem"
  ]
  node [
    id 903
    label "SLD"
  ]
  node [
    id 904
    label "Razem"
  ]
  node [
    id 905
    label "PiS"
  ]
  node [
    id 906
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 907
    label "partia"
  ]
  node [
    id 908
    label "Kuomintang"
  ]
  node [
    id 909
    label "ZSL"
  ]
  node [
    id 910
    label "rugby"
  ]
  node [
    id 911
    label "AWS"
  ]
  node [
    id 912
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 913
    label "blok"
  ]
  node [
    id 914
    label "PO"
  ]
  node [
    id 915
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 916
    label "Federali&#347;ci"
  ]
  node [
    id 917
    label "PSL"
  ]
  node [
    id 918
    label "wojsko"
  ]
  node [
    id 919
    label "Wigowie"
  ]
  node [
    id 920
    label "ZChN"
  ]
  node [
    id 921
    label "egzekutywa"
  ]
  node [
    id 922
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 923
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 924
    label "unit"
  ]
  node [
    id 925
    label "forma"
  ]
  node [
    id 926
    label "zapis"
  ]
  node [
    id 927
    label "chronograf"
  ]
  node [
    id 928
    label "latopis"
  ]
  node [
    id 929
    label "ksi&#281;ga"
  ]
  node [
    id 930
    label "psychotest"
  ]
  node [
    id 931
    label "pismo"
  ]
  node [
    id 932
    label "communication"
  ]
  node [
    id 933
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 934
    label "wk&#322;ad"
  ]
  node [
    id 935
    label "zajawka"
  ]
  node [
    id 936
    label "ok&#322;adka"
  ]
  node [
    id 937
    label "Zwrotnica"
  ]
  node [
    id 938
    label "prasa"
  ]
  node [
    id 939
    label "znaczek_pocztowy"
  ]
  node [
    id 940
    label "li&#347;&#263;"
  ]
  node [
    id 941
    label "epistolografia"
  ]
  node [
    id 942
    label "poczta"
  ]
  node [
    id 943
    label "poczta_elektroniczna"
  ]
  node [
    id 944
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 945
    label "przesy&#322;ka"
  ]
  node [
    id 946
    label "znoszenie"
  ]
  node [
    id 947
    label "nap&#322;ywanie"
  ]
  node [
    id 948
    label "signal"
  ]
  node [
    id 949
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 950
    label "znie&#347;&#263;"
  ]
  node [
    id 951
    label "znosi&#263;"
  ]
  node [
    id 952
    label "zniesienie"
  ]
  node [
    id 953
    label "zarys"
  ]
  node [
    id 954
    label "komunikat"
  ]
  node [
    id 955
    label "depesza_emska"
  ]
  node [
    id 956
    label "dochodzenie"
  ]
  node [
    id 957
    label "doj&#347;cie"
  ]
  node [
    id 958
    label "posy&#322;ka"
  ]
  node [
    id 959
    label "nadawca"
  ]
  node [
    id 960
    label "adres"
  ]
  node [
    id 961
    label "dochodzi&#263;"
  ]
  node [
    id 962
    label "doj&#347;&#263;"
  ]
  node [
    id 963
    label "pi&#347;miennictwo"
  ]
  node [
    id 964
    label "skrytka_pocztowa"
  ]
  node [
    id 965
    label "miejscownik"
  ]
  node [
    id 966
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 967
    label "mail"
  ]
  node [
    id 968
    label "plac&#243;wka"
  ]
  node [
    id 969
    label "szybkow&#243;z"
  ]
  node [
    id 970
    label "okienko"
  ]
  node [
    id 971
    label "pi&#322;kowanie"
  ]
  node [
    id 972
    label "cz&#322;on_p&#281;dowy"
  ]
  node [
    id 973
    label "nerwacja"
  ]
  node [
    id 974
    label "&#380;y&#322;kowanie"
  ]
  node [
    id 975
    label "ogonek"
  ]
  node [
    id 976
    label "organ_ro&#347;linny"
  ]
  node [
    id 977
    label "blaszka"
  ]
  node [
    id 978
    label "listowie"
  ]
  node [
    id 979
    label "foliofag"
  ]
  node [
    id 980
    label "zjednoczy&#263;"
  ]
  node [
    id 981
    label "stworzy&#263;"
  ]
  node [
    id 982
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 983
    label "incorporate"
  ]
  node [
    id 984
    label "zrobi&#263;"
  ]
  node [
    id 985
    label "connect"
  ]
  node [
    id 986
    label "relate"
  ]
  node [
    id 987
    label "po&#322;&#261;czenie"
  ]
  node [
    id 988
    label "permit"
  ]
  node [
    id 989
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 990
    label "create"
  ]
  node [
    id 991
    label "specjalista_od_public_relations"
  ]
  node [
    id 992
    label "wizerunek"
  ]
  node [
    id 993
    label "przygotowa&#263;"
  ]
  node [
    id 994
    label "consort"
  ]
  node [
    id 995
    label "post&#261;pi&#263;"
  ]
  node [
    id 996
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 997
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 998
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 999
    label "zorganizowa&#263;"
  ]
  node [
    id 1000
    label "appoint"
  ]
  node [
    id 1001
    label "wystylizowa&#263;"
  ]
  node [
    id 1002
    label "cause"
  ]
  node [
    id 1003
    label "przerobi&#263;"
  ]
  node [
    id 1004
    label "nabra&#263;"
  ]
  node [
    id 1005
    label "make"
  ]
  node [
    id 1006
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1007
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1008
    label "wydali&#263;"
  ]
  node [
    id 1009
    label "stworzenie"
  ]
  node [
    id 1010
    label "zespolenie"
  ]
  node [
    id 1011
    label "dressing"
  ]
  node [
    id 1012
    label "pomy&#347;lenie"
  ]
  node [
    id 1013
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 1014
    label "zjednoczenie"
  ]
  node [
    id 1015
    label "phreaker"
  ]
  node [
    id 1016
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 1017
    label "joining"
  ]
  node [
    id 1018
    label "billing"
  ]
  node [
    id 1019
    label "umo&#380;liwienie"
  ]
  node [
    id 1020
    label "akt_p&#322;ciowy"
  ]
  node [
    id 1021
    label "mention"
  ]
  node [
    id 1022
    label "kontakt"
  ]
  node [
    id 1023
    label "zwi&#261;zany"
  ]
  node [
    id 1024
    label "coalescence"
  ]
  node [
    id 1025
    label "port"
  ]
  node [
    id 1026
    label "komunikacja"
  ]
  node [
    id 1027
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1028
    label "rzucenie"
  ]
  node [
    id 1029
    label "zgrzeina"
  ]
  node [
    id 1030
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1031
    label "zestawienie"
  ]
  node [
    id 1032
    label "kafeteria"
  ]
  node [
    id 1033
    label "formularz"
  ]
  node [
    id 1034
    label "dokument"
  ]
  node [
    id 1035
    label "zestaw"
  ]
  node [
    id 1036
    label "formu&#322;a"
  ]
  node [
    id 1037
    label "bar"
  ]
  node [
    id 1038
    label "gastronomia"
  ]
  node [
    id 1039
    label "zak&#322;ad"
  ]
  node [
    id 1040
    label "ankieta"
  ]
  node [
    id 1041
    label "powiada&#263;"
  ]
  node [
    id 1042
    label "komunikowa&#263;"
  ]
  node [
    id 1043
    label "inform"
  ]
  node [
    id 1044
    label "communicate"
  ]
  node [
    id 1045
    label "mawia&#263;"
  ]
  node [
    id 1046
    label "m&#243;wi&#263;"
  ]
  node [
    id 1047
    label "maca&#263;"
  ]
  node [
    id 1048
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 1049
    label "sound"
  ]
  node [
    id 1050
    label "bada&#263;"
  ]
  node [
    id 1051
    label "question"
  ]
  node [
    id 1052
    label "analizowa&#263;"
  ]
  node [
    id 1053
    label "consider"
  ]
  node [
    id 1054
    label "badany"
  ]
  node [
    id 1055
    label "poddawa&#263;"
  ]
  node [
    id 1056
    label "rozpatrywa&#263;"
  ]
  node [
    id 1057
    label "sprawdza&#263;"
  ]
  node [
    id 1058
    label "poznawa&#263;"
  ]
  node [
    id 1059
    label "wypytywa&#263;"
  ]
  node [
    id 1060
    label "decydowa&#263;"
  ]
  node [
    id 1061
    label "examine"
  ]
  node [
    id 1062
    label "dr&#243;b"
  ]
  node [
    id 1063
    label "finger"
  ]
  node [
    id 1064
    label "grope"
  ]
  node [
    id 1065
    label "hodowa&#263;"
  ]
  node [
    id 1066
    label "dotyka&#263;"
  ]
  node [
    id 1067
    label "Gorbaczow"
  ]
  node [
    id 1068
    label "Korwin"
  ]
  node [
    id 1069
    label "McCarthy"
  ]
  node [
    id 1070
    label "Goebbels"
  ]
  node [
    id 1071
    label "Miko&#322;ajczyk"
  ]
  node [
    id 1072
    label "Ziobro"
  ]
  node [
    id 1073
    label "Katon"
  ]
  node [
    id 1074
    label "dzia&#322;acz"
  ]
  node [
    id 1075
    label "Moczar"
  ]
  node [
    id 1076
    label "Gierek"
  ]
  node [
    id 1077
    label "Arafat"
  ]
  node [
    id 1078
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 1079
    label "Naser"
  ]
  node [
    id 1080
    label "Bre&#380;niew"
  ]
  node [
    id 1081
    label "Mao"
  ]
  node [
    id 1082
    label "Nixon"
  ]
  node [
    id 1083
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 1084
    label "Perykles"
  ]
  node [
    id 1085
    label "Metternich"
  ]
  node [
    id 1086
    label "Kuro&#324;"
  ]
  node [
    id 1087
    label "Borel"
  ]
  node [
    id 1088
    label "Juliusz_Cezar"
  ]
  node [
    id 1089
    label "Bierut"
  ]
  node [
    id 1090
    label "bezpartyjny"
  ]
  node [
    id 1091
    label "Leszek_Miller"
  ]
  node [
    id 1092
    label "Falandysz"
  ]
  node [
    id 1093
    label "Fidel_Castro"
  ]
  node [
    id 1094
    label "Winston_Churchill"
  ]
  node [
    id 1095
    label "Sto&#322;ypin"
  ]
  node [
    id 1096
    label "Putin"
  ]
  node [
    id 1097
    label "J&#281;drzejewicz"
  ]
  node [
    id 1098
    label "Chruszczow"
  ]
  node [
    id 1099
    label "de_Gaulle"
  ]
  node [
    id 1100
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 1101
    label "Gomu&#322;ka"
  ]
  node [
    id 1102
    label "Asnyk"
  ]
  node [
    id 1103
    label "Michnik"
  ]
  node [
    id 1104
    label "Owsiak"
  ]
  node [
    id 1105
    label "komuna"
  ]
  node [
    id 1106
    label "Polska_Rzeczpospolita_Ludowa"
  ]
  node [
    id 1107
    label "Plan_Ko&#322;&#322;&#261;tajowski"
  ]
  node [
    id 1108
    label "o&#347;wiecenie"
  ]
  node [
    id 1109
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 1110
    label "bezpartyjnie"
  ]
  node [
    id 1111
    label "niezaanga&#380;owany"
  ]
  node [
    id 1112
    label "niezale&#380;ny"
  ]
  node [
    id 1113
    label "kognicja"
  ]
  node [
    id 1114
    label "rozprawa"
  ]
  node [
    id 1115
    label "wydarzenie"
  ]
  node [
    id 1116
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1117
    label "proposition"
  ]
  node [
    id 1118
    label "przes&#322;anka"
  ]
  node [
    id 1119
    label "idea"
  ]
  node [
    id 1120
    label "przebiec"
  ]
  node [
    id 1121
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1122
    label "motyw"
  ]
  node [
    id 1123
    label "przebiegni&#281;cie"
  ]
  node [
    id 1124
    label "fabu&#322;a"
  ]
  node [
    id 1125
    label "intelekt"
  ]
  node [
    id 1126
    label "Kant"
  ]
  node [
    id 1127
    label "p&#322;&#243;d"
  ]
  node [
    id 1128
    label "cel"
  ]
  node [
    id 1129
    label "poj&#281;cie"
  ]
  node [
    id 1130
    label "pomys&#322;"
  ]
  node [
    id 1131
    label "ideacja"
  ]
  node [
    id 1132
    label "s&#261;d"
  ]
  node [
    id 1133
    label "rozumowanie"
  ]
  node [
    id 1134
    label "opracowanie"
  ]
  node [
    id 1135
    label "obrady"
  ]
  node [
    id 1136
    label "cytat"
  ]
  node [
    id 1137
    label "tekst"
  ]
  node [
    id 1138
    label "obja&#347;nienie"
  ]
  node [
    id 1139
    label "s&#261;dzenie"
  ]
  node [
    id 1140
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 1141
    label "niuansowa&#263;"
  ]
  node [
    id 1142
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 1143
    label "sk&#322;adnik"
  ]
  node [
    id 1144
    label "zniuansowa&#263;"
  ]
  node [
    id 1145
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1146
    label "przyczyna"
  ]
  node [
    id 1147
    label "wnioskowanie"
  ]
  node [
    id 1148
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 1149
    label "wyraz_pochodny"
  ]
  node [
    id 1150
    label "fraza"
  ]
  node [
    id 1151
    label "forum"
  ]
  node [
    id 1152
    label "topik"
  ]
  node [
    id 1153
    label "otoczka"
  ]
  node [
    id 1154
    label "call"
  ]
  node [
    id 1155
    label "przemawia&#263;"
  ]
  node [
    id 1156
    label "address"
  ]
  node [
    id 1157
    label "prosi&#263;"
  ]
  node [
    id 1158
    label "odwo&#322;ywa&#263;_si&#281;"
  ]
  node [
    id 1159
    label "invite"
  ]
  node [
    id 1160
    label "poleca&#263;"
  ]
  node [
    id 1161
    label "trwa&#263;"
  ]
  node [
    id 1162
    label "zaprasza&#263;"
  ]
  node [
    id 1163
    label "zach&#281;ca&#263;"
  ]
  node [
    id 1164
    label "suffice"
  ]
  node [
    id 1165
    label "preach"
  ]
  node [
    id 1166
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 1167
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1168
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 1169
    label "pies"
  ]
  node [
    id 1170
    label "zezwala&#263;"
  ]
  node [
    id 1171
    label "ask"
  ]
  node [
    id 1172
    label "spoke"
  ]
  node [
    id 1173
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1174
    label "zaczyna&#263;"
  ]
  node [
    id 1175
    label "wyg&#322;asza&#263;"
  ]
  node [
    id 1176
    label "talk"
  ]
  node [
    id 1177
    label "say"
  ]
  node [
    id 1178
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1179
    label "wydobywa&#263;"
  ]
  node [
    id 1180
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 1181
    label "cover"
  ]
  node [
    id 1182
    label "uwydatnienie"
  ]
  node [
    id 1183
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1184
    label "surowszy"
  ]
  node [
    id 1185
    label "aggravation"
  ]
  node [
    id 1186
    label "eskalowanie"
  ]
  node [
    id 1187
    label "wzmo&#380;enie"
  ]
  node [
    id 1188
    label "przyprawienie"
  ]
  node [
    id 1189
    label "ostrzejszy"
  ]
  node [
    id 1190
    label "pogorszenie"
  ]
  node [
    id 1191
    label "ostry"
  ]
  node [
    id 1192
    label "bezproblemowy"
  ]
  node [
    id 1193
    label "campaign"
  ]
  node [
    id 1194
    label "causing"
  ]
  node [
    id 1195
    label "flavorer"
  ]
  node [
    id 1196
    label "przymocowanie"
  ]
  node [
    id 1197
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1198
    label "do&#322;o&#380;enie"
  ]
  node [
    id 1199
    label "seasoning"
  ]
  node [
    id 1200
    label "powi&#281;kszenie"
  ]
  node [
    id 1201
    label "pobudzenie"
  ]
  node [
    id 1202
    label "vivification"
  ]
  node [
    id 1203
    label "exploitation"
  ]
  node [
    id 1204
    label "escalation"
  ]
  node [
    id 1205
    label "t&#281;&#380;enie"
  ]
  node [
    id 1206
    label "wzmaganie"
  ]
  node [
    id 1207
    label "uwydatnienie_si&#281;"
  ]
  node [
    id 1208
    label "stress"
  ]
  node [
    id 1209
    label "podkre&#347;lenie"
  ]
  node [
    id 1210
    label "enhancement"
  ]
  node [
    id 1211
    label "nadanie"
  ]
  node [
    id 1212
    label "zmiana"
  ]
  node [
    id 1213
    label "worsening"
  ]
  node [
    id 1214
    label "zmienienie"
  ]
  node [
    id 1215
    label "gorszy"
  ]
  node [
    id 1216
    label "zapami&#281;ta&#322;y"
  ]
  node [
    id 1217
    label "mocny"
  ]
  node [
    id 1218
    label "trudny"
  ]
  node [
    id 1219
    label "nieneutralny"
  ]
  node [
    id 1220
    label "porywczy"
  ]
  node [
    id 1221
    label "dynamiczny"
  ]
  node [
    id 1222
    label "nieprzyjazny"
  ]
  node [
    id 1223
    label "skuteczny"
  ]
  node [
    id 1224
    label "kategoryczny"
  ]
  node [
    id 1225
    label "surowy"
  ]
  node [
    id 1226
    label "silny"
  ]
  node [
    id 1227
    label "bystro"
  ]
  node [
    id 1228
    label "wyra&#378;ny"
  ]
  node [
    id 1229
    label "raptowny"
  ]
  node [
    id 1230
    label "szorstki"
  ]
  node [
    id 1231
    label "energiczny"
  ]
  node [
    id 1232
    label "intensywny"
  ]
  node [
    id 1233
    label "dramatyczny"
  ]
  node [
    id 1234
    label "zdecydowany"
  ]
  node [
    id 1235
    label "nieoboj&#281;tny"
  ]
  node [
    id 1236
    label "widoczny"
  ]
  node [
    id 1237
    label "ostrzenie"
  ]
  node [
    id 1238
    label "k&#322;uj&#261;cy"
  ]
  node [
    id 1239
    label "ci&#281;&#380;ki"
  ]
  node [
    id 1240
    label "naostrzenie"
  ]
  node [
    id 1241
    label "gryz&#261;cy"
  ]
  node [
    id 1242
    label "dokuczliwy"
  ]
  node [
    id 1243
    label "dotkliwy"
  ]
  node [
    id 1244
    label "ostro"
  ]
  node [
    id 1245
    label "jednoznaczny"
  ]
  node [
    id 1246
    label "za&#380;arcie"
  ]
  node [
    id 1247
    label "nieobyczajny"
  ]
  node [
    id 1248
    label "niebezpieczny"
  ]
  node [
    id 1249
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1250
    label "podniecaj&#261;cy"
  ]
  node [
    id 1251
    label "osch&#322;y"
  ]
  node [
    id 1252
    label "powa&#380;ny"
  ]
  node [
    id 1253
    label "agresywny"
  ]
  node [
    id 1254
    label "gro&#378;ny"
  ]
  node [
    id 1255
    label "dziki"
  ]
  node [
    id 1256
    label "zaostrzanie"
  ]
  node [
    id 1257
    label "obostrzanie"
  ]
  node [
    id 1258
    label "obostrzenie"
  ]
  node [
    id 1259
    label "doros&#322;y"
  ]
  node [
    id 1260
    label "znaczny"
  ]
  node [
    id 1261
    label "niema&#322;o"
  ]
  node [
    id 1262
    label "wiele"
  ]
  node [
    id 1263
    label "rozwini&#281;ty"
  ]
  node [
    id 1264
    label "dorodny"
  ]
  node [
    id 1265
    label "wa&#380;ny"
  ]
  node [
    id 1266
    label "du&#380;o"
  ]
  node [
    id 1267
    label "&#380;ywny"
  ]
  node [
    id 1268
    label "szczery"
  ]
  node [
    id 1269
    label "naturalny"
  ]
  node [
    id 1270
    label "naprawd&#281;"
  ]
  node [
    id 1271
    label "realnie"
  ]
  node [
    id 1272
    label "zgodny"
  ]
  node [
    id 1273
    label "m&#261;dry"
  ]
  node [
    id 1274
    label "prawdziwie"
  ]
  node [
    id 1275
    label "znacznie"
  ]
  node [
    id 1276
    label "zauwa&#380;alny"
  ]
  node [
    id 1277
    label "wynios&#322;y"
  ]
  node [
    id 1278
    label "dono&#347;ny"
  ]
  node [
    id 1279
    label "wa&#380;nie"
  ]
  node [
    id 1280
    label "istotnie"
  ]
  node [
    id 1281
    label "eksponowany"
  ]
  node [
    id 1282
    label "ukszta&#322;towany"
  ]
  node [
    id 1283
    label "do&#347;cig&#322;y"
  ]
  node [
    id 1284
    label "&#378;ra&#322;y"
  ]
  node [
    id 1285
    label "zdr&#243;w"
  ]
  node [
    id 1286
    label "dorodnie"
  ]
  node [
    id 1287
    label "okaza&#322;y"
  ]
  node [
    id 1288
    label "mocno"
  ]
  node [
    id 1289
    label "wiela"
  ]
  node [
    id 1290
    label "bardzo"
  ]
  node [
    id 1291
    label "cz&#281;sto"
  ]
  node [
    id 1292
    label "wydoro&#347;lenie"
  ]
  node [
    id 1293
    label "doro&#347;lenie"
  ]
  node [
    id 1294
    label "doro&#347;le"
  ]
  node [
    id 1295
    label "dojrzale"
  ]
  node [
    id 1296
    label "dojrza&#322;y"
  ]
  node [
    id 1297
    label "doletni"
  ]
  node [
    id 1298
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 1299
    label "effectiveness"
  ]
  node [
    id 1300
    label "feature"
  ]
  node [
    id 1301
    label "zaleta"
  ]
  node [
    id 1302
    label "wyregulowanie"
  ]
  node [
    id 1303
    label "kompetencja"
  ]
  node [
    id 1304
    label "wyregulowa&#263;"
  ]
  node [
    id 1305
    label "regulowanie"
  ]
  node [
    id 1306
    label "regulowa&#263;"
  ]
  node [
    id 1307
    label "obrona"
  ]
  node [
    id 1308
    label "zaatakowanie"
  ]
  node [
    id 1309
    label "konfrontacyjny"
  ]
  node [
    id 1310
    label "contest"
  ]
  node [
    id 1311
    label "action"
  ]
  node [
    id 1312
    label "sambo"
  ]
  node [
    id 1313
    label "rywalizacja"
  ]
  node [
    id 1314
    label "trudno&#347;&#263;"
  ]
  node [
    id 1315
    label "sp&#243;r"
  ]
  node [
    id 1316
    label "wrestle"
  ]
  node [
    id 1317
    label "military_action"
  ]
  node [
    id 1318
    label "konflikt"
  ]
  node [
    id 1319
    label "clash"
  ]
  node [
    id 1320
    label "wsp&#243;r"
  ]
  node [
    id 1321
    label "funkcja"
  ]
  node [
    id 1322
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 1323
    label "napotka&#263;"
  ]
  node [
    id 1324
    label "subiekcja"
  ]
  node [
    id 1325
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 1326
    label "k&#322;opotliwy"
  ]
  node [
    id 1327
    label "napotkanie"
  ]
  node [
    id 1328
    label "poziom"
  ]
  node [
    id 1329
    label "difficulty"
  ]
  node [
    id 1330
    label "obstacle"
  ]
  node [
    id 1331
    label "sytuacja"
  ]
  node [
    id 1332
    label "egzamin"
  ]
  node [
    id 1333
    label "liga"
  ]
  node [
    id 1334
    label "gracz"
  ]
  node [
    id 1335
    label "protection"
  ]
  node [
    id 1336
    label "poparcie"
  ]
  node [
    id 1337
    label "mecz"
  ]
  node [
    id 1338
    label "reakcja"
  ]
  node [
    id 1339
    label "defense"
  ]
  node [
    id 1340
    label "auspices"
  ]
  node [
    id 1341
    label "gra"
  ]
  node [
    id 1342
    label "ochrona"
  ]
  node [
    id 1343
    label "post&#281;powanie"
  ]
  node [
    id 1344
    label "manewr"
  ]
  node [
    id 1345
    label "defensive_structure"
  ]
  node [
    id 1346
    label "guard_duty"
  ]
  node [
    id 1347
    label "strona"
  ]
  node [
    id 1348
    label "skrytykowanie"
  ]
  node [
    id 1349
    label "time"
  ]
  node [
    id 1350
    label "nast&#261;pienie"
  ]
  node [
    id 1351
    label "oddzia&#322;anie"
  ]
  node [
    id 1352
    label "przebycie"
  ]
  node [
    id 1353
    label "upolowanie"
  ]
  node [
    id 1354
    label "wdarcie_si&#281;"
  ]
  node [
    id 1355
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 1356
    label "sport"
  ]
  node [
    id 1357
    label "progress"
  ]
  node [
    id 1358
    label "spr&#243;bowanie"
  ]
  node [
    id 1359
    label "powiedzenie"
  ]
  node [
    id 1360
    label "rozegranie"
  ]
  node [
    id 1361
    label "zrobienie"
  ]
  node [
    id 1362
    label "konfrontacyjnie"
  ]
  node [
    id 1363
    label "zapasy"
  ]
  node [
    id 1364
    label "professional_wrestling"
  ]
  node [
    id 1365
    label "przest&#281;pstwo"
  ]
  node [
    id 1366
    label "reprodukcja"
  ]
  node [
    id 1367
    label "bandytyzm"
  ]
  node [
    id 1368
    label "nielegalno&#347;&#263;"
  ]
  node [
    id 1369
    label "bootleg"
  ]
  node [
    id 1370
    label "transmisja"
  ]
  node [
    id 1371
    label "plagiarism"
  ]
  node [
    id 1372
    label "terroryzm"
  ]
  node [
    id 1373
    label "brudny"
  ]
  node [
    id 1374
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 1375
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 1376
    label "crime"
  ]
  node [
    id 1377
    label "sprawstwo"
  ]
  node [
    id 1378
    label "barbarzy&#324;stwo"
  ]
  node [
    id 1379
    label "nieprawno&#347;&#263;"
  ]
  node [
    id 1380
    label "terrorism"
  ]
  node [
    id 1381
    label "przekaz"
  ]
  node [
    id 1382
    label "program"
  ]
  node [
    id 1383
    label "powielanie"
  ]
  node [
    id 1384
    label "impression"
  ]
  node [
    id 1385
    label "picture"
  ]
  node [
    id 1386
    label "kopia"
  ]
  node [
    id 1387
    label "produkcja"
  ]
  node [
    id 1388
    label "reproduction"
  ]
  node [
    id 1389
    label "nagranie"
  ]
  node [
    id 1390
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1391
    label "jednostka_monetarna"
  ]
  node [
    id 1392
    label "centym"
  ]
  node [
    id 1393
    label "Wilko"
  ]
  node [
    id 1394
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1395
    label "frymark"
  ]
  node [
    id 1396
    label "commodity"
  ]
  node [
    id 1397
    label "integer"
  ]
  node [
    id 1398
    label "zlewanie_si&#281;"
  ]
  node [
    id 1399
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 1400
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 1401
    label "pe&#322;ny"
  ]
  node [
    id 1402
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 1403
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 1404
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 1405
    label "stan"
  ]
  node [
    id 1406
    label "immoblizacja"
  ]
  node [
    id 1407
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1408
    label "przej&#347;cie"
  ]
  node [
    id 1409
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 1410
    label "rodowo&#347;&#263;"
  ]
  node [
    id 1411
    label "patent"
  ]
  node [
    id 1412
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 1413
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 1414
    label "przej&#347;&#263;"
  ]
  node [
    id 1415
    label "possession"
  ]
  node [
    id 1416
    label "zamiana"
  ]
  node [
    id 1417
    label "maj&#261;tek"
  ]
  node [
    id 1418
    label "Iwaszkiewicz"
  ]
  node [
    id 1419
    label "report"
  ]
  node [
    id 1420
    label "dyskalkulia"
  ]
  node [
    id 1421
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 1422
    label "wynagrodzenie"
  ]
  node [
    id 1423
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1424
    label "wymienia&#263;"
  ]
  node [
    id 1425
    label "posiada&#263;"
  ]
  node [
    id 1426
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1427
    label "wycenia&#263;"
  ]
  node [
    id 1428
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1429
    label "mierzy&#263;"
  ]
  node [
    id 1430
    label "rachowa&#263;"
  ]
  node [
    id 1431
    label "count"
  ]
  node [
    id 1432
    label "tell"
  ]
  node [
    id 1433
    label "odlicza&#263;"
  ]
  node [
    id 1434
    label "dodawa&#263;"
  ]
  node [
    id 1435
    label "wyznacza&#263;"
  ]
  node [
    id 1436
    label "admit"
  ]
  node [
    id 1437
    label "policza&#263;"
  ]
  node [
    id 1438
    label "okre&#347;la&#263;"
  ]
  node [
    id 1439
    label "odejmowa&#263;"
  ]
  node [
    id 1440
    label "odmierza&#263;"
  ]
  node [
    id 1441
    label "take"
  ]
  node [
    id 1442
    label "my&#347;le&#263;"
  ]
  node [
    id 1443
    label "involve"
  ]
  node [
    id 1444
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 1445
    label "uzyskiwa&#263;"
  ]
  node [
    id 1446
    label "dociera&#263;"
  ]
  node [
    id 1447
    label "mark"
  ]
  node [
    id 1448
    label "get"
  ]
  node [
    id 1449
    label "wiedzie&#263;"
  ]
  node [
    id 1450
    label "zawiera&#263;"
  ]
  node [
    id 1451
    label "mie&#263;"
  ]
  node [
    id 1452
    label "support"
  ]
  node [
    id 1453
    label "keep_open"
  ]
  node [
    id 1454
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1455
    label "podawa&#263;"
  ]
  node [
    id 1456
    label "mienia&#263;"
  ]
  node [
    id 1457
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1458
    label "zmienia&#263;"
  ]
  node [
    id 1459
    label "zakomunikowa&#263;"
  ]
  node [
    id 1460
    label "quote"
  ]
  node [
    id 1461
    label "dawa&#263;"
  ]
  node [
    id 1462
    label "bind"
  ]
  node [
    id 1463
    label "suma"
  ]
  node [
    id 1464
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 1465
    label "nadawa&#263;"
  ]
  node [
    id 1466
    label "set"
  ]
  node [
    id 1467
    label "zaznacza&#263;"
  ]
  node [
    id 1468
    label "wybiera&#263;"
  ]
  node [
    id 1469
    label "inflict"
  ]
  node [
    id 1470
    label "ustala&#263;"
  ]
  node [
    id 1471
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 1472
    label "porywa&#263;"
  ]
  node [
    id 1473
    label "korzysta&#263;"
  ]
  node [
    id 1474
    label "wchodzi&#263;"
  ]
  node [
    id 1475
    label "poczytywa&#263;"
  ]
  node [
    id 1476
    label "levy"
  ]
  node [
    id 1477
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1478
    label "pokonywa&#263;"
  ]
  node [
    id 1479
    label "by&#263;"
  ]
  node [
    id 1480
    label "przyjmowa&#263;"
  ]
  node [
    id 1481
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 1482
    label "rucha&#263;"
  ]
  node [
    id 1483
    label "prowadzi&#263;"
  ]
  node [
    id 1484
    label "za&#380;ywa&#263;"
  ]
  node [
    id 1485
    label "otrzymywa&#263;"
  ]
  node [
    id 1486
    label "&#263;pa&#263;"
  ]
  node [
    id 1487
    label "interpretowa&#263;"
  ]
  node [
    id 1488
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1489
    label "rusza&#263;"
  ]
  node [
    id 1490
    label "chwyta&#263;"
  ]
  node [
    id 1491
    label "grza&#263;"
  ]
  node [
    id 1492
    label "wch&#322;ania&#263;"
  ]
  node [
    id 1493
    label "wygrywa&#263;"
  ]
  node [
    id 1494
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1495
    label "ucieka&#263;"
  ]
  node [
    id 1496
    label "arise"
  ]
  node [
    id 1497
    label "uprawia&#263;_seks"
  ]
  node [
    id 1498
    label "abstract"
  ]
  node [
    id 1499
    label "towarzystwo"
  ]
  node [
    id 1500
    label "atakowa&#263;"
  ]
  node [
    id 1501
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1502
    label "zalicza&#263;"
  ]
  node [
    id 1503
    label "open"
  ]
  node [
    id 1504
    label "&#322;apa&#263;"
  ]
  node [
    id 1505
    label "przewa&#380;a&#263;"
  ]
  node [
    id 1506
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 1507
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 1508
    label "signify"
  ]
  node [
    id 1509
    label "style"
  ]
  node [
    id 1510
    label "umowa"
  ]
  node [
    id 1511
    label "stanowisko_archeologiczne"
  ]
  node [
    id 1512
    label "wlicza&#263;"
  ]
  node [
    id 1513
    label "appreciate"
  ]
  node [
    id 1514
    label "danie"
  ]
  node [
    id 1515
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 1516
    label "return"
  ]
  node [
    id 1517
    label "refund"
  ]
  node [
    id 1518
    label "liczenie"
  ]
  node [
    id 1519
    label "doch&#243;d"
  ]
  node [
    id 1520
    label "wynagrodzenie_brutto"
  ]
  node [
    id 1521
    label "koszt_rodzajowy"
  ]
  node [
    id 1522
    label "policzy&#263;"
  ]
  node [
    id 1523
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 1524
    label "ordynaria"
  ]
  node [
    id 1525
    label "bud&#380;et_domowy"
  ]
  node [
    id 1526
    label "policzenie"
  ]
  node [
    id 1527
    label "pay"
  ]
  node [
    id 1528
    label "zap&#322;ata"
  ]
  node [
    id 1529
    label "dysleksja"
  ]
  node [
    id 1530
    label "dyscalculia"
  ]
  node [
    id 1531
    label "analfabetyzm_matematyczny"
  ]
  node [
    id 1532
    label "miljon"
  ]
  node [
    id 1533
    label "ba&#324;ka"
  ]
  node [
    id 1534
    label "kategoria"
  ]
  node [
    id 1535
    label "pierwiastek"
  ]
  node [
    id 1536
    label "rozmiar"
  ]
  node [
    id 1537
    label "wyra&#380;enie"
  ]
  node [
    id 1538
    label "number"
  ]
  node [
    id 1539
    label "kwadrat_magiczny"
  ]
  node [
    id 1540
    label "gourd"
  ]
  node [
    id 1541
    label "narz&#281;dzie"
  ]
  node [
    id 1542
    label "kwota"
  ]
  node [
    id 1543
    label "naczynie"
  ]
  node [
    id 1544
    label "obiekt_naturalny"
  ]
  node [
    id 1545
    label "pojemnik"
  ]
  node [
    id 1546
    label "niedostateczny"
  ]
  node [
    id 1547
    label "&#322;eb"
  ]
  node [
    id 1548
    label "mak&#243;wka"
  ]
  node [
    id 1549
    label "bubble"
  ]
  node [
    id 1550
    label "dynia"
  ]
  node [
    id 1551
    label "w_prawo"
  ]
  node [
    id 1552
    label "s&#322;uszny"
  ]
  node [
    id 1553
    label "chwalebny"
  ]
  node [
    id 1554
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 1555
    label "zgodnie_z_prawem"
  ]
  node [
    id 1556
    label "zacny"
  ]
  node [
    id 1557
    label "moralny"
  ]
  node [
    id 1558
    label "prawicowy"
  ]
  node [
    id 1559
    label "na_prawo"
  ]
  node [
    id 1560
    label "cnotliwy"
  ]
  node [
    id 1561
    label "legalny"
  ]
  node [
    id 1562
    label "z_prawa"
  ]
  node [
    id 1563
    label "gajny"
  ]
  node [
    id 1564
    label "legalnie"
  ]
  node [
    id 1565
    label "s&#322;usznie"
  ]
  node [
    id 1566
    label "zasadny"
  ]
  node [
    id 1567
    label "solidny"
  ]
  node [
    id 1568
    label "moralnie"
  ]
  node [
    id 1569
    label "warto&#347;ciowy"
  ]
  node [
    id 1570
    label "etycznie"
  ]
  node [
    id 1571
    label "pochwalny"
  ]
  node [
    id 1572
    label "wspania&#322;y"
  ]
  node [
    id 1573
    label "szlachetny"
  ]
  node [
    id 1574
    label "chwalebnie"
  ]
  node [
    id 1575
    label "zacnie"
  ]
  node [
    id 1576
    label "skromny"
  ]
  node [
    id 1577
    label "niewinny"
  ]
  node [
    id 1578
    label "cny"
  ]
  node [
    id 1579
    label "cnotliwie"
  ]
  node [
    id 1580
    label "prostolinijny"
  ]
  node [
    id 1581
    label "zrozumia&#322;y"
  ]
  node [
    id 1582
    label "immanentny"
  ]
  node [
    id 1583
    label "bezsporny"
  ]
  node [
    id 1584
    label "organicznie"
  ]
  node [
    id 1585
    label "pierwotny"
  ]
  node [
    id 1586
    label "neutralny"
  ]
  node [
    id 1587
    label "normalny"
  ]
  node [
    id 1588
    label "rzeczywisty"
  ]
  node [
    id 1589
    label "naturalnie"
  ]
  node [
    id 1590
    label "prawicowo"
  ]
  node [
    id 1591
    label "prawoskr&#281;tny"
  ]
  node [
    id 1592
    label "konserwatywny"
  ]
  node [
    id 1593
    label "w&#322;asny"
  ]
  node [
    id 1594
    label "oryginalny"
  ]
  node [
    id 1595
    label "autorsko"
  ]
  node [
    id 1596
    label "samodzielny"
  ]
  node [
    id 1597
    label "czyj&#347;"
  ]
  node [
    id 1598
    label "swoisty"
  ]
  node [
    id 1599
    label "osobny"
  ]
  node [
    id 1600
    label "niespotykany"
  ]
  node [
    id 1601
    label "o&#380;ywczy"
  ]
  node [
    id 1602
    label "ekscentryczny"
  ]
  node [
    id 1603
    label "oryginalnie"
  ]
  node [
    id 1604
    label "inny"
  ]
  node [
    id 1605
    label "prawnie"
  ]
  node [
    id 1606
    label "indywidualnie"
  ]
  node [
    id 1607
    label "piwo"
  ]
  node [
    id 1608
    label "warzenie"
  ]
  node [
    id 1609
    label "nawarzy&#263;"
  ]
  node [
    id 1610
    label "nap&#243;j"
  ]
  node [
    id 1611
    label "bacik"
  ]
  node [
    id 1612
    label "wyj&#347;cie"
  ]
  node [
    id 1613
    label "uwarzy&#263;"
  ]
  node [
    id 1614
    label "birofilia"
  ]
  node [
    id 1615
    label "warzy&#263;"
  ]
  node [
    id 1616
    label "uwarzenie"
  ]
  node [
    id 1617
    label "browarnia"
  ]
  node [
    id 1618
    label "nawarzenie"
  ]
  node [
    id 1619
    label "anta&#322;"
  ]
  node [
    id 1620
    label "jedyny"
  ]
  node [
    id 1621
    label "calu&#347;ko"
  ]
  node [
    id 1622
    label "kompletny"
  ]
  node [
    id 1623
    label "&#380;ywy"
  ]
  node [
    id 1624
    label "ca&#322;o"
  ]
  node [
    id 1625
    label "kompletnie"
  ]
  node [
    id 1626
    label "zupe&#322;ny"
  ]
  node [
    id 1627
    label "w_pizdu"
  ]
  node [
    id 1628
    label "przypominanie"
  ]
  node [
    id 1629
    label "podobnie"
  ]
  node [
    id 1630
    label "upodabnianie_si&#281;"
  ]
  node [
    id 1631
    label "upodobnienie"
  ]
  node [
    id 1632
    label "drugi"
  ]
  node [
    id 1633
    label "upodobnienie_si&#281;"
  ]
  node [
    id 1634
    label "zasymilowanie"
  ]
  node [
    id 1635
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1636
    label "ukochany"
  ]
  node [
    id 1637
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1638
    label "najlepszy"
  ]
  node [
    id 1639
    label "optymalnie"
  ]
  node [
    id 1640
    label "zdrowy"
  ]
  node [
    id 1641
    label "szybki"
  ]
  node [
    id 1642
    label "&#380;ywotny"
  ]
  node [
    id 1643
    label "&#380;ywo"
  ]
  node [
    id 1644
    label "o&#380;ywianie"
  ]
  node [
    id 1645
    label "g&#322;&#281;boki"
  ]
  node [
    id 1646
    label "czynny"
  ]
  node [
    id 1647
    label "aktualny"
  ]
  node [
    id 1648
    label "zgrabny"
  ]
  node [
    id 1649
    label "realistyczny"
  ]
  node [
    id 1650
    label "nieograniczony"
  ]
  node [
    id 1651
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 1652
    label "satysfakcja"
  ]
  node [
    id 1653
    label "bezwzgl&#281;dny"
  ]
  node [
    id 1654
    label "wype&#322;nienie"
  ]
  node [
    id 1655
    label "pe&#322;no"
  ]
  node [
    id 1656
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 1657
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1658
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 1659
    label "r&#243;wny"
  ]
  node [
    id 1660
    label "nieuszkodzony"
  ]
  node [
    id 1661
    label "odpowiednio"
  ]
  node [
    id 1662
    label "cywilizacja"
  ]
  node [
    id 1663
    label "elita"
  ]
  node [
    id 1664
    label "aspo&#322;eczny"
  ]
  node [
    id 1665
    label "ludzie_pracy"
  ]
  node [
    id 1666
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 1667
    label "pozaklasowy"
  ]
  node [
    id 1668
    label "uwarstwienie"
  ]
  node [
    id 1669
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 1670
    label "community"
  ]
  node [
    id 1671
    label "klasa"
  ]
  node [
    id 1672
    label "kastowo&#347;&#263;"
  ]
  node [
    id 1673
    label "facylitacja"
  ]
  node [
    id 1674
    label "elite"
  ]
  node [
    id 1675
    label "&#347;rodowisko"
  ]
  node [
    id 1676
    label "wagon"
  ]
  node [
    id 1677
    label "mecz_mistrzowski"
  ]
  node [
    id 1678
    label "arrangement"
  ]
  node [
    id 1679
    label "class"
  ]
  node [
    id 1680
    label "&#322;awka"
  ]
  node [
    id 1681
    label "wykrzyknik"
  ]
  node [
    id 1682
    label "jednostka_systematyczna"
  ]
  node [
    id 1683
    label "programowanie_obiektowe"
  ]
  node [
    id 1684
    label "tablica"
  ]
  node [
    id 1685
    label "warstwa"
  ]
  node [
    id 1686
    label "rezerwa"
  ]
  node [
    id 1687
    label "gromada"
  ]
  node [
    id 1688
    label "Ekwici"
  ]
  node [
    id 1689
    label "sala"
  ]
  node [
    id 1690
    label "pomoc"
  ]
  node [
    id 1691
    label "form"
  ]
  node [
    id 1692
    label "przepisa&#263;"
  ]
  node [
    id 1693
    label "znak_jako&#347;ci"
  ]
  node [
    id 1694
    label "type"
  ]
  node [
    id 1695
    label "promocja"
  ]
  node [
    id 1696
    label "przepisanie"
  ]
  node [
    id 1697
    label "kurs"
  ]
  node [
    id 1698
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1699
    label "dziennik_lekcyjny"
  ]
  node [
    id 1700
    label "typ"
  ]
  node [
    id 1701
    label "fakcja"
  ]
  node [
    id 1702
    label "atak"
  ]
  node [
    id 1703
    label "botanika"
  ]
  node [
    id 1704
    label "uprawienie"
  ]
  node [
    id 1705
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1706
    label "p&#322;osa"
  ]
  node [
    id 1707
    label "ziemia"
  ]
  node [
    id 1708
    label "t&#322;o"
  ]
  node [
    id 1709
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 1710
    label "gospodarstwo"
  ]
  node [
    id 1711
    label "uprawi&#263;"
  ]
  node [
    id 1712
    label "room"
  ]
  node [
    id 1713
    label "dw&#243;r"
  ]
  node [
    id 1714
    label "okazja"
  ]
  node [
    id 1715
    label "irygowanie"
  ]
  node [
    id 1716
    label "compass"
  ]
  node [
    id 1717
    label "square"
  ]
  node [
    id 1718
    label "zmienna"
  ]
  node [
    id 1719
    label "irygowa&#263;"
  ]
  node [
    id 1720
    label "socjologia"
  ]
  node [
    id 1721
    label "boisko"
  ]
  node [
    id 1722
    label "baza_danych"
  ]
  node [
    id 1723
    label "region"
  ]
  node [
    id 1724
    label "zagon"
  ]
  node [
    id 1725
    label "obszar"
  ]
  node [
    id 1726
    label "powierzchnia"
  ]
  node [
    id 1727
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 1728
    label "plane"
  ]
  node [
    id 1729
    label "radlina"
  ]
  node [
    id 1730
    label "Fremeni"
  ]
  node [
    id 1731
    label "niskogatunkowy"
  ]
  node [
    id 1732
    label "condition"
  ]
  node [
    id 1733
    label "awansowa&#263;"
  ]
  node [
    id 1734
    label "awans"
  ]
  node [
    id 1735
    label "podmiotowo"
  ]
  node [
    id 1736
    label "awansowanie"
  ]
  node [
    id 1737
    label "niekorzystny"
  ]
  node [
    id 1738
    label "aspo&#322;ecznie"
  ]
  node [
    id 1739
    label "niech&#281;tny"
  ]
  node [
    id 1740
    label "civilization"
  ]
  node [
    id 1741
    label "faza"
  ]
  node [
    id 1742
    label "technika"
  ]
  node [
    id 1743
    label "rozw&#243;j"
  ]
  node [
    id 1744
    label "cywilizowanie"
  ]
  node [
    id 1745
    label "stratification"
  ]
  node [
    id 1746
    label "lamination"
  ]
  node [
    id 1747
    label "podzia&#322;"
  ]
  node [
    id 1748
    label "Alliance"
  ]
  node [
    id 1749
    label "Ryan"
  ]
  node [
    id 1750
    label "Paula"
  ]
  node [
    id 1751
    label "digital"
  ]
  node [
    id 1752
    label "Milennium"
  ]
  node [
    id 1753
    label "Act"
  ]
  node [
    id 1754
    label "Sonny"
  ]
  node [
    id 1755
    label "bono"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 1748
  ]
  edge [
    source 0
    target 1751
  ]
  edge [
    source 0
    target 1752
  ]
  edge [
    source 0
    target 1753
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 194
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 506
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 218
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 249
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 12
    target 579
  ]
  edge [
    source 12
    target 580
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 198
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 42
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 17
    target 235
  ]
  edge [
    source 17
    target 699
  ]
  edge [
    source 17
    target 700
  ]
  edge [
    source 17
    target 701
  ]
  edge [
    source 17
    target 702
  ]
  edge [
    source 17
    target 703
  ]
  edge [
    source 17
    target 704
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 706
  ]
  edge [
    source 17
    target 707
  ]
  edge [
    source 17
    target 244
  ]
  edge [
    source 17
    target 245
  ]
  edge [
    source 17
    target 246
  ]
  edge [
    source 17
    target 708
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 714
  ]
  edge [
    source 17
    target 248
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 249
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 252
  ]
  edge [
    source 17
    target 255
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 719
  ]
  edge [
    source 17
    target 257
  ]
  edge [
    source 17
    target 270
  ]
  edge [
    source 17
    target 271
  ]
  edge [
    source 17
    target 272
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 274
  ]
  edge [
    source 17
    target 273
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 491
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 721
  ]
  edge [
    source 17
    target 722
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 724
  ]
  edge [
    source 17
    target 725
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 726
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 728
  ]
  edge [
    source 17
    target 729
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 732
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 734
  ]
  edge [
    source 17
    target 735
  ]
  edge [
    source 17
    target 736
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 539
  ]
  edge [
    source 17
    target 738
  ]
  edge [
    source 17
    target 739
  ]
  edge [
    source 17
    target 740
  ]
  edge [
    source 17
    target 741
  ]
  edge [
    source 17
    target 742
  ]
  edge [
    source 17
    target 743
  ]
  edge [
    source 17
    target 744
  ]
  edge [
    source 17
    target 745
  ]
  edge [
    source 17
    target 746
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 416
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 423
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 372
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 432
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 321
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 343
  ]
  edge [
    source 17
    target 334
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 420
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 500
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 426
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 169
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 261
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 508
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 584
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 761
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 876
  ]
  edge [
    source 20
    target 877
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 878
  ]
  edge [
    source 21
    target 879
  ]
  edge [
    source 21
    target 880
  ]
  edge [
    source 21
    target 881
  ]
  edge [
    source 21
    target 416
  ]
  edge [
    source 21
    target 882
  ]
  edge [
    source 21
    target 883
  ]
  edge [
    source 21
    target 884
  ]
  edge [
    source 21
    target 885
  ]
  edge [
    source 21
    target 886
  ]
  edge [
    source 21
    target 887
  ]
  edge [
    source 21
    target 888
  ]
  edge [
    source 21
    target 889
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 891
  ]
  edge [
    source 21
    target 892
  ]
  edge [
    source 21
    target 893
  ]
  edge [
    source 21
    target 894
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 735
  ]
  edge [
    source 23
    target 895
  ]
  edge [
    source 23
    target 896
  ]
  edge [
    source 23
    target 897
  ]
  edge [
    source 23
    target 898
  ]
  edge [
    source 23
    target 136
  ]
  edge [
    source 23
    target 899
  ]
  edge [
    source 23
    target 900
  ]
  edge [
    source 23
    target 106
  ]
  edge [
    source 23
    target 901
  ]
  edge [
    source 23
    target 902
  ]
  edge [
    source 23
    target 903
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 904
  ]
  edge [
    source 23
    target 905
  ]
  edge [
    source 23
    target 423
  ]
  edge [
    source 23
    target 906
  ]
  edge [
    source 23
    target 907
  ]
  edge [
    source 23
    target 908
  ]
  edge [
    source 23
    target 909
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 23
    target 524
  ]
  edge [
    source 23
    target 456
  ]
  edge [
    source 23
    target 851
  ]
  edge [
    source 23
    target 910
  ]
  edge [
    source 23
    target 911
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 912
  ]
  edge [
    source 23
    target 913
  ]
  edge [
    source 23
    target 914
  ]
  edge [
    source 23
    target 839
  ]
  edge [
    source 23
    target 915
  ]
  edge [
    source 23
    target 916
  ]
  edge [
    source 23
    target 917
  ]
  edge [
    source 23
    target 415
  ]
  edge [
    source 23
    target 918
  ]
  edge [
    source 23
    target 919
  ]
  edge [
    source 23
    target 920
  ]
  edge [
    source 23
    target 921
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 922
  ]
  edge [
    source 23
    target 923
  ]
  edge [
    source 23
    target 924
  ]
  edge [
    source 23
    target 151
  ]
  edge [
    source 23
    target 925
  ]
  edge [
    source 23
    target 926
  ]
  edge [
    source 23
    target 927
  ]
  edge [
    source 23
    target 928
  ]
  edge [
    source 23
    target 929
  ]
  edge [
    source 23
    target 523
  ]
  edge [
    source 23
    target 930
  ]
  edge [
    source 23
    target 931
  ]
  edge [
    source 23
    target 932
  ]
  edge [
    source 23
    target 933
  ]
  edge [
    source 23
    target 934
  ]
  edge [
    source 23
    target 935
  ]
  edge [
    source 23
    target 936
  ]
  edge [
    source 23
    target 937
  ]
  edge [
    source 23
    target 163
  ]
  edge [
    source 23
    target 938
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 939
  ]
  edge [
    source 24
    target 940
  ]
  edge [
    source 24
    target 941
  ]
  edge [
    source 24
    target 942
  ]
  edge [
    source 24
    target 943
  ]
  edge [
    source 24
    target 944
  ]
  edge [
    source 24
    target 945
  ]
  edge [
    source 24
    target 946
  ]
  edge [
    source 24
    target 947
  ]
  edge [
    source 24
    target 932
  ]
  edge [
    source 24
    target 948
  ]
  edge [
    source 24
    target 949
  ]
  edge [
    source 24
    target 950
  ]
  edge [
    source 24
    target 951
  ]
  edge [
    source 24
    target 952
  ]
  edge [
    source 24
    target 953
  ]
  edge [
    source 24
    target 806
  ]
  edge [
    source 24
    target 954
  ]
  edge [
    source 24
    target 955
  ]
  edge [
    source 24
    target 956
  ]
  edge [
    source 24
    target 420
  ]
  edge [
    source 24
    target 957
  ]
  edge [
    source 24
    target 958
  ]
  edge [
    source 24
    target 959
  ]
  edge [
    source 24
    target 960
  ]
  edge [
    source 24
    target 961
  ]
  edge [
    source 24
    target 962
  ]
  edge [
    source 24
    target 316
  ]
  edge [
    source 24
    target 963
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 964
  ]
  edge [
    source 24
    target 139
  ]
  edge [
    source 24
    target 965
  ]
  edge [
    source 24
    target 568
  ]
  edge [
    source 24
    target 966
  ]
  edge [
    source 24
    target 967
  ]
  edge [
    source 24
    target 968
  ]
  edge [
    source 24
    target 969
  ]
  edge [
    source 24
    target 970
  ]
  edge [
    source 24
    target 971
  ]
  edge [
    source 24
    target 972
  ]
  edge [
    source 24
    target 250
  ]
  edge [
    source 24
    target 973
  ]
  edge [
    source 24
    target 974
  ]
  edge [
    source 24
    target 975
  ]
  edge [
    source 24
    target 976
  ]
  edge [
    source 24
    target 977
  ]
  edge [
    source 24
    target 978
  ]
  edge [
    source 24
    target 979
  ]
  edge [
    source 24
    target 149
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 980
  ]
  edge [
    source 25
    target 981
  ]
  edge [
    source 25
    target 982
  ]
  edge [
    source 25
    target 983
  ]
  edge [
    source 25
    target 984
  ]
  edge [
    source 25
    target 985
  ]
  edge [
    source 25
    target 667
  ]
  edge [
    source 25
    target 986
  ]
  edge [
    source 25
    target 987
  ]
  edge [
    source 25
    target 988
  ]
  edge [
    source 25
    target 989
  ]
  edge [
    source 25
    target 761
  ]
  edge [
    source 25
    target 990
  ]
  edge [
    source 25
    target 991
  ]
  edge [
    source 25
    target 992
  ]
  edge [
    source 25
    target 993
  ]
  edge [
    source 25
    target 994
  ]
  edge [
    source 25
    target 995
  ]
  edge [
    source 25
    target 996
  ]
  edge [
    source 25
    target 997
  ]
  edge [
    source 25
    target 998
  ]
  edge [
    source 25
    target 999
  ]
  edge [
    source 25
    target 1000
  ]
  edge [
    source 25
    target 1001
  ]
  edge [
    source 25
    target 1002
  ]
  edge [
    source 25
    target 1003
  ]
  edge [
    source 25
    target 1004
  ]
  edge [
    source 25
    target 1005
  ]
  edge [
    source 25
    target 1006
  ]
  edge [
    source 25
    target 1007
  ]
  edge [
    source 25
    target 1008
  ]
  edge [
    source 25
    target 1009
  ]
  edge [
    source 25
    target 1010
  ]
  edge [
    source 25
    target 1011
  ]
  edge [
    source 25
    target 1012
  ]
  edge [
    source 25
    target 1013
  ]
  edge [
    source 25
    target 1014
  ]
  edge [
    source 25
    target 413
  ]
  edge [
    source 25
    target 330
  ]
  edge [
    source 25
    target 1015
  ]
  edge [
    source 25
    target 1016
  ]
  edge [
    source 25
    target 635
  ]
  edge [
    source 25
    target 1017
  ]
  edge [
    source 25
    target 1018
  ]
  edge [
    source 25
    target 1019
  ]
  edge [
    source 25
    target 1020
  ]
  edge [
    source 25
    target 415
  ]
  edge [
    source 25
    target 1021
  ]
  edge [
    source 25
    target 1022
  ]
  edge [
    source 25
    target 1023
  ]
  edge [
    source 25
    target 1024
  ]
  edge [
    source 25
    target 1025
  ]
  edge [
    source 25
    target 1026
  ]
  edge [
    source 25
    target 1027
  ]
  edge [
    source 25
    target 1028
  ]
  edge [
    source 25
    target 1029
  ]
  edge [
    source 25
    target 1030
  ]
  edge [
    source 25
    target 1031
  ]
  edge [
    source 26
    target 1032
  ]
  edge [
    source 26
    target 1033
  ]
  edge [
    source 26
    target 726
  ]
  edge [
    source 26
    target 1034
  ]
  edge [
    source 26
    target 1035
  ]
  edge [
    source 26
    target 1036
  ]
  edge [
    source 26
    target 1037
  ]
  edge [
    source 26
    target 1038
  ]
  edge [
    source 26
    target 1039
  ]
  edge [
    source 26
    target 1040
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1041
  ]
  edge [
    source 27
    target 1042
  ]
  edge [
    source 27
    target 1043
  ]
  edge [
    source 27
    target 1044
  ]
  edge [
    source 27
    target 206
  ]
  edge [
    source 27
    target 1045
  ]
  edge [
    source 27
    target 1046
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1047
  ]
  edge [
    source 28
    target 1048
  ]
  edge [
    source 28
    target 1049
  ]
  edge [
    source 28
    target 1050
  ]
  edge [
    source 28
    target 1051
  ]
  edge [
    source 28
    target 1052
  ]
  edge [
    source 28
    target 1053
  ]
  edge [
    source 28
    target 1054
  ]
  edge [
    source 28
    target 1055
  ]
  edge [
    source 28
    target 1056
  ]
  edge [
    source 28
    target 1057
  ]
  edge [
    source 28
    target 1058
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 28
    target 1059
  ]
  edge [
    source 28
    target 1060
  ]
  edge [
    source 28
    target 1061
  ]
  edge [
    source 28
    target 1062
  ]
  edge [
    source 28
    target 1063
  ]
  edge [
    source 28
    target 1064
  ]
  edge [
    source 28
    target 1065
  ]
  edge [
    source 28
    target 1066
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1067
  ]
  edge [
    source 29
    target 1068
  ]
  edge [
    source 29
    target 1069
  ]
  edge [
    source 29
    target 1070
  ]
  edge [
    source 29
    target 1071
  ]
  edge [
    source 29
    target 1072
  ]
  edge [
    source 29
    target 1073
  ]
  edge [
    source 29
    target 1074
  ]
  edge [
    source 29
    target 1075
  ]
  edge [
    source 29
    target 1076
  ]
  edge [
    source 29
    target 1077
  ]
  edge [
    source 29
    target 1078
  ]
  edge [
    source 29
    target 1079
  ]
  edge [
    source 29
    target 1080
  ]
  edge [
    source 29
    target 1081
  ]
  edge [
    source 29
    target 1082
  ]
  edge [
    source 29
    target 1083
  ]
  edge [
    source 29
    target 1084
  ]
  edge [
    source 29
    target 1085
  ]
  edge [
    source 29
    target 1086
  ]
  edge [
    source 29
    target 1087
  ]
  edge [
    source 29
    target 1088
  ]
  edge [
    source 29
    target 1089
  ]
  edge [
    source 29
    target 1090
  ]
  edge [
    source 29
    target 1091
  ]
  edge [
    source 29
    target 1092
  ]
  edge [
    source 29
    target 1093
  ]
  edge [
    source 29
    target 1094
  ]
  edge [
    source 29
    target 1095
  ]
  edge [
    source 29
    target 1096
  ]
  edge [
    source 29
    target 1097
  ]
  edge [
    source 29
    target 1098
  ]
  edge [
    source 29
    target 1099
  ]
  edge [
    source 29
    target 1100
  ]
  edge [
    source 29
    target 1101
  ]
  edge [
    source 29
    target 1102
  ]
  edge [
    source 29
    target 1103
  ]
  edge [
    source 29
    target 1104
  ]
  edge [
    source 29
    target 334
  ]
  edge [
    source 29
    target 418
  ]
  edge [
    source 29
    target 730
  ]
  edge [
    source 29
    target 1105
  ]
  edge [
    source 29
    target 1106
  ]
  edge [
    source 29
    target 1107
  ]
  edge [
    source 29
    target 1108
  ]
  edge [
    source 29
    target 1109
  ]
  edge [
    source 29
    target 1110
  ]
  edge [
    source 29
    target 1111
  ]
  edge [
    source 29
    target 1112
  ]
  edge [
    source 30
    target 1113
  ]
  edge [
    source 30
    target 647
  ]
  edge [
    source 30
    target 1114
  ]
  edge [
    source 30
    target 648
  ]
  edge [
    source 30
    target 1115
  ]
  edge [
    source 30
    target 1116
  ]
  edge [
    source 30
    target 1117
  ]
  edge [
    source 30
    target 1118
  ]
  edge [
    source 30
    target 426
  ]
  edge [
    source 30
    target 1119
  ]
  edge [
    source 30
    target 1120
  ]
  edge [
    source 30
    target 273
  ]
  edge [
    source 30
    target 415
  ]
  edge [
    source 30
    target 1121
  ]
  edge [
    source 30
    target 1122
  ]
  edge [
    source 30
    target 1123
  ]
  edge [
    source 30
    target 1124
  ]
  edge [
    source 30
    target 550
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 1125
  ]
  edge [
    source 30
    target 1126
  ]
  edge [
    source 30
    target 1127
  ]
  edge [
    source 30
    target 1128
  ]
  edge [
    source 30
    target 1129
  ]
  edge [
    source 30
    target 644
  ]
  edge [
    source 30
    target 1130
  ]
  edge [
    source 30
    target 1131
  ]
  edge [
    source 30
    target 420
  ]
  edge [
    source 30
    target 649
  ]
  edge [
    source 30
    target 650
  ]
  edge [
    source 30
    target 508
  ]
  edge [
    source 30
    target 651
  ]
  edge [
    source 30
    target 652
  ]
  edge [
    source 30
    target 653
  ]
  edge [
    source 30
    target 654
  ]
  edge [
    source 30
    target 1132
  ]
  edge [
    source 30
    target 1133
  ]
  edge [
    source 30
    target 1134
  ]
  edge [
    source 30
    target 456
  ]
  edge [
    source 30
    target 1135
  ]
  edge [
    source 30
    target 1136
  ]
  edge [
    source 30
    target 1137
  ]
  edge [
    source 30
    target 1138
  ]
  edge [
    source 30
    target 1139
  ]
  edge [
    source 30
    target 1140
  ]
  edge [
    source 30
    target 1141
  ]
  edge [
    source 30
    target 635
  ]
  edge [
    source 30
    target 1142
  ]
  edge [
    source 30
    target 1143
  ]
  edge [
    source 30
    target 1144
  ]
  edge [
    source 30
    target 361
  ]
  edge [
    source 30
    target 1145
  ]
  edge [
    source 30
    target 1146
  ]
  edge [
    source 30
    target 1147
  ]
  edge [
    source 30
    target 1148
  ]
  edge [
    source 30
    target 1149
  ]
  edge [
    source 30
    target 626
  ]
  edge [
    source 30
    target 627
  ]
  edge [
    source 30
    target 132
  ]
  edge [
    source 30
    target 630
  ]
  edge [
    source 30
    target 1150
  ]
  edge [
    source 30
    target 632
  ]
  edge [
    source 30
    target 261
  ]
  edge [
    source 30
    target 1151
  ]
  edge [
    source 30
    target 1152
  ]
  edge [
    source 30
    target 637
  ]
  edge [
    source 30
    target 638
  ]
  edge [
    source 30
    target 639
  ]
  edge [
    source 30
    target 925
  ]
  edge [
    source 30
    target 641
  ]
  edge [
    source 30
    target 642
  ]
  edge [
    source 30
    target 95
  ]
  edge [
    source 30
    target 1153
  ]
  edge [
    source 30
    target 645
  ]
  edge [
    source 30
    target 646
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1154
  ]
  edge [
    source 32
    target 1155
  ]
  edge [
    source 32
    target 1156
  ]
  edge [
    source 32
    target 1157
  ]
  edge [
    source 32
    target 1158
  ]
  edge [
    source 32
    target 1159
  ]
  edge [
    source 32
    target 1160
  ]
  edge [
    source 32
    target 1161
  ]
  edge [
    source 32
    target 1162
  ]
  edge [
    source 32
    target 1163
  ]
  edge [
    source 32
    target 1164
  ]
  edge [
    source 32
    target 1165
  ]
  edge [
    source 32
    target 1166
  ]
  edge [
    source 32
    target 1167
  ]
  edge [
    source 32
    target 1168
  ]
  edge [
    source 32
    target 1169
  ]
  edge [
    source 32
    target 1170
  ]
  edge [
    source 32
    target 1171
  ]
  edge [
    source 32
    target 1172
  ]
  edge [
    source 32
    target 1173
  ]
  edge [
    source 32
    target 1174
  ]
  edge [
    source 32
    target 1175
  ]
  edge [
    source 32
    target 1176
  ]
  edge [
    source 32
    target 1177
  ]
  edge [
    source 32
    target 1178
  ]
  edge [
    source 32
    target 1179
  ]
  edge [
    source 32
    target 1180
  ]
  edge [
    source 32
    target 1181
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1182
  ]
  edge [
    source 33
    target 1183
  ]
  edge [
    source 33
    target 413
  ]
  edge [
    source 33
    target 1184
  ]
  edge [
    source 33
    target 1185
  ]
  edge [
    source 33
    target 1186
  ]
  edge [
    source 33
    target 1187
  ]
  edge [
    source 33
    target 1188
  ]
  edge [
    source 33
    target 1189
  ]
  edge [
    source 33
    target 1190
  ]
  edge [
    source 33
    target 1191
  ]
  edge [
    source 33
    target 415
  ]
  edge [
    source 33
    target 455
  ]
  edge [
    source 33
    target 1192
  ]
  edge [
    source 33
    target 1115
  ]
  edge [
    source 33
    target 1193
  ]
  edge [
    source 33
    target 1194
  ]
  edge [
    source 33
    target 1195
  ]
  edge [
    source 33
    target 1196
  ]
  edge [
    source 33
    target 1197
  ]
  edge [
    source 33
    target 1198
  ]
  edge [
    source 33
    target 1199
  ]
  edge [
    source 33
    target 1200
  ]
  edge [
    source 33
    target 1201
  ]
  edge [
    source 33
    target 1202
  ]
  edge [
    source 33
    target 1203
  ]
  edge [
    source 33
    target 1204
  ]
  edge [
    source 33
    target 1205
  ]
  edge [
    source 33
    target 1206
  ]
  edge [
    source 33
    target 1207
  ]
  edge [
    source 33
    target 1208
  ]
  edge [
    source 33
    target 1209
  ]
  edge [
    source 33
    target 1210
  ]
  edge [
    source 33
    target 1211
  ]
  edge [
    source 33
    target 1212
  ]
  edge [
    source 33
    target 1213
  ]
  edge [
    source 33
    target 1214
  ]
  edge [
    source 33
    target 1215
  ]
  edge [
    source 33
    target 1216
  ]
  edge [
    source 33
    target 1217
  ]
  edge [
    source 33
    target 1218
  ]
  edge [
    source 33
    target 1219
  ]
  edge [
    source 33
    target 1220
  ]
  edge [
    source 33
    target 1221
  ]
  edge [
    source 33
    target 1222
  ]
  edge [
    source 33
    target 1223
  ]
  edge [
    source 33
    target 1224
  ]
  edge [
    source 33
    target 1225
  ]
  edge [
    source 33
    target 1226
  ]
  edge [
    source 33
    target 1227
  ]
  edge [
    source 33
    target 1228
  ]
  edge [
    source 33
    target 1229
  ]
  edge [
    source 33
    target 1230
  ]
  edge [
    source 33
    target 1231
  ]
  edge [
    source 33
    target 1232
  ]
  edge [
    source 33
    target 1233
  ]
  edge [
    source 33
    target 1234
  ]
  edge [
    source 33
    target 1235
  ]
  edge [
    source 33
    target 1236
  ]
  edge [
    source 33
    target 1237
  ]
  edge [
    source 33
    target 1238
  ]
  edge [
    source 33
    target 1239
  ]
  edge [
    source 33
    target 1240
  ]
  edge [
    source 33
    target 1241
  ]
  edge [
    source 33
    target 1242
  ]
  edge [
    source 33
    target 1243
  ]
  edge [
    source 33
    target 1244
  ]
  edge [
    source 33
    target 1245
  ]
  edge [
    source 33
    target 1246
  ]
  edge [
    source 33
    target 1247
  ]
  edge [
    source 33
    target 1248
  ]
  edge [
    source 33
    target 1249
  ]
  edge [
    source 33
    target 1250
  ]
  edge [
    source 33
    target 1251
  ]
  edge [
    source 33
    target 1252
  ]
  edge [
    source 33
    target 1253
  ]
  edge [
    source 33
    target 1254
  ]
  edge [
    source 33
    target 1255
  ]
  edge [
    source 33
    target 1256
  ]
  edge [
    source 33
    target 1257
  ]
  edge [
    source 33
    target 1258
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1259
  ]
  edge [
    source 34
    target 1260
  ]
  edge [
    source 34
    target 1261
  ]
  edge [
    source 34
    target 1262
  ]
  edge [
    source 34
    target 1263
  ]
  edge [
    source 34
    target 1264
  ]
  edge [
    source 34
    target 1265
  ]
  edge [
    source 34
    target 92
  ]
  edge [
    source 34
    target 1266
  ]
  edge [
    source 34
    target 1267
  ]
  edge [
    source 34
    target 1268
  ]
  edge [
    source 34
    target 1269
  ]
  edge [
    source 34
    target 1270
  ]
  edge [
    source 34
    target 1271
  ]
  edge [
    source 34
    target 81
  ]
  edge [
    source 34
    target 1272
  ]
  edge [
    source 34
    target 1273
  ]
  edge [
    source 34
    target 1274
  ]
  edge [
    source 34
    target 1275
  ]
  edge [
    source 34
    target 1276
  ]
  edge [
    source 34
    target 1277
  ]
  edge [
    source 34
    target 1278
  ]
  edge [
    source 34
    target 1226
  ]
  edge [
    source 34
    target 1279
  ]
  edge [
    source 34
    target 1280
  ]
  edge [
    source 34
    target 1281
  ]
  edge [
    source 34
    target 76
  ]
  edge [
    source 34
    target 1282
  ]
  edge [
    source 34
    target 1283
  ]
  edge [
    source 34
    target 1284
  ]
  edge [
    source 34
    target 1285
  ]
  edge [
    source 34
    target 1286
  ]
  edge [
    source 34
    target 1287
  ]
  edge [
    source 34
    target 1288
  ]
  edge [
    source 34
    target 1289
  ]
  edge [
    source 34
    target 1290
  ]
  edge [
    source 34
    target 1291
  ]
  edge [
    source 34
    target 1292
  ]
  edge [
    source 34
    target 180
  ]
  edge [
    source 34
    target 53
  ]
  edge [
    source 34
    target 1293
  ]
  edge [
    source 34
    target 1294
  ]
  edge [
    source 34
    target 254
  ]
  edge [
    source 34
    target 1295
  ]
  edge [
    source 34
    target 237
  ]
  edge [
    source 34
    target 1296
  ]
  edge [
    source 34
    target 1297
  ]
  edge [
    source 34
    target 48
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1298
  ]
  edge [
    source 35
    target 1299
  ]
  edge [
    source 35
    target 132
  ]
  edge [
    source 35
    target 447
  ]
  edge [
    source 35
    target 491
  ]
  edge [
    source 35
    target 1300
  ]
  edge [
    source 35
    target 1301
  ]
  edge [
    source 35
    target 1302
  ]
  edge [
    source 35
    target 1303
  ]
  edge [
    source 35
    target 1304
  ]
  edge [
    source 35
    target 1305
  ]
  edge [
    source 35
    target 1306
  ]
  edge [
    source 35
    target 496
  ]
  edge [
    source 35
    target 310
  ]
  edge [
    source 35
    target 492
  ]
  edge [
    source 35
    target 493
  ]
  edge [
    source 35
    target 494
  ]
  edge [
    source 35
    target 495
  ]
  edge [
    source 35
    target 497
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1115
  ]
  edge [
    source 36
    target 1307
  ]
  edge [
    source 36
    target 1308
  ]
  edge [
    source 36
    target 1309
  ]
  edge [
    source 36
    target 1310
  ]
  edge [
    source 36
    target 1311
  ]
  edge [
    source 36
    target 1312
  ]
  edge [
    source 36
    target 362
  ]
  edge [
    source 36
    target 1313
  ]
  edge [
    source 36
    target 1314
  ]
  edge [
    source 36
    target 1315
  ]
  edge [
    source 36
    target 1316
  ]
  edge [
    source 36
    target 1317
  ]
  edge [
    source 36
    target 1120
  ]
  edge [
    source 36
    target 273
  ]
  edge [
    source 36
    target 415
  ]
  edge [
    source 36
    target 1121
  ]
  edge [
    source 36
    target 1122
  ]
  edge [
    source 36
    target 1123
  ]
  edge [
    source 36
    target 1124
  ]
  edge [
    source 36
    target 1318
  ]
  edge [
    source 36
    target 1319
  ]
  edge [
    source 36
    target 1320
  ]
  edge [
    source 36
    target 1321
  ]
  edge [
    source 36
    target 761
  ]
  edge [
    source 36
    target 1322
  ]
  edge [
    source 36
    target 1323
  ]
  edge [
    source 36
    target 1324
  ]
  edge [
    source 36
    target 1325
  ]
  edge [
    source 36
    target 1326
  ]
  edge [
    source 36
    target 1327
  ]
  edge [
    source 36
    target 1328
  ]
  edge [
    source 36
    target 1329
  ]
  edge [
    source 36
    target 1330
  ]
  edge [
    source 36
    target 132
  ]
  edge [
    source 36
    target 1331
  ]
  edge [
    source 36
    target 1332
  ]
  edge [
    source 36
    target 1333
  ]
  edge [
    source 36
    target 1334
  ]
  edge [
    source 36
    target 1129
  ]
  edge [
    source 36
    target 1335
  ]
  edge [
    source 36
    target 1336
  ]
  edge [
    source 36
    target 1337
  ]
  edge [
    source 36
    target 1338
  ]
  edge [
    source 36
    target 1339
  ]
  edge [
    source 36
    target 1132
  ]
  edge [
    source 36
    target 1340
  ]
  edge [
    source 36
    target 1341
  ]
  edge [
    source 36
    target 1342
  ]
  edge [
    source 36
    target 1343
  ]
  edge [
    source 36
    target 918
  ]
  edge [
    source 36
    target 1344
  ]
  edge [
    source 36
    target 1345
  ]
  edge [
    source 36
    target 1346
  ]
  edge [
    source 36
    target 1347
  ]
  edge [
    source 36
    target 1348
  ]
  edge [
    source 36
    target 1349
  ]
  edge [
    source 36
    target 1350
  ]
  edge [
    source 36
    target 1351
  ]
  edge [
    source 36
    target 1352
  ]
  edge [
    source 36
    target 1353
  ]
  edge [
    source 36
    target 1354
  ]
  edge [
    source 36
    target 1355
  ]
  edge [
    source 36
    target 1356
  ]
  edge [
    source 36
    target 1357
  ]
  edge [
    source 36
    target 1358
  ]
  edge [
    source 36
    target 1359
  ]
  edge [
    source 36
    target 1360
  ]
  edge [
    source 36
    target 1361
  ]
  edge [
    source 36
    target 1253
  ]
  edge [
    source 36
    target 1362
  ]
  edge [
    source 36
    target 1363
  ]
  edge [
    source 36
    target 1364
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1365
  ]
  edge [
    source 37
    target 1366
  ]
  edge [
    source 37
    target 1367
  ]
  edge [
    source 37
    target 1368
  ]
  edge [
    source 37
    target 1369
  ]
  edge [
    source 37
    target 1370
  ]
  edge [
    source 37
    target 1371
  ]
  edge [
    source 37
    target 1372
  ]
  edge [
    source 37
    target 1373
  ]
  edge [
    source 37
    target 1374
  ]
  edge [
    source 37
    target 1375
  ]
  edge [
    source 37
    target 1376
  ]
  edge [
    source 37
    target 1377
  ]
  edge [
    source 37
    target 1378
  ]
  edge [
    source 37
    target 1379
  ]
  edge [
    source 37
    target 1380
  ]
  edge [
    source 37
    target 1381
  ]
  edge [
    source 37
    target 1382
  ]
  edge [
    source 37
    target 423
  ]
  edge [
    source 37
    target 456
  ]
  edge [
    source 37
    target 1383
  ]
  edge [
    source 37
    target 1384
  ]
  edge [
    source 37
    target 1385
  ]
  edge [
    source 37
    target 1386
  ]
  edge [
    source 37
    target 438
  ]
  edge [
    source 37
    target 750
  ]
  edge [
    source 37
    target 1387
  ]
  edge [
    source 37
    target 1388
  ]
  edge [
    source 37
    target 1389
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 47
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 48
  ]
  edge [
    source 39
    target 1390
  ]
  edge [
    source 39
    target 1391
  ]
  edge [
    source 39
    target 1392
  ]
  edge [
    source 39
    target 1393
  ]
  edge [
    source 39
    target 650
  ]
  edge [
    source 39
    target 1394
  ]
  edge [
    source 39
    target 1395
  ]
  edge [
    source 39
    target 134
  ]
  edge [
    source 39
    target 1396
  ]
  edge [
    source 39
    target 1397
  ]
  edge [
    source 39
    target 745
  ]
  edge [
    source 39
    target 1398
  ]
  edge [
    source 39
    target 538
  ]
  edge [
    source 39
    target 499
  ]
  edge [
    source 39
    target 1399
  ]
  edge [
    source 39
    target 1400
  ]
  edge [
    source 39
    target 1401
  ]
  edge [
    source 39
    target 1402
  ]
  edge [
    source 39
    target 1403
  ]
  edge [
    source 39
    target 1404
  ]
  edge [
    source 39
    target 1405
  ]
  edge [
    source 39
    target 426
  ]
  edge [
    source 39
    target 1406
  ]
  edge [
    source 39
    target 1407
  ]
  edge [
    source 39
    target 1408
  ]
  edge [
    source 39
    target 1409
  ]
  edge [
    source 39
    target 1410
  ]
  edge [
    source 39
    target 1411
  ]
  edge [
    source 39
    target 1412
  ]
  edge [
    source 39
    target 1413
  ]
  edge [
    source 39
    target 1414
  ]
  edge [
    source 39
    target 1415
  ]
  edge [
    source 39
    target 1416
  ]
  edge [
    source 39
    target 1417
  ]
  edge [
    source 39
    target 1418
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1419
  ]
  edge [
    source 41
    target 1420
  ]
  edge [
    source 41
    target 1421
  ]
  edge [
    source 41
    target 1422
  ]
  edge [
    source 41
    target 1423
  ]
  edge [
    source 41
    target 1424
  ]
  edge [
    source 41
    target 1425
  ]
  edge [
    source 41
    target 1426
  ]
  edge [
    source 41
    target 1427
  ]
  edge [
    source 41
    target 198
  ]
  edge [
    source 41
    target 1428
  ]
  edge [
    source 41
    target 1429
  ]
  edge [
    source 41
    target 1430
  ]
  edge [
    source 41
    target 1431
  ]
  edge [
    source 41
    target 1432
  ]
  edge [
    source 41
    target 1433
  ]
  edge [
    source 41
    target 1434
  ]
  edge [
    source 41
    target 1435
  ]
  edge [
    source 41
    target 1436
  ]
  edge [
    source 41
    target 1437
  ]
  edge [
    source 41
    target 1438
  ]
  edge [
    source 41
    target 1439
  ]
  edge [
    source 41
    target 1440
  ]
  edge [
    source 41
    target 515
  ]
  edge [
    source 41
    target 1441
  ]
  edge [
    source 41
    target 1442
  ]
  edge [
    source 41
    target 1443
  ]
  edge [
    source 41
    target 1444
  ]
  edge [
    source 41
    target 1445
  ]
  edge [
    source 41
    target 1446
  ]
  edge [
    source 41
    target 1447
  ]
  edge [
    source 41
    target 1448
  ]
  edge [
    source 41
    target 1449
  ]
  edge [
    source 41
    target 1450
  ]
  edge [
    source 41
    target 1451
  ]
  edge [
    source 41
    target 1452
  ]
  edge [
    source 41
    target 770
  ]
  edge [
    source 41
    target 1453
  ]
  edge [
    source 41
    target 1454
  ]
  edge [
    source 41
    target 1455
  ]
  edge [
    source 41
    target 1456
  ]
  edge [
    source 41
    target 1457
  ]
  edge [
    source 41
    target 1458
  ]
  edge [
    source 41
    target 1459
  ]
  edge [
    source 41
    target 1460
  ]
  edge [
    source 41
    target 1021
  ]
  edge [
    source 41
    target 1461
  ]
  edge [
    source 41
    target 1462
  ]
  edge [
    source 41
    target 1463
  ]
  edge [
    source 41
    target 1464
  ]
  edge [
    source 41
    target 1465
  ]
  edge [
    source 41
    target 1466
  ]
  edge [
    source 41
    target 1467
  ]
  edge [
    source 41
    target 1468
  ]
  edge [
    source 41
    target 1469
  ]
  edge [
    source 41
    target 1470
  ]
  edge [
    source 41
    target 192
  ]
  edge [
    source 41
    target 1471
  ]
  edge [
    source 41
    target 1472
  ]
  edge [
    source 41
    target 1473
  ]
  edge [
    source 41
    target 1474
  ]
  edge [
    source 41
    target 1475
  ]
  edge [
    source 41
    target 1476
  ]
  edge [
    source 41
    target 1477
  ]
  edge [
    source 41
    target 672
  ]
  edge [
    source 41
    target 1478
  ]
  edge [
    source 41
    target 1479
  ]
  edge [
    source 41
    target 1480
  ]
  edge [
    source 41
    target 1481
  ]
  edge [
    source 41
    target 1482
  ]
  edge [
    source 41
    target 1483
  ]
  edge [
    source 41
    target 1484
  ]
  edge [
    source 41
    target 1485
  ]
  edge [
    source 41
    target 1486
  ]
  edge [
    source 41
    target 1487
  ]
  edge [
    source 41
    target 1488
  ]
  edge [
    source 41
    target 205
  ]
  edge [
    source 41
    target 1489
  ]
  edge [
    source 41
    target 1490
  ]
  edge [
    source 41
    target 1491
  ]
  edge [
    source 41
    target 1492
  ]
  edge [
    source 41
    target 1493
  ]
  edge [
    source 41
    target 1494
  ]
  edge [
    source 41
    target 1495
  ]
  edge [
    source 41
    target 1496
  ]
  edge [
    source 41
    target 1497
  ]
  edge [
    source 41
    target 1498
  ]
  edge [
    source 41
    target 1499
  ]
  edge [
    source 41
    target 1500
  ]
  edge [
    source 41
    target 614
  ]
  edge [
    source 41
    target 1501
  ]
  edge [
    source 41
    target 1502
  ]
  edge [
    source 41
    target 1503
  ]
  edge [
    source 41
    target 606
  ]
  edge [
    source 41
    target 1504
  ]
  edge [
    source 41
    target 1505
  ]
  edge [
    source 41
    target 1506
  ]
  edge [
    source 41
    target 1507
  ]
  edge [
    source 41
    target 1060
  ]
  edge [
    source 41
    target 1508
  ]
  edge [
    source 41
    target 1509
  ]
  edge [
    source 41
    target 206
  ]
  edge [
    source 41
    target 1510
  ]
  edge [
    source 41
    target 1181
  ]
  edge [
    source 41
    target 1511
  ]
  edge [
    source 41
    target 1512
  ]
  edge [
    source 41
    target 1513
  ]
  edge [
    source 41
    target 1514
  ]
  edge [
    source 41
    target 1515
  ]
  edge [
    source 41
    target 1516
  ]
  edge [
    source 41
    target 1517
  ]
  edge [
    source 41
    target 1518
  ]
  edge [
    source 41
    target 1519
  ]
  edge [
    source 41
    target 1520
  ]
  edge [
    source 41
    target 1521
  ]
  edge [
    source 41
    target 1522
  ]
  edge [
    source 41
    target 1523
  ]
  edge [
    source 41
    target 1524
  ]
  edge [
    source 41
    target 1525
  ]
  edge [
    source 41
    target 1526
  ]
  edge [
    source 41
    target 1527
  ]
  edge [
    source 41
    target 1528
  ]
  edge [
    source 41
    target 1529
  ]
  edge [
    source 41
    target 1530
  ]
  edge [
    source 41
    target 1531
  ]
  edge [
    source 42
    target 1532
  ]
  edge [
    source 42
    target 1533
  ]
  edge [
    source 42
    target 745
  ]
  edge [
    source 42
    target 1534
  ]
  edge [
    source 42
    target 1535
  ]
  edge [
    source 42
    target 1536
  ]
  edge [
    source 42
    target 1537
  ]
  edge [
    source 42
    target 1129
  ]
  edge [
    source 42
    target 1538
  ]
  edge [
    source 42
    target 132
  ]
  edge [
    source 42
    target 707
  ]
  edge [
    source 42
    target 150
  ]
  edge [
    source 42
    target 1539
  ]
  edge [
    source 42
    target 714
  ]
  edge [
    source 42
    target 1540
  ]
  edge [
    source 42
    target 1541
  ]
  edge [
    source 42
    target 1542
  ]
  edge [
    source 42
    target 1543
  ]
  edge [
    source 42
    target 1544
  ]
  edge [
    source 42
    target 1545
  ]
  edge [
    source 42
    target 1546
  ]
  edge [
    source 42
    target 1547
  ]
  edge [
    source 42
    target 1548
  ]
  edge [
    source 42
    target 1549
  ]
  edge [
    source 42
    target 790
  ]
  edge [
    source 42
    target 1550
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1551
  ]
  edge [
    source 44
    target 1552
  ]
  edge [
    source 44
    target 1269
  ]
  edge [
    source 44
    target 1553
  ]
  edge [
    source 44
    target 1554
  ]
  edge [
    source 44
    target 1555
  ]
  edge [
    source 44
    target 1556
  ]
  edge [
    source 44
    target 1557
  ]
  edge [
    source 44
    target 1558
  ]
  edge [
    source 44
    target 1559
  ]
  edge [
    source 44
    target 1560
  ]
  edge [
    source 44
    target 1561
  ]
  edge [
    source 44
    target 1562
  ]
  edge [
    source 44
    target 1563
  ]
  edge [
    source 44
    target 1564
  ]
  edge [
    source 44
    target 1565
  ]
  edge [
    source 44
    target 53
  ]
  edge [
    source 44
    target 1566
  ]
  edge [
    source 44
    target 88
  ]
  edge [
    source 44
    target 92
  ]
  edge [
    source 44
    target 1567
  ]
  edge [
    source 44
    target 1568
  ]
  edge [
    source 44
    target 1569
  ]
  edge [
    source 44
    target 1570
  ]
  edge [
    source 44
    target 76
  ]
  edge [
    source 44
    target 1571
  ]
  edge [
    source 44
    target 1572
  ]
  edge [
    source 44
    target 1573
  ]
  edge [
    source 44
    target 1252
  ]
  edge [
    source 44
    target 1574
  ]
  edge [
    source 44
    target 1575
  ]
  edge [
    source 44
    target 1576
  ]
  edge [
    source 44
    target 1577
  ]
  edge [
    source 44
    target 1578
  ]
  edge [
    source 44
    target 1579
  ]
  edge [
    source 44
    target 1580
  ]
  edge [
    source 44
    target 1268
  ]
  edge [
    source 44
    target 1581
  ]
  edge [
    source 44
    target 1582
  ]
  edge [
    source 44
    target 82
  ]
  edge [
    source 44
    target 1583
  ]
  edge [
    source 44
    target 1584
  ]
  edge [
    source 44
    target 1585
  ]
  edge [
    source 44
    target 1586
  ]
  edge [
    source 44
    target 1587
  ]
  edge [
    source 44
    target 1588
  ]
  edge [
    source 44
    target 1589
  ]
  edge [
    source 44
    target 1590
  ]
  edge [
    source 44
    target 1591
  ]
  edge [
    source 44
    target 1592
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1593
  ]
  edge [
    source 45
    target 1594
  ]
  edge [
    source 45
    target 1595
  ]
  edge [
    source 45
    target 1596
  ]
  edge [
    source 45
    target 1023
  ]
  edge [
    source 45
    target 1597
  ]
  edge [
    source 45
    target 1598
  ]
  edge [
    source 45
    target 1599
  ]
  edge [
    source 45
    target 1600
  ]
  edge [
    source 45
    target 1601
  ]
  edge [
    source 45
    target 1602
  ]
  edge [
    source 45
    target 69
  ]
  edge [
    source 45
    target 1603
  ]
  edge [
    source 45
    target 1604
  ]
  edge [
    source 45
    target 1585
  ]
  edge [
    source 45
    target 92
  ]
  edge [
    source 45
    target 1569
  ]
  edge [
    source 45
    target 1605
  ]
  edge [
    source 45
    target 1606
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1607
  ]
  edge [
    source 46
    target 1608
  ]
  edge [
    source 46
    target 1609
  ]
  edge [
    source 46
    target 769
  ]
  edge [
    source 46
    target 1610
  ]
  edge [
    source 46
    target 1611
  ]
  edge [
    source 46
    target 1612
  ]
  edge [
    source 46
    target 1613
  ]
  edge [
    source 46
    target 1614
  ]
  edge [
    source 46
    target 1615
  ]
  edge [
    source 46
    target 1616
  ]
  edge [
    source 46
    target 1617
  ]
  edge [
    source 46
    target 1618
  ]
  edge [
    source 46
    target 1619
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1620
  ]
  edge [
    source 48
    target 1285
  ]
  edge [
    source 48
    target 1621
  ]
  edge [
    source 48
    target 1622
  ]
  edge [
    source 48
    target 1623
  ]
  edge [
    source 48
    target 1401
  ]
  edge [
    source 48
    target 81
  ]
  edge [
    source 48
    target 1624
  ]
  edge [
    source 48
    target 1625
  ]
  edge [
    source 48
    target 1626
  ]
  edge [
    source 48
    target 1627
  ]
  edge [
    source 48
    target 1628
  ]
  edge [
    source 48
    target 1629
  ]
  edge [
    source 48
    target 1630
  ]
  edge [
    source 48
    target 236
  ]
  edge [
    source 48
    target 1631
  ]
  edge [
    source 48
    target 1632
  ]
  edge [
    source 48
    target 64
  ]
  edge [
    source 48
    target 1633
  ]
  edge [
    source 48
    target 1634
  ]
  edge [
    source 48
    target 1635
  ]
  edge [
    source 48
    target 1636
  ]
  edge [
    source 48
    target 1637
  ]
  edge [
    source 48
    target 1638
  ]
  edge [
    source 48
    target 1639
  ]
  edge [
    source 48
    target 1259
  ]
  edge [
    source 48
    target 1260
  ]
  edge [
    source 48
    target 1261
  ]
  edge [
    source 48
    target 1262
  ]
  edge [
    source 48
    target 1263
  ]
  edge [
    source 48
    target 1264
  ]
  edge [
    source 48
    target 1265
  ]
  edge [
    source 48
    target 92
  ]
  edge [
    source 48
    target 1266
  ]
  edge [
    source 48
    target 1640
  ]
  edge [
    source 48
    target 229
  ]
  edge [
    source 48
    target 1641
  ]
  edge [
    source 48
    target 1642
  ]
  edge [
    source 48
    target 1269
  ]
  edge [
    source 48
    target 1643
  ]
  edge [
    source 48
    target 180
  ]
  edge [
    source 48
    target 1644
  ]
  edge [
    source 48
    target 771
  ]
  edge [
    source 48
    target 1226
  ]
  edge [
    source 48
    target 1645
  ]
  edge [
    source 48
    target 1228
  ]
  edge [
    source 48
    target 1646
  ]
  edge [
    source 48
    target 1647
  ]
  edge [
    source 48
    target 1648
  ]
  edge [
    source 48
    target 1649
  ]
  edge [
    source 48
    target 1231
  ]
  edge [
    source 48
    target 1650
  ]
  edge [
    source 48
    target 1651
  ]
  edge [
    source 48
    target 1652
  ]
  edge [
    source 48
    target 1653
  ]
  edge [
    source 48
    target 71
  ]
  edge [
    source 48
    target 1654
  ]
  edge [
    source 48
    target 134
  ]
  edge [
    source 48
    target 1655
  ]
  edge [
    source 48
    target 1656
  ]
  edge [
    source 48
    target 1657
  ]
  edge [
    source 48
    target 1658
  ]
  edge [
    source 48
    target 1659
  ]
  edge [
    source 48
    target 1660
  ]
  edge [
    source 48
    target 1661
  ]
  edge [
    source 49
    target 1662
  ]
  edge [
    source 49
    target 390
  ]
  edge [
    source 49
    target 1663
  ]
  edge [
    source 49
    target 276
  ]
  edge [
    source 49
    target 851
  ]
  edge [
    source 49
    target 1664
  ]
  edge [
    source 49
    target 1665
  ]
  edge [
    source 49
    target 1666
  ]
  edge [
    source 49
    target 1667
  ]
  edge [
    source 49
    target 1668
  ]
  edge [
    source 49
    target 1669
  ]
  edge [
    source 49
    target 1670
  ]
  edge [
    source 49
    target 1671
  ]
  edge [
    source 49
    target 1672
  ]
  edge [
    source 49
    target 1673
  ]
  edge [
    source 49
    target 139
  ]
  edge [
    source 49
    target 1674
  ]
  edge [
    source 49
    target 1675
  ]
  edge [
    source 49
    target 1676
  ]
  edge [
    source 49
    target 1677
  ]
  edge [
    source 49
    target 420
  ]
  edge [
    source 49
    target 1678
  ]
  edge [
    source 49
    target 1679
  ]
  edge [
    source 49
    target 1680
  ]
  edge [
    source 49
    target 1681
  ]
  edge [
    source 49
    target 1301
  ]
  edge [
    source 49
    target 1682
  ]
  edge [
    source 49
    target 1683
  ]
  edge [
    source 49
    target 1684
  ]
  edge [
    source 49
    target 1685
  ]
  edge [
    source 49
    target 1686
  ]
  edge [
    source 49
    target 1687
  ]
  edge [
    source 49
    target 1688
  ]
  edge [
    source 49
    target 284
  ]
  edge [
    source 49
    target 1689
  ]
  edge [
    source 49
    target 1690
  ]
  edge [
    source 49
    target 1691
  ]
  edge [
    source 49
    target 150
  ]
  edge [
    source 49
    target 1692
  ]
  edge [
    source 49
    target 434
  ]
  edge [
    source 49
    target 1693
  ]
  edge [
    source 49
    target 1328
  ]
  edge [
    source 49
    target 1694
  ]
  edge [
    source 49
    target 1695
  ]
  edge [
    source 49
    target 1696
  ]
  edge [
    source 49
    target 1697
  ]
  edge [
    source 49
    target 651
  ]
  edge [
    source 49
    target 1698
  ]
  edge [
    source 49
    target 1699
  ]
  edge [
    source 49
    target 1700
  ]
  edge [
    source 49
    target 1701
  ]
  edge [
    source 49
    target 1307
  ]
  edge [
    source 49
    target 1702
  ]
  edge [
    source 49
    target 1703
  ]
  edge [
    source 49
    target 1704
  ]
  edge [
    source 49
    target 1705
  ]
  edge [
    source 49
    target 1706
  ]
  edge [
    source 49
    target 1707
  ]
  edge [
    source 49
    target 132
  ]
  edge [
    source 49
    target 1708
  ]
  edge [
    source 49
    target 1709
  ]
  edge [
    source 49
    target 1710
  ]
  edge [
    source 49
    target 1711
  ]
  edge [
    source 49
    target 1712
  ]
  edge [
    source 49
    target 1713
  ]
  edge [
    source 49
    target 1714
  ]
  edge [
    source 49
    target 1536
  ]
  edge [
    source 49
    target 1715
  ]
  edge [
    source 49
    target 1716
  ]
  edge [
    source 49
    target 1717
  ]
  edge [
    source 49
    target 1718
  ]
  edge [
    source 49
    target 1719
  ]
  edge [
    source 49
    target 1720
  ]
  edge [
    source 49
    target 1721
  ]
  edge [
    source 49
    target 816
  ]
  edge [
    source 49
    target 1722
  ]
  edge [
    source 49
    target 1723
  ]
  edge [
    source 49
    target 579
  ]
  edge [
    source 49
    target 1724
  ]
  edge [
    source 49
    target 1725
  ]
  edge [
    source 49
    target 130
  ]
  edge [
    source 49
    target 1726
  ]
  edge [
    source 49
    target 1727
  ]
  edge [
    source 49
    target 1728
  ]
  edge [
    source 49
    target 1729
  ]
  edge [
    source 49
    target 1730
  ]
  edge [
    source 49
    target 1731
  ]
  edge [
    source 49
    target 1732
  ]
  edge [
    source 49
    target 1733
  ]
  edge [
    source 49
    target 720
  ]
  edge [
    source 49
    target 1405
  ]
  edge [
    source 49
    target 1734
  ]
  edge [
    source 49
    target 1735
  ]
  edge [
    source 49
    target 1736
  ]
  edge [
    source 49
    target 1331
  ]
  edge [
    source 49
    target 1737
  ]
  edge [
    source 49
    target 1738
  ]
  edge [
    source 49
    target 55
  ]
  edge [
    source 49
    target 1739
  ]
  edge [
    source 49
    target 417
  ]
  edge [
    source 49
    target 419
  ]
  edge [
    source 49
    target 422
  ]
  edge [
    source 49
    target 423
  ]
  edge [
    source 49
    target 426
  ]
  edge [
    source 49
    target 425
  ]
  edge [
    source 49
    target 428
  ]
  edge [
    source 49
    target 1740
  ]
  edge [
    source 49
    target 194
  ]
  edge [
    source 49
    target 1741
  ]
  edge [
    source 49
    target 1742
  ]
  edge [
    source 49
    target 435
  ]
  edge [
    source 49
    target 1743
  ]
  edge [
    source 49
    target 437
  ]
  edge [
    source 49
    target 441
  ]
  edge [
    source 49
    target 442
  ]
  edge [
    source 49
    target 443
  ]
  edge [
    source 49
    target 1744
  ]
  edge [
    source 49
    target 444
  ]
  edge [
    source 49
    target 445
  ]
  edge [
    source 49
    target 446
  ]
  edge [
    source 49
    target 102
  ]
  edge [
    source 49
    target 1745
  ]
  edge [
    source 49
    target 1746
  ]
  edge [
    source 49
    target 1747
  ]
  edge [
    source 1749
    target 1750
  ]
  edge [
    source 1751
    target 1752
  ]
  edge [
    source 1751
    target 1753
  ]
  edge [
    source 1752
    target 1753
  ]
  edge [
    source 1753
    target 1754
  ]
  edge [
    source 1753
    target 1755
  ]
  edge [
    source 1754
    target 1755
  ]
]
