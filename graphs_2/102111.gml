graph [
  node [
    id 0
    label "zmiana"
    origin "text"
  ]
  node [
    id 1
    label "prze&#322;o&#380;enie"
    origin "text"
  ]
  node [
    id 2
    label "rewizja"
  ]
  node [
    id 3
    label "passage"
  ]
  node [
    id 4
    label "oznaka"
  ]
  node [
    id 5
    label "change"
  ]
  node [
    id 6
    label "ferment"
  ]
  node [
    id 7
    label "komplet"
  ]
  node [
    id 8
    label "anatomopatolog"
  ]
  node [
    id 9
    label "zmianka"
  ]
  node [
    id 10
    label "czas"
  ]
  node [
    id 11
    label "zjawisko"
  ]
  node [
    id 12
    label "amendment"
  ]
  node [
    id 13
    label "praca"
  ]
  node [
    id 14
    label "odmienianie"
  ]
  node [
    id 15
    label "tura"
  ]
  node [
    id 16
    label "proces"
  ]
  node [
    id 17
    label "boski"
  ]
  node [
    id 18
    label "krajobraz"
  ]
  node [
    id 19
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 20
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 21
    label "przywidzenie"
  ]
  node [
    id 22
    label "presence"
  ]
  node [
    id 23
    label "charakter"
  ]
  node [
    id 24
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 25
    label "lekcja"
  ]
  node [
    id 26
    label "ensemble"
  ]
  node [
    id 27
    label "grupa"
  ]
  node [
    id 28
    label "klasa"
  ]
  node [
    id 29
    label "zestaw"
  ]
  node [
    id 30
    label "poprzedzanie"
  ]
  node [
    id 31
    label "czasoprzestrze&#324;"
  ]
  node [
    id 32
    label "laba"
  ]
  node [
    id 33
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 34
    label "chronometria"
  ]
  node [
    id 35
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 36
    label "rachuba_czasu"
  ]
  node [
    id 37
    label "przep&#322;ywanie"
  ]
  node [
    id 38
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 39
    label "czasokres"
  ]
  node [
    id 40
    label "odczyt"
  ]
  node [
    id 41
    label "chwila"
  ]
  node [
    id 42
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 43
    label "dzieje"
  ]
  node [
    id 44
    label "kategoria_gramatyczna"
  ]
  node [
    id 45
    label "poprzedzenie"
  ]
  node [
    id 46
    label "trawienie"
  ]
  node [
    id 47
    label "pochodzi&#263;"
  ]
  node [
    id 48
    label "period"
  ]
  node [
    id 49
    label "okres_czasu"
  ]
  node [
    id 50
    label "poprzedza&#263;"
  ]
  node [
    id 51
    label "schy&#322;ek"
  ]
  node [
    id 52
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 53
    label "odwlekanie_si&#281;"
  ]
  node [
    id 54
    label "zegar"
  ]
  node [
    id 55
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 56
    label "czwarty_wymiar"
  ]
  node [
    id 57
    label "pochodzenie"
  ]
  node [
    id 58
    label "koniugacja"
  ]
  node [
    id 59
    label "Zeitgeist"
  ]
  node [
    id 60
    label "trawi&#263;"
  ]
  node [
    id 61
    label "pogoda"
  ]
  node [
    id 62
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 63
    label "poprzedzi&#263;"
  ]
  node [
    id 64
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 65
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 66
    label "time_period"
  ]
  node [
    id 67
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 68
    label "implikowa&#263;"
  ]
  node [
    id 69
    label "signal"
  ]
  node [
    id 70
    label "fakt"
  ]
  node [
    id 71
    label "symbol"
  ]
  node [
    id 72
    label "proces_my&#347;lowy"
  ]
  node [
    id 73
    label "dow&#243;d"
  ]
  node [
    id 74
    label "krytyka"
  ]
  node [
    id 75
    label "rekurs"
  ]
  node [
    id 76
    label "checkup"
  ]
  node [
    id 77
    label "kontrola"
  ]
  node [
    id 78
    label "odwo&#322;anie"
  ]
  node [
    id 79
    label "correction"
  ]
  node [
    id 80
    label "przegl&#261;d"
  ]
  node [
    id 81
    label "kipisz"
  ]
  node [
    id 82
    label "korekta"
  ]
  node [
    id 83
    label "bia&#322;ko"
  ]
  node [
    id 84
    label "immobilizowa&#263;"
  ]
  node [
    id 85
    label "poruszenie"
  ]
  node [
    id 86
    label "immobilizacja"
  ]
  node [
    id 87
    label "apoenzym"
  ]
  node [
    id 88
    label "zymaza"
  ]
  node [
    id 89
    label "enzyme"
  ]
  node [
    id 90
    label "immobilizowanie"
  ]
  node [
    id 91
    label "biokatalizator"
  ]
  node [
    id 92
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 93
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 94
    label "najem"
  ]
  node [
    id 95
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 96
    label "zak&#322;ad"
  ]
  node [
    id 97
    label "stosunek_pracy"
  ]
  node [
    id 98
    label "benedykty&#324;ski"
  ]
  node [
    id 99
    label "poda&#380;_pracy"
  ]
  node [
    id 100
    label "pracowanie"
  ]
  node [
    id 101
    label "tyrka"
  ]
  node [
    id 102
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 103
    label "wytw&#243;r"
  ]
  node [
    id 104
    label "miejsce"
  ]
  node [
    id 105
    label "zaw&#243;d"
  ]
  node [
    id 106
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 107
    label "tynkarski"
  ]
  node [
    id 108
    label "pracowa&#263;"
  ]
  node [
    id 109
    label "czynno&#347;&#263;"
  ]
  node [
    id 110
    label "czynnik_produkcji"
  ]
  node [
    id 111
    label "zobowi&#261;zanie"
  ]
  node [
    id 112
    label "kierownictwo"
  ]
  node [
    id 113
    label "siedziba"
  ]
  node [
    id 114
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 115
    label "patolog"
  ]
  node [
    id 116
    label "anatom"
  ]
  node [
    id 117
    label "sparafrazowanie"
  ]
  node [
    id 118
    label "zmienianie"
  ]
  node [
    id 119
    label "parafrazowanie"
  ]
  node [
    id 120
    label "zamiana"
  ]
  node [
    id 121
    label "wymienianie"
  ]
  node [
    id 122
    label "Transfiguration"
  ]
  node [
    id 123
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 124
    label "po&#322;o&#380;enie"
  ]
  node [
    id 125
    label "move"
  ]
  node [
    id 126
    label "rendition"
  ]
  node [
    id 127
    label "prym"
  ]
  node [
    id 128
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 129
    label "uznanie"
  ]
  node [
    id 130
    label "marriage"
  ]
  node [
    id 131
    label "przeniesienie"
  ]
  node [
    id 132
    label "ratio"
  ]
  node [
    id 133
    label "proporcja"
  ]
  node [
    id 134
    label "przemieszczenie"
  ]
  node [
    id 135
    label "wyra&#380;enie"
  ]
  node [
    id 136
    label "zekranizowanie"
  ]
  node [
    id 137
    label "marketing_afiliacyjny"
  ]
  node [
    id 138
    label "transfer"
  ]
  node [
    id 139
    label "przedstawienie"
  ]
  node [
    id 140
    label "w&#322;o&#380;enie"
  ]
  node [
    id 141
    label "zmienienie"
  ]
  node [
    id 142
    label "j&#281;zyk"
  ]
  node [
    id 143
    label "zrobienie"
  ]
  node [
    id 144
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 145
    label "artykulator"
  ]
  node [
    id 146
    label "kod"
  ]
  node [
    id 147
    label "kawa&#322;ek"
  ]
  node [
    id 148
    label "przedmiot"
  ]
  node [
    id 149
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 150
    label "gramatyka"
  ]
  node [
    id 151
    label "stylik"
  ]
  node [
    id 152
    label "przet&#322;umaczenie"
  ]
  node [
    id 153
    label "formalizowanie"
  ]
  node [
    id 154
    label "ssa&#263;"
  ]
  node [
    id 155
    label "ssanie"
  ]
  node [
    id 156
    label "language"
  ]
  node [
    id 157
    label "liza&#263;"
  ]
  node [
    id 158
    label "napisa&#263;"
  ]
  node [
    id 159
    label "konsonantyzm"
  ]
  node [
    id 160
    label "wokalizm"
  ]
  node [
    id 161
    label "pisa&#263;"
  ]
  node [
    id 162
    label "fonetyka"
  ]
  node [
    id 163
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 164
    label "jeniec"
  ]
  node [
    id 165
    label "but"
  ]
  node [
    id 166
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 167
    label "po_koroniarsku"
  ]
  node [
    id 168
    label "kultura_duchowa"
  ]
  node [
    id 169
    label "t&#322;umaczenie"
  ]
  node [
    id 170
    label "m&#243;wienie"
  ]
  node [
    id 171
    label "pype&#263;"
  ]
  node [
    id 172
    label "lizanie"
  ]
  node [
    id 173
    label "pismo"
  ]
  node [
    id 174
    label "formalizowa&#263;"
  ]
  node [
    id 175
    label "rozumie&#263;"
  ]
  node [
    id 176
    label "organ"
  ]
  node [
    id 177
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 178
    label "rozumienie"
  ]
  node [
    id 179
    label "spos&#243;b"
  ]
  node [
    id 180
    label "makroglosja"
  ]
  node [
    id 181
    label "m&#243;wi&#263;"
  ]
  node [
    id 182
    label "jama_ustna"
  ]
  node [
    id 183
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 184
    label "formacja_geologiczna"
  ]
  node [
    id 185
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 186
    label "natural_language"
  ]
  node [
    id 187
    label "s&#322;ownictwo"
  ]
  node [
    id 188
    label "urz&#261;dzenie"
  ]
  node [
    id 189
    label "dominacja"
  ]
  node [
    id 190
    label "przek&#322;adanie"
  ]
  node [
    id 191
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 192
    label "predilection"
  ]
  node [
    id 193
    label "przek&#322;ada&#263;"
  ]
  node [
    id 194
    label "preponderencja"
  ]
  node [
    id 195
    label "supremacja"
  ]
  node [
    id 196
    label "ilo&#347;&#263;"
  ]
  node [
    id 197
    label "przekaz"
  ]
  node [
    id 198
    label "release"
  ]
  node [
    id 199
    label "lista_transferowa"
  ]
  node [
    id 200
    label "narobienie"
  ]
  node [
    id 201
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 202
    label "creation"
  ]
  node [
    id 203
    label "porobienie"
  ]
  node [
    id 204
    label "activity"
  ]
  node [
    id 205
    label "bezproblemowy"
  ]
  node [
    id 206
    label "wydarzenie"
  ]
  node [
    id 207
    label "wyraz_skrajny"
  ]
  node [
    id 208
    label "porz&#261;dek"
  ]
  node [
    id 209
    label "stosunek"
  ]
  node [
    id 210
    label "relationship"
  ]
  node [
    id 211
    label "cecha"
  ]
  node [
    id 212
    label "iloraz"
  ]
  node [
    id 213
    label "obleczenie_si&#281;"
  ]
  node [
    id 214
    label "poubieranie"
  ]
  node [
    id 215
    label "przekazanie"
  ]
  node [
    id 216
    label "przyodzianie"
  ]
  node [
    id 217
    label "str&#243;j"
  ]
  node [
    id 218
    label "powk&#322;adanie"
  ]
  node [
    id 219
    label "infliction"
  ]
  node [
    id 220
    label "obleczenie"
  ]
  node [
    id 221
    label "przebranie"
  ]
  node [
    id 222
    label "umieszczenie"
  ]
  node [
    id 223
    label "przywdzianie"
  ]
  node [
    id 224
    label "deposit"
  ]
  node [
    id 225
    label "rozebranie"
  ]
  node [
    id 226
    label "przymierzenie"
  ]
  node [
    id 227
    label "variation"
  ]
  node [
    id 228
    label "exchange"
  ]
  node [
    id 229
    label "zape&#322;nienie"
  ]
  node [
    id 230
    label "przemeblowanie"
  ]
  node [
    id 231
    label "spowodowanie"
  ]
  node [
    id 232
    label "zrobienie_si&#281;"
  ]
  node [
    id 233
    label "przekwalifikowanie"
  ]
  node [
    id 234
    label "substytuowanie"
  ]
  node [
    id 235
    label "pr&#243;bowanie"
  ]
  node [
    id 236
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 237
    label "zademonstrowanie"
  ]
  node [
    id 238
    label "report"
  ]
  node [
    id 239
    label "obgadanie"
  ]
  node [
    id 240
    label "realizacja"
  ]
  node [
    id 241
    label "scena"
  ]
  node [
    id 242
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 243
    label "narration"
  ]
  node [
    id 244
    label "cyrk"
  ]
  node [
    id 245
    label "posta&#263;"
  ]
  node [
    id 246
    label "theatrical_performance"
  ]
  node [
    id 247
    label "opisanie"
  ]
  node [
    id 248
    label "malarstwo"
  ]
  node [
    id 249
    label "scenografia"
  ]
  node [
    id 250
    label "teatr"
  ]
  node [
    id 251
    label "ukazanie"
  ]
  node [
    id 252
    label "zapoznanie"
  ]
  node [
    id 253
    label "pokaz"
  ]
  node [
    id 254
    label "podanie"
  ]
  node [
    id 255
    label "ods&#322;ona"
  ]
  node [
    id 256
    label "exhibit"
  ]
  node [
    id 257
    label "pokazanie"
  ]
  node [
    id 258
    label "wyst&#261;pienie"
  ]
  node [
    id 259
    label "przedstawi&#263;"
  ]
  node [
    id 260
    label "przedstawianie"
  ]
  node [
    id 261
    label "przedstawia&#263;"
  ]
  node [
    id 262
    label "rola"
  ]
  node [
    id 263
    label "leksem"
  ]
  node [
    id 264
    label "sformu&#322;owanie"
  ]
  node [
    id 265
    label "zdarzenie_si&#281;"
  ]
  node [
    id 266
    label "poj&#281;cie"
  ]
  node [
    id 267
    label "poinformowanie"
  ]
  node [
    id 268
    label "wording"
  ]
  node [
    id 269
    label "kompozycja"
  ]
  node [
    id 270
    label "oznaczenie"
  ]
  node [
    id 271
    label "znak_j&#281;zykowy"
  ]
  node [
    id 272
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 273
    label "ozdobnik"
  ]
  node [
    id 274
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 275
    label "grupa_imienna"
  ]
  node [
    id 276
    label "jednostka_leksykalna"
  ]
  node [
    id 277
    label "term"
  ]
  node [
    id 278
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 279
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 280
    label "ujawnienie"
  ]
  node [
    id 281
    label "affirmation"
  ]
  node [
    id 282
    label "zapisanie"
  ]
  node [
    id 283
    label "rzucenie"
  ]
  node [
    id 284
    label "zaimponowanie"
  ]
  node [
    id 285
    label "honorowanie"
  ]
  node [
    id 286
    label "uszanowanie"
  ]
  node [
    id 287
    label "uhonorowa&#263;"
  ]
  node [
    id 288
    label "oznajmienie"
  ]
  node [
    id 289
    label "imponowanie"
  ]
  node [
    id 290
    label "uhonorowanie"
  ]
  node [
    id 291
    label "honorowa&#263;"
  ]
  node [
    id 292
    label "uszanowa&#263;"
  ]
  node [
    id 293
    label "mniemanie"
  ]
  node [
    id 294
    label "szacuneczek"
  ]
  node [
    id 295
    label "recognition"
  ]
  node [
    id 296
    label "rewerencja"
  ]
  node [
    id 297
    label "szanowa&#263;"
  ]
  node [
    id 298
    label "postawa"
  ]
  node [
    id 299
    label "acclaim"
  ]
  node [
    id 300
    label "przej&#347;cie"
  ]
  node [
    id 301
    label "przechodzenie"
  ]
  node [
    id 302
    label "ocenienie"
  ]
  node [
    id 303
    label "zachwyt"
  ]
  node [
    id 304
    label "respect"
  ]
  node [
    id 305
    label "fame"
  ]
  node [
    id 306
    label "przenocowanie"
  ]
  node [
    id 307
    label "pora&#380;ka"
  ]
  node [
    id 308
    label "nak&#322;adzenie"
  ]
  node [
    id 309
    label "pouk&#322;adanie"
  ]
  node [
    id 310
    label "pokrycie"
  ]
  node [
    id 311
    label "zepsucie"
  ]
  node [
    id 312
    label "ustawienie"
  ]
  node [
    id 313
    label "trim"
  ]
  node [
    id 314
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 315
    label "ugoszczenie"
  ]
  node [
    id 316
    label "le&#380;enie"
  ]
  node [
    id 317
    label "adres"
  ]
  node [
    id 318
    label "zbudowanie"
  ]
  node [
    id 319
    label "reading"
  ]
  node [
    id 320
    label "sytuacja"
  ]
  node [
    id 321
    label "zabicie"
  ]
  node [
    id 322
    label "wygranie"
  ]
  node [
    id 323
    label "presentation"
  ]
  node [
    id 324
    label "le&#380;e&#263;"
  ]
  node [
    id 325
    label "delokalizacja"
  ]
  node [
    id 326
    label "osiedlenie"
  ]
  node [
    id 327
    label "poprzemieszczanie"
  ]
  node [
    id 328
    label "shift"
  ]
  node [
    id 329
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 330
    label "dostosowanie"
  ]
  node [
    id 331
    label "rozpowszechnienie"
  ]
  node [
    id 332
    label "skopiowanie"
  ]
  node [
    id 333
    label "pocisk"
  ]
  node [
    id 334
    label "assignment"
  ]
  node [
    id 335
    label "przelecenie"
  ]
  node [
    id 336
    label "mechanizm_obronny"
  ]
  node [
    id 337
    label "strzelenie"
  ]
  node [
    id 338
    label "przesadzenie"
  ]
  node [
    id 339
    label "poprzesuwanie"
  ]
  node [
    id 340
    label "zaadaptowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
]
