graph [
  node [
    id 0
    label "student"
    origin "text"
  ]
  node [
    id 1
    label "tutor"
  ]
  node [
    id 2
    label "akademik"
  ]
  node [
    id 3
    label "s&#322;uchacz"
  ]
  node [
    id 4
    label "indeks"
  ]
  node [
    id 5
    label "immatrykulowanie"
  ]
  node [
    id 6
    label "absolwent"
  ]
  node [
    id 7
    label "immatrykulowa&#263;"
  ]
  node [
    id 8
    label "cz&#322;owiek"
  ]
  node [
    id 9
    label "odbiorca"
  ]
  node [
    id 10
    label "cz&#322;onek"
  ]
  node [
    id 11
    label "reprezentant"
  ]
  node [
    id 12
    label "artysta"
  ]
  node [
    id 13
    label "przedstawiciel"
  ]
  node [
    id 14
    label "akademia"
  ]
  node [
    id 15
    label "pracownik_naukowy"
  ]
  node [
    id 16
    label "dom"
  ]
  node [
    id 17
    label "directory"
  ]
  node [
    id 18
    label "znak_pisarski"
  ]
  node [
    id 19
    label "spis"
  ]
  node [
    id 20
    label "album"
  ]
  node [
    id 21
    label "za&#347;wiadczenie"
  ]
  node [
    id 22
    label "wska&#378;nik"
  ]
  node [
    id 23
    label "indeks_Lernera"
  ]
  node [
    id 24
    label "szko&#322;a"
  ]
  node [
    id 25
    label "ucze&#324;"
  ]
  node [
    id 26
    label "zapisywanie"
  ]
  node [
    id 27
    label "zapisanie"
  ]
  node [
    id 28
    label "zapisa&#263;"
  ]
  node [
    id 29
    label "zapisywa&#263;"
  ]
  node [
    id 30
    label "nauczyciel_akademicki"
  ]
  node [
    id 31
    label "mentor"
  ]
  node [
    id 32
    label "opiekun"
  ]
  node [
    id 33
    label "wychowawca"
  ]
  node [
    id 34
    label "nauczyciel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
]
