graph [
  node [
    id 0
    label "hrabstwo"
    origin "text"
  ]
  node [
    id 1
    label "van"
    origin "text"
  ]
  node [
    id 2
    label "buren"
    origin "text"
  ]
  node [
    id 3
    label "tennessee"
    origin "text"
  ]
  node [
    id 4
    label "jednostka_administracyjna"
  ]
  node [
    id 5
    label "maj&#261;tek"
  ]
  node [
    id 6
    label "Norfolk"
  ]
  node [
    id 7
    label "Kornwalia"
  ]
  node [
    id 8
    label "Yorkshire"
  ]
  node [
    id 9
    label "possession"
  ]
  node [
    id 10
    label "stan"
  ]
  node [
    id 11
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 12
    label "Wilko"
  ]
  node [
    id 13
    label "rodowo&#347;&#263;"
  ]
  node [
    id 14
    label "frymark"
  ]
  node [
    id 15
    label "dobra"
  ]
  node [
    id 16
    label "mienie"
  ]
  node [
    id 17
    label "rezultat"
  ]
  node [
    id 18
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 19
    label "kwota"
  ]
  node [
    id 20
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 21
    label "patent"
  ]
  node [
    id 22
    label "przej&#347;&#263;"
  ]
  node [
    id 23
    label "przej&#347;cie"
  ]
  node [
    id 24
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 25
    label "Anglia"
  ]
  node [
    id 26
    label "samoch&#243;d"
  ]
  node [
    id 27
    label "nadwozie"
  ]
  node [
    id 28
    label "reflektor"
  ]
  node [
    id 29
    label "karoseria"
  ]
  node [
    id 30
    label "obudowa"
  ]
  node [
    id 31
    label "pr&#243;g"
  ]
  node [
    id 32
    label "dach"
  ]
  node [
    id 33
    label "zderzak"
  ]
  node [
    id 34
    label "b&#322;otnik"
  ]
  node [
    id 35
    label "pojazd"
  ]
  node [
    id 36
    label "spoiler"
  ]
  node [
    id 37
    label "buda"
  ]
  node [
    id 38
    label "poduszka_powietrzna"
  ]
  node [
    id 39
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 40
    label "pompa_wodna"
  ]
  node [
    id 41
    label "bak"
  ]
  node [
    id 42
    label "deska_rozdzielcza"
  ]
  node [
    id 43
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 44
    label "spryskiwacz"
  ]
  node [
    id 45
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 46
    label "baga&#380;nik"
  ]
  node [
    id 47
    label "poci&#261;g_drogowy"
  ]
  node [
    id 48
    label "immobilizer"
  ]
  node [
    id 49
    label "kierownica"
  ]
  node [
    id 50
    label "ABS"
  ]
  node [
    id 51
    label "dwu&#347;lad"
  ]
  node [
    id 52
    label "tempomat"
  ]
  node [
    id 53
    label "pojazd_drogowy"
  ]
  node [
    id 54
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 55
    label "wycieraczka"
  ]
  node [
    id 56
    label "most"
  ]
  node [
    id 57
    label "silnik"
  ]
  node [
    id 58
    label "t&#322;umik"
  ]
  node [
    id 59
    label "dachowanie"
  ]
  node [
    id 60
    label "Buren"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 2
    target 3
  ]
]
