graph [
  node [
    id 0
    label "rocznik"
    origin "text"
  ]
  node [
    id 1
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 2
    label "wieloletni"
    origin "text"
  ]
  node [
    id 3
    label "program"
    origin "text"
  ]
  node [
    id 4
    label "zwalczanie"
    origin "text"
  ]
  node [
    id 5
    label "kontrola"
    origin "text"
  ]
  node [
    id 6
    label "monitorowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "choroba"
    origin "text"
  ]
  node [
    id 8
    label "zwierz&#281;"
    origin "text"
  ]
  node [
    id 9
    label "odzwierz&#281;cy"
    origin "text"
  ]
  node [
    id 10
    label "zg&#322;osi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "przez"
    origin "text"
  ]
  node [
    id 12
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 13
    label "cz&#322;onkowski"
    origin "text"
  ]
  node [
    id 14
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 15
    label "zatwierdzi&#263;"
    origin "text"
  ]
  node [
    id 16
    label "decyzja"
    origin "text"
  ]
  node [
    id 17
    label "komisja"
    origin "text"
  ]
  node [
    id 18
    label "zobowi&#261;zanie"
    origin "text"
  ]
  node [
    id 19
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 20
    label "wydatek"
    origin "text"
  ]
  node [
    id 21
    label "przyj&#281;ty"
    origin "text"
  ]
  node [
    id 22
    label "zgodnie"
    origin "text"
  ]
  node [
    id 23
    label "art"
    origin "text"
  ]
  node [
    id 24
    label "usta"
    origin "text"
  ]
  node [
    id 25
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 26
    label "rad"
    origin "text"
  ]
  node [
    id 27
    label "euratom"
    origin "text"
  ]
  node [
    id 28
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 29
    label "czerwiec"
    origin "text"
  ]
  node [
    id 30
    label "sprawa"
    origin "text"
  ]
  node [
    id 31
    label "finansowy"
    origin "text"
  ]
  node [
    id 32
    label "mai&#263;"
    origin "text"
  ]
  node [
    id 33
    label "zastosowanie"
    origin "text"
  ]
  node [
    id 34
    label "bud&#380;et"
    origin "text"
  ]
  node [
    id 35
    label "og&#243;lny"
    origin "text"
  ]
  node [
    id 36
    label "wsp&#243;lnota"
    origin "text"
  ]
  node [
    id 37
    label "europejski"
    origin "text"
  ]
  node [
    id 38
    label "pierwsza"
    origin "text"
  ]
  node [
    id 39
    label "bud&#380;etowy"
    origin "text"
  ]
  node [
    id 40
    label "przypadek"
    origin "text"
  ]
  node [
    id 41
    label "tychy"
    origin "text"
  ]
  node [
    id 42
    label "podj&#261;&#263;"
    origin "text"
  ]
  node [
    id 43
    label "zatwierdzenie"
    origin "text"
  ]
  node [
    id 44
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 45
    label "kolejny"
    origin "text"
  ]
  node [
    id 46
    label "roczny"
    origin "text"
  ]
  node [
    id 47
    label "by&#263;"
    origin "text"
  ]
  node [
    id 48
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 49
    label "zale&#380;no&#347;&#263;"
    origin "text"
  ]
  node [
    id 50
    label "wykonanie"
    origin "text"
  ]
  node [
    id 51
    label "rok"
    origin "text"
  ]
  node [
    id 52
    label "poprzedni"
    origin "text"
  ]
  node [
    id 53
    label "podstawa"
    origin "text"
  ]
  node [
    id 54
    label "przyznanie"
    origin "text"
  ]
  node [
    id 55
    label "wk&#322;ad"
    origin "text"
  ]
  node [
    id 56
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 57
    label "mowa"
    origin "text"
  ]
  node [
    id 58
    label "formacja"
  ]
  node [
    id 59
    label "yearbook"
  ]
  node [
    id 60
    label "czasopismo"
  ]
  node [
    id 61
    label "kronika"
  ]
  node [
    id 62
    label "Bund"
  ]
  node [
    id 63
    label "Mazowsze"
  ]
  node [
    id 64
    label "PPR"
  ]
  node [
    id 65
    label "Jakobici"
  ]
  node [
    id 66
    label "zesp&#243;&#322;"
  ]
  node [
    id 67
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 68
    label "leksem"
  ]
  node [
    id 69
    label "SLD"
  ]
  node [
    id 70
    label "zespolik"
  ]
  node [
    id 71
    label "Razem"
  ]
  node [
    id 72
    label "PiS"
  ]
  node [
    id 73
    label "zjawisko"
  ]
  node [
    id 74
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 75
    label "partia"
  ]
  node [
    id 76
    label "Kuomintang"
  ]
  node [
    id 77
    label "ZSL"
  ]
  node [
    id 78
    label "szko&#322;a"
  ]
  node [
    id 79
    label "jednostka"
  ]
  node [
    id 80
    label "proces"
  ]
  node [
    id 81
    label "organizacja"
  ]
  node [
    id 82
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 83
    label "rugby"
  ]
  node [
    id 84
    label "AWS"
  ]
  node [
    id 85
    label "posta&#263;"
  ]
  node [
    id 86
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 87
    label "blok"
  ]
  node [
    id 88
    label "PO"
  ]
  node [
    id 89
    label "si&#322;a"
  ]
  node [
    id 90
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 91
    label "Federali&#347;ci"
  ]
  node [
    id 92
    label "PSL"
  ]
  node [
    id 93
    label "czynno&#347;&#263;"
  ]
  node [
    id 94
    label "wojsko"
  ]
  node [
    id 95
    label "Wigowie"
  ]
  node [
    id 96
    label "ZChN"
  ]
  node [
    id 97
    label "egzekutywa"
  ]
  node [
    id 98
    label "The_Beatles"
  ]
  node [
    id 99
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 100
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 101
    label "unit"
  ]
  node [
    id 102
    label "Depeche_Mode"
  ]
  node [
    id 103
    label "forma"
  ]
  node [
    id 104
    label "zapis"
  ]
  node [
    id 105
    label "chronograf"
  ]
  node [
    id 106
    label "latopis"
  ]
  node [
    id 107
    label "ksi&#281;ga"
  ]
  node [
    id 108
    label "egzemplarz"
  ]
  node [
    id 109
    label "psychotest"
  ]
  node [
    id 110
    label "pismo"
  ]
  node [
    id 111
    label "communication"
  ]
  node [
    id 112
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 113
    label "zajawka"
  ]
  node [
    id 114
    label "ok&#322;adka"
  ]
  node [
    id 115
    label "Zwrotnica"
  ]
  node [
    id 116
    label "dzia&#322;"
  ]
  node [
    id 117
    label "prasa"
  ]
  node [
    id 118
    label "jaki&#347;"
  ]
  node [
    id 119
    label "przyzwoity"
  ]
  node [
    id 120
    label "ciekawy"
  ]
  node [
    id 121
    label "jako&#347;"
  ]
  node [
    id 122
    label "jako_tako"
  ]
  node [
    id 123
    label "niez&#322;y"
  ]
  node [
    id 124
    label "dziwny"
  ]
  node [
    id 125
    label "charakterystyczny"
  ]
  node [
    id 126
    label "d&#322;ugi"
  ]
  node [
    id 127
    label "d&#322;ugotrwa&#322;y"
  ]
  node [
    id 128
    label "d&#322;ugotrwale"
  ]
  node [
    id 129
    label "daleki"
  ]
  node [
    id 130
    label "ruch"
  ]
  node [
    id 131
    label "d&#322;ugo"
  ]
  node [
    id 132
    label "instalowa&#263;"
  ]
  node [
    id 133
    label "oprogramowanie"
  ]
  node [
    id 134
    label "odinstalowywa&#263;"
  ]
  node [
    id 135
    label "spis"
  ]
  node [
    id 136
    label "zaprezentowanie"
  ]
  node [
    id 137
    label "podprogram"
  ]
  node [
    id 138
    label "ogranicznik_referencyjny"
  ]
  node [
    id 139
    label "course_of_study"
  ]
  node [
    id 140
    label "booklet"
  ]
  node [
    id 141
    label "odinstalowanie"
  ]
  node [
    id 142
    label "broszura"
  ]
  node [
    id 143
    label "wytw&#243;r"
  ]
  node [
    id 144
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 145
    label "kana&#322;"
  ]
  node [
    id 146
    label "teleferie"
  ]
  node [
    id 147
    label "zainstalowanie"
  ]
  node [
    id 148
    label "struktura_organizacyjna"
  ]
  node [
    id 149
    label "pirat"
  ]
  node [
    id 150
    label "zaprezentowa&#263;"
  ]
  node [
    id 151
    label "prezentowanie"
  ]
  node [
    id 152
    label "prezentowa&#263;"
  ]
  node [
    id 153
    label "interfejs"
  ]
  node [
    id 154
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 155
    label "okno"
  ]
  node [
    id 156
    label "punkt"
  ]
  node [
    id 157
    label "folder"
  ]
  node [
    id 158
    label "zainstalowa&#263;"
  ]
  node [
    id 159
    label "za&#322;o&#380;enie"
  ]
  node [
    id 160
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 161
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 162
    label "ram&#243;wka"
  ]
  node [
    id 163
    label "tryb"
  ]
  node [
    id 164
    label "emitowa&#263;"
  ]
  node [
    id 165
    label "emitowanie"
  ]
  node [
    id 166
    label "odinstalowywanie"
  ]
  node [
    id 167
    label "instrukcja"
  ]
  node [
    id 168
    label "informatyka"
  ]
  node [
    id 169
    label "deklaracja"
  ]
  node [
    id 170
    label "sekcja_krytyczna"
  ]
  node [
    id 171
    label "menu"
  ]
  node [
    id 172
    label "furkacja"
  ]
  node [
    id 173
    label "instalowanie"
  ]
  node [
    id 174
    label "oferta"
  ]
  node [
    id 175
    label "odinstalowa&#263;"
  ]
  node [
    id 176
    label "druk_ulotny"
  ]
  node [
    id 177
    label "wydawnictwo"
  ]
  node [
    id 178
    label "pot&#281;ga"
  ]
  node [
    id 179
    label "documentation"
  ]
  node [
    id 180
    label "przedmiot"
  ]
  node [
    id 181
    label "column"
  ]
  node [
    id 182
    label "zasadzenie"
  ]
  node [
    id 183
    label "punkt_odniesienia"
  ]
  node [
    id 184
    label "zasadzi&#263;"
  ]
  node [
    id 185
    label "bok"
  ]
  node [
    id 186
    label "d&#243;&#322;"
  ]
  node [
    id 187
    label "dzieci&#281;ctwo"
  ]
  node [
    id 188
    label "background"
  ]
  node [
    id 189
    label "podstawowy"
  ]
  node [
    id 190
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 191
    label "strategia"
  ]
  node [
    id 192
    label "pomys&#322;"
  ]
  node [
    id 193
    label "&#347;ciana"
  ]
  node [
    id 194
    label "rozmiar"
  ]
  node [
    id 195
    label "zakres"
  ]
  node [
    id 196
    label "zasi&#261;g"
  ]
  node [
    id 197
    label "izochronizm"
  ]
  node [
    id 198
    label "bridge"
  ]
  node [
    id 199
    label "distribution"
  ]
  node [
    id 200
    label "p&#322;&#243;d"
  ]
  node [
    id 201
    label "work"
  ]
  node [
    id 202
    label "rezultat"
  ]
  node [
    id 203
    label "ko&#322;o"
  ]
  node [
    id 204
    label "spos&#243;b"
  ]
  node [
    id 205
    label "modalno&#347;&#263;"
  ]
  node [
    id 206
    label "z&#261;b"
  ]
  node [
    id 207
    label "cecha"
  ]
  node [
    id 208
    label "kategoria_gramatyczna"
  ]
  node [
    id 209
    label "skala"
  ]
  node [
    id 210
    label "funkcjonowa&#263;"
  ]
  node [
    id 211
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 212
    label "koniugacja"
  ]
  node [
    id 213
    label "offer"
  ]
  node [
    id 214
    label "propozycja"
  ]
  node [
    id 215
    label "o&#347;wiadczenie"
  ]
  node [
    id 216
    label "obietnica"
  ]
  node [
    id 217
    label "formularz"
  ]
  node [
    id 218
    label "statement"
  ]
  node [
    id 219
    label "announcement"
  ]
  node [
    id 220
    label "akt"
  ]
  node [
    id 221
    label "digest"
  ]
  node [
    id 222
    label "konstrukcja"
  ]
  node [
    id 223
    label "dokument"
  ]
  node [
    id 224
    label "o&#347;wiadczyny"
  ]
  node [
    id 225
    label "szaniec"
  ]
  node [
    id 226
    label "topologia_magistrali"
  ]
  node [
    id 227
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 228
    label "grodzisko"
  ]
  node [
    id 229
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 230
    label "tarapaty"
  ]
  node [
    id 231
    label "piaskownik"
  ]
  node [
    id 232
    label "struktura_anatomiczna"
  ]
  node [
    id 233
    label "miejsce"
  ]
  node [
    id 234
    label "bystrza"
  ]
  node [
    id 235
    label "pit"
  ]
  node [
    id 236
    label "odk&#322;ad"
  ]
  node [
    id 237
    label "chody"
  ]
  node [
    id 238
    label "klarownia"
  ]
  node [
    id 239
    label "kanalizacja"
  ]
  node [
    id 240
    label "przew&#243;d"
  ]
  node [
    id 241
    label "budowa"
  ]
  node [
    id 242
    label "ciek"
  ]
  node [
    id 243
    label "teatr"
  ]
  node [
    id 244
    label "gara&#380;"
  ]
  node [
    id 245
    label "zrzutowy"
  ]
  node [
    id 246
    label "warsztat"
  ]
  node [
    id 247
    label "syfon"
  ]
  node [
    id 248
    label "odwa&#322;"
  ]
  node [
    id 249
    label "urz&#261;dzenie"
  ]
  node [
    id 250
    label "zbi&#243;r"
  ]
  node [
    id 251
    label "catalog"
  ]
  node [
    id 252
    label "pozycja"
  ]
  node [
    id 253
    label "tekst"
  ]
  node [
    id 254
    label "sumariusz"
  ]
  node [
    id 255
    label "book"
  ]
  node [
    id 256
    label "stock"
  ]
  node [
    id 257
    label "figurowa&#263;"
  ]
  node [
    id 258
    label "wyliczanka"
  ]
  node [
    id 259
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 260
    label "HP"
  ]
  node [
    id 261
    label "dost&#281;p"
  ]
  node [
    id 262
    label "infa"
  ]
  node [
    id 263
    label "kierunek"
  ]
  node [
    id 264
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 265
    label "kryptologia"
  ]
  node [
    id 266
    label "baza_danych"
  ]
  node [
    id 267
    label "przetwarzanie_informacji"
  ]
  node [
    id 268
    label "sztuczna_inteligencja"
  ]
  node [
    id 269
    label "gramatyka_formalna"
  ]
  node [
    id 270
    label "zamek"
  ]
  node [
    id 271
    label "dziedzina_informatyki"
  ]
  node [
    id 272
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 273
    label "artefakt"
  ]
  node [
    id 274
    label "usuni&#281;cie"
  ]
  node [
    id 275
    label "parapet"
  ]
  node [
    id 276
    label "szyba"
  ]
  node [
    id 277
    label "okiennica"
  ]
  node [
    id 278
    label "prze&#347;wit"
  ]
  node [
    id 279
    label "pulpit"
  ]
  node [
    id 280
    label "transenna"
  ]
  node [
    id 281
    label "kwatera_okienna"
  ]
  node [
    id 282
    label "inspekt"
  ]
  node [
    id 283
    label "nora"
  ]
  node [
    id 284
    label "skrzyd&#322;o"
  ]
  node [
    id 285
    label "nadokiennik"
  ]
  node [
    id 286
    label "futryna"
  ]
  node [
    id 287
    label "lufcik"
  ]
  node [
    id 288
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 289
    label "casement"
  ]
  node [
    id 290
    label "menad&#380;er_okien"
  ]
  node [
    id 291
    label "otw&#243;r"
  ]
  node [
    id 292
    label "dostosowywa&#263;"
  ]
  node [
    id 293
    label "supply"
  ]
  node [
    id 294
    label "robi&#263;"
  ]
  node [
    id 295
    label "accommodate"
  ]
  node [
    id 296
    label "komputer"
  ]
  node [
    id 297
    label "umieszcza&#263;"
  ]
  node [
    id 298
    label "fit"
  ]
  node [
    id 299
    label "zdolno&#347;&#263;"
  ]
  node [
    id 300
    label "usuwa&#263;"
  ]
  node [
    id 301
    label "usuwanie"
  ]
  node [
    id 302
    label "dostosowa&#263;"
  ]
  node [
    id 303
    label "zrobi&#263;"
  ]
  node [
    id 304
    label "install"
  ]
  node [
    id 305
    label "umie&#347;ci&#263;"
  ]
  node [
    id 306
    label "umieszczanie"
  ]
  node [
    id 307
    label "installation"
  ]
  node [
    id 308
    label "collection"
  ]
  node [
    id 309
    label "robienie"
  ]
  node [
    id 310
    label "wmontowanie"
  ]
  node [
    id 311
    label "wmontowywanie"
  ]
  node [
    id 312
    label "fitting"
  ]
  node [
    id 313
    label "dostosowywanie"
  ]
  node [
    id 314
    label "usun&#261;&#263;"
  ]
  node [
    id 315
    label "dostosowanie"
  ]
  node [
    id 316
    label "pozak&#322;adanie"
  ]
  node [
    id 317
    label "proposition"
  ]
  node [
    id 318
    label "layout"
  ]
  node [
    id 319
    label "umieszczenie"
  ]
  node [
    id 320
    label "zrobienie"
  ]
  node [
    id 321
    label "przest&#281;pca"
  ]
  node [
    id 322
    label "kopiowa&#263;"
  ]
  node [
    id 323
    label "podr&#243;bka"
  ]
  node [
    id 324
    label "kieruj&#261;cy"
  ]
  node [
    id 325
    label "&#380;agl&#243;wka"
  ]
  node [
    id 326
    label "rum"
  ]
  node [
    id 327
    label "rozb&#243;jnik"
  ]
  node [
    id 328
    label "postrzeleniec"
  ]
  node [
    id 329
    label "testify"
  ]
  node [
    id 330
    label "przedstawi&#263;"
  ]
  node [
    id 331
    label "zapozna&#263;"
  ]
  node [
    id 332
    label "pokaza&#263;"
  ]
  node [
    id 333
    label "represent"
  ]
  node [
    id 334
    label "typify"
  ]
  node [
    id 335
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 336
    label "uprzedzi&#263;"
  ]
  node [
    id 337
    label "attest"
  ]
  node [
    id 338
    label "wyra&#380;anie"
  ]
  node [
    id 339
    label "uprzedzanie"
  ]
  node [
    id 340
    label "representation"
  ]
  node [
    id 341
    label "zapoznawanie"
  ]
  node [
    id 342
    label "present"
  ]
  node [
    id 343
    label "przedstawianie"
  ]
  node [
    id 344
    label "display"
  ]
  node [
    id 345
    label "demonstrowanie"
  ]
  node [
    id 346
    label "presentation"
  ]
  node [
    id 347
    label "granie"
  ]
  node [
    id 348
    label "zapoznanie"
  ]
  node [
    id 349
    label "zapoznanie_si&#281;"
  ]
  node [
    id 350
    label "exhibit"
  ]
  node [
    id 351
    label "pokazanie"
  ]
  node [
    id 352
    label "wyst&#261;pienie"
  ]
  node [
    id 353
    label "uprzedzenie"
  ]
  node [
    id 354
    label "przedstawienie"
  ]
  node [
    id 355
    label "gra&#263;"
  ]
  node [
    id 356
    label "zapoznawa&#263;"
  ]
  node [
    id 357
    label "uprzedza&#263;"
  ]
  node [
    id 358
    label "wyra&#380;a&#263;"
  ]
  node [
    id 359
    label "przedstawia&#263;"
  ]
  node [
    id 360
    label "rynek"
  ]
  node [
    id 361
    label "nadawa&#263;"
  ]
  node [
    id 362
    label "wysy&#322;a&#263;"
  ]
  node [
    id 363
    label "energia"
  ]
  node [
    id 364
    label "nada&#263;"
  ]
  node [
    id 365
    label "tembr"
  ]
  node [
    id 366
    label "air"
  ]
  node [
    id 367
    label "wydoby&#263;"
  ]
  node [
    id 368
    label "emit"
  ]
  node [
    id 369
    label "wys&#322;a&#263;"
  ]
  node [
    id 370
    label "wydzieli&#263;"
  ]
  node [
    id 371
    label "wydziela&#263;"
  ]
  node [
    id 372
    label "wprowadzi&#263;"
  ]
  node [
    id 373
    label "wydobywa&#263;"
  ]
  node [
    id 374
    label "wprowadza&#263;"
  ]
  node [
    id 375
    label "wysy&#322;anie"
  ]
  node [
    id 376
    label "wys&#322;anie"
  ]
  node [
    id 377
    label "wydzielenie"
  ]
  node [
    id 378
    label "wprowadzenie"
  ]
  node [
    id 379
    label "wydobycie"
  ]
  node [
    id 380
    label "wydzielanie"
  ]
  node [
    id 381
    label "wydobywanie"
  ]
  node [
    id 382
    label "nadawanie"
  ]
  node [
    id 383
    label "emission"
  ]
  node [
    id 384
    label "wprowadzanie"
  ]
  node [
    id 385
    label "nadanie"
  ]
  node [
    id 386
    label "issue"
  ]
  node [
    id 387
    label "ulotka"
  ]
  node [
    id 388
    label "wskaz&#243;wka"
  ]
  node [
    id 389
    label "instruktarz"
  ]
  node [
    id 390
    label "danie"
  ]
  node [
    id 391
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 392
    label "restauracja"
  ]
  node [
    id 393
    label "cennik"
  ]
  node [
    id 394
    label "chart"
  ]
  node [
    id 395
    label "karta"
  ]
  node [
    id 396
    label "zestaw"
  ]
  node [
    id 397
    label "bajt"
  ]
  node [
    id 398
    label "bloking"
  ]
  node [
    id 399
    label "j&#261;kanie"
  ]
  node [
    id 400
    label "przeszkoda"
  ]
  node [
    id 401
    label "blokada"
  ]
  node [
    id 402
    label "bry&#322;a"
  ]
  node [
    id 403
    label "kontynent"
  ]
  node [
    id 404
    label "nastawnia"
  ]
  node [
    id 405
    label "blockage"
  ]
  node [
    id 406
    label "block"
  ]
  node [
    id 407
    label "budynek"
  ]
  node [
    id 408
    label "start"
  ]
  node [
    id 409
    label "skorupa_ziemska"
  ]
  node [
    id 410
    label "zeszyt"
  ]
  node [
    id 411
    label "grupa"
  ]
  node [
    id 412
    label "blokowisko"
  ]
  node [
    id 413
    label "artyku&#322;"
  ]
  node [
    id 414
    label "barak"
  ]
  node [
    id 415
    label "stok_kontynentalny"
  ]
  node [
    id 416
    label "whole"
  ]
  node [
    id 417
    label "square"
  ]
  node [
    id 418
    label "siatk&#243;wka"
  ]
  node [
    id 419
    label "kr&#261;g"
  ]
  node [
    id 420
    label "obrona"
  ]
  node [
    id 421
    label "bie&#380;nia"
  ]
  node [
    id 422
    label "referat"
  ]
  node [
    id 423
    label "dom_wielorodzinny"
  ]
  node [
    id 424
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 425
    label "routine"
  ]
  node [
    id 426
    label "proceduralnie"
  ]
  node [
    id 427
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 428
    label "jednostka_organizacyjna"
  ]
  node [
    id 429
    label "urz&#261;d"
  ]
  node [
    id 430
    label "sfera"
  ]
  node [
    id 431
    label "miejsce_pracy"
  ]
  node [
    id 432
    label "insourcing"
  ]
  node [
    id 433
    label "stopie&#324;"
  ]
  node [
    id 434
    label "competence"
  ]
  node [
    id 435
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 436
    label "bezdro&#380;e"
  ]
  node [
    id 437
    label "poddzia&#322;"
  ]
  node [
    id 438
    label "podwini&#281;cie"
  ]
  node [
    id 439
    label "zap&#322;acenie"
  ]
  node [
    id 440
    label "przyodzianie"
  ]
  node [
    id 441
    label "budowla"
  ]
  node [
    id 442
    label "pokrycie"
  ]
  node [
    id 443
    label "rozebranie"
  ]
  node [
    id 444
    label "zak&#322;adka"
  ]
  node [
    id 445
    label "struktura"
  ]
  node [
    id 446
    label "poubieranie"
  ]
  node [
    id 447
    label "infliction"
  ]
  node [
    id 448
    label "spowodowanie"
  ]
  node [
    id 449
    label "przebranie"
  ]
  node [
    id 450
    label "przywdzianie"
  ]
  node [
    id 451
    label "obleczenie_si&#281;"
  ]
  node [
    id 452
    label "utworzenie"
  ]
  node [
    id 453
    label "str&#243;j"
  ]
  node [
    id 454
    label "twierdzenie"
  ]
  node [
    id 455
    label "obleczenie"
  ]
  node [
    id 456
    label "przygotowywanie"
  ]
  node [
    id 457
    label "przymierzenie"
  ]
  node [
    id 458
    label "wyko&#324;czenie"
  ]
  node [
    id 459
    label "point"
  ]
  node [
    id 460
    label "przygotowanie"
  ]
  node [
    id 461
    label "przewidzenie"
  ]
  node [
    id 462
    label "po&#322;o&#380;enie"
  ]
  node [
    id 463
    label "ust&#281;p"
  ]
  node [
    id 464
    label "plan"
  ]
  node [
    id 465
    label "obiekt_matematyczny"
  ]
  node [
    id 466
    label "problemat"
  ]
  node [
    id 467
    label "plamka"
  ]
  node [
    id 468
    label "stopie&#324;_pisma"
  ]
  node [
    id 469
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 470
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 471
    label "mark"
  ]
  node [
    id 472
    label "chwila"
  ]
  node [
    id 473
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 474
    label "prosta"
  ]
  node [
    id 475
    label "problematyka"
  ]
  node [
    id 476
    label "obiekt"
  ]
  node [
    id 477
    label "zapunktowa&#263;"
  ]
  node [
    id 478
    label "podpunkt"
  ]
  node [
    id 479
    label "kres"
  ]
  node [
    id 480
    label "przestrze&#324;"
  ]
  node [
    id 481
    label "reengineering"
  ]
  node [
    id 482
    label "scheduling"
  ]
  node [
    id 483
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 484
    label "okienko"
  ]
  node [
    id 485
    label "pokonywanie"
  ]
  node [
    id 486
    label "sprzeciwianie_si&#281;"
  ]
  node [
    id 487
    label "znoszenie"
  ]
  node [
    id 488
    label "radzenie_sobie"
  ]
  node [
    id 489
    label "wygranie"
  ]
  node [
    id 490
    label "atakowanie"
  ]
  node [
    id 491
    label "zmierzanie"
  ]
  node [
    id 492
    label "sojourn"
  ]
  node [
    id 493
    label "podbijanie"
  ]
  node [
    id 494
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 495
    label "legalizacja_ponowna"
  ]
  node [
    id 496
    label "instytucja"
  ]
  node [
    id 497
    label "w&#322;adza"
  ]
  node [
    id 498
    label "perlustracja"
  ]
  node [
    id 499
    label "legalizacja_pierwotna"
  ]
  node [
    id 500
    label "examination"
  ]
  node [
    id 501
    label "prawo"
  ]
  node [
    id 502
    label "cz&#322;owiek"
  ]
  node [
    id 503
    label "rz&#261;dzenie"
  ]
  node [
    id 504
    label "panowanie"
  ]
  node [
    id 505
    label "Kreml"
  ]
  node [
    id 506
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 507
    label "wydolno&#347;&#263;"
  ]
  node [
    id 508
    label "rz&#261;d"
  ]
  node [
    id 509
    label "osoba_prawna"
  ]
  node [
    id 510
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 511
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 512
    label "poj&#281;cie"
  ]
  node [
    id 513
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 514
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 515
    label "biuro"
  ]
  node [
    id 516
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 517
    label "Fundusze_Unijne"
  ]
  node [
    id 518
    label "zamyka&#263;"
  ]
  node [
    id 519
    label "establishment"
  ]
  node [
    id 520
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 521
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 522
    label "afiliowa&#263;"
  ]
  node [
    id 523
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 524
    label "standard"
  ]
  node [
    id 525
    label "zamykanie"
  ]
  node [
    id 526
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 527
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 528
    label "activity"
  ]
  node [
    id 529
    label "bezproblemowy"
  ]
  node [
    id 530
    label "wydarzenie"
  ]
  node [
    id 531
    label "kontrolowa&#263;"
  ]
  node [
    id 532
    label "obserwowa&#263;"
  ]
  node [
    id 533
    label "manipulate"
  ]
  node [
    id 534
    label "pracowa&#263;"
  ]
  node [
    id 535
    label "match"
  ]
  node [
    id 536
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 537
    label "examine"
  ]
  node [
    id 538
    label "dostrzega&#263;"
  ]
  node [
    id 539
    label "patrze&#263;"
  ]
  node [
    id 540
    label "look"
  ]
  node [
    id 541
    label "cholera"
  ]
  node [
    id 542
    label "ognisko"
  ]
  node [
    id 543
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 544
    label "powalenie"
  ]
  node [
    id 545
    label "odezwanie_si&#281;"
  ]
  node [
    id 546
    label "grupa_ryzyka"
  ]
  node [
    id 547
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 548
    label "bol&#261;czka"
  ]
  node [
    id 549
    label "nabawienie_si&#281;"
  ]
  node [
    id 550
    label "inkubacja"
  ]
  node [
    id 551
    label "kryzys"
  ]
  node [
    id 552
    label "powali&#263;"
  ]
  node [
    id 553
    label "remisja"
  ]
  node [
    id 554
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 555
    label "zajmowa&#263;"
  ]
  node [
    id 556
    label "zaburzenie"
  ]
  node [
    id 557
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 558
    label "chor&#243;bka"
  ]
  node [
    id 559
    label "badanie_histopatologiczne"
  ]
  node [
    id 560
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 561
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 562
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 563
    label "odzywanie_si&#281;"
  ]
  node [
    id 564
    label "diagnoza"
  ]
  node [
    id 565
    label "atakowa&#263;"
  ]
  node [
    id 566
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 567
    label "nabawianie_si&#281;"
  ]
  node [
    id 568
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 569
    label "zajmowanie"
  ]
  node [
    id 570
    label "strapienie"
  ]
  node [
    id 571
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 572
    label "discourtesy"
  ]
  node [
    id 573
    label "disorder"
  ]
  node [
    id 574
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 575
    label "transgresja"
  ]
  node [
    id 576
    label "sytuacja"
  ]
  node [
    id 577
    label "wyl&#281;g"
  ]
  node [
    id 578
    label "schorzenie"
  ]
  node [
    id 579
    label "July"
  ]
  node [
    id 580
    label "k&#322;opot"
  ]
  node [
    id 581
    label "cykl_koniunkturalny"
  ]
  node [
    id 582
    label "zwrot"
  ]
  node [
    id 583
    label "Marzec_'68"
  ]
  node [
    id 584
    label "pogorszenie"
  ]
  node [
    id 585
    label "head"
  ]
  node [
    id 586
    label "os&#322;abienie"
  ]
  node [
    id 587
    label "walni&#281;cie"
  ]
  node [
    id 588
    label "collapse"
  ]
  node [
    id 589
    label "zamordowanie"
  ]
  node [
    id 590
    label "prostration"
  ]
  node [
    id 591
    label "pacjent"
  ]
  node [
    id 592
    label "happening"
  ]
  node [
    id 593
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 594
    label "przyk&#322;ad"
  ]
  node [
    id 595
    label "przeznaczenie"
  ]
  node [
    id 596
    label "grasowanie"
  ]
  node [
    id 597
    label "napadanie"
  ]
  node [
    id 598
    label "rozgrywanie"
  ]
  node [
    id 599
    label "przewaga"
  ]
  node [
    id 600
    label "k&#322;&#243;cenie_si&#281;"
  ]
  node [
    id 601
    label "polowanie"
  ]
  node [
    id 602
    label "walczenie"
  ]
  node [
    id 603
    label "usi&#322;owanie"
  ]
  node [
    id 604
    label "epidemia"
  ]
  node [
    id 605
    label "sport"
  ]
  node [
    id 606
    label "m&#243;wienie"
  ]
  node [
    id 607
    label "wyskakiwanie_z_g&#281;b&#261;"
  ]
  node [
    id 608
    label "pojawianie_si&#281;"
  ]
  node [
    id 609
    label "krytykowanie"
  ]
  node [
    id 610
    label "torpedowanie"
  ]
  node [
    id 611
    label "szczucie"
  ]
  node [
    id 612
    label "przebywanie"
  ]
  node [
    id 613
    label "oddzia&#322;ywanie"
  ]
  node [
    id 614
    label "friction"
  ]
  node [
    id 615
    label "nast&#281;powanie"
  ]
  node [
    id 616
    label "dostarcza&#263;"
  ]
  node [
    id 617
    label "korzysta&#263;"
  ]
  node [
    id 618
    label "komornik"
  ]
  node [
    id 619
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 620
    label "return"
  ]
  node [
    id 621
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 622
    label "trwa&#263;"
  ]
  node [
    id 623
    label "bra&#263;"
  ]
  node [
    id 624
    label "rozciekawia&#263;"
  ]
  node [
    id 625
    label "klasyfikacja"
  ]
  node [
    id 626
    label "zadawa&#263;"
  ]
  node [
    id 627
    label "fill"
  ]
  node [
    id 628
    label "zabiera&#263;"
  ]
  node [
    id 629
    label "topographic_point"
  ]
  node [
    id 630
    label "obejmowa&#263;"
  ]
  node [
    id 631
    label "pali&#263;_si&#281;"
  ]
  node [
    id 632
    label "aim"
  ]
  node [
    id 633
    label "anektowa&#263;"
  ]
  node [
    id 634
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 635
    label "prosecute"
  ]
  node [
    id 636
    label "powodowa&#263;"
  ]
  node [
    id 637
    label "sake"
  ]
  node [
    id 638
    label "do"
  ]
  node [
    id 639
    label "diagnosis"
  ]
  node [
    id 640
    label "sprawdzian"
  ]
  node [
    id 641
    label "rozpoznanie"
  ]
  node [
    id 642
    label "ocena"
  ]
  node [
    id 643
    label "powodowanie"
  ]
  node [
    id 644
    label "lokowanie_si&#281;"
  ]
  node [
    id 645
    label "zajmowanie_si&#281;"
  ]
  node [
    id 646
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 647
    label "stosowanie"
  ]
  node [
    id 648
    label "anektowanie"
  ]
  node [
    id 649
    label "zabieranie"
  ]
  node [
    id 650
    label "sytuowanie_si&#281;"
  ]
  node [
    id 651
    label "wype&#322;nianie"
  ]
  node [
    id 652
    label "obejmowanie"
  ]
  node [
    id 653
    label "dzianie_si&#281;"
  ]
  node [
    id 654
    label "bycie"
  ]
  node [
    id 655
    label "branie"
  ]
  node [
    id 656
    label "occupation"
  ]
  node [
    id 657
    label "zadawanie"
  ]
  node [
    id 658
    label "zaj&#281;ty"
  ]
  node [
    id 659
    label "strike"
  ]
  node [
    id 660
    label "dzia&#322;a&#263;"
  ]
  node [
    id 661
    label "ofensywny"
  ]
  node [
    id 662
    label "attack"
  ]
  node [
    id 663
    label "rozgrywa&#263;"
  ]
  node [
    id 664
    label "krytykowa&#263;"
  ]
  node [
    id 665
    label "walczy&#263;"
  ]
  node [
    id 666
    label "trouble_oneself"
  ]
  node [
    id 667
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 668
    label "napada&#263;"
  ]
  node [
    id 669
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 670
    label "m&#243;wi&#263;"
  ]
  node [
    id 671
    label "nast&#281;powa&#263;"
  ]
  node [
    id 672
    label "usi&#322;owa&#263;"
  ]
  node [
    id 673
    label "skupisko"
  ]
  node [
    id 674
    label "&#347;wiat&#322;o"
  ]
  node [
    id 675
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 676
    label "impreza"
  ]
  node [
    id 677
    label "Hollywood"
  ]
  node [
    id 678
    label "center"
  ]
  node [
    id 679
    label "palenisko"
  ]
  node [
    id 680
    label "skupia&#263;"
  ]
  node [
    id 681
    label "o&#347;rodek"
  ]
  node [
    id 682
    label "watra"
  ]
  node [
    id 683
    label "hotbed"
  ]
  node [
    id 684
    label "skupi&#263;"
  ]
  node [
    id 685
    label "waln&#261;&#263;"
  ]
  node [
    id 686
    label "zamordowa&#263;"
  ]
  node [
    id 687
    label "drop"
  ]
  node [
    id 688
    label "os&#322;abi&#263;"
  ]
  node [
    id 689
    label "give"
  ]
  node [
    id 690
    label "spot"
  ]
  node [
    id 691
    label "wykrzyknik"
  ]
  node [
    id 692
    label "jasny_gwint"
  ]
  node [
    id 693
    label "chory"
  ]
  node [
    id 694
    label "skurczybyk"
  ]
  node [
    id 695
    label "holender"
  ]
  node [
    id 696
    label "przecinkowiec_cholery"
  ]
  node [
    id 697
    label "choroba_bakteryjna"
  ]
  node [
    id 698
    label "wyzwisko"
  ]
  node [
    id 699
    label "gniew"
  ]
  node [
    id 700
    label "charakternik"
  ]
  node [
    id 701
    label "cholewa"
  ]
  node [
    id 702
    label "istota_&#380;ywa"
  ]
  node [
    id 703
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 704
    label "przekle&#324;stwo"
  ]
  node [
    id 705
    label "fury"
  ]
  node [
    id 706
    label "degenerat"
  ]
  node [
    id 707
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 708
    label "zwyrol"
  ]
  node [
    id 709
    label "czerniak"
  ]
  node [
    id 710
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 711
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 712
    label "paszcza"
  ]
  node [
    id 713
    label "popapraniec"
  ]
  node [
    id 714
    label "skuba&#263;"
  ]
  node [
    id 715
    label "skubanie"
  ]
  node [
    id 716
    label "agresja"
  ]
  node [
    id 717
    label "skubni&#281;cie"
  ]
  node [
    id 718
    label "zwierz&#281;ta"
  ]
  node [
    id 719
    label "fukni&#281;cie"
  ]
  node [
    id 720
    label "farba"
  ]
  node [
    id 721
    label "fukanie"
  ]
  node [
    id 722
    label "gad"
  ]
  node [
    id 723
    label "tresowa&#263;"
  ]
  node [
    id 724
    label "siedzie&#263;"
  ]
  node [
    id 725
    label "oswaja&#263;"
  ]
  node [
    id 726
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 727
    label "poligamia"
  ]
  node [
    id 728
    label "oz&#243;r"
  ]
  node [
    id 729
    label "skubn&#261;&#263;"
  ]
  node [
    id 730
    label "wios&#322;owa&#263;"
  ]
  node [
    id 731
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 732
    label "le&#380;enie"
  ]
  node [
    id 733
    label "niecz&#322;owiek"
  ]
  node [
    id 734
    label "wios&#322;owanie"
  ]
  node [
    id 735
    label "napasienie_si&#281;"
  ]
  node [
    id 736
    label "wiwarium"
  ]
  node [
    id 737
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 738
    label "animalista"
  ]
  node [
    id 739
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 740
    label "hodowla"
  ]
  node [
    id 741
    label "pasienie_si&#281;"
  ]
  node [
    id 742
    label "sodomita"
  ]
  node [
    id 743
    label "monogamia"
  ]
  node [
    id 744
    label "przyssawka"
  ]
  node [
    id 745
    label "zachowanie"
  ]
  node [
    id 746
    label "budowa_cia&#322;a"
  ]
  node [
    id 747
    label "okrutnik"
  ]
  node [
    id 748
    label "grzbiet"
  ]
  node [
    id 749
    label "weterynarz"
  ]
  node [
    id 750
    label "&#322;eb"
  ]
  node [
    id 751
    label "wylinka"
  ]
  node [
    id 752
    label "bestia"
  ]
  node [
    id 753
    label "poskramia&#263;"
  ]
  node [
    id 754
    label "fauna"
  ]
  node [
    id 755
    label "treser"
  ]
  node [
    id 756
    label "siedzenie"
  ]
  node [
    id 757
    label "le&#380;e&#263;"
  ]
  node [
    id 758
    label "ludzko&#347;&#263;"
  ]
  node [
    id 759
    label "asymilowanie"
  ]
  node [
    id 760
    label "wapniak"
  ]
  node [
    id 761
    label "asymilowa&#263;"
  ]
  node [
    id 762
    label "os&#322;abia&#263;"
  ]
  node [
    id 763
    label "hominid"
  ]
  node [
    id 764
    label "podw&#322;adny"
  ]
  node [
    id 765
    label "os&#322;abianie"
  ]
  node [
    id 766
    label "g&#322;owa"
  ]
  node [
    id 767
    label "figura"
  ]
  node [
    id 768
    label "portrecista"
  ]
  node [
    id 769
    label "dwun&#243;g"
  ]
  node [
    id 770
    label "profanum"
  ]
  node [
    id 771
    label "mikrokosmos"
  ]
  node [
    id 772
    label "nasada"
  ]
  node [
    id 773
    label "duch"
  ]
  node [
    id 774
    label "antropochoria"
  ]
  node [
    id 775
    label "osoba"
  ]
  node [
    id 776
    label "wz&#243;r"
  ]
  node [
    id 777
    label "senior"
  ]
  node [
    id 778
    label "Adam"
  ]
  node [
    id 779
    label "homo_sapiens"
  ]
  node [
    id 780
    label "polifag"
  ]
  node [
    id 781
    label "element"
  ]
  node [
    id 782
    label "wykolejeniec"
  ]
  node [
    id 783
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 784
    label "&#380;y&#322;a"
  ]
  node [
    id 785
    label "okrutny"
  ]
  node [
    id 786
    label "pojeb"
  ]
  node [
    id 787
    label "nienormalny"
  ]
  node [
    id 788
    label "szalona_g&#322;owa"
  ]
  node [
    id 789
    label "dziwak"
  ]
  node [
    id 790
    label "sztuka"
  ]
  node [
    id 791
    label "dobi&#263;"
  ]
  node [
    id 792
    label "przer&#380;n&#261;&#263;"
  ]
  node [
    id 793
    label "reakcja"
  ]
  node [
    id 794
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 795
    label "tajemnica"
  ]
  node [
    id 796
    label "pochowanie"
  ]
  node [
    id 797
    label "zdyscyplinowanie"
  ]
  node [
    id 798
    label "post&#261;pienie"
  ]
  node [
    id 799
    label "post"
  ]
  node [
    id 800
    label "bearing"
  ]
  node [
    id 801
    label "behawior"
  ]
  node [
    id 802
    label "observation"
  ]
  node [
    id 803
    label "dieta"
  ]
  node [
    id 804
    label "podtrzymanie"
  ]
  node [
    id 805
    label "etolog"
  ]
  node [
    id 806
    label "przechowanie"
  ]
  node [
    id 807
    label "rapt"
  ]
  node [
    id 808
    label "okres_godowy"
  ]
  node [
    id 809
    label "aggression"
  ]
  node [
    id 810
    label "potop_szwedzki"
  ]
  node [
    id 811
    label "napad"
  ]
  node [
    id 812
    label "odsiedzenie"
  ]
  node [
    id 813
    label "wysiadywanie"
  ]
  node [
    id 814
    label "odsiadywanie"
  ]
  node [
    id 815
    label "otoczenie_si&#281;"
  ]
  node [
    id 816
    label "posiedzenie"
  ]
  node [
    id 817
    label "wysiedzenie"
  ]
  node [
    id 818
    label "posadzenie"
  ]
  node [
    id 819
    label "wychodzenie"
  ]
  node [
    id 820
    label "tkwienie"
  ]
  node [
    id 821
    label "spoczywanie"
  ]
  node [
    id 822
    label "sadzanie"
  ]
  node [
    id 823
    label "trybuna"
  ]
  node [
    id 824
    label "ocieranie_si&#281;"
  ]
  node [
    id 825
    label "room"
  ]
  node [
    id 826
    label "jadalnia"
  ]
  node [
    id 827
    label "residency"
  ]
  node [
    id 828
    label "pupa"
  ]
  node [
    id 829
    label "samolot"
  ]
  node [
    id 830
    label "touch"
  ]
  node [
    id 831
    label "otarcie_si&#281;"
  ]
  node [
    id 832
    label "trwanie"
  ]
  node [
    id 833
    label "position"
  ]
  node [
    id 834
    label "otaczanie_si&#281;"
  ]
  node [
    id 835
    label "wyj&#347;cie"
  ]
  node [
    id 836
    label "przedzia&#322;"
  ]
  node [
    id 837
    label "mieszkanie"
  ]
  node [
    id 838
    label "ujmowanie"
  ]
  node [
    id 839
    label "wstanie"
  ]
  node [
    id 840
    label "autobus"
  ]
  node [
    id 841
    label "pomieszczenie"
  ]
  node [
    id 842
    label "dziobanie"
  ]
  node [
    id 843
    label "zerwanie"
  ]
  node [
    id 844
    label "ukradzenie"
  ]
  node [
    id 845
    label "uszczkni&#281;cie"
  ]
  node [
    id 846
    label "chwycenie"
  ]
  node [
    id 847
    label "zjedzenie"
  ]
  node [
    id 848
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 849
    label "gutsiness"
  ]
  node [
    id 850
    label "urwanie"
  ]
  node [
    id 851
    label "stw&#243;r"
  ]
  node [
    id 852
    label "istota_fantastyczna"
  ]
  node [
    id 853
    label "educate"
  ]
  node [
    id 854
    label "uczy&#263;"
  ]
  node [
    id 855
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 856
    label "doskonali&#263;"
  ]
  node [
    id 857
    label "pojazd"
  ]
  node [
    id 858
    label "snicker"
  ]
  node [
    id 859
    label "brzmienie"
  ]
  node [
    id 860
    label "wydawanie"
  ]
  node [
    id 861
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 862
    label "wi&#281;&#378;"
  ]
  node [
    id 863
    label "polygamy"
  ]
  node [
    id 864
    label "harem"
  ]
  node [
    id 865
    label "akt_p&#322;ciowy"
  ]
  node [
    id 866
    label "mechanika"
  ]
  node [
    id 867
    label "organ"
  ]
  node [
    id 868
    label "kreacja"
  ]
  node [
    id 869
    label "r&#243;w"
  ]
  node [
    id 870
    label "posesja"
  ]
  node [
    id 871
    label "wjazd"
  ]
  node [
    id 872
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 873
    label "praca"
  ]
  node [
    id 874
    label "constitution"
  ]
  node [
    id 875
    label "urwa&#263;"
  ]
  node [
    id 876
    label "zerwa&#263;"
  ]
  node [
    id 877
    label "chwyci&#263;"
  ]
  node [
    id 878
    label "overcharge"
  ]
  node [
    id 879
    label "zje&#347;&#263;"
  ]
  node [
    id 880
    label "ukra&#347;&#263;"
  ]
  node [
    id 881
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 882
    label "pick"
  ]
  node [
    id 883
    label "lecie&#263;"
  ]
  node [
    id 884
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 885
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 886
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 887
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 888
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 889
    label "carry"
  ]
  node [
    id 890
    label "run"
  ]
  node [
    id 891
    label "bie&#380;e&#263;"
  ]
  node [
    id 892
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 893
    label "biega&#263;"
  ]
  node [
    id 894
    label "tent-fly"
  ]
  node [
    id 895
    label "rise"
  ]
  node [
    id 896
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 897
    label "proceed"
  ]
  node [
    id 898
    label "medycyna_weterynaryjna"
  ]
  node [
    id 899
    label "inspekcja_weterynaryjna"
  ]
  node [
    id 900
    label "zootechnik"
  ]
  node [
    id 901
    label "lekarz"
  ]
  node [
    id 902
    label "flop"
  ]
  node [
    id 903
    label "uzda"
  ]
  node [
    id 904
    label "pull"
  ]
  node [
    id 905
    label "je&#347;&#263;"
  ]
  node [
    id 906
    label "nap&#281;dza&#263;"
  ]
  node [
    id 907
    label "spoczywa&#263;"
  ]
  node [
    id 908
    label "lie"
  ]
  node [
    id 909
    label "pokrywa&#263;"
  ]
  node [
    id 910
    label "equate"
  ]
  node [
    id 911
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 912
    label "gr&#243;b"
  ]
  node [
    id 913
    label "pozarzynanie"
  ]
  node [
    id 914
    label "zabicie"
  ]
  node [
    id 915
    label "pobyczenie_si&#281;"
  ]
  node [
    id 916
    label "tarzanie_si&#281;"
  ]
  node [
    id 917
    label "przele&#380;enie"
  ]
  node [
    id 918
    label "odpowiedni"
  ]
  node [
    id 919
    label "zlegni&#281;cie"
  ]
  node [
    id 920
    label "pole&#380;enie"
  ]
  node [
    id 921
    label "zrywanie"
  ]
  node [
    id 922
    label "obgryzanie"
  ]
  node [
    id 923
    label "obgryzienie"
  ]
  node [
    id 924
    label "apprehension"
  ]
  node [
    id 925
    label "jedzenie"
  ]
  node [
    id 926
    label "odzieranie"
  ]
  node [
    id 927
    label "urywanie"
  ]
  node [
    id 928
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 929
    label "oskubanie"
  ]
  node [
    id 930
    label "monogamy"
  ]
  node [
    id 931
    label "sit"
  ]
  node [
    id 932
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 933
    label "tkwi&#263;"
  ]
  node [
    id 934
    label "ptak"
  ]
  node [
    id 935
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 936
    label "przebywa&#263;"
  ]
  node [
    id 937
    label "brood"
  ]
  node [
    id 938
    label "pause"
  ]
  node [
    id 939
    label "garowa&#263;"
  ]
  node [
    id 940
    label "doprowadza&#263;"
  ]
  node [
    id 941
    label "mieszka&#263;"
  ]
  node [
    id 942
    label "dewiant"
  ]
  node [
    id 943
    label "czciciel"
  ]
  node [
    id 944
    label "artysta"
  ]
  node [
    id 945
    label "plastyk"
  ]
  node [
    id 946
    label "wyrywa&#263;"
  ]
  node [
    id 947
    label "pick_at"
  ]
  node [
    id 948
    label "ci&#261;gn&#261;&#263;"
  ]
  node [
    id 949
    label "meet"
  ]
  node [
    id 950
    label "zrywa&#263;"
  ]
  node [
    id 951
    label "zdziera&#263;"
  ]
  node [
    id 952
    label "urywa&#263;"
  ]
  node [
    id 953
    label "oddala&#263;"
  ]
  node [
    id 954
    label "przyzwyczaja&#263;"
  ]
  node [
    id 955
    label "przywi&#261;zywa&#263;"
  ]
  node [
    id 956
    label "familiarize"
  ]
  node [
    id 957
    label "trener"
  ]
  node [
    id 958
    label "uk&#322;ada&#263;"
  ]
  node [
    id 959
    label "nowotw&#243;r_niez&#322;o&#347;liwy"
  ]
  node [
    id 960
    label "sk&#243;ra"
  ]
  node [
    id 961
    label "dermatoza"
  ]
  node [
    id 962
    label "guz_z&#322;o&#347;liwy"
  ]
  node [
    id 963
    label "melanoma"
  ]
  node [
    id 964
    label "zabrzmienie"
  ]
  node [
    id 965
    label "wydanie"
  ]
  node [
    id 966
    label "sniff"
  ]
  node [
    id 967
    label "powios&#322;owanie"
  ]
  node [
    id 968
    label "nap&#281;dzanie"
  ]
  node [
    id 969
    label "rowing"
  ]
  node [
    id 970
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 971
    label "crawl"
  ]
  node [
    id 972
    label "wlanie_si&#281;"
  ]
  node [
    id 973
    label "wpadni&#281;cie"
  ]
  node [
    id 974
    label "sp&#322;yni&#281;cie"
  ]
  node [
    id 975
    label "nadbiegni&#281;cie"
  ]
  node [
    id 976
    label "op&#322;yni&#281;cie"
  ]
  node [
    id 977
    label "przep&#322;ywanie"
  ]
  node [
    id 978
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 979
    label "op&#322;ywanie"
  ]
  node [
    id 980
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 981
    label "nadp&#322;ywanie"
  ]
  node [
    id 982
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 983
    label "pop&#322;yni&#281;cie"
  ]
  node [
    id 984
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 985
    label "zalewanie"
  ]
  node [
    id 986
    label "przesuwanie_si&#281;"
  ]
  node [
    id 987
    label "rozp&#322;yni&#281;cie_si&#281;"
  ]
  node [
    id 988
    label "przyp&#322;ywanie"
  ]
  node [
    id 989
    label "wzbieranie"
  ]
  node [
    id 990
    label "lanie_si&#281;"
  ]
  node [
    id 991
    label "wyp&#322;ywanie"
  ]
  node [
    id 992
    label "nap&#322;ywanie"
  ]
  node [
    id 993
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 994
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 995
    label "wezbranie"
  ]
  node [
    id 996
    label "nadp&#322;yni&#281;cie"
  ]
  node [
    id 997
    label "obfitowanie"
  ]
  node [
    id 998
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 999
    label "pass"
  ]
  node [
    id 1000
    label "sp&#322;ywanie"
  ]
  node [
    id 1001
    label "powstawanie"
  ]
  node [
    id 1002
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1003
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 1004
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 1005
    label "zalanie"
  ]
  node [
    id 1006
    label "odp&#322;ywanie"
  ]
  node [
    id 1007
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1008
    label "rozp&#322;ywanie_si&#281;"
  ]
  node [
    id 1009
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1010
    label "wpadanie"
  ]
  node [
    id 1011
    label "wlewanie_si&#281;"
  ]
  node [
    id 1012
    label "podp&#322;ywanie"
  ]
  node [
    id 1013
    label "flux"
  ]
  node [
    id 1014
    label "wiedza"
  ]
  node [
    id 1015
    label "noosfera"
  ]
  node [
    id 1016
    label "alkohol"
  ]
  node [
    id 1017
    label "umys&#322;"
  ]
  node [
    id 1018
    label "mak&#243;wka"
  ]
  node [
    id 1019
    label "morda"
  ]
  node [
    id 1020
    label "czaszka"
  ]
  node [
    id 1021
    label "dynia"
  ]
  node [
    id 1022
    label "nask&#243;rek"
  ]
  node [
    id 1023
    label "pow&#322;oka"
  ]
  node [
    id 1024
    label "przeobra&#380;anie"
  ]
  node [
    id 1025
    label "pr&#243;szy&#263;"
  ]
  node [
    id 1026
    label "kry&#263;"
  ]
  node [
    id 1027
    label "pr&#243;szenie"
  ]
  node [
    id 1028
    label "podk&#322;ad"
  ]
  node [
    id 1029
    label "blik"
  ]
  node [
    id 1030
    label "kolor"
  ]
  node [
    id 1031
    label "krycie"
  ]
  node [
    id 1032
    label "wypunktowa&#263;"
  ]
  node [
    id 1033
    label "substancja"
  ]
  node [
    id 1034
    label "krew"
  ]
  node [
    id 1035
    label "punktowa&#263;"
  ]
  node [
    id 1036
    label "wyrostek"
  ]
  node [
    id 1037
    label "uchwyt"
  ]
  node [
    id 1038
    label "sucker"
  ]
  node [
    id 1039
    label "Wielka_Racza"
  ]
  node [
    id 1040
    label "&#346;winica"
  ]
  node [
    id 1041
    label "&#346;l&#281;&#380;a"
  ]
  node [
    id 1042
    label "g&#243;ry"
  ]
  node [
    id 1043
    label "Che&#322;miec"
  ]
  node [
    id 1044
    label "wierzcho&#322;"
  ]
  node [
    id 1045
    label "wierzcho&#322;ek"
  ]
  node [
    id 1046
    label "prze&#322;&#281;cz"
  ]
  node [
    id 1047
    label "Radunia"
  ]
  node [
    id 1048
    label "Barania_G&#243;ra"
  ]
  node [
    id 1049
    label "Groniczki"
  ]
  node [
    id 1050
    label "wierch"
  ]
  node [
    id 1051
    label "Czupel"
  ]
  node [
    id 1052
    label "Jaworz"
  ]
  node [
    id 1053
    label "Okr&#261;glica"
  ]
  node [
    id 1054
    label "Walig&#243;ra"
  ]
  node [
    id 1055
    label "Wielka_Sowa"
  ]
  node [
    id 1056
    label "Ko&#322;owr&#243;t"
  ]
  node [
    id 1057
    label "&#321;omnica"
  ]
  node [
    id 1058
    label "szczyt"
  ]
  node [
    id 1059
    label "wzniesienie"
  ]
  node [
    id 1060
    label "Beskid"
  ]
  node [
    id 1061
    label "tu&#322;&#243;w"
  ]
  node [
    id 1062
    label "Wo&#322;ek"
  ]
  node [
    id 1063
    label "k&#322;&#261;b"
  ]
  node [
    id 1064
    label "bark"
  ]
  node [
    id 1065
    label "ty&#322;"
  ]
  node [
    id 1066
    label "Rysianka"
  ]
  node [
    id 1067
    label "Mody&#324;"
  ]
  node [
    id 1068
    label "shoulder"
  ]
  node [
    id 1069
    label "Obidowa"
  ]
  node [
    id 1070
    label "Jaworzyna"
  ]
  node [
    id 1071
    label "Czarna_G&#243;ra"
  ]
  node [
    id 1072
    label "Turbacz"
  ]
  node [
    id 1073
    label "Rudawiec"
  ]
  node [
    id 1074
    label "g&#243;ra"
  ]
  node [
    id 1075
    label "Ja&#322;owiec"
  ]
  node [
    id 1076
    label "Wielki_Chocz"
  ]
  node [
    id 1077
    label "Orlica"
  ]
  node [
    id 1078
    label "&#346;nie&#380;nik"
  ]
  node [
    id 1079
    label "Szrenica"
  ]
  node [
    id 1080
    label "Cubryna"
  ]
  node [
    id 1081
    label "mi&#281;sie&#324;_najszerszy_grzbietu"
  ]
  node [
    id 1082
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1083
    label "mi&#281;sie&#324;_r&#243;wnoleg&#322;oboczny"
  ]
  node [
    id 1084
    label "Wielki_Bukowiec"
  ]
  node [
    id 1085
    label "Magura"
  ]
  node [
    id 1086
    label "karapaks"
  ]
  node [
    id 1087
    label "korona"
  ]
  node [
    id 1088
    label "Lubogoszcz"
  ]
  node [
    id 1089
    label "strona"
  ]
  node [
    id 1090
    label "jama_g&#281;bowa"
  ]
  node [
    id 1091
    label "twarz"
  ]
  node [
    id 1092
    label "liczko"
  ]
  node [
    id 1093
    label "j&#281;zyk"
  ]
  node [
    id 1094
    label "podroby"
  ]
  node [
    id 1095
    label "gady"
  ]
  node [
    id 1096
    label "plugawiec"
  ]
  node [
    id 1097
    label "kloaka"
  ]
  node [
    id 1098
    label "owodniowiec"
  ]
  node [
    id 1099
    label "bazyliszek"
  ]
  node [
    id 1100
    label "zwyrodnialec"
  ]
  node [
    id 1101
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 1102
    label "biom"
  ]
  node [
    id 1103
    label "przyroda"
  ]
  node [
    id 1104
    label "awifauna"
  ]
  node [
    id 1105
    label "ichtiofauna"
  ]
  node [
    id 1106
    label "geosystem"
  ]
  node [
    id 1107
    label "potrzymanie"
  ]
  node [
    id 1108
    label "praca_rolnicza"
  ]
  node [
    id 1109
    label "rolnictwo"
  ]
  node [
    id 1110
    label "pod&#243;j"
  ]
  node [
    id 1111
    label "filiacja"
  ]
  node [
    id 1112
    label "licencjonowanie"
  ]
  node [
    id 1113
    label "opasa&#263;"
  ]
  node [
    id 1114
    label "ch&#243;w"
  ]
  node [
    id 1115
    label "licencja"
  ]
  node [
    id 1116
    label "sokolarnia"
  ]
  node [
    id 1117
    label "potrzyma&#263;"
  ]
  node [
    id 1118
    label "rozp&#322;&#243;d"
  ]
  node [
    id 1119
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1120
    label "wypas"
  ]
  node [
    id 1121
    label "wychowalnia"
  ]
  node [
    id 1122
    label "pstr&#261;garnia"
  ]
  node [
    id 1123
    label "krzy&#380;owanie"
  ]
  node [
    id 1124
    label "licencjonowa&#263;"
  ]
  node [
    id 1125
    label "odch&#243;w"
  ]
  node [
    id 1126
    label "tucz"
  ]
  node [
    id 1127
    label "ud&#243;j"
  ]
  node [
    id 1128
    label "klatka"
  ]
  node [
    id 1129
    label "opasienie"
  ]
  node [
    id 1130
    label "wych&#243;w"
  ]
  node [
    id 1131
    label "obrz&#261;dek"
  ]
  node [
    id 1132
    label "opasanie"
  ]
  node [
    id 1133
    label "polish"
  ]
  node [
    id 1134
    label "akwarium"
  ]
  node [
    id 1135
    label "biotechnika"
  ]
  node [
    id 1136
    label "j&#261;drowce"
  ]
  node [
    id 1137
    label "kr&#243;lestwo"
  ]
  node [
    id 1138
    label "report"
  ]
  node [
    id 1139
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1140
    label "poinformowa&#263;"
  ]
  node [
    id 1141
    label "write"
  ]
  node [
    id 1142
    label "announce"
  ]
  node [
    id 1143
    label "nastawi&#263;"
  ]
  node [
    id 1144
    label "draw"
  ]
  node [
    id 1145
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 1146
    label "incorporate"
  ]
  node [
    id 1147
    label "obejrze&#263;"
  ]
  node [
    id 1148
    label "impersonate"
  ]
  node [
    id 1149
    label "dokoptowa&#263;"
  ]
  node [
    id 1150
    label "uruchomi&#263;"
  ]
  node [
    id 1151
    label "zacz&#261;&#263;"
  ]
  node [
    id 1152
    label "inform"
  ]
  node [
    id 1153
    label "zakomunikowa&#263;"
  ]
  node [
    id 1154
    label "umowa"
  ]
  node [
    id 1155
    label "cover"
  ]
  node [
    id 1156
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1157
    label "Katar"
  ]
  node [
    id 1158
    label "Libia"
  ]
  node [
    id 1159
    label "Gwatemala"
  ]
  node [
    id 1160
    label "Ekwador"
  ]
  node [
    id 1161
    label "Afganistan"
  ]
  node [
    id 1162
    label "Tad&#380;ykistan"
  ]
  node [
    id 1163
    label "Bhutan"
  ]
  node [
    id 1164
    label "Argentyna"
  ]
  node [
    id 1165
    label "D&#380;ibuti"
  ]
  node [
    id 1166
    label "Wenezuela"
  ]
  node [
    id 1167
    label "Gabon"
  ]
  node [
    id 1168
    label "Ukraina"
  ]
  node [
    id 1169
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1170
    label "Rwanda"
  ]
  node [
    id 1171
    label "Liechtenstein"
  ]
  node [
    id 1172
    label "Sri_Lanka"
  ]
  node [
    id 1173
    label "Madagaskar"
  ]
  node [
    id 1174
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1175
    label "Kongo"
  ]
  node [
    id 1176
    label "Tonga"
  ]
  node [
    id 1177
    label "Bangladesz"
  ]
  node [
    id 1178
    label "Kanada"
  ]
  node [
    id 1179
    label "Wehrlen"
  ]
  node [
    id 1180
    label "Algieria"
  ]
  node [
    id 1181
    label "Uganda"
  ]
  node [
    id 1182
    label "Surinam"
  ]
  node [
    id 1183
    label "Sahara_Zachodnia"
  ]
  node [
    id 1184
    label "Chile"
  ]
  node [
    id 1185
    label "W&#281;gry"
  ]
  node [
    id 1186
    label "Birma"
  ]
  node [
    id 1187
    label "Kazachstan"
  ]
  node [
    id 1188
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1189
    label "Armenia"
  ]
  node [
    id 1190
    label "Tuwalu"
  ]
  node [
    id 1191
    label "Timor_Wschodni"
  ]
  node [
    id 1192
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1193
    label "Izrael"
  ]
  node [
    id 1194
    label "Estonia"
  ]
  node [
    id 1195
    label "Komory"
  ]
  node [
    id 1196
    label "Kamerun"
  ]
  node [
    id 1197
    label "Haiti"
  ]
  node [
    id 1198
    label "Belize"
  ]
  node [
    id 1199
    label "Sierra_Leone"
  ]
  node [
    id 1200
    label "Luksemburg"
  ]
  node [
    id 1201
    label "USA"
  ]
  node [
    id 1202
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1203
    label "Barbados"
  ]
  node [
    id 1204
    label "San_Marino"
  ]
  node [
    id 1205
    label "Bu&#322;garia"
  ]
  node [
    id 1206
    label "Indonezja"
  ]
  node [
    id 1207
    label "Wietnam"
  ]
  node [
    id 1208
    label "Malawi"
  ]
  node [
    id 1209
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1210
    label "Francja"
  ]
  node [
    id 1211
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1212
    label "Zambia"
  ]
  node [
    id 1213
    label "Angola"
  ]
  node [
    id 1214
    label "Grenada"
  ]
  node [
    id 1215
    label "Nepal"
  ]
  node [
    id 1216
    label "Panama"
  ]
  node [
    id 1217
    label "Rumunia"
  ]
  node [
    id 1218
    label "Czarnog&#243;ra"
  ]
  node [
    id 1219
    label "Malediwy"
  ]
  node [
    id 1220
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1221
    label "S&#322;owacja"
  ]
  node [
    id 1222
    label "para"
  ]
  node [
    id 1223
    label "Egipt"
  ]
  node [
    id 1224
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1225
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1226
    label "Mozambik"
  ]
  node [
    id 1227
    label "Kolumbia"
  ]
  node [
    id 1228
    label "Laos"
  ]
  node [
    id 1229
    label "Burundi"
  ]
  node [
    id 1230
    label "Suazi"
  ]
  node [
    id 1231
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1232
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1233
    label "Czechy"
  ]
  node [
    id 1234
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1235
    label "Wyspy_Marshalla"
  ]
  node [
    id 1236
    label "Dominika"
  ]
  node [
    id 1237
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1238
    label "Syria"
  ]
  node [
    id 1239
    label "Palau"
  ]
  node [
    id 1240
    label "Gwinea_Bissau"
  ]
  node [
    id 1241
    label "Liberia"
  ]
  node [
    id 1242
    label "Jamajka"
  ]
  node [
    id 1243
    label "Zimbabwe"
  ]
  node [
    id 1244
    label "Polska"
  ]
  node [
    id 1245
    label "Dominikana"
  ]
  node [
    id 1246
    label "Senegal"
  ]
  node [
    id 1247
    label "Togo"
  ]
  node [
    id 1248
    label "Gujana"
  ]
  node [
    id 1249
    label "Gruzja"
  ]
  node [
    id 1250
    label "Albania"
  ]
  node [
    id 1251
    label "Zair"
  ]
  node [
    id 1252
    label "Meksyk"
  ]
  node [
    id 1253
    label "Macedonia"
  ]
  node [
    id 1254
    label "Chorwacja"
  ]
  node [
    id 1255
    label "Kambod&#380;a"
  ]
  node [
    id 1256
    label "Monako"
  ]
  node [
    id 1257
    label "Mauritius"
  ]
  node [
    id 1258
    label "Gwinea"
  ]
  node [
    id 1259
    label "Mali"
  ]
  node [
    id 1260
    label "Nigeria"
  ]
  node [
    id 1261
    label "Kostaryka"
  ]
  node [
    id 1262
    label "Hanower"
  ]
  node [
    id 1263
    label "Paragwaj"
  ]
  node [
    id 1264
    label "W&#322;ochy"
  ]
  node [
    id 1265
    label "Seszele"
  ]
  node [
    id 1266
    label "Wyspy_Salomona"
  ]
  node [
    id 1267
    label "Hiszpania"
  ]
  node [
    id 1268
    label "Boliwia"
  ]
  node [
    id 1269
    label "Kirgistan"
  ]
  node [
    id 1270
    label "Irlandia"
  ]
  node [
    id 1271
    label "Czad"
  ]
  node [
    id 1272
    label "Irak"
  ]
  node [
    id 1273
    label "Lesoto"
  ]
  node [
    id 1274
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1275
    label "Malta"
  ]
  node [
    id 1276
    label "Andora"
  ]
  node [
    id 1277
    label "Chiny"
  ]
  node [
    id 1278
    label "Filipiny"
  ]
  node [
    id 1279
    label "Antarktis"
  ]
  node [
    id 1280
    label "Niemcy"
  ]
  node [
    id 1281
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1282
    label "Pakistan"
  ]
  node [
    id 1283
    label "terytorium"
  ]
  node [
    id 1284
    label "Nikaragua"
  ]
  node [
    id 1285
    label "Brazylia"
  ]
  node [
    id 1286
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1287
    label "Maroko"
  ]
  node [
    id 1288
    label "Portugalia"
  ]
  node [
    id 1289
    label "Niger"
  ]
  node [
    id 1290
    label "Kenia"
  ]
  node [
    id 1291
    label "Botswana"
  ]
  node [
    id 1292
    label "Fid&#380;i"
  ]
  node [
    id 1293
    label "Tunezja"
  ]
  node [
    id 1294
    label "Australia"
  ]
  node [
    id 1295
    label "Tajlandia"
  ]
  node [
    id 1296
    label "Burkina_Faso"
  ]
  node [
    id 1297
    label "interior"
  ]
  node [
    id 1298
    label "Tanzania"
  ]
  node [
    id 1299
    label "Benin"
  ]
  node [
    id 1300
    label "Indie"
  ]
  node [
    id 1301
    label "&#321;otwa"
  ]
  node [
    id 1302
    label "Kiribati"
  ]
  node [
    id 1303
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1304
    label "Rodezja"
  ]
  node [
    id 1305
    label "Cypr"
  ]
  node [
    id 1306
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1307
    label "Peru"
  ]
  node [
    id 1308
    label "Austria"
  ]
  node [
    id 1309
    label "Urugwaj"
  ]
  node [
    id 1310
    label "Jordania"
  ]
  node [
    id 1311
    label "Grecja"
  ]
  node [
    id 1312
    label "Azerbejd&#380;an"
  ]
  node [
    id 1313
    label "Turcja"
  ]
  node [
    id 1314
    label "Samoa"
  ]
  node [
    id 1315
    label "Sudan"
  ]
  node [
    id 1316
    label "Oman"
  ]
  node [
    id 1317
    label "ziemia"
  ]
  node [
    id 1318
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1319
    label "Uzbekistan"
  ]
  node [
    id 1320
    label "Portoryko"
  ]
  node [
    id 1321
    label "Honduras"
  ]
  node [
    id 1322
    label "Mongolia"
  ]
  node [
    id 1323
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1324
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1325
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1326
    label "Serbia"
  ]
  node [
    id 1327
    label "Tajwan"
  ]
  node [
    id 1328
    label "Wielka_Brytania"
  ]
  node [
    id 1329
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1330
    label "Liban"
  ]
  node [
    id 1331
    label "Japonia"
  ]
  node [
    id 1332
    label "Ghana"
  ]
  node [
    id 1333
    label "Belgia"
  ]
  node [
    id 1334
    label "Bahrajn"
  ]
  node [
    id 1335
    label "Mikronezja"
  ]
  node [
    id 1336
    label "Etiopia"
  ]
  node [
    id 1337
    label "Kuwejt"
  ]
  node [
    id 1338
    label "Bahamy"
  ]
  node [
    id 1339
    label "Rosja"
  ]
  node [
    id 1340
    label "Mo&#322;dawia"
  ]
  node [
    id 1341
    label "Litwa"
  ]
  node [
    id 1342
    label "S&#322;owenia"
  ]
  node [
    id 1343
    label "Szwajcaria"
  ]
  node [
    id 1344
    label "Erytrea"
  ]
  node [
    id 1345
    label "Arabia_Saudyjska"
  ]
  node [
    id 1346
    label "Kuba"
  ]
  node [
    id 1347
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1348
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1349
    label "Malezja"
  ]
  node [
    id 1350
    label "Korea"
  ]
  node [
    id 1351
    label "Jemen"
  ]
  node [
    id 1352
    label "Nowa_Zelandia"
  ]
  node [
    id 1353
    label "Namibia"
  ]
  node [
    id 1354
    label "Nauru"
  ]
  node [
    id 1355
    label "holoarktyka"
  ]
  node [
    id 1356
    label "Brunei"
  ]
  node [
    id 1357
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1358
    label "Khitai"
  ]
  node [
    id 1359
    label "Mauretania"
  ]
  node [
    id 1360
    label "Iran"
  ]
  node [
    id 1361
    label "Gambia"
  ]
  node [
    id 1362
    label "Somalia"
  ]
  node [
    id 1363
    label "Holandia"
  ]
  node [
    id 1364
    label "Turkmenistan"
  ]
  node [
    id 1365
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1366
    label "Salwador"
  ]
  node [
    id 1367
    label "pair"
  ]
  node [
    id 1368
    label "odparowywanie"
  ]
  node [
    id 1369
    label "gaz_cieplarniany"
  ]
  node [
    id 1370
    label "chodzi&#263;"
  ]
  node [
    id 1371
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 1372
    label "poker"
  ]
  node [
    id 1373
    label "moneta"
  ]
  node [
    id 1374
    label "parowanie"
  ]
  node [
    id 1375
    label "damp"
  ]
  node [
    id 1376
    label "nale&#380;e&#263;"
  ]
  node [
    id 1377
    label "odparowanie"
  ]
  node [
    id 1378
    label "odparowa&#263;"
  ]
  node [
    id 1379
    label "dodatek"
  ]
  node [
    id 1380
    label "jednostka_monetarna"
  ]
  node [
    id 1381
    label "smoke"
  ]
  node [
    id 1382
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 1383
    label "odparowywa&#263;"
  ]
  node [
    id 1384
    label "uk&#322;ad"
  ]
  node [
    id 1385
    label "gaz"
  ]
  node [
    id 1386
    label "wyparowanie"
  ]
  node [
    id 1387
    label "obszar"
  ]
  node [
    id 1388
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1389
    label "jednostka_administracyjna"
  ]
  node [
    id 1390
    label "Jukon"
  ]
  node [
    id 1391
    label "podmiot"
  ]
  node [
    id 1392
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1393
    label "TOPR"
  ]
  node [
    id 1394
    label "endecki"
  ]
  node [
    id 1395
    label "od&#322;am"
  ]
  node [
    id 1396
    label "przedstawicielstwo"
  ]
  node [
    id 1397
    label "Cepelia"
  ]
  node [
    id 1398
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1399
    label "ZBoWiD"
  ]
  node [
    id 1400
    label "organization"
  ]
  node [
    id 1401
    label "centrala"
  ]
  node [
    id 1402
    label "GOPR"
  ]
  node [
    id 1403
    label "ZOMO"
  ]
  node [
    id 1404
    label "ZMP"
  ]
  node [
    id 1405
    label "komitet_koordynacyjny"
  ]
  node [
    id 1406
    label "przybud&#243;wka"
  ]
  node [
    id 1407
    label "boj&#243;wka"
  ]
  node [
    id 1408
    label "turn"
  ]
  node [
    id 1409
    label "turning"
  ]
  node [
    id 1410
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1411
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1412
    label "skr&#281;t"
  ]
  node [
    id 1413
    label "obr&#243;t"
  ]
  node [
    id 1414
    label "fraza_czasownikowa"
  ]
  node [
    id 1415
    label "jednostka_leksykalna"
  ]
  node [
    id 1416
    label "zmiana"
  ]
  node [
    id 1417
    label "wyra&#380;enie"
  ]
  node [
    id 1418
    label "odm&#322;adzanie"
  ]
  node [
    id 1419
    label "liga"
  ]
  node [
    id 1420
    label "jednostka_systematyczna"
  ]
  node [
    id 1421
    label "gromada"
  ]
  node [
    id 1422
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 1423
    label "Entuzjastki"
  ]
  node [
    id 1424
    label "kompozycja"
  ]
  node [
    id 1425
    label "Terranie"
  ]
  node [
    id 1426
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1427
    label "category"
  ]
  node [
    id 1428
    label "pakiet_klimatyczny"
  ]
  node [
    id 1429
    label "oddzia&#322;"
  ]
  node [
    id 1430
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1431
    label "cz&#261;steczka"
  ]
  node [
    id 1432
    label "stage_set"
  ]
  node [
    id 1433
    label "type"
  ]
  node [
    id 1434
    label "specgrupa"
  ]
  node [
    id 1435
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1436
    label "&#346;wietliki"
  ]
  node [
    id 1437
    label "odm&#322;odzenie"
  ]
  node [
    id 1438
    label "Eurogrupa"
  ]
  node [
    id 1439
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1440
    label "formacja_geologiczna"
  ]
  node [
    id 1441
    label "harcerze_starsi"
  ]
  node [
    id 1442
    label "wybranek"
  ]
  node [
    id 1443
    label "package"
  ]
  node [
    id 1444
    label "gra"
  ]
  node [
    id 1445
    label "game"
  ]
  node [
    id 1446
    label "materia&#322;"
  ]
  node [
    id 1447
    label "niedoczas"
  ]
  node [
    id 1448
    label "aktyw"
  ]
  node [
    id 1449
    label "wybranka"
  ]
  node [
    id 1450
    label "szata_ro&#347;linna"
  ]
  node [
    id 1451
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 1452
    label "formacja_ro&#347;linna"
  ]
  node [
    id 1453
    label "zielono&#347;&#263;"
  ]
  node [
    id 1454
    label "pi&#281;tro"
  ]
  node [
    id 1455
    label "plant"
  ]
  node [
    id 1456
    label "ro&#347;lina"
  ]
  node [
    id 1457
    label "teren"
  ]
  node [
    id 1458
    label "Kaszmir"
  ]
  node [
    id 1459
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1460
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 1461
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1462
    label "Pend&#380;ab"
  ]
  node [
    id 1463
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 1464
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 1465
    label "funt_liba&#324;ski"
  ]
  node [
    id 1466
    label "strefa_euro"
  ]
  node [
    id 1467
    label "Pozna&#324;"
  ]
  node [
    id 1468
    label "lira_malta&#324;ska"
  ]
  node [
    id 1469
    label "Gozo"
  ]
  node [
    id 1470
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1471
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 1472
    label "Afryka_Zachodnia"
  ]
  node [
    id 1473
    label "Afryka_Wschodnia"
  ]
  node [
    id 1474
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1475
    label "dolar_namibijski"
  ]
  node [
    id 1476
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 1477
    label "milrejs"
  ]
  node [
    id 1478
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1479
    label "NATO"
  ]
  node [
    id 1480
    label "escudo_portugalskie"
  ]
  node [
    id 1481
    label "dolar_bahamski"
  ]
  node [
    id 1482
    label "Wielka_Bahama"
  ]
  node [
    id 1483
    label "Karaiby"
  ]
  node [
    id 1484
    label "dolar_liberyjski"
  ]
  node [
    id 1485
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1486
    label "riel"
  ]
  node [
    id 1487
    label "Karelia"
  ]
  node [
    id 1488
    label "Mari_El"
  ]
  node [
    id 1489
    label "Inguszetia"
  ]
  node [
    id 1490
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1491
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1492
    label "Udmurcja"
  ]
  node [
    id 1493
    label "Newa"
  ]
  node [
    id 1494
    label "&#321;adoga"
  ]
  node [
    id 1495
    label "Czeczenia"
  ]
  node [
    id 1496
    label "Anadyr"
  ]
  node [
    id 1497
    label "Syberia"
  ]
  node [
    id 1498
    label "Tatarstan"
  ]
  node [
    id 1499
    label "Wszechrosja"
  ]
  node [
    id 1500
    label "Azja"
  ]
  node [
    id 1501
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1502
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 1503
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1504
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1505
    label "Europa_Wschodnia"
  ]
  node [
    id 1506
    label "Witim"
  ]
  node [
    id 1507
    label "Kamczatka"
  ]
  node [
    id 1508
    label "Jama&#322;"
  ]
  node [
    id 1509
    label "Dagestan"
  ]
  node [
    id 1510
    label "Baszkiria"
  ]
  node [
    id 1511
    label "Tuwa"
  ]
  node [
    id 1512
    label "car"
  ]
  node [
    id 1513
    label "Komi"
  ]
  node [
    id 1514
    label "Czuwaszja"
  ]
  node [
    id 1515
    label "Chakasja"
  ]
  node [
    id 1516
    label "Perm"
  ]
  node [
    id 1517
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1518
    label "Ajon"
  ]
  node [
    id 1519
    label "Adygeja"
  ]
  node [
    id 1520
    label "Dniepr"
  ]
  node [
    id 1521
    label "rubel_rosyjski"
  ]
  node [
    id 1522
    label "Don"
  ]
  node [
    id 1523
    label "Mordowia"
  ]
  node [
    id 1524
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1525
    label "lew"
  ]
  node [
    id 1526
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1527
    label "Dobrud&#380;a"
  ]
  node [
    id 1528
    label "Unia_Europejska"
  ]
  node [
    id 1529
    label "lira_izraelska"
  ]
  node [
    id 1530
    label "szekel"
  ]
  node [
    id 1531
    label "Galilea"
  ]
  node [
    id 1532
    label "Judea"
  ]
  node [
    id 1533
    label "Luksemburgia"
  ]
  node [
    id 1534
    label "frank_belgijski"
  ]
  node [
    id 1535
    label "Limburgia"
  ]
  node [
    id 1536
    label "Brabancja"
  ]
  node [
    id 1537
    label "Walonia"
  ]
  node [
    id 1538
    label "Flandria"
  ]
  node [
    id 1539
    label "Niderlandy"
  ]
  node [
    id 1540
    label "dinar_iracki"
  ]
  node [
    id 1541
    label "Maghreb"
  ]
  node [
    id 1542
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 1543
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1544
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1545
    label "szyling_ugandyjski"
  ]
  node [
    id 1546
    label "kafar"
  ]
  node [
    id 1547
    label "dolar_jamajski"
  ]
  node [
    id 1548
    label "ringgit"
  ]
  node [
    id 1549
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1550
    label "Borneo"
  ]
  node [
    id 1551
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1552
    label "dolar_surinamski"
  ]
  node [
    id 1553
    label "funt_suda&#324;ski"
  ]
  node [
    id 1554
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1555
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1556
    label "Manica"
  ]
  node [
    id 1557
    label "escudo_mozambickie"
  ]
  node [
    id 1558
    label "Cabo_Delgado"
  ]
  node [
    id 1559
    label "Inhambane"
  ]
  node [
    id 1560
    label "Maputo"
  ]
  node [
    id 1561
    label "Gaza"
  ]
  node [
    id 1562
    label "Niasa"
  ]
  node [
    id 1563
    label "Nampula"
  ]
  node [
    id 1564
    label "metical"
  ]
  node [
    id 1565
    label "Sahara"
  ]
  node [
    id 1566
    label "inti"
  ]
  node [
    id 1567
    label "sol"
  ]
  node [
    id 1568
    label "kip"
  ]
  node [
    id 1569
    label "Pireneje"
  ]
  node [
    id 1570
    label "euro"
  ]
  node [
    id 1571
    label "kwacha_zambijska"
  ]
  node [
    id 1572
    label "tugrik"
  ]
  node [
    id 1573
    label "Azja_Wschodnia"
  ]
  node [
    id 1574
    label "Buriaci"
  ]
  node [
    id 1575
    label "ajmak"
  ]
  node [
    id 1576
    label "balboa"
  ]
  node [
    id 1577
    label "Ameryka_Centralna"
  ]
  node [
    id 1578
    label "dolar"
  ]
  node [
    id 1579
    label "gulden"
  ]
  node [
    id 1580
    label "Zelandia"
  ]
  node [
    id 1581
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1582
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1583
    label "Polinezja"
  ]
  node [
    id 1584
    label "dolar_Tuvalu"
  ]
  node [
    id 1585
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1586
    label "zair"
  ]
  node [
    id 1587
    label "Katanga"
  ]
  node [
    id 1588
    label "Europa_Zachodnia"
  ]
  node [
    id 1589
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1590
    label "frank_szwajcarski"
  ]
  node [
    id 1591
    label "Jukatan"
  ]
  node [
    id 1592
    label "dolar_Belize"
  ]
  node [
    id 1593
    label "colon"
  ]
  node [
    id 1594
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1595
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1596
    label "Dyja"
  ]
  node [
    id 1597
    label "korona_czeska"
  ]
  node [
    id 1598
    label "Izera"
  ]
  node [
    id 1599
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1600
    label "Lasko"
  ]
  node [
    id 1601
    label "ugija"
  ]
  node [
    id 1602
    label "szyling_kenijski"
  ]
  node [
    id 1603
    label "Nachiczewan"
  ]
  node [
    id 1604
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1605
    label "manat_azerski"
  ]
  node [
    id 1606
    label "Karabach"
  ]
  node [
    id 1607
    label "Bengal"
  ]
  node [
    id 1608
    label "taka"
  ]
  node [
    id 1609
    label "Ocean_Spokojny"
  ]
  node [
    id 1610
    label "dolar_Kiribati"
  ]
  node [
    id 1611
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1612
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1613
    label "Cebu"
  ]
  node [
    id 1614
    label "Atlantyk"
  ]
  node [
    id 1615
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1616
    label "Ulster"
  ]
  node [
    id 1617
    label "funt_irlandzki"
  ]
  node [
    id 1618
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1619
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1620
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1621
    label "cedi"
  ]
  node [
    id 1622
    label "ariary"
  ]
  node [
    id 1623
    label "Ocean_Indyjski"
  ]
  node [
    id 1624
    label "frank_malgaski"
  ]
  node [
    id 1625
    label "Andaluzja"
  ]
  node [
    id 1626
    label "Estremadura"
  ]
  node [
    id 1627
    label "Kastylia"
  ]
  node [
    id 1628
    label "Galicja"
  ]
  node [
    id 1629
    label "Rzym_Zachodni"
  ]
  node [
    id 1630
    label "Aragonia"
  ]
  node [
    id 1631
    label "hacjender"
  ]
  node [
    id 1632
    label "Asturia"
  ]
  node [
    id 1633
    label "Baskonia"
  ]
  node [
    id 1634
    label "Majorka"
  ]
  node [
    id 1635
    label "Walencja"
  ]
  node [
    id 1636
    label "peseta"
  ]
  node [
    id 1637
    label "Katalonia"
  ]
  node [
    id 1638
    label "peso_chilijskie"
  ]
  node [
    id 1639
    label "Indie_Zachodnie"
  ]
  node [
    id 1640
    label "Sikkim"
  ]
  node [
    id 1641
    label "Asam"
  ]
  node [
    id 1642
    label "rupia_indyjska"
  ]
  node [
    id 1643
    label "Indie_Portugalskie"
  ]
  node [
    id 1644
    label "Indie_Wschodnie"
  ]
  node [
    id 1645
    label "Kerala"
  ]
  node [
    id 1646
    label "Bollywood"
  ]
  node [
    id 1647
    label "jen"
  ]
  node [
    id 1648
    label "jinja"
  ]
  node [
    id 1649
    label "Okinawa"
  ]
  node [
    id 1650
    label "Japonica"
  ]
  node [
    id 1651
    label "Rugia"
  ]
  node [
    id 1652
    label "Saksonia"
  ]
  node [
    id 1653
    label "Dolna_Saksonia"
  ]
  node [
    id 1654
    label "Anglosas"
  ]
  node [
    id 1655
    label "Hesja"
  ]
  node [
    id 1656
    label "Szlezwik"
  ]
  node [
    id 1657
    label "Wirtembergia"
  ]
  node [
    id 1658
    label "Po&#322;abie"
  ]
  node [
    id 1659
    label "Germania"
  ]
  node [
    id 1660
    label "Frankonia"
  ]
  node [
    id 1661
    label "Badenia"
  ]
  node [
    id 1662
    label "Holsztyn"
  ]
  node [
    id 1663
    label "Bawaria"
  ]
  node [
    id 1664
    label "marka"
  ]
  node [
    id 1665
    label "Brandenburgia"
  ]
  node [
    id 1666
    label "Szwabia"
  ]
  node [
    id 1667
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1668
    label "Nadrenia"
  ]
  node [
    id 1669
    label "Westfalia"
  ]
  node [
    id 1670
    label "Turyngia"
  ]
  node [
    id 1671
    label "Helgoland"
  ]
  node [
    id 1672
    label "Karlsbad"
  ]
  node [
    id 1673
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1674
    label "Piemont"
  ]
  node [
    id 1675
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1676
    label "Italia"
  ]
  node [
    id 1677
    label "Kalabria"
  ]
  node [
    id 1678
    label "Sardynia"
  ]
  node [
    id 1679
    label "Apulia"
  ]
  node [
    id 1680
    label "Ok&#281;cie"
  ]
  node [
    id 1681
    label "Karyntia"
  ]
  node [
    id 1682
    label "Umbria"
  ]
  node [
    id 1683
    label "Romania"
  ]
  node [
    id 1684
    label "Sycylia"
  ]
  node [
    id 1685
    label "Warszawa"
  ]
  node [
    id 1686
    label "lir"
  ]
  node [
    id 1687
    label "Toskania"
  ]
  node [
    id 1688
    label "Lombardia"
  ]
  node [
    id 1689
    label "Liguria"
  ]
  node [
    id 1690
    label "Kampania"
  ]
  node [
    id 1691
    label "Dacja"
  ]
  node [
    id 1692
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1693
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1694
    label "Ba&#322;kany"
  ]
  node [
    id 1695
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1696
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1697
    label "funt_syryjski"
  ]
  node [
    id 1698
    label "alawizm"
  ]
  node [
    id 1699
    label "frank_rwandyjski"
  ]
  node [
    id 1700
    label "dinar_Bahrajnu"
  ]
  node [
    id 1701
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1702
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1703
    label "frank_luksemburski"
  ]
  node [
    id 1704
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1705
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1706
    label "frank_monakijski"
  ]
  node [
    id 1707
    label "dinar_algierski"
  ]
  node [
    id 1708
    label "Kabylia"
  ]
  node [
    id 1709
    label "Oceania"
  ]
  node [
    id 1710
    label "Wojwodina"
  ]
  node [
    id 1711
    label "Sand&#380;ak"
  ]
  node [
    id 1712
    label "dinar_serbski"
  ]
  node [
    id 1713
    label "boliwar"
  ]
  node [
    id 1714
    label "Orinoko"
  ]
  node [
    id 1715
    label "tenge"
  ]
  node [
    id 1716
    label "lek"
  ]
  node [
    id 1717
    label "frank_alba&#324;ski"
  ]
  node [
    id 1718
    label "dolar_Barbadosu"
  ]
  node [
    id 1719
    label "Antyle"
  ]
  node [
    id 1720
    label "kyat"
  ]
  node [
    id 1721
    label "Arakan"
  ]
  node [
    id 1722
    label "c&#243;rdoba"
  ]
  node [
    id 1723
    label "Paros"
  ]
  node [
    id 1724
    label "Epir"
  ]
  node [
    id 1725
    label "panhellenizm"
  ]
  node [
    id 1726
    label "Eubea"
  ]
  node [
    id 1727
    label "Rodos"
  ]
  node [
    id 1728
    label "Achaja"
  ]
  node [
    id 1729
    label "Termopile"
  ]
  node [
    id 1730
    label "Attyka"
  ]
  node [
    id 1731
    label "Hellada"
  ]
  node [
    id 1732
    label "Etolia"
  ]
  node [
    id 1733
    label "palestra"
  ]
  node [
    id 1734
    label "Kreta"
  ]
  node [
    id 1735
    label "drachma"
  ]
  node [
    id 1736
    label "Olimp"
  ]
  node [
    id 1737
    label "Tesalia"
  ]
  node [
    id 1738
    label "Peloponez"
  ]
  node [
    id 1739
    label "Eolia"
  ]
  node [
    id 1740
    label "Beocja"
  ]
  node [
    id 1741
    label "Parnas"
  ]
  node [
    id 1742
    label "Lesbos"
  ]
  node [
    id 1743
    label "Mariany"
  ]
  node [
    id 1744
    label "Salzburg"
  ]
  node [
    id 1745
    label "Rakuzy"
  ]
  node [
    id 1746
    label "Tyrol"
  ]
  node [
    id 1747
    label "konsulent"
  ]
  node [
    id 1748
    label "szyling_austryjacki"
  ]
  node [
    id 1749
    label "Amhara"
  ]
  node [
    id 1750
    label "birr"
  ]
  node [
    id 1751
    label "Syjon"
  ]
  node [
    id 1752
    label "negus"
  ]
  node [
    id 1753
    label "Jawa"
  ]
  node [
    id 1754
    label "Sumatra"
  ]
  node [
    id 1755
    label "rupia_indonezyjska"
  ]
  node [
    id 1756
    label "Nowa_Gwinea"
  ]
  node [
    id 1757
    label "Moluki"
  ]
  node [
    id 1758
    label "boliviano"
  ]
  node [
    id 1759
    label "Lotaryngia"
  ]
  node [
    id 1760
    label "Bordeaux"
  ]
  node [
    id 1761
    label "Pikardia"
  ]
  node [
    id 1762
    label "Alzacja"
  ]
  node [
    id 1763
    label "Masyw_Centralny"
  ]
  node [
    id 1764
    label "Akwitania"
  ]
  node [
    id 1765
    label "Sekwana"
  ]
  node [
    id 1766
    label "Langwedocja"
  ]
  node [
    id 1767
    label "Armagnac"
  ]
  node [
    id 1768
    label "Martynika"
  ]
  node [
    id 1769
    label "Bretania"
  ]
  node [
    id 1770
    label "Sabaudia"
  ]
  node [
    id 1771
    label "Korsyka"
  ]
  node [
    id 1772
    label "Normandia"
  ]
  node [
    id 1773
    label "Gaskonia"
  ]
  node [
    id 1774
    label "Burgundia"
  ]
  node [
    id 1775
    label "frank_francuski"
  ]
  node [
    id 1776
    label "Wandea"
  ]
  node [
    id 1777
    label "Prowansja"
  ]
  node [
    id 1778
    label "Gwadelupa"
  ]
  node [
    id 1779
    label "somoni"
  ]
  node [
    id 1780
    label "Melanezja"
  ]
  node [
    id 1781
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1782
    label "funt_cypryjski"
  ]
  node [
    id 1783
    label "Afrodyzje"
  ]
  node [
    id 1784
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1785
    label "Fryburg"
  ]
  node [
    id 1786
    label "Bazylea"
  ]
  node [
    id 1787
    label "Alpy"
  ]
  node [
    id 1788
    label "Helwecja"
  ]
  node [
    id 1789
    label "Berno"
  ]
  node [
    id 1790
    label "sum"
  ]
  node [
    id 1791
    label "Karaka&#322;pacja"
  ]
  node [
    id 1792
    label "Kurlandia"
  ]
  node [
    id 1793
    label "Windawa"
  ]
  node [
    id 1794
    label "&#322;at"
  ]
  node [
    id 1795
    label "Liwonia"
  ]
  node [
    id 1796
    label "rubel_&#322;otewski"
  ]
  node [
    id 1797
    label "Inflanty"
  ]
  node [
    id 1798
    label "&#379;mud&#378;"
  ]
  node [
    id 1799
    label "lit"
  ]
  node [
    id 1800
    label "frank_tunezyjski"
  ]
  node [
    id 1801
    label "dinar_tunezyjski"
  ]
  node [
    id 1802
    label "lempira"
  ]
  node [
    id 1803
    label "korona_w&#281;gierska"
  ]
  node [
    id 1804
    label "forint"
  ]
  node [
    id 1805
    label "Lipt&#243;w"
  ]
  node [
    id 1806
    label "dong"
  ]
  node [
    id 1807
    label "Annam"
  ]
  node [
    id 1808
    label "Tonkin"
  ]
  node [
    id 1809
    label "lud"
  ]
  node [
    id 1810
    label "frank_kongijski"
  ]
  node [
    id 1811
    label "szyling_somalijski"
  ]
  node [
    id 1812
    label "cruzado"
  ]
  node [
    id 1813
    label "real"
  ]
  node [
    id 1814
    label "Podole"
  ]
  node [
    id 1815
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1816
    label "Wsch&#243;d"
  ]
  node [
    id 1817
    label "Zakarpacie"
  ]
  node [
    id 1818
    label "Naddnieprze"
  ]
  node [
    id 1819
    label "Ma&#322;orosja"
  ]
  node [
    id 1820
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1821
    label "Nadbu&#380;e"
  ]
  node [
    id 1822
    label "hrywna"
  ]
  node [
    id 1823
    label "Zaporo&#380;e"
  ]
  node [
    id 1824
    label "Krym"
  ]
  node [
    id 1825
    label "Dniestr"
  ]
  node [
    id 1826
    label "Przykarpacie"
  ]
  node [
    id 1827
    label "Kozaczyzna"
  ]
  node [
    id 1828
    label "karbowaniec"
  ]
  node [
    id 1829
    label "Tasmania"
  ]
  node [
    id 1830
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1831
    label "dolar_australijski"
  ]
  node [
    id 1832
    label "gourde"
  ]
  node [
    id 1833
    label "escudo_angolskie"
  ]
  node [
    id 1834
    label "kwanza"
  ]
  node [
    id 1835
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1836
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1837
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1838
    label "Ad&#380;aria"
  ]
  node [
    id 1839
    label "lari"
  ]
  node [
    id 1840
    label "naira"
  ]
  node [
    id 1841
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1842
    label "Ohio"
  ]
  node [
    id 1843
    label "P&#243;&#322;noc"
  ]
  node [
    id 1844
    label "Nowy_York"
  ]
  node [
    id 1845
    label "Illinois"
  ]
  node [
    id 1846
    label "Po&#322;udnie"
  ]
  node [
    id 1847
    label "Kalifornia"
  ]
  node [
    id 1848
    label "Wirginia"
  ]
  node [
    id 1849
    label "Teksas"
  ]
  node [
    id 1850
    label "Waszyngton"
  ]
  node [
    id 1851
    label "zielona_karta"
  ]
  node [
    id 1852
    label "Alaska"
  ]
  node [
    id 1853
    label "Massachusetts"
  ]
  node [
    id 1854
    label "Hawaje"
  ]
  node [
    id 1855
    label "Maryland"
  ]
  node [
    id 1856
    label "Michigan"
  ]
  node [
    id 1857
    label "Arizona"
  ]
  node [
    id 1858
    label "Georgia"
  ]
  node [
    id 1859
    label "stan_wolny"
  ]
  node [
    id 1860
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1861
    label "Pensylwania"
  ]
  node [
    id 1862
    label "Luizjana"
  ]
  node [
    id 1863
    label "Nowy_Meksyk"
  ]
  node [
    id 1864
    label "Wuj_Sam"
  ]
  node [
    id 1865
    label "Alabama"
  ]
  node [
    id 1866
    label "Kansas"
  ]
  node [
    id 1867
    label "Oregon"
  ]
  node [
    id 1868
    label "Zach&#243;d"
  ]
  node [
    id 1869
    label "Oklahoma"
  ]
  node [
    id 1870
    label "Floryda"
  ]
  node [
    id 1871
    label "Hudson"
  ]
  node [
    id 1872
    label "som"
  ]
  node [
    id 1873
    label "peso_urugwajskie"
  ]
  node [
    id 1874
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1875
    label "dolar_Brunei"
  ]
  node [
    id 1876
    label "rial_ira&#324;ski"
  ]
  node [
    id 1877
    label "mu&#322;&#322;a"
  ]
  node [
    id 1878
    label "Persja"
  ]
  node [
    id 1879
    label "d&#380;amahirijja"
  ]
  node [
    id 1880
    label "dinar_libijski"
  ]
  node [
    id 1881
    label "nakfa"
  ]
  node [
    id 1882
    label "rial_katarski"
  ]
  node [
    id 1883
    label "quetzal"
  ]
  node [
    id 1884
    label "won"
  ]
  node [
    id 1885
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1886
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1887
    label "guarani"
  ]
  node [
    id 1888
    label "perper"
  ]
  node [
    id 1889
    label "dinar_kuwejcki"
  ]
  node [
    id 1890
    label "dalasi"
  ]
  node [
    id 1891
    label "dolar_Zimbabwe"
  ]
  node [
    id 1892
    label "Szantung"
  ]
  node [
    id 1893
    label "Chiny_Zachodnie"
  ]
  node [
    id 1894
    label "Kuantung"
  ]
  node [
    id 1895
    label "D&#380;ungaria"
  ]
  node [
    id 1896
    label "yuan"
  ]
  node [
    id 1897
    label "Hongkong"
  ]
  node [
    id 1898
    label "Chiny_Wschodnie"
  ]
  node [
    id 1899
    label "Guangdong"
  ]
  node [
    id 1900
    label "Junnan"
  ]
  node [
    id 1901
    label "Mand&#380;uria"
  ]
  node [
    id 1902
    label "Syczuan"
  ]
  node [
    id 1903
    label "Pa&#322;uki"
  ]
  node [
    id 1904
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1905
    label "Powi&#347;le"
  ]
  node [
    id 1906
    label "Wolin"
  ]
  node [
    id 1907
    label "z&#322;oty"
  ]
  node [
    id 1908
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1909
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1910
    label "So&#322;a"
  ]
  node [
    id 1911
    label "Krajna"
  ]
  node [
    id 1912
    label "Opolskie"
  ]
  node [
    id 1913
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1914
    label "Suwalszczyzna"
  ]
  node [
    id 1915
    label "barwy_polskie"
  ]
  node [
    id 1916
    label "Podlasie"
  ]
  node [
    id 1917
    label "Ma&#322;opolska"
  ]
  node [
    id 1918
    label "Warmia"
  ]
  node [
    id 1919
    label "Mazury"
  ]
  node [
    id 1920
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1921
    label "Kaczawa"
  ]
  node [
    id 1922
    label "Lubelszczyzna"
  ]
  node [
    id 1923
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1924
    label "Kielecczyzna"
  ]
  node [
    id 1925
    label "Lubuskie"
  ]
  node [
    id 1926
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1927
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1928
    label "Kujawy"
  ]
  node [
    id 1929
    label "Podkarpacie"
  ]
  node [
    id 1930
    label "Wielkopolska"
  ]
  node [
    id 1931
    label "Wis&#322;a"
  ]
  node [
    id 1932
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1933
    label "Bory_Tucholskie"
  ]
  node [
    id 1934
    label "Ujgur"
  ]
  node [
    id 1935
    label "Azja_Mniejsza"
  ]
  node [
    id 1936
    label "lira_turecka"
  ]
  node [
    id 1937
    label "kuna"
  ]
  node [
    id 1938
    label "dram"
  ]
  node [
    id 1939
    label "tala"
  ]
  node [
    id 1940
    label "korona_s&#322;owacka"
  ]
  node [
    id 1941
    label "Turiec"
  ]
  node [
    id 1942
    label "Himalaje"
  ]
  node [
    id 1943
    label "rupia_nepalska"
  ]
  node [
    id 1944
    label "frank_gwinejski"
  ]
  node [
    id 1945
    label "korona_esto&#324;ska"
  ]
  node [
    id 1946
    label "Skandynawia"
  ]
  node [
    id 1947
    label "marka_esto&#324;ska"
  ]
  node [
    id 1948
    label "Quebec"
  ]
  node [
    id 1949
    label "dolar_kanadyjski"
  ]
  node [
    id 1950
    label "Nowa_Fundlandia"
  ]
  node [
    id 1951
    label "Zanzibar"
  ]
  node [
    id 1952
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1953
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1954
    label "&#346;wite&#378;"
  ]
  node [
    id 1955
    label "peso_kolumbijskie"
  ]
  node [
    id 1956
    label "Synaj"
  ]
  node [
    id 1957
    label "paraszyt"
  ]
  node [
    id 1958
    label "funt_egipski"
  ]
  node [
    id 1959
    label "szach"
  ]
  node [
    id 1960
    label "Baktria"
  ]
  node [
    id 1961
    label "afgani"
  ]
  node [
    id 1962
    label "baht"
  ]
  node [
    id 1963
    label "tolar"
  ]
  node [
    id 1964
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1965
    label "Naddniestrze"
  ]
  node [
    id 1966
    label "Gagauzja"
  ]
  node [
    id 1967
    label "Anglia"
  ]
  node [
    id 1968
    label "Amazonia"
  ]
  node [
    id 1969
    label "plantowa&#263;"
  ]
  node [
    id 1970
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1971
    label "zapadnia"
  ]
  node [
    id 1972
    label "Zamojszczyzna"
  ]
  node [
    id 1973
    label "Turkiestan"
  ]
  node [
    id 1974
    label "Noworosja"
  ]
  node [
    id 1975
    label "Mezoameryka"
  ]
  node [
    id 1976
    label "glinowanie"
  ]
  node [
    id 1977
    label "Kurdystan"
  ]
  node [
    id 1978
    label "martwica"
  ]
  node [
    id 1979
    label "Szkocja"
  ]
  node [
    id 1980
    label "litosfera"
  ]
  node [
    id 1981
    label "penetrator"
  ]
  node [
    id 1982
    label "glinowa&#263;"
  ]
  node [
    id 1983
    label "Zabajkale"
  ]
  node [
    id 1984
    label "domain"
  ]
  node [
    id 1985
    label "Bojkowszczyzna"
  ]
  node [
    id 1986
    label "podglebie"
  ]
  node [
    id 1987
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1988
    label "Pamir"
  ]
  node [
    id 1989
    label "Indochiny"
  ]
  node [
    id 1990
    label "Kurpie"
  ]
  node [
    id 1991
    label "S&#261;decczyzna"
  ]
  node [
    id 1992
    label "kort"
  ]
  node [
    id 1993
    label "czynnik_produkcji"
  ]
  node [
    id 1994
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1995
    label "Huculszczyzna"
  ]
  node [
    id 1996
    label "powierzchnia"
  ]
  node [
    id 1997
    label "Podhale"
  ]
  node [
    id 1998
    label "pr&#243;chnica"
  ]
  node [
    id 1999
    label "Hercegowina"
  ]
  node [
    id 2000
    label "Walia"
  ]
  node [
    id 2001
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 2002
    label "ryzosfera"
  ]
  node [
    id 2003
    label "Kaukaz"
  ]
  node [
    id 2004
    label "Biskupizna"
  ]
  node [
    id 2005
    label "Bo&#347;nia"
  ]
  node [
    id 2006
    label "p&#322;aszczyzna"
  ]
  node [
    id 2007
    label "dotleni&#263;"
  ]
  node [
    id 2008
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 2009
    label "Opolszczyzna"
  ]
  node [
    id 2010
    label "&#321;emkowszczyzna"
  ]
  node [
    id 2011
    label "Podbeskidzie"
  ]
  node [
    id 2012
    label "Kaszuby"
  ]
  node [
    id 2013
    label "Ko&#322;yma"
  ]
  node [
    id 2014
    label "glej"
  ]
  node [
    id 2015
    label "posadzka"
  ]
  node [
    id 2016
    label "Polesie"
  ]
  node [
    id 2017
    label "Palestyna"
  ]
  node [
    id 2018
    label "Lauda"
  ]
  node [
    id 2019
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 2020
    label "Laponia"
  ]
  node [
    id 2021
    label "Yorkshire"
  ]
  node [
    id 2022
    label "Zag&#243;rze"
  ]
  node [
    id 2023
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 2024
    label "&#379;ywiecczyzna"
  ]
  node [
    id 2025
    label "Oksytania"
  ]
  node [
    id 2026
    label "Kociewie"
  ]
  node [
    id 2027
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 2028
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 2029
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 2030
    label "osta&#263;_si&#281;"
  ]
  node [
    id 2031
    label "change"
  ]
  node [
    id 2032
    label "pozosta&#263;"
  ]
  node [
    id 2033
    label "catch"
  ]
  node [
    id 2034
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 2035
    label "support"
  ]
  node [
    id 2036
    label "prze&#380;y&#263;"
  ]
  node [
    id 2037
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 2038
    label "dramatize"
  ]
  node [
    id 2039
    label "pozwoli&#263;"
  ]
  node [
    id 2040
    label "spowodowa&#263;"
  ]
  node [
    id 2041
    label "authorize"
  ]
  node [
    id 2042
    label "da&#263;"
  ]
  node [
    id 2043
    label "za&#322;atwi&#263;"
  ]
  node [
    id 2044
    label "zarekomendowa&#263;"
  ]
  node [
    id 2045
    label "przes&#322;a&#263;"
  ]
  node [
    id 2046
    label "donie&#347;&#263;"
  ]
  node [
    id 2047
    label "act"
  ]
  node [
    id 2048
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 2049
    label "pofolgowa&#263;"
  ]
  node [
    id 2050
    label "assent"
  ]
  node [
    id 2051
    label "uzna&#263;"
  ]
  node [
    id 2052
    label "leave"
  ]
  node [
    id 2053
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 2054
    label "management"
  ]
  node [
    id 2055
    label "resolution"
  ]
  node [
    id 2056
    label "zdecydowanie"
  ]
  node [
    id 2057
    label "&#347;wiadectwo"
  ]
  node [
    id 2058
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 2059
    label "parafa"
  ]
  node [
    id 2060
    label "plik"
  ]
  node [
    id 2061
    label "raport&#243;wka"
  ]
  node [
    id 2062
    label "utw&#243;r"
  ]
  node [
    id 2063
    label "record"
  ]
  node [
    id 2064
    label "fascyku&#322;"
  ]
  node [
    id 2065
    label "dokumentacja"
  ]
  node [
    id 2066
    label "registratura"
  ]
  node [
    id 2067
    label "writing"
  ]
  node [
    id 2068
    label "sygnatariusz"
  ]
  node [
    id 2069
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 2070
    label "pewnie"
  ]
  node [
    id 2071
    label "zdecydowany"
  ]
  node [
    id 2072
    label "zauwa&#380;alnie"
  ]
  node [
    id 2073
    label "oddzia&#322;anie"
  ]
  node [
    id 2074
    label "podj&#281;cie"
  ]
  node [
    id 2075
    label "resoluteness"
  ]
  node [
    id 2076
    label "judgment"
  ]
  node [
    id 2077
    label "podkomisja"
  ]
  node [
    id 2078
    label "obrady"
  ]
  node [
    id 2079
    label "Komisja_Europejska"
  ]
  node [
    id 2080
    label "dyskusja"
  ]
  node [
    id 2081
    label "conference"
  ]
  node [
    id 2082
    label "konsylium"
  ]
  node [
    id 2083
    label "skupienie"
  ]
  node [
    id 2084
    label "zabudowania"
  ]
  node [
    id 2085
    label "group"
  ]
  node [
    id 2086
    label "batch"
  ]
  node [
    id 2087
    label "tkanka"
  ]
  node [
    id 2088
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 2089
    label "tw&#243;r"
  ]
  node [
    id 2090
    label "organogeneza"
  ]
  node [
    id 2091
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 2092
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 2093
    label "dekortykacja"
  ]
  node [
    id 2094
    label "Izba_Konsyliarska"
  ]
  node [
    id 2095
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 2096
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 2097
    label "stomia"
  ]
  node [
    id 2098
    label "okolica"
  ]
  node [
    id 2099
    label "Komitet_Region&#243;w"
  ]
  node [
    id 2100
    label "subcommittee"
  ]
  node [
    id 2101
    label "stosunek_prawny"
  ]
  node [
    id 2102
    label "oblig"
  ]
  node [
    id 2103
    label "uregulowa&#263;"
  ]
  node [
    id 2104
    label "duty"
  ]
  node [
    id 2105
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 2106
    label "zapowied&#378;"
  ]
  node [
    id 2107
    label "obowi&#261;zek"
  ]
  node [
    id 2108
    label "zapewnienie"
  ]
  node [
    id 2109
    label "signal"
  ]
  node [
    id 2110
    label "przewidywanie"
  ]
  node [
    id 2111
    label "oznaka"
  ]
  node [
    id 2112
    label "zawiadomienie"
  ]
  node [
    id 2113
    label "declaration"
  ]
  node [
    id 2114
    label "wym&#243;g"
  ]
  node [
    id 2115
    label "obarczy&#263;"
  ]
  node [
    id 2116
    label "powinno&#347;&#263;"
  ]
  node [
    id 2117
    label "zadanie"
  ]
  node [
    id 2118
    label "reply"
  ]
  node [
    id 2119
    label "zahipnotyzowanie"
  ]
  node [
    id 2120
    label "zdarzenie_si&#281;"
  ]
  node [
    id 2121
    label "chemia"
  ]
  node [
    id 2122
    label "wdarcie_si&#281;"
  ]
  node [
    id 2123
    label "dotarcie"
  ]
  node [
    id 2124
    label "reakcja_chemiczna"
  ]
  node [
    id 2125
    label "automatyczny"
  ]
  node [
    id 2126
    label "za&#347;wiadczenie"
  ]
  node [
    id 2127
    label "poinformowanie"
  ]
  node [
    id 2128
    label "security"
  ]
  node [
    id 2129
    label "op&#322;aci&#263;"
  ]
  node [
    id 2130
    label "ulepszy&#263;"
  ]
  node [
    id 2131
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 2132
    label "normalize"
  ]
  node [
    id 2133
    label "rewers"
  ]
  node [
    id 2134
    label "attachment"
  ]
  node [
    id 2135
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 2136
    label "bargain"
  ]
  node [
    id 2137
    label "tycze&#263;"
  ]
  node [
    id 2138
    label "koszt"
  ]
  node [
    id 2139
    label "wych&#243;d"
  ]
  node [
    id 2140
    label "nak&#322;ad"
  ]
  node [
    id 2141
    label "ilo&#347;&#263;"
  ]
  node [
    id 2142
    label "kwota"
  ]
  node [
    id 2143
    label "liczba"
  ]
  node [
    id 2144
    label "ods&#322;oni&#281;cie"
  ]
  node [
    id 2145
    label "z&#322;o&#380;e"
  ]
  node [
    id 2146
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 2147
    label "sumpt"
  ]
  node [
    id 2148
    label "obowi&#261;zuj&#261;cy"
  ]
  node [
    id 2149
    label "znajomy"
  ]
  node [
    id 2150
    label "powszechny"
  ]
  node [
    id 2151
    label "znany"
  ]
  node [
    id 2152
    label "zbiorowy"
  ]
  node [
    id 2153
    label "cz&#281;sty"
  ]
  node [
    id 2154
    label "powszechnie"
  ]
  node [
    id 2155
    label "generalny"
  ]
  node [
    id 2156
    label "uprawomocnienie_si&#281;"
  ]
  node [
    id 2157
    label "uprawomocnianie_si&#281;"
  ]
  node [
    id 2158
    label "obowi&#261;zuj&#261;co"
  ]
  node [
    id 2159
    label "aktualny"
  ]
  node [
    id 2160
    label "sw&#243;j"
  ]
  node [
    id 2161
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 2162
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 2163
    label "znajomek"
  ]
  node [
    id 2164
    label "znajomo"
  ]
  node [
    id 2165
    label "pewien"
  ]
  node [
    id 2166
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 2167
    label "za_pan_brat"
  ]
  node [
    id 2168
    label "dobrze"
  ]
  node [
    id 2169
    label "spokojnie"
  ]
  node [
    id 2170
    label "zbie&#380;nie"
  ]
  node [
    id 2171
    label "zgodny"
  ]
  node [
    id 2172
    label "jednakowo"
  ]
  node [
    id 2173
    label "podobnie"
  ]
  node [
    id 2174
    label "zbie&#380;ny"
  ]
  node [
    id 2175
    label "jednakowy"
  ]
  node [
    id 2176
    label "identically"
  ]
  node [
    id 2177
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 2178
    label "odpowiednio"
  ]
  node [
    id 2179
    label "dobroczynnie"
  ]
  node [
    id 2180
    label "moralnie"
  ]
  node [
    id 2181
    label "korzystnie"
  ]
  node [
    id 2182
    label "pozytywnie"
  ]
  node [
    id 2183
    label "lepiej"
  ]
  node [
    id 2184
    label "wiele"
  ]
  node [
    id 2185
    label "skutecznie"
  ]
  node [
    id 2186
    label "pomy&#347;lnie"
  ]
  node [
    id 2187
    label "dobry"
  ]
  node [
    id 2188
    label "spokojny"
  ]
  node [
    id 2189
    label "bezproblemowo"
  ]
  node [
    id 2190
    label "przyjemnie"
  ]
  node [
    id 2191
    label "cichy"
  ]
  node [
    id 2192
    label "wolno"
  ]
  node [
    id 2193
    label "dzi&#243;b"
  ]
  node [
    id 2194
    label "&#322;uk_Kupidyna"
  ]
  node [
    id 2195
    label "otw&#243;r_g&#281;bowy"
  ]
  node [
    id 2196
    label "zacinanie"
  ]
  node [
    id 2197
    label "ssa&#263;"
  ]
  node [
    id 2198
    label "zacina&#263;"
  ]
  node [
    id 2199
    label "zaci&#261;&#263;"
  ]
  node [
    id 2200
    label "ssanie"
  ]
  node [
    id 2201
    label "jama_ustna"
  ]
  node [
    id 2202
    label "jadaczka"
  ]
  node [
    id 2203
    label "zaci&#281;cie"
  ]
  node [
    id 2204
    label "warga_dolna"
  ]
  node [
    id 2205
    label "warga_g&#243;rna"
  ]
  node [
    id 2206
    label "ryjek"
  ]
  node [
    id 2207
    label "grzebie&#324;"
  ]
  node [
    id 2208
    label "bow"
  ]
  node [
    id 2209
    label "statek"
  ]
  node [
    id 2210
    label "ustnik"
  ]
  node [
    id 2211
    label "sto&#380;ek_dziobowy"
  ]
  node [
    id 2212
    label "zako&#324;czenie"
  ]
  node [
    id 2213
    label "ostry"
  ]
  node [
    id 2214
    label "blizna"
  ]
  node [
    id 2215
    label "dziob&#243;wka"
  ]
  node [
    id 2216
    label "cera"
  ]
  node [
    id 2217
    label "wielko&#347;&#263;"
  ]
  node [
    id 2218
    label "rys"
  ]
  node [
    id 2219
    label "przedstawiciel"
  ]
  node [
    id 2220
    label "profil"
  ]
  node [
    id 2221
    label "p&#322;e&#263;"
  ]
  node [
    id 2222
    label "zas&#322;ona"
  ]
  node [
    id 2223
    label "p&#243;&#322;profil"
  ]
  node [
    id 2224
    label "policzek"
  ]
  node [
    id 2225
    label "brew"
  ]
  node [
    id 2226
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 2227
    label "uj&#281;cie"
  ]
  node [
    id 2228
    label "micha"
  ]
  node [
    id 2229
    label "reputacja"
  ]
  node [
    id 2230
    label "wyraz_twarzy"
  ]
  node [
    id 2231
    label "powieka"
  ]
  node [
    id 2232
    label "czo&#322;o"
  ]
  node [
    id 2233
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 2234
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 2235
    label "twarzyczka"
  ]
  node [
    id 2236
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 2237
    label "ucho"
  ]
  node [
    id 2238
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 2239
    label "prz&#243;d"
  ]
  node [
    id 2240
    label "oko"
  ]
  node [
    id 2241
    label "nos"
  ]
  node [
    id 2242
    label "podbr&#243;dek"
  ]
  node [
    id 2243
    label "pysk"
  ]
  node [
    id 2244
    label "maskowato&#347;&#263;"
  ]
  node [
    id 2245
    label "poderwa&#263;"
  ]
  node [
    id 2246
    label "ko&#324;"
  ]
  node [
    id 2247
    label "&#347;cisn&#261;&#263;"
  ]
  node [
    id 2248
    label "ostruga&#263;"
  ]
  node [
    id 2249
    label "nadci&#261;&#263;"
  ]
  node [
    id 2250
    label "bat"
  ]
  node [
    id 2251
    label "uderzy&#263;"
  ]
  node [
    id 2252
    label "zrani&#263;"
  ]
  node [
    id 2253
    label "w&#281;dka"
  ]
  node [
    id 2254
    label "lejce"
  ]
  node [
    id 2255
    label "wprawi&#263;"
  ]
  node [
    id 2256
    label "cut"
  ]
  node [
    id 2257
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 2258
    label "przerwa&#263;"
  ]
  node [
    id 2259
    label "ch&#322;osn&#261;&#263;"
  ]
  node [
    id 2260
    label "zepsu&#263;"
  ]
  node [
    id 2261
    label "naci&#261;&#263;"
  ]
  node [
    id 2262
    label "odebra&#263;"
  ]
  node [
    id 2263
    label "zablokowa&#263;"
  ]
  node [
    id 2264
    label "write_out"
  ]
  node [
    id 2265
    label "padanie"
  ]
  node [
    id 2266
    label "mina"
  ]
  node [
    id 2267
    label "w&#281;dkowanie"
  ]
  node [
    id 2268
    label "kaleczenie"
  ]
  node [
    id 2269
    label "nacinanie"
  ]
  node [
    id 2270
    label "podrywanie"
  ]
  node [
    id 2271
    label "powo&#380;enie"
  ]
  node [
    id 2272
    label "zaciskanie"
  ]
  node [
    id 2273
    label "struganie"
  ]
  node [
    id 2274
    label "ch&#322;ostanie"
  ]
  node [
    id 2275
    label "&#347;cina&#263;"
  ]
  node [
    id 2276
    label "zaciera&#263;"
  ]
  node [
    id 2277
    label "przerywa&#263;"
  ]
  node [
    id 2278
    label "psu&#263;"
  ]
  node [
    id 2279
    label "zaciska&#263;"
  ]
  node [
    id 2280
    label "odbiera&#263;"
  ]
  node [
    id 2281
    label "zakrawa&#263;"
  ]
  node [
    id 2282
    label "wprawia&#263;"
  ]
  node [
    id 2283
    label "podrywa&#263;"
  ]
  node [
    id 2284
    label "hack"
  ]
  node [
    id 2285
    label "reduce"
  ]
  node [
    id 2286
    label "przestawa&#263;"
  ]
  node [
    id 2287
    label "blokowa&#263;"
  ]
  node [
    id 2288
    label "nacina&#263;"
  ]
  node [
    id 2289
    label "pocina&#263;"
  ]
  node [
    id 2290
    label "uderza&#263;"
  ]
  node [
    id 2291
    label "ch&#322;osta&#263;"
  ]
  node [
    id 2292
    label "kaleczy&#263;"
  ]
  node [
    id 2293
    label "struga&#263;"
  ]
  node [
    id 2294
    label "ch&#322;o&#347;ni&#281;cie"
  ]
  node [
    id 2295
    label "&#347;ci&#347;ni&#281;cie"
  ]
  node [
    id 2296
    label "kszta&#322;t"
  ]
  node [
    id 2297
    label "naci&#281;cie"
  ]
  node [
    id 2298
    label "zaci&#281;ty"
  ]
  node [
    id 2299
    label "poderwanie"
  ]
  node [
    id 2300
    label "talent"
  ]
  node [
    id 2301
    label "go"
  ]
  node [
    id 2302
    label "capability"
  ]
  node [
    id 2303
    label "stanowczo"
  ]
  node [
    id 2304
    label "ostruganie"
  ]
  node [
    id 2305
    label "formacja_skalna"
  ]
  node [
    id 2306
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 2307
    label "potkni&#281;cie"
  ]
  node [
    id 2308
    label "zranienie"
  ]
  node [
    id 2309
    label "dash"
  ]
  node [
    id 2310
    label "uwa&#380;nie"
  ]
  node [
    id 2311
    label "nieust&#281;pliwie"
  ]
  node [
    id 2312
    label "&#347;lina"
  ]
  node [
    id 2313
    label "pi&#263;"
  ]
  node [
    id 2314
    label "sponge"
  ]
  node [
    id 2315
    label "mleko"
  ]
  node [
    id 2316
    label "rozpuszcza&#263;"
  ]
  node [
    id 2317
    label "wci&#261;ga&#263;"
  ]
  node [
    id 2318
    label "rusza&#263;"
  ]
  node [
    id 2319
    label "sucking"
  ]
  node [
    id 2320
    label "smoczek"
  ]
  node [
    id 2321
    label "narz&#261;d_g&#281;bowy"
  ]
  node [
    id 2322
    label "picie"
  ]
  node [
    id 2323
    label "ruszanie"
  ]
  node [
    id 2324
    label "consumption"
  ]
  node [
    id 2325
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 2326
    label "rozpuszczanie"
  ]
  node [
    id 2327
    label "aspiration"
  ]
  node [
    id 2328
    label "wci&#261;ganie"
  ]
  node [
    id 2329
    label "odci&#261;ganie"
  ]
  node [
    id 2330
    label "wessanie"
  ]
  node [
    id 2331
    label "ga&#378;nik"
  ]
  node [
    id 2332
    label "mechanizm"
  ]
  node [
    id 2333
    label "wysysanie"
  ]
  node [
    id 2334
    label "wyssanie"
  ]
  node [
    id 2335
    label "arrangement"
  ]
  node [
    id 2336
    label "zarz&#261;dzenie"
  ]
  node [
    id 2337
    label "polecenie"
  ]
  node [
    id 2338
    label "commission"
  ]
  node [
    id 2339
    label "ordonans"
  ]
  node [
    id 2340
    label "rule"
  ]
  node [
    id 2341
    label "stipulation"
  ]
  node [
    id 2342
    label "podnieci&#263;"
  ]
  node [
    id 2343
    label "scena"
  ]
  node [
    id 2344
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 2345
    label "numer"
  ]
  node [
    id 2346
    label "po&#380;ycie"
  ]
  node [
    id 2347
    label "podniecenie"
  ]
  node [
    id 2348
    label "nago&#347;&#263;"
  ]
  node [
    id 2349
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 2350
    label "seks"
  ]
  node [
    id 2351
    label "podniecanie"
  ]
  node [
    id 2352
    label "imisja"
  ]
  node [
    id 2353
    label "zwyczaj"
  ]
  node [
    id 2354
    label "rozmna&#380;anie"
  ]
  node [
    id 2355
    label "ruch_frykcyjny"
  ]
  node [
    id 2356
    label "ontologia"
  ]
  node [
    id 2357
    label "na_pieska"
  ]
  node [
    id 2358
    label "pozycja_misjonarska"
  ]
  node [
    id 2359
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 2360
    label "fragment"
  ]
  node [
    id 2361
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 2362
    label "z&#322;&#261;czenie"
  ]
  node [
    id 2363
    label "gra_wst&#281;pna"
  ]
  node [
    id 2364
    label "erotyka"
  ]
  node [
    id 2365
    label "urzeczywistnienie"
  ]
  node [
    id 2366
    label "baraszki"
  ]
  node [
    id 2367
    label "certificate"
  ]
  node [
    id 2368
    label "po&#380;&#261;danie"
  ]
  node [
    id 2369
    label "wzw&#243;d"
  ]
  node [
    id 2370
    label "funkcja"
  ]
  node [
    id 2371
    label "arystotelizm"
  ]
  node [
    id 2372
    label "podnieca&#263;"
  ]
  node [
    id 2373
    label "ukaz"
  ]
  node [
    id 2374
    label "pognanie"
  ]
  node [
    id 2375
    label "rekomendacja"
  ]
  node [
    id 2376
    label "wypowied&#378;"
  ]
  node [
    id 2377
    label "pobiegni&#281;cie"
  ]
  node [
    id 2378
    label "education"
  ]
  node [
    id 2379
    label "doradzenie"
  ]
  node [
    id 2380
    label "recommendation"
  ]
  node [
    id 2381
    label "zaordynowanie"
  ]
  node [
    id 2382
    label "powierzenie"
  ]
  node [
    id 2383
    label "przesadzenie"
  ]
  node [
    id 2384
    label "consign"
  ]
  node [
    id 2385
    label "dekret"
  ]
  node [
    id 2386
    label "berylowiec"
  ]
  node [
    id 2387
    label "content"
  ]
  node [
    id 2388
    label "pierwiastek_promieniotw&#243;rczy"
  ]
  node [
    id 2389
    label "jednostka_promieniowania"
  ]
  node [
    id 2390
    label "zadowolenie_si&#281;"
  ]
  node [
    id 2391
    label "miliradian"
  ]
  node [
    id 2392
    label "u&#347;miechni&#281;ty"
  ]
  node [
    id 2393
    label "usatysfakcjonowanie_si&#281;"
  ]
  node [
    id 2394
    label "mikroradian"
  ]
  node [
    id 2395
    label "przyswoi&#263;"
  ]
  node [
    id 2396
    label "one"
  ]
  node [
    id 2397
    label "ewoluowanie"
  ]
  node [
    id 2398
    label "supremum"
  ]
  node [
    id 2399
    label "przyswajanie"
  ]
  node [
    id 2400
    label "wyewoluowanie"
  ]
  node [
    id 2401
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 2402
    label "przeliczy&#263;"
  ]
  node [
    id 2403
    label "wyewoluowa&#263;"
  ]
  node [
    id 2404
    label "ewoluowa&#263;"
  ]
  node [
    id 2405
    label "matematyka"
  ]
  node [
    id 2406
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 2407
    label "rzut"
  ]
  node [
    id 2408
    label "liczba_naturalna"
  ]
  node [
    id 2409
    label "czynnik_biotyczny"
  ]
  node [
    id 2410
    label "individual"
  ]
  node [
    id 2411
    label "przyswaja&#263;"
  ]
  node [
    id 2412
    label "przyswojenie"
  ]
  node [
    id 2413
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 2414
    label "starzenie_si&#281;"
  ]
  node [
    id 2415
    label "przeliczanie"
  ]
  node [
    id 2416
    label "przelicza&#263;"
  ]
  node [
    id 2417
    label "infimum"
  ]
  node [
    id 2418
    label "przeliczenie"
  ]
  node [
    id 2419
    label "metal"
  ]
  node [
    id 2420
    label "nanoradian"
  ]
  node [
    id 2421
    label "radian"
  ]
  node [
    id 2422
    label "zadowolony"
  ]
  node [
    id 2423
    label "weso&#322;y"
  ]
  node [
    id 2424
    label "ranek"
  ]
  node [
    id 2425
    label "doba"
  ]
  node [
    id 2426
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 2427
    label "noc"
  ]
  node [
    id 2428
    label "podwiecz&#243;r"
  ]
  node [
    id 2429
    label "po&#322;udnie"
  ]
  node [
    id 2430
    label "godzina"
  ]
  node [
    id 2431
    label "przedpo&#322;udnie"
  ]
  node [
    id 2432
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 2433
    label "long_time"
  ]
  node [
    id 2434
    label "wiecz&#243;r"
  ]
  node [
    id 2435
    label "t&#322;usty_czwartek"
  ]
  node [
    id 2436
    label "popo&#322;udnie"
  ]
  node [
    id 2437
    label "walentynki"
  ]
  node [
    id 2438
    label "czynienie_si&#281;"
  ]
  node [
    id 2439
    label "s&#322;o&#324;ce"
  ]
  node [
    id 2440
    label "rano"
  ]
  node [
    id 2441
    label "tydzie&#324;"
  ]
  node [
    id 2442
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 2443
    label "wzej&#347;cie"
  ]
  node [
    id 2444
    label "czas"
  ]
  node [
    id 2445
    label "wsta&#263;"
  ]
  node [
    id 2446
    label "day"
  ]
  node [
    id 2447
    label "termin"
  ]
  node [
    id 2448
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 2449
    label "przedwiecz&#243;r"
  ]
  node [
    id 2450
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 2451
    label "Sylwester"
  ]
  node [
    id 2452
    label "poprzedzanie"
  ]
  node [
    id 2453
    label "czasoprzestrze&#324;"
  ]
  node [
    id 2454
    label "laba"
  ]
  node [
    id 2455
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 2456
    label "chronometria"
  ]
  node [
    id 2457
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 2458
    label "rachuba_czasu"
  ]
  node [
    id 2459
    label "czasokres"
  ]
  node [
    id 2460
    label "odczyt"
  ]
  node [
    id 2461
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 2462
    label "dzieje"
  ]
  node [
    id 2463
    label "poprzedzenie"
  ]
  node [
    id 2464
    label "trawienie"
  ]
  node [
    id 2465
    label "pochodzi&#263;"
  ]
  node [
    id 2466
    label "period"
  ]
  node [
    id 2467
    label "okres_czasu"
  ]
  node [
    id 2468
    label "poprzedza&#263;"
  ]
  node [
    id 2469
    label "schy&#322;ek"
  ]
  node [
    id 2470
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 2471
    label "odwlekanie_si&#281;"
  ]
  node [
    id 2472
    label "zegar"
  ]
  node [
    id 2473
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 2474
    label "czwarty_wymiar"
  ]
  node [
    id 2475
    label "pochodzenie"
  ]
  node [
    id 2476
    label "Zeitgeist"
  ]
  node [
    id 2477
    label "trawi&#263;"
  ]
  node [
    id 2478
    label "pogoda"
  ]
  node [
    id 2479
    label "poprzedzi&#263;"
  ]
  node [
    id 2480
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 2481
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 2482
    label "time_period"
  ]
  node [
    id 2483
    label "nazewnictwo"
  ]
  node [
    id 2484
    label "term"
  ]
  node [
    id 2485
    label "przypadni&#281;cie"
  ]
  node [
    id 2486
    label "ekspiracja"
  ]
  node [
    id 2487
    label "przypa&#347;&#263;"
  ]
  node [
    id 2488
    label "chronogram"
  ]
  node [
    id 2489
    label "praktyka"
  ]
  node [
    id 2490
    label "nazwa"
  ]
  node [
    id 2491
    label "przyj&#281;cie"
  ]
  node [
    id 2492
    label "spotkanie"
  ]
  node [
    id 2493
    label "night"
  ]
  node [
    id 2494
    label "zach&#243;d"
  ]
  node [
    id 2495
    label "vesper"
  ]
  node [
    id 2496
    label "pora"
  ]
  node [
    id 2497
    label "odwieczerz"
  ]
  node [
    id 2498
    label "blady_&#347;wit"
  ]
  node [
    id 2499
    label "podkurek"
  ]
  node [
    id 2500
    label "aurora"
  ]
  node [
    id 2501
    label "wsch&#243;d"
  ]
  node [
    id 2502
    label "&#347;rodek"
  ]
  node [
    id 2503
    label "Ziemia"
  ]
  node [
    id 2504
    label "dwunasta"
  ]
  node [
    id 2505
    label "strona_&#347;wiata"
  ]
  node [
    id 2506
    label "dopo&#322;udnie"
  ]
  node [
    id 2507
    label "p&#243;&#322;noc"
  ]
  node [
    id 2508
    label "nokturn"
  ]
  node [
    id 2509
    label "time"
  ]
  node [
    id 2510
    label "p&#243;&#322;godzina"
  ]
  node [
    id 2511
    label "jednostka_czasu"
  ]
  node [
    id 2512
    label "minuta"
  ]
  node [
    id 2513
    label "kwadrans"
  ]
  node [
    id 2514
    label "jednostka_geologiczna"
  ]
  node [
    id 2515
    label "weekend"
  ]
  node [
    id 2516
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 2517
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 2518
    label "miesi&#261;c"
  ]
  node [
    id 2519
    label "S&#322;o&#324;ce"
  ]
  node [
    id 2520
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 2521
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 2522
    label "kochanie"
  ]
  node [
    id 2523
    label "sunlight"
  ]
  node [
    id 2524
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 2525
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 2526
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 2527
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 2528
    label "mount"
  ]
  node [
    id 2529
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 2530
    label "wzej&#347;&#263;"
  ]
  node [
    id 2531
    label "ascend"
  ]
  node [
    id 2532
    label "kuca&#263;"
  ]
  node [
    id 2533
    label "wyzdrowie&#263;"
  ]
  node [
    id 2534
    label "opu&#347;ci&#263;"
  ]
  node [
    id 2535
    label "arise"
  ]
  node [
    id 2536
    label "stan&#261;&#263;"
  ]
  node [
    id 2537
    label "przesta&#263;"
  ]
  node [
    id 2538
    label "wyzdrowienie"
  ]
  node [
    id 2539
    label "kl&#281;czenie"
  ]
  node [
    id 2540
    label "opuszczenie"
  ]
  node [
    id 2541
    label "uniesienie_si&#281;"
  ]
  node [
    id 2542
    label "beginning"
  ]
  node [
    id 2543
    label "przestanie"
  ]
  node [
    id 2544
    label "grudzie&#324;"
  ]
  node [
    id 2545
    label "luty"
  ]
  node [
    id 2546
    label "ro&#347;lina_zielna"
  ]
  node [
    id 2547
    label "go&#378;dzikowate"
  ]
  node [
    id 2548
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 2549
    label "miech"
  ]
  node [
    id 2550
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 2551
    label "kalendy"
  ]
  node [
    id 2552
    label "go&#378;dzikowce"
  ]
  node [
    id 2553
    label "kognicja"
  ]
  node [
    id 2554
    label "object"
  ]
  node [
    id 2555
    label "rozprawa"
  ]
  node [
    id 2556
    label "temat"
  ]
  node [
    id 2557
    label "szczeg&#243;&#322;"
  ]
  node [
    id 2558
    label "przes&#322;anka"
  ]
  node [
    id 2559
    label "rzecz"
  ]
  node [
    id 2560
    label "idea"
  ]
  node [
    id 2561
    label "przebiec"
  ]
  node [
    id 2562
    label "charakter"
  ]
  node [
    id 2563
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 2564
    label "motyw"
  ]
  node [
    id 2565
    label "przebiegni&#281;cie"
  ]
  node [
    id 2566
    label "fabu&#322;a"
  ]
  node [
    id 2567
    label "ideologia"
  ]
  node [
    id 2568
    label "byt"
  ]
  node [
    id 2569
    label "intelekt"
  ]
  node [
    id 2570
    label "Kant"
  ]
  node [
    id 2571
    label "cel"
  ]
  node [
    id 2572
    label "istota"
  ]
  node [
    id 2573
    label "ideacja"
  ]
  node [
    id 2574
    label "mienie"
  ]
  node [
    id 2575
    label "kultura"
  ]
  node [
    id 2576
    label "wpa&#347;&#263;"
  ]
  node [
    id 2577
    label "wpada&#263;"
  ]
  node [
    id 2578
    label "s&#261;d"
  ]
  node [
    id 2579
    label "rozumowanie"
  ]
  node [
    id 2580
    label "opracowanie"
  ]
  node [
    id 2581
    label "cytat"
  ]
  node [
    id 2582
    label "obja&#347;nienie"
  ]
  node [
    id 2583
    label "s&#261;dzenie"
  ]
  node [
    id 2584
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 2585
    label "niuansowa&#263;"
  ]
  node [
    id 2586
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 2587
    label "sk&#322;adnik"
  ]
  node [
    id 2588
    label "zniuansowa&#263;"
  ]
  node [
    id 2589
    label "fakt"
  ]
  node [
    id 2590
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 2591
    label "przyczyna"
  ]
  node [
    id 2592
    label "wnioskowanie"
  ]
  node [
    id 2593
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 2594
    label "wyraz_pochodny"
  ]
  node [
    id 2595
    label "zboczenie"
  ]
  node [
    id 2596
    label "om&#243;wienie"
  ]
  node [
    id 2597
    label "omawia&#263;"
  ]
  node [
    id 2598
    label "fraza"
  ]
  node [
    id 2599
    label "tre&#347;&#263;"
  ]
  node [
    id 2600
    label "entity"
  ]
  node [
    id 2601
    label "forum"
  ]
  node [
    id 2602
    label "topik"
  ]
  node [
    id 2603
    label "tematyka"
  ]
  node [
    id 2604
    label "w&#261;tek"
  ]
  node [
    id 2605
    label "zbaczanie"
  ]
  node [
    id 2606
    label "om&#243;wi&#263;"
  ]
  node [
    id 2607
    label "omawianie"
  ]
  node [
    id 2608
    label "melodia"
  ]
  node [
    id 2609
    label "otoczka"
  ]
  node [
    id 2610
    label "zbacza&#263;"
  ]
  node [
    id 2611
    label "zboczy&#263;"
  ]
  node [
    id 2612
    label "finansowo"
  ]
  node [
    id 2613
    label "mi&#281;dzybankowy"
  ]
  node [
    id 2614
    label "pozamaterialny"
  ]
  node [
    id 2615
    label "materjalny"
  ]
  node [
    id 2616
    label "fizyczny"
  ]
  node [
    id 2617
    label "materialny"
  ]
  node [
    id 2618
    label "niematerialnie"
  ]
  node [
    id 2619
    label "financially"
  ]
  node [
    id 2620
    label "fiscally"
  ]
  node [
    id 2621
    label "bytowo"
  ]
  node [
    id 2622
    label "ekonomicznie"
  ]
  node [
    id 2623
    label "pracownik"
  ]
  node [
    id 2624
    label "fizykalnie"
  ]
  node [
    id 2625
    label "materializowanie"
  ]
  node [
    id 2626
    label "fizycznie"
  ]
  node [
    id 2627
    label "namacalny"
  ]
  node [
    id 2628
    label "widoczny"
  ]
  node [
    id 2629
    label "zmaterializowanie"
  ]
  node [
    id 2630
    label "organiczny"
  ]
  node [
    id 2631
    label "gimnastyczny"
  ]
  node [
    id 2632
    label "ozdabia&#263;"
  ]
  node [
    id 2633
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 2634
    label "trim"
  ]
  node [
    id 2635
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 2636
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 2637
    label "use"
  ]
  node [
    id 2638
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 2639
    label "thing"
  ]
  node [
    id 2640
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 2641
    label "narobienie"
  ]
  node [
    id 2642
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 2643
    label "creation"
  ]
  node [
    id 2644
    label "porobienie"
  ]
  node [
    id 2645
    label "czyn"
  ]
  node [
    id 2646
    label "addytywno&#347;&#263;"
  ]
  node [
    id 2647
    label "function"
  ]
  node [
    id 2648
    label "funkcjonowanie"
  ]
  node [
    id 2649
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 2650
    label "powierzanie"
  ]
  node [
    id 2651
    label "dziedzina"
  ]
  node [
    id 2652
    label "przeciwdziedzina"
  ]
  node [
    id 2653
    label "awansowa&#263;"
  ]
  node [
    id 2654
    label "stawia&#263;"
  ]
  node [
    id 2655
    label "wakowa&#263;"
  ]
  node [
    id 2656
    label "znaczenie"
  ]
  node [
    id 2657
    label "postawi&#263;"
  ]
  node [
    id 2658
    label "awansowanie"
  ]
  node [
    id 2659
    label "przejaskrawianie"
  ]
  node [
    id 2660
    label "zniszczenie"
  ]
  node [
    id 2661
    label "zu&#380;ywanie"
  ]
  node [
    id 2662
    label "rezerwa_rewaluacyjna"
  ]
  node [
    id 2663
    label "portfel"
  ]
  node [
    id 2664
    label "wydatki_bud&#380;etowe"
  ]
  node [
    id 2665
    label "etat"
  ]
  node [
    id 2666
    label "wynie&#347;&#263;"
  ]
  node [
    id 2667
    label "pieni&#261;dze"
  ]
  node [
    id 2668
    label "limit"
  ]
  node [
    id 2669
    label "wynosi&#263;"
  ]
  node [
    id 2670
    label "bag"
  ]
  node [
    id 2671
    label "pugilares"
  ]
  node [
    id 2672
    label "pojemnik"
  ]
  node [
    id 2673
    label "zas&#243;b"
  ]
  node [
    id 2674
    label "galanteria"
  ]
  node [
    id 2675
    label "wymiar"
  ]
  node [
    id 2676
    label "posada"
  ]
  node [
    id 2677
    label "og&#243;lnie"
  ]
  node [
    id 2678
    label "og&#243;&#322;owy"
  ]
  node [
    id 2679
    label "nadrz&#281;dny"
  ]
  node [
    id 2680
    label "ca&#322;y"
  ]
  node [
    id 2681
    label "kompletny"
  ]
  node [
    id 2682
    label "&#322;&#261;czny"
  ]
  node [
    id 2683
    label "jedyny"
  ]
  node [
    id 2684
    label "du&#380;y"
  ]
  node [
    id 2685
    label "zdr&#243;w"
  ]
  node [
    id 2686
    label "calu&#347;ko"
  ]
  node [
    id 2687
    label "&#380;ywy"
  ]
  node [
    id 2688
    label "pe&#322;ny"
  ]
  node [
    id 2689
    label "podobny"
  ]
  node [
    id 2690
    label "ca&#322;o"
  ]
  node [
    id 2691
    label "&#322;&#261;cznie"
  ]
  node [
    id 2692
    label "zbiorczy"
  ]
  node [
    id 2693
    label "pierwszorz&#281;dny"
  ]
  node [
    id 2694
    label "nadrz&#281;dnie"
  ]
  node [
    id 2695
    label "wsp&#243;lny"
  ]
  node [
    id 2696
    label "zbiorowo"
  ]
  node [
    id 2697
    label "kompletnie"
  ]
  node [
    id 2698
    label "zupe&#322;ny"
  ]
  node [
    id 2699
    label "w_pizdu"
  ]
  node [
    id 2700
    label "posp&#243;lnie"
  ]
  node [
    id 2701
    label "cz&#281;sto"
  ]
  node [
    id 2702
    label "zwi&#261;zanie"
  ]
  node [
    id 2703
    label "podobie&#324;stwo"
  ]
  node [
    id 2704
    label "partnership"
  ]
  node [
    id 2705
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 2706
    label "wi&#261;zanie"
  ]
  node [
    id 2707
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 2708
    label "society"
  ]
  node [
    id 2709
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 2710
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 2711
    label "bratnia_dusza"
  ]
  node [
    id 2712
    label "zwi&#261;zek"
  ]
  node [
    id 2713
    label "zwi&#261;za&#263;"
  ]
  node [
    id 2714
    label "marriage"
  ]
  node [
    id 2715
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 2716
    label "Fremeni"
  ]
  node [
    id 2717
    label "odwadnia&#263;"
  ]
  node [
    id 2718
    label "odwodni&#263;"
  ]
  node [
    id 2719
    label "powi&#261;zanie"
  ]
  node [
    id 2720
    label "konstytucja"
  ]
  node [
    id 2721
    label "odwadnianie"
  ]
  node [
    id 2722
    label "odwodnienie"
  ]
  node [
    id 2723
    label "marketing_afiliacyjny"
  ]
  node [
    id 2724
    label "substancja_chemiczna"
  ]
  node [
    id 2725
    label "koligacja"
  ]
  node [
    id 2726
    label "lokant"
  ]
  node [
    id 2727
    label "azeotrop"
  ]
  node [
    id 2728
    label "podobno&#347;&#263;"
  ]
  node [
    id 2729
    label "relacja"
  ]
  node [
    id 2730
    label "Rumelia"
  ]
  node [
    id 2731
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 2732
    label "narta"
  ]
  node [
    id 2733
    label "podwi&#261;zywanie"
  ]
  node [
    id 2734
    label "dressing"
  ]
  node [
    id 2735
    label "socket"
  ]
  node [
    id 2736
    label "szermierka"
  ]
  node [
    id 2737
    label "przywi&#261;zywanie"
  ]
  node [
    id 2738
    label "pakowanie"
  ]
  node [
    id 2739
    label "proces_chemiczny"
  ]
  node [
    id 2740
    label "my&#347;lenie"
  ]
  node [
    id 2741
    label "do&#322;&#261;czanie"
  ]
  node [
    id 2742
    label "wytwarzanie"
  ]
  node [
    id 2743
    label "cement"
  ]
  node [
    id 2744
    label "ceg&#322;a"
  ]
  node [
    id 2745
    label "combination"
  ]
  node [
    id 2746
    label "zobowi&#261;zywanie"
  ]
  node [
    id 2747
    label "szcz&#281;ka"
  ]
  node [
    id 2748
    label "anga&#380;owanie"
  ]
  node [
    id 2749
    label "wi&#261;za&#263;"
  ]
  node [
    id 2750
    label "twardnienie"
  ]
  node [
    id 2751
    label "tobo&#322;ek"
  ]
  node [
    id 2752
    label "podwi&#261;zanie"
  ]
  node [
    id 2753
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 2754
    label "przywi&#261;zanie"
  ]
  node [
    id 2755
    label "przymocowywanie"
  ]
  node [
    id 2756
    label "scalanie"
  ]
  node [
    id 2757
    label "mezomeria"
  ]
  node [
    id 2758
    label "fusion"
  ]
  node [
    id 2759
    label "kojarzenie_si&#281;"
  ]
  node [
    id 2760
    label "&#322;&#261;czenie"
  ]
  node [
    id 2761
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 2762
    label "rozmieszczenie"
  ]
  node [
    id 2763
    label "element_konstrukcyjny"
  ]
  node [
    id 2764
    label "obezw&#322;adnianie"
  ]
  node [
    id 2765
    label "manewr"
  ]
  node [
    id 2766
    label "miecz"
  ]
  node [
    id 2767
    label "obwi&#261;zanie"
  ]
  node [
    id 2768
    label "zawi&#261;zek"
  ]
  node [
    id 2769
    label "obwi&#261;zywanie"
  ]
  node [
    id 2770
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 2771
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 2772
    label "w&#281;ze&#322;"
  ]
  node [
    id 2773
    label "consort"
  ]
  node [
    id 2774
    label "opakowa&#263;"
  ]
  node [
    id 2775
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 2776
    label "relate"
  ]
  node [
    id 2777
    label "form"
  ]
  node [
    id 2778
    label "unify"
  ]
  node [
    id 2779
    label "bind"
  ]
  node [
    id 2780
    label "zawi&#261;za&#263;"
  ]
  node [
    id 2781
    label "zaprawa"
  ]
  node [
    id 2782
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 2783
    label "powi&#261;za&#263;"
  ]
  node [
    id 2784
    label "scali&#263;"
  ]
  node [
    id 2785
    label "zatrzyma&#263;"
  ]
  node [
    id 2786
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 2787
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 2788
    label "ograniczenie"
  ]
  node [
    id 2789
    label "po&#322;&#261;czenie"
  ]
  node [
    id 2790
    label "do&#322;&#261;czenie"
  ]
  node [
    id 2791
    label "opakowanie"
  ]
  node [
    id 2792
    label "obezw&#322;adnienie"
  ]
  node [
    id 2793
    label "zawi&#261;zanie"
  ]
  node [
    id 2794
    label "tying"
  ]
  node [
    id 2795
    label "st&#281;&#380;enie"
  ]
  node [
    id 2796
    label "affiliation"
  ]
  node [
    id 2797
    label "fastening"
  ]
  node [
    id 2798
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 2799
    label "po_europejsku"
  ]
  node [
    id 2800
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 2801
    label "European"
  ]
  node [
    id 2802
    label "typowy"
  ]
  node [
    id 2803
    label "europejsko"
  ]
  node [
    id 2804
    label "zwyczajny"
  ]
  node [
    id 2805
    label "typowo"
  ]
  node [
    id 2806
    label "zwyk&#322;y"
  ]
  node [
    id 2807
    label "charakterystycznie"
  ]
  node [
    id 2808
    label "szczeg&#243;lny"
  ]
  node [
    id 2809
    label "wyj&#261;tkowy"
  ]
  node [
    id 2810
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 2811
    label "nale&#380;ny"
  ]
  node [
    id 2812
    label "nale&#380;yty"
  ]
  node [
    id 2813
    label "uprawniony"
  ]
  node [
    id 2814
    label "zasadniczy"
  ]
  node [
    id 2815
    label "stosownie"
  ]
  node [
    id 2816
    label "taki"
  ]
  node [
    id 2817
    label "prawdziwy"
  ]
  node [
    id 2818
    label "ten"
  ]
  node [
    id 2819
    label "budgetary"
  ]
  node [
    id 2820
    label "bud&#380;etowo"
  ]
  node [
    id 2821
    label "etatowy"
  ]
  node [
    id 2822
    label "etatowo"
  ]
  node [
    id 2823
    label "sta&#322;y"
  ]
  node [
    id 2824
    label "oficjalny"
  ]
  node [
    id 2825
    label "zatrudniony"
  ]
  node [
    id 2826
    label "ilustracja"
  ]
  node [
    id 2827
    label "rzuci&#263;"
  ]
  node [
    id 2828
    label "destiny"
  ]
  node [
    id 2829
    label "ustalenie"
  ]
  node [
    id 2830
    label "przymus"
  ]
  node [
    id 2831
    label "przydzielenie"
  ]
  node [
    id 2832
    label "p&#243;j&#347;cie"
  ]
  node [
    id 2833
    label "oblat"
  ]
  node [
    id 2834
    label "rzucenie"
  ]
  node [
    id 2835
    label "wybranie"
  ]
  node [
    id 2836
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 2837
    label "klient"
  ]
  node [
    id 2838
    label "piel&#281;gniarz"
  ]
  node [
    id 2839
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 2840
    label "od&#322;&#261;czanie"
  ]
  node [
    id 2841
    label "od&#322;&#261;czenie"
  ]
  node [
    id 2842
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 2843
    label "szpitalnik"
  ]
  node [
    id 2844
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 2845
    label "zareagowa&#263;"
  ]
  node [
    id 2846
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 2847
    label "allude"
  ]
  node [
    id 2848
    label "zmieni&#263;"
  ]
  node [
    id 2849
    label "raise"
  ]
  node [
    id 2850
    label "odpowiedzie&#263;"
  ]
  node [
    id 2851
    label "react"
  ]
  node [
    id 2852
    label "sta&#263;_si&#281;"
  ]
  node [
    id 2853
    label "post&#261;pi&#263;"
  ]
  node [
    id 2854
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 2855
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 2856
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 2857
    label "zorganizowa&#263;"
  ]
  node [
    id 2858
    label "appoint"
  ]
  node [
    id 2859
    label "wystylizowa&#263;"
  ]
  node [
    id 2860
    label "cause"
  ]
  node [
    id 2861
    label "przerobi&#263;"
  ]
  node [
    id 2862
    label "nabra&#263;"
  ]
  node [
    id 2863
    label "make"
  ]
  node [
    id 2864
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 2865
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 2866
    label "wydali&#263;"
  ]
  node [
    id 2867
    label "sprawi&#263;"
  ]
  node [
    id 2868
    label "zast&#261;pi&#263;"
  ]
  node [
    id 2869
    label "come_up"
  ]
  node [
    id 2870
    label "przej&#347;&#263;"
  ]
  node [
    id 2871
    label "straci&#263;"
  ]
  node [
    id 2872
    label "zyska&#263;"
  ]
  node [
    id 2873
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 2874
    label "odj&#261;&#263;"
  ]
  node [
    id 2875
    label "introduce"
  ]
  node [
    id 2876
    label "begin"
  ]
  node [
    id 2877
    label "podpisanie"
  ]
  node [
    id 2878
    label "confirmation"
  ]
  node [
    id 2879
    label "konwalidacja"
  ]
  node [
    id 2880
    label "pozwolenie"
  ]
  node [
    id 2881
    label "campaign"
  ]
  node [
    id 2882
    label "causing"
  ]
  node [
    id 2883
    label "broadcast"
  ]
  node [
    id 2884
    label "nazwanie"
  ]
  node [
    id 2885
    label "przes&#322;anie"
  ]
  node [
    id 2886
    label "denomination"
  ]
  node [
    id 2887
    label "zwalnianie_si&#281;"
  ]
  node [
    id 2888
    label "authorization"
  ]
  node [
    id 2889
    label "koncesjonowanie"
  ]
  node [
    id 2890
    label "zwolnienie_si&#281;"
  ]
  node [
    id 2891
    label "pozwole&#324;stwo"
  ]
  node [
    id 2892
    label "bycie_w_stanie"
  ]
  node [
    id 2893
    label "odwieszenie"
  ]
  node [
    id 2894
    label "odpowied&#378;"
  ]
  node [
    id 2895
    label "pofolgowanie"
  ]
  node [
    id 2896
    label "license"
  ]
  node [
    id 2897
    label "franchise"
  ]
  node [
    id 2898
    label "umo&#380;liwienie"
  ]
  node [
    id 2899
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 2900
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 2901
    label "uznanie"
  ]
  node [
    id 2902
    label "ratyfikowanie"
  ]
  node [
    id 2903
    label "subscription"
  ]
  node [
    id 2904
    label "opatrzenie"
  ]
  node [
    id 2905
    label "nast&#281;pnie"
  ]
  node [
    id 2906
    label "inny"
  ]
  node [
    id 2907
    label "nastopny"
  ]
  node [
    id 2908
    label "kolejno"
  ]
  node [
    id 2909
    label "kt&#243;ry&#347;"
  ]
  node [
    id 2910
    label "osobno"
  ]
  node [
    id 2911
    label "r&#243;&#380;ny"
  ]
  node [
    id 2912
    label "inszy"
  ]
  node [
    id 2913
    label "inaczej"
  ]
  node [
    id 2914
    label "kilkunastomiesi&#281;czny"
  ]
  node [
    id 2915
    label "wielomiesi&#281;czny"
  ]
  node [
    id 2916
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 2917
    label "mie&#263;_miejsce"
  ]
  node [
    id 2918
    label "equal"
  ]
  node [
    id 2919
    label "si&#281;ga&#263;"
  ]
  node [
    id 2920
    label "stan"
  ]
  node [
    id 2921
    label "obecno&#347;&#263;"
  ]
  node [
    id 2922
    label "stand"
  ]
  node [
    id 2923
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 2924
    label "uczestniczy&#263;"
  ]
  node [
    id 2925
    label "participate"
  ]
  node [
    id 2926
    label "istnie&#263;"
  ]
  node [
    id 2927
    label "pozostawa&#263;"
  ]
  node [
    id 2928
    label "zostawa&#263;"
  ]
  node [
    id 2929
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 2930
    label "adhere"
  ]
  node [
    id 2931
    label "compass"
  ]
  node [
    id 2932
    label "appreciation"
  ]
  node [
    id 2933
    label "osi&#261;ga&#263;"
  ]
  node [
    id 2934
    label "dociera&#263;"
  ]
  node [
    id 2935
    label "get"
  ]
  node [
    id 2936
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 2937
    label "mierzy&#263;"
  ]
  node [
    id 2938
    label "u&#380;ywa&#263;"
  ]
  node [
    id 2939
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 2940
    label "exsert"
  ]
  node [
    id 2941
    label "being"
  ]
  node [
    id 2942
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 2943
    label "p&#322;ywa&#263;"
  ]
  node [
    id 2944
    label "bangla&#263;"
  ]
  node [
    id 2945
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 2946
    label "przebiega&#263;"
  ]
  node [
    id 2947
    label "wk&#322;ada&#263;"
  ]
  node [
    id 2948
    label "bywa&#263;"
  ]
  node [
    id 2949
    label "dziama&#263;"
  ]
  node [
    id 2950
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 2951
    label "stara&#263;_si&#281;"
  ]
  node [
    id 2952
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 2953
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 2954
    label "krok"
  ]
  node [
    id 2955
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 2956
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 2957
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 2958
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 2959
    label "continue"
  ]
  node [
    id 2960
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 2961
    label "wci&#281;cie"
  ]
  node [
    id 2962
    label "warstwa"
  ]
  node [
    id 2963
    label "samopoczucie"
  ]
  node [
    id 2964
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 2965
    label "state"
  ]
  node [
    id 2966
    label "wektor"
  ]
  node [
    id 2967
    label "Goa"
  ]
  node [
    id 2968
    label "poziom"
  ]
  node [
    id 2969
    label "shape"
  ]
  node [
    id 2970
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 2971
    label "podnosi&#263;"
  ]
  node [
    id 2972
    label "drive"
  ]
  node [
    id 2973
    label "zmienia&#263;"
  ]
  node [
    id 2974
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 2975
    label "admit"
  ]
  node [
    id 2976
    label "reagowa&#263;"
  ]
  node [
    id 2977
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 2978
    label "zaczyna&#263;"
  ]
  node [
    id 2979
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 2980
    label "escalate"
  ]
  node [
    id 2981
    label "pia&#263;"
  ]
  node [
    id 2982
    label "przybli&#380;a&#263;"
  ]
  node [
    id 2983
    label "ulepsza&#263;"
  ]
  node [
    id 2984
    label "tire"
  ]
  node [
    id 2985
    label "pomaga&#263;"
  ]
  node [
    id 2986
    label "liczy&#263;"
  ]
  node [
    id 2987
    label "express"
  ]
  node [
    id 2988
    label "przemieszcza&#263;"
  ]
  node [
    id 2989
    label "chwali&#263;"
  ]
  node [
    id 2990
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 2991
    label "os&#322;awia&#263;"
  ]
  node [
    id 2992
    label "odbudowywa&#263;"
  ]
  node [
    id 2993
    label "enhance"
  ]
  node [
    id 2994
    label "za&#322;apywa&#263;"
  ]
  node [
    id 2995
    label "lift"
  ]
  node [
    id 2996
    label "answer"
  ]
  node [
    id 2997
    label "odpowiada&#263;"
  ]
  node [
    id 2998
    label "organizowa&#263;"
  ]
  node [
    id 2999
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 3000
    label "czyni&#263;"
  ]
  node [
    id 3001
    label "stylizowa&#263;"
  ]
  node [
    id 3002
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 3003
    label "falowa&#263;"
  ]
  node [
    id 3004
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 3005
    label "peddle"
  ]
  node [
    id 3006
    label "wydala&#263;"
  ]
  node [
    id 3007
    label "tentegowa&#263;"
  ]
  node [
    id 3008
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 3009
    label "urz&#261;dza&#263;"
  ]
  node [
    id 3010
    label "oszukiwa&#263;"
  ]
  node [
    id 3011
    label "ukazywa&#263;"
  ]
  node [
    id 3012
    label "przerabia&#263;"
  ]
  node [
    id 3013
    label "post&#281;powa&#263;"
  ]
  node [
    id 3014
    label "traci&#263;"
  ]
  node [
    id 3015
    label "alternate"
  ]
  node [
    id 3016
    label "zast&#281;powa&#263;"
  ]
  node [
    id 3017
    label "sprawia&#263;"
  ]
  node [
    id 3018
    label "zyskiwa&#263;"
  ]
  node [
    id 3019
    label "przechodzi&#263;"
  ]
  node [
    id 3020
    label "zrelatywizowa&#263;"
  ]
  node [
    id 3021
    label "zrelatywizowanie"
  ]
  node [
    id 3022
    label "podporz&#261;dkowanie"
  ]
  node [
    id 3023
    label "niesamodzielno&#347;&#263;"
  ]
  node [
    id 3024
    label "status"
  ]
  node [
    id 3025
    label "relatywizowa&#263;"
  ]
  node [
    id 3026
    label "relatywizowanie"
  ]
  node [
    id 3027
    label "niezaradno&#347;&#263;"
  ]
  node [
    id 3028
    label "owini&#281;cie_wok&#243;&#322;_palca"
  ]
  node [
    id 3029
    label "wej&#347;cie_na_g&#322;ow&#281;"
  ]
  node [
    id 3030
    label "wej&#347;cie_na_&#322;eb"
  ]
  node [
    id 3031
    label "dopasowanie"
  ]
  node [
    id 3032
    label "subjugation"
  ]
  node [
    id 3033
    label "uzale&#380;nienie"
  ]
  node [
    id 3034
    label "condition"
  ]
  node [
    id 3035
    label "awans"
  ]
  node [
    id 3036
    label "podmiotowo"
  ]
  node [
    id 3037
    label "uzale&#380;nia&#263;"
  ]
  node [
    id 3038
    label "uzale&#380;ni&#263;"
  ]
  node [
    id 3039
    label "uzale&#380;nianie"
  ]
  node [
    id 3040
    label "fabrication"
  ]
  node [
    id 3041
    label "production"
  ]
  node [
    id 3042
    label "realizacja"
  ]
  node [
    id 3043
    label "dzie&#322;o"
  ]
  node [
    id 3044
    label "pojawienie_si&#281;"
  ]
  node [
    id 3045
    label "completion"
  ]
  node [
    id 3046
    label "ziszczenie_si&#281;"
  ]
  node [
    id 3047
    label "obrazowanie"
  ]
  node [
    id 3048
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 3049
    label "dorobek"
  ]
  node [
    id 3050
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 3051
    label "retrospektywa"
  ]
  node [
    id 3052
    label "works"
  ]
  node [
    id 3053
    label "tetralogia"
  ]
  node [
    id 3054
    label "komunikat"
  ]
  node [
    id 3055
    label "operacja"
  ]
  node [
    id 3056
    label "monta&#380;"
  ]
  node [
    id 3057
    label "postprodukcja"
  ]
  node [
    id 3058
    label "performance"
  ]
  node [
    id 3059
    label "p&#243;&#322;rocze"
  ]
  node [
    id 3060
    label "martwy_sezon"
  ]
  node [
    id 3061
    label "kalendarz"
  ]
  node [
    id 3062
    label "cykl_astronomiczny"
  ]
  node [
    id 3063
    label "lata"
  ]
  node [
    id 3064
    label "pora_roku"
  ]
  node [
    id 3065
    label "stulecie"
  ]
  node [
    id 3066
    label "kurs"
  ]
  node [
    id 3067
    label "jubileusz"
  ]
  node [
    id 3068
    label "kwarta&#322;"
  ]
  node [
    id 3069
    label "summer"
  ]
  node [
    id 3070
    label "rok_akademicki"
  ]
  node [
    id 3071
    label "rok_szkolny"
  ]
  node [
    id 3072
    label "semester"
  ]
  node [
    id 3073
    label "anniwersarz"
  ]
  node [
    id 3074
    label "rocznica"
  ]
  node [
    id 3075
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 3076
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 3077
    label "almanac"
  ]
  node [
    id 3078
    label "rozk&#322;ad"
  ]
  node [
    id 3079
    label "Juliusz_Cezar"
  ]
  node [
    id 3080
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 3081
    label "zwy&#380;kowanie"
  ]
  node [
    id 3082
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 3083
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 3084
    label "zaj&#281;cia"
  ]
  node [
    id 3085
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 3086
    label "trasa"
  ]
  node [
    id 3087
    label "przeorientowywanie"
  ]
  node [
    id 3088
    label "przejazd"
  ]
  node [
    id 3089
    label "przeorientowywa&#263;"
  ]
  node [
    id 3090
    label "nauka"
  ]
  node [
    id 3091
    label "przeorientowanie"
  ]
  node [
    id 3092
    label "klasa"
  ]
  node [
    id 3093
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 3094
    label "przeorientowa&#263;"
  ]
  node [
    id 3095
    label "manner"
  ]
  node [
    id 3096
    label "course"
  ]
  node [
    id 3097
    label "passage"
  ]
  node [
    id 3098
    label "zni&#380;kowanie"
  ]
  node [
    id 3099
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 3100
    label "seria"
  ]
  node [
    id 3101
    label "stawka"
  ]
  node [
    id 3102
    label "way"
  ]
  node [
    id 3103
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 3104
    label "deprecjacja"
  ]
  node [
    id 3105
    label "cedu&#322;a"
  ]
  node [
    id 3106
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 3107
    label "Lira"
  ]
  node [
    id 3108
    label "przesz&#322;y"
  ]
  node [
    id 3109
    label "wcze&#347;niejszy"
  ]
  node [
    id 3110
    label "poprzednio"
  ]
  node [
    id 3111
    label "miniony"
  ]
  node [
    id 3112
    label "ostatni"
  ]
  node [
    id 3113
    label "wcze&#347;niej"
  ]
  node [
    id 3114
    label "zbocze"
  ]
  node [
    id 3115
    label "przegroda"
  ]
  node [
    id 3116
    label "bariera"
  ]
  node [
    id 3117
    label "facebook"
  ]
  node [
    id 3118
    label "wielo&#347;cian"
  ]
  node [
    id 3119
    label "obstruction"
  ]
  node [
    id 3120
    label "wyrobisko"
  ]
  node [
    id 3121
    label "trudno&#347;&#263;"
  ]
  node [
    id 3122
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 3123
    label "wielok&#261;t"
  ]
  node [
    id 3124
    label "odcinek"
  ]
  node [
    id 3125
    label "strzelba"
  ]
  node [
    id 3126
    label "lufa"
  ]
  node [
    id 3127
    label "sponiewieranie"
  ]
  node [
    id 3128
    label "discipline"
  ]
  node [
    id 3129
    label "kr&#261;&#380;enie"
  ]
  node [
    id 3130
    label "sponiewiera&#263;"
  ]
  node [
    id 3131
    label "program_nauczania"
  ]
  node [
    id 3132
    label "wykopywa&#263;"
  ]
  node [
    id 3133
    label "wykopanie"
  ]
  node [
    id 3134
    label "&#347;piew"
  ]
  node [
    id 3135
    label "wykopywanie"
  ]
  node [
    id 3136
    label "hole"
  ]
  node [
    id 3137
    label "low"
  ]
  node [
    id 3138
    label "niski"
  ]
  node [
    id 3139
    label "depressive_disorder"
  ]
  node [
    id 3140
    label "d&#378;wi&#281;k"
  ]
  node [
    id 3141
    label "wykopa&#263;"
  ]
  node [
    id 3142
    label "za&#322;amanie"
  ]
  node [
    id 3143
    label "niezaawansowany"
  ]
  node [
    id 3144
    label "najwa&#380;niejszy"
  ]
  node [
    id 3145
    label "pocz&#261;tkowy"
  ]
  node [
    id 3146
    label "podstawowo"
  ]
  node [
    id 3147
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 3148
    label "establish"
  ]
  node [
    id 3149
    label "osnowa&#263;"
  ]
  node [
    id 3150
    label "przymocowa&#263;"
  ]
  node [
    id 3151
    label "wetkn&#261;&#263;"
  ]
  node [
    id 3152
    label "wetkni&#281;cie"
  ]
  node [
    id 3153
    label "przetkanie"
  ]
  node [
    id 3154
    label "anchor"
  ]
  node [
    id 3155
    label "przymocowanie"
  ]
  node [
    id 3156
    label "zaczerpni&#281;cie"
  ]
  node [
    id 3157
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 3158
    label "interposition"
  ]
  node [
    id 3159
    label "dzieci&#324;stwo"
  ]
  node [
    id 3160
    label "pocz&#261;tki"
  ]
  node [
    id 3161
    label "kontekst"
  ]
  node [
    id 3162
    label "system"
  ]
  node [
    id 3163
    label "metoda"
  ]
  node [
    id 3164
    label "wzorzec_projektowy"
  ]
  node [
    id 3165
    label "doktryna"
  ]
  node [
    id 3166
    label "wrinkle"
  ]
  node [
    id 3167
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 3168
    label "wyk&#322;adnik_pot&#281;gi"
  ]
  node [
    id 3169
    label "violence"
  ]
  node [
    id 3170
    label "podstawa_pot&#281;gi"
  ]
  node [
    id 3171
    label "potencja"
  ]
  node [
    id 3172
    label "iloczyn"
  ]
  node [
    id 3173
    label "confession"
  ]
  node [
    id 3174
    label "stwierdzenie"
  ]
  node [
    id 3175
    label "recognition"
  ]
  node [
    id 3176
    label "oznajmienie"
  ]
  node [
    id 3177
    label "obiecanie"
  ]
  node [
    id 3178
    label "cios"
  ]
  node [
    id 3179
    label "udost&#281;pnienie"
  ]
  node [
    id 3180
    label "rendition"
  ]
  node [
    id 3181
    label "wymienienie_si&#281;"
  ]
  node [
    id 3182
    label "eating"
  ]
  node [
    id 3183
    label "coup"
  ]
  node [
    id 3184
    label "hand"
  ]
  node [
    id 3185
    label "uprawianie_seksu"
  ]
  node [
    id 3186
    label "allow"
  ]
  node [
    id 3187
    label "dostarczenie"
  ]
  node [
    id 3188
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 3189
    label "uderzenie"
  ]
  node [
    id 3190
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 3191
    label "przekazanie"
  ]
  node [
    id 3192
    label "odst&#261;pienie"
  ]
  node [
    id 3193
    label "dodanie"
  ]
  node [
    id 3194
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 3195
    label "wyposa&#380;enie"
  ]
  node [
    id 3196
    label "dostanie"
  ]
  node [
    id 3197
    label "potrawa"
  ]
  node [
    id 3198
    label "uderzanie"
  ]
  node [
    id 3199
    label "wyposa&#380;anie"
  ]
  node [
    id 3200
    label "pobicie"
  ]
  node [
    id 3201
    label "posi&#322;ek"
  ]
  node [
    id 3202
    label "claim"
  ]
  node [
    id 3203
    label "wypowiedzenie"
  ]
  node [
    id 3204
    label "manifesto"
  ]
  node [
    id 3205
    label "zwiastowanie"
  ]
  node [
    id 3206
    label "apel"
  ]
  node [
    id 3207
    label "Manifest_lipcowy"
  ]
  node [
    id 3208
    label "kartka"
  ]
  node [
    id 3209
    label "uczestnictwo"
  ]
  node [
    id 3210
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 3211
    label "input"
  ]
  node [
    id 3212
    label "lokata"
  ]
  node [
    id 3213
    label "r&#243;&#380;niczka"
  ]
  node [
    id 3214
    label "&#347;rodowisko"
  ]
  node [
    id 3215
    label "materia"
  ]
  node [
    id 3216
    label "szambo"
  ]
  node [
    id 3217
    label "aspo&#322;eczny"
  ]
  node [
    id 3218
    label "component"
  ]
  node [
    id 3219
    label "szkodnik"
  ]
  node [
    id 3220
    label "gangsterski"
  ]
  node [
    id 3221
    label "underworld"
  ]
  node [
    id 3222
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 3223
    label "depozyt"
  ]
  node [
    id 3224
    label "inwestycja"
  ]
  node [
    id 3225
    label "faul"
  ]
  node [
    id 3226
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 3227
    label "s&#281;dzia"
  ]
  node [
    id 3228
    label "bon"
  ]
  node [
    id 3229
    label "ticket"
  ]
  node [
    id 3230
    label "arkusz"
  ]
  node [
    id 3231
    label "kartonik"
  ]
  node [
    id 3232
    label "kara"
  ]
  node [
    id 3233
    label "rozdzia&#322;"
  ]
  node [
    id 3234
    label "tytu&#322;"
  ]
  node [
    id 3235
    label "nomina&#322;"
  ]
  node [
    id 3236
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 3237
    label "ekslibris"
  ]
  node [
    id 3238
    label "przek&#322;adacz"
  ]
  node [
    id 3239
    label "bibliofilstwo"
  ]
  node [
    id 3240
    label "falc"
  ]
  node [
    id 3241
    label "pagina"
  ]
  node [
    id 3242
    label "zw&#243;j"
  ]
  node [
    id 3243
    label "impression"
  ]
  node [
    id 3244
    label "publikacja"
  ]
  node [
    id 3245
    label "kajet"
  ]
  node [
    id 3246
    label "oprawa"
  ]
  node [
    id 3247
    label "boarding"
  ]
  node [
    id 3248
    label "oprawianie"
  ]
  node [
    id 3249
    label "os&#322;ona"
  ]
  node [
    id 3250
    label "oprawia&#263;"
  ]
  node [
    id 3251
    label "gramatyka"
  ]
  node [
    id 3252
    label "kod"
  ]
  node [
    id 3253
    label "przet&#322;umaczenie"
  ]
  node [
    id 3254
    label "konsonantyzm"
  ]
  node [
    id 3255
    label "wokalizm"
  ]
  node [
    id 3256
    label "fonetyka"
  ]
  node [
    id 3257
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 3258
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 3259
    label "po_koroniarsku"
  ]
  node [
    id 3260
    label "t&#322;umaczenie"
  ]
  node [
    id 3261
    label "rozumie&#263;"
  ]
  node [
    id 3262
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 3263
    label "rozumienie"
  ]
  node [
    id 3264
    label "address"
  ]
  node [
    id 3265
    label "komunikacja"
  ]
  node [
    id 3266
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 3267
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 3268
    label "s&#322;ownictwo"
  ]
  node [
    id 3269
    label "tongue"
  ]
  node [
    id 3270
    label "pos&#322;uchanie"
  ]
  node [
    id 3271
    label "sparafrazowanie"
  ]
  node [
    id 3272
    label "strawestowa&#263;"
  ]
  node [
    id 3273
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 3274
    label "trawestowa&#263;"
  ]
  node [
    id 3275
    label "sparafrazowa&#263;"
  ]
  node [
    id 3276
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 3277
    label "sformu&#322;owanie"
  ]
  node [
    id 3278
    label "parafrazowanie"
  ]
  node [
    id 3279
    label "ozdobnik"
  ]
  node [
    id 3280
    label "delimitacja"
  ]
  node [
    id 3281
    label "parafrazowa&#263;"
  ]
  node [
    id 3282
    label "stylizacja"
  ]
  node [
    id 3283
    label "trawestowanie"
  ]
  node [
    id 3284
    label "strawestowanie"
  ]
  node [
    id 3285
    label "posiada&#263;"
  ]
  node [
    id 3286
    label "potencja&#322;"
  ]
  node [
    id 3287
    label "zapomnienie"
  ]
  node [
    id 3288
    label "zapomina&#263;"
  ]
  node [
    id 3289
    label "zapominanie"
  ]
  node [
    id 3290
    label "ability"
  ]
  node [
    id 3291
    label "obliczeniowo"
  ]
  node [
    id 3292
    label "zapomnie&#263;"
  ]
  node [
    id 3293
    label "transportation_system"
  ]
  node [
    id 3294
    label "explicite"
  ]
  node [
    id 3295
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 3296
    label "wydeptywanie"
  ]
  node [
    id 3297
    label "wydeptanie"
  ]
  node [
    id 3298
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 3299
    label "implicite"
  ]
  node [
    id 3300
    label "ekspedytor"
  ]
  node [
    id 3301
    label "language"
  ]
  node [
    id 3302
    label "code"
  ]
  node [
    id 3303
    label "szyfrowanie"
  ]
  node [
    id 3304
    label "ci&#261;g"
  ]
  node [
    id 3305
    label "szablon"
  ]
  node [
    id 3306
    label "public_speaking"
  ]
  node [
    id 3307
    label "powiadanie"
  ]
  node [
    id 3308
    label "przepowiadanie"
  ]
  node [
    id 3309
    label "wyznawanie"
  ]
  node [
    id 3310
    label "wypowiadanie"
  ]
  node [
    id 3311
    label "gaworzenie"
  ]
  node [
    id 3312
    label "formu&#322;owanie"
  ]
  node [
    id 3313
    label "dowalenie"
  ]
  node [
    id 3314
    label "przerywanie"
  ]
  node [
    id 3315
    label "dogadywanie_si&#281;"
  ]
  node [
    id 3316
    label "dodawanie"
  ]
  node [
    id 3317
    label "prawienie"
  ]
  node [
    id 3318
    label "opowiadanie"
  ]
  node [
    id 3319
    label "ozywanie_si&#281;"
  ]
  node [
    id 3320
    label "zapeszanie"
  ]
  node [
    id 3321
    label "zwracanie_si&#281;"
  ]
  node [
    id 3322
    label "dysfonia"
  ]
  node [
    id 3323
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 3324
    label "speaking"
  ]
  node [
    id 3325
    label "zauwa&#380;enie"
  ]
  node [
    id 3326
    label "mawianie"
  ]
  node [
    id 3327
    label "opowiedzenie"
  ]
  node [
    id 3328
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 3329
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 3330
    label "informowanie"
  ]
  node [
    id 3331
    label "dogadanie_si&#281;"
  ]
  node [
    id 3332
    label "wygadanie"
  ]
  node [
    id 3333
    label "handwriting"
  ]
  node [
    id 3334
    label "przekaz"
  ]
  node [
    id 3335
    label "paleograf"
  ]
  node [
    id 3336
    label "interpunkcja"
  ]
  node [
    id 3337
    label "grafia"
  ]
  node [
    id 3338
    label "script"
  ]
  node [
    id 3339
    label "list"
  ]
  node [
    id 3340
    label "adres"
  ]
  node [
    id 3341
    label "ortografia"
  ]
  node [
    id 3342
    label "letter"
  ]
  node [
    id 3343
    label "paleografia"
  ]
  node [
    id 3344
    label "terminology"
  ]
  node [
    id 3345
    label "fleksja"
  ]
  node [
    id 3346
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 3347
    label "sk&#322;adnia"
  ]
  node [
    id 3348
    label "morfologia"
  ]
  node [
    id 3349
    label "g&#322;osownia"
  ]
  node [
    id 3350
    label "zasymilowa&#263;"
  ]
  node [
    id 3351
    label "phonetics"
  ]
  node [
    id 3352
    label "palatogram"
  ]
  node [
    id 3353
    label "transkrypcja"
  ]
  node [
    id 3354
    label "zasymilowanie"
  ]
  node [
    id 3355
    label "explanation"
  ]
  node [
    id 3356
    label "bronienie"
  ]
  node [
    id 3357
    label "remark"
  ]
  node [
    id 3358
    label "przek&#322;adanie"
  ]
  node [
    id 3359
    label "zrozumia&#322;y"
  ]
  node [
    id 3360
    label "przekonywanie"
  ]
  node [
    id 3361
    label "uzasadnianie"
  ]
  node [
    id 3362
    label "rozwianie"
  ]
  node [
    id 3363
    label "rozwiewanie"
  ]
  node [
    id 3364
    label "gossip"
  ]
  node [
    id 3365
    label "kr&#281;ty"
  ]
  node [
    id 3366
    label "zinterpretowa&#263;"
  ]
  node [
    id 3367
    label "put"
  ]
  node [
    id 3368
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 3369
    label "przekona&#263;"
  ]
  node [
    id 3370
    label "frame"
  ]
  node [
    id 3371
    label "poja&#347;nia&#263;"
  ]
  node [
    id 3372
    label "u&#322;atwia&#263;"
  ]
  node [
    id 3373
    label "elaborate"
  ]
  node [
    id 3374
    label "suplikowa&#263;"
  ]
  node [
    id 3375
    label "przek&#322;ada&#263;"
  ]
  node [
    id 3376
    label "przekonywa&#263;"
  ]
  node [
    id 3377
    label "interpretowa&#263;"
  ]
  node [
    id 3378
    label "broni&#263;"
  ]
  node [
    id 3379
    label "explain"
  ]
  node [
    id 3380
    label "sprawowa&#263;"
  ]
  node [
    id 3381
    label "uzasadnia&#263;"
  ]
  node [
    id 3382
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 3383
    label "gaworzy&#263;"
  ]
  node [
    id 3384
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 3385
    label "rozmawia&#263;"
  ]
  node [
    id 3386
    label "umie&#263;"
  ]
  node [
    id 3387
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 3388
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 3389
    label "formu&#322;owa&#263;"
  ]
  node [
    id 3390
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 3391
    label "talk"
  ]
  node [
    id 3392
    label "prawi&#263;"
  ]
  node [
    id 3393
    label "powiada&#263;"
  ]
  node [
    id 3394
    label "tell"
  ]
  node [
    id 3395
    label "chew_the_fat"
  ]
  node [
    id 3396
    label "say"
  ]
  node [
    id 3397
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 3398
    label "informowa&#263;"
  ]
  node [
    id 3399
    label "okre&#347;la&#263;"
  ]
  node [
    id 3400
    label "hermeneutyka"
  ]
  node [
    id 3401
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 3402
    label "interpretation"
  ]
  node [
    id 3403
    label "czucie"
  ]
  node [
    id 3404
    label "realization"
  ]
  node [
    id 3405
    label "kumanie"
  ]
  node [
    id 3406
    label "wiedzie&#263;"
  ]
  node [
    id 3407
    label "kuma&#263;"
  ]
  node [
    id 3408
    label "czu&#263;"
  ]
  node [
    id 3409
    label "empatia"
  ]
  node [
    id 3410
    label "see"
  ]
  node [
    id 3411
    label "zna&#263;"
  ]
  node [
    id 3412
    label "rozporz&#261;dzi&#263;"
  ]
  node [
    id 3413
    label "rada"
  ]
  node [
    id 3414
    label "we"
  ]
  node [
    id 3415
    label "Euratom"
  ]
  node [
    id 3416
    label "nr"
  ]
  node [
    id 3417
    label "1605"
  ]
  node [
    id 3418
    label "2002"
  ]
  node [
    id 3419
    label "25"
  ]
  node [
    id 3420
    label "wyspa"
  ]
  node [
    id 3421
    label "mie&#263;"
  ]
  node [
    id 3422
    label "zastosowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 7
    target 596
  ]
  edge [
    source 7
    target 597
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 7
    target 599
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 600
  ]
  edge [
    source 7
    target 601
  ]
  edge [
    source 7
    target 602
  ]
  edge [
    source 7
    target 603
  ]
  edge [
    source 7
    target 604
  ]
  edge [
    source 7
    target 605
  ]
  edge [
    source 7
    target 606
  ]
  edge [
    source 7
    target 607
  ]
  edge [
    source 7
    target 608
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 609
  ]
  edge [
    source 7
    target 610
  ]
  edge [
    source 7
    target 611
  ]
  edge [
    source 7
    target 612
  ]
  edge [
    source 7
    target 613
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 615
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 616
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 617
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 619
  ]
  edge [
    source 7
    target 620
  ]
  edge [
    source 7
    target 621
  ]
  edge [
    source 7
    target 622
  ]
  edge [
    source 7
    target 623
  ]
  edge [
    source 7
    target 624
  ]
  edge [
    source 7
    target 625
  ]
  edge [
    source 7
    target 626
  ]
  edge [
    source 7
    target 627
  ]
  edge [
    source 7
    target 628
  ]
  edge [
    source 7
    target 629
  ]
  edge [
    source 7
    target 630
  ]
  edge [
    source 7
    target 631
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 7
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 8
    target 829
  ]
  edge [
    source 8
    target 830
  ]
  edge [
    source 8
    target 831
  ]
  edge [
    source 8
    target 832
  ]
  edge [
    source 8
    target 833
  ]
  edge [
    source 8
    target 834
  ]
  edge [
    source 8
    target 835
  ]
  edge [
    source 8
    target 836
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 837
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 838
  ]
  edge [
    source 8
    target 839
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 840
  ]
  edge [
    source 8
    target 841
  ]
  edge [
    source 8
    target 842
  ]
  edge [
    source 8
    target 843
  ]
  edge [
    source 8
    target 844
  ]
  edge [
    source 8
    target 845
  ]
  edge [
    source 8
    target 846
  ]
  edge [
    source 8
    target 847
  ]
  edge [
    source 8
    target 848
  ]
  edge [
    source 8
    target 849
  ]
  edge [
    source 8
    target 850
  ]
  edge [
    source 8
    target 851
  ]
  edge [
    source 8
    target 852
  ]
  edge [
    source 8
    target 853
  ]
  edge [
    source 8
    target 854
  ]
  edge [
    source 8
    target 855
  ]
  edge [
    source 8
    target 856
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 857
  ]
  edge [
    source 8
    target 858
  ]
  edge [
    source 8
    target 859
  ]
  edge [
    source 8
    target 860
  ]
  edge [
    source 8
    target 861
  ]
  edge [
    source 8
    target 862
  ]
  edge [
    source 8
    target 863
  ]
  edge [
    source 8
    target 864
  ]
  edge [
    source 8
    target 865
  ]
  edge [
    source 8
    target 866
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 867
  ]
  edge [
    source 8
    target 868
  ]
  edge [
    source 8
    target 869
  ]
  edge [
    source 8
    target 870
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 871
  ]
  edge [
    source 8
    target 872
  ]
  edge [
    source 8
    target 873
  ]
  edge [
    source 8
    target 874
  ]
  edge [
    source 8
    target 875
  ]
  edge [
    source 8
    target 876
  ]
  edge [
    source 8
    target 877
  ]
  edge [
    source 8
    target 878
  ]
  edge [
    source 8
    target 879
  ]
  edge [
    source 8
    target 880
  ]
  edge [
    source 8
    target 881
  ]
  edge [
    source 8
    target 882
  ]
  edge [
    source 8
    target 883
  ]
  edge [
    source 8
    target 884
  ]
  edge [
    source 8
    target 885
  ]
  edge [
    source 8
    target 886
  ]
  edge [
    source 8
    target 887
  ]
  edge [
    source 8
    target 888
  ]
  edge [
    source 8
    target 889
  ]
  edge [
    source 8
    target 890
  ]
  edge [
    source 8
    target 891
  ]
  edge [
    source 8
    target 892
  ]
  edge [
    source 8
    target 893
  ]
  edge [
    source 8
    target 894
  ]
  edge [
    source 8
    target 895
  ]
  edge [
    source 8
    target 896
  ]
  edge [
    source 8
    target 897
  ]
  edge [
    source 8
    target 898
  ]
  edge [
    source 8
    target 899
  ]
  edge [
    source 8
    target 900
  ]
  edge [
    source 8
    target 901
  ]
  edge [
    source 8
    target 902
  ]
  edge [
    source 8
    target 903
  ]
  edge [
    source 8
    target 904
  ]
  edge [
    source 8
    target 905
  ]
  edge [
    source 8
    target 906
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 907
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 908
  ]
  edge [
    source 8
    target 909
  ]
  edge [
    source 8
    target 910
  ]
  edge [
    source 8
    target 911
  ]
  edge [
    source 8
    target 912
  ]
  edge [
    source 8
    target 913
  ]
  edge [
    source 8
    target 914
  ]
  edge [
    source 8
    target 915
  ]
  edge [
    source 8
    target 916
  ]
  edge [
    source 8
    target 917
  ]
  edge [
    source 8
    target 918
  ]
  edge [
    source 8
    target 919
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 920
  ]
  edge [
    source 8
    target 921
  ]
  edge [
    source 8
    target 922
  ]
  edge [
    source 8
    target 923
  ]
  edge [
    source 8
    target 924
  ]
  edge [
    source 8
    target 925
  ]
  edge [
    source 8
    target 926
  ]
  edge [
    source 8
    target 927
  ]
  edge [
    source 8
    target 928
  ]
  edge [
    source 8
    target 929
  ]
  edge [
    source 8
    target 930
  ]
  edge [
    source 8
    target 931
  ]
  edge [
    source 8
    target 932
  ]
  edge [
    source 8
    target 933
  ]
  edge [
    source 8
    target 934
  ]
  edge [
    source 8
    target 935
  ]
  edge [
    source 8
    target 936
  ]
  edge [
    source 8
    target 937
  ]
  edge [
    source 8
    target 938
  ]
  edge [
    source 8
    target 939
  ]
  edge [
    source 8
    target 940
  ]
  edge [
    source 8
    target 941
  ]
  edge [
    source 8
    target 942
  ]
  edge [
    source 8
    target 943
  ]
  edge [
    source 8
    target 944
  ]
  edge [
    source 8
    target 945
  ]
  edge [
    source 8
    target 946
  ]
  edge [
    source 8
    target 947
  ]
  edge [
    source 8
    target 948
  ]
  edge [
    source 8
    target 949
  ]
  edge [
    source 8
    target 950
  ]
  edge [
    source 8
    target 951
  ]
  edge [
    source 8
    target 952
  ]
  edge [
    source 8
    target 953
  ]
  edge [
    source 8
    target 954
  ]
  edge [
    source 8
    target 955
  ]
  edge [
    source 8
    target 956
  ]
  edge [
    source 8
    target 957
  ]
  edge [
    source 8
    target 958
  ]
  edge [
    source 8
    target 959
  ]
  edge [
    source 8
    target 960
  ]
  edge [
    source 8
    target 961
  ]
  edge [
    source 8
    target 962
  ]
  edge [
    source 8
    target 963
  ]
  edge [
    source 8
    target 964
  ]
  edge [
    source 8
    target 965
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 966
  ]
  edge [
    source 8
    target 967
  ]
  edge [
    source 8
    target 968
  ]
  edge [
    source 8
    target 969
  ]
  edge [
    source 8
    target 970
  ]
  edge [
    source 8
    target 971
  ]
  edge [
    source 8
    target 972
  ]
  edge [
    source 8
    target 973
  ]
  edge [
    source 8
    target 974
  ]
  edge [
    source 8
    target 975
  ]
  edge [
    source 8
    target 976
  ]
  edge [
    source 8
    target 977
  ]
  edge [
    source 8
    target 978
  ]
  edge [
    source 8
    target 979
  ]
  edge [
    source 8
    target 980
  ]
  edge [
    source 8
    target 981
  ]
  edge [
    source 8
    target 982
  ]
  edge [
    source 8
    target 983
  ]
  edge [
    source 8
    target 984
  ]
  edge [
    source 8
    target 985
  ]
  edge [
    source 8
    target 986
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 987
  ]
  edge [
    source 8
    target 988
  ]
  edge [
    source 8
    target 989
  ]
  edge [
    source 8
    target 990
  ]
  edge [
    source 8
    target 991
  ]
  edge [
    source 8
    target 992
  ]
  edge [
    source 8
    target 993
  ]
  edge [
    source 8
    target 994
  ]
  edge [
    source 8
    target 995
  ]
  edge [
    source 8
    target 996
  ]
  edge [
    source 8
    target 997
  ]
  edge [
    source 8
    target 998
  ]
  edge [
    source 8
    target 999
  ]
  edge [
    source 8
    target 1000
  ]
  edge [
    source 8
    target 1001
  ]
  edge [
    source 8
    target 1002
  ]
  edge [
    source 8
    target 1003
  ]
  edge [
    source 8
    target 1004
  ]
  edge [
    source 8
    target 1005
  ]
  edge [
    source 8
    target 1006
  ]
  edge [
    source 8
    target 1007
  ]
  edge [
    source 8
    target 1008
  ]
  edge [
    source 8
    target 1009
  ]
  edge [
    source 8
    target 1010
  ]
  edge [
    source 8
    target 1011
  ]
  edge [
    source 8
    target 1012
  ]
  edge [
    source 8
    target 1013
  ]
  edge [
    source 8
    target 1014
  ]
  edge [
    source 8
    target 1015
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 1016
  ]
  edge [
    source 8
    target 1017
  ]
  edge [
    source 8
    target 1018
  ]
  edge [
    source 8
    target 1019
  ]
  edge [
    source 8
    target 1020
  ]
  edge [
    source 8
    target 1021
  ]
  edge [
    source 8
    target 1022
  ]
  edge [
    source 8
    target 1023
  ]
  edge [
    source 8
    target 1024
  ]
  edge [
    source 8
    target 1025
  ]
  edge [
    source 8
    target 1026
  ]
  edge [
    source 8
    target 1027
  ]
  edge [
    source 8
    target 1028
  ]
  edge [
    source 8
    target 1029
  ]
  edge [
    source 8
    target 1030
  ]
  edge [
    source 8
    target 1031
  ]
  edge [
    source 8
    target 1032
  ]
  edge [
    source 8
    target 1033
  ]
  edge [
    source 8
    target 1034
  ]
  edge [
    source 8
    target 1035
  ]
  edge [
    source 8
    target 1036
  ]
  edge [
    source 8
    target 1037
  ]
  edge [
    source 8
    target 1038
  ]
  edge [
    source 8
    target 1039
  ]
  edge [
    source 8
    target 1040
  ]
  edge [
    source 8
    target 1041
  ]
  edge [
    source 8
    target 1042
  ]
  edge [
    source 8
    target 1043
  ]
  edge [
    source 8
    target 1044
  ]
  edge [
    source 8
    target 1045
  ]
  edge [
    source 8
    target 1046
  ]
  edge [
    source 8
    target 1047
  ]
  edge [
    source 8
    target 1048
  ]
  edge [
    source 8
    target 1049
  ]
  edge [
    source 8
    target 1050
  ]
  edge [
    source 8
    target 1051
  ]
  edge [
    source 8
    target 1052
  ]
  edge [
    source 8
    target 1053
  ]
  edge [
    source 8
    target 1054
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 1055
  ]
  edge [
    source 8
    target 1056
  ]
  edge [
    source 8
    target 1057
  ]
  edge [
    source 8
    target 1058
  ]
  edge [
    source 8
    target 1059
  ]
  edge [
    source 8
    target 1060
  ]
  edge [
    source 8
    target 1061
  ]
  edge [
    source 8
    target 1062
  ]
  edge [
    source 8
    target 1063
  ]
  edge [
    source 8
    target 1064
  ]
  edge [
    source 8
    target 1065
  ]
  edge [
    source 8
    target 1066
  ]
  edge [
    source 8
    target 1067
  ]
  edge [
    source 8
    target 1068
  ]
  edge [
    source 8
    target 1069
  ]
  edge [
    source 8
    target 1070
  ]
  edge [
    source 8
    target 1071
  ]
  edge [
    source 8
    target 1072
  ]
  edge [
    source 8
    target 1073
  ]
  edge [
    source 8
    target 1074
  ]
  edge [
    source 8
    target 1075
  ]
  edge [
    source 8
    target 1076
  ]
  edge [
    source 8
    target 1077
  ]
  edge [
    source 8
    target 1078
  ]
  edge [
    source 8
    target 1079
  ]
  edge [
    source 8
    target 1080
  ]
  edge [
    source 8
    target 1081
  ]
  edge [
    source 8
    target 1082
  ]
  edge [
    source 8
    target 1083
  ]
  edge [
    source 8
    target 1084
  ]
  edge [
    source 8
    target 1085
  ]
  edge [
    source 8
    target 1086
  ]
  edge [
    source 8
    target 1087
  ]
  edge [
    source 8
    target 1088
  ]
  edge [
    source 8
    target 1089
  ]
  edge [
    source 8
    target 1090
  ]
  edge [
    source 8
    target 1091
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 1092
  ]
  edge [
    source 8
    target 1093
  ]
  edge [
    source 8
    target 1094
  ]
  edge [
    source 8
    target 1095
  ]
  edge [
    source 8
    target 1096
  ]
  edge [
    source 8
    target 1097
  ]
  edge [
    source 8
    target 1098
  ]
  edge [
    source 8
    target 1099
  ]
  edge [
    source 8
    target 1100
  ]
  edge [
    source 8
    target 1101
  ]
  edge [
    source 8
    target 1102
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 1103
  ]
  edge [
    source 8
    target 1104
  ]
  edge [
    source 8
    target 1105
  ]
  edge [
    source 8
    target 1106
  ]
  edge [
    source 8
    target 1107
  ]
  edge [
    source 8
    target 1108
  ]
  edge [
    source 8
    target 1109
  ]
  edge [
    source 8
    target 1110
  ]
  edge [
    source 8
    target 1111
  ]
  edge [
    source 8
    target 1112
  ]
  edge [
    source 8
    target 1113
  ]
  edge [
    source 8
    target 1114
  ]
  edge [
    source 8
    target 1115
  ]
  edge [
    source 8
    target 1116
  ]
  edge [
    source 8
    target 1117
  ]
  edge [
    source 8
    target 1118
  ]
  edge [
    source 8
    target 1119
  ]
  edge [
    source 8
    target 1120
  ]
  edge [
    source 8
    target 1121
  ]
  edge [
    source 8
    target 1122
  ]
  edge [
    source 8
    target 1123
  ]
  edge [
    source 8
    target 1124
  ]
  edge [
    source 8
    target 1125
  ]
  edge [
    source 8
    target 1126
  ]
  edge [
    source 8
    target 1127
  ]
  edge [
    source 8
    target 1128
  ]
  edge [
    source 8
    target 1129
  ]
  edge [
    source 8
    target 1130
  ]
  edge [
    source 8
    target 1131
  ]
  edge [
    source 8
    target 1132
  ]
  edge [
    source 8
    target 1133
  ]
  edge [
    source 8
    target 1134
  ]
  edge [
    source 8
    target 1135
  ]
  edge [
    source 8
    target 1136
  ]
  edge [
    source 8
    target 1137
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1138
  ]
  edge [
    source 10
    target 1139
  ]
  edge [
    source 10
    target 1140
  ]
  edge [
    source 10
    target 1141
  ]
  edge [
    source 10
    target 1142
  ]
  edge [
    source 10
    target 1143
  ]
  edge [
    source 10
    target 1144
  ]
  edge [
    source 10
    target 1145
  ]
  edge [
    source 10
    target 1146
  ]
  edge [
    source 10
    target 1147
  ]
  edge [
    source 10
    target 1148
  ]
  edge [
    source 10
    target 1149
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 1150
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 1151
  ]
  edge [
    source 10
    target 1152
  ]
  edge [
    source 10
    target 1153
  ]
  edge [
    source 10
    target 1154
  ]
  edge [
    source 10
    target 1155
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 48
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1156
  ]
  edge [
    source 12
    target 1157
  ]
  edge [
    source 12
    target 1158
  ]
  edge [
    source 12
    target 1159
  ]
  edge [
    source 12
    target 1160
  ]
  edge [
    source 12
    target 1161
  ]
  edge [
    source 12
    target 1162
  ]
  edge [
    source 12
    target 1163
  ]
  edge [
    source 12
    target 1164
  ]
  edge [
    source 12
    target 1165
  ]
  edge [
    source 12
    target 1166
  ]
  edge [
    source 12
    target 1167
  ]
  edge [
    source 12
    target 1168
  ]
  edge [
    source 12
    target 1169
  ]
  edge [
    source 12
    target 1170
  ]
  edge [
    source 12
    target 1171
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 1172
  ]
  edge [
    source 12
    target 1173
  ]
  edge [
    source 12
    target 1174
  ]
  edge [
    source 12
    target 1175
  ]
  edge [
    source 12
    target 1176
  ]
  edge [
    source 12
    target 1177
  ]
  edge [
    source 12
    target 1178
  ]
  edge [
    source 12
    target 1179
  ]
  edge [
    source 12
    target 1180
  ]
  edge [
    source 12
    target 1181
  ]
  edge [
    source 12
    target 1182
  ]
  edge [
    source 12
    target 1183
  ]
  edge [
    source 12
    target 1184
  ]
  edge [
    source 12
    target 1185
  ]
  edge [
    source 12
    target 1186
  ]
  edge [
    source 12
    target 1187
  ]
  edge [
    source 12
    target 1188
  ]
  edge [
    source 12
    target 1189
  ]
  edge [
    source 12
    target 1190
  ]
  edge [
    source 12
    target 1191
  ]
  edge [
    source 12
    target 1192
  ]
  edge [
    source 12
    target 1193
  ]
  edge [
    source 12
    target 1194
  ]
  edge [
    source 12
    target 1195
  ]
  edge [
    source 12
    target 1196
  ]
  edge [
    source 12
    target 1197
  ]
  edge [
    source 12
    target 1198
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 1227
  ]
  edge [
    source 12
    target 1228
  ]
  edge [
    source 12
    target 1229
  ]
  edge [
    source 12
    target 1230
  ]
  edge [
    source 12
    target 1231
  ]
  edge [
    source 12
    target 1232
  ]
  edge [
    source 12
    target 1233
  ]
  edge [
    source 12
    target 1234
  ]
  edge [
    source 12
    target 1235
  ]
  edge [
    source 12
    target 1236
  ]
  edge [
    source 12
    target 1237
  ]
  edge [
    source 12
    target 1238
  ]
  edge [
    source 12
    target 1239
  ]
  edge [
    source 12
    target 1240
  ]
  edge [
    source 12
    target 1241
  ]
  edge [
    source 12
    target 1242
  ]
  edge [
    source 12
    target 1243
  ]
  edge [
    source 12
    target 1244
  ]
  edge [
    source 12
    target 1245
  ]
  edge [
    source 12
    target 1246
  ]
  edge [
    source 12
    target 1247
  ]
  edge [
    source 12
    target 1248
  ]
  edge [
    source 12
    target 1249
  ]
  edge [
    source 12
    target 1250
  ]
  edge [
    source 12
    target 1251
  ]
  edge [
    source 12
    target 1252
  ]
  edge [
    source 12
    target 1253
  ]
  edge [
    source 12
    target 1254
  ]
  edge [
    source 12
    target 1255
  ]
  edge [
    source 12
    target 1256
  ]
  edge [
    source 12
    target 1257
  ]
  edge [
    source 12
    target 1258
  ]
  edge [
    source 12
    target 1259
  ]
  edge [
    source 12
    target 1260
  ]
  edge [
    source 12
    target 1261
  ]
  edge [
    source 12
    target 1262
  ]
  edge [
    source 12
    target 1263
  ]
  edge [
    source 12
    target 1264
  ]
  edge [
    source 12
    target 1265
  ]
  edge [
    source 12
    target 1266
  ]
  edge [
    source 12
    target 1267
  ]
  edge [
    source 12
    target 1268
  ]
  edge [
    source 12
    target 1269
  ]
  edge [
    source 12
    target 1270
  ]
  edge [
    source 12
    target 1271
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 12
    target 1301
  ]
  edge [
    source 12
    target 1302
  ]
  edge [
    source 12
    target 1303
  ]
  edge [
    source 12
    target 1304
  ]
  edge [
    source 12
    target 1305
  ]
  edge [
    source 12
    target 1306
  ]
  edge [
    source 12
    target 1307
  ]
  edge [
    source 12
    target 1308
  ]
  edge [
    source 12
    target 1309
  ]
  edge [
    source 12
    target 1310
  ]
  edge [
    source 12
    target 1311
  ]
  edge [
    source 12
    target 1312
  ]
  edge [
    source 12
    target 1313
  ]
  edge [
    source 12
    target 1314
  ]
  edge [
    source 12
    target 1315
  ]
  edge [
    source 12
    target 1316
  ]
  edge [
    source 12
    target 1317
  ]
  edge [
    source 12
    target 1318
  ]
  edge [
    source 12
    target 1319
  ]
  edge [
    source 12
    target 1320
  ]
  edge [
    source 12
    target 1321
  ]
  edge [
    source 12
    target 1322
  ]
  edge [
    source 12
    target 1323
  ]
  edge [
    source 12
    target 1324
  ]
  edge [
    source 12
    target 1325
  ]
  edge [
    source 12
    target 1326
  ]
  edge [
    source 12
    target 1327
  ]
  edge [
    source 12
    target 1328
  ]
  edge [
    source 12
    target 1329
  ]
  edge [
    source 12
    target 1330
  ]
  edge [
    source 12
    target 1331
  ]
  edge [
    source 12
    target 1332
  ]
  edge [
    source 12
    target 1333
  ]
  edge [
    source 12
    target 1334
  ]
  edge [
    source 12
    target 1335
  ]
  edge [
    source 12
    target 1336
  ]
  edge [
    source 12
    target 1337
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 1338
  ]
  edge [
    source 12
    target 1339
  ]
  edge [
    source 12
    target 1340
  ]
  edge [
    source 12
    target 1341
  ]
  edge [
    source 12
    target 1342
  ]
  edge [
    source 12
    target 1343
  ]
  edge [
    source 12
    target 1344
  ]
  edge [
    source 12
    target 1345
  ]
  edge [
    source 12
    target 1346
  ]
  edge [
    source 12
    target 1347
  ]
  edge [
    source 12
    target 1348
  ]
  edge [
    source 12
    target 1349
  ]
  edge [
    source 12
    target 1350
  ]
  edge [
    source 12
    target 1351
  ]
  edge [
    source 12
    target 1352
  ]
  edge [
    source 12
    target 1353
  ]
  edge [
    source 12
    target 1354
  ]
  edge [
    source 12
    target 1355
  ]
  edge [
    source 12
    target 1356
  ]
  edge [
    source 12
    target 1357
  ]
  edge [
    source 12
    target 1358
  ]
  edge [
    source 12
    target 1359
  ]
  edge [
    source 12
    target 1360
  ]
  edge [
    source 12
    target 1361
  ]
  edge [
    source 12
    target 1362
  ]
  edge [
    source 12
    target 1363
  ]
  edge [
    source 12
    target 1364
  ]
  edge [
    source 12
    target 1365
  ]
  edge [
    source 12
    target 1366
  ]
  edge [
    source 12
    target 1367
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 1368
  ]
  edge [
    source 12
    target 1369
  ]
  edge [
    source 12
    target 1370
  ]
  edge [
    source 12
    target 1371
  ]
  edge [
    source 12
    target 1372
  ]
  edge [
    source 12
    target 1373
  ]
  edge [
    source 12
    target 1374
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 1375
  ]
  edge [
    source 12
    target 1376
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 1377
  ]
  edge [
    source 12
    target 1378
  ]
  edge [
    source 12
    target 1379
  ]
  edge [
    source 12
    target 1380
  ]
  edge [
    source 12
    target 1381
  ]
  edge [
    source 12
    target 1382
  ]
  edge [
    source 12
    target 1383
  ]
  edge [
    source 12
    target 1384
  ]
  edge [
    source 12
    target 1385
  ]
  edge [
    source 12
    target 1386
  ]
  edge [
    source 12
    target 1387
  ]
  edge [
    source 12
    target 1388
  ]
  edge [
    source 12
    target 1389
  ]
  edge [
    source 12
    target 1390
  ]
  edge [
    source 12
    target 1391
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 1392
  ]
  edge [
    source 12
    target 1393
  ]
  edge [
    source 12
    target 1394
  ]
  edge [
    source 12
    target 1395
  ]
  edge [
    source 12
    target 1396
  ]
  edge [
    source 12
    target 1397
  ]
  edge [
    source 12
    target 1398
  ]
  edge [
    source 12
    target 1399
  ]
  edge [
    source 12
    target 1400
  ]
  edge [
    source 12
    target 1401
  ]
  edge [
    source 12
    target 1402
  ]
  edge [
    source 12
    target 1403
  ]
  edge [
    source 12
    target 1404
  ]
  edge [
    source 12
    target 1405
  ]
  edge [
    source 12
    target 1406
  ]
  edge [
    source 12
    target 1407
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 1408
  ]
  edge [
    source 12
    target 1409
  ]
  edge [
    source 12
    target 1410
  ]
  edge [
    source 12
    target 1411
  ]
  edge [
    source 12
    target 1412
  ]
  edge [
    source 12
    target 1413
  ]
  edge [
    source 12
    target 1414
  ]
  edge [
    source 12
    target 1415
  ]
  edge [
    source 12
    target 1416
  ]
  edge [
    source 12
    target 1417
  ]
  edge [
    source 12
    target 1418
  ]
  edge [
    source 12
    target 1419
  ]
  edge [
    source 12
    target 1420
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 1421
  ]
  edge [
    source 12
    target 1422
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 1423
  ]
  edge [
    source 12
    target 1424
  ]
  edge [
    source 12
    target 1425
  ]
  edge [
    source 12
    target 1426
  ]
  edge [
    source 12
    target 1427
  ]
  edge [
    source 12
    target 1428
  ]
  edge [
    source 12
    target 1429
  ]
  edge [
    source 12
    target 1430
  ]
  edge [
    source 12
    target 1431
  ]
  edge [
    source 12
    target 1432
  ]
  edge [
    source 12
    target 1433
  ]
  edge [
    source 12
    target 1434
  ]
  edge [
    source 12
    target 1435
  ]
  edge [
    source 12
    target 1436
  ]
  edge [
    source 12
    target 1437
  ]
  edge [
    source 12
    target 1438
  ]
  edge [
    source 12
    target 1439
  ]
  edge [
    source 12
    target 1440
  ]
  edge [
    source 12
    target 1441
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 1442
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 1443
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 1444
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 1445
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 1446
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 1447
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 1448
  ]
  edge [
    source 12
    target 1449
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 1102
  ]
  edge [
    source 12
    target 1450
  ]
  edge [
    source 12
    target 1451
  ]
  edge [
    source 12
    target 1452
  ]
  edge [
    source 12
    target 1103
  ]
  edge [
    source 12
    target 1453
  ]
  edge [
    source 12
    target 1454
  ]
  edge [
    source 12
    target 1455
  ]
  edge [
    source 12
    target 1456
  ]
  edge [
    source 12
    target 1106
  ]
  edge [
    source 12
    target 1457
  ]
  edge [
    source 12
    target 1458
  ]
  edge [
    source 12
    target 1459
  ]
  edge [
    source 12
    target 1460
  ]
  edge [
    source 12
    target 1461
  ]
  edge [
    source 12
    target 1462
  ]
  edge [
    source 12
    target 1463
  ]
  edge [
    source 12
    target 1464
  ]
  edge [
    source 12
    target 1465
  ]
  edge [
    source 12
    target 1466
  ]
  edge [
    source 12
    target 1467
  ]
  edge [
    source 12
    target 1468
  ]
  edge [
    source 12
    target 1469
  ]
  edge [
    source 12
    target 1470
  ]
  edge [
    source 12
    target 1471
  ]
  edge [
    source 12
    target 1472
  ]
  edge [
    source 12
    target 1473
  ]
  edge [
    source 12
    target 1474
  ]
  edge [
    source 12
    target 1475
  ]
  edge [
    source 12
    target 1476
  ]
  edge [
    source 12
    target 1477
  ]
  edge [
    source 12
    target 1478
  ]
  edge [
    source 12
    target 1479
  ]
  edge [
    source 12
    target 1480
  ]
  edge [
    source 12
    target 1481
  ]
  edge [
    source 12
    target 1482
  ]
  edge [
    source 12
    target 1483
  ]
  edge [
    source 12
    target 1484
  ]
  edge [
    source 12
    target 1485
  ]
  edge [
    source 12
    target 1486
  ]
  edge [
    source 12
    target 1487
  ]
  edge [
    source 12
    target 1488
  ]
  edge [
    source 12
    target 1489
  ]
  edge [
    source 12
    target 1490
  ]
  edge [
    source 12
    target 1491
  ]
  edge [
    source 12
    target 1492
  ]
  edge [
    source 12
    target 1493
  ]
  edge [
    source 12
    target 1494
  ]
  edge [
    source 12
    target 1495
  ]
  edge [
    source 12
    target 1496
  ]
  edge [
    source 12
    target 1497
  ]
  edge [
    source 12
    target 1498
  ]
  edge [
    source 12
    target 1499
  ]
  edge [
    source 12
    target 1500
  ]
  edge [
    source 12
    target 1501
  ]
  edge [
    source 12
    target 1502
  ]
  edge [
    source 12
    target 1503
  ]
  edge [
    source 12
    target 1504
  ]
  edge [
    source 12
    target 1505
  ]
  edge [
    source 12
    target 1506
  ]
  edge [
    source 12
    target 1507
  ]
  edge [
    source 12
    target 1508
  ]
  edge [
    source 12
    target 1509
  ]
  edge [
    source 12
    target 1510
  ]
  edge [
    source 12
    target 1511
  ]
  edge [
    source 12
    target 1512
  ]
  edge [
    source 12
    target 1513
  ]
  edge [
    source 12
    target 1514
  ]
  edge [
    source 12
    target 1515
  ]
  edge [
    source 12
    target 1516
  ]
  edge [
    source 12
    target 1517
  ]
  edge [
    source 12
    target 1518
  ]
  edge [
    source 12
    target 1519
  ]
  edge [
    source 12
    target 1520
  ]
  edge [
    source 12
    target 1521
  ]
  edge [
    source 12
    target 1522
  ]
  edge [
    source 12
    target 1523
  ]
  edge [
    source 12
    target 1524
  ]
  edge [
    source 12
    target 1525
  ]
  edge [
    source 12
    target 1526
  ]
  edge [
    source 12
    target 1527
  ]
  edge [
    source 12
    target 1528
  ]
  edge [
    source 12
    target 1529
  ]
  edge [
    source 12
    target 1530
  ]
  edge [
    source 12
    target 1531
  ]
  edge [
    source 12
    target 1532
  ]
  edge [
    source 12
    target 1533
  ]
  edge [
    source 12
    target 1534
  ]
  edge [
    source 12
    target 1535
  ]
  edge [
    source 12
    target 1536
  ]
  edge [
    source 12
    target 1537
  ]
  edge [
    source 12
    target 1538
  ]
  edge [
    source 12
    target 1539
  ]
  edge [
    source 12
    target 1540
  ]
  edge [
    source 12
    target 1541
  ]
  edge [
    source 12
    target 1542
  ]
  edge [
    source 12
    target 1543
  ]
  edge [
    source 12
    target 1544
  ]
  edge [
    source 12
    target 1545
  ]
  edge [
    source 12
    target 1546
  ]
  edge [
    source 12
    target 1547
  ]
  edge [
    source 12
    target 1548
  ]
  edge [
    source 12
    target 1549
  ]
  edge [
    source 12
    target 1550
  ]
  edge [
    source 12
    target 1551
  ]
  edge [
    source 12
    target 1552
  ]
  edge [
    source 12
    target 1553
  ]
  edge [
    source 12
    target 1554
  ]
  edge [
    source 12
    target 1555
  ]
  edge [
    source 12
    target 1556
  ]
  edge [
    source 12
    target 1557
  ]
  edge [
    source 12
    target 1558
  ]
  edge [
    source 12
    target 1559
  ]
  edge [
    source 12
    target 1560
  ]
  edge [
    source 12
    target 1561
  ]
  edge [
    source 12
    target 1562
  ]
  edge [
    source 12
    target 1563
  ]
  edge [
    source 12
    target 1564
  ]
  edge [
    source 12
    target 1565
  ]
  edge [
    source 12
    target 1566
  ]
  edge [
    source 12
    target 1567
  ]
  edge [
    source 12
    target 1568
  ]
  edge [
    source 12
    target 1569
  ]
  edge [
    source 12
    target 1570
  ]
  edge [
    source 12
    target 1571
  ]
  edge [
    source 12
    target 1572
  ]
  edge [
    source 12
    target 1573
  ]
  edge [
    source 12
    target 1574
  ]
  edge [
    source 12
    target 1575
  ]
  edge [
    source 12
    target 1576
  ]
  edge [
    source 12
    target 1577
  ]
  edge [
    source 12
    target 1578
  ]
  edge [
    source 12
    target 1579
  ]
  edge [
    source 12
    target 1580
  ]
  edge [
    source 12
    target 1581
  ]
  edge [
    source 12
    target 1582
  ]
  edge [
    source 12
    target 1583
  ]
  edge [
    source 12
    target 1584
  ]
  edge [
    source 12
    target 1585
  ]
  edge [
    source 12
    target 1586
  ]
  edge [
    source 12
    target 1587
  ]
  edge [
    source 12
    target 1588
  ]
  edge [
    source 12
    target 1589
  ]
  edge [
    source 12
    target 1590
  ]
  edge [
    source 12
    target 1591
  ]
  edge [
    source 12
    target 1592
  ]
  edge [
    source 12
    target 1593
  ]
  edge [
    source 12
    target 1594
  ]
  edge [
    source 12
    target 1595
  ]
  edge [
    source 12
    target 1596
  ]
  edge [
    source 12
    target 1597
  ]
  edge [
    source 12
    target 1598
  ]
  edge [
    source 12
    target 1599
  ]
  edge [
    source 12
    target 1600
  ]
  edge [
    source 12
    target 1601
  ]
  edge [
    source 12
    target 1602
  ]
  edge [
    source 12
    target 1603
  ]
  edge [
    source 12
    target 1604
  ]
  edge [
    source 12
    target 1605
  ]
  edge [
    source 12
    target 1606
  ]
  edge [
    source 12
    target 1607
  ]
  edge [
    source 12
    target 1608
  ]
  edge [
    source 12
    target 1609
  ]
  edge [
    source 12
    target 1610
  ]
  edge [
    source 12
    target 1611
  ]
  edge [
    source 12
    target 1612
  ]
  edge [
    source 12
    target 1613
  ]
  edge [
    source 12
    target 1614
  ]
  edge [
    source 12
    target 1615
  ]
  edge [
    source 12
    target 1616
  ]
  edge [
    source 12
    target 1617
  ]
  edge [
    source 12
    target 1618
  ]
  edge [
    source 12
    target 1619
  ]
  edge [
    source 12
    target 1620
  ]
  edge [
    source 12
    target 1621
  ]
  edge [
    source 12
    target 1622
  ]
  edge [
    source 12
    target 1623
  ]
  edge [
    source 12
    target 1624
  ]
  edge [
    source 12
    target 1625
  ]
  edge [
    source 12
    target 1626
  ]
  edge [
    source 12
    target 1627
  ]
  edge [
    source 12
    target 1628
  ]
  edge [
    source 12
    target 1629
  ]
  edge [
    source 12
    target 1630
  ]
  edge [
    source 12
    target 1631
  ]
  edge [
    source 12
    target 1632
  ]
  edge [
    source 12
    target 1633
  ]
  edge [
    source 12
    target 1634
  ]
  edge [
    source 12
    target 1635
  ]
  edge [
    source 12
    target 1636
  ]
  edge [
    source 12
    target 1637
  ]
  edge [
    source 12
    target 1638
  ]
  edge [
    source 12
    target 1639
  ]
  edge [
    source 12
    target 1640
  ]
  edge [
    source 12
    target 1641
  ]
  edge [
    source 12
    target 1642
  ]
  edge [
    source 12
    target 1643
  ]
  edge [
    source 12
    target 1644
  ]
  edge [
    source 12
    target 1645
  ]
  edge [
    source 12
    target 1646
  ]
  edge [
    source 12
    target 1647
  ]
  edge [
    source 12
    target 1648
  ]
  edge [
    source 12
    target 1649
  ]
  edge [
    source 12
    target 1650
  ]
  edge [
    source 12
    target 1651
  ]
  edge [
    source 12
    target 1652
  ]
  edge [
    source 12
    target 1653
  ]
  edge [
    source 12
    target 1654
  ]
  edge [
    source 12
    target 1655
  ]
  edge [
    source 12
    target 1656
  ]
  edge [
    source 12
    target 1657
  ]
  edge [
    source 12
    target 1658
  ]
  edge [
    source 12
    target 1659
  ]
  edge [
    source 12
    target 1660
  ]
  edge [
    source 12
    target 1661
  ]
  edge [
    source 12
    target 1662
  ]
  edge [
    source 12
    target 1663
  ]
  edge [
    source 12
    target 1664
  ]
  edge [
    source 12
    target 1665
  ]
  edge [
    source 12
    target 1666
  ]
  edge [
    source 12
    target 1667
  ]
  edge [
    source 12
    target 1668
  ]
  edge [
    source 12
    target 1669
  ]
  edge [
    source 12
    target 1670
  ]
  edge [
    source 12
    target 1671
  ]
  edge [
    source 12
    target 1672
  ]
  edge [
    source 12
    target 1673
  ]
  edge [
    source 12
    target 1674
  ]
  edge [
    source 12
    target 1675
  ]
  edge [
    source 12
    target 1676
  ]
  edge [
    source 12
    target 1677
  ]
  edge [
    source 12
    target 1678
  ]
  edge [
    source 12
    target 1679
  ]
  edge [
    source 12
    target 1680
  ]
  edge [
    source 12
    target 1681
  ]
  edge [
    source 12
    target 1682
  ]
  edge [
    source 12
    target 1683
  ]
  edge [
    source 12
    target 1684
  ]
  edge [
    source 12
    target 1685
  ]
  edge [
    source 12
    target 1686
  ]
  edge [
    source 12
    target 1687
  ]
  edge [
    source 12
    target 1688
  ]
  edge [
    source 12
    target 1689
  ]
  edge [
    source 12
    target 1690
  ]
  edge [
    source 12
    target 1691
  ]
  edge [
    source 12
    target 1692
  ]
  edge [
    source 12
    target 1693
  ]
  edge [
    source 12
    target 1694
  ]
  edge [
    source 12
    target 1695
  ]
  edge [
    source 12
    target 1696
  ]
  edge [
    source 12
    target 1697
  ]
  edge [
    source 12
    target 1698
  ]
  edge [
    source 12
    target 1699
  ]
  edge [
    source 12
    target 1700
  ]
  edge [
    source 12
    target 1701
  ]
  edge [
    source 12
    target 1702
  ]
  edge [
    source 12
    target 1703
  ]
  edge [
    source 12
    target 1704
  ]
  edge [
    source 12
    target 1705
  ]
  edge [
    source 12
    target 1706
  ]
  edge [
    source 12
    target 1707
  ]
  edge [
    source 12
    target 1708
  ]
  edge [
    source 12
    target 1709
  ]
  edge [
    source 12
    target 1710
  ]
  edge [
    source 12
    target 1711
  ]
  edge [
    source 12
    target 1712
  ]
  edge [
    source 12
    target 1713
  ]
  edge [
    source 12
    target 1714
  ]
  edge [
    source 12
    target 1715
  ]
  edge [
    source 12
    target 1716
  ]
  edge [
    source 12
    target 1717
  ]
  edge [
    source 12
    target 1718
  ]
  edge [
    source 12
    target 1719
  ]
  edge [
    source 12
    target 1720
  ]
  edge [
    source 12
    target 1721
  ]
  edge [
    source 12
    target 1722
  ]
  edge [
    source 12
    target 1723
  ]
  edge [
    source 12
    target 1724
  ]
  edge [
    source 12
    target 1725
  ]
  edge [
    source 12
    target 1726
  ]
  edge [
    source 12
    target 1727
  ]
  edge [
    source 12
    target 1728
  ]
  edge [
    source 12
    target 1729
  ]
  edge [
    source 12
    target 1730
  ]
  edge [
    source 12
    target 1731
  ]
  edge [
    source 12
    target 1732
  ]
  edge [
    source 12
    target 1733
  ]
  edge [
    source 12
    target 1734
  ]
  edge [
    source 12
    target 1735
  ]
  edge [
    source 12
    target 1736
  ]
  edge [
    source 12
    target 1737
  ]
  edge [
    source 12
    target 1738
  ]
  edge [
    source 12
    target 1739
  ]
  edge [
    source 12
    target 1740
  ]
  edge [
    source 12
    target 1741
  ]
  edge [
    source 12
    target 1742
  ]
  edge [
    source 12
    target 1743
  ]
  edge [
    source 12
    target 1744
  ]
  edge [
    source 12
    target 1745
  ]
  edge [
    source 12
    target 1746
  ]
  edge [
    source 12
    target 1747
  ]
  edge [
    source 12
    target 1748
  ]
  edge [
    source 12
    target 1749
  ]
  edge [
    source 12
    target 1750
  ]
  edge [
    source 12
    target 1751
  ]
  edge [
    source 12
    target 1752
  ]
  edge [
    source 12
    target 1753
  ]
  edge [
    source 12
    target 1754
  ]
  edge [
    source 12
    target 1755
  ]
  edge [
    source 12
    target 1756
  ]
  edge [
    source 12
    target 1757
  ]
  edge [
    source 12
    target 1758
  ]
  edge [
    source 12
    target 1759
  ]
  edge [
    source 12
    target 1760
  ]
  edge [
    source 12
    target 1761
  ]
  edge [
    source 12
    target 1762
  ]
  edge [
    source 12
    target 1763
  ]
  edge [
    source 12
    target 1764
  ]
  edge [
    source 12
    target 1765
  ]
  edge [
    source 12
    target 1766
  ]
  edge [
    source 12
    target 1767
  ]
  edge [
    source 12
    target 1768
  ]
  edge [
    source 12
    target 1769
  ]
  edge [
    source 12
    target 1770
  ]
  edge [
    source 12
    target 1771
  ]
  edge [
    source 12
    target 1772
  ]
  edge [
    source 12
    target 1773
  ]
  edge [
    source 12
    target 1774
  ]
  edge [
    source 12
    target 1775
  ]
  edge [
    source 12
    target 1776
  ]
  edge [
    source 12
    target 1777
  ]
  edge [
    source 12
    target 1778
  ]
  edge [
    source 12
    target 1779
  ]
  edge [
    source 12
    target 1780
  ]
  edge [
    source 12
    target 1781
  ]
  edge [
    source 12
    target 1782
  ]
  edge [
    source 12
    target 1783
  ]
  edge [
    source 12
    target 1784
  ]
  edge [
    source 12
    target 1785
  ]
  edge [
    source 12
    target 1786
  ]
  edge [
    source 12
    target 1787
  ]
  edge [
    source 12
    target 1788
  ]
  edge [
    source 12
    target 1789
  ]
  edge [
    source 12
    target 1790
  ]
  edge [
    source 12
    target 1791
  ]
  edge [
    source 12
    target 1792
  ]
  edge [
    source 12
    target 1793
  ]
  edge [
    source 12
    target 1794
  ]
  edge [
    source 12
    target 1795
  ]
  edge [
    source 12
    target 1796
  ]
  edge [
    source 12
    target 1797
  ]
  edge [
    source 12
    target 1798
  ]
  edge [
    source 12
    target 1799
  ]
  edge [
    source 12
    target 1800
  ]
  edge [
    source 12
    target 1801
  ]
  edge [
    source 12
    target 1802
  ]
  edge [
    source 12
    target 1803
  ]
  edge [
    source 12
    target 1804
  ]
  edge [
    source 12
    target 1805
  ]
  edge [
    source 12
    target 1806
  ]
  edge [
    source 12
    target 1807
  ]
  edge [
    source 12
    target 1808
  ]
  edge [
    source 12
    target 1809
  ]
  edge [
    source 12
    target 1810
  ]
  edge [
    source 12
    target 1811
  ]
  edge [
    source 12
    target 1812
  ]
  edge [
    source 12
    target 1813
  ]
  edge [
    source 12
    target 1814
  ]
  edge [
    source 12
    target 1815
  ]
  edge [
    source 12
    target 1816
  ]
  edge [
    source 12
    target 1817
  ]
  edge [
    source 12
    target 1818
  ]
  edge [
    source 12
    target 1819
  ]
  edge [
    source 12
    target 1820
  ]
  edge [
    source 12
    target 1821
  ]
  edge [
    source 12
    target 1822
  ]
  edge [
    source 12
    target 1823
  ]
  edge [
    source 12
    target 1824
  ]
  edge [
    source 12
    target 1825
  ]
  edge [
    source 12
    target 1826
  ]
  edge [
    source 12
    target 1827
  ]
  edge [
    source 12
    target 1828
  ]
  edge [
    source 12
    target 1829
  ]
  edge [
    source 12
    target 1830
  ]
  edge [
    source 12
    target 1831
  ]
  edge [
    source 12
    target 1832
  ]
  edge [
    source 12
    target 1833
  ]
  edge [
    source 12
    target 1834
  ]
  edge [
    source 12
    target 1835
  ]
  edge [
    source 12
    target 1836
  ]
  edge [
    source 12
    target 1837
  ]
  edge [
    source 12
    target 1838
  ]
  edge [
    source 12
    target 1839
  ]
  edge [
    source 12
    target 1840
  ]
  edge [
    source 12
    target 1841
  ]
  edge [
    source 12
    target 1842
  ]
  edge [
    source 12
    target 1843
  ]
  edge [
    source 12
    target 1844
  ]
  edge [
    source 12
    target 1845
  ]
  edge [
    source 12
    target 1846
  ]
  edge [
    source 12
    target 1847
  ]
  edge [
    source 12
    target 1848
  ]
  edge [
    source 12
    target 1849
  ]
  edge [
    source 12
    target 1850
  ]
  edge [
    source 12
    target 1851
  ]
  edge [
    source 12
    target 1852
  ]
  edge [
    source 12
    target 1853
  ]
  edge [
    source 12
    target 1854
  ]
  edge [
    source 12
    target 1855
  ]
  edge [
    source 12
    target 1856
  ]
  edge [
    source 12
    target 1857
  ]
  edge [
    source 12
    target 1858
  ]
  edge [
    source 12
    target 1859
  ]
  edge [
    source 12
    target 1860
  ]
  edge [
    source 12
    target 1861
  ]
  edge [
    source 12
    target 1862
  ]
  edge [
    source 12
    target 1863
  ]
  edge [
    source 12
    target 1864
  ]
  edge [
    source 12
    target 1865
  ]
  edge [
    source 12
    target 1866
  ]
  edge [
    source 12
    target 1867
  ]
  edge [
    source 12
    target 1868
  ]
  edge [
    source 12
    target 1869
  ]
  edge [
    source 12
    target 1870
  ]
  edge [
    source 12
    target 1871
  ]
  edge [
    source 12
    target 1872
  ]
  edge [
    source 12
    target 1873
  ]
  edge [
    source 12
    target 1874
  ]
  edge [
    source 12
    target 1875
  ]
  edge [
    source 12
    target 1876
  ]
  edge [
    source 12
    target 1877
  ]
  edge [
    source 12
    target 1878
  ]
  edge [
    source 12
    target 1879
  ]
  edge [
    source 12
    target 1880
  ]
  edge [
    source 12
    target 1881
  ]
  edge [
    source 12
    target 1882
  ]
  edge [
    source 12
    target 1883
  ]
  edge [
    source 12
    target 1884
  ]
  edge [
    source 12
    target 1885
  ]
  edge [
    source 12
    target 1886
  ]
  edge [
    source 12
    target 1887
  ]
  edge [
    source 12
    target 1888
  ]
  edge [
    source 12
    target 1889
  ]
  edge [
    source 12
    target 1890
  ]
  edge [
    source 12
    target 1891
  ]
  edge [
    source 12
    target 1892
  ]
  edge [
    source 12
    target 1893
  ]
  edge [
    source 12
    target 1894
  ]
  edge [
    source 12
    target 1895
  ]
  edge [
    source 12
    target 1896
  ]
  edge [
    source 12
    target 1897
  ]
  edge [
    source 12
    target 1898
  ]
  edge [
    source 12
    target 1899
  ]
  edge [
    source 12
    target 1900
  ]
  edge [
    source 12
    target 1901
  ]
  edge [
    source 12
    target 1902
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 1903
  ]
  edge [
    source 12
    target 1904
  ]
  edge [
    source 12
    target 1905
  ]
  edge [
    source 12
    target 1906
  ]
  edge [
    source 12
    target 1907
  ]
  edge [
    source 12
    target 1908
  ]
  edge [
    source 12
    target 1909
  ]
  edge [
    source 12
    target 1910
  ]
  edge [
    source 12
    target 1911
  ]
  edge [
    source 12
    target 1912
  ]
  edge [
    source 12
    target 1913
  ]
  edge [
    source 12
    target 1914
  ]
  edge [
    source 12
    target 1915
  ]
  edge [
    source 12
    target 1916
  ]
  edge [
    source 12
    target 1917
  ]
  edge [
    source 12
    target 1918
  ]
  edge [
    source 12
    target 1919
  ]
  edge [
    source 12
    target 1920
  ]
  edge [
    source 12
    target 1921
  ]
  edge [
    source 12
    target 1922
  ]
  edge [
    source 12
    target 1923
  ]
  edge [
    source 12
    target 1924
  ]
  edge [
    source 12
    target 1925
  ]
  edge [
    source 12
    target 1926
  ]
  edge [
    source 12
    target 1927
  ]
  edge [
    source 12
    target 1928
  ]
  edge [
    source 12
    target 1929
  ]
  edge [
    source 12
    target 1930
  ]
  edge [
    source 12
    target 1931
  ]
  edge [
    source 12
    target 1932
  ]
  edge [
    source 12
    target 1933
  ]
  edge [
    source 12
    target 1934
  ]
  edge [
    source 12
    target 1935
  ]
  edge [
    source 12
    target 1936
  ]
  edge [
    source 12
    target 1937
  ]
  edge [
    source 12
    target 1938
  ]
  edge [
    source 12
    target 1939
  ]
  edge [
    source 12
    target 1940
  ]
  edge [
    source 12
    target 1941
  ]
  edge [
    source 12
    target 1942
  ]
  edge [
    source 12
    target 1943
  ]
  edge [
    source 12
    target 1944
  ]
  edge [
    source 12
    target 1945
  ]
  edge [
    source 12
    target 1946
  ]
  edge [
    source 12
    target 1947
  ]
  edge [
    source 12
    target 1948
  ]
  edge [
    source 12
    target 1949
  ]
  edge [
    source 12
    target 1950
  ]
  edge [
    source 12
    target 1951
  ]
  edge [
    source 12
    target 1952
  ]
  edge [
    source 12
    target 1953
  ]
  edge [
    source 12
    target 1954
  ]
  edge [
    source 12
    target 1955
  ]
  edge [
    source 12
    target 1956
  ]
  edge [
    source 12
    target 1957
  ]
  edge [
    source 12
    target 1958
  ]
  edge [
    source 12
    target 1959
  ]
  edge [
    source 12
    target 1960
  ]
  edge [
    source 12
    target 1961
  ]
  edge [
    source 12
    target 1962
  ]
  edge [
    source 12
    target 1963
  ]
  edge [
    source 12
    target 1964
  ]
  edge [
    source 12
    target 1965
  ]
  edge [
    source 12
    target 1966
  ]
  edge [
    source 12
    target 1967
  ]
  edge [
    source 12
    target 1968
  ]
  edge [
    source 12
    target 1969
  ]
  edge [
    source 12
    target 1970
  ]
  edge [
    source 12
    target 1971
  ]
  edge [
    source 12
    target 1972
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 1973
  ]
  edge [
    source 12
    target 1974
  ]
  edge [
    source 12
    target 1975
  ]
  edge [
    source 12
    target 1976
  ]
  edge [
    source 12
    target 1977
  ]
  edge [
    source 12
    target 1978
  ]
  edge [
    source 12
    target 1979
  ]
  edge [
    source 12
    target 1980
  ]
  edge [
    source 12
    target 1981
  ]
  edge [
    source 12
    target 1982
  ]
  edge [
    source 12
    target 1983
  ]
  edge [
    source 12
    target 1984
  ]
  edge [
    source 12
    target 1985
  ]
  edge [
    source 12
    target 1986
  ]
  edge [
    source 12
    target 1987
  ]
  edge [
    source 12
    target 1988
  ]
  edge [
    source 12
    target 1989
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 1990
  ]
  edge [
    source 12
    target 1991
  ]
  edge [
    source 12
    target 1992
  ]
  edge [
    source 12
    target 1993
  ]
  edge [
    source 12
    target 1994
  ]
  edge [
    source 12
    target 1995
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 1996
  ]
  edge [
    source 12
    target 1997
  ]
  edge [
    source 12
    target 1998
  ]
  edge [
    source 12
    target 1999
  ]
  edge [
    source 12
    target 2000
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 2001
  ]
  edge [
    source 12
    target 2002
  ]
  edge [
    source 12
    target 2003
  ]
  edge [
    source 12
    target 2004
  ]
  edge [
    source 12
    target 2005
  ]
  edge [
    source 12
    target 2006
  ]
  edge [
    source 12
    target 2007
  ]
  edge [
    source 12
    target 2008
  ]
  edge [
    source 12
    target 2009
  ]
  edge [
    source 12
    target 2010
  ]
  edge [
    source 12
    target 2011
  ]
  edge [
    source 12
    target 2012
  ]
  edge [
    source 12
    target 2013
  ]
  edge [
    source 12
    target 2014
  ]
  edge [
    source 12
    target 2015
  ]
  edge [
    source 12
    target 2016
  ]
  edge [
    source 12
    target 2017
  ]
  edge [
    source 12
    target 2018
  ]
  edge [
    source 12
    target 2019
  ]
  edge [
    source 12
    target 2020
  ]
  edge [
    source 12
    target 2021
  ]
  edge [
    source 12
    target 2022
  ]
  edge [
    source 12
    target 2023
  ]
  edge [
    source 12
    target 2024
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 2025
  ]
  edge [
    source 12
    target 2026
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 42
  ]
  edge [
    source 14
    target 2027
  ]
  edge [
    source 14
    target 2028
  ]
  edge [
    source 14
    target 2029
  ]
  edge [
    source 14
    target 2030
  ]
  edge [
    source 14
    target 2031
  ]
  edge [
    source 14
    target 2032
  ]
  edge [
    source 14
    target 2033
  ]
  edge [
    source 14
    target 2034
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 2035
  ]
  edge [
    source 14
    target 2036
  ]
  edge [
    source 14
    target 2037
  ]
  edge [
    source 14
    target 14
  ]
  edge [
    source 14
    target 45
  ]
  edge [
    source 14
    target 50
  ]
  edge [
    source 14
    target 38
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 364
  ]
  edge [
    source 15
    target 2038
  ]
  edge [
    source 15
    target 2039
  ]
  edge [
    source 15
    target 2040
  ]
  edge [
    source 15
    target 2041
  ]
  edge [
    source 15
    target 2042
  ]
  edge [
    source 15
    target 2043
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 2044
  ]
  edge [
    source 15
    target 2045
  ]
  edge [
    source 15
    target 2046
  ]
  edge [
    source 15
    target 2028
  ]
  edge [
    source 15
    target 2047
  ]
  edge [
    source 15
    target 2048
  ]
  edge [
    source 15
    target 2049
  ]
  edge [
    source 15
    target 2050
  ]
  edge [
    source 15
    target 2051
  ]
  edge [
    source 15
    target 2052
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 53
  ]
  edge [
    source 16
    target 54
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 2053
  ]
  edge [
    source 16
    target 2054
  ]
  edge [
    source 16
    target 2055
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 2056
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 2057
  ]
  edge [
    source 16
    target 2058
  ]
  edge [
    source 16
    target 2059
  ]
  edge [
    source 16
    target 2060
  ]
  edge [
    source 16
    target 2061
  ]
  edge [
    source 16
    target 2062
  ]
  edge [
    source 16
    target 2063
  ]
  edge [
    source 16
    target 2064
  ]
  edge [
    source 16
    target 2065
  ]
  edge [
    source 16
    target 2066
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 2067
  ]
  edge [
    source 16
    target 2068
  ]
  edge [
    source 16
    target 180
  ]
  edge [
    source 16
    target 200
  ]
  edge [
    source 16
    target 201
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 66
  ]
  edge [
    source 16
    target 2069
  ]
  edge [
    source 16
    target 2070
  ]
  edge [
    source 16
    target 2071
  ]
  edge [
    source 16
    target 2072
  ]
  edge [
    source 16
    target 2073
  ]
  edge [
    source 16
    target 2074
  ]
  edge [
    source 16
    target 207
  ]
  edge [
    source 16
    target 2075
  ]
  edge [
    source 16
    target 2076
  ]
  edge [
    source 16
    target 320
  ]
  edge [
    source 16
    target 43
  ]
  edge [
    source 16
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 49
  ]
  edge [
    source 17
    target 2077
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 2078
  ]
  edge [
    source 17
    target 2079
  ]
  edge [
    source 17
    target 2080
  ]
  edge [
    source 17
    target 2081
  ]
  edge [
    source 17
    target 2082
  ]
  edge [
    source 17
    target 63
  ]
  edge [
    source 17
    target 1418
  ]
  edge [
    source 17
    target 1436
  ]
  edge [
    source 17
    target 250
  ]
  edge [
    source 17
    target 416
  ]
  edge [
    source 17
    target 2083
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 1439
  ]
  edge [
    source 17
    target 1426
  ]
  edge [
    source 17
    target 2084
  ]
  edge [
    source 17
    target 2085
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 17
    target 578
  ]
  edge [
    source 17
    target 1456
  ]
  edge [
    source 17
    target 411
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 2086
  ]
  edge [
    source 17
    target 1437
  ]
  edge [
    source 17
    target 2087
  ]
  edge [
    source 17
    target 428
  ]
  edge [
    source 17
    target 2088
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 2089
  ]
  edge [
    source 17
    target 2090
  ]
  edge [
    source 17
    target 2091
  ]
  edge [
    source 17
    target 232
  ]
  edge [
    source 17
    target 1384
  ]
  edge [
    source 17
    target 2092
  ]
  edge [
    source 17
    target 2093
  ]
  edge [
    source 17
    target 2094
  ]
  edge [
    source 17
    target 2095
  ]
  edge [
    source 17
    target 2096
  ]
  edge [
    source 17
    target 2097
  ]
  edge [
    source 17
    target 241
  ]
  edge [
    source 17
    target 2098
  ]
  edge [
    source 17
    target 435
  ]
  edge [
    source 17
    target 2099
  ]
  edge [
    source 17
    target 2100
  ]
  edge [
    source 17
    target 17
  ]
  edge [
    source 17
    target 53
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 38
  ]
  edge [
    source 18
    target 39
  ]
  edge [
    source 18
    target 45
  ]
  edge [
    source 18
    target 46
  ]
  edge [
    source 18
    target 2101
  ]
  edge [
    source 18
    target 2102
  ]
  edge [
    source 18
    target 2103
  ]
  edge [
    source 18
    target 2073
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 2104
  ]
  edge [
    source 18
    target 2105
  ]
  edge [
    source 18
    target 2106
  ]
  edge [
    source 18
    target 2107
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 2108
  ]
  edge [
    source 18
    target 2109
  ]
  edge [
    source 18
    target 2110
  ]
  edge [
    source 18
    target 2111
  ]
  edge [
    source 18
    target 2112
  ]
  edge [
    source 18
    target 2113
  ]
  edge [
    source 18
    target 2114
  ]
  edge [
    source 18
    target 2115
  ]
  edge [
    source 18
    target 2116
  ]
  edge [
    source 18
    target 2117
  ]
  edge [
    source 18
    target 2118
  ]
  edge [
    source 18
    target 2119
  ]
  edge [
    source 18
    target 448
  ]
  edge [
    source 18
    target 2120
  ]
  edge [
    source 18
    target 2121
  ]
  edge [
    source 18
    target 2122
  ]
  edge [
    source 18
    target 2123
  ]
  edge [
    source 18
    target 2124
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 2125
  ]
  edge [
    source 18
    target 216
  ]
  edge [
    source 18
    target 2126
  ]
  edge [
    source 18
    target 317
  ]
  edge [
    source 18
    target 2127
  ]
  edge [
    source 18
    target 2128
  ]
  edge [
    source 18
    target 320
  ]
  edge [
    source 18
    target 293
  ]
  edge [
    source 18
    target 2129
  ]
  edge [
    source 18
    target 2130
  ]
  edge [
    source 18
    target 2131
  ]
  edge [
    source 18
    target 2132
  ]
  edge [
    source 18
    target 2133
  ]
  edge [
    source 18
    target 2134
  ]
  edge [
    source 18
    target 36
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 2135
  ]
  edge [
    source 19
    target 2136
  ]
  edge [
    source 19
    target 2137
  ]
  edge [
    source 20
    target 2138
  ]
  edge [
    source 20
    target 2139
  ]
  edge [
    source 20
    target 2140
  ]
  edge [
    source 20
    target 2141
  ]
  edge [
    source 20
    target 2142
  ]
  edge [
    source 20
    target 2143
  ]
  edge [
    source 20
    target 2144
  ]
  edge [
    source 20
    target 2145
  ]
  edge [
    source 20
    target 835
  ]
  edge [
    source 20
    target 2146
  ]
  edge [
    source 20
    target 2147
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 2148
  ]
  edge [
    source 21
    target 2149
  ]
  edge [
    source 21
    target 2150
  ]
  edge [
    source 21
    target 2151
  ]
  edge [
    source 21
    target 2152
  ]
  edge [
    source 21
    target 2153
  ]
  edge [
    source 21
    target 2154
  ]
  edge [
    source 21
    target 2155
  ]
  edge [
    source 21
    target 2156
  ]
  edge [
    source 21
    target 2157
  ]
  edge [
    source 21
    target 2158
  ]
  edge [
    source 21
    target 2159
  ]
  edge [
    source 21
    target 348
  ]
  edge [
    source 21
    target 2160
  ]
  edge [
    source 21
    target 2161
  ]
  edge [
    source 21
    target 349
  ]
  edge [
    source 21
    target 2162
  ]
  edge [
    source 21
    target 2163
  ]
  edge [
    source 21
    target 341
  ]
  edge [
    source 21
    target 2164
  ]
  edge [
    source 21
    target 2165
  ]
  edge [
    source 21
    target 2166
  ]
  edge [
    source 21
    target 2167
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 2168
  ]
  edge [
    source 22
    target 2169
  ]
  edge [
    source 22
    target 2170
  ]
  edge [
    source 22
    target 2171
  ]
  edge [
    source 22
    target 2172
  ]
  edge [
    source 22
    target 2173
  ]
  edge [
    source 22
    target 2174
  ]
  edge [
    source 22
    target 2175
  ]
  edge [
    source 22
    target 2176
  ]
  edge [
    source 22
    target 2177
  ]
  edge [
    source 22
    target 2178
  ]
  edge [
    source 22
    target 2179
  ]
  edge [
    source 22
    target 2180
  ]
  edge [
    source 22
    target 2181
  ]
  edge [
    source 22
    target 2182
  ]
  edge [
    source 22
    target 2183
  ]
  edge [
    source 22
    target 2184
  ]
  edge [
    source 22
    target 2185
  ]
  edge [
    source 22
    target 2186
  ]
  edge [
    source 22
    target 2187
  ]
  edge [
    source 22
    target 2188
  ]
  edge [
    source 22
    target 2189
  ]
  edge [
    source 22
    target 2190
  ]
  edge [
    source 22
    target 2191
  ]
  edge [
    source 22
    target 2192
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 57
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 2193
  ]
  edge [
    source 24
    target 2194
  ]
  edge [
    source 24
    target 2195
  ]
  edge [
    source 24
    target 2196
  ]
  edge [
    source 24
    target 2197
  ]
  edge [
    source 24
    target 867
  ]
  edge [
    source 24
    target 2198
  ]
  edge [
    source 24
    target 2199
  ]
  edge [
    source 24
    target 2200
  ]
  edge [
    source 24
    target 2201
  ]
  edge [
    source 24
    target 2202
  ]
  edge [
    source 24
    target 2203
  ]
  edge [
    source 24
    target 2204
  ]
  edge [
    source 24
    target 1091
  ]
  edge [
    source 24
    target 2205
  ]
  edge [
    source 24
    target 2206
  ]
  edge [
    source 24
    target 2087
  ]
  edge [
    source 24
    target 428
  ]
  edge [
    source 24
    target 2088
  ]
  edge [
    source 24
    target 112
  ]
  edge [
    source 24
    target 2089
  ]
  edge [
    source 24
    target 2090
  ]
  edge [
    source 24
    target 66
  ]
  edge [
    source 24
    target 2091
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 24
    target 1384
  ]
  edge [
    source 24
    target 2092
  ]
  edge [
    source 24
    target 2093
  ]
  edge [
    source 24
    target 2094
  ]
  edge [
    source 24
    target 2095
  ]
  edge [
    source 24
    target 2096
  ]
  edge [
    source 24
    target 2097
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 2098
  ]
  edge [
    source 24
    target 435
  ]
  edge [
    source 24
    target 2099
  ]
  edge [
    source 24
    target 934
  ]
  edge [
    source 24
    target 2207
  ]
  edge [
    source 24
    target 2208
  ]
  edge [
    source 24
    target 2209
  ]
  edge [
    source 24
    target 2210
  ]
  edge [
    source 24
    target 2211
  ]
  edge [
    source 24
    target 829
  ]
  edge [
    source 24
    target 2212
  ]
  edge [
    source 24
    target 2213
  ]
  edge [
    source 24
    target 2214
  ]
  edge [
    source 24
    target 2215
  ]
  edge [
    source 24
    target 502
  ]
  edge [
    source 24
    target 2216
  ]
  edge [
    source 24
    target 2217
  ]
  edge [
    source 24
    target 2218
  ]
  edge [
    source 24
    target 2219
  ]
  edge [
    source 24
    target 2220
  ]
  edge [
    source 24
    target 2221
  ]
  edge [
    source 24
    target 85
  ]
  edge [
    source 24
    target 2222
  ]
  edge [
    source 24
    target 2223
  ]
  edge [
    source 24
    target 2224
  ]
  edge [
    source 24
    target 2225
  ]
  edge [
    source 24
    target 2226
  ]
  edge [
    source 24
    target 2227
  ]
  edge [
    source 24
    target 2228
  ]
  edge [
    source 24
    target 2229
  ]
  edge [
    source 24
    target 2230
  ]
  edge [
    source 24
    target 2231
  ]
  edge [
    source 24
    target 2232
  ]
  edge [
    source 24
    target 2233
  ]
  edge [
    source 24
    target 2234
  ]
  edge [
    source 24
    target 2235
  ]
  edge [
    source 24
    target 2236
  ]
  edge [
    source 24
    target 2237
  ]
  edge [
    source 24
    target 2238
  ]
  edge [
    source 24
    target 2239
  ]
  edge [
    source 24
    target 2240
  ]
  edge [
    source 24
    target 2241
  ]
  edge [
    source 24
    target 2242
  ]
  edge [
    source 24
    target 1092
  ]
  edge [
    source 24
    target 2243
  ]
  edge [
    source 24
    target 2244
  ]
  edge [
    source 24
    target 2245
  ]
  edge [
    source 24
    target 2246
  ]
  edge [
    source 24
    target 2247
  ]
  edge [
    source 24
    target 2248
  ]
  edge [
    source 24
    target 2249
  ]
  edge [
    source 24
    target 2250
  ]
  edge [
    source 24
    target 2251
  ]
  edge [
    source 24
    target 2252
  ]
  edge [
    source 24
    target 2253
  ]
  edge [
    source 24
    target 2254
  ]
  edge [
    source 24
    target 2255
  ]
  edge [
    source 24
    target 2256
  ]
  edge [
    source 24
    target 2257
  ]
  edge [
    source 24
    target 2258
  ]
  edge [
    source 24
    target 2259
  ]
  edge [
    source 24
    target 2260
  ]
  edge [
    source 24
    target 2261
  ]
  edge [
    source 24
    target 2262
  ]
  edge [
    source 24
    target 2263
  ]
  edge [
    source 24
    target 2264
  ]
  edge [
    source 24
    target 2265
  ]
  edge [
    source 24
    target 2266
  ]
  edge [
    source 24
    target 2267
  ]
  edge [
    source 24
    target 2268
  ]
  edge [
    source 24
    target 2269
  ]
  edge [
    source 24
    target 2270
  ]
  edge [
    source 24
    target 2271
  ]
  edge [
    source 24
    target 2272
  ]
  edge [
    source 24
    target 2273
  ]
  edge [
    source 24
    target 2274
  ]
  edge [
    source 24
    target 2275
  ]
  edge [
    source 24
    target 294
  ]
  edge [
    source 24
    target 2276
  ]
  edge [
    source 24
    target 2277
  ]
  edge [
    source 24
    target 2278
  ]
  edge [
    source 24
    target 2279
  ]
  edge [
    source 24
    target 2280
  ]
  edge [
    source 24
    target 2281
  ]
  edge [
    source 24
    target 2282
  ]
  edge [
    source 24
    target 2283
  ]
  edge [
    source 24
    target 2284
  ]
  edge [
    source 24
    target 2285
  ]
  edge [
    source 24
    target 2286
  ]
  edge [
    source 24
    target 2287
  ]
  edge [
    source 24
    target 2288
  ]
  edge [
    source 24
    target 2289
  ]
  edge [
    source 24
    target 2290
  ]
  edge [
    source 24
    target 2291
  ]
  edge [
    source 24
    target 2292
  ]
  edge [
    source 24
    target 2293
  ]
  edge [
    source 24
    target 2294
  ]
  edge [
    source 24
    target 2295
  ]
  edge [
    source 24
    target 2296
  ]
  edge [
    source 24
    target 2297
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 2298
  ]
  edge [
    source 24
    target 2299
  ]
  edge [
    source 24
    target 2300
  ]
  edge [
    source 24
    target 2301
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 2302
  ]
  edge [
    source 24
    target 2303
  ]
  edge [
    source 24
    target 2304
  ]
  edge [
    source 24
    target 2305
  ]
  edge [
    source 24
    target 2306
  ]
  edge [
    source 24
    target 2307
  ]
  edge [
    source 24
    target 2308
  ]
  edge [
    source 24
    target 1408
  ]
  edge [
    source 24
    target 2309
  ]
  edge [
    source 24
    target 2310
  ]
  edge [
    source 24
    target 2311
  ]
  edge [
    source 24
    target 2312
  ]
  edge [
    source 24
    target 2313
  ]
  edge [
    source 24
    target 2314
  ]
  edge [
    source 24
    target 2315
  ]
  edge [
    source 24
    target 2316
  ]
  edge [
    source 24
    target 2317
  ]
  edge [
    source 24
    target 1093
  ]
  edge [
    source 24
    target 2318
  ]
  edge [
    source 24
    target 2319
  ]
  edge [
    source 24
    target 2320
  ]
  edge [
    source 24
    target 2321
  ]
  edge [
    source 24
    target 2322
  ]
  edge [
    source 24
    target 2323
  ]
  edge [
    source 24
    target 2324
  ]
  edge [
    source 24
    target 2325
  ]
  edge [
    source 24
    target 2326
  ]
  edge [
    source 24
    target 2327
  ]
  edge [
    source 24
    target 2328
  ]
  edge [
    source 24
    target 2329
  ]
  edge [
    source 24
    target 2330
  ]
  edge [
    source 24
    target 2331
  ]
  edge [
    source 24
    target 2332
  ]
  edge [
    source 24
    target 2333
  ]
  edge [
    source 24
    target 2334
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 31
  ]
  edge [
    source 25
    target 2335
  ]
  edge [
    source 25
    target 2336
  ]
  edge [
    source 25
    target 2337
  ]
  edge [
    source 25
    target 2338
  ]
  edge [
    source 25
    target 2339
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 2340
  ]
  edge [
    source 25
    target 390
  ]
  edge [
    source 25
    target 2341
  ]
  edge [
    source 25
    target 2058
  ]
  edge [
    source 25
    target 2342
  ]
  edge [
    source 25
    target 2343
  ]
  edge [
    source 25
    target 2344
  ]
  edge [
    source 25
    target 2345
  ]
  edge [
    source 25
    target 2346
  ]
  edge [
    source 25
    target 512
  ]
  edge [
    source 25
    target 2347
  ]
  edge [
    source 25
    target 2348
  ]
  edge [
    source 25
    target 2349
  ]
  edge [
    source 25
    target 2064
  ]
  edge [
    source 25
    target 2350
  ]
  edge [
    source 25
    target 2351
  ]
  edge [
    source 25
    target 2352
  ]
  edge [
    source 25
    target 2353
  ]
  edge [
    source 25
    target 2354
  ]
  edge [
    source 25
    target 2355
  ]
  edge [
    source 25
    target 2356
  ]
  edge [
    source 25
    target 530
  ]
  edge [
    source 25
    target 2357
  ]
  edge [
    source 25
    target 2358
  ]
  edge [
    source 25
    target 2359
  ]
  edge [
    source 25
    target 2360
  ]
  edge [
    source 25
    target 2361
  ]
  edge [
    source 25
    target 2362
  ]
  edge [
    source 25
    target 93
  ]
  edge [
    source 25
    target 2363
  ]
  edge [
    source 25
    target 2364
  ]
  edge [
    source 25
    target 2365
  ]
  edge [
    source 25
    target 2366
  ]
  edge [
    source 25
    target 2367
  ]
  edge [
    source 25
    target 2368
  ]
  edge [
    source 25
    target 2369
  ]
  edge [
    source 25
    target 2370
  ]
  edge [
    source 25
    target 2047
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 2371
  ]
  edge [
    source 25
    target 2372
  ]
  edge [
    source 25
    target 2373
  ]
  edge [
    source 25
    target 2374
  ]
  edge [
    source 25
    target 2375
  ]
  edge [
    source 25
    target 2376
  ]
  edge [
    source 25
    target 2377
  ]
  edge [
    source 25
    target 2378
  ]
  edge [
    source 25
    target 2379
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 2380
  ]
  edge [
    source 25
    target 2117
  ]
  edge [
    source 25
    target 2381
  ]
  edge [
    source 25
    target 2382
  ]
  edge [
    source 25
    target 2383
  ]
  edge [
    source 25
    target 2384
  ]
  edge [
    source 25
    target 2385
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 2386
  ]
  edge [
    source 26
    target 79
  ]
  edge [
    source 26
    target 2387
  ]
  edge [
    source 26
    target 2388
  ]
  edge [
    source 26
    target 2389
  ]
  edge [
    source 26
    target 2390
  ]
  edge [
    source 26
    target 2391
  ]
  edge [
    source 26
    target 2392
  ]
  edge [
    source 26
    target 2393
  ]
  edge [
    source 26
    target 2394
  ]
  edge [
    source 26
    target 2395
  ]
  edge [
    source 26
    target 758
  ]
  edge [
    source 26
    target 2396
  ]
  edge [
    source 26
    target 512
  ]
  edge [
    source 26
    target 2397
  ]
  edge [
    source 26
    target 2398
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 1422
  ]
  edge [
    source 26
    target 2399
  ]
  edge [
    source 26
    target 2400
  ]
  edge [
    source 26
    target 793
  ]
  edge [
    source 26
    target 2401
  ]
  edge [
    source 26
    target 2402
  ]
  edge [
    source 26
    target 2403
  ]
  edge [
    source 26
    target 2404
  ]
  edge [
    source 26
    target 2405
  ]
  edge [
    source 26
    target 2406
  ]
  edge [
    source 26
    target 2407
  ]
  edge [
    source 26
    target 2408
  ]
  edge [
    source 26
    target 2409
  ]
  edge [
    source 26
    target 766
  ]
  edge [
    source 26
    target 767
  ]
  edge [
    source 26
    target 2410
  ]
  edge [
    source 26
    target 768
  ]
  edge [
    source 26
    target 476
  ]
  edge [
    source 26
    target 2411
  ]
  edge [
    source 26
    target 2412
  ]
  edge [
    source 26
    target 2413
  ]
  edge [
    source 26
    target 770
  ]
  edge [
    source 26
    target 771
  ]
  edge [
    source 26
    target 2414
  ]
  edge [
    source 26
    target 773
  ]
  edge [
    source 26
    target 2415
  ]
  edge [
    source 26
    target 775
  ]
  edge [
    source 26
    target 613
  ]
  edge [
    source 26
    target 774
  ]
  edge [
    source 26
    target 2370
  ]
  edge [
    source 26
    target 779
  ]
  edge [
    source 26
    target 2416
  ]
  edge [
    source 26
    target 435
  ]
  edge [
    source 26
    target 2417
  ]
  edge [
    source 26
    target 2418
  ]
  edge [
    source 26
    target 2419
  ]
  edge [
    source 26
    target 2420
  ]
  edge [
    source 26
    target 2421
  ]
  edge [
    source 26
    target 2422
  ]
  edge [
    source 26
    target 2423
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 2424
  ]
  edge [
    source 28
    target 2425
  ]
  edge [
    source 28
    target 2426
  ]
  edge [
    source 28
    target 2427
  ]
  edge [
    source 28
    target 2428
  ]
  edge [
    source 28
    target 2429
  ]
  edge [
    source 28
    target 2430
  ]
  edge [
    source 28
    target 2431
  ]
  edge [
    source 28
    target 2432
  ]
  edge [
    source 28
    target 2433
  ]
  edge [
    source 28
    target 2434
  ]
  edge [
    source 28
    target 2435
  ]
  edge [
    source 28
    target 2436
  ]
  edge [
    source 28
    target 2437
  ]
  edge [
    source 28
    target 2438
  ]
  edge [
    source 28
    target 2439
  ]
  edge [
    source 28
    target 2440
  ]
  edge [
    source 28
    target 2441
  ]
  edge [
    source 28
    target 2442
  ]
  edge [
    source 28
    target 2443
  ]
  edge [
    source 28
    target 2444
  ]
  edge [
    source 28
    target 2445
  ]
  edge [
    source 28
    target 2446
  ]
  edge [
    source 28
    target 2447
  ]
  edge [
    source 28
    target 2448
  ]
  edge [
    source 28
    target 839
  ]
  edge [
    source 28
    target 2449
  ]
  edge [
    source 28
    target 2450
  ]
  edge [
    source 28
    target 2451
  ]
  edge [
    source 28
    target 2452
  ]
  edge [
    source 28
    target 2453
  ]
  edge [
    source 28
    target 2454
  ]
  edge [
    source 28
    target 2455
  ]
  edge [
    source 28
    target 2456
  ]
  edge [
    source 28
    target 2457
  ]
  edge [
    source 28
    target 2458
  ]
  edge [
    source 28
    target 977
  ]
  edge [
    source 28
    target 980
  ]
  edge [
    source 28
    target 2459
  ]
  edge [
    source 28
    target 2460
  ]
  edge [
    source 28
    target 472
  ]
  edge [
    source 28
    target 2461
  ]
  edge [
    source 28
    target 2462
  ]
  edge [
    source 28
    target 208
  ]
  edge [
    source 28
    target 2463
  ]
  edge [
    source 28
    target 2464
  ]
  edge [
    source 28
    target 2465
  ]
  edge [
    source 28
    target 2466
  ]
  edge [
    source 28
    target 2467
  ]
  edge [
    source 28
    target 2468
  ]
  edge [
    source 28
    target 2469
  ]
  edge [
    source 28
    target 2470
  ]
  edge [
    source 28
    target 2471
  ]
  edge [
    source 28
    target 2472
  ]
  edge [
    source 28
    target 2473
  ]
  edge [
    source 28
    target 2474
  ]
  edge [
    source 28
    target 2475
  ]
  edge [
    source 28
    target 212
  ]
  edge [
    source 28
    target 2476
  ]
  edge [
    source 28
    target 2477
  ]
  edge [
    source 28
    target 2478
  ]
  edge [
    source 28
    target 1007
  ]
  edge [
    source 28
    target 2479
  ]
  edge [
    source 28
    target 2480
  ]
  edge [
    source 28
    target 2481
  ]
  edge [
    source 28
    target 2482
  ]
  edge [
    source 28
    target 2483
  ]
  edge [
    source 28
    target 2484
  ]
  edge [
    source 28
    target 2485
  ]
  edge [
    source 28
    target 2486
  ]
  edge [
    source 28
    target 2487
  ]
  edge [
    source 28
    target 2488
  ]
  edge [
    source 28
    target 2489
  ]
  edge [
    source 28
    target 2490
  ]
  edge [
    source 28
    target 2491
  ]
  edge [
    source 28
    target 2492
  ]
  edge [
    source 28
    target 2493
  ]
  edge [
    source 28
    target 2494
  ]
  edge [
    source 28
    target 2495
  ]
  edge [
    source 28
    target 2496
  ]
  edge [
    source 28
    target 2497
  ]
  edge [
    source 28
    target 2498
  ]
  edge [
    source 28
    target 2499
  ]
  edge [
    source 28
    target 2500
  ]
  edge [
    source 28
    target 2501
  ]
  edge [
    source 28
    target 73
  ]
  edge [
    source 28
    target 2502
  ]
  edge [
    source 28
    target 1387
  ]
  edge [
    source 28
    target 2503
  ]
  edge [
    source 28
    target 2504
  ]
  edge [
    source 28
    target 2505
  ]
  edge [
    source 28
    target 435
  ]
  edge [
    source 28
    target 2506
  ]
  edge [
    source 28
    target 2507
  ]
  edge [
    source 28
    target 2508
  ]
  edge [
    source 28
    target 2509
  ]
  edge [
    source 28
    target 2510
  ]
  edge [
    source 28
    target 2511
  ]
  edge [
    source 28
    target 2512
  ]
  edge [
    source 28
    target 2513
  ]
  edge [
    source 28
    target 2514
  ]
  edge [
    source 28
    target 2515
  ]
  edge [
    source 28
    target 2516
  ]
  edge [
    source 28
    target 2517
  ]
  edge [
    source 28
    target 2518
  ]
  edge [
    source 28
    target 2519
  ]
  edge [
    source 28
    target 2520
  ]
  edge [
    source 28
    target 674
  ]
  edge [
    source 28
    target 2521
  ]
  edge [
    source 28
    target 2522
  ]
  edge [
    source 28
    target 2523
  ]
  edge [
    source 28
    target 2524
  ]
  edge [
    source 28
    target 2525
  ]
  edge [
    source 28
    target 2526
  ]
  edge [
    source 28
    target 2527
  ]
  edge [
    source 28
    target 2528
  ]
  edge [
    source 28
    target 2529
  ]
  edge [
    source 28
    target 2530
  ]
  edge [
    source 28
    target 2531
  ]
  edge [
    source 28
    target 2532
  ]
  edge [
    source 28
    target 2533
  ]
  edge [
    source 28
    target 2534
  ]
  edge [
    source 28
    target 895
  ]
  edge [
    source 28
    target 2535
  ]
  edge [
    source 28
    target 2536
  ]
  edge [
    source 28
    target 2537
  ]
  edge [
    source 28
    target 2538
  ]
  edge [
    source 28
    target 732
  ]
  edge [
    source 28
    target 2539
  ]
  edge [
    source 28
    target 2540
  ]
  edge [
    source 28
    target 2541
  ]
  edge [
    source 28
    target 756
  ]
  edge [
    source 28
    target 2542
  ]
  edge [
    source 28
    target 2543
  ]
  edge [
    source 28
    target 2544
  ]
  edge [
    source 28
    target 2545
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 3412
  ]
  edge [
    source 28
    target 3413
  ]
  edge [
    source 28
    target 3414
  ]
  edge [
    source 28
    target 3415
  ]
  edge [
    source 28
    target 3416
  ]
  edge [
    source 28
    target 3417
  ]
  edge [
    source 28
    target 3418
  ]
  edge [
    source 28
    target 410
  ]
  edge [
    source 28
    target 3419
  ]
  edge [
    source 28
    target 51
  ]
  edge [
    source 28
    target 3420
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 3421
  ]
  edge [
    source 28
    target 3422
  ]
  edge [
    source 28
    target 638
  ]
  edge [
    source 28
    target 35
  ]
  edge [
    source 28
    target 36
  ]
  edge [
    source 28
    target 37
  ]
  edge [
    source 29
    target 2442
  ]
  edge [
    source 29
    target 2546
  ]
  edge [
    source 29
    target 2547
  ]
  edge [
    source 29
    target 2548
  ]
  edge [
    source 29
    target 2518
  ]
  edge [
    source 29
    target 2441
  ]
  edge [
    source 29
    target 2549
  ]
  edge [
    source 29
    target 2550
  ]
  edge [
    source 29
    target 2444
  ]
  edge [
    source 29
    target 51
  ]
  edge [
    source 29
    target 2551
  ]
  edge [
    source 29
    target 2552
  ]
  edge [
    source 29
    target 3412
  ]
  edge [
    source 29
    target 3413
  ]
  edge [
    source 29
    target 3414
  ]
  edge [
    source 29
    target 3415
  ]
  edge [
    source 29
    target 3416
  ]
  edge [
    source 29
    target 3417
  ]
  edge [
    source 29
    target 3418
  ]
  edge [
    source 29
    target 410
  ]
  edge [
    source 29
    target 3419
  ]
  edge [
    source 29
    target 3420
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 31
  ]
  edge [
    source 29
    target 3421
  ]
  edge [
    source 29
    target 3422
  ]
  edge [
    source 29
    target 638
  ]
  edge [
    source 29
    target 34
  ]
  edge [
    source 29
    target 35
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 29
    target 37
  ]
  edge [
    source 30
    target 2553
  ]
  edge [
    source 30
    target 2554
  ]
  edge [
    source 30
    target 2555
  ]
  edge [
    source 30
    target 2556
  ]
  edge [
    source 30
    target 530
  ]
  edge [
    source 30
    target 2557
  ]
  edge [
    source 30
    target 317
  ]
  edge [
    source 30
    target 2558
  ]
  edge [
    source 30
    target 2559
  ]
  edge [
    source 30
    target 2560
  ]
  edge [
    source 30
    target 2561
  ]
  edge [
    source 30
    target 2562
  ]
  edge [
    source 30
    target 93
  ]
  edge [
    source 30
    target 2563
  ]
  edge [
    source 30
    target 2564
  ]
  edge [
    source 30
    target 2565
  ]
  edge [
    source 30
    target 2566
  ]
  edge [
    source 30
    target 2567
  ]
  edge [
    source 30
    target 2568
  ]
  edge [
    source 30
    target 2569
  ]
  edge [
    source 30
    target 2570
  ]
  edge [
    source 30
    target 200
  ]
  edge [
    source 30
    target 2571
  ]
  edge [
    source 30
    target 512
  ]
  edge [
    source 30
    target 2572
  ]
  edge [
    source 30
    target 192
  ]
  edge [
    source 30
    target 2573
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 30
    target 973
  ]
  edge [
    source 30
    target 2574
  ]
  edge [
    source 30
    target 1103
  ]
  edge [
    source 30
    target 476
  ]
  edge [
    source 30
    target 2575
  ]
  edge [
    source 30
    target 2576
  ]
  edge [
    source 30
    target 1010
  ]
  edge [
    source 30
    target 2577
  ]
  edge [
    source 30
    target 2578
  ]
  edge [
    source 30
    target 2579
  ]
  edge [
    source 30
    target 2580
  ]
  edge [
    source 30
    target 80
  ]
  edge [
    source 30
    target 2078
  ]
  edge [
    source 30
    target 2581
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 2582
  ]
  edge [
    source 30
    target 2583
  ]
  edge [
    source 30
    target 2584
  ]
  edge [
    source 30
    target 2585
  ]
  edge [
    source 30
    target 781
  ]
  edge [
    source 30
    target 2586
  ]
  edge [
    source 30
    target 2587
  ]
  edge [
    source 30
    target 2588
  ]
  edge [
    source 30
    target 2589
  ]
  edge [
    source 30
    target 2590
  ]
  edge [
    source 30
    target 2591
  ]
  edge [
    source 30
    target 2592
  ]
  edge [
    source 30
    target 2593
  ]
  edge [
    source 30
    target 2594
  ]
  edge [
    source 30
    target 2595
  ]
  edge [
    source 30
    target 2596
  ]
  edge [
    source 30
    target 207
  ]
  edge [
    source 30
    target 2597
  ]
  edge [
    source 30
    target 2598
  ]
  edge [
    source 30
    target 2599
  ]
  edge [
    source 30
    target 2600
  ]
  edge [
    source 30
    target 2601
  ]
  edge [
    source 30
    target 2602
  ]
  edge [
    source 30
    target 2603
  ]
  edge [
    source 30
    target 2604
  ]
  edge [
    source 30
    target 2605
  ]
  edge [
    source 30
    target 103
  ]
  edge [
    source 30
    target 2606
  ]
  edge [
    source 30
    target 2607
  ]
  edge [
    source 30
    target 2608
  ]
  edge [
    source 30
    target 2609
  ]
  edge [
    source 30
    target 2610
  ]
  edge [
    source 30
    target 2611
  ]
  edge [
    source 30
    target 3412
  ]
  edge [
    source 30
    target 3413
  ]
  edge [
    source 30
    target 3414
  ]
  edge [
    source 30
    target 3415
  ]
  edge [
    source 30
    target 3416
  ]
  edge [
    source 30
    target 3417
  ]
  edge [
    source 30
    target 3418
  ]
  edge [
    source 30
    target 410
  ]
  edge [
    source 30
    target 3419
  ]
  edge [
    source 30
    target 51
  ]
  edge [
    source 30
    target 3420
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 3421
  ]
  edge [
    source 30
    target 3422
  ]
  edge [
    source 30
    target 638
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 36
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 2612
  ]
  edge [
    source 31
    target 2613
  ]
  edge [
    source 31
    target 2614
  ]
  edge [
    source 31
    target 2615
  ]
  edge [
    source 31
    target 2616
  ]
  edge [
    source 31
    target 2617
  ]
  edge [
    source 31
    target 2618
  ]
  edge [
    source 31
    target 2619
  ]
  edge [
    source 31
    target 2620
  ]
  edge [
    source 31
    target 2621
  ]
  edge [
    source 31
    target 2622
  ]
  edge [
    source 31
    target 2623
  ]
  edge [
    source 31
    target 2624
  ]
  edge [
    source 31
    target 2625
  ]
  edge [
    source 31
    target 2626
  ]
  edge [
    source 31
    target 2627
  ]
  edge [
    source 31
    target 2628
  ]
  edge [
    source 31
    target 2629
  ]
  edge [
    source 31
    target 2630
  ]
  edge [
    source 31
    target 2631
  ]
  edge [
    source 31
    target 3412
  ]
  edge [
    source 31
    target 3413
  ]
  edge [
    source 31
    target 3414
  ]
  edge [
    source 31
    target 3415
  ]
  edge [
    source 31
    target 3416
  ]
  edge [
    source 31
    target 3417
  ]
  edge [
    source 31
    target 3418
  ]
  edge [
    source 31
    target 410
  ]
  edge [
    source 31
    target 3419
  ]
  edge [
    source 31
    target 51
  ]
  edge [
    source 31
    target 3420
  ]
  edge [
    source 31
    target 3421
  ]
  edge [
    source 31
    target 3422
  ]
  edge [
    source 31
    target 638
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 2632
  ]
  edge [
    source 32
    target 2633
  ]
  edge [
    source 32
    target 2634
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 647
  ]
  edge [
    source 33
    target 2571
  ]
  edge [
    source 33
    target 2635
  ]
  edge [
    source 33
    target 2370
  ]
  edge [
    source 33
    target 2636
  ]
  edge [
    source 33
    target 2637
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 33
    target 156
  ]
  edge [
    source 33
    target 2638
  ]
  edge [
    source 33
    target 233
  ]
  edge [
    source 33
    target 202
  ]
  edge [
    source 33
    target 2639
  ]
  edge [
    source 33
    target 2640
  ]
  edge [
    source 33
    target 2559
  ]
  edge [
    source 33
    target 2641
  ]
  edge [
    source 33
    target 2642
  ]
  edge [
    source 33
    target 2643
  ]
  edge [
    source 33
    target 2644
  ]
  edge [
    source 33
    target 93
  ]
  edge [
    source 33
    target 2645
  ]
  edge [
    source 33
    target 2398
  ]
  edge [
    source 33
    target 2646
  ]
  edge [
    source 33
    target 2401
  ]
  edge [
    source 33
    target 79
  ]
  edge [
    source 33
    target 2647
  ]
  edge [
    source 33
    target 2405
  ]
  edge [
    source 33
    target 2648
  ]
  edge [
    source 33
    target 873
  ]
  edge [
    source 33
    target 2407
  ]
  edge [
    source 33
    target 2649
  ]
  edge [
    source 33
    target 2650
  ]
  edge [
    source 33
    target 2651
  ]
  edge [
    source 33
    target 2652
  ]
  edge [
    source 33
    target 2653
  ]
  edge [
    source 33
    target 2654
  ]
  edge [
    source 33
    target 2655
  ]
  edge [
    source 33
    target 2656
  ]
  edge [
    source 33
    target 2657
  ]
  edge [
    source 33
    target 2658
  ]
  edge [
    source 33
    target 2417
  ]
  edge [
    source 33
    target 2659
  ]
  edge [
    source 33
    target 2660
  ]
  edge [
    source 33
    target 309
  ]
  edge [
    source 33
    target 2661
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 2662
  ]
  edge [
    source 34
    target 2663
  ]
  edge [
    source 34
    target 2142
  ]
  edge [
    source 34
    target 2664
  ]
  edge [
    source 34
    target 2665
  ]
  edge [
    source 34
    target 2666
  ]
  edge [
    source 34
    target 2667
  ]
  edge [
    source 34
    target 2141
  ]
  edge [
    source 34
    target 2668
  ]
  edge [
    source 34
    target 2669
  ]
  edge [
    source 34
    target 2670
  ]
  edge [
    source 34
    target 2671
  ]
  edge [
    source 34
    target 2672
  ]
  edge [
    source 34
    target 2673
  ]
  edge [
    source 34
    target 1422
  ]
  edge [
    source 34
    target 2674
  ]
  edge [
    source 34
    target 2675
  ]
  edge [
    source 34
    target 2676
  ]
  edge [
    source 34
    target 223
  ]
  edge [
    source 34
    target 3412
  ]
  edge [
    source 34
    target 3413
  ]
  edge [
    source 34
    target 3414
  ]
  edge [
    source 34
    target 3415
  ]
  edge [
    source 34
    target 3416
  ]
  edge [
    source 34
    target 3417
  ]
  edge [
    source 34
    target 3418
  ]
  edge [
    source 34
    target 410
  ]
  edge [
    source 34
    target 3419
  ]
  edge [
    source 34
    target 51
  ]
  edge [
    source 34
    target 3420
  ]
  edge [
    source 34
    target 3421
  ]
  edge [
    source 34
    target 3422
  ]
  edge [
    source 34
    target 638
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 2677
  ]
  edge [
    source 35
    target 2152
  ]
  edge [
    source 35
    target 2678
  ]
  edge [
    source 35
    target 2679
  ]
  edge [
    source 35
    target 2680
  ]
  edge [
    source 35
    target 2681
  ]
  edge [
    source 35
    target 2682
  ]
  edge [
    source 35
    target 2154
  ]
  edge [
    source 35
    target 2683
  ]
  edge [
    source 35
    target 2684
  ]
  edge [
    source 35
    target 2685
  ]
  edge [
    source 35
    target 2686
  ]
  edge [
    source 35
    target 2687
  ]
  edge [
    source 35
    target 2688
  ]
  edge [
    source 35
    target 2689
  ]
  edge [
    source 35
    target 2690
  ]
  edge [
    source 35
    target 2691
  ]
  edge [
    source 35
    target 2692
  ]
  edge [
    source 35
    target 2693
  ]
  edge [
    source 35
    target 2694
  ]
  edge [
    source 35
    target 2695
  ]
  edge [
    source 35
    target 2696
  ]
  edge [
    source 35
    target 2697
  ]
  edge [
    source 35
    target 2698
  ]
  edge [
    source 35
    target 2699
  ]
  edge [
    source 35
    target 2700
  ]
  edge [
    source 35
    target 2155
  ]
  edge [
    source 35
    target 2150
  ]
  edge [
    source 35
    target 2701
  ]
  edge [
    source 35
    target 3412
  ]
  edge [
    source 35
    target 3413
  ]
  edge [
    source 35
    target 3414
  ]
  edge [
    source 35
    target 3415
  ]
  edge [
    source 35
    target 3416
  ]
  edge [
    source 35
    target 3417
  ]
  edge [
    source 35
    target 3418
  ]
  edge [
    source 35
    target 410
  ]
  edge [
    source 35
    target 3419
  ]
  edge [
    source 35
    target 51
  ]
  edge [
    source 35
    target 3420
  ]
  edge [
    source 35
    target 3421
  ]
  edge [
    source 35
    target 3422
  ]
  edge [
    source 35
    target 638
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 2702
  ]
  edge [
    source 36
    target 1459
  ]
  edge [
    source 36
    target 2703
  ]
  edge [
    source 36
    target 1946
  ]
  edge [
    source 36
    target 1464
  ]
  edge [
    source 36
    target 2704
  ]
  edge [
    source 36
    target 2705
  ]
  edge [
    source 36
    target 2706
  ]
  edge [
    source 36
    target 1694
  ]
  edge [
    source 36
    target 2707
  ]
  edge [
    source 36
    target 2708
  ]
  edge [
    source 36
    target 2709
  ]
  edge [
    source 36
    target 1635
  ]
  edge [
    source 36
    target 2710
  ]
  edge [
    source 36
    target 2711
  ]
  edge [
    source 36
    target 2712
  ]
  edge [
    source 36
    target 2713
  ]
  edge [
    source 36
    target 2714
  ]
  edge [
    source 36
    target 82
  ]
  edge [
    source 36
    target 2715
  ]
  edge [
    source 36
    target 2716
  ]
  edge [
    source 36
    target 2717
  ]
  edge [
    source 36
    target 2718
  ]
  edge [
    source 36
    target 2719
  ]
  edge [
    source 36
    target 2720
  ]
  edge [
    source 36
    target 81
  ]
  edge [
    source 36
    target 2721
  ]
  edge [
    source 36
    target 2722
  ]
  edge [
    source 36
    target 2723
  ]
  edge [
    source 36
    target 2724
  ]
  edge [
    source 36
    target 2725
  ]
  edge [
    source 36
    target 800
  ]
  edge [
    source 36
    target 2726
  ]
  edge [
    source 36
    target 2727
  ]
  edge [
    source 36
    target 2728
  ]
  edge [
    source 36
    target 2729
  ]
  edge [
    source 36
    target 207
  ]
  edge [
    source 36
    target 2730
  ]
  edge [
    source 36
    target 1696
  ]
  edge [
    source 36
    target 1526
  ]
  edge [
    source 36
    target 1618
  ]
  edge [
    source 36
    target 2731
  ]
  edge [
    source 36
    target 1654
  ]
  edge [
    source 36
    target 1267
  ]
  edge [
    source 36
    target 2732
  ]
  edge [
    source 36
    target 180
  ]
  edge [
    source 36
    target 2733
  ]
  edge [
    source 36
    target 2734
  ]
  edge [
    source 36
    target 2735
  ]
  edge [
    source 36
    target 2736
  ]
  edge [
    source 36
    target 2737
  ]
  edge [
    source 36
    target 2738
  ]
  edge [
    source 36
    target 2739
  ]
  edge [
    source 36
    target 2740
  ]
  edge [
    source 36
    target 2741
  ]
  edge [
    source 36
    target 111
  ]
  edge [
    source 36
    target 2742
  ]
  edge [
    source 36
    target 2743
  ]
  edge [
    source 36
    target 2744
  ]
  edge [
    source 36
    target 2745
  ]
  edge [
    source 36
    target 2746
  ]
  edge [
    source 36
    target 2747
  ]
  edge [
    source 36
    target 2748
  ]
  edge [
    source 36
    target 2749
  ]
  edge [
    source 36
    target 2750
  ]
  edge [
    source 36
    target 2751
  ]
  edge [
    source 36
    target 2752
  ]
  edge [
    source 36
    target 2753
  ]
  edge [
    source 36
    target 2754
  ]
  edge [
    source 36
    target 2755
  ]
  edge [
    source 36
    target 2756
  ]
  edge [
    source 36
    target 2757
  ]
  edge [
    source 36
    target 862
  ]
  edge [
    source 36
    target 2758
  ]
  edge [
    source 36
    target 2759
  ]
  edge [
    source 36
    target 2760
  ]
  edge [
    source 36
    target 1037
  ]
  edge [
    source 36
    target 2761
  ]
  edge [
    source 36
    target 2762
  ]
  edge [
    source 36
    target 1416
  ]
  edge [
    source 36
    target 2763
  ]
  edge [
    source 36
    target 2764
  ]
  edge [
    source 36
    target 2765
  ]
  edge [
    source 36
    target 2766
  ]
  edge [
    source 36
    target 613
  ]
  edge [
    source 36
    target 2767
  ]
  edge [
    source 36
    target 2768
  ]
  edge [
    source 36
    target 2769
  ]
  edge [
    source 36
    target 2770
  ]
  edge [
    source 36
    target 2771
  ]
  edge [
    source 36
    target 2772
  ]
  edge [
    source 36
    target 2773
  ]
  edge [
    source 36
    target 2774
  ]
  edge [
    source 36
    target 2775
  ]
  edge [
    source 36
    target 2776
  ]
  edge [
    source 36
    target 2777
  ]
  edge [
    source 36
    target 2778
  ]
  edge [
    source 36
    target 1146
  ]
  edge [
    source 36
    target 2779
  ]
  edge [
    source 36
    target 2780
  ]
  edge [
    source 36
    target 2781
  ]
  edge [
    source 36
    target 2782
  ]
  edge [
    source 36
    target 2783
  ]
  edge [
    source 36
    target 2784
  ]
  edge [
    source 36
    target 2785
  ]
  edge [
    source 36
    target 2786
  ]
  edge [
    source 36
    target 2787
  ]
  edge [
    source 36
    target 2788
  ]
  edge [
    source 36
    target 2789
  ]
  edge [
    source 36
    target 2790
  ]
  edge [
    source 36
    target 2791
  ]
  edge [
    source 36
    target 2134
  ]
  edge [
    source 36
    target 2792
  ]
  edge [
    source 36
    target 2793
  ]
  edge [
    source 36
    target 2794
  ]
  edge [
    source 36
    target 2795
  ]
  edge [
    source 36
    target 2796
  ]
  edge [
    source 36
    target 2797
  ]
  edge [
    source 36
    target 2798
  ]
  edge [
    source 36
    target 2362
  ]
  edge [
    source 36
    target 3412
  ]
  edge [
    source 36
    target 3413
  ]
  edge [
    source 36
    target 3414
  ]
  edge [
    source 36
    target 3415
  ]
  edge [
    source 36
    target 3416
  ]
  edge [
    source 36
    target 3417
  ]
  edge [
    source 36
    target 3418
  ]
  edge [
    source 36
    target 410
  ]
  edge [
    source 36
    target 3419
  ]
  edge [
    source 36
    target 51
  ]
  edge [
    source 36
    target 3420
  ]
  edge [
    source 36
    target 3421
  ]
  edge [
    source 36
    target 3422
  ]
  edge [
    source 36
    target 638
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 2799
  ]
  edge [
    source 37
    target 2800
  ]
  edge [
    source 37
    target 2801
  ]
  edge [
    source 37
    target 2802
  ]
  edge [
    source 37
    target 125
  ]
  edge [
    source 37
    target 2803
  ]
  edge [
    source 37
    target 2804
  ]
  edge [
    source 37
    target 2805
  ]
  edge [
    source 37
    target 2153
  ]
  edge [
    source 37
    target 2806
  ]
  edge [
    source 37
    target 2807
  ]
  edge [
    source 37
    target 2808
  ]
  edge [
    source 37
    target 2809
  ]
  edge [
    source 37
    target 2810
  ]
  edge [
    source 37
    target 2689
  ]
  edge [
    source 37
    target 2177
  ]
  edge [
    source 37
    target 2811
  ]
  edge [
    source 37
    target 2812
  ]
  edge [
    source 37
    target 2813
  ]
  edge [
    source 37
    target 2814
  ]
  edge [
    source 37
    target 2815
  ]
  edge [
    source 37
    target 2816
  ]
  edge [
    source 37
    target 2817
  ]
  edge [
    source 37
    target 2818
  ]
  edge [
    source 37
    target 2187
  ]
  edge [
    source 37
    target 3412
  ]
  edge [
    source 37
    target 3413
  ]
  edge [
    source 37
    target 3414
  ]
  edge [
    source 37
    target 3415
  ]
  edge [
    source 37
    target 3416
  ]
  edge [
    source 37
    target 3417
  ]
  edge [
    source 37
    target 3418
  ]
  edge [
    source 37
    target 410
  ]
  edge [
    source 37
    target 3419
  ]
  edge [
    source 37
    target 51
  ]
  edge [
    source 37
    target 3420
  ]
  edge [
    source 37
    target 3421
  ]
  edge [
    source 37
    target 3422
  ]
  edge [
    source 37
    target 638
  ]
  edge [
    source 38
    target 2430
  ]
  edge [
    source 38
    target 2509
  ]
  edge [
    source 38
    target 2425
  ]
  edge [
    source 38
    target 2510
  ]
  edge [
    source 38
    target 2511
  ]
  edge [
    source 38
    target 2444
  ]
  edge [
    source 38
    target 2512
  ]
  edge [
    source 38
    target 2513
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 2819
  ]
  edge [
    source 39
    target 2820
  ]
  edge [
    source 39
    target 2821
  ]
  edge [
    source 39
    target 2822
  ]
  edge [
    source 39
    target 2823
  ]
  edge [
    source 39
    target 2824
  ]
  edge [
    source 39
    target 2825
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 530
  ]
  edge [
    source 40
    target 591
  ]
  edge [
    source 40
    target 592
  ]
  edge [
    source 40
    target 593
  ]
  edge [
    source 40
    target 578
  ]
  edge [
    source 40
    target 594
  ]
  edge [
    source 40
    target 208
  ]
  edge [
    source 40
    target 595
  ]
  edge [
    source 40
    target 2589
  ]
  edge [
    source 40
    target 502
  ]
  edge [
    source 40
    target 2645
  ]
  edge [
    source 40
    target 2826
  ]
  edge [
    source 40
    target 2219
  ]
  edge [
    source 40
    target 2561
  ]
  edge [
    source 40
    target 2562
  ]
  edge [
    source 40
    target 93
  ]
  edge [
    source 40
    target 2563
  ]
  edge [
    source 40
    target 2564
  ]
  edge [
    source 40
    target 2565
  ]
  edge [
    source 40
    target 2566
  ]
  edge [
    source 40
    target 2827
  ]
  edge [
    source 40
    target 2828
  ]
  edge [
    source 40
    target 89
  ]
  edge [
    source 40
    target 2829
  ]
  edge [
    source 40
    target 2830
  ]
  edge [
    source 40
    target 2831
  ]
  edge [
    source 40
    target 2832
  ]
  edge [
    source 40
    target 2833
  ]
  edge [
    source 40
    target 2107
  ]
  edge [
    source 40
    target 2834
  ]
  edge [
    source 40
    target 2640
  ]
  edge [
    source 40
    target 2835
  ]
  edge [
    source 40
    target 320
  ]
  edge [
    source 40
    target 542
  ]
  edge [
    source 40
    target 543
  ]
  edge [
    source 40
    target 544
  ]
  edge [
    source 40
    target 545
  ]
  edge [
    source 40
    target 490
  ]
  edge [
    source 40
    target 546
  ]
  edge [
    source 40
    target 547
  ]
  edge [
    source 40
    target 549
  ]
  edge [
    source 40
    target 550
  ]
  edge [
    source 40
    target 551
  ]
  edge [
    source 40
    target 552
  ]
  edge [
    source 40
    target 553
  ]
  edge [
    source 40
    target 554
  ]
  edge [
    source 40
    target 555
  ]
  edge [
    source 40
    target 556
  ]
  edge [
    source 40
    target 557
  ]
  edge [
    source 40
    target 559
  ]
  edge [
    source 40
    target 560
  ]
  edge [
    source 40
    target 561
  ]
  edge [
    source 40
    target 562
  ]
  edge [
    source 40
    target 563
  ]
  edge [
    source 40
    target 564
  ]
  edge [
    source 40
    target 565
  ]
  edge [
    source 40
    target 566
  ]
  edge [
    source 40
    target 567
  ]
  edge [
    source 40
    target 568
  ]
  edge [
    source 40
    target 569
  ]
  edge [
    source 40
    target 2836
  ]
  edge [
    source 40
    target 2837
  ]
  edge [
    source 40
    target 2838
  ]
  edge [
    source 40
    target 2839
  ]
  edge [
    source 40
    target 2840
  ]
  edge [
    source 40
    target 693
  ]
  edge [
    source 40
    target 2841
  ]
  edge [
    source 40
    target 2842
  ]
  edge [
    source 40
    target 2843
  ]
  edge [
    source 40
    target 2844
  ]
  edge [
    source 40
    target 354
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 2845
  ]
  edge [
    source 42
    target 2846
  ]
  edge [
    source 42
    target 1144
  ]
  edge [
    source 42
    target 303
  ]
  edge [
    source 42
    target 2847
  ]
  edge [
    source 42
    target 2848
  ]
  edge [
    source 42
    target 1151
  ]
  edge [
    source 42
    target 2849
  ]
  edge [
    source 42
    target 2850
  ]
  edge [
    source 42
    target 2851
  ]
  edge [
    source 42
    target 2852
  ]
  edge [
    source 42
    target 2853
  ]
  edge [
    source 42
    target 2854
  ]
  edge [
    source 42
    target 2855
  ]
  edge [
    source 42
    target 2856
  ]
  edge [
    source 42
    target 2857
  ]
  edge [
    source 42
    target 2858
  ]
  edge [
    source 42
    target 2859
  ]
  edge [
    source 42
    target 2860
  ]
  edge [
    source 42
    target 2861
  ]
  edge [
    source 42
    target 2862
  ]
  edge [
    source 42
    target 2863
  ]
  edge [
    source 42
    target 2864
  ]
  edge [
    source 42
    target 2865
  ]
  edge [
    source 42
    target 2866
  ]
  edge [
    source 42
    target 2867
  ]
  edge [
    source 42
    target 2031
  ]
  edge [
    source 42
    target 2868
  ]
  edge [
    source 42
    target 2869
  ]
  edge [
    source 42
    target 2870
  ]
  edge [
    source 42
    target 2871
  ]
  edge [
    source 42
    target 2872
  ]
  edge [
    source 42
    target 2873
  ]
  edge [
    source 42
    target 2529
  ]
  edge [
    source 42
    target 2874
  ]
  edge [
    source 42
    target 2875
  ]
  edge [
    source 42
    target 2876
  ]
  edge [
    source 42
    target 638
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 2877
  ]
  edge [
    source 43
    target 2878
  ]
  edge [
    source 43
    target 448
  ]
  edge [
    source 43
    target 2879
  ]
  edge [
    source 43
    target 2880
  ]
  edge [
    source 43
    target 93
  ]
  edge [
    source 43
    target 385
  ]
  edge [
    source 43
    target 2881
  ]
  edge [
    source 43
    target 2882
  ]
  edge [
    source 43
    target 528
  ]
  edge [
    source 43
    target 529
  ]
  edge [
    source 43
    target 530
  ]
  edge [
    source 43
    target 2883
  ]
  edge [
    source 43
    target 2120
  ]
  edge [
    source 43
    target 2884
  ]
  edge [
    source 43
    target 2885
  ]
  edge [
    source 43
    target 220
  ]
  edge [
    source 43
    target 54
  ]
  edge [
    source 43
    target 2886
  ]
  edge [
    source 43
    target 1014
  ]
  edge [
    source 43
    target 2887
  ]
  edge [
    source 43
    target 2888
  ]
  edge [
    source 43
    target 2889
  ]
  edge [
    source 43
    target 2890
  ]
  edge [
    source 43
    target 2891
  ]
  edge [
    source 43
    target 2892
  ]
  edge [
    source 43
    target 2893
  ]
  edge [
    source 43
    target 2894
  ]
  edge [
    source 43
    target 2895
  ]
  edge [
    source 43
    target 2896
  ]
  edge [
    source 43
    target 2897
  ]
  edge [
    source 43
    target 2898
  ]
  edge [
    source 43
    target 2899
  ]
  edge [
    source 43
    target 2900
  ]
  edge [
    source 43
    target 223
  ]
  edge [
    source 43
    target 2901
  ]
  edge [
    source 43
    target 320
  ]
  edge [
    source 43
    target 512
  ]
  edge [
    source 43
    target 2902
  ]
  edge [
    source 43
    target 2903
  ]
  edge [
    source 43
    target 2904
  ]
  edge [
    source 43
    target 2126
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 118
  ]
  edge [
    source 44
    target 119
  ]
  edge [
    source 44
    target 120
  ]
  edge [
    source 44
    target 121
  ]
  edge [
    source 44
    target 122
  ]
  edge [
    source 44
    target 123
  ]
  edge [
    source 44
    target 124
  ]
  edge [
    source 44
    target 125
  ]
  edge [
    source 44
    target 55
  ]
  edge [
    source 45
    target 2905
  ]
  edge [
    source 45
    target 2906
  ]
  edge [
    source 45
    target 2907
  ]
  edge [
    source 45
    target 2908
  ]
  edge [
    source 45
    target 2909
  ]
  edge [
    source 45
    target 2910
  ]
  edge [
    source 45
    target 2911
  ]
  edge [
    source 45
    target 2912
  ]
  edge [
    source 45
    target 2913
  ]
  edge [
    source 45
    target 50
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 2914
  ]
  edge [
    source 46
    target 2915
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 2916
  ]
  edge [
    source 47
    target 2917
  ]
  edge [
    source 47
    target 2918
  ]
  edge [
    source 47
    target 622
  ]
  edge [
    source 47
    target 1370
  ]
  edge [
    source 47
    target 2919
  ]
  edge [
    source 47
    target 2920
  ]
  edge [
    source 47
    target 2921
  ]
  edge [
    source 47
    target 2922
  ]
  edge [
    source 47
    target 2923
  ]
  edge [
    source 47
    target 2924
  ]
  edge [
    source 47
    target 2925
  ]
  edge [
    source 47
    target 294
  ]
  edge [
    source 47
    target 2926
  ]
  edge [
    source 47
    target 2927
  ]
  edge [
    source 47
    target 2928
  ]
  edge [
    source 47
    target 2929
  ]
  edge [
    source 47
    target 2930
  ]
  edge [
    source 47
    target 2931
  ]
  edge [
    source 47
    target 617
  ]
  edge [
    source 47
    target 2932
  ]
  edge [
    source 47
    target 2933
  ]
  edge [
    source 47
    target 2934
  ]
  edge [
    source 47
    target 2935
  ]
  edge [
    source 47
    target 2936
  ]
  edge [
    source 47
    target 2937
  ]
  edge [
    source 47
    target 2938
  ]
  edge [
    source 47
    target 2457
  ]
  edge [
    source 47
    target 2939
  ]
  edge [
    source 47
    target 2940
  ]
  edge [
    source 47
    target 2941
  ]
  edge [
    source 47
    target 911
  ]
  edge [
    source 47
    target 207
  ]
  edge [
    source 47
    target 935
  ]
  edge [
    source 47
    target 2942
  ]
  edge [
    source 47
    target 2943
  ]
  edge [
    source 47
    target 890
  ]
  edge [
    source 47
    target 2944
  ]
  edge [
    source 47
    target 2945
  ]
  edge [
    source 47
    target 2946
  ]
  edge [
    source 47
    target 2947
  ]
  edge [
    source 47
    target 897
  ]
  edge [
    source 47
    target 887
  ]
  edge [
    source 47
    target 889
  ]
  edge [
    source 47
    target 2948
  ]
  edge [
    source 47
    target 2949
  ]
  edge [
    source 47
    target 2950
  ]
  edge [
    source 47
    target 2951
  ]
  edge [
    source 47
    target 1222
  ]
  edge [
    source 47
    target 885
  ]
  edge [
    source 47
    target 453
  ]
  edge [
    source 47
    target 2952
  ]
  edge [
    source 47
    target 2953
  ]
  edge [
    source 47
    target 2954
  ]
  edge [
    source 47
    target 163
  ]
  edge [
    source 47
    target 2955
  ]
  edge [
    source 47
    target 2956
  ]
  edge [
    source 47
    target 2957
  ]
  edge [
    source 47
    target 669
  ]
  edge [
    source 47
    target 2958
  ]
  edge [
    source 47
    target 2959
  ]
  edge [
    source 47
    target 2960
  ]
  edge [
    source 47
    target 1842
  ]
  edge [
    source 47
    target 2961
  ]
  edge [
    source 47
    target 1844
  ]
  edge [
    source 47
    target 2962
  ]
  edge [
    source 47
    target 2963
  ]
  edge [
    source 47
    target 1845
  ]
  edge [
    source 47
    target 2964
  ]
  edge [
    source 47
    target 2965
  ]
  edge [
    source 47
    target 1591
  ]
  edge [
    source 47
    target 1847
  ]
  edge [
    source 47
    target 1848
  ]
  edge [
    source 47
    target 2966
  ]
  edge [
    source 47
    target 1849
  ]
  edge [
    source 47
    target 2967
  ]
  edge [
    source 47
    target 1850
  ]
  edge [
    source 47
    target 233
  ]
  edge [
    source 47
    target 1853
  ]
  edge [
    source 47
    target 1852
  ]
  edge [
    source 47
    target 1721
  ]
  edge [
    source 47
    target 1854
  ]
  edge [
    source 47
    target 1855
  ]
  edge [
    source 47
    target 156
  ]
  edge [
    source 47
    target 1856
  ]
  edge [
    source 47
    target 1857
  ]
  edge [
    source 47
    target 2590
  ]
  edge [
    source 47
    target 1858
  ]
  edge [
    source 47
    target 2968
  ]
  edge [
    source 47
    target 1861
  ]
  edge [
    source 47
    target 2969
  ]
  edge [
    source 47
    target 1862
  ]
  edge [
    source 47
    target 1863
  ]
  edge [
    source 47
    target 1865
  ]
  edge [
    source 47
    target 2141
  ]
  edge [
    source 47
    target 1866
  ]
  edge [
    source 47
    target 1867
  ]
  edge [
    source 47
    target 1870
  ]
  edge [
    source 47
    target 1869
  ]
  edge [
    source 47
    target 1389
  ]
  edge [
    source 47
    target 2970
  ]
  edge [
    source 48
    target 2971
  ]
  edge [
    source 48
    target 294
  ]
  edge [
    source 48
    target 1144
  ]
  edge [
    source 48
    target 2972
  ]
  edge [
    source 48
    target 2973
  ]
  edge [
    source 48
    target 2974
  ]
  edge [
    source 48
    target 895
  ]
  edge [
    source 48
    target 2975
  ]
  edge [
    source 48
    target 2976
  ]
  edge [
    source 48
    target 2977
  ]
  edge [
    source 48
    target 2978
  ]
  edge [
    source 48
    target 2979
  ]
  edge [
    source 48
    target 2980
  ]
  edge [
    source 48
    target 2981
  ]
  edge [
    source 48
    target 2849
  ]
  edge [
    source 48
    target 2982
  ]
  edge [
    source 48
    target 2983
  ]
  edge [
    source 48
    target 2984
  ]
  edge [
    source 48
    target 2985
  ]
  edge [
    source 48
    target 2986
  ]
  edge [
    source 48
    target 2987
  ]
  edge [
    source 48
    target 2988
  ]
  edge [
    source 48
    target 2989
  ]
  edge [
    source 48
    target 2990
  ]
  edge [
    source 48
    target 2991
  ]
  edge [
    source 48
    target 2992
  ]
  edge [
    source 48
    target 2993
  ]
  edge [
    source 48
    target 2994
  ]
  edge [
    source 48
    target 2995
  ]
  edge [
    source 48
    target 2135
  ]
  edge [
    source 48
    target 2851
  ]
  edge [
    source 48
    target 2996
  ]
  edge [
    source 48
    target 2997
  ]
  edge [
    source 48
    target 2924
  ]
  edge [
    source 48
    target 2998
  ]
  edge [
    source 48
    target 2999
  ]
  edge [
    source 48
    target 3000
  ]
  edge [
    source 48
    target 689
  ]
  edge [
    source 48
    target 3001
  ]
  edge [
    source 48
    target 3002
  ]
  edge [
    source 48
    target 3003
  ]
  edge [
    source 48
    target 3004
  ]
  edge [
    source 48
    target 3005
  ]
  edge [
    source 48
    target 873
  ]
  edge [
    source 48
    target 3006
  ]
  edge [
    source 48
    target 932
  ]
  edge [
    source 48
    target 3007
  ]
  edge [
    source 48
    target 3008
  ]
  edge [
    source 48
    target 3009
  ]
  edge [
    source 48
    target 3010
  ]
  edge [
    source 48
    target 201
  ]
  edge [
    source 48
    target 3011
  ]
  edge [
    source 48
    target 3012
  ]
  edge [
    source 48
    target 2047
  ]
  edge [
    source 48
    target 3013
  ]
  edge [
    source 48
    target 3014
  ]
  edge [
    source 48
    target 3015
  ]
  edge [
    source 48
    target 2031
  ]
  edge [
    source 48
    target 481
  ]
  edge [
    source 48
    target 3016
  ]
  edge [
    source 48
    target 3017
  ]
  edge [
    source 48
    target 3018
  ]
  edge [
    source 48
    target 3019
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 3020
  ]
  edge [
    source 49
    target 3021
  ]
  edge [
    source 49
    target 3022
  ]
  edge [
    source 49
    target 3023
  ]
  edge [
    source 49
    target 3024
  ]
  edge [
    source 49
    target 3025
  ]
  edge [
    source 49
    target 2712
  ]
  edge [
    source 49
    target 3026
  ]
  edge [
    source 49
    target 2717
  ]
  edge [
    source 49
    target 2706
  ]
  edge [
    source 49
    target 2718
  ]
  edge [
    source 49
    target 2711
  ]
  edge [
    source 49
    target 2719
  ]
  edge [
    source 49
    target 2702
  ]
  edge [
    source 49
    target 2720
  ]
  edge [
    source 49
    target 81
  ]
  edge [
    source 49
    target 2714
  ]
  edge [
    source 49
    target 2705
  ]
  edge [
    source 49
    target 2710
  ]
  edge [
    source 49
    target 2707
  ]
  edge [
    source 49
    target 2713
  ]
  edge [
    source 49
    target 2721
  ]
  edge [
    source 49
    target 2722
  ]
  edge [
    source 49
    target 2723
  ]
  edge [
    source 49
    target 2724
  ]
  edge [
    source 49
    target 2725
  ]
  edge [
    source 49
    target 800
  ]
  edge [
    source 49
    target 2726
  ]
  edge [
    source 49
    target 2727
  ]
  edge [
    source 49
    target 3027
  ]
  edge [
    source 49
    target 3028
  ]
  edge [
    source 49
    target 794
  ]
  edge [
    source 49
    target 3029
  ]
  edge [
    source 49
    target 3030
  ]
  edge [
    source 49
    target 2920
  ]
  edge [
    source 49
    target 3031
  ]
  edge [
    source 49
    target 3032
  ]
  edge [
    source 49
    target 3033
  ]
  edge [
    source 49
    target 3034
  ]
  edge [
    source 49
    target 2653
  ]
  edge [
    source 49
    target 2715
  ]
  edge [
    source 49
    target 2656
  ]
  edge [
    source 49
    target 3035
  ]
  edge [
    source 49
    target 3036
  ]
  edge [
    source 49
    target 2658
  ]
  edge [
    source 49
    target 576
  ]
  edge [
    source 49
    target 3037
  ]
  edge [
    source 49
    target 3038
  ]
  edge [
    source 49
    target 3039
  ]
  edge [
    source 50
    target 3040
  ]
  edge [
    source 50
    target 3041
  ]
  edge [
    source 50
    target 3042
  ]
  edge [
    source 50
    target 3043
  ]
  edge [
    source 50
    target 3044
  ]
  edge [
    source 50
    target 3045
  ]
  edge [
    source 50
    target 3046
  ]
  edge [
    source 50
    target 93
  ]
  edge [
    source 50
    target 320
  ]
  edge [
    source 50
    target 3047
  ]
  edge [
    source 50
    target 3048
  ]
  edge [
    source 50
    target 3049
  ]
  edge [
    source 50
    target 103
  ]
  edge [
    source 50
    target 2599
  ]
  edge [
    source 50
    target 3050
  ]
  edge [
    source 50
    target 3051
  ]
  edge [
    source 50
    target 3052
  ]
  edge [
    source 50
    target 2643
  ]
  edge [
    source 50
    target 253
  ]
  edge [
    source 50
    target 3053
  ]
  edge [
    source 50
    target 3054
  ]
  edge [
    source 50
    target 2636
  ]
  edge [
    source 50
    target 873
  ]
  edge [
    source 50
    target 2641
  ]
  edge [
    source 50
    target 2642
  ]
  edge [
    source 50
    target 2644
  ]
  edge [
    source 50
    target 482
  ]
  edge [
    source 50
    target 3055
  ]
  edge [
    source 50
    target 80
  ]
  edge [
    source 50
    target 1398
  ]
  edge [
    source 50
    target 868
  ]
  edge [
    source 50
    target 3056
  ]
  edge [
    source 50
    target 3057
  ]
  edge [
    source 50
    target 3058
  ]
  edge [
    source 50
    target 528
  ]
  edge [
    source 50
    target 529
  ]
  edge [
    source 50
    target 530
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 3059
  ]
  edge [
    source 51
    target 3060
  ]
  edge [
    source 51
    target 3061
  ]
  edge [
    source 51
    target 3062
  ]
  edge [
    source 51
    target 3063
  ]
  edge [
    source 51
    target 3064
  ]
  edge [
    source 51
    target 3065
  ]
  edge [
    source 51
    target 3066
  ]
  edge [
    source 51
    target 2444
  ]
  edge [
    source 51
    target 3067
  ]
  edge [
    source 51
    target 411
  ]
  edge [
    source 51
    target 3068
  ]
  edge [
    source 51
    target 2518
  ]
  edge [
    source 51
    target 3069
  ]
  edge [
    source 51
    target 1418
  ]
  edge [
    source 51
    target 1419
  ]
  edge [
    source 51
    target 1420
  ]
  edge [
    source 51
    target 759
  ]
  edge [
    source 51
    target 1421
  ]
  edge [
    source 51
    target 1422
  ]
  edge [
    source 51
    target 761
  ]
  edge [
    source 51
    target 108
  ]
  edge [
    source 51
    target 1423
  ]
  edge [
    source 51
    target 250
  ]
  edge [
    source 51
    target 1424
  ]
  edge [
    source 51
    target 1425
  ]
  edge [
    source 51
    target 1426
  ]
  edge [
    source 51
    target 1427
  ]
  edge [
    source 51
    target 1428
  ]
  edge [
    source 51
    target 1429
  ]
  edge [
    source 51
    target 1430
  ]
  edge [
    source 51
    target 1431
  ]
  edge [
    source 51
    target 1432
  ]
  edge [
    source 51
    target 1433
  ]
  edge [
    source 51
    target 1434
  ]
  edge [
    source 51
    target 1435
  ]
  edge [
    source 51
    target 1436
  ]
  edge [
    source 51
    target 1437
  ]
  edge [
    source 51
    target 1438
  ]
  edge [
    source 51
    target 1439
  ]
  edge [
    source 51
    target 1440
  ]
  edge [
    source 51
    target 1441
  ]
  edge [
    source 51
    target 2452
  ]
  edge [
    source 51
    target 2453
  ]
  edge [
    source 51
    target 2454
  ]
  edge [
    source 51
    target 2455
  ]
  edge [
    source 51
    target 2456
  ]
  edge [
    source 51
    target 2457
  ]
  edge [
    source 51
    target 2458
  ]
  edge [
    source 51
    target 977
  ]
  edge [
    source 51
    target 980
  ]
  edge [
    source 51
    target 2459
  ]
  edge [
    source 51
    target 2460
  ]
  edge [
    source 51
    target 472
  ]
  edge [
    source 51
    target 2461
  ]
  edge [
    source 51
    target 2462
  ]
  edge [
    source 51
    target 208
  ]
  edge [
    source 51
    target 2463
  ]
  edge [
    source 51
    target 2464
  ]
  edge [
    source 51
    target 2465
  ]
  edge [
    source 51
    target 2466
  ]
  edge [
    source 51
    target 2467
  ]
  edge [
    source 51
    target 2468
  ]
  edge [
    source 51
    target 2469
  ]
  edge [
    source 51
    target 2470
  ]
  edge [
    source 51
    target 2471
  ]
  edge [
    source 51
    target 2472
  ]
  edge [
    source 51
    target 2473
  ]
  edge [
    source 51
    target 2474
  ]
  edge [
    source 51
    target 2475
  ]
  edge [
    source 51
    target 212
  ]
  edge [
    source 51
    target 2476
  ]
  edge [
    source 51
    target 2477
  ]
  edge [
    source 51
    target 2478
  ]
  edge [
    source 51
    target 1007
  ]
  edge [
    source 51
    target 2479
  ]
  edge [
    source 51
    target 2480
  ]
  edge [
    source 51
    target 2481
  ]
  edge [
    source 51
    target 2482
  ]
  edge [
    source 51
    target 2441
  ]
  edge [
    source 51
    target 2549
  ]
  edge [
    source 51
    target 2550
  ]
  edge [
    source 51
    target 2551
  ]
  edge [
    source 51
    target 2484
  ]
  edge [
    source 51
    target 3070
  ]
  edge [
    source 51
    target 3071
  ]
  edge [
    source 51
    target 3072
  ]
  edge [
    source 51
    target 3073
  ]
  edge [
    source 51
    target 3074
  ]
  edge [
    source 51
    target 1387
  ]
  edge [
    source 51
    target 3075
  ]
  edge [
    source 51
    target 2433
  ]
  edge [
    source 51
    target 3076
  ]
  edge [
    source 51
    target 3077
  ]
  edge [
    source 51
    target 3078
  ]
  edge [
    source 51
    target 177
  ]
  edge [
    source 51
    target 3079
  ]
  edge [
    source 51
    target 3080
  ]
  edge [
    source 51
    target 3081
  ]
  edge [
    source 51
    target 3082
  ]
  edge [
    source 51
    target 3083
  ]
  edge [
    source 51
    target 3084
  ]
  edge [
    source 51
    target 3085
  ]
  edge [
    source 51
    target 3086
  ]
  edge [
    source 51
    target 3087
  ]
  edge [
    source 51
    target 3088
  ]
  edge [
    source 51
    target 263
  ]
  edge [
    source 51
    target 3089
  ]
  edge [
    source 51
    target 3090
  ]
  edge [
    source 51
    target 3091
  ]
  edge [
    source 51
    target 3092
  ]
  edge [
    source 51
    target 3093
  ]
  edge [
    source 51
    target 3094
  ]
  edge [
    source 51
    target 3095
  ]
  edge [
    source 51
    target 3096
  ]
  edge [
    source 51
    target 3097
  ]
  edge [
    source 51
    target 3098
  ]
  edge [
    source 51
    target 3099
  ]
  edge [
    source 51
    target 3100
  ]
  edge [
    source 51
    target 3101
  ]
  edge [
    source 51
    target 3102
  ]
  edge [
    source 51
    target 3103
  ]
  edge [
    source 51
    target 204
  ]
  edge [
    source 51
    target 3104
  ]
  edge [
    source 51
    target 3105
  ]
  edge [
    source 51
    target 3106
  ]
  edge [
    source 51
    target 2972
  ]
  edge [
    source 51
    target 800
  ]
  edge [
    source 51
    target 3107
  ]
  edge [
    source 51
    target 3412
  ]
  edge [
    source 51
    target 3413
  ]
  edge [
    source 51
    target 3414
  ]
  edge [
    source 51
    target 3415
  ]
  edge [
    source 51
    target 3416
  ]
  edge [
    source 51
    target 3417
  ]
  edge [
    source 51
    target 3418
  ]
  edge [
    source 51
    target 410
  ]
  edge [
    source 51
    target 3419
  ]
  edge [
    source 51
    target 3420
  ]
  edge [
    source 51
    target 3421
  ]
  edge [
    source 51
    target 3422
  ]
  edge [
    source 51
    target 638
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 3108
  ]
  edge [
    source 52
    target 3109
  ]
  edge [
    source 52
    target 3110
  ]
  edge [
    source 52
    target 3111
  ]
  edge [
    source 52
    target 3112
  ]
  edge [
    source 52
    target 3113
  ]
  edge [
    source 53
    target 178
  ]
  edge [
    source 53
    target 179
  ]
  edge [
    source 53
    target 180
  ]
  edge [
    source 53
    target 181
  ]
  edge [
    source 53
    target 184
  ]
  edge [
    source 53
    target 159
  ]
  edge [
    source 53
    target 183
  ]
  edge [
    source 53
    target 182
  ]
  edge [
    source 53
    target 185
  ]
  edge [
    source 53
    target 186
  ]
  edge [
    source 53
    target 187
  ]
  edge [
    source 53
    target 188
  ]
  edge [
    source 53
    target 189
  ]
  edge [
    source 53
    target 190
  ]
  edge [
    source 53
    target 191
  ]
  edge [
    source 53
    target 192
  ]
  edge [
    source 53
    target 193
  ]
  edge [
    source 53
    target 438
  ]
  edge [
    source 53
    target 439
  ]
  edge [
    source 53
    target 440
  ]
  edge [
    source 53
    target 441
  ]
  edge [
    source 53
    target 442
  ]
  edge [
    source 53
    target 443
  ]
  edge [
    source 53
    target 444
  ]
  edge [
    source 53
    target 445
  ]
  edge [
    source 53
    target 446
  ]
  edge [
    source 53
    target 447
  ]
  edge [
    source 53
    target 448
  ]
  edge [
    source 53
    target 316
  ]
  edge [
    source 53
    target 449
  ]
  edge [
    source 53
    target 450
  ]
  edge [
    source 53
    target 451
  ]
  edge [
    source 53
    target 452
  ]
  edge [
    source 53
    target 453
  ]
  edge [
    source 53
    target 454
  ]
  edge [
    source 53
    target 455
  ]
  edge [
    source 53
    target 319
  ]
  edge [
    source 53
    target 93
  ]
  edge [
    source 53
    target 456
  ]
  edge [
    source 53
    target 457
  ]
  edge [
    source 53
    target 458
  ]
  edge [
    source 53
    target 459
  ]
  edge [
    source 53
    target 460
  ]
  edge [
    source 53
    target 317
  ]
  edge [
    source 53
    target 461
  ]
  edge [
    source 53
    target 320
  ]
  edge [
    source 53
    target 2220
  ]
  edge [
    source 53
    target 3114
  ]
  edge [
    source 53
    target 2296
  ]
  edge [
    source 53
    target 3115
  ]
  edge [
    source 53
    target 2006
  ]
  edge [
    source 53
    target 3116
  ]
  edge [
    source 53
    target 479
  ]
  edge [
    source 53
    target 3117
  ]
  edge [
    source 53
    target 3118
  ]
  edge [
    source 53
    target 3119
  ]
  edge [
    source 53
    target 1023
  ]
  edge [
    source 53
    target 3120
  ]
  edge [
    source 53
    target 233
  ]
  edge [
    source 53
    target 3121
  ]
  edge [
    source 53
    target 435
  ]
  edge [
    source 53
    target 1061
  ]
  edge [
    source 53
    target 263
  ]
  edge [
    source 53
    target 3122
  ]
  edge [
    source 53
    target 3123
  ]
  edge [
    source 53
    target 3124
  ]
  edge [
    source 53
    target 3125
  ]
  edge [
    source 53
    target 3126
  ]
  edge [
    source 53
    target 1089
  ]
  edge [
    source 53
    target 2595
  ]
  edge [
    source 53
    target 2596
  ]
  edge [
    source 53
    target 3127
  ]
  edge [
    source 53
    target 3128
  ]
  edge [
    source 53
    target 2559
  ]
  edge [
    source 53
    target 2597
  ]
  edge [
    source 53
    target 3129
  ]
  edge [
    source 53
    target 2599
  ]
  edge [
    source 53
    target 309
  ]
  edge [
    source 53
    target 3130
  ]
  edge [
    source 53
    target 781
  ]
  edge [
    source 53
    target 2600
  ]
  edge [
    source 53
    target 2950
  ]
  edge [
    source 53
    target 2603
  ]
  edge [
    source 53
    target 2604
  ]
  edge [
    source 53
    target 2562
  ]
  edge [
    source 53
    target 2605
  ]
  edge [
    source 53
    target 3131
  ]
  edge [
    source 53
    target 2606
  ]
  edge [
    source 53
    target 2607
  ]
  edge [
    source 53
    target 2639
  ]
  edge [
    source 53
    target 2575
  ]
  edge [
    source 53
    target 2572
  ]
  edge [
    source 53
    target 2610
  ]
  edge [
    source 53
    target 2611
  ]
  edge [
    source 53
    target 3132
  ]
  edge [
    source 53
    target 3133
  ]
  edge [
    source 53
    target 3134
  ]
  edge [
    source 53
    target 3135
  ]
  edge [
    source 53
    target 3136
  ]
  edge [
    source 53
    target 3137
  ]
  edge [
    source 53
    target 3138
  ]
  edge [
    source 53
    target 227
  ]
  edge [
    source 53
    target 3139
  ]
  edge [
    source 53
    target 3140
  ]
  edge [
    source 53
    target 3141
  ]
  edge [
    source 53
    target 3142
  ]
  edge [
    source 53
    target 3143
  ]
  edge [
    source 53
    target 3144
  ]
  edge [
    source 53
    target 3145
  ]
  edge [
    source 53
    target 3146
  ]
  edge [
    source 53
    target 3147
  ]
  edge [
    source 53
    target 3148
  ]
  edge [
    source 53
    target 1455
  ]
  edge [
    source 53
    target 3149
  ]
  edge [
    source 53
    target 3150
  ]
  edge [
    source 53
    target 305
  ]
  edge [
    source 53
    target 3151
  ]
  edge [
    source 53
    target 3152
  ]
  edge [
    source 53
    target 3153
  ]
  edge [
    source 53
    target 3154
  ]
  edge [
    source 53
    target 3155
  ]
  edge [
    source 53
    target 3156
  ]
  edge [
    source 53
    target 3157
  ]
  edge [
    source 53
    target 3158
  ]
  edge [
    source 53
    target 1437
  ]
  edge [
    source 53
    target 3159
  ]
  edge [
    source 53
    target 3160
  ]
  edge [
    source 53
    target 2475
  ]
  edge [
    source 53
    target 3161
  ]
  edge [
    source 53
    target 143
  ]
  edge [
    source 53
    target 880
  ]
  edge [
    source 53
    target 844
  ]
  edge [
    source 53
    target 2560
  ]
  edge [
    source 53
    target 3162
  ]
  edge [
    source 53
    target 154
  ]
  edge [
    source 53
    target 464
  ]
  edge [
    source 53
    target 3055
  ]
  edge [
    source 53
    target 3163
  ]
  edge [
    source 53
    target 1444
  ]
  edge [
    source 53
    target 3164
  ]
  edge [
    source 53
    target 2651
  ]
  edge [
    source 53
    target 3165
  ]
  edge [
    source 53
    target 3166
  ]
  edge [
    source 53
    target 223
  ]
  edge [
    source 53
    target 3167
  ]
  edge [
    source 53
    target 94
  ]
  edge [
    source 53
    target 3168
  ]
  edge [
    source 53
    target 81
  ]
  edge [
    source 53
    target 3169
  ]
  edge [
    source 53
    target 3170
  ]
  edge [
    source 53
    target 299
  ]
  edge [
    source 53
    target 3171
  ]
  edge [
    source 53
    target 3172
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 390
  ]
  edge [
    source 54
    target 3173
  ]
  edge [
    source 54
    target 3174
  ]
  edge [
    source 54
    target 3175
  ]
  edge [
    source 54
    target 3176
  ]
  edge [
    source 54
    target 3177
  ]
  edge [
    source 54
    target 439
  ]
  edge [
    source 54
    target 3178
  ]
  edge [
    source 54
    target 689
  ]
  edge [
    source 54
    target 3179
  ]
  edge [
    source 54
    target 3180
  ]
  edge [
    source 54
    target 3181
  ]
  edge [
    source 54
    target 3182
  ]
  edge [
    source 54
    target 3183
  ]
  edge [
    source 54
    target 3184
  ]
  edge [
    source 54
    target 3185
  ]
  edge [
    source 54
    target 3186
  ]
  edge [
    source 54
    target 3187
  ]
  edge [
    source 54
    target 3188
  ]
  edge [
    source 54
    target 3189
  ]
  edge [
    source 54
    target 2117
  ]
  edge [
    source 54
    target 2382
  ]
  edge [
    source 54
    target 595
  ]
  edge [
    source 54
    target 3190
  ]
  edge [
    source 54
    target 3191
  ]
  edge [
    source 54
    target 3192
  ]
  edge [
    source 54
    target 3193
  ]
  edge [
    source 54
    target 3194
  ]
  edge [
    source 54
    target 3195
  ]
  edge [
    source 54
    target 93
  ]
  edge [
    source 54
    target 3196
  ]
  edge [
    source 54
    target 395
  ]
  edge [
    source 54
    target 3197
  ]
  edge [
    source 54
    target 999
  ]
  edge [
    source 54
    target 171
  ]
  edge [
    source 54
    target 3198
  ]
  edge [
    source 54
    target 352
  ]
  edge [
    source 54
    target 925
  ]
  edge [
    source 54
    target 3199
  ]
  edge [
    source 54
    target 3200
  ]
  edge [
    source 54
    target 3201
  ]
  edge [
    source 54
    target 249
  ]
  edge [
    source 54
    target 320
  ]
  edge [
    source 54
    target 2376
  ]
  edge [
    source 54
    target 2829
  ]
  edge [
    source 54
    target 3202
  ]
  edge [
    source 54
    target 218
  ]
  edge [
    source 54
    target 3203
  ]
  edge [
    source 54
    target 3204
  ]
  edge [
    source 54
    target 3205
  ]
  edge [
    source 54
    target 219
  ]
  edge [
    source 54
    target 3206
  ]
  edge [
    source 54
    target 3207
  ]
  edge [
    source 54
    target 2127
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 3208
  ]
  edge [
    source 55
    target 2142
  ]
  edge [
    source 55
    target 3209
  ]
  edge [
    source 55
    target 114
  ]
  edge [
    source 55
    target 3210
  ]
  edge [
    source 55
    target 781
  ]
  edge [
    source 55
    target 3211
  ]
  edge [
    source 55
    target 60
  ]
  edge [
    source 55
    target 3212
  ]
  edge [
    source 55
    target 410
  ]
  edge [
    source 55
    target 3213
  ]
  edge [
    source 55
    target 3214
  ]
  edge [
    source 55
    target 180
  ]
  edge [
    source 55
    target 3215
  ]
  edge [
    source 55
    target 3216
  ]
  edge [
    source 55
    target 3217
  ]
  edge [
    source 55
    target 3218
  ]
  edge [
    source 55
    target 3219
  ]
  edge [
    source 55
    target 3220
  ]
  edge [
    source 55
    target 512
  ]
  edge [
    source 55
    target 3221
  ]
  edge [
    source 55
    target 3222
  ]
  edge [
    source 55
    target 1101
  ]
  edge [
    source 55
    target 435
  ]
  edge [
    source 55
    target 1422
  ]
  edge [
    source 55
    target 2666
  ]
  edge [
    source 55
    target 2667
  ]
  edge [
    source 55
    target 2141
  ]
  edge [
    source 55
    target 2668
  ]
  edge [
    source 55
    target 2669
  ]
  edge [
    source 55
    target 3024
  ]
  edge [
    source 55
    target 3223
  ]
  edge [
    source 55
    target 3224
  ]
  edge [
    source 55
    target 508
  ]
  edge [
    source 55
    target 1398
  ]
  edge [
    source 55
    target 3225
  ]
  edge [
    source 55
    target 3226
  ]
  edge [
    source 55
    target 3227
  ]
  edge [
    source 55
    target 3228
  ]
  edge [
    source 55
    target 3229
  ]
  edge [
    source 55
    target 3230
  ]
  edge [
    source 55
    target 3231
  ]
  edge [
    source 55
    target 3232
  ]
  edge [
    source 55
    target 1089
  ]
  edge [
    source 55
    target 108
  ]
  edge [
    source 55
    target 109
  ]
  edge [
    source 55
    target 110
  ]
  edge [
    source 55
    target 111
  ]
  edge [
    source 55
    target 112
  ]
  edge [
    source 55
    target 113
  ]
  edge [
    source 55
    target 115
  ]
  edge [
    source 55
    target 116
  ]
  edge [
    source 55
    target 117
  ]
  edge [
    source 55
    target 3233
  ]
  edge [
    source 55
    target 3234
  ]
  edge [
    source 55
    target 444
  ]
  edge [
    source 55
    target 3235
  ]
  edge [
    source 55
    target 3236
  ]
  edge [
    source 55
    target 177
  ]
  edge [
    source 55
    target 3237
  ]
  edge [
    source 55
    target 253
  ]
  edge [
    source 55
    target 3238
  ]
  edge [
    source 55
    target 3239
  ]
  edge [
    source 55
    target 3240
  ]
  edge [
    source 55
    target 3241
  ]
  edge [
    source 55
    target 3242
  ]
  edge [
    source 55
    target 3243
  ]
  edge [
    source 55
    target 3244
  ]
  edge [
    source 55
    target 965
  ]
  edge [
    source 55
    target 3245
  ]
  edge [
    source 55
    target 413
  ]
  edge [
    source 55
    target 87
  ]
  edge [
    source 55
    target 3246
  ]
  edge [
    source 55
    target 3247
  ]
  edge [
    source 55
    target 3248
  ]
  edge [
    source 55
    target 3249
  ]
  edge [
    source 55
    target 3250
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 3251
  ]
  edge [
    source 57
    target 3252
  ]
  edge [
    source 57
    target 3253
  ]
  edge [
    source 57
    target 3254
  ]
  edge [
    source 57
    target 299
  ]
  edge [
    source 57
    target 3255
  ]
  edge [
    source 57
    target 3256
  ]
  edge [
    source 57
    target 3257
  ]
  edge [
    source 57
    target 3258
  ]
  edge [
    source 57
    target 3259
  ]
  edge [
    source 57
    target 3260
  ]
  edge [
    source 57
    target 606
  ]
  edge [
    source 57
    target 110
  ]
  edge [
    source 57
    target 3261
  ]
  edge [
    source 57
    target 3262
  ]
  edge [
    source 57
    target 93
  ]
  edge [
    source 57
    target 3263
  ]
  edge [
    source 57
    target 2376
  ]
  edge [
    source 57
    target 3264
  ]
  edge [
    source 57
    target 670
  ]
  edge [
    source 57
    target 3265
  ]
  edge [
    source 57
    target 3266
  ]
  edge [
    source 57
    target 3267
  ]
  edge [
    source 57
    target 3268
  ]
  edge [
    source 57
    target 3269
  ]
  edge [
    source 57
    target 3270
  ]
  edge [
    source 57
    target 2578
  ]
  edge [
    source 57
    target 3271
  ]
  edge [
    source 57
    target 3272
  ]
  edge [
    source 57
    target 3273
  ]
  edge [
    source 57
    target 3274
  ]
  edge [
    source 57
    target 3275
  ]
  edge [
    source 57
    target 3276
  ]
  edge [
    source 57
    target 3277
  ]
  edge [
    source 57
    target 3278
  ]
  edge [
    source 57
    target 3279
  ]
  edge [
    source 57
    target 3280
  ]
  edge [
    source 57
    target 3281
  ]
  edge [
    source 57
    target 3282
  ]
  edge [
    source 57
    target 3054
  ]
  edge [
    source 57
    target 3283
  ]
  edge [
    source 57
    target 3284
  ]
  edge [
    source 57
    target 202
  ]
  edge [
    source 57
    target 3285
  ]
  edge [
    source 57
    target 207
  ]
  edge [
    source 57
    target 3286
  ]
  edge [
    source 57
    target 3287
  ]
  edge [
    source 57
    target 3288
  ]
  edge [
    source 57
    target 3289
  ]
  edge [
    source 57
    target 3290
  ]
  edge [
    source 57
    target 3291
  ]
  edge [
    source 57
    target 3292
  ]
  edge [
    source 57
    target 3293
  ]
  edge [
    source 57
    target 3294
  ]
  edge [
    source 57
    target 3295
  ]
  edge [
    source 57
    target 1398
  ]
  edge [
    source 57
    target 3296
  ]
  edge [
    source 57
    target 233
  ]
  edge [
    source 57
    target 3297
  ]
  edge [
    source 57
    target 3298
  ]
  edge [
    source 57
    target 3299
  ]
  edge [
    source 57
    target 3300
  ]
  edge [
    source 57
    target 528
  ]
  edge [
    source 57
    target 529
  ]
  edge [
    source 57
    target 530
  ]
  edge [
    source 57
    target 445
  ]
  edge [
    source 57
    target 3301
  ]
  edge [
    source 57
    target 3302
  ]
  edge [
    source 57
    target 3303
  ]
  edge [
    source 57
    target 3304
  ]
  edge [
    source 57
    target 3305
  ]
  edge [
    source 57
    target 1155
  ]
  edge [
    source 57
    target 1093
  ]
  edge [
    source 57
    target 3306
  ]
  edge [
    source 57
    target 3307
  ]
  edge [
    source 57
    target 3308
  ]
  edge [
    source 57
    target 3309
  ]
  edge [
    source 57
    target 3310
  ]
  edge [
    source 57
    target 381
  ]
  edge [
    source 57
    target 3311
  ]
  edge [
    source 57
    target 647
  ]
  edge [
    source 57
    target 338
  ]
  edge [
    source 57
    target 3312
  ]
  edge [
    source 57
    target 3313
  ]
  edge [
    source 57
    target 3314
  ]
  edge [
    source 57
    target 860
  ]
  edge [
    source 57
    target 3315
  ]
  edge [
    source 57
    target 3316
  ]
  edge [
    source 57
    target 3317
  ]
  edge [
    source 57
    target 3318
  ]
  edge [
    source 57
    target 3319
  ]
  edge [
    source 57
    target 3320
  ]
  edge [
    source 57
    target 3321
  ]
  edge [
    source 57
    target 3322
  ]
  edge [
    source 57
    target 3323
  ]
  edge [
    source 57
    target 3324
  ]
  edge [
    source 57
    target 3325
  ]
  edge [
    source 57
    target 3326
  ]
  edge [
    source 57
    target 3327
  ]
  edge [
    source 57
    target 3328
  ]
  edge [
    source 57
    target 3329
  ]
  edge [
    source 57
    target 3330
  ]
  edge [
    source 57
    target 3331
  ]
  edge [
    source 57
    target 3332
  ]
  edge [
    source 57
    target 109
  ]
  edge [
    source 57
    target 3333
  ]
  edge [
    source 57
    target 3334
  ]
  edge [
    source 57
    target 3043
  ]
  edge [
    source 57
    target 3335
  ]
  edge [
    source 57
    target 3336
  ]
  edge [
    source 57
    target 116
  ]
  edge [
    source 57
    target 3337
  ]
  edge [
    source 57
    target 108
  ]
  edge [
    source 57
    target 111
  ]
  edge [
    source 57
    target 3338
  ]
  edge [
    source 57
    target 113
  ]
  edge [
    source 57
    target 112
  ]
  edge [
    source 57
    target 3339
  ]
  edge [
    source 57
    target 3340
  ]
  edge [
    source 57
    target 115
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 57
    target 114
  ]
  edge [
    source 57
    target 3341
  ]
  edge [
    source 57
    target 3342
  ]
  edge [
    source 57
    target 3343
  ]
  edge [
    source 57
    target 223
  ]
  edge [
    source 57
    target 117
  ]
  edge [
    source 57
    target 3344
  ]
  edge [
    source 57
    target 2447
  ]
  edge [
    source 57
    target 3345
  ]
  edge [
    source 57
    target 3346
  ]
  edge [
    source 57
    target 3347
  ]
  edge [
    source 57
    target 208
  ]
  edge [
    source 57
    target 3348
  ]
  edge [
    source 57
    target 761
  ]
  edge [
    source 57
    target 3349
  ]
  edge [
    source 57
    target 3350
  ]
  edge [
    source 57
    target 3351
  ]
  edge [
    source 57
    target 759
  ]
  edge [
    source 57
    target 3352
  ]
  edge [
    source 57
    target 3353
  ]
  edge [
    source 57
    target 3354
  ]
  edge [
    source 57
    target 3355
  ]
  edge [
    source 57
    target 3356
  ]
  edge [
    source 57
    target 3357
  ]
  edge [
    source 57
    target 3358
  ]
  edge [
    source 57
    target 3359
  ]
  edge [
    source 57
    target 309
  ]
  edge [
    source 57
    target 3360
  ]
  edge [
    source 57
    target 3361
  ]
  edge [
    source 57
    target 3362
  ]
  edge [
    source 57
    target 3363
  ]
  edge [
    source 57
    target 253
  ]
  edge [
    source 57
    target 3364
  ]
  edge [
    source 57
    target 343
  ]
  edge [
    source 57
    target 3180
  ]
  edge [
    source 57
    target 3365
  ]
  edge [
    source 57
    target 3366
  ]
  edge [
    source 57
    target 3367
  ]
  edge [
    source 57
    target 3368
  ]
  edge [
    source 57
    target 303
  ]
  edge [
    source 57
    target 3369
  ]
  edge [
    source 57
    target 3370
  ]
  edge [
    source 57
    target 3371
  ]
  edge [
    source 57
    target 294
  ]
  edge [
    source 57
    target 3372
  ]
  edge [
    source 57
    target 3373
  ]
  edge [
    source 57
    target 689
  ]
  edge [
    source 57
    target 3374
  ]
  edge [
    source 57
    target 3375
  ]
  edge [
    source 57
    target 3376
  ]
  edge [
    source 57
    target 3377
  ]
  edge [
    source 57
    target 3378
  ]
  edge [
    source 57
    target 3379
  ]
  edge [
    source 57
    target 359
  ]
  edge [
    source 57
    target 3380
  ]
  edge [
    source 57
    target 3381
  ]
  edge [
    source 57
    target 3382
  ]
  edge [
    source 57
    target 320
  ]
  edge [
    source 57
    target 3383
  ]
  edge [
    source 57
    target 3384
  ]
  edge [
    source 57
    target 3385
  ]
  edge [
    source 57
    target 358
  ]
  edge [
    source 57
    target 3386
  ]
  edge [
    source 57
    target 3387
  ]
  edge [
    source 57
    target 2949
  ]
  edge [
    source 57
    target 3388
  ]
  edge [
    source 57
    target 3389
  ]
  edge [
    source 57
    target 2987
  ]
  edge [
    source 57
    target 3390
  ]
  edge [
    source 57
    target 3391
  ]
  edge [
    source 57
    target 2938
  ]
  edge [
    source 57
    target 3392
  ]
  edge [
    source 57
    target 667
  ]
  edge [
    source 57
    target 3393
  ]
  edge [
    source 57
    target 3394
  ]
  edge [
    source 57
    target 3395
  ]
  edge [
    source 57
    target 3396
  ]
  edge [
    source 57
    target 3397
  ]
  edge [
    source 57
    target 3398
  ]
  edge [
    source 57
    target 373
  ]
  edge [
    source 57
    target 3399
  ]
  edge [
    source 57
    target 3400
  ]
  edge [
    source 57
    target 654
  ]
  edge [
    source 57
    target 3161
  ]
  edge [
    source 57
    target 924
  ]
  edge [
    source 57
    target 143
  ]
  edge [
    source 57
    target 3401
  ]
  edge [
    source 57
    target 3402
  ]
  edge [
    source 57
    target 2582
  ]
  edge [
    source 57
    target 3403
  ]
  edge [
    source 57
    target 3404
  ]
  edge [
    source 57
    target 3405
  ]
  edge [
    source 57
    target 2592
  ]
  edge [
    source 57
    target 3406
  ]
  edge [
    source 57
    target 3407
  ]
  edge [
    source 57
    target 3408
  ]
  edge [
    source 57
    target 535
  ]
  edge [
    source 57
    target 3409
  ]
  edge [
    source 57
    target 2280
  ]
  edge [
    source 57
    target 3410
  ]
  edge [
    source 57
    target 3411
  ]
  edge [
    source 410
    target 3412
  ]
  edge [
    source 410
    target 3413
  ]
  edge [
    source 410
    target 3414
  ]
  edge [
    source 410
    target 3415
  ]
  edge [
    source 410
    target 3416
  ]
  edge [
    source 410
    target 3417
  ]
  edge [
    source 410
    target 3418
  ]
  edge [
    source 410
    target 3419
  ]
  edge [
    source 410
    target 3420
  ]
  edge [
    source 410
    target 3421
  ]
  edge [
    source 410
    target 3422
  ]
  edge [
    source 410
    target 638
  ]
  edge [
    source 638
    target 3412
  ]
  edge [
    source 638
    target 3413
  ]
  edge [
    source 638
    target 3414
  ]
  edge [
    source 638
    target 3415
  ]
  edge [
    source 638
    target 3416
  ]
  edge [
    source 638
    target 3417
  ]
  edge [
    source 638
    target 3418
  ]
  edge [
    source 638
    target 3419
  ]
  edge [
    source 638
    target 3420
  ]
  edge [
    source 638
    target 3421
  ]
  edge [
    source 638
    target 3422
  ]
  edge [
    source 3412
    target 3413
  ]
  edge [
    source 3412
    target 3414
  ]
  edge [
    source 3412
    target 3415
  ]
  edge [
    source 3412
    target 3416
  ]
  edge [
    source 3412
    target 3417
  ]
  edge [
    source 3412
    target 3418
  ]
  edge [
    source 3412
    target 3419
  ]
  edge [
    source 3412
    target 3420
  ]
  edge [
    source 3412
    target 3412
  ]
  edge [
    source 3412
    target 3421
  ]
  edge [
    source 3412
    target 3422
  ]
  edge [
    source 3413
    target 3414
  ]
  edge [
    source 3413
    target 3415
  ]
  edge [
    source 3413
    target 3416
  ]
  edge [
    source 3413
    target 3417
  ]
  edge [
    source 3413
    target 3418
  ]
  edge [
    source 3413
    target 3419
  ]
  edge [
    source 3413
    target 3420
  ]
  edge [
    source 3413
    target 3421
  ]
  edge [
    source 3413
    target 3422
  ]
  edge [
    source 3414
    target 3415
  ]
  edge [
    source 3414
    target 3416
  ]
  edge [
    source 3414
    target 3417
  ]
  edge [
    source 3414
    target 3418
  ]
  edge [
    source 3414
    target 3419
  ]
  edge [
    source 3414
    target 3420
  ]
  edge [
    source 3414
    target 3421
  ]
  edge [
    source 3414
    target 3422
  ]
  edge [
    source 3415
    target 3416
  ]
  edge [
    source 3415
    target 3417
  ]
  edge [
    source 3415
    target 3418
  ]
  edge [
    source 3415
    target 3419
  ]
  edge [
    source 3415
    target 3420
  ]
  edge [
    source 3415
    target 3421
  ]
  edge [
    source 3415
    target 3422
  ]
  edge [
    source 3416
    target 3417
  ]
  edge [
    source 3416
    target 3418
  ]
  edge [
    source 3416
    target 3419
  ]
  edge [
    source 3416
    target 3420
  ]
  edge [
    source 3416
    target 3421
  ]
  edge [
    source 3416
    target 3422
  ]
  edge [
    source 3417
    target 3418
  ]
  edge [
    source 3417
    target 3419
  ]
  edge [
    source 3417
    target 3420
  ]
  edge [
    source 3417
    target 3421
  ]
  edge [
    source 3417
    target 3422
  ]
  edge [
    source 3418
    target 3419
  ]
  edge [
    source 3418
    target 3418
  ]
  edge [
    source 3418
    target 3420
  ]
  edge [
    source 3418
    target 3421
  ]
  edge [
    source 3418
    target 3422
  ]
  edge [
    source 3419
    target 3420
  ]
  edge [
    source 3419
    target 3421
  ]
  edge [
    source 3419
    target 3422
  ]
  edge [
    source 3420
    target 3421
  ]
  edge [
    source 3420
    target 3422
  ]
  edge [
    source 3421
    target 3422
  ]
]
