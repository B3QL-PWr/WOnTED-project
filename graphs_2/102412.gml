graph [
  node [
    id 0
    label "festiwal"
    origin "text"
  ]
  node [
    id 1
    label "edukacja"
    origin "text"
  ]
  node [
    id 2
    label "nieformalny"
    origin "text"
  ]
  node [
    id 3
    label "bia&#322;oru&#347;"
    origin "text"
  ]
  node [
    id 4
    label "Przystanek_Woodstock"
  ]
  node [
    id 5
    label "Woodstock"
  ]
  node [
    id 6
    label "Opole"
  ]
  node [
    id 7
    label "Eurowizja"
  ]
  node [
    id 8
    label "Open'er"
  ]
  node [
    id 9
    label "Metalmania"
  ]
  node [
    id 10
    label "impreza"
  ]
  node [
    id 11
    label "Brutal"
  ]
  node [
    id 12
    label "FAMA"
  ]
  node [
    id 13
    label "Interwizja"
  ]
  node [
    id 14
    label "Nowe_Horyzonty"
  ]
  node [
    id 15
    label "impra"
  ]
  node [
    id 16
    label "rozrywka"
  ]
  node [
    id 17
    label "przyj&#281;cie"
  ]
  node [
    id 18
    label "okazja"
  ]
  node [
    id 19
    label "party"
  ]
  node [
    id 20
    label "&#346;wierkle"
  ]
  node [
    id 21
    label "hipster"
  ]
  node [
    id 22
    label "formation"
  ]
  node [
    id 23
    label "Karta_Nauczyciela"
  ]
  node [
    id 24
    label "wiedza"
  ]
  node [
    id 25
    label "heureza"
  ]
  node [
    id 26
    label "proces"
  ]
  node [
    id 27
    label "miasteczko_rowerowe"
  ]
  node [
    id 28
    label "urszulanki"
  ]
  node [
    id 29
    label "niepokalanki"
  ]
  node [
    id 30
    label "kwalifikacje"
  ]
  node [
    id 31
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 32
    label "&#322;awa_szkolna"
  ]
  node [
    id 33
    label "nauka"
  ]
  node [
    id 34
    label "skolaryzacja"
  ]
  node [
    id 35
    label "program_nauczania"
  ]
  node [
    id 36
    label "szkolnictwo"
  ]
  node [
    id 37
    label "form"
  ]
  node [
    id 38
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 39
    label "gospodarka"
  ]
  node [
    id 40
    label "porada"
  ]
  node [
    id 41
    label "fotowoltaika"
  ]
  node [
    id 42
    label "przem&#243;wienie"
  ]
  node [
    id 43
    label "nauki_o_poznaniu"
  ]
  node [
    id 44
    label "nomotetyczny"
  ]
  node [
    id 45
    label "systematyka"
  ]
  node [
    id 46
    label "typologia"
  ]
  node [
    id 47
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 48
    label "kultura_duchowa"
  ]
  node [
    id 49
    label "nauki_penalne"
  ]
  node [
    id 50
    label "dziedzina"
  ]
  node [
    id 51
    label "imagineskopia"
  ]
  node [
    id 52
    label "teoria_naukowa"
  ]
  node [
    id 53
    label "inwentyka"
  ]
  node [
    id 54
    label "metodologia"
  ]
  node [
    id 55
    label "nauki_o_Ziemi"
  ]
  node [
    id 56
    label "kognicja"
  ]
  node [
    id 57
    label "przebieg"
  ]
  node [
    id 58
    label "rozprawa"
  ]
  node [
    id 59
    label "wydarzenie"
  ]
  node [
    id 60
    label "legislacyjnie"
  ]
  node [
    id 61
    label "przes&#322;anka"
  ]
  node [
    id 62
    label "zjawisko"
  ]
  node [
    id 63
    label "nast&#281;pstwo"
  ]
  node [
    id 64
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 65
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 66
    label "eliminacje"
  ]
  node [
    id 67
    label "metoda"
  ]
  node [
    id 68
    label "nauczanie"
  ]
  node [
    id 69
    label "inwentarz"
  ]
  node [
    id 70
    label "rynek"
  ]
  node [
    id 71
    label "mieszkalnictwo"
  ]
  node [
    id 72
    label "agregat_ekonomiczny"
  ]
  node [
    id 73
    label "miejsce_pracy"
  ]
  node [
    id 74
    label "farmaceutyka"
  ]
  node [
    id 75
    label "produkowanie"
  ]
  node [
    id 76
    label "rolnictwo"
  ]
  node [
    id 77
    label "transport"
  ]
  node [
    id 78
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 79
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 80
    label "obronno&#347;&#263;"
  ]
  node [
    id 81
    label "sektor_prywatny"
  ]
  node [
    id 82
    label "sch&#322;adza&#263;"
  ]
  node [
    id 83
    label "czerwona_strefa"
  ]
  node [
    id 84
    label "struktura"
  ]
  node [
    id 85
    label "pole"
  ]
  node [
    id 86
    label "sektor_publiczny"
  ]
  node [
    id 87
    label "bankowo&#347;&#263;"
  ]
  node [
    id 88
    label "gospodarowanie"
  ]
  node [
    id 89
    label "obora"
  ]
  node [
    id 90
    label "gospodarka_wodna"
  ]
  node [
    id 91
    label "gospodarka_le&#347;na"
  ]
  node [
    id 92
    label "gospodarowa&#263;"
  ]
  node [
    id 93
    label "fabryka"
  ]
  node [
    id 94
    label "wytw&#243;rnia"
  ]
  node [
    id 95
    label "stodo&#322;a"
  ]
  node [
    id 96
    label "przemys&#322;"
  ]
  node [
    id 97
    label "spichlerz"
  ]
  node [
    id 98
    label "sch&#322;adzanie"
  ]
  node [
    id 99
    label "administracja"
  ]
  node [
    id 100
    label "sch&#322;odzenie"
  ]
  node [
    id 101
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 102
    label "zasada"
  ]
  node [
    id 103
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 104
    label "regulacja_cen"
  ]
  node [
    id 105
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 106
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 107
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 108
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 109
    label "wykszta&#322;cenie"
  ]
  node [
    id 110
    label "urszulanki_szare"
  ]
  node [
    id 111
    label "proporcja"
  ]
  node [
    id 112
    label "cognition"
  ]
  node [
    id 113
    label "intelekt"
  ]
  node [
    id 114
    label "pozwolenie"
  ]
  node [
    id 115
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 116
    label "zaawansowanie"
  ]
  node [
    id 117
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 118
    label "nieoficjalny"
  ]
  node [
    id 119
    label "nieformalnie"
  ]
  node [
    id 120
    label "nieoficjalnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
]
