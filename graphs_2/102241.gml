graph [
  node [
    id 0
    label "krzysztof"
    origin "text"
  ]
  node [
    id 1
    label "kolasi&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "sprawozdawca"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wiadek"
  ]
  node [
    id 4
    label "przekaziciel"
  ]
  node [
    id 5
    label "dziennikarz"
  ]
  node [
    id 6
    label "po&#347;rednik"
  ]
  node [
    id 7
    label "informator"
  ]
  node [
    id 8
    label "wys&#322;annik"
  ]
  node [
    id 9
    label "s&#261;d"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "uczestnik"
  ]
  node [
    id 12
    label "dru&#380;ba"
  ]
  node [
    id 13
    label "obserwator"
  ]
  node [
    id 14
    label "osoba_fizyczna"
  ]
  node [
    id 15
    label "publicysta"
  ]
  node [
    id 16
    label "nowiniarz"
  ]
  node [
    id 17
    label "bran&#380;owiec"
  ]
  node [
    id 18
    label "akredytowanie"
  ]
  node [
    id 19
    label "akredytowa&#263;"
  ]
  node [
    id 20
    label "Krzysztofa"
  ]
  node [
    id 21
    label "Kolasi&#324;ski"
  ]
  node [
    id 22
    label "Janusz"
  ]
  node [
    id 23
    label "Niemcewicz"
  ]
  node [
    id 24
    label "Jadwiga"
  ]
  node [
    id 25
    label "Sk&#243;rzewska"
  ]
  node [
    id 26
    label "&#321;osiak"
  ]
  node [
    id 27
    label "Jerzy"
  ]
  node [
    id 28
    label "St&#281;pie&#324;"
  ]
  node [
    id 29
    label "Gra&#380;yna"
  ]
  node [
    id 30
    label "Sza&#322;ygo"
  ]
  node [
    id 31
    label "rzecznik"
  ]
  node [
    id 32
    label "prawy"
  ]
  node [
    id 33
    label "obywatelski"
  ]
  node [
    id 34
    label "prokurator"
  ]
  node [
    id 35
    label "generalny"
  ]
  node [
    id 36
    label "rzeczpospolita"
  ]
  node [
    id 37
    label "polski"
  ]
  node [
    id 38
    label "ustawa"
  ]
  node [
    id 39
    label "zeszyt"
  ]
  node [
    id 40
    label "dzie&#324;"
  ]
  node [
    id 41
    label "28"
  ]
  node [
    id 42
    label "lipiec"
  ]
  node [
    id 43
    label "1990"
  ]
  node [
    id 44
    label "rok"
  ]
  node [
    id 45
    label "ojciec"
  ]
  node [
    id 46
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 47
    label "ubezpieczeniowy"
  ]
  node [
    id 48
    label "dziennik"
  ]
  node [
    id 49
    label "u"
  ]
  node [
    id 50
    label "fundusz"
  ]
  node [
    id 51
    label "gwarancyjny"
  ]
  node [
    id 52
    label "konstytucja"
  ]
  node [
    id 53
    label "RP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 52
  ]
  edge [
    source 37
    target 52
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 38
    target 42
  ]
  edge [
    source 38
    target 43
  ]
  edge [
    source 38
    target 44
  ]
  edge [
    source 38
    target 45
  ]
  edge [
    source 38
    target 46
  ]
  edge [
    source 38
    target 47
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 43
  ]
  edge [
    source 39
    target 44
  ]
  edge [
    source 39
    target 45
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 44
  ]
  edge [
    source 40
    target 45
  ]
  edge [
    source 40
    target 46
  ]
  edge [
    source 40
    target 47
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
]
