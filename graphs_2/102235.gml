graph [
  node [
    id 0
    label "zakres"
    origin "text"
  ]
  node [
    id 1
    label "zatrudnia&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pracownik"
    origin "text"
  ]
  node [
    id 3
    label "cela"
    origin "text"
  ]
  node [
    id 4
    label "skierowanie"
    origin "text"
  ]
  node [
    id 5
    label "pracodawca"
    origin "text"
  ]
  node [
    id 6
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 7
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 8
    label "by&#263;"
    origin "text"
  ]
  node [
    id 9
    label "lub"
    origin "text"
  ]
  node [
    id 10
    label "podmiot"
    origin "text"
  ]
  node [
    id 11
    label "rozumienie"
    origin "text"
  ]
  node [
    id 12
    label "kodeks"
    origin "text"
  ]
  node [
    id 13
    label "praca"
    origin "text"
  ]
  node [
    id 14
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 15
    label "agencja"
    origin "text"
  ]
  node [
    id 16
    label "tymczasowy"
    origin "text"
  ]
  node [
    id 17
    label "wyznacza&#263;"
    origin "text"
  ]
  node [
    id 18
    label "zadanie"
    origin "text"
  ]
  node [
    id 19
    label "wykonanie"
    origin "text"
  ]
  node [
    id 20
    label "granica"
  ]
  node [
    id 21
    label "zbi&#243;r"
  ]
  node [
    id 22
    label "sfera"
  ]
  node [
    id 23
    label "wielko&#347;&#263;"
  ]
  node [
    id 24
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 25
    label "podzakres"
  ]
  node [
    id 26
    label "dziedzina"
  ]
  node [
    id 27
    label "desygnat"
  ]
  node [
    id 28
    label "circle"
  ]
  node [
    id 29
    label "rozmiar"
  ]
  node [
    id 30
    label "izochronizm"
  ]
  node [
    id 31
    label "zasi&#261;g"
  ]
  node [
    id 32
    label "bridge"
  ]
  node [
    id 33
    label "distribution"
  ]
  node [
    id 34
    label "warunek_lokalowy"
  ]
  node [
    id 35
    label "liczba"
  ]
  node [
    id 36
    label "cecha"
  ]
  node [
    id 37
    label "rzadko&#347;&#263;"
  ]
  node [
    id 38
    label "zaleta"
  ]
  node [
    id 39
    label "ilo&#347;&#263;"
  ]
  node [
    id 40
    label "measure"
  ]
  node [
    id 41
    label "znaczenie"
  ]
  node [
    id 42
    label "opinia"
  ]
  node [
    id 43
    label "dymensja"
  ]
  node [
    id 44
    label "poj&#281;cie"
  ]
  node [
    id 45
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 46
    label "zdolno&#347;&#263;"
  ]
  node [
    id 47
    label "potencja"
  ]
  node [
    id 48
    label "property"
  ]
  node [
    id 49
    label "egzemplarz"
  ]
  node [
    id 50
    label "series"
  ]
  node [
    id 51
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 52
    label "uprawianie"
  ]
  node [
    id 53
    label "praca_rolnicza"
  ]
  node [
    id 54
    label "collection"
  ]
  node [
    id 55
    label "dane"
  ]
  node [
    id 56
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 57
    label "pakiet_klimatyczny"
  ]
  node [
    id 58
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 59
    label "sum"
  ]
  node [
    id 60
    label "gathering"
  ]
  node [
    id 61
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 62
    label "album"
  ]
  node [
    id 63
    label "przej&#347;cie"
  ]
  node [
    id 64
    label "kres"
  ]
  node [
    id 65
    label "granica_pa&#324;stwa"
  ]
  node [
    id 66
    label "Ural"
  ]
  node [
    id 67
    label "miara"
  ]
  node [
    id 68
    label "end"
  ]
  node [
    id 69
    label "pu&#322;ap"
  ]
  node [
    id 70
    label "koniec"
  ]
  node [
    id 71
    label "granice"
  ]
  node [
    id 72
    label "frontier"
  ]
  node [
    id 73
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 74
    label "funkcja"
  ]
  node [
    id 75
    label "bezdro&#380;e"
  ]
  node [
    id 76
    label "poddzia&#322;"
  ]
  node [
    id 77
    label "wymiar"
  ]
  node [
    id 78
    label "strefa"
  ]
  node [
    id 79
    label "kula"
  ]
  node [
    id 80
    label "class"
  ]
  node [
    id 81
    label "sector"
  ]
  node [
    id 82
    label "przestrze&#324;"
  ]
  node [
    id 83
    label "p&#243;&#322;kula"
  ]
  node [
    id 84
    label "huczek"
  ]
  node [
    id 85
    label "p&#243;&#322;sfera"
  ]
  node [
    id 86
    label "powierzchnia"
  ]
  node [
    id 87
    label "kolur"
  ]
  node [
    id 88
    label "grupa"
  ]
  node [
    id 89
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 90
    label "odpowiednik"
  ]
  node [
    id 91
    label "designatum"
  ]
  node [
    id 92
    label "nazwa_rzetelna"
  ]
  node [
    id 93
    label "nazwa_pozorna"
  ]
  node [
    id 94
    label "denotacja"
  ]
  node [
    id 95
    label "bra&#263;"
  ]
  node [
    id 96
    label "zatrudni&#263;"
  ]
  node [
    id 97
    label "zatrudnianie"
  ]
  node [
    id 98
    label "undertake"
  ]
  node [
    id 99
    label "robi&#263;"
  ]
  node [
    id 100
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 101
    label "porywa&#263;"
  ]
  node [
    id 102
    label "korzysta&#263;"
  ]
  node [
    id 103
    label "take"
  ]
  node [
    id 104
    label "wchodzi&#263;"
  ]
  node [
    id 105
    label "poczytywa&#263;"
  ]
  node [
    id 106
    label "levy"
  ]
  node [
    id 107
    label "wk&#322;ada&#263;"
  ]
  node [
    id 108
    label "raise"
  ]
  node [
    id 109
    label "pokonywa&#263;"
  ]
  node [
    id 110
    label "przyjmowa&#263;"
  ]
  node [
    id 111
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 112
    label "rucha&#263;"
  ]
  node [
    id 113
    label "prowadzi&#263;"
  ]
  node [
    id 114
    label "za&#380;ywa&#263;"
  ]
  node [
    id 115
    label "get"
  ]
  node [
    id 116
    label "otrzymywa&#263;"
  ]
  node [
    id 117
    label "&#263;pa&#263;"
  ]
  node [
    id 118
    label "interpretowa&#263;"
  ]
  node [
    id 119
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 120
    label "dostawa&#263;"
  ]
  node [
    id 121
    label "rusza&#263;"
  ]
  node [
    id 122
    label "chwyta&#263;"
  ]
  node [
    id 123
    label "grza&#263;"
  ]
  node [
    id 124
    label "wch&#322;ania&#263;"
  ]
  node [
    id 125
    label "wygrywa&#263;"
  ]
  node [
    id 126
    label "u&#380;ywa&#263;"
  ]
  node [
    id 127
    label "ucieka&#263;"
  ]
  node [
    id 128
    label "arise"
  ]
  node [
    id 129
    label "uprawia&#263;_seks"
  ]
  node [
    id 130
    label "abstract"
  ]
  node [
    id 131
    label "towarzystwo"
  ]
  node [
    id 132
    label "atakowa&#263;"
  ]
  node [
    id 133
    label "branie"
  ]
  node [
    id 134
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 135
    label "zalicza&#263;"
  ]
  node [
    id 136
    label "open"
  ]
  node [
    id 137
    label "wzi&#261;&#263;"
  ]
  node [
    id 138
    label "&#322;apa&#263;"
  ]
  node [
    id 139
    label "przewa&#380;a&#263;"
  ]
  node [
    id 140
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 141
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 142
    label "zatrudnienie"
  ]
  node [
    id 143
    label "engagement"
  ]
  node [
    id 144
    label "salariat"
  ]
  node [
    id 145
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 146
    label "cz&#322;owiek"
  ]
  node [
    id 147
    label "delegowanie"
  ]
  node [
    id 148
    label "pracu&#347;"
  ]
  node [
    id 149
    label "r&#281;ka"
  ]
  node [
    id 150
    label "delegowa&#263;"
  ]
  node [
    id 151
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 152
    label "warstwa"
  ]
  node [
    id 153
    label "p&#322;aca"
  ]
  node [
    id 154
    label "ludzko&#347;&#263;"
  ]
  node [
    id 155
    label "asymilowanie"
  ]
  node [
    id 156
    label "wapniak"
  ]
  node [
    id 157
    label "asymilowa&#263;"
  ]
  node [
    id 158
    label "os&#322;abia&#263;"
  ]
  node [
    id 159
    label "posta&#263;"
  ]
  node [
    id 160
    label "hominid"
  ]
  node [
    id 161
    label "podw&#322;adny"
  ]
  node [
    id 162
    label "os&#322;abianie"
  ]
  node [
    id 163
    label "g&#322;owa"
  ]
  node [
    id 164
    label "figura"
  ]
  node [
    id 165
    label "portrecista"
  ]
  node [
    id 166
    label "dwun&#243;g"
  ]
  node [
    id 167
    label "profanum"
  ]
  node [
    id 168
    label "mikrokosmos"
  ]
  node [
    id 169
    label "nasada"
  ]
  node [
    id 170
    label "duch"
  ]
  node [
    id 171
    label "antropochoria"
  ]
  node [
    id 172
    label "osoba"
  ]
  node [
    id 173
    label "wz&#243;r"
  ]
  node [
    id 174
    label "senior"
  ]
  node [
    id 175
    label "oddzia&#322;ywanie"
  ]
  node [
    id 176
    label "Adam"
  ]
  node [
    id 177
    label "homo_sapiens"
  ]
  node [
    id 178
    label "polifag"
  ]
  node [
    id 179
    label "krzy&#380;"
  ]
  node [
    id 180
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 181
    label "handwriting"
  ]
  node [
    id 182
    label "d&#322;o&#324;"
  ]
  node [
    id 183
    label "gestykulowa&#263;"
  ]
  node [
    id 184
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 185
    label "palec"
  ]
  node [
    id 186
    label "przedrami&#281;"
  ]
  node [
    id 187
    label "hand"
  ]
  node [
    id 188
    label "&#322;okie&#263;"
  ]
  node [
    id 189
    label "hazena"
  ]
  node [
    id 190
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 191
    label "bramkarz"
  ]
  node [
    id 192
    label "nadgarstek"
  ]
  node [
    id 193
    label "graba"
  ]
  node [
    id 194
    label "r&#261;czyna"
  ]
  node [
    id 195
    label "k&#322;&#261;b"
  ]
  node [
    id 196
    label "pi&#322;ka"
  ]
  node [
    id 197
    label "cmoknonsens"
  ]
  node [
    id 198
    label "pomocnik"
  ]
  node [
    id 199
    label "gestykulowanie"
  ]
  node [
    id 200
    label "chwytanie"
  ]
  node [
    id 201
    label "obietnica"
  ]
  node [
    id 202
    label "spos&#243;b"
  ]
  node [
    id 203
    label "zagrywka"
  ]
  node [
    id 204
    label "kroki"
  ]
  node [
    id 205
    label "hasta"
  ]
  node [
    id 206
    label "wykroczenie"
  ]
  node [
    id 207
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 208
    label "czerwona_kartka"
  ]
  node [
    id 209
    label "paw"
  ]
  node [
    id 210
    label "rami&#281;"
  ]
  node [
    id 211
    label "wysy&#322;a&#263;"
  ]
  node [
    id 212
    label "air"
  ]
  node [
    id 213
    label "wys&#322;a&#263;"
  ]
  node [
    id 214
    label "oddelegowa&#263;"
  ]
  node [
    id 215
    label "oddelegowywa&#263;"
  ]
  node [
    id 216
    label "zapaleniec"
  ]
  node [
    id 217
    label "wysy&#322;anie"
  ]
  node [
    id 218
    label "wys&#322;anie"
  ]
  node [
    id 219
    label "delegacy"
  ]
  node [
    id 220
    label "oddelegowywanie"
  ]
  node [
    id 221
    label "oddelegowanie"
  ]
  node [
    id 222
    label "klasztor"
  ]
  node [
    id 223
    label "pomieszczenie"
  ]
  node [
    id 224
    label "amfilada"
  ]
  node [
    id 225
    label "front"
  ]
  node [
    id 226
    label "apartment"
  ]
  node [
    id 227
    label "pod&#322;oga"
  ]
  node [
    id 228
    label "udost&#281;pnienie"
  ]
  node [
    id 229
    label "miejsce"
  ]
  node [
    id 230
    label "sklepienie"
  ]
  node [
    id 231
    label "sufit"
  ]
  node [
    id 232
    label "umieszczenie"
  ]
  node [
    id 233
    label "zakamarek"
  ]
  node [
    id 234
    label "siedziba"
  ]
  node [
    id 235
    label "wirydarz"
  ]
  node [
    id 236
    label "kustodia"
  ]
  node [
    id 237
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 238
    label "zakon"
  ]
  node [
    id 239
    label "refektarz"
  ]
  node [
    id 240
    label "kapitularz"
  ]
  node [
    id 241
    label "oratorium"
  ]
  node [
    id 242
    label "&#321;agiewniki"
  ]
  node [
    id 243
    label "ustawienie"
  ]
  node [
    id 244
    label "turn"
  ]
  node [
    id 245
    label "pismo"
  ]
  node [
    id 246
    label "podpowiedzenie"
  ]
  node [
    id 247
    label "przemieszczenie"
  ]
  node [
    id 248
    label "referral"
  ]
  node [
    id 249
    label "mission"
  ]
  node [
    id 250
    label "przeznaczenie"
  ]
  node [
    id 251
    label "delokalizacja"
  ]
  node [
    id 252
    label "osiedlenie"
  ]
  node [
    id 253
    label "move"
  ]
  node [
    id 254
    label "spowodowanie"
  ]
  node [
    id 255
    label "poprzemieszczanie"
  ]
  node [
    id 256
    label "shift"
  ]
  node [
    id 257
    label "zdarzenie_si&#281;"
  ]
  node [
    id 258
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 259
    label "czynno&#347;&#263;"
  ]
  node [
    id 260
    label "psychotest"
  ]
  node [
    id 261
    label "wk&#322;ad"
  ]
  node [
    id 262
    label "przekaz"
  ]
  node [
    id 263
    label "dzie&#322;o"
  ]
  node [
    id 264
    label "paleograf"
  ]
  node [
    id 265
    label "interpunkcja"
  ]
  node [
    id 266
    label "dzia&#322;"
  ]
  node [
    id 267
    label "grafia"
  ]
  node [
    id 268
    label "communication"
  ]
  node [
    id 269
    label "script"
  ]
  node [
    id 270
    label "zajawka"
  ]
  node [
    id 271
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 272
    label "list"
  ]
  node [
    id 273
    label "adres"
  ]
  node [
    id 274
    label "Zwrotnica"
  ]
  node [
    id 275
    label "czasopismo"
  ]
  node [
    id 276
    label "ok&#322;adka"
  ]
  node [
    id 277
    label "ortografia"
  ]
  node [
    id 278
    label "letter"
  ]
  node [
    id 279
    label "komunikacja"
  ]
  node [
    id 280
    label "paleografia"
  ]
  node [
    id 281
    label "j&#281;zyk"
  ]
  node [
    id 282
    label "dokument"
  ]
  node [
    id 283
    label "prasa"
  ]
  node [
    id 284
    label "przekazanie"
  ]
  node [
    id 285
    label "nakazanie"
  ]
  node [
    id 286
    label "wy&#322;o&#380;enie"
  ]
  node [
    id 287
    label "nabicie"
  ]
  node [
    id 288
    label "commitment"
  ]
  node [
    id 289
    label "p&#243;j&#347;cie"
  ]
  node [
    id 290
    label "bed"
  ]
  node [
    id 291
    label "stuffing"
  ]
  node [
    id 292
    label "wytworzenie"
  ]
  node [
    id 293
    label "rzuci&#263;"
  ]
  node [
    id 294
    label "destiny"
  ]
  node [
    id 295
    label "si&#322;a"
  ]
  node [
    id 296
    label "ustalenie"
  ]
  node [
    id 297
    label "przymus"
  ]
  node [
    id 298
    label "przydzielenie"
  ]
  node [
    id 299
    label "oblat"
  ]
  node [
    id 300
    label "obowi&#261;zek"
  ]
  node [
    id 301
    label "rzucenie"
  ]
  node [
    id 302
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 303
    label "wybranie"
  ]
  node [
    id 304
    label "zrobienie"
  ]
  node [
    id 305
    label "u&#322;o&#380;enie"
  ]
  node [
    id 306
    label "erection"
  ]
  node [
    id 307
    label "setup"
  ]
  node [
    id 308
    label "erecting"
  ]
  node [
    id 309
    label "rozmieszczenie"
  ]
  node [
    id 310
    label "poustawianie"
  ]
  node [
    id 311
    label "zinterpretowanie"
  ]
  node [
    id 312
    label "porozstawianie"
  ]
  node [
    id 313
    label "rola"
  ]
  node [
    id 314
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 315
    label "doradzenie"
  ]
  node [
    id 316
    label "powiedzenie"
  ]
  node [
    id 317
    label "pomo&#380;enie"
  ]
  node [
    id 318
    label "p&#322;atnik"
  ]
  node [
    id 319
    label "zwierzchnik"
  ]
  node [
    id 320
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 321
    label "pryncypa&#322;"
  ]
  node [
    id 322
    label "kierowa&#263;"
  ]
  node [
    id 323
    label "kierownictwo"
  ]
  node [
    id 324
    label "j&#281;zykowo"
  ]
  node [
    id 325
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 326
    label "byt"
  ]
  node [
    id 327
    label "osobowo&#347;&#263;"
  ]
  node [
    id 328
    label "organizacja"
  ]
  node [
    id 329
    label "prawo"
  ]
  node [
    id 330
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 331
    label "nauka_prawa"
  ]
  node [
    id 332
    label "tekst"
  ]
  node [
    id 333
    label "komunikacyjnie"
  ]
  node [
    id 334
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 335
    label "mie&#263;_miejsce"
  ]
  node [
    id 336
    label "equal"
  ]
  node [
    id 337
    label "trwa&#263;"
  ]
  node [
    id 338
    label "chodzi&#263;"
  ]
  node [
    id 339
    label "si&#281;ga&#263;"
  ]
  node [
    id 340
    label "stan"
  ]
  node [
    id 341
    label "obecno&#347;&#263;"
  ]
  node [
    id 342
    label "stand"
  ]
  node [
    id 343
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 344
    label "uczestniczy&#263;"
  ]
  node [
    id 345
    label "participate"
  ]
  node [
    id 346
    label "istnie&#263;"
  ]
  node [
    id 347
    label "pozostawa&#263;"
  ]
  node [
    id 348
    label "zostawa&#263;"
  ]
  node [
    id 349
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 350
    label "adhere"
  ]
  node [
    id 351
    label "compass"
  ]
  node [
    id 352
    label "appreciation"
  ]
  node [
    id 353
    label "osi&#261;ga&#263;"
  ]
  node [
    id 354
    label "dociera&#263;"
  ]
  node [
    id 355
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 356
    label "mierzy&#263;"
  ]
  node [
    id 357
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 358
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 359
    label "exsert"
  ]
  node [
    id 360
    label "being"
  ]
  node [
    id 361
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 362
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 363
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 364
    label "p&#322;ywa&#263;"
  ]
  node [
    id 365
    label "run"
  ]
  node [
    id 366
    label "bangla&#263;"
  ]
  node [
    id 367
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 368
    label "przebiega&#263;"
  ]
  node [
    id 369
    label "proceed"
  ]
  node [
    id 370
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 371
    label "carry"
  ]
  node [
    id 372
    label "bywa&#263;"
  ]
  node [
    id 373
    label "dziama&#263;"
  ]
  node [
    id 374
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 375
    label "stara&#263;_si&#281;"
  ]
  node [
    id 376
    label "para"
  ]
  node [
    id 377
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 378
    label "str&#243;j"
  ]
  node [
    id 379
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 380
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 381
    label "krok"
  ]
  node [
    id 382
    label "tryb"
  ]
  node [
    id 383
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 384
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 385
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 386
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 387
    label "continue"
  ]
  node [
    id 388
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 389
    label "Ohio"
  ]
  node [
    id 390
    label "wci&#281;cie"
  ]
  node [
    id 391
    label "Nowy_York"
  ]
  node [
    id 392
    label "samopoczucie"
  ]
  node [
    id 393
    label "Illinois"
  ]
  node [
    id 394
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 395
    label "state"
  ]
  node [
    id 396
    label "Jukatan"
  ]
  node [
    id 397
    label "Kalifornia"
  ]
  node [
    id 398
    label "Wirginia"
  ]
  node [
    id 399
    label "wektor"
  ]
  node [
    id 400
    label "Teksas"
  ]
  node [
    id 401
    label "Goa"
  ]
  node [
    id 402
    label "Waszyngton"
  ]
  node [
    id 403
    label "Massachusetts"
  ]
  node [
    id 404
    label "Alaska"
  ]
  node [
    id 405
    label "Arakan"
  ]
  node [
    id 406
    label "Hawaje"
  ]
  node [
    id 407
    label "Maryland"
  ]
  node [
    id 408
    label "punkt"
  ]
  node [
    id 409
    label "Michigan"
  ]
  node [
    id 410
    label "Arizona"
  ]
  node [
    id 411
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 412
    label "Georgia"
  ]
  node [
    id 413
    label "poziom"
  ]
  node [
    id 414
    label "Pensylwania"
  ]
  node [
    id 415
    label "shape"
  ]
  node [
    id 416
    label "Luizjana"
  ]
  node [
    id 417
    label "Nowy_Meksyk"
  ]
  node [
    id 418
    label "Alabama"
  ]
  node [
    id 419
    label "Kansas"
  ]
  node [
    id 420
    label "Oregon"
  ]
  node [
    id 421
    label "Floryda"
  ]
  node [
    id 422
    label "Oklahoma"
  ]
  node [
    id 423
    label "jednostka_administracyjna"
  ]
  node [
    id 424
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 425
    label "utrzymywanie"
  ]
  node [
    id 426
    label "bycie"
  ]
  node [
    id 427
    label "entity"
  ]
  node [
    id 428
    label "subsystencja"
  ]
  node [
    id 429
    label "utrzyma&#263;"
  ]
  node [
    id 430
    label "egzystencja"
  ]
  node [
    id 431
    label "wy&#380;ywienie"
  ]
  node [
    id 432
    label "ontologicznie"
  ]
  node [
    id 433
    label "utrzymanie"
  ]
  node [
    id 434
    label "utrzymywa&#263;"
  ]
  node [
    id 435
    label "jednostka_organizacyjna"
  ]
  node [
    id 436
    label "struktura"
  ]
  node [
    id 437
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 438
    label "TOPR"
  ]
  node [
    id 439
    label "endecki"
  ]
  node [
    id 440
    label "zesp&#243;&#322;"
  ]
  node [
    id 441
    label "od&#322;am"
  ]
  node [
    id 442
    label "przedstawicielstwo"
  ]
  node [
    id 443
    label "Cepelia"
  ]
  node [
    id 444
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 445
    label "ZBoWiD"
  ]
  node [
    id 446
    label "organization"
  ]
  node [
    id 447
    label "centrala"
  ]
  node [
    id 448
    label "GOPR"
  ]
  node [
    id 449
    label "ZOMO"
  ]
  node [
    id 450
    label "ZMP"
  ]
  node [
    id 451
    label "komitet_koordynacyjny"
  ]
  node [
    id 452
    label "przybud&#243;wka"
  ]
  node [
    id 453
    label "boj&#243;wka"
  ]
  node [
    id 454
    label "mentalno&#347;&#263;"
  ]
  node [
    id 455
    label "superego"
  ]
  node [
    id 456
    label "wyj&#261;tkowy"
  ]
  node [
    id 457
    label "psychika"
  ]
  node [
    id 458
    label "charakter"
  ]
  node [
    id 459
    label "wn&#281;trze"
  ]
  node [
    id 460
    label "self"
  ]
  node [
    id 461
    label "status"
  ]
  node [
    id 462
    label "umocowa&#263;"
  ]
  node [
    id 463
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 464
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 465
    label "procesualistyka"
  ]
  node [
    id 466
    label "regu&#322;a_Allena"
  ]
  node [
    id 467
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 468
    label "kryminalistyka"
  ]
  node [
    id 469
    label "szko&#322;a"
  ]
  node [
    id 470
    label "kierunek"
  ]
  node [
    id 471
    label "zasada_d'Alemberta"
  ]
  node [
    id 472
    label "obserwacja"
  ]
  node [
    id 473
    label "normatywizm"
  ]
  node [
    id 474
    label "jurisprudence"
  ]
  node [
    id 475
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 476
    label "kultura_duchowa"
  ]
  node [
    id 477
    label "przepis"
  ]
  node [
    id 478
    label "prawo_karne_procesowe"
  ]
  node [
    id 479
    label "criterion"
  ]
  node [
    id 480
    label "kazuistyka"
  ]
  node [
    id 481
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 482
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 483
    label "kryminologia"
  ]
  node [
    id 484
    label "opis"
  ]
  node [
    id 485
    label "regu&#322;a_Glogera"
  ]
  node [
    id 486
    label "prawo_Mendla"
  ]
  node [
    id 487
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 488
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 489
    label "prawo_karne"
  ]
  node [
    id 490
    label "legislacyjnie"
  ]
  node [
    id 491
    label "twierdzenie"
  ]
  node [
    id 492
    label "cywilistyka"
  ]
  node [
    id 493
    label "judykatura"
  ]
  node [
    id 494
    label "kanonistyka"
  ]
  node [
    id 495
    label "standard"
  ]
  node [
    id 496
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 497
    label "law"
  ]
  node [
    id 498
    label "qualification"
  ]
  node [
    id 499
    label "dominion"
  ]
  node [
    id 500
    label "wykonawczy"
  ]
  node [
    id 501
    label "zasada"
  ]
  node [
    id 502
    label "normalizacja"
  ]
  node [
    id 503
    label "hermeneutyka"
  ]
  node [
    id 504
    label "kontekst"
  ]
  node [
    id 505
    label "apprehension"
  ]
  node [
    id 506
    label "wytw&#243;r"
  ]
  node [
    id 507
    label "robienie"
  ]
  node [
    id 508
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 509
    label "interpretation"
  ]
  node [
    id 510
    label "obja&#347;nienie"
  ]
  node [
    id 511
    label "czucie"
  ]
  node [
    id 512
    label "realization"
  ]
  node [
    id 513
    label "kumanie"
  ]
  node [
    id 514
    label "wnioskowanie"
  ]
  node [
    id 515
    label "fabrication"
  ]
  node [
    id 516
    label "porobienie"
  ]
  node [
    id 517
    label "przedmiot"
  ]
  node [
    id 518
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 519
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 520
    label "creation"
  ]
  node [
    id 521
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 522
    label "act"
  ]
  node [
    id 523
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 524
    label "tentegowanie"
  ]
  node [
    id 525
    label "activity"
  ]
  node [
    id 526
    label "bezproblemowy"
  ]
  node [
    id 527
    label "wydarzenie"
  ]
  node [
    id 528
    label "postrzeganie"
  ]
  node [
    id 529
    label "doznanie"
  ]
  node [
    id 530
    label "przewidywanie"
  ]
  node [
    id 531
    label "sztywnienie"
  ]
  node [
    id 532
    label "zmys&#322;"
  ]
  node [
    id 533
    label "smell"
  ]
  node [
    id 534
    label "emotion"
  ]
  node [
    id 535
    label "sztywnie&#263;"
  ]
  node [
    id 536
    label "uczuwanie"
  ]
  node [
    id 537
    label "owiewanie"
  ]
  node [
    id 538
    label "ogarnianie"
  ]
  node [
    id 539
    label "tactile_property"
  ]
  node [
    id 540
    label "p&#322;&#243;d"
  ]
  node [
    id 541
    label "work"
  ]
  node [
    id 542
    label "rezultat"
  ]
  node [
    id 543
    label "explanation"
  ]
  node [
    id 544
    label "remark"
  ]
  node [
    id 545
    label "report"
  ]
  node [
    id 546
    label "zrozumia&#322;y"
  ]
  node [
    id 547
    label "przedstawienie"
  ]
  node [
    id 548
    label "informacja"
  ]
  node [
    id 549
    label "poinformowanie"
  ]
  node [
    id 550
    label "obejrzenie"
  ]
  node [
    id 551
    label "widzenie"
  ]
  node [
    id 552
    label "urzeczywistnianie"
  ]
  node [
    id 553
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 554
    label "przeszkodzenie"
  ]
  node [
    id 555
    label "produkowanie"
  ]
  node [
    id 556
    label "znikni&#281;cie"
  ]
  node [
    id 557
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 558
    label "przeszkadzanie"
  ]
  node [
    id 559
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 560
    label "wyprodukowanie"
  ]
  node [
    id 561
    label "proszenie"
  ]
  node [
    id 562
    label "dochodzenie"
  ]
  node [
    id 563
    label "proces_my&#347;lowy"
  ]
  node [
    id 564
    label "lead"
  ]
  node [
    id 565
    label "konkluzja"
  ]
  node [
    id 566
    label "sk&#322;adanie"
  ]
  node [
    id 567
    label "przes&#322;anka"
  ]
  node [
    id 568
    label "wniosek"
  ]
  node [
    id 569
    label "interpretacja"
  ]
  node [
    id 570
    label "hermeneutics"
  ]
  node [
    id 571
    label "&#347;rodowisko"
  ]
  node [
    id 572
    label "odniesienie"
  ]
  node [
    id 573
    label "otoczenie"
  ]
  node [
    id 574
    label "background"
  ]
  node [
    id 575
    label "causal_agent"
  ]
  node [
    id 576
    label "context"
  ]
  node [
    id 577
    label "warunki"
  ]
  node [
    id 578
    label "fragment"
  ]
  node [
    id 579
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 580
    label "artykulator"
  ]
  node [
    id 581
    label "kod"
  ]
  node [
    id 582
    label "kawa&#322;ek"
  ]
  node [
    id 583
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 584
    label "gramatyka"
  ]
  node [
    id 585
    label "stylik"
  ]
  node [
    id 586
    label "przet&#322;umaczenie"
  ]
  node [
    id 587
    label "formalizowanie"
  ]
  node [
    id 588
    label "ssanie"
  ]
  node [
    id 589
    label "ssa&#263;"
  ]
  node [
    id 590
    label "language"
  ]
  node [
    id 591
    label "liza&#263;"
  ]
  node [
    id 592
    label "napisa&#263;"
  ]
  node [
    id 593
    label "konsonantyzm"
  ]
  node [
    id 594
    label "wokalizm"
  ]
  node [
    id 595
    label "pisa&#263;"
  ]
  node [
    id 596
    label "fonetyka"
  ]
  node [
    id 597
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 598
    label "jeniec"
  ]
  node [
    id 599
    label "but"
  ]
  node [
    id 600
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 601
    label "po_koroniarsku"
  ]
  node [
    id 602
    label "t&#322;umaczenie"
  ]
  node [
    id 603
    label "m&#243;wienie"
  ]
  node [
    id 604
    label "pype&#263;"
  ]
  node [
    id 605
    label "lizanie"
  ]
  node [
    id 606
    label "formalizowa&#263;"
  ]
  node [
    id 607
    label "rozumie&#263;"
  ]
  node [
    id 608
    label "organ"
  ]
  node [
    id 609
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 610
    label "makroglosja"
  ]
  node [
    id 611
    label "m&#243;wi&#263;"
  ]
  node [
    id 612
    label "jama_ustna"
  ]
  node [
    id 613
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 614
    label "formacja_geologiczna"
  ]
  node [
    id 615
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 616
    label "natural_language"
  ]
  node [
    id 617
    label "s&#322;ownictwo"
  ]
  node [
    id 618
    label "urz&#261;dzenie"
  ]
  node [
    id 619
    label "obwiniony"
  ]
  node [
    id 620
    label "r&#281;kopis"
  ]
  node [
    id 621
    label "kodeks_pracy"
  ]
  node [
    id 622
    label "kodeks_morski"
  ]
  node [
    id 623
    label "Justynian"
  ]
  node [
    id 624
    label "code"
  ]
  node [
    id 625
    label "kodeks_drogowy"
  ]
  node [
    id 626
    label "kodeks_dyplomatyczny"
  ]
  node [
    id 627
    label "kodeks_rodzinny"
  ]
  node [
    id 628
    label "kodeks_wykrocze&#324;"
  ]
  node [
    id 629
    label "kodeks_cywilny"
  ]
  node [
    id 630
    label "kodeks_karny"
  ]
  node [
    id 631
    label "cymelium"
  ]
  node [
    id 632
    label "zapis"
  ]
  node [
    id 633
    label "base"
  ]
  node [
    id 634
    label "umowa"
  ]
  node [
    id 635
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 636
    label "moralno&#347;&#263;"
  ]
  node [
    id 637
    label "occupation"
  ]
  node [
    id 638
    label "podstawa"
  ]
  node [
    id 639
    label "substancja"
  ]
  node [
    id 640
    label "prawid&#322;o"
  ]
  node [
    id 641
    label "norma_prawna"
  ]
  node [
    id 642
    label "przedawnienie_si&#281;"
  ]
  node [
    id 643
    label "przedawnianie_si&#281;"
  ]
  node [
    id 644
    label "porada"
  ]
  node [
    id 645
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 646
    label "regulation"
  ]
  node [
    id 647
    label "recepta"
  ]
  node [
    id 648
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 649
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 650
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 651
    label "najem"
  ]
  node [
    id 652
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 653
    label "zak&#322;ad"
  ]
  node [
    id 654
    label "stosunek_pracy"
  ]
  node [
    id 655
    label "benedykty&#324;ski"
  ]
  node [
    id 656
    label "poda&#380;_pracy"
  ]
  node [
    id 657
    label "pracowanie"
  ]
  node [
    id 658
    label "tyrka"
  ]
  node [
    id 659
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 660
    label "zaw&#243;d"
  ]
  node [
    id 661
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 662
    label "tynkarski"
  ]
  node [
    id 663
    label "pracowa&#263;"
  ]
  node [
    id 664
    label "zmiana"
  ]
  node [
    id 665
    label "czynnik_produkcji"
  ]
  node [
    id 666
    label "zobowi&#261;zanie"
  ]
  node [
    id 667
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 668
    label "plac"
  ]
  node [
    id 669
    label "location"
  ]
  node [
    id 670
    label "uwaga"
  ]
  node [
    id 671
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 672
    label "chwila"
  ]
  node [
    id 673
    label "cia&#322;o"
  ]
  node [
    id 674
    label "rz&#261;d"
  ]
  node [
    id 675
    label "stosunek_prawny"
  ]
  node [
    id 676
    label "oblig"
  ]
  node [
    id 677
    label "uregulowa&#263;"
  ]
  node [
    id 678
    label "oddzia&#322;anie"
  ]
  node [
    id 679
    label "duty"
  ]
  node [
    id 680
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 681
    label "zapowied&#378;"
  ]
  node [
    id 682
    label "statement"
  ]
  node [
    id 683
    label "zapewnienie"
  ]
  node [
    id 684
    label "miejsce_pracy"
  ]
  node [
    id 685
    label "&#321;ubianka"
  ]
  node [
    id 686
    label "dzia&#322;_personalny"
  ]
  node [
    id 687
    label "Kreml"
  ]
  node [
    id 688
    label "Bia&#322;y_Dom"
  ]
  node [
    id 689
    label "budynek"
  ]
  node [
    id 690
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 691
    label "sadowisko"
  ]
  node [
    id 692
    label "zak&#322;adka"
  ]
  node [
    id 693
    label "instytucja"
  ]
  node [
    id 694
    label "wyko&#324;czenie"
  ]
  node [
    id 695
    label "firma"
  ]
  node [
    id 696
    label "czyn"
  ]
  node [
    id 697
    label "company"
  ]
  node [
    id 698
    label "instytut"
  ]
  node [
    id 699
    label "cierpliwy"
  ]
  node [
    id 700
    label "mozolny"
  ]
  node [
    id 701
    label "wytrwa&#322;y"
  ]
  node [
    id 702
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 703
    label "benedykty&#324;sko"
  ]
  node [
    id 704
    label "typowy"
  ]
  node [
    id 705
    label "po_benedykty&#324;sku"
  ]
  node [
    id 706
    label "rewizja"
  ]
  node [
    id 707
    label "passage"
  ]
  node [
    id 708
    label "oznaka"
  ]
  node [
    id 709
    label "change"
  ]
  node [
    id 710
    label "ferment"
  ]
  node [
    id 711
    label "komplet"
  ]
  node [
    id 712
    label "anatomopatolog"
  ]
  node [
    id 713
    label "zmianka"
  ]
  node [
    id 714
    label "czas"
  ]
  node [
    id 715
    label "zjawisko"
  ]
  node [
    id 716
    label "amendment"
  ]
  node [
    id 717
    label "odmienianie"
  ]
  node [
    id 718
    label "tura"
  ]
  node [
    id 719
    label "przepracowanie_si&#281;"
  ]
  node [
    id 720
    label "zarz&#261;dzanie"
  ]
  node [
    id 721
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 722
    label "podlizanie_si&#281;"
  ]
  node [
    id 723
    label "dopracowanie"
  ]
  node [
    id 724
    label "podlizywanie_si&#281;"
  ]
  node [
    id 725
    label "uruchamianie"
  ]
  node [
    id 726
    label "dzia&#322;anie"
  ]
  node [
    id 727
    label "d&#261;&#380;enie"
  ]
  node [
    id 728
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 729
    label "uruchomienie"
  ]
  node [
    id 730
    label "nakr&#281;canie"
  ]
  node [
    id 731
    label "funkcjonowanie"
  ]
  node [
    id 732
    label "tr&#243;jstronny"
  ]
  node [
    id 733
    label "postaranie_si&#281;"
  ]
  node [
    id 734
    label "odpocz&#281;cie"
  ]
  node [
    id 735
    label "nakr&#281;cenie"
  ]
  node [
    id 736
    label "zatrzymanie"
  ]
  node [
    id 737
    label "spracowanie_si&#281;"
  ]
  node [
    id 738
    label "skakanie"
  ]
  node [
    id 739
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 740
    label "podtrzymywanie"
  ]
  node [
    id 741
    label "w&#322;&#261;czanie"
  ]
  node [
    id 742
    label "zaprz&#281;ganie"
  ]
  node [
    id 743
    label "podejmowanie"
  ]
  node [
    id 744
    label "maszyna"
  ]
  node [
    id 745
    label "wyrabianie"
  ]
  node [
    id 746
    label "dzianie_si&#281;"
  ]
  node [
    id 747
    label "use"
  ]
  node [
    id 748
    label "przepracowanie"
  ]
  node [
    id 749
    label "poruszanie_si&#281;"
  ]
  node [
    id 750
    label "impact"
  ]
  node [
    id 751
    label "przepracowywanie"
  ]
  node [
    id 752
    label "awansowanie"
  ]
  node [
    id 753
    label "courtship"
  ]
  node [
    id 754
    label "zapracowanie"
  ]
  node [
    id 755
    label "wyrobienie"
  ]
  node [
    id 756
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 757
    label "w&#322;&#261;czenie"
  ]
  node [
    id 758
    label "zawodoznawstwo"
  ]
  node [
    id 759
    label "emocja"
  ]
  node [
    id 760
    label "office"
  ]
  node [
    id 761
    label "kwalifikacje"
  ]
  node [
    id 762
    label "craft"
  ]
  node [
    id 763
    label "transakcja"
  ]
  node [
    id 764
    label "endeavor"
  ]
  node [
    id 765
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 766
    label "podejmowa&#263;"
  ]
  node [
    id 767
    label "do"
  ]
  node [
    id 768
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 769
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 770
    label "dzia&#322;a&#263;"
  ]
  node [
    id 771
    label "funkcjonowa&#263;"
  ]
  node [
    id 772
    label "biuro"
  ]
  node [
    id 773
    label "w&#322;adza"
  ]
  node [
    id 774
    label "wy&#322;&#261;czny"
  ]
  node [
    id 775
    label "w&#322;asny"
  ]
  node [
    id 776
    label "unikatowy"
  ]
  node [
    id 777
    label "jedyny"
  ]
  node [
    id 778
    label "ajencja"
  ]
  node [
    id 779
    label "whole"
  ]
  node [
    id 780
    label "NASA"
  ]
  node [
    id 781
    label "oddzia&#322;"
  ]
  node [
    id 782
    label "bank"
  ]
  node [
    id 783
    label "filia"
  ]
  node [
    id 784
    label "Apeks"
  ]
  node [
    id 785
    label "zasoby"
  ]
  node [
    id 786
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 787
    label "zaufanie"
  ]
  node [
    id 788
    label "Hortex"
  ]
  node [
    id 789
    label "reengineering"
  ]
  node [
    id 790
    label "nazwa_w&#322;asna"
  ]
  node [
    id 791
    label "podmiot_gospodarczy"
  ]
  node [
    id 792
    label "paczkarnia"
  ]
  node [
    id 793
    label "Orlen"
  ]
  node [
    id 794
    label "interes"
  ]
  node [
    id 795
    label "Google"
  ]
  node [
    id 796
    label "Canon"
  ]
  node [
    id 797
    label "Pewex"
  ]
  node [
    id 798
    label "MAN_SE"
  ]
  node [
    id 799
    label "Spo&#322;em"
  ]
  node [
    id 800
    label "klasa"
  ]
  node [
    id 801
    label "networking"
  ]
  node [
    id 802
    label "MAC"
  ]
  node [
    id 803
    label "zasoby_ludzkie"
  ]
  node [
    id 804
    label "Baltona"
  ]
  node [
    id 805
    label "Orbis"
  ]
  node [
    id 806
    label "biurowiec"
  ]
  node [
    id 807
    label "HP"
  ]
  node [
    id 808
    label "osoba_prawna"
  ]
  node [
    id 809
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 810
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 811
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 812
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 813
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 814
    label "Fundusze_Unijne"
  ]
  node [
    id 815
    label "zamyka&#263;"
  ]
  node [
    id 816
    label "establishment"
  ]
  node [
    id 817
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 818
    label "urz&#261;d"
  ]
  node [
    id 819
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 820
    label "afiliowa&#263;"
  ]
  node [
    id 821
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 822
    label "zamykanie"
  ]
  node [
    id 823
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 824
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 825
    label "insourcing"
  ]
  node [
    id 826
    label "column"
  ]
  node [
    id 827
    label "stopie&#324;"
  ]
  node [
    id 828
    label "competence"
  ]
  node [
    id 829
    label "dzier&#380;awa"
  ]
  node [
    id 830
    label "system"
  ]
  node [
    id 831
    label "lias"
  ]
  node [
    id 832
    label "jednostka"
  ]
  node [
    id 833
    label "pi&#281;tro"
  ]
  node [
    id 834
    label "jednostka_geologiczna"
  ]
  node [
    id 835
    label "malm"
  ]
  node [
    id 836
    label "dogger"
  ]
  node [
    id 837
    label "promocja"
  ]
  node [
    id 838
    label "kurs"
  ]
  node [
    id 839
    label "formacja"
  ]
  node [
    id 840
    label "wojsko"
  ]
  node [
    id 841
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 842
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 843
    label "szpital"
  ]
  node [
    id 844
    label "agent_rozliczeniowy"
  ]
  node [
    id 845
    label "kwota"
  ]
  node [
    id 846
    label "konto"
  ]
  node [
    id 847
    label "wk&#322;adca"
  ]
  node [
    id 848
    label "eurorynek"
  ]
  node [
    id 849
    label "czasowo"
  ]
  node [
    id 850
    label "przepustka"
  ]
  node [
    id 851
    label "pobyt"
  ]
  node [
    id 852
    label "nieobecno&#347;&#263;"
  ]
  node [
    id 853
    label "czasowy"
  ]
  node [
    id 854
    label "zezwolenie"
  ]
  node [
    id 855
    label "temporarily"
  ]
  node [
    id 856
    label "set"
  ]
  node [
    id 857
    label "zaznacza&#263;"
  ]
  node [
    id 858
    label "wybiera&#263;"
  ]
  node [
    id 859
    label "inflict"
  ]
  node [
    id 860
    label "ustala&#263;"
  ]
  node [
    id 861
    label "okre&#347;la&#263;"
  ]
  node [
    id 862
    label "wyjmowa&#263;"
  ]
  node [
    id 863
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 864
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 865
    label "sie&#263;_rybacka"
  ]
  node [
    id 866
    label "kotwica"
  ]
  node [
    id 867
    label "peddle"
  ]
  node [
    id 868
    label "unwrap"
  ]
  node [
    id 869
    label "decydowa&#263;"
  ]
  node [
    id 870
    label "zmienia&#263;"
  ]
  node [
    id 871
    label "umacnia&#263;"
  ]
  node [
    id 872
    label "powodowa&#263;"
  ]
  node [
    id 873
    label "arrange"
  ]
  node [
    id 874
    label "uwydatnia&#263;"
  ]
  node [
    id 875
    label "podkre&#347;la&#263;"
  ]
  node [
    id 876
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 877
    label "wskazywa&#263;"
  ]
  node [
    id 878
    label "signify"
  ]
  node [
    id 879
    label "style"
  ]
  node [
    id 880
    label "gem"
  ]
  node [
    id 881
    label "kompozycja"
  ]
  node [
    id 882
    label "runda"
  ]
  node [
    id 883
    label "muzyka"
  ]
  node [
    id 884
    label "zestaw"
  ]
  node [
    id 885
    label "zaj&#281;cie"
  ]
  node [
    id 886
    label "yield"
  ]
  node [
    id 887
    label "zaszkodzenie"
  ]
  node [
    id 888
    label "za&#322;o&#380;enie"
  ]
  node [
    id 889
    label "powierzanie"
  ]
  node [
    id 890
    label "problem"
  ]
  node [
    id 891
    label "przepisanie"
  ]
  node [
    id 892
    label "nakarmienie"
  ]
  node [
    id 893
    label "przepisa&#263;"
  ]
  node [
    id 894
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 895
    label "danie"
  ]
  node [
    id 896
    label "feed"
  ]
  node [
    id 897
    label "zaspokojenie"
  ]
  node [
    id 898
    label "podwini&#281;cie"
  ]
  node [
    id 899
    label "zap&#322;acenie"
  ]
  node [
    id 900
    label "przyodzianie"
  ]
  node [
    id 901
    label "budowla"
  ]
  node [
    id 902
    label "pokrycie"
  ]
  node [
    id 903
    label "rozebranie"
  ]
  node [
    id 904
    label "poubieranie"
  ]
  node [
    id 905
    label "infliction"
  ]
  node [
    id 906
    label "pozak&#322;adanie"
  ]
  node [
    id 907
    label "program"
  ]
  node [
    id 908
    label "przebranie"
  ]
  node [
    id 909
    label "przywdzianie"
  ]
  node [
    id 910
    label "obleczenie_si&#281;"
  ]
  node [
    id 911
    label "utworzenie"
  ]
  node [
    id 912
    label "obleczenie"
  ]
  node [
    id 913
    label "przygotowywanie"
  ]
  node [
    id 914
    label "przymierzenie"
  ]
  node [
    id 915
    label "point"
  ]
  node [
    id 916
    label "przygotowanie"
  ]
  node [
    id 917
    label "proposition"
  ]
  node [
    id 918
    label "przewidzenie"
  ]
  node [
    id 919
    label "sprawa"
  ]
  node [
    id 920
    label "subiekcja"
  ]
  node [
    id 921
    label "problemat"
  ]
  node [
    id 922
    label "jajko_Kolumba"
  ]
  node [
    id 923
    label "obstruction"
  ]
  node [
    id 924
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 925
    label "problematyka"
  ]
  node [
    id 926
    label "trudno&#347;&#263;"
  ]
  node [
    id 927
    label "pierepa&#322;ka"
  ]
  node [
    id 928
    label "ambaras"
  ]
  node [
    id 929
    label "damage"
  ]
  node [
    id 930
    label "podniesienie"
  ]
  node [
    id 931
    label "zniesienie"
  ]
  node [
    id 932
    label "ud&#378;wigni&#281;cie"
  ]
  node [
    id 933
    label "ulepszenie"
  ]
  node [
    id 934
    label "heave"
  ]
  node [
    id 935
    label "odbudowanie"
  ]
  node [
    id 936
    label "oddawanie"
  ]
  node [
    id 937
    label "stanowisko"
  ]
  node [
    id 938
    label "zlecanie"
  ]
  node [
    id 939
    label "ufanie"
  ]
  node [
    id 940
    label "wyznawanie"
  ]
  node [
    id 941
    label "care"
  ]
  node [
    id 942
    label "career"
  ]
  node [
    id 943
    label "anektowanie"
  ]
  node [
    id 944
    label "dostarczenie"
  ]
  node [
    id 945
    label "u&#380;ycie"
  ]
  node [
    id 946
    label "klasyfikacja"
  ]
  node [
    id 947
    label "wzi&#281;cie"
  ]
  node [
    id 948
    label "wzbudzenie"
  ]
  node [
    id 949
    label "wype&#322;nienie"
  ]
  node [
    id 950
    label "zaj&#281;cie_si&#281;"
  ]
  node [
    id 951
    label "zapanowanie"
  ]
  node [
    id 952
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 953
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 954
    label "pozajmowanie"
  ]
  node [
    id 955
    label "ulokowanie_si&#281;"
  ]
  node [
    id 956
    label "usytuowanie_si&#281;"
  ]
  node [
    id 957
    label "obj&#281;cie"
  ]
  node [
    id 958
    label "zabranie"
  ]
  node [
    id 959
    label "skopiowanie"
  ]
  node [
    id 960
    label "arrangement"
  ]
  node [
    id 961
    label "przeniesienie"
  ]
  node [
    id 962
    label "testament"
  ]
  node [
    id 963
    label "lekarstwo"
  ]
  node [
    id 964
    label "answer"
  ]
  node [
    id 965
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 966
    label "transcription"
  ]
  node [
    id 967
    label "zalecenie"
  ]
  node [
    id 968
    label "przekaza&#263;"
  ]
  node [
    id 969
    label "supply"
  ]
  node [
    id 970
    label "zaleci&#263;"
  ]
  node [
    id 971
    label "rewrite"
  ]
  node [
    id 972
    label "zrzec_si&#281;"
  ]
  node [
    id 973
    label "skopiowa&#263;"
  ]
  node [
    id 974
    label "przenie&#347;&#263;"
  ]
  node [
    id 975
    label "production"
  ]
  node [
    id 976
    label "realizacja"
  ]
  node [
    id 977
    label "pojawienie_si&#281;"
  ]
  node [
    id 978
    label "completion"
  ]
  node [
    id 979
    label "ziszczenie_si&#281;"
  ]
  node [
    id 980
    label "obrazowanie"
  ]
  node [
    id 981
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 982
    label "dorobek"
  ]
  node [
    id 983
    label "forma"
  ]
  node [
    id 984
    label "tre&#347;&#263;"
  ]
  node [
    id 985
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 986
    label "retrospektywa"
  ]
  node [
    id 987
    label "works"
  ]
  node [
    id 988
    label "tetralogia"
  ]
  node [
    id 989
    label "komunikat"
  ]
  node [
    id 990
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 991
    label "narobienie"
  ]
  node [
    id 992
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 993
    label "scheduling"
  ]
  node [
    id 994
    label "operacja"
  ]
  node [
    id 995
    label "proces"
  ]
  node [
    id 996
    label "kreacja"
  ]
  node [
    id 997
    label "monta&#380;"
  ]
  node [
    id 998
    label "postprodukcja"
  ]
  node [
    id 999
    label "performance"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 152
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 47
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 36
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 11
    target 512
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 513
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 514
  ]
  edge [
    source 11
    target 515
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 517
  ]
  edge [
    source 11
    target 518
  ]
  edge [
    source 11
    target 519
  ]
  edge [
    source 11
    target 520
  ]
  edge [
    source 11
    target 521
  ]
  edge [
    source 11
    target 522
  ]
  edge [
    source 11
    target 523
  ]
  edge [
    source 11
    target 524
  ]
  edge [
    source 11
    target 525
  ]
  edge [
    source 11
    target 526
  ]
  edge [
    source 11
    target 527
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 229
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 259
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 34
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 13
    target 709
  ]
  edge [
    source 13
    target 710
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 712
  ]
  edge [
    source 13
    target 713
  ]
  edge [
    source 13
    target 714
  ]
  edge [
    source 13
    target 715
  ]
  edge [
    source 13
    target 716
  ]
  edge [
    source 13
    target 717
  ]
  edge [
    source 13
    target 718
  ]
  edge [
    source 13
    target 719
  ]
  edge [
    source 13
    target 720
  ]
  edge [
    source 13
    target 721
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 723
  ]
  edge [
    source 13
    target 724
  ]
  edge [
    source 13
    target 725
  ]
  edge [
    source 13
    target 726
  ]
  edge [
    source 13
    target 727
  ]
  edge [
    source 13
    target 728
  ]
  edge [
    source 13
    target 729
  ]
  edge [
    source 13
    target 730
  ]
  edge [
    source 13
    target 731
  ]
  edge [
    source 13
    target 732
  ]
  edge [
    source 13
    target 733
  ]
  edge [
    source 13
    target 734
  ]
  edge [
    source 13
    target 735
  ]
  edge [
    source 13
    target 736
  ]
  edge [
    source 13
    target 737
  ]
  edge [
    source 13
    target 738
  ]
  edge [
    source 13
    target 739
  ]
  edge [
    source 13
    target 740
  ]
  edge [
    source 13
    target 741
  ]
  edge [
    source 13
    target 742
  ]
  edge [
    source 13
    target 743
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 373
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 366
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 367
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 19
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 442
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 234
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 266
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 440
  ]
  edge [
    source 15
    target 328
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 44
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 495
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 229
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 73
  ]
  edge [
    source 15
    target 435
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 506
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 33
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 76
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 843
  ]
  edge [
    source 15
    target 844
  ]
  edge [
    source 15
    target 845
  ]
  edge [
    source 15
    target 21
  ]
  edge [
    source 15
    target 846
  ]
  edge [
    source 15
    target 847
  ]
  edge [
    source 15
    target 848
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 525
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 527
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 436
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 254
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 378
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 304
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 637
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 300
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 50
  ]
  edge [
    source 18
    target 51
  ]
  edge [
    source 18
    target 52
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 54
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 18
    target 44
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 59
  ]
  edge [
    source 18
    target 60
  ]
  edge [
    source 18
    target 61
  ]
  edge [
    source 18
    target 62
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 257
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 284
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 800
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 19
    target 515
  ]
  edge [
    source 19
    target 975
  ]
  edge [
    source 19
    target 976
  ]
  edge [
    source 19
    target 263
  ]
  edge [
    source 19
    target 977
  ]
  edge [
    source 19
    target 978
  ]
  edge [
    source 19
    target 979
  ]
  edge [
    source 19
    target 259
  ]
  edge [
    source 19
    target 304
  ]
  edge [
    source 19
    target 980
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 982
  ]
  edge [
    source 19
    target 983
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 520
  ]
  edge [
    source 19
    target 332
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 516
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 444
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 525
  ]
  edge [
    source 19
    target 526
  ]
  edge [
    source 19
    target 527
  ]
]
