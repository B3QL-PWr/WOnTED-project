graph [
  node [
    id 0
    label "eric"
    origin "text"
  ]
  node [
    id 1
    label "draper"
    origin "text"
  ]
  node [
    id 2
    label "Eric"
  ]
  node [
    id 3
    label "Draper"
  ]
  node [
    id 4
    label "George"
  ]
  node [
    id 5
    label "wyspa"
  ]
  node [
    id 6
    label "Bush"
  ]
  node [
    id 7
    label "biuro"
  ]
  node [
    id 8
    label "fotografia"
  ]
  node [
    id 9
    label "bia&#322;y"
  ]
  node [
    id 10
    label "dom"
  ]
  node [
    id 11
    label "California"
  ]
  node [
    id 12
    label "State"
  ]
  node [
    id 13
    label "University"
  ]
  node [
    id 14
    label "Long"
  ]
  node [
    id 15
    label "Beach"
  ]
  node [
    id 16
    label "The"
  ]
  node [
    id 17
    label "Seattle"
  ]
  node [
    id 18
    label "Times"
  ]
  node [
    id 19
    label "Associated"
  ]
  node [
    id 20
    label "Press"
  ]
  node [
    id 21
    label "mistrzostwo"
  ]
  node [
    id 22
    label "&#347;wiat"
  ]
  node [
    id 23
    label "pi&#322;ka"
  ]
  node [
    id 24
    label "no&#380;ny"
  ]
  node [
    id 25
    label "1998"
  ]
  node [
    id 26
    label "letni"
  ]
  node [
    id 27
    label "igrzysko"
  ]
  node [
    id 28
    label "olimpijski"
  ]
  node [
    id 29
    label "2000"
  ]
  node [
    id 30
    label "junior"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
]
