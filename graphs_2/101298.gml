graph [
  node [
    id 0
    label "katarzyna"
    origin "text"
  ]
  node [
    id 1
    label "za&#322;uska"
    origin "text"
  ]
  node [
    id 2
    label "Katarzyna"
  ]
  node [
    id 3
    label "za&#322;uski"
  ]
  node [
    id 4
    label "encyklopedia"
  ]
  node [
    id 5
    label "ko&#347;cielny"
  ]
  node [
    id 6
    label "Andrzej"
  ]
  node [
    id 7
    label "Chryzostom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
]
