graph [
  node [
    id 0
    label "dokument"
    origin "text"
  ]
  node [
    id 1
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "teraz"
    origin "text"
  ]
  node [
    id 3
    label "parlament"
    origin "text"
  ]
  node [
    id 4
    label "europejski"
    origin "text"
  ]
  node [
    id 5
    label "gdzie"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "opracowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ostateczny"
    origin "text"
  ]
  node [
    id 9
    label "kszta&#322;t"
    origin "text"
  ]
  node [
    id 10
    label "reguluj&#261;cy"
    origin "text"
  ]
  node [
    id 11
    label "zasada"
    origin "text"
  ]
  node [
    id 12
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "transport"
    origin "text"
  ]
  node [
    id 14
    label "drogowe"
    origin "text"
  ]
  node [
    id 15
    label "rok"
    origin "text"
  ]
  node [
    id 16
    label "zapis"
  ]
  node [
    id 17
    label "&#347;wiadectwo"
  ]
  node [
    id 18
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 19
    label "wytw&#243;r"
  ]
  node [
    id 20
    label "parafa"
  ]
  node [
    id 21
    label "plik"
  ]
  node [
    id 22
    label "raport&#243;wka"
  ]
  node [
    id 23
    label "utw&#243;r"
  ]
  node [
    id 24
    label "record"
  ]
  node [
    id 25
    label "registratura"
  ]
  node [
    id 26
    label "dokumentacja"
  ]
  node [
    id 27
    label "fascyku&#322;"
  ]
  node [
    id 28
    label "artyku&#322;"
  ]
  node [
    id 29
    label "writing"
  ]
  node [
    id 30
    label "sygnatariusz"
  ]
  node [
    id 31
    label "dow&#243;d"
  ]
  node [
    id 32
    label "o&#347;wiadczenie"
  ]
  node [
    id 33
    label "za&#347;wiadczenie"
  ]
  node [
    id 34
    label "certificate"
  ]
  node [
    id 35
    label "promocja"
  ]
  node [
    id 36
    label "spos&#243;b"
  ]
  node [
    id 37
    label "entrance"
  ]
  node [
    id 38
    label "czynno&#347;&#263;"
  ]
  node [
    id 39
    label "wpis"
  ]
  node [
    id 40
    label "normalizacja"
  ]
  node [
    id 41
    label "obrazowanie"
  ]
  node [
    id 42
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 43
    label "organ"
  ]
  node [
    id 44
    label "tre&#347;&#263;"
  ]
  node [
    id 45
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 46
    label "part"
  ]
  node [
    id 47
    label "element_anatomiczny"
  ]
  node [
    id 48
    label "tekst"
  ]
  node [
    id 49
    label "komunikat"
  ]
  node [
    id 50
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 51
    label "przedmiot"
  ]
  node [
    id 52
    label "p&#322;&#243;d"
  ]
  node [
    id 53
    label "work"
  ]
  node [
    id 54
    label "rezultat"
  ]
  node [
    id 55
    label "podkatalog"
  ]
  node [
    id 56
    label "nadpisa&#263;"
  ]
  node [
    id 57
    label "nadpisanie"
  ]
  node [
    id 58
    label "bundle"
  ]
  node [
    id 59
    label "folder"
  ]
  node [
    id 60
    label "nadpisywanie"
  ]
  node [
    id 61
    label "paczka"
  ]
  node [
    id 62
    label "nadpisywa&#263;"
  ]
  node [
    id 63
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 64
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 65
    label "przedstawiciel"
  ]
  node [
    id 66
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 67
    label "wydanie"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "torba"
  ]
  node [
    id 70
    label "ekscerpcja"
  ]
  node [
    id 71
    label "materia&#322;"
  ]
  node [
    id 72
    label "operat"
  ]
  node [
    id 73
    label "kosztorys"
  ]
  node [
    id 74
    label "biuro"
  ]
  node [
    id 75
    label "register"
  ]
  node [
    id 76
    label "blok"
  ]
  node [
    id 77
    label "prawda"
  ]
  node [
    id 78
    label "znak_j&#281;zykowy"
  ]
  node [
    id 79
    label "nag&#322;&#243;wek"
  ]
  node [
    id 80
    label "szkic"
  ]
  node [
    id 81
    label "line"
  ]
  node [
    id 82
    label "fragment"
  ]
  node [
    id 83
    label "wyr&#243;b"
  ]
  node [
    id 84
    label "rodzajnik"
  ]
  node [
    id 85
    label "towar"
  ]
  node [
    id 86
    label "paragraf"
  ]
  node [
    id 87
    label "paraph"
  ]
  node [
    id 88
    label "podpis"
  ]
  node [
    id 89
    label "dolecie&#263;"
  ]
  node [
    id 90
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 91
    label "spotka&#263;"
  ]
  node [
    id 92
    label "przypasowa&#263;"
  ]
  node [
    id 93
    label "hit"
  ]
  node [
    id 94
    label "pocisk"
  ]
  node [
    id 95
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 96
    label "stumble"
  ]
  node [
    id 97
    label "dotrze&#263;"
  ]
  node [
    id 98
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 99
    label "wpa&#347;&#263;"
  ]
  node [
    id 100
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 101
    label "znale&#378;&#263;"
  ]
  node [
    id 102
    label "happen"
  ]
  node [
    id 103
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 104
    label "insert"
  ]
  node [
    id 105
    label "visualize"
  ]
  node [
    id 106
    label "pozna&#263;"
  ]
  node [
    id 107
    label "befall"
  ]
  node [
    id 108
    label "spowodowa&#263;"
  ]
  node [
    id 109
    label "go_steady"
  ]
  node [
    id 110
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 111
    label "strike"
  ]
  node [
    id 112
    label "ulec"
  ]
  node [
    id 113
    label "collapse"
  ]
  node [
    id 114
    label "rzecz"
  ]
  node [
    id 115
    label "d&#378;wi&#281;k"
  ]
  node [
    id 116
    label "fall_upon"
  ]
  node [
    id 117
    label "ponie&#347;&#263;"
  ]
  node [
    id 118
    label "ogrom"
  ]
  node [
    id 119
    label "zapach"
  ]
  node [
    id 120
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 121
    label "uderzy&#263;"
  ]
  node [
    id 122
    label "wymy&#347;li&#263;"
  ]
  node [
    id 123
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 124
    label "wpada&#263;"
  ]
  node [
    id 125
    label "decline"
  ]
  node [
    id 126
    label "&#347;wiat&#322;o"
  ]
  node [
    id 127
    label "fall"
  ]
  node [
    id 128
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 129
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 130
    label "emocja"
  ]
  node [
    id 131
    label "odwiedzi&#263;"
  ]
  node [
    id 132
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 133
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 134
    label "pozyska&#263;"
  ]
  node [
    id 135
    label "oceni&#263;"
  ]
  node [
    id 136
    label "devise"
  ]
  node [
    id 137
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 138
    label "dozna&#263;"
  ]
  node [
    id 139
    label "wykry&#263;"
  ]
  node [
    id 140
    label "odzyska&#263;"
  ]
  node [
    id 141
    label "znaj&#347;&#263;"
  ]
  node [
    id 142
    label "invent"
  ]
  node [
    id 143
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 144
    label "utrze&#263;"
  ]
  node [
    id 145
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 146
    label "silnik"
  ]
  node [
    id 147
    label "catch"
  ]
  node [
    id 148
    label "dopasowa&#263;"
  ]
  node [
    id 149
    label "advance"
  ]
  node [
    id 150
    label "get"
  ]
  node [
    id 151
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 152
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 153
    label "dorobi&#263;"
  ]
  node [
    id 154
    label "become"
  ]
  node [
    id 155
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 156
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 157
    label "range"
  ]
  node [
    id 158
    label "flow"
  ]
  node [
    id 159
    label "doj&#347;&#263;"
  ]
  node [
    id 160
    label "moda"
  ]
  node [
    id 161
    label "popularny"
  ]
  node [
    id 162
    label "sensacja"
  ]
  node [
    id 163
    label "nowina"
  ]
  node [
    id 164
    label "odkrycie"
  ]
  node [
    id 165
    label "amunicja"
  ]
  node [
    id 166
    label "g&#322;owica"
  ]
  node [
    id 167
    label "trafienie"
  ]
  node [
    id 168
    label "trafianie"
  ]
  node [
    id 169
    label "kulka"
  ]
  node [
    id 170
    label "rdze&#324;"
  ]
  node [
    id 171
    label "prochownia"
  ]
  node [
    id 172
    label "przeniesienie"
  ]
  node [
    id 173
    label "&#322;adunek_bojowy"
  ]
  node [
    id 174
    label "przenoszenie"
  ]
  node [
    id 175
    label "przenie&#347;&#263;"
  ]
  node [
    id 176
    label "trafia&#263;"
  ]
  node [
    id 177
    label "przenosi&#263;"
  ]
  node [
    id 178
    label "bro&#324;"
  ]
  node [
    id 179
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 180
    label "chwila"
  ]
  node [
    id 181
    label "time"
  ]
  node [
    id 182
    label "czas"
  ]
  node [
    id 183
    label "urz&#261;d"
  ]
  node [
    id 184
    label "europarlament"
  ]
  node [
    id 185
    label "plankton_polityczny"
  ]
  node [
    id 186
    label "grupa"
  ]
  node [
    id 187
    label "grupa_bilateralna"
  ]
  node [
    id 188
    label "ustawodawca"
  ]
  node [
    id 189
    label "legislature"
  ]
  node [
    id 190
    label "ustanowiciel"
  ]
  node [
    id 191
    label "autor"
  ]
  node [
    id 192
    label "stanowisko"
  ]
  node [
    id 193
    label "position"
  ]
  node [
    id 194
    label "instytucja"
  ]
  node [
    id 195
    label "siedziba"
  ]
  node [
    id 196
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 197
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 198
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 199
    label "mianowaniec"
  ]
  node [
    id 200
    label "dzia&#322;"
  ]
  node [
    id 201
    label "okienko"
  ]
  node [
    id 202
    label "w&#322;adza"
  ]
  node [
    id 203
    label "odm&#322;adzanie"
  ]
  node [
    id 204
    label "liga"
  ]
  node [
    id 205
    label "jednostka_systematyczna"
  ]
  node [
    id 206
    label "asymilowanie"
  ]
  node [
    id 207
    label "gromada"
  ]
  node [
    id 208
    label "asymilowa&#263;"
  ]
  node [
    id 209
    label "egzemplarz"
  ]
  node [
    id 210
    label "Entuzjastki"
  ]
  node [
    id 211
    label "kompozycja"
  ]
  node [
    id 212
    label "Terranie"
  ]
  node [
    id 213
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 214
    label "category"
  ]
  node [
    id 215
    label "pakiet_klimatyczny"
  ]
  node [
    id 216
    label "oddzia&#322;"
  ]
  node [
    id 217
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 218
    label "cz&#261;steczka"
  ]
  node [
    id 219
    label "stage_set"
  ]
  node [
    id 220
    label "type"
  ]
  node [
    id 221
    label "specgrupa"
  ]
  node [
    id 222
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 223
    label "&#346;wietliki"
  ]
  node [
    id 224
    label "odm&#322;odzenie"
  ]
  node [
    id 225
    label "Eurogrupa"
  ]
  node [
    id 226
    label "odm&#322;adza&#263;"
  ]
  node [
    id 227
    label "formacja_geologiczna"
  ]
  node [
    id 228
    label "harcerze_starsi"
  ]
  node [
    id 229
    label "Bruksela"
  ]
  node [
    id 230
    label "eurowybory"
  ]
  node [
    id 231
    label "po_europejsku"
  ]
  node [
    id 232
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 233
    label "European"
  ]
  node [
    id 234
    label "typowy"
  ]
  node [
    id 235
    label "charakterystyczny"
  ]
  node [
    id 236
    label "europejsko"
  ]
  node [
    id 237
    label "charakterystycznie"
  ]
  node [
    id 238
    label "szczeg&#243;lny"
  ]
  node [
    id 239
    label "wyj&#261;tkowy"
  ]
  node [
    id 240
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 241
    label "podobny"
  ]
  node [
    id 242
    label "zwyczajny"
  ]
  node [
    id 243
    label "typowo"
  ]
  node [
    id 244
    label "cz&#281;sty"
  ]
  node [
    id 245
    label "zwyk&#322;y"
  ]
  node [
    id 246
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 247
    label "nale&#380;ny"
  ]
  node [
    id 248
    label "nale&#380;yty"
  ]
  node [
    id 249
    label "uprawniony"
  ]
  node [
    id 250
    label "zasadniczy"
  ]
  node [
    id 251
    label "stosownie"
  ]
  node [
    id 252
    label "taki"
  ]
  node [
    id 253
    label "prawdziwy"
  ]
  node [
    id 254
    label "ten"
  ]
  node [
    id 255
    label "dobry"
  ]
  node [
    id 256
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 257
    label "mie&#263;_miejsce"
  ]
  node [
    id 258
    label "equal"
  ]
  node [
    id 259
    label "trwa&#263;"
  ]
  node [
    id 260
    label "chodzi&#263;"
  ]
  node [
    id 261
    label "si&#281;ga&#263;"
  ]
  node [
    id 262
    label "stan"
  ]
  node [
    id 263
    label "obecno&#347;&#263;"
  ]
  node [
    id 264
    label "stand"
  ]
  node [
    id 265
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 266
    label "uczestniczy&#263;"
  ]
  node [
    id 267
    label "participate"
  ]
  node [
    id 268
    label "robi&#263;"
  ]
  node [
    id 269
    label "istnie&#263;"
  ]
  node [
    id 270
    label "pozostawa&#263;"
  ]
  node [
    id 271
    label "zostawa&#263;"
  ]
  node [
    id 272
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 273
    label "adhere"
  ]
  node [
    id 274
    label "compass"
  ]
  node [
    id 275
    label "korzysta&#263;"
  ]
  node [
    id 276
    label "appreciation"
  ]
  node [
    id 277
    label "osi&#261;ga&#263;"
  ]
  node [
    id 278
    label "dociera&#263;"
  ]
  node [
    id 279
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 280
    label "mierzy&#263;"
  ]
  node [
    id 281
    label "u&#380;ywa&#263;"
  ]
  node [
    id 282
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 283
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 284
    label "exsert"
  ]
  node [
    id 285
    label "being"
  ]
  node [
    id 286
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 287
    label "cecha"
  ]
  node [
    id 288
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 289
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 290
    label "p&#322;ywa&#263;"
  ]
  node [
    id 291
    label "run"
  ]
  node [
    id 292
    label "bangla&#263;"
  ]
  node [
    id 293
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 294
    label "przebiega&#263;"
  ]
  node [
    id 295
    label "wk&#322;ada&#263;"
  ]
  node [
    id 296
    label "proceed"
  ]
  node [
    id 297
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 298
    label "carry"
  ]
  node [
    id 299
    label "bywa&#263;"
  ]
  node [
    id 300
    label "dziama&#263;"
  ]
  node [
    id 301
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 302
    label "stara&#263;_si&#281;"
  ]
  node [
    id 303
    label "para"
  ]
  node [
    id 304
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 305
    label "str&#243;j"
  ]
  node [
    id 306
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 307
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 308
    label "krok"
  ]
  node [
    id 309
    label "tryb"
  ]
  node [
    id 310
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 311
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 312
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 313
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 314
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 315
    label "continue"
  ]
  node [
    id 316
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 317
    label "Ohio"
  ]
  node [
    id 318
    label "wci&#281;cie"
  ]
  node [
    id 319
    label "Nowy_York"
  ]
  node [
    id 320
    label "warstwa"
  ]
  node [
    id 321
    label "samopoczucie"
  ]
  node [
    id 322
    label "Illinois"
  ]
  node [
    id 323
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 324
    label "state"
  ]
  node [
    id 325
    label "Jukatan"
  ]
  node [
    id 326
    label "Kalifornia"
  ]
  node [
    id 327
    label "Wirginia"
  ]
  node [
    id 328
    label "wektor"
  ]
  node [
    id 329
    label "Teksas"
  ]
  node [
    id 330
    label "Goa"
  ]
  node [
    id 331
    label "Waszyngton"
  ]
  node [
    id 332
    label "miejsce"
  ]
  node [
    id 333
    label "Massachusetts"
  ]
  node [
    id 334
    label "Alaska"
  ]
  node [
    id 335
    label "Arakan"
  ]
  node [
    id 336
    label "Hawaje"
  ]
  node [
    id 337
    label "Maryland"
  ]
  node [
    id 338
    label "punkt"
  ]
  node [
    id 339
    label "Michigan"
  ]
  node [
    id 340
    label "Arizona"
  ]
  node [
    id 341
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 342
    label "Georgia"
  ]
  node [
    id 343
    label "poziom"
  ]
  node [
    id 344
    label "Pensylwania"
  ]
  node [
    id 345
    label "shape"
  ]
  node [
    id 346
    label "Luizjana"
  ]
  node [
    id 347
    label "Nowy_Meksyk"
  ]
  node [
    id 348
    label "Alabama"
  ]
  node [
    id 349
    label "ilo&#347;&#263;"
  ]
  node [
    id 350
    label "Kansas"
  ]
  node [
    id 351
    label "Oregon"
  ]
  node [
    id 352
    label "Floryda"
  ]
  node [
    id 353
    label "Oklahoma"
  ]
  node [
    id 354
    label "jednostka_administracyjna"
  ]
  node [
    id 355
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 356
    label "przygotowa&#263;"
  ]
  node [
    id 357
    label "set"
  ]
  node [
    id 358
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 359
    label "wykona&#263;"
  ]
  node [
    id 360
    label "cook"
  ]
  node [
    id 361
    label "wyszkoli&#263;"
  ]
  node [
    id 362
    label "train"
  ]
  node [
    id 363
    label "arrange"
  ]
  node [
    id 364
    label "zrobi&#263;"
  ]
  node [
    id 365
    label "wytworzy&#263;"
  ]
  node [
    id 366
    label "dress"
  ]
  node [
    id 367
    label "ukierunkowa&#263;"
  ]
  node [
    id 368
    label "konieczny"
  ]
  node [
    id 369
    label "ostatecznie"
  ]
  node [
    id 370
    label "zupe&#322;ny"
  ]
  node [
    id 371
    label "skrajny"
  ]
  node [
    id 372
    label "niezb&#281;dnie"
  ]
  node [
    id 373
    label "skrajnie"
  ]
  node [
    id 374
    label "okrajkowy"
  ]
  node [
    id 375
    label "og&#243;lnie"
  ]
  node [
    id 376
    label "w_pizdu"
  ]
  node [
    id 377
    label "ca&#322;y"
  ]
  node [
    id 378
    label "kompletnie"
  ]
  node [
    id 379
    label "&#322;&#261;czny"
  ]
  node [
    id 380
    label "zupe&#322;nie"
  ]
  node [
    id 381
    label "pe&#322;ny"
  ]
  node [
    id 382
    label "finalnie"
  ]
  node [
    id 383
    label "formacja"
  ]
  node [
    id 384
    label "punkt_widzenia"
  ]
  node [
    id 385
    label "wygl&#261;d"
  ]
  node [
    id 386
    label "g&#322;owa"
  ]
  node [
    id 387
    label "spirala"
  ]
  node [
    id 388
    label "p&#322;at"
  ]
  node [
    id 389
    label "comeliness"
  ]
  node [
    id 390
    label "kielich"
  ]
  node [
    id 391
    label "face"
  ]
  node [
    id 392
    label "blaszka"
  ]
  node [
    id 393
    label "charakter"
  ]
  node [
    id 394
    label "p&#281;tla"
  ]
  node [
    id 395
    label "obiekt"
  ]
  node [
    id 396
    label "pasmo"
  ]
  node [
    id 397
    label "linearno&#347;&#263;"
  ]
  node [
    id 398
    label "gwiazda"
  ]
  node [
    id 399
    label "miniatura"
  ]
  node [
    id 400
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 401
    label "wydarzenie"
  ]
  node [
    id 402
    label "cz&#322;owiek"
  ]
  node [
    id 403
    label "osobowo&#347;&#263;"
  ]
  node [
    id 404
    label "psychika"
  ]
  node [
    id 405
    label "posta&#263;"
  ]
  node [
    id 406
    label "kompleksja"
  ]
  node [
    id 407
    label "fizjonomia"
  ]
  node [
    id 408
    label "zjawisko"
  ]
  node [
    id 409
    label "entity"
  ]
  node [
    id 410
    label "charakterystyka"
  ]
  node [
    id 411
    label "m&#322;ot"
  ]
  node [
    id 412
    label "znak"
  ]
  node [
    id 413
    label "drzewo"
  ]
  node [
    id 414
    label "pr&#243;ba"
  ]
  node [
    id 415
    label "attribute"
  ]
  node [
    id 416
    label "marka"
  ]
  node [
    id 417
    label "postarzenie"
  ]
  node [
    id 418
    label "postarzanie"
  ]
  node [
    id 419
    label "brzydota"
  ]
  node [
    id 420
    label "portrecista"
  ]
  node [
    id 421
    label "postarza&#263;"
  ]
  node [
    id 422
    label "nadawanie"
  ]
  node [
    id 423
    label "postarzy&#263;"
  ]
  node [
    id 424
    label "widok"
  ]
  node [
    id 425
    label "prostota"
  ]
  node [
    id 426
    label "ubarwienie"
  ]
  node [
    id 427
    label "co&#347;"
  ]
  node [
    id 428
    label "budynek"
  ]
  node [
    id 429
    label "thing"
  ]
  node [
    id 430
    label "poj&#281;cie"
  ]
  node [
    id 431
    label "program"
  ]
  node [
    id 432
    label "strona"
  ]
  node [
    id 433
    label "Bund"
  ]
  node [
    id 434
    label "Mazowsze"
  ]
  node [
    id 435
    label "PPR"
  ]
  node [
    id 436
    label "Jakobici"
  ]
  node [
    id 437
    label "zesp&#243;&#322;"
  ]
  node [
    id 438
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 439
    label "leksem"
  ]
  node [
    id 440
    label "SLD"
  ]
  node [
    id 441
    label "zespolik"
  ]
  node [
    id 442
    label "Razem"
  ]
  node [
    id 443
    label "PiS"
  ]
  node [
    id 444
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 445
    label "partia"
  ]
  node [
    id 446
    label "Kuomintang"
  ]
  node [
    id 447
    label "ZSL"
  ]
  node [
    id 448
    label "szko&#322;a"
  ]
  node [
    id 449
    label "jednostka"
  ]
  node [
    id 450
    label "proces"
  ]
  node [
    id 451
    label "organizacja"
  ]
  node [
    id 452
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 453
    label "rugby"
  ]
  node [
    id 454
    label "AWS"
  ]
  node [
    id 455
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 456
    label "PO"
  ]
  node [
    id 457
    label "si&#322;a"
  ]
  node [
    id 458
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 459
    label "Federali&#347;ci"
  ]
  node [
    id 460
    label "PSL"
  ]
  node [
    id 461
    label "wojsko"
  ]
  node [
    id 462
    label "Wigowie"
  ]
  node [
    id 463
    label "ZChN"
  ]
  node [
    id 464
    label "egzekutywa"
  ]
  node [
    id 465
    label "rocznik"
  ]
  node [
    id 466
    label "The_Beatles"
  ]
  node [
    id 467
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 468
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 469
    label "unit"
  ]
  node [
    id 470
    label "Depeche_Mode"
  ]
  node [
    id 471
    label "forma"
  ]
  node [
    id 472
    label "signal"
  ]
  node [
    id 473
    label "li&#347;&#263;"
  ]
  node [
    id 474
    label "tw&#243;r"
  ]
  node [
    id 475
    label "odznaczenie"
  ]
  node [
    id 476
    label "kapelusz"
  ]
  node [
    id 477
    label "Arktur"
  ]
  node [
    id 478
    label "Gwiazda_Polarna"
  ]
  node [
    id 479
    label "agregatka"
  ]
  node [
    id 480
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 481
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 482
    label "S&#322;o&#324;ce"
  ]
  node [
    id 483
    label "Nibiru"
  ]
  node [
    id 484
    label "konstelacja"
  ]
  node [
    id 485
    label "ornament"
  ]
  node [
    id 486
    label "delta_Scuti"
  ]
  node [
    id 487
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 488
    label "s&#322;awa"
  ]
  node [
    id 489
    label "promie&#324;"
  ]
  node [
    id 490
    label "star"
  ]
  node [
    id 491
    label "gwiazdosz"
  ]
  node [
    id 492
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 493
    label "asocjacja_gwiazd"
  ]
  node [
    id 494
    label "supergrupa"
  ]
  node [
    id 495
    label "sid&#322;a"
  ]
  node [
    id 496
    label "ko&#322;o"
  ]
  node [
    id 497
    label "p&#281;tlica"
  ]
  node [
    id 498
    label "hank"
  ]
  node [
    id 499
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 500
    label "akrobacja_lotnicza"
  ]
  node [
    id 501
    label "zawi&#261;zywanie"
  ]
  node [
    id 502
    label "z&#322;&#261;czenie"
  ]
  node [
    id 503
    label "zawi&#261;zanie"
  ]
  node [
    id 504
    label "arrest"
  ]
  node [
    id 505
    label "zawi&#261;za&#263;"
  ]
  node [
    id 506
    label "koniec"
  ]
  node [
    id 507
    label "kra&#324;c&#243;wka"
  ]
  node [
    id 508
    label "roztruchan"
  ]
  node [
    id 509
    label "naczynie"
  ]
  node [
    id 510
    label "dzia&#322;ka"
  ]
  node [
    id 511
    label "kwiat"
  ]
  node [
    id 512
    label "puch_kielichowy"
  ]
  node [
    id 513
    label "zawarto&#347;&#263;"
  ]
  node [
    id 514
    label "Graal"
  ]
  node [
    id 515
    label "pryncypa&#322;"
  ]
  node [
    id 516
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 517
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 518
    label "wiedza"
  ]
  node [
    id 519
    label "kierowa&#263;"
  ]
  node [
    id 520
    label "alkohol"
  ]
  node [
    id 521
    label "zdolno&#347;&#263;"
  ]
  node [
    id 522
    label "&#380;ycie"
  ]
  node [
    id 523
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 524
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 525
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 526
    label "sztuka"
  ]
  node [
    id 527
    label "dekiel"
  ]
  node [
    id 528
    label "ro&#347;lina"
  ]
  node [
    id 529
    label "&#347;ci&#281;cie"
  ]
  node [
    id 530
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 531
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 532
    label "&#347;ci&#281;gno"
  ]
  node [
    id 533
    label "noosfera"
  ]
  node [
    id 534
    label "byd&#322;o"
  ]
  node [
    id 535
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 536
    label "makrocefalia"
  ]
  node [
    id 537
    label "ucho"
  ]
  node [
    id 538
    label "g&#243;ra"
  ]
  node [
    id 539
    label "m&#243;zg"
  ]
  node [
    id 540
    label "kierownictwo"
  ]
  node [
    id 541
    label "fryzura"
  ]
  node [
    id 542
    label "umys&#322;"
  ]
  node [
    id 543
    label "cia&#322;o"
  ]
  node [
    id 544
    label "cz&#322;onek"
  ]
  node [
    id 545
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 546
    label "czaszka"
  ]
  node [
    id 547
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 548
    label "whirl"
  ]
  node [
    id 549
    label "krzywa"
  ]
  node [
    id 550
    label "spiralny"
  ]
  node [
    id 551
    label "nagromadzenie"
  ]
  node [
    id 552
    label "wk&#322;adka"
  ]
  node [
    id 553
    label "spirograf"
  ]
  node [
    id 554
    label "spiral"
  ]
  node [
    id 555
    label "przebieg"
  ]
  node [
    id 556
    label "pas"
  ]
  node [
    id 557
    label "swath"
  ]
  node [
    id 558
    label "streak"
  ]
  node [
    id 559
    label "kana&#322;"
  ]
  node [
    id 560
    label "strip"
  ]
  node [
    id 561
    label "ulica"
  ]
  node [
    id 562
    label "kopia"
  ]
  node [
    id 563
    label "obraz"
  ]
  node [
    id 564
    label "ilustracja"
  ]
  node [
    id 565
    label "miniature"
  ]
  node [
    id 566
    label "kawa&#322;ek"
  ]
  node [
    id 567
    label "centrop&#322;at"
  ]
  node [
    id 568
    label "airfoil"
  ]
  node [
    id 569
    label "&#347;mig&#322;owiec"
  ]
  node [
    id 570
    label "samolot"
  ]
  node [
    id 571
    label "piece"
  ]
  node [
    id 572
    label "plaster"
  ]
  node [
    id 573
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 574
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 575
    label "regu&#322;a_Allena"
  ]
  node [
    id 576
    label "base"
  ]
  node [
    id 577
    label "umowa"
  ]
  node [
    id 578
    label "obserwacja"
  ]
  node [
    id 579
    label "zasada_d'Alemberta"
  ]
  node [
    id 580
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 581
    label "moralno&#347;&#263;"
  ]
  node [
    id 582
    label "criterion"
  ]
  node [
    id 583
    label "opis"
  ]
  node [
    id 584
    label "regu&#322;a_Glogera"
  ]
  node [
    id 585
    label "prawo_Mendla"
  ]
  node [
    id 586
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 587
    label "twierdzenie"
  ]
  node [
    id 588
    label "prawo"
  ]
  node [
    id 589
    label "standard"
  ]
  node [
    id 590
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 591
    label "dominion"
  ]
  node [
    id 592
    label "qualification"
  ]
  node [
    id 593
    label "occupation"
  ]
  node [
    id 594
    label "podstawa"
  ]
  node [
    id 595
    label "substancja"
  ]
  node [
    id 596
    label "prawid&#322;o"
  ]
  node [
    id 597
    label "dobro&#263;"
  ]
  node [
    id 598
    label "aretologia"
  ]
  node [
    id 599
    label "morality"
  ]
  node [
    id 600
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 601
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 602
    label "honesty"
  ]
  node [
    id 603
    label "model"
  ]
  node [
    id 604
    label "organizowa&#263;"
  ]
  node [
    id 605
    label "ordinariness"
  ]
  node [
    id 606
    label "zorganizowa&#263;"
  ]
  node [
    id 607
    label "taniec_towarzyski"
  ]
  node [
    id 608
    label "organizowanie"
  ]
  node [
    id 609
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 610
    label "zorganizowanie"
  ]
  node [
    id 611
    label "wypowied&#378;"
  ]
  node [
    id 612
    label "exposition"
  ]
  node [
    id 613
    label "obja&#347;nienie"
  ]
  node [
    id 614
    label "zawarcie"
  ]
  node [
    id 615
    label "zawrze&#263;"
  ]
  node [
    id 616
    label "czyn"
  ]
  node [
    id 617
    label "warunek"
  ]
  node [
    id 618
    label "gestia_transportowa"
  ]
  node [
    id 619
    label "contract"
  ]
  node [
    id 620
    label "porozumienie"
  ]
  node [
    id 621
    label "klauzula"
  ]
  node [
    id 622
    label "przenikanie"
  ]
  node [
    id 623
    label "byt"
  ]
  node [
    id 624
    label "materia"
  ]
  node [
    id 625
    label "temperatura_krytyczna"
  ]
  node [
    id 626
    label "przenika&#263;"
  ]
  node [
    id 627
    label "smolisty"
  ]
  node [
    id 628
    label "pot&#281;ga"
  ]
  node [
    id 629
    label "documentation"
  ]
  node [
    id 630
    label "column"
  ]
  node [
    id 631
    label "zasadzi&#263;"
  ]
  node [
    id 632
    label "za&#322;o&#380;enie"
  ]
  node [
    id 633
    label "punkt_odniesienia"
  ]
  node [
    id 634
    label "zasadzenie"
  ]
  node [
    id 635
    label "bok"
  ]
  node [
    id 636
    label "d&#243;&#322;"
  ]
  node [
    id 637
    label "dzieci&#281;ctwo"
  ]
  node [
    id 638
    label "background"
  ]
  node [
    id 639
    label "podstawowy"
  ]
  node [
    id 640
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 641
    label "strategia"
  ]
  node [
    id 642
    label "pomys&#322;"
  ]
  node [
    id 643
    label "&#347;ciana"
  ]
  node [
    id 644
    label "narz&#281;dzie"
  ]
  node [
    id 645
    label "nature"
  ]
  node [
    id 646
    label "shoetree"
  ]
  node [
    id 647
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 648
    label "alternatywa_Fredholma"
  ]
  node [
    id 649
    label "oznajmianie"
  ]
  node [
    id 650
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 651
    label "teoria"
  ]
  node [
    id 652
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 653
    label "paradoks_Leontiefa"
  ]
  node [
    id 654
    label "s&#261;d"
  ]
  node [
    id 655
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 656
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 657
    label "teza"
  ]
  node [
    id 658
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 659
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 660
    label "twierdzenie_Pettisa"
  ]
  node [
    id 661
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 662
    label "twierdzenie_Maya"
  ]
  node [
    id 663
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 664
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 665
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 666
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 667
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 668
    label "zapewnianie"
  ]
  node [
    id 669
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 670
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 671
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 672
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 673
    label "twierdzenie_Stokesa"
  ]
  node [
    id 674
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 675
    label "twierdzenie_Cevy"
  ]
  node [
    id 676
    label "twierdzenie_Pascala"
  ]
  node [
    id 677
    label "proposition"
  ]
  node [
    id 678
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 679
    label "komunikowanie"
  ]
  node [
    id 680
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 681
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 682
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 683
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 684
    label "relacja"
  ]
  node [
    id 685
    label "badanie"
  ]
  node [
    id 686
    label "proces_my&#347;lowy"
  ]
  node [
    id 687
    label "remark"
  ]
  node [
    id 688
    label "metoda"
  ]
  node [
    id 689
    label "stwierdzenie"
  ]
  node [
    id 690
    label "observation"
  ]
  node [
    id 691
    label "calibration"
  ]
  node [
    id 692
    label "operacja"
  ]
  node [
    id 693
    label "porz&#261;dek"
  ]
  node [
    id 694
    label "dominance"
  ]
  node [
    id 695
    label "zabieg"
  ]
  node [
    id 696
    label "standardization"
  ]
  node [
    id 697
    label "zmiana"
  ]
  node [
    id 698
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 699
    label "umocowa&#263;"
  ]
  node [
    id 700
    label "procesualistyka"
  ]
  node [
    id 701
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 702
    label "kryminalistyka"
  ]
  node [
    id 703
    label "struktura"
  ]
  node [
    id 704
    label "kierunek"
  ]
  node [
    id 705
    label "normatywizm"
  ]
  node [
    id 706
    label "jurisprudence"
  ]
  node [
    id 707
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 708
    label "kultura_duchowa"
  ]
  node [
    id 709
    label "przepis"
  ]
  node [
    id 710
    label "prawo_karne_procesowe"
  ]
  node [
    id 711
    label "kazuistyka"
  ]
  node [
    id 712
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 713
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 714
    label "kryminologia"
  ]
  node [
    id 715
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 716
    label "prawo_karne"
  ]
  node [
    id 717
    label "legislacyjnie"
  ]
  node [
    id 718
    label "cywilistyka"
  ]
  node [
    id 719
    label "judykatura"
  ]
  node [
    id 720
    label "kanonistyka"
  ]
  node [
    id 721
    label "nauka_prawa"
  ]
  node [
    id 722
    label "podmiot"
  ]
  node [
    id 723
    label "law"
  ]
  node [
    id 724
    label "wykonawczy"
  ]
  node [
    id 725
    label "rola"
  ]
  node [
    id 726
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 727
    label "wytwarza&#263;"
  ]
  node [
    id 728
    label "create"
  ]
  node [
    id 729
    label "muzyka"
  ]
  node [
    id 730
    label "praca"
  ]
  node [
    id 731
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 732
    label "czyni&#263;"
  ]
  node [
    id 733
    label "give"
  ]
  node [
    id 734
    label "stylizowa&#263;"
  ]
  node [
    id 735
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 736
    label "falowa&#263;"
  ]
  node [
    id 737
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 738
    label "peddle"
  ]
  node [
    id 739
    label "wydala&#263;"
  ]
  node [
    id 740
    label "tentegowa&#263;"
  ]
  node [
    id 741
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 742
    label "urz&#261;dza&#263;"
  ]
  node [
    id 743
    label "oszukiwa&#263;"
  ]
  node [
    id 744
    label "ukazywa&#263;"
  ]
  node [
    id 745
    label "przerabia&#263;"
  ]
  node [
    id 746
    label "act"
  ]
  node [
    id 747
    label "post&#281;powa&#263;"
  ]
  node [
    id 748
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 749
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 750
    label "najem"
  ]
  node [
    id 751
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 752
    label "zak&#322;ad"
  ]
  node [
    id 753
    label "stosunek_pracy"
  ]
  node [
    id 754
    label "benedykty&#324;ski"
  ]
  node [
    id 755
    label "poda&#380;_pracy"
  ]
  node [
    id 756
    label "pracowanie"
  ]
  node [
    id 757
    label "tyrka"
  ]
  node [
    id 758
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 759
    label "zaw&#243;d"
  ]
  node [
    id 760
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 761
    label "tynkarski"
  ]
  node [
    id 762
    label "pracowa&#263;"
  ]
  node [
    id 763
    label "czynnik_produkcji"
  ]
  node [
    id 764
    label "zobowi&#261;zanie"
  ]
  node [
    id 765
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 766
    label "wokalistyka"
  ]
  node [
    id 767
    label "wykonywanie"
  ]
  node [
    id 768
    label "muza"
  ]
  node [
    id 769
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 770
    label "beatbox"
  ]
  node [
    id 771
    label "komponowa&#263;"
  ]
  node [
    id 772
    label "komponowanie"
  ]
  node [
    id 773
    label "pasa&#380;"
  ]
  node [
    id 774
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 775
    label "notacja_muzyczna"
  ]
  node [
    id 776
    label "kontrapunkt"
  ]
  node [
    id 777
    label "nauka"
  ]
  node [
    id 778
    label "instrumentalistyka"
  ]
  node [
    id 779
    label "harmonia"
  ]
  node [
    id 780
    label "wys&#322;uchanie"
  ]
  node [
    id 781
    label "kapela"
  ]
  node [
    id 782
    label "britpop"
  ]
  node [
    id 783
    label "uprawienie"
  ]
  node [
    id 784
    label "dialog"
  ]
  node [
    id 785
    label "p&#322;osa"
  ]
  node [
    id 786
    label "ziemia"
  ]
  node [
    id 787
    label "ustawienie"
  ]
  node [
    id 788
    label "scenariusz"
  ]
  node [
    id 789
    label "pole"
  ]
  node [
    id 790
    label "gospodarstwo"
  ]
  node [
    id 791
    label "uprawi&#263;"
  ]
  node [
    id 792
    label "function"
  ]
  node [
    id 793
    label "zreinterpretowa&#263;"
  ]
  node [
    id 794
    label "zastosowanie"
  ]
  node [
    id 795
    label "reinterpretowa&#263;"
  ]
  node [
    id 796
    label "wrench"
  ]
  node [
    id 797
    label "irygowanie"
  ]
  node [
    id 798
    label "ustawi&#263;"
  ]
  node [
    id 799
    label "irygowa&#263;"
  ]
  node [
    id 800
    label "zreinterpretowanie"
  ]
  node [
    id 801
    label "cel"
  ]
  node [
    id 802
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 803
    label "gra&#263;"
  ]
  node [
    id 804
    label "aktorstwo"
  ]
  node [
    id 805
    label "kostium"
  ]
  node [
    id 806
    label "zagon"
  ]
  node [
    id 807
    label "znaczenie"
  ]
  node [
    id 808
    label "zagra&#263;"
  ]
  node [
    id 809
    label "reinterpretowanie"
  ]
  node [
    id 810
    label "sk&#322;ad"
  ]
  node [
    id 811
    label "zagranie"
  ]
  node [
    id 812
    label "radlina"
  ]
  node [
    id 813
    label "granie"
  ]
  node [
    id 814
    label "roz&#322;adunek"
  ]
  node [
    id 815
    label "sprz&#281;t"
  ]
  node [
    id 816
    label "cedu&#322;a"
  ]
  node [
    id 817
    label "jednoszynowy"
  ]
  node [
    id 818
    label "unos"
  ]
  node [
    id 819
    label "traffic"
  ]
  node [
    id 820
    label "prze&#322;adunek"
  ]
  node [
    id 821
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 822
    label "us&#322;uga"
  ]
  node [
    id 823
    label "komunikacja"
  ]
  node [
    id 824
    label "tyfon"
  ]
  node [
    id 825
    label "gospodarka"
  ]
  node [
    id 826
    label "za&#322;adunek"
  ]
  node [
    id 827
    label "produkt_gotowy"
  ]
  node [
    id 828
    label "service"
  ]
  node [
    id 829
    label "asortyment"
  ]
  node [
    id 830
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 831
    label "&#347;wiadczenie"
  ]
  node [
    id 832
    label "transportation_system"
  ]
  node [
    id 833
    label "explicite"
  ]
  node [
    id 834
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 835
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 836
    label "wydeptywanie"
  ]
  node [
    id 837
    label "wydeptanie"
  ]
  node [
    id 838
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 839
    label "implicite"
  ]
  node [
    id 840
    label "ekspedytor"
  ]
  node [
    id 841
    label "metka"
  ]
  node [
    id 842
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 843
    label "szprycowa&#263;"
  ]
  node [
    id 844
    label "naszprycowa&#263;"
  ]
  node [
    id 845
    label "rzuca&#263;"
  ]
  node [
    id 846
    label "tandeta"
  ]
  node [
    id 847
    label "obr&#243;t_handlowy"
  ]
  node [
    id 848
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 849
    label "rzuci&#263;"
  ]
  node [
    id 850
    label "naszprycowanie"
  ]
  node [
    id 851
    label "tkanina"
  ]
  node [
    id 852
    label "szprycowanie"
  ]
  node [
    id 853
    label "za&#322;adownia"
  ]
  node [
    id 854
    label "&#322;&#243;dzki"
  ]
  node [
    id 855
    label "narkobiznes"
  ]
  node [
    id 856
    label "rzucenie"
  ]
  node [
    id 857
    label "rzucanie"
  ]
  node [
    id 858
    label "temat"
  ]
  node [
    id 859
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 860
    label "wn&#281;trze"
  ]
  node [
    id 861
    label "informacja"
  ]
  node [
    id 862
    label "sprz&#281;cior"
  ]
  node [
    id 863
    label "penis"
  ]
  node [
    id 864
    label "kolekcja"
  ]
  node [
    id 865
    label "furniture"
  ]
  node [
    id 866
    label "sprz&#281;cik"
  ]
  node [
    id 867
    label "equipment"
  ]
  node [
    id 868
    label "relokacja"
  ]
  node [
    id 869
    label "kolej"
  ]
  node [
    id 870
    label "raport"
  ]
  node [
    id 871
    label "kurs"
  ]
  node [
    id 872
    label "spis"
  ]
  node [
    id 873
    label "bilet"
  ]
  node [
    id 874
    label "sygnalizator"
  ]
  node [
    id 875
    label "ci&#281;&#380;ar"
  ]
  node [
    id 876
    label "granica"
  ]
  node [
    id 877
    label "&#322;adunek"
  ]
  node [
    id 878
    label "inwentarz"
  ]
  node [
    id 879
    label "rynek"
  ]
  node [
    id 880
    label "mieszkalnictwo"
  ]
  node [
    id 881
    label "agregat_ekonomiczny"
  ]
  node [
    id 882
    label "miejsce_pracy"
  ]
  node [
    id 883
    label "farmaceutyka"
  ]
  node [
    id 884
    label "produkowanie"
  ]
  node [
    id 885
    label "rolnictwo"
  ]
  node [
    id 886
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 887
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 888
    label "obronno&#347;&#263;"
  ]
  node [
    id 889
    label "sektor_prywatny"
  ]
  node [
    id 890
    label "sch&#322;adza&#263;"
  ]
  node [
    id 891
    label "czerwona_strefa"
  ]
  node [
    id 892
    label "sektor_publiczny"
  ]
  node [
    id 893
    label "bankowo&#347;&#263;"
  ]
  node [
    id 894
    label "gospodarowanie"
  ]
  node [
    id 895
    label "obora"
  ]
  node [
    id 896
    label "gospodarka_wodna"
  ]
  node [
    id 897
    label "gospodarka_le&#347;na"
  ]
  node [
    id 898
    label "gospodarowa&#263;"
  ]
  node [
    id 899
    label "fabryka"
  ]
  node [
    id 900
    label "wytw&#243;rnia"
  ]
  node [
    id 901
    label "stodo&#322;a"
  ]
  node [
    id 902
    label "przemys&#322;"
  ]
  node [
    id 903
    label "spichlerz"
  ]
  node [
    id 904
    label "sch&#322;adzanie"
  ]
  node [
    id 905
    label "administracja"
  ]
  node [
    id 906
    label "sch&#322;odzenie"
  ]
  node [
    id 907
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 908
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 909
    label "regulacja_cen"
  ]
  node [
    id 910
    label "szkolnictwo"
  ]
  node [
    id 911
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 912
    label "p&#243;&#322;rocze"
  ]
  node [
    id 913
    label "martwy_sezon"
  ]
  node [
    id 914
    label "kalendarz"
  ]
  node [
    id 915
    label "cykl_astronomiczny"
  ]
  node [
    id 916
    label "lata"
  ]
  node [
    id 917
    label "pora_roku"
  ]
  node [
    id 918
    label "stulecie"
  ]
  node [
    id 919
    label "jubileusz"
  ]
  node [
    id 920
    label "kwarta&#322;"
  ]
  node [
    id 921
    label "miesi&#261;c"
  ]
  node [
    id 922
    label "summer"
  ]
  node [
    id 923
    label "poprzedzanie"
  ]
  node [
    id 924
    label "czasoprzestrze&#324;"
  ]
  node [
    id 925
    label "laba"
  ]
  node [
    id 926
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 927
    label "chronometria"
  ]
  node [
    id 928
    label "rachuba_czasu"
  ]
  node [
    id 929
    label "przep&#322;ywanie"
  ]
  node [
    id 930
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 931
    label "czasokres"
  ]
  node [
    id 932
    label "odczyt"
  ]
  node [
    id 933
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 934
    label "dzieje"
  ]
  node [
    id 935
    label "kategoria_gramatyczna"
  ]
  node [
    id 936
    label "poprzedzenie"
  ]
  node [
    id 937
    label "trawienie"
  ]
  node [
    id 938
    label "pochodzi&#263;"
  ]
  node [
    id 939
    label "period"
  ]
  node [
    id 940
    label "okres_czasu"
  ]
  node [
    id 941
    label "poprzedza&#263;"
  ]
  node [
    id 942
    label "schy&#322;ek"
  ]
  node [
    id 943
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 944
    label "odwlekanie_si&#281;"
  ]
  node [
    id 945
    label "zegar"
  ]
  node [
    id 946
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 947
    label "czwarty_wymiar"
  ]
  node [
    id 948
    label "pochodzenie"
  ]
  node [
    id 949
    label "koniugacja"
  ]
  node [
    id 950
    label "Zeitgeist"
  ]
  node [
    id 951
    label "trawi&#263;"
  ]
  node [
    id 952
    label "pogoda"
  ]
  node [
    id 953
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 954
    label "poprzedzi&#263;"
  ]
  node [
    id 955
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 956
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 957
    label "time_period"
  ]
  node [
    id 958
    label "tydzie&#324;"
  ]
  node [
    id 959
    label "miech"
  ]
  node [
    id 960
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 961
    label "kalendy"
  ]
  node [
    id 962
    label "term"
  ]
  node [
    id 963
    label "rok_akademicki"
  ]
  node [
    id 964
    label "rok_szkolny"
  ]
  node [
    id 965
    label "semester"
  ]
  node [
    id 966
    label "anniwersarz"
  ]
  node [
    id 967
    label "rocznica"
  ]
  node [
    id 968
    label "obszar"
  ]
  node [
    id 969
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 970
    label "long_time"
  ]
  node [
    id 971
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 972
    label "almanac"
  ]
  node [
    id 973
    label "rozk&#322;ad"
  ]
  node [
    id 974
    label "wydawnictwo"
  ]
  node [
    id 975
    label "Juliusz_Cezar"
  ]
  node [
    id 976
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 977
    label "zwy&#380;kowanie"
  ]
  node [
    id 978
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 979
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 980
    label "zaj&#281;cia"
  ]
  node [
    id 981
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 982
    label "trasa"
  ]
  node [
    id 983
    label "przeorientowywanie"
  ]
  node [
    id 984
    label "przejazd"
  ]
  node [
    id 985
    label "przeorientowywa&#263;"
  ]
  node [
    id 986
    label "przeorientowanie"
  ]
  node [
    id 987
    label "klasa"
  ]
  node [
    id 988
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 989
    label "przeorientowa&#263;"
  ]
  node [
    id 990
    label "manner"
  ]
  node [
    id 991
    label "course"
  ]
  node [
    id 992
    label "passage"
  ]
  node [
    id 993
    label "zni&#380;kowanie"
  ]
  node [
    id 994
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 995
    label "seria"
  ]
  node [
    id 996
    label "stawka"
  ]
  node [
    id 997
    label "way"
  ]
  node [
    id 998
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 999
    label "deprecjacja"
  ]
  node [
    id 1000
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1001
    label "drive"
  ]
  node [
    id 1002
    label "bearing"
  ]
  node [
    id 1003
    label "Lira"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 207
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 194
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 11
    target 608
  ]
  edge [
    source 11
    target 609
  ]
  edge [
    source 11
    target 610
  ]
  edge [
    source 11
    target 611
  ]
  edge [
    source 11
    target 612
  ]
  edge [
    source 11
    target 38
  ]
  edge [
    source 11
    target 613
  ]
  edge [
    source 11
    target 614
  ]
  edge [
    source 11
    target 615
  ]
  edge [
    source 11
    target 616
  ]
  edge [
    source 11
    target 617
  ]
  edge [
    source 11
    target 618
  ]
  edge [
    source 11
    target 619
  ]
  edge [
    source 11
    target 620
  ]
  edge [
    source 11
    target 621
  ]
  edge [
    source 11
    target 622
  ]
  edge [
    source 11
    target 623
  ]
  edge [
    source 11
    target 624
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 626
  ]
  edge [
    source 11
    target 627
  ]
  edge [
    source 11
    target 628
  ]
  edge [
    source 11
    target 629
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 630
  ]
  edge [
    source 11
    target 631
  ]
  edge [
    source 11
    target 632
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 11
    target 644
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 645
  ]
  edge [
    source 11
    target 646
  ]
  edge [
    source 11
    target 647
  ]
  edge [
    source 11
    target 648
  ]
  edge [
    source 11
    target 649
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 651
  ]
  edge [
    source 11
    target 652
  ]
  edge [
    source 11
    target 653
  ]
  edge [
    source 11
    target 654
  ]
  edge [
    source 11
    target 655
  ]
  edge [
    source 11
    target 656
  ]
  edge [
    source 11
    target 657
  ]
  edge [
    source 11
    target 658
  ]
  edge [
    source 11
    target 659
  ]
  edge [
    source 11
    target 660
  ]
  edge [
    source 11
    target 661
  ]
  edge [
    source 11
    target 662
  ]
  edge [
    source 11
    target 663
  ]
  edge [
    source 11
    target 664
  ]
  edge [
    source 11
    target 665
  ]
  edge [
    source 11
    target 666
  ]
  edge [
    source 11
    target 667
  ]
  edge [
    source 11
    target 668
  ]
  edge [
    source 11
    target 669
  ]
  edge [
    source 11
    target 670
  ]
  edge [
    source 11
    target 671
  ]
  edge [
    source 11
    target 672
  ]
  edge [
    source 11
    target 673
  ]
  edge [
    source 11
    target 674
  ]
  edge [
    source 11
    target 675
  ]
  edge [
    source 11
    target 676
  ]
  edge [
    source 11
    target 677
  ]
  edge [
    source 11
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 11
    target 695
  ]
  edge [
    source 11
    target 696
  ]
  edge [
    source 11
    target 697
  ]
  edge [
    source 11
    target 698
  ]
  edge [
    source 11
    target 699
  ]
  edge [
    source 11
    target 700
  ]
  edge [
    source 11
    target 701
  ]
  edge [
    source 11
    target 702
  ]
  edge [
    source 11
    target 703
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 704
  ]
  edge [
    source 11
    target 705
  ]
  edge [
    source 11
    target 706
  ]
  edge [
    source 11
    target 707
  ]
  edge [
    source 11
    target 708
  ]
  edge [
    source 11
    target 709
  ]
  edge [
    source 11
    target 710
  ]
  edge [
    source 11
    target 711
  ]
  edge [
    source 11
    target 712
  ]
  edge [
    source 11
    target 713
  ]
  edge [
    source 11
    target 714
  ]
  edge [
    source 11
    target 715
  ]
  edge [
    source 11
    target 716
  ]
  edge [
    source 11
    target 717
  ]
  edge [
    source 11
    target 718
  ]
  edge [
    source 11
    target 719
  ]
  edge [
    source 11
    target 720
  ]
  edge [
    source 11
    target 721
  ]
  edge [
    source 11
    target 722
  ]
  edge [
    source 11
    target 723
  ]
  edge [
    source 11
    target 724
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 38
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 195
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 214
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 219
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 221
  ]
  edge [
    source 13
    target 222
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 224
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 226
  ]
  edge [
    source 13
    target 227
  ]
  edge [
    source 13
    target 228
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 51
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 912
  ]
  edge [
    source 15
    target 913
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 916
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 918
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 920
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 922
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 15
    target 63
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 209
  ]
  edge [
    source 15
    target 210
  ]
  edge [
    source 15
    target 68
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 212
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 214
  ]
  edge [
    source 15
    target 215
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 217
  ]
  edge [
    source 15
    target 218
  ]
  edge [
    source 15
    target 219
  ]
  edge [
    source 15
    target 220
  ]
  edge [
    source 15
    target 221
  ]
  edge [
    source 15
    target 222
  ]
  edge [
    source 15
    target 223
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 225
  ]
  edge [
    source 15
    target 226
  ]
  edge [
    source 15
    target 227
  ]
  edge [
    source 15
    target 228
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 282
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 180
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 940
  ]
  edge [
    source 15
    target 941
  ]
  edge [
    source 15
    target 942
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 944
  ]
  edge [
    source 15
    target 945
  ]
  edge [
    source 15
    target 946
  ]
  edge [
    source 15
    target 947
  ]
  edge [
    source 15
    target 948
  ]
  edge [
    source 15
    target 949
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 15
    target 954
  ]
  edge [
    source 15
    target 955
  ]
  edge [
    source 15
    target 956
  ]
  edge [
    source 15
    target 957
  ]
  edge [
    source 15
    target 958
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 15
    target 971
  ]
  edge [
    source 15
    target 972
  ]
  edge [
    source 15
    target 973
  ]
  edge [
    source 15
    target 974
  ]
  edge [
    source 15
    target 975
  ]
  edge [
    source 15
    target 976
  ]
  edge [
    source 15
    target 977
  ]
  edge [
    source 15
    target 978
  ]
  edge [
    source 15
    target 979
  ]
  edge [
    source 15
    target 980
  ]
  edge [
    source 15
    target 981
  ]
  edge [
    source 15
    target 982
  ]
  edge [
    source 15
    target 983
  ]
  edge [
    source 15
    target 984
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 985
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 986
  ]
  edge [
    source 15
    target 987
  ]
  edge [
    source 15
    target 988
  ]
  edge [
    source 15
    target 989
  ]
  edge [
    source 15
    target 990
  ]
  edge [
    source 15
    target 991
  ]
  edge [
    source 15
    target 992
  ]
  edge [
    source 15
    target 993
  ]
  edge [
    source 15
    target 994
  ]
  edge [
    source 15
    target 995
  ]
  edge [
    source 15
    target 996
  ]
  edge [
    source 15
    target 997
  ]
  edge [
    source 15
    target 998
  ]
  edge [
    source 15
    target 36
  ]
  edge [
    source 15
    target 999
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 15
    target 1002
  ]
  edge [
    source 15
    target 1003
  ]
]
