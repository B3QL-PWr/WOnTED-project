graph [
  node [
    id 0
    label "klasowy"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "wjecha&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 4
    label "p&#243;&#322;metek"
    origin "text"
  ]
  node [
    id 5
    label "niejednolity"
  ]
  node [
    id 6
    label "przychylny"
  ]
  node [
    id 7
    label "klasowo"
  ]
  node [
    id 8
    label "wspania&#322;y"
  ]
  node [
    id 9
    label "zbiorowo"
  ]
  node [
    id 10
    label "wspaniale"
  ]
  node [
    id 11
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 12
    label "pomy&#347;lny"
  ]
  node [
    id 13
    label "pozytywny"
  ]
  node [
    id 14
    label "&#347;wietnie"
  ]
  node [
    id 15
    label "spania&#322;y"
  ]
  node [
    id 16
    label "och&#281;do&#380;ny"
  ]
  node [
    id 17
    label "warto&#347;ciowy"
  ]
  node [
    id 18
    label "zajebisty"
  ]
  node [
    id 19
    label "dobry"
  ]
  node [
    id 20
    label "bogato"
  ]
  node [
    id 21
    label "sprzyjaj&#261;cy"
  ]
  node [
    id 22
    label "przychylnie"
  ]
  node [
    id 23
    label "r&#243;&#380;ny"
  ]
  node [
    id 24
    label "niejednolicie"
  ]
  node [
    id 25
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 26
    label "niejednakowy"
  ]
  node [
    id 27
    label "dok&#322;adnie"
  ]
  node [
    id 28
    label "punctiliously"
  ]
  node [
    id 29
    label "meticulously"
  ]
  node [
    id 30
    label "precyzyjnie"
  ]
  node [
    id 31
    label "dok&#322;adny"
  ]
  node [
    id 32
    label "rzetelnie"
  ]
  node [
    id 33
    label "wkroczy&#263;"
  ]
  node [
    id 34
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 35
    label "move"
  ]
  node [
    id 36
    label "spell"
  ]
  node [
    id 37
    label "wsun&#261;&#263;"
  ]
  node [
    id 38
    label "intervene"
  ]
  node [
    id 39
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 40
    label "powiedzie&#263;"
  ]
  node [
    id 41
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 42
    label "wsun&#261;&#263;_si&#281;"
  ]
  node [
    id 43
    label "przekroczy&#263;"
  ]
  node [
    id 44
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 45
    label "wpa&#347;&#263;"
  ]
  node [
    id 46
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 47
    label "skrytykowa&#263;"
  ]
  node [
    id 48
    label "doj&#347;&#263;"
  ]
  node [
    id 49
    label "zacz&#261;&#263;"
  ]
  node [
    id 50
    label "post&#261;pi&#263;"
  ]
  node [
    id 51
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 52
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 53
    label "odj&#261;&#263;"
  ]
  node [
    id 54
    label "zrobi&#263;"
  ]
  node [
    id 55
    label "cause"
  ]
  node [
    id 56
    label "introduce"
  ]
  node [
    id 57
    label "begin"
  ]
  node [
    id 58
    label "do"
  ]
  node [
    id 59
    label "sta&#263;_si&#281;"
  ]
  node [
    id 60
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 61
    label "supervene"
  ]
  node [
    id 62
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 63
    label "zaj&#347;&#263;"
  ]
  node [
    id 64
    label "catch"
  ]
  node [
    id 65
    label "get"
  ]
  node [
    id 66
    label "bodziec"
  ]
  node [
    id 67
    label "informacja"
  ]
  node [
    id 68
    label "przesy&#322;ka"
  ]
  node [
    id 69
    label "dodatek"
  ]
  node [
    id 70
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 71
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 72
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 73
    label "heed"
  ]
  node [
    id 74
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 75
    label "spowodowa&#263;"
  ]
  node [
    id 76
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 77
    label "dozna&#263;"
  ]
  node [
    id 78
    label "dokoptowa&#263;"
  ]
  node [
    id 79
    label "postrzega&#263;"
  ]
  node [
    id 80
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 81
    label "orgazm"
  ]
  node [
    id 82
    label "dolecie&#263;"
  ]
  node [
    id 83
    label "drive"
  ]
  node [
    id 84
    label "dotrze&#263;"
  ]
  node [
    id 85
    label "uzyska&#263;"
  ]
  node [
    id 86
    label "dop&#322;ata"
  ]
  node [
    id 87
    label "become"
  ]
  node [
    id 88
    label "profit"
  ]
  node [
    id 89
    label "score"
  ]
  node [
    id 90
    label "make"
  ]
  node [
    id 91
    label "work"
  ]
  node [
    id 92
    label "chemia"
  ]
  node [
    id 93
    label "reakcja_chemiczna"
  ]
  node [
    id 94
    label "act"
  ]
  node [
    id 95
    label "discover"
  ]
  node [
    id 96
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 97
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 98
    label "wydoby&#263;"
  ]
  node [
    id 99
    label "poda&#263;"
  ]
  node [
    id 100
    label "okre&#347;li&#263;"
  ]
  node [
    id 101
    label "express"
  ]
  node [
    id 102
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 103
    label "wyrazi&#263;"
  ]
  node [
    id 104
    label "rzekn&#261;&#263;"
  ]
  node [
    id 105
    label "unwrap"
  ]
  node [
    id 106
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 107
    label "convey"
  ]
  node [
    id 108
    label "zaopiniowa&#263;"
  ]
  node [
    id 109
    label "review"
  ]
  node [
    id 110
    label "oceni&#263;"
  ]
  node [
    id 111
    label "motivate"
  ]
  node [
    id 112
    label "zaj&#261;&#263;"
  ]
  node [
    id 113
    label "induct"
  ]
  node [
    id 114
    label "wej&#347;&#263;"
  ]
  node [
    id 115
    label "poruszy&#263;"
  ]
  node [
    id 116
    label "zje&#347;&#263;"
  ]
  node [
    id 117
    label "umie&#347;ci&#263;"
  ]
  node [
    id 118
    label "insert"
  ]
  node [
    id 119
    label "strike"
  ]
  node [
    id 120
    label "ulec"
  ]
  node [
    id 121
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 122
    label "collapse"
  ]
  node [
    id 123
    label "rzecz"
  ]
  node [
    id 124
    label "d&#378;wi&#281;k"
  ]
  node [
    id 125
    label "fall_upon"
  ]
  node [
    id 126
    label "ponie&#347;&#263;"
  ]
  node [
    id 127
    label "ogrom"
  ]
  node [
    id 128
    label "zapach"
  ]
  node [
    id 129
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 130
    label "uderzy&#263;"
  ]
  node [
    id 131
    label "wymy&#347;li&#263;"
  ]
  node [
    id 132
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 133
    label "wpada&#263;"
  ]
  node [
    id 134
    label "decline"
  ]
  node [
    id 135
    label "&#347;wiat&#322;o"
  ]
  node [
    id 136
    label "fall"
  ]
  node [
    id 137
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 138
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 139
    label "emocja"
  ]
  node [
    id 140
    label "spotka&#263;"
  ]
  node [
    id 141
    label "odwiedzi&#263;"
  ]
  node [
    id 142
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 143
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 144
    label "ograniczenie"
  ]
  node [
    id 145
    label "przeby&#263;"
  ]
  node [
    id 146
    label "open"
  ]
  node [
    id 147
    label "min&#261;&#263;"
  ]
  node [
    id 148
    label "cut"
  ]
  node [
    id 149
    label "pique"
  ]
  node [
    id 150
    label "&#347;rodek"
  ]
  node [
    id 151
    label "impreza"
  ]
  node [
    id 152
    label "punkt"
  ]
  node [
    id 153
    label "spos&#243;b"
  ]
  node [
    id 154
    label "miejsce"
  ]
  node [
    id 155
    label "abstrakcja"
  ]
  node [
    id 156
    label "czas"
  ]
  node [
    id 157
    label "chemikalia"
  ]
  node [
    id 158
    label "substancja"
  ]
  node [
    id 159
    label "impra"
  ]
  node [
    id 160
    label "rozrywka"
  ]
  node [
    id 161
    label "przyj&#281;cie"
  ]
  node [
    id 162
    label "okazja"
  ]
  node [
    id 163
    label "party"
  ]
  node [
    id 164
    label "Gaba"
  ]
  node [
    id 165
    label "z"
  ]
  node [
    id 166
    label "Dominik"
  ]
  node [
    id 167
    label "Barbara"
  ]
  node [
    id 168
    label "Mateusz"
  ]
  node [
    id 169
    label "Emilia"
  ]
  node [
    id 170
    label "Seweryn"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 164
    target 166
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 165
    target 167
  ]
  edge [
    source 165
    target 168
  ]
  edge [
    source 165
    target 169
  ]
  edge [
    source 165
    target 170
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 169
    target 170
  ]
]
