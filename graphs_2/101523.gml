graph [
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "jerzy"
    origin "text"
  ]
  node [
    id 2
    label "kozdro&#324;"
    origin "text"
  ]
  node [
    id 3
    label "ablegat"
  ]
  node [
    id 4
    label "izba_ni&#380;sza"
  ]
  node [
    id 5
    label "Korwin"
  ]
  node [
    id 6
    label "dyscyplina_partyjna"
  ]
  node [
    id 7
    label "Miko&#322;ajczyk"
  ]
  node [
    id 8
    label "kurier_dyplomatyczny"
  ]
  node [
    id 9
    label "wys&#322;annik"
  ]
  node [
    id 10
    label "poselstwo"
  ]
  node [
    id 11
    label "parlamentarzysta"
  ]
  node [
    id 12
    label "przedstawiciel"
  ]
  node [
    id 13
    label "dyplomata"
  ]
  node [
    id 14
    label "klubista"
  ]
  node [
    id 15
    label "reprezentacja"
  ]
  node [
    id 16
    label "mandatariusz"
  ]
  node [
    id 17
    label "grupa_bilateralna"
  ]
  node [
    id 18
    label "polityk"
  ]
  node [
    id 19
    label "parlament"
  ]
  node [
    id 20
    label "klub"
  ]
  node [
    id 21
    label "cz&#322;onek"
  ]
  node [
    id 22
    label "mi&#322;y"
  ]
  node [
    id 23
    label "cz&#322;owiek"
  ]
  node [
    id 24
    label "korpus_dyplomatyczny"
  ]
  node [
    id 25
    label "dyplomatyczny"
  ]
  node [
    id 26
    label "takt"
  ]
  node [
    id 27
    label "Metternich"
  ]
  node [
    id 28
    label "dostojnik"
  ]
  node [
    id 29
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 30
    label "przyk&#322;ad"
  ]
  node [
    id 31
    label "substytuowa&#263;"
  ]
  node [
    id 32
    label "substytuowanie"
  ]
  node [
    id 33
    label "zast&#281;pca"
  ]
  node [
    id 34
    label "Jerzy"
  ]
  node [
    id 35
    label "Kozdro&#324;"
  ]
  node [
    id 36
    label "Jaros&#322;awa"
  ]
  node [
    id 37
    label "Kalinowski"
  ]
  node [
    id 38
    label "Barbara"
  ]
  node [
    id 39
    label "Bartu&#347;"
  ]
  node [
    id 40
    label "prawo"
  ]
  node [
    id 41
    label "i"
  ]
  node [
    id 42
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 43
    label "Ryszarda"
  ]
  node [
    id 44
    label "Kalisz"
  ]
  node [
    id 45
    label "stan"
  ]
  node [
    id 46
    label "zjednoczy&#263;"
  ]
  node [
    id 47
    label "ministerstwo"
  ]
  node [
    id 48
    label "Zbigniew"
  ]
  node [
    id 49
    label "Ziobro"
  ]
  node [
    id 50
    label "kodeks"
  ]
  node [
    id 51
    label "cywilny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
]
