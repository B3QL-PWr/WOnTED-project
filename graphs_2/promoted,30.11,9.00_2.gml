graph [
  node [
    id 0
    label "fanatyczny"
    origin "text"
  ]
  node [
    id 1
    label "nazistka"
    origin "text"
  ]
  node [
    id 2
    label "kaza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "skatowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kole&#380;anka"
    origin "text"
  ]
  node [
    id 5
    label "dlatego"
    origin "text"
  ]
  node [
    id 6
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "polski"
    origin "text"
  ]
  node [
    id 8
    label "fanatycznie"
  ]
  node [
    id 9
    label "nakaza&#263;"
  ]
  node [
    id 10
    label "zmusi&#263;"
  ]
  node [
    id 11
    label "wymaga&#263;"
  ]
  node [
    id 12
    label "wyg&#322;asza&#263;"
  ]
  node [
    id 13
    label "zmusza&#263;"
  ]
  node [
    id 14
    label "nakazywa&#263;"
  ]
  node [
    id 15
    label "wyg&#322;osi&#263;"
  ]
  node [
    id 16
    label "say"
  ]
  node [
    id 17
    label "command"
  ]
  node [
    id 18
    label "order"
  ]
  node [
    id 19
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 20
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 21
    label "sandbag"
  ]
  node [
    id 22
    label "powodowa&#263;"
  ]
  node [
    id 23
    label "pop&#281;dzi&#263;_kota"
  ]
  node [
    id 24
    label "zada&#263;_gwa&#322;t"
  ]
  node [
    id 25
    label "spowodowa&#263;"
  ]
  node [
    id 26
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 27
    label "force"
  ]
  node [
    id 28
    label "by&#263;"
  ]
  node [
    id 29
    label "claim"
  ]
  node [
    id 30
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 31
    label "take"
  ]
  node [
    id 32
    label "talk"
  ]
  node [
    id 33
    label "wypowiada&#263;"
  ]
  node [
    id 34
    label "powiedzie&#263;"
  ]
  node [
    id 35
    label "poleca&#263;"
  ]
  node [
    id 36
    label "pakowa&#263;"
  ]
  node [
    id 37
    label "inflict"
  ]
  node [
    id 38
    label "poleci&#263;"
  ]
  node [
    id 39
    label "zapakowa&#263;"
  ]
  node [
    id 40
    label "odznaka"
  ]
  node [
    id 41
    label "kawaler"
  ]
  node [
    id 42
    label "torment"
  ]
  node [
    id 43
    label "pobi&#263;"
  ]
  node [
    id 44
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 45
    label "beat"
  ]
  node [
    id 46
    label "zbi&#263;"
  ]
  node [
    id 47
    label "pozabija&#263;"
  ]
  node [
    id 48
    label "pousuwa&#263;"
  ]
  node [
    id 49
    label "wpierniczy&#263;"
  ]
  node [
    id 50
    label "pomacha&#263;"
  ]
  node [
    id 51
    label "obi&#263;"
  ]
  node [
    id 52
    label "transgress"
  ]
  node [
    id 53
    label "pokona&#263;"
  ]
  node [
    id 54
    label "poniszczy&#263;"
  ]
  node [
    id 55
    label "upset"
  ]
  node [
    id 56
    label "pouderza&#263;"
  ]
  node [
    id 57
    label "nala&#263;"
  ]
  node [
    id 58
    label "wygra&#263;"
  ]
  node [
    id 59
    label "kuma"
  ]
  node [
    id 60
    label "kumostwo"
  ]
  node [
    id 61
    label "gaworzy&#263;"
  ]
  node [
    id 62
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 63
    label "remark"
  ]
  node [
    id 64
    label "rozmawia&#263;"
  ]
  node [
    id 65
    label "wyra&#380;a&#263;"
  ]
  node [
    id 66
    label "umie&#263;"
  ]
  node [
    id 67
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 68
    label "dziama&#263;"
  ]
  node [
    id 69
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 70
    label "formu&#322;owa&#263;"
  ]
  node [
    id 71
    label "dysfonia"
  ]
  node [
    id 72
    label "express"
  ]
  node [
    id 73
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 74
    label "u&#380;ywa&#263;"
  ]
  node [
    id 75
    label "prawi&#263;"
  ]
  node [
    id 76
    label "powiada&#263;"
  ]
  node [
    id 77
    label "tell"
  ]
  node [
    id 78
    label "chew_the_fat"
  ]
  node [
    id 79
    label "j&#281;zyk"
  ]
  node [
    id 80
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 81
    label "informowa&#263;"
  ]
  node [
    id 82
    label "wydobywa&#263;"
  ]
  node [
    id 83
    label "okre&#347;la&#263;"
  ]
  node [
    id 84
    label "korzysta&#263;"
  ]
  node [
    id 85
    label "distribute"
  ]
  node [
    id 86
    label "give"
  ]
  node [
    id 87
    label "bash"
  ]
  node [
    id 88
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 89
    label "doznawa&#263;"
  ]
  node [
    id 90
    label "decydowa&#263;"
  ]
  node [
    id 91
    label "signify"
  ]
  node [
    id 92
    label "style"
  ]
  node [
    id 93
    label "komunikowa&#263;"
  ]
  node [
    id 94
    label "inform"
  ]
  node [
    id 95
    label "znaczy&#263;"
  ]
  node [
    id 96
    label "give_voice"
  ]
  node [
    id 97
    label "oznacza&#263;"
  ]
  node [
    id 98
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 99
    label "represent"
  ]
  node [
    id 100
    label "convey"
  ]
  node [
    id 101
    label "arouse"
  ]
  node [
    id 102
    label "robi&#263;"
  ]
  node [
    id 103
    label "determine"
  ]
  node [
    id 104
    label "work"
  ]
  node [
    id 105
    label "reakcja_chemiczna"
  ]
  node [
    id 106
    label "uwydatnia&#263;"
  ]
  node [
    id 107
    label "eksploatowa&#263;"
  ]
  node [
    id 108
    label "uzyskiwa&#263;"
  ]
  node [
    id 109
    label "wydostawa&#263;"
  ]
  node [
    id 110
    label "wyjmowa&#263;"
  ]
  node [
    id 111
    label "train"
  ]
  node [
    id 112
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 113
    label "wydawa&#263;"
  ]
  node [
    id 114
    label "dobywa&#263;"
  ]
  node [
    id 115
    label "ocala&#263;"
  ]
  node [
    id 116
    label "excavate"
  ]
  node [
    id 117
    label "g&#243;rnictwo"
  ]
  node [
    id 118
    label "raise"
  ]
  node [
    id 119
    label "wiedzie&#263;"
  ]
  node [
    id 120
    label "can"
  ]
  node [
    id 121
    label "m&#243;c"
  ]
  node [
    id 122
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 123
    label "rozumie&#263;"
  ]
  node [
    id 124
    label "szczeka&#263;"
  ]
  node [
    id 125
    label "funkcjonowa&#263;"
  ]
  node [
    id 126
    label "mawia&#263;"
  ]
  node [
    id 127
    label "opowiada&#263;"
  ]
  node [
    id 128
    label "chatter"
  ]
  node [
    id 129
    label "niemowl&#281;"
  ]
  node [
    id 130
    label "kosmetyk"
  ]
  node [
    id 131
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 132
    label "stanowisko_archeologiczne"
  ]
  node [
    id 133
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 134
    label "artykulator"
  ]
  node [
    id 135
    label "kod"
  ]
  node [
    id 136
    label "kawa&#322;ek"
  ]
  node [
    id 137
    label "przedmiot"
  ]
  node [
    id 138
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 139
    label "gramatyka"
  ]
  node [
    id 140
    label "stylik"
  ]
  node [
    id 141
    label "przet&#322;umaczenie"
  ]
  node [
    id 142
    label "formalizowanie"
  ]
  node [
    id 143
    label "ssa&#263;"
  ]
  node [
    id 144
    label "ssanie"
  ]
  node [
    id 145
    label "language"
  ]
  node [
    id 146
    label "liza&#263;"
  ]
  node [
    id 147
    label "napisa&#263;"
  ]
  node [
    id 148
    label "konsonantyzm"
  ]
  node [
    id 149
    label "wokalizm"
  ]
  node [
    id 150
    label "pisa&#263;"
  ]
  node [
    id 151
    label "fonetyka"
  ]
  node [
    id 152
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 153
    label "jeniec"
  ]
  node [
    id 154
    label "but"
  ]
  node [
    id 155
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 156
    label "po_koroniarsku"
  ]
  node [
    id 157
    label "kultura_duchowa"
  ]
  node [
    id 158
    label "t&#322;umaczenie"
  ]
  node [
    id 159
    label "m&#243;wienie"
  ]
  node [
    id 160
    label "pype&#263;"
  ]
  node [
    id 161
    label "lizanie"
  ]
  node [
    id 162
    label "pismo"
  ]
  node [
    id 163
    label "formalizowa&#263;"
  ]
  node [
    id 164
    label "organ"
  ]
  node [
    id 165
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 166
    label "rozumienie"
  ]
  node [
    id 167
    label "spos&#243;b"
  ]
  node [
    id 168
    label "makroglosja"
  ]
  node [
    id 169
    label "jama_ustna"
  ]
  node [
    id 170
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 171
    label "formacja_geologiczna"
  ]
  node [
    id 172
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 173
    label "natural_language"
  ]
  node [
    id 174
    label "s&#322;ownictwo"
  ]
  node [
    id 175
    label "urz&#261;dzenie"
  ]
  node [
    id 176
    label "dysphonia"
  ]
  node [
    id 177
    label "dysleksja"
  ]
  node [
    id 178
    label "Polish"
  ]
  node [
    id 179
    label "goniony"
  ]
  node [
    id 180
    label "oberek"
  ]
  node [
    id 181
    label "ryba_po_grecku"
  ]
  node [
    id 182
    label "sztajer"
  ]
  node [
    id 183
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 184
    label "krakowiak"
  ]
  node [
    id 185
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 186
    label "pierogi_ruskie"
  ]
  node [
    id 187
    label "lacki"
  ]
  node [
    id 188
    label "polak"
  ]
  node [
    id 189
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 190
    label "chodzony"
  ]
  node [
    id 191
    label "po_polsku"
  ]
  node [
    id 192
    label "mazur"
  ]
  node [
    id 193
    label "polsko"
  ]
  node [
    id 194
    label "skoczny"
  ]
  node [
    id 195
    label "drabant"
  ]
  node [
    id 196
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 197
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 198
    label "wschodnioeuropejski"
  ]
  node [
    id 199
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 200
    label "poga&#324;ski"
  ]
  node [
    id 201
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 202
    label "topielec"
  ]
  node [
    id 203
    label "europejski"
  ]
  node [
    id 204
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 205
    label "langosz"
  ]
  node [
    id 206
    label "zboczenie"
  ]
  node [
    id 207
    label "om&#243;wienie"
  ]
  node [
    id 208
    label "sponiewieranie"
  ]
  node [
    id 209
    label "discipline"
  ]
  node [
    id 210
    label "rzecz"
  ]
  node [
    id 211
    label "omawia&#263;"
  ]
  node [
    id 212
    label "kr&#261;&#380;enie"
  ]
  node [
    id 213
    label "tre&#347;&#263;"
  ]
  node [
    id 214
    label "robienie"
  ]
  node [
    id 215
    label "sponiewiera&#263;"
  ]
  node [
    id 216
    label "element"
  ]
  node [
    id 217
    label "entity"
  ]
  node [
    id 218
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 219
    label "tematyka"
  ]
  node [
    id 220
    label "w&#261;tek"
  ]
  node [
    id 221
    label "charakter"
  ]
  node [
    id 222
    label "zbaczanie"
  ]
  node [
    id 223
    label "program_nauczania"
  ]
  node [
    id 224
    label "om&#243;wi&#263;"
  ]
  node [
    id 225
    label "omawianie"
  ]
  node [
    id 226
    label "thing"
  ]
  node [
    id 227
    label "kultura"
  ]
  node [
    id 228
    label "istota"
  ]
  node [
    id 229
    label "zbacza&#263;"
  ]
  node [
    id 230
    label "zboczy&#263;"
  ]
  node [
    id 231
    label "gwardzista"
  ]
  node [
    id 232
    label "melodia"
  ]
  node [
    id 233
    label "taniec"
  ]
  node [
    id 234
    label "taniec_ludowy"
  ]
  node [
    id 235
    label "&#347;redniowieczny"
  ]
  node [
    id 236
    label "europejsko"
  ]
  node [
    id 237
    label "specjalny"
  ]
  node [
    id 238
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 239
    label "weso&#322;y"
  ]
  node [
    id 240
    label "sprawny"
  ]
  node [
    id 241
    label "rytmiczny"
  ]
  node [
    id 242
    label "skocznie"
  ]
  node [
    id 243
    label "energiczny"
  ]
  node [
    id 244
    label "przytup"
  ]
  node [
    id 245
    label "ho&#322;ubiec"
  ]
  node [
    id 246
    label "wodzi&#263;"
  ]
  node [
    id 247
    label "lendler"
  ]
  node [
    id 248
    label "austriacki"
  ]
  node [
    id 249
    label "polka"
  ]
  node [
    id 250
    label "ludowy"
  ]
  node [
    id 251
    label "pie&#347;&#324;"
  ]
  node [
    id 252
    label "mieszkaniec"
  ]
  node [
    id 253
    label "centu&#347;"
  ]
  node [
    id 254
    label "lalka"
  ]
  node [
    id 255
    label "Ma&#322;opolanin"
  ]
  node [
    id 256
    label "krakauer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
]
