graph [
  node [
    id 0
    label "bitwa"
    origin "text"
  ]
  node [
    id 1
    label "pod"
    origin "text"
  ]
  node [
    id 2
    label "suchodo&#322;em"
    origin "text"
  ]
  node [
    id 3
    label "walka"
  ]
  node [
    id 4
    label "action"
  ]
  node [
    id 5
    label "batalista"
  ]
  node [
    id 6
    label "bitwa_pod_Pi&#322;awcami"
  ]
  node [
    id 7
    label "zaj&#347;cie"
  ]
  node [
    id 8
    label "wydarzenie"
  ]
  node [
    id 9
    label "obrona"
  ]
  node [
    id 10
    label "zaatakowanie"
  ]
  node [
    id 11
    label "konfrontacyjny"
  ]
  node [
    id 12
    label "contest"
  ]
  node [
    id 13
    label "sambo"
  ]
  node [
    id 14
    label "czyn"
  ]
  node [
    id 15
    label "rywalizacja"
  ]
  node [
    id 16
    label "trudno&#347;&#263;"
  ]
  node [
    id 17
    label "sp&#243;r"
  ]
  node [
    id 18
    label "wrestle"
  ]
  node [
    id 19
    label "military_action"
  ]
  node [
    id 20
    label "set"
  ]
  node [
    id 21
    label "ploy"
  ]
  node [
    id 22
    label "doj&#347;cie"
  ]
  node [
    id 23
    label "skrycie_si&#281;"
  ]
  node [
    id 24
    label "odwiedzenie"
  ]
  node [
    id 25
    label "zakrycie"
  ]
  node [
    id 26
    label "happening"
  ]
  node [
    id 27
    label "porobienie_si&#281;"
  ]
  node [
    id 28
    label "krajobraz"
  ]
  node [
    id 29
    label "zaniesienie"
  ]
  node [
    id 30
    label "przyobleczenie_si&#281;"
  ]
  node [
    id 31
    label "stanie_si&#281;"
  ]
  node [
    id 32
    label "event"
  ]
  node [
    id 33
    label "entrance"
  ]
  node [
    id 34
    label "podej&#347;cie"
  ]
  node [
    id 35
    label "przestanie"
  ]
  node [
    id 36
    label "artysta"
  ]
  node [
    id 37
    label "Suchodo&#322;em"
  ]
  node [
    id 38
    label "Klemens"
  ]
  node [
    id 39
    label "zeszyt"
  ]
  node [
    id 40
    label "ruszczy&#263;"
  ]
  node [
    id 41
    label "Przemys&#322;"
  ]
  node [
    id 42
    label "i"
  ]
  node [
    id 43
    label "Boles&#322;awa"
  ]
  node [
    id 44
    label "wstydliwy"
  ]
  node [
    id 45
    label "mieszka&#263;"
  ]
  node [
    id 46
    label "ii"
  ]
  node [
    id 47
    label "oty&#322;y"
  ]
  node [
    id 48
    label "Konrada"
  ]
  node [
    id 49
    label "mazowiecki"
  ]
  node [
    id 50
    label "Henryka"
  ]
  node [
    id 51
    label "pobo&#380;ny"
  ]
  node [
    id 52
    label "Mieszko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 51
  ]
  edge [
    source 46
    target 52
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
]
