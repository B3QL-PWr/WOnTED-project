graph [
  node [
    id 0
    label "dziadowy"
    origin "text"
  ]
  node [
    id 1
    label "k&#322;oda"
    origin "text"
  ]
  node [
    id 2
    label "stacja"
    origin "text"
  ]
  node [
    id 3
    label "kolejowy"
    origin "text"
  ]
  node [
    id 4
    label "kiepski"
  ]
  node [
    id 5
    label "dziadowsko"
  ]
  node [
    id 6
    label "nieumiej&#281;tny"
  ]
  node [
    id 7
    label "marnie"
  ]
  node [
    id 8
    label "z&#322;y"
  ]
  node [
    id 9
    label "niemocny"
  ]
  node [
    id 10
    label "kiepsko"
  ]
  node [
    id 11
    label "dziadowski"
  ]
  node [
    id 12
    label "koszmarnie"
  ]
  node [
    id 13
    label "drewno_okr&#261;g&#322;e"
  ]
  node [
    id 14
    label "pryzma"
  ]
  node [
    id 15
    label "pie&#324;"
  ]
  node [
    id 16
    label "odziomek"
  ]
  node [
    id 17
    label "m&#243;zg"
  ]
  node [
    id 18
    label "morfem"
  ]
  node [
    id 19
    label "s&#322;&#243;j"
  ]
  node [
    id 20
    label "plombowanie"
  ]
  node [
    id 21
    label "drzewo"
  ]
  node [
    id 22
    label "organ_ro&#347;linny"
  ]
  node [
    id 23
    label "element_anatomiczny"
  ]
  node [
    id 24
    label "plombowa&#263;"
  ]
  node [
    id 25
    label "pniak"
  ]
  node [
    id 26
    label "spa&#322;a"
  ]
  node [
    id 27
    label "zaplombowa&#263;"
  ]
  node [
    id 28
    label "zaplombowanie"
  ]
  node [
    id 29
    label "usypisko"
  ]
  node [
    id 30
    label "sze&#347;cian"
  ]
  node [
    id 31
    label "sterta"
  ]
  node [
    id 32
    label "punkt"
  ]
  node [
    id 33
    label "instytucja"
  ]
  node [
    id 34
    label "siedziba"
  ]
  node [
    id 35
    label "miejsce"
  ]
  node [
    id 36
    label "droga_krzy&#380;owa"
  ]
  node [
    id 37
    label "urz&#261;dzenie"
  ]
  node [
    id 38
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 39
    label "przedmiot"
  ]
  node [
    id 40
    label "kom&#243;rka"
  ]
  node [
    id 41
    label "furnishing"
  ]
  node [
    id 42
    label "zabezpieczenie"
  ]
  node [
    id 43
    label "zrobienie"
  ]
  node [
    id 44
    label "wyrz&#261;dzenie"
  ]
  node [
    id 45
    label "zagospodarowanie"
  ]
  node [
    id 46
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 47
    label "ig&#322;a"
  ]
  node [
    id 48
    label "narz&#281;dzie"
  ]
  node [
    id 49
    label "wirnik"
  ]
  node [
    id 50
    label "aparatura"
  ]
  node [
    id 51
    label "system_energetyczny"
  ]
  node [
    id 52
    label "impulsator"
  ]
  node [
    id 53
    label "mechanizm"
  ]
  node [
    id 54
    label "sprz&#281;t"
  ]
  node [
    id 55
    label "czynno&#347;&#263;"
  ]
  node [
    id 56
    label "blokowanie"
  ]
  node [
    id 57
    label "set"
  ]
  node [
    id 58
    label "zablokowanie"
  ]
  node [
    id 59
    label "przygotowanie"
  ]
  node [
    id 60
    label "komora"
  ]
  node [
    id 61
    label "j&#281;zyk"
  ]
  node [
    id 62
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 63
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 64
    label "&#321;ubianka"
  ]
  node [
    id 65
    label "miejsce_pracy"
  ]
  node [
    id 66
    label "dzia&#322;_personalny"
  ]
  node [
    id 67
    label "Kreml"
  ]
  node [
    id 68
    label "Bia&#322;y_Dom"
  ]
  node [
    id 69
    label "budynek"
  ]
  node [
    id 70
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 71
    label "sadowisko"
  ]
  node [
    id 72
    label "po&#322;o&#380;enie"
  ]
  node [
    id 73
    label "sprawa"
  ]
  node [
    id 74
    label "ust&#281;p"
  ]
  node [
    id 75
    label "plan"
  ]
  node [
    id 76
    label "obiekt_matematyczny"
  ]
  node [
    id 77
    label "problemat"
  ]
  node [
    id 78
    label "plamka"
  ]
  node [
    id 79
    label "stopie&#324;_pisma"
  ]
  node [
    id 80
    label "jednostka"
  ]
  node [
    id 81
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 82
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 83
    label "mark"
  ]
  node [
    id 84
    label "chwila"
  ]
  node [
    id 85
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 86
    label "prosta"
  ]
  node [
    id 87
    label "problematyka"
  ]
  node [
    id 88
    label "obiekt"
  ]
  node [
    id 89
    label "zapunktowa&#263;"
  ]
  node [
    id 90
    label "podpunkt"
  ]
  node [
    id 91
    label "wojsko"
  ]
  node [
    id 92
    label "kres"
  ]
  node [
    id 93
    label "przestrze&#324;"
  ]
  node [
    id 94
    label "point"
  ]
  node [
    id 95
    label "pozycja"
  ]
  node [
    id 96
    label "warunek_lokalowy"
  ]
  node [
    id 97
    label "plac"
  ]
  node [
    id 98
    label "location"
  ]
  node [
    id 99
    label "uwaga"
  ]
  node [
    id 100
    label "status"
  ]
  node [
    id 101
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 102
    label "cia&#322;o"
  ]
  node [
    id 103
    label "cecha"
  ]
  node [
    id 104
    label "praca"
  ]
  node [
    id 105
    label "rz&#261;d"
  ]
  node [
    id 106
    label "osoba_prawna"
  ]
  node [
    id 107
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 108
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 109
    label "poj&#281;cie"
  ]
  node [
    id 110
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 111
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 112
    label "biuro"
  ]
  node [
    id 113
    label "organizacja"
  ]
  node [
    id 114
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 115
    label "Fundusze_Unijne"
  ]
  node [
    id 116
    label "zamyka&#263;"
  ]
  node [
    id 117
    label "establishment"
  ]
  node [
    id 118
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 119
    label "urz&#261;d"
  ]
  node [
    id 120
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 121
    label "afiliowa&#263;"
  ]
  node [
    id 122
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 123
    label "standard"
  ]
  node [
    id 124
    label "zamykanie"
  ]
  node [
    id 125
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 126
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 127
    label "komunikacyjny"
  ]
  node [
    id 128
    label "dogodny"
  ]
  node [
    id 129
    label "linia"
  ]
  node [
    id 130
    label "nr"
  ]
  node [
    id 131
    label "317"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 131
  ]
  edge [
    source 130
    target 131
  ]
]
