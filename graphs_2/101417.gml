graph [
  node [
    id 0
    label "villach"
    origin "text"
  ]
  node [
    id 1
    label "hauptbahnhof"
    origin "text"
  ]
  node [
    id 2
    label "Villach"
  ]
  node [
    id 3
    label "Hauptbahnhof"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
]
