graph [
  node [
    id 0
    label "gordon"
    origin "text"
  ]
  node [
    id 1
    label "willis"
    origin "text"
  ]
  node [
    id 2
    label "seter"
  ]
  node [
    id 3
    label "kuropatwiarz"
  ]
  node [
    id 4
    label "pies_legawy"
  ]
  node [
    id 5
    label "Willis"
  ]
  node [
    id 6
    label "nowy"
  ]
  node [
    id 7
    label "Jork"
  ]
  node [
    id 8
    label "ojciec"
  ]
  node [
    id 9
    label "chrzestny"
  ]
  node [
    id 10
    label "iii"
  ]
  node [
    id 11
    label "Woody"
  ]
  node [
    id 12
    label "allen"
  ]
  node [
    id 13
    label "Francis"
  ]
  node [
    id 14
    label "ford"
  ]
  node [
    id 15
    label "Coppola"
  ]
  node [
    id 16
    label "Alan"
  ]
  node [
    id 17
    label "J"
  ]
  node [
    id 18
    label "Pakula"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
]
