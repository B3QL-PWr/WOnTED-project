graph [
  node [
    id 0
    label "jak"
    origin "text"
  ]
  node [
    id 1
    label "wybra&#263;"
    origin "text"
  ]
  node [
    id 2
    label "model"
    origin "text"
  ]
  node [
    id 3
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 4
    label "zobo"
  ]
  node [
    id 5
    label "yakalo"
  ]
  node [
    id 6
    label "byd&#322;o"
  ]
  node [
    id 7
    label "dzo"
  ]
  node [
    id 8
    label "kr&#281;torogie"
  ]
  node [
    id 9
    label "zbi&#243;r"
  ]
  node [
    id 10
    label "g&#322;owa"
  ]
  node [
    id 11
    label "czochrad&#322;o"
  ]
  node [
    id 12
    label "posp&#243;lstwo"
  ]
  node [
    id 13
    label "kraal"
  ]
  node [
    id 14
    label "livestock"
  ]
  node [
    id 15
    label "prze&#380;uwacz"
  ]
  node [
    id 16
    label "zebu"
  ]
  node [
    id 17
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 18
    label "bizon"
  ]
  node [
    id 19
    label "byd&#322;o_domowe"
  ]
  node [
    id 20
    label "powo&#322;a&#263;"
  ]
  node [
    id 21
    label "sie&#263;_rybacka"
  ]
  node [
    id 22
    label "zu&#380;y&#263;"
  ]
  node [
    id 23
    label "wyj&#261;&#263;"
  ]
  node [
    id 24
    label "ustali&#263;"
  ]
  node [
    id 25
    label "distill"
  ]
  node [
    id 26
    label "pick"
  ]
  node [
    id 27
    label "kotwica"
  ]
  node [
    id 28
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 29
    label "wydzieli&#263;"
  ]
  node [
    id 30
    label "wy&#322;&#261;czy&#263;"
  ]
  node [
    id 31
    label "remove"
  ]
  node [
    id 32
    label "spowodowa&#263;"
  ]
  node [
    id 33
    label "consume"
  ]
  node [
    id 34
    label "zrobi&#263;"
  ]
  node [
    id 35
    label "appoint"
  ]
  node [
    id 36
    label "poborowy"
  ]
  node [
    id 37
    label "wskaza&#263;"
  ]
  node [
    id 38
    label "put"
  ]
  node [
    id 39
    label "zdecydowa&#263;"
  ]
  node [
    id 40
    label "bind"
  ]
  node [
    id 41
    label "umocni&#263;"
  ]
  node [
    id 42
    label "unwrap"
  ]
  node [
    id 43
    label "narz&#281;dzie"
  ]
  node [
    id 44
    label "kotwiczy&#263;"
  ]
  node [
    id 45
    label "zakotwiczenie"
  ]
  node [
    id 46
    label "emocja"
  ]
  node [
    id 47
    label "wybieranie"
  ]
  node [
    id 48
    label "wybiera&#263;"
  ]
  node [
    id 49
    label "statek"
  ]
  node [
    id 50
    label "zegar"
  ]
  node [
    id 51
    label "zakotwiczy&#263;"
  ]
  node [
    id 52
    label "wybranie"
  ]
  node [
    id 53
    label "kotwiczenie"
  ]
  node [
    id 54
    label "spos&#243;b"
  ]
  node [
    id 55
    label "cz&#322;owiek"
  ]
  node [
    id 56
    label "prezenter"
  ]
  node [
    id 57
    label "typ"
  ]
  node [
    id 58
    label "mildew"
  ]
  node [
    id 59
    label "zi&#243;&#322;ko"
  ]
  node [
    id 60
    label "motif"
  ]
  node [
    id 61
    label "pozowanie"
  ]
  node [
    id 62
    label "ideal"
  ]
  node [
    id 63
    label "wz&#243;r"
  ]
  node [
    id 64
    label "matryca"
  ]
  node [
    id 65
    label "adaptation"
  ]
  node [
    id 66
    label "ruch"
  ]
  node [
    id 67
    label "pozowa&#263;"
  ]
  node [
    id 68
    label "imitacja"
  ]
  node [
    id 69
    label "orygina&#322;"
  ]
  node [
    id 70
    label "facet"
  ]
  node [
    id 71
    label "miniatura"
  ]
  node [
    id 72
    label "gablotka"
  ]
  node [
    id 73
    label "pokaz"
  ]
  node [
    id 74
    label "szkatu&#322;ka"
  ]
  node [
    id 75
    label "pude&#322;ko"
  ]
  node [
    id 76
    label "bran&#380;owiec"
  ]
  node [
    id 77
    label "prowadz&#261;cy"
  ]
  node [
    id 78
    label "ludzko&#347;&#263;"
  ]
  node [
    id 79
    label "asymilowanie"
  ]
  node [
    id 80
    label "wapniak"
  ]
  node [
    id 81
    label "asymilowa&#263;"
  ]
  node [
    id 82
    label "os&#322;abia&#263;"
  ]
  node [
    id 83
    label "posta&#263;"
  ]
  node [
    id 84
    label "hominid"
  ]
  node [
    id 85
    label "podw&#322;adny"
  ]
  node [
    id 86
    label "os&#322;abianie"
  ]
  node [
    id 87
    label "figura"
  ]
  node [
    id 88
    label "portrecista"
  ]
  node [
    id 89
    label "dwun&#243;g"
  ]
  node [
    id 90
    label "profanum"
  ]
  node [
    id 91
    label "mikrokosmos"
  ]
  node [
    id 92
    label "nasada"
  ]
  node [
    id 93
    label "duch"
  ]
  node [
    id 94
    label "antropochoria"
  ]
  node [
    id 95
    label "osoba"
  ]
  node [
    id 96
    label "senior"
  ]
  node [
    id 97
    label "oddzia&#322;ywanie"
  ]
  node [
    id 98
    label "Adam"
  ]
  node [
    id 99
    label "homo_sapiens"
  ]
  node [
    id 100
    label "polifag"
  ]
  node [
    id 101
    label "kszta&#322;t"
  ]
  node [
    id 102
    label "przedmiot"
  ]
  node [
    id 103
    label "kopia"
  ]
  node [
    id 104
    label "utw&#243;r"
  ]
  node [
    id 105
    label "obraz"
  ]
  node [
    id 106
    label "obiekt"
  ]
  node [
    id 107
    label "ilustracja"
  ]
  node [
    id 108
    label "miniature"
  ]
  node [
    id 109
    label "zapis"
  ]
  node [
    id 110
    label "figure"
  ]
  node [
    id 111
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 112
    label "rule"
  ]
  node [
    id 113
    label "dekal"
  ]
  node [
    id 114
    label "motyw"
  ]
  node [
    id 115
    label "projekt"
  ]
  node [
    id 116
    label "technika"
  ]
  node [
    id 117
    label "praktyka"
  ]
  node [
    id 118
    label "na&#347;ladownictwo"
  ]
  node [
    id 119
    label "tryb"
  ]
  node [
    id 120
    label "nature"
  ]
  node [
    id 121
    label "bratek"
  ]
  node [
    id 122
    label "kod_genetyczny"
  ]
  node [
    id 123
    label "t&#322;ocznik"
  ]
  node [
    id 124
    label "aparat_cyfrowy"
  ]
  node [
    id 125
    label "detector"
  ]
  node [
    id 126
    label "forma"
  ]
  node [
    id 127
    label "jednostka_systematyczna"
  ]
  node [
    id 128
    label "kr&#243;lestwo"
  ]
  node [
    id 129
    label "autorament"
  ]
  node [
    id 130
    label "variety"
  ]
  node [
    id 131
    label "antycypacja"
  ]
  node [
    id 132
    label "przypuszczenie"
  ]
  node [
    id 133
    label "cynk"
  ]
  node [
    id 134
    label "obstawia&#263;"
  ]
  node [
    id 135
    label "gromada"
  ]
  node [
    id 136
    label "sztuka"
  ]
  node [
    id 137
    label "rezultat"
  ]
  node [
    id 138
    label "design"
  ]
  node [
    id 139
    label "sfotografowanie_si&#281;"
  ]
  node [
    id 140
    label "na&#347;ladowanie"
  ]
  node [
    id 141
    label "robienie"
  ]
  node [
    id 142
    label "fotografowanie_si&#281;"
  ]
  node [
    id 143
    label "czynno&#347;&#263;"
  ]
  node [
    id 144
    label "pretense"
  ]
  node [
    id 145
    label "sit"
  ]
  node [
    id 146
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 147
    label "robi&#263;"
  ]
  node [
    id 148
    label "dally"
  ]
  node [
    id 149
    label "mechanika"
  ]
  node [
    id 150
    label "utrzymywanie"
  ]
  node [
    id 151
    label "move"
  ]
  node [
    id 152
    label "poruszenie"
  ]
  node [
    id 153
    label "movement"
  ]
  node [
    id 154
    label "myk"
  ]
  node [
    id 155
    label "utrzyma&#263;"
  ]
  node [
    id 156
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 157
    label "zjawisko"
  ]
  node [
    id 158
    label "utrzymanie"
  ]
  node [
    id 159
    label "travel"
  ]
  node [
    id 160
    label "kanciasty"
  ]
  node [
    id 161
    label "commercial_enterprise"
  ]
  node [
    id 162
    label "strumie&#324;"
  ]
  node [
    id 163
    label "proces"
  ]
  node [
    id 164
    label "aktywno&#347;&#263;"
  ]
  node [
    id 165
    label "kr&#243;tki"
  ]
  node [
    id 166
    label "taktyka"
  ]
  node [
    id 167
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 168
    label "apraksja"
  ]
  node [
    id 169
    label "natural_process"
  ]
  node [
    id 170
    label "utrzymywa&#263;"
  ]
  node [
    id 171
    label "d&#322;ugi"
  ]
  node [
    id 172
    label "wydarzenie"
  ]
  node [
    id 173
    label "dyssypacja_energii"
  ]
  node [
    id 174
    label "tumult"
  ]
  node [
    id 175
    label "stopek"
  ]
  node [
    id 176
    label "zmiana"
  ]
  node [
    id 177
    label "manewr"
  ]
  node [
    id 178
    label "lokomocja"
  ]
  node [
    id 179
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 180
    label "komunikacja"
  ]
  node [
    id 181
    label "drift"
  ]
  node [
    id 182
    label "nicpo&#324;"
  ]
  node [
    id 183
    label "agent"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
]
