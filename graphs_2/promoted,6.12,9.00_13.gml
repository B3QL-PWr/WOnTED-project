graph [
  node [
    id 0
    label "pierwsza"
    origin "text"
  ]
  node [
    id 1
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 2
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 5
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 6
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 7
    label "skierowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "pozew"
    origin "text"
  ]
  node [
    id 9
    label "zbiorowy"
    origin "text"
  ]
  node [
    id 10
    label "przeciwko"
    origin "text"
  ]
  node [
    id 11
    label "skarb"
    origin "text"
  ]
  node [
    id 12
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 13
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 14
    label "ustali&#263;"
    origin "text"
  ]
  node [
    id 15
    label "odpowiedzialno&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "stan"
    origin "text"
  ]
  node [
    id 17
    label "powietrze"
    origin "text"
  ]
  node [
    id 18
    label "polska"
    origin "text"
  ]
  node [
    id 19
    label "godzina"
  ]
  node [
    id 20
    label "time"
  ]
  node [
    id 21
    label "doba"
  ]
  node [
    id 22
    label "p&#243;&#322;godzina"
  ]
  node [
    id 23
    label "jednostka_czasu"
  ]
  node [
    id 24
    label "czas"
  ]
  node [
    id 25
    label "minuta"
  ]
  node [
    id 26
    label "kwadrans"
  ]
  node [
    id 27
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 28
    label "medium"
  ]
  node [
    id 29
    label "poprzedzanie"
  ]
  node [
    id 30
    label "czasoprzestrze&#324;"
  ]
  node [
    id 31
    label "laba"
  ]
  node [
    id 32
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 33
    label "chronometria"
  ]
  node [
    id 34
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 35
    label "rachuba_czasu"
  ]
  node [
    id 36
    label "przep&#322;ywanie"
  ]
  node [
    id 37
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 38
    label "czasokres"
  ]
  node [
    id 39
    label "odczyt"
  ]
  node [
    id 40
    label "chwila"
  ]
  node [
    id 41
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 42
    label "dzieje"
  ]
  node [
    id 43
    label "kategoria_gramatyczna"
  ]
  node [
    id 44
    label "poprzedzenie"
  ]
  node [
    id 45
    label "trawienie"
  ]
  node [
    id 46
    label "pochodzi&#263;"
  ]
  node [
    id 47
    label "period"
  ]
  node [
    id 48
    label "okres_czasu"
  ]
  node [
    id 49
    label "poprzedza&#263;"
  ]
  node [
    id 50
    label "schy&#322;ek"
  ]
  node [
    id 51
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 52
    label "odwlekanie_si&#281;"
  ]
  node [
    id 53
    label "zegar"
  ]
  node [
    id 54
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 55
    label "czwarty_wymiar"
  ]
  node [
    id 56
    label "pochodzenie"
  ]
  node [
    id 57
    label "koniugacja"
  ]
  node [
    id 58
    label "Zeitgeist"
  ]
  node [
    id 59
    label "trawi&#263;"
  ]
  node [
    id 60
    label "pogoda"
  ]
  node [
    id 61
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 62
    label "poprzedzi&#263;"
  ]
  node [
    id 63
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 64
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 65
    label "time_period"
  ]
  node [
    id 66
    label "Rzym_Zachodni"
  ]
  node [
    id 67
    label "whole"
  ]
  node [
    id 68
    label "ilo&#347;&#263;"
  ]
  node [
    id 69
    label "element"
  ]
  node [
    id 70
    label "Rzym_Wschodni"
  ]
  node [
    id 71
    label "urz&#261;dzenie"
  ]
  node [
    id 72
    label "&#347;rodek"
  ]
  node [
    id 73
    label "jasnowidz"
  ]
  node [
    id 74
    label "hipnoza"
  ]
  node [
    id 75
    label "cz&#322;owiek"
  ]
  node [
    id 76
    label "spirytysta"
  ]
  node [
    id 77
    label "otoczenie"
  ]
  node [
    id 78
    label "publikator"
  ]
  node [
    id 79
    label "przekazior"
  ]
  node [
    id 80
    label "warunki"
  ]
  node [
    id 81
    label "strona"
  ]
  node [
    id 82
    label "kolejny"
  ]
  node [
    id 83
    label "nast&#281;pnie"
  ]
  node [
    id 84
    label "inny"
  ]
  node [
    id 85
    label "nastopny"
  ]
  node [
    id 86
    label "kolejno"
  ]
  node [
    id 87
    label "kt&#243;ry&#347;"
  ]
  node [
    id 88
    label "p&#243;&#322;rocze"
  ]
  node [
    id 89
    label "martwy_sezon"
  ]
  node [
    id 90
    label "kalendarz"
  ]
  node [
    id 91
    label "cykl_astronomiczny"
  ]
  node [
    id 92
    label "lata"
  ]
  node [
    id 93
    label "pora_roku"
  ]
  node [
    id 94
    label "stulecie"
  ]
  node [
    id 95
    label "kurs"
  ]
  node [
    id 96
    label "jubileusz"
  ]
  node [
    id 97
    label "grupa"
  ]
  node [
    id 98
    label "kwarta&#322;"
  ]
  node [
    id 99
    label "miesi&#261;c"
  ]
  node [
    id 100
    label "summer"
  ]
  node [
    id 101
    label "odm&#322;adzanie"
  ]
  node [
    id 102
    label "liga"
  ]
  node [
    id 103
    label "jednostka_systematyczna"
  ]
  node [
    id 104
    label "asymilowanie"
  ]
  node [
    id 105
    label "gromada"
  ]
  node [
    id 106
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 107
    label "asymilowa&#263;"
  ]
  node [
    id 108
    label "egzemplarz"
  ]
  node [
    id 109
    label "Entuzjastki"
  ]
  node [
    id 110
    label "zbi&#243;r"
  ]
  node [
    id 111
    label "kompozycja"
  ]
  node [
    id 112
    label "Terranie"
  ]
  node [
    id 113
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 114
    label "category"
  ]
  node [
    id 115
    label "pakiet_klimatyczny"
  ]
  node [
    id 116
    label "oddzia&#322;"
  ]
  node [
    id 117
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 118
    label "cz&#261;steczka"
  ]
  node [
    id 119
    label "stage_set"
  ]
  node [
    id 120
    label "type"
  ]
  node [
    id 121
    label "specgrupa"
  ]
  node [
    id 122
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 123
    label "&#346;wietliki"
  ]
  node [
    id 124
    label "odm&#322;odzenie"
  ]
  node [
    id 125
    label "Eurogrupa"
  ]
  node [
    id 126
    label "odm&#322;adza&#263;"
  ]
  node [
    id 127
    label "formacja_geologiczna"
  ]
  node [
    id 128
    label "harcerze_starsi"
  ]
  node [
    id 129
    label "term"
  ]
  node [
    id 130
    label "rok_akademicki"
  ]
  node [
    id 131
    label "rok_szkolny"
  ]
  node [
    id 132
    label "semester"
  ]
  node [
    id 133
    label "anniwersarz"
  ]
  node [
    id 134
    label "rocznica"
  ]
  node [
    id 135
    label "obszar"
  ]
  node [
    id 136
    label "tydzie&#324;"
  ]
  node [
    id 137
    label "miech"
  ]
  node [
    id 138
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 139
    label "kalendy"
  ]
  node [
    id 140
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 141
    label "long_time"
  ]
  node [
    id 142
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 143
    label "almanac"
  ]
  node [
    id 144
    label "rozk&#322;ad"
  ]
  node [
    id 145
    label "wydawnictwo"
  ]
  node [
    id 146
    label "Juliusz_Cezar"
  ]
  node [
    id 147
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 148
    label "zwy&#380;kowanie"
  ]
  node [
    id 149
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 150
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 151
    label "zaj&#281;cia"
  ]
  node [
    id 152
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 153
    label "trasa"
  ]
  node [
    id 154
    label "przeorientowywanie"
  ]
  node [
    id 155
    label "przejazd"
  ]
  node [
    id 156
    label "kierunek"
  ]
  node [
    id 157
    label "przeorientowywa&#263;"
  ]
  node [
    id 158
    label "nauka"
  ]
  node [
    id 159
    label "przeorientowanie"
  ]
  node [
    id 160
    label "klasa"
  ]
  node [
    id 161
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 162
    label "przeorientowa&#263;"
  ]
  node [
    id 163
    label "manner"
  ]
  node [
    id 164
    label "course"
  ]
  node [
    id 165
    label "passage"
  ]
  node [
    id 166
    label "zni&#380;kowanie"
  ]
  node [
    id 167
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 168
    label "seria"
  ]
  node [
    id 169
    label "stawka"
  ]
  node [
    id 170
    label "way"
  ]
  node [
    id 171
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 172
    label "spos&#243;b"
  ]
  node [
    id 173
    label "deprecjacja"
  ]
  node [
    id 174
    label "cedu&#322;a"
  ]
  node [
    id 175
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 176
    label "drive"
  ]
  node [
    id 177
    label "bearing"
  ]
  node [
    id 178
    label "Lira"
  ]
  node [
    id 179
    label "zesp&#243;&#322;"
  ]
  node [
    id 180
    label "podejrzany"
  ]
  node [
    id 181
    label "s&#261;downictwo"
  ]
  node [
    id 182
    label "system"
  ]
  node [
    id 183
    label "biuro"
  ]
  node [
    id 184
    label "wytw&#243;r"
  ]
  node [
    id 185
    label "court"
  ]
  node [
    id 186
    label "forum"
  ]
  node [
    id 187
    label "bronienie"
  ]
  node [
    id 188
    label "urz&#261;d"
  ]
  node [
    id 189
    label "wydarzenie"
  ]
  node [
    id 190
    label "oskar&#380;yciel"
  ]
  node [
    id 191
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 192
    label "skazany"
  ]
  node [
    id 193
    label "post&#281;powanie"
  ]
  node [
    id 194
    label "broni&#263;"
  ]
  node [
    id 195
    label "my&#347;l"
  ]
  node [
    id 196
    label "pods&#261;dny"
  ]
  node [
    id 197
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 198
    label "obrona"
  ]
  node [
    id 199
    label "wypowied&#378;"
  ]
  node [
    id 200
    label "instytucja"
  ]
  node [
    id 201
    label "antylogizm"
  ]
  node [
    id 202
    label "konektyw"
  ]
  node [
    id 203
    label "&#347;wiadek"
  ]
  node [
    id 204
    label "procesowicz"
  ]
  node [
    id 205
    label "przedmiot"
  ]
  node [
    id 206
    label "p&#322;&#243;d"
  ]
  node [
    id 207
    label "work"
  ]
  node [
    id 208
    label "rezultat"
  ]
  node [
    id 209
    label "Mazowsze"
  ]
  node [
    id 210
    label "skupienie"
  ]
  node [
    id 211
    label "The_Beatles"
  ]
  node [
    id 212
    label "zabudowania"
  ]
  node [
    id 213
    label "group"
  ]
  node [
    id 214
    label "zespolik"
  ]
  node [
    id 215
    label "schorzenie"
  ]
  node [
    id 216
    label "ro&#347;lina"
  ]
  node [
    id 217
    label "Depeche_Mode"
  ]
  node [
    id 218
    label "batch"
  ]
  node [
    id 219
    label "stanowisko"
  ]
  node [
    id 220
    label "position"
  ]
  node [
    id 221
    label "siedziba"
  ]
  node [
    id 222
    label "organ"
  ]
  node [
    id 223
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 224
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 225
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 226
    label "mianowaniec"
  ]
  node [
    id 227
    label "dzia&#322;"
  ]
  node [
    id 228
    label "okienko"
  ]
  node [
    id 229
    label "w&#322;adza"
  ]
  node [
    id 230
    label "przebiec"
  ]
  node [
    id 231
    label "charakter"
  ]
  node [
    id 232
    label "czynno&#347;&#263;"
  ]
  node [
    id 233
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 234
    label "motyw"
  ]
  node [
    id 235
    label "przebiegni&#281;cie"
  ]
  node [
    id 236
    label "fabu&#322;a"
  ]
  node [
    id 237
    label "osoba_prawna"
  ]
  node [
    id 238
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 239
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 240
    label "poj&#281;cie"
  ]
  node [
    id 241
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 242
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 243
    label "organizacja"
  ]
  node [
    id 244
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 245
    label "Fundusze_Unijne"
  ]
  node [
    id 246
    label "zamyka&#263;"
  ]
  node [
    id 247
    label "establishment"
  ]
  node [
    id 248
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 249
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 250
    label "afiliowa&#263;"
  ]
  node [
    id 251
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 252
    label "standard"
  ]
  node [
    id 253
    label "zamykanie"
  ]
  node [
    id 254
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 255
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 256
    label "pos&#322;uchanie"
  ]
  node [
    id 257
    label "sparafrazowanie"
  ]
  node [
    id 258
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 259
    label "strawestowa&#263;"
  ]
  node [
    id 260
    label "sparafrazowa&#263;"
  ]
  node [
    id 261
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 262
    label "trawestowa&#263;"
  ]
  node [
    id 263
    label "sformu&#322;owanie"
  ]
  node [
    id 264
    label "parafrazowanie"
  ]
  node [
    id 265
    label "ozdobnik"
  ]
  node [
    id 266
    label "delimitacja"
  ]
  node [
    id 267
    label "parafrazowa&#263;"
  ]
  node [
    id 268
    label "stylizacja"
  ]
  node [
    id 269
    label "komunikat"
  ]
  node [
    id 270
    label "trawestowanie"
  ]
  node [
    id 271
    label "strawestowanie"
  ]
  node [
    id 272
    label "szko&#322;a"
  ]
  node [
    id 273
    label "thinking"
  ]
  node [
    id 274
    label "umys&#322;"
  ]
  node [
    id 275
    label "political_orientation"
  ]
  node [
    id 276
    label "istota"
  ]
  node [
    id 277
    label "pomys&#322;"
  ]
  node [
    id 278
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 279
    label "idea"
  ]
  node [
    id 280
    label "fantomatyka"
  ]
  node [
    id 281
    label "kognicja"
  ]
  node [
    id 282
    label "campaign"
  ]
  node [
    id 283
    label "rozprawa"
  ]
  node [
    id 284
    label "zachowanie"
  ]
  node [
    id 285
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 286
    label "fashion"
  ]
  node [
    id 287
    label "robienie"
  ]
  node [
    id 288
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 289
    label "zmierzanie"
  ]
  node [
    id 290
    label "przes&#322;anka"
  ]
  node [
    id 291
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 292
    label "kazanie"
  ]
  node [
    id 293
    label "funktor"
  ]
  node [
    id 294
    label "ob&#380;a&#322;owany"
  ]
  node [
    id 295
    label "podejrzanie"
  ]
  node [
    id 296
    label "podmiot"
  ]
  node [
    id 297
    label "pos&#261;dzanie"
  ]
  node [
    id 298
    label "nieprzejrzysty"
  ]
  node [
    id 299
    label "niepewny"
  ]
  node [
    id 300
    label "z&#322;y"
  ]
  node [
    id 301
    label "dysponowanie"
  ]
  node [
    id 302
    label "dysponowa&#263;"
  ]
  node [
    id 303
    label "skar&#380;yciel"
  ]
  node [
    id 304
    label "egzamin"
  ]
  node [
    id 305
    label "walka"
  ]
  node [
    id 306
    label "gracz"
  ]
  node [
    id 307
    label "protection"
  ]
  node [
    id 308
    label "poparcie"
  ]
  node [
    id 309
    label "mecz"
  ]
  node [
    id 310
    label "reakcja"
  ]
  node [
    id 311
    label "defense"
  ]
  node [
    id 312
    label "auspices"
  ]
  node [
    id 313
    label "gra"
  ]
  node [
    id 314
    label "ochrona"
  ]
  node [
    id 315
    label "sp&#243;r"
  ]
  node [
    id 316
    label "wojsko"
  ]
  node [
    id 317
    label "manewr"
  ]
  node [
    id 318
    label "defensive_structure"
  ]
  node [
    id 319
    label "guard_duty"
  ]
  node [
    id 320
    label "uczestnik"
  ]
  node [
    id 321
    label "dru&#380;ba"
  ]
  node [
    id 322
    label "obserwator"
  ]
  node [
    id 323
    label "osoba_fizyczna"
  ]
  node [
    id 324
    label "fend"
  ]
  node [
    id 325
    label "reprezentowa&#263;"
  ]
  node [
    id 326
    label "robi&#263;"
  ]
  node [
    id 327
    label "zdawa&#263;"
  ]
  node [
    id 328
    label "czuwa&#263;"
  ]
  node [
    id 329
    label "preach"
  ]
  node [
    id 330
    label "chroni&#263;"
  ]
  node [
    id 331
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 332
    label "walczy&#263;"
  ]
  node [
    id 333
    label "resist"
  ]
  node [
    id 334
    label "adwokatowa&#263;"
  ]
  node [
    id 335
    label "rebuff"
  ]
  node [
    id 336
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 337
    label "udowadnia&#263;"
  ]
  node [
    id 338
    label "gra&#263;"
  ]
  node [
    id 339
    label "sprawowa&#263;"
  ]
  node [
    id 340
    label "refuse"
  ]
  node [
    id 341
    label "kartka"
  ]
  node [
    id 342
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 343
    label "logowanie"
  ]
  node [
    id 344
    label "plik"
  ]
  node [
    id 345
    label "adres_internetowy"
  ]
  node [
    id 346
    label "linia"
  ]
  node [
    id 347
    label "serwis_internetowy"
  ]
  node [
    id 348
    label "posta&#263;"
  ]
  node [
    id 349
    label "bok"
  ]
  node [
    id 350
    label "skr&#281;canie"
  ]
  node [
    id 351
    label "skr&#281;ca&#263;"
  ]
  node [
    id 352
    label "orientowanie"
  ]
  node [
    id 353
    label "skr&#281;ci&#263;"
  ]
  node [
    id 354
    label "uj&#281;cie"
  ]
  node [
    id 355
    label "zorientowanie"
  ]
  node [
    id 356
    label "ty&#322;"
  ]
  node [
    id 357
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 358
    label "fragment"
  ]
  node [
    id 359
    label "layout"
  ]
  node [
    id 360
    label "obiekt"
  ]
  node [
    id 361
    label "zorientowa&#263;"
  ]
  node [
    id 362
    label "pagina"
  ]
  node [
    id 363
    label "g&#243;ra"
  ]
  node [
    id 364
    label "orientowa&#263;"
  ]
  node [
    id 365
    label "voice"
  ]
  node [
    id 366
    label "orientacja"
  ]
  node [
    id 367
    label "prz&#243;d"
  ]
  node [
    id 368
    label "internet"
  ]
  node [
    id 369
    label "powierzchnia"
  ]
  node [
    id 370
    label "forma"
  ]
  node [
    id 371
    label "skr&#281;cenie"
  ]
  node [
    id 372
    label "niedost&#281;pny"
  ]
  node [
    id 373
    label "obstawanie"
  ]
  node [
    id 374
    label "adwokatowanie"
  ]
  node [
    id 375
    label "zdawanie"
  ]
  node [
    id 376
    label "walczenie"
  ]
  node [
    id 377
    label "zabezpieczenie"
  ]
  node [
    id 378
    label "t&#322;umaczenie"
  ]
  node [
    id 379
    label "parry"
  ]
  node [
    id 380
    label "or&#281;dowanie"
  ]
  node [
    id 381
    label "granie"
  ]
  node [
    id 382
    label "biurko"
  ]
  node [
    id 383
    label "boks"
  ]
  node [
    id 384
    label "palestra"
  ]
  node [
    id 385
    label "Biuro_Lustracyjne"
  ]
  node [
    id 386
    label "agency"
  ]
  node [
    id 387
    label "board"
  ]
  node [
    id 388
    label "pomieszczenie"
  ]
  node [
    id 389
    label "j&#261;dro"
  ]
  node [
    id 390
    label "systemik"
  ]
  node [
    id 391
    label "rozprz&#261;c"
  ]
  node [
    id 392
    label "oprogramowanie"
  ]
  node [
    id 393
    label "systemat"
  ]
  node [
    id 394
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 395
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 396
    label "model"
  ]
  node [
    id 397
    label "struktura"
  ]
  node [
    id 398
    label "usenet"
  ]
  node [
    id 399
    label "porz&#261;dek"
  ]
  node [
    id 400
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 401
    label "przyn&#281;ta"
  ]
  node [
    id 402
    label "net"
  ]
  node [
    id 403
    label "w&#281;dkarstwo"
  ]
  node [
    id 404
    label "eratem"
  ]
  node [
    id 405
    label "doktryna"
  ]
  node [
    id 406
    label "pulpit"
  ]
  node [
    id 407
    label "konstelacja"
  ]
  node [
    id 408
    label "jednostka_geologiczna"
  ]
  node [
    id 409
    label "o&#347;"
  ]
  node [
    id 410
    label "podsystem"
  ]
  node [
    id 411
    label "metoda"
  ]
  node [
    id 412
    label "ryba"
  ]
  node [
    id 413
    label "Leopard"
  ]
  node [
    id 414
    label "Android"
  ]
  node [
    id 415
    label "cybernetyk"
  ]
  node [
    id 416
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 417
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 418
    label "method"
  ]
  node [
    id 419
    label "sk&#322;ad"
  ]
  node [
    id 420
    label "podstawa"
  ]
  node [
    id 421
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 422
    label "relacja_logiczna"
  ]
  node [
    id 423
    label "judiciary"
  ]
  node [
    id 424
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 425
    label "grupa_dyskusyjna"
  ]
  node [
    id 426
    label "plac"
  ]
  node [
    id 427
    label "bazylika"
  ]
  node [
    id 428
    label "przestrze&#324;"
  ]
  node [
    id 429
    label "miejsce"
  ]
  node [
    id 430
    label "portal"
  ]
  node [
    id 431
    label "konferencja"
  ]
  node [
    id 432
    label "agora"
  ]
  node [
    id 433
    label "czyj&#347;"
  ]
  node [
    id 434
    label "m&#261;&#380;"
  ]
  node [
    id 435
    label "prywatny"
  ]
  node [
    id 436
    label "ma&#322;&#380;onek"
  ]
  node [
    id 437
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 438
    label "ch&#322;op"
  ]
  node [
    id 439
    label "pan_m&#322;ody"
  ]
  node [
    id 440
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 441
    label "&#347;lubny"
  ]
  node [
    id 442
    label "pan_domu"
  ]
  node [
    id 443
    label "pan_i_w&#322;adca"
  ]
  node [
    id 444
    label "stary"
  ]
  node [
    id 445
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 446
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 447
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 448
    label "osta&#263;_si&#281;"
  ]
  node [
    id 449
    label "change"
  ]
  node [
    id 450
    label "pozosta&#263;"
  ]
  node [
    id 451
    label "catch"
  ]
  node [
    id 452
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 453
    label "proceed"
  ]
  node [
    id 454
    label "support"
  ]
  node [
    id 455
    label "prze&#380;y&#263;"
  ]
  node [
    id 456
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 457
    label "return"
  ]
  node [
    id 458
    label "set"
  ]
  node [
    id 459
    label "dispatch"
  ]
  node [
    id 460
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 461
    label "przeznaczy&#263;"
  ]
  node [
    id 462
    label "ustawi&#263;"
  ]
  node [
    id 463
    label "wys&#322;a&#263;"
  ]
  node [
    id 464
    label "direct"
  ]
  node [
    id 465
    label "podpowiedzie&#263;"
  ]
  node [
    id 466
    label "precede"
  ]
  node [
    id 467
    label "poprawi&#263;"
  ]
  node [
    id 468
    label "nada&#263;"
  ]
  node [
    id 469
    label "peddle"
  ]
  node [
    id 470
    label "marshal"
  ]
  node [
    id 471
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 472
    label "wyznaczy&#263;"
  ]
  node [
    id 473
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 474
    label "spowodowa&#263;"
  ]
  node [
    id 475
    label "zabezpieczy&#263;"
  ]
  node [
    id 476
    label "umie&#347;ci&#263;"
  ]
  node [
    id 477
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 478
    label "zinterpretowa&#263;"
  ]
  node [
    id 479
    label "wskaza&#263;"
  ]
  node [
    id 480
    label "przyzna&#263;"
  ]
  node [
    id 481
    label "sk&#322;oni&#263;"
  ]
  node [
    id 482
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 483
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 484
    label "zdecydowa&#263;"
  ]
  node [
    id 485
    label "accommodate"
  ]
  node [
    id 486
    label "situate"
  ]
  node [
    id 487
    label "rola"
  ]
  node [
    id 488
    label "sta&#263;_si&#281;"
  ]
  node [
    id 489
    label "appoint"
  ]
  node [
    id 490
    label "zrobi&#263;"
  ]
  node [
    id 491
    label "oblat"
  ]
  node [
    id 492
    label "nakaza&#263;"
  ]
  node [
    id 493
    label "przekaza&#263;"
  ]
  node [
    id 494
    label "ship"
  ]
  node [
    id 495
    label "post"
  ]
  node [
    id 496
    label "line"
  ]
  node [
    id 497
    label "wytworzy&#263;"
  ]
  node [
    id 498
    label "convey"
  ]
  node [
    id 499
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 500
    label "prompt"
  ]
  node [
    id 501
    label "motivate"
  ]
  node [
    id 502
    label "doradzi&#263;"
  ]
  node [
    id 503
    label "powiedzie&#263;"
  ]
  node [
    id 504
    label "pom&#243;c"
  ]
  node [
    id 505
    label "go"
  ]
  node [
    id 506
    label "travel"
  ]
  node [
    id 507
    label "gem"
  ]
  node [
    id 508
    label "runda"
  ]
  node [
    id 509
    label "muzyka"
  ]
  node [
    id 510
    label "zestaw"
  ]
  node [
    id 511
    label "pow&#243;dztwo"
  ]
  node [
    id 512
    label "wniosek"
  ]
  node [
    id 513
    label "pismo"
  ]
  node [
    id 514
    label "prayer"
  ]
  node [
    id 515
    label "twierdzenie"
  ]
  node [
    id 516
    label "propozycja"
  ]
  node [
    id 517
    label "motion"
  ]
  node [
    id 518
    label "wnioskowanie"
  ]
  node [
    id 519
    label "wsp&#243;lny"
  ]
  node [
    id 520
    label "zbiorowo"
  ]
  node [
    id 521
    label "spolny"
  ]
  node [
    id 522
    label "wsp&#243;lnie"
  ]
  node [
    id 523
    label "sp&#243;lny"
  ]
  node [
    id 524
    label "jeden"
  ]
  node [
    id 525
    label "uwsp&#243;lnienie"
  ]
  node [
    id 526
    label "uwsp&#243;lnianie"
  ]
  node [
    id 527
    label "licznie"
  ]
  node [
    id 528
    label "brylant"
  ]
  node [
    id 529
    label "mienie"
  ]
  node [
    id 530
    label "kapita&#322;"
  ]
  node [
    id 531
    label "zboczenie"
  ]
  node [
    id 532
    label "om&#243;wienie"
  ]
  node [
    id 533
    label "sponiewieranie"
  ]
  node [
    id 534
    label "discipline"
  ]
  node [
    id 535
    label "rzecz"
  ]
  node [
    id 536
    label "omawia&#263;"
  ]
  node [
    id 537
    label "kr&#261;&#380;enie"
  ]
  node [
    id 538
    label "tre&#347;&#263;"
  ]
  node [
    id 539
    label "sponiewiera&#263;"
  ]
  node [
    id 540
    label "entity"
  ]
  node [
    id 541
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 542
    label "tematyka"
  ]
  node [
    id 543
    label "w&#261;tek"
  ]
  node [
    id 544
    label "zbaczanie"
  ]
  node [
    id 545
    label "program_nauczania"
  ]
  node [
    id 546
    label "om&#243;wi&#263;"
  ]
  node [
    id 547
    label "omawianie"
  ]
  node [
    id 548
    label "thing"
  ]
  node [
    id 549
    label "kultura"
  ]
  node [
    id 550
    label "zbacza&#263;"
  ]
  node [
    id 551
    label "zboczy&#263;"
  ]
  node [
    id 552
    label "przej&#347;cie"
  ]
  node [
    id 553
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 554
    label "rodowo&#347;&#263;"
  ]
  node [
    id 555
    label "patent"
  ]
  node [
    id 556
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 557
    label "dobra"
  ]
  node [
    id 558
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 559
    label "przej&#347;&#263;"
  ]
  node [
    id 560
    label "possession"
  ]
  node [
    id 561
    label "szlif_diament&#243;w"
  ]
  node [
    id 562
    label "stopie&#324;_pisma"
  ]
  node [
    id 563
    label "diamond"
  ]
  node [
    id 564
    label "talent"
  ]
  node [
    id 565
    label "tworzywo"
  ]
  node [
    id 566
    label "diament"
  ]
  node [
    id 567
    label "szlif_brylantowy"
  ]
  node [
    id 568
    label "absolutorium"
  ]
  node [
    id 569
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 570
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 571
    label "&#347;rodowisko"
  ]
  node [
    id 572
    label "nap&#322;ywanie"
  ]
  node [
    id 573
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 574
    label "zaleta"
  ]
  node [
    id 575
    label "podupada&#263;"
  ]
  node [
    id 576
    label "podupadanie"
  ]
  node [
    id 577
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 578
    label "kwestor"
  ]
  node [
    id 579
    label "zas&#243;b"
  ]
  node [
    id 580
    label "supernadz&#243;r"
  ]
  node [
    id 581
    label "uruchomienie"
  ]
  node [
    id 582
    label "uruchamia&#263;"
  ]
  node [
    id 583
    label "kapitalista"
  ]
  node [
    id 584
    label "uruchamianie"
  ]
  node [
    id 585
    label "czynnik_produkcji"
  ]
  node [
    id 586
    label "ludzko&#347;&#263;"
  ]
  node [
    id 587
    label "wapniak"
  ]
  node [
    id 588
    label "os&#322;abia&#263;"
  ]
  node [
    id 589
    label "hominid"
  ]
  node [
    id 590
    label "podw&#322;adny"
  ]
  node [
    id 591
    label "os&#322;abianie"
  ]
  node [
    id 592
    label "g&#322;owa"
  ]
  node [
    id 593
    label "figura"
  ]
  node [
    id 594
    label "portrecista"
  ]
  node [
    id 595
    label "dwun&#243;g"
  ]
  node [
    id 596
    label "profanum"
  ]
  node [
    id 597
    label "mikrokosmos"
  ]
  node [
    id 598
    label "nasada"
  ]
  node [
    id 599
    label "duch"
  ]
  node [
    id 600
    label "antropochoria"
  ]
  node [
    id 601
    label "osoba"
  ]
  node [
    id 602
    label "wz&#243;r"
  ]
  node [
    id 603
    label "senior"
  ]
  node [
    id 604
    label "oddzia&#322;ywanie"
  ]
  node [
    id 605
    label "Adam"
  ]
  node [
    id 606
    label "homo_sapiens"
  ]
  node [
    id 607
    label "polifag"
  ]
  node [
    id 608
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 609
    label "Katar"
  ]
  node [
    id 610
    label "Libia"
  ]
  node [
    id 611
    label "Gwatemala"
  ]
  node [
    id 612
    label "Ekwador"
  ]
  node [
    id 613
    label "Afganistan"
  ]
  node [
    id 614
    label "Tad&#380;ykistan"
  ]
  node [
    id 615
    label "Bhutan"
  ]
  node [
    id 616
    label "Argentyna"
  ]
  node [
    id 617
    label "D&#380;ibuti"
  ]
  node [
    id 618
    label "Wenezuela"
  ]
  node [
    id 619
    label "Gabon"
  ]
  node [
    id 620
    label "Ukraina"
  ]
  node [
    id 621
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 622
    label "Rwanda"
  ]
  node [
    id 623
    label "Liechtenstein"
  ]
  node [
    id 624
    label "Sri_Lanka"
  ]
  node [
    id 625
    label "Madagaskar"
  ]
  node [
    id 626
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 627
    label "Kongo"
  ]
  node [
    id 628
    label "Tonga"
  ]
  node [
    id 629
    label "Bangladesz"
  ]
  node [
    id 630
    label "Kanada"
  ]
  node [
    id 631
    label "Wehrlen"
  ]
  node [
    id 632
    label "Algieria"
  ]
  node [
    id 633
    label "Uganda"
  ]
  node [
    id 634
    label "Surinam"
  ]
  node [
    id 635
    label "Sahara_Zachodnia"
  ]
  node [
    id 636
    label "Chile"
  ]
  node [
    id 637
    label "W&#281;gry"
  ]
  node [
    id 638
    label "Birma"
  ]
  node [
    id 639
    label "Kazachstan"
  ]
  node [
    id 640
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 641
    label "Armenia"
  ]
  node [
    id 642
    label "Tuwalu"
  ]
  node [
    id 643
    label "Timor_Wschodni"
  ]
  node [
    id 644
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 645
    label "Izrael"
  ]
  node [
    id 646
    label "Estonia"
  ]
  node [
    id 647
    label "Komory"
  ]
  node [
    id 648
    label "Kamerun"
  ]
  node [
    id 649
    label "Haiti"
  ]
  node [
    id 650
    label "Belize"
  ]
  node [
    id 651
    label "Sierra_Leone"
  ]
  node [
    id 652
    label "Luksemburg"
  ]
  node [
    id 653
    label "USA"
  ]
  node [
    id 654
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 655
    label "Barbados"
  ]
  node [
    id 656
    label "San_Marino"
  ]
  node [
    id 657
    label "Bu&#322;garia"
  ]
  node [
    id 658
    label "Indonezja"
  ]
  node [
    id 659
    label "Wietnam"
  ]
  node [
    id 660
    label "Malawi"
  ]
  node [
    id 661
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 662
    label "Francja"
  ]
  node [
    id 663
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 664
    label "partia"
  ]
  node [
    id 665
    label "Zambia"
  ]
  node [
    id 666
    label "Angola"
  ]
  node [
    id 667
    label "Grenada"
  ]
  node [
    id 668
    label "Nepal"
  ]
  node [
    id 669
    label "Panama"
  ]
  node [
    id 670
    label "Rumunia"
  ]
  node [
    id 671
    label "Czarnog&#243;ra"
  ]
  node [
    id 672
    label "Malediwy"
  ]
  node [
    id 673
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 674
    label "S&#322;owacja"
  ]
  node [
    id 675
    label "para"
  ]
  node [
    id 676
    label "Egipt"
  ]
  node [
    id 677
    label "zwrot"
  ]
  node [
    id 678
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 679
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 680
    label "Mozambik"
  ]
  node [
    id 681
    label "Kolumbia"
  ]
  node [
    id 682
    label "Laos"
  ]
  node [
    id 683
    label "Burundi"
  ]
  node [
    id 684
    label "Suazi"
  ]
  node [
    id 685
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 686
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 687
    label "Czechy"
  ]
  node [
    id 688
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 689
    label "Wyspy_Marshalla"
  ]
  node [
    id 690
    label "Dominika"
  ]
  node [
    id 691
    label "Trynidad_i_Tobago"
  ]
  node [
    id 692
    label "Syria"
  ]
  node [
    id 693
    label "Palau"
  ]
  node [
    id 694
    label "Gwinea_Bissau"
  ]
  node [
    id 695
    label "Liberia"
  ]
  node [
    id 696
    label "Jamajka"
  ]
  node [
    id 697
    label "Zimbabwe"
  ]
  node [
    id 698
    label "Polska"
  ]
  node [
    id 699
    label "Dominikana"
  ]
  node [
    id 700
    label "Senegal"
  ]
  node [
    id 701
    label "Togo"
  ]
  node [
    id 702
    label "Gujana"
  ]
  node [
    id 703
    label "Gruzja"
  ]
  node [
    id 704
    label "Albania"
  ]
  node [
    id 705
    label "Zair"
  ]
  node [
    id 706
    label "Meksyk"
  ]
  node [
    id 707
    label "Macedonia"
  ]
  node [
    id 708
    label "Chorwacja"
  ]
  node [
    id 709
    label "Kambod&#380;a"
  ]
  node [
    id 710
    label "Monako"
  ]
  node [
    id 711
    label "Mauritius"
  ]
  node [
    id 712
    label "Gwinea"
  ]
  node [
    id 713
    label "Mali"
  ]
  node [
    id 714
    label "Nigeria"
  ]
  node [
    id 715
    label "Kostaryka"
  ]
  node [
    id 716
    label "Hanower"
  ]
  node [
    id 717
    label "Paragwaj"
  ]
  node [
    id 718
    label "W&#322;ochy"
  ]
  node [
    id 719
    label "Seszele"
  ]
  node [
    id 720
    label "Wyspy_Salomona"
  ]
  node [
    id 721
    label "Hiszpania"
  ]
  node [
    id 722
    label "Boliwia"
  ]
  node [
    id 723
    label "Kirgistan"
  ]
  node [
    id 724
    label "Irlandia"
  ]
  node [
    id 725
    label "Czad"
  ]
  node [
    id 726
    label "Irak"
  ]
  node [
    id 727
    label "Lesoto"
  ]
  node [
    id 728
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 729
    label "Malta"
  ]
  node [
    id 730
    label "Andora"
  ]
  node [
    id 731
    label "Chiny"
  ]
  node [
    id 732
    label "Filipiny"
  ]
  node [
    id 733
    label "Antarktis"
  ]
  node [
    id 734
    label "Niemcy"
  ]
  node [
    id 735
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 736
    label "Pakistan"
  ]
  node [
    id 737
    label "terytorium"
  ]
  node [
    id 738
    label "Nikaragua"
  ]
  node [
    id 739
    label "Brazylia"
  ]
  node [
    id 740
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 741
    label "Maroko"
  ]
  node [
    id 742
    label "Portugalia"
  ]
  node [
    id 743
    label "Niger"
  ]
  node [
    id 744
    label "Kenia"
  ]
  node [
    id 745
    label "Botswana"
  ]
  node [
    id 746
    label "Fid&#380;i"
  ]
  node [
    id 747
    label "Tunezja"
  ]
  node [
    id 748
    label "Australia"
  ]
  node [
    id 749
    label "Tajlandia"
  ]
  node [
    id 750
    label "Burkina_Faso"
  ]
  node [
    id 751
    label "interior"
  ]
  node [
    id 752
    label "Tanzania"
  ]
  node [
    id 753
    label "Benin"
  ]
  node [
    id 754
    label "Indie"
  ]
  node [
    id 755
    label "&#321;otwa"
  ]
  node [
    id 756
    label "Kiribati"
  ]
  node [
    id 757
    label "Antigua_i_Barbuda"
  ]
  node [
    id 758
    label "Rodezja"
  ]
  node [
    id 759
    label "Cypr"
  ]
  node [
    id 760
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 761
    label "Peru"
  ]
  node [
    id 762
    label "Austria"
  ]
  node [
    id 763
    label "Urugwaj"
  ]
  node [
    id 764
    label "Jordania"
  ]
  node [
    id 765
    label "Grecja"
  ]
  node [
    id 766
    label "Azerbejd&#380;an"
  ]
  node [
    id 767
    label "Turcja"
  ]
  node [
    id 768
    label "Samoa"
  ]
  node [
    id 769
    label "Sudan"
  ]
  node [
    id 770
    label "Oman"
  ]
  node [
    id 771
    label "ziemia"
  ]
  node [
    id 772
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 773
    label "Uzbekistan"
  ]
  node [
    id 774
    label "Portoryko"
  ]
  node [
    id 775
    label "Honduras"
  ]
  node [
    id 776
    label "Mongolia"
  ]
  node [
    id 777
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 778
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 779
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 780
    label "Serbia"
  ]
  node [
    id 781
    label "Tajwan"
  ]
  node [
    id 782
    label "Wielka_Brytania"
  ]
  node [
    id 783
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 784
    label "Liban"
  ]
  node [
    id 785
    label "Japonia"
  ]
  node [
    id 786
    label "Ghana"
  ]
  node [
    id 787
    label "Belgia"
  ]
  node [
    id 788
    label "Bahrajn"
  ]
  node [
    id 789
    label "Mikronezja"
  ]
  node [
    id 790
    label "Etiopia"
  ]
  node [
    id 791
    label "Kuwejt"
  ]
  node [
    id 792
    label "Bahamy"
  ]
  node [
    id 793
    label "Rosja"
  ]
  node [
    id 794
    label "Mo&#322;dawia"
  ]
  node [
    id 795
    label "Litwa"
  ]
  node [
    id 796
    label "S&#322;owenia"
  ]
  node [
    id 797
    label "Szwajcaria"
  ]
  node [
    id 798
    label "Erytrea"
  ]
  node [
    id 799
    label "Arabia_Saudyjska"
  ]
  node [
    id 800
    label "Kuba"
  ]
  node [
    id 801
    label "granica_pa&#324;stwa"
  ]
  node [
    id 802
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 803
    label "Malezja"
  ]
  node [
    id 804
    label "Korea"
  ]
  node [
    id 805
    label "Jemen"
  ]
  node [
    id 806
    label "Nowa_Zelandia"
  ]
  node [
    id 807
    label "Namibia"
  ]
  node [
    id 808
    label "Nauru"
  ]
  node [
    id 809
    label "holoarktyka"
  ]
  node [
    id 810
    label "Brunei"
  ]
  node [
    id 811
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 812
    label "Khitai"
  ]
  node [
    id 813
    label "Mauretania"
  ]
  node [
    id 814
    label "Iran"
  ]
  node [
    id 815
    label "Gambia"
  ]
  node [
    id 816
    label "Somalia"
  ]
  node [
    id 817
    label "Holandia"
  ]
  node [
    id 818
    label "Turkmenistan"
  ]
  node [
    id 819
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 820
    label "Salwador"
  ]
  node [
    id 821
    label "pair"
  ]
  node [
    id 822
    label "odparowywanie"
  ]
  node [
    id 823
    label "gaz_cieplarniany"
  ]
  node [
    id 824
    label "chodzi&#263;"
  ]
  node [
    id 825
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 826
    label "poker"
  ]
  node [
    id 827
    label "moneta"
  ]
  node [
    id 828
    label "parowanie"
  ]
  node [
    id 829
    label "damp"
  ]
  node [
    id 830
    label "nale&#380;e&#263;"
  ]
  node [
    id 831
    label "sztuka"
  ]
  node [
    id 832
    label "odparowanie"
  ]
  node [
    id 833
    label "odparowa&#263;"
  ]
  node [
    id 834
    label "dodatek"
  ]
  node [
    id 835
    label "jednostka_monetarna"
  ]
  node [
    id 836
    label "smoke"
  ]
  node [
    id 837
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 838
    label "odparowywa&#263;"
  ]
  node [
    id 839
    label "uk&#322;ad"
  ]
  node [
    id 840
    label "gaz"
  ]
  node [
    id 841
    label "wyparowanie"
  ]
  node [
    id 842
    label "Wile&#324;szczyzna"
  ]
  node [
    id 843
    label "jednostka_administracyjna"
  ]
  node [
    id 844
    label "Jukon"
  ]
  node [
    id 845
    label "jednostka_organizacyjna"
  ]
  node [
    id 846
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 847
    label "TOPR"
  ]
  node [
    id 848
    label "endecki"
  ]
  node [
    id 849
    label "od&#322;am"
  ]
  node [
    id 850
    label "przedstawicielstwo"
  ]
  node [
    id 851
    label "Cepelia"
  ]
  node [
    id 852
    label "ZBoWiD"
  ]
  node [
    id 853
    label "organization"
  ]
  node [
    id 854
    label "centrala"
  ]
  node [
    id 855
    label "GOPR"
  ]
  node [
    id 856
    label "ZOMO"
  ]
  node [
    id 857
    label "ZMP"
  ]
  node [
    id 858
    label "komitet_koordynacyjny"
  ]
  node [
    id 859
    label "przybud&#243;wka"
  ]
  node [
    id 860
    label "boj&#243;wka"
  ]
  node [
    id 861
    label "punkt"
  ]
  node [
    id 862
    label "turn"
  ]
  node [
    id 863
    label "turning"
  ]
  node [
    id 864
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 865
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 866
    label "skr&#281;t"
  ]
  node [
    id 867
    label "obr&#243;t"
  ]
  node [
    id 868
    label "fraza_czasownikowa"
  ]
  node [
    id 869
    label "jednostka_leksykalna"
  ]
  node [
    id 870
    label "zmiana"
  ]
  node [
    id 871
    label "wyra&#380;enie"
  ]
  node [
    id 872
    label "Bund"
  ]
  node [
    id 873
    label "PPR"
  ]
  node [
    id 874
    label "wybranek"
  ]
  node [
    id 875
    label "Jakobici"
  ]
  node [
    id 876
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 877
    label "SLD"
  ]
  node [
    id 878
    label "Razem"
  ]
  node [
    id 879
    label "PiS"
  ]
  node [
    id 880
    label "package"
  ]
  node [
    id 881
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 882
    label "Kuomintang"
  ]
  node [
    id 883
    label "ZSL"
  ]
  node [
    id 884
    label "AWS"
  ]
  node [
    id 885
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 886
    label "game"
  ]
  node [
    id 887
    label "blok"
  ]
  node [
    id 888
    label "materia&#322;"
  ]
  node [
    id 889
    label "PO"
  ]
  node [
    id 890
    label "si&#322;a"
  ]
  node [
    id 891
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 892
    label "niedoczas"
  ]
  node [
    id 893
    label "Federali&#347;ci"
  ]
  node [
    id 894
    label "PSL"
  ]
  node [
    id 895
    label "Wigowie"
  ]
  node [
    id 896
    label "ZChN"
  ]
  node [
    id 897
    label "egzekutywa"
  ]
  node [
    id 898
    label "aktyw"
  ]
  node [
    id 899
    label "wybranka"
  ]
  node [
    id 900
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 901
    label "unit"
  ]
  node [
    id 902
    label "biom"
  ]
  node [
    id 903
    label "szata_ro&#347;linna"
  ]
  node [
    id 904
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 905
    label "formacja_ro&#347;linna"
  ]
  node [
    id 906
    label "przyroda"
  ]
  node [
    id 907
    label "zielono&#347;&#263;"
  ]
  node [
    id 908
    label "pi&#281;tro"
  ]
  node [
    id 909
    label "plant"
  ]
  node [
    id 910
    label "geosystem"
  ]
  node [
    id 911
    label "teren"
  ]
  node [
    id 912
    label "Kaszmir"
  ]
  node [
    id 913
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 914
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 915
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 916
    label "Pend&#380;ab"
  ]
  node [
    id 917
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 918
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 919
    label "funt_liba&#324;ski"
  ]
  node [
    id 920
    label "strefa_euro"
  ]
  node [
    id 921
    label "Pozna&#324;"
  ]
  node [
    id 922
    label "lira_malta&#324;ska"
  ]
  node [
    id 923
    label "Gozo"
  ]
  node [
    id 924
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 925
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 926
    label "Afryka_Zachodnia"
  ]
  node [
    id 927
    label "Afryka_Wschodnia"
  ]
  node [
    id 928
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 929
    label "dolar_namibijski"
  ]
  node [
    id 930
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 931
    label "milrejs"
  ]
  node [
    id 932
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 933
    label "NATO"
  ]
  node [
    id 934
    label "escudo_portugalskie"
  ]
  node [
    id 935
    label "dolar_bahamski"
  ]
  node [
    id 936
    label "Wielka_Bahama"
  ]
  node [
    id 937
    label "Karaiby"
  ]
  node [
    id 938
    label "dolar_liberyjski"
  ]
  node [
    id 939
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 940
    label "riel"
  ]
  node [
    id 941
    label "Karelia"
  ]
  node [
    id 942
    label "Mari_El"
  ]
  node [
    id 943
    label "Inguszetia"
  ]
  node [
    id 944
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 945
    label "Udmurcja"
  ]
  node [
    id 946
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 947
    label "Newa"
  ]
  node [
    id 948
    label "&#321;adoga"
  ]
  node [
    id 949
    label "Czeczenia"
  ]
  node [
    id 950
    label "Anadyr"
  ]
  node [
    id 951
    label "Syberia"
  ]
  node [
    id 952
    label "Tatarstan"
  ]
  node [
    id 953
    label "Wszechrosja"
  ]
  node [
    id 954
    label "Azja"
  ]
  node [
    id 955
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 956
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 957
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 958
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 959
    label "Europa_Wschodnia"
  ]
  node [
    id 960
    label "Baszkiria"
  ]
  node [
    id 961
    label "Kamczatka"
  ]
  node [
    id 962
    label "Jama&#322;"
  ]
  node [
    id 963
    label "Dagestan"
  ]
  node [
    id 964
    label "Witim"
  ]
  node [
    id 965
    label "Tuwa"
  ]
  node [
    id 966
    label "car"
  ]
  node [
    id 967
    label "Komi"
  ]
  node [
    id 968
    label "Czuwaszja"
  ]
  node [
    id 969
    label "Chakasja"
  ]
  node [
    id 970
    label "Perm"
  ]
  node [
    id 971
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 972
    label "Ajon"
  ]
  node [
    id 973
    label "Adygeja"
  ]
  node [
    id 974
    label "Dniepr"
  ]
  node [
    id 975
    label "rubel_rosyjski"
  ]
  node [
    id 976
    label "Don"
  ]
  node [
    id 977
    label "Mordowia"
  ]
  node [
    id 978
    label "s&#322;owianofilstwo"
  ]
  node [
    id 979
    label "lew"
  ]
  node [
    id 980
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 981
    label "Dobrud&#380;a"
  ]
  node [
    id 982
    label "Unia_Europejska"
  ]
  node [
    id 983
    label "lira_izraelska"
  ]
  node [
    id 984
    label "szekel"
  ]
  node [
    id 985
    label "Galilea"
  ]
  node [
    id 986
    label "Judea"
  ]
  node [
    id 987
    label "Luksemburgia"
  ]
  node [
    id 988
    label "frank_belgijski"
  ]
  node [
    id 989
    label "Limburgia"
  ]
  node [
    id 990
    label "Brabancja"
  ]
  node [
    id 991
    label "Walonia"
  ]
  node [
    id 992
    label "Flandria"
  ]
  node [
    id 993
    label "Niderlandy"
  ]
  node [
    id 994
    label "dinar_iracki"
  ]
  node [
    id 995
    label "Maghreb"
  ]
  node [
    id 996
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 997
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 998
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 999
    label "szyling_ugandyjski"
  ]
  node [
    id 1000
    label "kafar"
  ]
  node [
    id 1001
    label "dolar_jamajski"
  ]
  node [
    id 1002
    label "ringgit"
  ]
  node [
    id 1003
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1004
    label "Borneo"
  ]
  node [
    id 1005
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1006
    label "dolar_surinamski"
  ]
  node [
    id 1007
    label "funt_suda&#324;ski"
  ]
  node [
    id 1008
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 1009
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1010
    label "Manica"
  ]
  node [
    id 1011
    label "escudo_mozambickie"
  ]
  node [
    id 1012
    label "Cabo_Delgado"
  ]
  node [
    id 1013
    label "Inhambane"
  ]
  node [
    id 1014
    label "Maputo"
  ]
  node [
    id 1015
    label "Gaza"
  ]
  node [
    id 1016
    label "Niasa"
  ]
  node [
    id 1017
    label "Nampula"
  ]
  node [
    id 1018
    label "metical"
  ]
  node [
    id 1019
    label "Sahara"
  ]
  node [
    id 1020
    label "inti"
  ]
  node [
    id 1021
    label "sol"
  ]
  node [
    id 1022
    label "kip"
  ]
  node [
    id 1023
    label "Pireneje"
  ]
  node [
    id 1024
    label "euro"
  ]
  node [
    id 1025
    label "kwacha_zambijska"
  ]
  node [
    id 1026
    label "Buriaci"
  ]
  node [
    id 1027
    label "Azja_Wschodnia"
  ]
  node [
    id 1028
    label "tugrik"
  ]
  node [
    id 1029
    label "ajmak"
  ]
  node [
    id 1030
    label "balboa"
  ]
  node [
    id 1031
    label "Ameryka_Centralna"
  ]
  node [
    id 1032
    label "dolar"
  ]
  node [
    id 1033
    label "gulden"
  ]
  node [
    id 1034
    label "Zelandia"
  ]
  node [
    id 1035
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1036
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 1037
    label "Polinezja"
  ]
  node [
    id 1038
    label "dolar_Tuvalu"
  ]
  node [
    id 1039
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 1040
    label "zair"
  ]
  node [
    id 1041
    label "Katanga"
  ]
  node [
    id 1042
    label "Europa_Zachodnia"
  ]
  node [
    id 1043
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 1044
    label "frank_szwajcarski"
  ]
  node [
    id 1045
    label "Jukatan"
  ]
  node [
    id 1046
    label "dolar_Belize"
  ]
  node [
    id 1047
    label "colon"
  ]
  node [
    id 1048
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 1049
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 1050
    label "Dyja"
  ]
  node [
    id 1051
    label "korona_czeska"
  ]
  node [
    id 1052
    label "Izera"
  ]
  node [
    id 1053
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 1054
    label "Lasko"
  ]
  node [
    id 1055
    label "ugija"
  ]
  node [
    id 1056
    label "szyling_kenijski"
  ]
  node [
    id 1057
    label "Nachiczewan"
  ]
  node [
    id 1058
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 1059
    label "manat_azerski"
  ]
  node [
    id 1060
    label "Karabach"
  ]
  node [
    id 1061
    label "Bengal"
  ]
  node [
    id 1062
    label "taka"
  ]
  node [
    id 1063
    label "Ocean_Spokojny"
  ]
  node [
    id 1064
    label "dolar_Kiribati"
  ]
  node [
    id 1065
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1066
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1067
    label "Cebu"
  ]
  node [
    id 1068
    label "Atlantyk"
  ]
  node [
    id 1069
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1070
    label "Ulster"
  ]
  node [
    id 1071
    label "funt_irlandzki"
  ]
  node [
    id 1072
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 1073
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1074
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 1075
    label "cedi"
  ]
  node [
    id 1076
    label "ariary"
  ]
  node [
    id 1077
    label "Ocean_Indyjski"
  ]
  node [
    id 1078
    label "frank_malgaski"
  ]
  node [
    id 1079
    label "Estremadura"
  ]
  node [
    id 1080
    label "Andaluzja"
  ]
  node [
    id 1081
    label "Kastylia"
  ]
  node [
    id 1082
    label "Galicja"
  ]
  node [
    id 1083
    label "Aragonia"
  ]
  node [
    id 1084
    label "hacjender"
  ]
  node [
    id 1085
    label "Asturia"
  ]
  node [
    id 1086
    label "Baskonia"
  ]
  node [
    id 1087
    label "Majorka"
  ]
  node [
    id 1088
    label "Walencja"
  ]
  node [
    id 1089
    label "peseta"
  ]
  node [
    id 1090
    label "Katalonia"
  ]
  node [
    id 1091
    label "peso_chilijskie"
  ]
  node [
    id 1092
    label "Indie_Zachodnie"
  ]
  node [
    id 1093
    label "Sikkim"
  ]
  node [
    id 1094
    label "Asam"
  ]
  node [
    id 1095
    label "rupia_indyjska"
  ]
  node [
    id 1096
    label "Indie_Portugalskie"
  ]
  node [
    id 1097
    label "Indie_Wschodnie"
  ]
  node [
    id 1098
    label "Kerala"
  ]
  node [
    id 1099
    label "Bollywood"
  ]
  node [
    id 1100
    label "jen"
  ]
  node [
    id 1101
    label "jinja"
  ]
  node [
    id 1102
    label "Okinawa"
  ]
  node [
    id 1103
    label "Japonica"
  ]
  node [
    id 1104
    label "Rugia"
  ]
  node [
    id 1105
    label "Saksonia"
  ]
  node [
    id 1106
    label "Dolna_Saksonia"
  ]
  node [
    id 1107
    label "Anglosas"
  ]
  node [
    id 1108
    label "Hesja"
  ]
  node [
    id 1109
    label "Szlezwik"
  ]
  node [
    id 1110
    label "Wirtembergia"
  ]
  node [
    id 1111
    label "Po&#322;abie"
  ]
  node [
    id 1112
    label "Germania"
  ]
  node [
    id 1113
    label "Frankonia"
  ]
  node [
    id 1114
    label "Badenia"
  ]
  node [
    id 1115
    label "Holsztyn"
  ]
  node [
    id 1116
    label "Bawaria"
  ]
  node [
    id 1117
    label "marka"
  ]
  node [
    id 1118
    label "Szwabia"
  ]
  node [
    id 1119
    label "Brandenburgia"
  ]
  node [
    id 1120
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1121
    label "Nadrenia"
  ]
  node [
    id 1122
    label "Westfalia"
  ]
  node [
    id 1123
    label "Turyngia"
  ]
  node [
    id 1124
    label "Helgoland"
  ]
  node [
    id 1125
    label "Karlsbad"
  ]
  node [
    id 1126
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1127
    label "Piemont"
  ]
  node [
    id 1128
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1129
    label "Italia"
  ]
  node [
    id 1130
    label "Kalabria"
  ]
  node [
    id 1131
    label "Sardynia"
  ]
  node [
    id 1132
    label "Apulia"
  ]
  node [
    id 1133
    label "Ok&#281;cie"
  ]
  node [
    id 1134
    label "Karyntia"
  ]
  node [
    id 1135
    label "Umbria"
  ]
  node [
    id 1136
    label "Romania"
  ]
  node [
    id 1137
    label "Sycylia"
  ]
  node [
    id 1138
    label "Warszawa"
  ]
  node [
    id 1139
    label "lir"
  ]
  node [
    id 1140
    label "Toskania"
  ]
  node [
    id 1141
    label "Lombardia"
  ]
  node [
    id 1142
    label "Liguria"
  ]
  node [
    id 1143
    label "Kampania"
  ]
  node [
    id 1144
    label "Dacja"
  ]
  node [
    id 1145
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1146
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1147
    label "Ba&#322;kany"
  ]
  node [
    id 1148
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1149
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 1150
    label "funt_syryjski"
  ]
  node [
    id 1151
    label "alawizm"
  ]
  node [
    id 1152
    label "frank_rwandyjski"
  ]
  node [
    id 1153
    label "dinar_Bahrajnu"
  ]
  node [
    id 1154
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1155
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1156
    label "frank_luksemburski"
  ]
  node [
    id 1157
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1158
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1159
    label "frank_monakijski"
  ]
  node [
    id 1160
    label "dinar_algierski"
  ]
  node [
    id 1161
    label "Kabylia"
  ]
  node [
    id 1162
    label "Oceania"
  ]
  node [
    id 1163
    label "Wojwodina"
  ]
  node [
    id 1164
    label "Sand&#380;ak"
  ]
  node [
    id 1165
    label "dinar_serbski"
  ]
  node [
    id 1166
    label "boliwar"
  ]
  node [
    id 1167
    label "Orinoko"
  ]
  node [
    id 1168
    label "tenge"
  ]
  node [
    id 1169
    label "lek"
  ]
  node [
    id 1170
    label "frank_alba&#324;ski"
  ]
  node [
    id 1171
    label "dolar_Barbadosu"
  ]
  node [
    id 1172
    label "Antyle"
  ]
  node [
    id 1173
    label "kyat"
  ]
  node [
    id 1174
    label "Arakan"
  ]
  node [
    id 1175
    label "c&#243;rdoba"
  ]
  node [
    id 1176
    label "Paros"
  ]
  node [
    id 1177
    label "Epir"
  ]
  node [
    id 1178
    label "panhellenizm"
  ]
  node [
    id 1179
    label "Eubea"
  ]
  node [
    id 1180
    label "Rodos"
  ]
  node [
    id 1181
    label "Achaja"
  ]
  node [
    id 1182
    label "Termopile"
  ]
  node [
    id 1183
    label "Attyka"
  ]
  node [
    id 1184
    label "Hellada"
  ]
  node [
    id 1185
    label "Etolia"
  ]
  node [
    id 1186
    label "Kreta"
  ]
  node [
    id 1187
    label "drachma"
  ]
  node [
    id 1188
    label "Olimp"
  ]
  node [
    id 1189
    label "Tesalia"
  ]
  node [
    id 1190
    label "Peloponez"
  ]
  node [
    id 1191
    label "Eolia"
  ]
  node [
    id 1192
    label "Beocja"
  ]
  node [
    id 1193
    label "Parnas"
  ]
  node [
    id 1194
    label "Lesbos"
  ]
  node [
    id 1195
    label "Mariany"
  ]
  node [
    id 1196
    label "Salzburg"
  ]
  node [
    id 1197
    label "Rakuzy"
  ]
  node [
    id 1198
    label "Tyrol"
  ]
  node [
    id 1199
    label "konsulent"
  ]
  node [
    id 1200
    label "szyling_austryjacki"
  ]
  node [
    id 1201
    label "Amhara"
  ]
  node [
    id 1202
    label "birr"
  ]
  node [
    id 1203
    label "Syjon"
  ]
  node [
    id 1204
    label "negus"
  ]
  node [
    id 1205
    label "Jawa"
  ]
  node [
    id 1206
    label "Sumatra"
  ]
  node [
    id 1207
    label "rupia_indonezyjska"
  ]
  node [
    id 1208
    label "Nowa_Gwinea"
  ]
  node [
    id 1209
    label "Moluki"
  ]
  node [
    id 1210
    label "boliviano"
  ]
  node [
    id 1211
    label "Lotaryngia"
  ]
  node [
    id 1212
    label "Bordeaux"
  ]
  node [
    id 1213
    label "Pikardia"
  ]
  node [
    id 1214
    label "Alzacja"
  ]
  node [
    id 1215
    label "Masyw_Centralny"
  ]
  node [
    id 1216
    label "Akwitania"
  ]
  node [
    id 1217
    label "Sekwana"
  ]
  node [
    id 1218
    label "Langwedocja"
  ]
  node [
    id 1219
    label "Armagnac"
  ]
  node [
    id 1220
    label "Martynika"
  ]
  node [
    id 1221
    label "Bretania"
  ]
  node [
    id 1222
    label "Sabaudia"
  ]
  node [
    id 1223
    label "Korsyka"
  ]
  node [
    id 1224
    label "Normandia"
  ]
  node [
    id 1225
    label "Gaskonia"
  ]
  node [
    id 1226
    label "Burgundia"
  ]
  node [
    id 1227
    label "frank_francuski"
  ]
  node [
    id 1228
    label "Wandea"
  ]
  node [
    id 1229
    label "Prowansja"
  ]
  node [
    id 1230
    label "Gwadelupa"
  ]
  node [
    id 1231
    label "somoni"
  ]
  node [
    id 1232
    label "Melanezja"
  ]
  node [
    id 1233
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1234
    label "funt_cypryjski"
  ]
  node [
    id 1235
    label "Afrodyzje"
  ]
  node [
    id 1236
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1237
    label "Fryburg"
  ]
  node [
    id 1238
    label "Bazylea"
  ]
  node [
    id 1239
    label "Alpy"
  ]
  node [
    id 1240
    label "Helwecja"
  ]
  node [
    id 1241
    label "Berno"
  ]
  node [
    id 1242
    label "sum"
  ]
  node [
    id 1243
    label "Karaka&#322;pacja"
  ]
  node [
    id 1244
    label "Kurlandia"
  ]
  node [
    id 1245
    label "Windawa"
  ]
  node [
    id 1246
    label "&#322;at"
  ]
  node [
    id 1247
    label "Liwonia"
  ]
  node [
    id 1248
    label "rubel_&#322;otewski"
  ]
  node [
    id 1249
    label "Inflanty"
  ]
  node [
    id 1250
    label "&#379;mud&#378;"
  ]
  node [
    id 1251
    label "lit"
  ]
  node [
    id 1252
    label "frank_tunezyjski"
  ]
  node [
    id 1253
    label "dinar_tunezyjski"
  ]
  node [
    id 1254
    label "lempira"
  ]
  node [
    id 1255
    label "korona_w&#281;gierska"
  ]
  node [
    id 1256
    label "forint"
  ]
  node [
    id 1257
    label "Lipt&#243;w"
  ]
  node [
    id 1258
    label "dong"
  ]
  node [
    id 1259
    label "Annam"
  ]
  node [
    id 1260
    label "Tonkin"
  ]
  node [
    id 1261
    label "lud"
  ]
  node [
    id 1262
    label "frank_kongijski"
  ]
  node [
    id 1263
    label "szyling_somalijski"
  ]
  node [
    id 1264
    label "cruzado"
  ]
  node [
    id 1265
    label "real"
  ]
  node [
    id 1266
    label "Podole"
  ]
  node [
    id 1267
    label "Ukraina_Zachodnia"
  ]
  node [
    id 1268
    label "Wsch&#243;d"
  ]
  node [
    id 1269
    label "Zakarpacie"
  ]
  node [
    id 1270
    label "Naddnieprze"
  ]
  node [
    id 1271
    label "Ma&#322;orosja"
  ]
  node [
    id 1272
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1273
    label "Nadbu&#380;e"
  ]
  node [
    id 1274
    label "hrywna"
  ]
  node [
    id 1275
    label "Zaporo&#380;e"
  ]
  node [
    id 1276
    label "Krym"
  ]
  node [
    id 1277
    label "Dniestr"
  ]
  node [
    id 1278
    label "Przykarpacie"
  ]
  node [
    id 1279
    label "Kozaczyzna"
  ]
  node [
    id 1280
    label "karbowaniec"
  ]
  node [
    id 1281
    label "Tasmania"
  ]
  node [
    id 1282
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1283
    label "dolar_australijski"
  ]
  node [
    id 1284
    label "gourde"
  ]
  node [
    id 1285
    label "escudo_angolskie"
  ]
  node [
    id 1286
    label "kwanza"
  ]
  node [
    id 1287
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1288
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 1289
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1290
    label "Ad&#380;aria"
  ]
  node [
    id 1291
    label "lari"
  ]
  node [
    id 1292
    label "naira"
  ]
  node [
    id 1293
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1294
    label "Ohio"
  ]
  node [
    id 1295
    label "P&#243;&#322;noc"
  ]
  node [
    id 1296
    label "Nowy_York"
  ]
  node [
    id 1297
    label "Illinois"
  ]
  node [
    id 1298
    label "Po&#322;udnie"
  ]
  node [
    id 1299
    label "Kalifornia"
  ]
  node [
    id 1300
    label "Wirginia"
  ]
  node [
    id 1301
    label "Teksas"
  ]
  node [
    id 1302
    label "Waszyngton"
  ]
  node [
    id 1303
    label "zielona_karta"
  ]
  node [
    id 1304
    label "Alaska"
  ]
  node [
    id 1305
    label "Massachusetts"
  ]
  node [
    id 1306
    label "Hawaje"
  ]
  node [
    id 1307
    label "Maryland"
  ]
  node [
    id 1308
    label "Michigan"
  ]
  node [
    id 1309
    label "Arizona"
  ]
  node [
    id 1310
    label "Georgia"
  ]
  node [
    id 1311
    label "stan_wolny"
  ]
  node [
    id 1312
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1313
    label "Pensylwania"
  ]
  node [
    id 1314
    label "Luizjana"
  ]
  node [
    id 1315
    label "Nowy_Meksyk"
  ]
  node [
    id 1316
    label "Wuj_Sam"
  ]
  node [
    id 1317
    label "Alabama"
  ]
  node [
    id 1318
    label "Kansas"
  ]
  node [
    id 1319
    label "Oregon"
  ]
  node [
    id 1320
    label "Zach&#243;d"
  ]
  node [
    id 1321
    label "Oklahoma"
  ]
  node [
    id 1322
    label "Floryda"
  ]
  node [
    id 1323
    label "Hudson"
  ]
  node [
    id 1324
    label "som"
  ]
  node [
    id 1325
    label "peso_urugwajskie"
  ]
  node [
    id 1326
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1327
    label "dolar_Brunei"
  ]
  node [
    id 1328
    label "rial_ira&#324;ski"
  ]
  node [
    id 1329
    label "mu&#322;&#322;a"
  ]
  node [
    id 1330
    label "Persja"
  ]
  node [
    id 1331
    label "d&#380;amahirijja"
  ]
  node [
    id 1332
    label "dinar_libijski"
  ]
  node [
    id 1333
    label "nakfa"
  ]
  node [
    id 1334
    label "rial_katarski"
  ]
  node [
    id 1335
    label "quetzal"
  ]
  node [
    id 1336
    label "won"
  ]
  node [
    id 1337
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1338
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1339
    label "guarani"
  ]
  node [
    id 1340
    label "perper"
  ]
  node [
    id 1341
    label "dinar_kuwejcki"
  ]
  node [
    id 1342
    label "dalasi"
  ]
  node [
    id 1343
    label "dolar_Zimbabwe"
  ]
  node [
    id 1344
    label "Szantung"
  ]
  node [
    id 1345
    label "Chiny_Zachodnie"
  ]
  node [
    id 1346
    label "Kuantung"
  ]
  node [
    id 1347
    label "D&#380;ungaria"
  ]
  node [
    id 1348
    label "yuan"
  ]
  node [
    id 1349
    label "Hongkong"
  ]
  node [
    id 1350
    label "Chiny_Wschodnie"
  ]
  node [
    id 1351
    label "Guangdong"
  ]
  node [
    id 1352
    label "Junnan"
  ]
  node [
    id 1353
    label "Mand&#380;uria"
  ]
  node [
    id 1354
    label "Syczuan"
  ]
  node [
    id 1355
    label "Pa&#322;uki"
  ]
  node [
    id 1356
    label "Pomorze_Zachodnie"
  ]
  node [
    id 1357
    label "Powi&#347;le"
  ]
  node [
    id 1358
    label "Wolin"
  ]
  node [
    id 1359
    label "z&#322;oty"
  ]
  node [
    id 1360
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 1361
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 1362
    label "So&#322;a"
  ]
  node [
    id 1363
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 1364
    label "Opolskie"
  ]
  node [
    id 1365
    label "Suwalszczyzna"
  ]
  node [
    id 1366
    label "Krajna"
  ]
  node [
    id 1367
    label "barwy_polskie"
  ]
  node [
    id 1368
    label "Podlasie"
  ]
  node [
    id 1369
    label "Ma&#322;opolska"
  ]
  node [
    id 1370
    label "Warmia"
  ]
  node [
    id 1371
    label "Mazury"
  ]
  node [
    id 1372
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1373
    label "Lubelszczyzna"
  ]
  node [
    id 1374
    label "Kaczawa"
  ]
  node [
    id 1375
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1376
    label "Kielecczyzna"
  ]
  node [
    id 1377
    label "Lubuskie"
  ]
  node [
    id 1378
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 1379
    label "&#321;&#243;dzkie"
  ]
  node [
    id 1380
    label "Kujawy"
  ]
  node [
    id 1381
    label "Podkarpacie"
  ]
  node [
    id 1382
    label "Wielkopolska"
  ]
  node [
    id 1383
    label "Wis&#322;a"
  ]
  node [
    id 1384
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1385
    label "Bory_Tucholskie"
  ]
  node [
    id 1386
    label "Ujgur"
  ]
  node [
    id 1387
    label "Azja_Mniejsza"
  ]
  node [
    id 1388
    label "lira_turecka"
  ]
  node [
    id 1389
    label "kuna"
  ]
  node [
    id 1390
    label "dram"
  ]
  node [
    id 1391
    label "tala"
  ]
  node [
    id 1392
    label "korona_s&#322;owacka"
  ]
  node [
    id 1393
    label "Turiec"
  ]
  node [
    id 1394
    label "Himalaje"
  ]
  node [
    id 1395
    label "rupia_nepalska"
  ]
  node [
    id 1396
    label "frank_gwinejski"
  ]
  node [
    id 1397
    label "korona_esto&#324;ska"
  ]
  node [
    id 1398
    label "Skandynawia"
  ]
  node [
    id 1399
    label "marka_esto&#324;ska"
  ]
  node [
    id 1400
    label "Quebec"
  ]
  node [
    id 1401
    label "dolar_kanadyjski"
  ]
  node [
    id 1402
    label "Nowa_Fundlandia"
  ]
  node [
    id 1403
    label "Zanzibar"
  ]
  node [
    id 1404
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1405
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1406
    label "&#346;wite&#378;"
  ]
  node [
    id 1407
    label "peso_kolumbijskie"
  ]
  node [
    id 1408
    label "Synaj"
  ]
  node [
    id 1409
    label "paraszyt"
  ]
  node [
    id 1410
    label "funt_egipski"
  ]
  node [
    id 1411
    label "szach"
  ]
  node [
    id 1412
    label "Baktria"
  ]
  node [
    id 1413
    label "afgani"
  ]
  node [
    id 1414
    label "baht"
  ]
  node [
    id 1415
    label "tolar"
  ]
  node [
    id 1416
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1417
    label "Naddniestrze"
  ]
  node [
    id 1418
    label "Gagauzja"
  ]
  node [
    id 1419
    label "Anglia"
  ]
  node [
    id 1420
    label "Amazonia"
  ]
  node [
    id 1421
    label "plantowa&#263;"
  ]
  node [
    id 1422
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 1423
    label "zapadnia"
  ]
  node [
    id 1424
    label "Zamojszczyzna"
  ]
  node [
    id 1425
    label "budynek"
  ]
  node [
    id 1426
    label "skorupa_ziemska"
  ]
  node [
    id 1427
    label "Turkiestan"
  ]
  node [
    id 1428
    label "Noworosja"
  ]
  node [
    id 1429
    label "Mezoameryka"
  ]
  node [
    id 1430
    label "glinowanie"
  ]
  node [
    id 1431
    label "Kurdystan"
  ]
  node [
    id 1432
    label "martwica"
  ]
  node [
    id 1433
    label "Szkocja"
  ]
  node [
    id 1434
    label "litosfera"
  ]
  node [
    id 1435
    label "penetrator"
  ]
  node [
    id 1436
    label "glinowa&#263;"
  ]
  node [
    id 1437
    label "Zabajkale"
  ]
  node [
    id 1438
    label "domain"
  ]
  node [
    id 1439
    label "Bojkowszczyzna"
  ]
  node [
    id 1440
    label "podglebie"
  ]
  node [
    id 1441
    label "kompleks_sorpcyjny"
  ]
  node [
    id 1442
    label "Pamir"
  ]
  node [
    id 1443
    label "Indochiny"
  ]
  node [
    id 1444
    label "Kurpie"
  ]
  node [
    id 1445
    label "S&#261;decczyzna"
  ]
  node [
    id 1446
    label "kort"
  ]
  node [
    id 1447
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 1448
    label "Huculszczyzna"
  ]
  node [
    id 1449
    label "pojazd"
  ]
  node [
    id 1450
    label "Podhale"
  ]
  node [
    id 1451
    label "pr&#243;chnica"
  ]
  node [
    id 1452
    label "Hercegowina"
  ]
  node [
    id 1453
    label "Walia"
  ]
  node [
    id 1454
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 1455
    label "ryzosfera"
  ]
  node [
    id 1456
    label "Kaukaz"
  ]
  node [
    id 1457
    label "Biskupizna"
  ]
  node [
    id 1458
    label "Bo&#347;nia"
  ]
  node [
    id 1459
    label "p&#322;aszczyzna"
  ]
  node [
    id 1460
    label "dotleni&#263;"
  ]
  node [
    id 1461
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 1462
    label "Opolszczyzna"
  ]
  node [
    id 1463
    label "&#321;emkowszczyzna"
  ]
  node [
    id 1464
    label "Podbeskidzie"
  ]
  node [
    id 1465
    label "Kaszuby"
  ]
  node [
    id 1466
    label "Ko&#322;yma"
  ]
  node [
    id 1467
    label "glej"
  ]
  node [
    id 1468
    label "posadzka"
  ]
  node [
    id 1469
    label "Polesie"
  ]
  node [
    id 1470
    label "Palestyna"
  ]
  node [
    id 1471
    label "Lauda"
  ]
  node [
    id 1472
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 1473
    label "Laponia"
  ]
  node [
    id 1474
    label "Yorkshire"
  ]
  node [
    id 1475
    label "Zag&#243;rze"
  ]
  node [
    id 1476
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 1477
    label "&#379;ywiecczyzna"
  ]
  node [
    id 1478
    label "Oksytania"
  ]
  node [
    id 1479
    label "Kociewie"
  ]
  node [
    id 1480
    label "put"
  ]
  node [
    id 1481
    label "bind"
  ]
  node [
    id 1482
    label "umocni&#263;"
  ]
  node [
    id 1483
    label "unwrap"
  ]
  node [
    id 1484
    label "podnie&#347;&#263;"
  ]
  node [
    id 1485
    label "umocnienie"
  ]
  node [
    id 1486
    label "utrwali&#263;"
  ]
  node [
    id 1487
    label "fixate"
  ]
  node [
    id 1488
    label "wzmocni&#263;"
  ]
  node [
    id 1489
    label "ustabilizowa&#263;"
  ]
  node [
    id 1490
    label "zmieni&#263;"
  ]
  node [
    id 1491
    label "act"
  ]
  node [
    id 1492
    label "podj&#261;&#263;"
  ]
  node [
    id 1493
    label "decide"
  ]
  node [
    id 1494
    label "determine"
  ]
  node [
    id 1495
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1496
    label "post&#261;pi&#263;"
  ]
  node [
    id 1497
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1498
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1499
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1500
    label "zorganizowa&#263;"
  ]
  node [
    id 1501
    label "wystylizowa&#263;"
  ]
  node [
    id 1502
    label "cause"
  ]
  node [
    id 1503
    label "przerobi&#263;"
  ]
  node [
    id 1504
    label "nabra&#263;"
  ]
  node [
    id 1505
    label "make"
  ]
  node [
    id 1506
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1507
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1508
    label "wydali&#263;"
  ]
  node [
    id 1509
    label "konsekwencja"
  ]
  node [
    id 1510
    label "wstyd"
  ]
  node [
    id 1511
    label "obarczy&#263;"
  ]
  node [
    id 1512
    label "obowi&#261;zek"
  ]
  node [
    id 1513
    label "liability"
  ]
  node [
    id 1514
    label "cecha"
  ]
  node [
    id 1515
    label "guilt"
  ]
  node [
    id 1516
    label "duty"
  ]
  node [
    id 1517
    label "wym&#243;g"
  ]
  node [
    id 1518
    label "powinno&#347;&#263;"
  ]
  node [
    id 1519
    label "zadanie"
  ]
  node [
    id 1520
    label "odczuwa&#263;"
  ]
  node [
    id 1521
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 1522
    label "skrupienie_si&#281;"
  ]
  node [
    id 1523
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 1524
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 1525
    label "odczucie"
  ]
  node [
    id 1526
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 1527
    label "koszula_Dejaniry"
  ]
  node [
    id 1528
    label "odczuwanie"
  ]
  node [
    id 1529
    label "event"
  ]
  node [
    id 1530
    label "skrupianie_si&#281;"
  ]
  node [
    id 1531
    label "odczu&#263;"
  ]
  node [
    id 1532
    label "charakterystyka"
  ]
  node [
    id 1533
    label "m&#322;ot"
  ]
  node [
    id 1534
    label "znak"
  ]
  node [
    id 1535
    label "drzewo"
  ]
  node [
    id 1536
    label "pr&#243;ba"
  ]
  node [
    id 1537
    label "attribute"
  ]
  node [
    id 1538
    label "oskar&#380;y&#263;"
  ]
  node [
    id 1539
    label "blame"
  ]
  node [
    id 1540
    label "charge"
  ]
  node [
    id 1541
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 1542
    label "load"
  ]
  node [
    id 1543
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 1544
    label "srom"
  ]
  node [
    id 1545
    label "emocja"
  ]
  node [
    id 1546
    label "dishonor"
  ]
  node [
    id 1547
    label "wina"
  ]
  node [
    id 1548
    label "konfuzja"
  ]
  node [
    id 1549
    label "shame"
  ]
  node [
    id 1550
    label "g&#322;upio"
  ]
  node [
    id 1551
    label "wci&#281;cie"
  ]
  node [
    id 1552
    label "warstwa"
  ]
  node [
    id 1553
    label "samopoczucie"
  ]
  node [
    id 1554
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1555
    label "state"
  ]
  node [
    id 1556
    label "wektor"
  ]
  node [
    id 1557
    label "by&#263;"
  ]
  node [
    id 1558
    label "Goa"
  ]
  node [
    id 1559
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1560
    label "poziom"
  ]
  node [
    id 1561
    label "shape"
  ]
  node [
    id 1562
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1563
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 1564
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 1565
    label "indentation"
  ]
  node [
    id 1566
    label "zjedzenie"
  ]
  node [
    id 1567
    label "snub"
  ]
  node [
    id 1568
    label "warunek_lokalowy"
  ]
  node [
    id 1569
    label "location"
  ]
  node [
    id 1570
    label "uwaga"
  ]
  node [
    id 1571
    label "status"
  ]
  node [
    id 1572
    label "cia&#322;o"
  ]
  node [
    id 1573
    label "praca"
  ]
  node [
    id 1574
    label "rz&#261;d"
  ]
  node [
    id 1575
    label "sk&#322;adnik"
  ]
  node [
    id 1576
    label "sytuacja"
  ]
  node [
    id 1577
    label "przek&#322;adaniec"
  ]
  node [
    id 1578
    label "covering"
  ]
  node [
    id 1579
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1580
    label "podwarstwa"
  ]
  node [
    id 1581
    label "dyspozycja"
  ]
  node [
    id 1582
    label "organizm"
  ]
  node [
    id 1583
    label "obiekt_matematyczny"
  ]
  node [
    id 1584
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1585
    label "zwrot_wektora"
  ]
  node [
    id 1586
    label "vector"
  ]
  node [
    id 1587
    label "parametryzacja"
  ]
  node [
    id 1588
    label "kwas_deoksyrybonukleinowy"
  ]
  node [
    id 1589
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1590
    label "sprawa"
  ]
  node [
    id 1591
    label "ust&#281;p"
  ]
  node [
    id 1592
    label "plan"
  ]
  node [
    id 1593
    label "problemat"
  ]
  node [
    id 1594
    label "plamka"
  ]
  node [
    id 1595
    label "jednostka"
  ]
  node [
    id 1596
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1597
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1598
    label "mark"
  ]
  node [
    id 1599
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1600
    label "prosta"
  ]
  node [
    id 1601
    label "problematyka"
  ]
  node [
    id 1602
    label "zapunktowa&#263;"
  ]
  node [
    id 1603
    label "podpunkt"
  ]
  node [
    id 1604
    label "kres"
  ]
  node [
    id 1605
    label "point"
  ]
  node [
    id 1606
    label "pozycja"
  ]
  node [
    id 1607
    label "jako&#347;&#263;"
  ]
  node [
    id 1608
    label "punkt_widzenia"
  ]
  node [
    id 1609
    label "wyk&#322;adnik"
  ]
  node [
    id 1610
    label "faza"
  ]
  node [
    id 1611
    label "szczebel"
  ]
  node [
    id 1612
    label "wysoko&#347;&#263;"
  ]
  node [
    id 1613
    label "ranga"
  ]
  node [
    id 1614
    label "rozmiar"
  ]
  node [
    id 1615
    label "part"
  ]
  node [
    id 1616
    label "Aleuty"
  ]
  node [
    id 1617
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1618
    label "mie&#263;_miejsce"
  ]
  node [
    id 1619
    label "equal"
  ]
  node [
    id 1620
    label "trwa&#263;"
  ]
  node [
    id 1621
    label "si&#281;ga&#263;"
  ]
  node [
    id 1622
    label "obecno&#347;&#263;"
  ]
  node [
    id 1623
    label "stand"
  ]
  node [
    id 1624
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1625
    label "uczestniczy&#263;"
  ]
  node [
    id 1626
    label "dmuchni&#281;cie"
  ]
  node [
    id 1627
    label "eter"
  ]
  node [
    id 1628
    label "dmuchn&#261;&#263;"
  ]
  node [
    id 1629
    label "breeze"
  ]
  node [
    id 1630
    label "mieszanina"
  ]
  node [
    id 1631
    label "front"
  ]
  node [
    id 1632
    label "napowietrzy&#263;"
  ]
  node [
    id 1633
    label "pneumatyczny"
  ]
  node [
    id 1634
    label "przewietrza&#263;"
  ]
  node [
    id 1635
    label "tlen"
  ]
  node [
    id 1636
    label "wydychanie"
  ]
  node [
    id 1637
    label "dmuchanie"
  ]
  node [
    id 1638
    label "wdychanie"
  ]
  node [
    id 1639
    label "przewietrzy&#263;"
  ]
  node [
    id 1640
    label "luft"
  ]
  node [
    id 1641
    label "dmucha&#263;"
  ]
  node [
    id 1642
    label "podgrzew"
  ]
  node [
    id 1643
    label "wydycha&#263;"
  ]
  node [
    id 1644
    label "wdycha&#263;"
  ]
  node [
    id 1645
    label "przewietrzanie"
  ]
  node [
    id 1646
    label "&#380;ywio&#322;"
  ]
  node [
    id 1647
    label "przewietrzenie"
  ]
  node [
    id 1648
    label "energia"
  ]
  node [
    id 1649
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1650
    label "materia"
  ]
  node [
    id 1651
    label "zajawka"
  ]
  node [
    id 1652
    label "class"
  ]
  node [
    id 1653
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 1654
    label "feblik"
  ]
  node [
    id 1655
    label "wedrze&#263;_si&#281;"
  ]
  node [
    id 1656
    label "wdarcie_si&#281;"
  ]
  node [
    id 1657
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1658
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1659
    label "wdziera&#263;_si&#281;"
  ]
  node [
    id 1660
    label "tendency"
  ]
  node [
    id 1661
    label "huczek"
  ]
  node [
    id 1662
    label "frakcja"
  ]
  node [
    id 1663
    label "substancja"
  ]
  node [
    id 1664
    label "synteza"
  ]
  node [
    id 1665
    label "tlenowiec"
  ]
  node [
    id 1666
    label "niemetal"
  ]
  node [
    id 1667
    label "paramagnetyk"
  ]
  node [
    id 1668
    label "przyducha"
  ]
  node [
    id 1669
    label "makroelement"
  ]
  node [
    id 1670
    label "hipoksja"
  ]
  node [
    id 1671
    label "anoksja"
  ]
  node [
    id 1672
    label "niedotlenienie"
  ]
  node [
    id 1673
    label "piec"
  ]
  node [
    id 1674
    label "przew&#243;d"
  ]
  node [
    id 1675
    label "komin"
  ]
  node [
    id 1676
    label "etyl"
  ]
  node [
    id 1677
    label "ciecz"
  ]
  node [
    id 1678
    label "rozpuszczalnik"
  ]
  node [
    id 1679
    label "ether"
  ]
  node [
    id 1680
    label "gleba"
  ]
  node [
    id 1681
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 1682
    label "fauna"
  ]
  node [
    id 1683
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 1684
    label "pneumatycznie"
  ]
  node [
    id 1685
    label "pneumatic"
  ]
  node [
    id 1686
    label "wypuszczenie"
  ]
  node [
    id 1687
    label "powianie"
  ]
  node [
    id 1688
    label "breath"
  ]
  node [
    id 1689
    label "zaliczanie"
  ]
  node [
    id 1690
    label "whiff"
  ]
  node [
    id 1691
    label "ukradzenie"
  ]
  node [
    id 1692
    label "uciekni&#281;cie"
  ]
  node [
    id 1693
    label "wzi&#281;cie"
  ]
  node [
    id 1694
    label "oddychanie"
  ]
  node [
    id 1695
    label "wydalanie"
  ]
  node [
    id 1696
    label "odholowa&#263;"
  ]
  node [
    id 1697
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 1698
    label "tabor"
  ]
  node [
    id 1699
    label "przyholowywanie"
  ]
  node [
    id 1700
    label "przyholowa&#263;"
  ]
  node [
    id 1701
    label "przyholowanie"
  ]
  node [
    id 1702
    label "fukni&#281;cie"
  ]
  node [
    id 1703
    label "l&#261;d"
  ]
  node [
    id 1704
    label "fukanie"
  ]
  node [
    id 1705
    label "przyholowywa&#263;"
  ]
  node [
    id 1706
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1707
    label "woda"
  ]
  node [
    id 1708
    label "przeszklenie"
  ]
  node [
    id 1709
    label "test_zderzeniowy"
  ]
  node [
    id 1710
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1711
    label "odzywka"
  ]
  node [
    id 1712
    label "nadwozie"
  ]
  node [
    id 1713
    label "odholowanie"
  ]
  node [
    id 1714
    label "prowadzenie_si&#281;"
  ]
  node [
    id 1715
    label "odholowywa&#263;"
  ]
  node [
    id 1716
    label "pod&#322;oga"
  ]
  node [
    id 1717
    label "odholowywanie"
  ]
  node [
    id 1718
    label "hamulec"
  ]
  node [
    id 1719
    label "podwozie"
  ]
  node [
    id 1720
    label "wia&#263;"
  ]
  node [
    id 1721
    label "bra&#263;"
  ]
  node [
    id 1722
    label "blow"
  ]
  node [
    id 1723
    label "rozdyma&#263;"
  ]
  node [
    id 1724
    label "rycze&#263;"
  ]
  node [
    id 1725
    label "wypuszcza&#263;"
  ]
  node [
    id 1726
    label "pump"
  ]
  node [
    id 1727
    label "inflate"
  ]
  node [
    id 1728
    label "wype&#322;nia&#263;"
  ]
  node [
    id 1729
    label "rokada"
  ]
  node [
    id 1730
    label "pole_bitwy"
  ]
  node [
    id 1731
    label "sfera"
  ]
  node [
    id 1732
    label "zaleganie"
  ]
  node [
    id 1733
    label "zjednoczenie"
  ]
  node [
    id 1734
    label "przedpole"
  ]
  node [
    id 1735
    label "szczyt"
  ]
  node [
    id 1736
    label "zalega&#263;"
  ]
  node [
    id 1737
    label "przycz&#243;&#322;ek"
  ]
  node [
    id 1738
    label "zjawisko"
  ]
  node [
    id 1739
    label "elewacja"
  ]
  node [
    id 1740
    label "stowarzyszenie"
  ]
  node [
    id 1741
    label "podmucha&#263;"
  ]
  node [
    id 1742
    label "zaliczy&#263;"
  ]
  node [
    id 1743
    label "wypu&#347;ci&#263;"
  ]
  node [
    id 1744
    label "uciec"
  ]
  node [
    id 1745
    label "powia&#263;"
  ]
  node [
    id 1746
    label "ukra&#347;&#263;"
  ]
  node [
    id 1747
    label "nawdychanie_si&#281;"
  ]
  node [
    id 1748
    label "aspiration"
  ]
  node [
    id 1749
    label "wci&#261;ganie"
  ]
  node [
    id 1750
    label "nasyci&#263;"
  ]
  node [
    id 1751
    label "wydala&#263;"
  ]
  node [
    id 1752
    label "zast&#281;powanie"
  ]
  node [
    id 1753
    label "przewietrzanie_si&#281;"
  ]
  node [
    id 1754
    label "aeration"
  ]
  node [
    id 1755
    label "traktowanie"
  ]
  node [
    id 1756
    label "czyszczenie"
  ]
  node [
    id 1757
    label "ventilation"
  ]
  node [
    id 1758
    label "refresher_course"
  ]
  node [
    id 1759
    label "oczyszczenie"
  ]
  node [
    id 1760
    label "wymienienie"
  ]
  node [
    id 1761
    label "vaporization"
  ]
  node [
    id 1762
    label "potraktowanie"
  ]
  node [
    id 1763
    label "przewietrzenie_si&#281;"
  ]
  node [
    id 1764
    label "wdmuchanie"
  ]
  node [
    id 1765
    label "wianie"
  ]
  node [
    id 1766
    label "wypuszczanie"
  ]
  node [
    id 1767
    label "uleganie"
  ]
  node [
    id 1768
    label "instrument_d&#281;ty"
  ]
  node [
    id 1769
    label "branie"
  ]
  node [
    id 1770
    label "wydmuchiwanie"
  ]
  node [
    id 1771
    label "wype&#322;nianie"
  ]
  node [
    id 1772
    label "wdmuchiwanie"
  ]
  node [
    id 1773
    label "blowing"
  ]
  node [
    id 1774
    label "czy&#347;ci&#263;"
  ]
  node [
    id 1775
    label "air"
  ]
  node [
    id 1776
    label "zmienia&#263;"
  ]
  node [
    id 1777
    label "poddawa&#263;"
  ]
  node [
    id 1778
    label "update"
  ]
  node [
    id 1779
    label "publicize"
  ]
  node [
    id 1780
    label "wyczy&#347;ci&#263;"
  ]
  node [
    id 1781
    label "podda&#263;"
  ]
  node [
    id 1782
    label "vent"
  ]
  node [
    id 1783
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1784
    label "cheat"
  ]
  node [
    id 1785
    label "wci&#261;ga&#263;"
  ]
  node [
    id 1786
    label "styka&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 528
  ]
  edge [
    source 11
    target 205
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 529
  ]
  edge [
    source 11
    target 530
  ]
  edge [
    source 11
    target 531
  ]
  edge [
    source 11
    target 532
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 534
  ]
  edge [
    source 11
    target 535
  ]
  edge [
    source 11
    target 536
  ]
  edge [
    source 11
    target 537
  ]
  edge [
    source 11
    target 538
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 539
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 540
  ]
  edge [
    source 11
    target 541
  ]
  edge [
    source 11
    target 542
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 11
    target 583
  ]
  edge [
    source 11
    target 584
  ]
  edge [
    source 11
    target 585
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 11
    target 599
  ]
  edge [
    source 11
    target 600
  ]
  edge [
    source 11
    target 601
  ]
  edge [
    source 11
    target 602
  ]
  edge [
    source 11
    target 603
  ]
  edge [
    source 11
    target 604
  ]
  edge [
    source 11
    target 605
  ]
  edge [
    source 11
    target 606
  ]
  edge [
    source 11
    target 607
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 243
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 296
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 216
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 914
  ]
  edge [
    source 12
    target 915
  ]
  edge [
    source 12
    target 916
  ]
  edge [
    source 12
    target 917
  ]
  edge [
    source 12
    target 918
  ]
  edge [
    source 12
    target 919
  ]
  edge [
    source 12
    target 920
  ]
  edge [
    source 12
    target 921
  ]
  edge [
    source 12
    target 922
  ]
  edge [
    source 12
    target 923
  ]
  edge [
    source 12
    target 924
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 12
    target 1021
  ]
  edge [
    source 12
    target 1022
  ]
  edge [
    source 12
    target 1023
  ]
  edge [
    source 12
    target 1024
  ]
  edge [
    source 12
    target 1025
  ]
  edge [
    source 12
    target 1026
  ]
  edge [
    source 12
    target 1027
  ]
  edge [
    source 12
    target 1028
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 1057
  ]
  edge [
    source 12
    target 1058
  ]
  edge [
    source 12
    target 1059
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 1070
  ]
  edge [
    source 12
    target 1071
  ]
  edge [
    source 12
    target 1072
  ]
  edge [
    source 12
    target 1073
  ]
  edge [
    source 12
    target 1074
  ]
  edge [
    source 12
    target 1075
  ]
  edge [
    source 12
    target 1076
  ]
  edge [
    source 12
    target 1077
  ]
  edge [
    source 12
    target 1078
  ]
  edge [
    source 12
    target 1079
  ]
  edge [
    source 12
    target 1080
  ]
  edge [
    source 12
    target 1081
  ]
  edge [
    source 12
    target 1082
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 1083
  ]
  edge [
    source 12
    target 1084
  ]
  edge [
    source 12
    target 1085
  ]
  edge [
    source 12
    target 1086
  ]
  edge [
    source 12
    target 1087
  ]
  edge [
    source 12
    target 1088
  ]
  edge [
    source 12
    target 1089
  ]
  edge [
    source 12
    target 1090
  ]
  edge [
    source 12
    target 1091
  ]
  edge [
    source 12
    target 1092
  ]
  edge [
    source 12
    target 1093
  ]
  edge [
    source 12
    target 1094
  ]
  edge [
    source 12
    target 1095
  ]
  edge [
    source 12
    target 1096
  ]
  edge [
    source 12
    target 1097
  ]
  edge [
    source 12
    target 1098
  ]
  edge [
    source 12
    target 1099
  ]
  edge [
    source 12
    target 1100
  ]
  edge [
    source 12
    target 1101
  ]
  edge [
    source 12
    target 1102
  ]
  edge [
    source 12
    target 1103
  ]
  edge [
    source 12
    target 1104
  ]
  edge [
    source 12
    target 1105
  ]
  edge [
    source 12
    target 1106
  ]
  edge [
    source 12
    target 1107
  ]
  edge [
    source 12
    target 1108
  ]
  edge [
    source 12
    target 1109
  ]
  edge [
    source 12
    target 1110
  ]
  edge [
    source 12
    target 1111
  ]
  edge [
    source 12
    target 1112
  ]
  edge [
    source 12
    target 1113
  ]
  edge [
    source 12
    target 1114
  ]
  edge [
    source 12
    target 1115
  ]
  edge [
    source 12
    target 1116
  ]
  edge [
    source 12
    target 1117
  ]
  edge [
    source 12
    target 1118
  ]
  edge [
    source 12
    target 1119
  ]
  edge [
    source 12
    target 1120
  ]
  edge [
    source 12
    target 1121
  ]
  edge [
    source 12
    target 1122
  ]
  edge [
    source 12
    target 1123
  ]
  edge [
    source 12
    target 1124
  ]
  edge [
    source 12
    target 1125
  ]
  edge [
    source 12
    target 1126
  ]
  edge [
    source 12
    target 1127
  ]
  edge [
    source 12
    target 1128
  ]
  edge [
    source 12
    target 1129
  ]
  edge [
    source 12
    target 1130
  ]
  edge [
    source 12
    target 1131
  ]
  edge [
    source 12
    target 1132
  ]
  edge [
    source 12
    target 1133
  ]
  edge [
    source 12
    target 1134
  ]
  edge [
    source 12
    target 1135
  ]
  edge [
    source 12
    target 1136
  ]
  edge [
    source 12
    target 1137
  ]
  edge [
    source 12
    target 1138
  ]
  edge [
    source 12
    target 1139
  ]
  edge [
    source 12
    target 1140
  ]
  edge [
    source 12
    target 1141
  ]
  edge [
    source 12
    target 1142
  ]
  edge [
    source 12
    target 1143
  ]
  edge [
    source 12
    target 1144
  ]
  edge [
    source 12
    target 1145
  ]
  edge [
    source 12
    target 1146
  ]
  edge [
    source 12
    target 1147
  ]
  edge [
    source 12
    target 1148
  ]
  edge [
    source 12
    target 1149
  ]
  edge [
    source 12
    target 1150
  ]
  edge [
    source 12
    target 1151
  ]
  edge [
    source 12
    target 1152
  ]
  edge [
    source 12
    target 1153
  ]
  edge [
    source 12
    target 1154
  ]
  edge [
    source 12
    target 1155
  ]
  edge [
    source 12
    target 1156
  ]
  edge [
    source 12
    target 1157
  ]
  edge [
    source 12
    target 1158
  ]
  edge [
    source 12
    target 1159
  ]
  edge [
    source 12
    target 1160
  ]
  edge [
    source 12
    target 1161
  ]
  edge [
    source 12
    target 1162
  ]
  edge [
    source 12
    target 1163
  ]
  edge [
    source 12
    target 1164
  ]
  edge [
    source 12
    target 1165
  ]
  edge [
    source 12
    target 1166
  ]
  edge [
    source 12
    target 1167
  ]
  edge [
    source 12
    target 1168
  ]
  edge [
    source 12
    target 1169
  ]
  edge [
    source 12
    target 1170
  ]
  edge [
    source 12
    target 1171
  ]
  edge [
    source 12
    target 1172
  ]
  edge [
    source 12
    target 1173
  ]
  edge [
    source 12
    target 1174
  ]
  edge [
    source 12
    target 1175
  ]
  edge [
    source 12
    target 1176
  ]
  edge [
    source 12
    target 1177
  ]
  edge [
    source 12
    target 1178
  ]
  edge [
    source 12
    target 1179
  ]
  edge [
    source 12
    target 1180
  ]
  edge [
    source 12
    target 1181
  ]
  edge [
    source 12
    target 1182
  ]
  edge [
    source 12
    target 1183
  ]
  edge [
    source 12
    target 1184
  ]
  edge [
    source 12
    target 1185
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 1186
  ]
  edge [
    source 12
    target 1187
  ]
  edge [
    source 12
    target 1188
  ]
  edge [
    source 12
    target 1189
  ]
  edge [
    source 12
    target 1190
  ]
  edge [
    source 12
    target 1191
  ]
  edge [
    source 12
    target 1192
  ]
  edge [
    source 12
    target 1193
  ]
  edge [
    source 12
    target 1194
  ]
  edge [
    source 12
    target 1195
  ]
  edge [
    source 12
    target 1196
  ]
  edge [
    source 12
    target 1197
  ]
  edge [
    source 12
    target 1198
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 1227
  ]
  edge [
    source 12
    target 1228
  ]
  edge [
    source 12
    target 1229
  ]
  edge [
    source 12
    target 1230
  ]
  edge [
    source 12
    target 1231
  ]
  edge [
    source 12
    target 1232
  ]
  edge [
    source 12
    target 1233
  ]
  edge [
    source 12
    target 1234
  ]
  edge [
    source 12
    target 1235
  ]
  edge [
    source 12
    target 1236
  ]
  edge [
    source 12
    target 1237
  ]
  edge [
    source 12
    target 1238
  ]
  edge [
    source 12
    target 1239
  ]
  edge [
    source 12
    target 1240
  ]
  edge [
    source 12
    target 1241
  ]
  edge [
    source 12
    target 1242
  ]
  edge [
    source 12
    target 1243
  ]
  edge [
    source 12
    target 1244
  ]
  edge [
    source 12
    target 1245
  ]
  edge [
    source 12
    target 1246
  ]
  edge [
    source 12
    target 1247
  ]
  edge [
    source 12
    target 1248
  ]
  edge [
    source 12
    target 1249
  ]
  edge [
    source 12
    target 1250
  ]
  edge [
    source 12
    target 1251
  ]
  edge [
    source 12
    target 1252
  ]
  edge [
    source 12
    target 1253
  ]
  edge [
    source 12
    target 1254
  ]
  edge [
    source 12
    target 1255
  ]
  edge [
    source 12
    target 1256
  ]
  edge [
    source 12
    target 1257
  ]
  edge [
    source 12
    target 1258
  ]
  edge [
    source 12
    target 1259
  ]
  edge [
    source 12
    target 1260
  ]
  edge [
    source 12
    target 1261
  ]
  edge [
    source 12
    target 1262
  ]
  edge [
    source 12
    target 1263
  ]
  edge [
    source 12
    target 1264
  ]
  edge [
    source 12
    target 1265
  ]
  edge [
    source 12
    target 1266
  ]
  edge [
    source 12
    target 1267
  ]
  edge [
    source 12
    target 1268
  ]
  edge [
    source 12
    target 1269
  ]
  edge [
    source 12
    target 1270
  ]
  edge [
    source 12
    target 1271
  ]
  edge [
    source 12
    target 1272
  ]
  edge [
    source 12
    target 1273
  ]
  edge [
    source 12
    target 1274
  ]
  edge [
    source 12
    target 1275
  ]
  edge [
    source 12
    target 1276
  ]
  edge [
    source 12
    target 1277
  ]
  edge [
    source 12
    target 1278
  ]
  edge [
    source 12
    target 1279
  ]
  edge [
    source 12
    target 1280
  ]
  edge [
    source 12
    target 1281
  ]
  edge [
    source 12
    target 1282
  ]
  edge [
    source 12
    target 1283
  ]
  edge [
    source 12
    target 1284
  ]
  edge [
    source 12
    target 1285
  ]
  edge [
    source 12
    target 1286
  ]
  edge [
    source 12
    target 1287
  ]
  edge [
    source 12
    target 1288
  ]
  edge [
    source 12
    target 1289
  ]
  edge [
    source 12
    target 1290
  ]
  edge [
    source 12
    target 1291
  ]
  edge [
    source 12
    target 1292
  ]
  edge [
    source 12
    target 1293
  ]
  edge [
    source 12
    target 1294
  ]
  edge [
    source 12
    target 1295
  ]
  edge [
    source 12
    target 1296
  ]
  edge [
    source 12
    target 1297
  ]
  edge [
    source 12
    target 1298
  ]
  edge [
    source 12
    target 1299
  ]
  edge [
    source 12
    target 1300
  ]
  edge [
    source 12
    target 1301
  ]
  edge [
    source 12
    target 1302
  ]
  edge [
    source 12
    target 1303
  ]
  edge [
    source 12
    target 1304
  ]
  edge [
    source 12
    target 1305
  ]
  edge [
    source 12
    target 1306
  ]
  edge [
    source 12
    target 1307
  ]
  edge [
    source 12
    target 1308
  ]
  edge [
    source 12
    target 1309
  ]
  edge [
    source 12
    target 1310
  ]
  edge [
    source 12
    target 1311
  ]
  edge [
    source 12
    target 1312
  ]
  edge [
    source 12
    target 1313
  ]
  edge [
    source 12
    target 1314
  ]
  edge [
    source 12
    target 1315
  ]
  edge [
    source 12
    target 1316
  ]
  edge [
    source 12
    target 1317
  ]
  edge [
    source 12
    target 1318
  ]
  edge [
    source 12
    target 1319
  ]
  edge [
    source 12
    target 1320
  ]
  edge [
    source 12
    target 1321
  ]
  edge [
    source 12
    target 1322
  ]
  edge [
    source 12
    target 1323
  ]
  edge [
    source 12
    target 1324
  ]
  edge [
    source 12
    target 1325
  ]
  edge [
    source 12
    target 1326
  ]
  edge [
    source 12
    target 1327
  ]
  edge [
    source 12
    target 1328
  ]
  edge [
    source 12
    target 1329
  ]
  edge [
    source 12
    target 1330
  ]
  edge [
    source 12
    target 1331
  ]
  edge [
    source 12
    target 1332
  ]
  edge [
    source 12
    target 1333
  ]
  edge [
    source 12
    target 1334
  ]
  edge [
    source 12
    target 1335
  ]
  edge [
    source 12
    target 1336
  ]
  edge [
    source 12
    target 1337
  ]
  edge [
    source 12
    target 1338
  ]
  edge [
    source 12
    target 1339
  ]
  edge [
    source 12
    target 1340
  ]
  edge [
    source 12
    target 1341
  ]
  edge [
    source 12
    target 1342
  ]
  edge [
    source 12
    target 1343
  ]
  edge [
    source 12
    target 1344
  ]
  edge [
    source 12
    target 1345
  ]
  edge [
    source 12
    target 1346
  ]
  edge [
    source 12
    target 1347
  ]
  edge [
    source 12
    target 1348
  ]
  edge [
    source 12
    target 1349
  ]
  edge [
    source 12
    target 1350
  ]
  edge [
    source 12
    target 1351
  ]
  edge [
    source 12
    target 1352
  ]
  edge [
    source 12
    target 1353
  ]
  edge [
    source 12
    target 1354
  ]
  edge [
    source 12
    target 209
  ]
  edge [
    source 12
    target 1355
  ]
  edge [
    source 12
    target 1356
  ]
  edge [
    source 12
    target 1357
  ]
  edge [
    source 12
    target 1358
  ]
  edge [
    source 12
    target 1359
  ]
  edge [
    source 12
    target 1360
  ]
  edge [
    source 12
    target 1361
  ]
  edge [
    source 12
    target 1362
  ]
  edge [
    source 12
    target 1363
  ]
  edge [
    source 12
    target 1364
  ]
  edge [
    source 12
    target 1365
  ]
  edge [
    source 12
    target 1366
  ]
  edge [
    source 12
    target 1367
  ]
  edge [
    source 12
    target 1368
  ]
  edge [
    source 12
    target 1369
  ]
  edge [
    source 12
    target 1370
  ]
  edge [
    source 12
    target 1371
  ]
  edge [
    source 12
    target 1372
  ]
  edge [
    source 12
    target 1373
  ]
  edge [
    source 12
    target 1374
  ]
  edge [
    source 12
    target 1375
  ]
  edge [
    source 12
    target 1376
  ]
  edge [
    source 12
    target 1377
  ]
  edge [
    source 12
    target 1378
  ]
  edge [
    source 12
    target 1379
  ]
  edge [
    source 12
    target 1380
  ]
  edge [
    source 12
    target 1381
  ]
  edge [
    source 12
    target 1382
  ]
  edge [
    source 12
    target 1383
  ]
  edge [
    source 12
    target 1384
  ]
  edge [
    source 12
    target 1385
  ]
  edge [
    source 12
    target 1386
  ]
  edge [
    source 12
    target 1387
  ]
  edge [
    source 12
    target 1388
  ]
  edge [
    source 12
    target 1389
  ]
  edge [
    source 12
    target 1390
  ]
  edge [
    source 12
    target 1391
  ]
  edge [
    source 12
    target 1392
  ]
  edge [
    source 12
    target 1393
  ]
  edge [
    source 12
    target 1394
  ]
  edge [
    source 12
    target 1395
  ]
  edge [
    source 12
    target 1396
  ]
  edge [
    source 12
    target 1397
  ]
  edge [
    source 12
    target 1398
  ]
  edge [
    source 12
    target 1399
  ]
  edge [
    source 12
    target 1400
  ]
  edge [
    source 12
    target 1401
  ]
  edge [
    source 12
    target 1402
  ]
  edge [
    source 12
    target 1403
  ]
  edge [
    source 12
    target 1404
  ]
  edge [
    source 12
    target 1405
  ]
  edge [
    source 12
    target 1406
  ]
  edge [
    source 12
    target 1407
  ]
  edge [
    source 12
    target 1408
  ]
  edge [
    source 12
    target 1409
  ]
  edge [
    source 12
    target 1410
  ]
  edge [
    source 12
    target 1411
  ]
  edge [
    source 12
    target 1412
  ]
  edge [
    source 12
    target 1413
  ]
  edge [
    source 12
    target 1414
  ]
  edge [
    source 12
    target 1415
  ]
  edge [
    source 12
    target 1416
  ]
  edge [
    source 12
    target 1417
  ]
  edge [
    source 12
    target 1418
  ]
  edge [
    source 12
    target 1419
  ]
  edge [
    source 12
    target 1420
  ]
  edge [
    source 12
    target 1421
  ]
  edge [
    source 12
    target 1422
  ]
  edge [
    source 12
    target 1423
  ]
  edge [
    source 12
    target 1424
  ]
  edge [
    source 12
    target 1425
  ]
  edge [
    source 12
    target 1426
  ]
  edge [
    source 12
    target 1427
  ]
  edge [
    source 12
    target 1428
  ]
  edge [
    source 12
    target 1429
  ]
  edge [
    source 12
    target 1430
  ]
  edge [
    source 12
    target 1431
  ]
  edge [
    source 12
    target 1432
  ]
  edge [
    source 12
    target 1433
  ]
  edge [
    source 12
    target 1434
  ]
  edge [
    source 12
    target 1435
  ]
  edge [
    source 12
    target 1436
  ]
  edge [
    source 12
    target 1437
  ]
  edge [
    source 12
    target 1438
  ]
  edge [
    source 12
    target 1439
  ]
  edge [
    source 12
    target 1440
  ]
  edge [
    source 12
    target 1441
  ]
  edge [
    source 12
    target 1442
  ]
  edge [
    source 12
    target 1443
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 1444
  ]
  edge [
    source 12
    target 1445
  ]
  edge [
    source 12
    target 1446
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 1447
  ]
  edge [
    source 12
    target 1448
  ]
  edge [
    source 12
    target 1449
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 1450
  ]
  edge [
    source 12
    target 1451
  ]
  edge [
    source 12
    target 1452
  ]
  edge [
    source 12
    target 1453
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 1454
  ]
  edge [
    source 12
    target 1455
  ]
  edge [
    source 12
    target 1456
  ]
  edge [
    source 12
    target 1457
  ]
  edge [
    source 12
    target 1458
  ]
  edge [
    source 12
    target 1459
  ]
  edge [
    source 12
    target 1460
  ]
  edge [
    source 12
    target 1461
  ]
  edge [
    source 12
    target 1462
  ]
  edge [
    source 12
    target 1463
  ]
  edge [
    source 12
    target 1464
  ]
  edge [
    source 12
    target 1465
  ]
  edge [
    source 12
    target 1466
  ]
  edge [
    source 12
    target 1467
  ]
  edge [
    source 12
    target 1468
  ]
  edge [
    source 12
    target 1469
  ]
  edge [
    source 12
    target 1470
  ]
  edge [
    source 12
    target 1471
  ]
  edge [
    source 12
    target 1472
  ]
  edge [
    source 12
    target 1473
  ]
  edge [
    source 12
    target 1474
  ]
  edge [
    source 12
    target 1475
  ]
  edge [
    source 12
    target 1476
  ]
  edge [
    source 12
    target 1477
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 1478
  ]
  edge [
    source 12
    target 1479
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1480
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 1481
  ]
  edge [
    source 14
    target 1482
  ]
  edge [
    source 14
    target 474
  ]
  edge [
    source 14
    target 1483
  ]
  edge [
    source 14
    target 1484
  ]
  edge [
    source 14
    target 1485
  ]
  edge [
    source 14
    target 1486
  ]
  edge [
    source 14
    target 1487
  ]
  edge [
    source 14
    target 1488
  ]
  edge [
    source 14
    target 1489
  ]
  edge [
    source 14
    target 1490
  ]
  edge [
    source 14
    target 475
  ]
  edge [
    source 14
    target 446
  ]
  edge [
    source 14
    target 1491
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 1492
  ]
  edge [
    source 14
    target 1493
  ]
  edge [
    source 14
    target 1494
  ]
  edge [
    source 14
    target 1495
  ]
  edge [
    source 14
    target 1496
  ]
  edge [
    source 14
    target 1497
  ]
  edge [
    source 14
    target 1498
  ]
  edge [
    source 14
    target 1499
  ]
  edge [
    source 14
    target 1500
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 1501
  ]
  edge [
    source 14
    target 1502
  ]
  edge [
    source 14
    target 1503
  ]
  edge [
    source 14
    target 1504
  ]
  edge [
    source 14
    target 1505
  ]
  edge [
    source 14
    target 1506
  ]
  edge [
    source 14
    target 1507
  ]
  edge [
    source 14
    target 1508
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1509
  ]
  edge [
    source 15
    target 1510
  ]
  edge [
    source 15
    target 1511
  ]
  edge [
    source 15
    target 1512
  ]
  edge [
    source 15
    target 1513
  ]
  edge [
    source 15
    target 1514
  ]
  edge [
    source 15
    target 1515
  ]
  edge [
    source 15
    target 1516
  ]
  edge [
    source 15
    target 1517
  ]
  edge [
    source 15
    target 1518
  ]
  edge [
    source 15
    target 1519
  ]
  edge [
    source 15
    target 1520
  ]
  edge [
    source 15
    target 1521
  ]
  edge [
    source 15
    target 1522
  ]
  edge [
    source 15
    target 1523
  ]
  edge [
    source 15
    target 1524
  ]
  edge [
    source 15
    target 1525
  ]
  edge [
    source 15
    target 1526
  ]
  edge [
    source 15
    target 1527
  ]
  edge [
    source 15
    target 1528
  ]
  edge [
    source 15
    target 1529
  ]
  edge [
    source 15
    target 208
  ]
  edge [
    source 15
    target 1530
  ]
  edge [
    source 15
    target 1531
  ]
  edge [
    source 15
    target 1532
  ]
  edge [
    source 15
    target 1533
  ]
  edge [
    source 15
    target 1534
  ]
  edge [
    source 15
    target 1535
  ]
  edge [
    source 15
    target 1536
  ]
  edge [
    source 15
    target 1537
  ]
  edge [
    source 15
    target 1117
  ]
  edge [
    source 15
    target 1538
  ]
  edge [
    source 15
    target 1539
  ]
  edge [
    source 15
    target 1540
  ]
  edge [
    source 15
    target 1541
  ]
  edge [
    source 15
    target 1542
  ]
  edge [
    source 15
    target 1543
  ]
  edge [
    source 15
    target 1544
  ]
  edge [
    source 15
    target 1545
  ]
  edge [
    source 15
    target 1546
  ]
  edge [
    source 15
    target 1547
  ]
  edge [
    source 15
    target 1548
  ]
  edge [
    source 15
    target 1549
  ]
  edge [
    source 15
    target 1550
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1294
  ]
  edge [
    source 16
    target 1551
  ]
  edge [
    source 16
    target 1296
  ]
  edge [
    source 16
    target 1552
  ]
  edge [
    source 16
    target 1553
  ]
  edge [
    source 16
    target 1297
  ]
  edge [
    source 16
    target 1554
  ]
  edge [
    source 16
    target 1555
  ]
  edge [
    source 16
    target 1045
  ]
  edge [
    source 16
    target 1299
  ]
  edge [
    source 16
    target 1300
  ]
  edge [
    source 16
    target 1556
  ]
  edge [
    source 16
    target 1557
  ]
  edge [
    source 16
    target 1301
  ]
  edge [
    source 16
    target 1558
  ]
  edge [
    source 16
    target 1302
  ]
  edge [
    source 16
    target 429
  ]
  edge [
    source 16
    target 1305
  ]
  edge [
    source 16
    target 1304
  ]
  edge [
    source 16
    target 1174
  ]
  edge [
    source 16
    target 1306
  ]
  edge [
    source 16
    target 1307
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 1308
  ]
  edge [
    source 16
    target 1309
  ]
  edge [
    source 16
    target 1559
  ]
  edge [
    source 16
    target 1310
  ]
  edge [
    source 16
    target 1560
  ]
  edge [
    source 16
    target 1313
  ]
  edge [
    source 16
    target 1561
  ]
  edge [
    source 16
    target 1314
  ]
  edge [
    source 16
    target 1315
  ]
  edge [
    source 16
    target 1317
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 1318
  ]
  edge [
    source 16
    target 1319
  ]
  edge [
    source 16
    target 1322
  ]
  edge [
    source 16
    target 1321
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 1562
  ]
  edge [
    source 16
    target 1563
  ]
  edge [
    source 16
    target 1564
  ]
  edge [
    source 16
    target 1565
  ]
  edge [
    source 16
    target 1566
  ]
  edge [
    source 16
    target 1567
  ]
  edge [
    source 16
    target 1568
  ]
  edge [
    source 16
    target 426
  ]
  edge [
    source 16
    target 1569
  ]
  edge [
    source 16
    target 1570
  ]
  edge [
    source 16
    target 428
  ]
  edge [
    source 16
    target 1571
  ]
  edge [
    source 16
    target 357
  ]
  edge [
    source 16
    target 40
  ]
  edge [
    source 16
    target 1572
  ]
  edge [
    source 16
    target 1514
  ]
  edge [
    source 16
    target 27
  ]
  edge [
    source 16
    target 1573
  ]
  edge [
    source 16
    target 1574
  ]
  edge [
    source 16
    target 1575
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 1576
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 1459
  ]
  edge [
    source 16
    target 1577
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 1578
  ]
  edge [
    source 16
    target 1579
  ]
  edge [
    source 16
    target 1580
  ]
  edge [
    source 16
    target 342
  ]
  edge [
    source 16
    target 1581
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 1582
  ]
  edge [
    source 16
    target 1583
  ]
  edge [
    source 16
    target 1584
  ]
  edge [
    source 16
    target 1585
  ]
  edge [
    source 16
    target 1586
  ]
  edge [
    source 16
    target 1587
  ]
  edge [
    source 16
    target 1588
  ]
  edge [
    source 16
    target 1589
  ]
  edge [
    source 16
    target 1590
  ]
  edge [
    source 16
    target 1591
  ]
  edge [
    source 16
    target 1592
  ]
  edge [
    source 16
    target 1593
  ]
  edge [
    source 16
    target 1594
  ]
  edge [
    source 16
    target 562
  ]
  edge [
    source 16
    target 1595
  ]
  edge [
    source 16
    target 1596
  ]
  edge [
    source 16
    target 1597
  ]
  edge [
    source 16
    target 1598
  ]
  edge [
    source 16
    target 1599
  ]
  edge [
    source 16
    target 1600
  ]
  edge [
    source 16
    target 1601
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 16
    target 1602
  ]
  edge [
    source 16
    target 1603
  ]
  edge [
    source 16
    target 316
  ]
  edge [
    source 16
    target 1604
  ]
  edge [
    source 16
    target 1605
  ]
  edge [
    source 16
    target 1606
  ]
  edge [
    source 16
    target 1607
  ]
  edge [
    source 16
    target 1608
  ]
  edge [
    source 16
    target 1609
  ]
  edge [
    source 16
    target 1610
  ]
  edge [
    source 16
    target 1611
  ]
  edge [
    source 16
    target 1425
  ]
  edge [
    source 16
    target 1612
  ]
  edge [
    source 16
    target 1613
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 1614
  ]
  edge [
    source 16
    target 1615
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 1472
  ]
  edge [
    source 16
    target 1096
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 1037
  ]
  edge [
    source 16
    target 1616
  ]
  edge [
    source 16
    target 1287
  ]
  edge [
    source 16
    target 1617
  ]
  edge [
    source 16
    target 1618
  ]
  edge [
    source 16
    target 1619
  ]
  edge [
    source 16
    target 1620
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 1621
  ]
  edge [
    source 16
    target 1622
  ]
  edge [
    source 16
    target 1623
  ]
  edge [
    source 16
    target 1624
  ]
  edge [
    source 16
    target 1625
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1626
  ]
  edge [
    source 17
    target 1627
  ]
  edge [
    source 17
    target 1628
  ]
  edge [
    source 17
    target 1629
  ]
  edge [
    source 17
    target 1630
  ]
  edge [
    source 17
    target 1631
  ]
  edge [
    source 17
    target 1632
  ]
  edge [
    source 17
    target 1633
  ]
  edge [
    source 17
    target 1634
  ]
  edge [
    source 17
    target 1635
  ]
  edge [
    source 17
    target 1636
  ]
  edge [
    source 17
    target 1637
  ]
  edge [
    source 17
    target 1638
  ]
  edge [
    source 17
    target 1639
  ]
  edge [
    source 17
    target 1640
  ]
  edge [
    source 17
    target 1641
  ]
  edge [
    source 17
    target 1642
  ]
  edge [
    source 17
    target 1643
  ]
  edge [
    source 17
    target 1644
  ]
  edge [
    source 17
    target 1645
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 1449
  ]
  edge [
    source 17
    target 1646
  ]
  edge [
    source 17
    target 1647
  ]
  edge [
    source 17
    target 1648
  ]
  edge [
    source 17
    target 1649
  ]
  edge [
    source 17
    target 1650
  ]
  edge [
    source 17
    target 1651
  ]
  edge [
    source 17
    target 1652
  ]
  edge [
    source 17
    target 1653
  ]
  edge [
    source 17
    target 1654
  ]
  edge [
    source 17
    target 1655
  ]
  edge [
    source 17
    target 1656
  ]
  edge [
    source 17
    target 1657
  ]
  edge [
    source 17
    target 1658
  ]
  edge [
    source 17
    target 1659
  ]
  edge [
    source 17
    target 1660
  ]
  edge [
    source 17
    target 97
  ]
  edge [
    source 17
    target 1661
  ]
  edge [
    source 17
    target 1662
  ]
  edge [
    source 17
    target 1663
  ]
  edge [
    source 17
    target 1664
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 1665
  ]
  edge [
    source 17
    target 1666
  ]
  edge [
    source 17
    target 1667
  ]
  edge [
    source 17
    target 1668
  ]
  edge [
    source 17
    target 1669
  ]
  edge [
    source 17
    target 1670
  ]
  edge [
    source 17
    target 1671
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 1672
  ]
  edge [
    source 17
    target 1673
  ]
  edge [
    source 17
    target 1674
  ]
  edge [
    source 17
    target 1675
  ]
  edge [
    source 17
    target 1676
  ]
  edge [
    source 17
    target 428
  ]
  edge [
    source 17
    target 1677
  ]
  edge [
    source 17
    target 1678
  ]
  edge [
    source 17
    target 1679
  ]
  edge [
    source 17
    target 117
  ]
  edge [
    source 17
    target 1680
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 1681
  ]
  edge [
    source 17
    target 1682
  ]
  edge [
    source 17
    target 1683
  ]
  edge [
    source 17
    target 1684
  ]
  edge [
    source 17
    target 1685
  ]
  edge [
    source 17
    target 1686
  ]
  edge [
    source 17
    target 1687
  ]
  edge [
    source 17
    target 1688
  ]
  edge [
    source 17
    target 1689
  ]
  edge [
    source 17
    target 1690
  ]
  edge [
    source 17
    target 1691
  ]
  edge [
    source 17
    target 1692
  ]
  edge [
    source 17
    target 1693
  ]
  edge [
    source 17
    target 1694
  ]
  edge [
    source 17
    target 1695
  ]
  edge [
    source 17
    target 1696
  ]
  edge [
    source 17
    target 1697
  ]
  edge [
    source 17
    target 1698
  ]
  edge [
    source 17
    target 1699
  ]
  edge [
    source 17
    target 1700
  ]
  edge [
    source 17
    target 1701
  ]
  edge [
    source 17
    target 1702
  ]
  edge [
    source 17
    target 1703
  ]
  edge [
    source 17
    target 1303
  ]
  edge [
    source 17
    target 1704
  ]
  edge [
    source 17
    target 1705
  ]
  edge [
    source 17
    target 1706
  ]
  edge [
    source 17
    target 1707
  ]
  edge [
    source 17
    target 1708
  ]
  edge [
    source 17
    target 1709
  ]
  edge [
    source 17
    target 1710
  ]
  edge [
    source 17
    target 1711
  ]
  edge [
    source 17
    target 1712
  ]
  edge [
    source 17
    target 1713
  ]
  edge [
    source 17
    target 1714
  ]
  edge [
    source 17
    target 1715
  ]
  edge [
    source 17
    target 1716
  ]
  edge [
    source 17
    target 1717
  ]
  edge [
    source 17
    target 1718
  ]
  edge [
    source 17
    target 1719
  ]
  edge [
    source 17
    target 1720
  ]
  edge [
    source 17
    target 1721
  ]
  edge [
    source 17
    target 1722
  ]
  edge [
    source 17
    target 1723
  ]
  edge [
    source 17
    target 1724
  ]
  edge [
    source 17
    target 1725
  ]
  edge [
    source 17
    target 1726
  ]
  edge [
    source 17
    target 1727
  ]
  edge [
    source 17
    target 338
  ]
  edge [
    source 17
    target 1728
  ]
  edge [
    source 17
    target 1729
  ]
  edge [
    source 17
    target 346
  ]
  edge [
    source 17
    target 1730
  ]
  edge [
    source 17
    target 1731
  ]
  edge [
    source 17
    target 1732
  ]
  edge [
    source 17
    target 1733
  ]
  edge [
    source 17
    target 1734
  ]
  edge [
    source 17
    target 367
  ]
  edge [
    source 17
    target 1425
  ]
  edge [
    source 17
    target 1735
  ]
  edge [
    source 17
    target 1736
  ]
  edge [
    source 17
    target 1737
  ]
  edge [
    source 17
    target 1738
  ]
  edge [
    source 17
    target 388
  ]
  edge [
    source 17
    target 1739
  ]
  edge [
    source 17
    target 1740
  ]
  edge [
    source 17
    target 1741
  ]
  edge [
    source 17
    target 1742
  ]
  edge [
    source 17
    target 1743
  ]
  edge [
    source 17
    target 1744
  ]
  edge [
    source 17
    target 1745
  ]
  edge [
    source 17
    target 1746
  ]
  edge [
    source 17
    target 1747
  ]
  edge [
    source 17
    target 1748
  ]
  edge [
    source 17
    target 1749
  ]
  edge [
    source 17
    target 1750
  ]
  edge [
    source 17
    target 1751
  ]
  edge [
    source 17
    target 1752
  ]
  edge [
    source 17
    target 1753
  ]
  edge [
    source 17
    target 1754
  ]
  edge [
    source 17
    target 1755
  ]
  edge [
    source 17
    target 1756
  ]
  edge [
    source 17
    target 1757
  ]
  edge [
    source 17
    target 1758
  ]
  edge [
    source 17
    target 1759
  ]
  edge [
    source 17
    target 1760
  ]
  edge [
    source 17
    target 1761
  ]
  edge [
    source 17
    target 1762
  ]
  edge [
    source 17
    target 1763
  ]
  edge [
    source 17
    target 1764
  ]
  edge [
    source 17
    target 1765
  ]
  edge [
    source 17
    target 1766
  ]
  edge [
    source 17
    target 1767
  ]
  edge [
    source 17
    target 1768
  ]
  edge [
    source 17
    target 1769
  ]
  edge [
    source 17
    target 1770
  ]
  edge [
    source 17
    target 1771
  ]
  edge [
    source 17
    target 1772
  ]
  edge [
    source 17
    target 1773
  ]
  edge [
    source 17
    target 381
  ]
  edge [
    source 17
    target 1774
  ]
  edge [
    source 17
    target 1775
  ]
  edge [
    source 17
    target 1776
  ]
  edge [
    source 17
    target 1777
  ]
  edge [
    source 17
    target 1778
  ]
  edge [
    source 17
    target 1779
  ]
  edge [
    source 17
    target 1780
  ]
  edge [
    source 17
    target 1781
  ]
  edge [
    source 17
    target 1490
  ]
  edge [
    source 17
    target 1782
  ]
  edge [
    source 17
    target 1783
  ]
  edge [
    source 17
    target 1784
  ]
  edge [
    source 17
    target 1785
  ]
  edge [
    source 17
    target 1786
  ]
]
