graph [
  node [
    id 0
    label "antoni"
    origin "text"
  ]
  node [
    id 1
    label "urba&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "polityk"
    origin "text"
  ]
  node [
    id 3
    label "Gorbaczow"
  ]
  node [
    id 4
    label "Korwin"
  ]
  node [
    id 5
    label "McCarthy"
  ]
  node [
    id 6
    label "Goebbels"
  ]
  node [
    id 7
    label "Miko&#322;ajczyk"
  ]
  node [
    id 8
    label "Ziobro"
  ]
  node [
    id 9
    label "Katon"
  ]
  node [
    id 10
    label "dzia&#322;acz"
  ]
  node [
    id 11
    label "Moczar"
  ]
  node [
    id 12
    label "Gierek"
  ]
  node [
    id 13
    label "Arafat"
  ]
  node [
    id 14
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 15
    label "Mao"
  ]
  node [
    id 16
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 17
    label "Bre&#380;niew"
  ]
  node [
    id 18
    label "Nixon"
  ]
  node [
    id 19
    label "Naser"
  ]
  node [
    id 20
    label "Perykles"
  ]
  node [
    id 21
    label "Metternich"
  ]
  node [
    id 22
    label "Kuro&#324;"
  ]
  node [
    id 23
    label "Borel"
  ]
  node [
    id 24
    label "Juliusz_Cezar"
  ]
  node [
    id 25
    label "Bierut"
  ]
  node [
    id 26
    label "bezpartyjny"
  ]
  node [
    id 27
    label "Falandysz"
  ]
  node [
    id 28
    label "Leszek_Miller"
  ]
  node [
    id 29
    label "Fidel_Castro"
  ]
  node [
    id 30
    label "Winston_Churchill"
  ]
  node [
    id 31
    label "Sto&#322;ypin"
  ]
  node [
    id 32
    label "J&#281;drzejewicz"
  ]
  node [
    id 33
    label "Putin"
  ]
  node [
    id 34
    label "Chruszczow"
  ]
  node [
    id 35
    label "de_Gaulle"
  ]
  node [
    id 36
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 37
    label "Gomu&#322;ka"
  ]
  node [
    id 38
    label "Asnyk"
  ]
  node [
    id 39
    label "Michnik"
  ]
  node [
    id 40
    label "Owsiak"
  ]
  node [
    id 41
    label "cz&#322;onek"
  ]
  node [
    id 42
    label "komuna"
  ]
  node [
    id 43
    label "Plan_Ko&#322;&#322;&#261;tajowski"
  ]
  node [
    id 44
    label "o&#347;wiecenie"
  ]
  node [
    id 45
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 46
    label "Polska_Rzeczpospolita_Ludowa"
  ]
  node [
    id 47
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 48
    label "Aspazja"
  ]
  node [
    id 49
    label "bezpartyjnie"
  ]
  node [
    id 50
    label "niezaanga&#380;owany"
  ]
  node [
    id 51
    label "niezale&#380;ny"
  ]
  node [
    id 52
    label "Antoni"
  ]
  node [
    id 53
    label "Urba&#324;ski"
  ]
  node [
    id 54
    label "d&#261;b"
  ]
  node [
    id 55
    label "szlachecki"
  ]
  node [
    id 56
    label "krajowy"
  ]
  node [
    id 57
    label "rada"
  ]
  node [
    id 58
    label "narodowy"
  ]
  node [
    id 59
    label "sejm"
  ]
  node [
    id 60
    label "ustawodawczy"
  ]
  node [
    id 61
    label "wydzia&#322;"
  ]
  node [
    id 62
    label "Prawno"
  ]
  node [
    id 63
    label "ekonomiczny"
  ]
  node [
    id 64
    label "uniwersytet"
  ]
  node [
    id 65
    label "pozna&#324;ski"
  ]
  node [
    id 66
    label "akademia"
  ]
  node [
    id 67
    label "handlowy"
  ]
  node [
    id 68
    label "ii"
  ]
  node [
    id 69
    label "wojna"
  ]
  node [
    id 70
    label "&#347;wiatowy"
  ]
  node [
    id 71
    label "pozna&#324;sko"
  ]
  node [
    id 72
    label "warszawski"
  ]
  node [
    id 73
    label "towarzystwo"
  ]
  node [
    id 74
    label "ubezpieczeniowy"
  ]
  node [
    id 75
    label "pa&#324;stwowy"
  ]
  node [
    id 76
    label "centrala"
  ]
  node [
    id 77
    label "rzemie&#347;lniczy"
  ]
  node [
    id 78
    label "centralny"
  ]
  node [
    id 79
    label "zwi&#261;zek"
  ]
  node [
    id 80
    label "sp&#243;&#322;dzielczo&#347;&#263;"
  ]
  node [
    id 81
    label "praca"
  ]
  node [
    id 82
    label "Franciszka"
  ]
  node [
    id 83
    label "Ma&#324;kowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 67
    target 75
  ]
  edge [
    source 67
    target 76
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 70
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 73
  ]
  edge [
    source 71
    target 74
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 74
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 80
  ]
  edge [
    source 78
    target 81
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 81
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 82
    target 83
  ]
]
