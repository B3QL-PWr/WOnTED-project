graph [
  node [
    id 0
    label "zawie&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "hamulec"
    origin "text"
  ]
  node [
    id 2
    label "postojowy"
    origin "text"
  ]
  node [
    id 3
    label "uciec"
    origin "text"
  ]
  node [
    id 4
    label "kierowca"
    origin "text"
  ]
  node [
    id 5
    label "gdy"
    origin "text"
  ]
  node [
    id 6
    label "wysie&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "moderate"
  ]
  node [
    id 8
    label "wzbudzi&#263;"
  ]
  node [
    id 9
    label "wywo&#322;a&#263;"
  ]
  node [
    id 10
    label "arouse"
  ]
  node [
    id 11
    label "pow&#347;ci&#261;g"
  ]
  node [
    id 12
    label "uk&#322;ad_hamulcowy"
  ]
  node [
    id 13
    label "czuwak"
  ]
  node [
    id 14
    label "przeszkoda"
  ]
  node [
    id 15
    label "szcz&#281;ka"
  ]
  node [
    id 16
    label "pojazd"
  ]
  node [
    id 17
    label "brake"
  ]
  node [
    id 18
    label "luzownik"
  ]
  node [
    id 19
    label "urz&#261;dzenie"
  ]
  node [
    id 20
    label "dzielenie"
  ]
  node [
    id 21
    label "je&#378;dziectwo"
  ]
  node [
    id 22
    label "obstruction"
  ]
  node [
    id 23
    label "trudno&#347;&#263;"
  ]
  node [
    id 24
    label "bieg_prze&#322;ajowy"
  ]
  node [
    id 25
    label "podzielenie"
  ]
  node [
    id 26
    label "przedmiot"
  ]
  node [
    id 27
    label "kom&#243;rka"
  ]
  node [
    id 28
    label "furnishing"
  ]
  node [
    id 29
    label "zabezpieczenie"
  ]
  node [
    id 30
    label "zrobienie"
  ]
  node [
    id 31
    label "wyrz&#261;dzenie"
  ]
  node [
    id 32
    label "zagospodarowanie"
  ]
  node [
    id 33
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 34
    label "ig&#322;a"
  ]
  node [
    id 35
    label "narz&#281;dzie"
  ]
  node [
    id 36
    label "wirnik"
  ]
  node [
    id 37
    label "aparatura"
  ]
  node [
    id 38
    label "system_energetyczny"
  ]
  node [
    id 39
    label "impulsator"
  ]
  node [
    id 40
    label "mechanizm"
  ]
  node [
    id 41
    label "sprz&#281;t"
  ]
  node [
    id 42
    label "czynno&#347;&#263;"
  ]
  node [
    id 43
    label "blokowanie"
  ]
  node [
    id 44
    label "set"
  ]
  node [
    id 45
    label "zablokowanie"
  ]
  node [
    id 46
    label "przygotowanie"
  ]
  node [
    id 47
    label "komora"
  ]
  node [
    id 48
    label "j&#281;zyk"
  ]
  node [
    id 49
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 50
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 51
    label "antybodziec"
  ]
  node [
    id 52
    label "&#322;&#281;kotka"
  ]
  node [
    id 53
    label "artykulator"
  ]
  node [
    id 54
    label "trzewioczaszka"
  ]
  node [
    id 55
    label "wi&#261;zanie"
  ]
  node [
    id 56
    label "imad&#322;o"
  ]
  node [
    id 57
    label "szczena"
  ]
  node [
    id 58
    label "guzowato&#347;&#263;_br&#243;dkowa"
  ]
  node [
    id 59
    label "dzi&#261;s&#322;o"
  ]
  node [
    id 60
    label "jama_ustna"
  ]
  node [
    id 61
    label "ko&#347;&#263;_z&#281;bowa"
  ]
  node [
    id 62
    label "z&#261;b"
  ]
  node [
    id 63
    label "z&#281;bod&#243;&#322;"
  ]
  node [
    id 64
    label "ko&#347;&#263;"
  ]
  node [
    id 65
    label "artykulacja"
  ]
  node [
    id 66
    label "czaszka"
  ]
  node [
    id 67
    label "kram"
  ]
  node [
    id 68
    label "odholowa&#263;"
  ]
  node [
    id 69
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 70
    label "tabor"
  ]
  node [
    id 71
    label "przyholowywanie"
  ]
  node [
    id 72
    label "przyholowa&#263;"
  ]
  node [
    id 73
    label "przyholowanie"
  ]
  node [
    id 74
    label "fukni&#281;cie"
  ]
  node [
    id 75
    label "l&#261;d"
  ]
  node [
    id 76
    label "zielona_karta"
  ]
  node [
    id 77
    label "fukanie"
  ]
  node [
    id 78
    label "przyholowywa&#263;"
  ]
  node [
    id 79
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 80
    label "woda"
  ]
  node [
    id 81
    label "przeszklenie"
  ]
  node [
    id 82
    label "test_zderzeniowy"
  ]
  node [
    id 83
    label "powietrze"
  ]
  node [
    id 84
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 85
    label "odzywka"
  ]
  node [
    id 86
    label "nadwozie"
  ]
  node [
    id 87
    label "odholowanie"
  ]
  node [
    id 88
    label "prowadzenie_si&#281;"
  ]
  node [
    id 89
    label "odholowywa&#263;"
  ]
  node [
    id 90
    label "pod&#322;oga"
  ]
  node [
    id 91
    label "odholowywanie"
  ]
  node [
    id 92
    label "podwozie"
  ]
  node [
    id 93
    label "pojazd_szynowy"
  ]
  node [
    id 94
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 95
    label "zwia&#263;"
  ]
  node [
    id 96
    label "wzi&#261;&#263;"
  ]
  node [
    id 97
    label "wypierdoli&#263;"
  ]
  node [
    id 98
    label "fly"
  ]
  node [
    id 99
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 100
    label "spieprzy&#263;"
  ]
  node [
    id 101
    label "pass"
  ]
  node [
    id 102
    label "wyczy&#347;ci&#263;"
  ]
  node [
    id 103
    label "beat"
  ]
  node [
    id 104
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 105
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 106
    label "wyrzuci&#263;"
  ]
  node [
    id 107
    label "zepsu&#263;"
  ]
  node [
    id 108
    label "odziedziczy&#263;"
  ]
  node [
    id 109
    label "ruszy&#263;"
  ]
  node [
    id 110
    label "take"
  ]
  node [
    id 111
    label "zaatakowa&#263;"
  ]
  node [
    id 112
    label "skorzysta&#263;"
  ]
  node [
    id 113
    label "receive"
  ]
  node [
    id 114
    label "nakaza&#263;"
  ]
  node [
    id 115
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 116
    label "obskoczy&#263;"
  ]
  node [
    id 117
    label "bra&#263;"
  ]
  node [
    id 118
    label "u&#380;y&#263;"
  ]
  node [
    id 119
    label "zrobi&#263;"
  ]
  node [
    id 120
    label "get"
  ]
  node [
    id 121
    label "wyrucha&#263;"
  ]
  node [
    id 122
    label "World_Health_Organization"
  ]
  node [
    id 123
    label "wyciupcia&#263;"
  ]
  node [
    id 124
    label "wygra&#263;"
  ]
  node [
    id 125
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 126
    label "withdraw"
  ]
  node [
    id 127
    label "wzi&#281;cie"
  ]
  node [
    id 128
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 129
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 130
    label "poczyta&#263;"
  ]
  node [
    id 131
    label "obj&#261;&#263;"
  ]
  node [
    id 132
    label "seize"
  ]
  node [
    id 133
    label "aim"
  ]
  node [
    id 134
    label "chwyci&#263;"
  ]
  node [
    id 135
    label "przyj&#261;&#263;"
  ]
  node [
    id 136
    label "pokona&#263;"
  ]
  node [
    id 137
    label "arise"
  ]
  node [
    id 138
    label "uda&#263;_si&#281;"
  ]
  node [
    id 139
    label "zacz&#261;&#263;"
  ]
  node [
    id 140
    label "otrzyma&#263;"
  ]
  node [
    id 141
    label "wej&#347;&#263;"
  ]
  node [
    id 142
    label "poruszy&#263;"
  ]
  node [
    id 143
    label "dosta&#263;"
  ]
  node [
    id 144
    label "transportowiec"
  ]
  node [
    id 145
    label "cz&#322;owiek"
  ]
  node [
    id 146
    label "ludzko&#347;&#263;"
  ]
  node [
    id 147
    label "asymilowanie"
  ]
  node [
    id 148
    label "wapniak"
  ]
  node [
    id 149
    label "asymilowa&#263;"
  ]
  node [
    id 150
    label "os&#322;abia&#263;"
  ]
  node [
    id 151
    label "posta&#263;"
  ]
  node [
    id 152
    label "hominid"
  ]
  node [
    id 153
    label "podw&#322;adny"
  ]
  node [
    id 154
    label "os&#322;abianie"
  ]
  node [
    id 155
    label "g&#322;owa"
  ]
  node [
    id 156
    label "figura"
  ]
  node [
    id 157
    label "portrecista"
  ]
  node [
    id 158
    label "dwun&#243;g"
  ]
  node [
    id 159
    label "profanum"
  ]
  node [
    id 160
    label "mikrokosmos"
  ]
  node [
    id 161
    label "nasada"
  ]
  node [
    id 162
    label "duch"
  ]
  node [
    id 163
    label "antropochoria"
  ]
  node [
    id 164
    label "osoba"
  ]
  node [
    id 165
    label "wz&#243;r"
  ]
  node [
    id 166
    label "senior"
  ]
  node [
    id 167
    label "oddzia&#322;ywanie"
  ]
  node [
    id 168
    label "Adam"
  ]
  node [
    id 169
    label "homo_sapiens"
  ]
  node [
    id 170
    label "polifag"
  ]
  node [
    id 171
    label "pracownik"
  ]
  node [
    id 172
    label "statek_handlowy"
  ]
  node [
    id 173
    label "okr&#281;t_nawodny"
  ]
  node [
    id 174
    label "bran&#380;owiec"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 5
    target 6
  ]
]
