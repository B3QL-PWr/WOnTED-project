graph [
  node [
    id 0
    label "wrzuca&#263;"
    origin "text"
  ]
  node [
    id 1
    label "najbardziej"
    origin "text"
  ]
  node [
    id 2
    label "rakowy"
    origin "text"
  ]
  node [
    id 3
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 4
    label "give"
  ]
  node [
    id 5
    label "umieszcza&#263;"
  ]
  node [
    id 6
    label "plasowa&#263;"
  ]
  node [
    id 7
    label "umie&#347;ci&#263;"
  ]
  node [
    id 8
    label "robi&#263;"
  ]
  node [
    id 9
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 10
    label "pomieszcza&#263;"
  ]
  node [
    id 11
    label "accommodate"
  ]
  node [
    id 12
    label "zmienia&#263;"
  ]
  node [
    id 13
    label "powodowa&#263;"
  ]
  node [
    id 14
    label "venture"
  ]
  node [
    id 15
    label "wpiernicza&#263;"
  ]
  node [
    id 16
    label "okre&#347;la&#263;"
  ]
  node [
    id 17
    label "mi&#281;sny"
  ]
  node [
    id 18
    label "specjalny"
  ]
  node [
    id 19
    label "naturalny"
  ]
  node [
    id 20
    label "hodowlany"
  ]
  node [
    id 21
    label "sklep"
  ]
  node [
    id 22
    label "bia&#322;kowy"
  ]
  node [
    id 23
    label "follow-up"
  ]
  node [
    id 24
    label "term"
  ]
  node [
    id 25
    label "ustalenie"
  ]
  node [
    id 26
    label "appointment"
  ]
  node [
    id 27
    label "localization"
  ]
  node [
    id 28
    label "ozdobnik"
  ]
  node [
    id 29
    label "denomination"
  ]
  node [
    id 30
    label "zdecydowanie"
  ]
  node [
    id 31
    label "przewidzenie"
  ]
  node [
    id 32
    label "wyra&#380;enie"
  ]
  node [
    id 33
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 34
    label "decyzja"
  ]
  node [
    id 35
    label "pewnie"
  ]
  node [
    id 36
    label "zdecydowany"
  ]
  node [
    id 37
    label "zauwa&#380;alnie"
  ]
  node [
    id 38
    label "oddzia&#322;anie"
  ]
  node [
    id 39
    label "podj&#281;cie"
  ]
  node [
    id 40
    label "cecha"
  ]
  node [
    id 41
    label "resoluteness"
  ]
  node [
    id 42
    label "judgment"
  ]
  node [
    id 43
    label "zrobienie"
  ]
  node [
    id 44
    label "leksem"
  ]
  node [
    id 45
    label "sformu&#322;owanie"
  ]
  node [
    id 46
    label "zdarzenie_si&#281;"
  ]
  node [
    id 47
    label "poj&#281;cie"
  ]
  node [
    id 48
    label "poinformowanie"
  ]
  node [
    id 49
    label "wording"
  ]
  node [
    id 50
    label "kompozycja"
  ]
  node [
    id 51
    label "oznaczenie"
  ]
  node [
    id 52
    label "znak_j&#281;zykowy"
  ]
  node [
    id 53
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 54
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 55
    label "grupa_imienna"
  ]
  node [
    id 56
    label "jednostka_leksykalna"
  ]
  node [
    id 57
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 58
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 59
    label "ujawnienie"
  ]
  node [
    id 60
    label "affirmation"
  ]
  node [
    id 61
    label "zapisanie"
  ]
  node [
    id 62
    label "rzucenie"
  ]
  node [
    id 63
    label "umocnienie"
  ]
  node [
    id 64
    label "spowodowanie"
  ]
  node [
    id 65
    label "informacja"
  ]
  node [
    id 66
    label "czynno&#347;&#263;"
  ]
  node [
    id 67
    label "obliczenie"
  ]
  node [
    id 68
    label "spodziewanie_si&#281;"
  ]
  node [
    id 69
    label "zaplanowanie"
  ]
  node [
    id 70
    label "vision"
  ]
  node [
    id 71
    label "przedmiot"
  ]
  node [
    id 72
    label "dekor"
  ]
  node [
    id 73
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 74
    label "wypowied&#378;"
  ]
  node [
    id 75
    label "ornamentyka"
  ]
  node [
    id 76
    label "ilustracja"
  ]
  node [
    id 77
    label "d&#378;wi&#281;k"
  ]
  node [
    id 78
    label "dekoracja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
]
