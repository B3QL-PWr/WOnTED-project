graph [
  node [
    id 0
    label "typowy"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 3
    label "lambo"
    origin "text"
  ]
  node [
    id 4
    label "jak&#380;e"
    origin "text"
  ]
  node [
    id 5
    label "pi&#281;kne"
    origin "text"
  ]
  node [
    id 6
    label "kraj"
    origin "text"
  ]
  node [
    id 7
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 8
    label "zwyczajny"
  ]
  node [
    id 9
    label "typowo"
  ]
  node [
    id 10
    label "cz&#281;sty"
  ]
  node [
    id 11
    label "zwyk&#322;y"
  ]
  node [
    id 12
    label "przeci&#281;tny"
  ]
  node [
    id 13
    label "oswojony"
  ]
  node [
    id 14
    label "zwyczajnie"
  ]
  node [
    id 15
    label "zwykle"
  ]
  node [
    id 16
    label "na&#322;o&#380;ny"
  ]
  node [
    id 17
    label "okre&#347;lony"
  ]
  node [
    id 18
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 19
    label "nale&#380;ny"
  ]
  node [
    id 20
    label "nale&#380;yty"
  ]
  node [
    id 21
    label "uprawniony"
  ]
  node [
    id 22
    label "zasadniczy"
  ]
  node [
    id 23
    label "stosownie"
  ]
  node [
    id 24
    label "taki"
  ]
  node [
    id 25
    label "charakterystyczny"
  ]
  node [
    id 26
    label "prawdziwy"
  ]
  node [
    id 27
    label "ten"
  ]
  node [
    id 28
    label "dobry"
  ]
  node [
    id 29
    label "cz&#281;sto"
  ]
  node [
    id 30
    label "podmiot"
  ]
  node [
    id 31
    label "wykupienie"
  ]
  node [
    id 32
    label "bycie_w_posiadaniu"
  ]
  node [
    id 33
    label "wykupywanie"
  ]
  node [
    id 34
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 35
    label "byt"
  ]
  node [
    id 36
    label "cz&#322;owiek"
  ]
  node [
    id 37
    label "osobowo&#347;&#263;"
  ]
  node [
    id 38
    label "organizacja"
  ]
  node [
    id 39
    label "prawo"
  ]
  node [
    id 40
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 41
    label "nauka_prawa"
  ]
  node [
    id 42
    label "kupowanie"
  ]
  node [
    id 43
    label "odkupywanie"
  ]
  node [
    id 44
    label "wyczerpywanie"
  ]
  node [
    id 45
    label "wyswobadzanie"
  ]
  node [
    id 46
    label "wyczerpanie"
  ]
  node [
    id 47
    label "odkupienie"
  ]
  node [
    id 48
    label "wyswobodzenie"
  ]
  node [
    id 49
    label "kupienie"
  ]
  node [
    id 50
    label "ransom"
  ]
  node [
    id 51
    label "supersamoch&#243;d"
  ]
  node [
    id 52
    label "samoch&#243;d"
  ]
  node [
    id 53
    label "Lamborghini"
  ]
  node [
    id 54
    label "pojazd_drogowy"
  ]
  node [
    id 55
    label "spryskiwacz"
  ]
  node [
    id 56
    label "most"
  ]
  node [
    id 57
    label "baga&#380;nik"
  ]
  node [
    id 58
    label "silnik"
  ]
  node [
    id 59
    label "dachowanie"
  ]
  node [
    id 60
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 61
    label "pompa_wodna"
  ]
  node [
    id 62
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 63
    label "poduszka_powietrzna"
  ]
  node [
    id 64
    label "tempomat"
  ]
  node [
    id 65
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 66
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 67
    label "deska_rozdzielcza"
  ]
  node [
    id 68
    label "immobilizer"
  ]
  node [
    id 69
    label "t&#322;umik"
  ]
  node [
    id 70
    label "ABS"
  ]
  node [
    id 71
    label "kierownica"
  ]
  node [
    id 72
    label "bak"
  ]
  node [
    id 73
    label "dwu&#347;lad"
  ]
  node [
    id 74
    label "poci&#261;g_drogowy"
  ]
  node [
    id 75
    label "wycieraczka"
  ]
  node [
    id 76
    label "samoch&#243;d_sportowy"
  ]
  node [
    id 77
    label "lamborghini"
  ]
  node [
    id 78
    label "Katar"
  ]
  node [
    id 79
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 80
    label "Mazowsze"
  ]
  node [
    id 81
    label "Libia"
  ]
  node [
    id 82
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 83
    label "Gwatemala"
  ]
  node [
    id 84
    label "Anglia"
  ]
  node [
    id 85
    label "Amazonia"
  ]
  node [
    id 86
    label "Afganistan"
  ]
  node [
    id 87
    label "Ekwador"
  ]
  node [
    id 88
    label "Bordeaux"
  ]
  node [
    id 89
    label "Tad&#380;ykistan"
  ]
  node [
    id 90
    label "Bhutan"
  ]
  node [
    id 91
    label "Argentyna"
  ]
  node [
    id 92
    label "D&#380;ibuti"
  ]
  node [
    id 93
    label "Wenezuela"
  ]
  node [
    id 94
    label "Ukraina"
  ]
  node [
    id 95
    label "Gabon"
  ]
  node [
    id 96
    label "Naddniestrze"
  ]
  node [
    id 97
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 98
    label "Europa_Zachodnia"
  ]
  node [
    id 99
    label "Armagnac"
  ]
  node [
    id 100
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 101
    label "Rwanda"
  ]
  node [
    id 102
    label "Liechtenstein"
  ]
  node [
    id 103
    label "Amhara"
  ]
  node [
    id 104
    label "Sri_Lanka"
  ]
  node [
    id 105
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 106
    label "Zamojszczyzna"
  ]
  node [
    id 107
    label "Madagaskar"
  ]
  node [
    id 108
    label "Tonga"
  ]
  node [
    id 109
    label "Kongo"
  ]
  node [
    id 110
    label "Bangladesz"
  ]
  node [
    id 111
    label "Kanada"
  ]
  node [
    id 112
    label "Ma&#322;opolska"
  ]
  node [
    id 113
    label "Wehrlen"
  ]
  node [
    id 114
    label "Turkiestan"
  ]
  node [
    id 115
    label "Algieria"
  ]
  node [
    id 116
    label "Noworosja"
  ]
  node [
    id 117
    label "Surinam"
  ]
  node [
    id 118
    label "Chile"
  ]
  node [
    id 119
    label "Sahara_Zachodnia"
  ]
  node [
    id 120
    label "Uganda"
  ]
  node [
    id 121
    label "Lubelszczyzna"
  ]
  node [
    id 122
    label "W&#281;gry"
  ]
  node [
    id 123
    label "Mezoameryka"
  ]
  node [
    id 124
    label "Birma"
  ]
  node [
    id 125
    label "Ba&#322;kany"
  ]
  node [
    id 126
    label "Kurdystan"
  ]
  node [
    id 127
    label "Kazachstan"
  ]
  node [
    id 128
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 129
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 130
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 131
    label "Armenia"
  ]
  node [
    id 132
    label "Tuwalu"
  ]
  node [
    id 133
    label "Timor_Wschodni"
  ]
  node [
    id 134
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 135
    label "Szkocja"
  ]
  node [
    id 136
    label "Baszkiria"
  ]
  node [
    id 137
    label "Tonkin"
  ]
  node [
    id 138
    label "Maghreb"
  ]
  node [
    id 139
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 140
    label "Izrael"
  ]
  node [
    id 141
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 142
    label "Nadrenia"
  ]
  node [
    id 143
    label "Estonia"
  ]
  node [
    id 144
    label "Komory"
  ]
  node [
    id 145
    label "Podhale"
  ]
  node [
    id 146
    label "Wielkopolska"
  ]
  node [
    id 147
    label "Zabajkale"
  ]
  node [
    id 148
    label "Kamerun"
  ]
  node [
    id 149
    label "Haiti"
  ]
  node [
    id 150
    label "Belize"
  ]
  node [
    id 151
    label "Sierra_Leone"
  ]
  node [
    id 152
    label "Apulia"
  ]
  node [
    id 153
    label "Luksemburg"
  ]
  node [
    id 154
    label "brzeg"
  ]
  node [
    id 155
    label "USA"
  ]
  node [
    id 156
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 157
    label "Barbados"
  ]
  node [
    id 158
    label "San_Marino"
  ]
  node [
    id 159
    label "Bu&#322;garia"
  ]
  node [
    id 160
    label "Wietnam"
  ]
  node [
    id 161
    label "Indonezja"
  ]
  node [
    id 162
    label "Bojkowszczyzna"
  ]
  node [
    id 163
    label "Malawi"
  ]
  node [
    id 164
    label "Francja"
  ]
  node [
    id 165
    label "Zambia"
  ]
  node [
    id 166
    label "Kujawy"
  ]
  node [
    id 167
    label "Angola"
  ]
  node [
    id 168
    label "Liguria"
  ]
  node [
    id 169
    label "Grenada"
  ]
  node [
    id 170
    label "Pamir"
  ]
  node [
    id 171
    label "Nepal"
  ]
  node [
    id 172
    label "Panama"
  ]
  node [
    id 173
    label "Rumunia"
  ]
  node [
    id 174
    label "Indochiny"
  ]
  node [
    id 175
    label "Podlasie"
  ]
  node [
    id 176
    label "Polinezja"
  ]
  node [
    id 177
    label "Kurpie"
  ]
  node [
    id 178
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 179
    label "S&#261;decczyzna"
  ]
  node [
    id 180
    label "Umbria"
  ]
  node [
    id 181
    label "Czarnog&#243;ra"
  ]
  node [
    id 182
    label "Malediwy"
  ]
  node [
    id 183
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 184
    label "S&#322;owacja"
  ]
  node [
    id 185
    label "Karaiby"
  ]
  node [
    id 186
    label "Ukraina_Zachodnia"
  ]
  node [
    id 187
    label "Kielecczyzna"
  ]
  node [
    id 188
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 189
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 190
    label "Egipt"
  ]
  node [
    id 191
    label "Kolumbia"
  ]
  node [
    id 192
    label "Mozambik"
  ]
  node [
    id 193
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 194
    label "Laos"
  ]
  node [
    id 195
    label "Burundi"
  ]
  node [
    id 196
    label "Suazi"
  ]
  node [
    id 197
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 198
    label "Czechy"
  ]
  node [
    id 199
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 200
    label "Wyspy_Marshalla"
  ]
  node [
    id 201
    label "Trynidad_i_Tobago"
  ]
  node [
    id 202
    label "Dominika"
  ]
  node [
    id 203
    label "Palau"
  ]
  node [
    id 204
    label "Syria"
  ]
  node [
    id 205
    label "Skandynawia"
  ]
  node [
    id 206
    label "Gwinea_Bissau"
  ]
  node [
    id 207
    label "Liberia"
  ]
  node [
    id 208
    label "Zimbabwe"
  ]
  node [
    id 209
    label "Polska"
  ]
  node [
    id 210
    label "Jamajka"
  ]
  node [
    id 211
    label "Tyrol"
  ]
  node [
    id 212
    label "Huculszczyzna"
  ]
  node [
    id 213
    label "Bory_Tucholskie"
  ]
  node [
    id 214
    label "Turyngia"
  ]
  node [
    id 215
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 216
    label "Dominikana"
  ]
  node [
    id 217
    label "Senegal"
  ]
  node [
    id 218
    label "Gruzja"
  ]
  node [
    id 219
    label "Chorwacja"
  ]
  node [
    id 220
    label "Togo"
  ]
  node [
    id 221
    label "Meksyk"
  ]
  node [
    id 222
    label "jednostka_administracyjna"
  ]
  node [
    id 223
    label "Macedonia"
  ]
  node [
    id 224
    label "Gujana"
  ]
  node [
    id 225
    label "Zair"
  ]
  node [
    id 226
    label "Kambod&#380;a"
  ]
  node [
    id 227
    label "Albania"
  ]
  node [
    id 228
    label "Mauritius"
  ]
  node [
    id 229
    label "Monako"
  ]
  node [
    id 230
    label "Gwinea"
  ]
  node [
    id 231
    label "Mali"
  ]
  node [
    id 232
    label "Nigeria"
  ]
  node [
    id 233
    label "Kalabria"
  ]
  node [
    id 234
    label "Hercegowina"
  ]
  node [
    id 235
    label "Kostaryka"
  ]
  node [
    id 236
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 237
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 238
    label "Lotaryngia"
  ]
  node [
    id 239
    label "Hanower"
  ]
  node [
    id 240
    label "Paragwaj"
  ]
  node [
    id 241
    label "W&#322;ochy"
  ]
  node [
    id 242
    label "Wyspy_Salomona"
  ]
  node [
    id 243
    label "Seszele"
  ]
  node [
    id 244
    label "Hiszpania"
  ]
  node [
    id 245
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 246
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 247
    label "Walia"
  ]
  node [
    id 248
    label "Boliwia"
  ]
  node [
    id 249
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 250
    label "Opolskie"
  ]
  node [
    id 251
    label "Kirgistan"
  ]
  node [
    id 252
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 253
    label "Irlandia"
  ]
  node [
    id 254
    label "Kampania"
  ]
  node [
    id 255
    label "Czad"
  ]
  node [
    id 256
    label "Irak"
  ]
  node [
    id 257
    label "Lesoto"
  ]
  node [
    id 258
    label "Malta"
  ]
  node [
    id 259
    label "Andora"
  ]
  node [
    id 260
    label "Sand&#380;ak"
  ]
  node [
    id 261
    label "Chiny"
  ]
  node [
    id 262
    label "Filipiny"
  ]
  node [
    id 263
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 264
    label "Syjon"
  ]
  node [
    id 265
    label "Niemcy"
  ]
  node [
    id 266
    label "Kabylia"
  ]
  node [
    id 267
    label "Lombardia"
  ]
  node [
    id 268
    label "Warmia"
  ]
  node [
    id 269
    label "Brazylia"
  ]
  node [
    id 270
    label "Nikaragua"
  ]
  node [
    id 271
    label "Pakistan"
  ]
  node [
    id 272
    label "&#321;emkowszczyzna"
  ]
  node [
    id 273
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 274
    label "Kaszmir"
  ]
  node [
    id 275
    label "Kenia"
  ]
  node [
    id 276
    label "Niger"
  ]
  node [
    id 277
    label "Tunezja"
  ]
  node [
    id 278
    label "Portugalia"
  ]
  node [
    id 279
    label "Fid&#380;i"
  ]
  node [
    id 280
    label "Maroko"
  ]
  node [
    id 281
    label "Botswana"
  ]
  node [
    id 282
    label "Tajlandia"
  ]
  node [
    id 283
    label "Australia"
  ]
  node [
    id 284
    label "&#321;&#243;dzkie"
  ]
  node [
    id 285
    label "Europa_Wschodnia"
  ]
  node [
    id 286
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 287
    label "Burkina_Faso"
  ]
  node [
    id 288
    label "Benin"
  ]
  node [
    id 289
    label "Tanzania"
  ]
  node [
    id 290
    label "interior"
  ]
  node [
    id 291
    label "Indie"
  ]
  node [
    id 292
    label "&#321;otwa"
  ]
  node [
    id 293
    label "Biskupizna"
  ]
  node [
    id 294
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 295
    label "Kiribati"
  ]
  node [
    id 296
    label "Kaukaz"
  ]
  node [
    id 297
    label "Antigua_i_Barbuda"
  ]
  node [
    id 298
    label "Rodezja"
  ]
  node [
    id 299
    label "Afryka_Wschodnia"
  ]
  node [
    id 300
    label "Cypr"
  ]
  node [
    id 301
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 302
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 303
    label "Podkarpacie"
  ]
  node [
    id 304
    label "obszar"
  ]
  node [
    id 305
    label "Peru"
  ]
  node [
    id 306
    label "Toskania"
  ]
  node [
    id 307
    label "Afryka_Zachodnia"
  ]
  node [
    id 308
    label "Austria"
  ]
  node [
    id 309
    label "Podbeskidzie"
  ]
  node [
    id 310
    label "Urugwaj"
  ]
  node [
    id 311
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 312
    label "Jordania"
  ]
  node [
    id 313
    label "Bo&#347;nia"
  ]
  node [
    id 314
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 315
    label "Grecja"
  ]
  node [
    id 316
    label "Azerbejd&#380;an"
  ]
  node [
    id 317
    label "Oceania"
  ]
  node [
    id 318
    label "Turcja"
  ]
  node [
    id 319
    label "Pomorze_Zachodnie"
  ]
  node [
    id 320
    label "Samoa"
  ]
  node [
    id 321
    label "Powi&#347;le"
  ]
  node [
    id 322
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 323
    label "ziemia"
  ]
  node [
    id 324
    label "Oman"
  ]
  node [
    id 325
    label "Sudan"
  ]
  node [
    id 326
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 327
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 328
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 329
    label "Uzbekistan"
  ]
  node [
    id 330
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 331
    label "Honduras"
  ]
  node [
    id 332
    label "Mongolia"
  ]
  node [
    id 333
    label "Portoryko"
  ]
  node [
    id 334
    label "Kaszuby"
  ]
  node [
    id 335
    label "Ko&#322;yma"
  ]
  node [
    id 336
    label "Szlezwik"
  ]
  node [
    id 337
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 338
    label "Serbia"
  ]
  node [
    id 339
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 340
    label "Tajwan"
  ]
  node [
    id 341
    label "Wielka_Brytania"
  ]
  node [
    id 342
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 343
    label "Liban"
  ]
  node [
    id 344
    label "Japonia"
  ]
  node [
    id 345
    label "Ghana"
  ]
  node [
    id 346
    label "Bahrajn"
  ]
  node [
    id 347
    label "Belgia"
  ]
  node [
    id 348
    label "Etiopia"
  ]
  node [
    id 349
    label "Mikronezja"
  ]
  node [
    id 350
    label "Polesie"
  ]
  node [
    id 351
    label "Kuwejt"
  ]
  node [
    id 352
    label "Kerala"
  ]
  node [
    id 353
    label "Mazury"
  ]
  node [
    id 354
    label "Bahamy"
  ]
  node [
    id 355
    label "Rosja"
  ]
  node [
    id 356
    label "Mo&#322;dawia"
  ]
  node [
    id 357
    label "Palestyna"
  ]
  node [
    id 358
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 359
    label "Lauda"
  ]
  node [
    id 360
    label "Azja_Wschodnia"
  ]
  node [
    id 361
    label "Litwa"
  ]
  node [
    id 362
    label "S&#322;owenia"
  ]
  node [
    id 363
    label "Szwajcaria"
  ]
  node [
    id 364
    label "Erytrea"
  ]
  node [
    id 365
    label "Lubuskie"
  ]
  node [
    id 366
    label "Kuba"
  ]
  node [
    id 367
    label "Arabia_Saudyjska"
  ]
  node [
    id 368
    label "Galicja"
  ]
  node [
    id 369
    label "Zakarpacie"
  ]
  node [
    id 370
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 371
    label "Laponia"
  ]
  node [
    id 372
    label "granica_pa&#324;stwa"
  ]
  node [
    id 373
    label "Malezja"
  ]
  node [
    id 374
    label "Korea"
  ]
  node [
    id 375
    label "Yorkshire"
  ]
  node [
    id 376
    label "Bawaria"
  ]
  node [
    id 377
    label "Zag&#243;rze"
  ]
  node [
    id 378
    label "Jemen"
  ]
  node [
    id 379
    label "Nowa_Zelandia"
  ]
  node [
    id 380
    label "Andaluzja"
  ]
  node [
    id 381
    label "Namibia"
  ]
  node [
    id 382
    label "Nauru"
  ]
  node [
    id 383
    label "&#379;ywiecczyzna"
  ]
  node [
    id 384
    label "Brunei"
  ]
  node [
    id 385
    label "Oksytania"
  ]
  node [
    id 386
    label "Opolszczyzna"
  ]
  node [
    id 387
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 388
    label "Kociewie"
  ]
  node [
    id 389
    label "Khitai"
  ]
  node [
    id 390
    label "Mauretania"
  ]
  node [
    id 391
    label "Iran"
  ]
  node [
    id 392
    label "Gambia"
  ]
  node [
    id 393
    label "Somalia"
  ]
  node [
    id 394
    label "Holandia"
  ]
  node [
    id 395
    label "Lasko"
  ]
  node [
    id 396
    label "Turkmenistan"
  ]
  node [
    id 397
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 398
    label "Salwador"
  ]
  node [
    id 399
    label "woda"
  ]
  node [
    id 400
    label "linia"
  ]
  node [
    id 401
    label "zbi&#243;r"
  ]
  node [
    id 402
    label "ekoton"
  ]
  node [
    id 403
    label "str&#261;d"
  ]
  node [
    id 404
    label "koniec"
  ]
  node [
    id 405
    label "plantowa&#263;"
  ]
  node [
    id 406
    label "zapadnia"
  ]
  node [
    id 407
    label "budynek"
  ]
  node [
    id 408
    label "skorupa_ziemska"
  ]
  node [
    id 409
    label "glinowanie"
  ]
  node [
    id 410
    label "martwica"
  ]
  node [
    id 411
    label "teren"
  ]
  node [
    id 412
    label "litosfera"
  ]
  node [
    id 413
    label "penetrator"
  ]
  node [
    id 414
    label "glinowa&#263;"
  ]
  node [
    id 415
    label "domain"
  ]
  node [
    id 416
    label "podglebie"
  ]
  node [
    id 417
    label "kompleks_sorpcyjny"
  ]
  node [
    id 418
    label "miejsce"
  ]
  node [
    id 419
    label "kort"
  ]
  node [
    id 420
    label "czynnik_produkcji"
  ]
  node [
    id 421
    label "pojazd"
  ]
  node [
    id 422
    label "powierzchnia"
  ]
  node [
    id 423
    label "pr&#243;chnica"
  ]
  node [
    id 424
    label "pomieszczenie"
  ]
  node [
    id 425
    label "ryzosfera"
  ]
  node [
    id 426
    label "p&#322;aszczyzna"
  ]
  node [
    id 427
    label "dotleni&#263;"
  ]
  node [
    id 428
    label "glej"
  ]
  node [
    id 429
    label "pa&#324;stwo"
  ]
  node [
    id 430
    label "posadzka"
  ]
  node [
    id 431
    label "geosystem"
  ]
  node [
    id 432
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 433
    label "przestrze&#324;"
  ]
  node [
    id 434
    label "jednostka_organizacyjna"
  ]
  node [
    id 435
    label "struktura"
  ]
  node [
    id 436
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 437
    label "TOPR"
  ]
  node [
    id 438
    label "endecki"
  ]
  node [
    id 439
    label "zesp&#243;&#322;"
  ]
  node [
    id 440
    label "przedstawicielstwo"
  ]
  node [
    id 441
    label "od&#322;am"
  ]
  node [
    id 442
    label "Cepelia"
  ]
  node [
    id 443
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 444
    label "ZBoWiD"
  ]
  node [
    id 445
    label "organization"
  ]
  node [
    id 446
    label "centrala"
  ]
  node [
    id 447
    label "GOPR"
  ]
  node [
    id 448
    label "ZOMO"
  ]
  node [
    id 449
    label "ZMP"
  ]
  node [
    id 450
    label "komitet_koordynacyjny"
  ]
  node [
    id 451
    label "przybud&#243;wka"
  ]
  node [
    id 452
    label "boj&#243;wka"
  ]
  node [
    id 453
    label "p&#243;&#322;noc"
  ]
  node [
    id 454
    label "Kosowo"
  ]
  node [
    id 455
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 456
    label "Zab&#322;ocie"
  ]
  node [
    id 457
    label "zach&#243;d"
  ]
  node [
    id 458
    label "po&#322;udnie"
  ]
  node [
    id 459
    label "Pow&#261;zki"
  ]
  node [
    id 460
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 461
    label "Piotrowo"
  ]
  node [
    id 462
    label "Olszanica"
  ]
  node [
    id 463
    label "Ruda_Pabianicka"
  ]
  node [
    id 464
    label "holarktyka"
  ]
  node [
    id 465
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 466
    label "Ludwin&#243;w"
  ]
  node [
    id 467
    label "Arktyka"
  ]
  node [
    id 468
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 469
    label "Zabu&#380;e"
  ]
  node [
    id 470
    label "antroposfera"
  ]
  node [
    id 471
    label "Neogea"
  ]
  node [
    id 472
    label "terytorium"
  ]
  node [
    id 473
    label "Syberia_Zachodnia"
  ]
  node [
    id 474
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 475
    label "zakres"
  ]
  node [
    id 476
    label "pas_planetoid"
  ]
  node [
    id 477
    label "Syberia_Wschodnia"
  ]
  node [
    id 478
    label "Antarktyka"
  ]
  node [
    id 479
    label "Rakowice"
  ]
  node [
    id 480
    label "akrecja"
  ]
  node [
    id 481
    label "wymiar"
  ]
  node [
    id 482
    label "&#321;&#281;g"
  ]
  node [
    id 483
    label "Kresy_Zachodnie"
  ]
  node [
    id 484
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 485
    label "wsch&#243;d"
  ]
  node [
    id 486
    label "Notogea"
  ]
  node [
    id 487
    label "inti"
  ]
  node [
    id 488
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 489
    label "sol"
  ]
  node [
    id 490
    label "baht"
  ]
  node [
    id 491
    label "boliviano"
  ]
  node [
    id 492
    label "dong"
  ]
  node [
    id 493
    label "Annam"
  ]
  node [
    id 494
    label "colon"
  ]
  node [
    id 495
    label "Ameryka_Centralna"
  ]
  node [
    id 496
    label "Piemont"
  ]
  node [
    id 497
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 498
    label "NATO"
  ]
  node [
    id 499
    label "Sardynia"
  ]
  node [
    id 500
    label "Italia"
  ]
  node [
    id 501
    label "strefa_euro"
  ]
  node [
    id 502
    label "Ok&#281;cie"
  ]
  node [
    id 503
    label "Karyntia"
  ]
  node [
    id 504
    label "Romania"
  ]
  node [
    id 505
    label "Warszawa"
  ]
  node [
    id 506
    label "lir"
  ]
  node [
    id 507
    label "Sycylia"
  ]
  node [
    id 508
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 509
    label "Ad&#380;aria"
  ]
  node [
    id 510
    label "lari"
  ]
  node [
    id 511
    label "Jukatan"
  ]
  node [
    id 512
    label "dolar_Belize"
  ]
  node [
    id 513
    label "dolar"
  ]
  node [
    id 514
    label "Ohio"
  ]
  node [
    id 515
    label "P&#243;&#322;noc"
  ]
  node [
    id 516
    label "Nowy_York"
  ]
  node [
    id 517
    label "Illinois"
  ]
  node [
    id 518
    label "Po&#322;udnie"
  ]
  node [
    id 519
    label "Kalifornia"
  ]
  node [
    id 520
    label "Wirginia"
  ]
  node [
    id 521
    label "Teksas"
  ]
  node [
    id 522
    label "Waszyngton"
  ]
  node [
    id 523
    label "zielona_karta"
  ]
  node [
    id 524
    label "Massachusetts"
  ]
  node [
    id 525
    label "Alaska"
  ]
  node [
    id 526
    label "Hawaje"
  ]
  node [
    id 527
    label "Maryland"
  ]
  node [
    id 528
    label "Michigan"
  ]
  node [
    id 529
    label "Arizona"
  ]
  node [
    id 530
    label "Georgia"
  ]
  node [
    id 531
    label "stan_wolny"
  ]
  node [
    id 532
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 533
    label "Pensylwania"
  ]
  node [
    id 534
    label "Luizjana"
  ]
  node [
    id 535
    label "Nowy_Meksyk"
  ]
  node [
    id 536
    label "Wuj_Sam"
  ]
  node [
    id 537
    label "Alabama"
  ]
  node [
    id 538
    label "Kansas"
  ]
  node [
    id 539
    label "Oregon"
  ]
  node [
    id 540
    label "Zach&#243;d"
  ]
  node [
    id 541
    label "Floryda"
  ]
  node [
    id 542
    label "Oklahoma"
  ]
  node [
    id 543
    label "Hudson"
  ]
  node [
    id 544
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 545
    label "somoni"
  ]
  node [
    id 546
    label "perper"
  ]
  node [
    id 547
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 548
    label "euro"
  ]
  node [
    id 549
    label "Bengal"
  ]
  node [
    id 550
    label "taka"
  ]
  node [
    id 551
    label "Karelia"
  ]
  node [
    id 552
    label "Mari_El"
  ]
  node [
    id 553
    label "Inguszetia"
  ]
  node [
    id 554
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 555
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 556
    label "Udmurcja"
  ]
  node [
    id 557
    label "Newa"
  ]
  node [
    id 558
    label "&#321;adoga"
  ]
  node [
    id 559
    label "Czeczenia"
  ]
  node [
    id 560
    label "Anadyr"
  ]
  node [
    id 561
    label "Syberia"
  ]
  node [
    id 562
    label "Tatarstan"
  ]
  node [
    id 563
    label "Wszechrosja"
  ]
  node [
    id 564
    label "Azja"
  ]
  node [
    id 565
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 566
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 567
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 568
    label "Witim"
  ]
  node [
    id 569
    label "Kamczatka"
  ]
  node [
    id 570
    label "Jama&#322;"
  ]
  node [
    id 571
    label "Dagestan"
  ]
  node [
    id 572
    label "Tuwa"
  ]
  node [
    id 573
    label "car"
  ]
  node [
    id 574
    label "Komi"
  ]
  node [
    id 575
    label "Czuwaszja"
  ]
  node [
    id 576
    label "Chakasja"
  ]
  node [
    id 577
    label "Perm"
  ]
  node [
    id 578
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 579
    label "Ajon"
  ]
  node [
    id 580
    label "Adygeja"
  ]
  node [
    id 581
    label "Dniepr"
  ]
  node [
    id 582
    label "rubel_rosyjski"
  ]
  node [
    id 583
    label "Don"
  ]
  node [
    id 584
    label "Mordowia"
  ]
  node [
    id 585
    label "s&#322;owianofilstwo"
  ]
  node [
    id 586
    label "gourde"
  ]
  node [
    id 587
    label "escudo_angolskie"
  ]
  node [
    id 588
    label "kwanza"
  ]
  node [
    id 589
    label "ariary"
  ]
  node [
    id 590
    label "Ocean_Indyjski"
  ]
  node [
    id 591
    label "frank_malgaski"
  ]
  node [
    id 592
    label "Unia_Europejska"
  ]
  node [
    id 593
    label "Wile&#324;szczyzna"
  ]
  node [
    id 594
    label "Windawa"
  ]
  node [
    id 595
    label "&#379;mud&#378;"
  ]
  node [
    id 596
    label "lit"
  ]
  node [
    id 597
    label "Synaj"
  ]
  node [
    id 598
    label "paraszyt"
  ]
  node [
    id 599
    label "funt_egipski"
  ]
  node [
    id 600
    label "birr"
  ]
  node [
    id 601
    label "negus"
  ]
  node [
    id 602
    label "peso_kolumbijskie"
  ]
  node [
    id 603
    label "Orinoko"
  ]
  node [
    id 604
    label "rial_katarski"
  ]
  node [
    id 605
    label "dram"
  ]
  node [
    id 606
    label "Limburgia"
  ]
  node [
    id 607
    label "gulden"
  ]
  node [
    id 608
    label "Zelandia"
  ]
  node [
    id 609
    label "Niderlandy"
  ]
  node [
    id 610
    label "Brabancja"
  ]
  node [
    id 611
    label "cedi"
  ]
  node [
    id 612
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 613
    label "milrejs"
  ]
  node [
    id 614
    label "cruzado"
  ]
  node [
    id 615
    label "real"
  ]
  node [
    id 616
    label "frank_monakijski"
  ]
  node [
    id 617
    label "Fryburg"
  ]
  node [
    id 618
    label "Bazylea"
  ]
  node [
    id 619
    label "Alpy"
  ]
  node [
    id 620
    label "frank_szwajcarski"
  ]
  node [
    id 621
    label "Helwecja"
  ]
  node [
    id 622
    label "Berno"
  ]
  node [
    id 623
    label "lej_mo&#322;dawski"
  ]
  node [
    id 624
    label "Dniestr"
  ]
  node [
    id 625
    label "Gagauzja"
  ]
  node [
    id 626
    label "Indie_Zachodnie"
  ]
  node [
    id 627
    label "Sikkim"
  ]
  node [
    id 628
    label "Asam"
  ]
  node [
    id 629
    label "rupia_indyjska"
  ]
  node [
    id 630
    label "Indie_Portugalskie"
  ]
  node [
    id 631
    label "Indie_Wschodnie"
  ]
  node [
    id 632
    label "Bollywood"
  ]
  node [
    id 633
    label "Pend&#380;ab"
  ]
  node [
    id 634
    label "boliwar"
  ]
  node [
    id 635
    label "naira"
  ]
  node [
    id 636
    label "frank_gwinejski"
  ]
  node [
    id 637
    label "sum"
  ]
  node [
    id 638
    label "Karaka&#322;pacja"
  ]
  node [
    id 639
    label "dolar_liberyjski"
  ]
  node [
    id 640
    label "Dacja"
  ]
  node [
    id 641
    label "lej_rumu&#324;ski"
  ]
  node [
    id 642
    label "Siedmiogr&#243;d"
  ]
  node [
    id 643
    label "Dobrud&#380;a"
  ]
  node [
    id 644
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 645
    label "dolar_namibijski"
  ]
  node [
    id 646
    label "kuna"
  ]
  node [
    id 647
    label "Rugia"
  ]
  node [
    id 648
    label "Saksonia"
  ]
  node [
    id 649
    label "Dolna_Saksonia"
  ]
  node [
    id 650
    label "Anglosas"
  ]
  node [
    id 651
    label "Hesja"
  ]
  node [
    id 652
    label "Wirtembergia"
  ]
  node [
    id 653
    label "Po&#322;abie"
  ]
  node [
    id 654
    label "Germania"
  ]
  node [
    id 655
    label "Frankonia"
  ]
  node [
    id 656
    label "Badenia"
  ]
  node [
    id 657
    label "Holsztyn"
  ]
  node [
    id 658
    label "marka"
  ]
  node [
    id 659
    label "Szwabia"
  ]
  node [
    id 660
    label "Brandenburgia"
  ]
  node [
    id 661
    label "Niemcy_Zachodnie"
  ]
  node [
    id 662
    label "Westfalia"
  ]
  node [
    id 663
    label "Helgoland"
  ]
  node [
    id 664
    label "Karlsbad"
  ]
  node [
    id 665
    label "Niemcy_Wschodnie"
  ]
  node [
    id 666
    label "korona_w&#281;gierska"
  ]
  node [
    id 667
    label "forint"
  ]
  node [
    id 668
    label "Lipt&#243;w"
  ]
  node [
    id 669
    label "tenge"
  ]
  node [
    id 670
    label "szach"
  ]
  node [
    id 671
    label "Baktria"
  ]
  node [
    id 672
    label "afgani"
  ]
  node [
    id 673
    label "kip"
  ]
  node [
    id 674
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 675
    label "Salzburg"
  ]
  node [
    id 676
    label "Rakuzy"
  ]
  node [
    id 677
    label "Dyja"
  ]
  node [
    id 678
    label "konsulent"
  ]
  node [
    id 679
    label "szyling_austryjacki"
  ]
  node [
    id 680
    label "peso_urugwajskie"
  ]
  node [
    id 681
    label "rial_jeme&#324;ski"
  ]
  node [
    id 682
    label "korona_esto&#324;ska"
  ]
  node [
    id 683
    label "Inflanty"
  ]
  node [
    id 684
    label "marka_esto&#324;ska"
  ]
  node [
    id 685
    label "tala"
  ]
  node [
    id 686
    label "Podole"
  ]
  node [
    id 687
    label "Wsch&#243;d"
  ]
  node [
    id 688
    label "Naddnieprze"
  ]
  node [
    id 689
    label "Ma&#322;orosja"
  ]
  node [
    id 690
    label "Wo&#322;y&#324;"
  ]
  node [
    id 691
    label "Nadbu&#380;e"
  ]
  node [
    id 692
    label "hrywna"
  ]
  node [
    id 693
    label "Zaporo&#380;e"
  ]
  node [
    id 694
    label "Krym"
  ]
  node [
    id 695
    label "Przykarpacie"
  ]
  node [
    id 696
    label "Kozaczyzna"
  ]
  node [
    id 697
    label "karbowaniec"
  ]
  node [
    id 698
    label "riel"
  ]
  node [
    id 699
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 700
    label "kyat"
  ]
  node [
    id 701
    label "Arakan"
  ]
  node [
    id 702
    label "funt_liba&#324;ski"
  ]
  node [
    id 703
    label "Mariany"
  ]
  node [
    id 704
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 705
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 706
    label "dinar_algierski"
  ]
  node [
    id 707
    label "ringgit"
  ]
  node [
    id 708
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 709
    label "Borneo"
  ]
  node [
    id 710
    label "peso_dominika&#324;skie"
  ]
  node [
    id 711
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 712
    label "peso_kuba&#324;skie"
  ]
  node [
    id 713
    label "lira_izraelska"
  ]
  node [
    id 714
    label "szekel"
  ]
  node [
    id 715
    label "Galilea"
  ]
  node [
    id 716
    label "Judea"
  ]
  node [
    id 717
    label "tolar"
  ]
  node [
    id 718
    label "frank_luksemburski"
  ]
  node [
    id 719
    label "lempira"
  ]
  node [
    id 720
    label "Pozna&#324;"
  ]
  node [
    id 721
    label "lira_malta&#324;ska"
  ]
  node [
    id 722
    label "Gozo"
  ]
  node [
    id 723
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 724
    label "Paros"
  ]
  node [
    id 725
    label "Epir"
  ]
  node [
    id 726
    label "panhellenizm"
  ]
  node [
    id 727
    label "Eubea"
  ]
  node [
    id 728
    label "Rodos"
  ]
  node [
    id 729
    label "Achaja"
  ]
  node [
    id 730
    label "Termopile"
  ]
  node [
    id 731
    label "Attyka"
  ]
  node [
    id 732
    label "Hellada"
  ]
  node [
    id 733
    label "Etolia"
  ]
  node [
    id 734
    label "palestra"
  ]
  node [
    id 735
    label "Kreta"
  ]
  node [
    id 736
    label "drachma"
  ]
  node [
    id 737
    label "Olimp"
  ]
  node [
    id 738
    label "Tesalia"
  ]
  node [
    id 739
    label "Peloponez"
  ]
  node [
    id 740
    label "Eolia"
  ]
  node [
    id 741
    label "Beocja"
  ]
  node [
    id 742
    label "Parnas"
  ]
  node [
    id 743
    label "Lesbos"
  ]
  node [
    id 744
    label "Atlantyk"
  ]
  node [
    id 745
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 746
    label "Ulster"
  ]
  node [
    id 747
    label "funt_irlandzki"
  ]
  node [
    id 748
    label "Buriaci"
  ]
  node [
    id 749
    label "tugrik"
  ]
  node [
    id 750
    label "ajmak"
  ]
  node [
    id 751
    label "denar_macedo&#324;ski"
  ]
  node [
    id 752
    label "Pikardia"
  ]
  node [
    id 753
    label "Masyw_Centralny"
  ]
  node [
    id 754
    label "Akwitania"
  ]
  node [
    id 755
    label "Alzacja"
  ]
  node [
    id 756
    label "Sekwana"
  ]
  node [
    id 757
    label "Langwedocja"
  ]
  node [
    id 758
    label "Martynika"
  ]
  node [
    id 759
    label "Bretania"
  ]
  node [
    id 760
    label "Sabaudia"
  ]
  node [
    id 761
    label "Korsyka"
  ]
  node [
    id 762
    label "Normandia"
  ]
  node [
    id 763
    label "Gaskonia"
  ]
  node [
    id 764
    label "Burgundia"
  ]
  node [
    id 765
    label "frank_francuski"
  ]
  node [
    id 766
    label "Wandea"
  ]
  node [
    id 767
    label "Prowansja"
  ]
  node [
    id 768
    label "Gwadelupa"
  ]
  node [
    id 769
    label "lew"
  ]
  node [
    id 770
    label "c&#243;rdoba"
  ]
  node [
    id 771
    label "dolar_Zimbabwe"
  ]
  node [
    id 772
    label "frank_rwandyjski"
  ]
  node [
    id 773
    label "kwacha_zambijska"
  ]
  node [
    id 774
    label "&#322;at"
  ]
  node [
    id 775
    label "Kurlandia"
  ]
  node [
    id 776
    label "Liwonia"
  ]
  node [
    id 777
    label "rubel_&#322;otewski"
  ]
  node [
    id 778
    label "Himalaje"
  ]
  node [
    id 779
    label "rupia_nepalska"
  ]
  node [
    id 780
    label "funt_suda&#324;ski"
  ]
  node [
    id 781
    label "dolar_bahamski"
  ]
  node [
    id 782
    label "Wielka_Bahama"
  ]
  node [
    id 783
    label "Pa&#322;uki"
  ]
  node [
    id 784
    label "Wolin"
  ]
  node [
    id 785
    label "z&#322;oty"
  ]
  node [
    id 786
    label "So&#322;a"
  ]
  node [
    id 787
    label "Suwalszczyzna"
  ]
  node [
    id 788
    label "Krajna"
  ]
  node [
    id 789
    label "barwy_polskie"
  ]
  node [
    id 790
    label "Izera"
  ]
  node [
    id 791
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 792
    label "Kaczawa"
  ]
  node [
    id 793
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 794
    label "Wis&#322;a"
  ]
  node [
    id 795
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 796
    label "Antyle"
  ]
  node [
    id 797
    label "dolar_Tuvalu"
  ]
  node [
    id 798
    label "dinar_iracki"
  ]
  node [
    id 799
    label "korona_s&#322;owacka"
  ]
  node [
    id 800
    label "Turiec"
  ]
  node [
    id 801
    label "jen"
  ]
  node [
    id 802
    label "jinja"
  ]
  node [
    id 803
    label "Okinawa"
  ]
  node [
    id 804
    label "Japonica"
  ]
  node [
    id 805
    label "manat_turkme&#324;ski"
  ]
  node [
    id 806
    label "szyling_kenijski"
  ]
  node [
    id 807
    label "peso_chilijskie"
  ]
  node [
    id 808
    label "Zanzibar"
  ]
  node [
    id 809
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 810
    label "peso_filipi&#324;skie"
  ]
  node [
    id 811
    label "Cebu"
  ]
  node [
    id 812
    label "Sahara"
  ]
  node [
    id 813
    label "Tasmania"
  ]
  node [
    id 814
    label "Nowy_&#346;wiat"
  ]
  node [
    id 815
    label "dolar_australijski"
  ]
  node [
    id 816
    label "Quebec"
  ]
  node [
    id 817
    label "dolar_kanadyjski"
  ]
  node [
    id 818
    label "Nowa_Fundlandia"
  ]
  node [
    id 819
    label "quetzal"
  ]
  node [
    id 820
    label "Manica"
  ]
  node [
    id 821
    label "escudo_mozambickie"
  ]
  node [
    id 822
    label "Cabo_Delgado"
  ]
  node [
    id 823
    label "Inhambane"
  ]
  node [
    id 824
    label "Maputo"
  ]
  node [
    id 825
    label "Gaza"
  ]
  node [
    id 826
    label "Niasa"
  ]
  node [
    id 827
    label "Nampula"
  ]
  node [
    id 828
    label "metical"
  ]
  node [
    id 829
    label "frank_tunezyjski"
  ]
  node [
    id 830
    label "dinar_tunezyjski"
  ]
  node [
    id 831
    label "lud"
  ]
  node [
    id 832
    label "frank_kongijski"
  ]
  node [
    id 833
    label "peso_argenty&#324;skie"
  ]
  node [
    id 834
    label "dinar_Bahrajnu"
  ]
  node [
    id 835
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 836
    label "escudo_portugalskie"
  ]
  node [
    id 837
    label "Melanezja"
  ]
  node [
    id 838
    label "dolar_Fid&#380;i"
  ]
  node [
    id 839
    label "d&#380;amahirijja"
  ]
  node [
    id 840
    label "dinar_libijski"
  ]
  node [
    id 841
    label "balboa"
  ]
  node [
    id 842
    label "dolar_surinamski"
  ]
  node [
    id 843
    label "dolar_Brunei"
  ]
  node [
    id 844
    label "Estremadura"
  ]
  node [
    id 845
    label "Kastylia"
  ]
  node [
    id 846
    label "Rzym_Zachodni"
  ]
  node [
    id 847
    label "Aragonia"
  ]
  node [
    id 848
    label "hacjender"
  ]
  node [
    id 849
    label "Asturia"
  ]
  node [
    id 850
    label "Baskonia"
  ]
  node [
    id 851
    label "Majorka"
  ]
  node [
    id 852
    label "Walencja"
  ]
  node [
    id 853
    label "peseta"
  ]
  node [
    id 854
    label "Katalonia"
  ]
  node [
    id 855
    label "Luksemburgia"
  ]
  node [
    id 856
    label "frank_belgijski"
  ]
  node [
    id 857
    label "Walonia"
  ]
  node [
    id 858
    label "Flandria"
  ]
  node [
    id 859
    label "dolar_guja&#324;ski"
  ]
  node [
    id 860
    label "dolar_Barbadosu"
  ]
  node [
    id 861
    label "korona_czeska"
  ]
  node [
    id 862
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 863
    label "Wojwodina"
  ]
  node [
    id 864
    label "dinar_serbski"
  ]
  node [
    id 865
    label "funt_syryjski"
  ]
  node [
    id 866
    label "alawizm"
  ]
  node [
    id 867
    label "Szantung"
  ]
  node [
    id 868
    label "Chiny_Zachodnie"
  ]
  node [
    id 869
    label "Kuantung"
  ]
  node [
    id 870
    label "D&#380;ungaria"
  ]
  node [
    id 871
    label "yuan"
  ]
  node [
    id 872
    label "Hongkong"
  ]
  node [
    id 873
    label "Chiny_Wschodnie"
  ]
  node [
    id 874
    label "Guangdong"
  ]
  node [
    id 875
    label "Junnan"
  ]
  node [
    id 876
    label "Mand&#380;uria"
  ]
  node [
    id 877
    label "Syczuan"
  ]
  node [
    id 878
    label "zair"
  ]
  node [
    id 879
    label "Katanga"
  ]
  node [
    id 880
    label "ugija"
  ]
  node [
    id 881
    label "dalasi"
  ]
  node [
    id 882
    label "funt_cypryjski"
  ]
  node [
    id 883
    label "Afrodyzje"
  ]
  node [
    id 884
    label "para"
  ]
  node [
    id 885
    label "frank_alba&#324;ski"
  ]
  node [
    id 886
    label "lek"
  ]
  node [
    id 887
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 888
    label "dolar_jamajski"
  ]
  node [
    id 889
    label "kafar"
  ]
  node [
    id 890
    label "Ocean_Spokojny"
  ]
  node [
    id 891
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 892
    label "som"
  ]
  node [
    id 893
    label "guarani"
  ]
  node [
    id 894
    label "rial_ira&#324;ski"
  ]
  node [
    id 895
    label "mu&#322;&#322;a"
  ]
  node [
    id 896
    label "Persja"
  ]
  node [
    id 897
    label "Jawa"
  ]
  node [
    id 898
    label "Sumatra"
  ]
  node [
    id 899
    label "rupia_indonezyjska"
  ]
  node [
    id 900
    label "Nowa_Gwinea"
  ]
  node [
    id 901
    label "Moluki"
  ]
  node [
    id 902
    label "szyling_somalijski"
  ]
  node [
    id 903
    label "szyling_ugandyjski"
  ]
  node [
    id 904
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 905
    label "Ujgur"
  ]
  node [
    id 906
    label "Azja_Mniejsza"
  ]
  node [
    id 907
    label "lira_turecka"
  ]
  node [
    id 908
    label "Pireneje"
  ]
  node [
    id 909
    label "nakfa"
  ]
  node [
    id 910
    label "won"
  ]
  node [
    id 911
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 912
    label "&#346;wite&#378;"
  ]
  node [
    id 913
    label "dinar_kuwejcki"
  ]
  node [
    id 914
    label "Nachiczewan"
  ]
  node [
    id 915
    label "manat_azerski"
  ]
  node [
    id 916
    label "Karabach"
  ]
  node [
    id 917
    label "dolar_Kiribati"
  ]
  node [
    id 918
    label "moszaw"
  ]
  node [
    id 919
    label "Kanaan"
  ]
  node [
    id 920
    label "Aruba"
  ]
  node [
    id 921
    label "Kajmany"
  ]
  node [
    id 922
    label "Anguilla"
  ]
  node [
    id 923
    label "Mogielnica"
  ]
  node [
    id 924
    label "jezioro"
  ]
  node [
    id 925
    label "Rumelia"
  ]
  node [
    id 926
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 927
    label "Poprad"
  ]
  node [
    id 928
    label "Tatry"
  ]
  node [
    id 929
    label "Podtatrze"
  ]
  node [
    id 930
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 931
    label "Austro-W&#281;gry"
  ]
  node [
    id 932
    label "Biskupice"
  ]
  node [
    id 933
    label "Iwanowice"
  ]
  node [
    id 934
    label "Ziemia_Sandomierska"
  ]
  node [
    id 935
    label "Rogo&#378;nik"
  ]
  node [
    id 936
    label "Ropa"
  ]
  node [
    id 937
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 938
    label "Karpaty"
  ]
  node [
    id 939
    label "Beskidy_Zachodnie"
  ]
  node [
    id 940
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 941
    label "Beskid_Niski"
  ]
  node [
    id 942
    label "Etruria"
  ]
  node [
    id 943
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 944
    label "Bojanowo"
  ]
  node [
    id 945
    label "Obra"
  ]
  node [
    id 946
    label "Wilkowo_Polskie"
  ]
  node [
    id 947
    label "Dobra"
  ]
  node [
    id 948
    label "Buriacja"
  ]
  node [
    id 949
    label "Rozewie"
  ]
  node [
    id 950
    label "&#346;l&#261;sk"
  ]
  node [
    id 951
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 952
    label "Norwegia"
  ]
  node [
    id 953
    label "Szwecja"
  ]
  node [
    id 954
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 955
    label "Finlandia"
  ]
  node [
    id 956
    label "Wiktoria"
  ]
  node [
    id 957
    label "Guernsey"
  ]
  node [
    id 958
    label "Conrad"
  ]
  node [
    id 959
    label "funt_szterling"
  ]
  node [
    id 960
    label "Portland"
  ]
  node [
    id 961
    label "El&#380;bieta_I"
  ]
  node [
    id 962
    label "Kornwalia"
  ]
  node [
    id 963
    label "Amazonka"
  ]
  node [
    id 964
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 965
    label "Imperium_Rosyjskie"
  ]
  node [
    id 966
    label "Moza"
  ]
  node [
    id 967
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 968
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 969
    label "Paj&#281;czno"
  ]
  node [
    id 970
    label "Tar&#322;&#243;w"
  ]
  node [
    id 971
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 972
    label "Gop&#322;o"
  ]
  node [
    id 973
    label "Jerozolima"
  ]
  node [
    id 974
    label "Dolna_Frankonia"
  ]
  node [
    id 975
    label "funt_szkocki"
  ]
  node [
    id 976
    label "Kaledonia"
  ]
  node [
    id 977
    label "Abchazja"
  ]
  node [
    id 978
    label "Sarmata"
  ]
  node [
    id 979
    label "Eurazja"
  ]
  node [
    id 980
    label "Mariensztat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 769
  ]
  edge [
    source 6
    target 770
  ]
  edge [
    source 6
    target 771
  ]
  edge [
    source 6
    target 772
  ]
  edge [
    source 6
    target 773
  ]
  edge [
    source 6
    target 774
  ]
  edge [
    source 6
    target 775
  ]
  edge [
    source 6
    target 776
  ]
  edge [
    source 6
    target 777
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 6
    target 784
  ]
  edge [
    source 6
    target 785
  ]
  edge [
    source 6
    target 786
  ]
  edge [
    source 6
    target 787
  ]
  edge [
    source 6
    target 788
  ]
  edge [
    source 6
    target 789
  ]
  edge [
    source 6
    target 790
  ]
  edge [
    source 6
    target 791
  ]
  edge [
    source 6
    target 792
  ]
  edge [
    source 6
    target 793
  ]
  edge [
    source 6
    target 794
  ]
  edge [
    source 6
    target 795
  ]
  edge [
    source 6
    target 796
  ]
  edge [
    source 6
    target 797
  ]
  edge [
    source 6
    target 798
  ]
  edge [
    source 6
    target 799
  ]
  edge [
    source 6
    target 800
  ]
  edge [
    source 6
    target 801
  ]
  edge [
    source 6
    target 802
  ]
  edge [
    source 6
    target 803
  ]
  edge [
    source 6
    target 804
  ]
  edge [
    source 6
    target 805
  ]
  edge [
    source 6
    target 806
  ]
  edge [
    source 6
    target 807
  ]
  edge [
    source 6
    target 808
  ]
  edge [
    source 6
    target 809
  ]
  edge [
    source 6
    target 810
  ]
  edge [
    source 6
    target 811
  ]
  edge [
    source 6
    target 812
  ]
  edge [
    source 6
    target 813
  ]
  edge [
    source 6
    target 814
  ]
  edge [
    source 6
    target 815
  ]
  edge [
    source 6
    target 816
  ]
  edge [
    source 6
    target 817
  ]
  edge [
    source 6
    target 818
  ]
  edge [
    source 6
    target 819
  ]
  edge [
    source 6
    target 820
  ]
  edge [
    source 6
    target 821
  ]
  edge [
    source 6
    target 822
  ]
  edge [
    source 6
    target 823
  ]
  edge [
    source 6
    target 824
  ]
  edge [
    source 6
    target 825
  ]
  edge [
    source 6
    target 826
  ]
  edge [
    source 6
    target 827
  ]
  edge [
    source 6
    target 828
  ]
  edge [
    source 6
    target 829
  ]
  edge [
    source 6
    target 830
  ]
  edge [
    source 6
    target 831
  ]
  edge [
    source 6
    target 832
  ]
  edge [
    source 6
    target 833
  ]
  edge [
    source 6
    target 834
  ]
  edge [
    source 6
    target 835
  ]
  edge [
    source 6
    target 836
  ]
  edge [
    source 6
    target 837
  ]
  edge [
    source 6
    target 838
  ]
  edge [
    source 6
    target 839
  ]
  edge [
    source 6
    target 840
  ]
  edge [
    source 6
    target 841
  ]
  edge [
    source 6
    target 842
  ]
  edge [
    source 6
    target 843
  ]
  edge [
    source 6
    target 844
  ]
  edge [
    source 6
    target 845
  ]
  edge [
    source 6
    target 846
  ]
  edge [
    source 6
    target 847
  ]
  edge [
    source 6
    target 848
  ]
  edge [
    source 6
    target 849
  ]
  edge [
    source 6
    target 850
  ]
  edge [
    source 6
    target 851
  ]
  edge [
    source 6
    target 852
  ]
  edge [
    source 6
    target 853
  ]
  edge [
    source 6
    target 854
  ]
  edge [
    source 6
    target 855
  ]
  edge [
    source 6
    target 856
  ]
  edge [
    source 6
    target 857
  ]
  edge [
    source 6
    target 858
  ]
  edge [
    source 6
    target 859
  ]
  edge [
    source 6
    target 860
  ]
  edge [
    source 6
    target 861
  ]
  edge [
    source 6
    target 862
  ]
  edge [
    source 6
    target 863
  ]
  edge [
    source 6
    target 864
  ]
  edge [
    source 6
    target 865
  ]
  edge [
    source 6
    target 866
  ]
  edge [
    source 6
    target 867
  ]
  edge [
    source 6
    target 868
  ]
  edge [
    source 6
    target 869
  ]
  edge [
    source 6
    target 870
  ]
  edge [
    source 6
    target 871
  ]
  edge [
    source 6
    target 872
  ]
  edge [
    source 6
    target 873
  ]
  edge [
    source 6
    target 874
  ]
  edge [
    source 6
    target 875
  ]
  edge [
    source 6
    target 876
  ]
  edge [
    source 6
    target 877
  ]
  edge [
    source 6
    target 878
  ]
  edge [
    source 6
    target 879
  ]
  edge [
    source 6
    target 880
  ]
  edge [
    source 6
    target 881
  ]
  edge [
    source 6
    target 882
  ]
  edge [
    source 6
    target 883
  ]
  edge [
    source 6
    target 884
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 6
    target 892
  ]
  edge [
    source 6
    target 893
  ]
  edge [
    source 6
    target 894
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 896
  ]
  edge [
    source 6
    target 897
  ]
  edge [
    source 6
    target 898
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 904
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 918
  ]
  edge [
    source 6
    target 919
  ]
  edge [
    source 6
    target 920
  ]
  edge [
    source 6
    target 921
  ]
  edge [
    source 6
    target 922
  ]
  edge [
    source 6
    target 923
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 925
  ]
  edge [
    source 6
    target 926
  ]
  edge [
    source 6
    target 927
  ]
  edge [
    source 6
    target 928
  ]
  edge [
    source 6
    target 929
  ]
  edge [
    source 6
    target 930
  ]
  edge [
    source 6
    target 931
  ]
  edge [
    source 6
    target 932
  ]
  edge [
    source 6
    target 933
  ]
  edge [
    source 6
    target 934
  ]
  edge [
    source 6
    target 935
  ]
  edge [
    source 6
    target 936
  ]
  edge [
    source 6
    target 937
  ]
  edge [
    source 6
    target 938
  ]
  edge [
    source 6
    target 939
  ]
  edge [
    source 6
    target 940
  ]
  edge [
    source 6
    target 941
  ]
  edge [
    source 6
    target 942
  ]
  edge [
    source 6
    target 943
  ]
  edge [
    source 6
    target 944
  ]
  edge [
    source 6
    target 945
  ]
  edge [
    source 6
    target 946
  ]
  edge [
    source 6
    target 947
  ]
  edge [
    source 6
    target 948
  ]
  edge [
    source 6
    target 949
  ]
  edge [
    source 6
    target 950
  ]
  edge [
    source 6
    target 951
  ]
  edge [
    source 6
    target 952
  ]
  edge [
    source 6
    target 953
  ]
  edge [
    source 6
    target 954
  ]
  edge [
    source 6
    target 955
  ]
  edge [
    source 6
    target 956
  ]
  edge [
    source 6
    target 957
  ]
  edge [
    source 6
    target 958
  ]
  edge [
    source 6
    target 959
  ]
  edge [
    source 6
    target 960
  ]
  edge [
    source 6
    target 961
  ]
  edge [
    source 6
    target 962
  ]
  edge [
    source 6
    target 963
  ]
  edge [
    source 6
    target 964
  ]
  edge [
    source 6
    target 965
  ]
  edge [
    source 6
    target 966
  ]
  edge [
    source 6
    target 967
  ]
  edge [
    source 6
    target 968
  ]
  edge [
    source 6
    target 969
  ]
  edge [
    source 6
    target 970
  ]
  edge [
    source 6
    target 971
  ]
  edge [
    source 6
    target 972
  ]
  edge [
    source 6
    target 973
  ]
  edge [
    source 6
    target 974
  ]
  edge [
    source 6
    target 975
  ]
  edge [
    source 6
    target 976
  ]
  edge [
    source 6
    target 977
  ]
  edge [
    source 6
    target 978
  ]
  edge [
    source 6
    target 979
  ]
  edge [
    source 6
    target 980
  ]
]
