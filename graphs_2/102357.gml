graph [
  node [
    id 0
    label "autor"
    origin "text"
  ]
  node [
    id 1
    label "niniejszy"
    origin "text"
  ]
  node [
    id 2
    label "blog"
    origin "text"
  ]
  node [
    id 3
    label "urodzi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "dawno"
    origin "text"
  ]
  node [
    id 6
    label "rok"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "mocno"
    origin "text"
  ]
  node [
    id 9
    label "przedwojenny"
    origin "text"
  ]
  node [
    id 10
    label "wykszta&#322;cenie"
    origin "text"
  ]
  node [
    id 11
    label "matematyk"
    origin "text"
  ]
  node [
    id 12
    label "przez"
    origin "text"
  ]
  node [
    id 13
    label "wiele"
    origin "text"
  ]
  node [
    id 14
    label "lata"
    origin "text"
  ]
  node [
    id 15
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "ten"
    origin "text"
  ]
  node [
    id 17
    label "zaw&#243;d"
    origin "text"
  ]
  node [
    id 18
    label "jako"
    origin "text"
  ]
  node [
    id 19
    label "nauczyciel"
    origin "text"
  ]
  node [
    id 20
    label "akademicki"
    origin "text"
  ]
  node [
    id 21
    label "kilka"
    origin "text"
  ]
  node [
    id 22
    label "wysoki"
    origin "text"
  ]
  node [
    id 23
    label "uczelnia"
    origin "text"
  ]
  node [
    id 24
    label "tym"
    origin "text"
  ]
  node [
    id 25
    label "siebie"
    origin "text"
  ]
  node [
    id 26
    label "ceni&#263;"
    origin "text"
  ]
  node [
    id 27
    label "wysoce"
    origin "text"
  ]
  node [
    id 28
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 29
    label "pracownik"
    origin "text"
  ]
  node [
    id 30
    label "pan"
    origin "text"
  ]
  node [
    id 31
    label "pi&#281;&#263;"
    origin "text"
  ]
  node [
    id 32
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 33
    label "zaszczyt"
    origin "text"
  ]
  node [
    id 34
    label "tak&#380;e"
    origin "text"
  ]
  node [
    id 35
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 36
    label "komitet"
    origin "text"
  ]
  node [
    id 37
    label "prognoza"
    origin "text"
  ]
  node [
    id 38
    label "polska"
    origin "text"
  ]
  node [
    id 39
    label "plus"
    origin "text"
  ]
  node [
    id 40
    label "szczyci&#263;"
    origin "text"
  ]
  node [
    id 41
    label "wchodzi&#263;"
    origin "text"
  ]
  node [
    id 42
    label "sk&#322;ad"
    origin "text"
  ]
  node [
    id 43
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 44
    label "programista"
    origin "text"
  ]
  node [
    id 45
    label "pierwsza"
    origin "text"
  ]
  node [
    id 46
    label "polski"
    origin "text"
  ]
  node [
    id 47
    label "maszyna"
    origin "text"
  ]
  node [
    id 48
    label "matematyczny"
    origin "text"
  ]
  node [
    id 49
    label "xyz"
    origin "text"
  ]
  node [
    id 50
    label "kszta&#322;ciciel"
  ]
  node [
    id 51
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 52
    label "tworzyciel"
  ]
  node [
    id 53
    label "wykonawca"
  ]
  node [
    id 54
    label "pomys&#322;odawca"
  ]
  node [
    id 55
    label "&#347;w"
  ]
  node [
    id 56
    label "inicjator"
  ]
  node [
    id 57
    label "podmiot_gospodarczy"
  ]
  node [
    id 58
    label "artysta"
  ]
  node [
    id 59
    label "cz&#322;owiek"
  ]
  node [
    id 60
    label "muzyk"
  ]
  node [
    id 61
    label "okre&#347;lony"
  ]
  node [
    id 62
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 63
    label "komcio"
  ]
  node [
    id 64
    label "blogosfera"
  ]
  node [
    id 65
    label "pami&#281;tnik"
  ]
  node [
    id 66
    label "strona"
  ]
  node [
    id 67
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 68
    label "pami&#261;tka"
  ]
  node [
    id 69
    label "notes"
  ]
  node [
    id 70
    label "zapiski"
  ]
  node [
    id 71
    label "raptularz"
  ]
  node [
    id 72
    label "album"
  ]
  node [
    id 73
    label "utw&#243;r_epicki"
  ]
  node [
    id 74
    label "kartka"
  ]
  node [
    id 75
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 76
    label "logowanie"
  ]
  node [
    id 77
    label "plik"
  ]
  node [
    id 78
    label "s&#261;d"
  ]
  node [
    id 79
    label "adres_internetowy"
  ]
  node [
    id 80
    label "linia"
  ]
  node [
    id 81
    label "serwis_internetowy"
  ]
  node [
    id 82
    label "posta&#263;"
  ]
  node [
    id 83
    label "bok"
  ]
  node [
    id 84
    label "skr&#281;canie"
  ]
  node [
    id 85
    label "skr&#281;ca&#263;"
  ]
  node [
    id 86
    label "orientowanie"
  ]
  node [
    id 87
    label "skr&#281;ci&#263;"
  ]
  node [
    id 88
    label "uj&#281;cie"
  ]
  node [
    id 89
    label "zorientowanie"
  ]
  node [
    id 90
    label "ty&#322;"
  ]
  node [
    id 91
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 92
    label "fragment"
  ]
  node [
    id 93
    label "layout"
  ]
  node [
    id 94
    label "obiekt"
  ]
  node [
    id 95
    label "zorientowa&#263;"
  ]
  node [
    id 96
    label "pagina"
  ]
  node [
    id 97
    label "podmiot"
  ]
  node [
    id 98
    label "g&#243;ra"
  ]
  node [
    id 99
    label "orientowa&#263;"
  ]
  node [
    id 100
    label "voice"
  ]
  node [
    id 101
    label "orientacja"
  ]
  node [
    id 102
    label "prz&#243;d"
  ]
  node [
    id 103
    label "internet"
  ]
  node [
    id 104
    label "powierzchnia"
  ]
  node [
    id 105
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 106
    label "forma"
  ]
  node [
    id 107
    label "skr&#281;cenie"
  ]
  node [
    id 108
    label "zbi&#243;r"
  ]
  node [
    id 109
    label "komentarz"
  ]
  node [
    id 110
    label "powi&#263;"
  ]
  node [
    id 111
    label "zlec"
  ]
  node [
    id 112
    label "porodzi&#263;"
  ]
  node [
    id 113
    label "zrobi&#263;"
  ]
  node [
    id 114
    label "narodzi&#263;"
  ]
  node [
    id 115
    label "engender"
  ]
  node [
    id 116
    label "post&#261;pi&#263;"
  ]
  node [
    id 117
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 118
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 119
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 120
    label "zorganizowa&#263;"
  ]
  node [
    id 121
    label "appoint"
  ]
  node [
    id 122
    label "wystylizowa&#263;"
  ]
  node [
    id 123
    label "cause"
  ]
  node [
    id 124
    label "przerobi&#263;"
  ]
  node [
    id 125
    label "nabra&#263;"
  ]
  node [
    id 126
    label "make"
  ]
  node [
    id 127
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 128
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 129
    label "wydali&#263;"
  ]
  node [
    id 130
    label "po&#322;&#243;g"
  ]
  node [
    id 131
    label "zachorowa&#263;"
  ]
  node [
    id 132
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 133
    label "po&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 134
    label "dawny"
  ]
  node [
    id 135
    label "d&#322;ugotrwale"
  ]
  node [
    id 136
    label "wcze&#347;niej"
  ]
  node [
    id 137
    label "ongi&#347;"
  ]
  node [
    id 138
    label "dawnie"
  ]
  node [
    id 139
    label "drzewiej"
  ]
  node [
    id 140
    label "niegdysiejszy"
  ]
  node [
    id 141
    label "kiedy&#347;"
  ]
  node [
    id 142
    label "d&#322;ugotrwa&#322;y"
  ]
  node [
    id 143
    label "d&#322;ugo"
  ]
  node [
    id 144
    label "wcze&#347;niejszy"
  ]
  node [
    id 145
    label "przestarza&#322;y"
  ]
  node [
    id 146
    label "odleg&#322;y"
  ]
  node [
    id 147
    label "przesz&#322;y"
  ]
  node [
    id 148
    label "od_dawna"
  ]
  node [
    id 149
    label "poprzedni"
  ]
  node [
    id 150
    label "d&#322;ugoletni"
  ]
  node [
    id 151
    label "anachroniczny"
  ]
  node [
    id 152
    label "dawniej"
  ]
  node [
    id 153
    label "kombatant"
  ]
  node [
    id 154
    label "stary"
  ]
  node [
    id 155
    label "p&#243;&#322;rocze"
  ]
  node [
    id 156
    label "martwy_sezon"
  ]
  node [
    id 157
    label "kalendarz"
  ]
  node [
    id 158
    label "cykl_astronomiczny"
  ]
  node [
    id 159
    label "pora_roku"
  ]
  node [
    id 160
    label "stulecie"
  ]
  node [
    id 161
    label "kurs"
  ]
  node [
    id 162
    label "czas"
  ]
  node [
    id 163
    label "jubileusz"
  ]
  node [
    id 164
    label "grupa"
  ]
  node [
    id 165
    label "kwarta&#322;"
  ]
  node [
    id 166
    label "miesi&#261;c"
  ]
  node [
    id 167
    label "summer"
  ]
  node [
    id 168
    label "odm&#322;adzanie"
  ]
  node [
    id 169
    label "liga"
  ]
  node [
    id 170
    label "jednostka_systematyczna"
  ]
  node [
    id 171
    label "asymilowanie"
  ]
  node [
    id 172
    label "gromada"
  ]
  node [
    id 173
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 174
    label "asymilowa&#263;"
  ]
  node [
    id 175
    label "egzemplarz"
  ]
  node [
    id 176
    label "Entuzjastki"
  ]
  node [
    id 177
    label "kompozycja"
  ]
  node [
    id 178
    label "Terranie"
  ]
  node [
    id 179
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 180
    label "category"
  ]
  node [
    id 181
    label "pakiet_klimatyczny"
  ]
  node [
    id 182
    label "oddzia&#322;"
  ]
  node [
    id 183
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 184
    label "cz&#261;steczka"
  ]
  node [
    id 185
    label "stage_set"
  ]
  node [
    id 186
    label "type"
  ]
  node [
    id 187
    label "specgrupa"
  ]
  node [
    id 188
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 189
    label "&#346;wietliki"
  ]
  node [
    id 190
    label "odm&#322;odzenie"
  ]
  node [
    id 191
    label "Eurogrupa"
  ]
  node [
    id 192
    label "odm&#322;adza&#263;"
  ]
  node [
    id 193
    label "formacja_geologiczna"
  ]
  node [
    id 194
    label "harcerze_starsi"
  ]
  node [
    id 195
    label "poprzedzanie"
  ]
  node [
    id 196
    label "czasoprzestrze&#324;"
  ]
  node [
    id 197
    label "laba"
  ]
  node [
    id 198
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 199
    label "chronometria"
  ]
  node [
    id 200
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 201
    label "rachuba_czasu"
  ]
  node [
    id 202
    label "przep&#322;ywanie"
  ]
  node [
    id 203
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 204
    label "czasokres"
  ]
  node [
    id 205
    label "odczyt"
  ]
  node [
    id 206
    label "chwila"
  ]
  node [
    id 207
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 208
    label "dzieje"
  ]
  node [
    id 209
    label "kategoria_gramatyczna"
  ]
  node [
    id 210
    label "poprzedzenie"
  ]
  node [
    id 211
    label "trawienie"
  ]
  node [
    id 212
    label "pochodzi&#263;"
  ]
  node [
    id 213
    label "period"
  ]
  node [
    id 214
    label "okres_czasu"
  ]
  node [
    id 215
    label "poprzedza&#263;"
  ]
  node [
    id 216
    label "schy&#322;ek"
  ]
  node [
    id 217
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 218
    label "odwlekanie_si&#281;"
  ]
  node [
    id 219
    label "zegar"
  ]
  node [
    id 220
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 221
    label "czwarty_wymiar"
  ]
  node [
    id 222
    label "pochodzenie"
  ]
  node [
    id 223
    label "koniugacja"
  ]
  node [
    id 224
    label "Zeitgeist"
  ]
  node [
    id 225
    label "trawi&#263;"
  ]
  node [
    id 226
    label "pogoda"
  ]
  node [
    id 227
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 228
    label "poprzedzi&#263;"
  ]
  node [
    id 229
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 230
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 231
    label "time_period"
  ]
  node [
    id 232
    label "tydzie&#324;"
  ]
  node [
    id 233
    label "miech"
  ]
  node [
    id 234
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 235
    label "kalendy"
  ]
  node [
    id 236
    label "term"
  ]
  node [
    id 237
    label "rok_akademicki"
  ]
  node [
    id 238
    label "rok_szkolny"
  ]
  node [
    id 239
    label "semester"
  ]
  node [
    id 240
    label "anniwersarz"
  ]
  node [
    id 241
    label "rocznica"
  ]
  node [
    id 242
    label "obszar"
  ]
  node [
    id 243
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 244
    label "long_time"
  ]
  node [
    id 245
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 246
    label "almanac"
  ]
  node [
    id 247
    label "rozk&#322;ad"
  ]
  node [
    id 248
    label "wydawnictwo"
  ]
  node [
    id 249
    label "Juliusz_Cezar"
  ]
  node [
    id 250
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 251
    label "zwy&#380;kowanie"
  ]
  node [
    id 252
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 253
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 254
    label "zaj&#281;cia"
  ]
  node [
    id 255
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 256
    label "trasa"
  ]
  node [
    id 257
    label "przeorientowywanie"
  ]
  node [
    id 258
    label "przejazd"
  ]
  node [
    id 259
    label "kierunek"
  ]
  node [
    id 260
    label "przeorientowywa&#263;"
  ]
  node [
    id 261
    label "nauka"
  ]
  node [
    id 262
    label "przeorientowanie"
  ]
  node [
    id 263
    label "klasa"
  ]
  node [
    id 264
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 265
    label "przeorientowa&#263;"
  ]
  node [
    id 266
    label "manner"
  ]
  node [
    id 267
    label "course"
  ]
  node [
    id 268
    label "passage"
  ]
  node [
    id 269
    label "zni&#380;kowanie"
  ]
  node [
    id 270
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 271
    label "seria"
  ]
  node [
    id 272
    label "stawka"
  ]
  node [
    id 273
    label "way"
  ]
  node [
    id 274
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 275
    label "spos&#243;b"
  ]
  node [
    id 276
    label "deprecjacja"
  ]
  node [
    id 277
    label "cedu&#322;a"
  ]
  node [
    id 278
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 279
    label "drive"
  ]
  node [
    id 280
    label "bearing"
  ]
  node [
    id 281
    label "Lira"
  ]
  node [
    id 282
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 283
    label "mie&#263;_miejsce"
  ]
  node [
    id 284
    label "equal"
  ]
  node [
    id 285
    label "trwa&#263;"
  ]
  node [
    id 286
    label "chodzi&#263;"
  ]
  node [
    id 287
    label "si&#281;ga&#263;"
  ]
  node [
    id 288
    label "stan"
  ]
  node [
    id 289
    label "obecno&#347;&#263;"
  ]
  node [
    id 290
    label "stand"
  ]
  node [
    id 291
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 292
    label "uczestniczy&#263;"
  ]
  node [
    id 293
    label "participate"
  ]
  node [
    id 294
    label "robi&#263;"
  ]
  node [
    id 295
    label "istnie&#263;"
  ]
  node [
    id 296
    label "pozostawa&#263;"
  ]
  node [
    id 297
    label "zostawa&#263;"
  ]
  node [
    id 298
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 299
    label "adhere"
  ]
  node [
    id 300
    label "compass"
  ]
  node [
    id 301
    label "korzysta&#263;"
  ]
  node [
    id 302
    label "appreciation"
  ]
  node [
    id 303
    label "osi&#261;ga&#263;"
  ]
  node [
    id 304
    label "dociera&#263;"
  ]
  node [
    id 305
    label "get"
  ]
  node [
    id 306
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 307
    label "mierzy&#263;"
  ]
  node [
    id 308
    label "u&#380;ywa&#263;"
  ]
  node [
    id 309
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 310
    label "exsert"
  ]
  node [
    id 311
    label "being"
  ]
  node [
    id 312
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 313
    label "cecha"
  ]
  node [
    id 314
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 315
    label "p&#322;ywa&#263;"
  ]
  node [
    id 316
    label "run"
  ]
  node [
    id 317
    label "bangla&#263;"
  ]
  node [
    id 318
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 319
    label "przebiega&#263;"
  ]
  node [
    id 320
    label "wk&#322;ada&#263;"
  ]
  node [
    id 321
    label "proceed"
  ]
  node [
    id 322
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 323
    label "carry"
  ]
  node [
    id 324
    label "bywa&#263;"
  ]
  node [
    id 325
    label "dziama&#263;"
  ]
  node [
    id 326
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 327
    label "stara&#263;_si&#281;"
  ]
  node [
    id 328
    label "para"
  ]
  node [
    id 329
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 330
    label "str&#243;j"
  ]
  node [
    id 331
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 332
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 333
    label "krok"
  ]
  node [
    id 334
    label "tryb"
  ]
  node [
    id 335
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 336
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 337
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 338
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 339
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 340
    label "continue"
  ]
  node [
    id 341
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 342
    label "Ohio"
  ]
  node [
    id 343
    label "wci&#281;cie"
  ]
  node [
    id 344
    label "Nowy_York"
  ]
  node [
    id 345
    label "warstwa"
  ]
  node [
    id 346
    label "samopoczucie"
  ]
  node [
    id 347
    label "Illinois"
  ]
  node [
    id 348
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 349
    label "state"
  ]
  node [
    id 350
    label "Jukatan"
  ]
  node [
    id 351
    label "Kalifornia"
  ]
  node [
    id 352
    label "Wirginia"
  ]
  node [
    id 353
    label "wektor"
  ]
  node [
    id 354
    label "Teksas"
  ]
  node [
    id 355
    label "Goa"
  ]
  node [
    id 356
    label "Waszyngton"
  ]
  node [
    id 357
    label "miejsce"
  ]
  node [
    id 358
    label "Massachusetts"
  ]
  node [
    id 359
    label "Alaska"
  ]
  node [
    id 360
    label "Arakan"
  ]
  node [
    id 361
    label "Hawaje"
  ]
  node [
    id 362
    label "Maryland"
  ]
  node [
    id 363
    label "punkt"
  ]
  node [
    id 364
    label "Michigan"
  ]
  node [
    id 365
    label "Arizona"
  ]
  node [
    id 366
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 367
    label "Georgia"
  ]
  node [
    id 368
    label "poziom"
  ]
  node [
    id 369
    label "Pensylwania"
  ]
  node [
    id 370
    label "shape"
  ]
  node [
    id 371
    label "Luizjana"
  ]
  node [
    id 372
    label "Nowy_Meksyk"
  ]
  node [
    id 373
    label "Alabama"
  ]
  node [
    id 374
    label "ilo&#347;&#263;"
  ]
  node [
    id 375
    label "Kansas"
  ]
  node [
    id 376
    label "Oregon"
  ]
  node [
    id 377
    label "Floryda"
  ]
  node [
    id 378
    label "Oklahoma"
  ]
  node [
    id 379
    label "jednostka_administracyjna"
  ]
  node [
    id 380
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 381
    label "intensywny"
  ]
  node [
    id 382
    label "mocny"
  ]
  node [
    id 383
    label "silny"
  ]
  node [
    id 384
    label "przekonuj&#261;co"
  ]
  node [
    id 385
    label "niema&#322;o"
  ]
  node [
    id 386
    label "powerfully"
  ]
  node [
    id 387
    label "widocznie"
  ]
  node [
    id 388
    label "szczerze"
  ]
  node [
    id 389
    label "konkretnie"
  ]
  node [
    id 390
    label "niepodwa&#380;alnie"
  ]
  node [
    id 391
    label "stabilnie"
  ]
  node [
    id 392
    label "silnie"
  ]
  node [
    id 393
    label "zdecydowanie"
  ]
  node [
    id 394
    label "strongly"
  ]
  node [
    id 395
    label "szybki"
  ]
  node [
    id 396
    label "znacz&#261;cy"
  ]
  node [
    id 397
    label "zwarty"
  ]
  node [
    id 398
    label "efektywny"
  ]
  node [
    id 399
    label "ogrodnictwo"
  ]
  node [
    id 400
    label "dynamiczny"
  ]
  node [
    id 401
    label "pe&#322;ny"
  ]
  node [
    id 402
    label "intensywnie"
  ]
  node [
    id 403
    label "nieproporcjonalny"
  ]
  node [
    id 404
    label "specjalny"
  ]
  node [
    id 405
    label "szczery"
  ]
  node [
    id 406
    label "niepodwa&#380;alny"
  ]
  node [
    id 407
    label "zdecydowany"
  ]
  node [
    id 408
    label "stabilny"
  ]
  node [
    id 409
    label "trudny"
  ]
  node [
    id 410
    label "krzepki"
  ]
  node [
    id 411
    label "du&#380;y"
  ]
  node [
    id 412
    label "wyrazisty"
  ]
  node [
    id 413
    label "przekonuj&#261;cy"
  ]
  node [
    id 414
    label "widoczny"
  ]
  node [
    id 415
    label "wzmocni&#263;"
  ]
  node [
    id 416
    label "wzmacnia&#263;"
  ]
  node [
    id 417
    label "konkretny"
  ]
  node [
    id 418
    label "wytrzyma&#322;y"
  ]
  node [
    id 419
    label "meflochina"
  ]
  node [
    id 420
    label "dobry"
  ]
  node [
    id 421
    label "krzepienie"
  ]
  node [
    id 422
    label "&#380;ywotny"
  ]
  node [
    id 423
    label "pokrzepienie"
  ]
  node [
    id 424
    label "zdrowy"
  ]
  node [
    id 425
    label "zajebisty"
  ]
  node [
    id 426
    label "zajebi&#347;cie"
  ]
  node [
    id 427
    label "dusznie"
  ]
  node [
    id 428
    label "s&#322;usznie"
  ]
  node [
    id 429
    label "szczero"
  ]
  node [
    id 430
    label "hojnie"
  ]
  node [
    id 431
    label "honestly"
  ]
  node [
    id 432
    label "outspokenly"
  ]
  node [
    id 433
    label "artlessly"
  ]
  node [
    id 434
    label "uczciwie"
  ]
  node [
    id 435
    label "bluffly"
  ]
  node [
    id 436
    label "visibly"
  ]
  node [
    id 437
    label "poznawalnie"
  ]
  node [
    id 438
    label "widno"
  ]
  node [
    id 439
    label "wyra&#378;nie"
  ]
  node [
    id 440
    label "widzialnie"
  ]
  node [
    id 441
    label "dostrzegalnie"
  ]
  node [
    id 442
    label "widomie"
  ]
  node [
    id 443
    label "jasno"
  ]
  node [
    id 444
    label "posilnie"
  ]
  node [
    id 445
    label "dok&#322;adnie"
  ]
  node [
    id 446
    label "tre&#347;ciwie"
  ]
  node [
    id 447
    label "po&#380;ywnie"
  ]
  node [
    id 448
    label "solidny"
  ]
  node [
    id 449
    label "&#322;adnie"
  ]
  node [
    id 450
    label "nie&#378;le"
  ]
  node [
    id 451
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 452
    label "decyzja"
  ]
  node [
    id 453
    label "pewnie"
  ]
  node [
    id 454
    label "zauwa&#380;alnie"
  ]
  node [
    id 455
    label "oddzia&#322;anie"
  ]
  node [
    id 456
    label "podj&#281;cie"
  ]
  node [
    id 457
    label "resoluteness"
  ]
  node [
    id 458
    label "judgment"
  ]
  node [
    id 459
    label "zrobienie"
  ]
  node [
    id 460
    label "skutecznie"
  ]
  node [
    id 461
    label "convincingly"
  ]
  node [
    id 462
    label "trwale"
  ]
  node [
    id 463
    label "stale"
  ]
  node [
    id 464
    label "porz&#261;dnie"
  ]
  node [
    id 465
    label "rozwini&#281;cie"
  ]
  node [
    id 466
    label "zapoznanie"
  ]
  node [
    id 467
    label "wys&#322;anie"
  ]
  node [
    id 468
    label "udoskonalenie"
  ]
  node [
    id 469
    label "pomo&#380;enie"
  ]
  node [
    id 470
    label "wiedza"
  ]
  node [
    id 471
    label "urszulanki"
  ]
  node [
    id 472
    label "training"
  ]
  node [
    id 473
    label "niepokalanki"
  ]
  node [
    id 474
    label "o&#347;wiecenie"
  ]
  node [
    id 475
    label "kwalifikacje"
  ]
  node [
    id 476
    label "sophistication"
  ]
  node [
    id 477
    label "skolaryzacja"
  ]
  node [
    id 478
    label "form"
  ]
  node [
    id 479
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 480
    label "eliminacje"
  ]
  node [
    id 481
    label "ustawienie"
  ]
  node [
    id 482
    label "exploitation"
  ]
  node [
    id 483
    label "powi&#281;kszenie"
  ]
  node [
    id 484
    label "dodanie"
  ]
  node [
    id 485
    label "enlargement"
  ]
  node [
    id 486
    label "przed&#322;o&#380;enie"
  ]
  node [
    id 487
    label "development"
  ]
  node [
    id 488
    label "rozpakowanie"
  ]
  node [
    id 489
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 490
    label "rozpostarcie"
  ]
  node [
    id 491
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 492
    label "rozstawienie"
  ]
  node [
    id 493
    label "przekazanie"
  ]
  node [
    id 494
    label "nakazanie"
  ]
  node [
    id 495
    label "wy&#322;o&#380;enie"
  ]
  node [
    id 496
    label "nabicie"
  ]
  node [
    id 497
    label "commitment"
  ]
  node [
    id 498
    label "p&#243;j&#347;cie"
  ]
  node [
    id 499
    label "bed"
  ]
  node [
    id 500
    label "stuffing"
  ]
  node [
    id 501
    label "wytworzenie"
  ]
  node [
    id 502
    label "wyci&#261;gni&#281;cie_pomocnej_d&#322;oni"
  ]
  node [
    id 503
    label "comfort"
  ]
  node [
    id 504
    label "poskutkowanie"
  ]
  node [
    id 505
    label "u&#322;atwienie"
  ]
  node [
    id 506
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 507
    label "representation"
  ]
  node [
    id 508
    label "zawarcie"
  ]
  node [
    id 509
    label "znajomy"
  ]
  node [
    id 510
    label "obznajomienie"
  ]
  node [
    id 511
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 512
    label "umo&#380;liwienie"
  ]
  node [
    id 513
    label "gathering"
  ]
  node [
    id 514
    label "poinformowanie"
  ]
  node [
    id 515
    label "knowing"
  ]
  node [
    id 516
    label "poprawa"
  ]
  node [
    id 517
    label "doskonalszy"
  ]
  node [
    id 518
    label "modyfikacja"
  ]
  node [
    id 519
    label "wyszlifowanie"
  ]
  node [
    id 520
    label "ulepszenie"
  ]
  node [
    id 521
    label "proporcja"
  ]
  node [
    id 522
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 523
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 524
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 525
    label "urszulanki_szare"
  ]
  node [
    id 526
    label "cognition"
  ]
  node [
    id 527
    label "intelekt"
  ]
  node [
    id 528
    label "pozwolenie"
  ]
  node [
    id 529
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 530
    label "zaawansowanie"
  ]
  node [
    id 531
    label "iluminacja"
  ]
  node [
    id 532
    label "epoka"
  ]
  node [
    id 533
    label "poznanie"
  ]
  node [
    id 534
    label "spowodowanie"
  ]
  node [
    id 535
    label "consciousness"
  ]
  node [
    id 536
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 537
    label "nowo&#380;ytno&#347;&#263;"
  ]
  node [
    id 538
    label "encyklopedyzm"
  ]
  node [
    id 539
    label "encyklopedy&#347;ci"
  ]
  node [
    id 540
    label "czynno&#347;&#263;"
  ]
  node [
    id 541
    label "u&#347;wiadomienie"
  ]
  node [
    id 542
    label "nauczenie"
  ]
  node [
    id 543
    label "Kartezjusz"
  ]
  node [
    id 544
    label "Berkeley"
  ]
  node [
    id 545
    label "Biot"
  ]
  node [
    id 546
    label "Archimedes"
  ]
  node [
    id 547
    label "Ptolemeusz"
  ]
  node [
    id 548
    label "Gauss"
  ]
  node [
    id 549
    label "Newton"
  ]
  node [
    id 550
    label "Kepler"
  ]
  node [
    id 551
    label "Fourier"
  ]
  node [
    id 552
    label "Bayes"
  ]
  node [
    id 553
    label "Doppler"
  ]
  node [
    id 554
    label "Laplace"
  ]
  node [
    id 555
    label "Euklides"
  ]
  node [
    id 556
    label "Borel"
  ]
  node [
    id 557
    label "naukowiec"
  ]
  node [
    id 558
    label "Galileusz"
  ]
  node [
    id 559
    label "Pitagoras"
  ]
  node [
    id 560
    label "Pascal"
  ]
  node [
    id 561
    label "Maxwell"
  ]
  node [
    id 562
    label "&#347;ledziciel"
  ]
  node [
    id 563
    label "uczony"
  ]
  node [
    id 564
    label "Miczurin"
  ]
  node [
    id 565
    label "belfer"
  ]
  node [
    id 566
    label "preceptor"
  ]
  node [
    id 567
    label "pedagog"
  ]
  node [
    id 568
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 569
    label "szkolnik"
  ]
  node [
    id 570
    label "profesor"
  ]
  node [
    id 571
    label "popularyzator"
  ]
  node [
    id 572
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 573
    label "j&#281;zyk_programowania"
  ]
  node [
    id 574
    label "filozofia"
  ]
  node [
    id 575
    label "wyra&#378;no&#347;&#263;"
  ]
  node [
    id 576
    label "rozci&#261;g&#322;y"
  ]
  node [
    id 577
    label "pogl&#261;dy"
  ]
  node [
    id 578
    label "dorobek"
  ]
  node [
    id 579
    label "pitagorejczyk"
  ]
  node [
    id 580
    label "ideologia"
  ]
  node [
    id 581
    label "wiela"
  ]
  node [
    id 582
    label "du&#380;o"
  ]
  node [
    id 583
    label "doros&#322;y"
  ]
  node [
    id 584
    label "znaczny"
  ]
  node [
    id 585
    label "rozwini&#281;ty"
  ]
  node [
    id 586
    label "dorodny"
  ]
  node [
    id 587
    label "wa&#380;ny"
  ]
  node [
    id 588
    label "prawdziwy"
  ]
  node [
    id 589
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 590
    label "wytwarza&#263;"
  ]
  node [
    id 591
    label "muzyka"
  ]
  node [
    id 592
    label "work"
  ]
  node [
    id 593
    label "create"
  ]
  node [
    id 594
    label "praca"
  ]
  node [
    id 595
    label "rola"
  ]
  node [
    id 596
    label "organizowa&#263;"
  ]
  node [
    id 597
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 598
    label "czyni&#263;"
  ]
  node [
    id 599
    label "give"
  ]
  node [
    id 600
    label "stylizowa&#263;"
  ]
  node [
    id 601
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 602
    label "falowa&#263;"
  ]
  node [
    id 603
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 604
    label "peddle"
  ]
  node [
    id 605
    label "wydala&#263;"
  ]
  node [
    id 606
    label "tentegowa&#263;"
  ]
  node [
    id 607
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 608
    label "urz&#261;dza&#263;"
  ]
  node [
    id 609
    label "oszukiwa&#263;"
  ]
  node [
    id 610
    label "ukazywa&#263;"
  ]
  node [
    id 611
    label "przerabia&#263;"
  ]
  node [
    id 612
    label "act"
  ]
  node [
    id 613
    label "post&#281;powa&#263;"
  ]
  node [
    id 614
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 615
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 616
    label "najem"
  ]
  node [
    id 617
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 618
    label "zak&#322;ad"
  ]
  node [
    id 619
    label "stosunek_pracy"
  ]
  node [
    id 620
    label "benedykty&#324;ski"
  ]
  node [
    id 621
    label "poda&#380;_pracy"
  ]
  node [
    id 622
    label "pracowanie"
  ]
  node [
    id 623
    label "tyrka"
  ]
  node [
    id 624
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 625
    label "wytw&#243;r"
  ]
  node [
    id 626
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 627
    label "tynkarski"
  ]
  node [
    id 628
    label "pracowa&#263;"
  ]
  node [
    id 629
    label "zmiana"
  ]
  node [
    id 630
    label "czynnik_produkcji"
  ]
  node [
    id 631
    label "zobowi&#261;zanie"
  ]
  node [
    id 632
    label "kierownictwo"
  ]
  node [
    id 633
    label "siedziba"
  ]
  node [
    id 634
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 635
    label "uprawienie"
  ]
  node [
    id 636
    label "kszta&#322;t"
  ]
  node [
    id 637
    label "dialog"
  ]
  node [
    id 638
    label "p&#322;osa"
  ]
  node [
    id 639
    label "wykonywanie"
  ]
  node [
    id 640
    label "ziemia"
  ]
  node [
    id 641
    label "czyn"
  ]
  node [
    id 642
    label "scenariusz"
  ]
  node [
    id 643
    label "pole"
  ]
  node [
    id 644
    label "gospodarstwo"
  ]
  node [
    id 645
    label "uprawi&#263;"
  ]
  node [
    id 646
    label "function"
  ]
  node [
    id 647
    label "zreinterpretowa&#263;"
  ]
  node [
    id 648
    label "zastosowanie"
  ]
  node [
    id 649
    label "reinterpretowa&#263;"
  ]
  node [
    id 650
    label "wrench"
  ]
  node [
    id 651
    label "irygowanie"
  ]
  node [
    id 652
    label "ustawi&#263;"
  ]
  node [
    id 653
    label "irygowa&#263;"
  ]
  node [
    id 654
    label "zreinterpretowanie"
  ]
  node [
    id 655
    label "cel"
  ]
  node [
    id 656
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 657
    label "gra&#263;"
  ]
  node [
    id 658
    label "aktorstwo"
  ]
  node [
    id 659
    label "kostium"
  ]
  node [
    id 660
    label "zagon"
  ]
  node [
    id 661
    label "znaczenie"
  ]
  node [
    id 662
    label "zagra&#263;"
  ]
  node [
    id 663
    label "reinterpretowanie"
  ]
  node [
    id 664
    label "tekst"
  ]
  node [
    id 665
    label "zagranie"
  ]
  node [
    id 666
    label "radlina"
  ]
  node [
    id 667
    label "granie"
  ]
  node [
    id 668
    label "wokalistyka"
  ]
  node [
    id 669
    label "przedmiot"
  ]
  node [
    id 670
    label "muza"
  ]
  node [
    id 671
    label "zjawisko"
  ]
  node [
    id 672
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 673
    label "beatbox"
  ]
  node [
    id 674
    label "komponowa&#263;"
  ]
  node [
    id 675
    label "szko&#322;a"
  ]
  node [
    id 676
    label "komponowanie"
  ]
  node [
    id 677
    label "pasa&#380;"
  ]
  node [
    id 678
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 679
    label "notacja_muzyczna"
  ]
  node [
    id 680
    label "kontrapunkt"
  ]
  node [
    id 681
    label "sztuka"
  ]
  node [
    id 682
    label "instrumentalistyka"
  ]
  node [
    id 683
    label "harmonia"
  ]
  node [
    id 684
    label "set"
  ]
  node [
    id 685
    label "wys&#322;uchanie"
  ]
  node [
    id 686
    label "kapela"
  ]
  node [
    id 687
    label "britpop"
  ]
  node [
    id 688
    label "wiadomy"
  ]
  node [
    id 689
    label "zawodoznawstwo"
  ]
  node [
    id 690
    label "emocja"
  ]
  node [
    id 691
    label "office"
  ]
  node [
    id 692
    label "craft"
  ]
  node [
    id 693
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 694
    label "ogrom"
  ]
  node [
    id 695
    label "iskrzy&#263;"
  ]
  node [
    id 696
    label "d&#322;awi&#263;"
  ]
  node [
    id 697
    label "ostygn&#261;&#263;"
  ]
  node [
    id 698
    label "stygn&#261;&#263;"
  ]
  node [
    id 699
    label "temperatura"
  ]
  node [
    id 700
    label "wpa&#347;&#263;"
  ]
  node [
    id 701
    label "afekt"
  ]
  node [
    id 702
    label "wpada&#263;"
  ]
  node [
    id 703
    label "rozszerzyciel"
  ]
  node [
    id 704
    label "wyprawka"
  ]
  node [
    id 705
    label "mundurek"
  ]
  node [
    id 706
    label "tarcza"
  ]
  node [
    id 707
    label "elew"
  ]
  node [
    id 708
    label "absolwent"
  ]
  node [
    id 709
    label "stopie&#324;_naukowy"
  ]
  node [
    id 710
    label "nauczyciel_akademicki"
  ]
  node [
    id 711
    label "tytu&#322;"
  ]
  node [
    id 712
    label "profesura"
  ]
  node [
    id 713
    label "konsulent"
  ]
  node [
    id 714
    label "wirtuoz"
  ]
  node [
    id 715
    label "zwierzchnik"
  ]
  node [
    id 716
    label "ekspert"
  ]
  node [
    id 717
    label "ochotnik"
  ]
  node [
    id 718
    label "pomocnik"
  ]
  node [
    id 719
    label "student"
  ]
  node [
    id 720
    label "nauczyciel_muzyki"
  ]
  node [
    id 721
    label "zakonnik"
  ]
  node [
    id 722
    label "J&#281;drzejewicz"
  ]
  node [
    id 723
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 724
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 725
    label "John_Dewey"
  ]
  node [
    id 726
    label "naukowy"
  ]
  node [
    id 727
    label "teoretyczny"
  ]
  node [
    id 728
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 729
    label "studencki"
  ]
  node [
    id 730
    label "prawid&#322;owy"
  ]
  node [
    id 731
    label "odpowiedni"
  ]
  node [
    id 732
    label "szkolny"
  ]
  node [
    id 733
    label "tradycyjny"
  ]
  node [
    id 734
    label "intelektualny"
  ]
  node [
    id 735
    label "akademicko"
  ]
  node [
    id 736
    label "akademiczny"
  ]
  node [
    id 737
    label "po_akademicku"
  ]
  node [
    id 738
    label "naukowo"
  ]
  node [
    id 739
    label "edukacyjnie"
  ]
  node [
    id 740
    label "scjentyficzny"
  ]
  node [
    id 741
    label "skomplikowany"
  ]
  node [
    id 742
    label "specjalistyczny"
  ]
  node [
    id 743
    label "zgodny"
  ]
  node [
    id 744
    label "s&#322;uszny"
  ]
  node [
    id 745
    label "symetryczny"
  ]
  node [
    id 746
    label "prawid&#322;owo"
  ]
  node [
    id 747
    label "po&#380;&#261;dany"
  ]
  node [
    id 748
    label "zdarzony"
  ]
  node [
    id 749
    label "odpowiednio"
  ]
  node [
    id 750
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 751
    label "nale&#380;ny"
  ]
  node [
    id 752
    label "nale&#380;yty"
  ]
  node [
    id 753
    label "stosownie"
  ]
  node [
    id 754
    label "odpowiadanie"
  ]
  node [
    id 755
    label "modelowy"
  ]
  node [
    id 756
    label "tradycyjnie"
  ]
  node [
    id 757
    label "surowy"
  ]
  node [
    id 758
    label "zwyk&#322;y"
  ]
  node [
    id 759
    label "zachowawczy"
  ]
  node [
    id 760
    label "nienowoczesny"
  ]
  node [
    id 761
    label "przyj&#281;ty"
  ]
  node [
    id 762
    label "wierny"
  ]
  node [
    id 763
    label "zwyczajowy"
  ]
  node [
    id 764
    label "szkolnie"
  ]
  node [
    id 765
    label "podstawowy"
  ]
  node [
    id 766
    label "prosty"
  ]
  node [
    id 767
    label "szkoleniowy"
  ]
  node [
    id 768
    label "intelektualnie"
  ]
  node [
    id 769
    label "my&#347;l&#261;cy"
  ]
  node [
    id 770
    label "wznios&#322;y"
  ]
  node [
    id 771
    label "g&#322;&#281;boki"
  ]
  node [
    id 772
    label "umys&#322;owy"
  ]
  node [
    id 773
    label "inteligentny"
  ]
  node [
    id 774
    label "nierealny"
  ]
  node [
    id 775
    label "teoretycznie"
  ]
  node [
    id 776
    label "typowy"
  ]
  node [
    id 777
    label "uprawniony"
  ]
  node [
    id 778
    label "zasadniczy"
  ]
  node [
    id 779
    label "taki"
  ]
  node [
    id 780
    label "charakterystyczny"
  ]
  node [
    id 781
    label "po_studencku"
  ]
  node [
    id 782
    label "m&#322;ody"
  ]
  node [
    id 783
    label "charakterystycznie"
  ]
  node [
    id 784
    label "ryba"
  ]
  node [
    id 785
    label "&#347;ledziowate"
  ]
  node [
    id 786
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 787
    label "kr&#281;gowiec"
  ]
  node [
    id 788
    label "systemik"
  ]
  node [
    id 789
    label "doniczkowiec"
  ]
  node [
    id 790
    label "mi&#281;so"
  ]
  node [
    id 791
    label "system"
  ]
  node [
    id 792
    label "patroszy&#263;"
  ]
  node [
    id 793
    label "rakowato&#347;&#263;"
  ]
  node [
    id 794
    label "w&#281;dkarstwo"
  ]
  node [
    id 795
    label "ryby"
  ]
  node [
    id 796
    label "fish"
  ]
  node [
    id 797
    label "linia_boczna"
  ]
  node [
    id 798
    label "tar&#322;o"
  ]
  node [
    id 799
    label "wyrostek_filtracyjny"
  ]
  node [
    id 800
    label "m&#281;tnooki"
  ]
  node [
    id 801
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 802
    label "pokrywa_skrzelowa"
  ]
  node [
    id 803
    label "ikra"
  ]
  node [
    id 804
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 805
    label "szczelina_skrzelowa"
  ]
  node [
    id 806
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 807
    label "wyrafinowany"
  ]
  node [
    id 808
    label "niepo&#347;ledni"
  ]
  node [
    id 809
    label "chwalebny"
  ]
  node [
    id 810
    label "z_wysoka"
  ]
  node [
    id 811
    label "daleki"
  ]
  node [
    id 812
    label "szczytnie"
  ]
  node [
    id 813
    label "warto&#347;ciowy"
  ]
  node [
    id 814
    label "wysoko"
  ]
  node [
    id 815
    label "uprzywilejowany"
  ]
  node [
    id 816
    label "znacznie"
  ]
  node [
    id 817
    label "zauwa&#380;alny"
  ]
  node [
    id 818
    label "szczeg&#243;lny"
  ]
  node [
    id 819
    label "lekki"
  ]
  node [
    id 820
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 821
    label "niez&#322;y"
  ]
  node [
    id 822
    label "niepo&#347;lednio"
  ]
  node [
    id 823
    label "wyj&#261;tkowy"
  ]
  node [
    id 824
    label "szlachetny"
  ]
  node [
    id 825
    label "powa&#380;ny"
  ]
  node [
    id 826
    label "podnios&#322;y"
  ]
  node [
    id 827
    label "wznio&#347;le"
  ]
  node [
    id 828
    label "oderwany"
  ]
  node [
    id 829
    label "pi&#281;kny"
  ]
  node [
    id 830
    label "pochwalny"
  ]
  node [
    id 831
    label "wspania&#322;y"
  ]
  node [
    id 832
    label "chwalebnie"
  ]
  node [
    id 833
    label "obyty"
  ]
  node [
    id 834
    label "wykwintny"
  ]
  node [
    id 835
    label "wyrafinowanie"
  ]
  node [
    id 836
    label "wymy&#347;lny"
  ]
  node [
    id 837
    label "rewaluowanie"
  ]
  node [
    id 838
    label "warto&#347;ciowo"
  ]
  node [
    id 839
    label "drogi"
  ]
  node [
    id 840
    label "u&#380;yteczny"
  ]
  node [
    id 841
    label "zrewaluowanie"
  ]
  node [
    id 842
    label "ogl&#281;dny"
  ]
  node [
    id 843
    label "d&#322;ugi"
  ]
  node [
    id 844
    label "daleko"
  ]
  node [
    id 845
    label "zwi&#261;zany"
  ]
  node [
    id 846
    label "r&#243;&#380;ny"
  ]
  node [
    id 847
    label "s&#322;aby"
  ]
  node [
    id 848
    label "odlegle"
  ]
  node [
    id 849
    label "oddalony"
  ]
  node [
    id 850
    label "obcy"
  ]
  node [
    id 851
    label "nieobecny"
  ]
  node [
    id 852
    label "przysz&#322;y"
  ]
  node [
    id 853
    label "g&#243;rno"
  ]
  node [
    id 854
    label "szczytny"
  ]
  node [
    id 855
    label "wielki"
  ]
  node [
    id 856
    label "niezmiernie"
  ]
  node [
    id 857
    label "kanclerz"
  ]
  node [
    id 858
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 859
    label "podkanclerz"
  ]
  node [
    id 860
    label "miasteczko_studenckie"
  ]
  node [
    id 861
    label "kwestura"
  ]
  node [
    id 862
    label "wyk&#322;adanie"
  ]
  node [
    id 863
    label "rektorat"
  ]
  node [
    id 864
    label "school"
  ]
  node [
    id 865
    label "senat"
  ]
  node [
    id 866
    label "promotorstwo"
  ]
  node [
    id 867
    label "do&#347;wiadczenie"
  ]
  node [
    id 868
    label "teren_szko&#322;y"
  ]
  node [
    id 869
    label "Mickiewicz"
  ]
  node [
    id 870
    label "podr&#281;cznik"
  ]
  node [
    id 871
    label "praktyka"
  ]
  node [
    id 872
    label "zda&#263;"
  ]
  node [
    id 873
    label "gabinet"
  ]
  node [
    id 874
    label "sztuba"
  ]
  node [
    id 875
    label "&#322;awa_szkolna"
  ]
  node [
    id 876
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 877
    label "przepisa&#263;"
  ]
  node [
    id 878
    label "lekcja"
  ]
  node [
    id 879
    label "metoda"
  ]
  node [
    id 880
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 881
    label "przepisanie"
  ]
  node [
    id 882
    label "zdanie"
  ]
  node [
    id 883
    label "stopek"
  ]
  node [
    id 884
    label "sekretariat"
  ]
  node [
    id 885
    label "lesson"
  ]
  node [
    id 886
    label "instytucja"
  ]
  node [
    id 887
    label "szkolenie"
  ]
  node [
    id 888
    label "kara"
  ]
  node [
    id 889
    label "tablica"
  ]
  node [
    id 890
    label "bursary"
  ]
  node [
    id 891
    label "urz&#261;d"
  ]
  node [
    id 892
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 893
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 894
    label "biuro"
  ]
  node [
    id 895
    label "parlament"
  ]
  node [
    id 896
    label "organ"
  ]
  node [
    id 897
    label "deputation"
  ]
  node [
    id 898
    label "kolegium"
  ]
  node [
    id 899
    label "izba_wy&#380;sza"
  ]
  node [
    id 900
    label "magistrat"
  ]
  node [
    id 901
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 902
    label "uczy&#263;"
  ]
  node [
    id 903
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 904
    label "wyjmowa&#263;"
  ]
  node [
    id 905
    label "translate"
  ]
  node [
    id 906
    label "elaborate"
  ]
  node [
    id 907
    label "urz&#281;dnik"
  ]
  node [
    id 908
    label "puszczenie"
  ]
  node [
    id 909
    label "issue"
  ]
  node [
    id 910
    label "wyjmowanie"
  ]
  node [
    id 911
    label "pokrywanie"
  ]
  node [
    id 912
    label "robienie"
  ]
  node [
    id 913
    label "wystawianie"
  ]
  node [
    id 914
    label "k&#322;adzenie"
  ]
  node [
    id 915
    label "t&#322;umaczenie"
  ]
  node [
    id 916
    label "uczenie"
  ]
  node [
    id 917
    label "presentation"
  ]
  node [
    id 918
    label "opieka"
  ]
  node [
    id 919
    label "Bismarck"
  ]
  node [
    id 920
    label "kuria"
  ]
  node [
    id 921
    label "premier"
  ]
  node [
    id 922
    label "Goebbels"
  ]
  node [
    id 923
    label "duchowny"
  ]
  node [
    id 924
    label "dostojnik"
  ]
  node [
    id 925
    label "liczy&#263;"
  ]
  node [
    id 926
    label "prize"
  ]
  node [
    id 927
    label "appreciate"
  ]
  node [
    id 928
    label "szanowa&#263;"
  ]
  node [
    id 929
    label "ustala&#263;"
  ]
  node [
    id 930
    label "uznawa&#263;"
  ]
  node [
    id 931
    label "unwrap"
  ]
  node [
    id 932
    label "decydowa&#263;"
  ]
  node [
    id 933
    label "zmienia&#263;"
  ]
  node [
    id 934
    label "umacnia&#263;"
  ]
  node [
    id 935
    label "powodowa&#263;"
  ]
  node [
    id 936
    label "arrange"
  ]
  node [
    id 937
    label "os&#261;dza&#263;"
  ]
  node [
    id 938
    label "consider"
  ]
  node [
    id 939
    label "notice"
  ]
  node [
    id 940
    label "stwierdza&#263;"
  ]
  node [
    id 941
    label "przyznawa&#263;"
  ]
  node [
    id 942
    label "powa&#380;anie"
  ]
  node [
    id 943
    label "treasure"
  ]
  node [
    id 944
    label "czu&#263;"
  ]
  node [
    id 945
    label "respektowa&#263;"
  ]
  node [
    id 946
    label "wyra&#380;a&#263;"
  ]
  node [
    id 947
    label "chowa&#263;"
  ]
  node [
    id 948
    label "report"
  ]
  node [
    id 949
    label "dyskalkulia"
  ]
  node [
    id 950
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 951
    label "wynagrodzenie"
  ]
  node [
    id 952
    label "wymienia&#263;"
  ]
  node [
    id 953
    label "posiada&#263;"
  ]
  node [
    id 954
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 955
    label "wycenia&#263;"
  ]
  node [
    id 956
    label "bra&#263;"
  ]
  node [
    id 957
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 958
    label "rachowa&#263;"
  ]
  node [
    id 959
    label "count"
  ]
  node [
    id 960
    label "tell"
  ]
  node [
    id 961
    label "odlicza&#263;"
  ]
  node [
    id 962
    label "dodawa&#263;"
  ]
  node [
    id 963
    label "wyznacza&#263;"
  ]
  node [
    id 964
    label "admit"
  ]
  node [
    id 965
    label "policza&#263;"
  ]
  node [
    id 966
    label "okre&#347;la&#263;"
  ]
  node [
    id 967
    label "g&#281;sto"
  ]
  node [
    id 968
    label "dynamicznie"
  ]
  node [
    id 969
    label "nieprzeci&#281;tny"
  ]
  node [
    id 970
    label "wybitny"
  ]
  node [
    id 971
    label "dupny"
  ]
  node [
    id 972
    label "salariat"
  ]
  node [
    id 973
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 974
    label "delegowanie"
  ]
  node [
    id 975
    label "pracu&#347;"
  ]
  node [
    id 976
    label "r&#281;ka"
  ]
  node [
    id 977
    label "delegowa&#263;"
  ]
  node [
    id 978
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 979
    label "p&#322;aca"
  ]
  node [
    id 980
    label "ludzko&#347;&#263;"
  ]
  node [
    id 981
    label "wapniak"
  ]
  node [
    id 982
    label "os&#322;abia&#263;"
  ]
  node [
    id 983
    label "hominid"
  ]
  node [
    id 984
    label "podw&#322;adny"
  ]
  node [
    id 985
    label "os&#322;abianie"
  ]
  node [
    id 986
    label "g&#322;owa"
  ]
  node [
    id 987
    label "figura"
  ]
  node [
    id 988
    label "portrecista"
  ]
  node [
    id 989
    label "dwun&#243;g"
  ]
  node [
    id 990
    label "profanum"
  ]
  node [
    id 991
    label "mikrokosmos"
  ]
  node [
    id 992
    label "nasada"
  ]
  node [
    id 993
    label "duch"
  ]
  node [
    id 994
    label "antropochoria"
  ]
  node [
    id 995
    label "osoba"
  ]
  node [
    id 996
    label "wz&#243;r"
  ]
  node [
    id 997
    label "senior"
  ]
  node [
    id 998
    label "oddzia&#322;ywanie"
  ]
  node [
    id 999
    label "Adam"
  ]
  node [
    id 1000
    label "homo_sapiens"
  ]
  node [
    id 1001
    label "polifag"
  ]
  node [
    id 1002
    label "krzy&#380;"
  ]
  node [
    id 1003
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 1004
    label "handwriting"
  ]
  node [
    id 1005
    label "d&#322;o&#324;"
  ]
  node [
    id 1006
    label "gestykulowa&#263;"
  ]
  node [
    id 1007
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 1008
    label "palec"
  ]
  node [
    id 1009
    label "przedrami&#281;"
  ]
  node [
    id 1010
    label "hand"
  ]
  node [
    id 1011
    label "&#322;okie&#263;"
  ]
  node [
    id 1012
    label "hazena"
  ]
  node [
    id 1013
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 1014
    label "bramkarz"
  ]
  node [
    id 1015
    label "nadgarstek"
  ]
  node [
    id 1016
    label "graba"
  ]
  node [
    id 1017
    label "r&#261;czyna"
  ]
  node [
    id 1018
    label "k&#322;&#261;b"
  ]
  node [
    id 1019
    label "pi&#322;ka"
  ]
  node [
    id 1020
    label "chwyta&#263;"
  ]
  node [
    id 1021
    label "cmoknonsens"
  ]
  node [
    id 1022
    label "gestykulowanie"
  ]
  node [
    id 1023
    label "chwytanie"
  ]
  node [
    id 1024
    label "obietnica"
  ]
  node [
    id 1025
    label "zagrywka"
  ]
  node [
    id 1026
    label "kroki"
  ]
  node [
    id 1027
    label "hasta"
  ]
  node [
    id 1028
    label "wykroczenie"
  ]
  node [
    id 1029
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 1030
    label "czerwona_kartka"
  ]
  node [
    id 1031
    label "paw"
  ]
  node [
    id 1032
    label "rami&#281;"
  ]
  node [
    id 1033
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1034
    label "air"
  ]
  node [
    id 1035
    label "wys&#322;a&#263;"
  ]
  node [
    id 1036
    label "oddelegowa&#263;"
  ]
  node [
    id 1037
    label "oddelegowywa&#263;"
  ]
  node [
    id 1038
    label "zapaleniec"
  ]
  node [
    id 1039
    label "wysy&#322;anie"
  ]
  node [
    id 1040
    label "delegacy"
  ]
  node [
    id 1041
    label "oddelegowywanie"
  ]
  node [
    id 1042
    label "oddelegowanie"
  ]
  node [
    id 1043
    label "murza"
  ]
  node [
    id 1044
    label "ojciec"
  ]
  node [
    id 1045
    label "samiec"
  ]
  node [
    id 1046
    label "androlog"
  ]
  node [
    id 1047
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 1048
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 1049
    label "efendi"
  ]
  node [
    id 1050
    label "opiekun"
  ]
  node [
    id 1051
    label "pa&#324;stwo"
  ]
  node [
    id 1052
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 1053
    label "bratek"
  ]
  node [
    id 1054
    label "Mieszko_I"
  ]
  node [
    id 1055
    label "Midas"
  ]
  node [
    id 1056
    label "m&#261;&#380;"
  ]
  node [
    id 1057
    label "bogaty"
  ]
  node [
    id 1058
    label "pracodawca"
  ]
  node [
    id 1059
    label "nabab"
  ]
  node [
    id 1060
    label "pupil"
  ]
  node [
    id 1061
    label "andropauza"
  ]
  node [
    id 1062
    label "zwrot"
  ]
  node [
    id 1063
    label "przyw&#243;dca"
  ]
  node [
    id 1064
    label "rz&#261;dzenie"
  ]
  node [
    id 1065
    label "jegomo&#347;&#263;"
  ]
  node [
    id 1066
    label "ch&#322;opina"
  ]
  node [
    id 1067
    label "w&#322;odarz"
  ]
  node [
    id 1068
    label "gra_w_karty"
  ]
  node [
    id 1069
    label "w&#322;adza"
  ]
  node [
    id 1070
    label "Fidel_Castro"
  ]
  node [
    id 1071
    label "Anders"
  ]
  node [
    id 1072
    label "Ko&#347;ciuszko"
  ]
  node [
    id 1073
    label "Tito"
  ]
  node [
    id 1074
    label "Miko&#322;ajczyk"
  ]
  node [
    id 1075
    label "lider"
  ]
  node [
    id 1076
    label "Mao"
  ]
  node [
    id 1077
    label "Sabataj_Cwi"
  ]
  node [
    id 1078
    label "p&#322;atnik"
  ]
  node [
    id 1079
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 1080
    label "nadzorca"
  ]
  node [
    id 1081
    label "funkcjonariusz"
  ]
  node [
    id 1082
    label "wykupienie"
  ]
  node [
    id 1083
    label "bycie_w_posiadaniu"
  ]
  node [
    id 1084
    label "wykupywanie"
  ]
  node [
    id 1085
    label "wydoro&#347;lenie"
  ]
  node [
    id 1086
    label "doro&#347;lenie"
  ]
  node [
    id 1087
    label "&#378;ra&#322;y"
  ]
  node [
    id 1088
    label "doro&#347;le"
  ]
  node [
    id 1089
    label "dojrzale"
  ]
  node [
    id 1090
    label "dojrza&#322;y"
  ]
  node [
    id 1091
    label "m&#261;dry"
  ]
  node [
    id 1092
    label "doletni"
  ]
  node [
    id 1093
    label "turn"
  ]
  node [
    id 1094
    label "turning"
  ]
  node [
    id 1095
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1096
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1097
    label "skr&#281;t"
  ]
  node [
    id 1098
    label "obr&#243;t"
  ]
  node [
    id 1099
    label "fraza_czasownikowa"
  ]
  node [
    id 1100
    label "jednostka_leksykalna"
  ]
  node [
    id 1101
    label "wyra&#380;enie"
  ]
  node [
    id 1102
    label "starosta"
  ]
  node [
    id 1103
    label "zarz&#261;dca"
  ]
  node [
    id 1104
    label "w&#322;adca"
  ]
  node [
    id 1105
    label "bogacz"
  ]
  node [
    id 1106
    label "kuwada"
  ]
  node [
    id 1107
    label "rodzice"
  ]
  node [
    id 1108
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 1109
    label "rodzic"
  ]
  node [
    id 1110
    label "ojczym"
  ]
  node [
    id 1111
    label "przodek"
  ]
  node [
    id 1112
    label "papa"
  ]
  node [
    id 1113
    label "kochanek"
  ]
  node [
    id 1114
    label "fio&#322;ek"
  ]
  node [
    id 1115
    label "facet"
  ]
  node [
    id 1116
    label "brat"
  ]
  node [
    id 1117
    label "zwierz&#281;"
  ]
  node [
    id 1118
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 1119
    label "ma&#322;&#380;onek"
  ]
  node [
    id 1120
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 1121
    label "m&#243;j"
  ]
  node [
    id 1122
    label "ch&#322;op"
  ]
  node [
    id 1123
    label "pan_m&#322;ody"
  ]
  node [
    id 1124
    label "&#347;lubny"
  ]
  node [
    id 1125
    label "pan_domu"
  ]
  node [
    id 1126
    label "pan_i_w&#322;adca"
  ]
  node [
    id 1127
    label "mo&#347;&#263;"
  ]
  node [
    id 1128
    label "Frygia"
  ]
  node [
    id 1129
    label "sprawowanie"
  ]
  node [
    id 1130
    label "dominion"
  ]
  node [
    id 1131
    label "dominowanie"
  ]
  node [
    id 1132
    label "reign"
  ]
  node [
    id 1133
    label "rule"
  ]
  node [
    id 1134
    label "zwierz&#281;_domowe"
  ]
  node [
    id 1135
    label "specjalista"
  ]
  node [
    id 1136
    label "&#380;ycie"
  ]
  node [
    id 1137
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 1138
    label "Turek"
  ]
  node [
    id 1139
    label "effendi"
  ]
  node [
    id 1140
    label "obfituj&#261;cy"
  ]
  node [
    id 1141
    label "r&#243;&#380;norodny"
  ]
  node [
    id 1142
    label "spania&#322;y"
  ]
  node [
    id 1143
    label "obficie"
  ]
  node [
    id 1144
    label "sytuowany"
  ]
  node [
    id 1145
    label "och&#281;do&#380;ny"
  ]
  node [
    id 1146
    label "forsiasty"
  ]
  node [
    id 1147
    label "zapa&#347;ny"
  ]
  node [
    id 1148
    label "bogato"
  ]
  node [
    id 1149
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 1150
    label "Katar"
  ]
  node [
    id 1151
    label "Libia"
  ]
  node [
    id 1152
    label "Gwatemala"
  ]
  node [
    id 1153
    label "Ekwador"
  ]
  node [
    id 1154
    label "Afganistan"
  ]
  node [
    id 1155
    label "Tad&#380;ykistan"
  ]
  node [
    id 1156
    label "Bhutan"
  ]
  node [
    id 1157
    label "Argentyna"
  ]
  node [
    id 1158
    label "D&#380;ibuti"
  ]
  node [
    id 1159
    label "Wenezuela"
  ]
  node [
    id 1160
    label "Gabon"
  ]
  node [
    id 1161
    label "Ukraina"
  ]
  node [
    id 1162
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 1163
    label "Rwanda"
  ]
  node [
    id 1164
    label "Liechtenstein"
  ]
  node [
    id 1165
    label "organizacja"
  ]
  node [
    id 1166
    label "Sri_Lanka"
  ]
  node [
    id 1167
    label "Madagaskar"
  ]
  node [
    id 1168
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 1169
    label "Kongo"
  ]
  node [
    id 1170
    label "Tonga"
  ]
  node [
    id 1171
    label "Bangladesz"
  ]
  node [
    id 1172
    label "Kanada"
  ]
  node [
    id 1173
    label "Wehrlen"
  ]
  node [
    id 1174
    label "Algieria"
  ]
  node [
    id 1175
    label "Uganda"
  ]
  node [
    id 1176
    label "Surinam"
  ]
  node [
    id 1177
    label "Sahara_Zachodnia"
  ]
  node [
    id 1178
    label "Chile"
  ]
  node [
    id 1179
    label "W&#281;gry"
  ]
  node [
    id 1180
    label "Birma"
  ]
  node [
    id 1181
    label "Kazachstan"
  ]
  node [
    id 1182
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 1183
    label "Armenia"
  ]
  node [
    id 1184
    label "Tuwalu"
  ]
  node [
    id 1185
    label "Timor_Wschodni"
  ]
  node [
    id 1186
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 1187
    label "Izrael"
  ]
  node [
    id 1188
    label "Estonia"
  ]
  node [
    id 1189
    label "Komory"
  ]
  node [
    id 1190
    label "Kamerun"
  ]
  node [
    id 1191
    label "Haiti"
  ]
  node [
    id 1192
    label "Belize"
  ]
  node [
    id 1193
    label "Sierra_Leone"
  ]
  node [
    id 1194
    label "Luksemburg"
  ]
  node [
    id 1195
    label "USA"
  ]
  node [
    id 1196
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 1197
    label "Barbados"
  ]
  node [
    id 1198
    label "San_Marino"
  ]
  node [
    id 1199
    label "Bu&#322;garia"
  ]
  node [
    id 1200
    label "Indonezja"
  ]
  node [
    id 1201
    label "Wietnam"
  ]
  node [
    id 1202
    label "Malawi"
  ]
  node [
    id 1203
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 1204
    label "Francja"
  ]
  node [
    id 1205
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1206
    label "partia"
  ]
  node [
    id 1207
    label "Zambia"
  ]
  node [
    id 1208
    label "Angola"
  ]
  node [
    id 1209
    label "Grenada"
  ]
  node [
    id 1210
    label "Nepal"
  ]
  node [
    id 1211
    label "Panama"
  ]
  node [
    id 1212
    label "Rumunia"
  ]
  node [
    id 1213
    label "Czarnog&#243;ra"
  ]
  node [
    id 1214
    label "Malediwy"
  ]
  node [
    id 1215
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 1216
    label "S&#322;owacja"
  ]
  node [
    id 1217
    label "Egipt"
  ]
  node [
    id 1218
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 1219
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 1220
    label "Mozambik"
  ]
  node [
    id 1221
    label "Kolumbia"
  ]
  node [
    id 1222
    label "Laos"
  ]
  node [
    id 1223
    label "Burundi"
  ]
  node [
    id 1224
    label "Suazi"
  ]
  node [
    id 1225
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 1226
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 1227
    label "Czechy"
  ]
  node [
    id 1228
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 1229
    label "Wyspy_Marshalla"
  ]
  node [
    id 1230
    label "Dominika"
  ]
  node [
    id 1231
    label "Trynidad_i_Tobago"
  ]
  node [
    id 1232
    label "Syria"
  ]
  node [
    id 1233
    label "Palau"
  ]
  node [
    id 1234
    label "Gwinea_Bissau"
  ]
  node [
    id 1235
    label "Liberia"
  ]
  node [
    id 1236
    label "Jamajka"
  ]
  node [
    id 1237
    label "Zimbabwe"
  ]
  node [
    id 1238
    label "Polska"
  ]
  node [
    id 1239
    label "Dominikana"
  ]
  node [
    id 1240
    label "Senegal"
  ]
  node [
    id 1241
    label "Togo"
  ]
  node [
    id 1242
    label "Gujana"
  ]
  node [
    id 1243
    label "Gruzja"
  ]
  node [
    id 1244
    label "Albania"
  ]
  node [
    id 1245
    label "Zair"
  ]
  node [
    id 1246
    label "Meksyk"
  ]
  node [
    id 1247
    label "Macedonia"
  ]
  node [
    id 1248
    label "Chorwacja"
  ]
  node [
    id 1249
    label "Kambod&#380;a"
  ]
  node [
    id 1250
    label "Monako"
  ]
  node [
    id 1251
    label "Mauritius"
  ]
  node [
    id 1252
    label "Gwinea"
  ]
  node [
    id 1253
    label "Mali"
  ]
  node [
    id 1254
    label "Nigeria"
  ]
  node [
    id 1255
    label "Kostaryka"
  ]
  node [
    id 1256
    label "Hanower"
  ]
  node [
    id 1257
    label "Paragwaj"
  ]
  node [
    id 1258
    label "W&#322;ochy"
  ]
  node [
    id 1259
    label "Seszele"
  ]
  node [
    id 1260
    label "Wyspy_Salomona"
  ]
  node [
    id 1261
    label "Hiszpania"
  ]
  node [
    id 1262
    label "Boliwia"
  ]
  node [
    id 1263
    label "Kirgistan"
  ]
  node [
    id 1264
    label "Irlandia"
  ]
  node [
    id 1265
    label "Czad"
  ]
  node [
    id 1266
    label "Irak"
  ]
  node [
    id 1267
    label "Lesoto"
  ]
  node [
    id 1268
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 1269
    label "Malta"
  ]
  node [
    id 1270
    label "Andora"
  ]
  node [
    id 1271
    label "Chiny"
  ]
  node [
    id 1272
    label "Filipiny"
  ]
  node [
    id 1273
    label "Antarktis"
  ]
  node [
    id 1274
    label "Niemcy"
  ]
  node [
    id 1275
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 1276
    label "Pakistan"
  ]
  node [
    id 1277
    label "terytorium"
  ]
  node [
    id 1278
    label "Nikaragua"
  ]
  node [
    id 1279
    label "Brazylia"
  ]
  node [
    id 1280
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 1281
    label "Maroko"
  ]
  node [
    id 1282
    label "Portugalia"
  ]
  node [
    id 1283
    label "Niger"
  ]
  node [
    id 1284
    label "Kenia"
  ]
  node [
    id 1285
    label "Botswana"
  ]
  node [
    id 1286
    label "Fid&#380;i"
  ]
  node [
    id 1287
    label "Tunezja"
  ]
  node [
    id 1288
    label "Australia"
  ]
  node [
    id 1289
    label "Tajlandia"
  ]
  node [
    id 1290
    label "Burkina_Faso"
  ]
  node [
    id 1291
    label "interior"
  ]
  node [
    id 1292
    label "Tanzania"
  ]
  node [
    id 1293
    label "Benin"
  ]
  node [
    id 1294
    label "Indie"
  ]
  node [
    id 1295
    label "&#321;otwa"
  ]
  node [
    id 1296
    label "Kiribati"
  ]
  node [
    id 1297
    label "Antigua_i_Barbuda"
  ]
  node [
    id 1298
    label "Rodezja"
  ]
  node [
    id 1299
    label "Cypr"
  ]
  node [
    id 1300
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 1301
    label "Peru"
  ]
  node [
    id 1302
    label "Austria"
  ]
  node [
    id 1303
    label "Urugwaj"
  ]
  node [
    id 1304
    label "Jordania"
  ]
  node [
    id 1305
    label "Grecja"
  ]
  node [
    id 1306
    label "Azerbejd&#380;an"
  ]
  node [
    id 1307
    label "Turcja"
  ]
  node [
    id 1308
    label "Samoa"
  ]
  node [
    id 1309
    label "Sudan"
  ]
  node [
    id 1310
    label "Oman"
  ]
  node [
    id 1311
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 1312
    label "Uzbekistan"
  ]
  node [
    id 1313
    label "Portoryko"
  ]
  node [
    id 1314
    label "Honduras"
  ]
  node [
    id 1315
    label "Mongolia"
  ]
  node [
    id 1316
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 1317
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 1318
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 1319
    label "Serbia"
  ]
  node [
    id 1320
    label "Tajwan"
  ]
  node [
    id 1321
    label "Wielka_Brytania"
  ]
  node [
    id 1322
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 1323
    label "Liban"
  ]
  node [
    id 1324
    label "Japonia"
  ]
  node [
    id 1325
    label "Ghana"
  ]
  node [
    id 1326
    label "Belgia"
  ]
  node [
    id 1327
    label "Bahrajn"
  ]
  node [
    id 1328
    label "Mikronezja"
  ]
  node [
    id 1329
    label "Etiopia"
  ]
  node [
    id 1330
    label "Kuwejt"
  ]
  node [
    id 1331
    label "Bahamy"
  ]
  node [
    id 1332
    label "Rosja"
  ]
  node [
    id 1333
    label "Mo&#322;dawia"
  ]
  node [
    id 1334
    label "Litwa"
  ]
  node [
    id 1335
    label "S&#322;owenia"
  ]
  node [
    id 1336
    label "Szwajcaria"
  ]
  node [
    id 1337
    label "Erytrea"
  ]
  node [
    id 1338
    label "Arabia_Saudyjska"
  ]
  node [
    id 1339
    label "Kuba"
  ]
  node [
    id 1340
    label "granica_pa&#324;stwa"
  ]
  node [
    id 1341
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 1342
    label "Malezja"
  ]
  node [
    id 1343
    label "Korea"
  ]
  node [
    id 1344
    label "Jemen"
  ]
  node [
    id 1345
    label "Nowa_Zelandia"
  ]
  node [
    id 1346
    label "Namibia"
  ]
  node [
    id 1347
    label "Nauru"
  ]
  node [
    id 1348
    label "holoarktyka"
  ]
  node [
    id 1349
    label "Brunei"
  ]
  node [
    id 1350
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 1351
    label "Khitai"
  ]
  node [
    id 1352
    label "Mauretania"
  ]
  node [
    id 1353
    label "Iran"
  ]
  node [
    id 1354
    label "Gambia"
  ]
  node [
    id 1355
    label "Somalia"
  ]
  node [
    id 1356
    label "Holandia"
  ]
  node [
    id 1357
    label "Turkmenistan"
  ]
  node [
    id 1358
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 1359
    label "Salwador"
  ]
  node [
    id 1360
    label "proszek"
  ]
  node [
    id 1361
    label "tablet"
  ]
  node [
    id 1362
    label "dawka"
  ]
  node [
    id 1363
    label "blister"
  ]
  node [
    id 1364
    label "lekarstwo"
  ]
  node [
    id 1365
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 1366
    label "honours"
  ]
  node [
    id 1367
    label "trophy"
  ]
  node [
    id 1368
    label "oznaczenie"
  ]
  node [
    id 1369
    label "potraktowanie"
  ]
  node [
    id 1370
    label "nagrodzenie"
  ]
  node [
    id 1371
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1372
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 1373
    label "ptaszek"
  ]
  node [
    id 1374
    label "element_anatomiczny"
  ]
  node [
    id 1375
    label "cia&#322;o"
  ]
  node [
    id 1376
    label "przyrodzenie"
  ]
  node [
    id 1377
    label "fiut"
  ]
  node [
    id 1378
    label "shaft"
  ]
  node [
    id 1379
    label "wchodzenie"
  ]
  node [
    id 1380
    label "przedstawiciel"
  ]
  node [
    id 1381
    label "wej&#347;cie"
  ]
  node [
    id 1382
    label "tkanka"
  ]
  node [
    id 1383
    label "jednostka_organizacyjna"
  ]
  node [
    id 1384
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1385
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1386
    label "tw&#243;r"
  ]
  node [
    id 1387
    label "organogeneza"
  ]
  node [
    id 1388
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 1389
    label "struktura_anatomiczna"
  ]
  node [
    id 1390
    label "uk&#322;ad"
  ]
  node [
    id 1391
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 1392
    label "dekortykacja"
  ]
  node [
    id 1393
    label "Izba_Konsyliarska"
  ]
  node [
    id 1394
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1395
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1396
    label "stomia"
  ]
  node [
    id 1397
    label "budowa"
  ]
  node [
    id 1398
    label "okolica"
  ]
  node [
    id 1399
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1400
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 1401
    label "przyk&#322;ad"
  ]
  node [
    id 1402
    label "substytuowa&#263;"
  ]
  node [
    id 1403
    label "substytuowanie"
  ]
  node [
    id 1404
    label "zast&#281;pca"
  ]
  node [
    id 1405
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 1406
    label "byt"
  ]
  node [
    id 1407
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1408
    label "prawo"
  ]
  node [
    id 1409
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 1410
    label "nauka_prawa"
  ]
  node [
    id 1411
    label "penis"
  ]
  node [
    id 1412
    label "ciul"
  ]
  node [
    id 1413
    label "wyzwisko"
  ]
  node [
    id 1414
    label "skurwysyn"
  ]
  node [
    id 1415
    label "dupek"
  ]
  node [
    id 1416
    label "agent"
  ]
  node [
    id 1417
    label "tick"
  ]
  node [
    id 1418
    label "znaczek"
  ]
  node [
    id 1419
    label "nicpo&#324;"
  ]
  node [
    id 1420
    label "genitalia"
  ]
  node [
    id 1421
    label "moszna"
  ]
  node [
    id 1422
    label "ekshumowanie"
  ]
  node [
    id 1423
    label "p&#322;aszczyzna"
  ]
  node [
    id 1424
    label "odwadnia&#263;"
  ]
  node [
    id 1425
    label "zabalsamowanie"
  ]
  node [
    id 1426
    label "odwodni&#263;"
  ]
  node [
    id 1427
    label "sk&#243;ra"
  ]
  node [
    id 1428
    label "staw"
  ]
  node [
    id 1429
    label "ow&#322;osienie"
  ]
  node [
    id 1430
    label "zabalsamowa&#263;"
  ]
  node [
    id 1431
    label "unerwienie"
  ]
  node [
    id 1432
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 1433
    label "kremacja"
  ]
  node [
    id 1434
    label "biorytm"
  ]
  node [
    id 1435
    label "sekcja"
  ]
  node [
    id 1436
    label "istota_&#380;ywa"
  ]
  node [
    id 1437
    label "otworzy&#263;"
  ]
  node [
    id 1438
    label "otwiera&#263;"
  ]
  node [
    id 1439
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 1440
    label "otworzenie"
  ]
  node [
    id 1441
    label "materia"
  ]
  node [
    id 1442
    label "pochowanie"
  ]
  node [
    id 1443
    label "otwieranie"
  ]
  node [
    id 1444
    label "szkielet"
  ]
  node [
    id 1445
    label "tanatoplastyk"
  ]
  node [
    id 1446
    label "odwadnianie"
  ]
  node [
    id 1447
    label "odwodnienie"
  ]
  node [
    id 1448
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 1449
    label "pochowa&#263;"
  ]
  node [
    id 1450
    label "tanatoplastyka"
  ]
  node [
    id 1451
    label "balsamowa&#263;"
  ]
  node [
    id 1452
    label "nieumar&#322;y"
  ]
  node [
    id 1453
    label "balsamowanie"
  ]
  node [
    id 1454
    label "ekshumowa&#263;"
  ]
  node [
    id 1455
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1456
    label "pogrzeb"
  ]
  node [
    id 1457
    label "struktura"
  ]
  node [
    id 1458
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1459
    label "TOPR"
  ]
  node [
    id 1460
    label "endecki"
  ]
  node [
    id 1461
    label "od&#322;am"
  ]
  node [
    id 1462
    label "przedstawicielstwo"
  ]
  node [
    id 1463
    label "Cepelia"
  ]
  node [
    id 1464
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1465
    label "ZBoWiD"
  ]
  node [
    id 1466
    label "organization"
  ]
  node [
    id 1467
    label "centrala"
  ]
  node [
    id 1468
    label "GOPR"
  ]
  node [
    id 1469
    label "ZOMO"
  ]
  node [
    id 1470
    label "ZMP"
  ]
  node [
    id 1471
    label "komitet_koordynacyjny"
  ]
  node [
    id 1472
    label "przybud&#243;wka"
  ]
  node [
    id 1473
    label "boj&#243;wka"
  ]
  node [
    id 1474
    label "dochodzenie"
  ]
  node [
    id 1475
    label "przy&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 1476
    label "atakowanie"
  ]
  node [
    id 1477
    label "w&#322;&#261;czanie_si&#281;"
  ]
  node [
    id 1478
    label "wpuszczanie"
  ]
  node [
    id 1479
    label "zaliczanie_si&#281;"
  ]
  node [
    id 1480
    label "pchanie_si&#281;"
  ]
  node [
    id 1481
    label "poznawanie"
  ]
  node [
    id 1482
    label "entrance"
  ]
  node [
    id 1483
    label "dostawanie_si&#281;"
  ]
  node [
    id 1484
    label "stawanie_si&#281;"
  ]
  node [
    id 1485
    label "&#322;a&#380;enie"
  ]
  node [
    id 1486
    label "wnikanie"
  ]
  node [
    id 1487
    label "zaczynanie"
  ]
  node [
    id 1488
    label "zag&#322;&#281;bianie_si&#281;"
  ]
  node [
    id 1489
    label "spotykanie"
  ]
  node [
    id 1490
    label "nadeptanie"
  ]
  node [
    id 1491
    label "pojawianie_si&#281;"
  ]
  node [
    id 1492
    label "wznoszenie_si&#281;"
  ]
  node [
    id 1493
    label "ingress"
  ]
  node [
    id 1494
    label "przenikanie"
  ]
  node [
    id 1495
    label "climb"
  ]
  node [
    id 1496
    label "nast&#281;powanie"
  ]
  node [
    id 1497
    label "osi&#261;ganie"
  ]
  node [
    id 1498
    label "przekraczanie"
  ]
  node [
    id 1499
    label "wnikni&#281;cie"
  ]
  node [
    id 1500
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 1501
    label "spotkanie"
  ]
  node [
    id 1502
    label "pojawienie_si&#281;"
  ]
  node [
    id 1503
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1504
    label "przenikni&#281;cie"
  ]
  node [
    id 1505
    label "wpuszczenie"
  ]
  node [
    id 1506
    label "zaatakowanie"
  ]
  node [
    id 1507
    label "trespass"
  ]
  node [
    id 1508
    label "dost&#281;p"
  ]
  node [
    id 1509
    label "doj&#347;cie"
  ]
  node [
    id 1510
    label "przekroczenie"
  ]
  node [
    id 1511
    label "otw&#243;r"
  ]
  node [
    id 1512
    label "wzi&#281;cie"
  ]
  node [
    id 1513
    label "vent"
  ]
  node [
    id 1514
    label "stimulation"
  ]
  node [
    id 1515
    label "dostanie_si&#281;"
  ]
  node [
    id 1516
    label "pocz&#261;tek"
  ]
  node [
    id 1517
    label "approach"
  ]
  node [
    id 1518
    label "release"
  ]
  node [
    id 1519
    label "wnij&#347;cie"
  ]
  node [
    id 1520
    label "bramka"
  ]
  node [
    id 1521
    label "wzniesienie_si&#281;"
  ]
  node [
    id 1522
    label "podw&#243;rze"
  ]
  node [
    id 1523
    label "dom"
  ]
  node [
    id 1524
    label "wch&#243;d"
  ]
  node [
    id 1525
    label "nast&#261;pienie"
  ]
  node [
    id 1526
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1527
    label "zacz&#281;cie"
  ]
  node [
    id 1528
    label "stanie_si&#281;"
  ]
  node [
    id 1529
    label "obsi&#261;dni&#281;cie"
  ]
  node [
    id 1530
    label "w&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1531
    label "urz&#261;dzenie"
  ]
  node [
    id 1532
    label "Komitet_Obrony_Robotnik&#243;w"
  ]
  node [
    id 1533
    label "Mazowsze"
  ]
  node [
    id 1534
    label "whole"
  ]
  node [
    id 1535
    label "skupienie"
  ]
  node [
    id 1536
    label "The_Beatles"
  ]
  node [
    id 1537
    label "zabudowania"
  ]
  node [
    id 1538
    label "group"
  ]
  node [
    id 1539
    label "zespolik"
  ]
  node [
    id 1540
    label "schorzenie"
  ]
  node [
    id 1541
    label "ro&#347;lina"
  ]
  node [
    id 1542
    label "Depeche_Mode"
  ]
  node [
    id 1543
    label "batch"
  ]
  node [
    id 1544
    label "perspektywa"
  ]
  node [
    id 1545
    label "diagnoza"
  ]
  node [
    id 1546
    label "przewidywanie"
  ]
  node [
    id 1547
    label "providence"
  ]
  node [
    id 1548
    label "spodziewanie_si&#281;"
  ]
  node [
    id 1549
    label "zapowied&#378;"
  ]
  node [
    id 1550
    label "zamierzanie"
  ]
  node [
    id 1551
    label "diagnosis"
  ]
  node [
    id 1552
    label "sprawdzian"
  ]
  node [
    id 1553
    label "rozpoznanie"
  ]
  node [
    id 1554
    label "dokument"
  ]
  node [
    id 1555
    label "ocena"
  ]
  node [
    id 1556
    label "patrzenie"
  ]
  node [
    id 1557
    label "figura_geometryczna"
  ]
  node [
    id 1558
    label "dystans"
  ]
  node [
    id 1559
    label "patrze&#263;"
  ]
  node [
    id 1560
    label "decentracja"
  ]
  node [
    id 1561
    label "anticipation"
  ]
  node [
    id 1562
    label "plan"
  ]
  node [
    id 1563
    label "krajobraz"
  ]
  node [
    id 1564
    label "expectation"
  ]
  node [
    id 1565
    label "scene"
  ]
  node [
    id 1566
    label "pojmowanie"
  ]
  node [
    id 1567
    label "widzie&#263;"
  ]
  node [
    id 1568
    label "obraz"
  ]
  node [
    id 1569
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 1570
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1571
    label "warto&#347;&#263;"
  ]
  node [
    id 1572
    label "liczba"
  ]
  node [
    id 1573
    label "rewaluowa&#263;"
  ]
  node [
    id 1574
    label "zrewaluowa&#263;"
  ]
  node [
    id 1575
    label "znak_matematyczny"
  ]
  node [
    id 1576
    label "korzy&#347;&#263;"
  ]
  node [
    id 1577
    label "stopie&#324;"
  ]
  node [
    id 1578
    label "dodawanie"
  ]
  node [
    id 1579
    label "wabik"
  ]
  node [
    id 1580
    label "pogl&#261;d"
  ]
  node [
    id 1581
    label "sofcik"
  ]
  node [
    id 1582
    label "kryterium"
  ]
  node [
    id 1583
    label "informacja"
  ]
  node [
    id 1584
    label "appraisal"
  ]
  node [
    id 1585
    label "kategoria"
  ]
  node [
    id 1586
    label "pierwiastek"
  ]
  node [
    id 1587
    label "rozmiar"
  ]
  node [
    id 1588
    label "poj&#281;cie"
  ]
  node [
    id 1589
    label "number"
  ]
  node [
    id 1590
    label "kwadrat_magiczny"
  ]
  node [
    id 1591
    label "podstopie&#324;"
  ]
  node [
    id 1592
    label "wielko&#347;&#263;"
  ]
  node [
    id 1593
    label "rank"
  ]
  node [
    id 1594
    label "minuta"
  ]
  node [
    id 1595
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1596
    label "wschodek"
  ]
  node [
    id 1597
    label "przymiotnik"
  ]
  node [
    id 1598
    label "gama"
  ]
  node [
    id 1599
    label "jednostka"
  ]
  node [
    id 1600
    label "podzia&#322;"
  ]
  node [
    id 1601
    label "element"
  ]
  node [
    id 1602
    label "schody"
  ]
  node [
    id 1603
    label "przys&#322;&#243;wek"
  ]
  node [
    id 1604
    label "degree"
  ]
  node [
    id 1605
    label "szczebel"
  ]
  node [
    id 1606
    label "podn&#243;&#380;ek"
  ]
  node [
    id 1607
    label "zaleta"
  ]
  node [
    id 1608
    label "dobro"
  ]
  node [
    id 1609
    label "zmienna"
  ]
  node [
    id 1610
    label "wskazywanie"
  ]
  node [
    id 1611
    label "wskazywa&#263;"
  ]
  node [
    id 1612
    label "worth"
  ]
  node [
    id 1613
    label "do&#322;&#261;czanie"
  ]
  node [
    id 1614
    label "addition"
  ]
  node [
    id 1615
    label "liczenie"
  ]
  node [
    id 1616
    label "dop&#322;acanie"
  ]
  node [
    id 1617
    label "summation"
  ]
  node [
    id 1618
    label "rozdzielno&#347;&#263;_mno&#380;enia_wzgl&#281;dem_dodawania"
  ]
  node [
    id 1619
    label "uzupe&#322;nianie"
  ]
  node [
    id 1620
    label "dokupowanie"
  ]
  node [
    id 1621
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 1622
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 1623
    label "do&#347;wietlanie"
  ]
  node [
    id 1624
    label "suma"
  ]
  node [
    id 1625
    label "wspominanie"
  ]
  node [
    id 1626
    label "podniesienie"
  ]
  node [
    id 1627
    label "przewarto&#347;ciowywanie"
  ]
  node [
    id 1628
    label "podnoszenie"
  ]
  node [
    id 1629
    label "podnosi&#263;"
  ]
  node [
    id 1630
    label "podnie&#347;&#263;"
  ]
  node [
    id 1631
    label "czynnik"
  ]
  node [
    id 1632
    label "magnes"
  ]
  node [
    id 1633
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 1634
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 1635
    label "zaziera&#263;"
  ]
  node [
    id 1636
    label "move"
  ]
  node [
    id 1637
    label "zaczyna&#263;"
  ]
  node [
    id 1638
    label "spotyka&#263;"
  ]
  node [
    id 1639
    label "przenika&#263;"
  ]
  node [
    id 1640
    label "nast&#281;powa&#263;"
  ]
  node [
    id 1641
    label "mount"
  ]
  node [
    id 1642
    label "go"
  ]
  node [
    id 1643
    label "&#322;oi&#263;"
  ]
  node [
    id 1644
    label "intervene"
  ]
  node [
    id 1645
    label "scale"
  ]
  node [
    id 1646
    label "poznawa&#263;"
  ]
  node [
    id 1647
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 1648
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 1649
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 1650
    label "dochodzi&#263;"
  ]
  node [
    id 1651
    label "przekracza&#263;"
  ]
  node [
    id 1652
    label "wnika&#263;"
  ]
  node [
    id 1653
    label "atakowa&#263;"
  ]
  node [
    id 1654
    label "invade"
  ]
  node [
    id 1655
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 1656
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 1657
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 1658
    label "strike"
  ]
  node [
    id 1659
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1660
    label "ofensywny"
  ]
  node [
    id 1661
    label "przewaga"
  ]
  node [
    id 1662
    label "sport"
  ]
  node [
    id 1663
    label "epidemia"
  ]
  node [
    id 1664
    label "attack"
  ]
  node [
    id 1665
    label "rozgrywa&#263;"
  ]
  node [
    id 1666
    label "krytykowa&#263;"
  ]
  node [
    id 1667
    label "walczy&#263;"
  ]
  node [
    id 1668
    label "aim"
  ]
  node [
    id 1669
    label "trouble_oneself"
  ]
  node [
    id 1670
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1671
    label "napada&#263;"
  ]
  node [
    id 1672
    label "m&#243;wi&#263;"
  ]
  node [
    id 1673
    label "usi&#322;owa&#263;"
  ]
  node [
    id 1674
    label "ograniczenie"
  ]
  node [
    id 1675
    label "przebywa&#263;"
  ]
  node [
    id 1676
    label "conflict"
  ]
  node [
    id 1677
    label "transgress"
  ]
  node [
    id 1678
    label "appear"
  ]
  node [
    id 1679
    label "mija&#263;"
  ]
  node [
    id 1680
    label "zawiera&#263;"
  ]
  node [
    id 1681
    label "cognizance"
  ]
  node [
    id 1682
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 1683
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1684
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 1685
    label "go_steady"
  ]
  node [
    id 1686
    label "detect"
  ]
  node [
    id 1687
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 1688
    label "hurt"
  ]
  node [
    id 1689
    label "styka&#263;_si&#281;"
  ]
  node [
    id 1690
    label "transpire"
  ]
  node [
    id 1691
    label "naciska&#263;"
  ]
  node [
    id 1692
    label "alternate"
  ]
  node [
    id 1693
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 1694
    label "chance"
  ]
  node [
    id 1695
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 1696
    label "uzyskiwa&#263;"
  ]
  node [
    id 1697
    label "mark"
  ]
  node [
    id 1698
    label "claim"
  ]
  node [
    id 1699
    label "ripen"
  ]
  node [
    id 1700
    label "supervene"
  ]
  node [
    id 1701
    label "doczeka&#263;"
  ]
  node [
    id 1702
    label "przesy&#322;ka"
  ]
  node [
    id 1703
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 1704
    label "doznawa&#263;"
  ]
  node [
    id 1705
    label "reach"
  ]
  node [
    id 1706
    label "zachodzi&#263;"
  ]
  node [
    id 1707
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1708
    label "postrzega&#263;"
  ]
  node [
    id 1709
    label "orgazm"
  ]
  node [
    id 1710
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 1711
    label "dokoptowywa&#263;"
  ]
  node [
    id 1712
    label "dolatywa&#263;"
  ]
  node [
    id 1713
    label "submit"
  ]
  node [
    id 1714
    label "odejmowa&#263;"
  ]
  node [
    id 1715
    label "bankrupt"
  ]
  node [
    id 1716
    label "open"
  ]
  node [
    id 1717
    label "set_about"
  ]
  node [
    id 1718
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 1719
    label "begin"
  ]
  node [
    id 1720
    label "fall"
  ]
  node [
    id 1721
    label "znajdowa&#263;"
  ]
  node [
    id 1722
    label "happen"
  ]
  node [
    id 1723
    label "goban"
  ]
  node [
    id 1724
    label "gra_planszowa"
  ]
  node [
    id 1725
    label "sport_umys&#322;owy"
  ]
  node [
    id 1726
    label "chi&#324;ski"
  ]
  node [
    id 1727
    label "zagl&#261;da&#263;"
  ]
  node [
    id 1728
    label "pokonywa&#263;"
  ]
  node [
    id 1729
    label "nat&#322;uszcza&#263;"
  ]
  node [
    id 1730
    label "je&#378;dzi&#263;"
  ]
  node [
    id 1731
    label "obgadywa&#263;"
  ]
  node [
    id 1732
    label "bi&#263;"
  ]
  node [
    id 1733
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 1734
    label "naci&#261;ga&#263;"
  ]
  node [
    id 1735
    label "tankowa&#263;"
  ]
  node [
    id 1736
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 1737
    label "bang"
  ]
  node [
    id 1738
    label "domy&#347;la&#263;_si&#281;"
  ]
  node [
    id 1739
    label "drench"
  ]
  node [
    id 1740
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 1741
    label "meet"
  ]
  node [
    id 1742
    label "substancja"
  ]
  node [
    id 1743
    label "saturate"
  ]
  node [
    id 1744
    label "rozpowszechnia&#263;_si&#281;"
  ]
  node [
    id 1745
    label "tworzy&#263;"
  ]
  node [
    id 1746
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 1747
    label "porywa&#263;"
  ]
  node [
    id 1748
    label "take"
  ]
  node [
    id 1749
    label "poczytywa&#263;"
  ]
  node [
    id 1750
    label "levy"
  ]
  node [
    id 1751
    label "raise"
  ]
  node [
    id 1752
    label "przyjmowa&#263;"
  ]
  node [
    id 1753
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 1754
    label "rucha&#263;"
  ]
  node [
    id 1755
    label "prowadzi&#263;"
  ]
  node [
    id 1756
    label "za&#380;ywa&#263;"
  ]
  node [
    id 1757
    label "otrzymywa&#263;"
  ]
  node [
    id 1758
    label "&#263;pa&#263;"
  ]
  node [
    id 1759
    label "interpretowa&#263;"
  ]
  node [
    id 1760
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1761
    label "dostawa&#263;"
  ]
  node [
    id 1762
    label "rusza&#263;"
  ]
  node [
    id 1763
    label "grza&#263;"
  ]
  node [
    id 1764
    label "wch&#322;ania&#263;"
  ]
  node [
    id 1765
    label "wygrywa&#263;"
  ]
  node [
    id 1766
    label "ucieka&#263;"
  ]
  node [
    id 1767
    label "arise"
  ]
  node [
    id 1768
    label "uprawia&#263;_seks"
  ]
  node [
    id 1769
    label "abstract"
  ]
  node [
    id 1770
    label "towarzystwo"
  ]
  node [
    id 1771
    label "branie"
  ]
  node [
    id 1772
    label "zalicza&#263;"
  ]
  node [
    id 1773
    label "wzi&#261;&#263;"
  ]
  node [
    id 1774
    label "&#322;apa&#263;"
  ]
  node [
    id 1775
    label "przewa&#380;a&#263;"
  ]
  node [
    id 1776
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 1777
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 1778
    label "blokada"
  ]
  node [
    id 1779
    label "hurtownia"
  ]
  node [
    id 1780
    label "pomieszczenie"
  ]
  node [
    id 1781
    label "pas"
  ]
  node [
    id 1782
    label "basic"
  ]
  node [
    id 1783
    label "sk&#322;adnik"
  ]
  node [
    id 1784
    label "sklep"
  ]
  node [
    id 1785
    label "obr&#243;bka"
  ]
  node [
    id 1786
    label "constitution"
  ]
  node [
    id 1787
    label "fabryka"
  ]
  node [
    id 1788
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1789
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 1790
    label "syf"
  ]
  node [
    id 1791
    label "rank_and_file"
  ]
  node [
    id 1792
    label "tabulacja"
  ]
  node [
    id 1793
    label "mechanika"
  ]
  node [
    id 1794
    label "o&#347;"
  ]
  node [
    id 1795
    label "usenet"
  ]
  node [
    id 1796
    label "rozprz&#261;c"
  ]
  node [
    id 1797
    label "zachowanie"
  ]
  node [
    id 1798
    label "cybernetyk"
  ]
  node [
    id 1799
    label "podsystem"
  ]
  node [
    id 1800
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1801
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1802
    label "systemat"
  ]
  node [
    id 1803
    label "konstrukcja"
  ]
  node [
    id 1804
    label "konstelacja"
  ]
  node [
    id 1805
    label "amfilada"
  ]
  node [
    id 1806
    label "front"
  ]
  node [
    id 1807
    label "apartment"
  ]
  node [
    id 1808
    label "pod&#322;oga"
  ]
  node [
    id 1809
    label "udost&#281;pnienie"
  ]
  node [
    id 1810
    label "sklepienie"
  ]
  node [
    id 1811
    label "sufit"
  ]
  node [
    id 1812
    label "umieszczenie"
  ]
  node [
    id 1813
    label "zakamarek"
  ]
  node [
    id 1814
    label "proces_technologiczny"
  ]
  node [
    id 1815
    label "proces"
  ]
  node [
    id 1816
    label "ekscerpcja"
  ]
  node [
    id 1817
    label "j&#281;zykowo"
  ]
  node [
    id 1818
    label "wypowied&#378;"
  ]
  node [
    id 1819
    label "redakcja"
  ]
  node [
    id 1820
    label "pomini&#281;cie"
  ]
  node [
    id 1821
    label "dzie&#322;o"
  ]
  node [
    id 1822
    label "preparacja"
  ]
  node [
    id 1823
    label "odmianka"
  ]
  node [
    id 1824
    label "opu&#347;ci&#263;"
  ]
  node [
    id 1825
    label "koniektura"
  ]
  node [
    id 1826
    label "pisa&#263;"
  ]
  node [
    id 1827
    label "obelga"
  ]
  node [
    id 1828
    label "warunek_lokalowy"
  ]
  node [
    id 1829
    label "plac"
  ]
  node [
    id 1830
    label "location"
  ]
  node [
    id 1831
    label "uwaga"
  ]
  node [
    id 1832
    label "przestrze&#324;"
  ]
  node [
    id 1833
    label "status"
  ]
  node [
    id 1834
    label "rz&#261;d"
  ]
  node [
    id 1835
    label "surowiec"
  ]
  node [
    id 1836
    label "fixture"
  ]
  node [
    id 1837
    label "divisor"
  ]
  node [
    id 1838
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1839
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 1840
    label "energia"
  ]
  node [
    id 1841
    label "&#347;wieci&#263;"
  ]
  node [
    id 1842
    label "odst&#281;p"
  ]
  node [
    id 1843
    label "wpadni&#281;cie"
  ]
  node [
    id 1844
    label "interpretacja"
  ]
  node [
    id 1845
    label "fotokataliza"
  ]
  node [
    id 1846
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 1847
    label "rzuca&#263;"
  ]
  node [
    id 1848
    label "obsadnik"
  ]
  node [
    id 1849
    label "promieniowanie_optyczne"
  ]
  node [
    id 1850
    label "lampa"
  ]
  node [
    id 1851
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 1852
    label "ja&#347;nia"
  ]
  node [
    id 1853
    label "light"
  ]
  node [
    id 1854
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 1855
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 1856
    label "rzuci&#263;"
  ]
  node [
    id 1857
    label "o&#347;wietlenie"
  ]
  node [
    id 1858
    label "punkt_widzenia"
  ]
  node [
    id 1859
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 1860
    label "przy&#263;mienie"
  ]
  node [
    id 1861
    label "instalacja"
  ]
  node [
    id 1862
    label "&#347;wiecenie"
  ]
  node [
    id 1863
    label "radiance"
  ]
  node [
    id 1864
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 1865
    label "przy&#263;mi&#263;"
  ]
  node [
    id 1866
    label "b&#322;ysk"
  ]
  node [
    id 1867
    label "&#347;wiat&#322;y"
  ]
  node [
    id 1868
    label "promie&#324;"
  ]
  node [
    id 1869
    label "m&#261;drze"
  ]
  node [
    id 1870
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 1871
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1872
    label "lighting"
  ]
  node [
    id 1873
    label "lighter"
  ]
  node [
    id 1874
    label "rzucenie"
  ]
  node [
    id 1875
    label "plama"
  ]
  node [
    id 1876
    label "&#347;rednica"
  ]
  node [
    id 1877
    label "wpadanie"
  ]
  node [
    id 1878
    label "przy&#263;miewanie"
  ]
  node [
    id 1879
    label "rzucanie"
  ]
  node [
    id 1880
    label "bloking"
  ]
  node [
    id 1881
    label "znieczulenie"
  ]
  node [
    id 1882
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1883
    label "kolej"
  ]
  node [
    id 1884
    label "block"
  ]
  node [
    id 1885
    label "utrudnienie"
  ]
  node [
    id 1886
    label "arrest"
  ]
  node [
    id 1887
    label "anestezja"
  ]
  node [
    id 1888
    label "ochrona"
  ]
  node [
    id 1889
    label "izolacja"
  ]
  node [
    id 1890
    label "blok"
  ]
  node [
    id 1891
    label "zwrotnica"
  ]
  node [
    id 1892
    label "siatk&#243;wka"
  ]
  node [
    id 1893
    label "sankcja"
  ]
  node [
    id 1894
    label "semafor"
  ]
  node [
    id 1895
    label "obrona"
  ]
  node [
    id 1896
    label "deadlock"
  ]
  node [
    id 1897
    label "lock"
  ]
  node [
    id 1898
    label "niemo&#380;no&#347;&#263;"
  ]
  node [
    id 1899
    label "jako&#347;&#263;"
  ]
  node [
    id 1900
    label "syphilis"
  ]
  node [
    id 1901
    label "tragedia"
  ]
  node [
    id 1902
    label "nieporz&#261;dek"
  ]
  node [
    id 1903
    label "kr&#281;tek_blady"
  ]
  node [
    id 1904
    label "krosta"
  ]
  node [
    id 1905
    label "choroba_dworska"
  ]
  node [
    id 1906
    label "choroba_bakteryjna"
  ]
  node [
    id 1907
    label "zabrudzenie"
  ]
  node [
    id 1908
    label "choroba_weneryczna"
  ]
  node [
    id 1909
    label "ki&#322;a_wrodzona"
  ]
  node [
    id 1910
    label "szankier_twardy"
  ]
  node [
    id 1911
    label "spot"
  ]
  node [
    id 1912
    label "zanieczyszczenie"
  ]
  node [
    id 1913
    label "z&#322;y"
  ]
  node [
    id 1914
    label "tabulation"
  ]
  node [
    id 1915
    label "edycja"
  ]
  node [
    id 1916
    label "dodatek"
  ]
  node [
    id 1917
    label "licytacja"
  ]
  node [
    id 1918
    label "kawa&#322;ek"
  ]
  node [
    id 1919
    label "figura_heraldyczna"
  ]
  node [
    id 1920
    label "bielizna"
  ]
  node [
    id 1921
    label "heraldyka"
  ]
  node [
    id 1922
    label "odznaka"
  ]
  node [
    id 1923
    label "tarcza_herbowa"
  ]
  node [
    id 1924
    label "nap&#281;d"
  ]
  node [
    id 1925
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 1926
    label "prz&#281;dzalnia"
  ]
  node [
    id 1927
    label "rurownia"
  ]
  node [
    id 1928
    label "wytrawialnia"
  ]
  node [
    id 1929
    label "ucieralnia"
  ]
  node [
    id 1930
    label "tkalnia"
  ]
  node [
    id 1931
    label "farbiarnia"
  ]
  node [
    id 1932
    label "szwalnia"
  ]
  node [
    id 1933
    label "szlifiernia"
  ]
  node [
    id 1934
    label "probiernia"
  ]
  node [
    id 1935
    label "fryzernia"
  ]
  node [
    id 1936
    label "celulozownia"
  ]
  node [
    id 1937
    label "hala"
  ]
  node [
    id 1938
    label "magazyn"
  ]
  node [
    id 1939
    label "gospodarka"
  ]
  node [
    id 1940
    label "dziewiarnia"
  ]
  node [
    id 1941
    label "firma"
  ]
  node [
    id 1942
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1943
    label "t&#322;o"
  ]
  node [
    id 1944
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 1945
    label "room"
  ]
  node [
    id 1946
    label "dw&#243;r"
  ]
  node [
    id 1947
    label "okazja"
  ]
  node [
    id 1948
    label "square"
  ]
  node [
    id 1949
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1950
    label "socjologia"
  ]
  node [
    id 1951
    label "boisko"
  ]
  node [
    id 1952
    label "dziedzina"
  ]
  node [
    id 1953
    label "baza_danych"
  ]
  node [
    id 1954
    label "region"
  ]
  node [
    id 1955
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 1956
    label "plane"
  ]
  node [
    id 1957
    label "gem"
  ]
  node [
    id 1958
    label "runda"
  ]
  node [
    id 1959
    label "zestaw"
  ]
  node [
    id 1960
    label "p&#243;&#322;ka"
  ]
  node [
    id 1961
    label "stoisko"
  ]
  node [
    id 1962
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 1963
    label "obiekt_handlowy"
  ]
  node [
    id 1964
    label "zaplecze"
  ]
  node [
    id 1965
    label "witryna"
  ]
  node [
    id 1966
    label "ognisko"
  ]
  node [
    id 1967
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 1968
    label "powalenie"
  ]
  node [
    id 1969
    label "odezwanie_si&#281;"
  ]
  node [
    id 1970
    label "grupa_ryzyka"
  ]
  node [
    id 1971
    label "przypadek"
  ]
  node [
    id 1972
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 1973
    label "nabawienie_si&#281;"
  ]
  node [
    id 1974
    label "inkubacja"
  ]
  node [
    id 1975
    label "kryzys"
  ]
  node [
    id 1976
    label "powali&#263;"
  ]
  node [
    id 1977
    label "remisja"
  ]
  node [
    id 1978
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 1979
    label "zajmowa&#263;"
  ]
  node [
    id 1980
    label "zaburzenie"
  ]
  node [
    id 1981
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1982
    label "badanie_histopatologiczne"
  ]
  node [
    id 1983
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 1984
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 1985
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1986
    label "odzywanie_si&#281;"
  ]
  node [
    id 1987
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1988
    label "nabawianie_si&#281;"
  ]
  node [
    id 1989
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 1990
    label "zajmowanie"
  ]
  node [
    id 1991
    label "series"
  ]
  node [
    id 1992
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1993
    label "uprawianie"
  ]
  node [
    id 1994
    label "praca_rolnicza"
  ]
  node [
    id 1995
    label "collection"
  ]
  node [
    id 1996
    label "dane"
  ]
  node [
    id 1997
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1998
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1999
    label "sum"
  ]
  node [
    id 2000
    label "agglomeration"
  ]
  node [
    id 2001
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 2002
    label "przegrupowanie"
  ]
  node [
    id 2003
    label "congestion"
  ]
  node [
    id 2004
    label "zgromadzenie"
  ]
  node [
    id 2005
    label "kupienie"
  ]
  node [
    id 2006
    label "z&#322;&#261;czenie"
  ]
  node [
    id 2007
    label "po&#322;&#261;czenie"
  ]
  node [
    id 2008
    label "concentration"
  ]
  node [
    id 2009
    label "kompleks"
  ]
  node [
    id 2010
    label "Kurpie"
  ]
  node [
    id 2011
    label "Mogielnica"
  ]
  node [
    id 2012
    label "odtwarzanie"
  ]
  node [
    id 2013
    label "uatrakcyjnianie"
  ]
  node [
    id 2014
    label "zast&#281;powanie"
  ]
  node [
    id 2015
    label "odbudowywanie"
  ]
  node [
    id 2016
    label "rejuvenation"
  ]
  node [
    id 2017
    label "m&#322;odszy"
  ]
  node [
    id 2018
    label "odbudowywa&#263;"
  ]
  node [
    id 2019
    label "m&#322;odzi&#263;"
  ]
  node [
    id 2020
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 2021
    label "przewietrza&#263;"
  ]
  node [
    id 2022
    label "odtwarza&#263;"
  ]
  node [
    id 2023
    label "uatrakcyjni&#263;"
  ]
  node [
    id 2024
    label "przewietrzy&#263;"
  ]
  node [
    id 2025
    label "regenerate"
  ]
  node [
    id 2026
    label "odtworzy&#263;"
  ]
  node [
    id 2027
    label "wymieni&#263;"
  ]
  node [
    id 2028
    label "odbudowa&#263;"
  ]
  node [
    id 2029
    label "wymienienie"
  ]
  node [
    id 2030
    label "uatrakcyjnienie"
  ]
  node [
    id 2031
    label "odbudowanie"
  ]
  node [
    id 2032
    label "odtworzenie"
  ]
  node [
    id 2033
    label "zbiorowisko"
  ]
  node [
    id 2034
    label "ro&#347;liny"
  ]
  node [
    id 2035
    label "p&#281;d"
  ]
  node [
    id 2036
    label "wegetowanie"
  ]
  node [
    id 2037
    label "zadziorek"
  ]
  node [
    id 2038
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 2039
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 2040
    label "do&#322;owa&#263;"
  ]
  node [
    id 2041
    label "wegetacja"
  ]
  node [
    id 2042
    label "owoc"
  ]
  node [
    id 2043
    label "strzyc"
  ]
  node [
    id 2044
    label "w&#322;&#243;kno"
  ]
  node [
    id 2045
    label "g&#322;uszenie"
  ]
  node [
    id 2046
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 2047
    label "fitotron"
  ]
  node [
    id 2048
    label "bulwka"
  ]
  node [
    id 2049
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 2050
    label "odn&#243;&#380;ka"
  ]
  node [
    id 2051
    label "epiderma"
  ]
  node [
    id 2052
    label "gumoza"
  ]
  node [
    id 2053
    label "strzy&#380;enie"
  ]
  node [
    id 2054
    label "wypotnik"
  ]
  node [
    id 2055
    label "flawonoid"
  ]
  node [
    id 2056
    label "wyro&#347;le"
  ]
  node [
    id 2057
    label "do&#322;owanie"
  ]
  node [
    id 2058
    label "g&#322;uszy&#263;"
  ]
  node [
    id 2059
    label "pora&#380;a&#263;"
  ]
  node [
    id 2060
    label "fitocenoza"
  ]
  node [
    id 2061
    label "hodowla"
  ]
  node [
    id 2062
    label "fotoautotrof"
  ]
  node [
    id 2063
    label "nieuleczalnie_chory"
  ]
  node [
    id 2064
    label "wegetowa&#263;"
  ]
  node [
    id 2065
    label "pochewka"
  ]
  node [
    id 2066
    label "sok"
  ]
  node [
    id 2067
    label "system_korzeniowy"
  ]
  node [
    id 2068
    label "zawi&#261;zek"
  ]
  node [
    id 2069
    label "informatyk"
  ]
  node [
    id 2070
    label "godzina"
  ]
  node [
    id 2071
    label "time"
  ]
  node [
    id 2072
    label "doba"
  ]
  node [
    id 2073
    label "p&#243;&#322;godzina"
  ]
  node [
    id 2074
    label "jednostka_czasu"
  ]
  node [
    id 2075
    label "kwadrans"
  ]
  node [
    id 2076
    label "Polish"
  ]
  node [
    id 2077
    label "goniony"
  ]
  node [
    id 2078
    label "oberek"
  ]
  node [
    id 2079
    label "ryba_po_grecku"
  ]
  node [
    id 2080
    label "sztajer"
  ]
  node [
    id 2081
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 2082
    label "krakowiak"
  ]
  node [
    id 2083
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 2084
    label "pierogi_ruskie"
  ]
  node [
    id 2085
    label "lacki"
  ]
  node [
    id 2086
    label "polak"
  ]
  node [
    id 2087
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 2088
    label "chodzony"
  ]
  node [
    id 2089
    label "po_polsku"
  ]
  node [
    id 2090
    label "mazur"
  ]
  node [
    id 2091
    label "polsko"
  ]
  node [
    id 2092
    label "skoczny"
  ]
  node [
    id 2093
    label "drabant"
  ]
  node [
    id 2094
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 2095
    label "j&#281;zyk"
  ]
  node [
    id 2096
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 2097
    label "artykulator"
  ]
  node [
    id 2098
    label "kod"
  ]
  node [
    id 2099
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 2100
    label "gramatyka"
  ]
  node [
    id 2101
    label "stylik"
  ]
  node [
    id 2102
    label "przet&#322;umaczenie"
  ]
  node [
    id 2103
    label "formalizowanie"
  ]
  node [
    id 2104
    label "ssanie"
  ]
  node [
    id 2105
    label "ssa&#263;"
  ]
  node [
    id 2106
    label "language"
  ]
  node [
    id 2107
    label "liza&#263;"
  ]
  node [
    id 2108
    label "napisa&#263;"
  ]
  node [
    id 2109
    label "konsonantyzm"
  ]
  node [
    id 2110
    label "wokalizm"
  ]
  node [
    id 2111
    label "fonetyka"
  ]
  node [
    id 2112
    label "jeniec"
  ]
  node [
    id 2113
    label "but"
  ]
  node [
    id 2114
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 2115
    label "po_koroniarsku"
  ]
  node [
    id 2116
    label "kultura_duchowa"
  ]
  node [
    id 2117
    label "m&#243;wienie"
  ]
  node [
    id 2118
    label "pype&#263;"
  ]
  node [
    id 2119
    label "lizanie"
  ]
  node [
    id 2120
    label "pismo"
  ]
  node [
    id 2121
    label "formalizowa&#263;"
  ]
  node [
    id 2122
    label "rozumie&#263;"
  ]
  node [
    id 2123
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 2124
    label "rozumienie"
  ]
  node [
    id 2125
    label "makroglosja"
  ]
  node [
    id 2126
    label "jama_ustna"
  ]
  node [
    id 2127
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 2128
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 2129
    label "natural_language"
  ]
  node [
    id 2130
    label "s&#322;ownictwo"
  ]
  node [
    id 2131
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 2132
    label "wschodnioeuropejski"
  ]
  node [
    id 2133
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 2134
    label "poga&#324;ski"
  ]
  node [
    id 2135
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 2136
    label "topielec"
  ]
  node [
    id 2137
    label "europejski"
  ]
  node [
    id 2138
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 2139
    label "langosz"
  ]
  node [
    id 2140
    label "zboczenie"
  ]
  node [
    id 2141
    label "om&#243;wienie"
  ]
  node [
    id 2142
    label "sponiewieranie"
  ]
  node [
    id 2143
    label "discipline"
  ]
  node [
    id 2144
    label "rzecz"
  ]
  node [
    id 2145
    label "omawia&#263;"
  ]
  node [
    id 2146
    label "kr&#261;&#380;enie"
  ]
  node [
    id 2147
    label "tre&#347;&#263;"
  ]
  node [
    id 2148
    label "sponiewiera&#263;"
  ]
  node [
    id 2149
    label "entity"
  ]
  node [
    id 2150
    label "tematyka"
  ]
  node [
    id 2151
    label "w&#261;tek"
  ]
  node [
    id 2152
    label "charakter"
  ]
  node [
    id 2153
    label "zbaczanie"
  ]
  node [
    id 2154
    label "program_nauczania"
  ]
  node [
    id 2155
    label "om&#243;wi&#263;"
  ]
  node [
    id 2156
    label "omawianie"
  ]
  node [
    id 2157
    label "thing"
  ]
  node [
    id 2158
    label "kultura"
  ]
  node [
    id 2159
    label "istota"
  ]
  node [
    id 2160
    label "zbacza&#263;"
  ]
  node [
    id 2161
    label "zboczy&#263;"
  ]
  node [
    id 2162
    label "gwardzista"
  ]
  node [
    id 2163
    label "melodia"
  ]
  node [
    id 2164
    label "taniec"
  ]
  node [
    id 2165
    label "taniec_ludowy"
  ]
  node [
    id 2166
    label "&#347;redniowieczny"
  ]
  node [
    id 2167
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 2168
    label "weso&#322;y"
  ]
  node [
    id 2169
    label "sprawny"
  ]
  node [
    id 2170
    label "rytmiczny"
  ]
  node [
    id 2171
    label "skocznie"
  ]
  node [
    id 2172
    label "energiczny"
  ]
  node [
    id 2173
    label "lendler"
  ]
  node [
    id 2174
    label "austriacki"
  ]
  node [
    id 2175
    label "polka"
  ]
  node [
    id 2176
    label "europejsko"
  ]
  node [
    id 2177
    label "przytup"
  ]
  node [
    id 2178
    label "ho&#322;ubiec"
  ]
  node [
    id 2179
    label "wodzi&#263;"
  ]
  node [
    id 2180
    label "ludowy"
  ]
  node [
    id 2181
    label "pie&#347;&#324;"
  ]
  node [
    id 2182
    label "mieszkaniec"
  ]
  node [
    id 2183
    label "centu&#347;"
  ]
  node [
    id 2184
    label "lalka"
  ]
  node [
    id 2185
    label "Ma&#322;opolanin"
  ]
  node [
    id 2186
    label "krakauer"
  ]
  node [
    id 2187
    label "przeci&#261;&#380;alno&#347;&#263;"
  ]
  node [
    id 2188
    label "mimo&#347;r&#243;d"
  ]
  node [
    id 2189
    label "tuleja"
  ]
  node [
    id 2190
    label "kad&#322;ub"
  ]
  node [
    id 2191
    label "n&#243;&#380;"
  ]
  node [
    id 2192
    label "b&#281;benek"
  ]
  node [
    id 2193
    label "wa&#322;"
  ]
  node [
    id 2194
    label "maszyneria"
  ]
  node [
    id 2195
    label "prototypownia"
  ]
  node [
    id 2196
    label "trawers"
  ]
  node [
    id 2197
    label "deflektor"
  ]
  node [
    id 2198
    label "kolumna"
  ]
  node [
    id 2199
    label "mechanizm"
  ]
  node [
    id 2200
    label "wa&#322;ek"
  ]
  node [
    id 2201
    label "b&#281;ben"
  ]
  node [
    id 2202
    label "rz&#281;zi&#263;"
  ]
  node [
    id 2203
    label "przyk&#322;adka"
  ]
  node [
    id 2204
    label "t&#322;ok"
  ]
  node [
    id 2205
    label "dehumanizacja"
  ]
  node [
    id 2206
    label "rz&#281;&#380;enie"
  ]
  node [
    id 2207
    label "kom&#243;rka"
  ]
  node [
    id 2208
    label "furnishing"
  ]
  node [
    id 2209
    label "zabezpieczenie"
  ]
  node [
    id 2210
    label "wyrz&#261;dzenie"
  ]
  node [
    id 2211
    label "zagospodarowanie"
  ]
  node [
    id 2212
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 2213
    label "ig&#322;a"
  ]
  node [
    id 2214
    label "narz&#281;dzie"
  ]
  node [
    id 2215
    label "wirnik"
  ]
  node [
    id 2216
    label "aparatura"
  ]
  node [
    id 2217
    label "system_energetyczny"
  ]
  node [
    id 2218
    label "impulsator"
  ]
  node [
    id 2219
    label "sprz&#281;t"
  ]
  node [
    id 2220
    label "blokowanie"
  ]
  node [
    id 2221
    label "zablokowanie"
  ]
  node [
    id 2222
    label "przygotowanie"
  ]
  node [
    id 2223
    label "komora"
  ]
  node [
    id 2224
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 2225
    label "opakowanie"
  ]
  node [
    id 2226
    label "tuba"
  ]
  node [
    id 2227
    label "box"
  ]
  node [
    id 2228
    label "rura"
  ]
  node [
    id 2229
    label "press"
  ]
  node [
    id 2230
    label "magiel"
  ]
  node [
    id 2231
    label "puppy_love"
  ]
  node [
    id 2232
    label "ciasnota"
  ]
  node [
    id 2233
    label "narz&#261;d_ruchu"
  ]
  node [
    id 2234
    label "mi&#281;sie&#324;_&#322;okciowy"
  ]
  node [
    id 2235
    label "ko&#347;&#263;_ramieniowa"
  ]
  node [
    id 2236
    label "triceps"
  ]
  node [
    id 2237
    label "biceps"
  ]
  node [
    id 2238
    label "robot_przemys&#322;owy"
  ]
  node [
    id 2239
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 2240
    label "podstawa"
  ]
  node [
    id 2241
    label "b&#322;ona"
  ]
  node [
    id 2242
    label "eardrum"
  ]
  node [
    id 2243
    label "ucho_&#347;rodkowe"
  ]
  node [
    id 2244
    label "cylinder"
  ]
  node [
    id 2245
    label "brzuszysko"
  ]
  node [
    id 2246
    label "dziecko"
  ]
  node [
    id 2247
    label "membranofon"
  ]
  node [
    id 2248
    label "kopu&#322;a"
  ]
  node [
    id 2249
    label "naci&#261;g_perkusyjny"
  ]
  node [
    id 2250
    label "szpulka"
  ]
  node [
    id 2251
    label "d&#378;wig"
  ]
  node [
    id 2252
    label "pikuty"
  ]
  node [
    id 2253
    label "sztuciec"
  ]
  node [
    id 2254
    label "ostrze"
  ]
  node [
    id 2255
    label "kosa"
  ]
  node [
    id 2256
    label "knife"
  ]
  node [
    id 2257
    label "parametr"
  ]
  node [
    id 2258
    label "przewa&#322;"
  ]
  node [
    id 2259
    label "wy&#380;ymaczka"
  ]
  node [
    id 2260
    label "chutzpa"
  ]
  node [
    id 2261
    label "wa&#322;kowanie"
  ]
  node [
    id 2262
    label "fa&#322;da"
  ]
  node [
    id 2263
    label "p&#243;&#322;wa&#322;ek"
  ]
  node [
    id 2264
    label "poduszka"
  ]
  node [
    id 2265
    label "post&#281;pek"
  ]
  node [
    id 2266
    label "walec"
  ]
  node [
    id 2267
    label "amortyzator"
  ]
  node [
    id 2268
    label "ventilator"
  ]
  node [
    id 2269
    label "deflector"
  ]
  node [
    id 2270
    label "przyrz&#261;d_nawigacyjny"
  ]
  node [
    id 2271
    label "lot"
  ]
  node [
    id 2272
    label "podpora"
  ]
  node [
    id 2273
    label "&#263;wiczenie"
  ]
  node [
    id 2274
    label "droga_wspinaczkowa"
  ]
  node [
    id 2275
    label "traversal"
  ]
  node [
    id 2276
    label "wspinaczka"
  ]
  node [
    id 2277
    label "belka"
  ]
  node [
    id 2278
    label "grobla"
  ]
  node [
    id 2279
    label "column"
  ]
  node [
    id 2280
    label "s&#322;up"
  ]
  node [
    id 2281
    label "awangarda"
  ]
  node [
    id 2282
    label "heading"
  ]
  node [
    id 2283
    label "dzia&#322;"
  ]
  node [
    id 2284
    label "wykres"
  ]
  node [
    id 2285
    label "ogniwo_galwaniczne"
  ]
  node [
    id 2286
    label "pomnik"
  ]
  node [
    id 2287
    label "artyku&#322;"
  ]
  node [
    id 2288
    label "g&#322;o&#347;nik"
  ]
  node [
    id 2289
    label "plinta"
  ]
  node [
    id 2290
    label "tabela"
  ]
  node [
    id 2291
    label "trzon"
  ]
  node [
    id 2292
    label "szyk"
  ]
  node [
    id 2293
    label "megaron"
  ]
  node [
    id 2294
    label "macierz"
  ]
  node [
    id 2295
    label "reprezentacja"
  ]
  node [
    id 2296
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 2297
    label "baza"
  ]
  node [
    id 2298
    label "ariergarda"
  ]
  node [
    id 2299
    label "&#322;am"
  ]
  node [
    id 2300
    label "kierownica"
  ]
  node [
    id 2301
    label "roller"
  ]
  node [
    id 2302
    label "usypisko"
  ]
  node [
    id 2303
    label "szaniec"
  ]
  node [
    id 2304
    label "naiwniak"
  ]
  node [
    id 2305
    label "pojazd_mechaniczny"
  ]
  node [
    id 2306
    label "maszyna_robocza"
  ]
  node [
    id 2307
    label "grodzisko"
  ]
  node [
    id 2308
    label "pojazd_budowlany"
  ]
  node [
    id 2309
    label "emblemat"
  ]
  node [
    id 2310
    label "rotating_shaft"
  ]
  node [
    id 2311
    label "obwa&#322;owanie"
  ]
  node [
    id 2312
    label "narys_bastionowy"
  ]
  node [
    id 2313
    label "kil"
  ]
  node [
    id 2314
    label "nadst&#281;pka"
  ]
  node [
    id 2315
    label "pachwina"
  ]
  node [
    id 2316
    label "brzuch"
  ]
  node [
    id 2317
    label "statek"
  ]
  node [
    id 2318
    label "konstrukcyjna_linia_wodna"
  ]
  node [
    id 2319
    label "dekolt"
  ]
  node [
    id 2320
    label "zad"
  ]
  node [
    id 2321
    label "z&#322;ad"
  ]
  node [
    id 2322
    label "z&#281;za"
  ]
  node [
    id 2323
    label "korpus"
  ]
  node [
    id 2324
    label "pupa"
  ]
  node [
    id 2325
    label "samolot"
  ]
  node [
    id 2326
    label "krocze"
  ]
  node [
    id 2327
    label "pier&#347;"
  ]
  node [
    id 2328
    label "p&#322;atowiec"
  ]
  node [
    id 2329
    label "poszycie"
  ]
  node [
    id 2330
    label "gr&#243;d&#378;"
  ]
  node [
    id 2331
    label "wr&#281;ga"
  ]
  node [
    id 2332
    label "blokownia"
  ]
  node [
    id 2333
    label "plecy"
  ]
  node [
    id 2334
    label "stojak"
  ]
  node [
    id 2335
    label "falszkil"
  ]
  node [
    id 2336
    label "klatka_piersiowa"
  ]
  node [
    id 2337
    label "biodro"
  ]
  node [
    id 2338
    label "pacha"
  ]
  node [
    id 2339
    label "podwodzie"
  ]
  node [
    id 2340
    label "stewa"
  ]
  node [
    id 2341
    label "zgrzyta&#263;"
  ]
  node [
    id 2342
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 2343
    label "wheeze"
  ]
  node [
    id 2344
    label "silnik"
  ]
  node [
    id 2345
    label "wy&#263;"
  ]
  node [
    id 2346
    label "&#347;wista&#263;"
  ]
  node [
    id 2347
    label "os&#322;uchiwanie"
  ]
  node [
    id 2348
    label "oddycha&#263;"
  ]
  node [
    id 2349
    label "warcze&#263;"
  ]
  node [
    id 2350
    label "rattle"
  ]
  node [
    id 2351
    label "kaszlak"
  ]
  node [
    id 2352
    label "p&#322;uca"
  ]
  node [
    id 2353
    label "wydobywa&#263;"
  ]
  node [
    id 2354
    label "zdolno&#347;&#263;"
  ]
  node [
    id 2355
    label "oddychanie"
  ]
  node [
    id 2356
    label "wydobywanie"
  ]
  node [
    id 2357
    label "brzmienie"
  ]
  node [
    id 2358
    label "wydawanie"
  ]
  node [
    id 2359
    label "przepracowanie_si&#281;"
  ]
  node [
    id 2360
    label "zarz&#261;dzanie"
  ]
  node [
    id 2361
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 2362
    label "podlizanie_si&#281;"
  ]
  node [
    id 2363
    label "dopracowanie"
  ]
  node [
    id 2364
    label "podlizywanie_si&#281;"
  ]
  node [
    id 2365
    label "uruchamianie"
  ]
  node [
    id 2366
    label "dzia&#322;anie"
  ]
  node [
    id 2367
    label "d&#261;&#380;enie"
  ]
  node [
    id 2368
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 2369
    label "uruchomienie"
  ]
  node [
    id 2370
    label "nakr&#281;canie"
  ]
  node [
    id 2371
    label "funkcjonowanie"
  ]
  node [
    id 2372
    label "tr&#243;jstronny"
  ]
  node [
    id 2373
    label "postaranie_si&#281;"
  ]
  node [
    id 2374
    label "odpocz&#281;cie"
  ]
  node [
    id 2375
    label "nakr&#281;cenie"
  ]
  node [
    id 2376
    label "zatrzymanie"
  ]
  node [
    id 2377
    label "spracowanie_si&#281;"
  ]
  node [
    id 2378
    label "skakanie"
  ]
  node [
    id 2379
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 2380
    label "podtrzymywanie"
  ]
  node [
    id 2381
    label "w&#322;&#261;czanie"
  ]
  node [
    id 2382
    label "zaprz&#281;ganie"
  ]
  node [
    id 2383
    label "podejmowanie"
  ]
  node [
    id 2384
    label "wyrabianie"
  ]
  node [
    id 2385
    label "dzianie_si&#281;"
  ]
  node [
    id 2386
    label "use"
  ]
  node [
    id 2387
    label "przepracowanie"
  ]
  node [
    id 2388
    label "poruszanie_si&#281;"
  ]
  node [
    id 2389
    label "funkcja"
  ]
  node [
    id 2390
    label "impact"
  ]
  node [
    id 2391
    label "przepracowywanie"
  ]
  node [
    id 2392
    label "awansowanie"
  ]
  node [
    id 2393
    label "courtship"
  ]
  node [
    id 2394
    label "zapracowanie"
  ]
  node [
    id 2395
    label "wyrobienie"
  ]
  node [
    id 2396
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 2397
    label "w&#322;&#261;czenie"
  ]
  node [
    id 2398
    label "endeavor"
  ]
  node [
    id 2399
    label "podejmowa&#263;"
  ]
  node [
    id 2400
    label "do"
  ]
  node [
    id 2401
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 2402
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 2403
    label "funkcjonowa&#263;"
  ]
  node [
    id 2404
    label "zepsucie"
  ]
  node [
    id 2405
    label "automat"
  ]
  node [
    id 2406
    label "intelektualizacja"
  ]
  node [
    id 2407
    label "matematycznie"
  ]
  node [
    id 2408
    label "dok&#322;adny"
  ]
  node [
    id 2409
    label "sprecyzowanie"
  ]
  node [
    id 2410
    label "precyzyjny"
  ]
  node [
    id 2411
    label "miliamperomierz"
  ]
  node [
    id 2412
    label "precyzowanie"
  ]
  node [
    id 2413
    label "rzetelny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 543
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 17
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 32
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 210
  ]
  edge [
    source 14
    target 211
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 213
  ]
  edge [
    source 14
    target 214
  ]
  edge [
    source 14
    target 215
  ]
  edge [
    source 14
    target 216
  ]
  edge [
    source 14
    target 217
  ]
  edge [
    source 14
    target 218
  ]
  edge [
    source 14
    target 219
  ]
  edge [
    source 14
    target 220
  ]
  edge [
    source 14
    target 221
  ]
  edge [
    source 14
    target 222
  ]
  edge [
    source 14
    target 223
  ]
  edge [
    source 14
    target 224
  ]
  edge [
    source 14
    target 225
  ]
  edge [
    source 14
    target 226
  ]
  edge [
    source 14
    target 227
  ]
  edge [
    source 14
    target 228
  ]
  edge [
    source 14
    target 229
  ]
  edge [
    source 14
    target 230
  ]
  edge [
    source 14
    target 231
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 589
  ]
  edge [
    source 15
    target 294
  ]
  edge [
    source 15
    target 590
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 357
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 540
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 481
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 42
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 261
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 61
  ]
  edge [
    source 16
    target 62
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 691
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 17
    target 692
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 479
  ]
  edge [
    source 17
    target 480
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 17
    target 288
  ]
  edge [
    source 17
    target 699
  ]
  edge [
    source 17
    target 700
  ]
  edge [
    source 17
    target 701
  ]
  edge [
    source 17
    target 702
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 614
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 619
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 621
  ]
  edge [
    source 17
    target 622
  ]
  edge [
    source 17
    target 623
  ]
  edge [
    source 17
    target 624
  ]
  edge [
    source 17
    target 625
  ]
  edge [
    source 17
    target 357
  ]
  edge [
    source 17
    target 626
  ]
  edge [
    source 17
    target 627
  ]
  edge [
    source 17
    target 628
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 629
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 632
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 40
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 565
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 19
    target 566
  ]
  edge [
    source 19
    target 567
  ]
  edge [
    source 19
    target 568
  ]
  edge [
    source 19
    target 569
  ]
  edge [
    source 19
    target 570
  ]
  edge [
    source 19
    target 571
  ]
  edge [
    source 19
    target 703
  ]
  edge [
    source 19
    target 59
  ]
  edge [
    source 19
    target 704
  ]
  edge [
    source 19
    target 705
  ]
  edge [
    source 19
    target 675
  ]
  edge [
    source 19
    target 706
  ]
  edge [
    source 19
    target 707
  ]
  edge [
    source 19
    target 708
  ]
  edge [
    source 19
    target 263
  ]
  edge [
    source 19
    target 709
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 712
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 714
  ]
  edge [
    source 19
    target 715
  ]
  edge [
    source 19
    target 716
  ]
  edge [
    source 19
    target 717
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 719
  ]
  edge [
    source 19
    target 720
  ]
  edge [
    source 19
    target 721
  ]
  edge [
    source 19
    target 722
  ]
  edge [
    source 19
    target 723
  ]
  edge [
    source 19
    target 724
  ]
  edge [
    source 19
    target 725
  ]
  edge [
    source 19
    target 30
  ]
  edge [
    source 19
    target 44
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 726
  ]
  edge [
    source 20
    target 727
  ]
  edge [
    source 20
    target 728
  ]
  edge [
    source 20
    target 729
  ]
  edge [
    source 20
    target 730
  ]
  edge [
    source 20
    target 731
  ]
  edge [
    source 20
    target 732
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 734
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 736
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 739
  ]
  edge [
    source 20
    target 740
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 743
  ]
  edge [
    source 20
    target 404
  ]
  edge [
    source 20
    target 744
  ]
  edge [
    source 20
    target 745
  ]
  edge [
    source 20
    target 746
  ]
  edge [
    source 20
    target 747
  ]
  edge [
    source 20
    target 420
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 751
  ]
  edge [
    source 20
    target 752
  ]
  edge [
    source 20
    target 753
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 755
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 759
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 761
  ]
  edge [
    source 20
    target 762
  ]
  edge [
    source 20
    target 763
  ]
  edge [
    source 20
    target 764
  ]
  edge [
    source 20
    target 765
  ]
  edge [
    source 20
    target 766
  ]
  edge [
    source 20
    target 767
  ]
  edge [
    source 20
    target 768
  ]
  edge [
    source 20
    target 769
  ]
  edge [
    source 20
    target 770
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 20
    target 772
  ]
  edge [
    source 20
    target 773
  ]
  edge [
    source 20
    target 774
  ]
  edge [
    source 20
    target 775
  ]
  edge [
    source 20
    target 776
  ]
  edge [
    source 20
    target 777
  ]
  edge [
    source 20
    target 778
  ]
  edge [
    source 20
    target 779
  ]
  edge [
    source 20
    target 780
  ]
  edge [
    source 20
    target 588
  ]
  edge [
    source 20
    target 781
  ]
  edge [
    source 20
    target 782
  ]
  edge [
    source 20
    target 783
  ]
  edge [
    source 20
    target 44
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 784
  ]
  edge [
    source 21
    target 785
  ]
  edge [
    source 21
    target 786
  ]
  edge [
    source 21
    target 787
  ]
  edge [
    source 21
    target 59
  ]
  edge [
    source 21
    target 788
  ]
  edge [
    source 21
    target 789
  ]
  edge [
    source 21
    target 790
  ]
  edge [
    source 21
    target 791
  ]
  edge [
    source 21
    target 792
  ]
  edge [
    source 21
    target 793
  ]
  edge [
    source 21
    target 794
  ]
  edge [
    source 21
    target 795
  ]
  edge [
    source 21
    target 796
  ]
  edge [
    source 21
    target 797
  ]
  edge [
    source 21
    target 798
  ]
  edge [
    source 21
    target 799
  ]
  edge [
    source 21
    target 800
  ]
  edge [
    source 21
    target 801
  ]
  edge [
    source 21
    target 802
  ]
  edge [
    source 21
    target 803
  ]
  edge [
    source 21
    target 804
  ]
  edge [
    source 21
    target 805
  ]
  edge [
    source 21
    target 806
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 807
  ]
  edge [
    source 22
    target 808
  ]
  edge [
    source 22
    target 411
  ]
  edge [
    source 22
    target 809
  ]
  edge [
    source 22
    target 810
  ]
  edge [
    source 22
    target 770
  ]
  edge [
    source 22
    target 811
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 812
  ]
  edge [
    source 22
    target 584
  ]
  edge [
    source 22
    target 813
  ]
  edge [
    source 22
    target 814
  ]
  edge [
    source 22
    target 815
  ]
  edge [
    source 22
    target 583
  ]
  edge [
    source 22
    target 385
  ]
  edge [
    source 22
    target 585
  ]
  edge [
    source 22
    target 586
  ]
  edge [
    source 22
    target 587
  ]
  edge [
    source 22
    target 588
  ]
  edge [
    source 22
    target 582
  ]
  edge [
    source 22
    target 816
  ]
  edge [
    source 22
    target 817
  ]
  edge [
    source 22
    target 818
  ]
  edge [
    source 22
    target 819
  ]
  edge [
    source 22
    target 820
  ]
  edge [
    source 22
    target 821
  ]
  edge [
    source 22
    target 822
  ]
  edge [
    source 22
    target 823
  ]
  edge [
    source 22
    target 824
  ]
  edge [
    source 22
    target 825
  ]
  edge [
    source 22
    target 826
  ]
  edge [
    source 22
    target 827
  ]
  edge [
    source 22
    target 828
  ]
  edge [
    source 22
    target 829
  ]
  edge [
    source 22
    target 830
  ]
  edge [
    source 22
    target 831
  ]
  edge [
    source 22
    target 832
  ]
  edge [
    source 22
    target 833
  ]
  edge [
    source 22
    target 834
  ]
  edge [
    source 22
    target 835
  ]
  edge [
    source 22
    target 836
  ]
  edge [
    source 22
    target 837
  ]
  edge [
    source 22
    target 838
  ]
  edge [
    source 22
    target 839
  ]
  edge [
    source 22
    target 840
  ]
  edge [
    source 22
    target 841
  ]
  edge [
    source 22
    target 420
  ]
  edge [
    source 22
    target 134
  ]
  edge [
    source 22
    target 842
  ]
  edge [
    source 22
    target 843
  ]
  edge [
    source 22
    target 844
  ]
  edge [
    source 22
    target 146
  ]
  edge [
    source 22
    target 845
  ]
  edge [
    source 22
    target 846
  ]
  edge [
    source 22
    target 847
  ]
  edge [
    source 22
    target 848
  ]
  edge [
    source 22
    target 849
  ]
  edge [
    source 22
    target 771
  ]
  edge [
    source 22
    target 850
  ]
  edge [
    source 22
    target 851
  ]
  edge [
    source 22
    target 852
  ]
  edge [
    source 22
    target 853
  ]
  edge [
    source 22
    target 854
  ]
  edge [
    source 22
    target 402
  ]
  edge [
    source 22
    target 855
  ]
  edge [
    source 22
    target 856
  ]
  edge [
    source 22
    target 46
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 857
  ]
  edge [
    source 23
    target 675
  ]
  edge [
    source 23
    target 858
  ]
  edge [
    source 23
    target 859
  ]
  edge [
    source 23
    target 860
  ]
  edge [
    source 23
    target 861
  ]
  edge [
    source 23
    target 862
  ]
  edge [
    source 23
    target 863
  ]
  edge [
    source 23
    target 864
  ]
  edge [
    source 23
    target 865
  ]
  edge [
    source 23
    target 866
  ]
  edge [
    source 23
    target 867
  ]
  edge [
    source 23
    target 868
  ]
  edge [
    source 23
    target 470
  ]
  edge [
    source 23
    target 869
  ]
  edge [
    source 23
    target 475
  ]
  edge [
    source 23
    target 870
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 871
  ]
  edge [
    source 23
    target 791
  ]
  edge [
    source 23
    target 872
  ]
  edge [
    source 23
    target 873
  ]
  edge [
    source 23
    target 471
  ]
  edge [
    source 23
    target 874
  ]
  edge [
    source 23
    target 875
  ]
  edge [
    source 23
    target 261
  ]
  edge [
    source 23
    target 876
  ]
  edge [
    source 23
    target 877
  ]
  edge [
    source 23
    target 591
  ]
  edge [
    source 23
    target 164
  ]
  edge [
    source 23
    target 478
  ]
  edge [
    source 23
    target 263
  ]
  edge [
    source 23
    target 878
  ]
  edge [
    source 23
    target 879
  ]
  edge [
    source 23
    target 880
  ]
  edge [
    source 23
    target 881
  ]
  edge [
    source 23
    target 162
  ]
  edge [
    source 23
    target 477
  ]
  edge [
    source 23
    target 882
  ]
  edge [
    source 23
    target 883
  ]
  edge [
    source 23
    target 884
  ]
  edge [
    source 23
    target 580
  ]
  edge [
    source 23
    target 885
  ]
  edge [
    source 23
    target 886
  ]
  edge [
    source 23
    target 473
  ]
  edge [
    source 23
    target 633
  ]
  edge [
    source 23
    target 887
  ]
  edge [
    source 23
    target 888
  ]
  edge [
    source 23
    target 889
  ]
  edge [
    source 23
    target 890
  ]
  edge [
    source 23
    target 891
  ]
  edge [
    source 23
    target 892
  ]
  edge [
    source 23
    target 893
  ]
  edge [
    source 23
    target 894
  ]
  edge [
    source 23
    target 895
  ]
  edge [
    source 23
    target 896
  ]
  edge [
    source 23
    target 897
  ]
  edge [
    source 23
    target 898
  ]
  edge [
    source 23
    target 899
  ]
  edge [
    source 23
    target 900
  ]
  edge [
    source 23
    target 901
  ]
  edge [
    source 23
    target 902
  ]
  edge [
    source 23
    target 903
  ]
  edge [
    source 23
    target 904
  ]
  edge [
    source 23
    target 905
  ]
  edge [
    source 23
    target 906
  ]
  edge [
    source 23
    target 599
  ]
  edge [
    source 23
    target 907
  ]
  edge [
    source 23
    target 908
  ]
  edge [
    source 23
    target 909
  ]
  edge [
    source 23
    target 910
  ]
  edge [
    source 23
    target 911
  ]
  edge [
    source 23
    target 912
  ]
  edge [
    source 23
    target 913
  ]
  edge [
    source 23
    target 914
  ]
  edge [
    source 23
    target 915
  ]
  edge [
    source 23
    target 916
  ]
  edge [
    source 23
    target 540
  ]
  edge [
    source 23
    target 917
  ]
  edge [
    source 23
    target 918
  ]
  edge [
    source 23
    target 919
  ]
  edge [
    source 23
    target 920
  ]
  edge [
    source 23
    target 921
  ]
  edge [
    source 23
    target 922
  ]
  edge [
    source 23
    target 923
  ]
  edge [
    source 23
    target 924
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 41
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 925
  ]
  edge [
    source 26
    target 926
  ]
  edge [
    source 26
    target 927
  ]
  edge [
    source 26
    target 928
  ]
  edge [
    source 26
    target 929
  ]
  edge [
    source 26
    target 930
  ]
  edge [
    source 26
    target 294
  ]
  edge [
    source 26
    target 604
  ]
  edge [
    source 26
    target 931
  ]
  edge [
    source 26
    target 932
  ]
  edge [
    source 26
    target 933
  ]
  edge [
    source 26
    target 934
  ]
  edge [
    source 26
    target 935
  ]
  edge [
    source 26
    target 936
  ]
  edge [
    source 26
    target 937
  ]
  edge [
    source 26
    target 938
  ]
  edge [
    source 26
    target 939
  ]
  edge [
    source 26
    target 940
  ]
  edge [
    source 26
    target 941
  ]
  edge [
    source 26
    target 942
  ]
  edge [
    source 26
    target 943
  ]
  edge [
    source 26
    target 944
  ]
  edge [
    source 26
    target 945
  ]
  edge [
    source 26
    target 946
  ]
  edge [
    source 26
    target 947
  ]
  edge [
    source 26
    target 948
  ]
  edge [
    source 26
    target 949
  ]
  edge [
    source 26
    target 950
  ]
  edge [
    source 26
    target 951
  ]
  edge [
    source 26
    target 303
  ]
  edge [
    source 26
    target 952
  ]
  edge [
    source 26
    target 953
  ]
  edge [
    source 26
    target 954
  ]
  edge [
    source 26
    target 955
  ]
  edge [
    source 26
    target 956
  ]
  edge [
    source 26
    target 957
  ]
  edge [
    source 26
    target 307
  ]
  edge [
    source 26
    target 958
  ]
  edge [
    source 26
    target 959
  ]
  edge [
    source 26
    target 960
  ]
  edge [
    source 26
    target 961
  ]
  edge [
    source 26
    target 962
  ]
  edge [
    source 26
    target 963
  ]
  edge [
    source 26
    target 964
  ]
  edge [
    source 26
    target 965
  ]
  edge [
    source 26
    target 966
  ]
  edge [
    source 27
    target 402
  ]
  edge [
    source 27
    target 855
  ]
  edge [
    source 27
    target 381
  ]
  edge [
    source 27
    target 967
  ]
  edge [
    source 27
    target 968
  ]
  edge [
    source 27
    target 807
  ]
  edge [
    source 27
    target 808
  ]
  edge [
    source 27
    target 411
  ]
  edge [
    source 27
    target 809
  ]
  edge [
    source 27
    target 810
  ]
  edge [
    source 27
    target 770
  ]
  edge [
    source 27
    target 811
  ]
  edge [
    source 27
    target 812
  ]
  edge [
    source 27
    target 584
  ]
  edge [
    source 27
    target 813
  ]
  edge [
    source 27
    target 814
  ]
  edge [
    source 27
    target 815
  ]
  edge [
    source 27
    target 823
  ]
  edge [
    source 27
    target 969
  ]
  edge [
    source 27
    target 587
  ]
  edge [
    source 27
    target 588
  ]
  edge [
    source 27
    target 970
  ]
  edge [
    source 27
    target 971
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 972
  ]
  edge [
    source 29
    target 973
  ]
  edge [
    source 29
    target 59
  ]
  edge [
    source 29
    target 974
  ]
  edge [
    source 29
    target 975
  ]
  edge [
    source 29
    target 976
  ]
  edge [
    source 29
    target 977
  ]
  edge [
    source 29
    target 978
  ]
  edge [
    source 29
    target 345
  ]
  edge [
    source 29
    target 979
  ]
  edge [
    source 29
    target 980
  ]
  edge [
    source 29
    target 171
  ]
  edge [
    source 29
    target 981
  ]
  edge [
    source 29
    target 174
  ]
  edge [
    source 29
    target 982
  ]
  edge [
    source 29
    target 82
  ]
  edge [
    source 29
    target 983
  ]
  edge [
    source 29
    target 984
  ]
  edge [
    source 29
    target 985
  ]
  edge [
    source 29
    target 986
  ]
  edge [
    source 29
    target 987
  ]
  edge [
    source 29
    target 988
  ]
  edge [
    source 29
    target 989
  ]
  edge [
    source 29
    target 990
  ]
  edge [
    source 29
    target 991
  ]
  edge [
    source 29
    target 992
  ]
  edge [
    source 29
    target 993
  ]
  edge [
    source 29
    target 994
  ]
  edge [
    source 29
    target 995
  ]
  edge [
    source 29
    target 996
  ]
  edge [
    source 29
    target 997
  ]
  edge [
    source 29
    target 998
  ]
  edge [
    source 29
    target 999
  ]
  edge [
    source 29
    target 1000
  ]
  edge [
    source 29
    target 1001
  ]
  edge [
    source 29
    target 1002
  ]
  edge [
    source 29
    target 1003
  ]
  edge [
    source 29
    target 1004
  ]
  edge [
    source 29
    target 1005
  ]
  edge [
    source 29
    target 1006
  ]
  edge [
    source 29
    target 1007
  ]
  edge [
    source 29
    target 1008
  ]
  edge [
    source 29
    target 1009
  ]
  edge [
    source 29
    target 313
  ]
  edge [
    source 29
    target 1010
  ]
  edge [
    source 29
    target 1011
  ]
  edge [
    source 29
    target 1012
  ]
  edge [
    source 29
    target 1013
  ]
  edge [
    source 29
    target 1014
  ]
  edge [
    source 29
    target 1015
  ]
  edge [
    source 29
    target 1016
  ]
  edge [
    source 29
    target 1017
  ]
  edge [
    source 29
    target 1018
  ]
  edge [
    source 29
    target 1019
  ]
  edge [
    source 29
    target 1020
  ]
  edge [
    source 29
    target 1021
  ]
  edge [
    source 29
    target 718
  ]
  edge [
    source 29
    target 1022
  ]
  edge [
    source 29
    target 1023
  ]
  edge [
    source 29
    target 1024
  ]
  edge [
    source 29
    target 275
  ]
  edge [
    source 29
    target 1025
  ]
  edge [
    source 29
    target 1026
  ]
  edge [
    source 29
    target 1027
  ]
  edge [
    source 29
    target 1028
  ]
  edge [
    source 29
    target 1029
  ]
  edge [
    source 29
    target 1030
  ]
  edge [
    source 29
    target 1031
  ]
  edge [
    source 29
    target 1032
  ]
  edge [
    source 29
    target 1033
  ]
  edge [
    source 29
    target 1034
  ]
  edge [
    source 29
    target 1035
  ]
  edge [
    source 29
    target 1036
  ]
  edge [
    source 29
    target 1037
  ]
  edge [
    source 29
    target 1038
  ]
  edge [
    source 29
    target 1039
  ]
  edge [
    source 29
    target 467
  ]
  edge [
    source 29
    target 1040
  ]
  edge [
    source 29
    target 1041
  ]
  edge [
    source 29
    target 1042
  ]
  edge [
    source 30
    target 37
  ]
  edge [
    source 30
    target 38
  ]
  edge [
    source 30
    target 565
  ]
  edge [
    source 30
    target 1043
  ]
  edge [
    source 30
    target 59
  ]
  edge [
    source 30
    target 1044
  ]
  edge [
    source 30
    target 1045
  ]
  edge [
    source 30
    target 1046
  ]
  edge [
    source 30
    target 1047
  ]
  edge [
    source 30
    target 1048
  ]
  edge [
    source 30
    target 1049
  ]
  edge [
    source 30
    target 1050
  ]
  edge [
    source 30
    target 568
  ]
  edge [
    source 30
    target 1051
  ]
  edge [
    source 30
    target 1052
  ]
  edge [
    source 30
    target 1053
  ]
  edge [
    source 30
    target 1054
  ]
  edge [
    source 30
    target 1055
  ]
  edge [
    source 30
    target 1056
  ]
  edge [
    source 30
    target 1057
  ]
  edge [
    source 30
    target 571
  ]
  edge [
    source 30
    target 1058
  ]
  edge [
    source 30
    target 50
  ]
  edge [
    source 30
    target 566
  ]
  edge [
    source 30
    target 1059
  ]
  edge [
    source 30
    target 1060
  ]
  edge [
    source 30
    target 1061
  ]
  edge [
    source 30
    target 1062
  ]
  edge [
    source 30
    target 1063
  ]
  edge [
    source 30
    target 583
  ]
  edge [
    source 30
    target 567
  ]
  edge [
    source 30
    target 1064
  ]
  edge [
    source 30
    target 1065
  ]
  edge [
    source 30
    target 569
  ]
  edge [
    source 30
    target 1066
  ]
  edge [
    source 30
    target 1067
  ]
  edge [
    source 30
    target 570
  ]
  edge [
    source 30
    target 1068
  ]
  edge [
    source 30
    target 1069
  ]
  edge [
    source 30
    target 1070
  ]
  edge [
    source 30
    target 1071
  ]
  edge [
    source 30
    target 1072
  ]
  edge [
    source 30
    target 1073
  ]
  edge [
    source 30
    target 1074
  ]
  edge [
    source 30
    target 1075
  ]
  edge [
    source 30
    target 1076
  ]
  edge [
    source 30
    target 1077
  ]
  edge [
    source 30
    target 1078
  ]
  edge [
    source 30
    target 715
  ]
  edge [
    source 30
    target 1079
  ]
  edge [
    source 30
    target 1080
  ]
  edge [
    source 30
    target 1081
  ]
  edge [
    source 30
    target 97
  ]
  edge [
    source 30
    target 1082
  ]
  edge [
    source 30
    target 1083
  ]
  edge [
    source 30
    target 1084
  ]
  edge [
    source 30
    target 703
  ]
  edge [
    source 30
    target 980
  ]
  edge [
    source 30
    target 171
  ]
  edge [
    source 30
    target 981
  ]
  edge [
    source 30
    target 174
  ]
  edge [
    source 30
    target 982
  ]
  edge [
    source 30
    target 82
  ]
  edge [
    source 30
    target 983
  ]
  edge [
    source 30
    target 984
  ]
  edge [
    source 30
    target 985
  ]
  edge [
    source 30
    target 986
  ]
  edge [
    source 30
    target 987
  ]
  edge [
    source 30
    target 988
  ]
  edge [
    source 30
    target 989
  ]
  edge [
    source 30
    target 990
  ]
  edge [
    source 30
    target 991
  ]
  edge [
    source 30
    target 992
  ]
  edge [
    source 30
    target 993
  ]
  edge [
    source 30
    target 994
  ]
  edge [
    source 30
    target 995
  ]
  edge [
    source 30
    target 996
  ]
  edge [
    source 30
    target 997
  ]
  edge [
    source 30
    target 998
  ]
  edge [
    source 30
    target 999
  ]
  edge [
    source 30
    target 1000
  ]
  edge [
    source 30
    target 1001
  ]
  edge [
    source 30
    target 1085
  ]
  edge [
    source 30
    target 411
  ]
  edge [
    source 30
    target 728
  ]
  edge [
    source 30
    target 1086
  ]
  edge [
    source 30
    target 1087
  ]
  edge [
    source 30
    target 1088
  ]
  edge [
    source 30
    target 1089
  ]
  edge [
    source 30
    target 1090
  ]
  edge [
    source 30
    target 1091
  ]
  edge [
    source 30
    target 1092
  ]
  edge [
    source 30
    target 363
  ]
  edge [
    source 30
    target 1093
  ]
  edge [
    source 30
    target 1094
  ]
  edge [
    source 30
    target 1095
  ]
  edge [
    source 30
    target 1096
  ]
  edge [
    source 30
    target 1097
  ]
  edge [
    source 30
    target 1098
  ]
  edge [
    source 30
    target 1099
  ]
  edge [
    source 30
    target 1100
  ]
  edge [
    source 30
    target 629
  ]
  edge [
    source 30
    target 1101
  ]
  edge [
    source 30
    target 1102
  ]
  edge [
    source 30
    target 1103
  ]
  edge [
    source 30
    target 1104
  ]
  edge [
    source 30
    target 709
  ]
  edge [
    source 30
    target 710
  ]
  edge [
    source 30
    target 711
  ]
  edge [
    source 30
    target 712
  ]
  edge [
    source 30
    target 713
  ]
  edge [
    source 30
    target 714
  ]
  edge [
    source 30
    target 704
  ]
  edge [
    source 30
    target 705
  ]
  edge [
    source 30
    target 675
  ]
  edge [
    source 30
    target 706
  ]
  edge [
    source 30
    target 707
  ]
  edge [
    source 30
    target 708
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 716
  ]
  edge [
    source 30
    target 717
  ]
  edge [
    source 30
    target 718
  ]
  edge [
    source 30
    target 719
  ]
  edge [
    source 30
    target 720
  ]
  edge [
    source 30
    target 721
  ]
  edge [
    source 30
    target 907
  ]
  edge [
    source 30
    target 1105
  ]
  edge [
    source 30
    target 924
  ]
  edge [
    source 30
    target 51
  ]
  edge [
    source 30
    target 1106
  ]
  edge [
    source 30
    target 52
  ]
  edge [
    source 30
    target 1107
  ]
  edge [
    source 30
    target 1108
  ]
  edge [
    source 30
    target 55
  ]
  edge [
    source 30
    target 54
  ]
  edge [
    source 30
    target 1109
  ]
  edge [
    source 30
    target 53
  ]
  edge [
    source 30
    target 1110
  ]
  edge [
    source 30
    target 1111
  ]
  edge [
    source 30
    target 1112
  ]
  edge [
    source 30
    target 154
  ]
  edge [
    source 30
    target 1113
  ]
  edge [
    source 30
    target 1114
  ]
  edge [
    source 30
    target 1115
  ]
  edge [
    source 30
    target 1116
  ]
  edge [
    source 30
    target 1117
  ]
  edge [
    source 30
    target 1118
  ]
  edge [
    source 30
    target 1119
  ]
  edge [
    source 30
    target 1120
  ]
  edge [
    source 30
    target 1121
  ]
  edge [
    source 30
    target 1122
  ]
  edge [
    source 30
    target 1123
  ]
  edge [
    source 30
    target 1124
  ]
  edge [
    source 30
    target 1125
  ]
  edge [
    source 30
    target 1126
  ]
  edge [
    source 30
    target 1127
  ]
  edge [
    source 30
    target 1128
  ]
  edge [
    source 30
    target 1129
  ]
  edge [
    source 30
    target 1130
  ]
  edge [
    source 30
    target 1131
  ]
  edge [
    source 30
    target 1132
  ]
  edge [
    source 30
    target 1133
  ]
  edge [
    source 30
    target 1134
  ]
  edge [
    source 30
    target 722
  ]
  edge [
    source 30
    target 723
  ]
  edge [
    source 30
    target 724
  ]
  edge [
    source 30
    target 725
  ]
  edge [
    source 30
    target 1135
  ]
  edge [
    source 30
    target 1136
  ]
  edge [
    source 30
    target 1137
  ]
  edge [
    source 30
    target 1138
  ]
  edge [
    source 30
    target 1139
  ]
  edge [
    source 30
    target 1140
  ]
  edge [
    source 30
    target 1141
  ]
  edge [
    source 30
    target 1142
  ]
  edge [
    source 30
    target 1143
  ]
  edge [
    source 30
    target 1144
  ]
  edge [
    source 30
    target 1145
  ]
  edge [
    source 30
    target 1146
  ]
  edge [
    source 30
    target 1147
  ]
  edge [
    source 30
    target 1148
  ]
  edge [
    source 30
    target 1149
  ]
  edge [
    source 30
    target 1150
  ]
  edge [
    source 30
    target 1151
  ]
  edge [
    source 30
    target 1152
  ]
  edge [
    source 30
    target 1153
  ]
  edge [
    source 30
    target 1154
  ]
  edge [
    source 30
    target 1155
  ]
  edge [
    source 30
    target 1156
  ]
  edge [
    source 30
    target 1157
  ]
  edge [
    source 30
    target 1158
  ]
  edge [
    source 30
    target 1159
  ]
  edge [
    source 30
    target 1160
  ]
  edge [
    source 30
    target 1161
  ]
  edge [
    source 30
    target 1162
  ]
  edge [
    source 30
    target 1163
  ]
  edge [
    source 30
    target 1164
  ]
  edge [
    source 30
    target 1165
  ]
  edge [
    source 30
    target 1166
  ]
  edge [
    source 30
    target 1167
  ]
  edge [
    source 30
    target 1168
  ]
  edge [
    source 30
    target 1169
  ]
  edge [
    source 30
    target 1170
  ]
  edge [
    source 30
    target 1171
  ]
  edge [
    source 30
    target 1172
  ]
  edge [
    source 30
    target 1173
  ]
  edge [
    source 30
    target 1174
  ]
  edge [
    source 30
    target 1175
  ]
  edge [
    source 30
    target 1176
  ]
  edge [
    source 30
    target 1177
  ]
  edge [
    source 30
    target 1178
  ]
  edge [
    source 30
    target 1179
  ]
  edge [
    source 30
    target 1180
  ]
  edge [
    source 30
    target 1181
  ]
  edge [
    source 30
    target 1182
  ]
  edge [
    source 30
    target 1183
  ]
  edge [
    source 30
    target 1184
  ]
  edge [
    source 30
    target 1185
  ]
  edge [
    source 30
    target 1186
  ]
  edge [
    source 30
    target 1187
  ]
  edge [
    source 30
    target 1188
  ]
  edge [
    source 30
    target 1189
  ]
  edge [
    source 30
    target 1190
  ]
  edge [
    source 30
    target 1191
  ]
  edge [
    source 30
    target 1192
  ]
  edge [
    source 30
    target 1193
  ]
  edge [
    source 30
    target 1194
  ]
  edge [
    source 30
    target 1195
  ]
  edge [
    source 30
    target 1196
  ]
  edge [
    source 30
    target 1197
  ]
  edge [
    source 30
    target 1198
  ]
  edge [
    source 30
    target 1199
  ]
  edge [
    source 30
    target 1200
  ]
  edge [
    source 30
    target 1201
  ]
  edge [
    source 30
    target 1202
  ]
  edge [
    source 30
    target 1203
  ]
  edge [
    source 30
    target 1204
  ]
  edge [
    source 30
    target 1205
  ]
  edge [
    source 30
    target 1206
  ]
  edge [
    source 30
    target 1207
  ]
  edge [
    source 30
    target 1208
  ]
  edge [
    source 30
    target 1209
  ]
  edge [
    source 30
    target 1210
  ]
  edge [
    source 30
    target 1211
  ]
  edge [
    source 30
    target 1212
  ]
  edge [
    source 30
    target 1213
  ]
  edge [
    source 30
    target 1214
  ]
  edge [
    source 30
    target 1215
  ]
  edge [
    source 30
    target 1216
  ]
  edge [
    source 30
    target 328
  ]
  edge [
    source 30
    target 1217
  ]
  edge [
    source 30
    target 1218
  ]
  edge [
    source 30
    target 1219
  ]
  edge [
    source 30
    target 1220
  ]
  edge [
    source 30
    target 1221
  ]
  edge [
    source 30
    target 1222
  ]
  edge [
    source 30
    target 1223
  ]
  edge [
    source 30
    target 1224
  ]
  edge [
    source 30
    target 1225
  ]
  edge [
    source 30
    target 1226
  ]
  edge [
    source 30
    target 1227
  ]
  edge [
    source 30
    target 1228
  ]
  edge [
    source 30
    target 1229
  ]
  edge [
    source 30
    target 1230
  ]
  edge [
    source 30
    target 1231
  ]
  edge [
    source 30
    target 1232
  ]
  edge [
    source 30
    target 1233
  ]
  edge [
    source 30
    target 1234
  ]
  edge [
    source 30
    target 1235
  ]
  edge [
    source 30
    target 1236
  ]
  edge [
    source 30
    target 1237
  ]
  edge [
    source 30
    target 1238
  ]
  edge [
    source 30
    target 1239
  ]
  edge [
    source 30
    target 1240
  ]
  edge [
    source 30
    target 1241
  ]
  edge [
    source 30
    target 1242
  ]
  edge [
    source 30
    target 1243
  ]
  edge [
    source 30
    target 1244
  ]
  edge [
    source 30
    target 1245
  ]
  edge [
    source 30
    target 1246
  ]
  edge [
    source 30
    target 1247
  ]
  edge [
    source 30
    target 1248
  ]
  edge [
    source 30
    target 1249
  ]
  edge [
    source 30
    target 1250
  ]
  edge [
    source 30
    target 1251
  ]
  edge [
    source 30
    target 1252
  ]
  edge [
    source 30
    target 1253
  ]
  edge [
    source 30
    target 1254
  ]
  edge [
    source 30
    target 1255
  ]
  edge [
    source 30
    target 1256
  ]
  edge [
    source 30
    target 1257
  ]
  edge [
    source 30
    target 1258
  ]
  edge [
    source 30
    target 1259
  ]
  edge [
    source 30
    target 1260
  ]
  edge [
    source 30
    target 1261
  ]
  edge [
    source 30
    target 1262
  ]
  edge [
    source 30
    target 1263
  ]
  edge [
    source 30
    target 1264
  ]
  edge [
    source 30
    target 1265
  ]
  edge [
    source 30
    target 1266
  ]
  edge [
    source 30
    target 1267
  ]
  edge [
    source 30
    target 1268
  ]
  edge [
    source 30
    target 1269
  ]
  edge [
    source 30
    target 1270
  ]
  edge [
    source 30
    target 1271
  ]
  edge [
    source 30
    target 1272
  ]
  edge [
    source 30
    target 1273
  ]
  edge [
    source 30
    target 1274
  ]
  edge [
    source 30
    target 1275
  ]
  edge [
    source 30
    target 1276
  ]
  edge [
    source 30
    target 1277
  ]
  edge [
    source 30
    target 1278
  ]
  edge [
    source 30
    target 1279
  ]
  edge [
    source 30
    target 1280
  ]
  edge [
    source 30
    target 1281
  ]
  edge [
    source 30
    target 1282
  ]
  edge [
    source 30
    target 1283
  ]
  edge [
    source 30
    target 1284
  ]
  edge [
    source 30
    target 1285
  ]
  edge [
    source 30
    target 1286
  ]
  edge [
    source 30
    target 1287
  ]
  edge [
    source 30
    target 1288
  ]
  edge [
    source 30
    target 1289
  ]
  edge [
    source 30
    target 1290
  ]
  edge [
    source 30
    target 1291
  ]
  edge [
    source 30
    target 1292
  ]
  edge [
    source 30
    target 1293
  ]
  edge [
    source 30
    target 1294
  ]
  edge [
    source 30
    target 1295
  ]
  edge [
    source 30
    target 1296
  ]
  edge [
    source 30
    target 1297
  ]
  edge [
    source 30
    target 1298
  ]
  edge [
    source 30
    target 1299
  ]
  edge [
    source 30
    target 1300
  ]
  edge [
    source 30
    target 1301
  ]
  edge [
    source 30
    target 1302
  ]
  edge [
    source 30
    target 1303
  ]
  edge [
    source 30
    target 1304
  ]
  edge [
    source 30
    target 1305
  ]
  edge [
    source 30
    target 1306
  ]
  edge [
    source 30
    target 1307
  ]
  edge [
    source 30
    target 1308
  ]
  edge [
    source 30
    target 1309
  ]
  edge [
    source 30
    target 1310
  ]
  edge [
    source 30
    target 640
  ]
  edge [
    source 30
    target 1311
  ]
  edge [
    source 30
    target 1312
  ]
  edge [
    source 30
    target 1313
  ]
  edge [
    source 30
    target 1314
  ]
  edge [
    source 30
    target 1315
  ]
  edge [
    source 30
    target 1316
  ]
  edge [
    source 30
    target 1317
  ]
  edge [
    source 30
    target 1318
  ]
  edge [
    source 30
    target 1319
  ]
  edge [
    source 30
    target 1320
  ]
  edge [
    source 30
    target 1321
  ]
  edge [
    source 30
    target 1322
  ]
  edge [
    source 30
    target 1323
  ]
  edge [
    source 30
    target 1324
  ]
  edge [
    source 30
    target 1325
  ]
  edge [
    source 30
    target 1326
  ]
  edge [
    source 30
    target 1327
  ]
  edge [
    source 30
    target 1328
  ]
  edge [
    source 30
    target 1329
  ]
  edge [
    source 30
    target 1330
  ]
  edge [
    source 30
    target 164
  ]
  edge [
    source 30
    target 1331
  ]
  edge [
    source 30
    target 1332
  ]
  edge [
    source 30
    target 1333
  ]
  edge [
    source 30
    target 1334
  ]
  edge [
    source 30
    target 1335
  ]
  edge [
    source 30
    target 1336
  ]
  edge [
    source 30
    target 1337
  ]
  edge [
    source 30
    target 1338
  ]
  edge [
    source 30
    target 1339
  ]
  edge [
    source 30
    target 1340
  ]
  edge [
    source 30
    target 1341
  ]
  edge [
    source 30
    target 1342
  ]
  edge [
    source 30
    target 1343
  ]
  edge [
    source 30
    target 1344
  ]
  edge [
    source 30
    target 1345
  ]
  edge [
    source 30
    target 1346
  ]
  edge [
    source 30
    target 1347
  ]
  edge [
    source 30
    target 1348
  ]
  edge [
    source 30
    target 1349
  ]
  edge [
    source 30
    target 1350
  ]
  edge [
    source 30
    target 1351
  ]
  edge [
    source 30
    target 1352
  ]
  edge [
    source 30
    target 1353
  ]
  edge [
    source 30
    target 1354
  ]
  edge [
    source 30
    target 1355
  ]
  edge [
    source 30
    target 1356
  ]
  edge [
    source 30
    target 1357
  ]
  edge [
    source 30
    target 1358
  ]
  edge [
    source 30
    target 1359
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1360
  ]
  edge [
    source 32
    target 1361
  ]
  edge [
    source 32
    target 1362
  ]
  edge [
    source 32
    target 1363
  ]
  edge [
    source 32
    target 1364
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 33
    target 1365
  ]
  edge [
    source 33
    target 1366
  ]
  edge [
    source 33
    target 926
  ]
  edge [
    source 33
    target 1367
  ]
  edge [
    source 33
    target 1368
  ]
  edge [
    source 33
    target 1369
  ]
  edge [
    source 33
    target 1370
  ]
  edge [
    source 33
    target 1371
  ]
  edge [
    source 33
    target 459
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 97
  ]
  edge [
    source 35
    target 1372
  ]
  edge [
    source 35
    target 59
  ]
  edge [
    source 35
    target 896
  ]
  edge [
    source 35
    target 1373
  ]
  edge [
    source 35
    target 1165
  ]
  edge [
    source 35
    target 1374
  ]
  edge [
    source 35
    target 1375
  ]
  edge [
    source 35
    target 1376
  ]
  edge [
    source 35
    target 1377
  ]
  edge [
    source 35
    target 1378
  ]
  edge [
    source 35
    target 1379
  ]
  edge [
    source 35
    target 164
  ]
  edge [
    source 35
    target 1380
  ]
  edge [
    source 35
    target 1381
  ]
  edge [
    source 35
    target 1382
  ]
  edge [
    source 35
    target 1383
  ]
  edge [
    source 35
    target 1384
  ]
  edge [
    source 35
    target 1385
  ]
  edge [
    source 35
    target 1386
  ]
  edge [
    source 35
    target 1387
  ]
  edge [
    source 35
    target 43
  ]
  edge [
    source 35
    target 1388
  ]
  edge [
    source 35
    target 1389
  ]
  edge [
    source 35
    target 1390
  ]
  edge [
    source 35
    target 1391
  ]
  edge [
    source 35
    target 1392
  ]
  edge [
    source 35
    target 1393
  ]
  edge [
    source 35
    target 1394
  ]
  edge [
    source 35
    target 1395
  ]
  edge [
    source 35
    target 1396
  ]
  edge [
    source 35
    target 1397
  ]
  edge [
    source 35
    target 1398
  ]
  edge [
    source 35
    target 105
  ]
  edge [
    source 35
    target 1399
  ]
  edge [
    source 35
    target 1400
  ]
  edge [
    source 35
    target 1401
  ]
  edge [
    source 35
    target 1402
  ]
  edge [
    source 35
    target 1403
  ]
  edge [
    source 35
    target 1404
  ]
  edge [
    source 35
    target 980
  ]
  edge [
    source 35
    target 171
  ]
  edge [
    source 35
    target 981
  ]
  edge [
    source 35
    target 174
  ]
  edge [
    source 35
    target 982
  ]
  edge [
    source 35
    target 82
  ]
  edge [
    source 35
    target 983
  ]
  edge [
    source 35
    target 984
  ]
  edge [
    source 35
    target 985
  ]
  edge [
    source 35
    target 986
  ]
  edge [
    source 35
    target 987
  ]
  edge [
    source 35
    target 988
  ]
  edge [
    source 35
    target 989
  ]
  edge [
    source 35
    target 990
  ]
  edge [
    source 35
    target 991
  ]
  edge [
    source 35
    target 992
  ]
  edge [
    source 35
    target 993
  ]
  edge [
    source 35
    target 994
  ]
  edge [
    source 35
    target 995
  ]
  edge [
    source 35
    target 996
  ]
  edge [
    source 35
    target 997
  ]
  edge [
    source 35
    target 998
  ]
  edge [
    source 35
    target 999
  ]
  edge [
    source 35
    target 1000
  ]
  edge [
    source 35
    target 1001
  ]
  edge [
    source 35
    target 1405
  ]
  edge [
    source 35
    target 1406
  ]
  edge [
    source 35
    target 1407
  ]
  edge [
    source 35
    target 1408
  ]
  edge [
    source 35
    target 1409
  ]
  edge [
    source 35
    target 1410
  ]
  edge [
    source 35
    target 1411
  ]
  edge [
    source 35
    target 1412
  ]
  edge [
    source 35
    target 1413
  ]
  edge [
    source 35
    target 1414
  ]
  edge [
    source 35
    target 1415
  ]
  edge [
    source 35
    target 1416
  ]
  edge [
    source 35
    target 1417
  ]
  edge [
    source 35
    target 1418
  ]
  edge [
    source 35
    target 1419
  ]
  edge [
    source 35
    target 1420
  ]
  edge [
    source 35
    target 1421
  ]
  edge [
    source 35
    target 1422
  ]
  edge [
    source 35
    target 1423
  ]
  edge [
    source 35
    target 1424
  ]
  edge [
    source 35
    target 1425
  ]
  edge [
    source 35
    target 1426
  ]
  edge [
    source 35
    target 1427
  ]
  edge [
    source 35
    target 1428
  ]
  edge [
    source 35
    target 1429
  ]
  edge [
    source 35
    target 790
  ]
  edge [
    source 35
    target 1430
  ]
  edge [
    source 35
    target 1431
  ]
  edge [
    source 35
    target 1432
  ]
  edge [
    source 35
    target 108
  ]
  edge [
    source 35
    target 1433
  ]
  edge [
    source 35
    target 357
  ]
  edge [
    source 35
    target 1434
  ]
  edge [
    source 35
    target 1435
  ]
  edge [
    source 35
    target 1436
  ]
  edge [
    source 35
    target 1437
  ]
  edge [
    source 35
    target 1438
  ]
  edge [
    source 35
    target 1439
  ]
  edge [
    source 35
    target 1440
  ]
  edge [
    source 35
    target 1441
  ]
  edge [
    source 35
    target 1442
  ]
  edge [
    source 35
    target 1443
  ]
  edge [
    source 35
    target 1444
  ]
  edge [
    source 35
    target 90
  ]
  edge [
    source 35
    target 1445
  ]
  edge [
    source 35
    target 1446
  ]
  edge [
    source 35
    target 1447
  ]
  edge [
    source 35
    target 1448
  ]
  edge [
    source 35
    target 1449
  ]
  edge [
    source 35
    target 1450
  ]
  edge [
    source 35
    target 1451
  ]
  edge [
    source 35
    target 1452
  ]
  edge [
    source 35
    target 699
  ]
  edge [
    source 35
    target 1453
  ]
  edge [
    source 35
    target 1454
  ]
  edge [
    source 35
    target 1455
  ]
  edge [
    source 35
    target 102
  ]
  edge [
    source 35
    target 1456
  ]
  edge [
    source 35
    target 168
  ]
  edge [
    source 35
    target 169
  ]
  edge [
    source 35
    target 170
  ]
  edge [
    source 35
    target 172
  ]
  edge [
    source 35
    target 173
  ]
  edge [
    source 35
    target 175
  ]
  edge [
    source 35
    target 176
  ]
  edge [
    source 35
    target 177
  ]
  edge [
    source 35
    target 178
  ]
  edge [
    source 35
    target 179
  ]
  edge [
    source 35
    target 180
  ]
  edge [
    source 35
    target 181
  ]
  edge [
    source 35
    target 182
  ]
  edge [
    source 35
    target 183
  ]
  edge [
    source 35
    target 184
  ]
  edge [
    source 35
    target 185
  ]
  edge [
    source 35
    target 186
  ]
  edge [
    source 35
    target 187
  ]
  edge [
    source 35
    target 188
  ]
  edge [
    source 35
    target 189
  ]
  edge [
    source 35
    target 190
  ]
  edge [
    source 35
    target 191
  ]
  edge [
    source 35
    target 192
  ]
  edge [
    source 35
    target 193
  ]
  edge [
    source 35
    target 194
  ]
  edge [
    source 35
    target 1457
  ]
  edge [
    source 35
    target 1458
  ]
  edge [
    source 35
    target 1459
  ]
  edge [
    source 35
    target 1460
  ]
  edge [
    source 35
    target 1461
  ]
  edge [
    source 35
    target 1462
  ]
  edge [
    source 35
    target 1463
  ]
  edge [
    source 35
    target 1464
  ]
  edge [
    source 35
    target 1465
  ]
  edge [
    source 35
    target 1466
  ]
  edge [
    source 35
    target 1467
  ]
  edge [
    source 35
    target 1468
  ]
  edge [
    source 35
    target 1469
  ]
  edge [
    source 35
    target 1470
  ]
  edge [
    source 35
    target 1471
  ]
  edge [
    source 35
    target 1472
  ]
  edge [
    source 35
    target 1473
  ]
  edge [
    source 35
    target 1474
  ]
  edge [
    source 35
    target 1475
  ]
  edge [
    source 35
    target 1476
  ]
  edge [
    source 35
    target 1477
  ]
  edge [
    source 35
    target 1478
  ]
  edge [
    source 35
    target 1479
  ]
  edge [
    source 35
    target 1480
  ]
  edge [
    source 35
    target 1481
  ]
  edge [
    source 35
    target 1482
  ]
  edge [
    source 35
    target 1483
  ]
  edge [
    source 35
    target 1484
  ]
  edge [
    source 35
    target 1485
  ]
  edge [
    source 35
    target 1486
  ]
  edge [
    source 35
    target 1487
  ]
  edge [
    source 35
    target 1488
  ]
  edge [
    source 35
    target 1489
  ]
  edge [
    source 35
    target 1490
  ]
  edge [
    source 35
    target 1491
  ]
  edge [
    source 35
    target 1492
  ]
  edge [
    source 35
    target 1493
  ]
  edge [
    source 35
    target 1494
  ]
  edge [
    source 35
    target 1495
  ]
  edge [
    source 35
    target 1496
  ]
  edge [
    source 35
    target 1497
  ]
  edge [
    source 35
    target 1498
  ]
  edge [
    source 35
    target 1499
  ]
  edge [
    source 35
    target 1500
  ]
  edge [
    source 35
    target 1501
  ]
  edge [
    source 35
    target 533
  ]
  edge [
    source 35
    target 1502
  ]
  edge [
    source 35
    target 1503
  ]
  edge [
    source 35
    target 1504
  ]
  edge [
    source 35
    target 1505
  ]
  edge [
    source 35
    target 1506
  ]
  edge [
    source 35
    target 1507
  ]
  edge [
    source 35
    target 1508
  ]
  edge [
    source 35
    target 1509
  ]
  edge [
    source 35
    target 1510
  ]
  edge [
    source 35
    target 1511
  ]
  edge [
    source 35
    target 1512
  ]
  edge [
    source 35
    target 1513
  ]
  edge [
    source 35
    target 1514
  ]
  edge [
    source 35
    target 1515
  ]
  edge [
    source 35
    target 1516
  ]
  edge [
    source 35
    target 1517
  ]
  edge [
    source 35
    target 1518
  ]
  edge [
    source 35
    target 1519
  ]
  edge [
    source 35
    target 1520
  ]
  edge [
    source 35
    target 1521
  ]
  edge [
    source 35
    target 1522
  ]
  edge [
    source 35
    target 1371
  ]
  edge [
    source 35
    target 1523
  ]
  edge [
    source 35
    target 1524
  ]
  edge [
    source 35
    target 1525
  ]
  edge [
    source 35
    target 1526
  ]
  edge [
    source 35
    target 1527
  ]
  edge [
    source 35
    target 1528
  ]
  edge [
    source 35
    target 1529
  ]
  edge [
    source 35
    target 1530
  ]
  edge [
    source 35
    target 1531
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 43
  ]
  edge [
    source 36
    target 1532
  ]
  edge [
    source 36
    target 896
  ]
  edge [
    source 36
    target 1533
  ]
  edge [
    source 36
    target 168
  ]
  edge [
    source 36
    target 189
  ]
  edge [
    source 36
    target 108
  ]
  edge [
    source 36
    target 1534
  ]
  edge [
    source 36
    target 1535
  ]
  edge [
    source 36
    target 1536
  ]
  edge [
    source 36
    target 179
  ]
  edge [
    source 36
    target 192
  ]
  edge [
    source 36
    target 1537
  ]
  edge [
    source 36
    target 1538
  ]
  edge [
    source 36
    target 1539
  ]
  edge [
    source 36
    target 1540
  ]
  edge [
    source 36
    target 1541
  ]
  edge [
    source 36
    target 164
  ]
  edge [
    source 36
    target 1542
  ]
  edge [
    source 36
    target 1543
  ]
  edge [
    source 36
    target 190
  ]
  edge [
    source 36
    target 1382
  ]
  edge [
    source 36
    target 1383
  ]
  edge [
    source 36
    target 1397
  ]
  edge [
    source 36
    target 1384
  ]
  edge [
    source 36
    target 1385
  ]
  edge [
    source 36
    target 1386
  ]
  edge [
    source 36
    target 1387
  ]
  edge [
    source 36
    target 1388
  ]
  edge [
    source 36
    target 1389
  ]
  edge [
    source 36
    target 1390
  ]
  edge [
    source 36
    target 1391
  ]
  edge [
    source 36
    target 1394
  ]
  edge [
    source 36
    target 1393
  ]
  edge [
    source 36
    target 1395
  ]
  edge [
    source 36
    target 1396
  ]
  edge [
    source 36
    target 1392
  ]
  edge [
    source 36
    target 1398
  ]
  edge [
    source 36
    target 105
  ]
  edge [
    source 36
    target 1399
  ]
  edge [
    source 37
    target 1544
  ]
  edge [
    source 37
    target 1545
  ]
  edge [
    source 37
    target 1546
  ]
  edge [
    source 37
    target 1547
  ]
  edge [
    source 37
    target 1548
  ]
  edge [
    source 37
    target 625
  ]
  edge [
    source 37
    target 912
  ]
  edge [
    source 37
    target 1549
  ]
  edge [
    source 37
    target 1550
  ]
  edge [
    source 37
    target 540
  ]
  edge [
    source 37
    target 1551
  ]
  edge [
    source 37
    target 1552
  ]
  edge [
    source 37
    target 1540
  ]
  edge [
    source 37
    target 1553
  ]
  edge [
    source 37
    target 1554
  ]
  edge [
    source 37
    target 1555
  ]
  edge [
    source 37
    target 1556
  ]
  edge [
    source 37
    target 1557
  ]
  edge [
    source 37
    target 1558
  ]
  edge [
    source 37
    target 1559
  ]
  edge [
    source 37
    target 1560
  ]
  edge [
    source 37
    target 1561
  ]
  edge [
    source 37
    target 1562
  ]
  edge [
    source 37
    target 1563
  ]
  edge [
    source 37
    target 879
  ]
  edge [
    source 37
    target 1564
  ]
  edge [
    source 37
    target 1565
  ]
  edge [
    source 37
    target 82
  ]
  edge [
    source 37
    target 1566
  ]
  edge [
    source 37
    target 1567
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 1568
  ]
  edge [
    source 37
    target 1569
  ]
  edge [
    source 37
    target 1570
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1571
  ]
  edge [
    source 39
    target 1572
  ]
  edge [
    source 39
    target 1573
  ]
  edge [
    source 39
    target 1574
  ]
  edge [
    source 39
    target 837
  ]
  edge [
    source 39
    target 1575
  ]
  edge [
    source 39
    target 1576
  ]
  edge [
    source 39
    target 1577
  ]
  edge [
    source 39
    target 841
  ]
  edge [
    source 39
    target 1578
  ]
  edge [
    source 39
    target 1555
  ]
  edge [
    source 39
    target 1579
  ]
  edge [
    source 39
    target 66
  ]
  edge [
    source 39
    target 1580
  ]
  edge [
    source 39
    target 452
  ]
  edge [
    source 39
    target 1581
  ]
  edge [
    source 39
    target 1582
  ]
  edge [
    source 39
    target 1583
  ]
  edge [
    source 39
    target 1584
  ]
  edge [
    source 39
    target 1585
  ]
  edge [
    source 39
    target 1586
  ]
  edge [
    source 39
    target 1587
  ]
  edge [
    source 39
    target 1588
  ]
  edge [
    source 39
    target 1589
  ]
  edge [
    source 39
    target 313
  ]
  edge [
    source 39
    target 209
  ]
  edge [
    source 39
    target 164
  ]
  edge [
    source 39
    target 1590
  ]
  edge [
    source 39
    target 1101
  ]
  edge [
    source 39
    target 223
  ]
  edge [
    source 39
    target 636
  ]
  edge [
    source 39
    target 1591
  ]
  edge [
    source 39
    target 1592
  ]
  edge [
    source 39
    target 1593
  ]
  edge [
    source 39
    target 1594
  ]
  edge [
    source 39
    target 1595
  ]
  edge [
    source 39
    target 1596
  ]
  edge [
    source 39
    target 1597
  ]
  edge [
    source 39
    target 1598
  ]
  edge [
    source 39
    target 1599
  ]
  edge [
    source 39
    target 1600
  ]
  edge [
    source 39
    target 357
  ]
  edge [
    source 39
    target 1601
  ]
  edge [
    source 39
    target 1602
  ]
  edge [
    source 39
    target 368
  ]
  edge [
    source 39
    target 1603
  ]
  edge [
    source 39
    target 1604
  ]
  edge [
    source 39
    target 1605
  ]
  edge [
    source 39
    target 661
  ]
  edge [
    source 39
    target 1606
  ]
  edge [
    source 39
    target 106
  ]
  edge [
    source 39
    target 1607
  ]
  edge [
    source 39
    target 1608
  ]
  edge [
    source 39
    target 1609
  ]
  edge [
    source 39
    target 1610
  ]
  edge [
    source 39
    target 655
  ]
  edge [
    source 39
    target 1611
  ]
  edge [
    source 39
    target 1612
  ]
  edge [
    source 39
    target 74
  ]
  edge [
    source 39
    target 75
  ]
  edge [
    source 39
    target 76
  ]
  edge [
    source 39
    target 77
  ]
  edge [
    source 39
    target 78
  ]
  edge [
    source 39
    target 79
  ]
  edge [
    source 39
    target 80
  ]
  edge [
    source 39
    target 81
  ]
  edge [
    source 39
    target 82
  ]
  edge [
    source 39
    target 83
  ]
  edge [
    source 39
    target 84
  ]
  edge [
    source 39
    target 85
  ]
  edge [
    source 39
    target 86
  ]
  edge [
    source 39
    target 87
  ]
  edge [
    source 39
    target 88
  ]
  edge [
    source 39
    target 89
  ]
  edge [
    source 39
    target 90
  ]
  edge [
    source 39
    target 91
  ]
  edge [
    source 39
    target 92
  ]
  edge [
    source 39
    target 93
  ]
  edge [
    source 39
    target 94
  ]
  edge [
    source 39
    target 95
  ]
  edge [
    source 39
    target 96
  ]
  edge [
    source 39
    target 97
  ]
  edge [
    source 39
    target 98
  ]
  edge [
    source 39
    target 99
  ]
  edge [
    source 39
    target 100
  ]
  edge [
    source 39
    target 101
  ]
  edge [
    source 39
    target 102
  ]
  edge [
    source 39
    target 103
  ]
  edge [
    source 39
    target 104
  ]
  edge [
    source 39
    target 105
  ]
  edge [
    source 39
    target 107
  ]
  edge [
    source 39
    target 1613
  ]
  edge [
    source 39
    target 1614
  ]
  edge [
    source 39
    target 1615
  ]
  edge [
    source 39
    target 1616
  ]
  edge [
    source 39
    target 1617
  ]
  edge [
    source 39
    target 1618
  ]
  edge [
    source 39
    target 1619
  ]
  edge [
    source 39
    target 1620
  ]
  edge [
    source 39
    target 1621
  ]
  edge [
    source 39
    target 1622
  ]
  edge [
    source 39
    target 1623
  ]
  edge [
    source 39
    target 1624
  ]
  edge [
    source 39
    target 1625
  ]
  edge [
    source 39
    target 1626
  ]
  edge [
    source 39
    target 1627
  ]
  edge [
    source 39
    target 1628
  ]
  edge [
    source 39
    target 813
  ]
  edge [
    source 39
    target 927
  ]
  edge [
    source 39
    target 1629
  ]
  edge [
    source 39
    target 1630
  ]
  edge [
    source 39
    target 1631
  ]
  edge [
    source 39
    target 669
  ]
  edge [
    source 39
    target 1632
  ]
  edge [
    source 39
    target 1633
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1634
  ]
  edge [
    source 41
    target 1635
  ]
  edge [
    source 41
    target 1636
  ]
  edge [
    source 41
    target 1637
  ]
  edge [
    source 41
    target 1638
  ]
  edge [
    source 41
    target 1639
  ]
  edge [
    source 41
    target 303
  ]
  edge [
    source 41
    target 1640
  ]
  edge [
    source 41
    target 1641
  ]
  edge [
    source 41
    target 956
  ]
  edge [
    source 41
    target 1642
  ]
  edge [
    source 41
    target 1643
  ]
  edge [
    source 41
    target 1644
  ]
  edge [
    source 41
    target 1645
  ]
  edge [
    source 41
    target 1646
  ]
  edge [
    source 41
    target 1647
  ]
  edge [
    source 41
    target 1648
  ]
  edge [
    source 41
    target 1649
  ]
  edge [
    source 41
    target 1650
  ]
  edge [
    source 41
    target 1651
  ]
  edge [
    source 41
    target 1652
  ]
  edge [
    source 41
    target 1653
  ]
  edge [
    source 41
    target 1654
  ]
  edge [
    source 41
    target 1655
  ]
  edge [
    source 41
    target 338
  ]
  edge [
    source 41
    target 1656
  ]
  edge [
    source 41
    target 1657
  ]
  edge [
    source 41
    target 1658
  ]
  edge [
    source 41
    target 294
  ]
  edge [
    source 41
    target 1540
  ]
  edge [
    source 41
    target 1659
  ]
  edge [
    source 41
    target 1660
  ]
  edge [
    source 41
    target 1661
  ]
  edge [
    source 41
    target 1662
  ]
  edge [
    source 41
    target 1663
  ]
  edge [
    source 41
    target 1664
  ]
  edge [
    source 41
    target 1665
  ]
  edge [
    source 41
    target 1666
  ]
  edge [
    source 41
    target 1667
  ]
  edge [
    source 41
    target 1668
  ]
  edge [
    source 41
    target 1669
  ]
  edge [
    source 41
    target 1670
  ]
  edge [
    source 41
    target 1671
  ]
  edge [
    source 41
    target 1672
  ]
  edge [
    source 41
    target 1673
  ]
  edge [
    source 41
    target 1674
  ]
  edge [
    source 41
    target 1675
  ]
  edge [
    source 41
    target 1676
  ]
  edge [
    source 41
    target 1677
  ]
  edge [
    source 41
    target 1678
  ]
  edge [
    source 41
    target 1679
  ]
  edge [
    source 41
    target 1680
  ]
  edge [
    source 41
    target 1681
  ]
  edge [
    source 41
    target 1682
  ]
  edge [
    source 41
    target 1683
  ]
  edge [
    source 41
    target 1684
  ]
  edge [
    source 41
    target 1685
  ]
  edge [
    source 41
    target 1686
  ]
  edge [
    source 41
    target 126
  ]
  edge [
    source 41
    target 1687
  ]
  edge [
    source 41
    target 1688
  ]
  edge [
    source 41
    target 1689
  ]
  edge [
    source 41
    target 1507
  ]
  edge [
    source 41
    target 1690
  ]
  edge [
    source 41
    target 1691
  ]
  edge [
    source 41
    target 283
  ]
  edge [
    source 41
    target 1692
  ]
  edge [
    source 41
    target 1693
  ]
  edge [
    source 41
    target 1694
  ]
  edge [
    source 41
    target 1695
  ]
  edge [
    source 41
    target 1696
  ]
  edge [
    source 41
    target 304
  ]
  edge [
    source 41
    target 1697
  ]
  edge [
    source 41
    target 305
  ]
  edge [
    source 41
    target 1698
  ]
  edge [
    source 41
    target 200
  ]
  edge [
    source 41
    target 1699
  ]
  edge [
    source 41
    target 1700
  ]
  edge [
    source 41
    target 1701
  ]
  edge [
    source 41
    target 1702
  ]
  edge [
    source 41
    target 1703
  ]
  edge [
    source 41
    target 1704
  ]
  edge [
    source 41
    target 1705
  ]
  edge [
    source 41
    target 332
  ]
  edge [
    source 41
    target 1706
  ]
  edge [
    source 41
    target 1707
  ]
  edge [
    source 41
    target 1708
  ]
  edge [
    source 41
    target 1709
  ]
  edge [
    source 41
    target 1710
  ]
  edge [
    source 41
    target 1711
  ]
  edge [
    source 41
    target 1712
  ]
  edge [
    source 41
    target 935
  ]
  edge [
    source 41
    target 1713
  ]
  edge [
    source 41
    target 1714
  ]
  edge [
    source 41
    target 1715
  ]
  edge [
    source 41
    target 1716
  ]
  edge [
    source 41
    target 1717
  ]
  edge [
    source 41
    target 1718
  ]
  edge [
    source 41
    target 1719
  ]
  edge [
    source 41
    target 613
  ]
  edge [
    source 41
    target 314
  ]
  edge [
    source 41
    target 1720
  ]
  edge [
    source 41
    target 1721
  ]
  edge [
    source 41
    target 1722
  ]
  edge [
    source 41
    target 1723
  ]
  edge [
    source 41
    target 1724
  ]
  edge [
    source 41
    target 1725
  ]
  edge [
    source 41
    target 1726
  ]
  edge [
    source 41
    target 1727
  ]
  edge [
    source 41
    target 702
  ]
  edge [
    source 41
    target 1728
  ]
  edge [
    source 41
    target 1729
  ]
  edge [
    source 41
    target 1730
  ]
  edge [
    source 41
    target 604
  ]
  edge [
    source 41
    target 1731
  ]
  edge [
    source 41
    target 1732
  ]
  edge [
    source 41
    target 1733
  ]
  edge [
    source 41
    target 1734
  ]
  edge [
    source 41
    target 657
  ]
  edge [
    source 41
    target 1735
  ]
  edge [
    source 41
    target 1736
  ]
  edge [
    source 41
    target 1737
  ]
  edge [
    source 41
    target 1738
  ]
  edge [
    source 41
    target 1739
  ]
  edge [
    source 41
    target 1740
  ]
  edge [
    source 41
    target 1741
  ]
  edge [
    source 41
    target 1742
  ]
  edge [
    source 41
    target 1743
  ]
  edge [
    source 41
    target 1744
  ]
  edge [
    source 41
    target 1745
  ]
  edge [
    source 41
    target 1746
  ]
  edge [
    source 41
    target 1747
  ]
  edge [
    source 41
    target 301
  ]
  edge [
    source 41
    target 1748
  ]
  edge [
    source 41
    target 1749
  ]
  edge [
    source 41
    target 1750
  ]
  edge [
    source 41
    target 320
  ]
  edge [
    source 41
    target 1751
  ]
  edge [
    source 41
    target 1752
  ]
  edge [
    source 41
    target 1753
  ]
  edge [
    source 41
    target 1754
  ]
  edge [
    source 41
    target 1755
  ]
  edge [
    source 41
    target 1756
  ]
  edge [
    source 41
    target 1757
  ]
  edge [
    source 41
    target 1758
  ]
  edge [
    source 41
    target 1759
  ]
  edge [
    source 41
    target 1760
  ]
  edge [
    source 41
    target 1761
  ]
  edge [
    source 41
    target 1762
  ]
  edge [
    source 41
    target 1020
  ]
  edge [
    source 41
    target 1763
  ]
  edge [
    source 41
    target 1764
  ]
  edge [
    source 41
    target 1765
  ]
  edge [
    source 41
    target 308
  ]
  edge [
    source 41
    target 1766
  ]
  edge [
    source 41
    target 1767
  ]
  edge [
    source 41
    target 1768
  ]
  edge [
    source 41
    target 1769
  ]
  edge [
    source 41
    target 1770
  ]
  edge [
    source 41
    target 1771
  ]
  edge [
    source 41
    target 337
  ]
  edge [
    source 41
    target 1772
  ]
  edge [
    source 41
    target 1773
  ]
  edge [
    source 41
    target 1774
  ]
  edge [
    source 41
    target 1775
  ]
  edge [
    source 41
    target 1776
  ]
  edge [
    source 41
    target 1777
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1778
  ]
  edge [
    source 42
    target 1779
  ]
  edge [
    source 42
    target 1780
  ]
  edge [
    source 42
    target 1457
  ]
  edge [
    source 42
    target 643
  ]
  edge [
    source 42
    target 1781
  ]
  edge [
    source 42
    target 357
  ]
  edge [
    source 42
    target 1782
  ]
  edge [
    source 42
    target 1783
  ]
  edge [
    source 42
    target 1784
  ]
  edge [
    source 42
    target 1785
  ]
  edge [
    source 42
    target 1786
  ]
  edge [
    source 42
    target 1787
  ]
  edge [
    source 42
    target 1788
  ]
  edge [
    source 42
    target 1789
  ]
  edge [
    source 42
    target 1790
  ]
  edge [
    source 42
    target 1791
  ]
  edge [
    source 42
    target 684
  ]
  edge [
    source 42
    target 1792
  ]
  edge [
    source 42
    target 664
  ]
  edge [
    source 42
    target 1793
  ]
  edge [
    source 42
    target 1794
  ]
  edge [
    source 42
    target 1795
  ]
  edge [
    source 42
    target 1796
  ]
  edge [
    source 42
    target 1797
  ]
  edge [
    source 42
    target 1798
  ]
  edge [
    source 42
    target 1799
  ]
  edge [
    source 42
    target 791
  ]
  edge [
    source 42
    target 1800
  ]
  edge [
    source 42
    target 1801
  ]
  edge [
    source 42
    target 1802
  ]
  edge [
    source 42
    target 313
  ]
  edge [
    source 42
    target 1803
  ]
  edge [
    source 42
    target 173
  ]
  edge [
    source 42
    target 1804
  ]
  edge [
    source 42
    target 1805
  ]
  edge [
    source 42
    target 1806
  ]
  edge [
    source 42
    target 1807
  ]
  edge [
    source 42
    target 1808
  ]
  edge [
    source 42
    target 1809
  ]
  edge [
    source 42
    target 1810
  ]
  edge [
    source 42
    target 1811
  ]
  edge [
    source 42
    target 1812
  ]
  edge [
    source 42
    target 1813
  ]
  edge [
    source 42
    target 1533
  ]
  edge [
    source 42
    target 168
  ]
  edge [
    source 42
    target 189
  ]
  edge [
    source 42
    target 108
  ]
  edge [
    source 42
    target 1534
  ]
  edge [
    source 42
    target 1535
  ]
  edge [
    source 42
    target 1536
  ]
  edge [
    source 42
    target 192
  ]
  edge [
    source 42
    target 179
  ]
  edge [
    source 42
    target 1537
  ]
  edge [
    source 42
    target 1538
  ]
  edge [
    source 42
    target 1539
  ]
  edge [
    source 42
    target 1540
  ]
  edge [
    source 42
    target 1541
  ]
  edge [
    source 42
    target 164
  ]
  edge [
    source 42
    target 1542
  ]
  edge [
    source 42
    target 1543
  ]
  edge [
    source 42
    target 190
  ]
  edge [
    source 42
    target 1814
  ]
  edge [
    source 42
    target 540
  ]
  edge [
    source 42
    target 1464
  ]
  edge [
    source 42
    target 1815
  ]
  edge [
    source 42
    target 1816
  ]
  edge [
    source 42
    target 1817
  ]
  edge [
    source 42
    target 1818
  ]
  edge [
    source 42
    target 1819
  ]
  edge [
    source 42
    target 625
  ]
  edge [
    source 42
    target 1820
  ]
  edge [
    source 42
    target 1821
  ]
  edge [
    source 42
    target 1822
  ]
  edge [
    source 42
    target 1823
  ]
  edge [
    source 42
    target 1824
  ]
  edge [
    source 42
    target 1825
  ]
  edge [
    source 42
    target 1826
  ]
  edge [
    source 42
    target 1827
  ]
  edge [
    source 42
    target 1828
  ]
  edge [
    source 42
    target 1829
  ]
  edge [
    source 42
    target 1830
  ]
  edge [
    source 42
    target 1831
  ]
  edge [
    source 42
    target 1832
  ]
  edge [
    source 42
    target 1833
  ]
  edge [
    source 42
    target 91
  ]
  edge [
    source 42
    target 206
  ]
  edge [
    source 42
    target 1375
  ]
  edge [
    source 42
    target 105
  ]
  edge [
    source 42
    target 594
  ]
  edge [
    source 42
    target 1834
  ]
  edge [
    source 42
    target 1835
  ]
  edge [
    source 42
    target 1836
  ]
  edge [
    source 42
    target 1837
  ]
  edge [
    source 42
    target 1838
  ]
  edge [
    source 42
    target 1624
  ]
  edge [
    source 42
    target 1839
  ]
  edge [
    source 42
    target 1840
  ]
  edge [
    source 42
    target 1841
  ]
  edge [
    source 42
    target 1842
  ]
  edge [
    source 42
    target 1843
  ]
  edge [
    source 42
    target 1844
  ]
  edge [
    source 42
    target 671
  ]
  edge [
    source 42
    target 1845
  ]
  edge [
    source 42
    target 1846
  ]
  edge [
    source 42
    target 700
  ]
  edge [
    source 42
    target 1847
  ]
  edge [
    source 42
    target 1848
  ]
  edge [
    source 42
    target 1849
  ]
  edge [
    source 42
    target 1850
  ]
  edge [
    source 42
    target 1851
  ]
  edge [
    source 42
    target 1852
  ]
  edge [
    source 42
    target 1853
  ]
  edge [
    source 42
    target 1854
  ]
  edge [
    source 42
    target 1855
  ]
  edge [
    source 42
    target 702
  ]
  edge [
    source 42
    target 1856
  ]
  edge [
    source 42
    target 1857
  ]
  edge [
    source 42
    target 1858
  ]
  edge [
    source 42
    target 1859
  ]
  edge [
    source 42
    target 1860
  ]
  edge [
    source 42
    target 1861
  ]
  edge [
    source 42
    target 1862
  ]
  edge [
    source 42
    target 1863
  ]
  edge [
    source 42
    target 1864
  ]
  edge [
    source 42
    target 1865
  ]
  edge [
    source 42
    target 1866
  ]
  edge [
    source 42
    target 1867
  ]
  edge [
    source 42
    target 1868
  ]
  edge [
    source 42
    target 1869
  ]
  edge [
    source 42
    target 1870
  ]
  edge [
    source 42
    target 1871
  ]
  edge [
    source 42
    target 1872
  ]
  edge [
    source 42
    target 1873
  ]
  edge [
    source 42
    target 1874
  ]
  edge [
    source 42
    target 1875
  ]
  edge [
    source 42
    target 1876
  ]
  edge [
    source 42
    target 1877
  ]
  edge [
    source 42
    target 1878
  ]
  edge [
    source 42
    target 1879
  ]
  edge [
    source 42
    target 1880
  ]
  edge [
    source 42
    target 1881
  ]
  edge [
    source 42
    target 1882
  ]
  edge [
    source 42
    target 1883
  ]
  edge [
    source 42
    target 1884
  ]
  edge [
    source 42
    target 1885
  ]
  edge [
    source 42
    target 1886
  ]
  edge [
    source 42
    target 1887
  ]
  edge [
    source 42
    target 1888
  ]
  edge [
    source 42
    target 1889
  ]
  edge [
    source 42
    target 1890
  ]
  edge [
    source 42
    target 1891
  ]
  edge [
    source 42
    target 1892
  ]
  edge [
    source 42
    target 1893
  ]
  edge [
    source 42
    target 1894
  ]
  edge [
    source 42
    target 1895
  ]
  edge [
    source 42
    target 1896
  ]
  edge [
    source 42
    target 1897
  ]
  edge [
    source 42
    target 1898
  ]
  edge [
    source 42
    target 1531
  ]
  edge [
    source 42
    target 1899
  ]
  edge [
    source 42
    target 1900
  ]
  edge [
    source 42
    target 1901
  ]
  edge [
    source 42
    target 1902
  ]
  edge [
    source 42
    target 1903
  ]
  edge [
    source 42
    target 1904
  ]
  edge [
    source 42
    target 1905
  ]
  edge [
    source 42
    target 1906
  ]
  edge [
    source 42
    target 1907
  ]
  edge [
    source 42
    target 1742
  ]
  edge [
    source 42
    target 1908
  ]
  edge [
    source 42
    target 1909
  ]
  edge [
    source 42
    target 1910
  ]
  edge [
    source 42
    target 1911
  ]
  edge [
    source 42
    target 1912
  ]
  edge [
    source 42
    target 1913
  ]
  edge [
    source 42
    target 1914
  ]
  edge [
    source 42
    target 1915
  ]
  edge [
    source 42
    target 1916
  ]
  edge [
    source 42
    target 80
  ]
  edge [
    source 42
    target 1917
  ]
  edge [
    source 42
    target 1918
  ]
  edge [
    source 42
    target 1919
  ]
  edge [
    source 42
    target 343
  ]
  edge [
    source 42
    target 242
  ]
  edge [
    source 42
    target 1920
  ]
  edge [
    source 42
    target 94
  ]
  edge [
    source 42
    target 665
  ]
  edge [
    source 42
    target 1921
  ]
  edge [
    source 42
    target 1922
  ]
  edge [
    source 42
    target 1923
  ]
  edge [
    source 42
    target 1924
  ]
  edge [
    source 42
    target 1925
  ]
  edge [
    source 42
    target 1926
  ]
  edge [
    source 42
    target 1927
  ]
  edge [
    source 42
    target 1928
  ]
  edge [
    source 42
    target 1929
  ]
  edge [
    source 42
    target 1930
  ]
  edge [
    source 42
    target 1931
  ]
  edge [
    source 42
    target 1932
  ]
  edge [
    source 42
    target 1933
  ]
  edge [
    source 42
    target 1934
  ]
  edge [
    source 42
    target 1935
  ]
  edge [
    source 42
    target 1936
  ]
  edge [
    source 42
    target 1937
  ]
  edge [
    source 42
    target 1938
  ]
  edge [
    source 42
    target 1939
  ]
  edge [
    source 42
    target 1940
  ]
  edge [
    source 42
    target 1941
  ]
  edge [
    source 42
    target 635
  ]
  edge [
    source 42
    target 1942
  ]
  edge [
    source 42
    target 638
  ]
  edge [
    source 42
    target 640
  ]
  edge [
    source 42
    target 1943
  ]
  edge [
    source 42
    target 1944
  ]
  edge [
    source 42
    target 644
  ]
  edge [
    source 42
    target 645
  ]
  edge [
    source 42
    target 1945
  ]
  edge [
    source 42
    target 1946
  ]
  edge [
    source 42
    target 1947
  ]
  edge [
    source 42
    target 1587
  ]
  edge [
    source 42
    target 651
  ]
  edge [
    source 42
    target 300
  ]
  edge [
    source 42
    target 1948
  ]
  edge [
    source 42
    target 1609
  ]
  edge [
    source 42
    target 653
  ]
  edge [
    source 42
    target 1949
  ]
  edge [
    source 42
    target 1950
  ]
  edge [
    source 42
    target 1951
  ]
  edge [
    source 42
    target 1952
  ]
  edge [
    source 42
    target 1953
  ]
  edge [
    source 42
    target 1954
  ]
  edge [
    source 42
    target 660
  ]
  edge [
    source 42
    target 104
  ]
  edge [
    source 42
    target 1955
  ]
  edge [
    source 42
    target 1956
  ]
  edge [
    source 42
    target 666
  ]
  edge [
    source 42
    target 1957
  ]
  edge [
    source 42
    target 177
  ]
  edge [
    source 42
    target 1958
  ]
  edge [
    source 42
    target 591
  ]
  edge [
    source 42
    target 1959
  ]
  edge [
    source 42
    target 1960
  ]
  edge [
    source 42
    target 1961
  ]
  edge [
    source 42
    target 1962
  ]
  edge [
    source 42
    target 1963
  ]
  edge [
    source 42
    target 1964
  ]
  edge [
    source 42
    target 1965
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1533
  ]
  edge [
    source 43
    target 168
  ]
  edge [
    source 43
    target 189
  ]
  edge [
    source 43
    target 108
  ]
  edge [
    source 43
    target 1534
  ]
  edge [
    source 43
    target 1535
  ]
  edge [
    source 43
    target 1536
  ]
  edge [
    source 43
    target 179
  ]
  edge [
    source 43
    target 192
  ]
  edge [
    source 43
    target 1537
  ]
  edge [
    source 43
    target 1538
  ]
  edge [
    source 43
    target 1539
  ]
  edge [
    source 43
    target 1540
  ]
  edge [
    source 43
    target 1541
  ]
  edge [
    source 43
    target 164
  ]
  edge [
    source 43
    target 1542
  ]
  edge [
    source 43
    target 1543
  ]
  edge [
    source 43
    target 190
  ]
  edge [
    source 43
    target 169
  ]
  edge [
    source 43
    target 170
  ]
  edge [
    source 43
    target 171
  ]
  edge [
    source 43
    target 172
  ]
  edge [
    source 43
    target 173
  ]
  edge [
    source 43
    target 174
  ]
  edge [
    source 43
    target 175
  ]
  edge [
    source 43
    target 176
  ]
  edge [
    source 43
    target 177
  ]
  edge [
    source 43
    target 178
  ]
  edge [
    source 43
    target 180
  ]
  edge [
    source 43
    target 181
  ]
  edge [
    source 43
    target 182
  ]
  edge [
    source 43
    target 183
  ]
  edge [
    source 43
    target 184
  ]
  edge [
    source 43
    target 185
  ]
  edge [
    source 43
    target 186
  ]
  edge [
    source 43
    target 187
  ]
  edge [
    source 43
    target 188
  ]
  edge [
    source 43
    target 191
  ]
  edge [
    source 43
    target 193
  ]
  edge [
    source 43
    target 194
  ]
  edge [
    source 43
    target 1966
  ]
  edge [
    source 43
    target 1967
  ]
  edge [
    source 43
    target 1968
  ]
  edge [
    source 43
    target 1969
  ]
  edge [
    source 43
    target 1476
  ]
  edge [
    source 43
    target 1970
  ]
  edge [
    source 43
    target 1971
  ]
  edge [
    source 43
    target 1972
  ]
  edge [
    source 43
    target 1973
  ]
  edge [
    source 43
    target 1974
  ]
  edge [
    source 43
    target 1975
  ]
  edge [
    source 43
    target 1976
  ]
  edge [
    source 43
    target 1977
  ]
  edge [
    source 43
    target 1978
  ]
  edge [
    source 43
    target 1979
  ]
  edge [
    source 43
    target 1980
  ]
  edge [
    source 43
    target 1981
  ]
  edge [
    source 43
    target 1982
  ]
  edge [
    source 43
    target 1983
  ]
  edge [
    source 43
    target 1984
  ]
  edge [
    source 43
    target 1985
  ]
  edge [
    source 43
    target 1986
  ]
  edge [
    source 43
    target 1545
  ]
  edge [
    source 43
    target 1653
  ]
  edge [
    source 43
    target 1987
  ]
  edge [
    source 43
    target 1988
  ]
  edge [
    source 43
    target 1989
  ]
  edge [
    source 43
    target 1990
  ]
  edge [
    source 43
    target 1991
  ]
  edge [
    source 43
    target 1992
  ]
  edge [
    source 43
    target 1993
  ]
  edge [
    source 43
    target 1994
  ]
  edge [
    source 43
    target 1995
  ]
  edge [
    source 43
    target 1996
  ]
  edge [
    source 43
    target 1997
  ]
  edge [
    source 43
    target 1588
  ]
  edge [
    source 43
    target 1998
  ]
  edge [
    source 43
    target 1999
  ]
  edge [
    source 43
    target 513
  ]
  edge [
    source 43
    target 72
  ]
  edge [
    source 43
    target 2000
  ]
  edge [
    source 43
    target 1831
  ]
  edge [
    source 43
    target 2001
  ]
  edge [
    source 43
    target 2002
  ]
  edge [
    source 43
    target 534
  ]
  edge [
    source 43
    target 2003
  ]
  edge [
    source 43
    target 2004
  ]
  edge [
    source 43
    target 2005
  ]
  edge [
    source 43
    target 2006
  ]
  edge [
    source 43
    target 540
  ]
  edge [
    source 43
    target 2007
  ]
  edge [
    source 43
    target 2008
  ]
  edge [
    source 43
    target 2009
  ]
  edge [
    source 43
    target 242
  ]
  edge [
    source 43
    target 1238
  ]
  edge [
    source 43
    target 2010
  ]
  edge [
    source 43
    target 2011
  ]
  edge [
    source 43
    target 2012
  ]
  edge [
    source 43
    target 2013
  ]
  edge [
    source 43
    target 2014
  ]
  edge [
    source 43
    target 2015
  ]
  edge [
    source 43
    target 2016
  ]
  edge [
    source 43
    target 2017
  ]
  edge [
    source 43
    target 2018
  ]
  edge [
    source 43
    target 2019
  ]
  edge [
    source 43
    target 2020
  ]
  edge [
    source 43
    target 2021
  ]
  edge [
    source 43
    target 952
  ]
  edge [
    source 43
    target 2022
  ]
  edge [
    source 43
    target 2023
  ]
  edge [
    source 43
    target 2024
  ]
  edge [
    source 43
    target 2025
  ]
  edge [
    source 43
    target 2026
  ]
  edge [
    source 43
    target 2027
  ]
  edge [
    source 43
    target 2028
  ]
  edge [
    source 43
    target 2029
  ]
  edge [
    source 43
    target 2030
  ]
  edge [
    source 43
    target 2031
  ]
  edge [
    source 43
    target 2032
  ]
  edge [
    source 43
    target 2033
  ]
  edge [
    source 43
    target 2034
  ]
  edge [
    source 43
    target 2035
  ]
  edge [
    source 43
    target 2036
  ]
  edge [
    source 43
    target 2037
  ]
  edge [
    source 43
    target 2038
  ]
  edge [
    source 43
    target 2039
  ]
  edge [
    source 43
    target 2040
  ]
  edge [
    source 43
    target 2041
  ]
  edge [
    source 43
    target 2042
  ]
  edge [
    source 43
    target 1205
  ]
  edge [
    source 43
    target 2043
  ]
  edge [
    source 43
    target 2044
  ]
  edge [
    source 43
    target 2045
  ]
  edge [
    source 43
    target 2046
  ]
  edge [
    source 43
    target 2047
  ]
  edge [
    source 43
    target 2048
  ]
  edge [
    source 43
    target 2049
  ]
  edge [
    source 43
    target 2050
  ]
  edge [
    source 43
    target 2051
  ]
  edge [
    source 43
    target 2052
  ]
  edge [
    source 43
    target 2053
  ]
  edge [
    source 43
    target 2054
  ]
  edge [
    source 43
    target 2055
  ]
  edge [
    source 43
    target 2056
  ]
  edge [
    source 43
    target 2057
  ]
  edge [
    source 43
    target 2058
  ]
  edge [
    source 43
    target 2059
  ]
  edge [
    source 43
    target 2060
  ]
  edge [
    source 43
    target 2061
  ]
  edge [
    source 43
    target 2062
  ]
  edge [
    source 43
    target 2063
  ]
  edge [
    source 43
    target 2064
  ]
  edge [
    source 43
    target 2065
  ]
  edge [
    source 43
    target 2066
  ]
  edge [
    source 43
    target 2067
  ]
  edge [
    source 43
    target 2068
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 2069
  ]
  edge [
    source 44
    target 1135
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 2070
  ]
  edge [
    source 45
    target 2071
  ]
  edge [
    source 45
    target 2072
  ]
  edge [
    source 45
    target 2073
  ]
  edge [
    source 45
    target 2074
  ]
  edge [
    source 45
    target 162
  ]
  edge [
    source 45
    target 1594
  ]
  edge [
    source 45
    target 2075
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 669
  ]
  edge [
    source 46
    target 2076
  ]
  edge [
    source 46
    target 2077
  ]
  edge [
    source 46
    target 2078
  ]
  edge [
    source 46
    target 2079
  ]
  edge [
    source 46
    target 2080
  ]
  edge [
    source 46
    target 2081
  ]
  edge [
    source 46
    target 2082
  ]
  edge [
    source 46
    target 2083
  ]
  edge [
    source 46
    target 2084
  ]
  edge [
    source 46
    target 2085
  ]
  edge [
    source 46
    target 2086
  ]
  edge [
    source 46
    target 2087
  ]
  edge [
    source 46
    target 2088
  ]
  edge [
    source 46
    target 2089
  ]
  edge [
    source 46
    target 2090
  ]
  edge [
    source 46
    target 2091
  ]
  edge [
    source 46
    target 2092
  ]
  edge [
    source 46
    target 2093
  ]
  edge [
    source 46
    target 2094
  ]
  edge [
    source 46
    target 2095
  ]
  edge [
    source 46
    target 2096
  ]
  edge [
    source 46
    target 2097
  ]
  edge [
    source 46
    target 2098
  ]
  edge [
    source 46
    target 1918
  ]
  edge [
    source 46
    target 2099
  ]
  edge [
    source 46
    target 2100
  ]
  edge [
    source 46
    target 2101
  ]
  edge [
    source 46
    target 2102
  ]
  edge [
    source 46
    target 2103
  ]
  edge [
    source 46
    target 2104
  ]
  edge [
    source 46
    target 2105
  ]
  edge [
    source 46
    target 2106
  ]
  edge [
    source 46
    target 2107
  ]
  edge [
    source 46
    target 2108
  ]
  edge [
    source 46
    target 2109
  ]
  edge [
    source 46
    target 2110
  ]
  edge [
    source 46
    target 1826
  ]
  edge [
    source 46
    target 2111
  ]
  edge [
    source 46
    target 901
  ]
  edge [
    source 46
    target 2112
  ]
  edge [
    source 46
    target 2113
  ]
  edge [
    source 46
    target 2114
  ]
  edge [
    source 46
    target 2115
  ]
  edge [
    source 46
    target 2116
  ]
  edge [
    source 46
    target 915
  ]
  edge [
    source 46
    target 2117
  ]
  edge [
    source 46
    target 2118
  ]
  edge [
    source 46
    target 2119
  ]
  edge [
    source 46
    target 2120
  ]
  edge [
    source 46
    target 2121
  ]
  edge [
    source 46
    target 2122
  ]
  edge [
    source 46
    target 896
  ]
  edge [
    source 46
    target 2123
  ]
  edge [
    source 46
    target 2124
  ]
  edge [
    source 46
    target 275
  ]
  edge [
    source 46
    target 2125
  ]
  edge [
    source 46
    target 1672
  ]
  edge [
    source 46
    target 2126
  ]
  edge [
    source 46
    target 2127
  ]
  edge [
    source 46
    target 193
  ]
  edge [
    source 46
    target 2128
  ]
  edge [
    source 46
    target 2129
  ]
  edge [
    source 46
    target 2130
  ]
  edge [
    source 46
    target 1531
  ]
  edge [
    source 46
    target 2131
  ]
  edge [
    source 46
    target 2132
  ]
  edge [
    source 46
    target 2133
  ]
  edge [
    source 46
    target 2134
  ]
  edge [
    source 46
    target 2135
  ]
  edge [
    source 46
    target 2136
  ]
  edge [
    source 46
    target 2137
  ]
  edge [
    source 46
    target 2138
  ]
  edge [
    source 46
    target 2139
  ]
  edge [
    source 46
    target 2140
  ]
  edge [
    source 46
    target 2141
  ]
  edge [
    source 46
    target 2142
  ]
  edge [
    source 46
    target 2143
  ]
  edge [
    source 46
    target 2144
  ]
  edge [
    source 46
    target 2145
  ]
  edge [
    source 46
    target 2146
  ]
  edge [
    source 46
    target 2147
  ]
  edge [
    source 46
    target 912
  ]
  edge [
    source 46
    target 2148
  ]
  edge [
    source 46
    target 1601
  ]
  edge [
    source 46
    target 2149
  ]
  edge [
    source 46
    target 326
  ]
  edge [
    source 46
    target 2150
  ]
  edge [
    source 46
    target 2151
  ]
  edge [
    source 46
    target 2152
  ]
  edge [
    source 46
    target 2153
  ]
  edge [
    source 46
    target 2154
  ]
  edge [
    source 46
    target 2155
  ]
  edge [
    source 46
    target 2156
  ]
  edge [
    source 46
    target 2157
  ]
  edge [
    source 46
    target 2158
  ]
  edge [
    source 46
    target 2159
  ]
  edge [
    source 46
    target 2160
  ]
  edge [
    source 46
    target 2161
  ]
  edge [
    source 46
    target 2162
  ]
  edge [
    source 46
    target 2163
  ]
  edge [
    source 46
    target 2164
  ]
  edge [
    source 46
    target 2165
  ]
  edge [
    source 46
    target 2166
  ]
  edge [
    source 46
    target 404
  ]
  edge [
    source 46
    target 2167
  ]
  edge [
    source 46
    target 2168
  ]
  edge [
    source 46
    target 2169
  ]
  edge [
    source 46
    target 2170
  ]
  edge [
    source 46
    target 2171
  ]
  edge [
    source 46
    target 2172
  ]
  edge [
    source 46
    target 2173
  ]
  edge [
    source 46
    target 2174
  ]
  edge [
    source 46
    target 2175
  ]
  edge [
    source 46
    target 2176
  ]
  edge [
    source 46
    target 2177
  ]
  edge [
    source 46
    target 2178
  ]
  edge [
    source 46
    target 2179
  ]
  edge [
    source 46
    target 2180
  ]
  edge [
    source 46
    target 2181
  ]
  edge [
    source 46
    target 2182
  ]
  edge [
    source 46
    target 2183
  ]
  edge [
    source 46
    target 2184
  ]
  edge [
    source 46
    target 2185
  ]
  edge [
    source 46
    target 2186
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 2187
  ]
  edge [
    source 47
    target 59
  ]
  edge [
    source 47
    target 2188
  ]
  edge [
    source 47
    target 2189
  ]
  edge [
    source 47
    target 622
  ]
  edge [
    source 47
    target 2190
  ]
  edge [
    source 47
    target 2191
  ]
  edge [
    source 47
    target 2192
  ]
  edge [
    source 47
    target 2193
  ]
  edge [
    source 47
    target 2194
  ]
  edge [
    source 47
    target 2195
  ]
  edge [
    source 47
    target 2196
  ]
  edge [
    source 47
    target 2197
  ]
  edge [
    source 47
    target 2198
  ]
  edge [
    source 47
    target 2199
  ]
  edge [
    source 47
    target 2200
  ]
  edge [
    source 47
    target 628
  ]
  edge [
    source 47
    target 2201
  ]
  edge [
    source 47
    target 2202
  ]
  edge [
    source 47
    target 2203
  ]
  edge [
    source 47
    target 2204
  ]
  edge [
    source 47
    target 2205
  ]
  edge [
    source 47
    target 1032
  ]
  edge [
    source 47
    target 2206
  ]
  edge [
    source 47
    target 1531
  ]
  edge [
    source 47
    target 1882
  ]
  edge [
    source 47
    target 669
  ]
  edge [
    source 47
    target 2207
  ]
  edge [
    source 47
    target 2208
  ]
  edge [
    source 47
    target 2209
  ]
  edge [
    source 47
    target 459
  ]
  edge [
    source 47
    target 2210
  ]
  edge [
    source 47
    target 2211
  ]
  edge [
    source 47
    target 2212
  ]
  edge [
    source 47
    target 2213
  ]
  edge [
    source 47
    target 2214
  ]
  edge [
    source 47
    target 2215
  ]
  edge [
    source 47
    target 2216
  ]
  edge [
    source 47
    target 2217
  ]
  edge [
    source 47
    target 2218
  ]
  edge [
    source 47
    target 2219
  ]
  edge [
    source 47
    target 540
  ]
  edge [
    source 47
    target 2220
  ]
  edge [
    source 47
    target 684
  ]
  edge [
    source 47
    target 2221
  ]
  edge [
    source 47
    target 2222
  ]
  edge [
    source 47
    target 2223
  ]
  edge [
    source 47
    target 2095
  ]
  edge [
    source 47
    target 105
  ]
  edge [
    source 47
    target 2224
  ]
  edge [
    source 47
    target 980
  ]
  edge [
    source 47
    target 171
  ]
  edge [
    source 47
    target 981
  ]
  edge [
    source 47
    target 174
  ]
  edge [
    source 47
    target 982
  ]
  edge [
    source 47
    target 82
  ]
  edge [
    source 47
    target 983
  ]
  edge [
    source 47
    target 984
  ]
  edge [
    source 47
    target 985
  ]
  edge [
    source 47
    target 986
  ]
  edge [
    source 47
    target 987
  ]
  edge [
    source 47
    target 988
  ]
  edge [
    source 47
    target 989
  ]
  edge [
    source 47
    target 990
  ]
  edge [
    source 47
    target 991
  ]
  edge [
    source 47
    target 992
  ]
  edge [
    source 47
    target 993
  ]
  edge [
    source 47
    target 994
  ]
  edge [
    source 47
    target 995
  ]
  edge [
    source 47
    target 996
  ]
  edge [
    source 47
    target 997
  ]
  edge [
    source 47
    target 998
  ]
  edge [
    source 47
    target 999
  ]
  edge [
    source 47
    target 1000
  ]
  edge [
    source 47
    target 1001
  ]
  edge [
    source 47
    target 2225
  ]
  edge [
    source 47
    target 2226
  ]
  edge [
    source 47
    target 2227
  ]
  edge [
    source 47
    target 2228
  ]
  edge [
    source 47
    target 2229
  ]
  edge [
    source 47
    target 2230
  ]
  edge [
    source 47
    target 2231
  ]
  edge [
    source 47
    target 2232
  ]
  edge [
    source 47
    target 2233
  ]
  edge [
    source 47
    target 2234
  ]
  edge [
    source 47
    target 2235
  ]
  edge [
    source 47
    target 2236
  ]
  edge [
    source 47
    target 1601
  ]
  edge [
    source 47
    target 976
  ]
  edge [
    source 47
    target 2237
  ]
  edge [
    source 47
    target 2238
  ]
  edge [
    source 47
    target 275
  ]
  edge [
    source 47
    target 2239
  ]
  edge [
    source 47
    target 2240
  ]
  edge [
    source 47
    target 1374
  ]
  edge [
    source 47
    target 2241
  ]
  edge [
    source 47
    target 2242
  ]
  edge [
    source 47
    target 2243
  ]
  edge [
    source 47
    target 2244
  ]
  edge [
    source 47
    target 2245
  ]
  edge [
    source 47
    target 2246
  ]
  edge [
    source 47
    target 2247
  ]
  edge [
    source 47
    target 2248
  ]
  edge [
    source 47
    target 2249
  ]
  edge [
    source 47
    target 2250
  ]
  edge [
    source 47
    target 2251
  ]
  edge [
    source 47
    target 2252
  ]
  edge [
    source 47
    target 2253
  ]
  edge [
    source 47
    target 2254
  ]
  edge [
    source 47
    target 2255
  ]
  edge [
    source 47
    target 2256
  ]
  edge [
    source 47
    target 626
  ]
  edge [
    source 47
    target 2257
  ]
  edge [
    source 47
    target 2258
  ]
  edge [
    source 47
    target 2259
  ]
  edge [
    source 47
    target 2260
  ]
  edge [
    source 47
    target 2261
  ]
  edge [
    source 47
    target 2262
  ]
  edge [
    source 47
    target 2263
  ]
  edge [
    source 47
    target 2264
  ]
  edge [
    source 47
    target 2265
  ]
  edge [
    source 47
    target 2266
  ]
  edge [
    source 47
    target 2267
  ]
  edge [
    source 47
    target 2268
  ]
  edge [
    source 47
    target 2269
  ]
  edge [
    source 47
    target 2270
  ]
  edge [
    source 47
    target 2271
  ]
  edge [
    source 47
    target 259
  ]
  edge [
    source 47
    target 2272
  ]
  edge [
    source 47
    target 2273
  ]
  edge [
    source 47
    target 2274
  ]
  edge [
    source 47
    target 2275
  ]
  edge [
    source 47
    target 2276
  ]
  edge [
    source 47
    target 2006
  ]
  edge [
    source 47
    target 2277
  ]
  edge [
    source 47
    target 2278
  ]
  edge [
    source 47
    target 636
  ]
  edge [
    source 47
    target 2279
  ]
  edge [
    source 47
    target 2280
  ]
  edge [
    source 47
    target 2281
  ]
  edge [
    source 47
    target 2282
  ]
  edge [
    source 47
    target 2283
  ]
  edge [
    source 47
    target 2284
  ]
  edge [
    source 47
    target 2285
  ]
  edge [
    source 47
    target 357
  ]
  edge [
    source 47
    target 2286
  ]
  edge [
    source 47
    target 2287
  ]
  edge [
    source 47
    target 2288
  ]
  edge [
    source 47
    target 2289
  ]
  edge [
    source 47
    target 2290
  ]
  edge [
    source 47
    target 2291
  ]
  edge [
    source 47
    target 2292
  ]
  edge [
    source 47
    target 2293
  ]
  edge [
    source 47
    target 2294
  ]
  edge [
    source 47
    target 2295
  ]
  edge [
    source 47
    target 2296
  ]
  edge [
    source 47
    target 2297
  ]
  edge [
    source 47
    target 1834
  ]
  edge [
    source 47
    target 2298
  ]
  edge [
    source 47
    target 2299
  ]
  edge [
    source 47
    target 2300
  ]
  edge [
    source 47
    target 66
  ]
  edge [
    source 47
    target 2301
  ]
  edge [
    source 47
    target 2302
  ]
  edge [
    source 47
    target 2303
  ]
  edge [
    source 47
    target 2304
  ]
  edge [
    source 47
    target 2305
  ]
  edge [
    source 47
    target 2306
  ]
  edge [
    source 47
    target 2307
  ]
  edge [
    source 47
    target 2308
  ]
  edge [
    source 47
    target 2309
  ]
  edge [
    source 47
    target 2310
  ]
  edge [
    source 47
    target 2311
  ]
  edge [
    source 47
    target 2312
  ]
  edge [
    source 47
    target 2313
  ]
  edge [
    source 47
    target 2314
  ]
  edge [
    source 47
    target 2315
  ]
  edge [
    source 47
    target 2316
  ]
  edge [
    source 47
    target 2317
  ]
  edge [
    source 47
    target 2318
  ]
  edge [
    source 47
    target 2319
  ]
  edge [
    source 47
    target 2320
  ]
  edge [
    source 47
    target 2321
  ]
  edge [
    source 47
    target 2322
  ]
  edge [
    source 47
    target 2323
  ]
  edge [
    source 47
    target 1389
  ]
  edge [
    source 47
    target 83
  ]
  edge [
    source 47
    target 2324
  ]
  edge [
    source 47
    target 2325
  ]
  edge [
    source 47
    target 2326
  ]
  edge [
    source 47
    target 2327
  ]
  edge [
    source 47
    target 2328
  ]
  edge [
    source 47
    target 2329
  ]
  edge [
    source 47
    target 2330
  ]
  edge [
    source 47
    target 2331
  ]
  edge [
    source 47
    target 2332
  ]
  edge [
    source 47
    target 2333
  ]
  edge [
    source 47
    target 2334
  ]
  edge [
    source 47
    target 2335
  ]
  edge [
    source 47
    target 2336
  ]
  edge [
    source 47
    target 2337
  ]
  edge [
    source 47
    target 2338
  ]
  edge [
    source 47
    target 2339
  ]
  edge [
    source 47
    target 2340
  ]
  edge [
    source 47
    target 2341
  ]
  edge [
    source 47
    target 2342
  ]
  edge [
    source 47
    target 2343
  ]
  edge [
    source 47
    target 2344
  ]
  edge [
    source 47
    target 2345
  ]
  edge [
    source 47
    target 2346
  ]
  edge [
    source 47
    target 2347
  ]
  edge [
    source 47
    target 2348
  ]
  edge [
    source 47
    target 2349
  ]
  edge [
    source 47
    target 2350
  ]
  edge [
    source 47
    target 2351
  ]
  edge [
    source 47
    target 2352
  ]
  edge [
    source 47
    target 2353
  ]
  edge [
    source 47
    target 2354
  ]
  edge [
    source 47
    target 2355
  ]
  edge [
    source 47
    target 2356
  ]
  edge [
    source 47
    target 2357
  ]
  edge [
    source 47
    target 2358
  ]
  edge [
    source 47
    target 2359
  ]
  edge [
    source 47
    target 2360
  ]
  edge [
    source 47
    target 2361
  ]
  edge [
    source 47
    target 2362
  ]
  edge [
    source 47
    target 2363
  ]
  edge [
    source 47
    target 2364
  ]
  edge [
    source 47
    target 2365
  ]
  edge [
    source 47
    target 2366
  ]
  edge [
    source 47
    target 2367
  ]
  edge [
    source 47
    target 2368
  ]
  edge [
    source 47
    target 2369
  ]
  edge [
    source 47
    target 2370
  ]
  edge [
    source 47
    target 2371
  ]
  edge [
    source 47
    target 2372
  ]
  edge [
    source 47
    target 2373
  ]
  edge [
    source 47
    target 2374
  ]
  edge [
    source 47
    target 2375
  ]
  edge [
    source 47
    target 594
  ]
  edge [
    source 47
    target 2376
  ]
  edge [
    source 47
    target 2377
  ]
  edge [
    source 47
    target 2378
  ]
  edge [
    source 47
    target 2379
  ]
  edge [
    source 47
    target 2380
  ]
  edge [
    source 47
    target 2381
  ]
  edge [
    source 47
    target 2382
  ]
  edge [
    source 47
    target 2383
  ]
  edge [
    source 47
    target 2384
  ]
  edge [
    source 47
    target 2385
  ]
  edge [
    source 47
    target 2386
  ]
  edge [
    source 47
    target 2387
  ]
  edge [
    source 47
    target 2388
  ]
  edge [
    source 47
    target 2389
  ]
  edge [
    source 47
    target 2390
  ]
  edge [
    source 47
    target 2391
  ]
  edge [
    source 47
    target 2392
  ]
  edge [
    source 47
    target 2393
  ]
  edge [
    source 47
    target 2394
  ]
  edge [
    source 47
    target 2395
  ]
  edge [
    source 47
    target 2396
  ]
  edge [
    source 47
    target 2397
  ]
  edge [
    source 47
    target 2398
  ]
  edge [
    source 47
    target 589
  ]
  edge [
    source 47
    target 283
  ]
  edge [
    source 47
    target 2399
  ]
  edge [
    source 47
    target 325
  ]
  edge [
    source 47
    target 2400
  ]
  edge [
    source 47
    target 2401
  ]
  edge [
    source 47
    target 317
  ]
  edge [
    source 47
    target 2402
  ]
  edge [
    source 47
    target 592
  ]
  edge [
    source 47
    target 1659
  ]
  edge [
    source 47
    target 318
  ]
  edge [
    source 47
    target 334
  ]
  edge [
    source 47
    target 2403
  ]
  edge [
    source 47
    target 2404
  ]
  edge [
    source 47
    target 2405
  ]
  edge [
    source 47
    target 1815
  ]
  edge [
    source 47
    target 671
  ]
  edge [
    source 47
    target 2406
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 2407
  ]
  edge [
    source 48
    target 2408
  ]
  edge [
    source 48
    target 2409
  ]
  edge [
    source 48
    target 445
  ]
  edge [
    source 48
    target 2410
  ]
  edge [
    source 48
    target 2411
  ]
  edge [
    source 48
    target 2412
  ]
  edge [
    source 48
    target 2413
  ]
]
