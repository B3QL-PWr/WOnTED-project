graph [
  node [
    id 0
    label "zwykle"
    origin "text"
  ]
  node [
    id 1
    label "polska"
    origin "text"
  ]
  node [
    id 2
    label "tyle"
    origin "text"
  ]
  node [
    id 3
    label "rozwini&#281;ta"
    origin "text"
  ]
  node [
    id 4
    label "pe&#322;ny"
    origin "text"
  ]
  node [
    id 5
    label "demokracja"
    origin "text"
  ]
  node [
    id 6
    label "kraj"
    origin "text"
  ]
  node [
    id 7
    label "cz&#281;sto"
  ]
  node [
    id 8
    label "zwyk&#322;y"
  ]
  node [
    id 9
    label "cz&#281;sty"
  ]
  node [
    id 10
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 11
    label "przeci&#281;tny"
  ]
  node [
    id 12
    label "zwyczajnie"
  ]
  node [
    id 13
    label "okre&#347;lony"
  ]
  node [
    id 14
    label "wiele"
  ]
  node [
    id 15
    label "konkretnie"
  ]
  node [
    id 16
    label "nieznacznie"
  ]
  node [
    id 17
    label "wiela"
  ]
  node [
    id 18
    label "du&#380;y"
  ]
  node [
    id 19
    label "nieistotnie"
  ]
  node [
    id 20
    label "nieznaczny"
  ]
  node [
    id 21
    label "jasno"
  ]
  node [
    id 22
    label "posilnie"
  ]
  node [
    id 23
    label "dok&#322;adnie"
  ]
  node [
    id 24
    label "tre&#347;ciwie"
  ]
  node [
    id 25
    label "po&#380;ywnie"
  ]
  node [
    id 26
    label "konkretny"
  ]
  node [
    id 27
    label "solidny"
  ]
  node [
    id 28
    label "&#322;adnie"
  ]
  node [
    id 29
    label "nie&#378;le"
  ]
  node [
    id 30
    label "krzywa"
  ]
  node [
    id 31
    label "figura_geometryczna"
  ]
  node [
    id 32
    label "linia"
  ]
  node [
    id 33
    label "poprowadzi&#263;"
  ]
  node [
    id 34
    label "prowadzi&#263;"
  ]
  node [
    id 35
    label "prowadzenie"
  ]
  node [
    id 36
    label "curvature"
  ]
  node [
    id 37
    label "curve"
  ]
  node [
    id 38
    label "nieograniczony"
  ]
  node [
    id 39
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 40
    label "satysfakcja"
  ]
  node [
    id 41
    label "bezwzgl&#281;dny"
  ]
  node [
    id 42
    label "ca&#322;y"
  ]
  node [
    id 43
    label "otwarty"
  ]
  node [
    id 44
    label "wype&#322;nienie"
  ]
  node [
    id 45
    label "kompletny"
  ]
  node [
    id 46
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 47
    label "pe&#322;no"
  ]
  node [
    id 48
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 49
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 50
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 51
    label "zupe&#322;ny"
  ]
  node [
    id 52
    label "r&#243;wny"
  ]
  node [
    id 53
    label "jedyny"
  ]
  node [
    id 54
    label "zdr&#243;w"
  ]
  node [
    id 55
    label "calu&#347;ko"
  ]
  node [
    id 56
    label "&#380;ywy"
  ]
  node [
    id 57
    label "podobny"
  ]
  node [
    id 58
    label "ca&#322;o"
  ]
  node [
    id 59
    label "kompletnie"
  ]
  node [
    id 60
    label "w_pizdu"
  ]
  node [
    id 61
    label "dowolny"
  ]
  node [
    id 62
    label "przestrze&#324;"
  ]
  node [
    id 63
    label "rozleg&#322;y"
  ]
  node [
    id 64
    label "nieograniczenie"
  ]
  node [
    id 65
    label "otworzysty"
  ]
  node [
    id 66
    label "aktywny"
  ]
  node [
    id 67
    label "publiczny"
  ]
  node [
    id 68
    label "zdecydowany"
  ]
  node [
    id 69
    label "prostoduszny"
  ]
  node [
    id 70
    label "jawnie"
  ]
  node [
    id 71
    label "bezpo&#347;redni"
  ]
  node [
    id 72
    label "aktualny"
  ]
  node [
    id 73
    label "kontaktowy"
  ]
  node [
    id 74
    label "otwarcie"
  ]
  node [
    id 75
    label "ewidentny"
  ]
  node [
    id 76
    label "dost&#281;pny"
  ]
  node [
    id 77
    label "gotowy"
  ]
  node [
    id 78
    label "mundurowanie"
  ]
  node [
    id 79
    label "klawy"
  ]
  node [
    id 80
    label "dor&#243;wnywanie"
  ]
  node [
    id 81
    label "zr&#243;wnanie_si&#281;"
  ]
  node [
    id 82
    label "jednotonny"
  ]
  node [
    id 83
    label "taki&#380;"
  ]
  node [
    id 84
    label "dobry"
  ]
  node [
    id 85
    label "jednolity"
  ]
  node [
    id 86
    label "mundurowa&#263;"
  ]
  node [
    id 87
    label "r&#243;wnanie"
  ]
  node [
    id 88
    label "jednoczesny"
  ]
  node [
    id 89
    label "zr&#243;wnanie"
  ]
  node [
    id 90
    label "miarowo"
  ]
  node [
    id 91
    label "r&#243;wno"
  ]
  node [
    id 92
    label "jednakowo"
  ]
  node [
    id 93
    label "zr&#243;wnywanie"
  ]
  node [
    id 94
    label "identyczny"
  ]
  node [
    id 95
    label "regularny"
  ]
  node [
    id 96
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 97
    label "prosty"
  ]
  node [
    id 98
    label "stabilny"
  ]
  node [
    id 99
    label "integer"
  ]
  node [
    id 100
    label "liczba"
  ]
  node [
    id 101
    label "zlewanie_si&#281;"
  ]
  node [
    id 102
    label "ilo&#347;&#263;"
  ]
  node [
    id 103
    label "uk&#322;ad"
  ]
  node [
    id 104
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 105
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 106
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 107
    label "og&#243;lnie"
  ]
  node [
    id 108
    label "&#322;&#261;czny"
  ]
  node [
    id 109
    label "zupe&#322;nie"
  ]
  node [
    id 110
    label "uzupe&#322;nienie"
  ]
  node [
    id 111
    label "woof"
  ]
  node [
    id 112
    label "activity"
  ]
  node [
    id 113
    label "spowodowanie"
  ]
  node [
    id 114
    label "control"
  ]
  node [
    id 115
    label "zdarzenie_si&#281;"
  ]
  node [
    id 116
    label "znalezienie_si&#281;"
  ]
  node [
    id 117
    label "element"
  ]
  node [
    id 118
    label "completion"
  ]
  node [
    id 119
    label "bash"
  ]
  node [
    id 120
    label "umieszczenie"
  ]
  node [
    id 121
    label "rubryka"
  ]
  node [
    id 122
    label "ziszczenie_si&#281;"
  ]
  node [
    id 123
    label "nasilenie_si&#281;"
  ]
  node [
    id 124
    label "poczucie"
  ]
  node [
    id 125
    label "performance"
  ]
  node [
    id 126
    label "zrobienie"
  ]
  node [
    id 127
    label "zadowolony"
  ]
  node [
    id 128
    label "pomy&#347;lny"
  ]
  node [
    id 129
    label "udany"
  ]
  node [
    id 130
    label "szcz&#281;&#347;liwie"
  ]
  node [
    id 131
    label "pogodny"
  ]
  node [
    id 132
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 133
    label "return"
  ]
  node [
    id 134
    label "realizowa&#263;_si&#281;"
  ]
  node [
    id 135
    label "emocja"
  ]
  node [
    id 136
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 137
    label "realizowanie_si&#281;"
  ]
  node [
    id 138
    label "enjoyment"
  ]
  node [
    id 139
    label "gratyfikacja"
  ]
  node [
    id 140
    label "generalizowa&#263;"
  ]
  node [
    id 141
    label "obiektywny"
  ]
  node [
    id 142
    label "surowy"
  ]
  node [
    id 143
    label "okrutny"
  ]
  node [
    id 144
    label "bezwzgl&#281;dnie"
  ]
  node [
    id 145
    label "bezsporny"
  ]
  node [
    id 146
    label "jednoznaczny"
  ]
  node [
    id 147
    label "partia"
  ]
  node [
    id 148
    label "demokratyzm"
  ]
  node [
    id 149
    label "ustr&#243;j"
  ]
  node [
    id 150
    label "pluralizm"
  ]
  node [
    id 151
    label "porz&#261;dek"
  ]
  node [
    id 152
    label "podstawa"
  ]
  node [
    id 153
    label "cia&#322;o"
  ]
  node [
    id 154
    label "Bund"
  ]
  node [
    id 155
    label "PPR"
  ]
  node [
    id 156
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 157
    label "wybranek"
  ]
  node [
    id 158
    label "Jakobici"
  ]
  node [
    id 159
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 160
    label "SLD"
  ]
  node [
    id 161
    label "Razem"
  ]
  node [
    id 162
    label "PiS"
  ]
  node [
    id 163
    label "package"
  ]
  node [
    id 164
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 165
    label "Kuomintang"
  ]
  node [
    id 166
    label "ZSL"
  ]
  node [
    id 167
    label "organizacja"
  ]
  node [
    id 168
    label "AWS"
  ]
  node [
    id 169
    label "gra"
  ]
  node [
    id 170
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 171
    label "game"
  ]
  node [
    id 172
    label "grupa"
  ]
  node [
    id 173
    label "blok"
  ]
  node [
    id 174
    label "materia&#322;"
  ]
  node [
    id 175
    label "PO"
  ]
  node [
    id 176
    label "si&#322;a"
  ]
  node [
    id 177
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 178
    label "niedoczas"
  ]
  node [
    id 179
    label "Federali&#347;ci"
  ]
  node [
    id 180
    label "PSL"
  ]
  node [
    id 181
    label "Wigowie"
  ]
  node [
    id 182
    label "ZChN"
  ]
  node [
    id 183
    label "egzekutywa"
  ]
  node [
    id 184
    label "aktyw"
  ]
  node [
    id 185
    label "wybranka"
  ]
  node [
    id 186
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 187
    label "unit"
  ]
  node [
    id 188
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 189
    label "koncepcja"
  ]
  node [
    id 190
    label "zasada"
  ]
  node [
    id 191
    label "pluralism"
  ]
  node [
    id 192
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 193
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 194
    label "Katar"
  ]
  node [
    id 195
    label "Mazowsze"
  ]
  node [
    id 196
    label "Libia"
  ]
  node [
    id 197
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 198
    label "Gwatemala"
  ]
  node [
    id 199
    label "Anglia"
  ]
  node [
    id 200
    label "Amazonia"
  ]
  node [
    id 201
    label "Ekwador"
  ]
  node [
    id 202
    label "Afganistan"
  ]
  node [
    id 203
    label "Bordeaux"
  ]
  node [
    id 204
    label "Tad&#380;ykistan"
  ]
  node [
    id 205
    label "Bhutan"
  ]
  node [
    id 206
    label "Argentyna"
  ]
  node [
    id 207
    label "D&#380;ibuti"
  ]
  node [
    id 208
    label "Wenezuela"
  ]
  node [
    id 209
    label "Gabon"
  ]
  node [
    id 210
    label "Ukraina"
  ]
  node [
    id 211
    label "Naddniestrze"
  ]
  node [
    id 212
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 213
    label "Europa_Zachodnia"
  ]
  node [
    id 214
    label "Armagnac"
  ]
  node [
    id 215
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 216
    label "Rwanda"
  ]
  node [
    id 217
    label "Liechtenstein"
  ]
  node [
    id 218
    label "Amhara"
  ]
  node [
    id 219
    label "Sri_Lanka"
  ]
  node [
    id 220
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 221
    label "Zamojszczyzna"
  ]
  node [
    id 222
    label "Madagaskar"
  ]
  node [
    id 223
    label "Kongo"
  ]
  node [
    id 224
    label "Tonga"
  ]
  node [
    id 225
    label "Bangladesz"
  ]
  node [
    id 226
    label "Kanada"
  ]
  node [
    id 227
    label "Turkiestan"
  ]
  node [
    id 228
    label "Wehrlen"
  ]
  node [
    id 229
    label "Ma&#322;opolska"
  ]
  node [
    id 230
    label "Algieria"
  ]
  node [
    id 231
    label "Noworosja"
  ]
  node [
    id 232
    label "Uganda"
  ]
  node [
    id 233
    label "Surinam"
  ]
  node [
    id 234
    label "Sahara_Zachodnia"
  ]
  node [
    id 235
    label "Chile"
  ]
  node [
    id 236
    label "Lubelszczyzna"
  ]
  node [
    id 237
    label "W&#281;gry"
  ]
  node [
    id 238
    label "Mezoameryka"
  ]
  node [
    id 239
    label "Birma"
  ]
  node [
    id 240
    label "Ba&#322;kany"
  ]
  node [
    id 241
    label "Kurdystan"
  ]
  node [
    id 242
    label "Kazachstan"
  ]
  node [
    id 243
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 244
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 245
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 246
    label "Armenia"
  ]
  node [
    id 247
    label "Tuwalu"
  ]
  node [
    id 248
    label "Timor_Wschodni"
  ]
  node [
    id 249
    label "Baszkiria"
  ]
  node [
    id 250
    label "Szkocja"
  ]
  node [
    id 251
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 252
    label "Tonkin"
  ]
  node [
    id 253
    label "Maghreb"
  ]
  node [
    id 254
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 255
    label "Izrael"
  ]
  node [
    id 256
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 257
    label "Nadrenia"
  ]
  node [
    id 258
    label "Estonia"
  ]
  node [
    id 259
    label "Komory"
  ]
  node [
    id 260
    label "Podhale"
  ]
  node [
    id 261
    label "Wielkopolska"
  ]
  node [
    id 262
    label "Zabajkale"
  ]
  node [
    id 263
    label "Kamerun"
  ]
  node [
    id 264
    label "Haiti"
  ]
  node [
    id 265
    label "Belize"
  ]
  node [
    id 266
    label "Sierra_Leone"
  ]
  node [
    id 267
    label "Apulia"
  ]
  node [
    id 268
    label "Luksemburg"
  ]
  node [
    id 269
    label "brzeg"
  ]
  node [
    id 270
    label "USA"
  ]
  node [
    id 271
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 272
    label "Barbados"
  ]
  node [
    id 273
    label "San_Marino"
  ]
  node [
    id 274
    label "Bu&#322;garia"
  ]
  node [
    id 275
    label "Indonezja"
  ]
  node [
    id 276
    label "Wietnam"
  ]
  node [
    id 277
    label "Bojkowszczyzna"
  ]
  node [
    id 278
    label "Malawi"
  ]
  node [
    id 279
    label "Francja"
  ]
  node [
    id 280
    label "Zambia"
  ]
  node [
    id 281
    label "Kujawy"
  ]
  node [
    id 282
    label "Angola"
  ]
  node [
    id 283
    label "Liguria"
  ]
  node [
    id 284
    label "Grenada"
  ]
  node [
    id 285
    label "Pamir"
  ]
  node [
    id 286
    label "Nepal"
  ]
  node [
    id 287
    label "Panama"
  ]
  node [
    id 288
    label "Rumunia"
  ]
  node [
    id 289
    label "Indochiny"
  ]
  node [
    id 290
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 291
    label "Polinezja"
  ]
  node [
    id 292
    label "Kurpie"
  ]
  node [
    id 293
    label "Podlasie"
  ]
  node [
    id 294
    label "S&#261;decczyzna"
  ]
  node [
    id 295
    label "Umbria"
  ]
  node [
    id 296
    label "Czarnog&#243;ra"
  ]
  node [
    id 297
    label "Malediwy"
  ]
  node [
    id 298
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 299
    label "S&#322;owacja"
  ]
  node [
    id 300
    label "Karaiby"
  ]
  node [
    id 301
    label "Ukraina_Zachodnia"
  ]
  node [
    id 302
    label "Kielecczyzna"
  ]
  node [
    id 303
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 304
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 305
    label "Egipt"
  ]
  node [
    id 306
    label "Kalabria"
  ]
  node [
    id 307
    label "Kolumbia"
  ]
  node [
    id 308
    label "Mozambik"
  ]
  node [
    id 309
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 310
    label "Laos"
  ]
  node [
    id 311
    label "Burundi"
  ]
  node [
    id 312
    label "Suazi"
  ]
  node [
    id 313
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 314
    label "Czechy"
  ]
  node [
    id 315
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 316
    label "Wyspy_Marshalla"
  ]
  node [
    id 317
    label "Dominika"
  ]
  node [
    id 318
    label "Trynidad_i_Tobago"
  ]
  node [
    id 319
    label "Syria"
  ]
  node [
    id 320
    label "Palau"
  ]
  node [
    id 321
    label "Skandynawia"
  ]
  node [
    id 322
    label "Gwinea_Bissau"
  ]
  node [
    id 323
    label "Liberia"
  ]
  node [
    id 324
    label "Jamajka"
  ]
  node [
    id 325
    label "Zimbabwe"
  ]
  node [
    id 326
    label "Polska"
  ]
  node [
    id 327
    label "Bory_Tucholskie"
  ]
  node [
    id 328
    label "Huculszczyzna"
  ]
  node [
    id 329
    label "Tyrol"
  ]
  node [
    id 330
    label "Turyngia"
  ]
  node [
    id 331
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 332
    label "Dominikana"
  ]
  node [
    id 333
    label "Senegal"
  ]
  node [
    id 334
    label "Togo"
  ]
  node [
    id 335
    label "Gujana"
  ]
  node [
    id 336
    label "jednostka_administracyjna"
  ]
  node [
    id 337
    label "Albania"
  ]
  node [
    id 338
    label "Zair"
  ]
  node [
    id 339
    label "Meksyk"
  ]
  node [
    id 340
    label "Gruzja"
  ]
  node [
    id 341
    label "Macedonia"
  ]
  node [
    id 342
    label "Kambod&#380;a"
  ]
  node [
    id 343
    label "Chorwacja"
  ]
  node [
    id 344
    label "Monako"
  ]
  node [
    id 345
    label "Mauritius"
  ]
  node [
    id 346
    label "Gwinea"
  ]
  node [
    id 347
    label "Mali"
  ]
  node [
    id 348
    label "Nigeria"
  ]
  node [
    id 349
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 350
    label "Hercegowina"
  ]
  node [
    id 351
    label "Kostaryka"
  ]
  node [
    id 352
    label "Lotaryngia"
  ]
  node [
    id 353
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 354
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 355
    label "Hanower"
  ]
  node [
    id 356
    label "Paragwaj"
  ]
  node [
    id 357
    label "W&#322;ochy"
  ]
  node [
    id 358
    label "Seszele"
  ]
  node [
    id 359
    label "Wyspy_Salomona"
  ]
  node [
    id 360
    label "Hiszpania"
  ]
  node [
    id 361
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 362
    label "Walia"
  ]
  node [
    id 363
    label "Boliwia"
  ]
  node [
    id 364
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 365
    label "Opolskie"
  ]
  node [
    id 366
    label "Kirgistan"
  ]
  node [
    id 367
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 368
    label "Irlandia"
  ]
  node [
    id 369
    label "Kampania"
  ]
  node [
    id 370
    label "Czad"
  ]
  node [
    id 371
    label "Irak"
  ]
  node [
    id 372
    label "Lesoto"
  ]
  node [
    id 373
    label "Malta"
  ]
  node [
    id 374
    label "Andora"
  ]
  node [
    id 375
    label "Sand&#380;ak"
  ]
  node [
    id 376
    label "Chiny"
  ]
  node [
    id 377
    label "Filipiny"
  ]
  node [
    id 378
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 379
    label "Syjon"
  ]
  node [
    id 380
    label "Niemcy"
  ]
  node [
    id 381
    label "Kabylia"
  ]
  node [
    id 382
    label "Lombardia"
  ]
  node [
    id 383
    label "Warmia"
  ]
  node [
    id 384
    label "Nikaragua"
  ]
  node [
    id 385
    label "Pakistan"
  ]
  node [
    id 386
    label "Brazylia"
  ]
  node [
    id 387
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 388
    label "Kaszmir"
  ]
  node [
    id 389
    label "Maroko"
  ]
  node [
    id 390
    label "Portugalia"
  ]
  node [
    id 391
    label "Niger"
  ]
  node [
    id 392
    label "Kenia"
  ]
  node [
    id 393
    label "Botswana"
  ]
  node [
    id 394
    label "Fid&#380;i"
  ]
  node [
    id 395
    label "Tunezja"
  ]
  node [
    id 396
    label "Australia"
  ]
  node [
    id 397
    label "Tajlandia"
  ]
  node [
    id 398
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 399
    label "&#321;&#243;dzkie"
  ]
  node [
    id 400
    label "Kaukaz"
  ]
  node [
    id 401
    label "Burkina_Faso"
  ]
  node [
    id 402
    label "Tanzania"
  ]
  node [
    id 403
    label "Benin"
  ]
  node [
    id 404
    label "Europa_Wschodnia"
  ]
  node [
    id 405
    label "interior"
  ]
  node [
    id 406
    label "Indie"
  ]
  node [
    id 407
    label "&#321;otwa"
  ]
  node [
    id 408
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 409
    label "Biskupizna"
  ]
  node [
    id 410
    label "Kiribati"
  ]
  node [
    id 411
    label "Antigua_i_Barbuda"
  ]
  node [
    id 412
    label "Rodezja"
  ]
  node [
    id 413
    label "Afryka_Wschodnia"
  ]
  node [
    id 414
    label "Cypr"
  ]
  node [
    id 415
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 416
    label "Podkarpacie"
  ]
  node [
    id 417
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 418
    label "obszar"
  ]
  node [
    id 419
    label "Peru"
  ]
  node [
    id 420
    label "Afryka_Zachodnia"
  ]
  node [
    id 421
    label "Toskania"
  ]
  node [
    id 422
    label "Austria"
  ]
  node [
    id 423
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 424
    label "Urugwaj"
  ]
  node [
    id 425
    label "Podbeskidzie"
  ]
  node [
    id 426
    label "Jordania"
  ]
  node [
    id 427
    label "Bo&#347;nia"
  ]
  node [
    id 428
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 429
    label "Grecja"
  ]
  node [
    id 430
    label "Azerbejd&#380;an"
  ]
  node [
    id 431
    label "Oceania"
  ]
  node [
    id 432
    label "Turcja"
  ]
  node [
    id 433
    label "Pomorze_Zachodnie"
  ]
  node [
    id 434
    label "Samoa"
  ]
  node [
    id 435
    label "Powi&#347;le"
  ]
  node [
    id 436
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 437
    label "ziemia"
  ]
  node [
    id 438
    label "Sudan"
  ]
  node [
    id 439
    label "Oman"
  ]
  node [
    id 440
    label "&#321;emkowszczyzna"
  ]
  node [
    id 441
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 442
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 443
    label "Uzbekistan"
  ]
  node [
    id 444
    label "Portoryko"
  ]
  node [
    id 445
    label "Honduras"
  ]
  node [
    id 446
    label "Mongolia"
  ]
  node [
    id 447
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 448
    label "Kaszuby"
  ]
  node [
    id 449
    label "Ko&#322;yma"
  ]
  node [
    id 450
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 451
    label "Szlezwik"
  ]
  node [
    id 452
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 453
    label "Serbia"
  ]
  node [
    id 454
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 455
    label "Tajwan"
  ]
  node [
    id 456
    label "Wielka_Brytania"
  ]
  node [
    id 457
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 458
    label "Liban"
  ]
  node [
    id 459
    label "Japonia"
  ]
  node [
    id 460
    label "Ghana"
  ]
  node [
    id 461
    label "Belgia"
  ]
  node [
    id 462
    label "Bahrajn"
  ]
  node [
    id 463
    label "Mikronezja"
  ]
  node [
    id 464
    label "Etiopia"
  ]
  node [
    id 465
    label "Polesie"
  ]
  node [
    id 466
    label "Kuwejt"
  ]
  node [
    id 467
    label "Kerala"
  ]
  node [
    id 468
    label "Mazury"
  ]
  node [
    id 469
    label "Bahamy"
  ]
  node [
    id 470
    label "Rosja"
  ]
  node [
    id 471
    label "Mo&#322;dawia"
  ]
  node [
    id 472
    label "Palestyna"
  ]
  node [
    id 473
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 474
    label "Lauda"
  ]
  node [
    id 475
    label "Azja_Wschodnia"
  ]
  node [
    id 476
    label "Litwa"
  ]
  node [
    id 477
    label "S&#322;owenia"
  ]
  node [
    id 478
    label "Szwajcaria"
  ]
  node [
    id 479
    label "Erytrea"
  ]
  node [
    id 480
    label "Zakarpacie"
  ]
  node [
    id 481
    label "Arabia_Saudyjska"
  ]
  node [
    id 482
    label "Kuba"
  ]
  node [
    id 483
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 484
    label "Galicja"
  ]
  node [
    id 485
    label "Lubuskie"
  ]
  node [
    id 486
    label "Laponia"
  ]
  node [
    id 487
    label "granica_pa&#324;stwa"
  ]
  node [
    id 488
    label "Malezja"
  ]
  node [
    id 489
    label "Korea"
  ]
  node [
    id 490
    label "Yorkshire"
  ]
  node [
    id 491
    label "Bawaria"
  ]
  node [
    id 492
    label "Zag&#243;rze"
  ]
  node [
    id 493
    label "Jemen"
  ]
  node [
    id 494
    label "Nowa_Zelandia"
  ]
  node [
    id 495
    label "Andaluzja"
  ]
  node [
    id 496
    label "Namibia"
  ]
  node [
    id 497
    label "Nauru"
  ]
  node [
    id 498
    label "&#379;ywiecczyzna"
  ]
  node [
    id 499
    label "Brunei"
  ]
  node [
    id 500
    label "Oksytania"
  ]
  node [
    id 501
    label "Opolszczyzna"
  ]
  node [
    id 502
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 503
    label "Kociewie"
  ]
  node [
    id 504
    label "Khitai"
  ]
  node [
    id 505
    label "Mauretania"
  ]
  node [
    id 506
    label "Iran"
  ]
  node [
    id 507
    label "Gambia"
  ]
  node [
    id 508
    label "Somalia"
  ]
  node [
    id 509
    label "Holandia"
  ]
  node [
    id 510
    label "Lasko"
  ]
  node [
    id 511
    label "Turkmenistan"
  ]
  node [
    id 512
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 513
    label "Salwador"
  ]
  node [
    id 514
    label "woda"
  ]
  node [
    id 515
    label "zbi&#243;r"
  ]
  node [
    id 516
    label "ekoton"
  ]
  node [
    id 517
    label "str&#261;d"
  ]
  node [
    id 518
    label "koniec"
  ]
  node [
    id 519
    label "plantowa&#263;"
  ]
  node [
    id 520
    label "zapadnia"
  ]
  node [
    id 521
    label "budynek"
  ]
  node [
    id 522
    label "skorupa_ziemska"
  ]
  node [
    id 523
    label "glinowanie"
  ]
  node [
    id 524
    label "martwica"
  ]
  node [
    id 525
    label "teren"
  ]
  node [
    id 526
    label "litosfera"
  ]
  node [
    id 527
    label "penetrator"
  ]
  node [
    id 528
    label "glinowa&#263;"
  ]
  node [
    id 529
    label "domain"
  ]
  node [
    id 530
    label "podglebie"
  ]
  node [
    id 531
    label "kompleks_sorpcyjny"
  ]
  node [
    id 532
    label "miejsce"
  ]
  node [
    id 533
    label "kort"
  ]
  node [
    id 534
    label "czynnik_produkcji"
  ]
  node [
    id 535
    label "pojazd"
  ]
  node [
    id 536
    label "powierzchnia"
  ]
  node [
    id 537
    label "pr&#243;chnica"
  ]
  node [
    id 538
    label "pomieszczenie"
  ]
  node [
    id 539
    label "ryzosfera"
  ]
  node [
    id 540
    label "p&#322;aszczyzna"
  ]
  node [
    id 541
    label "dotleni&#263;"
  ]
  node [
    id 542
    label "glej"
  ]
  node [
    id 543
    label "pa&#324;stwo"
  ]
  node [
    id 544
    label "posadzka"
  ]
  node [
    id 545
    label "geosystem"
  ]
  node [
    id 546
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 547
    label "podmiot"
  ]
  node [
    id 548
    label "jednostka_organizacyjna"
  ]
  node [
    id 549
    label "struktura"
  ]
  node [
    id 550
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 551
    label "TOPR"
  ]
  node [
    id 552
    label "endecki"
  ]
  node [
    id 553
    label "zesp&#243;&#322;"
  ]
  node [
    id 554
    label "od&#322;am"
  ]
  node [
    id 555
    label "przedstawicielstwo"
  ]
  node [
    id 556
    label "Cepelia"
  ]
  node [
    id 557
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 558
    label "ZBoWiD"
  ]
  node [
    id 559
    label "organization"
  ]
  node [
    id 560
    label "centrala"
  ]
  node [
    id 561
    label "GOPR"
  ]
  node [
    id 562
    label "ZOMO"
  ]
  node [
    id 563
    label "ZMP"
  ]
  node [
    id 564
    label "komitet_koordynacyjny"
  ]
  node [
    id 565
    label "przybud&#243;wka"
  ]
  node [
    id 566
    label "boj&#243;wka"
  ]
  node [
    id 567
    label "p&#243;&#322;noc"
  ]
  node [
    id 568
    label "Kosowo"
  ]
  node [
    id 569
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 570
    label "Zab&#322;ocie"
  ]
  node [
    id 571
    label "zach&#243;d"
  ]
  node [
    id 572
    label "po&#322;udnie"
  ]
  node [
    id 573
    label "Pow&#261;zki"
  ]
  node [
    id 574
    label "Piotrowo"
  ]
  node [
    id 575
    label "Olszanica"
  ]
  node [
    id 576
    label "holarktyka"
  ]
  node [
    id 577
    label "Ruda_Pabianicka"
  ]
  node [
    id 578
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 579
    label "Ludwin&#243;w"
  ]
  node [
    id 580
    label "Arktyka"
  ]
  node [
    id 581
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 582
    label "Zabu&#380;e"
  ]
  node [
    id 583
    label "antroposfera"
  ]
  node [
    id 584
    label "terytorium"
  ]
  node [
    id 585
    label "Neogea"
  ]
  node [
    id 586
    label "Syberia_Zachodnia"
  ]
  node [
    id 587
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 588
    label "zakres"
  ]
  node [
    id 589
    label "pas_planetoid"
  ]
  node [
    id 590
    label "Syberia_Wschodnia"
  ]
  node [
    id 591
    label "Antarktyka"
  ]
  node [
    id 592
    label "Rakowice"
  ]
  node [
    id 593
    label "akrecja"
  ]
  node [
    id 594
    label "wymiar"
  ]
  node [
    id 595
    label "&#321;&#281;g"
  ]
  node [
    id 596
    label "Kresy_Zachodnie"
  ]
  node [
    id 597
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 598
    label "wsch&#243;d"
  ]
  node [
    id 599
    label "Notogea"
  ]
  node [
    id 600
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 601
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 602
    label "Pend&#380;ab"
  ]
  node [
    id 603
    label "funt_liba&#324;ski"
  ]
  node [
    id 604
    label "strefa_euro"
  ]
  node [
    id 605
    label "Pozna&#324;"
  ]
  node [
    id 606
    label "lira_malta&#324;ska"
  ]
  node [
    id 607
    label "Gozo"
  ]
  node [
    id 608
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 609
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 610
    label "dolar_namibijski"
  ]
  node [
    id 611
    label "milrejs"
  ]
  node [
    id 612
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 613
    label "NATO"
  ]
  node [
    id 614
    label "escudo_portugalskie"
  ]
  node [
    id 615
    label "dolar_bahamski"
  ]
  node [
    id 616
    label "Wielka_Bahama"
  ]
  node [
    id 617
    label "dolar_liberyjski"
  ]
  node [
    id 618
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 619
    label "riel"
  ]
  node [
    id 620
    label "Karelia"
  ]
  node [
    id 621
    label "Mari_El"
  ]
  node [
    id 622
    label "Inguszetia"
  ]
  node [
    id 623
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 624
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 625
    label "Udmurcja"
  ]
  node [
    id 626
    label "Newa"
  ]
  node [
    id 627
    label "&#321;adoga"
  ]
  node [
    id 628
    label "Czeczenia"
  ]
  node [
    id 629
    label "Anadyr"
  ]
  node [
    id 630
    label "Syberia"
  ]
  node [
    id 631
    label "Tatarstan"
  ]
  node [
    id 632
    label "Wszechrosja"
  ]
  node [
    id 633
    label "Azja"
  ]
  node [
    id 634
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 635
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 636
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 637
    label "Witim"
  ]
  node [
    id 638
    label "Kamczatka"
  ]
  node [
    id 639
    label "Jama&#322;"
  ]
  node [
    id 640
    label "Dagestan"
  ]
  node [
    id 641
    label "Tuwa"
  ]
  node [
    id 642
    label "car"
  ]
  node [
    id 643
    label "Komi"
  ]
  node [
    id 644
    label "Czuwaszja"
  ]
  node [
    id 645
    label "Chakasja"
  ]
  node [
    id 646
    label "Perm"
  ]
  node [
    id 647
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 648
    label "Ajon"
  ]
  node [
    id 649
    label "Adygeja"
  ]
  node [
    id 650
    label "Dniepr"
  ]
  node [
    id 651
    label "rubel_rosyjski"
  ]
  node [
    id 652
    label "Don"
  ]
  node [
    id 653
    label "Mordowia"
  ]
  node [
    id 654
    label "s&#322;owianofilstwo"
  ]
  node [
    id 655
    label "lew"
  ]
  node [
    id 656
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 657
    label "Dobrud&#380;a"
  ]
  node [
    id 658
    label "Unia_Europejska"
  ]
  node [
    id 659
    label "lira_izraelska"
  ]
  node [
    id 660
    label "szekel"
  ]
  node [
    id 661
    label "Galilea"
  ]
  node [
    id 662
    label "Judea"
  ]
  node [
    id 663
    label "Luksemburgia"
  ]
  node [
    id 664
    label "frank_belgijski"
  ]
  node [
    id 665
    label "Limburgia"
  ]
  node [
    id 666
    label "Walonia"
  ]
  node [
    id 667
    label "Brabancja"
  ]
  node [
    id 668
    label "Flandria"
  ]
  node [
    id 669
    label "Niderlandy"
  ]
  node [
    id 670
    label "dinar_iracki"
  ]
  node [
    id 671
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 672
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 673
    label "szyling_ugandyjski"
  ]
  node [
    id 674
    label "dolar_jamajski"
  ]
  node [
    id 675
    label "kafar"
  ]
  node [
    id 676
    label "ringgit"
  ]
  node [
    id 677
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 678
    label "Borneo"
  ]
  node [
    id 679
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 680
    label "dolar_surinamski"
  ]
  node [
    id 681
    label "funt_suda&#324;ski"
  ]
  node [
    id 682
    label "dolar_guja&#324;ski"
  ]
  node [
    id 683
    label "Manica"
  ]
  node [
    id 684
    label "escudo_mozambickie"
  ]
  node [
    id 685
    label "Cabo_Delgado"
  ]
  node [
    id 686
    label "Inhambane"
  ]
  node [
    id 687
    label "Maputo"
  ]
  node [
    id 688
    label "Gaza"
  ]
  node [
    id 689
    label "Niasa"
  ]
  node [
    id 690
    label "Nampula"
  ]
  node [
    id 691
    label "metical"
  ]
  node [
    id 692
    label "Sahara"
  ]
  node [
    id 693
    label "inti"
  ]
  node [
    id 694
    label "sol"
  ]
  node [
    id 695
    label "kip"
  ]
  node [
    id 696
    label "Pireneje"
  ]
  node [
    id 697
    label "euro"
  ]
  node [
    id 698
    label "kwacha_zambijska"
  ]
  node [
    id 699
    label "Buriaci"
  ]
  node [
    id 700
    label "tugrik"
  ]
  node [
    id 701
    label "ajmak"
  ]
  node [
    id 702
    label "balboa"
  ]
  node [
    id 703
    label "Ameryka_Centralna"
  ]
  node [
    id 704
    label "dolar"
  ]
  node [
    id 705
    label "gulden"
  ]
  node [
    id 706
    label "Zelandia"
  ]
  node [
    id 707
    label "manat_turkme&#324;ski"
  ]
  node [
    id 708
    label "dolar_Tuvalu"
  ]
  node [
    id 709
    label "zair"
  ]
  node [
    id 710
    label "Katanga"
  ]
  node [
    id 711
    label "frank_szwajcarski"
  ]
  node [
    id 712
    label "Jukatan"
  ]
  node [
    id 713
    label "dolar_Belize"
  ]
  node [
    id 714
    label "colon"
  ]
  node [
    id 715
    label "Dyja"
  ]
  node [
    id 716
    label "korona_czeska"
  ]
  node [
    id 717
    label "Izera"
  ]
  node [
    id 718
    label "ugija"
  ]
  node [
    id 719
    label "szyling_kenijski"
  ]
  node [
    id 720
    label "Nachiczewan"
  ]
  node [
    id 721
    label "manat_azerski"
  ]
  node [
    id 722
    label "Karabach"
  ]
  node [
    id 723
    label "Bengal"
  ]
  node [
    id 724
    label "taka"
  ]
  node [
    id 725
    label "Ocean_Spokojny"
  ]
  node [
    id 726
    label "dolar_Kiribati"
  ]
  node [
    id 727
    label "peso_filipi&#324;skie"
  ]
  node [
    id 728
    label "Cebu"
  ]
  node [
    id 729
    label "Atlantyk"
  ]
  node [
    id 730
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 731
    label "Ulster"
  ]
  node [
    id 732
    label "funt_irlandzki"
  ]
  node [
    id 733
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 734
    label "cedi"
  ]
  node [
    id 735
    label "ariary"
  ]
  node [
    id 736
    label "Ocean_Indyjski"
  ]
  node [
    id 737
    label "frank_malgaski"
  ]
  node [
    id 738
    label "Estremadura"
  ]
  node [
    id 739
    label "Kastylia"
  ]
  node [
    id 740
    label "Rzym_Zachodni"
  ]
  node [
    id 741
    label "Aragonia"
  ]
  node [
    id 742
    label "hacjender"
  ]
  node [
    id 743
    label "Asturia"
  ]
  node [
    id 744
    label "Baskonia"
  ]
  node [
    id 745
    label "Majorka"
  ]
  node [
    id 746
    label "Walencja"
  ]
  node [
    id 747
    label "peseta"
  ]
  node [
    id 748
    label "Katalonia"
  ]
  node [
    id 749
    label "peso_chilijskie"
  ]
  node [
    id 750
    label "Indie_Zachodnie"
  ]
  node [
    id 751
    label "Sikkim"
  ]
  node [
    id 752
    label "Asam"
  ]
  node [
    id 753
    label "rupia_indyjska"
  ]
  node [
    id 754
    label "Indie_Portugalskie"
  ]
  node [
    id 755
    label "Indie_Wschodnie"
  ]
  node [
    id 756
    label "Bollywood"
  ]
  node [
    id 757
    label "jen"
  ]
  node [
    id 758
    label "jinja"
  ]
  node [
    id 759
    label "Okinawa"
  ]
  node [
    id 760
    label "Japonica"
  ]
  node [
    id 761
    label "Rugia"
  ]
  node [
    id 762
    label "Saksonia"
  ]
  node [
    id 763
    label "Dolna_Saksonia"
  ]
  node [
    id 764
    label "Anglosas"
  ]
  node [
    id 765
    label "Hesja"
  ]
  node [
    id 766
    label "Wirtembergia"
  ]
  node [
    id 767
    label "Po&#322;abie"
  ]
  node [
    id 768
    label "Germania"
  ]
  node [
    id 769
    label "Frankonia"
  ]
  node [
    id 770
    label "Badenia"
  ]
  node [
    id 771
    label "Holsztyn"
  ]
  node [
    id 772
    label "marka"
  ]
  node [
    id 773
    label "Brandenburgia"
  ]
  node [
    id 774
    label "Szwabia"
  ]
  node [
    id 775
    label "Niemcy_Zachodnie"
  ]
  node [
    id 776
    label "Westfalia"
  ]
  node [
    id 777
    label "Helgoland"
  ]
  node [
    id 778
    label "Karlsbad"
  ]
  node [
    id 779
    label "Niemcy_Wschodnie"
  ]
  node [
    id 780
    label "Piemont"
  ]
  node [
    id 781
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 782
    label "Italia"
  ]
  node [
    id 783
    label "Sardynia"
  ]
  node [
    id 784
    label "Ok&#281;cie"
  ]
  node [
    id 785
    label "Karyntia"
  ]
  node [
    id 786
    label "Romania"
  ]
  node [
    id 787
    label "Sycylia"
  ]
  node [
    id 788
    label "Warszawa"
  ]
  node [
    id 789
    label "lir"
  ]
  node [
    id 790
    label "Dacja"
  ]
  node [
    id 791
    label "lej_rumu&#324;ski"
  ]
  node [
    id 792
    label "Siedmiogr&#243;d"
  ]
  node [
    id 793
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 794
    label "funt_syryjski"
  ]
  node [
    id 795
    label "alawizm"
  ]
  node [
    id 796
    label "frank_rwandyjski"
  ]
  node [
    id 797
    label "dinar_Bahrajnu"
  ]
  node [
    id 798
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 799
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 800
    label "frank_luksemburski"
  ]
  node [
    id 801
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 802
    label "peso_kuba&#324;skie"
  ]
  node [
    id 803
    label "frank_monakijski"
  ]
  node [
    id 804
    label "dinar_algierski"
  ]
  node [
    id 805
    label "Wojwodina"
  ]
  node [
    id 806
    label "dinar_serbski"
  ]
  node [
    id 807
    label "Orinoko"
  ]
  node [
    id 808
    label "boliwar"
  ]
  node [
    id 809
    label "tenge"
  ]
  node [
    id 810
    label "para"
  ]
  node [
    id 811
    label "lek"
  ]
  node [
    id 812
    label "frank_alba&#324;ski"
  ]
  node [
    id 813
    label "dolar_Barbadosu"
  ]
  node [
    id 814
    label "Antyle"
  ]
  node [
    id 815
    label "kyat"
  ]
  node [
    id 816
    label "Arakan"
  ]
  node [
    id 817
    label "c&#243;rdoba"
  ]
  node [
    id 818
    label "Paros"
  ]
  node [
    id 819
    label "Epir"
  ]
  node [
    id 820
    label "panhellenizm"
  ]
  node [
    id 821
    label "Eubea"
  ]
  node [
    id 822
    label "Rodos"
  ]
  node [
    id 823
    label "Achaja"
  ]
  node [
    id 824
    label "Termopile"
  ]
  node [
    id 825
    label "Attyka"
  ]
  node [
    id 826
    label "Hellada"
  ]
  node [
    id 827
    label "Etolia"
  ]
  node [
    id 828
    label "palestra"
  ]
  node [
    id 829
    label "Kreta"
  ]
  node [
    id 830
    label "drachma"
  ]
  node [
    id 831
    label "Olimp"
  ]
  node [
    id 832
    label "Tesalia"
  ]
  node [
    id 833
    label "Peloponez"
  ]
  node [
    id 834
    label "Eolia"
  ]
  node [
    id 835
    label "Beocja"
  ]
  node [
    id 836
    label "Parnas"
  ]
  node [
    id 837
    label "Lesbos"
  ]
  node [
    id 838
    label "Mariany"
  ]
  node [
    id 839
    label "Salzburg"
  ]
  node [
    id 840
    label "Rakuzy"
  ]
  node [
    id 841
    label "konsulent"
  ]
  node [
    id 842
    label "szyling_austryjacki"
  ]
  node [
    id 843
    label "birr"
  ]
  node [
    id 844
    label "negus"
  ]
  node [
    id 845
    label "Jawa"
  ]
  node [
    id 846
    label "Sumatra"
  ]
  node [
    id 847
    label "rupia_indonezyjska"
  ]
  node [
    id 848
    label "Nowa_Gwinea"
  ]
  node [
    id 849
    label "Moluki"
  ]
  node [
    id 850
    label "boliviano"
  ]
  node [
    id 851
    label "Pikardia"
  ]
  node [
    id 852
    label "Alzacja"
  ]
  node [
    id 853
    label "Masyw_Centralny"
  ]
  node [
    id 854
    label "Akwitania"
  ]
  node [
    id 855
    label "Sekwana"
  ]
  node [
    id 856
    label "Langwedocja"
  ]
  node [
    id 857
    label "Martynika"
  ]
  node [
    id 858
    label "Bretania"
  ]
  node [
    id 859
    label "Sabaudia"
  ]
  node [
    id 860
    label "Korsyka"
  ]
  node [
    id 861
    label "Normandia"
  ]
  node [
    id 862
    label "Gaskonia"
  ]
  node [
    id 863
    label "Burgundia"
  ]
  node [
    id 864
    label "frank_francuski"
  ]
  node [
    id 865
    label "Wandea"
  ]
  node [
    id 866
    label "Prowansja"
  ]
  node [
    id 867
    label "Gwadelupa"
  ]
  node [
    id 868
    label "somoni"
  ]
  node [
    id 869
    label "Melanezja"
  ]
  node [
    id 870
    label "dolar_Fid&#380;i"
  ]
  node [
    id 871
    label "funt_cypryjski"
  ]
  node [
    id 872
    label "Afrodyzje"
  ]
  node [
    id 873
    label "peso_dominika&#324;skie"
  ]
  node [
    id 874
    label "Fryburg"
  ]
  node [
    id 875
    label "Bazylea"
  ]
  node [
    id 876
    label "Alpy"
  ]
  node [
    id 877
    label "Helwecja"
  ]
  node [
    id 878
    label "Berno"
  ]
  node [
    id 879
    label "sum"
  ]
  node [
    id 880
    label "Karaka&#322;pacja"
  ]
  node [
    id 881
    label "Kurlandia"
  ]
  node [
    id 882
    label "Windawa"
  ]
  node [
    id 883
    label "&#322;at"
  ]
  node [
    id 884
    label "Liwonia"
  ]
  node [
    id 885
    label "rubel_&#322;otewski"
  ]
  node [
    id 886
    label "Inflanty"
  ]
  node [
    id 887
    label "Wile&#324;szczyzna"
  ]
  node [
    id 888
    label "&#379;mud&#378;"
  ]
  node [
    id 889
    label "lit"
  ]
  node [
    id 890
    label "frank_tunezyjski"
  ]
  node [
    id 891
    label "dinar_tunezyjski"
  ]
  node [
    id 892
    label "lempira"
  ]
  node [
    id 893
    label "korona_w&#281;gierska"
  ]
  node [
    id 894
    label "forint"
  ]
  node [
    id 895
    label "Lipt&#243;w"
  ]
  node [
    id 896
    label "dong"
  ]
  node [
    id 897
    label "Annam"
  ]
  node [
    id 898
    label "lud"
  ]
  node [
    id 899
    label "frank_kongijski"
  ]
  node [
    id 900
    label "szyling_somalijski"
  ]
  node [
    id 901
    label "cruzado"
  ]
  node [
    id 902
    label "real"
  ]
  node [
    id 903
    label "Podole"
  ]
  node [
    id 904
    label "Wsch&#243;d"
  ]
  node [
    id 905
    label "Naddnieprze"
  ]
  node [
    id 906
    label "Ma&#322;orosja"
  ]
  node [
    id 907
    label "Wo&#322;y&#324;"
  ]
  node [
    id 908
    label "Nadbu&#380;e"
  ]
  node [
    id 909
    label "hrywna"
  ]
  node [
    id 910
    label "Zaporo&#380;e"
  ]
  node [
    id 911
    label "Krym"
  ]
  node [
    id 912
    label "Dniestr"
  ]
  node [
    id 913
    label "Przykarpacie"
  ]
  node [
    id 914
    label "Kozaczyzna"
  ]
  node [
    id 915
    label "karbowaniec"
  ]
  node [
    id 916
    label "Tasmania"
  ]
  node [
    id 917
    label "Nowy_&#346;wiat"
  ]
  node [
    id 918
    label "dolar_australijski"
  ]
  node [
    id 919
    label "gourde"
  ]
  node [
    id 920
    label "escudo_angolskie"
  ]
  node [
    id 921
    label "kwanza"
  ]
  node [
    id 922
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 923
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 924
    label "Ad&#380;aria"
  ]
  node [
    id 925
    label "lari"
  ]
  node [
    id 926
    label "naira"
  ]
  node [
    id 927
    label "Ohio"
  ]
  node [
    id 928
    label "P&#243;&#322;noc"
  ]
  node [
    id 929
    label "Nowy_York"
  ]
  node [
    id 930
    label "Illinois"
  ]
  node [
    id 931
    label "Po&#322;udnie"
  ]
  node [
    id 932
    label "Kalifornia"
  ]
  node [
    id 933
    label "Wirginia"
  ]
  node [
    id 934
    label "Teksas"
  ]
  node [
    id 935
    label "Waszyngton"
  ]
  node [
    id 936
    label "zielona_karta"
  ]
  node [
    id 937
    label "Alaska"
  ]
  node [
    id 938
    label "Massachusetts"
  ]
  node [
    id 939
    label "Hawaje"
  ]
  node [
    id 940
    label "Maryland"
  ]
  node [
    id 941
    label "Michigan"
  ]
  node [
    id 942
    label "Arizona"
  ]
  node [
    id 943
    label "Georgia"
  ]
  node [
    id 944
    label "stan_wolny"
  ]
  node [
    id 945
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 946
    label "Pensylwania"
  ]
  node [
    id 947
    label "Luizjana"
  ]
  node [
    id 948
    label "Nowy_Meksyk"
  ]
  node [
    id 949
    label "Wuj_Sam"
  ]
  node [
    id 950
    label "Alabama"
  ]
  node [
    id 951
    label "Kansas"
  ]
  node [
    id 952
    label "Oregon"
  ]
  node [
    id 953
    label "Zach&#243;d"
  ]
  node [
    id 954
    label "Oklahoma"
  ]
  node [
    id 955
    label "Floryda"
  ]
  node [
    id 956
    label "Hudson"
  ]
  node [
    id 957
    label "som"
  ]
  node [
    id 958
    label "peso_urugwajskie"
  ]
  node [
    id 959
    label "denar_macedo&#324;ski"
  ]
  node [
    id 960
    label "dolar_Brunei"
  ]
  node [
    id 961
    label "rial_ira&#324;ski"
  ]
  node [
    id 962
    label "mu&#322;&#322;a"
  ]
  node [
    id 963
    label "Persja"
  ]
  node [
    id 964
    label "d&#380;amahirijja"
  ]
  node [
    id 965
    label "dinar_libijski"
  ]
  node [
    id 966
    label "nakfa"
  ]
  node [
    id 967
    label "rial_katarski"
  ]
  node [
    id 968
    label "quetzal"
  ]
  node [
    id 969
    label "won"
  ]
  node [
    id 970
    label "rial_jeme&#324;ski"
  ]
  node [
    id 971
    label "peso_argenty&#324;skie"
  ]
  node [
    id 972
    label "guarani"
  ]
  node [
    id 973
    label "perper"
  ]
  node [
    id 974
    label "dinar_kuwejcki"
  ]
  node [
    id 975
    label "dalasi"
  ]
  node [
    id 976
    label "dolar_Zimbabwe"
  ]
  node [
    id 977
    label "Szantung"
  ]
  node [
    id 978
    label "Chiny_Zachodnie"
  ]
  node [
    id 979
    label "Kuantung"
  ]
  node [
    id 980
    label "D&#380;ungaria"
  ]
  node [
    id 981
    label "yuan"
  ]
  node [
    id 982
    label "Hongkong"
  ]
  node [
    id 983
    label "Chiny_Wschodnie"
  ]
  node [
    id 984
    label "Guangdong"
  ]
  node [
    id 985
    label "Junnan"
  ]
  node [
    id 986
    label "Mand&#380;uria"
  ]
  node [
    id 987
    label "Syczuan"
  ]
  node [
    id 988
    label "Pa&#322;uki"
  ]
  node [
    id 989
    label "Wolin"
  ]
  node [
    id 990
    label "z&#322;oty"
  ]
  node [
    id 991
    label "So&#322;a"
  ]
  node [
    id 992
    label "Suwalszczyzna"
  ]
  node [
    id 993
    label "Krajna"
  ]
  node [
    id 994
    label "barwy_polskie"
  ]
  node [
    id 995
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 996
    label "Kaczawa"
  ]
  node [
    id 997
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 998
    label "Wis&#322;a"
  ]
  node [
    id 999
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1000
    label "Ujgur"
  ]
  node [
    id 1001
    label "Azja_Mniejsza"
  ]
  node [
    id 1002
    label "lira_turecka"
  ]
  node [
    id 1003
    label "kuna"
  ]
  node [
    id 1004
    label "dram"
  ]
  node [
    id 1005
    label "tala"
  ]
  node [
    id 1006
    label "korona_s&#322;owacka"
  ]
  node [
    id 1007
    label "Turiec"
  ]
  node [
    id 1008
    label "Himalaje"
  ]
  node [
    id 1009
    label "rupia_nepalska"
  ]
  node [
    id 1010
    label "frank_gwinejski"
  ]
  node [
    id 1011
    label "korona_esto&#324;ska"
  ]
  node [
    id 1012
    label "marka_esto&#324;ska"
  ]
  node [
    id 1013
    label "Quebec"
  ]
  node [
    id 1014
    label "dolar_kanadyjski"
  ]
  node [
    id 1015
    label "Nowa_Fundlandia"
  ]
  node [
    id 1016
    label "Zanzibar"
  ]
  node [
    id 1017
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1018
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1019
    label "&#346;wite&#378;"
  ]
  node [
    id 1020
    label "peso_kolumbijskie"
  ]
  node [
    id 1021
    label "Synaj"
  ]
  node [
    id 1022
    label "paraszyt"
  ]
  node [
    id 1023
    label "funt_egipski"
  ]
  node [
    id 1024
    label "szach"
  ]
  node [
    id 1025
    label "Baktria"
  ]
  node [
    id 1026
    label "afgani"
  ]
  node [
    id 1027
    label "baht"
  ]
  node [
    id 1028
    label "tolar"
  ]
  node [
    id 1029
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1030
    label "Gagauzja"
  ]
  node [
    id 1031
    label "moszaw"
  ]
  node [
    id 1032
    label "Kanaan"
  ]
  node [
    id 1033
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1034
    label "Jerozolima"
  ]
  node [
    id 1035
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1036
    label "Wiktoria"
  ]
  node [
    id 1037
    label "Guernsey"
  ]
  node [
    id 1038
    label "Conrad"
  ]
  node [
    id 1039
    label "funt_szterling"
  ]
  node [
    id 1040
    label "Portland"
  ]
  node [
    id 1041
    label "El&#380;bieta_I"
  ]
  node [
    id 1042
    label "Kornwalia"
  ]
  node [
    id 1043
    label "Dolna_Frankonia"
  ]
  node [
    id 1044
    label "Karpaty"
  ]
  node [
    id 1045
    label "Beskid_Niski"
  ]
  node [
    id 1046
    label "Mariensztat"
  ]
  node [
    id 1047
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1048
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1049
    label "Paj&#281;czno"
  ]
  node [
    id 1050
    label "Mogielnica"
  ]
  node [
    id 1051
    label "Gop&#322;o"
  ]
  node [
    id 1052
    label "Moza"
  ]
  node [
    id 1053
    label "Poprad"
  ]
  node [
    id 1054
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1055
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1056
    label "Bojanowo"
  ]
  node [
    id 1057
    label "Obra"
  ]
  node [
    id 1058
    label "Wilkowo_Polskie"
  ]
  node [
    id 1059
    label "Dobra"
  ]
  node [
    id 1060
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1061
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1062
    label "Etruria"
  ]
  node [
    id 1063
    label "Rumelia"
  ]
  node [
    id 1064
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1065
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1066
    label "Abchazja"
  ]
  node [
    id 1067
    label "Sarmata"
  ]
  node [
    id 1068
    label "Eurazja"
  ]
  node [
    id 1069
    label "Tatry"
  ]
  node [
    id 1070
    label "Podtatrze"
  ]
  node [
    id 1071
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1072
    label "jezioro"
  ]
  node [
    id 1073
    label "&#346;l&#261;sk"
  ]
  node [
    id 1074
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1075
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1076
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1077
    label "Austro-W&#281;gry"
  ]
  node [
    id 1078
    label "funt_szkocki"
  ]
  node [
    id 1079
    label "Kaledonia"
  ]
  node [
    id 1080
    label "Biskupice"
  ]
  node [
    id 1081
    label "Iwanowice"
  ]
  node [
    id 1082
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1083
    label "Rogo&#378;nik"
  ]
  node [
    id 1084
    label "Ropa"
  ]
  node [
    id 1085
    label "Buriacja"
  ]
  node [
    id 1086
    label "Rozewie"
  ]
  node [
    id 1087
    label "Norwegia"
  ]
  node [
    id 1088
    label "Szwecja"
  ]
  node [
    id 1089
    label "Finlandia"
  ]
  node [
    id 1090
    label "Aruba"
  ]
  node [
    id 1091
    label "Kajmany"
  ]
  node [
    id 1092
    label "Anguilla"
  ]
  node [
    id 1093
    label "Amazonka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 769
  ]
  edge [
    source 6
    target 770
  ]
  edge [
    source 6
    target 771
  ]
  edge [
    source 6
    target 772
  ]
  edge [
    source 6
    target 773
  ]
  edge [
    source 6
    target 774
  ]
  edge [
    source 6
    target 775
  ]
  edge [
    source 6
    target 776
  ]
  edge [
    source 6
    target 777
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 6
    target 784
  ]
  edge [
    source 6
    target 785
  ]
  edge [
    source 6
    target 786
  ]
  edge [
    source 6
    target 787
  ]
  edge [
    source 6
    target 788
  ]
  edge [
    source 6
    target 789
  ]
  edge [
    source 6
    target 790
  ]
  edge [
    source 6
    target 791
  ]
  edge [
    source 6
    target 792
  ]
  edge [
    source 6
    target 793
  ]
  edge [
    source 6
    target 794
  ]
  edge [
    source 6
    target 795
  ]
  edge [
    source 6
    target 796
  ]
  edge [
    source 6
    target 797
  ]
  edge [
    source 6
    target 798
  ]
  edge [
    source 6
    target 799
  ]
  edge [
    source 6
    target 800
  ]
  edge [
    source 6
    target 801
  ]
  edge [
    source 6
    target 802
  ]
  edge [
    source 6
    target 803
  ]
  edge [
    source 6
    target 804
  ]
  edge [
    source 6
    target 805
  ]
  edge [
    source 6
    target 806
  ]
  edge [
    source 6
    target 807
  ]
  edge [
    source 6
    target 808
  ]
  edge [
    source 6
    target 809
  ]
  edge [
    source 6
    target 810
  ]
  edge [
    source 6
    target 811
  ]
  edge [
    source 6
    target 812
  ]
  edge [
    source 6
    target 813
  ]
  edge [
    source 6
    target 814
  ]
  edge [
    source 6
    target 815
  ]
  edge [
    source 6
    target 816
  ]
  edge [
    source 6
    target 817
  ]
  edge [
    source 6
    target 818
  ]
  edge [
    source 6
    target 819
  ]
  edge [
    source 6
    target 820
  ]
  edge [
    source 6
    target 821
  ]
  edge [
    source 6
    target 822
  ]
  edge [
    source 6
    target 823
  ]
  edge [
    source 6
    target 824
  ]
  edge [
    source 6
    target 825
  ]
  edge [
    source 6
    target 826
  ]
  edge [
    source 6
    target 827
  ]
  edge [
    source 6
    target 828
  ]
  edge [
    source 6
    target 829
  ]
  edge [
    source 6
    target 830
  ]
  edge [
    source 6
    target 831
  ]
  edge [
    source 6
    target 832
  ]
  edge [
    source 6
    target 833
  ]
  edge [
    source 6
    target 834
  ]
  edge [
    source 6
    target 835
  ]
  edge [
    source 6
    target 836
  ]
  edge [
    source 6
    target 837
  ]
  edge [
    source 6
    target 838
  ]
  edge [
    source 6
    target 839
  ]
  edge [
    source 6
    target 840
  ]
  edge [
    source 6
    target 841
  ]
  edge [
    source 6
    target 842
  ]
  edge [
    source 6
    target 843
  ]
  edge [
    source 6
    target 844
  ]
  edge [
    source 6
    target 845
  ]
  edge [
    source 6
    target 846
  ]
  edge [
    source 6
    target 847
  ]
  edge [
    source 6
    target 848
  ]
  edge [
    source 6
    target 849
  ]
  edge [
    source 6
    target 850
  ]
  edge [
    source 6
    target 851
  ]
  edge [
    source 6
    target 852
  ]
  edge [
    source 6
    target 853
  ]
  edge [
    source 6
    target 854
  ]
  edge [
    source 6
    target 855
  ]
  edge [
    source 6
    target 856
  ]
  edge [
    source 6
    target 857
  ]
  edge [
    source 6
    target 858
  ]
  edge [
    source 6
    target 859
  ]
  edge [
    source 6
    target 860
  ]
  edge [
    source 6
    target 861
  ]
  edge [
    source 6
    target 862
  ]
  edge [
    source 6
    target 863
  ]
  edge [
    source 6
    target 864
  ]
  edge [
    source 6
    target 865
  ]
  edge [
    source 6
    target 866
  ]
  edge [
    source 6
    target 867
  ]
  edge [
    source 6
    target 868
  ]
  edge [
    source 6
    target 869
  ]
  edge [
    source 6
    target 870
  ]
  edge [
    source 6
    target 871
  ]
  edge [
    source 6
    target 872
  ]
  edge [
    source 6
    target 873
  ]
  edge [
    source 6
    target 874
  ]
  edge [
    source 6
    target 875
  ]
  edge [
    source 6
    target 876
  ]
  edge [
    source 6
    target 877
  ]
  edge [
    source 6
    target 878
  ]
  edge [
    source 6
    target 879
  ]
  edge [
    source 6
    target 880
  ]
  edge [
    source 6
    target 881
  ]
  edge [
    source 6
    target 882
  ]
  edge [
    source 6
    target 883
  ]
  edge [
    source 6
    target 884
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 6
    target 892
  ]
  edge [
    source 6
    target 893
  ]
  edge [
    source 6
    target 894
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 896
  ]
  edge [
    source 6
    target 897
  ]
  edge [
    source 6
    target 898
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 904
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 918
  ]
  edge [
    source 6
    target 919
  ]
  edge [
    source 6
    target 920
  ]
  edge [
    source 6
    target 921
  ]
  edge [
    source 6
    target 922
  ]
  edge [
    source 6
    target 923
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 925
  ]
  edge [
    source 6
    target 926
  ]
  edge [
    source 6
    target 927
  ]
  edge [
    source 6
    target 928
  ]
  edge [
    source 6
    target 929
  ]
  edge [
    source 6
    target 930
  ]
  edge [
    source 6
    target 931
  ]
  edge [
    source 6
    target 932
  ]
  edge [
    source 6
    target 933
  ]
  edge [
    source 6
    target 934
  ]
  edge [
    source 6
    target 935
  ]
  edge [
    source 6
    target 936
  ]
  edge [
    source 6
    target 937
  ]
  edge [
    source 6
    target 938
  ]
  edge [
    source 6
    target 939
  ]
  edge [
    source 6
    target 940
  ]
  edge [
    source 6
    target 941
  ]
  edge [
    source 6
    target 942
  ]
  edge [
    source 6
    target 943
  ]
  edge [
    source 6
    target 944
  ]
  edge [
    source 6
    target 945
  ]
  edge [
    source 6
    target 946
  ]
  edge [
    source 6
    target 947
  ]
  edge [
    source 6
    target 948
  ]
  edge [
    source 6
    target 949
  ]
  edge [
    source 6
    target 950
  ]
  edge [
    source 6
    target 951
  ]
  edge [
    source 6
    target 952
  ]
  edge [
    source 6
    target 953
  ]
  edge [
    source 6
    target 954
  ]
  edge [
    source 6
    target 955
  ]
  edge [
    source 6
    target 956
  ]
  edge [
    source 6
    target 957
  ]
  edge [
    source 6
    target 958
  ]
  edge [
    source 6
    target 959
  ]
  edge [
    source 6
    target 960
  ]
  edge [
    source 6
    target 961
  ]
  edge [
    source 6
    target 962
  ]
  edge [
    source 6
    target 963
  ]
  edge [
    source 6
    target 964
  ]
  edge [
    source 6
    target 965
  ]
  edge [
    source 6
    target 966
  ]
  edge [
    source 6
    target 967
  ]
  edge [
    source 6
    target 968
  ]
  edge [
    source 6
    target 969
  ]
  edge [
    source 6
    target 970
  ]
  edge [
    source 6
    target 971
  ]
  edge [
    source 6
    target 972
  ]
  edge [
    source 6
    target 973
  ]
  edge [
    source 6
    target 974
  ]
  edge [
    source 6
    target 975
  ]
  edge [
    source 6
    target 976
  ]
  edge [
    source 6
    target 977
  ]
  edge [
    source 6
    target 978
  ]
  edge [
    source 6
    target 979
  ]
  edge [
    source 6
    target 980
  ]
  edge [
    source 6
    target 981
  ]
  edge [
    source 6
    target 982
  ]
  edge [
    source 6
    target 983
  ]
  edge [
    source 6
    target 984
  ]
  edge [
    source 6
    target 985
  ]
  edge [
    source 6
    target 986
  ]
  edge [
    source 6
    target 987
  ]
  edge [
    source 6
    target 988
  ]
  edge [
    source 6
    target 989
  ]
  edge [
    source 6
    target 990
  ]
  edge [
    source 6
    target 991
  ]
  edge [
    source 6
    target 992
  ]
  edge [
    source 6
    target 993
  ]
  edge [
    source 6
    target 994
  ]
  edge [
    source 6
    target 995
  ]
  edge [
    source 6
    target 996
  ]
  edge [
    source 6
    target 997
  ]
  edge [
    source 6
    target 998
  ]
  edge [
    source 6
    target 999
  ]
  edge [
    source 6
    target 1000
  ]
  edge [
    source 6
    target 1001
  ]
  edge [
    source 6
    target 1002
  ]
  edge [
    source 6
    target 1003
  ]
  edge [
    source 6
    target 1004
  ]
  edge [
    source 6
    target 1005
  ]
  edge [
    source 6
    target 1006
  ]
  edge [
    source 6
    target 1007
  ]
  edge [
    source 6
    target 1008
  ]
  edge [
    source 6
    target 1009
  ]
  edge [
    source 6
    target 1010
  ]
  edge [
    source 6
    target 1011
  ]
  edge [
    source 6
    target 1012
  ]
  edge [
    source 6
    target 1013
  ]
  edge [
    source 6
    target 1014
  ]
  edge [
    source 6
    target 1015
  ]
  edge [
    source 6
    target 1016
  ]
  edge [
    source 6
    target 1017
  ]
  edge [
    source 6
    target 1018
  ]
  edge [
    source 6
    target 1019
  ]
  edge [
    source 6
    target 1020
  ]
  edge [
    source 6
    target 1021
  ]
  edge [
    source 6
    target 1022
  ]
  edge [
    source 6
    target 1023
  ]
  edge [
    source 6
    target 1024
  ]
  edge [
    source 6
    target 1025
  ]
  edge [
    source 6
    target 1026
  ]
  edge [
    source 6
    target 1027
  ]
  edge [
    source 6
    target 1028
  ]
  edge [
    source 6
    target 1029
  ]
  edge [
    source 6
    target 1030
  ]
  edge [
    source 6
    target 1031
  ]
  edge [
    source 6
    target 1032
  ]
  edge [
    source 6
    target 1033
  ]
  edge [
    source 6
    target 1034
  ]
  edge [
    source 6
    target 1035
  ]
  edge [
    source 6
    target 1036
  ]
  edge [
    source 6
    target 1037
  ]
  edge [
    source 6
    target 1038
  ]
  edge [
    source 6
    target 1039
  ]
  edge [
    source 6
    target 1040
  ]
  edge [
    source 6
    target 1041
  ]
  edge [
    source 6
    target 1042
  ]
  edge [
    source 6
    target 1043
  ]
  edge [
    source 6
    target 1044
  ]
  edge [
    source 6
    target 1045
  ]
  edge [
    source 6
    target 1046
  ]
  edge [
    source 6
    target 1047
  ]
  edge [
    source 6
    target 1048
  ]
  edge [
    source 6
    target 1049
  ]
  edge [
    source 6
    target 1050
  ]
  edge [
    source 6
    target 1051
  ]
  edge [
    source 6
    target 1052
  ]
  edge [
    source 6
    target 1053
  ]
  edge [
    source 6
    target 1054
  ]
  edge [
    source 6
    target 1055
  ]
  edge [
    source 6
    target 1056
  ]
  edge [
    source 6
    target 1057
  ]
  edge [
    source 6
    target 1058
  ]
  edge [
    source 6
    target 1059
  ]
  edge [
    source 6
    target 1060
  ]
  edge [
    source 6
    target 1061
  ]
  edge [
    source 6
    target 1062
  ]
  edge [
    source 6
    target 1063
  ]
  edge [
    source 6
    target 1064
  ]
  edge [
    source 6
    target 1065
  ]
  edge [
    source 6
    target 1066
  ]
  edge [
    source 6
    target 1067
  ]
  edge [
    source 6
    target 1068
  ]
  edge [
    source 6
    target 1069
  ]
  edge [
    source 6
    target 1070
  ]
  edge [
    source 6
    target 1071
  ]
  edge [
    source 6
    target 1072
  ]
  edge [
    source 6
    target 1073
  ]
  edge [
    source 6
    target 1074
  ]
  edge [
    source 6
    target 1075
  ]
  edge [
    source 6
    target 1076
  ]
  edge [
    source 6
    target 1077
  ]
  edge [
    source 6
    target 1078
  ]
  edge [
    source 6
    target 1079
  ]
  edge [
    source 6
    target 1080
  ]
  edge [
    source 6
    target 1081
  ]
  edge [
    source 6
    target 1082
  ]
  edge [
    source 6
    target 1083
  ]
  edge [
    source 6
    target 1084
  ]
  edge [
    source 6
    target 1085
  ]
  edge [
    source 6
    target 1086
  ]
  edge [
    source 6
    target 1087
  ]
  edge [
    source 6
    target 1088
  ]
  edge [
    source 6
    target 1089
  ]
  edge [
    source 6
    target 1090
  ]
  edge [
    source 6
    target 1091
  ]
  edge [
    source 6
    target 1092
  ]
  edge [
    source 6
    target 1093
  ]
]
