graph [
  node [
    id 0
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 1
    label "tym"
    origin "text"
  ]
  node [
    id 2
    label "moja"
    origin "text"
  ]
  node [
    id 3
    label "praca"
    origin "text"
  ]
  node [
    id 4
    label "nad"
    origin "text"
  ]
  node [
    id 5
    label "doktorat"
    origin "text"
  ]
  node [
    id 6
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 7
    label "powoli"
    origin "text"
  ]
  node [
    id 8
    label "wkracza&#263;"
    origin "text"
  ]
  node [
    id 9
    label "decyduj&#261;cy"
    origin "text"
  ]
  node [
    id 10
    label "faza"
    origin "text"
  ]
  node [
    id 11
    label "zacz&#261;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "coraz"
    origin "text"
  ]
  node [
    id 13
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 14
    label "czyta&#263;"
    origin "text"
  ]
  node [
    id 15
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 16
    label "fan"
    origin "text"
  ]
  node [
    id 17
    label "sprowadza&#263;"
    origin "text"
  ]
  node [
    id 18
    label "czas"
    origin "text"
  ]
  node [
    id 19
    label "ci&#281;&#380;ki"
    origin "text"
  ]
  node [
    id 20
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 21
    label "stany"
    origin "text"
  ]
  node [
    id 22
    label "odwadnia&#263;"
  ]
  node [
    id 23
    label "wi&#261;zanie"
  ]
  node [
    id 24
    label "odwodni&#263;"
  ]
  node [
    id 25
    label "bratnia_dusza"
  ]
  node [
    id 26
    label "powi&#261;zanie"
  ]
  node [
    id 27
    label "zwi&#261;zanie"
  ]
  node [
    id 28
    label "konstytucja"
  ]
  node [
    id 29
    label "organizacja"
  ]
  node [
    id 30
    label "marriage"
  ]
  node [
    id 31
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 32
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 33
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 34
    label "zwi&#261;za&#263;"
  ]
  node [
    id 35
    label "odwadnianie"
  ]
  node [
    id 36
    label "odwodnienie"
  ]
  node [
    id 37
    label "marketing_afiliacyjny"
  ]
  node [
    id 38
    label "substancja_chemiczna"
  ]
  node [
    id 39
    label "koligacja"
  ]
  node [
    id 40
    label "bearing"
  ]
  node [
    id 41
    label "lokant"
  ]
  node [
    id 42
    label "azeotrop"
  ]
  node [
    id 43
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 44
    label "dehydration"
  ]
  node [
    id 45
    label "oznaka"
  ]
  node [
    id 46
    label "osuszenie"
  ]
  node [
    id 47
    label "spowodowanie"
  ]
  node [
    id 48
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 49
    label "cia&#322;o"
  ]
  node [
    id 50
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 51
    label "odprowadzenie"
  ]
  node [
    id 52
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 53
    label "odsuni&#281;cie"
  ]
  node [
    id 54
    label "odsun&#261;&#263;"
  ]
  node [
    id 55
    label "drain"
  ]
  node [
    id 56
    label "spowodowa&#263;"
  ]
  node [
    id 57
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 58
    label "odprowadzi&#263;"
  ]
  node [
    id 59
    label "osuszy&#263;"
  ]
  node [
    id 60
    label "numeracja"
  ]
  node [
    id 61
    label "odprowadza&#263;"
  ]
  node [
    id 62
    label "powodowa&#263;"
  ]
  node [
    id 63
    label "osusza&#263;"
  ]
  node [
    id 64
    label "odci&#261;ga&#263;"
  ]
  node [
    id 65
    label "odsuwa&#263;"
  ]
  node [
    id 66
    label "struktura"
  ]
  node [
    id 67
    label "zbi&#243;r"
  ]
  node [
    id 68
    label "akt"
  ]
  node [
    id 69
    label "cezar"
  ]
  node [
    id 70
    label "dokument"
  ]
  node [
    id 71
    label "budowa"
  ]
  node [
    id 72
    label "uchwa&#322;a"
  ]
  node [
    id 73
    label "odprowadzanie"
  ]
  node [
    id 74
    label "powodowanie"
  ]
  node [
    id 75
    label "odci&#261;ganie"
  ]
  node [
    id 76
    label "dehydratacja"
  ]
  node [
    id 77
    label "osuszanie"
  ]
  node [
    id 78
    label "proces_chemiczny"
  ]
  node [
    id 79
    label "odsuwanie"
  ]
  node [
    id 80
    label "ograniczenie"
  ]
  node [
    id 81
    label "po&#322;&#261;czenie"
  ]
  node [
    id 82
    label "do&#322;&#261;czenie"
  ]
  node [
    id 83
    label "opakowanie"
  ]
  node [
    id 84
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 85
    label "attachment"
  ]
  node [
    id 86
    label "obezw&#322;adnienie"
  ]
  node [
    id 87
    label "zawi&#261;zanie"
  ]
  node [
    id 88
    label "wi&#281;&#378;"
  ]
  node [
    id 89
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 90
    label "tying"
  ]
  node [
    id 91
    label "st&#281;&#380;enie"
  ]
  node [
    id 92
    label "affiliation"
  ]
  node [
    id 93
    label "fastening"
  ]
  node [
    id 94
    label "zaprawa"
  ]
  node [
    id 95
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 96
    label "z&#322;&#261;czenie"
  ]
  node [
    id 97
    label "zobowi&#261;zanie"
  ]
  node [
    id 98
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 99
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 100
    label "w&#281;ze&#322;"
  ]
  node [
    id 101
    label "consort"
  ]
  node [
    id 102
    label "cement"
  ]
  node [
    id 103
    label "opakowa&#263;"
  ]
  node [
    id 104
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 105
    label "relate"
  ]
  node [
    id 106
    label "form"
  ]
  node [
    id 107
    label "tobo&#322;ek"
  ]
  node [
    id 108
    label "unify"
  ]
  node [
    id 109
    label "incorporate"
  ]
  node [
    id 110
    label "bind"
  ]
  node [
    id 111
    label "zawi&#261;za&#263;"
  ]
  node [
    id 112
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 113
    label "powi&#261;za&#263;"
  ]
  node [
    id 114
    label "scali&#263;"
  ]
  node [
    id 115
    label "zatrzyma&#263;"
  ]
  node [
    id 116
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 117
    label "narta"
  ]
  node [
    id 118
    label "przedmiot"
  ]
  node [
    id 119
    label "podwi&#261;zywanie"
  ]
  node [
    id 120
    label "dressing"
  ]
  node [
    id 121
    label "socket"
  ]
  node [
    id 122
    label "szermierka"
  ]
  node [
    id 123
    label "przywi&#261;zywanie"
  ]
  node [
    id 124
    label "pakowanie"
  ]
  node [
    id 125
    label "my&#347;lenie"
  ]
  node [
    id 126
    label "do&#322;&#261;czanie"
  ]
  node [
    id 127
    label "communication"
  ]
  node [
    id 128
    label "wytwarzanie"
  ]
  node [
    id 129
    label "ceg&#322;a"
  ]
  node [
    id 130
    label "combination"
  ]
  node [
    id 131
    label "zobowi&#261;zywanie"
  ]
  node [
    id 132
    label "szcz&#281;ka"
  ]
  node [
    id 133
    label "anga&#380;owanie"
  ]
  node [
    id 134
    label "wi&#261;za&#263;"
  ]
  node [
    id 135
    label "twardnienie"
  ]
  node [
    id 136
    label "podwi&#261;zanie"
  ]
  node [
    id 137
    label "przywi&#261;zanie"
  ]
  node [
    id 138
    label "przymocowywanie"
  ]
  node [
    id 139
    label "scalanie"
  ]
  node [
    id 140
    label "mezomeria"
  ]
  node [
    id 141
    label "fusion"
  ]
  node [
    id 142
    label "kojarzenie_si&#281;"
  ]
  node [
    id 143
    label "&#322;&#261;czenie"
  ]
  node [
    id 144
    label "uchwyt"
  ]
  node [
    id 145
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 146
    label "rozmieszczenie"
  ]
  node [
    id 147
    label "zmiana"
  ]
  node [
    id 148
    label "element_konstrukcyjny"
  ]
  node [
    id 149
    label "obezw&#322;adnianie"
  ]
  node [
    id 150
    label "manewr"
  ]
  node [
    id 151
    label "miecz"
  ]
  node [
    id 152
    label "oddzia&#322;ywanie"
  ]
  node [
    id 153
    label "obwi&#261;zanie"
  ]
  node [
    id 154
    label "zawi&#261;zek"
  ]
  node [
    id 155
    label "obwi&#261;zywanie"
  ]
  node [
    id 156
    label "roztw&#243;r"
  ]
  node [
    id 157
    label "podmiot"
  ]
  node [
    id 158
    label "jednostka_organizacyjna"
  ]
  node [
    id 159
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 160
    label "TOPR"
  ]
  node [
    id 161
    label "endecki"
  ]
  node [
    id 162
    label "zesp&#243;&#322;"
  ]
  node [
    id 163
    label "przedstawicielstwo"
  ]
  node [
    id 164
    label "od&#322;am"
  ]
  node [
    id 165
    label "Cepelia"
  ]
  node [
    id 166
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 167
    label "ZBoWiD"
  ]
  node [
    id 168
    label "organization"
  ]
  node [
    id 169
    label "centrala"
  ]
  node [
    id 170
    label "GOPR"
  ]
  node [
    id 171
    label "ZOMO"
  ]
  node [
    id 172
    label "ZMP"
  ]
  node [
    id 173
    label "komitet_koordynacyjny"
  ]
  node [
    id 174
    label "przybud&#243;wka"
  ]
  node [
    id 175
    label "boj&#243;wka"
  ]
  node [
    id 176
    label "zrelatywizowa&#263;"
  ]
  node [
    id 177
    label "zrelatywizowanie"
  ]
  node [
    id 178
    label "mention"
  ]
  node [
    id 179
    label "pomy&#347;lenie"
  ]
  node [
    id 180
    label "relatywizowa&#263;"
  ]
  node [
    id 181
    label "relatywizowanie"
  ]
  node [
    id 182
    label "kontakt"
  ]
  node [
    id 183
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 184
    label "najem"
  ]
  node [
    id 185
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 186
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 187
    label "zak&#322;ad"
  ]
  node [
    id 188
    label "stosunek_pracy"
  ]
  node [
    id 189
    label "benedykty&#324;ski"
  ]
  node [
    id 190
    label "poda&#380;_pracy"
  ]
  node [
    id 191
    label "pracowanie"
  ]
  node [
    id 192
    label "tyrka"
  ]
  node [
    id 193
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 194
    label "wytw&#243;r"
  ]
  node [
    id 195
    label "miejsce"
  ]
  node [
    id 196
    label "zaw&#243;d"
  ]
  node [
    id 197
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 198
    label "tynkarski"
  ]
  node [
    id 199
    label "pracowa&#263;"
  ]
  node [
    id 200
    label "czynno&#347;&#263;"
  ]
  node [
    id 201
    label "czynnik_produkcji"
  ]
  node [
    id 202
    label "kierownictwo"
  ]
  node [
    id 203
    label "siedziba"
  ]
  node [
    id 204
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 205
    label "p&#322;&#243;d"
  ]
  node [
    id 206
    label "work"
  ]
  node [
    id 207
    label "rezultat"
  ]
  node [
    id 208
    label "activity"
  ]
  node [
    id 209
    label "bezproblemowy"
  ]
  node [
    id 210
    label "wydarzenie"
  ]
  node [
    id 211
    label "warunek_lokalowy"
  ]
  node [
    id 212
    label "plac"
  ]
  node [
    id 213
    label "location"
  ]
  node [
    id 214
    label "uwaga"
  ]
  node [
    id 215
    label "przestrze&#324;"
  ]
  node [
    id 216
    label "status"
  ]
  node [
    id 217
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 218
    label "chwila"
  ]
  node [
    id 219
    label "cecha"
  ]
  node [
    id 220
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 221
    label "rz&#261;d"
  ]
  node [
    id 222
    label "stosunek_prawny"
  ]
  node [
    id 223
    label "oblig"
  ]
  node [
    id 224
    label "uregulowa&#263;"
  ]
  node [
    id 225
    label "oddzia&#322;anie"
  ]
  node [
    id 226
    label "occupation"
  ]
  node [
    id 227
    label "duty"
  ]
  node [
    id 228
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 229
    label "zapowied&#378;"
  ]
  node [
    id 230
    label "obowi&#261;zek"
  ]
  node [
    id 231
    label "statement"
  ]
  node [
    id 232
    label "zapewnienie"
  ]
  node [
    id 233
    label "miejsce_pracy"
  ]
  node [
    id 234
    label "zak&#322;adka"
  ]
  node [
    id 235
    label "instytucja"
  ]
  node [
    id 236
    label "wyko&#324;czenie"
  ]
  node [
    id 237
    label "firma"
  ]
  node [
    id 238
    label "czyn"
  ]
  node [
    id 239
    label "company"
  ]
  node [
    id 240
    label "instytut"
  ]
  node [
    id 241
    label "umowa"
  ]
  node [
    id 242
    label "&#321;ubianka"
  ]
  node [
    id 243
    label "dzia&#322;_personalny"
  ]
  node [
    id 244
    label "Kreml"
  ]
  node [
    id 245
    label "Bia&#322;y_Dom"
  ]
  node [
    id 246
    label "budynek"
  ]
  node [
    id 247
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 248
    label "sadowisko"
  ]
  node [
    id 249
    label "rewizja"
  ]
  node [
    id 250
    label "passage"
  ]
  node [
    id 251
    label "change"
  ]
  node [
    id 252
    label "ferment"
  ]
  node [
    id 253
    label "komplet"
  ]
  node [
    id 254
    label "anatomopatolog"
  ]
  node [
    id 255
    label "zmianka"
  ]
  node [
    id 256
    label "zjawisko"
  ]
  node [
    id 257
    label "amendment"
  ]
  node [
    id 258
    label "odmienianie"
  ]
  node [
    id 259
    label "tura"
  ]
  node [
    id 260
    label "cierpliwy"
  ]
  node [
    id 261
    label "mozolny"
  ]
  node [
    id 262
    label "wytrwa&#322;y"
  ]
  node [
    id 263
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 264
    label "benedykty&#324;sko"
  ]
  node [
    id 265
    label "typowy"
  ]
  node [
    id 266
    label "po_benedykty&#324;sku"
  ]
  node [
    id 267
    label "endeavor"
  ]
  node [
    id 268
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 269
    label "mie&#263;_miejsce"
  ]
  node [
    id 270
    label "podejmowa&#263;"
  ]
  node [
    id 271
    label "dziama&#263;"
  ]
  node [
    id 272
    label "do"
  ]
  node [
    id 273
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 274
    label "bangla&#263;"
  ]
  node [
    id 275
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 276
    label "maszyna"
  ]
  node [
    id 277
    label "dzia&#322;a&#263;"
  ]
  node [
    id 278
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 279
    label "tryb"
  ]
  node [
    id 280
    label "funkcjonowa&#263;"
  ]
  node [
    id 281
    label "zawodoznawstwo"
  ]
  node [
    id 282
    label "emocja"
  ]
  node [
    id 283
    label "office"
  ]
  node [
    id 284
    label "kwalifikacje"
  ]
  node [
    id 285
    label "craft"
  ]
  node [
    id 286
    label "przepracowanie_si&#281;"
  ]
  node [
    id 287
    label "zarz&#261;dzanie"
  ]
  node [
    id 288
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 289
    label "podlizanie_si&#281;"
  ]
  node [
    id 290
    label "dopracowanie"
  ]
  node [
    id 291
    label "podlizywanie_si&#281;"
  ]
  node [
    id 292
    label "uruchamianie"
  ]
  node [
    id 293
    label "dzia&#322;anie"
  ]
  node [
    id 294
    label "d&#261;&#380;enie"
  ]
  node [
    id 295
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 296
    label "uruchomienie"
  ]
  node [
    id 297
    label "nakr&#281;canie"
  ]
  node [
    id 298
    label "funkcjonowanie"
  ]
  node [
    id 299
    label "tr&#243;jstronny"
  ]
  node [
    id 300
    label "postaranie_si&#281;"
  ]
  node [
    id 301
    label "odpocz&#281;cie"
  ]
  node [
    id 302
    label "nakr&#281;cenie"
  ]
  node [
    id 303
    label "zatrzymanie"
  ]
  node [
    id 304
    label "spracowanie_si&#281;"
  ]
  node [
    id 305
    label "skakanie"
  ]
  node [
    id 306
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 307
    label "podtrzymywanie"
  ]
  node [
    id 308
    label "w&#322;&#261;czanie"
  ]
  node [
    id 309
    label "zaprz&#281;ganie"
  ]
  node [
    id 310
    label "podejmowanie"
  ]
  node [
    id 311
    label "wyrabianie"
  ]
  node [
    id 312
    label "dzianie_si&#281;"
  ]
  node [
    id 313
    label "use"
  ]
  node [
    id 314
    label "przepracowanie"
  ]
  node [
    id 315
    label "poruszanie_si&#281;"
  ]
  node [
    id 316
    label "funkcja"
  ]
  node [
    id 317
    label "impact"
  ]
  node [
    id 318
    label "przepracowywanie"
  ]
  node [
    id 319
    label "awansowanie"
  ]
  node [
    id 320
    label "courtship"
  ]
  node [
    id 321
    label "zapracowanie"
  ]
  node [
    id 322
    label "wyrobienie"
  ]
  node [
    id 323
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 324
    label "w&#322;&#261;czenie"
  ]
  node [
    id 325
    label "transakcja"
  ]
  node [
    id 326
    label "biuro"
  ]
  node [
    id 327
    label "lead"
  ]
  node [
    id 328
    label "w&#322;adza"
  ]
  node [
    id 329
    label "stopie&#324;_naukowy"
  ]
  node [
    id 330
    label "doctor's_degree"
  ]
  node [
    id 331
    label "dysertacja"
  ]
  node [
    id 332
    label "obrona"
  ]
  node [
    id 333
    label "egzamin"
  ]
  node [
    id 334
    label "walka"
  ]
  node [
    id 335
    label "liga"
  ]
  node [
    id 336
    label "gracz"
  ]
  node [
    id 337
    label "poj&#281;cie"
  ]
  node [
    id 338
    label "protection"
  ]
  node [
    id 339
    label "poparcie"
  ]
  node [
    id 340
    label "mecz"
  ]
  node [
    id 341
    label "reakcja"
  ]
  node [
    id 342
    label "defense"
  ]
  node [
    id 343
    label "s&#261;d"
  ]
  node [
    id 344
    label "auspices"
  ]
  node [
    id 345
    label "gra"
  ]
  node [
    id 346
    label "ochrona"
  ]
  node [
    id 347
    label "sp&#243;r"
  ]
  node [
    id 348
    label "post&#281;powanie"
  ]
  node [
    id 349
    label "wojsko"
  ]
  node [
    id 350
    label "defensive_structure"
  ]
  node [
    id 351
    label "guard_duty"
  ]
  node [
    id 352
    label "strona"
  ]
  node [
    id 353
    label "rozprawa"
  ]
  node [
    id 354
    label "odejmowa&#263;"
  ]
  node [
    id 355
    label "bankrupt"
  ]
  node [
    id 356
    label "open"
  ]
  node [
    id 357
    label "set_about"
  ]
  node [
    id 358
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 359
    label "begin"
  ]
  node [
    id 360
    label "post&#281;powa&#263;"
  ]
  node [
    id 361
    label "zabiera&#263;"
  ]
  node [
    id 362
    label "liczy&#263;"
  ]
  node [
    id 363
    label "reduce"
  ]
  node [
    id 364
    label "take"
  ]
  node [
    id 365
    label "abstract"
  ]
  node [
    id 366
    label "ujemny"
  ]
  node [
    id 367
    label "oddziela&#263;"
  ]
  node [
    id 368
    label "oddala&#263;"
  ]
  node [
    id 369
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 370
    label "robi&#263;"
  ]
  node [
    id 371
    label "go"
  ]
  node [
    id 372
    label "przybiera&#263;"
  ]
  node [
    id 373
    label "act"
  ]
  node [
    id 374
    label "i&#347;&#263;"
  ]
  node [
    id 375
    label "niespiesznie"
  ]
  node [
    id 376
    label "spokojny"
  ]
  node [
    id 377
    label "wolny"
  ]
  node [
    id 378
    label "stopniowo"
  ]
  node [
    id 379
    label "bezproblemowo"
  ]
  node [
    id 380
    label "wolniej"
  ]
  node [
    id 381
    label "linearnie"
  ]
  node [
    id 382
    label "stopniowy"
  ]
  node [
    id 383
    label "udanie"
  ]
  node [
    id 384
    label "niespieszny"
  ]
  node [
    id 385
    label "measuredly"
  ]
  node [
    id 386
    label "uspokajanie_si&#281;"
  ]
  node [
    id 387
    label "spokojnie"
  ]
  node [
    id 388
    label "uspokojenie_si&#281;"
  ]
  node [
    id 389
    label "cicho"
  ]
  node [
    id 390
    label "uspokojenie"
  ]
  node [
    id 391
    label "przyjemny"
  ]
  node [
    id 392
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 393
    label "nietrudny"
  ]
  node [
    id 394
    label "uspokajanie"
  ]
  node [
    id 395
    label "rozrzedzenie"
  ]
  node [
    id 396
    label "rzedni&#281;cie"
  ]
  node [
    id 397
    label "zwalnianie_si&#281;"
  ]
  node [
    id 398
    label "wakowa&#263;"
  ]
  node [
    id 399
    label "rozwadnianie"
  ]
  node [
    id 400
    label "niezale&#380;ny"
  ]
  node [
    id 401
    label "zrzedni&#281;cie"
  ]
  node [
    id 402
    label "swobodnie"
  ]
  node [
    id 403
    label "rozrzedzanie"
  ]
  node [
    id 404
    label "rozwodnienie"
  ]
  node [
    id 405
    label "strza&#322;"
  ]
  node [
    id 406
    label "wolnie"
  ]
  node [
    id 407
    label "zwolnienie_si&#281;"
  ]
  node [
    id 408
    label "wolno"
  ]
  node [
    id 409
    label "lu&#378;no"
  ]
  node [
    id 410
    label "zajmowa&#263;"
  ]
  node [
    id 411
    label "invade"
  ]
  node [
    id 412
    label "intervene"
  ]
  node [
    id 413
    label "wchodzi&#263;"
  ]
  node [
    id 414
    label "porusza&#263;"
  ]
  node [
    id 415
    label "w&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 416
    label "dochodzi&#263;"
  ]
  node [
    id 417
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 418
    label "zaziera&#263;"
  ]
  node [
    id 419
    label "move"
  ]
  node [
    id 420
    label "spotyka&#263;"
  ]
  node [
    id 421
    label "przenika&#263;"
  ]
  node [
    id 422
    label "osi&#261;ga&#263;"
  ]
  node [
    id 423
    label "nast&#281;powa&#263;"
  ]
  node [
    id 424
    label "mount"
  ]
  node [
    id 425
    label "bra&#263;"
  ]
  node [
    id 426
    label "&#322;oi&#263;"
  ]
  node [
    id 427
    label "scale"
  ]
  node [
    id 428
    label "poznawa&#263;"
  ]
  node [
    id 429
    label "zag&#322;&#281;bia&#263;_si&#281;"
  ]
  node [
    id 430
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 431
    label "przekracza&#263;"
  ]
  node [
    id 432
    label "wnika&#263;"
  ]
  node [
    id 433
    label "atakowa&#263;"
  ]
  node [
    id 434
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 435
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 436
    label "drapa&#263;_si&#281;"
  ]
  node [
    id 437
    label "wznosi&#263;_si&#281;"
  ]
  node [
    id 438
    label "uzyskiwa&#263;"
  ]
  node [
    id 439
    label "claim"
  ]
  node [
    id 440
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 441
    label "ripen"
  ]
  node [
    id 442
    label "supervene"
  ]
  node [
    id 443
    label "doczeka&#263;"
  ]
  node [
    id 444
    label "przesy&#322;ka"
  ]
  node [
    id 445
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 446
    label "doznawa&#263;"
  ]
  node [
    id 447
    label "reach"
  ]
  node [
    id 448
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 449
    label "dociera&#263;"
  ]
  node [
    id 450
    label "zachodzi&#263;"
  ]
  node [
    id 451
    label "postrzega&#263;"
  ]
  node [
    id 452
    label "orgazm"
  ]
  node [
    id 453
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 454
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 455
    label "dokoptowywa&#263;"
  ]
  node [
    id 456
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 457
    label "dolatywa&#263;"
  ]
  node [
    id 458
    label "submit"
  ]
  node [
    id 459
    label "dostarcza&#263;"
  ]
  node [
    id 460
    label "korzysta&#263;"
  ]
  node [
    id 461
    label "schorzenie"
  ]
  node [
    id 462
    label "komornik"
  ]
  node [
    id 463
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 464
    label "return"
  ]
  node [
    id 465
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 466
    label "trwa&#263;"
  ]
  node [
    id 467
    label "rozciekawia&#263;"
  ]
  node [
    id 468
    label "klasyfikacja"
  ]
  node [
    id 469
    label "zadawa&#263;"
  ]
  node [
    id 470
    label "fill"
  ]
  node [
    id 471
    label "topographic_point"
  ]
  node [
    id 472
    label "obejmowa&#263;"
  ]
  node [
    id 473
    label "pali&#263;_si&#281;"
  ]
  node [
    id 474
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 475
    label "aim"
  ]
  node [
    id 476
    label "anektowa&#263;"
  ]
  node [
    id 477
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 478
    label "prosecute"
  ]
  node [
    id 479
    label "sake"
  ]
  node [
    id 480
    label "podnosi&#263;"
  ]
  node [
    id 481
    label "drive"
  ]
  node [
    id 482
    label "meet"
  ]
  node [
    id 483
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 484
    label "wzbudza&#263;"
  ]
  node [
    id 485
    label "porobi&#263;"
  ]
  node [
    id 486
    label "najwa&#380;niejszy"
  ]
  node [
    id 487
    label "decyduj&#261;co"
  ]
  node [
    id 488
    label "g&#322;&#243;wnie"
  ]
  node [
    id 489
    label "cykl_astronomiczny"
  ]
  node [
    id 490
    label "coil"
  ]
  node [
    id 491
    label "fotoelement"
  ]
  node [
    id 492
    label "komutowanie"
  ]
  node [
    id 493
    label "stan_skupienia"
  ]
  node [
    id 494
    label "nastr&#243;j"
  ]
  node [
    id 495
    label "przerywacz"
  ]
  node [
    id 496
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 497
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 498
    label "kraw&#281;d&#378;"
  ]
  node [
    id 499
    label "obsesja"
  ]
  node [
    id 500
    label "dw&#243;jnik"
  ]
  node [
    id 501
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 502
    label "okres"
  ]
  node [
    id 503
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 504
    label "przew&#243;d"
  ]
  node [
    id 505
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 506
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 507
    label "obw&#243;d"
  ]
  node [
    id 508
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 509
    label "degree"
  ]
  node [
    id 510
    label "komutowa&#263;"
  ]
  node [
    id 511
    label "proces"
  ]
  node [
    id 512
    label "boski"
  ]
  node [
    id 513
    label "krajobraz"
  ]
  node [
    id 514
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 515
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 516
    label "przywidzenie"
  ]
  node [
    id 517
    label "presence"
  ]
  node [
    id 518
    label "charakter"
  ]
  node [
    id 519
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 520
    label "state"
  ]
  node [
    id 521
    label "klimat"
  ]
  node [
    id 522
    label "stan"
  ]
  node [
    id 523
    label "samopoczucie"
  ]
  node [
    id 524
    label "kwas"
  ]
  node [
    id 525
    label "graf"
  ]
  node [
    id 526
    label "para"
  ]
  node [
    id 527
    label "ochraniacz"
  ]
  node [
    id 528
    label "end"
  ]
  node [
    id 529
    label "koniec"
  ]
  node [
    id 530
    label "sytuacja"
  ]
  node [
    id 531
    label "network"
  ]
  node [
    id 532
    label "opornik"
  ]
  node [
    id 533
    label "lampa_elektronowa"
  ]
  node [
    id 534
    label "cewka"
  ]
  node [
    id 535
    label "linia"
  ]
  node [
    id 536
    label "prze&#322;&#261;cznik"
  ]
  node [
    id 537
    label "&#263;wiczenie"
  ]
  node [
    id 538
    label "pa&#324;stwo"
  ]
  node [
    id 539
    label "bezpiecznik"
  ]
  node [
    id 540
    label "rozmiar"
  ]
  node [
    id 541
    label "tranzystor"
  ]
  node [
    id 542
    label "okr&#281;g_przemys&#322;owy"
  ]
  node [
    id 543
    label "kondensator"
  ]
  node [
    id 544
    label "circumference"
  ]
  node [
    id 545
    label "styk"
  ]
  node [
    id 546
    label "region"
  ]
  node [
    id 547
    label "uk&#322;ad"
  ]
  node [
    id 548
    label "cyrkumferencja"
  ]
  node [
    id 549
    label "trening"
  ]
  node [
    id 550
    label "sekwencja"
  ]
  node [
    id 551
    label "jednostka_administracyjna"
  ]
  node [
    id 552
    label "poprzedzanie"
  ]
  node [
    id 553
    label "czasoprzestrze&#324;"
  ]
  node [
    id 554
    label "laba"
  ]
  node [
    id 555
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 556
    label "chronometria"
  ]
  node [
    id 557
    label "rachuba_czasu"
  ]
  node [
    id 558
    label "przep&#322;ywanie"
  ]
  node [
    id 559
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 560
    label "czasokres"
  ]
  node [
    id 561
    label "odczyt"
  ]
  node [
    id 562
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 563
    label "dzieje"
  ]
  node [
    id 564
    label "kategoria_gramatyczna"
  ]
  node [
    id 565
    label "poprzedzenie"
  ]
  node [
    id 566
    label "trawienie"
  ]
  node [
    id 567
    label "pochodzi&#263;"
  ]
  node [
    id 568
    label "period"
  ]
  node [
    id 569
    label "okres_czasu"
  ]
  node [
    id 570
    label "poprzedza&#263;"
  ]
  node [
    id 571
    label "schy&#322;ek"
  ]
  node [
    id 572
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 573
    label "odwlekanie_si&#281;"
  ]
  node [
    id 574
    label "zegar"
  ]
  node [
    id 575
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 576
    label "czwarty_wymiar"
  ]
  node [
    id 577
    label "pochodzenie"
  ]
  node [
    id 578
    label "koniugacja"
  ]
  node [
    id 579
    label "Zeitgeist"
  ]
  node [
    id 580
    label "trawi&#263;"
  ]
  node [
    id 581
    label "pogoda"
  ]
  node [
    id 582
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 583
    label "poprzedzi&#263;"
  ]
  node [
    id 584
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 585
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 586
    label "time_period"
  ]
  node [
    id 587
    label "zapami&#281;tanie"
  ]
  node [
    id 588
    label "pierdolec"
  ]
  node [
    id 589
    label "temper"
  ]
  node [
    id 590
    label "szajba"
  ]
  node [
    id 591
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 592
    label "ciecz"
  ]
  node [
    id 593
    label "faza_termodynamiczna"
  ]
  node [
    id 594
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 595
    label "prze&#322;&#261;cza&#263;"
  ]
  node [
    id 596
    label "commutation"
  ]
  node [
    id 597
    label "prze&#322;&#261;czanie"
  ]
  node [
    id 598
    label "okres_amazo&#324;ski"
  ]
  node [
    id 599
    label "stater"
  ]
  node [
    id 600
    label "flow"
  ]
  node [
    id 601
    label "choroba_przyrodzona"
  ]
  node [
    id 602
    label "ordowik"
  ]
  node [
    id 603
    label "postglacja&#322;"
  ]
  node [
    id 604
    label "kreda"
  ]
  node [
    id 605
    label "okres_hesperyjski"
  ]
  node [
    id 606
    label "sylur"
  ]
  node [
    id 607
    label "paleogen"
  ]
  node [
    id 608
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 609
    label "okres_halsztacki"
  ]
  node [
    id 610
    label "riak"
  ]
  node [
    id 611
    label "czwartorz&#281;d"
  ]
  node [
    id 612
    label "podokres"
  ]
  node [
    id 613
    label "trzeciorz&#281;d"
  ]
  node [
    id 614
    label "kalim"
  ]
  node [
    id 615
    label "fala"
  ]
  node [
    id 616
    label "perm"
  ]
  node [
    id 617
    label "retoryka"
  ]
  node [
    id 618
    label "prekambr"
  ]
  node [
    id 619
    label "neogen"
  ]
  node [
    id 620
    label "pulsacja"
  ]
  node [
    id 621
    label "proces_fizjologiczny"
  ]
  node [
    id 622
    label "kambr"
  ]
  node [
    id 623
    label "kriogen"
  ]
  node [
    id 624
    label "jednostka_geologiczna"
  ]
  node [
    id 625
    label "ton"
  ]
  node [
    id 626
    label "orosir"
  ]
  node [
    id 627
    label "poprzednik"
  ]
  node [
    id 628
    label "spell"
  ]
  node [
    id 629
    label "sider"
  ]
  node [
    id 630
    label "interstadia&#322;"
  ]
  node [
    id 631
    label "ektas"
  ]
  node [
    id 632
    label "epoka"
  ]
  node [
    id 633
    label "rok_akademicki"
  ]
  node [
    id 634
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 635
    label "cykl"
  ]
  node [
    id 636
    label "ciota"
  ]
  node [
    id 637
    label "okres_noachijski"
  ]
  node [
    id 638
    label "pierwszorz&#281;d"
  ]
  node [
    id 639
    label "ediakar"
  ]
  node [
    id 640
    label "zdanie"
  ]
  node [
    id 641
    label "nast&#281;pnik"
  ]
  node [
    id 642
    label "condition"
  ]
  node [
    id 643
    label "jura"
  ]
  node [
    id 644
    label "glacja&#322;"
  ]
  node [
    id 645
    label "sten"
  ]
  node [
    id 646
    label "era"
  ]
  node [
    id 647
    label "trias"
  ]
  node [
    id 648
    label "p&#243;&#322;okres"
  ]
  node [
    id 649
    label "rok_szkolny"
  ]
  node [
    id 650
    label "dewon"
  ]
  node [
    id 651
    label "karbon"
  ]
  node [
    id 652
    label "izochronizm"
  ]
  node [
    id 653
    label "preglacja&#322;"
  ]
  node [
    id 654
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 655
    label "drugorz&#281;d"
  ]
  node [
    id 656
    label "semester"
  ]
  node [
    id 657
    label "kognicja"
  ]
  node [
    id 658
    label "przy&#322;&#261;cze"
  ]
  node [
    id 659
    label "organ"
  ]
  node [
    id 660
    label "przes&#322;anka"
  ]
  node [
    id 661
    label "przewodnictwo"
  ]
  node [
    id 662
    label "tr&#243;jnik"
  ]
  node [
    id 663
    label "wtyczka"
  ]
  node [
    id 664
    label "&#380;y&#322;a"
  ]
  node [
    id 665
    label "duct"
  ]
  node [
    id 666
    label "urz&#261;dzenie"
  ]
  node [
    id 667
    label "k&#261;piel_fotograficzna"
  ]
  node [
    id 668
    label "klapa"
  ]
  node [
    id 669
    label "interrupter"
  ]
  node [
    id 670
    label "du&#380;y"
  ]
  node [
    id 671
    label "mocno"
  ]
  node [
    id 672
    label "wiela"
  ]
  node [
    id 673
    label "bardzo"
  ]
  node [
    id 674
    label "cz&#281;sto"
  ]
  node [
    id 675
    label "wiele"
  ]
  node [
    id 676
    label "doros&#322;y"
  ]
  node [
    id 677
    label "znaczny"
  ]
  node [
    id 678
    label "niema&#322;o"
  ]
  node [
    id 679
    label "rozwini&#281;ty"
  ]
  node [
    id 680
    label "dorodny"
  ]
  node [
    id 681
    label "wa&#380;ny"
  ]
  node [
    id 682
    label "prawdziwy"
  ]
  node [
    id 683
    label "intensywny"
  ]
  node [
    id 684
    label "mocny"
  ]
  node [
    id 685
    label "silny"
  ]
  node [
    id 686
    label "przekonuj&#261;co"
  ]
  node [
    id 687
    label "powerfully"
  ]
  node [
    id 688
    label "widocznie"
  ]
  node [
    id 689
    label "szczerze"
  ]
  node [
    id 690
    label "konkretnie"
  ]
  node [
    id 691
    label "niepodwa&#380;alnie"
  ]
  node [
    id 692
    label "stabilnie"
  ]
  node [
    id 693
    label "silnie"
  ]
  node [
    id 694
    label "zdecydowanie"
  ]
  node [
    id 695
    label "strongly"
  ]
  node [
    id 696
    label "w_chuj"
  ]
  node [
    id 697
    label "cz&#281;sty"
  ]
  node [
    id 698
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 699
    label "dysleksja"
  ]
  node [
    id 700
    label "wr&#243;&#380;y&#263;"
  ]
  node [
    id 701
    label "odczytywa&#263;"
  ]
  node [
    id 702
    label "umie&#263;"
  ]
  node [
    id 703
    label "obserwowa&#263;"
  ]
  node [
    id 704
    label "read"
  ]
  node [
    id 705
    label "przetwarza&#263;"
  ]
  node [
    id 706
    label "dostrzega&#263;"
  ]
  node [
    id 707
    label "patrze&#263;"
  ]
  node [
    id 708
    label "look"
  ]
  node [
    id 709
    label "sprawdza&#263;"
  ]
  node [
    id 710
    label "podawa&#263;"
  ]
  node [
    id 711
    label "interpretowa&#263;"
  ]
  node [
    id 712
    label "convert"
  ]
  node [
    id 713
    label "wyzyskiwa&#263;"
  ]
  node [
    id 714
    label "opracowywa&#263;"
  ]
  node [
    id 715
    label "analizowa&#263;"
  ]
  node [
    id 716
    label "tworzy&#263;"
  ]
  node [
    id 717
    label "przewidywa&#263;"
  ]
  node [
    id 718
    label "wnioskowa&#263;"
  ]
  node [
    id 719
    label "harbinger"
  ]
  node [
    id 720
    label "bode"
  ]
  node [
    id 721
    label "bespeak"
  ]
  node [
    id 722
    label "przepowiada&#263;"
  ]
  node [
    id 723
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 724
    label "zawiera&#263;"
  ]
  node [
    id 725
    label "cognizance"
  ]
  node [
    id 726
    label "szkoli&#263;_si&#281;"
  ]
  node [
    id 727
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 728
    label "go_steady"
  ]
  node [
    id 729
    label "detect"
  ]
  node [
    id 730
    label "make"
  ]
  node [
    id 731
    label "rozr&#243;&#380;nia&#263;"
  ]
  node [
    id 732
    label "hurt"
  ]
  node [
    id 733
    label "styka&#263;_si&#281;"
  ]
  node [
    id 734
    label "wiedzie&#263;"
  ]
  node [
    id 735
    label "can"
  ]
  node [
    id 736
    label "m&#243;c"
  ]
  node [
    id 737
    label "dysfunkcja"
  ]
  node [
    id 738
    label "dyslexia"
  ]
  node [
    id 739
    label "czytanie"
  ]
  node [
    id 740
    label "fan_club"
  ]
  node [
    id 741
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 742
    label "fandom"
  ]
  node [
    id 743
    label "sympatyk"
  ]
  node [
    id 744
    label "entuzjasta"
  ]
  node [
    id 745
    label "grupa"
  ]
  node [
    id 746
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 747
    label "u&#322;amek"
  ]
  node [
    id 748
    label "kierowa&#263;"
  ]
  node [
    id 749
    label "prowadzi&#263;"
  ]
  node [
    id 750
    label "zmienia&#263;"
  ]
  node [
    id 751
    label "deliver"
  ]
  node [
    id 752
    label "ogranicza&#263;"
  ]
  node [
    id 753
    label "wprowadza&#263;"
  ]
  node [
    id 754
    label "head"
  ]
  node [
    id 755
    label "upraszcza&#263;"
  ]
  node [
    id 756
    label "sterowa&#263;"
  ]
  node [
    id 757
    label "wysy&#322;a&#263;"
  ]
  node [
    id 758
    label "manipulate"
  ]
  node [
    id 759
    label "zwierzchnik"
  ]
  node [
    id 760
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 761
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 762
    label "ustawia&#263;"
  ]
  node [
    id 763
    label "give"
  ]
  node [
    id 764
    label "przeznacza&#263;"
  ]
  node [
    id 765
    label "control"
  ]
  node [
    id 766
    label "match"
  ]
  node [
    id 767
    label "motywowa&#263;"
  ]
  node [
    id 768
    label "administrowa&#263;"
  ]
  node [
    id 769
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 770
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 771
    label "order"
  ]
  node [
    id 772
    label "indicate"
  ]
  node [
    id 773
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 774
    label "&#380;y&#263;"
  ]
  node [
    id 775
    label "g&#243;rowa&#263;"
  ]
  node [
    id 776
    label "krzywa"
  ]
  node [
    id 777
    label "linia_melodyczna"
  ]
  node [
    id 778
    label "string"
  ]
  node [
    id 779
    label "ukierunkowywa&#263;"
  ]
  node [
    id 780
    label "kre&#347;li&#263;"
  ]
  node [
    id 781
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 782
    label "message"
  ]
  node [
    id 783
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 784
    label "eksponowa&#263;"
  ]
  node [
    id 785
    label "navigate"
  ]
  node [
    id 786
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 787
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 788
    label "przesuwa&#263;"
  ]
  node [
    id 789
    label "partner"
  ]
  node [
    id 790
    label "prowadzenie"
  ]
  node [
    id 791
    label "traci&#263;"
  ]
  node [
    id 792
    label "alternate"
  ]
  node [
    id 793
    label "reengineering"
  ]
  node [
    id 794
    label "zast&#281;powa&#263;"
  ]
  node [
    id 795
    label "sprawia&#263;"
  ]
  node [
    id 796
    label "zyskiwa&#263;"
  ]
  node [
    id 797
    label "przechodzi&#263;"
  ]
  node [
    id 798
    label "suppress"
  ]
  node [
    id 799
    label "wytycza&#263;"
  ]
  node [
    id 800
    label "zmniejsza&#263;"
  ]
  node [
    id 801
    label "environment"
  ]
  node [
    id 802
    label "bound"
  ]
  node [
    id 803
    label "stanowi&#263;"
  ]
  node [
    id 804
    label "wi&#281;zienie"
  ]
  node [
    id 805
    label "u&#322;atwia&#263;"
  ]
  node [
    id 806
    label "simplify"
  ]
  node [
    id 807
    label "deformowa&#263;"
  ]
  node [
    id 808
    label "goban"
  ]
  node [
    id 809
    label "gra_planszowa"
  ]
  node [
    id 810
    label "sport_umys&#322;owy"
  ]
  node [
    id 811
    label "chi&#324;ski"
  ]
  node [
    id 812
    label "rynek"
  ]
  node [
    id 813
    label "zak&#322;&#243;ca&#263;"
  ]
  node [
    id 814
    label "wprawia&#263;"
  ]
  node [
    id 815
    label "wpisywa&#263;"
  ]
  node [
    id 816
    label "zapoznawa&#263;"
  ]
  node [
    id 817
    label "inflict"
  ]
  node [
    id 818
    label "umieszcza&#263;"
  ]
  node [
    id 819
    label "schodzi&#263;"
  ]
  node [
    id 820
    label "induct"
  ]
  node [
    id 821
    label "doprowadza&#263;"
  ]
  node [
    id 822
    label "kawa&#322;ek"
  ]
  node [
    id 823
    label "chip"
  ]
  node [
    id 824
    label "sprowadzanie"
  ]
  node [
    id 825
    label "fraction"
  ]
  node [
    id 826
    label "mianownik"
  ]
  node [
    id 827
    label "sprowadzenie"
  ]
  node [
    id 828
    label "sprowadzi&#263;"
  ]
  node [
    id 829
    label "iloraz"
  ]
  node [
    id 830
    label "licznik"
  ]
  node [
    id 831
    label "time"
  ]
  node [
    id 832
    label "blok"
  ]
  node [
    id 833
    label "handout"
  ]
  node [
    id 834
    label "pomiar"
  ]
  node [
    id 835
    label "lecture"
  ]
  node [
    id 836
    label "reading"
  ]
  node [
    id 837
    label "podawanie"
  ]
  node [
    id 838
    label "wyk&#322;ad"
  ]
  node [
    id 839
    label "potrzyma&#263;"
  ]
  node [
    id 840
    label "warunki"
  ]
  node [
    id 841
    label "pok&#243;j"
  ]
  node [
    id 842
    label "atak"
  ]
  node [
    id 843
    label "program"
  ]
  node [
    id 844
    label "meteorology"
  ]
  node [
    id 845
    label "weather"
  ]
  node [
    id 846
    label "prognoza_meteorologiczna"
  ]
  node [
    id 847
    label "czas_wolny"
  ]
  node [
    id 848
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 849
    label "metrologia"
  ]
  node [
    id 850
    label "godzinnik"
  ]
  node [
    id 851
    label "bicie"
  ]
  node [
    id 852
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 853
    label "wahad&#322;o"
  ]
  node [
    id 854
    label "kurant"
  ]
  node [
    id 855
    label "cyferblat"
  ]
  node [
    id 856
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 857
    label "nabicie"
  ]
  node [
    id 858
    label "werk"
  ]
  node [
    id 859
    label "czasomierz"
  ]
  node [
    id 860
    label "tyka&#263;"
  ]
  node [
    id 861
    label "tykn&#261;&#263;"
  ]
  node [
    id 862
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 863
    label "kotwica"
  ]
  node [
    id 864
    label "fleksja"
  ]
  node [
    id 865
    label "liczba"
  ]
  node [
    id 866
    label "coupling"
  ]
  node [
    id 867
    label "osoba"
  ]
  node [
    id 868
    label "czasownik"
  ]
  node [
    id 869
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 870
    label "orz&#281;sek"
  ]
  node [
    id 871
    label "usuwa&#263;"
  ]
  node [
    id 872
    label "lutowa&#263;"
  ]
  node [
    id 873
    label "marnowa&#263;"
  ]
  node [
    id 874
    label "przetrawia&#263;"
  ]
  node [
    id 875
    label "poch&#322;ania&#263;"
  ]
  node [
    id 876
    label "digest"
  ]
  node [
    id 877
    label "metal"
  ]
  node [
    id 878
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 879
    label "sp&#281;dza&#263;"
  ]
  node [
    id 880
    label "digestion"
  ]
  node [
    id 881
    label "unicestwianie"
  ]
  node [
    id 882
    label "sp&#281;dzanie"
  ]
  node [
    id 883
    label "contemplation"
  ]
  node [
    id 884
    label "rozk&#322;adanie"
  ]
  node [
    id 885
    label "marnowanie"
  ]
  node [
    id 886
    label "przetrawianie"
  ]
  node [
    id 887
    label "perystaltyka"
  ]
  node [
    id 888
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 889
    label "zaczynanie_si&#281;"
  ]
  node [
    id 890
    label "str&#243;j"
  ]
  node [
    id 891
    label "wynikanie"
  ]
  node [
    id 892
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 893
    label "origin"
  ]
  node [
    id 894
    label "background"
  ]
  node [
    id 895
    label "geneza"
  ]
  node [
    id 896
    label "beginning"
  ]
  node [
    id 897
    label "przeby&#263;"
  ]
  node [
    id 898
    label "min&#261;&#263;"
  ]
  node [
    id 899
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 900
    label "swimming"
  ]
  node [
    id 901
    label "zago&#347;ci&#263;"
  ]
  node [
    id 902
    label "cross"
  ]
  node [
    id 903
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 904
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 905
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 906
    label "przebywa&#263;"
  ]
  node [
    id 907
    label "pour"
  ]
  node [
    id 908
    label "carry"
  ]
  node [
    id 909
    label "sail"
  ]
  node [
    id 910
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 911
    label "go&#347;ci&#263;"
  ]
  node [
    id 912
    label "mija&#263;"
  ]
  node [
    id 913
    label "proceed"
  ]
  node [
    id 914
    label "mini&#281;cie"
  ]
  node [
    id 915
    label "doznanie"
  ]
  node [
    id 916
    label "zaistnienie"
  ]
  node [
    id 917
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 918
    label "przebycie"
  ]
  node [
    id 919
    label "cruise"
  ]
  node [
    id 920
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 921
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 922
    label "zjawianie_si&#281;"
  ]
  node [
    id 923
    label "przebywanie"
  ]
  node [
    id 924
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 925
    label "mijanie"
  ]
  node [
    id 926
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 927
    label "zaznawanie"
  ]
  node [
    id 928
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 929
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 930
    label "flux"
  ]
  node [
    id 931
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 932
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 933
    label "zrobi&#263;"
  ]
  node [
    id 934
    label "opatrzy&#263;"
  ]
  node [
    id 935
    label "overwhelm"
  ]
  node [
    id 936
    label "opatrywanie"
  ]
  node [
    id 937
    label "odej&#347;cie"
  ]
  node [
    id 938
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 939
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 940
    label "zanikni&#281;cie"
  ]
  node [
    id 941
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 942
    label "opuszczenie"
  ]
  node [
    id 943
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 944
    label "departure"
  ]
  node [
    id 945
    label "oddalenie_si&#281;"
  ]
  node [
    id 946
    label "date"
  ]
  node [
    id 947
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 948
    label "wynika&#263;"
  ]
  node [
    id 949
    label "fall"
  ]
  node [
    id 950
    label "poby&#263;"
  ]
  node [
    id 951
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 952
    label "bolt"
  ]
  node [
    id 953
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 954
    label "uda&#263;_si&#281;"
  ]
  node [
    id 955
    label "opatrzenie"
  ]
  node [
    id 956
    label "zdarzenie_si&#281;"
  ]
  node [
    id 957
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 958
    label "progress"
  ]
  node [
    id 959
    label "opatrywa&#263;"
  ]
  node [
    id 960
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 961
    label "kres"
  ]
  node [
    id 962
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 963
    label "monumentalny"
  ]
  node [
    id 964
    label "trudny"
  ]
  node [
    id 965
    label "kompletny"
  ]
  node [
    id 966
    label "masywny"
  ]
  node [
    id 967
    label "wielki"
  ]
  node [
    id 968
    label "wymagaj&#261;cy"
  ]
  node [
    id 969
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 970
    label "przyswajalny"
  ]
  node [
    id 971
    label "niezgrabny"
  ]
  node [
    id 972
    label "liczny"
  ]
  node [
    id 973
    label "nieprzejrzysty"
  ]
  node [
    id 974
    label "niedelikatny"
  ]
  node [
    id 975
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 976
    label "nieudany"
  ]
  node [
    id 977
    label "zbrojny"
  ]
  node [
    id 978
    label "dotkliwy"
  ]
  node [
    id 979
    label "charakterystyczny"
  ]
  node [
    id 980
    label "ci&#281;&#380;ko"
  ]
  node [
    id 981
    label "bojowy"
  ]
  node [
    id 982
    label "k&#322;opotliwy"
  ]
  node [
    id 983
    label "ambitny"
  ]
  node [
    id 984
    label "grubo"
  ]
  node [
    id 985
    label "gro&#378;ny"
  ]
  node [
    id 986
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 987
    label "druzgoc&#261;cy"
  ]
  node [
    id 988
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 989
    label "bezwzgl&#281;dny"
  ]
  node [
    id 990
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 991
    label "oci&#281;&#380;ale"
  ]
  node [
    id 992
    label "obwis&#322;y"
  ]
  node [
    id 993
    label "kompletnie"
  ]
  node [
    id 994
    label "zupe&#322;ny"
  ]
  node [
    id 995
    label "w_pizdu"
  ]
  node [
    id 996
    label "pe&#322;ny"
  ]
  node [
    id 997
    label "rojenie_si&#281;"
  ]
  node [
    id 998
    label "licznie"
  ]
  node [
    id 999
    label "przykry"
  ]
  node [
    id 1000
    label "dotkliwie"
  ]
  node [
    id 1001
    label "wyj&#261;tkowy"
  ]
  node [
    id 1002
    label "nieprzeci&#281;tny"
  ]
  node [
    id 1003
    label "wysoce"
  ]
  node [
    id 1004
    label "wybitny"
  ]
  node [
    id 1005
    label "dupny"
  ]
  node [
    id 1006
    label "niestosowny"
  ]
  node [
    id 1007
    label "niekszta&#322;tny"
  ]
  node [
    id 1008
    label "niezgrabnie"
  ]
  node [
    id 1009
    label "g&#322;o&#347;ny"
  ]
  node [
    id 1010
    label "masywnie"
  ]
  node [
    id 1011
    label "szybki"
  ]
  node [
    id 1012
    label "znacz&#261;cy"
  ]
  node [
    id 1013
    label "zwarty"
  ]
  node [
    id 1014
    label "efektywny"
  ]
  node [
    id 1015
    label "ogrodnictwo"
  ]
  node [
    id 1016
    label "dynamiczny"
  ]
  node [
    id 1017
    label "intensywnie"
  ]
  node [
    id 1018
    label "nieproporcjonalny"
  ]
  node [
    id 1019
    label "specjalny"
  ]
  node [
    id 1020
    label "mo&#380;liwy"
  ]
  node [
    id 1021
    label "skomplikowany"
  ]
  node [
    id 1022
    label "niebezpieczny"
  ]
  node [
    id 1023
    label "gro&#378;nie"
  ]
  node [
    id 1024
    label "nad&#261;sany"
  ]
  node [
    id 1025
    label "nieprzyjemny"
  ]
  node [
    id 1026
    label "niegrzeczny"
  ]
  node [
    id 1027
    label "nieoboj&#281;tny"
  ]
  node [
    id 1028
    label "niewra&#380;liwy"
  ]
  node [
    id 1029
    label "wytrzyma&#322;y"
  ]
  node [
    id 1030
    label "niedelikatnie"
  ]
  node [
    id 1031
    label "bojowo"
  ]
  node [
    id 1032
    label "pewny"
  ]
  node [
    id 1033
    label "&#347;mia&#322;y"
  ]
  node [
    id 1034
    label "zadziorny"
  ]
  node [
    id 1035
    label "bojowniczy"
  ]
  node [
    id 1036
    label "waleczny"
  ]
  node [
    id 1037
    label "zbrojnie"
  ]
  node [
    id 1038
    label "przyodziany"
  ]
  node [
    id 1039
    label "uzbrojony"
  ]
  node [
    id 1040
    label "ostry"
  ]
  node [
    id 1041
    label "k&#322;opotliwie"
  ]
  node [
    id 1042
    label "niewygodny"
  ]
  node [
    id 1043
    label "wymagaj&#261;co"
  ]
  node [
    id 1044
    label "monumentalnie"
  ]
  node [
    id 1045
    label "wznios&#322;y"
  ]
  node [
    id 1046
    label "nieudanie"
  ]
  node [
    id 1047
    label "nieciekawy"
  ]
  node [
    id 1048
    label "z&#322;y"
  ]
  node [
    id 1049
    label "szczery"
  ]
  node [
    id 1050
    label "niepodwa&#380;alny"
  ]
  node [
    id 1051
    label "zdecydowany"
  ]
  node [
    id 1052
    label "stabilny"
  ]
  node [
    id 1053
    label "krzepki"
  ]
  node [
    id 1054
    label "wyrazisty"
  ]
  node [
    id 1055
    label "przekonuj&#261;cy"
  ]
  node [
    id 1056
    label "widoczny"
  ]
  node [
    id 1057
    label "wzmocni&#263;"
  ]
  node [
    id 1058
    label "wzmacnia&#263;"
  ]
  node [
    id 1059
    label "konkretny"
  ]
  node [
    id 1060
    label "meflochina"
  ]
  node [
    id 1061
    label "dobry"
  ]
  node [
    id 1062
    label "charakterystycznie"
  ]
  node [
    id 1063
    label "szczeg&#243;lny"
  ]
  node [
    id 1064
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1065
    label "podobny"
  ]
  node [
    id 1066
    label "hard"
  ]
  node [
    id 1067
    label "&#378;le"
  ]
  node [
    id 1068
    label "heavily"
  ]
  node [
    id 1069
    label "gruby"
  ]
  node [
    id 1070
    label "niegrzecznie"
  ]
  node [
    id 1071
    label "dono&#347;nie"
  ]
  node [
    id 1072
    label "grubia&#324;ski"
  ]
  node [
    id 1073
    label "fajnie"
  ]
  node [
    id 1074
    label "prostacko"
  ]
  node [
    id 1075
    label "ciep&#322;o"
  ]
  node [
    id 1076
    label "m&#261;cenie"
  ]
  node [
    id 1077
    label "niejawny"
  ]
  node [
    id 1078
    label "zanieczyszczanie"
  ]
  node [
    id 1079
    label "ciemny"
  ]
  node [
    id 1080
    label "nieklarowny"
  ]
  node [
    id 1081
    label "niezrozumia&#322;y"
  ]
  node [
    id 1082
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 1083
    label "zanieczyszczenie"
  ]
  node [
    id 1084
    label "niepewny"
  ]
  node [
    id 1085
    label "samodzielny"
  ]
  node [
    id 1086
    label "ambitnie"
  ]
  node [
    id 1087
    label "zdeterminowany"
  ]
  node [
    id 1088
    label "wysokich_lot&#243;w"
  ]
  node [
    id 1089
    label "rozmienia&#263;"
  ]
  node [
    id 1090
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 1091
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 1092
    label "jednostka_monetarna"
  ]
  node [
    id 1093
    label "moniak"
  ]
  node [
    id 1094
    label "nomina&#322;"
  ]
  node [
    id 1095
    label "zdewaluowa&#263;"
  ]
  node [
    id 1096
    label "dewaluowanie"
  ]
  node [
    id 1097
    label "pieni&#261;dze"
  ]
  node [
    id 1098
    label "numizmat"
  ]
  node [
    id 1099
    label "rozmienianie"
  ]
  node [
    id 1100
    label "rozmieni&#263;"
  ]
  node [
    id 1101
    label "zdewaluowanie"
  ]
  node [
    id 1102
    label "rozmienienie"
  ]
  node [
    id 1103
    label "dewaluowa&#263;"
  ]
  node [
    id 1104
    label "coin"
  ]
  node [
    id 1105
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 1106
    label "moneta"
  ]
  node [
    id 1107
    label "drobne"
  ]
  node [
    id 1108
    label "medal"
  ]
  node [
    id 1109
    label "numismatics"
  ]
  node [
    id 1110
    label "okaz"
  ]
  node [
    id 1111
    label "zmieni&#263;"
  ]
  node [
    id 1112
    label "warto&#347;&#263;"
  ]
  node [
    id 1113
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1114
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1115
    label "par_value"
  ]
  node [
    id 1116
    label "cena"
  ]
  node [
    id 1117
    label "znaczek"
  ]
  node [
    id 1118
    label "wymienienie"
  ]
  node [
    id 1119
    label "zast&#281;powanie"
  ]
  node [
    id 1120
    label "obni&#380;anie"
  ]
  node [
    id 1121
    label "umniejszanie"
  ]
  node [
    id 1122
    label "devaluation"
  ]
  node [
    id 1123
    label "adulteration"
  ]
  node [
    id 1124
    label "obni&#380;enie"
  ]
  node [
    id 1125
    label "umniejszenie"
  ]
  node [
    id 1126
    label "obni&#380;a&#263;"
  ]
  node [
    id 1127
    label "umniejsza&#263;"
  ]
  node [
    id 1128
    label "knock"
  ]
  node [
    id 1129
    label "devalue"
  ]
  node [
    id 1130
    label "depreciate"
  ]
  node [
    id 1131
    label "umniejszy&#263;"
  ]
  node [
    id 1132
    label "obni&#380;y&#263;"
  ]
  node [
    id 1133
    label "portfel"
  ]
  node [
    id 1134
    label "kwota"
  ]
  node [
    id 1135
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 1136
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 1137
    label "forsa"
  ]
  node [
    id 1138
    label "kapanie"
  ]
  node [
    id 1139
    label "kapn&#261;&#263;"
  ]
  node [
    id 1140
    label "kapa&#263;"
  ]
  node [
    id 1141
    label "kapita&#322;"
  ]
  node [
    id 1142
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 1143
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 1144
    label "kapni&#281;cie"
  ]
  node [
    id 1145
    label "wyda&#263;"
  ]
  node [
    id 1146
    label "hajs"
  ]
  node [
    id 1147
    label "dydki"
  ]
  node [
    id 1148
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 1149
    label "Science"
  ]
  node [
    id 1150
    label "Fiction"
  ]
  node [
    id 1151
    label "Audiences"
  ]
  node [
    id 1152
    label "John"
  ]
  node [
    id 1153
    label "Tulloch"
  ]
  node [
    id 1154
    label "henr"
  ]
  node [
    id 1155
    label "Jenkins"
  ]
  node [
    id 1156
    label "Doctor"
  ]
  node [
    id 1157
    label "WHO"
  ]
  node [
    id 1158
    label "star"
  ]
  node [
    id 1159
    label "Trek"
  ]
  node [
    id 1160
    label "David"
  ]
  node [
    id 1161
    label "Morleya"
  ]
  node [
    id 1162
    label "The"
  ]
  node [
    id 1163
    label "Nationwide"
  ]
  node [
    id 1164
    label "Audience"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 528
  ]
  edge [
    source 10
    target 529
  ]
  edge [
    source 10
    target 530
  ]
  edge [
    source 10
    target 531
  ]
  edge [
    source 10
    target 532
  ]
  edge [
    source 10
    target 533
  ]
  edge [
    source 10
    target 534
  ]
  edge [
    source 10
    target 535
  ]
  edge [
    source 10
    target 536
  ]
  edge [
    source 10
    target 537
  ]
  edge [
    source 10
    target 538
  ]
  edge [
    source 10
    target 539
  ]
  edge [
    source 10
    target 540
  ]
  edge [
    source 10
    target 541
  ]
  edge [
    source 10
    target 542
  ]
  edge [
    source 10
    target 543
  ]
  edge [
    source 10
    target 544
  ]
  edge [
    source 10
    target 545
  ]
  edge [
    source 10
    target 546
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 587
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 588
  ]
  edge [
    source 10
    target 589
  ]
  edge [
    source 10
    target 590
  ]
  edge [
    source 10
    target 591
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 593
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 595
  ]
  edge [
    source 10
    target 596
  ]
  edge [
    source 10
    target 597
  ]
  edge [
    source 10
    target 598
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 600
  ]
  edge [
    source 10
    target 601
  ]
  edge [
    source 10
    target 602
  ]
  edge [
    source 10
    target 603
  ]
  edge [
    source 10
    target 604
  ]
  edge [
    source 10
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 14
    target 699
  ]
  edge [
    source 14
    target 428
  ]
  edge [
    source 14
    target 700
  ]
  edge [
    source 14
    target 701
  ]
  edge [
    source 14
    target 702
  ]
  edge [
    source 14
    target 703
  ]
  edge [
    source 14
    target 704
  ]
  edge [
    source 14
    target 705
  ]
  edge [
    source 14
    target 706
  ]
  edge [
    source 14
    target 707
  ]
  edge [
    source 14
    target 708
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 14
    target 734
  ]
  edge [
    source 14
    target 735
  ]
  edge [
    source 14
    target 736
  ]
  edge [
    source 14
    target 737
  ]
  edge [
    source 14
    target 738
  ]
  edge [
    source 14
    target 739
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 20
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 748
  ]
  edge [
    source 17
    target 371
  ]
  edge [
    source 17
    target 363
  ]
  edge [
    source 17
    target 749
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 751
  ]
  edge [
    source 17
    target 730
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 753
  ]
  edge [
    source 17
    target 754
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 759
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 761
  ]
  edge [
    source 17
    target 762
  ]
  edge [
    source 17
    target 763
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 766
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 770
  ]
  edge [
    source 17
    target 771
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 269
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 373
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 370
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 776
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 268
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 251
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 727
  ]
  edge [
    source 17
    target 413
  ]
  edge [
    source 17
    target 364
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 359
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 552
  ]
  edge [
    source 18
    target 553
  ]
  edge [
    source 18
    target 554
  ]
  edge [
    source 18
    target 555
  ]
  edge [
    source 18
    target 556
  ]
  edge [
    source 18
    target 440
  ]
  edge [
    source 18
    target 557
  ]
  edge [
    source 18
    target 558
  ]
  edge [
    source 18
    target 559
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 561
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 562
  ]
  edge [
    source 18
    target 563
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 567
  ]
  edge [
    source 18
    target 568
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 256
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 279
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 787
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 278
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 56
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 518
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 636
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 963
  ]
  edge [
    source 19
    target 684
  ]
  edge [
    source 19
    target 964
  ]
  edge [
    source 19
    target 965
  ]
  edge [
    source 19
    target 966
  ]
  edge [
    source 19
    target 967
  ]
  edge [
    source 19
    target 968
  ]
  edge [
    source 19
    target 969
  ]
  edge [
    source 19
    target 970
  ]
  edge [
    source 19
    target 971
  ]
  edge [
    source 19
    target 972
  ]
  edge [
    source 19
    target 973
  ]
  edge [
    source 19
    target 974
  ]
  edge [
    source 19
    target 975
  ]
  edge [
    source 19
    target 683
  ]
  edge [
    source 19
    target 377
  ]
  edge [
    source 19
    target 976
  ]
  edge [
    source 19
    target 977
  ]
  edge [
    source 19
    target 978
  ]
  edge [
    source 19
    target 979
  ]
  edge [
    source 19
    target 980
  ]
  edge [
    source 19
    target 981
  ]
  edge [
    source 19
    target 982
  ]
  edge [
    source 19
    target 983
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 697
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 677
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 681
  ]
  edge [
    source 19
    target 682
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 670
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 395
  ]
  edge [
    source 19
    target 396
  ]
  edge [
    source 19
    target 384
  ]
  edge [
    source 19
    target 397
  ]
  edge [
    source 19
    target 398
  ]
  edge [
    source 19
    target 399
  ]
  edge [
    source 19
    target 400
  ]
  edge [
    source 19
    target 401
  ]
  edge [
    source 19
    target 402
  ]
  edge [
    source 19
    target 403
  ]
  edge [
    source 19
    target 404
  ]
  edge [
    source 19
    target 405
  ]
  edge [
    source 19
    target 406
  ]
  edge [
    source 19
    target 407
  ]
  edge [
    source 19
    target 408
  ]
  edge [
    source 19
    target 409
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 19
    target 1034
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1036
  ]
  edge [
    source 19
    target 1037
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1053
  ]
  edge [
    source 19
    target 685
  ]
  edge [
    source 19
    target 1054
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 671
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 1059
  ]
  edge [
    source 19
    target 693
  ]
  edge [
    source 19
    target 1060
  ]
  edge [
    source 19
    target 1061
  ]
  edge [
    source 19
    target 1062
  ]
  edge [
    source 19
    target 1063
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 1064
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 1068
  ]
  edge [
    source 19
    target 1069
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 678
  ]
  edge [
    source 19
    target 1071
  ]
  edge [
    source 19
    target 1072
  ]
  edge [
    source 19
    target 1073
  ]
  edge [
    source 19
    target 1074
  ]
  edge [
    source 19
    target 1075
  ]
  edge [
    source 19
    target 1076
  ]
  edge [
    source 19
    target 592
  ]
  edge [
    source 19
    target 1077
  ]
  edge [
    source 19
    target 1078
  ]
  edge [
    source 19
    target 1079
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1089
  ]
  edge [
    source 20
    target 1090
  ]
  edge [
    source 20
    target 1091
  ]
  edge [
    source 20
    target 1092
  ]
  edge [
    source 20
    target 1093
  ]
  edge [
    source 20
    target 1094
  ]
  edge [
    source 20
    target 1095
  ]
  edge [
    source 20
    target 1096
  ]
  edge [
    source 20
    target 1097
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 1098
  ]
  edge [
    source 20
    target 1099
  ]
  edge [
    source 20
    target 1100
  ]
  edge [
    source 20
    target 1101
  ]
  edge [
    source 20
    target 1102
  ]
  edge [
    source 20
    target 1103
  ]
  edge [
    source 20
    target 1104
  ]
  edge [
    source 20
    target 1105
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 1106
  ]
  edge [
    source 20
    target 1107
  ]
  edge [
    source 20
    target 1108
  ]
  edge [
    source 20
    target 1109
  ]
  edge [
    source 20
    target 1110
  ]
  edge [
    source 20
    target 251
  ]
  edge [
    source 20
    target 1111
  ]
  edge [
    source 20
    target 1112
  ]
  edge [
    source 20
    target 1113
  ]
  edge [
    source 20
    target 1114
  ]
  edge [
    source 20
    target 1115
  ]
  edge [
    source 20
    target 1116
  ]
  edge [
    source 20
    target 1117
  ]
  edge [
    source 20
    target 1118
  ]
  edge [
    source 20
    target 1119
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 792
  ]
  edge [
    source 20
    target 1120
  ]
  edge [
    source 20
    target 1121
  ]
  edge [
    source 20
    target 1122
  ]
  edge [
    source 20
    target 1123
  ]
  edge [
    source 20
    target 1124
  ]
  edge [
    source 20
    target 1125
  ]
  edge [
    source 20
    target 1126
  ]
  edge [
    source 20
    target 1127
  ]
  edge [
    source 20
    target 1128
  ]
  edge [
    source 20
    target 1129
  ]
  edge [
    source 20
    target 1130
  ]
  edge [
    source 20
    target 1131
  ]
  edge [
    source 20
    target 1132
  ]
  edge [
    source 20
    target 1133
  ]
  edge [
    source 20
    target 1134
  ]
  edge [
    source 20
    target 1135
  ]
  edge [
    source 20
    target 1136
  ]
  edge [
    source 20
    target 1137
  ]
  edge [
    source 20
    target 1138
  ]
  edge [
    source 20
    target 1139
  ]
  edge [
    source 20
    target 1140
  ]
  edge [
    source 20
    target 1141
  ]
  edge [
    source 20
    target 1142
  ]
  edge [
    source 20
    target 1143
  ]
  edge [
    source 20
    target 1144
  ]
  edge [
    source 20
    target 1145
  ]
  edge [
    source 20
    target 1146
  ]
  edge [
    source 20
    target 1147
  ]
  edge [
    source 20
    target 1148
  ]
  edge [
    source 1149
    target 1150
  ]
  edge [
    source 1149
    target 1151
  ]
  edge [
    source 1150
    target 1151
  ]
  edge [
    source 1152
    target 1153
  ]
  edge [
    source 1154
    target 1155
  ]
  edge [
    source 1156
    target 1157
  ]
  edge [
    source 1158
    target 1159
  ]
  edge [
    source 1160
    target 1161
  ]
  edge [
    source 1162
    target 1163
  ]
  edge [
    source 1162
    target 1164
  ]
  edge [
    source 1163
    target 1164
  ]
]
