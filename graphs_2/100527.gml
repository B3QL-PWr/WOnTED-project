graph [
  node [
    id 0
    label "felicite"
    origin "text"
  ]
  node [
    id 1
    label "island"
    origin "text"
  ]
  node [
    id 2
    label "Felicite"
  ]
  node [
    id 3
    label "Island"
  ]
  node [
    id 4
    label "&#206;le"
  ]
  node [
    id 5
    label "F&#233;licit&#233;"
  ]
  node [
    id 6
    label "la"
  ]
  node [
    id 7
    label "Digue"
  ]
  node [
    id 8
    label "wyspa"
  ]
  node [
    id 9
    label "wewn&#281;trzny"
  ]
  node [
    id 10
    label "mary"
  ]
  node [
    id 11
    label "Anne"
  ]
  node [
    id 12
    label "The"
  ]
  node [
    id 13
    label "Sisters"
  ]
  node [
    id 14
    label "coca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
]
