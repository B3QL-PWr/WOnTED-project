graph [
  node [
    id 0
    label "wielki"
    origin "text"
  ]
  node [
    id 1
    label "wojownik"
    origin "text"
  ]
  node [
    id 2
    label "znaczny"
  ]
  node [
    id 3
    label "wyj&#261;tkowy"
  ]
  node [
    id 4
    label "nieprzeci&#281;tny"
  ]
  node [
    id 5
    label "wysoce"
  ]
  node [
    id 6
    label "wa&#380;ny"
  ]
  node [
    id 7
    label "prawdziwy"
  ]
  node [
    id 8
    label "wybitny"
  ]
  node [
    id 9
    label "dupny"
  ]
  node [
    id 10
    label "wysoki"
  ]
  node [
    id 11
    label "intensywnie"
  ]
  node [
    id 12
    label "nieprzeci&#281;tnie"
  ]
  node [
    id 13
    label "niespotykany"
  ]
  node [
    id 14
    label "wydatny"
  ]
  node [
    id 15
    label "wspania&#322;y"
  ]
  node [
    id 16
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 17
    label "&#347;wietny"
  ]
  node [
    id 18
    label "imponuj&#261;cy"
  ]
  node [
    id 19
    label "wybitnie"
  ]
  node [
    id 20
    label "celny"
  ]
  node [
    id 21
    label "&#380;ywny"
  ]
  node [
    id 22
    label "szczery"
  ]
  node [
    id 23
    label "naturalny"
  ]
  node [
    id 24
    label "naprawd&#281;"
  ]
  node [
    id 25
    label "realnie"
  ]
  node [
    id 26
    label "podobny"
  ]
  node [
    id 27
    label "zgodny"
  ]
  node [
    id 28
    label "m&#261;dry"
  ]
  node [
    id 29
    label "prawdziwie"
  ]
  node [
    id 30
    label "wyj&#261;tkowo"
  ]
  node [
    id 31
    label "inny"
  ]
  node [
    id 32
    label "znacznie"
  ]
  node [
    id 33
    label "zauwa&#380;alny"
  ]
  node [
    id 34
    label "wynios&#322;y"
  ]
  node [
    id 35
    label "dono&#347;ny"
  ]
  node [
    id 36
    label "silny"
  ]
  node [
    id 37
    label "wa&#380;nie"
  ]
  node [
    id 38
    label "istotnie"
  ]
  node [
    id 39
    label "eksponowany"
  ]
  node [
    id 40
    label "dobry"
  ]
  node [
    id 41
    label "do_dupy"
  ]
  node [
    id 42
    label "z&#322;y"
  ]
  node [
    id 43
    label "osada"
  ]
  node [
    id 44
    label "&#380;o&#322;nierz"
  ]
  node [
    id 45
    label "walcz&#261;cy"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 48
    label "harcap"
  ]
  node [
    id 49
    label "wojsko"
  ]
  node [
    id 50
    label "elew"
  ]
  node [
    id 51
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 52
    label "demobilizowanie"
  ]
  node [
    id 53
    label "demobilizowa&#263;"
  ]
  node [
    id 54
    label "zdemobilizowanie"
  ]
  node [
    id 55
    label "Gurkha"
  ]
  node [
    id 56
    label "so&#322;dat"
  ]
  node [
    id 57
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 58
    label "mundurowy"
  ]
  node [
    id 59
    label "rota"
  ]
  node [
    id 60
    label "zdemobilizowa&#263;"
  ]
  node [
    id 61
    label "&#380;o&#322;dowy"
  ]
  node [
    id 62
    label "ludzko&#347;&#263;"
  ]
  node [
    id 63
    label "asymilowanie"
  ]
  node [
    id 64
    label "wapniak"
  ]
  node [
    id 65
    label "asymilowa&#263;"
  ]
  node [
    id 66
    label "os&#322;abia&#263;"
  ]
  node [
    id 67
    label "posta&#263;"
  ]
  node [
    id 68
    label "hominid"
  ]
  node [
    id 69
    label "podw&#322;adny"
  ]
  node [
    id 70
    label "os&#322;abianie"
  ]
  node [
    id 71
    label "g&#322;owa"
  ]
  node [
    id 72
    label "figura"
  ]
  node [
    id 73
    label "portrecista"
  ]
  node [
    id 74
    label "dwun&#243;g"
  ]
  node [
    id 75
    label "profanum"
  ]
  node [
    id 76
    label "mikrokosmos"
  ]
  node [
    id 77
    label "nasada"
  ]
  node [
    id 78
    label "duch"
  ]
  node [
    id 79
    label "antropochoria"
  ]
  node [
    id 80
    label "osoba"
  ]
  node [
    id 81
    label "wz&#243;r"
  ]
  node [
    id 82
    label "senior"
  ]
  node [
    id 83
    label "oddzia&#322;ywanie"
  ]
  node [
    id 84
    label "Adam"
  ]
  node [
    id 85
    label "homo_sapiens"
  ]
  node [
    id 86
    label "polifag"
  ]
  node [
    id 87
    label "&#379;ebrowo"
  ]
  node [
    id 88
    label "Nowy_Korczyn"
  ]
  node [
    id 89
    label "Grzybowo"
  ]
  node [
    id 90
    label "crew"
  ]
  node [
    id 91
    label "W&#243;lka"
  ]
  node [
    id 92
    label "Wieniec-Zdr&#243;j"
  ]
  node [
    id 93
    label "kwiatostan"
  ]
  node [
    id 94
    label "&#346;mie&#322;&#243;w"
  ]
  node [
    id 95
    label "zbi&#243;r"
  ]
  node [
    id 96
    label "strza&#322;a"
  ]
  node [
    id 97
    label "Rog&#243;w"
  ]
  node [
    id 98
    label "Gr&#243;dek"
  ]
  node [
    id 99
    label "Antoniewo"
  ]
  node [
    id 100
    label "grupa"
  ]
  node [
    id 101
    label "Grabowiec"
  ]
  node [
    id 102
    label "ochrona"
  ]
  node [
    id 103
    label "dru&#380;yna"
  ]
  node [
    id 104
    label "Babin"
  ]
  node [
    id 105
    label "Falenty"
  ]
  node [
    id 106
    label "Rejowiec"
  ]
  node [
    id 107
    label "Izbica"
  ]
  node [
    id 108
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 109
    label "Sobk&#243;w"
  ]
  node [
    id 110
    label "Paszk&#243;w"
  ]
  node [
    id 111
    label "Go&#322;&#261;bek"
  ]
  node [
    id 112
    label "Gwiazdowo"
  ]
  node [
    id 113
    label "skupienie"
  ]
  node [
    id 114
    label "Pokrowskoje"
  ]
  node [
    id 115
    label "G&#243;rczyn"
  ]
  node [
    id 116
    label "Turlej"
  ]
  node [
    id 117
    label "Skotniki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
]
