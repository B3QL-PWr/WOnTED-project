graph [
  node [
    id 0
    label "playstation"
    origin "text"
  ]
  node [
    id 1
    label "classic"
    origin "text"
  ]
  node [
    id 2
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "celebracja"
    origin "text"
  ]
  node [
    id 5
    label "bogaty"
    origin "text"
  ]
  node [
    id 6
    label "dziedzictwo"
    origin "text"
  ]
  node [
    id 7
    label "pierwsza"
    origin "text"
  ]
  node [
    id 8
    label "konsola"
    origin "text"
  ]
  node [
    id 9
    label "gra"
    origin "text"
  ]
  node [
    id 10
    label "sony"
    origin "text"
  ]
  node [
    id 11
    label "hide"
  ]
  node [
    id 12
    label "czu&#263;"
  ]
  node [
    id 13
    label "support"
  ]
  node [
    id 14
    label "need"
  ]
  node [
    id 15
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 16
    label "wykonawca"
  ]
  node [
    id 17
    label "interpretator"
  ]
  node [
    id 18
    label "cover"
  ]
  node [
    id 19
    label "postrzega&#263;"
  ]
  node [
    id 20
    label "przewidywa&#263;"
  ]
  node [
    id 21
    label "smell"
  ]
  node [
    id 22
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 23
    label "uczuwa&#263;"
  ]
  node [
    id 24
    label "spirit"
  ]
  node [
    id 25
    label "doznawa&#263;"
  ]
  node [
    id 26
    label "anticipate"
  ]
  node [
    id 27
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 28
    label "mie&#263;_miejsce"
  ]
  node [
    id 29
    label "equal"
  ]
  node [
    id 30
    label "trwa&#263;"
  ]
  node [
    id 31
    label "chodzi&#263;"
  ]
  node [
    id 32
    label "si&#281;ga&#263;"
  ]
  node [
    id 33
    label "stan"
  ]
  node [
    id 34
    label "obecno&#347;&#263;"
  ]
  node [
    id 35
    label "stand"
  ]
  node [
    id 36
    label "uczestniczy&#263;"
  ]
  node [
    id 37
    label "participate"
  ]
  node [
    id 38
    label "robi&#263;"
  ]
  node [
    id 39
    label "istnie&#263;"
  ]
  node [
    id 40
    label "pozostawa&#263;"
  ]
  node [
    id 41
    label "zostawa&#263;"
  ]
  node [
    id 42
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 43
    label "adhere"
  ]
  node [
    id 44
    label "compass"
  ]
  node [
    id 45
    label "korzysta&#263;"
  ]
  node [
    id 46
    label "appreciation"
  ]
  node [
    id 47
    label "osi&#261;ga&#263;"
  ]
  node [
    id 48
    label "dociera&#263;"
  ]
  node [
    id 49
    label "get"
  ]
  node [
    id 50
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 51
    label "mierzy&#263;"
  ]
  node [
    id 52
    label "u&#380;ywa&#263;"
  ]
  node [
    id 53
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 54
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 55
    label "exsert"
  ]
  node [
    id 56
    label "being"
  ]
  node [
    id 57
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 58
    label "cecha"
  ]
  node [
    id 59
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 60
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 61
    label "p&#322;ywa&#263;"
  ]
  node [
    id 62
    label "run"
  ]
  node [
    id 63
    label "bangla&#263;"
  ]
  node [
    id 64
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 65
    label "przebiega&#263;"
  ]
  node [
    id 66
    label "wk&#322;ada&#263;"
  ]
  node [
    id 67
    label "proceed"
  ]
  node [
    id 68
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 69
    label "carry"
  ]
  node [
    id 70
    label "bywa&#263;"
  ]
  node [
    id 71
    label "dziama&#263;"
  ]
  node [
    id 72
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 73
    label "stara&#263;_si&#281;"
  ]
  node [
    id 74
    label "para"
  ]
  node [
    id 75
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 76
    label "str&#243;j"
  ]
  node [
    id 77
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 78
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 79
    label "krok"
  ]
  node [
    id 80
    label "tryb"
  ]
  node [
    id 81
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 82
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 83
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 84
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 85
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 86
    label "continue"
  ]
  node [
    id 87
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 88
    label "Ohio"
  ]
  node [
    id 89
    label "wci&#281;cie"
  ]
  node [
    id 90
    label "Nowy_York"
  ]
  node [
    id 91
    label "warstwa"
  ]
  node [
    id 92
    label "samopoczucie"
  ]
  node [
    id 93
    label "Illinois"
  ]
  node [
    id 94
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 95
    label "state"
  ]
  node [
    id 96
    label "Jukatan"
  ]
  node [
    id 97
    label "Kalifornia"
  ]
  node [
    id 98
    label "Wirginia"
  ]
  node [
    id 99
    label "wektor"
  ]
  node [
    id 100
    label "Goa"
  ]
  node [
    id 101
    label "Teksas"
  ]
  node [
    id 102
    label "Waszyngton"
  ]
  node [
    id 103
    label "miejsce"
  ]
  node [
    id 104
    label "Massachusetts"
  ]
  node [
    id 105
    label "Alaska"
  ]
  node [
    id 106
    label "Arakan"
  ]
  node [
    id 107
    label "Hawaje"
  ]
  node [
    id 108
    label "Maryland"
  ]
  node [
    id 109
    label "punkt"
  ]
  node [
    id 110
    label "Michigan"
  ]
  node [
    id 111
    label "Arizona"
  ]
  node [
    id 112
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 113
    label "Georgia"
  ]
  node [
    id 114
    label "poziom"
  ]
  node [
    id 115
    label "Pensylwania"
  ]
  node [
    id 116
    label "shape"
  ]
  node [
    id 117
    label "Luizjana"
  ]
  node [
    id 118
    label "Nowy_Meksyk"
  ]
  node [
    id 119
    label "Alabama"
  ]
  node [
    id 120
    label "ilo&#347;&#263;"
  ]
  node [
    id 121
    label "Kansas"
  ]
  node [
    id 122
    label "Oregon"
  ]
  node [
    id 123
    label "Oklahoma"
  ]
  node [
    id 124
    label "Floryda"
  ]
  node [
    id 125
    label "jednostka_administracyjna"
  ]
  node [
    id 126
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 127
    label "celebra"
  ]
  node [
    id 128
    label "namaszczenie"
  ]
  node [
    id 129
    label "msza"
  ]
  node [
    id 130
    label "Mass"
  ]
  node [
    id 131
    label "dzie&#322;o"
  ]
  node [
    id 132
    label "katolicyzm"
  ]
  node [
    id 133
    label "utw&#243;r"
  ]
  node [
    id 134
    label "ofertorium"
  ]
  node [
    id 135
    label "komunia"
  ]
  node [
    id 136
    label "prefacja"
  ]
  node [
    id 137
    label "ofiarowanie"
  ]
  node [
    id 138
    label "prezbiter"
  ]
  node [
    id 139
    label "przeistoczenie"
  ]
  node [
    id 140
    label "gloria"
  ]
  node [
    id 141
    label "confiteor"
  ]
  node [
    id 142
    label "ewangelia"
  ]
  node [
    id 143
    label "sekreta"
  ]
  node [
    id 144
    label "podniesienie"
  ]
  node [
    id 145
    label "credo"
  ]
  node [
    id 146
    label "episto&#322;a"
  ]
  node [
    id 147
    label "czytanie"
  ]
  node [
    id 148
    label "prawos&#322;awie"
  ]
  node [
    id 149
    label "kazanie"
  ]
  node [
    id 150
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 151
    label "kanon"
  ]
  node [
    id 152
    label "kolekta"
  ]
  node [
    id 153
    label "naznaczenie"
  ]
  node [
    id 154
    label "u&#347;wi&#281;cenie"
  ]
  node [
    id 155
    label "posmarowanie"
  ]
  node [
    id 156
    label "anointing"
  ]
  node [
    id 157
    label "powaga"
  ]
  node [
    id 158
    label "obfituj&#261;cy"
  ]
  node [
    id 159
    label "nabab"
  ]
  node [
    id 160
    label "cz&#322;owiek"
  ]
  node [
    id 161
    label "r&#243;&#380;norodny"
  ]
  node [
    id 162
    label "spania&#322;y"
  ]
  node [
    id 163
    label "obficie"
  ]
  node [
    id 164
    label "sytuowany"
  ]
  node [
    id 165
    label "och&#281;do&#380;ny"
  ]
  node [
    id 166
    label "forsiasty"
  ]
  node [
    id 167
    label "zapa&#347;ny"
  ]
  node [
    id 168
    label "bogato"
  ]
  node [
    id 169
    label "&#347;wietny"
  ]
  node [
    id 170
    label "wspania&#322;y"
  ]
  node [
    id 171
    label "och&#281;do&#380;nie"
  ]
  node [
    id 172
    label "porz&#261;dny"
  ]
  node [
    id 173
    label "ch&#281;dogo"
  ]
  node [
    id 174
    label "smaczny"
  ]
  node [
    id 175
    label "rezerwowy"
  ]
  node [
    id 176
    label "zapa&#347;nie"
  ]
  node [
    id 177
    label "zapasowy"
  ]
  node [
    id 178
    label "urz&#281;dnik"
  ]
  node [
    id 179
    label "bogacz"
  ]
  node [
    id 180
    label "zarz&#261;dca"
  ]
  node [
    id 181
    label "dostojnik"
  ]
  node [
    id 182
    label "obfito"
  ]
  node [
    id 183
    label "obfity"
  ]
  node [
    id 184
    label "pe&#322;no"
  ]
  node [
    id 185
    label "r&#243;&#380;norodnie"
  ]
  node [
    id 186
    label "intensywnie"
  ]
  node [
    id 187
    label "poka&#378;ny"
  ]
  node [
    id 188
    label "r&#243;&#380;ny"
  ]
  node [
    id 189
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 190
    label "ludzko&#347;&#263;"
  ]
  node [
    id 191
    label "asymilowanie"
  ]
  node [
    id 192
    label "wapniak"
  ]
  node [
    id 193
    label "asymilowa&#263;"
  ]
  node [
    id 194
    label "os&#322;abia&#263;"
  ]
  node [
    id 195
    label "posta&#263;"
  ]
  node [
    id 196
    label "hominid"
  ]
  node [
    id 197
    label "podw&#322;adny"
  ]
  node [
    id 198
    label "os&#322;abianie"
  ]
  node [
    id 199
    label "g&#322;owa"
  ]
  node [
    id 200
    label "figura"
  ]
  node [
    id 201
    label "portrecista"
  ]
  node [
    id 202
    label "dwun&#243;g"
  ]
  node [
    id 203
    label "profanum"
  ]
  node [
    id 204
    label "mikrokosmos"
  ]
  node [
    id 205
    label "nasada"
  ]
  node [
    id 206
    label "duch"
  ]
  node [
    id 207
    label "antropochoria"
  ]
  node [
    id 208
    label "osoba"
  ]
  node [
    id 209
    label "wz&#243;r"
  ]
  node [
    id 210
    label "senior"
  ]
  node [
    id 211
    label "oddzia&#322;ywanie"
  ]
  node [
    id 212
    label "Adam"
  ]
  node [
    id 213
    label "homo_sapiens"
  ]
  node [
    id 214
    label "polifag"
  ]
  node [
    id 215
    label "pe&#322;ny"
  ]
  node [
    id 216
    label "zachowek"
  ]
  node [
    id 217
    label "mienie"
  ]
  node [
    id 218
    label "wydziedziczenie"
  ]
  node [
    id 219
    label "scheda_spadkowa"
  ]
  node [
    id 220
    label "sukcesja"
  ]
  node [
    id 221
    label "wydziedziczy&#263;"
  ]
  node [
    id 222
    label "prawo"
  ]
  node [
    id 223
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 224
    label "umocowa&#263;"
  ]
  node [
    id 225
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 226
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 227
    label "procesualistyka"
  ]
  node [
    id 228
    label "regu&#322;a_Allena"
  ]
  node [
    id 229
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 230
    label "kryminalistyka"
  ]
  node [
    id 231
    label "struktura"
  ]
  node [
    id 232
    label "szko&#322;a"
  ]
  node [
    id 233
    label "kierunek"
  ]
  node [
    id 234
    label "zasada_d'Alemberta"
  ]
  node [
    id 235
    label "obserwacja"
  ]
  node [
    id 236
    label "normatywizm"
  ]
  node [
    id 237
    label "jurisprudence"
  ]
  node [
    id 238
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 239
    label "kultura_duchowa"
  ]
  node [
    id 240
    label "przepis"
  ]
  node [
    id 241
    label "prawo_karne_procesowe"
  ]
  node [
    id 242
    label "criterion"
  ]
  node [
    id 243
    label "kazuistyka"
  ]
  node [
    id 244
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 245
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 246
    label "kryminologia"
  ]
  node [
    id 247
    label "opis"
  ]
  node [
    id 248
    label "regu&#322;a_Glogera"
  ]
  node [
    id 249
    label "prawo_Mendla"
  ]
  node [
    id 250
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 251
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 252
    label "prawo_karne"
  ]
  node [
    id 253
    label "legislacyjnie"
  ]
  node [
    id 254
    label "twierdzenie"
  ]
  node [
    id 255
    label "cywilistyka"
  ]
  node [
    id 256
    label "judykatura"
  ]
  node [
    id 257
    label "kanonistyka"
  ]
  node [
    id 258
    label "standard"
  ]
  node [
    id 259
    label "nauka_prawa"
  ]
  node [
    id 260
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 261
    label "podmiot"
  ]
  node [
    id 262
    label "law"
  ]
  node [
    id 263
    label "qualification"
  ]
  node [
    id 264
    label "dominion"
  ]
  node [
    id 265
    label "wykonawczy"
  ]
  node [
    id 266
    label "zasada"
  ]
  node [
    id 267
    label "normalizacja"
  ]
  node [
    id 268
    label "przej&#347;cie"
  ]
  node [
    id 269
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 270
    label "rodowo&#347;&#263;"
  ]
  node [
    id 271
    label "patent"
  ]
  node [
    id 272
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 273
    label "dobra"
  ]
  node [
    id 274
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 275
    label "przej&#347;&#263;"
  ]
  node [
    id 276
    label "possession"
  ]
  node [
    id 277
    label "spadek"
  ]
  node [
    id 278
    label "zabranie"
  ]
  node [
    id 279
    label "disinheritance"
  ]
  node [
    id 280
    label "zabra&#263;"
  ]
  node [
    id 281
    label "disinherit"
  ]
  node [
    id 282
    label "proces_biologiczny"
  ]
  node [
    id 283
    label "seniorat"
  ]
  node [
    id 284
    label "godzina"
  ]
  node [
    id 285
    label "time"
  ]
  node [
    id 286
    label "doba"
  ]
  node [
    id 287
    label "p&#243;&#322;godzina"
  ]
  node [
    id 288
    label "jednostka_czasu"
  ]
  node [
    id 289
    label "czas"
  ]
  node [
    id 290
    label "minuta"
  ]
  node [
    id 291
    label "kwadrans"
  ]
  node [
    id 292
    label "pad"
  ]
  node [
    id 293
    label "tremo"
  ]
  node [
    id 294
    label "ozdobny"
  ]
  node [
    id 295
    label "stolik"
  ]
  node [
    id 296
    label "pulpit"
  ]
  node [
    id 297
    label "wspornik"
  ]
  node [
    id 298
    label "urz&#261;dzenie"
  ]
  node [
    id 299
    label "przedmiot"
  ]
  node [
    id 300
    label "kom&#243;rka"
  ]
  node [
    id 301
    label "furnishing"
  ]
  node [
    id 302
    label "zabezpieczenie"
  ]
  node [
    id 303
    label "zrobienie"
  ]
  node [
    id 304
    label "wyrz&#261;dzenie"
  ]
  node [
    id 305
    label "zagospodarowanie"
  ]
  node [
    id 306
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 307
    label "ig&#322;a"
  ]
  node [
    id 308
    label "narz&#281;dzie"
  ]
  node [
    id 309
    label "wirnik"
  ]
  node [
    id 310
    label "aparatura"
  ]
  node [
    id 311
    label "system_energetyczny"
  ]
  node [
    id 312
    label "impulsator"
  ]
  node [
    id 313
    label "mechanizm"
  ]
  node [
    id 314
    label "sprz&#281;t"
  ]
  node [
    id 315
    label "czynno&#347;&#263;"
  ]
  node [
    id 316
    label "blokowanie"
  ]
  node [
    id 317
    label "set"
  ]
  node [
    id 318
    label "zablokowanie"
  ]
  node [
    id 319
    label "przygotowanie"
  ]
  node [
    id 320
    label "komora"
  ]
  node [
    id 321
    label "j&#281;zyk"
  ]
  node [
    id 322
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 323
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 324
    label "blat"
  ]
  node [
    id 325
    label "interfejs"
  ]
  node [
    id 326
    label "okno"
  ]
  node [
    id 327
    label "obszar"
  ]
  node [
    id 328
    label "ikona"
  ]
  node [
    id 329
    label "podstawa"
  ]
  node [
    id 330
    label "system_operacyjny"
  ]
  node [
    id 331
    label "mebel"
  ]
  node [
    id 332
    label "czwarty"
  ]
  node [
    id 333
    label "st&#243;&#322;"
  ]
  node [
    id 334
    label "bryd&#380;ysta"
  ]
  node [
    id 335
    label "partner"
  ]
  node [
    id 336
    label "grupa"
  ]
  node [
    id 337
    label "podpora"
  ]
  node [
    id 338
    label "element"
  ]
  node [
    id 339
    label "podci&#261;gnik"
  ]
  node [
    id 340
    label "komputer"
  ]
  node [
    id 341
    label "kontroler_gier"
  ]
  node [
    id 342
    label "lustro"
  ]
  node [
    id 343
    label "&#322;adny"
  ]
  node [
    id 344
    label "ozdobnie"
  ]
  node [
    id 345
    label "zmienno&#347;&#263;"
  ]
  node [
    id 346
    label "play"
  ]
  node [
    id 347
    label "rozgrywka"
  ]
  node [
    id 348
    label "apparent_motion"
  ]
  node [
    id 349
    label "wydarzenie"
  ]
  node [
    id 350
    label "contest"
  ]
  node [
    id 351
    label "akcja"
  ]
  node [
    id 352
    label "komplet"
  ]
  node [
    id 353
    label "zabawa"
  ]
  node [
    id 354
    label "rywalizacja"
  ]
  node [
    id 355
    label "zbijany"
  ]
  node [
    id 356
    label "post&#281;powanie"
  ]
  node [
    id 357
    label "game"
  ]
  node [
    id 358
    label "odg&#322;os"
  ]
  node [
    id 359
    label "Pok&#233;mon"
  ]
  node [
    id 360
    label "synteza"
  ]
  node [
    id 361
    label "odtworzenie"
  ]
  node [
    id 362
    label "rekwizyt_do_gry"
  ]
  node [
    id 363
    label "resonance"
  ]
  node [
    id 364
    label "wydanie"
  ]
  node [
    id 365
    label "wpadni&#281;cie"
  ]
  node [
    id 366
    label "d&#378;wi&#281;k"
  ]
  node [
    id 367
    label "wpadanie"
  ]
  node [
    id 368
    label "wydawa&#263;"
  ]
  node [
    id 369
    label "sound"
  ]
  node [
    id 370
    label "brzmienie"
  ]
  node [
    id 371
    label "zjawisko"
  ]
  node [
    id 372
    label "wyda&#263;"
  ]
  node [
    id 373
    label "wpa&#347;&#263;"
  ]
  node [
    id 374
    label "note"
  ]
  node [
    id 375
    label "onomatopeja"
  ]
  node [
    id 376
    label "wpada&#263;"
  ]
  node [
    id 377
    label "s&#261;d"
  ]
  node [
    id 378
    label "kognicja"
  ]
  node [
    id 379
    label "campaign"
  ]
  node [
    id 380
    label "rozprawa"
  ]
  node [
    id 381
    label "zachowanie"
  ]
  node [
    id 382
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 383
    label "fashion"
  ]
  node [
    id 384
    label "robienie"
  ]
  node [
    id 385
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 386
    label "zmierzanie"
  ]
  node [
    id 387
    label "przes&#322;anka"
  ]
  node [
    id 388
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 389
    label "trafienie"
  ]
  node [
    id 390
    label "rewan&#380;owy"
  ]
  node [
    id 391
    label "zagrywka"
  ]
  node [
    id 392
    label "faza"
  ]
  node [
    id 393
    label "euroliga"
  ]
  node [
    id 394
    label "interliga"
  ]
  node [
    id 395
    label "runda"
  ]
  node [
    id 396
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 397
    label "rozrywka"
  ]
  node [
    id 398
    label "impreza"
  ]
  node [
    id 399
    label "igraszka"
  ]
  node [
    id 400
    label "taniec"
  ]
  node [
    id 401
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 402
    label "gambling"
  ]
  node [
    id 403
    label "chwyt"
  ]
  node [
    id 404
    label "igra"
  ]
  node [
    id 405
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 406
    label "nabawienie_si&#281;"
  ]
  node [
    id 407
    label "ubaw"
  ]
  node [
    id 408
    label "wodzirej"
  ]
  node [
    id 409
    label "activity"
  ]
  node [
    id 410
    label "bezproblemowy"
  ]
  node [
    id 411
    label "przebiec"
  ]
  node [
    id 412
    label "charakter"
  ]
  node [
    id 413
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 414
    label "motyw"
  ]
  node [
    id 415
    label "przebiegni&#281;cie"
  ]
  node [
    id 416
    label "fabu&#322;a"
  ]
  node [
    id 417
    label "proces_technologiczny"
  ]
  node [
    id 418
    label "mieszanina"
  ]
  node [
    id 419
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 420
    label "fusion"
  ]
  node [
    id 421
    label "poj&#281;cie"
  ]
  node [
    id 422
    label "reakcja_chemiczna"
  ]
  node [
    id 423
    label "zestawienie"
  ]
  node [
    id 424
    label "uog&#243;lnienie"
  ]
  node [
    id 425
    label "puszczenie"
  ]
  node [
    id 426
    label "ustalenie"
  ]
  node [
    id 427
    label "wyst&#281;p"
  ]
  node [
    id 428
    label "reproduction"
  ]
  node [
    id 429
    label "przedstawienie"
  ]
  node [
    id 430
    label "przywr&#243;cenie"
  ]
  node [
    id 431
    label "w&#322;&#261;czenie"
  ]
  node [
    id 432
    label "zregenerowanie_si&#281;"
  ]
  node [
    id 433
    label "restoration"
  ]
  node [
    id 434
    label "odbudowanie"
  ]
  node [
    id 435
    label "lekcja"
  ]
  node [
    id 436
    label "ensemble"
  ]
  node [
    id 437
    label "klasa"
  ]
  node [
    id 438
    label "zestaw"
  ]
  node [
    id 439
    label "base"
  ]
  node [
    id 440
    label "umowa"
  ]
  node [
    id 441
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 442
    label "moralno&#347;&#263;"
  ]
  node [
    id 443
    label "spos&#243;b"
  ]
  node [
    id 444
    label "occupation"
  ]
  node [
    id 445
    label "substancja"
  ]
  node [
    id 446
    label "prawid&#322;o"
  ]
  node [
    id 447
    label "dywidenda"
  ]
  node [
    id 448
    label "przebieg"
  ]
  node [
    id 449
    label "operacja"
  ]
  node [
    id 450
    label "udzia&#322;"
  ]
  node [
    id 451
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 452
    label "commotion"
  ]
  node [
    id 453
    label "jazda"
  ]
  node [
    id 454
    label "czyn"
  ]
  node [
    id 455
    label "stock"
  ]
  node [
    id 456
    label "sp&#243;&#322;ka_akcyjna"
  ]
  node [
    id 457
    label "w&#281;ze&#322;"
  ]
  node [
    id 458
    label "wysoko&#347;&#263;"
  ]
  node [
    id 459
    label "instrument_strunowy"
  ]
  node [
    id 460
    label "pi&#322;ka"
  ]
  node [
    id 461
    label "Classic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 461
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 9
    target 350
  ]
  edge [
    source 9
    target 351
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 353
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 354
  ]
  edge [
    source 9
    target 355
  ]
  edge [
    source 9
    target 356
  ]
  edge [
    source 9
    target 357
  ]
  edge [
    source 9
    target 358
  ]
  edge [
    source 9
    target 359
  ]
  edge [
    source 9
    target 315
  ]
  edge [
    source 9
    target 360
  ]
  edge [
    source 9
    target 361
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 376
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 377
  ]
  edge [
    source 9
    target 378
  ]
  edge [
    source 9
    target 379
  ]
  edge [
    source 9
    target 380
  ]
  edge [
    source 9
    target 381
  ]
  edge [
    source 9
    target 382
  ]
  edge [
    source 9
    target 383
  ]
  edge [
    source 9
    target 384
  ]
  edge [
    source 9
    target 385
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 329
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
]
