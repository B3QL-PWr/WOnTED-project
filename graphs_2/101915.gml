graph [
  node [
    id 0
    label "obwieszczenie"
    origin "text"
  ]
  node [
    id 1
    label "enunciation"
  ]
  node [
    id 2
    label "remark"
  ]
  node [
    id 3
    label "podanie"
  ]
  node [
    id 4
    label "dowiedzenie"
  ]
  node [
    id 5
    label "komunikat"
  ]
  node [
    id 6
    label "ustawienie"
  ]
  node [
    id 7
    label "danie"
  ]
  node [
    id 8
    label "narrative"
  ]
  node [
    id 9
    label "pismo"
  ]
  node [
    id 10
    label "nafaszerowanie"
  ]
  node [
    id 11
    label "tenis"
  ]
  node [
    id 12
    label "prayer"
  ]
  node [
    id 13
    label "siatk&#243;wka"
  ]
  node [
    id 14
    label "pi&#322;ka"
  ]
  node [
    id 15
    label "give"
  ]
  node [
    id 16
    label "myth"
  ]
  node [
    id 17
    label "service"
  ]
  node [
    id 18
    label "jedzenie"
  ]
  node [
    id 19
    label "zagranie"
  ]
  node [
    id 20
    label "poinformowanie"
  ]
  node [
    id 21
    label "zaserwowanie"
  ]
  node [
    id 22
    label "opowie&#347;&#263;"
  ]
  node [
    id 23
    label "pass"
  ]
  node [
    id 24
    label "communication"
  ]
  node [
    id 25
    label "kreacjonista"
  ]
  node [
    id 26
    label "roi&#263;_si&#281;"
  ]
  node [
    id 27
    label "wytw&#243;r"
  ]
  node [
    id 28
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 29
    label "doprowadzenie"
  ]
  node [
    id 30
    label "certificate"
  ]
  node [
    id 31
    label "assertion"
  ]
  node [
    id 32
    label "zanalizowanie"
  ]
  node [
    id 33
    label "uzasadnienie"
  ]
  node [
    id 34
    label "zrobienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
]
