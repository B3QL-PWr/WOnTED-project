graph [
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "atrapa"
    origin "text"
  ]
  node [
    id 2
    label "radiow&#243;z"
    origin "text"
  ]
  node [
    id 3
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 4
    label "sta&#322;y"
    origin "text"
  ]
  node [
    id 5
    label "kiedy&#347;"
    origin "text"
  ]
  node [
    id 6
    label "przy"
    origin "text"
  ]
  node [
    id 7
    label "droga"
    origin "text"
  ]
  node [
    id 8
    label "krajowy"
    origin "text"
  ]
  node [
    id 9
    label "echo"
  ]
  node [
    id 10
    label "pilnowa&#263;"
  ]
  node [
    id 11
    label "robi&#263;"
  ]
  node [
    id 12
    label "recall"
  ]
  node [
    id 13
    label "si&#281;ga&#263;"
  ]
  node [
    id 14
    label "take_care"
  ]
  node [
    id 15
    label "troska&#263;_si&#281;"
  ]
  node [
    id 16
    label "chowa&#263;"
  ]
  node [
    id 17
    label "zachowywa&#263;"
  ]
  node [
    id 18
    label "zna&#263;"
  ]
  node [
    id 19
    label "think"
  ]
  node [
    id 20
    label "report"
  ]
  node [
    id 21
    label "hide"
  ]
  node [
    id 22
    label "znosi&#263;"
  ]
  node [
    id 23
    label "czu&#263;"
  ]
  node [
    id 24
    label "train"
  ]
  node [
    id 25
    label "przetrzymywa&#263;"
  ]
  node [
    id 26
    label "hodowa&#263;"
  ]
  node [
    id 27
    label "meliniarz"
  ]
  node [
    id 28
    label "umieszcza&#263;"
  ]
  node [
    id 29
    label "ukrywa&#263;"
  ]
  node [
    id 30
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 31
    label "continue"
  ]
  node [
    id 32
    label "wk&#322;ada&#263;"
  ]
  node [
    id 33
    label "tajemnica"
  ]
  node [
    id 34
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 35
    label "zdyscyplinowanie"
  ]
  node [
    id 36
    label "podtrzymywa&#263;"
  ]
  node [
    id 37
    label "post"
  ]
  node [
    id 38
    label "control"
  ]
  node [
    id 39
    label "przechowywa&#263;"
  ]
  node [
    id 40
    label "behave"
  ]
  node [
    id 41
    label "dieta"
  ]
  node [
    id 42
    label "hold"
  ]
  node [
    id 43
    label "post&#281;powa&#263;"
  ]
  node [
    id 44
    label "compass"
  ]
  node [
    id 45
    label "korzysta&#263;"
  ]
  node [
    id 46
    label "appreciation"
  ]
  node [
    id 47
    label "osi&#261;ga&#263;"
  ]
  node [
    id 48
    label "dociera&#263;"
  ]
  node [
    id 49
    label "get"
  ]
  node [
    id 50
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 51
    label "mierzy&#263;"
  ]
  node [
    id 52
    label "u&#380;ywa&#263;"
  ]
  node [
    id 53
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 54
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 55
    label "exsert"
  ]
  node [
    id 56
    label "organizowa&#263;"
  ]
  node [
    id 57
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 58
    label "czyni&#263;"
  ]
  node [
    id 59
    label "give"
  ]
  node [
    id 60
    label "stylizowa&#263;"
  ]
  node [
    id 61
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 62
    label "falowa&#263;"
  ]
  node [
    id 63
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 64
    label "peddle"
  ]
  node [
    id 65
    label "praca"
  ]
  node [
    id 66
    label "wydala&#263;"
  ]
  node [
    id 67
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 68
    label "tentegowa&#263;"
  ]
  node [
    id 69
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 70
    label "urz&#261;dza&#263;"
  ]
  node [
    id 71
    label "oszukiwa&#263;"
  ]
  node [
    id 72
    label "work"
  ]
  node [
    id 73
    label "ukazywa&#263;"
  ]
  node [
    id 74
    label "przerabia&#263;"
  ]
  node [
    id 75
    label "act"
  ]
  node [
    id 76
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 77
    label "cognizance"
  ]
  node [
    id 78
    label "wiedzie&#263;"
  ]
  node [
    id 79
    label "resonance"
  ]
  node [
    id 80
    label "zjawisko"
  ]
  node [
    id 81
    label "os&#322;ona"
  ]
  node [
    id 82
    label "mock-up"
  ]
  node [
    id 83
    label "imitacja"
  ]
  node [
    id 84
    label "radiator"
  ]
  node [
    id 85
    label "poz&#243;r"
  ]
  node [
    id 86
    label "nieprawda"
  ]
  node [
    id 87
    label "semblance"
  ]
  node [
    id 88
    label "ochrona"
  ]
  node [
    id 89
    label "oddzia&#322;"
  ]
  node [
    id 90
    label "operacja"
  ]
  node [
    id 91
    label "farmaceutyk"
  ]
  node [
    id 92
    label "technika"
  ]
  node [
    id 93
    label "praktyka"
  ]
  node [
    id 94
    label "na&#347;ladownictwo"
  ]
  node [
    id 95
    label "silnik"
  ]
  node [
    id 96
    label "wzmacniacz"
  ]
  node [
    id 97
    label "regulator"
  ]
  node [
    id 98
    label "samoch&#243;d"
  ]
  node [
    id 99
    label "misiow&#243;z"
  ]
  node [
    id 100
    label "pojazd_drogowy"
  ]
  node [
    id 101
    label "spryskiwacz"
  ]
  node [
    id 102
    label "most"
  ]
  node [
    id 103
    label "baga&#380;nik"
  ]
  node [
    id 104
    label "dachowanie"
  ]
  node [
    id 105
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 106
    label "pompa_wodna"
  ]
  node [
    id 107
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 108
    label "poduszka_powietrzna"
  ]
  node [
    id 109
    label "tempomat"
  ]
  node [
    id 110
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 111
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 112
    label "deska_rozdzielcza"
  ]
  node [
    id 113
    label "immobilizer"
  ]
  node [
    id 114
    label "t&#322;umik"
  ]
  node [
    id 115
    label "ABS"
  ]
  node [
    id 116
    label "kierownica"
  ]
  node [
    id 117
    label "bak"
  ]
  node [
    id 118
    label "dwu&#347;lad"
  ]
  node [
    id 119
    label "poci&#261;g_drogowy"
  ]
  node [
    id 120
    label "wycieraczka"
  ]
  node [
    id 121
    label "regularny"
  ]
  node [
    id 122
    label "jednakowy"
  ]
  node [
    id 123
    label "zwyk&#322;y"
  ]
  node [
    id 124
    label "stale"
  ]
  node [
    id 125
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 126
    label "zr&#243;wnanie"
  ]
  node [
    id 127
    label "mundurowanie"
  ]
  node [
    id 128
    label "taki&#380;"
  ]
  node [
    id 129
    label "jednakowo"
  ]
  node [
    id 130
    label "mundurowa&#263;"
  ]
  node [
    id 131
    label "zr&#243;wnywanie"
  ]
  node [
    id 132
    label "identyczny"
  ]
  node [
    id 133
    label "przeci&#281;tny"
  ]
  node [
    id 134
    label "zwyczajnie"
  ]
  node [
    id 135
    label "zwykle"
  ]
  node [
    id 136
    label "cz&#281;sty"
  ]
  node [
    id 137
    label "okre&#347;lony"
  ]
  node [
    id 138
    label "zorganizowany"
  ]
  node [
    id 139
    label "powtarzalny"
  ]
  node [
    id 140
    label "regularnie"
  ]
  node [
    id 141
    label "harmonijny"
  ]
  node [
    id 142
    label "zawsze"
  ]
  node [
    id 143
    label "ekskursja"
  ]
  node [
    id 144
    label "bezsilnikowy"
  ]
  node [
    id 145
    label "budowla"
  ]
  node [
    id 146
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 147
    label "trasa"
  ]
  node [
    id 148
    label "podbieg"
  ]
  node [
    id 149
    label "turystyka"
  ]
  node [
    id 150
    label "nawierzchnia"
  ]
  node [
    id 151
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 152
    label "rajza"
  ]
  node [
    id 153
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 154
    label "korona_drogi"
  ]
  node [
    id 155
    label "passage"
  ]
  node [
    id 156
    label "wylot"
  ]
  node [
    id 157
    label "ekwipunek"
  ]
  node [
    id 158
    label "zbior&#243;wka"
  ]
  node [
    id 159
    label "marszrutyzacja"
  ]
  node [
    id 160
    label "wyb&#243;j"
  ]
  node [
    id 161
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 162
    label "drogowskaz"
  ]
  node [
    id 163
    label "spos&#243;b"
  ]
  node [
    id 164
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 165
    label "pobocze"
  ]
  node [
    id 166
    label "journey"
  ]
  node [
    id 167
    label "ruch"
  ]
  node [
    id 168
    label "przebieg"
  ]
  node [
    id 169
    label "infrastruktura"
  ]
  node [
    id 170
    label "w&#281;ze&#322;"
  ]
  node [
    id 171
    label "obudowanie"
  ]
  node [
    id 172
    label "obudowywa&#263;"
  ]
  node [
    id 173
    label "zbudowa&#263;"
  ]
  node [
    id 174
    label "obudowa&#263;"
  ]
  node [
    id 175
    label "kolumnada"
  ]
  node [
    id 176
    label "korpus"
  ]
  node [
    id 177
    label "Sukiennice"
  ]
  node [
    id 178
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 179
    label "fundament"
  ]
  node [
    id 180
    label "postanie"
  ]
  node [
    id 181
    label "obudowywanie"
  ]
  node [
    id 182
    label "zbudowanie"
  ]
  node [
    id 183
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 184
    label "stan_surowy"
  ]
  node [
    id 185
    label "konstrukcja"
  ]
  node [
    id 186
    label "rzecz"
  ]
  node [
    id 187
    label "model"
  ]
  node [
    id 188
    label "narz&#281;dzie"
  ]
  node [
    id 189
    label "zbi&#243;r"
  ]
  node [
    id 190
    label "tryb"
  ]
  node [
    id 191
    label "nature"
  ]
  node [
    id 192
    label "ton"
  ]
  node [
    id 193
    label "rozmiar"
  ]
  node [
    id 194
    label "odcinek"
  ]
  node [
    id 195
    label "ambitus"
  ]
  node [
    id 196
    label "czas"
  ]
  node [
    id 197
    label "skala"
  ]
  node [
    id 198
    label "mechanika"
  ]
  node [
    id 199
    label "utrzymywanie"
  ]
  node [
    id 200
    label "move"
  ]
  node [
    id 201
    label "poruszenie"
  ]
  node [
    id 202
    label "movement"
  ]
  node [
    id 203
    label "myk"
  ]
  node [
    id 204
    label "utrzyma&#263;"
  ]
  node [
    id 205
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 206
    label "utrzymanie"
  ]
  node [
    id 207
    label "travel"
  ]
  node [
    id 208
    label "kanciasty"
  ]
  node [
    id 209
    label "commercial_enterprise"
  ]
  node [
    id 210
    label "strumie&#324;"
  ]
  node [
    id 211
    label "proces"
  ]
  node [
    id 212
    label "aktywno&#347;&#263;"
  ]
  node [
    id 213
    label "kr&#243;tki"
  ]
  node [
    id 214
    label "taktyka"
  ]
  node [
    id 215
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 216
    label "apraksja"
  ]
  node [
    id 217
    label "natural_process"
  ]
  node [
    id 218
    label "utrzymywa&#263;"
  ]
  node [
    id 219
    label "d&#322;ugi"
  ]
  node [
    id 220
    label "wydarzenie"
  ]
  node [
    id 221
    label "dyssypacja_energii"
  ]
  node [
    id 222
    label "tumult"
  ]
  node [
    id 223
    label "stopek"
  ]
  node [
    id 224
    label "czynno&#347;&#263;"
  ]
  node [
    id 225
    label "zmiana"
  ]
  node [
    id 226
    label "manewr"
  ]
  node [
    id 227
    label "lokomocja"
  ]
  node [
    id 228
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 229
    label "komunikacja"
  ]
  node [
    id 230
    label "drift"
  ]
  node [
    id 231
    label "r&#281;kaw"
  ]
  node [
    id 232
    label "kontusz"
  ]
  node [
    id 233
    label "koniec"
  ]
  node [
    id 234
    label "otw&#243;r"
  ]
  node [
    id 235
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 236
    label "warstwa"
  ]
  node [
    id 237
    label "pokrycie"
  ]
  node [
    id 238
    label "fingerpost"
  ]
  node [
    id 239
    label "tablica"
  ]
  node [
    id 240
    label "przydro&#380;e"
  ]
  node [
    id 241
    label "autostrada"
  ]
  node [
    id 242
    label "bieg"
  ]
  node [
    id 243
    label "podr&#243;&#380;"
  ]
  node [
    id 244
    label "mieszanie_si&#281;"
  ]
  node [
    id 245
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 246
    label "chodzenie"
  ]
  node [
    id 247
    label "digress"
  ]
  node [
    id 248
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 249
    label "pozostawa&#263;"
  ]
  node [
    id 250
    label "s&#261;dzi&#263;"
  ]
  node [
    id 251
    label "chodzi&#263;"
  ]
  node [
    id 252
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 253
    label "stray"
  ]
  node [
    id 254
    label "kocher"
  ]
  node [
    id 255
    label "wyposa&#380;enie"
  ]
  node [
    id 256
    label "nie&#347;miertelnik"
  ]
  node [
    id 257
    label "moderunek"
  ]
  node [
    id 258
    label "dormitorium"
  ]
  node [
    id 259
    label "sk&#322;adanka"
  ]
  node [
    id 260
    label "wyprawa"
  ]
  node [
    id 261
    label "polowanie"
  ]
  node [
    id 262
    label "spis"
  ]
  node [
    id 263
    label "pomieszczenie"
  ]
  node [
    id 264
    label "fotografia"
  ]
  node [
    id 265
    label "beznap&#281;dowy"
  ]
  node [
    id 266
    label "cz&#322;owiek"
  ]
  node [
    id 267
    label "ukochanie"
  ]
  node [
    id 268
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 269
    label "feblik"
  ]
  node [
    id 270
    label "podnieci&#263;"
  ]
  node [
    id 271
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 272
    label "numer"
  ]
  node [
    id 273
    label "po&#380;ycie"
  ]
  node [
    id 274
    label "tendency"
  ]
  node [
    id 275
    label "podniecenie"
  ]
  node [
    id 276
    label "afekt"
  ]
  node [
    id 277
    label "zakochanie"
  ]
  node [
    id 278
    label "zajawka"
  ]
  node [
    id 279
    label "seks"
  ]
  node [
    id 280
    label "podniecanie"
  ]
  node [
    id 281
    label "imisja"
  ]
  node [
    id 282
    label "love"
  ]
  node [
    id 283
    label "rozmna&#380;anie"
  ]
  node [
    id 284
    label "ruch_frykcyjny"
  ]
  node [
    id 285
    label "na_pieska"
  ]
  node [
    id 286
    label "serce"
  ]
  node [
    id 287
    label "pozycja_misjonarska"
  ]
  node [
    id 288
    label "wi&#281;&#378;"
  ]
  node [
    id 289
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 290
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 291
    label "z&#322;&#261;czenie"
  ]
  node [
    id 292
    label "gra_wst&#281;pna"
  ]
  node [
    id 293
    label "erotyka"
  ]
  node [
    id 294
    label "emocja"
  ]
  node [
    id 295
    label "baraszki"
  ]
  node [
    id 296
    label "drogi"
  ]
  node [
    id 297
    label "po&#380;&#261;danie"
  ]
  node [
    id 298
    label "wzw&#243;d"
  ]
  node [
    id 299
    label "podnieca&#263;"
  ]
  node [
    id 300
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 301
    label "kochanka"
  ]
  node [
    id 302
    label "kultura_fizyczna"
  ]
  node [
    id 303
    label "turyzm"
  ]
  node [
    id 304
    label "rodzimy"
  ]
  node [
    id 305
    label "w&#322;asny"
  ]
  node [
    id 306
    label "tutejszy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
]
