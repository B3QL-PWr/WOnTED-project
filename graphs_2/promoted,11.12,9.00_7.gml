graph [
  node [
    id 0
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 1
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 2
    label "po&#322;udnie"
    origin "text"
  ]
  node [
    id 3
    label "problem"
    origin "text"
  ]
  node [
    id 4
    label "dost&#281;p"
    origin "text"
  ]
  node [
    id 5
    label "us&#322;uga"
    origin "text"
  ]
  node [
    id 6
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zg&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 8
    label "klient"
    origin "text"
  ]
  node [
    id 9
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 10
    label "pi&#261;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 12
    label "bank"
    origin "text"
  ]
  node [
    id 13
    label "naraz"
    origin "text"
  ]
  node [
    id 14
    label "doba"
  ]
  node [
    id 15
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 16
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 17
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 18
    label "dzi&#347;"
  ]
  node [
    id 19
    label "teraz"
  ]
  node [
    id 20
    label "czas"
  ]
  node [
    id 21
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 22
    label "jednocze&#347;nie"
  ]
  node [
    id 23
    label "tydzie&#324;"
  ]
  node [
    id 24
    label "noc"
  ]
  node [
    id 25
    label "dzie&#324;"
  ]
  node [
    id 26
    label "godzina"
  ]
  node [
    id 27
    label "long_time"
  ]
  node [
    id 28
    label "jednostka_geologiczna"
  ]
  node [
    id 29
    label "&#347;rodek"
  ]
  node [
    id 30
    label "obszar"
  ]
  node [
    id 31
    label "Ziemia"
  ]
  node [
    id 32
    label "dwunasta"
  ]
  node [
    id 33
    label "strona_&#347;wiata"
  ]
  node [
    id 34
    label "pora"
  ]
  node [
    id 35
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 36
    label "punkt"
  ]
  node [
    id 37
    label "spos&#243;b"
  ]
  node [
    id 38
    label "miejsce"
  ]
  node [
    id 39
    label "abstrakcja"
  ]
  node [
    id 40
    label "chemikalia"
  ]
  node [
    id 41
    label "substancja"
  ]
  node [
    id 42
    label "run"
  ]
  node [
    id 43
    label "okres_czasu"
  ]
  node [
    id 44
    label "time"
  ]
  node [
    id 45
    label "p&#243;&#322;godzina"
  ]
  node [
    id 46
    label "jednostka_czasu"
  ]
  node [
    id 47
    label "minuta"
  ]
  node [
    id 48
    label "kwadrans"
  ]
  node [
    id 49
    label "Rzym_Zachodni"
  ]
  node [
    id 50
    label "whole"
  ]
  node [
    id 51
    label "ilo&#347;&#263;"
  ]
  node [
    id 52
    label "element"
  ]
  node [
    id 53
    label "Rzym_Wschodni"
  ]
  node [
    id 54
    label "urz&#261;dzenie"
  ]
  node [
    id 55
    label "p&#243;&#322;nocek"
  ]
  node [
    id 56
    label "ranek"
  ]
  node [
    id 57
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 58
    label "podwiecz&#243;r"
  ]
  node [
    id 59
    label "przedpo&#322;udnie"
  ]
  node [
    id 60
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 61
    label "wiecz&#243;r"
  ]
  node [
    id 62
    label "t&#322;usty_czwartek"
  ]
  node [
    id 63
    label "popo&#322;udnie"
  ]
  node [
    id 64
    label "walentynki"
  ]
  node [
    id 65
    label "czynienie_si&#281;"
  ]
  node [
    id 66
    label "s&#322;o&#324;ce"
  ]
  node [
    id 67
    label "rano"
  ]
  node [
    id 68
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 69
    label "wzej&#347;cie"
  ]
  node [
    id 70
    label "wsta&#263;"
  ]
  node [
    id 71
    label "day"
  ]
  node [
    id 72
    label "termin"
  ]
  node [
    id 73
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 74
    label "wstanie"
  ]
  node [
    id 75
    label "przedwiecz&#243;r"
  ]
  node [
    id 76
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 77
    label "Sylwester"
  ]
  node [
    id 78
    label "Stary_&#346;wiat"
  ]
  node [
    id 79
    label "p&#243;&#322;noc"
  ]
  node [
    id 80
    label "geosfera"
  ]
  node [
    id 81
    label "przyroda"
  ]
  node [
    id 82
    label "rze&#378;ba"
  ]
  node [
    id 83
    label "morze"
  ]
  node [
    id 84
    label "hydrosfera"
  ]
  node [
    id 85
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 86
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 87
    label "geotermia"
  ]
  node [
    id 88
    label "ozonosfera"
  ]
  node [
    id 89
    label "biosfera"
  ]
  node [
    id 90
    label "magnetosfera"
  ]
  node [
    id 91
    label "Nowy_&#346;wiat"
  ]
  node [
    id 92
    label "biegun"
  ]
  node [
    id 93
    label "litosfera"
  ]
  node [
    id 94
    label "mikrokosmos"
  ]
  node [
    id 95
    label "p&#243;&#322;kula"
  ]
  node [
    id 96
    label "barysfera"
  ]
  node [
    id 97
    label "atmosfera"
  ]
  node [
    id 98
    label "geoida"
  ]
  node [
    id 99
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 100
    label "Kosowo"
  ]
  node [
    id 101
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 102
    label "Zab&#322;ocie"
  ]
  node [
    id 103
    label "zach&#243;d"
  ]
  node [
    id 104
    label "Pow&#261;zki"
  ]
  node [
    id 105
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 106
    label "Piotrowo"
  ]
  node [
    id 107
    label "Olszanica"
  ]
  node [
    id 108
    label "zbi&#243;r"
  ]
  node [
    id 109
    label "Ruda_Pabianicka"
  ]
  node [
    id 110
    label "holarktyka"
  ]
  node [
    id 111
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 112
    label "Ludwin&#243;w"
  ]
  node [
    id 113
    label "Arktyka"
  ]
  node [
    id 114
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 115
    label "Zabu&#380;e"
  ]
  node [
    id 116
    label "antroposfera"
  ]
  node [
    id 117
    label "Neogea"
  ]
  node [
    id 118
    label "terytorium"
  ]
  node [
    id 119
    label "Syberia_Zachodnia"
  ]
  node [
    id 120
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 121
    label "zakres"
  ]
  node [
    id 122
    label "pas_planetoid"
  ]
  node [
    id 123
    label "Syberia_Wschodnia"
  ]
  node [
    id 124
    label "Antarktyka"
  ]
  node [
    id 125
    label "Rakowice"
  ]
  node [
    id 126
    label "akrecja"
  ]
  node [
    id 127
    label "wymiar"
  ]
  node [
    id 128
    label "&#321;&#281;g"
  ]
  node [
    id 129
    label "Kresy_Zachodnie"
  ]
  node [
    id 130
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 131
    label "przestrze&#324;"
  ]
  node [
    id 132
    label "wsch&#243;d"
  ]
  node [
    id 133
    label "Notogea"
  ]
  node [
    id 134
    label "sprawa"
  ]
  node [
    id 135
    label "subiekcja"
  ]
  node [
    id 136
    label "problemat"
  ]
  node [
    id 137
    label "jajko_Kolumba"
  ]
  node [
    id 138
    label "obstruction"
  ]
  node [
    id 139
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 140
    label "problematyka"
  ]
  node [
    id 141
    label "trudno&#347;&#263;"
  ]
  node [
    id 142
    label "pierepa&#322;ka"
  ]
  node [
    id 143
    label "ambaras"
  ]
  node [
    id 144
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 145
    label "napotka&#263;"
  ]
  node [
    id 146
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 147
    label "k&#322;opotliwy"
  ]
  node [
    id 148
    label "napotkanie"
  ]
  node [
    id 149
    label "poziom"
  ]
  node [
    id 150
    label "difficulty"
  ]
  node [
    id 151
    label "obstacle"
  ]
  node [
    id 152
    label "cecha"
  ]
  node [
    id 153
    label "sytuacja"
  ]
  node [
    id 154
    label "kognicja"
  ]
  node [
    id 155
    label "object"
  ]
  node [
    id 156
    label "rozprawa"
  ]
  node [
    id 157
    label "temat"
  ]
  node [
    id 158
    label "wydarzenie"
  ]
  node [
    id 159
    label "szczeg&#243;&#322;"
  ]
  node [
    id 160
    label "proposition"
  ]
  node [
    id 161
    label "przes&#322;anka"
  ]
  node [
    id 162
    label "rzecz"
  ]
  node [
    id 163
    label "idea"
  ]
  node [
    id 164
    label "k&#322;opot"
  ]
  node [
    id 165
    label "informatyka"
  ]
  node [
    id 166
    label "operacja"
  ]
  node [
    id 167
    label "konto"
  ]
  node [
    id 168
    label "has&#322;o"
  ]
  node [
    id 169
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 170
    label "posiada&#263;"
  ]
  node [
    id 171
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 172
    label "egzekutywa"
  ]
  node [
    id 173
    label "potencja&#322;"
  ]
  node [
    id 174
    label "wyb&#243;r"
  ]
  node [
    id 175
    label "prospect"
  ]
  node [
    id 176
    label "ability"
  ]
  node [
    id 177
    label "obliczeniowo"
  ]
  node [
    id 178
    label "alternatywa"
  ]
  node [
    id 179
    label "operator_modalny"
  ]
  node [
    id 180
    label "proces_my&#347;lowy"
  ]
  node [
    id 181
    label "liczenie"
  ]
  node [
    id 182
    label "czyn"
  ]
  node [
    id 183
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 184
    label "supremum"
  ]
  node [
    id 185
    label "laparotomia"
  ]
  node [
    id 186
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 187
    label "jednostka"
  ]
  node [
    id 188
    label "matematyka"
  ]
  node [
    id 189
    label "rzut"
  ]
  node [
    id 190
    label "liczy&#263;"
  ]
  node [
    id 191
    label "strategia"
  ]
  node [
    id 192
    label "torakotomia"
  ]
  node [
    id 193
    label "chirurg"
  ]
  node [
    id 194
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 195
    label "zabieg"
  ]
  node [
    id 196
    label "szew"
  ]
  node [
    id 197
    label "funkcja"
  ]
  node [
    id 198
    label "mathematical_process"
  ]
  node [
    id 199
    label "infimum"
  ]
  node [
    id 200
    label "warunek_lokalowy"
  ]
  node [
    id 201
    label "plac"
  ]
  node [
    id 202
    label "location"
  ]
  node [
    id 203
    label "uwaga"
  ]
  node [
    id 204
    label "status"
  ]
  node [
    id 205
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 206
    label "chwila"
  ]
  node [
    id 207
    label "cia&#322;o"
  ]
  node [
    id 208
    label "praca"
  ]
  node [
    id 209
    label "rz&#261;d"
  ]
  node [
    id 210
    label "definicja"
  ]
  node [
    id 211
    label "sztuka_dla_sztuki"
  ]
  node [
    id 212
    label "rozwi&#261;zanie"
  ]
  node [
    id 213
    label "kod"
  ]
  node [
    id 214
    label "solicitation"
  ]
  node [
    id 215
    label "powiedzenie"
  ]
  node [
    id 216
    label "leksem"
  ]
  node [
    id 217
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 218
    label "pozycja"
  ]
  node [
    id 219
    label "sygna&#322;"
  ]
  node [
    id 220
    label "przes&#322;anie"
  ]
  node [
    id 221
    label "artyku&#322;_has&#322;owy"
  ]
  node [
    id 222
    label "kwalifikator"
  ]
  node [
    id 223
    label "ochrona"
  ]
  node [
    id 224
    label "artyku&#322;"
  ]
  node [
    id 225
    label "guide_word"
  ]
  node [
    id 226
    label "wyra&#380;enie"
  ]
  node [
    id 227
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 228
    label "dorobek"
  ]
  node [
    id 229
    label "mienie"
  ]
  node [
    id 230
    label "subkonto"
  ]
  node [
    id 231
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 232
    label "debet"
  ]
  node [
    id 233
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 234
    label "kariera"
  ]
  node [
    id 235
    label "reprezentacja"
  ]
  node [
    id 236
    label "rachunek"
  ]
  node [
    id 237
    label "kredyt"
  ]
  node [
    id 238
    label "HP"
  ]
  node [
    id 239
    label "infa"
  ]
  node [
    id 240
    label "przedmiot"
  ]
  node [
    id 241
    label "kierunek"
  ]
  node [
    id 242
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 243
    label "kryptologia"
  ]
  node [
    id 244
    label "baza_danych"
  ]
  node [
    id 245
    label "przetwarzanie_informacji"
  ]
  node [
    id 246
    label "sztuczna_inteligencja"
  ]
  node [
    id 247
    label "gramatyka_formalna"
  ]
  node [
    id 248
    label "program"
  ]
  node [
    id 249
    label "zamek"
  ]
  node [
    id 250
    label "dziedzina_informatyki"
  ]
  node [
    id 251
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 252
    label "artefakt"
  ]
  node [
    id 253
    label "produkt_gotowy"
  ]
  node [
    id 254
    label "service"
  ]
  node [
    id 255
    label "asortyment"
  ]
  node [
    id 256
    label "czynno&#347;&#263;"
  ]
  node [
    id 257
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 258
    label "&#347;wiadczenie"
  ]
  node [
    id 259
    label "activity"
  ]
  node [
    id 260
    label "bezproblemowy"
  ]
  node [
    id 261
    label "supply"
  ]
  node [
    id 262
    label "testify"
  ]
  node [
    id 263
    label "op&#322;aca&#263;"
  ]
  node [
    id 264
    label "by&#263;"
  ]
  node [
    id 265
    label "wyraz"
  ]
  node [
    id 266
    label "sk&#322;ada&#263;"
  ]
  node [
    id 267
    label "pracowa&#263;"
  ]
  node [
    id 268
    label "represent"
  ]
  node [
    id 269
    label "bespeak"
  ]
  node [
    id 270
    label "opowiada&#263;"
  ]
  node [
    id 271
    label "attest"
  ]
  node [
    id 272
    label "informowa&#263;"
  ]
  node [
    id 273
    label "czyni&#263;_dobro"
  ]
  node [
    id 274
    label "towar"
  ]
  node [
    id 275
    label "range"
  ]
  node [
    id 276
    label "czynienie_dobra"
  ]
  node [
    id 277
    label "zobowi&#261;zanie"
  ]
  node [
    id 278
    label "p&#322;acenie"
  ]
  node [
    id 279
    label "koszt_rodzajowy"
  ]
  node [
    id 280
    label "znaczenie"
  ]
  node [
    id 281
    label "przekonywanie"
  ]
  node [
    id 282
    label "sk&#322;adanie"
  ]
  node [
    id 283
    label "informowanie"
  ]
  node [
    id 284
    label "command"
  ]
  node [
    id 285
    label "performance"
  ]
  node [
    id 286
    label "pracowanie"
  ]
  node [
    id 287
    label "opowiadanie"
  ]
  node [
    id 288
    label "post&#261;pi&#263;"
  ]
  node [
    id 289
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 290
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 291
    label "odj&#261;&#263;"
  ]
  node [
    id 292
    label "zrobi&#263;"
  ]
  node [
    id 293
    label "cause"
  ]
  node [
    id 294
    label "introduce"
  ]
  node [
    id 295
    label "begin"
  ]
  node [
    id 296
    label "do"
  ]
  node [
    id 297
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 298
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 299
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 300
    label "zorganizowa&#263;"
  ]
  node [
    id 301
    label "appoint"
  ]
  node [
    id 302
    label "wystylizowa&#263;"
  ]
  node [
    id 303
    label "przerobi&#263;"
  ]
  node [
    id 304
    label "nabra&#263;"
  ]
  node [
    id 305
    label "make"
  ]
  node [
    id 306
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 307
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 308
    label "wydali&#263;"
  ]
  node [
    id 309
    label "withdraw"
  ]
  node [
    id 310
    label "zabra&#263;"
  ]
  node [
    id 311
    label "oddzieli&#263;"
  ]
  node [
    id 312
    label "policzy&#263;"
  ]
  node [
    id 313
    label "reduce"
  ]
  node [
    id 314
    label "oddali&#263;"
  ]
  node [
    id 315
    label "separate"
  ]
  node [
    id 316
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 317
    label "advance"
  ]
  node [
    id 318
    label "act"
  ]
  node [
    id 319
    label "see"
  ]
  node [
    id 320
    label "his"
  ]
  node [
    id 321
    label "d&#378;wi&#281;k"
  ]
  node [
    id 322
    label "ut"
  ]
  node [
    id 323
    label "C"
  ]
  node [
    id 324
    label "report"
  ]
  node [
    id 325
    label "write"
  ]
  node [
    id 326
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 327
    label "zaczyna&#263;"
  ]
  node [
    id 328
    label "nastawia&#263;"
  ]
  node [
    id 329
    label "get_in_touch"
  ]
  node [
    id 330
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 331
    label "dokoptowywa&#263;"
  ]
  node [
    id 332
    label "ogl&#261;da&#263;"
  ]
  node [
    id 333
    label "uruchamia&#263;"
  ]
  node [
    id 334
    label "involve"
  ]
  node [
    id 335
    label "umieszcza&#263;"
  ]
  node [
    id 336
    label "connect"
  ]
  node [
    id 337
    label "powiada&#263;"
  ]
  node [
    id 338
    label "komunikowa&#263;"
  ]
  node [
    id 339
    label "inform"
  ]
  node [
    id 340
    label "umowa"
  ]
  node [
    id 341
    label "cover"
  ]
  node [
    id 342
    label "agent_rozliczeniowy"
  ]
  node [
    id 343
    label "komputer_cyfrowy"
  ]
  node [
    id 344
    label "us&#322;ugobiorca"
  ]
  node [
    id 345
    label "cz&#322;owiek"
  ]
  node [
    id 346
    label "Rzymianin"
  ]
  node [
    id 347
    label "szlachcic"
  ]
  node [
    id 348
    label "obywatel"
  ]
  node [
    id 349
    label "klientela"
  ]
  node [
    id 350
    label "bratek"
  ]
  node [
    id 351
    label "szlachciura"
  ]
  node [
    id 352
    label "przedstawiciel"
  ]
  node [
    id 353
    label "szlachta"
  ]
  node [
    id 354
    label "notabl"
  ]
  node [
    id 355
    label "Cyceron"
  ]
  node [
    id 356
    label "mieszkaniec"
  ]
  node [
    id 357
    label "Horacy"
  ]
  node [
    id 358
    label "W&#322;och"
  ]
  node [
    id 359
    label "miastowy"
  ]
  node [
    id 360
    label "pa&#324;stwo"
  ]
  node [
    id 361
    label "ludzko&#347;&#263;"
  ]
  node [
    id 362
    label "asymilowanie"
  ]
  node [
    id 363
    label "wapniak"
  ]
  node [
    id 364
    label "asymilowa&#263;"
  ]
  node [
    id 365
    label "os&#322;abia&#263;"
  ]
  node [
    id 366
    label "posta&#263;"
  ]
  node [
    id 367
    label "hominid"
  ]
  node [
    id 368
    label "podw&#322;adny"
  ]
  node [
    id 369
    label "os&#322;abianie"
  ]
  node [
    id 370
    label "g&#322;owa"
  ]
  node [
    id 371
    label "figura"
  ]
  node [
    id 372
    label "portrecista"
  ]
  node [
    id 373
    label "dwun&#243;g"
  ]
  node [
    id 374
    label "profanum"
  ]
  node [
    id 375
    label "nasada"
  ]
  node [
    id 376
    label "duch"
  ]
  node [
    id 377
    label "antropochoria"
  ]
  node [
    id 378
    label "osoba"
  ]
  node [
    id 379
    label "wz&#243;r"
  ]
  node [
    id 380
    label "senior"
  ]
  node [
    id 381
    label "oddzia&#322;ywanie"
  ]
  node [
    id 382
    label "Adam"
  ]
  node [
    id 383
    label "homo_sapiens"
  ]
  node [
    id 384
    label "polifag"
  ]
  node [
    id 385
    label "podmiot"
  ]
  node [
    id 386
    label "instalowa&#263;"
  ]
  node [
    id 387
    label "oprogramowanie"
  ]
  node [
    id 388
    label "odinstalowywa&#263;"
  ]
  node [
    id 389
    label "spis"
  ]
  node [
    id 390
    label "zaprezentowanie"
  ]
  node [
    id 391
    label "podprogram"
  ]
  node [
    id 392
    label "ogranicznik_referencyjny"
  ]
  node [
    id 393
    label "course_of_study"
  ]
  node [
    id 394
    label "booklet"
  ]
  node [
    id 395
    label "dzia&#322;"
  ]
  node [
    id 396
    label "odinstalowanie"
  ]
  node [
    id 397
    label "broszura"
  ]
  node [
    id 398
    label "wytw&#243;r"
  ]
  node [
    id 399
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 400
    label "kana&#322;"
  ]
  node [
    id 401
    label "teleferie"
  ]
  node [
    id 402
    label "zainstalowanie"
  ]
  node [
    id 403
    label "struktura_organizacyjna"
  ]
  node [
    id 404
    label "pirat"
  ]
  node [
    id 405
    label "zaprezentowa&#263;"
  ]
  node [
    id 406
    label "prezentowanie"
  ]
  node [
    id 407
    label "prezentowa&#263;"
  ]
  node [
    id 408
    label "interfejs"
  ]
  node [
    id 409
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 410
    label "okno"
  ]
  node [
    id 411
    label "blok"
  ]
  node [
    id 412
    label "folder"
  ]
  node [
    id 413
    label "zainstalowa&#263;"
  ]
  node [
    id 414
    label "za&#322;o&#380;enie"
  ]
  node [
    id 415
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 416
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 417
    label "ram&#243;wka"
  ]
  node [
    id 418
    label "tryb"
  ]
  node [
    id 419
    label "emitowa&#263;"
  ]
  node [
    id 420
    label "emitowanie"
  ]
  node [
    id 421
    label "odinstalowywanie"
  ]
  node [
    id 422
    label "instrukcja"
  ]
  node [
    id 423
    label "deklaracja"
  ]
  node [
    id 424
    label "menu"
  ]
  node [
    id 425
    label "sekcja_krytyczna"
  ]
  node [
    id 426
    label "furkacja"
  ]
  node [
    id 427
    label "podstawa"
  ]
  node [
    id 428
    label "instalowanie"
  ]
  node [
    id 429
    label "oferta"
  ]
  node [
    id 430
    label "odinstalowa&#263;"
  ]
  node [
    id 431
    label "kochanek"
  ]
  node [
    id 432
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 433
    label "fio&#322;ek"
  ]
  node [
    id 434
    label "facet"
  ]
  node [
    id 435
    label "brat"
  ]
  node [
    id 436
    label "clientele"
  ]
  node [
    id 437
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 438
    label "nieznaczny"
  ]
  node [
    id 439
    label "pomiernie"
  ]
  node [
    id 440
    label "kr&#243;tko"
  ]
  node [
    id 441
    label "mikroskopijnie"
  ]
  node [
    id 442
    label "nieliczny"
  ]
  node [
    id 443
    label "mo&#380;liwie"
  ]
  node [
    id 444
    label "nieistotnie"
  ]
  node [
    id 445
    label "ma&#322;y"
  ]
  node [
    id 446
    label "niepowa&#380;nie"
  ]
  node [
    id 447
    label "niewa&#380;ny"
  ]
  node [
    id 448
    label "mo&#380;liwy"
  ]
  node [
    id 449
    label "zno&#347;nie"
  ]
  node [
    id 450
    label "kr&#243;tki"
  ]
  node [
    id 451
    label "nieznacznie"
  ]
  node [
    id 452
    label "drobnostkowy"
  ]
  node [
    id 453
    label "malusie&#324;ko"
  ]
  node [
    id 454
    label "mikroskopijny"
  ]
  node [
    id 455
    label "bardzo"
  ]
  node [
    id 456
    label "szybki"
  ]
  node [
    id 457
    label "przeci&#281;tny"
  ]
  node [
    id 458
    label "wstydliwy"
  ]
  node [
    id 459
    label "s&#322;aby"
  ]
  node [
    id 460
    label "ch&#322;opiec"
  ]
  node [
    id 461
    label "m&#322;ody"
  ]
  node [
    id 462
    label "marny"
  ]
  node [
    id 463
    label "n&#281;dznie"
  ]
  node [
    id 464
    label "nielicznie"
  ]
  node [
    id 465
    label "licho"
  ]
  node [
    id 466
    label "proporcjonalnie"
  ]
  node [
    id 467
    label "pomierny"
  ]
  node [
    id 468
    label "miernie"
  ]
  node [
    id 469
    label "doros&#322;y"
  ]
  node [
    id 470
    label "znaczny"
  ]
  node [
    id 471
    label "niema&#322;o"
  ]
  node [
    id 472
    label "wiele"
  ]
  node [
    id 473
    label "rozwini&#281;ty"
  ]
  node [
    id 474
    label "dorodny"
  ]
  node [
    id 475
    label "wa&#380;ny"
  ]
  node [
    id 476
    label "prawdziwy"
  ]
  node [
    id 477
    label "du&#380;o"
  ]
  node [
    id 478
    label "&#380;ywny"
  ]
  node [
    id 479
    label "szczery"
  ]
  node [
    id 480
    label "naturalny"
  ]
  node [
    id 481
    label "naprawd&#281;"
  ]
  node [
    id 482
    label "realnie"
  ]
  node [
    id 483
    label "podobny"
  ]
  node [
    id 484
    label "zgodny"
  ]
  node [
    id 485
    label "m&#261;dry"
  ]
  node [
    id 486
    label "prawdziwie"
  ]
  node [
    id 487
    label "znacznie"
  ]
  node [
    id 488
    label "zauwa&#380;alny"
  ]
  node [
    id 489
    label "wynios&#322;y"
  ]
  node [
    id 490
    label "dono&#347;ny"
  ]
  node [
    id 491
    label "silny"
  ]
  node [
    id 492
    label "wa&#380;nie"
  ]
  node [
    id 493
    label "istotnie"
  ]
  node [
    id 494
    label "eksponowany"
  ]
  node [
    id 495
    label "dobry"
  ]
  node [
    id 496
    label "ukszta&#322;towany"
  ]
  node [
    id 497
    label "do&#347;cig&#322;y"
  ]
  node [
    id 498
    label "&#378;ra&#322;y"
  ]
  node [
    id 499
    label "zdr&#243;w"
  ]
  node [
    id 500
    label "dorodnie"
  ]
  node [
    id 501
    label "okaza&#322;y"
  ]
  node [
    id 502
    label "mocno"
  ]
  node [
    id 503
    label "wiela"
  ]
  node [
    id 504
    label "cz&#281;sto"
  ]
  node [
    id 505
    label "wydoro&#347;lenie"
  ]
  node [
    id 506
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 507
    label "doro&#347;lenie"
  ]
  node [
    id 508
    label "doro&#347;le"
  ]
  node [
    id 509
    label "dojrzale"
  ]
  node [
    id 510
    label "dojrza&#322;y"
  ]
  node [
    id 511
    label "doletni"
  ]
  node [
    id 512
    label "kwota"
  ]
  node [
    id 513
    label "instytucja"
  ]
  node [
    id 514
    label "siedziba"
  ]
  node [
    id 515
    label "wk&#322;adca"
  ]
  node [
    id 516
    label "agencja"
  ]
  node [
    id 517
    label "eurorynek"
  ]
  node [
    id 518
    label "egzemplarz"
  ]
  node [
    id 519
    label "series"
  ]
  node [
    id 520
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 521
    label "uprawianie"
  ]
  node [
    id 522
    label "praca_rolnicza"
  ]
  node [
    id 523
    label "collection"
  ]
  node [
    id 524
    label "dane"
  ]
  node [
    id 525
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 526
    label "pakiet_klimatyczny"
  ]
  node [
    id 527
    label "poj&#281;cie"
  ]
  node [
    id 528
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 529
    label "sum"
  ]
  node [
    id 530
    label "gathering"
  ]
  node [
    id 531
    label "album"
  ]
  node [
    id 532
    label "wynie&#347;&#263;"
  ]
  node [
    id 533
    label "pieni&#261;dze"
  ]
  node [
    id 534
    label "limit"
  ]
  node [
    id 535
    label "wynosi&#263;"
  ]
  node [
    id 536
    label "osoba_prawna"
  ]
  node [
    id 537
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 538
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 539
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 540
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 541
    label "biuro"
  ]
  node [
    id 542
    label "organizacja"
  ]
  node [
    id 543
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 544
    label "Fundusze_Unijne"
  ]
  node [
    id 545
    label "zamyka&#263;"
  ]
  node [
    id 546
    label "establishment"
  ]
  node [
    id 547
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 548
    label "urz&#261;d"
  ]
  node [
    id 549
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 550
    label "afiliowa&#263;"
  ]
  node [
    id 551
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 552
    label "standard"
  ]
  node [
    id 553
    label "zamykanie"
  ]
  node [
    id 554
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 555
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 556
    label "&#321;ubianka"
  ]
  node [
    id 557
    label "miejsce_pracy"
  ]
  node [
    id 558
    label "dzia&#322;_personalny"
  ]
  node [
    id 559
    label "Kreml"
  ]
  node [
    id 560
    label "Bia&#322;y_Dom"
  ]
  node [
    id 561
    label "budynek"
  ]
  node [
    id 562
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 563
    label "sadowisko"
  ]
  node [
    id 564
    label "rynek_finansowy"
  ]
  node [
    id 565
    label "sie&#263;"
  ]
  node [
    id 566
    label "rynek_mi&#281;dzynarodowy"
  ]
  node [
    id 567
    label "ajencja"
  ]
  node [
    id 568
    label "przedstawicielstwo"
  ]
  node [
    id 569
    label "firma"
  ]
  node [
    id 570
    label "NASA"
  ]
  node [
    id 571
    label "oddzia&#322;"
  ]
  node [
    id 572
    label "filia"
  ]
  node [
    id 573
    label "niespodziewanie"
  ]
  node [
    id 574
    label "&#322;&#261;cznie"
  ]
  node [
    id 575
    label "raptownie"
  ]
  node [
    id 576
    label "jednoczesny"
  ]
  node [
    id 577
    label "synchronously"
  ]
  node [
    id 578
    label "concurrently"
  ]
  node [
    id 579
    label "coincidentally"
  ]
  node [
    id 580
    label "simultaneously"
  ]
  node [
    id 581
    label "&#322;&#261;czny"
  ]
  node [
    id 582
    label "zbiorczo"
  ]
  node [
    id 583
    label "raptowny"
  ]
  node [
    id 584
    label "szybko"
  ]
  node [
    id 585
    label "nieprzewidzianie"
  ]
  node [
    id 586
    label "zaskakuj&#261;co"
  ]
  node [
    id 587
    label "nieoczekiwany"
  ]
  node [
    id 588
    label "Pekao"
  ]
  node [
    id 589
    label "sekunda"
  ]
  node [
    id 590
    label "a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 165
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 11
    target 507
  ]
  edge [
    source 11
    target 508
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 509
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 510
  ]
  edge [
    source 11
    target 511
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 38
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 229
  ]
  edge [
    source 12
    target 230
  ]
  edge [
    source 12
    target 231
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 233
  ]
  edge [
    source 12
    target 234
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 236
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 588
    target 589
  ]
  edge [
    source 588
    target 590
  ]
  edge [
    source 589
    target 590
  ]
]
