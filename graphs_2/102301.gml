graph [
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "dobre"
    origin "text"
  ]
  node [
    id 2
    label "miejsce"
    origin "text"
  ]
  node [
    id 3
    label "przerwa"
    origin "text"
  ]
  node [
    id 4
    label "ci&#281;&#380;ki"
    origin "text"
  ]
  node [
    id 5
    label "rozwa&#380;anie"
    origin "text"
  ]
  node [
    id 6
    label "teoretyczny"
    origin "text"
  ]
  node [
    id 7
    label "ciekawy"
    origin "text"
  ]
  node [
    id 8
    label "inspirowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "rozdzia&#322;"
    origin "text"
  ]
  node [
    id 10
    label "przed"
    origin "text"
  ]
  node [
    id 11
    label "bogactwo"
    origin "text"
  ]
  node [
    id 12
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 14
    label "nasa"
    origin "text"
  ]
  node [
    id 15
    label "wszyscy"
    origin "text"
  ]
  node [
    id 16
    label "chyba"
    origin "text"
  ]
  node [
    id 17
    label "prawo"
    origin "text"
  ]
  node [
    id 18
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 19
    label "inaczej"
    origin "text"
  ]
  node [
    id 20
    label "w_chuj"
  ]
  node [
    id 21
    label "warunek_lokalowy"
  ]
  node [
    id 22
    label "plac"
  ]
  node [
    id 23
    label "location"
  ]
  node [
    id 24
    label "uwaga"
  ]
  node [
    id 25
    label "przestrze&#324;"
  ]
  node [
    id 26
    label "status"
  ]
  node [
    id 27
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 28
    label "chwila"
  ]
  node [
    id 29
    label "cia&#322;o"
  ]
  node [
    id 30
    label "cecha"
  ]
  node [
    id 31
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 32
    label "praca"
  ]
  node [
    id 33
    label "rz&#261;d"
  ]
  node [
    id 34
    label "charakterystyka"
  ]
  node [
    id 35
    label "m&#322;ot"
  ]
  node [
    id 36
    label "znak"
  ]
  node [
    id 37
    label "drzewo"
  ]
  node [
    id 38
    label "pr&#243;ba"
  ]
  node [
    id 39
    label "attribute"
  ]
  node [
    id 40
    label "marka"
  ]
  node [
    id 41
    label "Rzym_Zachodni"
  ]
  node [
    id 42
    label "whole"
  ]
  node [
    id 43
    label "ilo&#347;&#263;"
  ]
  node [
    id 44
    label "element"
  ]
  node [
    id 45
    label "Rzym_Wschodni"
  ]
  node [
    id 46
    label "urz&#261;dzenie"
  ]
  node [
    id 47
    label "wypowied&#378;"
  ]
  node [
    id 48
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 49
    label "stan"
  ]
  node [
    id 50
    label "nagana"
  ]
  node [
    id 51
    label "tekst"
  ]
  node [
    id 52
    label "upomnienie"
  ]
  node [
    id 53
    label "dzienniczek"
  ]
  node [
    id 54
    label "wzgl&#261;d"
  ]
  node [
    id 55
    label "gossip"
  ]
  node [
    id 56
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 57
    label "najem"
  ]
  node [
    id 58
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 59
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 60
    label "zak&#322;ad"
  ]
  node [
    id 61
    label "stosunek_pracy"
  ]
  node [
    id 62
    label "benedykty&#324;ski"
  ]
  node [
    id 63
    label "poda&#380;_pracy"
  ]
  node [
    id 64
    label "pracowanie"
  ]
  node [
    id 65
    label "tyrka"
  ]
  node [
    id 66
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 67
    label "wytw&#243;r"
  ]
  node [
    id 68
    label "zaw&#243;d"
  ]
  node [
    id 69
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 70
    label "tynkarski"
  ]
  node [
    id 71
    label "pracowa&#263;"
  ]
  node [
    id 72
    label "czynno&#347;&#263;"
  ]
  node [
    id 73
    label "zmiana"
  ]
  node [
    id 74
    label "czynnik_produkcji"
  ]
  node [
    id 75
    label "zobowi&#261;zanie"
  ]
  node [
    id 76
    label "kierownictwo"
  ]
  node [
    id 77
    label "siedziba"
  ]
  node [
    id 78
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 79
    label "rozdzielanie"
  ]
  node [
    id 80
    label "bezbrze&#380;e"
  ]
  node [
    id 81
    label "punkt"
  ]
  node [
    id 82
    label "czasoprzestrze&#324;"
  ]
  node [
    id 83
    label "zbi&#243;r"
  ]
  node [
    id 84
    label "niezmierzony"
  ]
  node [
    id 85
    label "przedzielenie"
  ]
  node [
    id 86
    label "nielito&#347;ciwy"
  ]
  node [
    id 87
    label "rozdziela&#263;"
  ]
  node [
    id 88
    label "oktant"
  ]
  node [
    id 89
    label "przedzieli&#263;"
  ]
  node [
    id 90
    label "przestw&#243;r"
  ]
  node [
    id 91
    label "condition"
  ]
  node [
    id 92
    label "awansowa&#263;"
  ]
  node [
    id 93
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 94
    label "znaczenie"
  ]
  node [
    id 95
    label "awans"
  ]
  node [
    id 96
    label "podmiotowo"
  ]
  node [
    id 97
    label "awansowanie"
  ]
  node [
    id 98
    label "sytuacja"
  ]
  node [
    id 99
    label "time"
  ]
  node [
    id 100
    label "czas"
  ]
  node [
    id 101
    label "rozmiar"
  ]
  node [
    id 102
    label "liczba"
  ]
  node [
    id 103
    label "circumference"
  ]
  node [
    id 104
    label "leksem"
  ]
  node [
    id 105
    label "cyrkumferencja"
  ]
  node [
    id 106
    label "strona"
  ]
  node [
    id 107
    label "ekshumowanie"
  ]
  node [
    id 108
    label "jednostka_organizacyjna"
  ]
  node [
    id 109
    label "p&#322;aszczyzna"
  ]
  node [
    id 110
    label "odwadnia&#263;"
  ]
  node [
    id 111
    label "zabalsamowanie"
  ]
  node [
    id 112
    label "zesp&#243;&#322;"
  ]
  node [
    id 113
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 114
    label "odwodni&#263;"
  ]
  node [
    id 115
    label "sk&#243;ra"
  ]
  node [
    id 116
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 117
    label "staw"
  ]
  node [
    id 118
    label "ow&#322;osienie"
  ]
  node [
    id 119
    label "mi&#281;so"
  ]
  node [
    id 120
    label "zabalsamowa&#263;"
  ]
  node [
    id 121
    label "Izba_Konsyliarska"
  ]
  node [
    id 122
    label "unerwienie"
  ]
  node [
    id 123
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 124
    label "kremacja"
  ]
  node [
    id 125
    label "biorytm"
  ]
  node [
    id 126
    label "sekcja"
  ]
  node [
    id 127
    label "istota_&#380;ywa"
  ]
  node [
    id 128
    label "otworzy&#263;"
  ]
  node [
    id 129
    label "otwiera&#263;"
  ]
  node [
    id 130
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 131
    label "otworzenie"
  ]
  node [
    id 132
    label "materia"
  ]
  node [
    id 133
    label "pochowanie"
  ]
  node [
    id 134
    label "otwieranie"
  ]
  node [
    id 135
    label "ty&#322;"
  ]
  node [
    id 136
    label "szkielet"
  ]
  node [
    id 137
    label "tanatoplastyk"
  ]
  node [
    id 138
    label "odwadnianie"
  ]
  node [
    id 139
    label "Komitet_Region&#243;w"
  ]
  node [
    id 140
    label "odwodnienie"
  ]
  node [
    id 141
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 142
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 143
    label "nieumar&#322;y"
  ]
  node [
    id 144
    label "pochowa&#263;"
  ]
  node [
    id 145
    label "balsamowa&#263;"
  ]
  node [
    id 146
    label "tanatoplastyka"
  ]
  node [
    id 147
    label "temperatura"
  ]
  node [
    id 148
    label "ekshumowa&#263;"
  ]
  node [
    id 149
    label "balsamowanie"
  ]
  node [
    id 150
    label "uk&#322;ad"
  ]
  node [
    id 151
    label "prz&#243;d"
  ]
  node [
    id 152
    label "l&#281;d&#378;wie"
  ]
  node [
    id 153
    label "cz&#322;onek"
  ]
  node [
    id 154
    label "pogrzeb"
  ]
  node [
    id 155
    label "&#321;ubianka"
  ]
  node [
    id 156
    label "area"
  ]
  node [
    id 157
    label "Majdan"
  ]
  node [
    id 158
    label "pole_bitwy"
  ]
  node [
    id 159
    label "stoisko"
  ]
  node [
    id 160
    label "obszar"
  ]
  node [
    id 161
    label "pierzeja"
  ]
  node [
    id 162
    label "obiekt_handlowy"
  ]
  node [
    id 163
    label "zgromadzenie"
  ]
  node [
    id 164
    label "miasto"
  ]
  node [
    id 165
    label "targowica"
  ]
  node [
    id 166
    label "kram"
  ]
  node [
    id 167
    label "przybli&#380;enie"
  ]
  node [
    id 168
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 169
    label "kategoria"
  ]
  node [
    id 170
    label "szpaler"
  ]
  node [
    id 171
    label "lon&#380;a"
  ]
  node [
    id 172
    label "uporz&#261;dkowanie"
  ]
  node [
    id 173
    label "instytucja"
  ]
  node [
    id 174
    label "jednostka_systematyczna"
  ]
  node [
    id 175
    label "egzekutywa"
  ]
  node [
    id 176
    label "premier"
  ]
  node [
    id 177
    label "Londyn"
  ]
  node [
    id 178
    label "gabinet_cieni"
  ]
  node [
    id 179
    label "gromada"
  ]
  node [
    id 180
    label "number"
  ]
  node [
    id 181
    label "Konsulat"
  ]
  node [
    id 182
    label "tract"
  ]
  node [
    id 183
    label "klasa"
  ]
  node [
    id 184
    label "w&#322;adza"
  ]
  node [
    id 185
    label "pauza"
  ]
  node [
    id 186
    label "przedzia&#322;"
  ]
  node [
    id 187
    label "poprzedzanie"
  ]
  node [
    id 188
    label "laba"
  ]
  node [
    id 189
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 190
    label "chronometria"
  ]
  node [
    id 191
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 192
    label "rachuba_czasu"
  ]
  node [
    id 193
    label "przep&#322;ywanie"
  ]
  node [
    id 194
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 195
    label "czasokres"
  ]
  node [
    id 196
    label "odczyt"
  ]
  node [
    id 197
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 198
    label "dzieje"
  ]
  node [
    id 199
    label "kategoria_gramatyczna"
  ]
  node [
    id 200
    label "poprzedzenie"
  ]
  node [
    id 201
    label "trawienie"
  ]
  node [
    id 202
    label "pochodzi&#263;"
  ]
  node [
    id 203
    label "period"
  ]
  node [
    id 204
    label "okres_czasu"
  ]
  node [
    id 205
    label "poprzedza&#263;"
  ]
  node [
    id 206
    label "schy&#322;ek"
  ]
  node [
    id 207
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 208
    label "odwlekanie_si&#281;"
  ]
  node [
    id 209
    label "zegar"
  ]
  node [
    id 210
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 211
    label "czwarty_wymiar"
  ]
  node [
    id 212
    label "pochodzenie"
  ]
  node [
    id 213
    label "koniugacja"
  ]
  node [
    id 214
    label "Zeitgeist"
  ]
  node [
    id 215
    label "trawi&#263;"
  ]
  node [
    id 216
    label "pogoda"
  ]
  node [
    id 217
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 218
    label "poprzedzi&#263;"
  ]
  node [
    id 219
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 220
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 221
    label "time_period"
  ]
  node [
    id 222
    label "przegroda"
  ]
  node [
    id 223
    label "miejsce_le&#380;&#261;ce"
  ]
  node [
    id 224
    label "part"
  ]
  node [
    id 225
    label "podzia&#322;"
  ]
  node [
    id 226
    label "pomieszczenie"
  ]
  node [
    id 227
    label "skala"
  ]
  node [
    id 228
    label "miejsce_siedz&#261;ce"
  ]
  node [
    id 229
    label "farewell"
  ]
  node [
    id 230
    label "hyphen"
  ]
  node [
    id 231
    label "znak_muzyczny"
  ]
  node [
    id 232
    label "znak_graficzny"
  ]
  node [
    id 233
    label "monumentalny"
  ]
  node [
    id 234
    label "mocny"
  ]
  node [
    id 235
    label "trudny"
  ]
  node [
    id 236
    label "kompletny"
  ]
  node [
    id 237
    label "masywny"
  ]
  node [
    id 238
    label "wielki"
  ]
  node [
    id 239
    label "wymagaj&#261;cy"
  ]
  node [
    id 240
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 241
    label "przyswajalny"
  ]
  node [
    id 242
    label "niezgrabny"
  ]
  node [
    id 243
    label "liczny"
  ]
  node [
    id 244
    label "nieprzejrzysty"
  ]
  node [
    id 245
    label "niedelikatny"
  ]
  node [
    id 246
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 247
    label "intensywny"
  ]
  node [
    id 248
    label "wolny"
  ]
  node [
    id 249
    label "nieudany"
  ]
  node [
    id 250
    label "zbrojny"
  ]
  node [
    id 251
    label "dotkliwy"
  ]
  node [
    id 252
    label "charakterystyczny"
  ]
  node [
    id 253
    label "ci&#281;&#380;ko"
  ]
  node [
    id 254
    label "bojowy"
  ]
  node [
    id 255
    label "k&#322;opotliwy"
  ]
  node [
    id 256
    label "ambitny"
  ]
  node [
    id 257
    label "grubo"
  ]
  node [
    id 258
    label "gro&#378;ny"
  ]
  node [
    id 259
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 260
    label "druzgoc&#261;cy"
  ]
  node [
    id 261
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 262
    label "bezwzgl&#281;dny"
  ]
  node [
    id 263
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 264
    label "oci&#281;&#380;ale"
  ]
  node [
    id 265
    label "obwis&#322;y"
  ]
  node [
    id 266
    label "kompletnie"
  ]
  node [
    id 267
    label "zupe&#322;ny"
  ]
  node [
    id 268
    label "w_pizdu"
  ]
  node [
    id 269
    label "pe&#322;ny"
  ]
  node [
    id 270
    label "cz&#281;sty"
  ]
  node [
    id 271
    label "licznie"
  ]
  node [
    id 272
    label "rojenie_si&#281;"
  ]
  node [
    id 273
    label "znaczny"
  ]
  node [
    id 274
    label "wyj&#261;tkowy"
  ]
  node [
    id 275
    label "nieprzeci&#281;tny"
  ]
  node [
    id 276
    label "wysoce"
  ]
  node [
    id 277
    label "wa&#380;ny"
  ]
  node [
    id 278
    label "prawdziwy"
  ]
  node [
    id 279
    label "wybitny"
  ]
  node [
    id 280
    label "dupny"
  ]
  node [
    id 281
    label "przykry"
  ]
  node [
    id 282
    label "dotkliwie"
  ]
  node [
    id 283
    label "niestosowny"
  ]
  node [
    id 284
    label "niekszta&#322;tny"
  ]
  node [
    id 285
    label "niezgrabnie"
  ]
  node [
    id 286
    label "du&#380;y"
  ]
  node [
    id 287
    label "g&#322;o&#347;ny"
  ]
  node [
    id 288
    label "masywnie"
  ]
  node [
    id 289
    label "szybki"
  ]
  node [
    id 290
    label "znacz&#261;cy"
  ]
  node [
    id 291
    label "zwarty"
  ]
  node [
    id 292
    label "efektywny"
  ]
  node [
    id 293
    label "ogrodnictwo"
  ]
  node [
    id 294
    label "dynamiczny"
  ]
  node [
    id 295
    label "intensywnie"
  ]
  node [
    id 296
    label "nieproporcjonalny"
  ]
  node [
    id 297
    label "specjalny"
  ]
  node [
    id 298
    label "mo&#380;liwy"
  ]
  node [
    id 299
    label "rozrzedzenie"
  ]
  node [
    id 300
    label "rzedni&#281;cie"
  ]
  node [
    id 301
    label "niespieszny"
  ]
  node [
    id 302
    label "zwalnianie_si&#281;"
  ]
  node [
    id 303
    label "wakowa&#263;"
  ]
  node [
    id 304
    label "rozwadnianie"
  ]
  node [
    id 305
    label "niezale&#380;ny"
  ]
  node [
    id 306
    label "zrzedni&#281;cie"
  ]
  node [
    id 307
    label "swobodnie"
  ]
  node [
    id 308
    label "rozrzedzanie"
  ]
  node [
    id 309
    label "rozwodnienie"
  ]
  node [
    id 310
    label "strza&#322;"
  ]
  node [
    id 311
    label "wolnie"
  ]
  node [
    id 312
    label "zwolnienie_si&#281;"
  ]
  node [
    id 313
    label "wolno"
  ]
  node [
    id 314
    label "lu&#378;no"
  ]
  node [
    id 315
    label "skomplikowany"
  ]
  node [
    id 316
    label "niebezpieczny"
  ]
  node [
    id 317
    label "gro&#378;nie"
  ]
  node [
    id 318
    label "nad&#261;sany"
  ]
  node [
    id 319
    label "nieprzyjemny"
  ]
  node [
    id 320
    label "niegrzeczny"
  ]
  node [
    id 321
    label "nieoboj&#281;tny"
  ]
  node [
    id 322
    label "niewra&#380;liwy"
  ]
  node [
    id 323
    label "wytrzyma&#322;y"
  ]
  node [
    id 324
    label "niedelikatnie"
  ]
  node [
    id 325
    label "zbrojnie"
  ]
  node [
    id 326
    label "przyodziany"
  ]
  node [
    id 327
    label "uzbrojony"
  ]
  node [
    id 328
    label "ostry"
  ]
  node [
    id 329
    label "bojowo"
  ]
  node [
    id 330
    label "pewny"
  ]
  node [
    id 331
    label "&#347;mia&#322;y"
  ]
  node [
    id 332
    label "zadziorny"
  ]
  node [
    id 333
    label "bojowniczy"
  ]
  node [
    id 334
    label "waleczny"
  ]
  node [
    id 335
    label "wymagaj&#261;co"
  ]
  node [
    id 336
    label "k&#322;opotliwie"
  ]
  node [
    id 337
    label "niewygodny"
  ]
  node [
    id 338
    label "monumentalnie"
  ]
  node [
    id 339
    label "wznios&#322;y"
  ]
  node [
    id 340
    label "nieudanie"
  ]
  node [
    id 341
    label "nieciekawy"
  ]
  node [
    id 342
    label "z&#322;y"
  ]
  node [
    id 343
    label "szczery"
  ]
  node [
    id 344
    label "niepodwa&#380;alny"
  ]
  node [
    id 345
    label "zdecydowany"
  ]
  node [
    id 346
    label "stabilny"
  ]
  node [
    id 347
    label "krzepki"
  ]
  node [
    id 348
    label "silny"
  ]
  node [
    id 349
    label "wyrazisty"
  ]
  node [
    id 350
    label "przekonuj&#261;cy"
  ]
  node [
    id 351
    label "widoczny"
  ]
  node [
    id 352
    label "mocno"
  ]
  node [
    id 353
    label "wzmocni&#263;"
  ]
  node [
    id 354
    label "wzmacnia&#263;"
  ]
  node [
    id 355
    label "konkretny"
  ]
  node [
    id 356
    label "silnie"
  ]
  node [
    id 357
    label "meflochina"
  ]
  node [
    id 358
    label "dobry"
  ]
  node [
    id 359
    label "charakterystycznie"
  ]
  node [
    id 360
    label "szczeg&#243;lny"
  ]
  node [
    id 361
    label "typowy"
  ]
  node [
    id 362
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 363
    label "podobny"
  ]
  node [
    id 364
    label "hard"
  ]
  node [
    id 365
    label "&#378;le"
  ]
  node [
    id 366
    label "heavily"
  ]
  node [
    id 367
    label "gruby"
  ]
  node [
    id 368
    label "niegrzecznie"
  ]
  node [
    id 369
    label "niema&#322;o"
  ]
  node [
    id 370
    label "dono&#347;nie"
  ]
  node [
    id 371
    label "grubia&#324;ski"
  ]
  node [
    id 372
    label "fajnie"
  ]
  node [
    id 373
    label "prostacko"
  ]
  node [
    id 374
    label "ciep&#322;o"
  ]
  node [
    id 375
    label "niepewny"
  ]
  node [
    id 376
    label "m&#261;cenie"
  ]
  node [
    id 377
    label "ciecz"
  ]
  node [
    id 378
    label "niejawny"
  ]
  node [
    id 379
    label "ciemny"
  ]
  node [
    id 380
    label "nieklarowny"
  ]
  node [
    id 381
    label "niezrozumia&#322;y"
  ]
  node [
    id 382
    label "zanieczyszczanie"
  ]
  node [
    id 383
    label "zanieczyszczenie"
  ]
  node [
    id 384
    label "nieprzejrzy&#347;cie"
  ]
  node [
    id 385
    label "samodzielny"
  ]
  node [
    id 386
    label "ambitnie"
  ]
  node [
    id 387
    label "zdeterminowany"
  ]
  node [
    id 388
    label "wysokich_lot&#243;w"
  ]
  node [
    id 389
    label "contemplation"
  ]
  node [
    id 390
    label "porcjowanie"
  ]
  node [
    id 391
    label "przemy&#347;liwanie"
  ]
  node [
    id 392
    label "examination"
  ]
  node [
    id 393
    label "przedmiot"
  ]
  node [
    id 394
    label "p&#322;&#243;d"
  ]
  node [
    id 395
    label "work"
  ]
  node [
    id 396
    label "rezultat"
  ]
  node [
    id 397
    label "dzielenie"
  ]
  node [
    id 398
    label "film_editing"
  ]
  node [
    id 399
    label "meditation"
  ]
  node [
    id 400
    label "decydowanie_si&#281;"
  ]
  node [
    id 401
    label "my&#347;lenie"
  ]
  node [
    id 402
    label "nierealny"
  ]
  node [
    id 403
    label "teoretycznie"
  ]
  node [
    id 404
    label "niemo&#380;liwy"
  ]
  node [
    id 405
    label "niepodobny"
  ]
  node [
    id 406
    label "nieprawdziwy"
  ]
  node [
    id 407
    label "nierealnie"
  ]
  node [
    id 408
    label "nietuzinkowy"
  ]
  node [
    id 409
    label "cz&#322;owiek"
  ]
  node [
    id 410
    label "intryguj&#261;cy"
  ]
  node [
    id 411
    label "ch&#281;tny"
  ]
  node [
    id 412
    label "swoisty"
  ]
  node [
    id 413
    label "interesowanie"
  ]
  node [
    id 414
    label "dziwny"
  ]
  node [
    id 415
    label "interesuj&#261;cy"
  ]
  node [
    id 416
    label "ciekawie"
  ]
  node [
    id 417
    label "indagator"
  ]
  node [
    id 418
    label "niespotykany"
  ]
  node [
    id 419
    label "nietuzinkowo"
  ]
  node [
    id 420
    label "interesuj&#261;co"
  ]
  node [
    id 421
    label "atrakcyjny"
  ]
  node [
    id 422
    label "intryguj&#261;co"
  ]
  node [
    id 423
    label "ch&#281;tliwy"
  ]
  node [
    id 424
    label "ch&#281;tnie"
  ]
  node [
    id 425
    label "napalony"
  ]
  node [
    id 426
    label "chy&#380;y"
  ]
  node [
    id 427
    label "&#380;yczliwy"
  ]
  node [
    id 428
    label "przychylny"
  ]
  node [
    id 429
    label "gotowy"
  ]
  node [
    id 430
    label "ludzko&#347;&#263;"
  ]
  node [
    id 431
    label "asymilowanie"
  ]
  node [
    id 432
    label "wapniak"
  ]
  node [
    id 433
    label "asymilowa&#263;"
  ]
  node [
    id 434
    label "os&#322;abia&#263;"
  ]
  node [
    id 435
    label "posta&#263;"
  ]
  node [
    id 436
    label "hominid"
  ]
  node [
    id 437
    label "podw&#322;adny"
  ]
  node [
    id 438
    label "os&#322;abianie"
  ]
  node [
    id 439
    label "g&#322;owa"
  ]
  node [
    id 440
    label "figura"
  ]
  node [
    id 441
    label "portrecista"
  ]
  node [
    id 442
    label "dwun&#243;g"
  ]
  node [
    id 443
    label "profanum"
  ]
  node [
    id 444
    label "mikrokosmos"
  ]
  node [
    id 445
    label "nasada"
  ]
  node [
    id 446
    label "duch"
  ]
  node [
    id 447
    label "antropochoria"
  ]
  node [
    id 448
    label "osoba"
  ]
  node [
    id 449
    label "wz&#243;r"
  ]
  node [
    id 450
    label "senior"
  ]
  node [
    id 451
    label "oddzia&#322;ywanie"
  ]
  node [
    id 452
    label "Adam"
  ]
  node [
    id 453
    label "homo_sapiens"
  ]
  node [
    id 454
    label "polifag"
  ]
  node [
    id 455
    label "dziwnie"
  ]
  node [
    id 456
    label "dziwy"
  ]
  node [
    id 457
    label "inny"
  ]
  node [
    id 458
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 459
    label "odr&#281;bny"
  ]
  node [
    id 460
    label "swoi&#347;cie"
  ]
  node [
    id 461
    label "occupation"
  ]
  node [
    id 462
    label "bycie"
  ]
  node [
    id 463
    label "dobrze"
  ]
  node [
    id 464
    label "ciekawski"
  ]
  node [
    id 465
    label "tug"
  ]
  node [
    id 466
    label "kopiowa&#263;"
  ]
  node [
    id 467
    label "motywowa&#263;"
  ]
  node [
    id 468
    label "wk&#322;ada&#263;"
  ]
  node [
    id 469
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 470
    label "explain"
  ]
  node [
    id 471
    label "pobudza&#263;"
  ]
  node [
    id 472
    label "robi&#263;"
  ]
  node [
    id 473
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 474
    label "wytwarza&#263;"
  ]
  node [
    id 475
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 476
    label "transcribe"
  ]
  node [
    id 477
    label "pirat"
  ]
  node [
    id 478
    label "mock"
  ]
  node [
    id 479
    label "przekazywa&#263;"
  ]
  node [
    id 480
    label "obleka&#263;"
  ]
  node [
    id 481
    label "odziewa&#263;"
  ]
  node [
    id 482
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 483
    label "ubiera&#263;"
  ]
  node [
    id 484
    label "pour"
  ]
  node [
    id 485
    label "nosi&#263;"
  ]
  node [
    id 486
    label "introduce"
  ]
  node [
    id 487
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 488
    label "wzbudza&#263;"
  ]
  node [
    id 489
    label "umieszcza&#263;"
  ]
  node [
    id 490
    label "place"
  ]
  node [
    id 491
    label "wpaja&#263;"
  ]
  node [
    id 492
    label "wydarzenie"
  ]
  node [
    id 493
    label "faza"
  ]
  node [
    id 494
    label "interruption"
  ]
  node [
    id 495
    label "podrozdzia&#322;"
  ]
  node [
    id 496
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 497
    label "fragment"
  ]
  node [
    id 498
    label "przebiec"
  ]
  node [
    id 499
    label "charakter"
  ]
  node [
    id 500
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 501
    label "motyw"
  ]
  node [
    id 502
    label "przebiegni&#281;cie"
  ]
  node [
    id 503
    label "fabu&#322;a"
  ]
  node [
    id 504
    label "cykl_astronomiczny"
  ]
  node [
    id 505
    label "coil"
  ]
  node [
    id 506
    label "zjawisko"
  ]
  node [
    id 507
    label "fotoelement"
  ]
  node [
    id 508
    label "komutowanie"
  ]
  node [
    id 509
    label "stan_skupienia"
  ]
  node [
    id 510
    label "nastr&#243;j"
  ]
  node [
    id 511
    label "przerywacz"
  ]
  node [
    id 512
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 513
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 514
    label "kraw&#281;d&#378;"
  ]
  node [
    id 515
    label "obsesja"
  ]
  node [
    id 516
    label "dw&#243;jnik"
  ]
  node [
    id 517
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 518
    label "okres"
  ]
  node [
    id 519
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 520
    label "przew&#243;d"
  ]
  node [
    id 521
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 522
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 523
    label "obw&#243;d"
  ]
  node [
    id 524
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 525
    label "degree"
  ]
  node [
    id 526
    label "komutowa&#263;"
  ]
  node [
    id 527
    label "eksdywizja"
  ]
  node [
    id 528
    label "blastogeneza"
  ]
  node [
    id 529
    label "stopie&#324;"
  ]
  node [
    id 530
    label "competence"
  ]
  node [
    id 531
    label "fission"
  ]
  node [
    id 532
    label "distribution"
  ]
  node [
    id 533
    label "utw&#243;r"
  ]
  node [
    id 534
    label "egzemplarz"
  ]
  node [
    id 535
    label "wk&#322;ad"
  ]
  node [
    id 536
    label "tytu&#322;"
  ]
  node [
    id 537
    label "zak&#322;adka"
  ]
  node [
    id 538
    label "nomina&#322;"
  ]
  node [
    id 539
    label "ok&#322;adka"
  ]
  node [
    id 540
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 541
    label "wydawnictwo"
  ]
  node [
    id 542
    label "ekslibris"
  ]
  node [
    id 543
    label "przek&#322;adacz"
  ]
  node [
    id 544
    label "bibliofilstwo"
  ]
  node [
    id 545
    label "falc"
  ]
  node [
    id 546
    label "pagina"
  ]
  node [
    id 547
    label "zw&#243;j"
  ]
  node [
    id 548
    label "wysyp"
  ]
  node [
    id 549
    label "fullness"
  ]
  node [
    id 550
    label "podostatek"
  ]
  node [
    id 551
    label "mienie"
  ]
  node [
    id 552
    label "fortune"
  ]
  node [
    id 553
    label "z&#322;ote_czasy"
  ]
  node [
    id 554
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 555
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 556
    label "warunki"
  ]
  node [
    id 557
    label "szczeg&#243;&#322;"
  ]
  node [
    id 558
    label "state"
  ]
  node [
    id 559
    label "realia"
  ]
  node [
    id 560
    label "przej&#347;cie"
  ]
  node [
    id 561
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 562
    label "rodowo&#347;&#263;"
  ]
  node [
    id 563
    label "patent"
  ]
  node [
    id 564
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 565
    label "dobra"
  ]
  node [
    id 566
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 567
    label "przej&#347;&#263;"
  ]
  node [
    id 568
    label "possession"
  ]
  node [
    id 569
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 570
    label "rozpraszanie_si&#281;"
  ]
  node [
    id 571
    label "discrimination"
  ]
  node [
    id 572
    label "diverseness"
  ]
  node [
    id 573
    label "eklektyk"
  ]
  node [
    id 574
    label "rozproszenie_si&#281;"
  ]
  node [
    id 575
    label "differentiation"
  ]
  node [
    id 576
    label "multikulturalizm"
  ]
  node [
    id 577
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 578
    label "rozdzielenie"
  ]
  node [
    id 579
    label "nadanie"
  ]
  node [
    id 580
    label "podzielenie"
  ]
  node [
    id 581
    label "zrobienie"
  ]
  node [
    id 582
    label "urodzaj"
  ]
  node [
    id 583
    label "kszta&#322;t"
  ]
  node [
    id 584
    label "provider"
  ]
  node [
    id 585
    label "biznes_elektroniczny"
  ]
  node [
    id 586
    label "zasadzka"
  ]
  node [
    id 587
    label "mesh"
  ]
  node [
    id 588
    label "plecionka"
  ]
  node [
    id 589
    label "gauze"
  ]
  node [
    id 590
    label "struktura"
  ]
  node [
    id 591
    label "web"
  ]
  node [
    id 592
    label "organizacja"
  ]
  node [
    id 593
    label "gra_sieciowa"
  ]
  node [
    id 594
    label "net"
  ]
  node [
    id 595
    label "media"
  ]
  node [
    id 596
    label "sie&#263;_komputerowa"
  ]
  node [
    id 597
    label "nitka"
  ]
  node [
    id 598
    label "snu&#263;"
  ]
  node [
    id 599
    label "vane"
  ]
  node [
    id 600
    label "instalacja"
  ]
  node [
    id 601
    label "wysnu&#263;"
  ]
  node [
    id 602
    label "organization"
  ]
  node [
    id 603
    label "obiekt"
  ]
  node [
    id 604
    label "us&#322;uga_internetowa"
  ]
  node [
    id 605
    label "rozmieszczenie"
  ]
  node [
    id 606
    label "podcast"
  ]
  node [
    id 607
    label "hipertekst"
  ]
  node [
    id 608
    label "cyberprzestrze&#324;"
  ]
  node [
    id 609
    label "mem"
  ]
  node [
    id 610
    label "grooming"
  ]
  node [
    id 611
    label "punkt_dost&#281;pu"
  ]
  node [
    id 612
    label "netbook"
  ]
  node [
    id 613
    label "e-hazard"
  ]
  node [
    id 614
    label "zastawia&#263;"
  ]
  node [
    id 615
    label "zastawi&#263;"
  ]
  node [
    id 616
    label "ambush"
  ]
  node [
    id 617
    label "atak"
  ]
  node [
    id 618
    label "podst&#281;p"
  ]
  node [
    id 619
    label "formacja"
  ]
  node [
    id 620
    label "punkt_widzenia"
  ]
  node [
    id 621
    label "wygl&#261;d"
  ]
  node [
    id 622
    label "spirala"
  ]
  node [
    id 623
    label "p&#322;at"
  ]
  node [
    id 624
    label "comeliness"
  ]
  node [
    id 625
    label "kielich"
  ]
  node [
    id 626
    label "face"
  ]
  node [
    id 627
    label "blaszka"
  ]
  node [
    id 628
    label "p&#281;tla"
  ]
  node [
    id 629
    label "pasmo"
  ]
  node [
    id 630
    label "linearno&#347;&#263;"
  ]
  node [
    id 631
    label "gwiazda"
  ]
  node [
    id 632
    label "miniatura"
  ]
  node [
    id 633
    label "integer"
  ]
  node [
    id 634
    label "zlewanie_si&#281;"
  ]
  node [
    id 635
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 636
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 637
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 638
    label "u&#322;o&#380;enie"
  ]
  node [
    id 639
    label "porozmieszczanie"
  ]
  node [
    id 640
    label "wyst&#281;powanie"
  ]
  node [
    id 641
    label "layout"
  ]
  node [
    id 642
    label "umieszczenie"
  ]
  node [
    id 643
    label "mechanika"
  ]
  node [
    id 644
    label "o&#347;"
  ]
  node [
    id 645
    label "usenet"
  ]
  node [
    id 646
    label "rozprz&#261;c"
  ]
  node [
    id 647
    label "zachowanie"
  ]
  node [
    id 648
    label "cybernetyk"
  ]
  node [
    id 649
    label "podsystem"
  ]
  node [
    id 650
    label "system"
  ]
  node [
    id 651
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 652
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 653
    label "sk&#322;ad"
  ]
  node [
    id 654
    label "systemat"
  ]
  node [
    id 655
    label "konstrukcja"
  ]
  node [
    id 656
    label "konstelacja"
  ]
  node [
    id 657
    label "podmiot"
  ]
  node [
    id 658
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 659
    label "TOPR"
  ]
  node [
    id 660
    label "endecki"
  ]
  node [
    id 661
    label "przedstawicielstwo"
  ]
  node [
    id 662
    label "od&#322;am"
  ]
  node [
    id 663
    label "Cepelia"
  ]
  node [
    id 664
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 665
    label "ZBoWiD"
  ]
  node [
    id 666
    label "centrala"
  ]
  node [
    id 667
    label "GOPR"
  ]
  node [
    id 668
    label "ZOMO"
  ]
  node [
    id 669
    label "ZMP"
  ]
  node [
    id 670
    label "komitet_koordynacyjny"
  ]
  node [
    id 671
    label "przybud&#243;wka"
  ]
  node [
    id 672
    label "boj&#243;wka"
  ]
  node [
    id 673
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 674
    label "proces"
  ]
  node [
    id 675
    label "kompozycja"
  ]
  node [
    id 676
    label "uzbrajanie"
  ]
  node [
    id 677
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 678
    label "co&#347;"
  ]
  node [
    id 679
    label "budynek"
  ]
  node [
    id 680
    label "thing"
  ]
  node [
    id 681
    label "poj&#281;cie"
  ]
  node [
    id 682
    label "program"
  ]
  node [
    id 683
    label "rzecz"
  ]
  node [
    id 684
    label "mass-media"
  ]
  node [
    id 685
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 686
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 687
    label "przekazior"
  ]
  node [
    id 688
    label "medium"
  ]
  node [
    id 689
    label "ornament"
  ]
  node [
    id 690
    label "splot"
  ]
  node [
    id 691
    label "braid"
  ]
  node [
    id 692
    label "szachulec"
  ]
  node [
    id 693
    label "b&#322;&#261;d"
  ]
  node [
    id 694
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 695
    label "nawijad&#322;o"
  ]
  node [
    id 696
    label "sznur"
  ]
  node [
    id 697
    label "motowid&#322;o"
  ]
  node [
    id 698
    label "makaron"
  ]
  node [
    id 699
    label "internet"
  ]
  node [
    id 700
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 701
    label "kartka"
  ]
  node [
    id 702
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 703
    label "logowanie"
  ]
  node [
    id 704
    label "plik"
  ]
  node [
    id 705
    label "s&#261;d"
  ]
  node [
    id 706
    label "adres_internetowy"
  ]
  node [
    id 707
    label "linia"
  ]
  node [
    id 708
    label "serwis_internetowy"
  ]
  node [
    id 709
    label "bok"
  ]
  node [
    id 710
    label "skr&#281;canie"
  ]
  node [
    id 711
    label "skr&#281;ca&#263;"
  ]
  node [
    id 712
    label "orientowanie"
  ]
  node [
    id 713
    label "skr&#281;ci&#263;"
  ]
  node [
    id 714
    label "uj&#281;cie"
  ]
  node [
    id 715
    label "zorientowanie"
  ]
  node [
    id 716
    label "zorientowa&#263;"
  ]
  node [
    id 717
    label "g&#243;ra"
  ]
  node [
    id 718
    label "orientowa&#263;"
  ]
  node [
    id 719
    label "voice"
  ]
  node [
    id 720
    label "orientacja"
  ]
  node [
    id 721
    label "powierzchnia"
  ]
  node [
    id 722
    label "forma"
  ]
  node [
    id 723
    label "skr&#281;cenie"
  ]
  node [
    id 724
    label "paj&#261;k"
  ]
  node [
    id 725
    label "devise"
  ]
  node [
    id 726
    label "wyjmowa&#263;"
  ]
  node [
    id 727
    label "project"
  ]
  node [
    id 728
    label "my&#347;le&#263;"
  ]
  node [
    id 729
    label "produkowa&#263;"
  ]
  node [
    id 730
    label "uk&#322;ada&#263;"
  ]
  node [
    id 731
    label "tworzy&#263;"
  ]
  node [
    id 732
    label "wyj&#261;&#263;"
  ]
  node [
    id 733
    label "stworzy&#263;"
  ]
  node [
    id 734
    label "zasadzi&#263;"
  ]
  node [
    id 735
    label "dostawca"
  ]
  node [
    id 736
    label "telefonia"
  ]
  node [
    id 737
    label "meme"
  ]
  node [
    id 738
    label "hazard"
  ]
  node [
    id 739
    label "molestowanie_seksualne"
  ]
  node [
    id 740
    label "piel&#281;gnacja"
  ]
  node [
    id 741
    label "zwierz&#281;_domowe"
  ]
  node [
    id 742
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 743
    label "ma&#322;y"
  ]
  node [
    id 744
    label "pauzowa&#263;"
  ]
  node [
    id 745
    label "by&#263;"
  ]
  node [
    id 746
    label "oczekiwa&#263;"
  ]
  node [
    id 747
    label "decydowa&#263;"
  ]
  node [
    id 748
    label "sp&#281;dza&#263;"
  ]
  node [
    id 749
    label "look"
  ]
  node [
    id 750
    label "hold"
  ]
  node [
    id 751
    label "anticipate"
  ]
  node [
    id 752
    label "stylizacja"
  ]
  node [
    id 753
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 754
    label "mie&#263;_miejsce"
  ]
  node [
    id 755
    label "equal"
  ]
  node [
    id 756
    label "trwa&#263;"
  ]
  node [
    id 757
    label "chodzi&#263;"
  ]
  node [
    id 758
    label "si&#281;ga&#263;"
  ]
  node [
    id 759
    label "obecno&#347;&#263;"
  ]
  node [
    id 760
    label "stand"
  ]
  node [
    id 761
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 762
    label "uczestniczy&#263;"
  ]
  node [
    id 763
    label "hesitate"
  ]
  node [
    id 764
    label "odpoczywa&#263;"
  ]
  node [
    id 765
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 766
    label "decide"
  ]
  node [
    id 767
    label "klasyfikator"
  ]
  node [
    id 768
    label "mean"
  ]
  node [
    id 769
    label "usuwa&#263;"
  ]
  node [
    id 770
    label "base_on_balls"
  ]
  node [
    id 771
    label "przykrzy&#263;"
  ]
  node [
    id 772
    label "p&#281;dzi&#263;"
  ]
  node [
    id 773
    label "przep&#281;dza&#263;"
  ]
  node [
    id 774
    label "doprowadza&#263;"
  ]
  node [
    id 775
    label "authorize"
  ]
  node [
    id 776
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 777
    label "chcie&#263;"
  ]
  node [
    id 778
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 779
    label "umocowa&#263;"
  ]
  node [
    id 780
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 781
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 782
    label "procesualistyka"
  ]
  node [
    id 783
    label "regu&#322;a_Allena"
  ]
  node [
    id 784
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 785
    label "kryminalistyka"
  ]
  node [
    id 786
    label "szko&#322;a"
  ]
  node [
    id 787
    label "kierunek"
  ]
  node [
    id 788
    label "zasada_d'Alemberta"
  ]
  node [
    id 789
    label "obserwacja"
  ]
  node [
    id 790
    label "normatywizm"
  ]
  node [
    id 791
    label "jurisprudence"
  ]
  node [
    id 792
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 793
    label "kultura_duchowa"
  ]
  node [
    id 794
    label "przepis"
  ]
  node [
    id 795
    label "prawo_karne_procesowe"
  ]
  node [
    id 796
    label "criterion"
  ]
  node [
    id 797
    label "kazuistyka"
  ]
  node [
    id 798
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 799
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 800
    label "kryminologia"
  ]
  node [
    id 801
    label "opis"
  ]
  node [
    id 802
    label "regu&#322;a_Glogera"
  ]
  node [
    id 803
    label "prawo_Mendla"
  ]
  node [
    id 804
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 805
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 806
    label "prawo_karne"
  ]
  node [
    id 807
    label "legislacyjnie"
  ]
  node [
    id 808
    label "twierdzenie"
  ]
  node [
    id 809
    label "cywilistyka"
  ]
  node [
    id 810
    label "judykatura"
  ]
  node [
    id 811
    label "kanonistyka"
  ]
  node [
    id 812
    label "standard"
  ]
  node [
    id 813
    label "nauka_prawa"
  ]
  node [
    id 814
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 815
    label "law"
  ]
  node [
    id 816
    label "qualification"
  ]
  node [
    id 817
    label "dominion"
  ]
  node [
    id 818
    label "wykonawczy"
  ]
  node [
    id 819
    label "zasada"
  ]
  node [
    id 820
    label "normalizacja"
  ]
  node [
    id 821
    label "exposition"
  ]
  node [
    id 822
    label "obja&#347;nienie"
  ]
  node [
    id 823
    label "model"
  ]
  node [
    id 824
    label "organizowa&#263;"
  ]
  node [
    id 825
    label "ordinariness"
  ]
  node [
    id 826
    label "zorganizowa&#263;"
  ]
  node [
    id 827
    label "taniec_towarzyski"
  ]
  node [
    id 828
    label "organizowanie"
  ]
  node [
    id 829
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 830
    label "zorganizowanie"
  ]
  node [
    id 831
    label "przebieg"
  ]
  node [
    id 832
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 833
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 834
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 835
    label "praktyka"
  ]
  node [
    id 836
    label "przeorientowywanie"
  ]
  node [
    id 837
    label "studia"
  ]
  node [
    id 838
    label "przeorientowywa&#263;"
  ]
  node [
    id 839
    label "przeorientowanie"
  ]
  node [
    id 840
    label "przeorientowa&#263;"
  ]
  node [
    id 841
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 842
    label "metoda"
  ]
  node [
    id 843
    label "spos&#243;b"
  ]
  node [
    id 844
    label "ideologia"
  ]
  node [
    id 845
    label "bearing"
  ]
  node [
    id 846
    label "do&#347;wiadczenie"
  ]
  node [
    id 847
    label "teren_szko&#322;y"
  ]
  node [
    id 848
    label "wiedza"
  ]
  node [
    id 849
    label "Mickiewicz"
  ]
  node [
    id 850
    label "kwalifikacje"
  ]
  node [
    id 851
    label "podr&#281;cznik"
  ]
  node [
    id 852
    label "absolwent"
  ]
  node [
    id 853
    label "school"
  ]
  node [
    id 854
    label "zda&#263;"
  ]
  node [
    id 855
    label "gabinet"
  ]
  node [
    id 856
    label "urszulanki"
  ]
  node [
    id 857
    label "sztuba"
  ]
  node [
    id 858
    label "&#322;awa_szkolna"
  ]
  node [
    id 859
    label "nauka"
  ]
  node [
    id 860
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 861
    label "przepisa&#263;"
  ]
  node [
    id 862
    label "muzyka"
  ]
  node [
    id 863
    label "grupa"
  ]
  node [
    id 864
    label "form"
  ]
  node [
    id 865
    label "lekcja"
  ]
  node [
    id 866
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 867
    label "przepisanie"
  ]
  node [
    id 868
    label "skolaryzacja"
  ]
  node [
    id 869
    label "zdanie"
  ]
  node [
    id 870
    label "stopek"
  ]
  node [
    id 871
    label "sekretariat"
  ]
  node [
    id 872
    label "lesson"
  ]
  node [
    id 873
    label "niepokalanki"
  ]
  node [
    id 874
    label "szkolenie"
  ]
  node [
    id 875
    label "kara"
  ]
  node [
    id 876
    label "tablica"
  ]
  node [
    id 877
    label "posiada&#263;"
  ]
  node [
    id 878
    label "potencja&#322;"
  ]
  node [
    id 879
    label "wyb&#243;r"
  ]
  node [
    id 880
    label "prospect"
  ]
  node [
    id 881
    label "ability"
  ]
  node [
    id 882
    label "obliczeniowo"
  ]
  node [
    id 883
    label "alternatywa"
  ]
  node [
    id 884
    label "operator_modalny"
  ]
  node [
    id 885
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 886
    label "alternatywa_Fredholma"
  ]
  node [
    id 887
    label "oznajmianie"
  ]
  node [
    id 888
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 889
    label "teoria"
  ]
  node [
    id 890
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 891
    label "paradoks_Leontiefa"
  ]
  node [
    id 892
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 893
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 894
    label "teza"
  ]
  node [
    id 895
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 896
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 897
    label "twierdzenie_Pettisa"
  ]
  node [
    id 898
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 899
    label "twierdzenie_Maya"
  ]
  node [
    id 900
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 901
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 902
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 903
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 904
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 905
    label "zapewnianie"
  ]
  node [
    id 906
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 907
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 908
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 909
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 910
    label "twierdzenie_Stokesa"
  ]
  node [
    id 911
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 912
    label "twierdzenie_Cevy"
  ]
  node [
    id 913
    label "twierdzenie_Pascala"
  ]
  node [
    id 914
    label "proposition"
  ]
  node [
    id 915
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 916
    label "komunikowanie"
  ]
  node [
    id 917
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 918
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 919
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 920
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 921
    label "relacja"
  ]
  node [
    id 922
    label "badanie"
  ]
  node [
    id 923
    label "proces_my&#347;lowy"
  ]
  node [
    id 924
    label "remark"
  ]
  node [
    id 925
    label "stwierdzenie"
  ]
  node [
    id 926
    label "observation"
  ]
  node [
    id 927
    label "calibration"
  ]
  node [
    id 928
    label "operacja"
  ]
  node [
    id 929
    label "porz&#261;dek"
  ]
  node [
    id 930
    label "dominance"
  ]
  node [
    id 931
    label "zabieg"
  ]
  node [
    id 932
    label "standardization"
  ]
  node [
    id 933
    label "orzecznictwo"
  ]
  node [
    id 934
    label "wykonawczo"
  ]
  node [
    id 935
    label "byt"
  ]
  node [
    id 936
    label "osobowo&#347;&#263;"
  ]
  node [
    id 937
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 938
    label "procedura"
  ]
  node [
    id 939
    label "set"
  ]
  node [
    id 940
    label "nada&#263;"
  ]
  node [
    id 941
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 942
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 943
    label "cook"
  ]
  node [
    id 944
    label "base"
  ]
  node [
    id 945
    label "umowa"
  ]
  node [
    id 946
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 947
    label "moralno&#347;&#263;"
  ]
  node [
    id 948
    label "podstawa"
  ]
  node [
    id 949
    label "substancja"
  ]
  node [
    id 950
    label "prawid&#322;o"
  ]
  node [
    id 951
    label "norma_prawna"
  ]
  node [
    id 952
    label "przedawnienie_si&#281;"
  ]
  node [
    id 953
    label "przedawnianie_si&#281;"
  ]
  node [
    id 954
    label "porada"
  ]
  node [
    id 955
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 956
    label "regulation"
  ]
  node [
    id 957
    label "recepta"
  ]
  node [
    id 958
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 959
    label "kodeks"
  ]
  node [
    id 960
    label "casuistry"
  ]
  node [
    id 961
    label "manipulacja"
  ]
  node [
    id 962
    label "probabilizm"
  ]
  node [
    id 963
    label "dermatoglifika"
  ]
  node [
    id 964
    label "mikro&#347;lad"
  ]
  node [
    id 965
    label "technika_&#347;ledcza"
  ]
  node [
    id 966
    label "dzia&#322;"
  ]
  node [
    id 967
    label "podj&#261;&#263;"
  ]
  node [
    id 968
    label "sta&#263;_si&#281;"
  ]
  node [
    id 969
    label "determine"
  ]
  node [
    id 970
    label "zrobi&#263;"
  ]
  node [
    id 971
    label "zareagowa&#263;"
  ]
  node [
    id 972
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 973
    label "draw"
  ]
  node [
    id 974
    label "allude"
  ]
  node [
    id 975
    label "zmieni&#263;"
  ]
  node [
    id 976
    label "zacz&#261;&#263;"
  ]
  node [
    id 977
    label "raise"
  ]
  node [
    id 978
    label "post&#261;pi&#263;"
  ]
  node [
    id 979
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 980
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 981
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 982
    label "appoint"
  ]
  node [
    id 983
    label "wystylizowa&#263;"
  ]
  node [
    id 984
    label "cause"
  ]
  node [
    id 985
    label "przerobi&#263;"
  ]
  node [
    id 986
    label "nabra&#263;"
  ]
  node [
    id 987
    label "make"
  ]
  node [
    id 988
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 989
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 990
    label "wydali&#263;"
  ]
  node [
    id 991
    label "niestandardowo"
  ]
  node [
    id 992
    label "kolejny"
  ]
  node [
    id 993
    label "osobno"
  ]
  node [
    id 994
    label "r&#243;&#380;ny"
  ]
  node [
    id 995
    label "inszy"
  ]
  node [
    id 996
    label "niestandardowy"
  ]
  node [
    id 997
    label "niekonwencjonalnie"
  ]
  node [
    id 998
    label "nietypowo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 550
  ]
  edge [
    source 11
    target 551
  ]
  edge [
    source 11
    target 43
  ]
  edge [
    source 11
    target 552
  ]
  edge [
    source 11
    target 553
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 554
  ]
  edge [
    source 11
    target 34
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 11
    target 38
  ]
  edge [
    source 11
    target 39
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 555
  ]
  edge [
    source 11
    target 556
  ]
  edge [
    source 11
    target 557
  ]
  edge [
    source 11
    target 558
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 559
  ]
  edge [
    source 11
    target 560
  ]
  edge [
    source 11
    target 561
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 563
  ]
  edge [
    source 11
    target 564
  ]
  edge [
    source 11
    target 565
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 11
    target 566
  ]
  edge [
    source 11
    target 567
  ]
  edge [
    source 11
    target 568
  ]
  edge [
    source 11
    target 569
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 570
  ]
  edge [
    source 11
    target 571
  ]
  edge [
    source 11
    target 572
  ]
  edge [
    source 11
    target 573
  ]
  edge [
    source 11
    target 574
  ]
  edge [
    source 11
    target 575
  ]
  edge [
    source 11
    target 576
  ]
  edge [
    source 11
    target 577
  ]
  edge [
    source 11
    target 578
  ]
  edge [
    source 11
    target 579
  ]
  edge [
    source 11
    target 580
  ]
  edge [
    source 11
    target 581
  ]
  edge [
    source 11
    target 582
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 583
  ]
  edge [
    source 12
    target 584
  ]
  edge [
    source 12
    target 585
  ]
  edge [
    source 12
    target 586
  ]
  edge [
    source 12
    target 587
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 589
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 590
  ]
  edge [
    source 12
    target 591
  ]
  edge [
    source 12
    target 592
  ]
  edge [
    source 12
    target 593
  ]
  edge [
    source 12
    target 594
  ]
  edge [
    source 12
    target 595
  ]
  edge [
    source 12
    target 596
  ]
  edge [
    source 12
    target 597
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 599
  ]
  edge [
    source 12
    target 600
  ]
  edge [
    source 12
    target 601
  ]
  edge [
    source 12
    target 602
  ]
  edge [
    source 12
    target 603
  ]
  edge [
    source 12
    target 604
  ]
  edge [
    source 12
    target 605
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 607
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 609
  ]
  edge [
    source 12
    target 610
  ]
  edge [
    source 12
    target 611
  ]
  edge [
    source 12
    target 612
  ]
  edge [
    source 12
    target 613
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 614
  ]
  edge [
    source 12
    target 615
  ]
  edge [
    source 12
    target 616
  ]
  edge [
    source 12
    target 617
  ]
  edge [
    source 12
    target 618
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 619
  ]
  edge [
    source 12
    target 620
  ]
  edge [
    source 12
    target 621
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 622
  ]
  edge [
    source 12
    target 623
  ]
  edge [
    source 12
    target 624
  ]
  edge [
    source 12
    target 625
  ]
  edge [
    source 12
    target 626
  ]
  edge [
    source 12
    target 627
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 628
  ]
  edge [
    source 12
    target 629
  ]
  edge [
    source 12
    target 30
  ]
  edge [
    source 12
    target 630
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 632
  ]
  edge [
    source 12
    target 633
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 634
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 635
  ]
  edge [
    source 12
    target 636
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 637
  ]
  edge [
    source 12
    target 638
  ]
  edge [
    source 12
    target 639
  ]
  edge [
    source 12
    target 640
  ]
  edge [
    source 12
    target 641
  ]
  edge [
    source 12
    target 642
  ]
  edge [
    source 12
    target 643
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 135
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 744
  ]
  edge [
    source 13
    target 745
  ]
  edge [
    source 13
    target 746
  ]
  edge [
    source 13
    target 747
  ]
  edge [
    source 13
    target 748
  ]
  edge [
    source 13
    target 749
  ]
  edge [
    source 13
    target 750
  ]
  edge [
    source 13
    target 751
  ]
  edge [
    source 13
    target 752
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 753
  ]
  edge [
    source 13
    target 754
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 756
  ]
  edge [
    source 13
    target 757
  ]
  edge [
    source 13
    target 758
  ]
  edge [
    source 13
    target 49
  ]
  edge [
    source 13
    target 759
  ]
  edge [
    source 13
    target 760
  ]
  edge [
    source 13
    target 761
  ]
  edge [
    source 13
    target 762
  ]
  edge [
    source 13
    target 763
  ]
  edge [
    source 13
    target 764
  ]
  edge [
    source 13
    target 765
  ]
  edge [
    source 13
    target 766
  ]
  edge [
    source 13
    target 767
  ]
  edge [
    source 13
    target 768
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 769
  ]
  edge [
    source 13
    target 770
  ]
  edge [
    source 13
    target 771
  ]
  edge [
    source 13
    target 772
  ]
  edge [
    source 13
    target 773
  ]
  edge [
    source 13
    target 774
  ]
  edge [
    source 13
    target 775
  ]
  edge [
    source 13
    target 776
  ]
  edge [
    source 13
    target 777
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 779
  ]
  edge [
    source 17
    target 780
  ]
  edge [
    source 17
    target 781
  ]
  edge [
    source 17
    target 782
  ]
  edge [
    source 17
    target 783
  ]
  edge [
    source 17
    target 784
  ]
  edge [
    source 17
    target 785
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 786
  ]
  edge [
    source 17
    target 787
  ]
  edge [
    source 17
    target 788
  ]
  edge [
    source 17
    target 789
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 791
  ]
  edge [
    source 17
    target 792
  ]
  edge [
    source 17
    target 793
  ]
  edge [
    source 17
    target 794
  ]
  edge [
    source 17
    target 795
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 797
  ]
  edge [
    source 17
    target 798
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 801
  ]
  edge [
    source 17
    target 802
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 17
    target 805
  ]
  edge [
    source 17
    target 806
  ]
  edge [
    source 17
    target 807
  ]
  edge [
    source 17
    target 808
  ]
  edge [
    source 17
    target 809
  ]
  edge [
    source 17
    target 810
  ]
  edge [
    source 17
    target 811
  ]
  edge [
    source 17
    target 812
  ]
  edge [
    source 17
    target 813
  ]
  edge [
    source 17
    target 814
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 815
  ]
  edge [
    source 17
    target 816
  ]
  edge [
    source 17
    target 817
  ]
  edge [
    source 17
    target 818
  ]
  edge [
    source 17
    target 819
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 47
  ]
  edge [
    source 17
    target 821
  ]
  edge [
    source 17
    target 72
  ]
  edge [
    source 17
    target 822
  ]
  edge [
    source 17
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 173
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 649
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 30
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 569
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 707
  ]
  edge [
    source 17
    target 709
  ]
  edge [
    source 17
    target 710
  ]
  edge [
    source 17
    target 711
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 715
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 716
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 718
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 720
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 723
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 77
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 555
  ]
  edge [
    source 17
    target 492
  ]
  edge [
    source 17
    target 175
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 705
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 409
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 592
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 457
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
]
