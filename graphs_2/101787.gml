graph [
  node [
    id 0
    label "astrologia"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ani"
    origin "text"
  ]
  node [
    id 3
    label "&#322;atwy"
    origin "text"
  ]
  node [
    id 4
    label "rozrywka"
    origin "text"
  ]
  node [
    id 5
    label "te&#380;"
    origin "text"
  ]
  node [
    id 6
    label "zabawa"
    origin "text"
  ]
  node [
    id 7
    label "grunt"
    origin "text"
  ]
  node [
    id 8
    label "rzecz"
    origin "text"
  ]
  node [
    id 9
    label "wcale"
    origin "text"
  ]
  node [
    id 10
    label "rozleg&#322;y"
    origin "text"
  ]
  node [
    id 11
    label "skomplikowany"
    origin "text"
  ]
  node [
    id 12
    label "system"
    origin "text"
  ]
  node [
    id 13
    label "wiedza"
    origin "text"
  ]
  node [
    id 14
    label "czego"
    origin "text"
  ]
  node [
    id 15
    label "u&#347;wiadamia&#263;"
    origin "text"
  ]
  node [
    id 16
    label "siebie"
    origin "text"
  ]
  node [
    id 17
    label "og&#243;&#322;"
    origin "text"
  ]
  node [
    id 18
    label "liczny"
    origin "text"
  ]
  node [
    id 19
    label "rzesza"
    origin "text"
  ]
  node [
    id 20
    label "amator"
    origin "text"
  ]
  node [
    id 21
    label "bezsensowny"
    origin "text"
  ]
  node [
    id 22
    label "horoskop"
    origin "text"
  ]
  node [
    id 23
    label "gazetowe"
    origin "text"
  ]
  node [
    id 24
    label "maja"
    origin "text"
  ]
  node [
    id 25
    label "bowiem"
    origin "text"
  ]
  node [
    id 26
    label "nic"
    origin "text"
  ]
  node [
    id 27
    label "wsp&#243;lny"
    origin "text"
  ]
  node [
    id 28
    label "ur&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 29
    label "najstarszy"
    origin "text"
  ]
  node [
    id 30
    label "znany"
    origin "text"
  ]
  node [
    id 31
    label "obszar"
    origin "text"
  ]
  node [
    id 32
    label "usystematyzowa&#263;"
    origin "text"
  ]
  node [
    id 33
    label "zg&#322;&#281;bia&#263;"
    origin "text"
  ]
  node [
    id 34
    label "przez"
    origin "text"
  ]
  node [
    id 35
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 36
    label "umys&#322;"
    origin "text"
  ]
  node [
    id 37
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 38
    label "bez"
    origin "text"
  ]
  node [
    id 39
    label "wyj&#261;tek"
    origin "text"
  ]
  node [
    id 40
    label "epoka"
    origin "text"
  ]
  node [
    id 41
    label "historyczny"
    origin "text"
  ]
  node [
    id 42
    label "uprawianie"
    origin "text"
  ]
  node [
    id 43
    label "polega&#263;"
    origin "text"
  ]
  node [
    id 44
    label "oblicza&#263;"
    origin "text"
  ]
  node [
    id 45
    label "nad"
    origin "text"
  ]
  node [
    id 46
    label "wszystko"
    origin "text"
  ]
  node [
    id 47
    label "interpretacja"
    origin "text"
  ]
  node [
    id 48
    label "wymaga&#263;"
    origin "text"
  ]
  node [
    id 49
    label "d&#322;ugotrwa&#322;y"
    origin "text"
  ]
  node [
    id 50
    label "studia"
    origin "text"
  ]
  node [
    id 51
    label "umiej&#281;tny"
    origin "text"
  ]
  node [
    id 52
    label "&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 53
    label "element"
    origin "text"
  ]
  node [
    id 54
    label "psychologiczny"
    origin "text"
  ]
  node [
    id 55
    label "astronomiczny"
    origin "text"
  ]
  node [
    id 56
    label "biologiczny"
    origin "text"
  ]
  node [
    id 57
    label "medyczny"
    origin "text"
  ]
  node [
    id 58
    label "socjologiczny"
    origin "text"
  ]
  node [
    id 59
    label "przed"
    origin "text"
  ]
  node [
    id 60
    label "wszyscy"
    origin "text"
  ]
  node [
    id 61
    label "wieloletni"
    origin "text"
  ]
  node [
    id 62
    label "praktyka"
    origin "text"
  ]
  node [
    id 63
    label "astrology"
  ]
  node [
    id 64
    label "astrologia_naturalna"
  ]
  node [
    id 65
    label "wr&#243;&#380;biarstwo"
  ]
  node [
    id 66
    label "augury"
  ]
  node [
    id 67
    label "magia"
  ]
  node [
    id 68
    label "przepowiednia"
  ]
  node [
    id 69
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 70
    label "mie&#263;_miejsce"
  ]
  node [
    id 71
    label "equal"
  ]
  node [
    id 72
    label "trwa&#263;"
  ]
  node [
    id 73
    label "chodzi&#263;"
  ]
  node [
    id 74
    label "si&#281;ga&#263;"
  ]
  node [
    id 75
    label "stan"
  ]
  node [
    id 76
    label "obecno&#347;&#263;"
  ]
  node [
    id 77
    label "stand"
  ]
  node [
    id 78
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 79
    label "uczestniczy&#263;"
  ]
  node [
    id 80
    label "participate"
  ]
  node [
    id 81
    label "robi&#263;"
  ]
  node [
    id 82
    label "istnie&#263;"
  ]
  node [
    id 83
    label "pozostawa&#263;"
  ]
  node [
    id 84
    label "zostawa&#263;"
  ]
  node [
    id 85
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 86
    label "adhere"
  ]
  node [
    id 87
    label "compass"
  ]
  node [
    id 88
    label "korzysta&#263;"
  ]
  node [
    id 89
    label "appreciation"
  ]
  node [
    id 90
    label "osi&#261;ga&#263;"
  ]
  node [
    id 91
    label "dociera&#263;"
  ]
  node [
    id 92
    label "get"
  ]
  node [
    id 93
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 94
    label "mierzy&#263;"
  ]
  node [
    id 95
    label "u&#380;ywa&#263;"
  ]
  node [
    id 96
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 97
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 98
    label "exsert"
  ]
  node [
    id 99
    label "being"
  ]
  node [
    id 100
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 101
    label "cecha"
  ]
  node [
    id 102
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 103
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 104
    label "p&#322;ywa&#263;"
  ]
  node [
    id 105
    label "run"
  ]
  node [
    id 106
    label "bangla&#263;"
  ]
  node [
    id 107
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 108
    label "przebiega&#263;"
  ]
  node [
    id 109
    label "wk&#322;ada&#263;"
  ]
  node [
    id 110
    label "proceed"
  ]
  node [
    id 111
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 112
    label "carry"
  ]
  node [
    id 113
    label "bywa&#263;"
  ]
  node [
    id 114
    label "dziama&#263;"
  ]
  node [
    id 115
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 116
    label "stara&#263;_si&#281;"
  ]
  node [
    id 117
    label "para"
  ]
  node [
    id 118
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 119
    label "str&#243;j"
  ]
  node [
    id 120
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 121
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 122
    label "krok"
  ]
  node [
    id 123
    label "tryb"
  ]
  node [
    id 124
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 125
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 126
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 127
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 128
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 129
    label "continue"
  ]
  node [
    id 130
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 131
    label "Ohio"
  ]
  node [
    id 132
    label "wci&#281;cie"
  ]
  node [
    id 133
    label "Nowy_York"
  ]
  node [
    id 134
    label "warstwa"
  ]
  node [
    id 135
    label "samopoczucie"
  ]
  node [
    id 136
    label "Illinois"
  ]
  node [
    id 137
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 138
    label "state"
  ]
  node [
    id 139
    label "Jukatan"
  ]
  node [
    id 140
    label "Kalifornia"
  ]
  node [
    id 141
    label "Wirginia"
  ]
  node [
    id 142
    label "wektor"
  ]
  node [
    id 143
    label "Teksas"
  ]
  node [
    id 144
    label "Goa"
  ]
  node [
    id 145
    label "Waszyngton"
  ]
  node [
    id 146
    label "miejsce"
  ]
  node [
    id 147
    label "Massachusetts"
  ]
  node [
    id 148
    label "Alaska"
  ]
  node [
    id 149
    label "Arakan"
  ]
  node [
    id 150
    label "Hawaje"
  ]
  node [
    id 151
    label "Maryland"
  ]
  node [
    id 152
    label "punkt"
  ]
  node [
    id 153
    label "Michigan"
  ]
  node [
    id 154
    label "Arizona"
  ]
  node [
    id 155
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 156
    label "Georgia"
  ]
  node [
    id 157
    label "poziom"
  ]
  node [
    id 158
    label "Pensylwania"
  ]
  node [
    id 159
    label "shape"
  ]
  node [
    id 160
    label "Luizjana"
  ]
  node [
    id 161
    label "Nowy_Meksyk"
  ]
  node [
    id 162
    label "Alabama"
  ]
  node [
    id 163
    label "ilo&#347;&#263;"
  ]
  node [
    id 164
    label "Kansas"
  ]
  node [
    id 165
    label "Oregon"
  ]
  node [
    id 166
    label "Floryda"
  ]
  node [
    id 167
    label "Oklahoma"
  ]
  node [
    id 168
    label "jednostka_administracyjna"
  ]
  node [
    id 169
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 170
    label "&#322;atwo"
  ]
  node [
    id 171
    label "letki"
  ]
  node [
    id 172
    label "prosty"
  ]
  node [
    id 173
    label "&#322;acny"
  ]
  node [
    id 174
    label "snadny"
  ]
  node [
    id 175
    label "przyjemny"
  ]
  node [
    id 176
    label "lekki"
  ]
  node [
    id 177
    label "delikatny"
  ]
  node [
    id 178
    label "r&#261;czy"
  ]
  node [
    id 179
    label "sprawny"
  ]
  node [
    id 180
    label "beztroski"
  ]
  node [
    id 181
    label "snadnie"
  ]
  node [
    id 182
    label "&#322;acno"
  ]
  node [
    id 183
    label "prosto"
  ]
  node [
    id 184
    label "przyjemnie"
  ]
  node [
    id 185
    label "&#322;atwie"
  ]
  node [
    id 186
    label "szybko"
  ]
  node [
    id 187
    label "dobry"
  ]
  node [
    id 188
    label "skromny"
  ]
  node [
    id 189
    label "po_prostu"
  ]
  node [
    id 190
    label "naturalny"
  ]
  node [
    id 191
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 192
    label "rozprostowanie"
  ]
  node [
    id 193
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 194
    label "prostowanie_si&#281;"
  ]
  node [
    id 195
    label "niepozorny"
  ]
  node [
    id 196
    label "cios"
  ]
  node [
    id 197
    label "prostoduszny"
  ]
  node [
    id 198
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 199
    label "naiwny"
  ]
  node [
    id 200
    label "prostowanie"
  ]
  node [
    id 201
    label "zwyk&#322;y"
  ]
  node [
    id 202
    label "czasoumilacz"
  ]
  node [
    id 203
    label "odpoczynek"
  ]
  node [
    id 204
    label "game"
  ]
  node [
    id 205
    label "urozmaicenie"
  ]
  node [
    id 206
    label "wyraj"
  ]
  node [
    id 207
    label "wczas"
  ]
  node [
    id 208
    label "diversion"
  ]
  node [
    id 209
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 210
    label "impreza"
  ]
  node [
    id 211
    label "igraszka"
  ]
  node [
    id 212
    label "taniec"
  ]
  node [
    id 213
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 214
    label "gambling"
  ]
  node [
    id 215
    label "chwyt"
  ]
  node [
    id 216
    label "igra"
  ]
  node [
    id 217
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 218
    label "nabawienie_si&#281;"
  ]
  node [
    id 219
    label "ubaw"
  ]
  node [
    id 220
    label "wodzirej"
  ]
  node [
    id 221
    label "charakterystyka"
  ]
  node [
    id 222
    label "m&#322;ot"
  ]
  node [
    id 223
    label "znak"
  ]
  node [
    id 224
    label "drzewo"
  ]
  node [
    id 225
    label "pr&#243;ba"
  ]
  node [
    id 226
    label "attribute"
  ]
  node [
    id 227
    label "marka"
  ]
  node [
    id 228
    label "impra"
  ]
  node [
    id 229
    label "przyj&#281;cie"
  ]
  node [
    id 230
    label "okazja"
  ]
  node [
    id 231
    label "party"
  ]
  node [
    id 232
    label "spos&#243;b"
  ]
  node [
    id 233
    label "kompozycja"
  ]
  node [
    id 234
    label "zacisk"
  ]
  node [
    id 235
    label "strategia"
  ]
  node [
    id 236
    label "zabieg"
  ]
  node [
    id 237
    label "ruch"
  ]
  node [
    id 238
    label "uchwyt"
  ]
  node [
    id 239
    label "uj&#281;cie"
  ]
  node [
    id 240
    label "zbi&#243;r"
  ]
  node [
    id 241
    label "karnet"
  ]
  node [
    id 242
    label "posuwisto&#347;&#263;"
  ]
  node [
    id 243
    label "utw&#243;r"
  ]
  node [
    id 244
    label "parkiet"
  ]
  node [
    id 245
    label "choreologia"
  ]
  node [
    id 246
    label "czynno&#347;&#263;"
  ]
  node [
    id 247
    label "krok_taneczny"
  ]
  node [
    id 248
    label "twardzioszek_przydro&#380;ny"
  ]
  node [
    id 249
    label "rado&#347;&#263;"
  ]
  node [
    id 250
    label "narz&#281;dzie"
  ]
  node [
    id 251
    label "Fidel_Castro"
  ]
  node [
    id 252
    label "Anders"
  ]
  node [
    id 253
    label "Ko&#347;ciuszko"
  ]
  node [
    id 254
    label "Tito"
  ]
  node [
    id 255
    label "Miko&#322;ajczyk"
  ]
  node [
    id 256
    label "lider"
  ]
  node [
    id 257
    label "Mao"
  ]
  node [
    id 258
    label "Sabataj_Cwi"
  ]
  node [
    id 259
    label "mistrz_ceremonii"
  ]
  node [
    id 260
    label "wesele"
  ]
  node [
    id 261
    label "dotleni&#263;"
  ]
  node [
    id 262
    label "pr&#243;chnica"
  ]
  node [
    id 263
    label "podglebie"
  ]
  node [
    id 264
    label "kompleks_sorpcyjny"
  ]
  node [
    id 265
    label "plantowa&#263;"
  ]
  node [
    id 266
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 267
    label "documentation"
  ]
  node [
    id 268
    label "zasadzi&#263;"
  ]
  node [
    id 269
    label "zasadzenie"
  ]
  node [
    id 270
    label "glej"
  ]
  node [
    id 271
    label "podstawowy"
  ]
  node [
    id 272
    label "ryzosfera"
  ]
  node [
    id 273
    label "glinowanie"
  ]
  node [
    id 274
    label "za&#322;o&#380;enie"
  ]
  node [
    id 275
    label "punkt_odniesienia"
  ]
  node [
    id 276
    label "martwica"
  ]
  node [
    id 277
    label "czynnik_produkcji"
  ]
  node [
    id 278
    label "geosystem"
  ]
  node [
    id 279
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 280
    label "teren"
  ]
  node [
    id 281
    label "litosfera"
  ]
  node [
    id 282
    label "podk&#322;ad"
  ]
  node [
    id 283
    label "penetrator"
  ]
  node [
    id 284
    label "przestrze&#324;"
  ]
  node [
    id 285
    label "glinowa&#263;"
  ]
  node [
    id 286
    label "dno"
  ]
  node [
    id 287
    label "powierzchnia"
  ]
  node [
    id 288
    label "podwini&#281;cie"
  ]
  node [
    id 289
    label "zap&#322;acenie"
  ]
  node [
    id 290
    label "przyodzianie"
  ]
  node [
    id 291
    label "budowla"
  ]
  node [
    id 292
    label "pokrycie"
  ]
  node [
    id 293
    label "rozebranie"
  ]
  node [
    id 294
    label "zak&#322;adka"
  ]
  node [
    id 295
    label "struktura"
  ]
  node [
    id 296
    label "poubieranie"
  ]
  node [
    id 297
    label "infliction"
  ]
  node [
    id 298
    label "spowodowanie"
  ]
  node [
    id 299
    label "pozak&#322;adanie"
  ]
  node [
    id 300
    label "program"
  ]
  node [
    id 301
    label "przebranie"
  ]
  node [
    id 302
    label "przywdzianie"
  ]
  node [
    id 303
    label "obleczenie_si&#281;"
  ]
  node [
    id 304
    label "utworzenie"
  ]
  node [
    id 305
    label "twierdzenie"
  ]
  node [
    id 306
    label "obleczenie"
  ]
  node [
    id 307
    label "umieszczenie"
  ]
  node [
    id 308
    label "przygotowywanie"
  ]
  node [
    id 309
    label "przymierzenie"
  ]
  node [
    id 310
    label "wyko&#324;czenie"
  ]
  node [
    id 311
    label "point"
  ]
  node [
    id 312
    label "przygotowanie"
  ]
  node [
    id 313
    label "proposition"
  ]
  node [
    id 314
    label "przewidzenie"
  ]
  node [
    id 315
    label "zrobienie"
  ]
  node [
    id 316
    label "integer"
  ]
  node [
    id 317
    label "liczba"
  ]
  node [
    id 318
    label "zlewanie_si&#281;"
  ]
  node [
    id 319
    label "uk&#322;ad"
  ]
  node [
    id 320
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 321
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 322
    label "pe&#322;ny"
  ]
  node [
    id 323
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 324
    label "kosmetyk"
  ]
  node [
    id 325
    label "tor"
  ]
  node [
    id 326
    label "szczep"
  ]
  node [
    id 327
    label "farba"
  ]
  node [
    id 328
    label "substrate"
  ]
  node [
    id 329
    label "layer"
  ]
  node [
    id 330
    label "melodia"
  ]
  node [
    id 331
    label "ro&#347;lina"
  ]
  node [
    id 332
    label "base"
  ]
  node [
    id 333
    label "partia"
  ]
  node [
    id 334
    label "puder"
  ]
  node [
    id 335
    label "p&#322;aszczyzna"
  ]
  node [
    id 336
    label "osady_denne"
  ]
  node [
    id 337
    label "zero"
  ]
  node [
    id 338
    label "poszycie_denne"
  ]
  node [
    id 339
    label "mato&#322;"
  ]
  node [
    id 340
    label "ground"
  ]
  node [
    id 341
    label "sp&#243;d"
  ]
  node [
    id 342
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 343
    label "mienie"
  ]
  node [
    id 344
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 345
    label "immoblizacja"
  ]
  node [
    id 346
    label "wymiar"
  ]
  node [
    id 347
    label "zakres"
  ]
  node [
    id 348
    label "kontekst"
  ]
  node [
    id 349
    label "miejsce_pracy"
  ]
  node [
    id 350
    label "nation"
  ]
  node [
    id 351
    label "krajobraz"
  ]
  node [
    id 352
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 353
    label "przyroda"
  ]
  node [
    id 354
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 355
    label "w&#322;adza"
  ]
  node [
    id 356
    label "rozdzielanie"
  ]
  node [
    id 357
    label "bezbrze&#380;e"
  ]
  node [
    id 358
    label "czasoprzestrze&#324;"
  ]
  node [
    id 359
    label "niezmierzony"
  ]
  node [
    id 360
    label "przedzielenie"
  ]
  node [
    id 361
    label "nielito&#347;ciwy"
  ]
  node [
    id 362
    label "rozdziela&#263;"
  ]
  node [
    id 363
    label "oktant"
  ]
  node [
    id 364
    label "przedzieli&#263;"
  ]
  node [
    id 365
    label "przestw&#243;r"
  ]
  node [
    id 366
    label "rozmiar"
  ]
  node [
    id 367
    label "poj&#281;cie"
  ]
  node [
    id 368
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 369
    label "zwierciad&#322;o"
  ]
  node [
    id 370
    label "capacity"
  ]
  node [
    id 371
    label "plane"
  ]
  node [
    id 372
    label "niezaawansowany"
  ]
  node [
    id 373
    label "najwa&#380;niejszy"
  ]
  node [
    id 374
    label "pocz&#261;tkowy"
  ]
  node [
    id 375
    label "podstawowo"
  ]
  node [
    id 376
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 377
    label "establish"
  ]
  node [
    id 378
    label "podstawa"
  ]
  node [
    id 379
    label "plant"
  ]
  node [
    id 380
    label "osnowa&#263;"
  ]
  node [
    id 381
    label "przymocowa&#263;"
  ]
  node [
    id 382
    label "umie&#347;ci&#263;"
  ]
  node [
    id 383
    label "wetkn&#261;&#263;"
  ]
  node [
    id 384
    label "wetkni&#281;cie"
  ]
  node [
    id 385
    label "przetkanie"
  ]
  node [
    id 386
    label "anchor"
  ]
  node [
    id 387
    label "przymocowanie"
  ]
  node [
    id 388
    label "zaczerpni&#281;cie"
  ]
  node [
    id 389
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 390
    label "interposition"
  ]
  node [
    id 391
    label "odm&#322;odzenie"
  ]
  node [
    id 392
    label "wzbogacanie"
  ]
  node [
    id 393
    label "gleba"
  ]
  node [
    id 394
    label "zabezpieczanie"
  ]
  node [
    id 395
    label "pokrywanie"
  ]
  node [
    id 396
    label "aluminize"
  ]
  node [
    id 397
    label "metalizowanie"
  ]
  node [
    id 398
    label "metalizowa&#263;"
  ]
  node [
    id 399
    label "wzbogaca&#263;"
  ]
  node [
    id 400
    label "pokrywa&#263;"
  ]
  node [
    id 401
    label "zabezpiecza&#263;"
  ]
  node [
    id 402
    label "woda"
  ]
  node [
    id 403
    label "nasyci&#263;"
  ]
  node [
    id 404
    label "od&#347;wie&#380;y&#263;_si&#281;"
  ]
  node [
    id 405
    label "dostarczy&#263;"
  ]
  node [
    id 406
    label "level"
  ]
  node [
    id 407
    label "r&#243;wna&#263;"
  ]
  node [
    id 408
    label "uprawia&#263;"
  ]
  node [
    id 409
    label "ziemia"
  ]
  node [
    id 410
    label "urz&#261;dzenie"
  ]
  node [
    id 411
    label "system_korzeniowy"
  ]
  node [
    id 412
    label "bakteria"
  ]
  node [
    id 413
    label "ubytek"
  ]
  node [
    id 414
    label "fleczer"
  ]
  node [
    id 415
    label "choroba_bakteryjna"
  ]
  node [
    id 416
    label "schorzenie"
  ]
  node [
    id 417
    label "kwas_huminowy"
  ]
  node [
    id 418
    label "substancja_szara"
  ]
  node [
    id 419
    label "tkanka"
  ]
  node [
    id 420
    label "kom&#243;rka_glejowa"
  ]
  node [
    id 421
    label "neuroglia"
  ]
  node [
    id 422
    label "kamfenol"
  ]
  node [
    id 423
    label "&#322;yko"
  ]
  node [
    id 424
    label "necrosis"
  ]
  node [
    id 425
    label "odle&#380;yna"
  ]
  node [
    id 426
    label "zanikni&#281;cie"
  ]
  node [
    id 427
    label "zmiana_wsteczna"
  ]
  node [
    id 428
    label "ska&#322;a_osadowa"
  ]
  node [
    id 429
    label "korek"
  ]
  node [
    id 430
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 431
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 432
    label "Ziemia"
  ]
  node [
    id 433
    label "sialma"
  ]
  node [
    id 434
    label "skorupa_ziemska"
  ]
  node [
    id 435
    label "warstwa_perydotytowa"
  ]
  node [
    id 436
    label "warstwa_granitowa"
  ]
  node [
    id 437
    label "powietrze"
  ]
  node [
    id 438
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 439
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 440
    label "fauna"
  ]
  node [
    id 441
    label "object"
  ]
  node [
    id 442
    label "przedmiot"
  ]
  node [
    id 443
    label "temat"
  ]
  node [
    id 444
    label "wpadni&#281;cie"
  ]
  node [
    id 445
    label "istota"
  ]
  node [
    id 446
    label "obiekt"
  ]
  node [
    id 447
    label "kultura"
  ]
  node [
    id 448
    label "wpa&#347;&#263;"
  ]
  node [
    id 449
    label "wpadanie"
  ]
  node [
    id 450
    label "wpada&#263;"
  ]
  node [
    id 451
    label "co&#347;"
  ]
  node [
    id 452
    label "budynek"
  ]
  node [
    id 453
    label "thing"
  ]
  node [
    id 454
    label "strona"
  ]
  node [
    id 455
    label "zboczenie"
  ]
  node [
    id 456
    label "om&#243;wienie"
  ]
  node [
    id 457
    label "sponiewieranie"
  ]
  node [
    id 458
    label "discipline"
  ]
  node [
    id 459
    label "omawia&#263;"
  ]
  node [
    id 460
    label "kr&#261;&#380;enie"
  ]
  node [
    id 461
    label "tre&#347;&#263;"
  ]
  node [
    id 462
    label "robienie"
  ]
  node [
    id 463
    label "sponiewiera&#263;"
  ]
  node [
    id 464
    label "entity"
  ]
  node [
    id 465
    label "tematyka"
  ]
  node [
    id 466
    label "w&#261;tek"
  ]
  node [
    id 467
    label "charakter"
  ]
  node [
    id 468
    label "zbaczanie"
  ]
  node [
    id 469
    label "program_nauczania"
  ]
  node [
    id 470
    label "om&#243;wi&#263;"
  ]
  node [
    id 471
    label "omawianie"
  ]
  node [
    id 472
    label "zbacza&#263;"
  ]
  node [
    id 473
    label "zboczy&#263;"
  ]
  node [
    id 474
    label "mentalno&#347;&#263;"
  ]
  node [
    id 475
    label "superego"
  ]
  node [
    id 476
    label "psychika"
  ]
  node [
    id 477
    label "znaczenie"
  ]
  node [
    id 478
    label "wn&#281;trze"
  ]
  node [
    id 479
    label "asymilowanie_si&#281;"
  ]
  node [
    id 480
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 481
    label "Wsch&#243;d"
  ]
  node [
    id 482
    label "praca_rolnicza"
  ]
  node [
    id 483
    label "przejmowanie"
  ]
  node [
    id 484
    label "zjawisko"
  ]
  node [
    id 485
    label "makrokosmos"
  ]
  node [
    id 486
    label "konwencja"
  ]
  node [
    id 487
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 488
    label "propriety"
  ]
  node [
    id 489
    label "przejmowa&#263;"
  ]
  node [
    id 490
    label "brzoskwiniarnia"
  ]
  node [
    id 491
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 492
    label "sztuka"
  ]
  node [
    id 493
    label "zwyczaj"
  ]
  node [
    id 494
    label "jako&#347;&#263;"
  ]
  node [
    id 495
    label "kuchnia"
  ]
  node [
    id 496
    label "tradycja"
  ]
  node [
    id 497
    label "populace"
  ]
  node [
    id 498
    label "hodowla"
  ]
  node [
    id 499
    label "religia"
  ]
  node [
    id 500
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 501
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 502
    label "przej&#281;cie"
  ]
  node [
    id 503
    label "przej&#261;&#263;"
  ]
  node [
    id 504
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 505
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 506
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 507
    label "mikrokosmos"
  ]
  node [
    id 508
    label "ekosystem"
  ]
  node [
    id 509
    label "stw&#243;r"
  ]
  node [
    id 510
    label "obiekt_naturalny"
  ]
  node [
    id 511
    label "environment"
  ]
  node [
    id 512
    label "przyra"
  ]
  node [
    id 513
    label "wszechstworzenie"
  ]
  node [
    id 514
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 515
    label "biota"
  ]
  node [
    id 516
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 517
    label "strike"
  ]
  node [
    id 518
    label "zaziera&#263;"
  ]
  node [
    id 519
    label "zachodzi&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 520
    label "czu&#263;"
  ]
  node [
    id 521
    label "spotyka&#263;"
  ]
  node [
    id 522
    label "drop"
  ]
  node [
    id 523
    label "pogo"
  ]
  node [
    id 524
    label "d&#378;wi&#281;k"
  ]
  node [
    id 525
    label "ogrom"
  ]
  node [
    id 526
    label "zapach"
  ]
  node [
    id 527
    label "wpierdala&#263;_si&#281;"
  ]
  node [
    id 528
    label "popada&#263;"
  ]
  node [
    id 529
    label "odwiedza&#263;"
  ]
  node [
    id 530
    label "wymy&#347;la&#263;"
  ]
  node [
    id 531
    label "przypomina&#263;"
  ]
  node [
    id 532
    label "ujmowa&#263;"
  ]
  node [
    id 533
    label "wp&#322;ywa&#263;"
  ]
  node [
    id 534
    label "&#347;wiat&#322;o"
  ]
  node [
    id 535
    label "fall"
  ]
  node [
    id 536
    label "chowa&#263;"
  ]
  node [
    id 537
    label "wpieprza&#263;_si&#281;"
  ]
  node [
    id 538
    label "demaskowa&#263;"
  ]
  node [
    id 539
    label "ulega&#263;"
  ]
  node [
    id 540
    label "wkl&#281;sa&#263;"
  ]
  node [
    id 541
    label "emocja"
  ]
  node [
    id 542
    label "flatten"
  ]
  node [
    id 543
    label "ulec"
  ]
  node [
    id 544
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 545
    label "collapse"
  ]
  node [
    id 546
    label "fall_upon"
  ]
  node [
    id 547
    label "ponie&#347;&#263;"
  ]
  node [
    id 548
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 549
    label "uderzy&#263;"
  ]
  node [
    id 550
    label "wymy&#347;li&#263;"
  ]
  node [
    id 551
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 552
    label "decline"
  ]
  node [
    id 553
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 554
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 555
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 556
    label "spotka&#263;"
  ]
  node [
    id 557
    label "odwiedzi&#263;"
  ]
  node [
    id 558
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 559
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 560
    label "uleganie"
  ]
  node [
    id 561
    label "dostawanie_si&#281;"
  ]
  node [
    id 562
    label "odwiedzanie"
  ]
  node [
    id 563
    label "ciecz"
  ]
  node [
    id 564
    label "spotykanie"
  ]
  node [
    id 565
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 566
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 567
    label "postrzeganie"
  ]
  node [
    id 568
    label "rzeka"
  ]
  node [
    id 569
    label "wymy&#347;lanie"
  ]
  node [
    id 570
    label "wpierdalanie_si&#281;"
  ]
  node [
    id 571
    label "ingress"
  ]
  node [
    id 572
    label "dzianie_si&#281;"
  ]
  node [
    id 573
    label "wp&#322;ywanie"
  ]
  node [
    id 574
    label "wpieprzanie_si&#281;"
  ]
  node [
    id 575
    label "overlap"
  ]
  node [
    id 576
    label "wkl&#281;sanie"
  ]
  node [
    id 577
    label "wymy&#347;lenie"
  ]
  node [
    id 578
    label "spotkanie"
  ]
  node [
    id 579
    label "wkl&#281;&#347;ni&#281;cie"
  ]
  node [
    id 580
    label "wpieprzenie_si&#281;"
  ]
  node [
    id 581
    label "ulegni&#281;cie"
  ]
  node [
    id 582
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 583
    label "poniesienie"
  ]
  node [
    id 584
    label "zaj&#347;cie_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 585
    label "odwiedzenie"
  ]
  node [
    id 586
    label "uderzenie"
  ]
  node [
    id 587
    label "zakochiwanie_si&#281;"
  ]
  node [
    id 588
    label "wp&#322;yni&#281;cie"
  ]
  node [
    id 589
    label "dostanie_si&#281;"
  ]
  node [
    id 590
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 591
    label "release"
  ]
  node [
    id 592
    label "rozbicie_si&#281;"
  ]
  node [
    id 593
    label "wpierdolenie_si&#281;"
  ]
  node [
    id 594
    label "przej&#347;cie"
  ]
  node [
    id 595
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 596
    label "rodowo&#347;&#263;"
  ]
  node [
    id 597
    label "patent"
  ]
  node [
    id 598
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 599
    label "dobra"
  ]
  node [
    id 600
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 601
    label "przej&#347;&#263;"
  ]
  node [
    id 602
    label "possession"
  ]
  node [
    id 603
    label "sprawa"
  ]
  node [
    id 604
    label "wyraz_pochodny"
  ]
  node [
    id 605
    label "fraza"
  ]
  node [
    id 606
    label "forum"
  ]
  node [
    id 607
    label "topik"
  ]
  node [
    id 608
    label "forma"
  ]
  node [
    id 609
    label "otoczka"
  ]
  node [
    id 610
    label "ni_chuja"
  ]
  node [
    id 611
    label "zupe&#322;nie"
  ]
  node [
    id 612
    label "ca&#322;kiem"
  ]
  node [
    id 613
    label "zupe&#322;ny"
  ]
  node [
    id 614
    label "wniwecz"
  ]
  node [
    id 615
    label "kompletny"
  ]
  node [
    id 616
    label "szeroki"
  ]
  node [
    id 617
    label "rozlegle"
  ]
  node [
    id 618
    label "szeroko"
  ]
  node [
    id 619
    label "rozdeptanie"
  ]
  node [
    id 620
    label "rozdeptywanie"
  ]
  node [
    id 621
    label "lu&#378;no"
  ]
  node [
    id 622
    label "across_the_board"
  ]
  node [
    id 623
    label "rozci&#261;gle"
  ]
  node [
    id 624
    label "doros&#322;y"
  ]
  node [
    id 625
    label "znaczny"
  ]
  node [
    id 626
    label "niema&#322;o"
  ]
  node [
    id 627
    label "wiele"
  ]
  node [
    id 628
    label "rozwini&#281;ty"
  ]
  node [
    id 629
    label "dorodny"
  ]
  node [
    id 630
    label "wa&#380;ny"
  ]
  node [
    id 631
    label "prawdziwy"
  ]
  node [
    id 632
    label "du&#380;o"
  ]
  node [
    id 633
    label "trudny"
  ]
  node [
    id 634
    label "skomplikowanie"
  ]
  node [
    id 635
    label "k&#322;opotliwy"
  ]
  node [
    id 636
    label "ci&#281;&#380;ko"
  ]
  node [
    id 637
    label "wymagaj&#261;cy"
  ]
  node [
    id 638
    label "skomplikowanie_si&#281;"
  ]
  node [
    id 639
    label "utrudnienie"
  ]
  node [
    id 640
    label "niezrozumia&#322;y"
  ]
  node [
    id 641
    label "trudno&#347;&#263;"
  ]
  node [
    id 642
    label "pokomplikowanie"
  ]
  node [
    id 643
    label "zamulenie"
  ]
  node [
    id 644
    label "j&#261;dro"
  ]
  node [
    id 645
    label "systemik"
  ]
  node [
    id 646
    label "rozprz&#261;c"
  ]
  node [
    id 647
    label "oprogramowanie"
  ]
  node [
    id 648
    label "systemat"
  ]
  node [
    id 649
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 650
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 651
    label "model"
  ]
  node [
    id 652
    label "usenet"
  ]
  node [
    id 653
    label "s&#261;d"
  ]
  node [
    id 654
    label "porz&#261;dek"
  ]
  node [
    id 655
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 656
    label "przyn&#281;ta"
  ]
  node [
    id 657
    label "p&#322;&#243;d"
  ]
  node [
    id 658
    label "net"
  ]
  node [
    id 659
    label "w&#281;dkarstwo"
  ]
  node [
    id 660
    label "eratem"
  ]
  node [
    id 661
    label "oddzia&#322;"
  ]
  node [
    id 662
    label "doktryna"
  ]
  node [
    id 663
    label "pulpit"
  ]
  node [
    id 664
    label "konstelacja"
  ]
  node [
    id 665
    label "jednostka_geologiczna"
  ]
  node [
    id 666
    label "o&#347;"
  ]
  node [
    id 667
    label "podsystem"
  ]
  node [
    id 668
    label "metoda"
  ]
  node [
    id 669
    label "ryba"
  ]
  node [
    id 670
    label "Leopard"
  ]
  node [
    id 671
    label "Android"
  ]
  node [
    id 672
    label "zachowanie"
  ]
  node [
    id 673
    label "cybernetyk"
  ]
  node [
    id 674
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 675
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 676
    label "method"
  ]
  node [
    id 677
    label "sk&#322;ad"
  ]
  node [
    id 678
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 679
    label "nature"
  ]
  node [
    id 680
    label "pot&#281;ga"
  ]
  node [
    id 681
    label "column"
  ]
  node [
    id 682
    label "bok"
  ]
  node [
    id 683
    label "d&#243;&#322;"
  ]
  node [
    id 684
    label "dzieci&#281;ctwo"
  ]
  node [
    id 685
    label "background"
  ]
  node [
    id 686
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 687
    label "pomys&#322;"
  ]
  node [
    id 688
    label "&#347;ciana"
  ]
  node [
    id 689
    label "relacja"
  ]
  node [
    id 690
    label "zasada"
  ]
  node [
    id 691
    label "styl_architektoniczny"
  ]
  node [
    id 692
    label "normalizacja"
  ]
  node [
    id 693
    label "pos&#322;uchanie"
  ]
  node [
    id 694
    label "skumanie"
  ]
  node [
    id 695
    label "orientacja"
  ]
  node [
    id 696
    label "wytw&#243;r"
  ]
  node [
    id 697
    label "zorientowanie"
  ]
  node [
    id 698
    label "teoria"
  ]
  node [
    id 699
    label "clasp"
  ]
  node [
    id 700
    label "przem&#243;wienie"
  ]
  node [
    id 701
    label "egzemplarz"
  ]
  node [
    id 702
    label "series"
  ]
  node [
    id 703
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 704
    label "collection"
  ]
  node [
    id 705
    label "dane"
  ]
  node [
    id 706
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 707
    label "pakiet_klimatyczny"
  ]
  node [
    id 708
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 709
    label "sum"
  ]
  node [
    id 710
    label "gathering"
  ]
  node [
    id 711
    label "album"
  ]
  node [
    id 712
    label "system_komputerowy"
  ]
  node [
    id 713
    label "sprz&#281;t"
  ]
  node [
    id 714
    label "mechanika"
  ]
  node [
    id 715
    label "konstrukcja"
  ]
  node [
    id 716
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 717
    label "moczownik"
  ]
  node [
    id 718
    label "embryo"
  ]
  node [
    id 719
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 720
    label "zarodek"
  ]
  node [
    id 721
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 722
    label "latawiec"
  ]
  node [
    id 723
    label "reengineering"
  ]
  node [
    id 724
    label "urz&#261;dzenie_mobilne"
  ]
  node [
    id 725
    label "grupa_dyskusyjna"
  ]
  node [
    id 726
    label "doctrine"
  ]
  node [
    id 727
    label "pu&#322;apka"
  ]
  node [
    id 728
    label "pon&#281;ta"
  ]
  node [
    id 729
    label "wabik"
  ]
  node [
    id 730
    label "sport"
  ]
  node [
    id 731
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 732
    label "kr&#281;gowiec"
  ]
  node [
    id 733
    label "cz&#322;owiek"
  ]
  node [
    id 734
    label "doniczkowiec"
  ]
  node [
    id 735
    label "mi&#281;so"
  ]
  node [
    id 736
    label "patroszy&#263;"
  ]
  node [
    id 737
    label "rakowato&#347;&#263;"
  ]
  node [
    id 738
    label "ryby"
  ]
  node [
    id 739
    label "fish"
  ]
  node [
    id 740
    label "linia_boczna"
  ]
  node [
    id 741
    label "tar&#322;o"
  ]
  node [
    id 742
    label "wyrostek_filtracyjny"
  ]
  node [
    id 743
    label "m&#281;tnooki"
  ]
  node [
    id 744
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 745
    label "pokrywa_skrzelowa"
  ]
  node [
    id 746
    label "ikra"
  ]
  node [
    id 747
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 748
    label "szczelina_skrzelowa"
  ]
  node [
    id 749
    label "blat"
  ]
  node [
    id 750
    label "interfejs"
  ]
  node [
    id 751
    label "okno"
  ]
  node [
    id 752
    label "ikona"
  ]
  node [
    id 753
    label "system_operacyjny"
  ]
  node [
    id 754
    label "mebel"
  ]
  node [
    id 755
    label "zdolno&#347;&#263;"
  ]
  node [
    id 756
    label "relaxation"
  ]
  node [
    id 757
    label "os&#322;abienie"
  ]
  node [
    id 758
    label "oswobodzenie"
  ]
  node [
    id 759
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 760
    label "zdezorganizowanie"
  ]
  node [
    id 761
    label "reakcja"
  ]
  node [
    id 762
    label "podporz&#261;dkowanie_si&#281;"
  ]
  node [
    id 763
    label "tajemnica"
  ]
  node [
    id 764
    label "wydarzenie"
  ]
  node [
    id 765
    label "pochowanie"
  ]
  node [
    id 766
    label "zdyscyplinowanie"
  ]
  node [
    id 767
    label "post&#261;pienie"
  ]
  node [
    id 768
    label "post"
  ]
  node [
    id 769
    label "bearing"
  ]
  node [
    id 770
    label "zwierz&#281;"
  ]
  node [
    id 771
    label "behawior"
  ]
  node [
    id 772
    label "observation"
  ]
  node [
    id 773
    label "dieta"
  ]
  node [
    id 774
    label "podtrzymanie"
  ]
  node [
    id 775
    label "etolog"
  ]
  node [
    id 776
    label "przechowanie"
  ]
  node [
    id 777
    label "oswobodzi&#263;"
  ]
  node [
    id 778
    label "os&#322;abi&#263;"
  ]
  node [
    id 779
    label "disengage"
  ]
  node [
    id 780
    label "zdezorganizowa&#263;"
  ]
  node [
    id 781
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 782
    label "naukowiec"
  ]
  node [
    id 783
    label "provider"
  ]
  node [
    id 784
    label "b&#322;&#261;d"
  ]
  node [
    id 785
    label "hipertekst"
  ]
  node [
    id 786
    label "cyberprzestrze&#324;"
  ]
  node [
    id 787
    label "mem"
  ]
  node [
    id 788
    label "gra_sieciowa"
  ]
  node [
    id 789
    label "grooming"
  ]
  node [
    id 790
    label "media"
  ]
  node [
    id 791
    label "biznes_elektroniczny"
  ]
  node [
    id 792
    label "sie&#263;_komputerowa"
  ]
  node [
    id 793
    label "punkt_dost&#281;pu"
  ]
  node [
    id 794
    label "us&#322;uga_internetowa"
  ]
  node [
    id 795
    label "netbook"
  ]
  node [
    id 796
    label "e-hazard"
  ]
  node [
    id 797
    label "podcast"
  ]
  node [
    id 798
    label "prezenter"
  ]
  node [
    id 799
    label "typ"
  ]
  node [
    id 800
    label "mildew"
  ]
  node [
    id 801
    label "zi&#243;&#322;ko"
  ]
  node [
    id 802
    label "motif"
  ]
  node [
    id 803
    label "pozowanie"
  ]
  node [
    id 804
    label "ideal"
  ]
  node [
    id 805
    label "wz&#243;r"
  ]
  node [
    id 806
    label "matryca"
  ]
  node [
    id 807
    label "adaptation"
  ]
  node [
    id 808
    label "pozowa&#263;"
  ]
  node [
    id 809
    label "imitacja"
  ]
  node [
    id 810
    label "orygina&#322;"
  ]
  node [
    id 811
    label "facet"
  ]
  node [
    id 812
    label "miniatura"
  ]
  node [
    id 813
    label "zesp&#243;&#322;"
  ]
  node [
    id 814
    label "podejrzany"
  ]
  node [
    id 815
    label "s&#261;downictwo"
  ]
  node [
    id 816
    label "biuro"
  ]
  node [
    id 817
    label "court"
  ]
  node [
    id 818
    label "bronienie"
  ]
  node [
    id 819
    label "urz&#261;d"
  ]
  node [
    id 820
    label "oskar&#380;yciel"
  ]
  node [
    id 821
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 822
    label "skazany"
  ]
  node [
    id 823
    label "post&#281;powanie"
  ]
  node [
    id 824
    label "broni&#263;"
  ]
  node [
    id 825
    label "my&#347;l"
  ]
  node [
    id 826
    label "pods&#261;dny"
  ]
  node [
    id 827
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 828
    label "obrona"
  ]
  node [
    id 829
    label "wypowied&#378;"
  ]
  node [
    id 830
    label "instytucja"
  ]
  node [
    id 831
    label "antylogizm"
  ]
  node [
    id 832
    label "konektyw"
  ]
  node [
    id 833
    label "&#347;wiadek"
  ]
  node [
    id 834
    label "procesowicz"
  ]
  node [
    id 835
    label "lias"
  ]
  node [
    id 836
    label "dzia&#322;"
  ]
  node [
    id 837
    label "jednostka"
  ]
  node [
    id 838
    label "pi&#281;tro"
  ]
  node [
    id 839
    label "klasa"
  ]
  node [
    id 840
    label "filia"
  ]
  node [
    id 841
    label "malm"
  ]
  node [
    id 842
    label "whole"
  ]
  node [
    id 843
    label "dogger"
  ]
  node [
    id 844
    label "promocja"
  ]
  node [
    id 845
    label "kurs"
  ]
  node [
    id 846
    label "bank"
  ]
  node [
    id 847
    label "formacja"
  ]
  node [
    id 848
    label "ajencja"
  ]
  node [
    id 849
    label "wojsko"
  ]
  node [
    id 850
    label "siedziba"
  ]
  node [
    id 851
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 852
    label "agencja"
  ]
  node [
    id 853
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 854
    label "szpital"
  ]
  node [
    id 855
    label "algebra_liniowa"
  ]
  node [
    id 856
    label "macierz_j&#261;drowa"
  ]
  node [
    id 857
    label "atom"
  ]
  node [
    id 858
    label "nukleon"
  ]
  node [
    id 859
    label "kariokineza"
  ]
  node [
    id 860
    label "core"
  ]
  node [
    id 861
    label "chemia_j&#261;drowa"
  ]
  node [
    id 862
    label "anorchizm"
  ]
  node [
    id 863
    label "os&#322;onka_pochwowa_j&#261;dra"
  ]
  node [
    id 864
    label "nasieniak"
  ]
  node [
    id 865
    label "wn&#281;trostwo"
  ]
  node [
    id 866
    label "ziarno"
  ]
  node [
    id 867
    label "kom&#243;rka_Sertolego"
  ]
  node [
    id 868
    label "j&#261;derko"
  ]
  node [
    id 869
    label "kom&#243;rka_Leydiga"
  ]
  node [
    id 870
    label "jajo"
  ]
  node [
    id 871
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 872
    label "chromosom"
  ]
  node [
    id 873
    label "organellum"
  ]
  node [
    id 874
    label "moszna"
  ]
  node [
    id 875
    label "przeciwobraz"
  ]
  node [
    id 876
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 877
    label "&#347;rodek"
  ]
  node [
    id 878
    label "protoplazma"
  ]
  node [
    id 879
    label "gruczo&#322;_rozrodczy"
  ]
  node [
    id 880
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 881
    label "nukleosynteza"
  ]
  node [
    id 882
    label "subsystem"
  ]
  node [
    id 883
    label "ko&#322;o"
  ]
  node [
    id 884
    label "granica"
  ]
  node [
    id 885
    label "p&#243;&#322;o&#347;"
  ]
  node [
    id 886
    label "suport"
  ]
  node [
    id 887
    label "prosta"
  ]
  node [
    id 888
    label "o&#347;rodek"
  ]
  node [
    id 889
    label "eonotem"
  ]
  node [
    id 890
    label "constellation"
  ]
  node [
    id 891
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 892
    label "Ptak_Rajski"
  ]
  node [
    id 893
    label "W&#281;&#380;ownik"
  ]
  node [
    id 894
    label "Panna"
  ]
  node [
    id 895
    label "W&#261;&#380;"
  ]
  node [
    id 896
    label "blokada"
  ]
  node [
    id 897
    label "hurtownia"
  ]
  node [
    id 898
    label "pomieszczenie"
  ]
  node [
    id 899
    label "pole"
  ]
  node [
    id 900
    label "pas"
  ]
  node [
    id 901
    label "basic"
  ]
  node [
    id 902
    label "sk&#322;adnik"
  ]
  node [
    id 903
    label "sklep"
  ]
  node [
    id 904
    label "obr&#243;bka"
  ]
  node [
    id 905
    label "constitution"
  ]
  node [
    id 906
    label "fabryka"
  ]
  node [
    id 907
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 908
    label "syf"
  ]
  node [
    id 909
    label "rank_and_file"
  ]
  node [
    id 910
    label "set"
  ]
  node [
    id 911
    label "tabulacja"
  ]
  node [
    id 912
    label "tekst"
  ]
  node [
    id 913
    label "cognition"
  ]
  node [
    id 914
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 915
    label "intelekt"
  ]
  node [
    id 916
    label "pozwolenie"
  ]
  node [
    id 917
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 918
    label "zaawansowanie"
  ]
  node [
    id 919
    label "wykszta&#322;cenie"
  ]
  node [
    id 920
    label "ekstraspekcja"
  ]
  node [
    id 921
    label "feeling"
  ]
  node [
    id 922
    label "zemdle&#263;"
  ]
  node [
    id 923
    label "Freud"
  ]
  node [
    id 924
    label "psychoanaliza"
  ]
  node [
    id 925
    label "conscience"
  ]
  node [
    id 926
    label "rozwini&#281;cie"
  ]
  node [
    id 927
    label "zapoznanie"
  ]
  node [
    id 928
    label "wys&#322;anie"
  ]
  node [
    id 929
    label "udoskonalenie"
  ]
  node [
    id 930
    label "pomo&#380;enie"
  ]
  node [
    id 931
    label "urszulanki"
  ]
  node [
    id 932
    label "training"
  ]
  node [
    id 933
    label "niepokalanki"
  ]
  node [
    id 934
    label "o&#347;wiecenie"
  ]
  node [
    id 935
    label "kwalifikacje"
  ]
  node [
    id 936
    label "sophistication"
  ]
  node [
    id 937
    label "skolaryzacja"
  ]
  node [
    id 938
    label "form"
  ]
  node [
    id 939
    label "noosfera"
  ]
  node [
    id 940
    label "stopie&#324;"
  ]
  node [
    id 941
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 942
    label "decyzja"
  ]
  node [
    id 943
    label "zwalnianie_si&#281;"
  ]
  node [
    id 944
    label "authorization"
  ]
  node [
    id 945
    label "koncesjonowanie"
  ]
  node [
    id 946
    label "zwolnienie_si&#281;"
  ]
  node [
    id 947
    label "pozwole&#324;stwo"
  ]
  node [
    id 948
    label "bycie_w_stanie"
  ]
  node [
    id 949
    label "odwieszenie"
  ]
  node [
    id 950
    label "odpowied&#378;"
  ]
  node [
    id 951
    label "pofolgowanie"
  ]
  node [
    id 952
    label "license"
  ]
  node [
    id 953
    label "franchise"
  ]
  node [
    id 954
    label "umo&#380;liwienie"
  ]
  node [
    id 955
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 956
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 957
    label "dokument"
  ]
  node [
    id 958
    label "uznanie"
  ]
  node [
    id 959
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 960
    label "informowa&#263;"
  ]
  node [
    id 961
    label "explain"
  ]
  node [
    id 962
    label "powiada&#263;"
  ]
  node [
    id 963
    label "komunikowa&#263;"
  ]
  node [
    id 964
    label "inform"
  ]
  node [
    id 965
    label "poja&#347;nia&#263;"
  ]
  node [
    id 966
    label "u&#322;atwia&#263;"
  ]
  node [
    id 967
    label "elaborate"
  ]
  node [
    id 968
    label "give"
  ]
  node [
    id 969
    label "suplikowa&#263;"
  ]
  node [
    id 970
    label "przek&#322;ada&#263;"
  ]
  node [
    id 971
    label "przekonywa&#263;"
  ]
  node [
    id 972
    label "interpretowa&#263;"
  ]
  node [
    id 973
    label "j&#281;zyk"
  ]
  node [
    id 974
    label "przedstawia&#263;"
  ]
  node [
    id 975
    label "sprawowa&#263;"
  ]
  node [
    id 976
    label "uzasadnia&#263;"
  ]
  node [
    id 977
    label "cywilizacja"
  ]
  node [
    id 978
    label "elita"
  ]
  node [
    id 979
    label "status"
  ]
  node [
    id 980
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 981
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 982
    label "aspo&#322;eczny"
  ]
  node [
    id 983
    label "ludzie_pracy"
  ]
  node [
    id 984
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 985
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 986
    label "pozaklasowy"
  ]
  node [
    id 987
    label "Fremeni"
  ]
  node [
    id 988
    label "uwarstwienie"
  ]
  node [
    id 989
    label "community"
  ]
  node [
    id 990
    label "kastowo&#347;&#263;"
  ]
  node [
    id 991
    label "facylitacja"
  ]
  node [
    id 992
    label "nieograniczony"
  ]
  node [
    id 993
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 994
    label "satysfakcja"
  ]
  node [
    id 995
    label "bezwzgl&#281;dny"
  ]
  node [
    id 996
    label "ca&#322;y"
  ]
  node [
    id 997
    label "otwarty"
  ]
  node [
    id 998
    label "wype&#322;nienie"
  ]
  node [
    id 999
    label "pe&#322;no"
  ]
  node [
    id 1000
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 1001
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 1002
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 1003
    label "r&#243;wny"
  ]
  node [
    id 1004
    label "toni&#281;cie"
  ]
  node [
    id 1005
    label "zatoni&#281;cie"
  ]
  node [
    id 1006
    label "part"
  ]
  node [
    id 1007
    label "niskogatunkowy"
  ]
  node [
    id 1008
    label "condition"
  ]
  node [
    id 1009
    label "awansowa&#263;"
  ]
  node [
    id 1010
    label "awans"
  ]
  node [
    id 1011
    label "podmiotowo"
  ]
  node [
    id 1012
    label "awansowanie"
  ]
  node [
    id 1013
    label "sytuacja"
  ]
  node [
    id 1014
    label "niekorzystny"
  ]
  node [
    id 1015
    label "aspo&#322;ecznie"
  ]
  node [
    id 1016
    label "typowy"
  ]
  node [
    id 1017
    label "niech&#281;tny"
  ]
  node [
    id 1018
    label "civilization"
  ]
  node [
    id 1019
    label "faza"
  ]
  node [
    id 1020
    label "technika"
  ]
  node [
    id 1021
    label "rozw&#243;j"
  ]
  node [
    id 1022
    label "cywilizowanie"
  ]
  node [
    id 1023
    label "stratification"
  ]
  node [
    id 1024
    label "lamination"
  ]
  node [
    id 1025
    label "podzia&#322;"
  ]
  node [
    id 1026
    label "elite"
  ]
  node [
    id 1027
    label "&#347;rodowisko"
  ]
  node [
    id 1028
    label "wagon"
  ]
  node [
    id 1029
    label "mecz_mistrzowski"
  ]
  node [
    id 1030
    label "arrangement"
  ]
  node [
    id 1031
    label "class"
  ]
  node [
    id 1032
    label "&#322;awka"
  ]
  node [
    id 1033
    label "wykrzyknik"
  ]
  node [
    id 1034
    label "zaleta"
  ]
  node [
    id 1035
    label "jednostka_systematyczna"
  ]
  node [
    id 1036
    label "programowanie_obiektowe"
  ]
  node [
    id 1037
    label "tablica"
  ]
  node [
    id 1038
    label "rezerwa"
  ]
  node [
    id 1039
    label "gromada"
  ]
  node [
    id 1040
    label "Ekwici"
  ]
  node [
    id 1041
    label "szko&#322;a"
  ]
  node [
    id 1042
    label "organizacja"
  ]
  node [
    id 1043
    label "sala"
  ]
  node [
    id 1044
    label "pomoc"
  ]
  node [
    id 1045
    label "grupa"
  ]
  node [
    id 1046
    label "przepisa&#263;"
  ]
  node [
    id 1047
    label "znak_jako&#347;ci"
  ]
  node [
    id 1048
    label "type"
  ]
  node [
    id 1049
    label "przepisanie"
  ]
  node [
    id 1050
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 1051
    label "dziennik_lekcyjny"
  ]
  node [
    id 1052
    label "fakcja"
  ]
  node [
    id 1053
    label "atak"
  ]
  node [
    id 1054
    label "botanika"
  ]
  node [
    id 1055
    label "uprawienie"
  ]
  node [
    id 1056
    label "u&#322;o&#380;enie"
  ]
  node [
    id 1057
    label "p&#322;osa"
  ]
  node [
    id 1058
    label "t&#322;o"
  ]
  node [
    id 1059
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 1060
    label "gospodarstwo"
  ]
  node [
    id 1061
    label "uprawi&#263;"
  ]
  node [
    id 1062
    label "room"
  ]
  node [
    id 1063
    label "dw&#243;r"
  ]
  node [
    id 1064
    label "irygowanie"
  ]
  node [
    id 1065
    label "square"
  ]
  node [
    id 1066
    label "zmienna"
  ]
  node [
    id 1067
    label "irygowa&#263;"
  ]
  node [
    id 1068
    label "socjologia"
  ]
  node [
    id 1069
    label "boisko"
  ]
  node [
    id 1070
    label "dziedzina"
  ]
  node [
    id 1071
    label "baza_danych"
  ]
  node [
    id 1072
    label "region"
  ]
  node [
    id 1073
    label "zagon"
  ]
  node [
    id 1074
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 1075
    label "radlina"
  ]
  node [
    id 1076
    label "cz&#281;sty"
  ]
  node [
    id 1077
    label "licznie"
  ]
  node [
    id 1078
    label "rojenie_si&#281;"
  ]
  node [
    id 1079
    label "cz&#281;sto"
  ]
  node [
    id 1080
    label "demofobia"
  ]
  node [
    id 1081
    label "najazd"
  ]
  node [
    id 1082
    label "odm&#322;adzanie"
  ]
  node [
    id 1083
    label "liga"
  ]
  node [
    id 1084
    label "asymilowanie"
  ]
  node [
    id 1085
    label "asymilowa&#263;"
  ]
  node [
    id 1086
    label "Entuzjastki"
  ]
  node [
    id 1087
    label "Terranie"
  ]
  node [
    id 1088
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1089
    label "category"
  ]
  node [
    id 1090
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1091
    label "cz&#261;steczka"
  ]
  node [
    id 1092
    label "stage_set"
  ]
  node [
    id 1093
    label "specgrupa"
  ]
  node [
    id 1094
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1095
    label "&#346;wietliki"
  ]
  node [
    id 1096
    label "Eurogrupa"
  ]
  node [
    id 1097
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1098
    label "formacja_geologiczna"
  ]
  node [
    id 1099
    label "harcerze_starsi"
  ]
  node [
    id 1100
    label "inpouring"
  ]
  node [
    id 1101
    label "przybycie"
  ]
  node [
    id 1102
    label "t&#322;um"
  ]
  node [
    id 1103
    label "rapt"
  ]
  node [
    id 1104
    label "narciarstwo"
  ]
  node [
    id 1105
    label "skocznia"
  ]
  node [
    id 1106
    label "nieoczekiwany"
  ]
  node [
    id 1107
    label "potop_szwedzki"
  ]
  node [
    id 1108
    label "napad"
  ]
  node [
    id 1109
    label "fobia"
  ]
  node [
    id 1110
    label "nieprofesjonalista"
  ]
  node [
    id 1111
    label "klient"
  ]
  node [
    id 1112
    label "sportowiec"
  ]
  node [
    id 1113
    label "ch&#281;tny"
  ]
  node [
    id 1114
    label "sympatyk"
  ]
  node [
    id 1115
    label "rekreacja"
  ]
  node [
    id 1116
    label "entuzjasta"
  ]
  node [
    id 1117
    label "agent_rozliczeniowy"
  ]
  node [
    id 1118
    label "komputer_cyfrowy"
  ]
  node [
    id 1119
    label "us&#322;ugobiorca"
  ]
  node [
    id 1120
    label "Rzymianin"
  ]
  node [
    id 1121
    label "szlachcic"
  ]
  node [
    id 1122
    label "obywatel"
  ]
  node [
    id 1123
    label "klientela"
  ]
  node [
    id 1124
    label "bratek"
  ]
  node [
    id 1125
    label "ch&#281;tliwy"
  ]
  node [
    id 1126
    label "ch&#281;tnie"
  ]
  node [
    id 1127
    label "napalony"
  ]
  node [
    id 1128
    label "chy&#380;y"
  ]
  node [
    id 1129
    label "&#380;yczliwy"
  ]
  node [
    id 1130
    label "przychylny"
  ]
  node [
    id 1131
    label "gotowy"
  ]
  node [
    id 1132
    label "ludzko&#347;&#263;"
  ]
  node [
    id 1133
    label "wapniak"
  ]
  node [
    id 1134
    label "os&#322;abia&#263;"
  ]
  node [
    id 1135
    label "posta&#263;"
  ]
  node [
    id 1136
    label "hominid"
  ]
  node [
    id 1137
    label "podw&#322;adny"
  ]
  node [
    id 1138
    label "os&#322;abianie"
  ]
  node [
    id 1139
    label "g&#322;owa"
  ]
  node [
    id 1140
    label "figura"
  ]
  node [
    id 1141
    label "portrecista"
  ]
  node [
    id 1142
    label "dwun&#243;g"
  ]
  node [
    id 1143
    label "profanum"
  ]
  node [
    id 1144
    label "nasada"
  ]
  node [
    id 1145
    label "duch"
  ]
  node [
    id 1146
    label "antropochoria"
  ]
  node [
    id 1147
    label "osoba"
  ]
  node [
    id 1148
    label "senior"
  ]
  node [
    id 1149
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1150
    label "Adam"
  ]
  node [
    id 1151
    label "homo_sapiens"
  ]
  node [
    id 1152
    label "polifag"
  ]
  node [
    id 1153
    label "zapaleniec"
  ]
  node [
    id 1154
    label "zwolennik"
  ]
  node [
    id 1155
    label "zgrupowanie"
  ]
  node [
    id 1156
    label "bezsensowy"
  ]
  node [
    id 1157
    label "ja&#322;owy"
  ]
  node [
    id 1158
    label "nielogiczny"
  ]
  node [
    id 1159
    label "nieskuteczny"
  ]
  node [
    id 1160
    label "nieuzasadniony"
  ]
  node [
    id 1161
    label "bezsensownie"
  ]
  node [
    id 1162
    label "wyja&#322;owienie"
  ]
  node [
    id 1163
    label "czczy"
  ]
  node [
    id 1164
    label "p&#322;onny"
  ]
  node [
    id 1165
    label "ja&#322;owo"
  ]
  node [
    id 1166
    label "czysty"
  ]
  node [
    id 1167
    label "wyja&#322;awianie"
  ]
  node [
    id 1168
    label "ubogi"
  ]
  node [
    id 1169
    label "nieurodzajnie"
  ]
  node [
    id 1170
    label "nijaki"
  ]
  node [
    id 1171
    label "&#347;redni"
  ]
  node [
    id 1172
    label "bezskutecznie"
  ]
  node [
    id 1173
    label "z&#322;y"
  ]
  node [
    id 1174
    label "niewyja&#347;niony"
  ]
  node [
    id 1175
    label "niezrozumiale"
  ]
  node [
    id 1176
    label "oddalony"
  ]
  node [
    id 1177
    label "nieprzyst&#281;pny"
  ]
  node [
    id 1178
    label "komplikowanie_si&#281;"
  ]
  node [
    id 1179
    label "dziwny"
  ]
  node [
    id 1180
    label "powik&#322;anie"
  ]
  node [
    id 1181
    label "komplikowanie"
  ]
  node [
    id 1182
    label "bezpodstawny"
  ]
  node [
    id 1183
    label "nielogicznie"
  ]
  node [
    id 1184
    label "nieracjonalny"
  ]
  node [
    id 1185
    label "sygnifikator"
  ]
  node [
    id 1186
    label "horoscope"
  ]
  node [
    id 1187
    label "znak_zodiaku"
  ]
  node [
    id 1188
    label "prediction"
  ]
  node [
    id 1189
    label "intuicja"
  ]
  node [
    id 1190
    label "przewidywanie"
  ]
  node [
    id 1191
    label "karta_tarota"
  ]
  node [
    id 1192
    label "symbol"
  ]
  node [
    id 1193
    label "energia"
  ]
  node [
    id 1194
    label "wedyzm"
  ]
  node [
    id 1195
    label "buddyzm"
  ]
  node [
    id 1196
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1197
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 1198
    label "emitowa&#263;"
  ]
  node [
    id 1199
    label "egzergia"
  ]
  node [
    id 1200
    label "kwant_energii"
  ]
  node [
    id 1201
    label "szwung"
  ]
  node [
    id 1202
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1203
    label "power"
  ]
  node [
    id 1204
    label "emitowanie"
  ]
  node [
    id 1205
    label "energy"
  ]
  node [
    id 1206
    label "kalpa"
  ]
  node [
    id 1207
    label "lampka_ma&#347;lana"
  ]
  node [
    id 1208
    label "Buddhism"
  ]
  node [
    id 1209
    label "dana"
  ]
  node [
    id 1210
    label "mahajana"
  ]
  node [
    id 1211
    label "asura"
  ]
  node [
    id 1212
    label "wad&#378;rajana"
  ]
  node [
    id 1213
    label "bonzo"
  ]
  node [
    id 1214
    label "therawada"
  ]
  node [
    id 1215
    label "tantryzm"
  ]
  node [
    id 1216
    label "hinajana"
  ]
  node [
    id 1217
    label "bardo"
  ]
  node [
    id 1218
    label "arahant"
  ]
  node [
    id 1219
    label "ahinsa"
  ]
  node [
    id 1220
    label "li"
  ]
  node [
    id 1221
    label "hinduizm"
  ]
  node [
    id 1222
    label "ciura"
  ]
  node [
    id 1223
    label "miernota"
  ]
  node [
    id 1224
    label "g&#243;wno"
  ]
  node [
    id 1225
    label "love"
  ]
  node [
    id 1226
    label "brak"
  ]
  node [
    id 1227
    label "nieistnienie"
  ]
  node [
    id 1228
    label "odej&#347;cie"
  ]
  node [
    id 1229
    label "defect"
  ]
  node [
    id 1230
    label "gap"
  ]
  node [
    id 1231
    label "odej&#347;&#263;"
  ]
  node [
    id 1232
    label "kr&#243;tki"
  ]
  node [
    id 1233
    label "wada"
  ]
  node [
    id 1234
    label "odchodzi&#263;"
  ]
  node [
    id 1235
    label "wyr&#243;b"
  ]
  node [
    id 1236
    label "odchodzenie"
  ]
  node [
    id 1237
    label "prywatywny"
  ]
  node [
    id 1238
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 1239
    label "tandetno&#347;&#263;"
  ]
  node [
    id 1240
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 1241
    label "ka&#322;"
  ]
  node [
    id 1242
    label "tandeta"
  ]
  node [
    id 1243
    label "drobiazg"
  ]
  node [
    id 1244
    label "chor&#261;&#380;y"
  ]
  node [
    id 1245
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 1246
    label "spolny"
  ]
  node [
    id 1247
    label "wsp&#243;lnie"
  ]
  node [
    id 1248
    label "sp&#243;lny"
  ]
  node [
    id 1249
    label "jeden"
  ]
  node [
    id 1250
    label "uwsp&#243;lnienie"
  ]
  node [
    id 1251
    label "uwsp&#243;lnianie"
  ]
  node [
    id 1252
    label "sp&#243;lnie"
  ]
  node [
    id 1253
    label "dostosowanie"
  ]
  node [
    id 1254
    label "udost&#281;pnienie"
  ]
  node [
    id 1255
    label "udost&#281;pnianie"
  ]
  node [
    id 1256
    label "dostosowywanie"
  ]
  node [
    id 1257
    label "shot"
  ]
  node [
    id 1258
    label "jednakowy"
  ]
  node [
    id 1259
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 1260
    label "ujednolicenie"
  ]
  node [
    id 1261
    label "jaki&#347;"
  ]
  node [
    id 1262
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 1263
    label "jednolicie"
  ]
  node [
    id 1264
    label "kieliszek"
  ]
  node [
    id 1265
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 1266
    label "w&#243;dka"
  ]
  node [
    id 1267
    label "ten"
  ]
  node [
    id 1268
    label "mistreat"
  ]
  node [
    id 1269
    label "obra&#380;a&#263;"
  ]
  node [
    id 1270
    label "tease"
  ]
  node [
    id 1271
    label "przynosi&#263;"
  ]
  node [
    id 1272
    label "kpi&#263;"
  ]
  node [
    id 1273
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 1274
    label "narusza&#263;"
  ]
  node [
    id 1275
    label "call"
  ]
  node [
    id 1276
    label "enlarge"
  ]
  node [
    id 1277
    label "dawa&#263;"
  ]
  node [
    id 1278
    label "zanosi&#263;"
  ]
  node [
    id 1279
    label "podawa&#263;"
  ]
  node [
    id 1280
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 1281
    label "dodawa&#263;"
  ]
  node [
    id 1282
    label "nie&#347;&#263;"
  ]
  node [
    id 1283
    label "begin"
  ]
  node [
    id 1284
    label "ur&#261;ga&#263;_si&#281;"
  ]
  node [
    id 1285
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 1286
    label "lekcewa&#380;y&#263;"
  ]
  node [
    id 1287
    label "laugh"
  ]
  node [
    id 1288
    label "jeer"
  ]
  node [
    id 1289
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 1290
    label "nalewa&#263;_si&#281;"
  ]
  node [
    id 1291
    label "obgadywa&#263;"
  ]
  node [
    id 1292
    label "&#347;mia&#263;_si&#281;"
  ]
  node [
    id 1293
    label "wystawia&#263;"
  ]
  node [
    id 1294
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 1295
    label "wielki"
  ]
  node [
    id 1296
    label "rozpowszechnianie"
  ]
  node [
    id 1297
    label "wyj&#261;tkowy"
  ]
  node [
    id 1298
    label "nieprzeci&#281;tny"
  ]
  node [
    id 1299
    label "wysoce"
  ]
  node [
    id 1300
    label "wybitny"
  ]
  node [
    id 1301
    label "dupny"
  ]
  node [
    id 1302
    label "ujawnienie_si&#281;"
  ]
  node [
    id 1303
    label "powstanie"
  ]
  node [
    id 1304
    label "wydostanie_si&#281;"
  ]
  node [
    id 1305
    label "opuszczenie"
  ]
  node [
    id 1306
    label "ukazanie_si&#281;"
  ]
  node [
    id 1307
    label "emergence"
  ]
  node [
    id 1308
    label "wyr&#243;&#380;nienie_si&#281;"
  ]
  node [
    id 1309
    label "zgini&#281;cie"
  ]
  node [
    id 1310
    label "dochodzenie"
  ]
  node [
    id 1311
    label "powodowanie"
  ]
  node [
    id 1312
    label "deployment"
  ]
  node [
    id 1313
    label "nuklearyzacja"
  ]
  node [
    id 1314
    label "rozpowszechnianie_si&#281;"
  ]
  node [
    id 1315
    label "p&#243;&#322;noc"
  ]
  node [
    id 1316
    label "Kosowo"
  ]
  node [
    id 1317
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 1318
    label "Zab&#322;ocie"
  ]
  node [
    id 1319
    label "zach&#243;d"
  ]
  node [
    id 1320
    label "po&#322;udnie"
  ]
  node [
    id 1321
    label "Pow&#261;zki"
  ]
  node [
    id 1322
    label "Piotrowo"
  ]
  node [
    id 1323
    label "Olszanica"
  ]
  node [
    id 1324
    label "holarktyka"
  ]
  node [
    id 1325
    label "Ruda_Pabianicka"
  ]
  node [
    id 1326
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 1327
    label "Ludwin&#243;w"
  ]
  node [
    id 1328
    label "Arktyka"
  ]
  node [
    id 1329
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 1330
    label "Zabu&#380;e"
  ]
  node [
    id 1331
    label "antroposfera"
  ]
  node [
    id 1332
    label "terytorium"
  ]
  node [
    id 1333
    label "Neogea"
  ]
  node [
    id 1334
    label "Syberia_Zachodnia"
  ]
  node [
    id 1335
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1336
    label "pas_planetoid"
  ]
  node [
    id 1337
    label "Syberia_Wschodnia"
  ]
  node [
    id 1338
    label "Antarktyka"
  ]
  node [
    id 1339
    label "Rakowice"
  ]
  node [
    id 1340
    label "akrecja"
  ]
  node [
    id 1341
    label "&#321;&#281;g"
  ]
  node [
    id 1342
    label "Kresy_Zachodnie"
  ]
  node [
    id 1343
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1344
    label "wsch&#243;d"
  ]
  node [
    id 1345
    label "Notogea"
  ]
  node [
    id 1346
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1347
    label "Kanada"
  ]
  node [
    id 1348
    label "Jukon"
  ]
  node [
    id 1349
    label "warunek_lokalowy"
  ]
  node [
    id 1350
    label "plac"
  ]
  node [
    id 1351
    label "location"
  ]
  node [
    id 1352
    label "uwaga"
  ]
  node [
    id 1353
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1354
    label "chwila"
  ]
  node [
    id 1355
    label "cia&#322;o"
  ]
  node [
    id 1356
    label "praca"
  ]
  node [
    id 1357
    label "rz&#261;d"
  ]
  node [
    id 1358
    label "parametr"
  ]
  node [
    id 1359
    label "wielko&#347;&#263;"
  ]
  node [
    id 1360
    label "dymensja"
  ]
  node [
    id 1361
    label "wiecz&#243;r"
  ]
  node [
    id 1362
    label "sunset"
  ]
  node [
    id 1363
    label "szar&#243;wka"
  ]
  node [
    id 1364
    label "usi&#322;owanie"
  ]
  node [
    id 1365
    label "strona_&#347;wiata"
  ]
  node [
    id 1366
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1367
    label "pora"
  ]
  node [
    id 1368
    label "trud"
  ]
  node [
    id 1369
    label "s&#322;o&#324;ce"
  ]
  node [
    id 1370
    label "dzie&#324;"
  ]
  node [
    id 1371
    label "dwunasta"
  ]
  node [
    id 1372
    label "godzina"
  ]
  node [
    id 1373
    label "brzask"
  ]
  node [
    id 1374
    label "pocz&#261;tek"
  ]
  node [
    id 1375
    label "szabas"
  ]
  node [
    id 1376
    label "rano"
  ]
  node [
    id 1377
    label "Boreasz"
  ]
  node [
    id 1378
    label "noc"
  ]
  node [
    id 1379
    label "&#347;wiat"
  ]
  node [
    id 1380
    label "p&#243;&#322;nocek"
  ]
  node [
    id 1381
    label "Podg&#243;rze"
  ]
  node [
    id 1382
    label "palearktyka"
  ]
  node [
    id 1383
    label "nearktyka"
  ]
  node [
    id 1384
    label "Serbia"
  ]
  node [
    id 1385
    label "euro"
  ]
  node [
    id 1386
    label "Bie&#380;an&#243;w-Prokocim"
  ]
  node [
    id 1387
    label "Warszawa"
  ]
  node [
    id 1388
    label "Kaw&#281;czyn"
  ]
  node [
    id 1389
    label "Kresy"
  ]
  node [
    id 1390
    label "biosfera"
  ]
  node [
    id 1391
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 1392
    label "D&#281;bniki"
  ]
  node [
    id 1393
    label "lodowiec_kontynentalny"
  ]
  node [
    id 1394
    label "Antarktyda"
  ]
  node [
    id 1395
    label "Pr&#261;dnik_Czerwony"
  ]
  node [
    id 1396
    label "Czy&#380;yny"
  ]
  node [
    id 1397
    label "Zwierzyniec"
  ]
  node [
    id 1398
    label "Rataje"
  ]
  node [
    id 1399
    label "G&#322;uszyna"
  ]
  node [
    id 1400
    label "rozrost"
  ]
  node [
    id 1401
    label "wzrost"
  ]
  node [
    id 1402
    label "dysk_akrecyjny"
  ]
  node [
    id 1403
    label "proces_biologiczny"
  ]
  node [
    id 1404
    label "accretion"
  ]
  node [
    id 1405
    label "sfera"
  ]
  node [
    id 1406
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 1407
    label "podzakres"
  ]
  node [
    id 1408
    label "desygnat"
  ]
  node [
    id 1409
    label "circle"
  ]
  node [
    id 1410
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 1411
    label "systematize"
  ]
  node [
    id 1412
    label "zadba&#263;"
  ]
  node [
    id 1413
    label "ustawi&#263;"
  ]
  node [
    id 1414
    label "zorganizowa&#263;"
  ]
  node [
    id 1415
    label "zebra&#263;"
  ]
  node [
    id 1416
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1417
    label "posprz&#261;ta&#263;"
  ]
  node [
    id 1418
    label "order"
  ]
  node [
    id 1419
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 1420
    label "read"
  ]
  node [
    id 1421
    label "escalate"
  ]
  node [
    id 1422
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 1423
    label "zmienia&#263;"
  ]
  node [
    id 1424
    label "increase"
  ]
  node [
    id 1425
    label "&#380;ywny"
  ]
  node [
    id 1426
    label "szczery"
  ]
  node [
    id 1427
    label "naprawd&#281;"
  ]
  node [
    id 1428
    label "realnie"
  ]
  node [
    id 1429
    label "podobny"
  ]
  node [
    id 1430
    label "zgodny"
  ]
  node [
    id 1431
    label "m&#261;dry"
  ]
  node [
    id 1432
    label "prawdziwie"
  ]
  node [
    id 1433
    label "znacznie"
  ]
  node [
    id 1434
    label "zauwa&#380;alny"
  ]
  node [
    id 1435
    label "wynios&#322;y"
  ]
  node [
    id 1436
    label "dono&#347;ny"
  ]
  node [
    id 1437
    label "silny"
  ]
  node [
    id 1438
    label "wa&#380;nie"
  ]
  node [
    id 1439
    label "istotnie"
  ]
  node [
    id 1440
    label "eksponowany"
  ]
  node [
    id 1441
    label "ukszta&#322;towany"
  ]
  node [
    id 1442
    label "do&#347;cig&#322;y"
  ]
  node [
    id 1443
    label "&#378;ra&#322;y"
  ]
  node [
    id 1444
    label "zdr&#243;w"
  ]
  node [
    id 1445
    label "dorodnie"
  ]
  node [
    id 1446
    label "okaza&#322;y"
  ]
  node [
    id 1447
    label "mocno"
  ]
  node [
    id 1448
    label "wiela"
  ]
  node [
    id 1449
    label "bardzo"
  ]
  node [
    id 1450
    label "wydoro&#347;lenie"
  ]
  node [
    id 1451
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1452
    label "doro&#347;lenie"
  ]
  node [
    id 1453
    label "doro&#347;le"
  ]
  node [
    id 1454
    label "dojrzale"
  ]
  node [
    id 1455
    label "dojrza&#322;y"
  ]
  node [
    id 1456
    label "doletni"
  ]
  node [
    id 1457
    label "pami&#281;&#263;"
  ]
  node [
    id 1458
    label "pomieszanie_si&#281;"
  ]
  node [
    id 1459
    label "wyobra&#378;nia"
  ]
  node [
    id 1460
    label "hipokamp"
  ]
  node [
    id 1461
    label "sprz&#281;t_komputerowy"
  ]
  node [
    id 1462
    label "memory"
  ]
  node [
    id 1463
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1464
    label "komputer"
  ]
  node [
    id 1465
    label "pami&#281;tno&#347;&#263;"
  ]
  node [
    id 1466
    label "zachowa&#263;"
  ]
  node [
    id 1467
    label "wymazanie"
  ]
  node [
    id 1468
    label "imagineskopia"
  ]
  node [
    id 1469
    label "fondness"
  ]
  node [
    id 1470
    label "kraina"
  ]
  node [
    id 1471
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1472
    label "esteta"
  ]
  node [
    id 1473
    label "umeblowanie"
  ]
  node [
    id 1474
    label "psychologia"
  ]
  node [
    id 1475
    label "przyzwoity"
  ]
  node [
    id 1476
    label "ciekawy"
  ]
  node [
    id 1477
    label "jako&#347;"
  ]
  node [
    id 1478
    label "jako_tako"
  ]
  node [
    id 1479
    label "niez&#322;y"
  ]
  node [
    id 1480
    label "charakterystyczny"
  ]
  node [
    id 1481
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 1482
    label "krzew"
  ]
  node [
    id 1483
    label "delfinidyna"
  ]
  node [
    id 1484
    label "pi&#380;maczkowate"
  ]
  node [
    id 1485
    label "ki&#347;&#263;"
  ]
  node [
    id 1486
    label "hy&#263;ka"
  ]
  node [
    id 1487
    label "pestkowiec"
  ]
  node [
    id 1488
    label "kwiat"
  ]
  node [
    id 1489
    label "owoc"
  ]
  node [
    id 1490
    label "oliwkowate"
  ]
  node [
    id 1491
    label "lilac"
  ]
  node [
    id 1492
    label "kostka"
  ]
  node [
    id 1493
    label "kita"
  ]
  node [
    id 1494
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 1495
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 1496
    label "d&#322;o&#324;"
  ]
  node [
    id 1497
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 1498
    label "powerball"
  ]
  node [
    id 1499
    label "&#380;ubr"
  ]
  node [
    id 1500
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 1501
    label "p&#281;k"
  ]
  node [
    id 1502
    label "r&#281;ka"
  ]
  node [
    id 1503
    label "ogon"
  ]
  node [
    id 1504
    label "zako&#324;czenie"
  ]
  node [
    id 1505
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 1506
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 1507
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 1508
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 1509
    label "flakon"
  ]
  node [
    id 1510
    label "przykoronek"
  ]
  node [
    id 1511
    label "kielich"
  ]
  node [
    id 1512
    label "dno_kwiatowe"
  ]
  node [
    id 1513
    label "organ_ro&#347;linny"
  ]
  node [
    id 1514
    label "warga"
  ]
  node [
    id 1515
    label "korona"
  ]
  node [
    id 1516
    label "rurka"
  ]
  node [
    id 1517
    label "ozdoba"
  ]
  node [
    id 1518
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 1519
    label "karczowa&#263;"
  ]
  node [
    id 1520
    label "wykarczowanie"
  ]
  node [
    id 1521
    label "skupina"
  ]
  node [
    id 1522
    label "wykarczowa&#263;"
  ]
  node [
    id 1523
    label "karczowanie"
  ]
  node [
    id 1524
    label "fanerofit"
  ]
  node [
    id 1525
    label "zbiorowisko"
  ]
  node [
    id 1526
    label "ro&#347;liny"
  ]
  node [
    id 1527
    label "p&#281;d"
  ]
  node [
    id 1528
    label "wegetowanie"
  ]
  node [
    id 1529
    label "zadziorek"
  ]
  node [
    id 1530
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1531
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1532
    label "do&#322;owa&#263;"
  ]
  node [
    id 1533
    label "wegetacja"
  ]
  node [
    id 1534
    label "strzyc"
  ]
  node [
    id 1535
    label "w&#322;&#243;kno"
  ]
  node [
    id 1536
    label "g&#322;uszenie"
  ]
  node [
    id 1537
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1538
    label "fitotron"
  ]
  node [
    id 1539
    label "bulwka"
  ]
  node [
    id 1540
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1541
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1542
    label "epiderma"
  ]
  node [
    id 1543
    label "gumoza"
  ]
  node [
    id 1544
    label "strzy&#380;enie"
  ]
  node [
    id 1545
    label "wypotnik"
  ]
  node [
    id 1546
    label "flawonoid"
  ]
  node [
    id 1547
    label "wyro&#347;le"
  ]
  node [
    id 1548
    label "do&#322;owanie"
  ]
  node [
    id 1549
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1550
    label "pora&#380;a&#263;"
  ]
  node [
    id 1551
    label "fitocenoza"
  ]
  node [
    id 1552
    label "fotoautotrof"
  ]
  node [
    id 1553
    label "nieuleczalnie_chory"
  ]
  node [
    id 1554
    label "wegetowa&#263;"
  ]
  node [
    id 1555
    label "pochewka"
  ]
  node [
    id 1556
    label "sok"
  ]
  node [
    id 1557
    label "zawi&#261;zek"
  ]
  node [
    id 1558
    label "pestka"
  ]
  node [
    id 1559
    label "mi&#261;&#380;sz"
  ]
  node [
    id 1560
    label "frukt"
  ]
  node [
    id 1561
    label "drylowanie"
  ]
  node [
    id 1562
    label "produkt"
  ]
  node [
    id 1563
    label "owocnia"
  ]
  node [
    id 1564
    label "fruktoza"
  ]
  node [
    id 1565
    label "gniazdo_nasienne"
  ]
  node [
    id 1566
    label "rezultat"
  ]
  node [
    id 1567
    label "glukoza"
  ]
  node [
    id 1568
    label "antocyjanidyn"
  ]
  node [
    id 1569
    label "szczeciowce"
  ]
  node [
    id 1570
    label "jasnotowce"
  ]
  node [
    id 1571
    label "Oleaceae"
  ]
  node [
    id 1572
    label "wielkopolski"
  ]
  node [
    id 1573
    label "bez_czarny"
  ]
  node [
    id 1574
    label "wyci&#261;g"
  ]
  node [
    id 1575
    label "passage"
  ]
  node [
    id 1576
    label "leksem"
  ]
  node [
    id 1577
    label "ortografia"
  ]
  node [
    id 1578
    label "cytat"
  ]
  node [
    id 1579
    label "ekscerptor"
  ]
  node [
    id 1580
    label "urywek"
  ]
  node [
    id 1581
    label "wordnet"
  ]
  node [
    id 1582
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 1583
    label "wypowiedzenie"
  ]
  node [
    id 1584
    label "morfem"
  ]
  node [
    id 1585
    label "s&#322;ownictwo"
  ]
  node [
    id 1586
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 1587
    label "pole_semantyczne"
  ]
  node [
    id 1588
    label "pisanie_si&#281;"
  ]
  node [
    id 1589
    label "nag&#322;os"
  ]
  node [
    id 1590
    label "wyg&#322;os"
  ]
  node [
    id 1591
    label "jednostka_leksykalna"
  ]
  node [
    id 1592
    label "fragment"
  ]
  node [
    id 1593
    label "warunki"
  ]
  node [
    id 1594
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1595
    label "motyw"
  ]
  node [
    id 1596
    label "realia"
  ]
  node [
    id 1597
    label "pismo"
  ]
  node [
    id 1598
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 1599
    label "fonetyzacja"
  ]
  node [
    id 1600
    label "alegacja"
  ]
  node [
    id 1601
    label "wyimek"
  ]
  node [
    id 1602
    label "konkordancja"
  ]
  node [
    id 1603
    label "skr&#243;t"
  ]
  node [
    id 1604
    label "skipass"
  ]
  node [
    id 1605
    label "okap"
  ]
  node [
    id 1606
    label "wentylator"
  ]
  node [
    id 1607
    label "mieszanina"
  ]
  node [
    id 1608
    label "bro&#324;_lufowa"
  ]
  node [
    id 1609
    label "urz&#261;dzenie_rekreacyjne"
  ]
  node [
    id 1610
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 1611
    label "extraction"
  ]
  node [
    id 1612
    label "d&#378;wig"
  ]
  node [
    id 1613
    label "aalen"
  ]
  node [
    id 1614
    label "jura_wczesna"
  ]
  node [
    id 1615
    label "holocen"
  ]
  node [
    id 1616
    label "pliocen"
  ]
  node [
    id 1617
    label "plejstocen"
  ]
  node [
    id 1618
    label "paleocen"
  ]
  node [
    id 1619
    label "dzieje"
  ]
  node [
    id 1620
    label "bajos"
  ]
  node [
    id 1621
    label "kelowej"
  ]
  node [
    id 1622
    label "eocen"
  ]
  node [
    id 1623
    label "okres"
  ]
  node [
    id 1624
    label "schy&#322;ek"
  ]
  node [
    id 1625
    label "miocen"
  ]
  node [
    id 1626
    label "&#347;rodkowy_trias"
  ]
  node [
    id 1627
    label "czas"
  ]
  node [
    id 1628
    label "term"
  ]
  node [
    id 1629
    label "Zeitgeist"
  ]
  node [
    id 1630
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 1631
    label "wczesny_trias"
  ]
  node [
    id 1632
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1633
    label "jura_&#347;rodkowa"
  ]
  node [
    id 1634
    label "oligocen"
  ]
  node [
    id 1635
    label "poprzedzanie"
  ]
  node [
    id 1636
    label "laba"
  ]
  node [
    id 1637
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1638
    label "chronometria"
  ]
  node [
    id 1639
    label "rachuba_czasu"
  ]
  node [
    id 1640
    label "przep&#322;ywanie"
  ]
  node [
    id 1641
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1642
    label "czasokres"
  ]
  node [
    id 1643
    label "odczyt"
  ]
  node [
    id 1644
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1645
    label "kategoria_gramatyczna"
  ]
  node [
    id 1646
    label "poprzedzenie"
  ]
  node [
    id 1647
    label "trawienie"
  ]
  node [
    id 1648
    label "pochodzi&#263;"
  ]
  node [
    id 1649
    label "period"
  ]
  node [
    id 1650
    label "okres_czasu"
  ]
  node [
    id 1651
    label "poprzedza&#263;"
  ]
  node [
    id 1652
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1653
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1654
    label "zegar"
  ]
  node [
    id 1655
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1656
    label "czwarty_wymiar"
  ]
  node [
    id 1657
    label "pochodzenie"
  ]
  node [
    id 1658
    label "koniugacja"
  ]
  node [
    id 1659
    label "trawi&#263;"
  ]
  node [
    id 1660
    label "pogoda"
  ]
  node [
    id 1661
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1662
    label "poprzedzi&#263;"
  ]
  node [
    id 1663
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1664
    label "time_period"
  ]
  node [
    id 1665
    label "gelas"
  ]
  node [
    id 1666
    label "kalabr"
  ]
  node [
    id 1667
    label "megaterium"
  ]
  node [
    id 1668
    label "czwartorz&#281;d"
  ]
  node [
    id 1669
    label "lutet"
  ]
  node [
    id 1670
    label "barton"
  ]
  node [
    id 1671
    label "iprez"
  ]
  node [
    id 1672
    label "paleogen"
  ]
  node [
    id 1673
    label "priabon"
  ]
  node [
    id 1674
    label "aluwium"
  ]
  node [
    id 1675
    label "tanet"
  ]
  node [
    id 1676
    label "dan"
  ]
  node [
    id 1677
    label "zeland"
  ]
  node [
    id 1678
    label "szat"
  ]
  node [
    id 1679
    label "rupel"
  ]
  node [
    id 1680
    label "messyn"
  ]
  node [
    id 1681
    label "serrawal"
  ]
  node [
    id 1682
    label "neogen"
  ]
  node [
    id 1683
    label "torton"
  ]
  node [
    id 1684
    label "akwitan"
  ]
  node [
    id 1685
    label "lang"
  ]
  node [
    id 1686
    label "burdyga&#322;"
  ]
  node [
    id 1687
    label "zankl"
  ]
  node [
    id 1688
    label "piacent"
  ]
  node [
    id 1689
    label "okres_amazo&#324;ski"
  ]
  node [
    id 1690
    label "stater"
  ]
  node [
    id 1691
    label "flow"
  ]
  node [
    id 1692
    label "choroba_przyrodzona"
  ]
  node [
    id 1693
    label "ordowik"
  ]
  node [
    id 1694
    label "postglacja&#322;"
  ]
  node [
    id 1695
    label "kreda"
  ]
  node [
    id 1696
    label "okres_hesperyjski"
  ]
  node [
    id 1697
    label "sylur"
  ]
  node [
    id 1698
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 1699
    label "okres_halsztacki"
  ]
  node [
    id 1700
    label "riak"
  ]
  node [
    id 1701
    label "podokres"
  ]
  node [
    id 1702
    label "trzeciorz&#281;d"
  ]
  node [
    id 1703
    label "kalim"
  ]
  node [
    id 1704
    label "fala"
  ]
  node [
    id 1705
    label "perm"
  ]
  node [
    id 1706
    label "retoryka"
  ]
  node [
    id 1707
    label "prekambr"
  ]
  node [
    id 1708
    label "pulsacja"
  ]
  node [
    id 1709
    label "proces_fizjologiczny"
  ]
  node [
    id 1710
    label "kambr"
  ]
  node [
    id 1711
    label "kriogen"
  ]
  node [
    id 1712
    label "ton"
  ]
  node [
    id 1713
    label "orosir"
  ]
  node [
    id 1714
    label "poprzednik"
  ]
  node [
    id 1715
    label "spell"
  ]
  node [
    id 1716
    label "sider"
  ]
  node [
    id 1717
    label "interstadia&#322;"
  ]
  node [
    id 1718
    label "ektas"
  ]
  node [
    id 1719
    label "rok_akademicki"
  ]
  node [
    id 1720
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 1721
    label "cykl"
  ]
  node [
    id 1722
    label "ciota"
  ]
  node [
    id 1723
    label "okres_noachijski"
  ]
  node [
    id 1724
    label "pierwszorz&#281;d"
  ]
  node [
    id 1725
    label "ediakar"
  ]
  node [
    id 1726
    label "zdanie"
  ]
  node [
    id 1727
    label "nast&#281;pnik"
  ]
  node [
    id 1728
    label "jura"
  ]
  node [
    id 1729
    label "glacja&#322;"
  ]
  node [
    id 1730
    label "sten"
  ]
  node [
    id 1731
    label "era"
  ]
  node [
    id 1732
    label "trias"
  ]
  node [
    id 1733
    label "p&#243;&#322;okres"
  ]
  node [
    id 1734
    label "rok_szkolny"
  ]
  node [
    id 1735
    label "dewon"
  ]
  node [
    id 1736
    label "karbon"
  ]
  node [
    id 1737
    label "izochronizm"
  ]
  node [
    id 1738
    label "preglacja&#322;"
  ]
  node [
    id 1739
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 1740
    label "drugorz&#281;d"
  ]
  node [
    id 1741
    label "semester"
  ]
  node [
    id 1742
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1743
    label "kres"
  ]
  node [
    id 1744
    label "dawny"
  ]
  node [
    id 1745
    label "wiekopomny"
  ]
  node [
    id 1746
    label "dziejowo"
  ]
  node [
    id 1747
    label "historycznie"
  ]
  node [
    id 1748
    label "zgodnie"
  ]
  node [
    id 1749
    label "zbie&#380;ny"
  ]
  node [
    id 1750
    label "spokojny"
  ]
  node [
    id 1751
    label "przestarza&#322;y"
  ]
  node [
    id 1752
    label "odleg&#322;y"
  ]
  node [
    id 1753
    label "przesz&#322;y"
  ]
  node [
    id 1754
    label "od_dawna"
  ]
  node [
    id 1755
    label "poprzedni"
  ]
  node [
    id 1756
    label "dawno"
  ]
  node [
    id 1757
    label "d&#322;ugoletni"
  ]
  node [
    id 1758
    label "anachroniczny"
  ]
  node [
    id 1759
    label "dawniej"
  ]
  node [
    id 1760
    label "niegdysiejszy"
  ]
  node [
    id 1761
    label "wcze&#347;niejszy"
  ]
  node [
    id 1762
    label "kombatant"
  ]
  node [
    id 1763
    label "stary"
  ]
  node [
    id 1764
    label "donios&#322;y"
  ]
  node [
    id 1765
    label "pierwotnie"
  ]
  node [
    id 1766
    label "pielenie"
  ]
  node [
    id 1767
    label "culture"
  ]
  node [
    id 1768
    label "sianie"
  ]
  node [
    id 1769
    label "stanowisko"
  ]
  node [
    id 1770
    label "sadzenie"
  ]
  node [
    id 1771
    label "oprysk"
  ]
  node [
    id 1772
    label "szczepienie"
  ]
  node [
    id 1773
    label "orka"
  ]
  node [
    id 1774
    label "rolnictwo"
  ]
  node [
    id 1775
    label "siew"
  ]
  node [
    id 1776
    label "exercise"
  ]
  node [
    id 1777
    label "koszenie"
  ]
  node [
    id 1778
    label "obrabianie"
  ]
  node [
    id 1779
    label "zajmowanie_si&#281;"
  ]
  node [
    id 1780
    label "use"
  ]
  node [
    id 1781
    label "biotechnika"
  ]
  node [
    id 1782
    label "hodowanie"
  ]
  node [
    id 1783
    label "activity"
  ]
  node [
    id 1784
    label "bezproblemowy"
  ]
  node [
    id 1785
    label "zapuszczanie"
  ]
  node [
    id 1786
    label "breeding"
  ]
  node [
    id 1787
    label "zootechnika"
  ]
  node [
    id 1788
    label "padanie"
  ]
  node [
    id 1789
    label "rozprzestrzenianie"
  ]
  node [
    id 1790
    label "przepuszczanie"
  ]
  node [
    id 1791
    label "strzelanie"
  ]
  node [
    id 1792
    label "spread"
  ]
  node [
    id 1793
    label "rozsiewanie"
  ]
  node [
    id 1794
    label "sypanie"
  ]
  node [
    id 1795
    label "gubienie"
  ]
  node [
    id 1796
    label "diffusion"
  ]
  node [
    id 1797
    label "rozsypywanie"
  ]
  node [
    id 1798
    label "uodpornianie"
  ]
  node [
    id 1799
    label "wariolacja"
  ]
  node [
    id 1800
    label "immunizacja"
  ]
  node [
    id 1801
    label "uszlachetnianie"
  ]
  node [
    id 1802
    label "inoculation"
  ]
  node [
    id 1803
    label "immunizacja_czynna"
  ]
  node [
    id 1804
    label "plewienie"
  ]
  node [
    id 1805
    label "weed"
  ]
  node [
    id 1806
    label "popielenie"
  ]
  node [
    id 1807
    label "usuwanie"
  ]
  node [
    id 1808
    label "strzemi&#261;czko"
  ]
  node [
    id 1809
    label "umieszczanie"
  ]
  node [
    id 1810
    label "plantation"
  ]
  node [
    id 1811
    label "zabijanie"
  ]
  node [
    id 1812
    label "oblewanie"
  ]
  node [
    id 1813
    label "egzaminator"
  ]
  node [
    id 1814
    label "perfusion"
  ]
  node [
    id 1815
    label "pozyskiwanie"
  ]
  node [
    id 1816
    label "podbieracz"
  ]
  node [
    id 1817
    label "egzaminowanie"
  ]
  node [
    id 1818
    label "killer_whale"
  ]
  node [
    id 1819
    label "ssak_morski"
  ]
  node [
    id 1820
    label "oraczka"
  ]
  node [
    id 1821
    label "delfinowate"
  ]
  node [
    id 1822
    label "drudgery"
  ]
  node [
    id 1823
    label "uprawka"
  ]
  node [
    id 1824
    label "mi&#281;so&#380;erca"
  ]
  node [
    id 1825
    label "nasiennictwo"
  ]
  node [
    id 1826
    label "agrotechnika"
  ]
  node [
    id 1827
    label "agroekologia"
  ]
  node [
    id 1828
    label "agrobiznes"
  ]
  node [
    id 1829
    label "intensyfikacja"
  ]
  node [
    id 1830
    label "gleboznawstwo"
  ]
  node [
    id 1831
    label "gospodarka"
  ]
  node [
    id 1832
    label "ogrodnictwo"
  ]
  node [
    id 1833
    label "agronomia"
  ]
  node [
    id 1834
    label "agrochemia"
  ]
  node [
    id 1835
    label "farmerstwo"
  ]
  node [
    id 1836
    label "zgarniacz"
  ]
  node [
    id 1837
    label "nauka"
  ]
  node [
    id 1838
    label "sadownictwo"
  ]
  node [
    id 1839
    label "&#322;&#261;karstwo"
  ]
  node [
    id 1840
    label "&#322;owiectwo"
  ]
  node [
    id 1841
    label "wyka&#324;czanie"
  ]
  node [
    id 1842
    label "conversion"
  ]
  node [
    id 1843
    label "krytykowanie"
  ]
  node [
    id 1844
    label "zabieranie"
  ]
  node [
    id 1845
    label "kszta&#322;towanie"
  ]
  node [
    id 1846
    label "deprecation"
  ]
  node [
    id 1847
    label "szczekanie"
  ]
  node [
    id 1848
    label "blanszownik"
  ]
  node [
    id 1849
    label "rabowanie"
  ]
  node [
    id 1850
    label "plotkowanie"
  ]
  node [
    id 1851
    label "napadanie"
  ]
  node [
    id 1852
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1853
    label "pogl&#261;d"
  ]
  node [
    id 1854
    label "stawia&#263;"
  ]
  node [
    id 1855
    label "wakowa&#263;"
  ]
  node [
    id 1856
    label "powierzanie"
  ]
  node [
    id 1857
    label "postawi&#263;"
  ]
  node [
    id 1858
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 1859
    label "consist"
  ]
  node [
    id 1860
    label "ufa&#263;"
  ]
  node [
    id 1861
    label "trust"
  ]
  node [
    id 1862
    label "wierza&#263;"
  ]
  node [
    id 1863
    label "powierzy&#263;"
  ]
  node [
    id 1864
    label "powierza&#263;"
  ]
  node [
    id 1865
    label "monopol"
  ]
  node [
    id 1866
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1867
    label "report"
  ]
  node [
    id 1868
    label "dyskalkulia"
  ]
  node [
    id 1869
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 1870
    label "rachowa&#263;"
  ]
  node [
    id 1871
    label "okre&#347;la&#263;"
  ]
  node [
    id 1872
    label "my&#347;le&#263;"
  ]
  node [
    id 1873
    label "involve"
  ]
  node [
    id 1874
    label "decydowa&#263;"
  ]
  node [
    id 1875
    label "signify"
  ]
  node [
    id 1876
    label "style"
  ]
  node [
    id 1877
    label "powodowa&#263;"
  ]
  node [
    id 1878
    label "umowa"
  ]
  node [
    id 1879
    label "cover"
  ]
  node [
    id 1880
    label "liczy&#263;"
  ]
  node [
    id 1881
    label "dysleksja"
  ]
  node [
    id 1882
    label "liczenie"
  ]
  node [
    id 1883
    label "dyscalculia"
  ]
  node [
    id 1884
    label "analfabetyzm_matematyczny"
  ]
  node [
    id 1885
    label "lock"
  ]
  node [
    id 1886
    label "absolut"
  ]
  node [
    id 1887
    label "olejek_eteryczny"
  ]
  node [
    id 1888
    label "byt"
  ]
  node [
    id 1889
    label "explanation"
  ]
  node [
    id 1890
    label "hermeneutyka"
  ]
  node [
    id 1891
    label "wypracowanie"
  ]
  node [
    id 1892
    label "realizacja"
  ]
  node [
    id 1893
    label "interpretation"
  ]
  node [
    id 1894
    label "obja&#347;nienie"
  ]
  node [
    id 1895
    label "fabrication"
  ]
  node [
    id 1896
    label "scheduling"
  ]
  node [
    id 1897
    label "operacja"
  ]
  node [
    id 1898
    label "proces"
  ]
  node [
    id 1899
    label "dzie&#322;o"
  ]
  node [
    id 1900
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1901
    label "kreacja"
  ]
  node [
    id 1902
    label "monta&#380;"
  ]
  node [
    id 1903
    label "postprodukcja"
  ]
  node [
    id 1904
    label "performance"
  ]
  node [
    id 1905
    label "sparafrazowanie"
  ]
  node [
    id 1906
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1907
    label "strawestowa&#263;"
  ]
  node [
    id 1908
    label "sparafrazowa&#263;"
  ]
  node [
    id 1909
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1910
    label "trawestowa&#263;"
  ]
  node [
    id 1911
    label "sformu&#322;owanie"
  ]
  node [
    id 1912
    label "parafrazowanie"
  ]
  node [
    id 1913
    label "ozdobnik"
  ]
  node [
    id 1914
    label "delimitacja"
  ]
  node [
    id 1915
    label "parafrazowa&#263;"
  ]
  node [
    id 1916
    label "stylizacja"
  ]
  node [
    id 1917
    label "komunikat"
  ]
  node [
    id 1918
    label "trawestowanie"
  ]
  node [
    id 1919
    label "strawestowanie"
  ]
  node [
    id 1920
    label "praca_pisemna"
  ]
  node [
    id 1921
    label "draft"
  ]
  node [
    id 1922
    label "papier_kancelaryjny"
  ]
  node [
    id 1923
    label "work"
  ]
  node [
    id 1924
    label "remark"
  ]
  node [
    id 1925
    label "zrozumia&#322;y"
  ]
  node [
    id 1926
    label "przedstawienie"
  ]
  node [
    id 1927
    label "informacja"
  ]
  node [
    id 1928
    label "poinformowanie"
  ]
  node [
    id 1929
    label "odniesienie"
  ]
  node [
    id 1930
    label "otoczenie"
  ]
  node [
    id 1931
    label "causal_agent"
  ]
  node [
    id 1932
    label "context"
  ]
  node [
    id 1933
    label "hermeneutics"
  ]
  node [
    id 1934
    label "claim"
  ]
  node [
    id 1935
    label "&#380;&#261;da&#263;"
  ]
  node [
    id 1936
    label "zmusza&#263;"
  ]
  node [
    id 1937
    label "take"
  ]
  node [
    id 1938
    label "force"
  ]
  node [
    id 1939
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1940
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 1941
    label "sandbag"
  ]
  node [
    id 1942
    label "domaga&#263;_si&#281;"
  ]
  node [
    id 1943
    label "woo"
  ]
  node [
    id 1944
    label "chcie&#263;"
  ]
  node [
    id 1945
    label "d&#322;ugi"
  ]
  node [
    id 1946
    label "d&#322;ugotrwale"
  ]
  node [
    id 1947
    label "daleki"
  ]
  node [
    id 1948
    label "d&#322;ugo"
  ]
  node [
    id 1949
    label "badanie"
  ]
  node [
    id 1950
    label "obserwowanie"
  ]
  node [
    id 1951
    label "zrecenzowanie"
  ]
  node [
    id 1952
    label "kontrola"
  ]
  node [
    id 1953
    label "analysis"
  ]
  node [
    id 1954
    label "rektalny"
  ]
  node [
    id 1955
    label "ustalenie"
  ]
  node [
    id 1956
    label "macanie"
  ]
  node [
    id 1957
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 1958
    label "udowadnianie"
  ]
  node [
    id 1959
    label "bia&#322;a_niedziela"
  ]
  node [
    id 1960
    label "diagnostyka"
  ]
  node [
    id 1961
    label "dociekanie"
  ]
  node [
    id 1962
    label "sprawdzanie"
  ]
  node [
    id 1963
    label "penetrowanie"
  ]
  node [
    id 1964
    label "ustalanie"
  ]
  node [
    id 1965
    label "rozpatrywanie"
  ]
  node [
    id 1966
    label "investigation"
  ]
  node [
    id 1967
    label "wziernikowanie"
  ]
  node [
    id 1968
    label "examination"
  ]
  node [
    id 1969
    label "miasteczko_rowerowe"
  ]
  node [
    id 1970
    label "porada"
  ]
  node [
    id 1971
    label "fotowoltaika"
  ]
  node [
    id 1972
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 1973
    label "nauki_o_poznaniu"
  ]
  node [
    id 1974
    label "nomotetyczny"
  ]
  node [
    id 1975
    label "systematyka"
  ]
  node [
    id 1976
    label "typologia"
  ]
  node [
    id 1977
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 1978
    label "kultura_duchowa"
  ]
  node [
    id 1979
    label "&#322;awa_szkolna"
  ]
  node [
    id 1980
    label "nauki_penalne"
  ]
  node [
    id 1981
    label "teoria_naukowa"
  ]
  node [
    id 1982
    label "inwentyka"
  ]
  node [
    id 1983
    label "metodologia"
  ]
  node [
    id 1984
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 1985
    label "nauki_o_Ziemi"
  ]
  node [
    id 1986
    label "umiej&#281;tnie"
  ]
  node [
    id 1987
    label "udany"
  ]
  node [
    id 1988
    label "umny"
  ]
  node [
    id 1989
    label "udanie"
  ]
  node [
    id 1990
    label "fajny"
  ]
  node [
    id 1991
    label "dobroczynny"
  ]
  node [
    id 1992
    label "czw&#243;rka"
  ]
  node [
    id 1993
    label "skuteczny"
  ]
  node [
    id 1994
    label "&#347;mieszny"
  ]
  node [
    id 1995
    label "mi&#322;y"
  ]
  node [
    id 1996
    label "grzeczny"
  ]
  node [
    id 1997
    label "powitanie"
  ]
  node [
    id 1998
    label "dobrze"
  ]
  node [
    id 1999
    label "zwrot"
  ]
  node [
    id 2000
    label "pomy&#347;lny"
  ]
  node [
    id 2001
    label "moralny"
  ]
  node [
    id 2002
    label "drogi"
  ]
  node [
    id 2003
    label "pozytywny"
  ]
  node [
    id 2004
    label "odpowiedni"
  ]
  node [
    id 2005
    label "korzystny"
  ]
  node [
    id 2006
    label "pos&#322;uszny"
  ]
  node [
    id 2007
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 2008
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 2009
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 2010
    label "relate"
  ]
  node [
    id 2011
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 2012
    label "organizowa&#263;"
  ]
  node [
    id 2013
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 2014
    label "czyni&#263;"
  ]
  node [
    id 2015
    label "stylizowa&#263;"
  ]
  node [
    id 2016
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 2017
    label "falowa&#263;"
  ]
  node [
    id 2018
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 2019
    label "peddle"
  ]
  node [
    id 2020
    label "wydala&#263;"
  ]
  node [
    id 2021
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 2022
    label "tentegowa&#263;"
  ]
  node [
    id 2023
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 2024
    label "urz&#261;dza&#263;"
  ]
  node [
    id 2025
    label "oszukiwa&#263;"
  ]
  node [
    id 2026
    label "ukazywa&#263;"
  ]
  node [
    id 2027
    label "przerabia&#263;"
  ]
  node [
    id 2028
    label "act"
  ]
  node [
    id 2029
    label "post&#281;powa&#263;"
  ]
  node [
    id 2030
    label "intercede"
  ]
  node [
    id 2031
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 2032
    label "motywowa&#263;"
  ]
  node [
    id 2033
    label "connect"
  ]
  node [
    id 2034
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 2035
    label "copulate"
  ]
  node [
    id 2036
    label "&#380;y&#263;"
  ]
  node [
    id 2037
    label "r&#243;&#380;niczka"
  ]
  node [
    id 2038
    label "materia"
  ]
  node [
    id 2039
    label "szambo"
  ]
  node [
    id 2040
    label "component"
  ]
  node [
    id 2041
    label "szkodnik"
  ]
  node [
    id 2042
    label "gangsterski"
  ]
  node [
    id 2043
    label "underworld"
  ]
  node [
    id 2044
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 2045
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 2046
    label "materia&#322;"
  ]
  node [
    id 2047
    label "ropa"
  ]
  node [
    id 2048
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 2049
    label "Rzym_Zachodni"
  ]
  node [
    id 2050
    label "Rzym_Wschodni"
  ]
  node [
    id 2051
    label "fumigacja"
  ]
  node [
    id 2052
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 2053
    label "niszczyciel"
  ]
  node [
    id 2054
    label "zwierz&#281;_domowe"
  ]
  node [
    id 2055
    label "vermin"
  ]
  node [
    id 2056
    label "huczek"
  ]
  node [
    id 2057
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 2058
    label "po_gangstersku"
  ]
  node [
    id 2059
    label "przest&#281;pczy"
  ]
  node [
    id 2060
    label "smr&#243;d"
  ]
  node [
    id 2061
    label "gips"
  ]
  node [
    id 2062
    label "koszmar"
  ]
  node [
    id 2063
    label "pasztet"
  ]
  node [
    id 2064
    label "kanalizacja"
  ]
  node [
    id 2065
    label "mire"
  ]
  node [
    id 2066
    label "zbiornik"
  ]
  node [
    id 2067
    label "kloaka"
  ]
  node [
    id 2068
    label "astronomicznie"
  ]
  node [
    id 2069
    label "olbrzymi"
  ]
  node [
    id 2070
    label "ogromny"
  ]
  node [
    id 2071
    label "jebitny"
  ]
  node [
    id 2072
    label "ogromnie"
  ]
  node [
    id 2073
    label "olbrzymio"
  ]
  node [
    id 2074
    label "&#380;ywy"
  ]
  node [
    id 2075
    label "biologicznie"
  ]
  node [
    id 2076
    label "&#380;ywo"
  ]
  node [
    id 2077
    label "rozwojowo"
  ]
  node [
    id 2078
    label "biologically"
  ]
  node [
    id 2079
    label "&#380;yciowy"
  ]
  node [
    id 2080
    label "szybki"
  ]
  node [
    id 2081
    label "&#380;ywotny"
  ]
  node [
    id 2082
    label "o&#380;ywianie"
  ]
  node [
    id 2083
    label "&#380;ycie"
  ]
  node [
    id 2084
    label "g&#322;&#281;boki"
  ]
  node [
    id 2085
    label "wyra&#378;ny"
  ]
  node [
    id 2086
    label "czynny"
  ]
  node [
    id 2087
    label "aktualny"
  ]
  node [
    id 2088
    label "zgrabny"
  ]
  node [
    id 2089
    label "realistyczny"
  ]
  node [
    id 2090
    label "energiczny"
  ]
  node [
    id 2091
    label "leczniczy"
  ]
  node [
    id 2092
    label "lekarsko"
  ]
  node [
    id 2093
    label "medycznie"
  ]
  node [
    id 2094
    label "paramedyczny"
  ]
  node [
    id 2095
    label "profilowy"
  ]
  node [
    id 2096
    label "bia&#322;y"
  ]
  node [
    id 2097
    label "praktyczny"
  ]
  node [
    id 2098
    label "specjalistyczny"
  ]
  node [
    id 2099
    label "specjalny"
  ]
  node [
    id 2100
    label "intencjonalny"
  ]
  node [
    id 2101
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 2102
    label "niedorozw&#243;j"
  ]
  node [
    id 2103
    label "szczeg&#243;lny"
  ]
  node [
    id 2104
    label "specjalnie"
  ]
  node [
    id 2105
    label "nieetatowy"
  ]
  node [
    id 2106
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 2107
    label "nienormalny"
  ]
  node [
    id 2108
    label "umy&#347;lnie"
  ]
  node [
    id 2109
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 2110
    label "leczniczo"
  ]
  node [
    id 2111
    label "specjalistycznie"
  ]
  node [
    id 2112
    label "fachowo"
  ]
  node [
    id 2113
    label "fachowy"
  ]
  node [
    id 2114
    label "racjonalny"
  ]
  node [
    id 2115
    label "u&#380;yteczny"
  ]
  node [
    id 2116
    label "praktycznie"
  ]
  node [
    id 2117
    label "oficjalnie"
  ]
  node [
    id 2118
    label "lekarski"
  ]
  node [
    id 2119
    label "carat"
  ]
  node [
    id 2120
    label "bia&#322;y_murzyn"
  ]
  node [
    id 2121
    label "Rosjanin"
  ]
  node [
    id 2122
    label "bia&#322;e"
  ]
  node [
    id 2123
    label "jasnosk&#243;ry"
  ]
  node [
    id 2124
    label "bierka_szachowa"
  ]
  node [
    id 2125
    label "bia&#322;y_taniec"
  ]
  node [
    id 2126
    label "dzia&#322;acz"
  ]
  node [
    id 2127
    label "bezbarwny"
  ]
  node [
    id 2128
    label "siwy"
  ]
  node [
    id 2129
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 2130
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 2131
    label "Polak"
  ]
  node [
    id 2132
    label "bia&#322;o"
  ]
  node [
    id 2133
    label "typ_orientalny"
  ]
  node [
    id 2134
    label "libera&#322;"
  ]
  node [
    id 2135
    label "&#347;nie&#380;nie"
  ]
  node [
    id 2136
    label "konserwatysta"
  ]
  node [
    id 2137
    label "&#347;nie&#380;no"
  ]
  node [
    id 2138
    label "bia&#322;as"
  ]
  node [
    id 2139
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 2140
    label "blady"
  ]
  node [
    id 2141
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 2142
    label "nacjonalista"
  ]
  node [
    id 2143
    label "jasny"
  ]
  node [
    id 2144
    label "paramedycznie"
  ]
  node [
    id 2145
    label "practice"
  ]
  node [
    id 2146
    label "znawstwo"
  ]
  node [
    id 2147
    label "skill"
  ]
  node [
    id 2148
    label "czyn"
  ]
  node [
    id 2149
    label "eksperiencja"
  ]
  node [
    id 2150
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 2151
    label "najem"
  ]
  node [
    id 2152
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 2153
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 2154
    label "zak&#322;ad"
  ]
  node [
    id 2155
    label "stosunek_pracy"
  ]
  node [
    id 2156
    label "benedykty&#324;ski"
  ]
  node [
    id 2157
    label "poda&#380;_pracy"
  ]
  node [
    id 2158
    label "pracowanie"
  ]
  node [
    id 2159
    label "tyrka"
  ]
  node [
    id 2160
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 2161
    label "zaw&#243;d"
  ]
  node [
    id 2162
    label "tynkarski"
  ]
  node [
    id 2163
    label "pracowa&#263;"
  ]
  node [
    id 2164
    label "zmiana"
  ]
  node [
    id 2165
    label "zobowi&#261;zanie"
  ]
  node [
    id 2166
    label "kierownictwo"
  ]
  node [
    id 2167
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 2168
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 2169
    label "ceremony"
  ]
  node [
    id 2170
    label "funkcja"
  ]
  node [
    id 2171
    label "znajomo&#347;&#263;"
  ]
  node [
    id 2172
    label "information"
  ]
  node [
    id 2173
    label "kiperstwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 8
    target 586
  ]
  edge [
    source 8
    target 587
  ]
  edge [
    source 8
    target 588
  ]
  edge [
    source 8
    target 589
  ]
  edge [
    source 8
    target 590
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 633
  ]
  edge [
    source 11
    target 634
  ]
  edge [
    source 11
    target 635
  ]
  edge [
    source 11
    target 636
  ]
  edge [
    source 11
    target 637
  ]
  edge [
    source 11
    target 638
  ]
  edge [
    source 11
    target 639
  ]
  edge [
    source 11
    target 640
  ]
  edge [
    source 11
    target 641
  ]
  edge [
    source 11
    target 642
  ]
  edge [
    source 11
    target 643
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 644
  ]
  edge [
    source 12
    target 645
  ]
  edge [
    source 12
    target 646
  ]
  edge [
    source 12
    target 647
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 648
  ]
  edge [
    source 12
    target 649
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 650
  ]
  edge [
    source 12
    target 651
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 652
  ]
  edge [
    source 12
    target 653
  ]
  edge [
    source 12
    target 240
  ]
  edge [
    source 12
    target 654
  ]
  edge [
    source 12
    target 655
  ]
  edge [
    source 12
    target 656
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 658
  ]
  edge [
    source 12
    target 659
  ]
  edge [
    source 12
    target 660
  ]
  edge [
    source 12
    target 661
  ]
  edge [
    source 12
    target 662
  ]
  edge [
    source 12
    target 663
  ]
  edge [
    source 12
    target 664
  ]
  edge [
    source 12
    target 665
  ]
  edge [
    source 12
    target 666
  ]
  edge [
    source 12
    target 667
  ]
  edge [
    source 12
    target 668
  ]
  edge [
    source 12
    target 669
  ]
  edge [
    source 12
    target 670
  ]
  edge [
    source 12
    target 232
  ]
  edge [
    source 12
    target 671
  ]
  edge [
    source 12
    target 672
  ]
  edge [
    source 12
    target 673
  ]
  edge [
    source 12
    target 674
  ]
  edge [
    source 12
    target 675
  ]
  edge [
    source 12
    target 676
  ]
  edge [
    source 12
    target 677
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 678
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 679
  ]
  edge [
    source 12
    target 680
  ]
  edge [
    source 12
    target 267
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 681
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 682
  ]
  edge [
    source 12
    target 683
  ]
  edge [
    source 12
    target 684
  ]
  edge [
    source 12
    target 685
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 686
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 12
    target 687
  ]
  edge [
    source 12
    target 688
  ]
  edge [
    source 12
    target 689
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 690
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 691
  ]
  edge [
    source 12
    target 692
  ]
  edge [
    source 12
    target 693
  ]
  edge [
    source 12
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
  edge [
    source 12
    target 698
  ]
  edge [
    source 12
    target 582
  ]
  edge [
    source 12
    target 699
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 700
  ]
  edge [
    source 12
    target 701
  ]
  edge [
    source 12
    target 702
  ]
  edge [
    source 12
    target 703
  ]
  edge [
    source 12
    target 42
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 704
  ]
  edge [
    source 12
    target 705
  ]
  edge [
    source 12
    target 706
  ]
  edge [
    source 12
    target 707
  ]
  edge [
    source 12
    target 708
  ]
  edge [
    source 12
    target 709
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 711
  ]
  edge [
    source 12
    target 712
  ]
  edge [
    source 12
    target 713
  ]
  edge [
    source 12
    target 714
  ]
  edge [
    source 12
    target 715
  ]
  edge [
    source 12
    target 716
  ]
  edge [
    source 12
    target 717
  ]
  edge [
    source 12
    target 718
  ]
  edge [
    source 12
    target 719
  ]
  edge [
    source 12
    target 720
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 722
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 300
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 724
  ]
  edge [
    source 12
    target 725
  ]
  edge [
    source 12
    target 726
  ]
  edge [
    source 12
    target 205
  ]
  edge [
    source 12
    target 727
  ]
  edge [
    source 12
    target 728
  ]
  edge [
    source 12
    target 729
  ]
  edge [
    source 12
    target 730
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 732
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 735
  ]
  edge [
    source 12
    target 736
  ]
  edge [
    source 12
    target 737
  ]
  edge [
    source 12
    target 738
  ]
  edge [
    source 12
    target 739
  ]
  edge [
    source 12
    target 740
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 742
  ]
  edge [
    source 12
    target 743
  ]
  edge [
    source 12
    target 744
  ]
  edge [
    source 12
    target 745
  ]
  edge [
    source 12
    target 746
  ]
  edge [
    source 12
    target 747
  ]
  edge [
    source 12
    target 748
  ]
  edge [
    source 12
    target 749
  ]
  edge [
    source 12
    target 750
  ]
  edge [
    source 12
    target 751
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 752
  ]
  edge [
    source 12
    target 753
  ]
  edge [
    source 12
    target 754
  ]
  edge [
    source 12
    target 755
  ]
  edge [
    source 12
    target 756
  ]
  edge [
    source 12
    target 757
  ]
  edge [
    source 12
    target 758
  ]
  edge [
    source 12
    target 759
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 237
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 606
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 833
  ]
  edge [
    source 12
    target 834
  ]
  edge [
    source 12
    target 835
  ]
  edge [
    source 12
    target 836
  ]
  edge [
    source 12
    target 837
  ]
  edge [
    source 12
    target 838
  ]
  edge [
    source 12
    target 839
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 841
  ]
  edge [
    source 12
    target 842
  ]
  edge [
    source 12
    target 843
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 844
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 846
  ]
  edge [
    source 12
    target 847
  ]
  edge [
    source 12
    target 848
  ]
  edge [
    source 12
    target 849
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 851
  ]
  edge [
    source 12
    target 852
  ]
  edge [
    source 12
    target 853
  ]
  edge [
    source 12
    target 854
  ]
  edge [
    source 12
    target 855
  ]
  edge [
    source 12
    target 856
  ]
  edge [
    source 12
    target 857
  ]
  edge [
    source 12
    target 858
  ]
  edge [
    source 12
    target 859
  ]
  edge [
    source 12
    target 860
  ]
  edge [
    source 12
    target 861
  ]
  edge [
    source 12
    target 862
  ]
  edge [
    source 12
    target 863
  ]
  edge [
    source 12
    target 864
  ]
  edge [
    source 12
    target 865
  ]
  edge [
    source 12
    target 866
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 908
  ]
  edge [
    source 12
    target 909
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 13
    target 53
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 755
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 50
  ]
  edge [
    source 13
    target 62
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 959
  ]
  edge [
    source 15
    target 960
  ]
  edge [
    source 15
    target 961
  ]
  edge [
    source 15
    target 962
  ]
  edge [
    source 15
    target 963
  ]
  edge [
    source 15
    target 964
  ]
  edge [
    source 15
    target 965
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 966
  ]
  edge [
    source 15
    target 967
  ]
  edge [
    source 15
    target 968
  ]
  edge [
    source 15
    target 969
  ]
  edge [
    source 15
    target 970
  ]
  edge [
    source 15
    target 971
  ]
  edge [
    source 15
    target 972
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 973
  ]
  edge [
    source 15
    target 974
  ]
  edge [
    source 15
    target 975
  ]
  edge [
    source 15
    target 976
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 318
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 322
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 323
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 266
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 613
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 366
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 477
  ]
  edge [
    source 17
    target 75
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 479
  ]
  edge [
    source 17
    target 481
  ]
  edge [
    source 17
    target 442
  ]
  edge [
    source 17
    target 483
  ]
  edge [
    source 17
    target 484
  ]
  edge [
    source 17
    target 485
  ]
  edge [
    source 17
    target 487
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 489
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 495
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 497
  ]
  edge [
    source 17
    target 501
  ]
  edge [
    source 17
    target 502
  ]
  edge [
    source 17
    target 503
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 504
  ]
  edge [
    source 17
    target 505
  ]
  edge [
    source 17
    target 506
  ]
  edge [
    source 17
    target 295
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 494
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 446
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 799
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 409
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 230
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 284
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 287
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 371
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1076
  ]
  edge [
    source 18
    target 1077
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1079
  ]
  edge [
    source 18
    target 55
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1080
  ]
  edge [
    source 19
    target 1081
  ]
  edge [
    source 19
    target 1082
  ]
  edge [
    source 19
    target 1083
  ]
  edge [
    source 19
    target 1035
  ]
  edge [
    source 19
    target 1084
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 266
  ]
  edge [
    source 19
    target 1085
  ]
  edge [
    source 19
    target 701
  ]
  edge [
    source 19
    target 1086
  ]
  edge [
    source 19
    target 240
  ]
  edge [
    source 19
    target 233
  ]
  edge [
    source 19
    target 1087
  ]
  edge [
    source 19
    target 1088
  ]
  edge [
    source 19
    target 1089
  ]
  edge [
    source 19
    target 707
  ]
  edge [
    source 19
    target 661
  ]
  edge [
    source 19
    target 1090
  ]
  edge [
    source 19
    target 1091
  ]
  edge [
    source 19
    target 1092
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 1093
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 1095
  ]
  edge [
    source 19
    target 391
  ]
  edge [
    source 19
    target 1096
  ]
  edge [
    source 19
    target 1097
  ]
  edge [
    source 19
    target 1098
  ]
  edge [
    source 19
    target 1099
  ]
  edge [
    source 19
    target 1100
  ]
  edge [
    source 19
    target 1101
  ]
  edge [
    source 19
    target 1102
  ]
  edge [
    source 19
    target 1103
  ]
  edge [
    source 19
    target 1104
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1106
  ]
  edge [
    source 19
    target 1107
  ]
  edge [
    source 19
    target 1108
  ]
  edge [
    source 19
    target 1109
  ]
  edge [
    source 19
    target 42
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 1110
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 1111
  ]
  edge [
    source 20
    target 1112
  ]
  edge [
    source 20
    target 1113
  ]
  edge [
    source 20
    target 1114
  ]
  edge [
    source 20
    target 1115
  ]
  edge [
    source 20
    target 1116
  ]
  edge [
    source 20
    target 1117
  ]
  edge [
    source 20
    target 1118
  ]
  edge [
    source 20
    target 1119
  ]
  edge [
    source 20
    target 1120
  ]
  edge [
    source 20
    target 1121
  ]
  edge [
    source 20
    target 1122
  ]
  edge [
    source 20
    target 1123
  ]
  edge [
    source 20
    target 1124
  ]
  edge [
    source 20
    target 300
  ]
  edge [
    source 20
    target 1125
  ]
  edge [
    source 20
    target 1126
  ]
  edge [
    source 20
    target 1127
  ]
  edge [
    source 20
    target 1128
  ]
  edge [
    source 20
    target 1129
  ]
  edge [
    source 20
    target 1130
  ]
  edge [
    source 20
    target 1131
  ]
  edge [
    source 20
    target 1132
  ]
  edge [
    source 20
    target 1084
  ]
  edge [
    source 20
    target 1133
  ]
  edge [
    source 20
    target 1085
  ]
  edge [
    source 20
    target 1134
  ]
  edge [
    source 20
    target 1135
  ]
  edge [
    source 20
    target 1136
  ]
  edge [
    source 20
    target 1137
  ]
  edge [
    source 20
    target 1138
  ]
  edge [
    source 20
    target 1139
  ]
  edge [
    source 20
    target 1140
  ]
  edge [
    source 20
    target 1141
  ]
  edge [
    source 20
    target 1142
  ]
  edge [
    source 20
    target 1143
  ]
  edge [
    source 20
    target 507
  ]
  edge [
    source 20
    target 1144
  ]
  edge [
    source 20
    target 1145
  ]
  edge [
    source 20
    target 1146
  ]
  edge [
    source 20
    target 1147
  ]
  edge [
    source 20
    target 805
  ]
  edge [
    source 20
    target 1148
  ]
  edge [
    source 20
    target 1149
  ]
  edge [
    source 20
    target 1150
  ]
  edge [
    source 20
    target 1151
  ]
  edge [
    source 20
    target 1152
  ]
  edge [
    source 20
    target 1153
  ]
  edge [
    source 20
    target 1154
  ]
  edge [
    source 20
    target 1155
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 237
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1156
  ]
  edge [
    source 21
    target 1157
  ]
  edge [
    source 21
    target 1158
  ]
  edge [
    source 21
    target 1159
  ]
  edge [
    source 21
    target 640
  ]
  edge [
    source 21
    target 1160
  ]
  edge [
    source 21
    target 1161
  ]
  edge [
    source 21
    target 1162
  ]
  edge [
    source 21
    target 1163
  ]
  edge [
    source 21
    target 1164
  ]
  edge [
    source 21
    target 1165
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 1168
  ]
  edge [
    source 21
    target 1169
  ]
  edge [
    source 21
    target 1170
  ]
  edge [
    source 21
    target 1171
  ]
  edge [
    source 21
    target 1172
  ]
  edge [
    source 21
    target 1173
  ]
  edge [
    source 21
    target 1174
  ]
  edge [
    source 21
    target 638
  ]
  edge [
    source 21
    target 1175
  ]
  edge [
    source 21
    target 1176
  ]
  edge [
    source 21
    target 1177
  ]
  edge [
    source 21
    target 1178
  ]
  edge [
    source 21
    target 1179
  ]
  edge [
    source 21
    target 1180
  ]
  edge [
    source 21
    target 1181
  ]
  edge [
    source 21
    target 1182
  ]
  edge [
    source 21
    target 1183
  ]
  edge [
    source 21
    target 1184
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 44
  ]
  edge [
    source 22
    target 45
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 68
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 65
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 53
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1193
  ]
  edge [
    source 24
    target 1194
  ]
  edge [
    source 24
    target 1195
  ]
  edge [
    source 24
    target 1196
  ]
  edge [
    source 24
    target 1197
  ]
  edge [
    source 24
    target 1198
  ]
  edge [
    source 24
    target 1199
  ]
  edge [
    source 24
    target 1200
  ]
  edge [
    source 24
    target 1201
  ]
  edge [
    source 24
    target 1202
  ]
  edge [
    source 24
    target 1203
  ]
  edge [
    source 24
    target 484
  ]
  edge [
    source 24
    target 101
  ]
  edge [
    source 24
    target 1204
  ]
  edge [
    source 24
    target 1205
  ]
  edge [
    source 24
    target 1206
  ]
  edge [
    source 24
    target 1207
  ]
  edge [
    source 24
    target 1208
  ]
  edge [
    source 24
    target 1209
  ]
  edge [
    source 24
    target 1210
  ]
  edge [
    source 24
    target 1211
  ]
  edge [
    source 24
    target 1212
  ]
  edge [
    source 24
    target 1213
  ]
  edge [
    source 24
    target 1214
  ]
  edge [
    source 24
    target 1215
  ]
  edge [
    source 24
    target 1216
  ]
  edge [
    source 24
    target 1217
  ]
  edge [
    source 24
    target 1218
  ]
  edge [
    source 24
    target 499
  ]
  edge [
    source 24
    target 1219
  ]
  edge [
    source 24
    target 1220
  ]
  edge [
    source 24
    target 1221
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 163
  ]
  edge [
    source 26
    target 1222
  ]
  edge [
    source 26
    target 1223
  ]
  edge [
    source 26
    target 1224
  ]
  edge [
    source 26
    target 1225
  ]
  edge [
    source 26
    target 1226
  ]
  edge [
    source 26
    target 1227
  ]
  edge [
    source 26
    target 1228
  ]
  edge [
    source 26
    target 1229
  ]
  edge [
    source 26
    target 1230
  ]
  edge [
    source 26
    target 1231
  ]
  edge [
    source 26
    target 1232
  ]
  edge [
    source 26
    target 1233
  ]
  edge [
    source 26
    target 1234
  ]
  edge [
    source 26
    target 1235
  ]
  edge [
    source 26
    target 1236
  ]
  edge [
    source 26
    target 1237
  ]
  edge [
    source 26
    target 266
  ]
  edge [
    source 26
    target 366
  ]
  edge [
    source 26
    target 1006
  ]
  edge [
    source 26
    target 494
  ]
  edge [
    source 26
    target 733
  ]
  edge [
    source 26
    target 1238
  ]
  edge [
    source 26
    target 1239
  ]
  edge [
    source 26
    target 1240
  ]
  edge [
    source 26
    target 1241
  ]
  edge [
    source 26
    target 1242
  ]
  edge [
    source 26
    target 337
  ]
  edge [
    source 26
    target 1243
  ]
  edge [
    source 26
    target 1244
  ]
  edge [
    source 26
    target 1245
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1246
  ]
  edge [
    source 27
    target 1247
  ]
  edge [
    source 27
    target 1248
  ]
  edge [
    source 27
    target 1249
  ]
  edge [
    source 27
    target 1250
  ]
  edge [
    source 27
    target 1251
  ]
  edge [
    source 27
    target 1252
  ]
  edge [
    source 27
    target 1253
  ]
  edge [
    source 27
    target 1254
  ]
  edge [
    source 27
    target 1255
  ]
  edge [
    source 27
    target 1256
  ]
  edge [
    source 27
    target 1257
  ]
  edge [
    source 27
    target 1258
  ]
  edge [
    source 27
    target 1259
  ]
  edge [
    source 27
    target 1260
  ]
  edge [
    source 27
    target 1261
  ]
  edge [
    source 27
    target 1262
  ]
  edge [
    source 27
    target 1263
  ]
  edge [
    source 27
    target 1264
  ]
  edge [
    source 27
    target 1265
  ]
  edge [
    source 27
    target 1266
  ]
  edge [
    source 27
    target 1267
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1268
  ]
  edge [
    source 28
    target 1269
  ]
  edge [
    source 28
    target 1270
  ]
  edge [
    source 28
    target 1271
  ]
  edge [
    source 28
    target 1272
  ]
  edge [
    source 28
    target 530
  ]
  edge [
    source 28
    target 1273
  ]
  edge [
    source 28
    target 1274
  ]
  edge [
    source 28
    target 1275
  ]
  edge [
    source 28
    target 1276
  ]
  edge [
    source 28
    target 1277
  ]
  edge [
    source 28
    target 1278
  ]
  edge [
    source 28
    target 1279
  ]
  edge [
    source 28
    target 1280
  ]
  edge [
    source 28
    target 92
  ]
  edge [
    source 28
    target 1281
  ]
  edge [
    source 28
    target 1282
  ]
  edge [
    source 28
    target 1283
  ]
  edge [
    source 28
    target 1284
  ]
  edge [
    source 28
    target 1285
  ]
  edge [
    source 28
    target 1286
  ]
  edge [
    source 28
    target 1287
  ]
  edge [
    source 28
    target 1288
  ]
  edge [
    source 28
    target 1289
  ]
  edge [
    source 28
    target 130
  ]
  edge [
    source 28
    target 1290
  ]
  edge [
    source 28
    target 1291
  ]
  edge [
    source 28
    target 1292
  ]
  edge [
    source 28
    target 1293
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 28
    target 55
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1294
  ]
  edge [
    source 30
    target 1295
  ]
  edge [
    source 30
    target 1296
  ]
  edge [
    source 30
    target 625
  ]
  edge [
    source 30
    target 1297
  ]
  edge [
    source 30
    target 1298
  ]
  edge [
    source 30
    target 1299
  ]
  edge [
    source 30
    target 630
  ]
  edge [
    source 30
    target 631
  ]
  edge [
    source 30
    target 1300
  ]
  edge [
    source 30
    target 1301
  ]
  edge [
    source 30
    target 1302
  ]
  edge [
    source 30
    target 1303
  ]
  edge [
    source 30
    target 1304
  ]
  edge [
    source 30
    target 1305
  ]
  edge [
    source 30
    target 1306
  ]
  edge [
    source 30
    target 1307
  ]
  edge [
    source 30
    target 1308
  ]
  edge [
    source 30
    target 1309
  ]
  edge [
    source 30
    target 1310
  ]
  edge [
    source 30
    target 1311
  ]
  edge [
    source 30
    target 1312
  ]
  edge [
    source 30
    target 462
  ]
  edge [
    source 30
    target 1313
  ]
  edge [
    source 30
    target 1314
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 55
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1315
  ]
  edge [
    source 31
    target 1316
  ]
  edge [
    source 31
    target 1317
  ]
  edge [
    source 31
    target 1318
  ]
  edge [
    source 31
    target 1319
  ]
  edge [
    source 31
    target 1320
  ]
  edge [
    source 31
    target 1321
  ]
  edge [
    source 31
    target 266
  ]
  edge [
    source 31
    target 1322
  ]
  edge [
    source 31
    target 1323
  ]
  edge [
    source 31
    target 240
  ]
  edge [
    source 31
    target 1324
  ]
  edge [
    source 31
    target 1325
  ]
  edge [
    source 31
    target 1326
  ]
  edge [
    source 31
    target 1327
  ]
  edge [
    source 31
    target 1328
  ]
  edge [
    source 31
    target 1329
  ]
  edge [
    source 31
    target 1330
  ]
  edge [
    source 31
    target 146
  ]
  edge [
    source 31
    target 1331
  ]
  edge [
    source 31
    target 1332
  ]
  edge [
    source 31
    target 1333
  ]
  edge [
    source 31
    target 1334
  ]
  edge [
    source 31
    target 1335
  ]
  edge [
    source 31
    target 347
  ]
  edge [
    source 31
    target 1336
  ]
  edge [
    source 31
    target 1337
  ]
  edge [
    source 31
    target 1338
  ]
  edge [
    source 31
    target 1339
  ]
  edge [
    source 31
    target 1340
  ]
  edge [
    source 31
    target 346
  ]
  edge [
    source 31
    target 1341
  ]
  edge [
    source 31
    target 1342
  ]
  edge [
    source 31
    target 1343
  ]
  edge [
    source 31
    target 284
  ]
  edge [
    source 31
    target 1344
  ]
  edge [
    source 31
    target 1345
  ]
  edge [
    source 31
    target 356
  ]
  edge [
    source 31
    target 357
  ]
  edge [
    source 31
    target 152
  ]
  edge [
    source 31
    target 358
  ]
  edge [
    source 31
    target 359
  ]
  edge [
    source 31
    target 360
  ]
  edge [
    source 31
    target 361
  ]
  edge [
    source 31
    target 362
  ]
  edge [
    source 31
    target 363
  ]
  edge [
    source 31
    target 364
  ]
  edge [
    source 31
    target 365
  ]
  edge [
    source 31
    target 701
  ]
  edge [
    source 31
    target 702
  ]
  edge [
    source 31
    target 703
  ]
  edge [
    source 31
    target 42
  ]
  edge [
    source 31
    target 482
  ]
  edge [
    source 31
    target 704
  ]
  edge [
    source 31
    target 705
  ]
  edge [
    source 31
    target 706
  ]
  edge [
    source 31
    target 707
  ]
  edge [
    source 31
    target 367
  ]
  edge [
    source 31
    target 708
  ]
  edge [
    source 31
    target 709
  ]
  edge [
    source 31
    target 710
  ]
  edge [
    source 31
    target 711
  ]
  edge [
    source 31
    target 1346
  ]
  edge [
    source 31
    target 1347
  ]
  edge [
    source 31
    target 168
  ]
  edge [
    source 31
    target 1348
  ]
  edge [
    source 31
    target 1349
  ]
  edge [
    source 31
    target 1350
  ]
  edge [
    source 31
    target 1351
  ]
  edge [
    source 31
    target 1352
  ]
  edge [
    source 31
    target 979
  ]
  edge [
    source 31
    target 1353
  ]
  edge [
    source 31
    target 1354
  ]
  edge [
    source 31
    target 1355
  ]
  edge [
    source 31
    target 101
  ]
  edge [
    source 31
    target 880
  ]
  edge [
    source 31
    target 1356
  ]
  edge [
    source 31
    target 1357
  ]
  edge [
    source 31
    target 1358
  ]
  edge [
    source 31
    target 317
  ]
  edge [
    source 31
    target 163
  ]
  edge [
    source 31
    target 157
  ]
  edge [
    source 31
    target 477
  ]
  edge [
    source 31
    target 1359
  ]
  edge [
    source 31
    target 1360
  ]
  edge [
    source 31
    target 454
  ]
  edge [
    source 31
    target 316
  ]
  edge [
    source 31
    target 318
  ]
  edge [
    source 31
    target 319
  ]
  edge [
    source 31
    target 320
  ]
  edge [
    source 31
    target 321
  ]
  edge [
    source 31
    target 322
  ]
  edge [
    source 31
    target 323
  ]
  edge [
    source 31
    target 1361
  ]
  edge [
    source 31
    target 1362
  ]
  edge [
    source 31
    target 1363
  ]
  edge [
    source 31
    target 1364
  ]
  edge [
    source 31
    target 1365
  ]
  edge [
    source 31
    target 1366
  ]
  edge [
    source 31
    target 484
  ]
  edge [
    source 31
    target 1367
  ]
  edge [
    source 31
    target 1368
  ]
  edge [
    source 31
    target 1369
  ]
  edge [
    source 31
    target 877
  ]
  edge [
    source 31
    target 432
  ]
  edge [
    source 31
    target 1370
  ]
  edge [
    source 31
    target 1371
  ]
  edge [
    source 31
    target 1372
  ]
  edge [
    source 31
    target 1373
  ]
  edge [
    source 31
    target 1374
  ]
  edge [
    source 31
    target 1375
  ]
  edge [
    source 31
    target 1376
  ]
  edge [
    source 31
    target 1377
  ]
  edge [
    source 31
    target 1378
  ]
  edge [
    source 31
    target 1379
  ]
  edge [
    source 31
    target 1380
  ]
  edge [
    source 31
    target 1381
  ]
  edge [
    source 31
    target 1382
  ]
  edge [
    source 31
    target 1383
  ]
  edge [
    source 31
    target 1384
  ]
  edge [
    source 31
    target 1385
  ]
  edge [
    source 31
    target 1386
  ]
  edge [
    source 31
    target 1387
  ]
  edge [
    source 31
    target 1388
  ]
  edge [
    source 31
    target 1389
  ]
  edge [
    source 31
    target 1390
  ]
  edge [
    source 31
    target 1391
  ]
  edge [
    source 31
    target 1392
  ]
  edge [
    source 31
    target 1393
  ]
  edge [
    source 31
    target 1394
  ]
  edge [
    source 31
    target 1395
  ]
  edge [
    source 31
    target 1396
  ]
  edge [
    source 31
    target 1397
  ]
  edge [
    source 31
    target 1398
  ]
  edge [
    source 31
    target 1399
  ]
  edge [
    source 31
    target 1400
  ]
  edge [
    source 31
    target 1401
  ]
  edge [
    source 31
    target 1402
  ]
  edge [
    source 31
    target 1403
  ]
  edge [
    source 31
    target 1404
  ]
  edge [
    source 31
    target 1405
  ]
  edge [
    source 31
    target 884
  ]
  edge [
    source 31
    target 1406
  ]
  edge [
    source 31
    target 1407
  ]
  edge [
    source 31
    target 1070
  ]
  edge [
    source 31
    target 1408
  ]
  edge [
    source 31
    target 1409
  ]
  edge [
    source 32
    target 1410
  ]
  edge [
    source 32
    target 1411
  ]
  edge [
    source 32
    target 1412
  ]
  edge [
    source 32
    target 1413
  ]
  edge [
    source 32
    target 1414
  ]
  edge [
    source 32
    target 1415
  ]
  edge [
    source 32
    target 1416
  ]
  edge [
    source 32
    target 1417
  ]
  edge [
    source 32
    target 1418
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1419
  ]
  edge [
    source 33
    target 1420
  ]
  edge [
    source 33
    target 1421
  ]
  edge [
    source 33
    target 1422
  ]
  edge [
    source 33
    target 1423
  ]
  edge [
    source 33
    target 1424
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 624
  ]
  edge [
    source 35
    target 625
  ]
  edge [
    source 35
    target 626
  ]
  edge [
    source 35
    target 627
  ]
  edge [
    source 35
    target 628
  ]
  edge [
    source 35
    target 629
  ]
  edge [
    source 35
    target 630
  ]
  edge [
    source 35
    target 631
  ]
  edge [
    source 35
    target 632
  ]
  edge [
    source 35
    target 1425
  ]
  edge [
    source 35
    target 1426
  ]
  edge [
    source 35
    target 190
  ]
  edge [
    source 35
    target 1427
  ]
  edge [
    source 35
    target 1428
  ]
  edge [
    source 35
    target 1429
  ]
  edge [
    source 35
    target 1430
  ]
  edge [
    source 35
    target 1431
  ]
  edge [
    source 35
    target 1432
  ]
  edge [
    source 35
    target 1433
  ]
  edge [
    source 35
    target 1434
  ]
  edge [
    source 35
    target 1435
  ]
  edge [
    source 35
    target 1436
  ]
  edge [
    source 35
    target 1437
  ]
  edge [
    source 35
    target 1438
  ]
  edge [
    source 35
    target 1439
  ]
  edge [
    source 35
    target 1440
  ]
  edge [
    source 35
    target 187
  ]
  edge [
    source 35
    target 1441
  ]
  edge [
    source 35
    target 1442
  ]
  edge [
    source 35
    target 1443
  ]
  edge [
    source 35
    target 1444
  ]
  edge [
    source 35
    target 1445
  ]
  edge [
    source 35
    target 1446
  ]
  edge [
    source 35
    target 1447
  ]
  edge [
    source 35
    target 1448
  ]
  edge [
    source 35
    target 1449
  ]
  edge [
    source 35
    target 1079
  ]
  edge [
    source 35
    target 1450
  ]
  edge [
    source 35
    target 733
  ]
  edge [
    source 35
    target 1451
  ]
  edge [
    source 35
    target 1452
  ]
  edge [
    source 35
    target 1453
  ]
  edge [
    source 35
    target 1148
  ]
  edge [
    source 35
    target 1454
  ]
  edge [
    source 35
    target 1133
  ]
  edge [
    source 35
    target 1455
  ]
  edge [
    source 35
    target 1456
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1457
  ]
  edge [
    source 36
    target 733
  ]
  edge [
    source 36
    target 915
  ]
  edge [
    source 36
    target 1458
  ]
  edge [
    source 36
    target 478
  ]
  edge [
    source 36
    target 1459
  ]
  edge [
    source 36
    target 1132
  ]
  edge [
    source 36
    target 1084
  ]
  edge [
    source 36
    target 1133
  ]
  edge [
    source 36
    target 1085
  ]
  edge [
    source 36
    target 1134
  ]
  edge [
    source 36
    target 1135
  ]
  edge [
    source 36
    target 1136
  ]
  edge [
    source 36
    target 1137
  ]
  edge [
    source 36
    target 1138
  ]
  edge [
    source 36
    target 1139
  ]
  edge [
    source 36
    target 1140
  ]
  edge [
    source 36
    target 1141
  ]
  edge [
    source 36
    target 1142
  ]
  edge [
    source 36
    target 1143
  ]
  edge [
    source 36
    target 507
  ]
  edge [
    source 36
    target 1144
  ]
  edge [
    source 36
    target 1145
  ]
  edge [
    source 36
    target 1146
  ]
  edge [
    source 36
    target 1147
  ]
  edge [
    source 36
    target 805
  ]
  edge [
    source 36
    target 1148
  ]
  edge [
    source 36
    target 1149
  ]
  edge [
    source 36
    target 1150
  ]
  edge [
    source 36
    target 1151
  ]
  edge [
    source 36
    target 1152
  ]
  edge [
    source 36
    target 1460
  ]
  edge [
    source 36
    target 1461
  ]
  edge [
    source 36
    target 696
  ]
  edge [
    source 36
    target 1462
  ]
  edge [
    source 36
    target 1463
  ]
  edge [
    source 36
    target 1464
  ]
  edge [
    source 36
    target 1465
  ]
  edge [
    source 36
    target 1466
  ]
  edge [
    source 36
    target 1467
  ]
  edge [
    source 36
    target 939
  ]
  edge [
    source 36
    target 101
  ]
  edge [
    source 36
    target 1468
  ]
  edge [
    source 36
    target 1469
  ]
  edge [
    source 36
    target 755
  ]
  edge [
    source 36
    target 1470
  ]
  edge [
    source 36
    target 240
  ]
  edge [
    source 36
    target 1471
  ]
  edge [
    source 36
    target 146
  ]
  edge [
    source 36
    target 1472
  ]
  edge [
    source 36
    target 898
  ]
  edge [
    source 36
    target 1473
  ]
  edge [
    source 36
    target 1474
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1261
  ]
  edge [
    source 37
    target 1475
  ]
  edge [
    source 37
    target 1476
  ]
  edge [
    source 37
    target 1477
  ]
  edge [
    source 37
    target 1478
  ]
  edge [
    source 37
    target 1479
  ]
  edge [
    source 37
    target 1179
  ]
  edge [
    source 37
    target 1480
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1481
  ]
  edge [
    source 38
    target 1482
  ]
  edge [
    source 38
    target 1483
  ]
  edge [
    source 38
    target 1484
  ]
  edge [
    source 38
    target 1485
  ]
  edge [
    source 38
    target 1486
  ]
  edge [
    source 38
    target 1487
  ]
  edge [
    source 38
    target 1488
  ]
  edge [
    source 38
    target 331
  ]
  edge [
    source 38
    target 1489
  ]
  edge [
    source 38
    target 1490
  ]
  edge [
    source 38
    target 1491
  ]
  edge [
    source 38
    target 1492
  ]
  edge [
    source 38
    target 1493
  ]
  edge [
    source 38
    target 1494
  ]
  edge [
    source 38
    target 1495
  ]
  edge [
    source 38
    target 1496
  ]
  edge [
    source 38
    target 1497
  ]
  edge [
    source 38
    target 1498
  ]
  edge [
    source 38
    target 1499
  ]
  edge [
    source 38
    target 1500
  ]
  edge [
    source 38
    target 1501
  ]
  edge [
    source 38
    target 1502
  ]
  edge [
    source 38
    target 1503
  ]
  edge [
    source 38
    target 1504
  ]
  edge [
    source 38
    target 1505
  ]
  edge [
    source 38
    target 1506
  ]
  edge [
    source 38
    target 1507
  ]
  edge [
    source 38
    target 1508
  ]
  edge [
    source 38
    target 1509
  ]
  edge [
    source 38
    target 1510
  ]
  edge [
    source 38
    target 1511
  ]
  edge [
    source 38
    target 1512
  ]
  edge [
    source 38
    target 1513
  ]
  edge [
    source 38
    target 1514
  ]
  edge [
    source 38
    target 1515
  ]
  edge [
    source 38
    target 1516
  ]
  edge [
    source 38
    target 1517
  ]
  edge [
    source 38
    target 423
  ]
  edge [
    source 38
    target 1518
  ]
  edge [
    source 38
    target 1519
  ]
  edge [
    source 38
    target 1520
  ]
  edge [
    source 38
    target 1521
  ]
  edge [
    source 38
    target 1522
  ]
  edge [
    source 38
    target 1523
  ]
  edge [
    source 38
    target 1524
  ]
  edge [
    source 38
    target 1525
  ]
  edge [
    source 38
    target 1526
  ]
  edge [
    source 38
    target 1527
  ]
  edge [
    source 38
    target 1528
  ]
  edge [
    source 38
    target 1529
  ]
  edge [
    source 38
    target 1530
  ]
  edge [
    source 38
    target 1531
  ]
  edge [
    source 38
    target 1532
  ]
  edge [
    source 38
    target 1533
  ]
  edge [
    source 38
    target 438
  ]
  edge [
    source 38
    target 1534
  ]
  edge [
    source 38
    target 1535
  ]
  edge [
    source 38
    target 1536
  ]
  edge [
    source 38
    target 1537
  ]
  edge [
    source 38
    target 1538
  ]
  edge [
    source 38
    target 1539
  ]
  edge [
    source 38
    target 1540
  ]
  edge [
    source 38
    target 1541
  ]
  edge [
    source 38
    target 1542
  ]
  edge [
    source 38
    target 1543
  ]
  edge [
    source 38
    target 1544
  ]
  edge [
    source 38
    target 1545
  ]
  edge [
    source 38
    target 1546
  ]
  edge [
    source 38
    target 1547
  ]
  edge [
    source 38
    target 1548
  ]
  edge [
    source 38
    target 1549
  ]
  edge [
    source 38
    target 1550
  ]
  edge [
    source 38
    target 1551
  ]
  edge [
    source 38
    target 498
  ]
  edge [
    source 38
    target 1552
  ]
  edge [
    source 38
    target 1553
  ]
  edge [
    source 38
    target 1554
  ]
  edge [
    source 38
    target 1555
  ]
  edge [
    source 38
    target 1556
  ]
  edge [
    source 38
    target 411
  ]
  edge [
    source 38
    target 1557
  ]
  edge [
    source 38
    target 1558
  ]
  edge [
    source 38
    target 1559
  ]
  edge [
    source 38
    target 1560
  ]
  edge [
    source 38
    target 1561
  ]
  edge [
    source 38
    target 1562
  ]
  edge [
    source 38
    target 1563
  ]
  edge [
    source 38
    target 1564
  ]
  edge [
    source 38
    target 446
  ]
  edge [
    source 38
    target 1565
  ]
  edge [
    source 38
    target 1566
  ]
  edge [
    source 38
    target 1567
  ]
  edge [
    source 38
    target 1568
  ]
  edge [
    source 38
    target 1569
  ]
  edge [
    source 38
    target 1570
  ]
  edge [
    source 38
    target 1571
  ]
  edge [
    source 38
    target 1572
  ]
  edge [
    source 38
    target 1573
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 1574
  ]
  edge [
    source 39
    target 1575
  ]
  edge [
    source 39
    target 1576
  ]
  edge [
    source 39
    target 1577
  ]
  edge [
    source 39
    target 1578
  ]
  edge [
    source 39
    target 1579
  ]
  edge [
    source 39
    target 1013
  ]
  edge [
    source 39
    target 1580
  ]
  edge [
    source 39
    target 1581
  ]
  edge [
    source 39
    target 1582
  ]
  edge [
    source 39
    target 1583
  ]
  edge [
    source 39
    target 1584
  ]
  edge [
    source 39
    target 1585
  ]
  edge [
    source 39
    target 1586
  ]
  edge [
    source 39
    target 1033
  ]
  edge [
    source 39
    target 1587
  ]
  edge [
    source 39
    target 1353
  ]
  edge [
    source 39
    target 1588
  ]
  edge [
    source 39
    target 1589
  ]
  edge [
    source 39
    target 1590
  ]
  edge [
    source 39
    target 1591
  ]
  edge [
    source 39
    target 1592
  ]
  edge [
    source 39
    target 155
  ]
  edge [
    source 39
    target 1593
  ]
  edge [
    source 39
    target 1594
  ]
  edge [
    source 39
    target 138
  ]
  edge [
    source 39
    target 1595
  ]
  edge [
    source 39
    target 1596
  ]
  edge [
    source 39
    target 1597
  ]
  edge [
    source 39
    target 1598
  ]
  edge [
    source 39
    target 1599
  ]
  edge [
    source 39
    target 1600
  ]
  edge [
    source 39
    target 1601
  ]
  edge [
    source 39
    target 1602
  ]
  edge [
    source 39
    target 782
  ]
  edge [
    source 39
    target 1603
  ]
  edge [
    source 39
    target 1604
  ]
  edge [
    source 39
    target 1605
  ]
  edge [
    source 39
    target 1606
  ]
  edge [
    source 39
    target 1607
  ]
  edge [
    source 39
    target 1608
  ]
  edge [
    source 39
    target 1609
  ]
  edge [
    source 39
    target 1610
  ]
  edge [
    source 39
    target 957
  ]
  edge [
    source 39
    target 1611
  ]
  edge [
    source 39
    target 1612
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 1613
  ]
  edge [
    source 40
    target 1614
  ]
  edge [
    source 40
    target 1615
  ]
  edge [
    source 40
    target 1616
  ]
  edge [
    source 40
    target 1617
  ]
  edge [
    source 40
    target 1618
  ]
  edge [
    source 40
    target 1619
  ]
  edge [
    source 40
    target 1620
  ]
  edge [
    source 40
    target 1621
  ]
  edge [
    source 40
    target 1622
  ]
  edge [
    source 40
    target 665
  ]
  edge [
    source 40
    target 1623
  ]
  edge [
    source 40
    target 1624
  ]
  edge [
    source 40
    target 1625
  ]
  edge [
    source 40
    target 1626
  ]
  edge [
    source 40
    target 1627
  ]
  edge [
    source 40
    target 1628
  ]
  edge [
    source 40
    target 1629
  ]
  edge [
    source 40
    target 1630
  ]
  edge [
    source 40
    target 1631
  ]
  edge [
    source 40
    target 1632
  ]
  edge [
    source 40
    target 1633
  ]
  edge [
    source 40
    target 1634
  ]
  edge [
    source 40
    target 1635
  ]
  edge [
    source 40
    target 358
  ]
  edge [
    source 40
    target 1636
  ]
  edge [
    source 40
    target 1637
  ]
  edge [
    source 40
    target 1638
  ]
  edge [
    source 40
    target 96
  ]
  edge [
    source 40
    target 1639
  ]
  edge [
    source 40
    target 1640
  ]
  edge [
    source 40
    target 1641
  ]
  edge [
    source 40
    target 1642
  ]
  edge [
    source 40
    target 1643
  ]
  edge [
    source 40
    target 1354
  ]
  edge [
    source 40
    target 1644
  ]
  edge [
    source 40
    target 1645
  ]
  edge [
    source 40
    target 1646
  ]
  edge [
    source 40
    target 1647
  ]
  edge [
    source 40
    target 1648
  ]
  edge [
    source 40
    target 1649
  ]
  edge [
    source 40
    target 1650
  ]
  edge [
    source 40
    target 1651
  ]
  edge [
    source 40
    target 1652
  ]
  edge [
    source 40
    target 1653
  ]
  edge [
    source 40
    target 1654
  ]
  edge [
    source 40
    target 1655
  ]
  edge [
    source 40
    target 1656
  ]
  edge [
    source 40
    target 1657
  ]
  edge [
    source 40
    target 1658
  ]
  edge [
    source 40
    target 1659
  ]
  edge [
    source 40
    target 1660
  ]
  edge [
    source 40
    target 1661
  ]
  edge [
    source 40
    target 1662
  ]
  edge [
    source 40
    target 1663
  ]
  edge [
    source 40
    target 1664
  ]
  edge [
    source 40
    target 1665
  ]
  edge [
    source 40
    target 1666
  ]
  edge [
    source 40
    target 1667
  ]
  edge [
    source 40
    target 1098
  ]
  edge [
    source 40
    target 1668
  ]
  edge [
    source 40
    target 1669
  ]
  edge [
    source 40
    target 1670
  ]
  edge [
    source 40
    target 1671
  ]
  edge [
    source 40
    target 1672
  ]
  edge [
    source 40
    target 1673
  ]
  edge [
    source 40
    target 1674
  ]
  edge [
    source 40
    target 1675
  ]
  edge [
    source 40
    target 1676
  ]
  edge [
    source 40
    target 1677
  ]
  edge [
    source 40
    target 1678
  ]
  edge [
    source 40
    target 1679
  ]
  edge [
    source 40
    target 1680
  ]
  edge [
    source 40
    target 1681
  ]
  edge [
    source 40
    target 1682
  ]
  edge [
    source 40
    target 1683
  ]
  edge [
    source 40
    target 1684
  ]
  edge [
    source 40
    target 1685
  ]
  edge [
    source 40
    target 1686
  ]
  edge [
    source 40
    target 1687
  ]
  edge [
    source 40
    target 1688
  ]
  edge [
    source 40
    target 1689
  ]
  edge [
    source 40
    target 1690
  ]
  edge [
    source 40
    target 1691
  ]
  edge [
    source 40
    target 1692
  ]
  edge [
    source 40
    target 1693
  ]
  edge [
    source 40
    target 1694
  ]
  edge [
    source 40
    target 1695
  ]
  edge [
    source 40
    target 1696
  ]
  edge [
    source 40
    target 1697
  ]
  edge [
    source 40
    target 1698
  ]
  edge [
    source 40
    target 1699
  ]
  edge [
    source 40
    target 1700
  ]
  edge [
    source 40
    target 1701
  ]
  edge [
    source 40
    target 1702
  ]
  edge [
    source 40
    target 1703
  ]
  edge [
    source 40
    target 1704
  ]
  edge [
    source 40
    target 1705
  ]
  edge [
    source 40
    target 1706
  ]
  edge [
    source 40
    target 1707
  ]
  edge [
    source 40
    target 1019
  ]
  edge [
    source 40
    target 1708
  ]
  edge [
    source 40
    target 1709
  ]
  edge [
    source 40
    target 1710
  ]
  edge [
    source 40
    target 1711
  ]
  edge [
    source 40
    target 1712
  ]
  edge [
    source 40
    target 1713
  ]
  edge [
    source 40
    target 1714
  ]
  edge [
    source 40
    target 1715
  ]
  edge [
    source 40
    target 1716
  ]
  edge [
    source 40
    target 1717
  ]
  edge [
    source 40
    target 1718
  ]
  edge [
    source 40
    target 1719
  ]
  edge [
    source 40
    target 1720
  ]
  edge [
    source 40
    target 1721
  ]
  edge [
    source 40
    target 1722
  ]
  edge [
    source 40
    target 1723
  ]
  edge [
    source 40
    target 1724
  ]
  edge [
    source 40
    target 1725
  ]
  edge [
    source 40
    target 1726
  ]
  edge [
    source 40
    target 1727
  ]
  edge [
    source 40
    target 1008
  ]
  edge [
    source 40
    target 1728
  ]
  edge [
    source 40
    target 1729
  ]
  edge [
    source 40
    target 1730
  ]
  edge [
    source 40
    target 1731
  ]
  edge [
    source 40
    target 1732
  ]
  edge [
    source 40
    target 1733
  ]
  edge [
    source 40
    target 1734
  ]
  edge [
    source 40
    target 1735
  ]
  edge [
    source 40
    target 1736
  ]
  edge [
    source 40
    target 1737
  ]
  edge [
    source 40
    target 1738
  ]
  edge [
    source 40
    target 1739
  ]
  edge [
    source 40
    target 1740
  ]
  edge [
    source 40
    target 1741
  ]
  edge [
    source 40
    target 1742
  ]
  edge [
    source 40
    target 367
  ]
  edge [
    source 40
    target 1743
  ]
  edge [
    source 40
    target 467
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 1744
  ]
  edge [
    source 41
    target 1745
  ]
  edge [
    source 41
    target 1746
  ]
  edge [
    source 41
    target 631
  ]
  edge [
    source 41
    target 1430
  ]
  edge [
    source 41
    target 1747
  ]
  edge [
    source 41
    target 1748
  ]
  edge [
    source 41
    target 1749
  ]
  edge [
    source 41
    target 1750
  ]
  edge [
    source 41
    target 187
  ]
  edge [
    source 41
    target 1425
  ]
  edge [
    source 41
    target 1426
  ]
  edge [
    source 41
    target 190
  ]
  edge [
    source 41
    target 1427
  ]
  edge [
    source 41
    target 1428
  ]
  edge [
    source 41
    target 1429
  ]
  edge [
    source 41
    target 1431
  ]
  edge [
    source 41
    target 1432
  ]
  edge [
    source 41
    target 1751
  ]
  edge [
    source 41
    target 1752
  ]
  edge [
    source 41
    target 1753
  ]
  edge [
    source 41
    target 1754
  ]
  edge [
    source 41
    target 1755
  ]
  edge [
    source 41
    target 1756
  ]
  edge [
    source 41
    target 1757
  ]
  edge [
    source 41
    target 1758
  ]
  edge [
    source 41
    target 1759
  ]
  edge [
    source 41
    target 1760
  ]
  edge [
    source 41
    target 1761
  ]
  edge [
    source 41
    target 1762
  ]
  edge [
    source 41
    target 1763
  ]
  edge [
    source 41
    target 1295
  ]
  edge [
    source 41
    target 1764
  ]
  edge [
    source 41
    target 1765
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1766
  ]
  edge [
    source 42
    target 1767
  ]
  edge [
    source 42
    target 1768
  ]
  edge [
    source 42
    target 240
  ]
  edge [
    source 42
    target 1769
  ]
  edge [
    source 42
    target 1770
  ]
  edge [
    source 42
    target 1771
  ]
  edge [
    source 42
    target 1772
  ]
  edge [
    source 42
    target 1773
  ]
  edge [
    source 42
    target 1774
  ]
  edge [
    source 42
    target 1775
  ]
  edge [
    source 42
    target 1776
  ]
  edge [
    source 42
    target 1777
  ]
  edge [
    source 42
    target 1778
  ]
  edge [
    source 42
    target 246
  ]
  edge [
    source 42
    target 1779
  ]
  edge [
    source 42
    target 1780
  ]
  edge [
    source 42
    target 1781
  ]
  edge [
    source 42
    target 1782
  ]
  edge [
    source 42
    target 1783
  ]
  edge [
    source 42
    target 1784
  ]
  edge [
    source 42
    target 764
  ]
  edge [
    source 42
    target 1785
  ]
  edge [
    source 42
    target 462
  ]
  edge [
    source 42
    target 1786
  ]
  edge [
    source 42
    target 498
  ]
  edge [
    source 42
    target 1787
  ]
  edge [
    source 42
    target 1788
  ]
  edge [
    source 42
    target 1789
  ]
  edge [
    source 42
    target 1790
  ]
  edge [
    source 42
    target 1791
  ]
  edge [
    source 42
    target 1792
  ]
  edge [
    source 42
    target 1793
  ]
  edge [
    source 42
    target 1794
  ]
  edge [
    source 42
    target 1795
  ]
  edge [
    source 42
    target 1796
  ]
  edge [
    source 42
    target 1797
  ]
  edge [
    source 42
    target 1296
  ]
  edge [
    source 42
    target 1798
  ]
  edge [
    source 42
    target 668
  ]
  edge [
    source 42
    target 236
  ]
  edge [
    source 42
    target 1799
  ]
  edge [
    source 42
    target 1800
  ]
  edge [
    source 42
    target 1801
  ]
  edge [
    source 42
    target 1802
  ]
  edge [
    source 42
    target 1803
  ]
  edge [
    source 42
    target 1804
  ]
  edge [
    source 42
    target 1805
  ]
  edge [
    source 42
    target 1806
  ]
  edge [
    source 42
    target 1807
  ]
  edge [
    source 42
    target 1808
  ]
  edge [
    source 42
    target 1809
  ]
  edge [
    source 42
    target 1810
  ]
  edge [
    source 42
    target 1811
  ]
  edge [
    source 42
    target 1812
  ]
  edge [
    source 42
    target 1813
  ]
  edge [
    source 42
    target 1814
  ]
  edge [
    source 42
    target 1815
  ]
  edge [
    source 42
    target 1816
  ]
  edge [
    source 42
    target 1817
  ]
  edge [
    source 42
    target 1544
  ]
  edge [
    source 42
    target 1818
  ]
  edge [
    source 42
    target 1819
  ]
  edge [
    source 42
    target 1820
  ]
  edge [
    source 42
    target 1821
  ]
  edge [
    source 42
    target 1822
  ]
  edge [
    source 42
    target 1823
  ]
  edge [
    source 42
    target 1824
  ]
  edge [
    source 42
    target 1356
  ]
  edge [
    source 42
    target 701
  ]
  edge [
    source 42
    target 702
  ]
  edge [
    source 42
    target 703
  ]
  edge [
    source 42
    target 482
  ]
  edge [
    source 42
    target 704
  ]
  edge [
    source 42
    target 705
  ]
  edge [
    source 42
    target 706
  ]
  edge [
    source 42
    target 707
  ]
  edge [
    source 42
    target 367
  ]
  edge [
    source 42
    target 708
  ]
  edge [
    source 42
    target 709
  ]
  edge [
    source 42
    target 710
  ]
  edge [
    source 42
    target 266
  ]
  edge [
    source 42
    target 711
  ]
  edge [
    source 42
    target 1825
  ]
  edge [
    source 42
    target 1826
  ]
  edge [
    source 42
    target 1827
  ]
  edge [
    source 42
    target 1828
  ]
  edge [
    source 42
    target 1829
  ]
  edge [
    source 42
    target 1830
  ]
  edge [
    source 42
    target 1831
  ]
  edge [
    source 42
    target 1832
  ]
  edge [
    source 42
    target 1833
  ]
  edge [
    source 42
    target 1834
  ]
  edge [
    source 42
    target 1835
  ]
  edge [
    source 42
    target 1836
  ]
  edge [
    source 42
    target 1837
  ]
  edge [
    source 42
    target 1838
  ]
  edge [
    source 42
    target 1839
  ]
  edge [
    source 42
    target 1840
  ]
  edge [
    source 42
    target 1841
  ]
  edge [
    source 42
    target 1842
  ]
  edge [
    source 42
    target 1311
  ]
  edge [
    source 42
    target 1843
  ]
  edge [
    source 42
    target 1844
  ]
  edge [
    source 42
    target 1845
  ]
  edge [
    source 42
    target 1846
  ]
  edge [
    source 42
    target 1847
  ]
  edge [
    source 42
    target 1848
  ]
  edge [
    source 42
    target 1849
  ]
  edge [
    source 42
    target 1850
  ]
  edge [
    source 42
    target 1851
  ]
  edge [
    source 42
    target 308
  ]
  edge [
    source 42
    target 1852
  ]
  edge [
    source 42
    target 152
  ]
  edge [
    source 42
    target 1853
  ]
  edge [
    source 42
    target 849
  ]
  edge [
    source 42
    target 1009
  ]
  edge [
    source 42
    target 1854
  ]
  edge [
    source 42
    target 1855
  ]
  edge [
    source 42
    target 1856
  ]
  edge [
    source 42
    target 1857
  ]
  edge [
    source 42
    target 146
  ]
  edge [
    source 42
    target 1012
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1858
  ]
  edge [
    source 43
    target 1859
  ]
  edge [
    source 43
    target 1860
  ]
  edge [
    source 43
    target 1861
  ]
  edge [
    source 43
    target 1862
  ]
  edge [
    source 43
    target 1863
  ]
  edge [
    source 43
    target 520
  ]
  edge [
    source 43
    target 536
  ]
  edge [
    source 43
    target 1864
  ]
  edge [
    source 43
    target 1865
  ]
  edge [
    source 44
    target 1866
  ]
  edge [
    source 44
    target 1867
  ]
  edge [
    source 44
    target 1868
  ]
  edge [
    source 44
    target 1869
  ]
  edge [
    source 44
    target 1870
  ]
  edge [
    source 44
    target 1871
  ]
  edge [
    source 44
    target 1872
  ]
  edge [
    source 44
    target 1873
  ]
  edge [
    source 44
    target 1874
  ]
  edge [
    source 44
    target 1875
  ]
  edge [
    source 44
    target 1876
  ]
  edge [
    source 44
    target 1877
  ]
  edge [
    source 44
    target 1878
  ]
  edge [
    source 44
    target 1879
  ]
  edge [
    source 44
    target 1880
  ]
  edge [
    source 44
    target 1881
  ]
  edge [
    source 44
    target 1882
  ]
  edge [
    source 44
    target 1883
  ]
  edge [
    source 44
    target 1884
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 1885
  ]
  edge [
    source 46
    target 1886
  ]
  edge [
    source 46
    target 266
  ]
  edge [
    source 46
    target 316
  ]
  edge [
    source 46
    target 317
  ]
  edge [
    source 46
    target 318
  ]
  edge [
    source 46
    target 163
  ]
  edge [
    source 46
    target 319
  ]
  edge [
    source 46
    target 320
  ]
  edge [
    source 46
    target 321
  ]
  edge [
    source 46
    target 322
  ]
  edge [
    source 46
    target 323
  ]
  edge [
    source 46
    target 1887
  ]
  edge [
    source 46
    target 1888
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 1889
  ]
  edge [
    source 47
    target 1890
  ]
  edge [
    source 47
    target 232
  ]
  edge [
    source 47
    target 1891
  ]
  edge [
    source 47
    target 348
  ]
  edge [
    source 47
    target 829
  ]
  edge [
    source 47
    target 696
  ]
  edge [
    source 47
    target 1892
  ]
  edge [
    source 47
    target 1893
  ]
  edge [
    source 47
    target 1894
  ]
  edge [
    source 47
    target 1895
  ]
  edge [
    source 47
    target 1896
  ]
  edge [
    source 47
    target 1897
  ]
  edge [
    source 47
    target 1898
  ]
  edge [
    source 47
    target 1899
  ]
  edge [
    source 47
    target 1900
  ]
  edge [
    source 47
    target 1901
  ]
  edge [
    source 47
    target 1902
  ]
  edge [
    source 47
    target 1903
  ]
  edge [
    source 47
    target 1904
  ]
  edge [
    source 47
    target 651
  ]
  edge [
    source 47
    target 250
  ]
  edge [
    source 47
    target 240
  ]
  edge [
    source 47
    target 123
  ]
  edge [
    source 47
    target 679
  ]
  edge [
    source 47
    target 693
  ]
  edge [
    source 47
    target 653
  ]
  edge [
    source 47
    target 1905
  ]
  edge [
    source 47
    target 1906
  ]
  edge [
    source 47
    target 1907
  ]
  edge [
    source 47
    target 1908
  ]
  edge [
    source 47
    target 1909
  ]
  edge [
    source 47
    target 1910
  ]
  edge [
    source 47
    target 1911
  ]
  edge [
    source 47
    target 1912
  ]
  edge [
    source 47
    target 1913
  ]
  edge [
    source 47
    target 1914
  ]
  edge [
    source 47
    target 1915
  ]
  edge [
    source 47
    target 1916
  ]
  edge [
    source 47
    target 1917
  ]
  edge [
    source 47
    target 1918
  ]
  edge [
    source 47
    target 1919
  ]
  edge [
    source 47
    target 1566
  ]
  edge [
    source 47
    target 1920
  ]
  edge [
    source 47
    target 1921
  ]
  edge [
    source 47
    target 941
  ]
  edge [
    source 47
    target 1922
  ]
  edge [
    source 47
    target 442
  ]
  edge [
    source 47
    target 657
  ]
  edge [
    source 47
    target 1923
  ]
  edge [
    source 47
    target 1924
  ]
  edge [
    source 47
    target 1867
  ]
  edge [
    source 47
    target 1925
  ]
  edge [
    source 47
    target 1926
  ]
  edge [
    source 47
    target 1927
  ]
  edge [
    source 47
    target 1928
  ]
  edge [
    source 47
    target 1027
  ]
  edge [
    source 47
    target 1929
  ]
  edge [
    source 47
    target 155
  ]
  edge [
    source 47
    target 1930
  ]
  edge [
    source 47
    target 685
  ]
  edge [
    source 47
    target 1931
  ]
  edge [
    source 47
    target 1932
  ]
  edge [
    source 47
    target 1593
  ]
  edge [
    source 47
    target 1592
  ]
  edge [
    source 47
    target 1041
  ]
  edge [
    source 47
    target 1933
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 1934
  ]
  edge [
    source 48
    target 1935
  ]
  edge [
    source 48
    target 1936
  ]
  edge [
    source 48
    target 1937
  ]
  edge [
    source 48
    target 1938
  ]
  edge [
    source 48
    target 1939
  ]
  edge [
    source 48
    target 1940
  ]
  edge [
    source 48
    target 1941
  ]
  edge [
    source 48
    target 1877
  ]
  edge [
    source 48
    target 69
  ]
  edge [
    source 48
    target 70
  ]
  edge [
    source 48
    target 71
  ]
  edge [
    source 48
    target 72
  ]
  edge [
    source 48
    target 73
  ]
  edge [
    source 48
    target 74
  ]
  edge [
    source 48
    target 75
  ]
  edge [
    source 48
    target 76
  ]
  edge [
    source 48
    target 77
  ]
  edge [
    source 48
    target 78
  ]
  edge [
    source 48
    target 79
  ]
  edge [
    source 48
    target 1942
  ]
  edge [
    source 48
    target 1943
  ]
  edge [
    source 48
    target 1944
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 1945
  ]
  edge [
    source 49
    target 1946
  ]
  edge [
    source 49
    target 1947
  ]
  edge [
    source 49
    target 237
  ]
  edge [
    source 49
    target 1948
  ]
  edge [
    source 49
    target 61
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 1949
  ]
  edge [
    source 50
    target 1837
  ]
  edge [
    source 50
    target 1950
  ]
  edge [
    source 50
    target 1951
  ]
  edge [
    source 50
    target 1952
  ]
  edge [
    source 50
    target 1953
  ]
  edge [
    source 50
    target 1954
  ]
  edge [
    source 50
    target 1955
  ]
  edge [
    source 50
    target 1956
  ]
  edge [
    source 50
    target 1957
  ]
  edge [
    source 50
    target 1364
  ]
  edge [
    source 50
    target 1958
  ]
  edge [
    source 50
    target 1356
  ]
  edge [
    source 50
    target 1959
  ]
  edge [
    source 50
    target 1960
  ]
  edge [
    source 50
    target 1961
  ]
  edge [
    source 50
    target 1566
  ]
  edge [
    source 50
    target 1962
  ]
  edge [
    source 50
    target 1963
  ]
  edge [
    source 50
    target 246
  ]
  edge [
    source 50
    target 1843
  ]
  edge [
    source 50
    target 471
  ]
  edge [
    source 50
    target 1964
  ]
  edge [
    source 50
    target 1965
  ]
  edge [
    source 50
    target 1966
  ]
  edge [
    source 50
    target 1967
  ]
  edge [
    source 50
    target 1968
  ]
  edge [
    source 50
    target 1969
  ]
  edge [
    source 50
    target 1970
  ]
  edge [
    source 50
    target 1971
  ]
  edge [
    source 50
    target 1972
  ]
  edge [
    source 50
    target 700
  ]
  edge [
    source 50
    target 1973
  ]
  edge [
    source 50
    target 1974
  ]
  edge [
    source 50
    target 1975
  ]
  edge [
    source 50
    target 1898
  ]
  edge [
    source 50
    target 1976
  ]
  edge [
    source 50
    target 1977
  ]
  edge [
    source 50
    target 1978
  ]
  edge [
    source 50
    target 1979
  ]
  edge [
    source 50
    target 1980
  ]
  edge [
    source 50
    target 1070
  ]
  edge [
    source 50
    target 1468
  ]
  edge [
    source 50
    target 1981
  ]
  edge [
    source 50
    target 1982
  ]
  edge [
    source 50
    target 1983
  ]
  edge [
    source 50
    target 1984
  ]
  edge [
    source 50
    target 1985
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 1986
  ]
  edge [
    source 51
    target 1987
  ]
  edge [
    source 51
    target 1988
  ]
  edge [
    source 51
    target 187
  ]
  edge [
    source 51
    target 1989
  ]
  edge [
    source 51
    target 175
  ]
  edge [
    source 51
    target 1990
  ]
  edge [
    source 51
    target 1991
  ]
  edge [
    source 51
    target 1992
  ]
  edge [
    source 51
    target 1750
  ]
  edge [
    source 51
    target 1993
  ]
  edge [
    source 51
    target 1994
  ]
  edge [
    source 51
    target 1995
  ]
  edge [
    source 51
    target 1996
  ]
  edge [
    source 51
    target 1451
  ]
  edge [
    source 51
    target 1997
  ]
  edge [
    source 51
    target 1998
  ]
  edge [
    source 51
    target 996
  ]
  edge [
    source 51
    target 1999
  ]
  edge [
    source 51
    target 2000
  ]
  edge [
    source 51
    target 2001
  ]
  edge [
    source 51
    target 2002
  ]
  edge [
    source 51
    target 2003
  ]
  edge [
    source 51
    target 2004
  ]
  edge [
    source 51
    target 2005
  ]
  edge [
    source 51
    target 2006
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 81
  ]
  edge [
    source 52
    target 2007
  ]
  edge [
    source 52
    target 2008
  ]
  edge [
    source 52
    target 1877
  ]
  edge [
    source 52
    target 2009
  ]
  edge [
    source 52
    target 2010
  ]
  edge [
    source 52
    target 2011
  ]
  edge [
    source 52
    target 2012
  ]
  edge [
    source 52
    target 2013
  ]
  edge [
    source 52
    target 2014
  ]
  edge [
    source 52
    target 968
  ]
  edge [
    source 52
    target 2015
  ]
  edge [
    source 52
    target 2016
  ]
  edge [
    source 52
    target 2017
  ]
  edge [
    source 52
    target 2018
  ]
  edge [
    source 52
    target 2019
  ]
  edge [
    source 52
    target 1356
  ]
  edge [
    source 52
    target 2020
  ]
  edge [
    source 52
    target 2021
  ]
  edge [
    source 52
    target 2022
  ]
  edge [
    source 52
    target 2023
  ]
  edge [
    source 52
    target 2024
  ]
  edge [
    source 52
    target 2025
  ]
  edge [
    source 52
    target 1923
  ]
  edge [
    source 52
    target 2026
  ]
  edge [
    source 52
    target 2027
  ]
  edge [
    source 52
    target 2028
  ]
  edge [
    source 52
    target 2029
  ]
  edge [
    source 52
    target 2030
  ]
  edge [
    source 52
    target 70
  ]
  edge [
    source 52
    target 2031
  ]
  edge [
    source 52
    target 2032
  ]
  edge [
    source 52
    target 1939
  ]
  edge [
    source 52
    target 2033
  ]
  edge [
    source 52
    target 2034
  ]
  edge [
    source 52
    target 2035
  ]
  edge [
    source 52
    target 2036
  ]
  edge [
    source 53
    target 2037
  ]
  edge [
    source 53
    target 1027
  ]
  edge [
    source 53
    target 442
  ]
  edge [
    source 53
    target 2038
  ]
  edge [
    source 53
    target 2039
  ]
  edge [
    source 53
    target 982
  ]
  edge [
    source 53
    target 2040
  ]
  edge [
    source 53
    target 2041
  ]
  edge [
    source 53
    target 2042
  ]
  edge [
    source 53
    target 367
  ]
  edge [
    source 53
    target 2043
  ]
  edge [
    source 53
    target 2044
  ]
  edge [
    source 53
    target 2045
  ]
  edge [
    source 53
    target 880
  ]
  edge [
    source 53
    target 266
  ]
  edge [
    source 53
    target 2046
  ]
  edge [
    source 53
    target 443
  ]
  edge [
    source 53
    target 1888
  ]
  edge [
    source 53
    target 1594
  ]
  edge [
    source 53
    target 2047
  ]
  edge [
    source 53
    target 1927
  ]
  edge [
    source 53
    target 2048
  ]
  edge [
    source 53
    target 2049
  ]
  edge [
    source 53
    target 842
  ]
  edge [
    source 53
    target 163
  ]
  edge [
    source 53
    target 2050
  ]
  edge [
    source 53
    target 410
  ]
  edge [
    source 53
    target 693
  ]
  edge [
    source 53
    target 694
  ]
  edge [
    source 53
    target 695
  ]
  edge [
    source 53
    target 696
  ]
  edge [
    source 53
    target 697
  ]
  edge [
    source 53
    target 698
  ]
  edge [
    source 53
    target 582
  ]
  edge [
    source 53
    target 699
  ]
  edge [
    source 53
    target 608
  ]
  edge [
    source 53
    target 700
  ]
  edge [
    source 53
    target 733
  ]
  edge [
    source 53
    target 2051
  ]
  edge [
    source 53
    target 770
  ]
  edge [
    source 53
    target 2052
  ]
  edge [
    source 53
    target 2053
  ]
  edge [
    source 53
    target 2054
  ]
  edge [
    source 53
    target 2055
  ]
  edge [
    source 53
    target 1031
  ]
  edge [
    source 53
    target 813
  ]
  edge [
    source 53
    target 510
  ]
  edge [
    source 53
    target 1930
  ]
  edge [
    source 53
    target 438
  ]
  edge [
    source 53
    target 511
  ]
  edge [
    source 53
    target 2056
  ]
  edge [
    source 53
    target 508
  ]
  edge [
    source 53
    target 513
  ]
  edge [
    source 53
    target 1045
  ]
  edge [
    source 53
    target 402
  ]
  edge [
    source 53
    target 514
  ]
  edge [
    source 53
    target 2057
  ]
  edge [
    source 53
    target 280
  ]
  edge [
    source 53
    target 507
  ]
  edge [
    source 53
    target 509
  ]
  edge [
    source 53
    target 1593
  ]
  edge [
    source 53
    target 432
  ]
  edge [
    source 53
    target 440
  ]
  edge [
    source 53
    target 515
  ]
  edge [
    source 53
    target 316
  ]
  edge [
    source 53
    target 317
  ]
  edge [
    source 53
    target 318
  ]
  edge [
    source 53
    target 319
  ]
  edge [
    source 53
    target 320
  ]
  edge [
    source 53
    target 321
  ]
  edge [
    source 53
    target 322
  ]
  edge [
    source 53
    target 323
  ]
  edge [
    source 53
    target 455
  ]
  edge [
    source 53
    target 456
  ]
  edge [
    source 53
    target 457
  ]
  edge [
    source 53
    target 458
  ]
  edge [
    source 53
    target 459
  ]
  edge [
    source 53
    target 460
  ]
  edge [
    source 53
    target 461
  ]
  edge [
    source 53
    target 462
  ]
  edge [
    source 53
    target 463
  ]
  edge [
    source 53
    target 464
  ]
  edge [
    source 53
    target 115
  ]
  edge [
    source 53
    target 465
  ]
  edge [
    source 53
    target 466
  ]
  edge [
    source 53
    target 467
  ]
  edge [
    source 53
    target 468
  ]
  edge [
    source 53
    target 469
  ]
  edge [
    source 53
    target 470
  ]
  edge [
    source 53
    target 471
  ]
  edge [
    source 53
    target 453
  ]
  edge [
    source 53
    target 447
  ]
  edge [
    source 53
    target 445
  ]
  edge [
    source 53
    target 472
  ]
  edge [
    source 53
    target 473
  ]
  edge [
    source 53
    target 2058
  ]
  edge [
    source 53
    target 2059
  ]
  edge [
    source 53
    target 2060
  ]
  edge [
    source 53
    target 2061
  ]
  edge [
    source 53
    target 2062
  ]
  edge [
    source 53
    target 2063
  ]
  edge [
    source 53
    target 2064
  ]
  edge [
    source 53
    target 2065
  ]
  edge [
    source 53
    target 291
  ]
  edge [
    source 53
    target 2066
  ]
  edge [
    source 53
    target 2067
  ]
  edge [
    source 53
    target 1014
  ]
  edge [
    source 53
    target 1015
  ]
  edge [
    source 53
    target 981
  ]
  edge [
    source 53
    target 1016
  ]
  edge [
    source 53
    target 1017
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 2068
  ]
  edge [
    source 55
    target 2069
  ]
  edge [
    source 55
    target 2070
  ]
  edge [
    source 55
    target 2071
  ]
  edge [
    source 55
    target 1436
  ]
  edge [
    source 55
    target 1297
  ]
  edge [
    source 55
    target 631
  ]
  edge [
    source 55
    target 630
  ]
  edge [
    source 55
    target 2072
  ]
  edge [
    source 55
    target 2073
  ]
  edge [
    source 55
    target 625
  ]
  edge [
    source 55
    target 1449
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 2074
  ]
  edge [
    source 56
    target 2075
  ]
  edge [
    source 56
    target 2076
  ]
  edge [
    source 56
    target 2077
  ]
  edge [
    source 56
    target 2078
  ]
  edge [
    source 56
    target 2079
  ]
  edge [
    source 56
    target 1476
  ]
  edge [
    source 56
    target 2080
  ]
  edge [
    source 56
    target 2081
  ]
  edge [
    source 56
    target 190
  ]
  edge [
    source 56
    target 733
  ]
  edge [
    source 56
    target 2082
  ]
  edge [
    source 56
    target 2083
  ]
  edge [
    source 56
    target 1437
  ]
  edge [
    source 56
    target 2084
  ]
  edge [
    source 56
    target 2085
  ]
  edge [
    source 56
    target 2086
  ]
  edge [
    source 56
    target 2087
  ]
  edge [
    source 56
    target 2088
  ]
  edge [
    source 56
    target 631
  ]
  edge [
    source 56
    target 2089
  ]
  edge [
    source 56
    target 2090
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 2091
  ]
  edge [
    source 57
    target 2092
  ]
  edge [
    source 57
    target 2093
  ]
  edge [
    source 57
    target 2094
  ]
  edge [
    source 57
    target 2095
  ]
  edge [
    source 57
    target 2096
  ]
  edge [
    source 57
    target 2097
  ]
  edge [
    source 57
    target 2098
  ]
  edge [
    source 57
    target 1430
  ]
  edge [
    source 57
    target 2099
  ]
  edge [
    source 57
    target 2100
  ]
  edge [
    source 57
    target 2101
  ]
  edge [
    source 57
    target 2102
  ]
  edge [
    source 57
    target 2103
  ]
  edge [
    source 57
    target 2104
  ]
  edge [
    source 57
    target 2105
  ]
  edge [
    source 57
    target 2106
  ]
  edge [
    source 57
    target 2107
  ]
  edge [
    source 57
    target 2108
  ]
  edge [
    source 57
    target 2004
  ]
  edge [
    source 57
    target 2109
  ]
  edge [
    source 57
    target 2110
  ]
  edge [
    source 57
    target 2111
  ]
  edge [
    source 57
    target 2112
  ]
  edge [
    source 57
    target 2113
  ]
  edge [
    source 57
    target 1748
  ]
  edge [
    source 57
    target 1749
  ]
  edge [
    source 57
    target 1750
  ]
  edge [
    source 57
    target 187
  ]
  edge [
    source 57
    target 2114
  ]
  edge [
    source 57
    target 2115
  ]
  edge [
    source 57
    target 2116
  ]
  edge [
    source 57
    target 2117
  ]
  edge [
    source 57
    target 2118
  ]
  edge [
    source 57
    target 2119
  ]
  edge [
    source 57
    target 2120
  ]
  edge [
    source 57
    target 2121
  ]
  edge [
    source 57
    target 733
  ]
  edge [
    source 57
    target 2122
  ]
  edge [
    source 57
    target 2123
  ]
  edge [
    source 57
    target 2124
  ]
  edge [
    source 57
    target 2125
  ]
  edge [
    source 57
    target 2126
  ]
  edge [
    source 57
    target 2127
  ]
  edge [
    source 57
    target 2128
  ]
  edge [
    source 57
    target 2129
  ]
  edge [
    source 57
    target 2130
  ]
  edge [
    source 57
    target 2131
  ]
  edge [
    source 57
    target 2132
  ]
  edge [
    source 57
    target 2133
  ]
  edge [
    source 57
    target 2134
  ]
  edge [
    source 57
    target 1166
  ]
  edge [
    source 57
    target 2135
  ]
  edge [
    source 57
    target 2136
  ]
  edge [
    source 57
    target 2137
  ]
  edge [
    source 57
    target 2138
  ]
  edge [
    source 57
    target 2139
  ]
  edge [
    source 57
    target 2140
  ]
  edge [
    source 57
    target 2141
  ]
  edge [
    source 57
    target 2142
  ]
  edge [
    source 57
    target 2143
  ]
  edge [
    source 57
    target 1429
  ]
  edge [
    source 57
    target 2144
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 1945
  ]
  edge [
    source 61
    target 1946
  ]
  edge [
    source 61
    target 1947
  ]
  edge [
    source 61
    target 237
  ]
  edge [
    source 61
    target 1948
  ]
  edge [
    source 62
    target 2145
  ]
  edge [
    source 62
    target 2146
  ]
  edge [
    source 62
    target 2147
  ]
  edge [
    source 62
    target 2148
  ]
  edge [
    source 62
    target 1837
  ]
  edge [
    source 62
    target 493
  ]
  edge [
    source 62
    target 2149
  ]
  edge [
    source 62
    target 1356
  ]
  edge [
    source 62
    target 2150
  ]
  edge [
    source 62
    target 2151
  ]
  edge [
    source 62
    target 2152
  ]
  edge [
    source 62
    target 2153
  ]
  edge [
    source 62
    target 2154
  ]
  edge [
    source 62
    target 2155
  ]
  edge [
    source 62
    target 2156
  ]
  edge [
    source 62
    target 2157
  ]
  edge [
    source 62
    target 2158
  ]
  edge [
    source 62
    target 2159
  ]
  edge [
    source 62
    target 2160
  ]
  edge [
    source 62
    target 696
  ]
  edge [
    source 62
    target 146
  ]
  edge [
    source 62
    target 2161
  ]
  edge [
    source 62
    target 1202
  ]
  edge [
    source 62
    target 2162
  ]
  edge [
    source 62
    target 2163
  ]
  edge [
    source 62
    target 246
  ]
  edge [
    source 62
    target 2164
  ]
  edge [
    source 62
    target 277
  ]
  edge [
    source 62
    target 2165
  ]
  edge [
    source 62
    target 2166
  ]
  edge [
    source 62
    target 850
  ]
  edge [
    source 62
    target 2167
  ]
  edge [
    source 62
    target 2168
  ]
  edge [
    source 62
    target 672
  ]
  edge [
    source 62
    target 1978
  ]
  edge [
    source 62
    target 447
  ]
  edge [
    source 62
    target 2169
  ]
  edge [
    source 62
    target 1969
  ]
  edge [
    source 62
    target 1970
  ]
  edge [
    source 62
    target 1971
  ]
  edge [
    source 62
    target 1972
  ]
  edge [
    source 62
    target 700
  ]
  edge [
    source 62
    target 1973
  ]
  edge [
    source 62
    target 1974
  ]
  edge [
    source 62
    target 1975
  ]
  edge [
    source 62
    target 1898
  ]
  edge [
    source 62
    target 1976
  ]
  edge [
    source 62
    target 1977
  ]
  edge [
    source 62
    target 1979
  ]
  edge [
    source 62
    target 1980
  ]
  edge [
    source 62
    target 1070
  ]
  edge [
    source 62
    target 1468
  ]
  edge [
    source 62
    target 1981
  ]
  edge [
    source 62
    target 1982
  ]
  edge [
    source 62
    target 1983
  ]
  edge [
    source 62
    target 1984
  ]
  edge [
    source 62
    target 1985
  ]
  edge [
    source 62
    target 2170
  ]
  edge [
    source 62
    target 2028
  ]
  edge [
    source 62
    target 913
  ]
  edge [
    source 62
    target 914
  ]
  edge [
    source 62
    target 915
  ]
  edge [
    source 62
    target 916
  ]
  edge [
    source 62
    target 917
  ]
  edge [
    source 62
    target 918
  ]
  edge [
    source 62
    target 919
  ]
  edge [
    source 62
    target 266
  ]
  edge [
    source 62
    target 2171
  ]
  edge [
    source 62
    target 2172
  ]
  edge [
    source 62
    target 2173
  ]
]
