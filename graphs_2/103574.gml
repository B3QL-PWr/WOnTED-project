graph [
  node [
    id 0
    label "mie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "nadzieja"
    origin "text"
  ]
  node [
    id 2
    label "nasze"
    origin "text"
  ]
  node [
    id 3
    label "dziecko"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "chrzci&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pierwsza"
    origin "text"
  ]
  node [
    id 7
    label "komunia"
    origin "text"
  ]
  node [
    id 8
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 11
    label "gdybyby&#263;"
    origin "text"
  ]
  node [
    id 12
    label "mieszka&#263;"
    origin "text"
  ]
  node [
    id 13
    label "polska"
    origin "text"
  ]
  node [
    id 14
    label "wiedzie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 15
    label "jaki"
    origin "text"
  ]
  node [
    id 16
    label "lekarz"
    origin "text"
  ]
  node [
    id 17
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 18
    label "wszystko"
    origin "text"
  ]
  node [
    id 19
    label "trudny"
    origin "text"
  ]
  node [
    id 20
    label "ogarn&#261;&#263;"
    origin "text"
  ]
  node [
    id 21
    label "tak"
    origin "text"
  ]
  node [
    id 22
    label "blisko"
    origin "text"
  ]
  node [
    id 23
    label "podobny"
    origin "text"
  ]
  node [
    id 24
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 25
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 26
    label "zupe&#322;nie"
    origin "text"
  ]
  node [
    id 27
    label "inny"
    origin "text"
  ]
  node [
    id 28
    label "szansa"
  ]
  node [
    id 29
    label "spoczywa&#263;"
  ]
  node [
    id 30
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 31
    label "oczekiwanie"
  ]
  node [
    id 32
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 33
    label "wierzy&#263;"
  ]
  node [
    id 34
    label "posiada&#263;"
  ]
  node [
    id 35
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 36
    label "wydarzenie"
  ]
  node [
    id 37
    label "egzekutywa"
  ]
  node [
    id 38
    label "potencja&#322;"
  ]
  node [
    id 39
    label "wyb&#243;r"
  ]
  node [
    id 40
    label "prospect"
  ]
  node [
    id 41
    label "ability"
  ]
  node [
    id 42
    label "obliczeniowo"
  ]
  node [
    id 43
    label "alternatywa"
  ]
  node [
    id 44
    label "cecha"
  ]
  node [
    id 45
    label "operator_modalny"
  ]
  node [
    id 46
    label "wytrzymanie"
  ]
  node [
    id 47
    label "czekanie"
  ]
  node [
    id 48
    label "spodziewanie_si&#281;"
  ]
  node [
    id 49
    label "anticipation"
  ]
  node [
    id 50
    label "przewidywanie"
  ]
  node [
    id 51
    label "wytrzymywanie"
  ]
  node [
    id 52
    label "spotykanie"
  ]
  node [
    id 53
    label "wait"
  ]
  node [
    id 54
    label "wierza&#263;"
  ]
  node [
    id 55
    label "trust"
  ]
  node [
    id 56
    label "powierzy&#263;"
  ]
  node [
    id 57
    label "wyznawa&#263;"
  ]
  node [
    id 58
    label "czu&#263;"
  ]
  node [
    id 59
    label "faith"
  ]
  node [
    id 60
    label "chowa&#263;"
  ]
  node [
    id 61
    label "powierza&#263;"
  ]
  node [
    id 62
    label "uznawa&#263;"
  ]
  node [
    id 63
    label "lie"
  ]
  node [
    id 64
    label "odpoczywa&#263;"
  ]
  node [
    id 65
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 66
    label "gr&#243;b"
  ]
  node [
    id 67
    label "utulenie"
  ]
  node [
    id 68
    label "pediatra"
  ]
  node [
    id 69
    label "dzieciak"
  ]
  node [
    id 70
    label "utulanie"
  ]
  node [
    id 71
    label "dzieciarnia"
  ]
  node [
    id 72
    label "niepe&#322;noletni"
  ]
  node [
    id 73
    label "organizm"
  ]
  node [
    id 74
    label "utula&#263;"
  ]
  node [
    id 75
    label "cz&#322;owieczek"
  ]
  node [
    id 76
    label "fledgling"
  ]
  node [
    id 77
    label "zwierz&#281;"
  ]
  node [
    id 78
    label "utuli&#263;"
  ]
  node [
    id 79
    label "m&#322;odzik"
  ]
  node [
    id 80
    label "pedofil"
  ]
  node [
    id 81
    label "m&#322;odziak"
  ]
  node [
    id 82
    label "potomek"
  ]
  node [
    id 83
    label "entliczek-pentliczek"
  ]
  node [
    id 84
    label "potomstwo"
  ]
  node [
    id 85
    label "sraluch"
  ]
  node [
    id 86
    label "zbi&#243;r"
  ]
  node [
    id 87
    label "czeladka"
  ]
  node [
    id 88
    label "dzietno&#347;&#263;"
  ]
  node [
    id 89
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 90
    label "bawienie_si&#281;"
  ]
  node [
    id 91
    label "pomiot"
  ]
  node [
    id 92
    label "grupa"
  ]
  node [
    id 93
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 94
    label "kinderbal"
  ]
  node [
    id 95
    label "krewny"
  ]
  node [
    id 96
    label "ludzko&#347;&#263;"
  ]
  node [
    id 97
    label "asymilowanie"
  ]
  node [
    id 98
    label "wapniak"
  ]
  node [
    id 99
    label "asymilowa&#263;"
  ]
  node [
    id 100
    label "os&#322;abia&#263;"
  ]
  node [
    id 101
    label "posta&#263;"
  ]
  node [
    id 102
    label "hominid"
  ]
  node [
    id 103
    label "podw&#322;adny"
  ]
  node [
    id 104
    label "os&#322;abianie"
  ]
  node [
    id 105
    label "g&#322;owa"
  ]
  node [
    id 106
    label "figura"
  ]
  node [
    id 107
    label "portrecista"
  ]
  node [
    id 108
    label "dwun&#243;g"
  ]
  node [
    id 109
    label "profanum"
  ]
  node [
    id 110
    label "mikrokosmos"
  ]
  node [
    id 111
    label "nasada"
  ]
  node [
    id 112
    label "duch"
  ]
  node [
    id 113
    label "antropochoria"
  ]
  node [
    id 114
    label "osoba"
  ]
  node [
    id 115
    label "wz&#243;r"
  ]
  node [
    id 116
    label "senior"
  ]
  node [
    id 117
    label "oddzia&#322;ywanie"
  ]
  node [
    id 118
    label "Adam"
  ]
  node [
    id 119
    label "homo_sapiens"
  ]
  node [
    id 120
    label "polifag"
  ]
  node [
    id 121
    label "ma&#322;oletny"
  ]
  node [
    id 122
    label "m&#322;ody"
  ]
  node [
    id 123
    label "p&#322;aszczyzna"
  ]
  node [
    id 124
    label "odwadnia&#263;"
  ]
  node [
    id 125
    label "przyswoi&#263;"
  ]
  node [
    id 126
    label "sk&#243;ra"
  ]
  node [
    id 127
    label "odwodni&#263;"
  ]
  node [
    id 128
    label "ewoluowanie"
  ]
  node [
    id 129
    label "staw"
  ]
  node [
    id 130
    label "ow&#322;osienie"
  ]
  node [
    id 131
    label "unerwienie"
  ]
  node [
    id 132
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 133
    label "reakcja"
  ]
  node [
    id 134
    label "wyewoluowanie"
  ]
  node [
    id 135
    label "przyswajanie"
  ]
  node [
    id 136
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 137
    label "wyewoluowa&#263;"
  ]
  node [
    id 138
    label "miejsce"
  ]
  node [
    id 139
    label "biorytm"
  ]
  node [
    id 140
    label "ewoluowa&#263;"
  ]
  node [
    id 141
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 142
    label "istota_&#380;ywa"
  ]
  node [
    id 143
    label "otworzy&#263;"
  ]
  node [
    id 144
    label "otwiera&#263;"
  ]
  node [
    id 145
    label "czynnik_biotyczny"
  ]
  node [
    id 146
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 147
    label "otworzenie"
  ]
  node [
    id 148
    label "otwieranie"
  ]
  node [
    id 149
    label "individual"
  ]
  node [
    id 150
    label "szkielet"
  ]
  node [
    id 151
    label "ty&#322;"
  ]
  node [
    id 152
    label "obiekt"
  ]
  node [
    id 153
    label "przyswaja&#263;"
  ]
  node [
    id 154
    label "przyswojenie"
  ]
  node [
    id 155
    label "odwadnianie"
  ]
  node [
    id 156
    label "odwodnienie"
  ]
  node [
    id 157
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 158
    label "starzenie_si&#281;"
  ]
  node [
    id 159
    label "prz&#243;d"
  ]
  node [
    id 160
    label "uk&#322;ad"
  ]
  node [
    id 161
    label "temperatura"
  ]
  node [
    id 162
    label "l&#281;d&#378;wie"
  ]
  node [
    id 163
    label "cia&#322;o"
  ]
  node [
    id 164
    label "cz&#322;onek"
  ]
  node [
    id 165
    label "degenerat"
  ]
  node [
    id 166
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 167
    label "zwyrol"
  ]
  node [
    id 168
    label "czerniak"
  ]
  node [
    id 169
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 170
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 171
    label "paszcza"
  ]
  node [
    id 172
    label "popapraniec"
  ]
  node [
    id 173
    label "skuba&#263;"
  ]
  node [
    id 174
    label "skubanie"
  ]
  node [
    id 175
    label "skubni&#281;cie"
  ]
  node [
    id 176
    label "agresja"
  ]
  node [
    id 177
    label "zwierz&#281;ta"
  ]
  node [
    id 178
    label "fukni&#281;cie"
  ]
  node [
    id 179
    label "farba"
  ]
  node [
    id 180
    label "fukanie"
  ]
  node [
    id 181
    label "gad"
  ]
  node [
    id 182
    label "siedzie&#263;"
  ]
  node [
    id 183
    label "oswaja&#263;"
  ]
  node [
    id 184
    label "tresowa&#263;"
  ]
  node [
    id 185
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 186
    label "poligamia"
  ]
  node [
    id 187
    label "oz&#243;r"
  ]
  node [
    id 188
    label "skubn&#261;&#263;"
  ]
  node [
    id 189
    label "wios&#322;owa&#263;"
  ]
  node [
    id 190
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 191
    label "le&#380;enie"
  ]
  node [
    id 192
    label "niecz&#322;owiek"
  ]
  node [
    id 193
    label "wios&#322;owanie"
  ]
  node [
    id 194
    label "napasienie_si&#281;"
  ]
  node [
    id 195
    label "wiwarium"
  ]
  node [
    id 196
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 197
    label "animalista"
  ]
  node [
    id 198
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 199
    label "budowa"
  ]
  node [
    id 200
    label "hodowla"
  ]
  node [
    id 201
    label "pasienie_si&#281;"
  ]
  node [
    id 202
    label "sodomita"
  ]
  node [
    id 203
    label "monogamia"
  ]
  node [
    id 204
    label "przyssawka"
  ]
  node [
    id 205
    label "zachowanie"
  ]
  node [
    id 206
    label "budowa_cia&#322;a"
  ]
  node [
    id 207
    label "okrutnik"
  ]
  node [
    id 208
    label "grzbiet"
  ]
  node [
    id 209
    label "weterynarz"
  ]
  node [
    id 210
    label "&#322;eb"
  ]
  node [
    id 211
    label "wylinka"
  ]
  node [
    id 212
    label "bestia"
  ]
  node [
    id 213
    label "poskramia&#263;"
  ]
  node [
    id 214
    label "fauna"
  ]
  node [
    id 215
    label "treser"
  ]
  node [
    id 216
    label "siedzenie"
  ]
  node [
    id 217
    label "le&#380;e&#263;"
  ]
  node [
    id 218
    label "uspokojenie"
  ]
  node [
    id 219
    label "utulenie_si&#281;"
  ]
  node [
    id 220
    label "u&#347;pienie"
  ]
  node [
    id 221
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 222
    label "uspokoi&#263;"
  ]
  node [
    id 223
    label "utulanie_si&#281;"
  ]
  node [
    id 224
    label "usypianie"
  ]
  node [
    id 225
    label "pocieszanie"
  ]
  node [
    id 226
    label "uspokajanie"
  ]
  node [
    id 227
    label "usypia&#263;"
  ]
  node [
    id 228
    label "uspokaja&#263;"
  ]
  node [
    id 229
    label "wyliczanka"
  ]
  node [
    id 230
    label "specjalista"
  ]
  node [
    id 231
    label "harcerz"
  ]
  node [
    id 232
    label "ch&#322;opta&#347;"
  ]
  node [
    id 233
    label "zawodnik"
  ]
  node [
    id 234
    label "go&#322;ow&#261;s"
  ]
  node [
    id 235
    label "m&#322;ode"
  ]
  node [
    id 236
    label "stopie&#324;_harcerski"
  ]
  node [
    id 237
    label "g&#243;wniarz"
  ]
  node [
    id 238
    label "beniaminek"
  ]
  node [
    id 239
    label "dewiant"
  ]
  node [
    id 240
    label "istotka"
  ]
  node [
    id 241
    label "bech"
  ]
  node [
    id 242
    label "dziecinny"
  ]
  node [
    id 243
    label "naiwniak"
  ]
  node [
    id 244
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 245
    label "mie&#263;_miejsce"
  ]
  node [
    id 246
    label "equal"
  ]
  node [
    id 247
    label "trwa&#263;"
  ]
  node [
    id 248
    label "chodzi&#263;"
  ]
  node [
    id 249
    label "si&#281;ga&#263;"
  ]
  node [
    id 250
    label "stan"
  ]
  node [
    id 251
    label "obecno&#347;&#263;"
  ]
  node [
    id 252
    label "stand"
  ]
  node [
    id 253
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 254
    label "uczestniczy&#263;"
  ]
  node [
    id 255
    label "participate"
  ]
  node [
    id 256
    label "robi&#263;"
  ]
  node [
    id 257
    label "istnie&#263;"
  ]
  node [
    id 258
    label "pozostawa&#263;"
  ]
  node [
    id 259
    label "zostawa&#263;"
  ]
  node [
    id 260
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 261
    label "adhere"
  ]
  node [
    id 262
    label "compass"
  ]
  node [
    id 263
    label "korzysta&#263;"
  ]
  node [
    id 264
    label "appreciation"
  ]
  node [
    id 265
    label "osi&#261;ga&#263;"
  ]
  node [
    id 266
    label "dociera&#263;"
  ]
  node [
    id 267
    label "get"
  ]
  node [
    id 268
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 269
    label "mierzy&#263;"
  ]
  node [
    id 270
    label "u&#380;ywa&#263;"
  ]
  node [
    id 271
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 272
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 273
    label "exsert"
  ]
  node [
    id 274
    label "being"
  ]
  node [
    id 275
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 276
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 277
    label "p&#322;ywa&#263;"
  ]
  node [
    id 278
    label "run"
  ]
  node [
    id 279
    label "bangla&#263;"
  ]
  node [
    id 280
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 281
    label "przebiega&#263;"
  ]
  node [
    id 282
    label "wk&#322;ada&#263;"
  ]
  node [
    id 283
    label "proceed"
  ]
  node [
    id 284
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 285
    label "carry"
  ]
  node [
    id 286
    label "bywa&#263;"
  ]
  node [
    id 287
    label "dziama&#263;"
  ]
  node [
    id 288
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 289
    label "stara&#263;_si&#281;"
  ]
  node [
    id 290
    label "para"
  ]
  node [
    id 291
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 292
    label "str&#243;j"
  ]
  node [
    id 293
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 294
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 295
    label "krok"
  ]
  node [
    id 296
    label "tryb"
  ]
  node [
    id 297
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 298
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 299
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 300
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 301
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 302
    label "continue"
  ]
  node [
    id 303
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 304
    label "Ohio"
  ]
  node [
    id 305
    label "wci&#281;cie"
  ]
  node [
    id 306
    label "Nowy_York"
  ]
  node [
    id 307
    label "warstwa"
  ]
  node [
    id 308
    label "samopoczucie"
  ]
  node [
    id 309
    label "Illinois"
  ]
  node [
    id 310
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 311
    label "state"
  ]
  node [
    id 312
    label "Jukatan"
  ]
  node [
    id 313
    label "Kalifornia"
  ]
  node [
    id 314
    label "Wirginia"
  ]
  node [
    id 315
    label "wektor"
  ]
  node [
    id 316
    label "Goa"
  ]
  node [
    id 317
    label "Teksas"
  ]
  node [
    id 318
    label "Waszyngton"
  ]
  node [
    id 319
    label "Massachusetts"
  ]
  node [
    id 320
    label "Alaska"
  ]
  node [
    id 321
    label "Arakan"
  ]
  node [
    id 322
    label "Hawaje"
  ]
  node [
    id 323
    label "Maryland"
  ]
  node [
    id 324
    label "punkt"
  ]
  node [
    id 325
    label "Michigan"
  ]
  node [
    id 326
    label "Arizona"
  ]
  node [
    id 327
    label "Georgia"
  ]
  node [
    id 328
    label "poziom"
  ]
  node [
    id 329
    label "Pensylwania"
  ]
  node [
    id 330
    label "shape"
  ]
  node [
    id 331
    label "Luizjana"
  ]
  node [
    id 332
    label "Nowy_Meksyk"
  ]
  node [
    id 333
    label "Alabama"
  ]
  node [
    id 334
    label "ilo&#347;&#263;"
  ]
  node [
    id 335
    label "Kansas"
  ]
  node [
    id 336
    label "Oregon"
  ]
  node [
    id 337
    label "Oklahoma"
  ]
  node [
    id 338
    label "Floryda"
  ]
  node [
    id 339
    label "jednostka_administracyjna"
  ]
  node [
    id 340
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 341
    label "do&#347;wiadcza&#263;"
  ]
  node [
    id 342
    label "dub"
  ]
  node [
    id 343
    label "nazywa&#263;"
  ]
  node [
    id 344
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 345
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 346
    label "usprawiedliwia&#263;"
  ]
  node [
    id 347
    label "opija&#263;"
  ]
  node [
    id 348
    label "rozcie&#324;cza&#263;"
  ]
  node [
    id 349
    label "przedstawia&#263;"
  ]
  node [
    id 350
    label "rozrzedza&#263;"
  ]
  node [
    id 351
    label "body_of_water"
  ]
  node [
    id 352
    label "teatr"
  ]
  node [
    id 353
    label "exhibit"
  ]
  node [
    id 354
    label "podawa&#263;"
  ]
  node [
    id 355
    label "display"
  ]
  node [
    id 356
    label "pokazywa&#263;"
  ]
  node [
    id 357
    label "demonstrowa&#263;"
  ]
  node [
    id 358
    label "przedstawienie"
  ]
  node [
    id 359
    label "zapoznawa&#263;"
  ]
  node [
    id 360
    label "opisywa&#263;"
  ]
  node [
    id 361
    label "ukazywa&#263;"
  ]
  node [
    id 362
    label "represent"
  ]
  node [
    id 363
    label "zg&#322;asza&#263;"
  ]
  node [
    id 364
    label "typify"
  ]
  node [
    id 365
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 366
    label "attest"
  ]
  node [
    id 367
    label "stanowi&#263;"
  ]
  node [
    id 368
    label "spill"
  ]
  node [
    id 369
    label "moisture"
  ]
  node [
    id 370
    label "paso&#380;ytowa&#263;"
  ]
  node [
    id 371
    label "alkohol"
  ]
  node [
    id 372
    label "connect"
  ]
  node [
    id 373
    label "poi&#263;"
  ]
  node [
    id 374
    label "odurza&#263;"
  ]
  node [
    id 375
    label "doprowadza&#263;"
  ]
  node [
    id 376
    label "try"
  ]
  node [
    id 377
    label "eksperymentowa&#263;"
  ]
  node [
    id 378
    label "hurt"
  ]
  node [
    id 379
    label "traktowa&#263;"
  ]
  node [
    id 380
    label "obchodzi&#263;"
  ]
  node [
    id 381
    label "bless"
  ]
  node [
    id 382
    label "organizowa&#263;"
  ]
  node [
    id 383
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 384
    label "czyni&#263;"
  ]
  node [
    id 385
    label "give"
  ]
  node [
    id 386
    label "stylizowa&#263;"
  ]
  node [
    id 387
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 388
    label "falowa&#263;"
  ]
  node [
    id 389
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 390
    label "peddle"
  ]
  node [
    id 391
    label "praca"
  ]
  node [
    id 392
    label "wydala&#263;"
  ]
  node [
    id 393
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 394
    label "tentegowa&#263;"
  ]
  node [
    id 395
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 396
    label "urz&#261;dza&#263;"
  ]
  node [
    id 397
    label "oszukiwa&#263;"
  ]
  node [
    id 398
    label "work"
  ]
  node [
    id 399
    label "przerabia&#263;"
  ]
  node [
    id 400
    label "act"
  ]
  node [
    id 401
    label "post&#281;powa&#263;"
  ]
  node [
    id 402
    label "mieni&#263;"
  ]
  node [
    id 403
    label "nadawa&#263;"
  ]
  node [
    id 404
    label "okre&#347;la&#263;"
  ]
  node [
    id 405
    label "zaczyna&#263;"
  ]
  node [
    id 406
    label "nastawia&#263;"
  ]
  node [
    id 407
    label "get_in_touch"
  ]
  node [
    id 408
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 409
    label "dokoptowywa&#263;"
  ]
  node [
    id 410
    label "ogl&#261;da&#263;"
  ]
  node [
    id 411
    label "uruchamia&#263;"
  ]
  node [
    id 412
    label "involve"
  ]
  node [
    id 413
    label "umieszcza&#263;"
  ]
  node [
    id 414
    label "elaborate"
  ]
  node [
    id 415
    label "potwierdza&#263;"
  ]
  node [
    id 416
    label "broni&#263;"
  ]
  node [
    id 417
    label "explain"
  ]
  node [
    id 418
    label "uzasadnia&#263;"
  ]
  node [
    id 419
    label "nawijka"
  ]
  node [
    id 420
    label "reggae"
  ]
  node [
    id 421
    label "godzina"
  ]
  node [
    id 422
    label "time"
  ]
  node [
    id 423
    label "doba"
  ]
  node [
    id 424
    label "p&#243;&#322;godzina"
  ]
  node [
    id 425
    label "jednostka_czasu"
  ]
  node [
    id 426
    label "czas"
  ]
  node [
    id 427
    label "minuta"
  ]
  node [
    id 428
    label "kwadrans"
  ]
  node [
    id 429
    label "bia&#322;y_tydzie&#324;"
  ]
  node [
    id 430
    label "przedmiot"
  ]
  node [
    id 431
    label "association"
  ]
  node [
    id 432
    label "msza"
  ]
  node [
    id 433
    label "praktyka"
  ]
  node [
    id 434
    label "zgoda"
  ]
  node [
    id 435
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 436
    label "sakrament"
  ]
  node [
    id 437
    label "zwi&#261;zek"
  ]
  node [
    id 438
    label "wi&#261;zanie"
  ]
  node [
    id 439
    label "bratnia_dusza"
  ]
  node [
    id 440
    label "powi&#261;zanie"
  ]
  node [
    id 441
    label "zwi&#261;zanie"
  ]
  node [
    id 442
    label "konstytucja"
  ]
  node [
    id 443
    label "organizacja"
  ]
  node [
    id 444
    label "marriage"
  ]
  node [
    id 445
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 446
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 447
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 448
    label "zwi&#261;za&#263;"
  ]
  node [
    id 449
    label "marketing_afiliacyjny"
  ]
  node [
    id 450
    label "substancja_chemiczna"
  ]
  node [
    id 451
    label "koligacja"
  ]
  node [
    id 452
    label "bearing"
  ]
  node [
    id 453
    label "lokant"
  ]
  node [
    id 454
    label "azeotrop"
  ]
  node [
    id 455
    label "decyzja"
  ]
  node [
    id 456
    label "wiedza"
  ]
  node [
    id 457
    label "consensus"
  ]
  node [
    id 458
    label "zwalnianie_si&#281;"
  ]
  node [
    id 459
    label "odpowied&#378;"
  ]
  node [
    id 460
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 461
    label "jednomy&#347;lno&#347;&#263;"
  ]
  node [
    id 462
    label "spok&#243;j"
  ]
  node [
    id 463
    label "license"
  ]
  node [
    id 464
    label "agreement"
  ]
  node [
    id 465
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 466
    label "zwolnienie_si&#281;"
  ]
  node [
    id 467
    label "entity"
  ]
  node [
    id 468
    label "pozwole&#324;stwo"
  ]
  node [
    id 469
    label "zboczenie"
  ]
  node [
    id 470
    label "om&#243;wienie"
  ]
  node [
    id 471
    label "sponiewieranie"
  ]
  node [
    id 472
    label "discipline"
  ]
  node [
    id 473
    label "rzecz"
  ]
  node [
    id 474
    label "omawia&#263;"
  ]
  node [
    id 475
    label "kr&#261;&#380;enie"
  ]
  node [
    id 476
    label "tre&#347;&#263;"
  ]
  node [
    id 477
    label "robienie"
  ]
  node [
    id 478
    label "sponiewiera&#263;"
  ]
  node [
    id 479
    label "element"
  ]
  node [
    id 480
    label "tematyka"
  ]
  node [
    id 481
    label "w&#261;tek"
  ]
  node [
    id 482
    label "charakter"
  ]
  node [
    id 483
    label "zbaczanie"
  ]
  node [
    id 484
    label "program_nauczania"
  ]
  node [
    id 485
    label "om&#243;wi&#263;"
  ]
  node [
    id 486
    label "omawianie"
  ]
  node [
    id 487
    label "thing"
  ]
  node [
    id 488
    label "kultura"
  ]
  node [
    id 489
    label "istota"
  ]
  node [
    id 490
    label "zbacza&#263;"
  ]
  node [
    id 491
    label "zboczy&#263;"
  ]
  node [
    id 492
    label "przyrzeczenie_harcerskie"
  ]
  node [
    id 493
    label "przysi&#281;ga_wojskowa"
  ]
  node [
    id 494
    label "egzaltacja"
  ]
  node [
    id 495
    label "patos"
  ]
  node [
    id 496
    label "atmosfera"
  ]
  node [
    id 497
    label "practice"
  ]
  node [
    id 498
    label "znawstwo"
  ]
  node [
    id 499
    label "skill"
  ]
  node [
    id 500
    label "czyn"
  ]
  node [
    id 501
    label "nauka"
  ]
  node [
    id 502
    label "zwyczaj"
  ]
  node [
    id 503
    label "eksperiencja"
  ]
  node [
    id 504
    label "poj&#281;cie"
  ]
  node [
    id 505
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 506
    label "Mass"
  ]
  node [
    id 507
    label "dzie&#322;o"
  ]
  node [
    id 508
    label "katolicyzm"
  ]
  node [
    id 509
    label "utw&#243;r"
  ]
  node [
    id 510
    label "ofertorium"
  ]
  node [
    id 511
    label "prefacja"
  ]
  node [
    id 512
    label "ofiarowanie"
  ]
  node [
    id 513
    label "prezbiter"
  ]
  node [
    id 514
    label "przeistoczenie"
  ]
  node [
    id 515
    label "gloria"
  ]
  node [
    id 516
    label "confiteor"
  ]
  node [
    id 517
    label "ewangelia"
  ]
  node [
    id 518
    label "sekreta"
  ]
  node [
    id 519
    label "podniesienie"
  ]
  node [
    id 520
    label "credo"
  ]
  node [
    id 521
    label "episto&#322;a"
  ]
  node [
    id 522
    label "czytanie"
  ]
  node [
    id 523
    label "prawos&#322;awie"
  ]
  node [
    id 524
    label "kazanie"
  ]
  node [
    id 525
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 526
    label "kanon"
  ]
  node [
    id 527
    label "kolekta"
  ]
  node [
    id 528
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 529
    label "sta&#263;"
  ]
  node [
    id 530
    label "zajmowa&#263;"
  ]
  node [
    id 531
    label "room"
  ]
  node [
    id 532
    label "przebywa&#263;"
  ]
  node [
    id 533
    label "fall"
  ]
  node [
    id 534
    label "panowa&#263;"
  ]
  node [
    id 535
    label "manipulate"
  ]
  node [
    id 536
    label "kontrolowa&#263;"
  ]
  node [
    id 537
    label "kierowa&#263;"
  ]
  node [
    id 538
    label "dominowa&#263;"
  ]
  node [
    id 539
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 540
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 541
    label "control"
  ]
  node [
    id 542
    label "przewa&#380;a&#263;"
  ]
  node [
    id 543
    label "dostarcza&#263;"
  ]
  node [
    id 544
    label "schorzenie"
  ]
  node [
    id 545
    label "komornik"
  ]
  node [
    id 546
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 547
    label "return"
  ]
  node [
    id 548
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 549
    label "bra&#263;"
  ]
  node [
    id 550
    label "rozciekawia&#263;"
  ]
  node [
    id 551
    label "klasyfikacja"
  ]
  node [
    id 552
    label "zadawa&#263;"
  ]
  node [
    id 553
    label "fill"
  ]
  node [
    id 554
    label "zabiera&#263;"
  ]
  node [
    id 555
    label "topographic_point"
  ]
  node [
    id 556
    label "obejmowa&#263;"
  ]
  node [
    id 557
    label "pali&#263;_si&#281;"
  ]
  node [
    id 558
    label "aim"
  ]
  node [
    id 559
    label "anektowa&#263;"
  ]
  node [
    id 560
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 561
    label "prosecute"
  ]
  node [
    id 562
    label "powodowa&#263;"
  ]
  node [
    id 563
    label "sake"
  ]
  node [
    id 564
    label "do"
  ]
  node [
    id 565
    label "tkwi&#263;"
  ]
  node [
    id 566
    label "pause"
  ]
  node [
    id 567
    label "przestawa&#263;"
  ]
  node [
    id 568
    label "hesitate"
  ]
  node [
    id 569
    label "wystarczy&#263;"
  ]
  node [
    id 570
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 571
    label "kosztowa&#263;"
  ]
  node [
    id 572
    label "undertaking"
  ]
  node [
    id 573
    label "digest"
  ]
  node [
    id 574
    label "wystawa&#263;"
  ]
  node [
    id 575
    label "wystarcza&#263;"
  ]
  node [
    id 576
    label "base"
  ]
  node [
    id 577
    label "sprawowa&#263;"
  ]
  node [
    id 578
    label "czeka&#263;"
  ]
  node [
    id 579
    label "Mesmer"
  ]
  node [
    id 580
    label "pracownik"
  ]
  node [
    id 581
    label "Galen"
  ]
  node [
    id 582
    label "zbada&#263;"
  ]
  node [
    id 583
    label "medyk"
  ]
  node [
    id 584
    label "eskulap"
  ]
  node [
    id 585
    label "lekarze"
  ]
  node [
    id 586
    label "Hipokrates"
  ]
  node [
    id 587
    label "dokt&#243;r"
  ]
  node [
    id 588
    label "&#347;rodowisko"
  ]
  node [
    id 589
    label "student"
  ]
  node [
    id 590
    label "praktyk"
  ]
  node [
    id 591
    label "salariat"
  ]
  node [
    id 592
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 593
    label "delegowanie"
  ]
  node [
    id 594
    label "pracu&#347;"
  ]
  node [
    id 595
    label "r&#281;ka"
  ]
  node [
    id 596
    label "delegowa&#263;"
  ]
  node [
    id 597
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 598
    label "Aesculapius"
  ]
  node [
    id 599
    label "sprawdzi&#263;"
  ]
  node [
    id 600
    label "pozna&#263;"
  ]
  node [
    id 601
    label "zdecydowa&#263;"
  ]
  node [
    id 602
    label "zrobi&#263;"
  ]
  node [
    id 603
    label "wybada&#263;"
  ]
  node [
    id 604
    label "examine"
  ]
  node [
    id 605
    label "sail"
  ]
  node [
    id 606
    label "leave"
  ]
  node [
    id 607
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 608
    label "travel"
  ]
  node [
    id 609
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 610
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 611
    label "zmieni&#263;"
  ]
  node [
    id 612
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 613
    label "zosta&#263;"
  ]
  node [
    id 614
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 615
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 616
    label "przyj&#261;&#263;"
  ]
  node [
    id 617
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 618
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 619
    label "uda&#263;_si&#281;"
  ]
  node [
    id 620
    label "zacz&#261;&#263;"
  ]
  node [
    id 621
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 622
    label "play_along"
  ]
  node [
    id 623
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 624
    label "opu&#347;ci&#263;"
  ]
  node [
    id 625
    label "become"
  ]
  node [
    id 626
    label "post&#261;pi&#263;"
  ]
  node [
    id 627
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 628
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 629
    label "odj&#261;&#263;"
  ]
  node [
    id 630
    label "cause"
  ]
  node [
    id 631
    label "introduce"
  ]
  node [
    id 632
    label "begin"
  ]
  node [
    id 633
    label "przybra&#263;"
  ]
  node [
    id 634
    label "strike"
  ]
  node [
    id 635
    label "zobowi&#261;za&#263;_si&#281;"
  ]
  node [
    id 636
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 637
    label "wpu&#347;ci&#263;"
  ]
  node [
    id 638
    label "dopu&#347;ci&#263;"
  ]
  node [
    id 639
    label "receive"
  ]
  node [
    id 640
    label "obra&#263;"
  ]
  node [
    id 641
    label "uzna&#263;"
  ]
  node [
    id 642
    label "draw"
  ]
  node [
    id 643
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 644
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 645
    label "przyj&#281;cie"
  ]
  node [
    id 646
    label "swallow"
  ]
  node [
    id 647
    label "odebra&#263;"
  ]
  node [
    id 648
    label "dostarczy&#263;"
  ]
  node [
    id 649
    label "umie&#347;ci&#263;"
  ]
  node [
    id 650
    label "wzi&#261;&#263;"
  ]
  node [
    id 651
    label "absorb"
  ]
  node [
    id 652
    label "undertake"
  ]
  node [
    id 653
    label "sprawi&#263;"
  ]
  node [
    id 654
    label "change"
  ]
  node [
    id 655
    label "zast&#261;pi&#263;"
  ]
  node [
    id 656
    label "come_up"
  ]
  node [
    id 657
    label "przej&#347;&#263;"
  ]
  node [
    id 658
    label "straci&#263;"
  ]
  node [
    id 659
    label "zyska&#263;"
  ]
  node [
    id 660
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 661
    label "osta&#263;_si&#281;"
  ]
  node [
    id 662
    label "pozosta&#263;"
  ]
  node [
    id 663
    label "catch"
  ]
  node [
    id 664
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 665
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 666
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 667
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 668
    label "zorganizowa&#263;"
  ]
  node [
    id 669
    label "appoint"
  ]
  node [
    id 670
    label "wystylizowa&#263;"
  ]
  node [
    id 671
    label "przerobi&#263;"
  ]
  node [
    id 672
    label "nabra&#263;"
  ]
  node [
    id 673
    label "make"
  ]
  node [
    id 674
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 675
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 676
    label "wydali&#263;"
  ]
  node [
    id 677
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 678
    label "pozostawi&#263;"
  ]
  node [
    id 679
    label "obni&#380;y&#263;"
  ]
  node [
    id 680
    label "zostawi&#263;"
  ]
  node [
    id 681
    label "przesta&#263;"
  ]
  node [
    id 682
    label "potani&#263;"
  ]
  node [
    id 683
    label "drop"
  ]
  node [
    id 684
    label "evacuate"
  ]
  node [
    id 685
    label "humiliate"
  ]
  node [
    id 686
    label "tekst"
  ]
  node [
    id 687
    label "authorize"
  ]
  node [
    id 688
    label "omin&#261;&#263;"
  ]
  node [
    id 689
    label "loom"
  ]
  node [
    id 690
    label "result"
  ]
  node [
    id 691
    label "ukaza&#263;_si&#281;"
  ]
  node [
    id 692
    label "wybi&#263;_si&#281;"
  ]
  node [
    id 693
    label "wyj&#347;&#263;_na_jaw"
  ]
  node [
    id 694
    label "appear"
  ]
  node [
    id 695
    label "zgin&#261;&#263;"
  ]
  node [
    id 696
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 697
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 698
    label "rise"
  ]
  node [
    id 699
    label "lock"
  ]
  node [
    id 700
    label "absolut"
  ]
  node [
    id 701
    label "integer"
  ]
  node [
    id 702
    label "liczba"
  ]
  node [
    id 703
    label "zlewanie_si&#281;"
  ]
  node [
    id 704
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 705
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 706
    label "pe&#322;ny"
  ]
  node [
    id 707
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 708
    label "olejek_eteryczny"
  ]
  node [
    id 709
    label "byt"
  ]
  node [
    id 710
    label "k&#322;opotliwy"
  ]
  node [
    id 711
    label "skomplikowany"
  ]
  node [
    id 712
    label "ci&#281;&#380;ko"
  ]
  node [
    id 713
    label "wymagaj&#261;cy"
  ]
  node [
    id 714
    label "monumentalnie"
  ]
  node [
    id 715
    label "charakterystycznie"
  ]
  node [
    id 716
    label "gro&#378;nie"
  ]
  node [
    id 717
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 718
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 719
    label "nieudanie"
  ]
  node [
    id 720
    label "mocno"
  ]
  node [
    id 721
    label "wolno"
  ]
  node [
    id 722
    label "kompletnie"
  ]
  node [
    id 723
    label "ci&#281;&#380;ki"
  ]
  node [
    id 724
    label "dotkliwie"
  ]
  node [
    id 725
    label "niezgrabnie"
  ]
  node [
    id 726
    label "hard"
  ]
  node [
    id 727
    label "&#378;le"
  ]
  node [
    id 728
    label "masywnie"
  ]
  node [
    id 729
    label "heavily"
  ]
  node [
    id 730
    label "niedelikatnie"
  ]
  node [
    id 731
    label "intensywnie"
  ]
  node [
    id 732
    label "skomplikowanie"
  ]
  node [
    id 733
    label "k&#322;opotliwie"
  ]
  node [
    id 734
    label "nieprzyjemny"
  ]
  node [
    id 735
    label "niewygodny"
  ]
  node [
    id 736
    label "wymagaj&#261;co"
  ]
  node [
    id 737
    label "otoczy&#263;"
  ]
  node [
    id 738
    label "spotka&#263;"
  ]
  node [
    id 739
    label "spowodowa&#263;"
  ]
  node [
    id 740
    label "visit"
  ]
  node [
    id 741
    label "environment"
  ]
  node [
    id 742
    label "dotkn&#261;&#263;"
  ]
  node [
    id 743
    label "insert"
  ]
  node [
    id 744
    label "visualize"
  ]
  node [
    id 745
    label "befall"
  ]
  node [
    id 746
    label "go_steady"
  ]
  node [
    id 747
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 748
    label "znale&#378;&#263;"
  ]
  node [
    id 749
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 750
    label "dotkn&#261;&#263;_si&#281;"
  ]
  node [
    id 751
    label "allude"
  ]
  node [
    id 752
    label "range"
  ]
  node [
    id 753
    label "diss"
  ]
  node [
    id 754
    label "pique"
  ]
  node [
    id 755
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 756
    label "wzbudzi&#263;"
  ]
  node [
    id 757
    label "obdarowa&#263;"
  ]
  node [
    id 758
    label "sta&#263;_si&#281;"
  ]
  node [
    id 759
    label "span"
  ]
  node [
    id 760
    label "admit"
  ]
  node [
    id 761
    label "roztoczy&#263;"
  ]
  node [
    id 762
    label "performance"
  ]
  node [
    id 763
    label "sztuka"
  ]
  node [
    id 764
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 765
    label "bliski"
  ]
  node [
    id 766
    label "dok&#322;adnie"
  ]
  node [
    id 767
    label "silnie"
  ]
  node [
    id 768
    label "znajomy"
  ]
  node [
    id 769
    label "zwi&#261;zany"
  ]
  node [
    id 770
    label "przesz&#322;y"
  ]
  node [
    id 771
    label "silny"
  ]
  node [
    id 772
    label "zbli&#380;enie"
  ]
  node [
    id 773
    label "kr&#243;tki"
  ]
  node [
    id 774
    label "oddalony"
  ]
  node [
    id 775
    label "dok&#322;adny"
  ]
  node [
    id 776
    label "nieodleg&#322;y"
  ]
  node [
    id 777
    label "przysz&#322;y"
  ]
  node [
    id 778
    label "gotowy"
  ]
  node [
    id 779
    label "ma&#322;y"
  ]
  node [
    id 780
    label "meticulously"
  ]
  node [
    id 781
    label "punctiliously"
  ]
  node [
    id 782
    label "precyzyjnie"
  ]
  node [
    id 783
    label "rzetelnie"
  ]
  node [
    id 784
    label "mocny"
  ]
  node [
    id 785
    label "zajebi&#347;cie"
  ]
  node [
    id 786
    label "przekonuj&#261;co"
  ]
  node [
    id 787
    label "powerfully"
  ]
  node [
    id 788
    label "konkretnie"
  ]
  node [
    id 789
    label "niepodwa&#380;alnie"
  ]
  node [
    id 790
    label "zdecydowanie"
  ]
  node [
    id 791
    label "dusznie"
  ]
  node [
    id 792
    label "strongly"
  ]
  node [
    id 793
    label "przypominanie"
  ]
  node [
    id 794
    label "podobnie"
  ]
  node [
    id 795
    label "upodabnianie_si&#281;"
  ]
  node [
    id 796
    label "upodobnienie"
  ]
  node [
    id 797
    label "drugi"
  ]
  node [
    id 798
    label "taki"
  ]
  node [
    id 799
    label "charakterystyczny"
  ]
  node [
    id 800
    label "upodobnienie_si&#281;"
  ]
  node [
    id 801
    label "zasymilowanie"
  ]
  node [
    id 802
    label "okre&#347;lony"
  ]
  node [
    id 803
    label "jaki&#347;"
  ]
  node [
    id 804
    label "szczeg&#243;lny"
  ]
  node [
    id 805
    label "wyj&#261;tkowy"
  ]
  node [
    id 806
    label "typowy"
  ]
  node [
    id 807
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 808
    label "dobywanie"
  ]
  node [
    id 809
    label "recall"
  ]
  node [
    id 810
    label "u&#347;wiadamianie"
  ]
  node [
    id 811
    label "informowanie"
  ]
  node [
    id 812
    label "pobranie"
  ]
  node [
    id 813
    label "assimilation"
  ]
  node [
    id 814
    label "emotion"
  ]
  node [
    id 815
    label "zaczerpni&#281;cie"
  ]
  node [
    id 816
    label "g&#322;oska"
  ]
  node [
    id 817
    label "zmienienie"
  ]
  node [
    id 818
    label "fonetyka"
  ]
  node [
    id 819
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 820
    label "zjawisko_fonetyczne"
  ]
  node [
    id 821
    label "proces"
  ]
  node [
    id 822
    label "dopasowanie"
  ]
  node [
    id 823
    label "kolejny"
  ]
  node [
    id 824
    label "sw&#243;j"
  ]
  node [
    id 825
    label "przeciwny"
  ]
  node [
    id 826
    label "wt&#243;ry"
  ]
  node [
    id 827
    label "dzie&#324;"
  ]
  node [
    id 828
    label "odwrotnie"
  ]
  node [
    id 829
    label "asymilowanie_si&#281;"
  ]
  node [
    id 830
    label "absorption"
  ]
  node [
    id 831
    label "pobieranie"
  ]
  node [
    id 832
    label "czerpanie"
  ]
  node [
    id 833
    label "acquisition"
  ]
  node [
    id 834
    label "zmienianie"
  ]
  node [
    id 835
    label "upodabnianie"
  ]
  node [
    id 836
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 837
    label "artykulator"
  ]
  node [
    id 838
    label "kod"
  ]
  node [
    id 839
    label "kawa&#322;ek"
  ]
  node [
    id 840
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 841
    label "gramatyka"
  ]
  node [
    id 842
    label "stylik"
  ]
  node [
    id 843
    label "przet&#322;umaczenie"
  ]
  node [
    id 844
    label "formalizowanie"
  ]
  node [
    id 845
    label "ssanie"
  ]
  node [
    id 846
    label "ssa&#263;"
  ]
  node [
    id 847
    label "language"
  ]
  node [
    id 848
    label "liza&#263;"
  ]
  node [
    id 849
    label "napisa&#263;"
  ]
  node [
    id 850
    label "konsonantyzm"
  ]
  node [
    id 851
    label "wokalizm"
  ]
  node [
    id 852
    label "pisa&#263;"
  ]
  node [
    id 853
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 854
    label "jeniec"
  ]
  node [
    id 855
    label "but"
  ]
  node [
    id 856
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 857
    label "po_koroniarsku"
  ]
  node [
    id 858
    label "kultura_duchowa"
  ]
  node [
    id 859
    label "t&#322;umaczenie"
  ]
  node [
    id 860
    label "m&#243;wienie"
  ]
  node [
    id 861
    label "pype&#263;"
  ]
  node [
    id 862
    label "lizanie"
  ]
  node [
    id 863
    label "pismo"
  ]
  node [
    id 864
    label "formalizowa&#263;"
  ]
  node [
    id 865
    label "rozumie&#263;"
  ]
  node [
    id 866
    label "organ"
  ]
  node [
    id 867
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 868
    label "rozumienie"
  ]
  node [
    id 869
    label "spos&#243;b"
  ]
  node [
    id 870
    label "makroglosja"
  ]
  node [
    id 871
    label "m&#243;wi&#263;"
  ]
  node [
    id 872
    label "jama_ustna"
  ]
  node [
    id 873
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 874
    label "formacja_geologiczna"
  ]
  node [
    id 875
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 876
    label "natural_language"
  ]
  node [
    id 877
    label "s&#322;ownictwo"
  ]
  node [
    id 878
    label "urz&#261;dzenie"
  ]
  node [
    id 879
    label "kawa&#322;"
  ]
  node [
    id 880
    label "plot"
  ]
  node [
    id 881
    label "podp&#322;yn&#261;&#263;"
  ]
  node [
    id 882
    label "piece"
  ]
  node [
    id 883
    label "podp&#322;ywa&#263;"
  ]
  node [
    id 884
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 885
    label "podp&#322;ywanie"
  ]
  node [
    id 886
    label "podp&#322;yni&#281;cie"
  ]
  node [
    id 887
    label "model"
  ]
  node [
    id 888
    label "narz&#281;dzie"
  ]
  node [
    id 889
    label "nature"
  ]
  node [
    id 890
    label "struktura"
  ]
  node [
    id 891
    label "code"
  ]
  node [
    id 892
    label "szyfrowanie"
  ]
  node [
    id 893
    label "ci&#261;g"
  ]
  node [
    id 894
    label "szablon"
  ]
  node [
    id 895
    label "&#380;o&#322;nierz"
  ]
  node [
    id 896
    label "internowanie"
  ]
  node [
    id 897
    label "ojczyc"
  ]
  node [
    id 898
    label "pojmaniec"
  ]
  node [
    id 899
    label "niewolnik"
  ]
  node [
    id 900
    label "internowa&#263;"
  ]
  node [
    id 901
    label "aparat_artykulacyjny"
  ]
  node [
    id 902
    label "tkanka"
  ]
  node [
    id 903
    label "jednostka_organizacyjna"
  ]
  node [
    id 904
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 905
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 906
    label "tw&#243;r"
  ]
  node [
    id 907
    label "organogeneza"
  ]
  node [
    id 908
    label "zesp&#243;&#322;"
  ]
  node [
    id 909
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 910
    label "struktura_anatomiczna"
  ]
  node [
    id 911
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 912
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 913
    label "Izba_Konsyliarska"
  ]
  node [
    id 914
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 915
    label "stomia"
  ]
  node [
    id 916
    label "dekortykacja"
  ]
  node [
    id 917
    label "okolica"
  ]
  node [
    id 918
    label "Komitet_Region&#243;w"
  ]
  node [
    id 919
    label "zapi&#281;tek"
  ]
  node [
    id 920
    label "sznurowad&#322;o"
  ]
  node [
    id 921
    label "rozbijarka"
  ]
  node [
    id 922
    label "podeszwa"
  ]
  node [
    id 923
    label "obcas"
  ]
  node [
    id 924
    label "wytw&#243;r"
  ]
  node [
    id 925
    label "wzuwanie"
  ]
  node [
    id 926
    label "wzu&#263;"
  ]
  node [
    id 927
    label "przyszwa"
  ]
  node [
    id 928
    label "raki"
  ]
  node [
    id 929
    label "cholewa"
  ]
  node [
    id 930
    label "cholewka"
  ]
  node [
    id 931
    label "zel&#243;wka"
  ]
  node [
    id 932
    label "obuwie"
  ]
  node [
    id 933
    label "napi&#281;tek"
  ]
  node [
    id 934
    label "wzucie"
  ]
  node [
    id 935
    label "kom&#243;rka"
  ]
  node [
    id 936
    label "furnishing"
  ]
  node [
    id 937
    label "zabezpieczenie"
  ]
  node [
    id 938
    label "zrobienie"
  ]
  node [
    id 939
    label "wyrz&#261;dzenie"
  ]
  node [
    id 940
    label "zagospodarowanie"
  ]
  node [
    id 941
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 942
    label "ig&#322;a"
  ]
  node [
    id 943
    label "wirnik"
  ]
  node [
    id 944
    label "aparatura"
  ]
  node [
    id 945
    label "system_energetyczny"
  ]
  node [
    id 946
    label "impulsator"
  ]
  node [
    id 947
    label "mechanizm"
  ]
  node [
    id 948
    label "sprz&#281;t"
  ]
  node [
    id 949
    label "czynno&#347;&#263;"
  ]
  node [
    id 950
    label "blokowanie"
  ]
  node [
    id 951
    label "set"
  ]
  node [
    id 952
    label "zablokowanie"
  ]
  node [
    id 953
    label "przygotowanie"
  ]
  node [
    id 954
    label "komora"
  ]
  node [
    id 955
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 956
    label "public_speaking"
  ]
  node [
    id 957
    label "powiadanie"
  ]
  node [
    id 958
    label "przepowiadanie"
  ]
  node [
    id 959
    label "wyznawanie"
  ]
  node [
    id 960
    label "wypowiadanie"
  ]
  node [
    id 961
    label "wydobywanie"
  ]
  node [
    id 962
    label "gaworzenie"
  ]
  node [
    id 963
    label "stosowanie"
  ]
  node [
    id 964
    label "wyra&#380;anie"
  ]
  node [
    id 965
    label "formu&#322;owanie"
  ]
  node [
    id 966
    label "dowalenie"
  ]
  node [
    id 967
    label "przerywanie"
  ]
  node [
    id 968
    label "wydawanie"
  ]
  node [
    id 969
    label "dogadywanie_si&#281;"
  ]
  node [
    id 970
    label "dodawanie"
  ]
  node [
    id 971
    label "prawienie"
  ]
  node [
    id 972
    label "opowiadanie"
  ]
  node [
    id 973
    label "ozywanie_si&#281;"
  ]
  node [
    id 974
    label "zapeszanie"
  ]
  node [
    id 975
    label "zwracanie_si&#281;"
  ]
  node [
    id 976
    label "dysfonia"
  ]
  node [
    id 977
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 978
    label "speaking"
  ]
  node [
    id 979
    label "zauwa&#380;enie"
  ]
  node [
    id 980
    label "mawianie"
  ]
  node [
    id 981
    label "opowiedzenie"
  ]
  node [
    id 982
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 983
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 984
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 985
    label "dogadanie_si&#281;"
  ]
  node [
    id 986
    label "wygadanie"
  ]
  node [
    id 987
    label "psychotest"
  ]
  node [
    id 988
    label "wk&#322;ad"
  ]
  node [
    id 989
    label "handwriting"
  ]
  node [
    id 990
    label "przekaz"
  ]
  node [
    id 991
    label "paleograf"
  ]
  node [
    id 992
    label "interpunkcja"
  ]
  node [
    id 993
    label "dzia&#322;"
  ]
  node [
    id 994
    label "grafia"
  ]
  node [
    id 995
    label "egzemplarz"
  ]
  node [
    id 996
    label "communication"
  ]
  node [
    id 997
    label "script"
  ]
  node [
    id 998
    label "zajawka"
  ]
  node [
    id 999
    label "list"
  ]
  node [
    id 1000
    label "adres"
  ]
  node [
    id 1001
    label "Zwrotnica"
  ]
  node [
    id 1002
    label "czasopismo"
  ]
  node [
    id 1003
    label "ok&#322;adka"
  ]
  node [
    id 1004
    label "ortografia"
  ]
  node [
    id 1005
    label "letter"
  ]
  node [
    id 1006
    label "komunikacja"
  ]
  node [
    id 1007
    label "paleografia"
  ]
  node [
    id 1008
    label "dokument"
  ]
  node [
    id 1009
    label "prasa"
  ]
  node [
    id 1010
    label "terminology"
  ]
  node [
    id 1011
    label "termin"
  ]
  node [
    id 1012
    label "fleksja"
  ]
  node [
    id 1013
    label "j&#281;zykoznawstwo"
  ]
  node [
    id 1014
    label "sk&#322;adnia"
  ]
  node [
    id 1015
    label "kategoria_gramatyczna"
  ]
  node [
    id 1016
    label "morfologia"
  ]
  node [
    id 1017
    label "g&#322;osownia"
  ]
  node [
    id 1018
    label "zasymilowa&#263;"
  ]
  node [
    id 1019
    label "phonetics"
  ]
  node [
    id 1020
    label "palatogram"
  ]
  node [
    id 1021
    label "transkrypcja"
  ]
  node [
    id 1022
    label "styl"
  ]
  node [
    id 1023
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1024
    label "ozdabia&#263;"
  ]
  node [
    id 1025
    label "stawia&#263;"
  ]
  node [
    id 1026
    label "spell"
  ]
  node [
    id 1027
    label "skryba"
  ]
  node [
    id 1028
    label "read"
  ]
  node [
    id 1029
    label "donosi&#263;"
  ]
  node [
    id 1030
    label "dysgrafia"
  ]
  node [
    id 1031
    label "dysortografia"
  ]
  node [
    id 1032
    label "tworzy&#263;"
  ]
  node [
    id 1033
    label "stworzy&#263;"
  ]
  node [
    id 1034
    label "postawi&#263;"
  ]
  node [
    id 1035
    label "write"
  ]
  node [
    id 1036
    label "donie&#347;&#263;"
  ]
  node [
    id 1037
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 1038
    label "explanation"
  ]
  node [
    id 1039
    label "bronienie"
  ]
  node [
    id 1040
    label "remark"
  ]
  node [
    id 1041
    label "przek&#322;adanie"
  ]
  node [
    id 1042
    label "zrozumia&#322;y"
  ]
  node [
    id 1043
    label "przekonywanie"
  ]
  node [
    id 1044
    label "uzasadnianie"
  ]
  node [
    id 1045
    label "rozwianie"
  ]
  node [
    id 1046
    label "rozwiewanie"
  ]
  node [
    id 1047
    label "gossip"
  ]
  node [
    id 1048
    label "przedstawianie"
  ]
  node [
    id 1049
    label "rendition"
  ]
  node [
    id 1050
    label "kr&#281;ty"
  ]
  node [
    id 1051
    label "zinterpretowa&#263;"
  ]
  node [
    id 1052
    label "put"
  ]
  node [
    id 1053
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 1054
    label "przekona&#263;"
  ]
  node [
    id 1055
    label "frame"
  ]
  node [
    id 1056
    label "poja&#347;nia&#263;"
  ]
  node [
    id 1057
    label "u&#322;atwia&#263;"
  ]
  node [
    id 1058
    label "suplikowa&#263;"
  ]
  node [
    id 1059
    label "przek&#322;ada&#263;"
  ]
  node [
    id 1060
    label "przekonywa&#263;"
  ]
  node [
    id 1061
    label "interpretowa&#263;"
  ]
  node [
    id 1062
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 1063
    label "gaworzy&#263;"
  ]
  node [
    id 1064
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 1065
    label "rozmawia&#263;"
  ]
  node [
    id 1066
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1067
    label "umie&#263;"
  ]
  node [
    id 1068
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 1069
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 1070
    label "express"
  ]
  node [
    id 1071
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1072
    label "talk"
  ]
  node [
    id 1073
    label "prawi&#263;"
  ]
  node [
    id 1074
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1075
    label "powiada&#263;"
  ]
  node [
    id 1076
    label "tell"
  ]
  node [
    id 1077
    label "chew_the_fat"
  ]
  node [
    id 1078
    label "say"
  ]
  node [
    id 1079
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 1080
    label "informowa&#263;"
  ]
  node [
    id 1081
    label "wydobywa&#263;"
  ]
  node [
    id 1082
    label "hermeneutyka"
  ]
  node [
    id 1083
    label "bycie"
  ]
  node [
    id 1084
    label "kontekst"
  ]
  node [
    id 1085
    label "apprehension"
  ]
  node [
    id 1086
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 1087
    label "interpretation"
  ]
  node [
    id 1088
    label "obja&#347;nienie"
  ]
  node [
    id 1089
    label "czucie"
  ]
  node [
    id 1090
    label "realization"
  ]
  node [
    id 1091
    label "kumanie"
  ]
  node [
    id 1092
    label "wnioskowanie"
  ]
  node [
    id 1093
    label "wiedzie&#263;"
  ]
  node [
    id 1094
    label "kuma&#263;"
  ]
  node [
    id 1095
    label "match"
  ]
  node [
    id 1096
    label "empatia"
  ]
  node [
    id 1097
    label "odbiera&#263;"
  ]
  node [
    id 1098
    label "see"
  ]
  node [
    id 1099
    label "zna&#263;"
  ]
  node [
    id 1100
    label "validate"
  ]
  node [
    id 1101
    label "precyzowa&#263;"
  ]
  node [
    id 1102
    label "nadawanie"
  ]
  node [
    id 1103
    label "precyzowanie"
  ]
  node [
    id 1104
    label "formalny"
  ]
  node [
    id 1105
    label "picie"
  ]
  node [
    id 1106
    label "usta"
  ]
  node [
    id 1107
    label "ruszanie"
  ]
  node [
    id 1108
    label "&#347;lina"
  ]
  node [
    id 1109
    label "consumption"
  ]
  node [
    id 1110
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 1111
    label "rozpuszczanie"
  ]
  node [
    id 1112
    label "aspiration"
  ]
  node [
    id 1113
    label "wci&#261;ganie"
  ]
  node [
    id 1114
    label "odci&#261;ganie"
  ]
  node [
    id 1115
    label "wessanie"
  ]
  node [
    id 1116
    label "ga&#378;nik"
  ]
  node [
    id 1117
    label "wysysanie"
  ]
  node [
    id 1118
    label "wyssanie"
  ]
  node [
    id 1119
    label "wada_wrodzona"
  ]
  node [
    id 1120
    label "znami&#281;"
  ]
  node [
    id 1121
    label "krosta"
  ]
  node [
    id 1122
    label "spot"
  ]
  node [
    id 1123
    label "brodawka"
  ]
  node [
    id 1124
    label "pip"
  ]
  node [
    id 1125
    label "dotykanie"
  ]
  node [
    id 1126
    label "przesuwanie"
  ]
  node [
    id 1127
    label "zlizanie"
  ]
  node [
    id 1128
    label "g&#322;askanie"
  ]
  node [
    id 1129
    label "wylizywanie"
  ]
  node [
    id 1130
    label "zlizywanie"
  ]
  node [
    id 1131
    label "wylizanie"
  ]
  node [
    id 1132
    label "pi&#263;"
  ]
  node [
    id 1133
    label "sponge"
  ]
  node [
    id 1134
    label "mleko"
  ]
  node [
    id 1135
    label "rozpuszcza&#263;"
  ]
  node [
    id 1136
    label "wci&#261;ga&#263;"
  ]
  node [
    id 1137
    label "rusza&#263;"
  ]
  node [
    id 1138
    label "sucking"
  ]
  node [
    id 1139
    label "smoczek"
  ]
  node [
    id 1140
    label "salt_lick"
  ]
  node [
    id 1141
    label "dotyka&#263;"
  ]
  node [
    id 1142
    label "muska&#263;"
  ]
  node [
    id 1143
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 1144
    label "cz&#322;owiekowate"
  ]
  node [
    id 1145
    label "konsument"
  ]
  node [
    id 1146
    label "Chocho&#322;"
  ]
  node [
    id 1147
    label "Herkules_Poirot"
  ]
  node [
    id 1148
    label "Edyp"
  ]
  node [
    id 1149
    label "parali&#380;owa&#263;"
  ]
  node [
    id 1150
    label "Harry_Potter"
  ]
  node [
    id 1151
    label "Casanova"
  ]
  node [
    id 1152
    label "Gargantua"
  ]
  node [
    id 1153
    label "Zgredek"
  ]
  node [
    id 1154
    label "Winnetou"
  ]
  node [
    id 1155
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 1156
    label "Dulcynea"
  ]
  node [
    id 1157
    label "person"
  ]
  node [
    id 1158
    label "Sherlock_Holmes"
  ]
  node [
    id 1159
    label "Quasimodo"
  ]
  node [
    id 1160
    label "Plastu&#347;"
  ]
  node [
    id 1161
    label "Faust"
  ]
  node [
    id 1162
    label "Wallenrod"
  ]
  node [
    id 1163
    label "Dwukwiat"
  ]
  node [
    id 1164
    label "koniugacja"
  ]
  node [
    id 1165
    label "Don_Juan"
  ]
  node [
    id 1166
    label "Don_Kiszot"
  ]
  node [
    id 1167
    label "Hamlet"
  ]
  node [
    id 1168
    label "Werter"
  ]
  node [
    id 1169
    label "Szwejk"
  ]
  node [
    id 1170
    label "doros&#322;y"
  ]
  node [
    id 1171
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 1172
    label "jajko"
  ]
  node [
    id 1173
    label "rodzic"
  ]
  node [
    id 1174
    label "wapniaki"
  ]
  node [
    id 1175
    label "zwierzchnik"
  ]
  node [
    id 1176
    label "feuda&#322;"
  ]
  node [
    id 1177
    label "starzec"
  ]
  node [
    id 1178
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 1179
    label "komendancja"
  ]
  node [
    id 1180
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 1181
    label "de-escalation"
  ]
  node [
    id 1182
    label "powodowanie"
  ]
  node [
    id 1183
    label "os&#322;abienie"
  ]
  node [
    id 1184
    label "kondycja_fizyczna"
  ]
  node [
    id 1185
    label "os&#322;abi&#263;"
  ]
  node [
    id 1186
    label "debilitation"
  ]
  node [
    id 1187
    label "zdrowie"
  ]
  node [
    id 1188
    label "zmniejszanie"
  ]
  node [
    id 1189
    label "s&#322;abszy"
  ]
  node [
    id 1190
    label "pogarszanie"
  ]
  node [
    id 1191
    label "suppress"
  ]
  node [
    id 1192
    label "zmniejsza&#263;"
  ]
  node [
    id 1193
    label "bate"
  ]
  node [
    id 1194
    label "assimilate"
  ]
  node [
    id 1195
    label "dostosowywa&#263;"
  ]
  node [
    id 1196
    label "dostosowa&#263;"
  ]
  node [
    id 1197
    label "przejmowa&#263;"
  ]
  node [
    id 1198
    label "upodobni&#263;"
  ]
  node [
    id 1199
    label "przej&#261;&#263;"
  ]
  node [
    id 1200
    label "upodabnia&#263;"
  ]
  node [
    id 1201
    label "pobiera&#263;"
  ]
  node [
    id 1202
    label "pobra&#263;"
  ]
  node [
    id 1203
    label "charakterystyka"
  ]
  node [
    id 1204
    label "zaistnie&#263;"
  ]
  node [
    id 1205
    label "Osjan"
  ]
  node [
    id 1206
    label "kto&#347;"
  ]
  node [
    id 1207
    label "wygl&#261;d"
  ]
  node [
    id 1208
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 1209
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1210
    label "trim"
  ]
  node [
    id 1211
    label "poby&#263;"
  ]
  node [
    id 1212
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 1213
    label "Aspazja"
  ]
  node [
    id 1214
    label "punkt_widzenia"
  ]
  node [
    id 1215
    label "kompleksja"
  ]
  node [
    id 1216
    label "wytrzyma&#263;"
  ]
  node [
    id 1217
    label "formacja"
  ]
  node [
    id 1218
    label "point"
  ]
  node [
    id 1219
    label "go&#347;&#263;"
  ]
  node [
    id 1220
    label "zapis"
  ]
  node [
    id 1221
    label "figure"
  ]
  node [
    id 1222
    label "typ"
  ]
  node [
    id 1223
    label "mildew"
  ]
  node [
    id 1224
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 1225
    label "ideal"
  ]
  node [
    id 1226
    label "rule"
  ]
  node [
    id 1227
    label "ruch"
  ]
  node [
    id 1228
    label "dekal"
  ]
  node [
    id 1229
    label "motyw"
  ]
  node [
    id 1230
    label "projekt"
  ]
  node [
    id 1231
    label "pryncypa&#322;"
  ]
  node [
    id 1232
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 1233
    label "kszta&#322;t"
  ]
  node [
    id 1234
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 1235
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1236
    label "&#380;ycie"
  ]
  node [
    id 1237
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 1238
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 1239
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1240
    label "dekiel"
  ]
  node [
    id 1241
    label "ro&#347;lina"
  ]
  node [
    id 1242
    label "&#347;ci&#281;cie"
  ]
  node [
    id 1243
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 1244
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 1245
    label "&#347;ci&#281;gno"
  ]
  node [
    id 1246
    label "noosfera"
  ]
  node [
    id 1247
    label "byd&#322;o"
  ]
  node [
    id 1248
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 1249
    label "makrocefalia"
  ]
  node [
    id 1250
    label "ucho"
  ]
  node [
    id 1251
    label "g&#243;ra"
  ]
  node [
    id 1252
    label "m&#243;zg"
  ]
  node [
    id 1253
    label "kierownictwo"
  ]
  node [
    id 1254
    label "fryzura"
  ]
  node [
    id 1255
    label "umys&#322;"
  ]
  node [
    id 1256
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 1257
    label "czaszka"
  ]
  node [
    id 1258
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 1259
    label "dziedzina"
  ]
  node [
    id 1260
    label "hipnotyzowanie"
  ]
  node [
    id 1261
    label "&#347;lad"
  ]
  node [
    id 1262
    label "docieranie"
  ]
  node [
    id 1263
    label "natural_process"
  ]
  node [
    id 1264
    label "reakcja_chemiczna"
  ]
  node [
    id 1265
    label "wdzieranie_si&#281;"
  ]
  node [
    id 1266
    label "zjawisko"
  ]
  node [
    id 1267
    label "rezultat"
  ]
  node [
    id 1268
    label "lobbysta"
  ]
  node [
    id 1269
    label "allochoria"
  ]
  node [
    id 1270
    label "fotograf"
  ]
  node [
    id 1271
    label "malarz"
  ]
  node [
    id 1272
    label "artysta"
  ]
  node [
    id 1273
    label "bierka_szachowa"
  ]
  node [
    id 1274
    label "obiekt_matematyczny"
  ]
  node [
    id 1275
    label "gestaltyzm"
  ]
  node [
    id 1276
    label "obraz"
  ]
  node [
    id 1277
    label "d&#378;wi&#281;k"
  ]
  node [
    id 1278
    label "character"
  ]
  node [
    id 1279
    label "rze&#378;ba"
  ]
  node [
    id 1280
    label "stylistyka"
  ]
  node [
    id 1281
    label "antycypacja"
  ]
  node [
    id 1282
    label "ornamentyka"
  ]
  node [
    id 1283
    label "informacja"
  ]
  node [
    id 1284
    label "facet"
  ]
  node [
    id 1285
    label "popis"
  ]
  node [
    id 1286
    label "wiersz"
  ]
  node [
    id 1287
    label "symetria"
  ]
  node [
    id 1288
    label "lingwistyka_kognitywna"
  ]
  node [
    id 1289
    label "karta"
  ]
  node [
    id 1290
    label "podzbi&#243;r"
  ]
  node [
    id 1291
    label "perspektywa"
  ]
  node [
    id 1292
    label "nak&#322;adka"
  ]
  node [
    id 1293
    label "li&#347;&#263;"
  ]
  node [
    id 1294
    label "jama_gard&#322;owa"
  ]
  node [
    id 1295
    label "rezonator"
  ]
  node [
    id 1296
    label "podstawa"
  ]
  node [
    id 1297
    label "piek&#322;o"
  ]
  node [
    id 1298
    label "human_body"
  ]
  node [
    id 1299
    label "ofiarowywanie"
  ]
  node [
    id 1300
    label "sfera_afektywna"
  ]
  node [
    id 1301
    label "nekromancja"
  ]
  node [
    id 1302
    label "Po&#347;wist"
  ]
  node [
    id 1303
    label "podekscytowanie"
  ]
  node [
    id 1304
    label "deformowanie"
  ]
  node [
    id 1305
    label "sumienie"
  ]
  node [
    id 1306
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 1307
    label "deformowa&#263;"
  ]
  node [
    id 1308
    label "psychika"
  ]
  node [
    id 1309
    label "zjawa"
  ]
  node [
    id 1310
    label "zmar&#322;y"
  ]
  node [
    id 1311
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1312
    label "power"
  ]
  node [
    id 1313
    label "ofiarowywa&#263;"
  ]
  node [
    id 1314
    label "oddech"
  ]
  node [
    id 1315
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1316
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1317
    label "si&#322;a"
  ]
  node [
    id 1318
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1319
    label "ego"
  ]
  node [
    id 1320
    label "fizjonomia"
  ]
  node [
    id 1321
    label "kompleks"
  ]
  node [
    id 1322
    label "zapalno&#347;&#263;"
  ]
  node [
    id 1323
    label "T&#281;sknica"
  ]
  node [
    id 1324
    label "ofiarowa&#263;"
  ]
  node [
    id 1325
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1326
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1327
    label "passion"
  ]
  node [
    id 1328
    label "odbicie"
  ]
  node [
    id 1329
    label "atom"
  ]
  node [
    id 1330
    label "przyroda"
  ]
  node [
    id 1331
    label "Ziemia"
  ]
  node [
    id 1332
    label "kosmos"
  ]
  node [
    id 1333
    label "miniatura"
  ]
  node [
    id 1334
    label "kompletny"
  ]
  node [
    id 1335
    label "zupe&#322;ny"
  ]
  node [
    id 1336
    label "wniwecz"
  ]
  node [
    id 1337
    label "w_pizdu"
  ]
  node [
    id 1338
    label "og&#243;lnie"
  ]
  node [
    id 1339
    label "ca&#322;y"
  ]
  node [
    id 1340
    label "&#322;&#261;czny"
  ]
  node [
    id 1341
    label "osobno"
  ]
  node [
    id 1342
    label "r&#243;&#380;ny"
  ]
  node [
    id 1343
    label "inszy"
  ]
  node [
    id 1344
    label "inaczej"
  ]
  node [
    id 1345
    label "odr&#281;bny"
  ]
  node [
    id 1346
    label "nast&#281;pnie"
  ]
  node [
    id 1347
    label "nastopny"
  ]
  node [
    id 1348
    label "kolejno"
  ]
  node [
    id 1349
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1350
    label "r&#243;&#380;nie"
  ]
  node [
    id 1351
    label "niestandardowo"
  ]
  node [
    id 1352
    label "individually"
  ]
  node [
    id 1353
    label "udzielnie"
  ]
  node [
    id 1354
    label "osobnie"
  ]
  node [
    id 1355
    label "odr&#281;bnie"
  ]
  node [
    id 1356
    label "osobny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 257
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 263
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 12
    target 247
  ]
  edge [
    source 12
    target 549
  ]
  edge [
    source 12
    target 550
  ]
  edge [
    source 12
    target 551
  ]
  edge [
    source 12
    target 552
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 554
  ]
  edge [
    source 12
    target 555
  ]
  edge [
    source 12
    target 556
  ]
  edge [
    source 12
    target 557
  ]
  edge [
    source 12
    target 558
  ]
  edge [
    source 12
    target 559
  ]
  edge [
    source 12
    target 560
  ]
  edge [
    source 12
    target 561
  ]
  edge [
    source 12
    target 562
  ]
  edge [
    source 12
    target 563
  ]
  edge [
    source 12
    target 564
  ]
  edge [
    source 12
    target 565
  ]
  edge [
    source 12
    target 284
  ]
  edge [
    source 12
    target 566
  ]
  edge [
    source 12
    target 567
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 568
  ]
  edge [
    source 12
    target 569
  ]
  edge [
    source 12
    target 570
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 571
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 573
  ]
  edge [
    source 12
    target 574
  ]
  edge [
    source 12
    target 575
  ]
  edge [
    source 12
    target 576
  ]
  edge [
    source 12
    target 252
  ]
  edge [
    source 12
    target 577
  ]
  edge [
    source 12
    target 578
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 592
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 593
  ]
  edge [
    source 16
    target 594
  ]
  edge [
    source 16
    target 595
  ]
  edge [
    source 16
    target 596
  ]
  edge [
    source 16
    target 597
  ]
  edge [
    source 16
    target 598
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 600
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 605
  ]
  edge [
    source 17
    target 606
  ]
  edge [
    source 17
    target 607
  ]
  edge [
    source 17
    target 608
  ]
  edge [
    source 17
    target 283
  ]
  edge [
    source 17
    target 609
  ]
  edge [
    source 17
    target 610
  ]
  edge [
    source 17
    target 602
  ]
  edge [
    source 17
    target 611
  ]
  edge [
    source 17
    target 612
  ]
  edge [
    source 17
    target 613
  ]
  edge [
    source 17
    target 614
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 619
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 621
  ]
  edge [
    source 17
    target 622
  ]
  edge [
    source 17
    target 623
  ]
  edge [
    source 17
    target 624
  ]
  edge [
    source 17
    target 625
  ]
  edge [
    source 17
    target 626
  ]
  edge [
    source 17
    target 627
  ]
  edge [
    source 17
    target 628
  ]
  edge [
    source 17
    target 629
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 632
  ]
  edge [
    source 17
    target 564
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 635
  ]
  edge [
    source 17
    target 636
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 638
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 640
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 17
    target 648
  ]
  edge [
    source 17
    target 649
  ]
  edge [
    source 17
    target 650
  ]
  edge [
    source 17
    target 651
  ]
  edge [
    source 17
    target 652
  ]
  edge [
    source 17
    target 653
  ]
  edge [
    source 17
    target 654
  ]
  edge [
    source 17
    target 655
  ]
  edge [
    source 17
    target 656
  ]
  edge [
    source 17
    target 657
  ]
  edge [
    source 17
    target 658
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 528
  ]
  edge [
    source 17
    target 661
  ]
  edge [
    source 17
    target 662
  ]
  edge [
    source 17
    target 663
  ]
  edge [
    source 17
    target 664
  ]
  edge [
    source 17
    target 665
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 667
  ]
  edge [
    source 17
    target 668
  ]
  edge [
    source 17
    target 669
  ]
  edge [
    source 17
    target 670
  ]
  edge [
    source 17
    target 671
  ]
  edge [
    source 17
    target 672
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 674
  ]
  edge [
    source 17
    target 675
  ]
  edge [
    source 17
    target 676
  ]
  edge [
    source 17
    target 677
  ]
  edge [
    source 17
    target 678
  ]
  edge [
    source 17
    target 679
  ]
  edge [
    source 17
    target 680
  ]
  edge [
    source 17
    target 681
  ]
  edge [
    source 17
    target 682
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 684
  ]
  edge [
    source 17
    target 685
  ]
  edge [
    source 17
    target 686
  ]
  edge [
    source 17
    target 687
  ]
  edge [
    source 17
    target 688
  ]
  edge [
    source 17
    target 689
  ]
  edge [
    source 17
    target 690
  ]
  edge [
    source 17
    target 691
  ]
  edge [
    source 17
    target 692
  ]
  edge [
    source 17
    target 693
  ]
  edge [
    source 17
    target 694
  ]
  edge [
    source 17
    target 695
  ]
  edge [
    source 17
    target 696
  ]
  edge [
    source 17
    target 697
  ]
  edge [
    source 17
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 334
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 710
  ]
  edge [
    source 19
    target 711
  ]
  edge [
    source 19
    target 712
  ]
  edge [
    source 19
    target 713
  ]
  edge [
    source 19
    target 714
  ]
  edge [
    source 19
    target 715
  ]
  edge [
    source 19
    target 716
  ]
  edge [
    source 19
    target 717
  ]
  edge [
    source 19
    target 718
  ]
  edge [
    source 19
    target 719
  ]
  edge [
    source 19
    target 720
  ]
  edge [
    source 19
    target 721
  ]
  edge [
    source 19
    target 722
  ]
  edge [
    source 19
    target 723
  ]
  edge [
    source 19
    target 724
  ]
  edge [
    source 19
    target 725
  ]
  edge [
    source 19
    target 726
  ]
  edge [
    source 19
    target 727
  ]
  edge [
    source 19
    target 728
  ]
  edge [
    source 19
    target 729
  ]
  edge [
    source 19
    target 730
  ]
  edge [
    source 19
    target 731
  ]
  edge [
    source 19
    target 732
  ]
  edge [
    source 19
    target 733
  ]
  edge [
    source 19
    target 734
  ]
  edge [
    source 19
    target 735
  ]
  edge [
    source 19
    target 736
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 535
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 739
  ]
  edge [
    source 20
    target 740
  ]
  edge [
    source 20
    target 412
  ]
  edge [
    source 20
    target 741
  ]
  edge [
    source 20
    target 742
  ]
  edge [
    source 20
    target 621
  ]
  edge [
    source 20
    target 400
  ]
  edge [
    source 20
    target 743
  ]
  edge [
    source 20
    target 744
  ]
  edge [
    source 20
    target 600
  ]
  edge [
    source 20
    target 745
  ]
  edge [
    source 20
    target 746
  ]
  edge [
    source 20
    target 747
  ]
  edge [
    source 20
    target 748
  ]
  edge [
    source 20
    target 749
  ]
  edge [
    source 20
    target 602
  ]
  edge [
    source 20
    target 750
  ]
  edge [
    source 20
    target 751
  ]
  edge [
    source 20
    target 752
  ]
  edge [
    source 20
    target 753
  ]
  edge [
    source 20
    target 754
  ]
  edge [
    source 20
    target 755
  ]
  edge [
    source 20
    target 756
  ]
  edge [
    source 20
    target 757
  ]
  edge [
    source 20
    target 758
  ]
  edge [
    source 20
    target 759
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 761
  ]
  edge [
    source 20
    target 620
  ]
  edge [
    source 20
    target 762
  ]
  edge [
    source 20
    target 763
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 22
    target 764
  ]
  edge [
    source 22
    target 765
  ]
  edge [
    source 22
    target 766
  ]
  edge [
    source 22
    target 767
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 768
  ]
  edge [
    source 22
    target 769
  ]
  edge [
    source 22
    target 770
  ]
  edge [
    source 22
    target 771
  ]
  edge [
    source 22
    target 772
  ]
  edge [
    source 22
    target 773
  ]
  edge [
    source 22
    target 774
  ]
  edge [
    source 22
    target 775
  ]
  edge [
    source 22
    target 776
  ]
  edge [
    source 22
    target 777
  ]
  edge [
    source 22
    target 778
  ]
  edge [
    source 22
    target 779
  ]
  edge [
    source 22
    target 780
  ]
  edge [
    source 22
    target 781
  ]
  edge [
    source 22
    target 782
  ]
  edge [
    source 22
    target 783
  ]
  edge [
    source 22
    target 784
  ]
  edge [
    source 22
    target 785
  ]
  edge [
    source 22
    target 786
  ]
  edge [
    source 22
    target 787
  ]
  edge [
    source 22
    target 788
  ]
  edge [
    source 22
    target 789
  ]
  edge [
    source 22
    target 790
  ]
  edge [
    source 22
    target 791
  ]
  edge [
    source 22
    target 731
  ]
  edge [
    source 22
    target 792
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 793
  ]
  edge [
    source 23
    target 794
  ]
  edge [
    source 23
    target 795
  ]
  edge [
    source 23
    target 97
  ]
  edge [
    source 23
    target 796
  ]
  edge [
    source 23
    target 797
  ]
  edge [
    source 23
    target 798
  ]
  edge [
    source 23
    target 799
  ]
  edge [
    source 23
    target 800
  ]
  edge [
    source 23
    target 801
  ]
  edge [
    source 23
    target 802
  ]
  edge [
    source 23
    target 803
  ]
  edge [
    source 23
    target 715
  ]
  edge [
    source 23
    target 804
  ]
  edge [
    source 23
    target 805
  ]
  edge [
    source 23
    target 806
  ]
  edge [
    source 23
    target 807
  ]
  edge [
    source 23
    target 808
  ]
  edge [
    source 23
    target 809
  ]
  edge [
    source 23
    target 810
  ]
  edge [
    source 23
    target 811
  ]
  edge [
    source 23
    target 812
  ]
  edge [
    source 23
    target 73
  ]
  edge [
    source 23
    target 813
  ]
  edge [
    source 23
    target 814
  ]
  edge [
    source 23
    target 815
  ]
  edge [
    source 23
    target 816
  ]
  edge [
    source 23
    target 488
  ]
  edge [
    source 23
    target 817
  ]
  edge [
    source 23
    target 818
  ]
  edge [
    source 23
    target 819
  ]
  edge [
    source 23
    target 820
  ]
  edge [
    source 23
    target 821
  ]
  edge [
    source 23
    target 822
  ]
  edge [
    source 23
    target 823
  ]
  edge [
    source 23
    target 824
  ]
  edge [
    source 23
    target 825
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 826
  ]
  edge [
    source 23
    target 827
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 828
  ]
  edge [
    source 23
    target 829
  ]
  edge [
    source 23
    target 830
  ]
  edge [
    source 23
    target 831
  ]
  edge [
    source 23
    target 832
  ]
  edge [
    source 23
    target 833
  ]
  edge [
    source 23
    target 834
  ]
  edge [
    source 23
    target 835
  ]
  edge [
    source 23
    target 92
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 836
  ]
  edge [
    source 24
    target 837
  ]
  edge [
    source 24
    target 838
  ]
  edge [
    source 24
    target 839
  ]
  edge [
    source 24
    target 430
  ]
  edge [
    source 24
    target 840
  ]
  edge [
    source 24
    target 841
  ]
  edge [
    source 24
    target 842
  ]
  edge [
    source 24
    target 843
  ]
  edge [
    source 24
    target 844
  ]
  edge [
    source 24
    target 845
  ]
  edge [
    source 24
    target 846
  ]
  edge [
    source 24
    target 847
  ]
  edge [
    source 24
    target 848
  ]
  edge [
    source 24
    target 849
  ]
  edge [
    source 24
    target 850
  ]
  edge [
    source 24
    target 851
  ]
  edge [
    source 24
    target 852
  ]
  edge [
    source 24
    target 818
  ]
  edge [
    source 24
    target 853
  ]
  edge [
    source 24
    target 854
  ]
  edge [
    source 24
    target 855
  ]
  edge [
    source 24
    target 856
  ]
  edge [
    source 24
    target 857
  ]
  edge [
    source 24
    target 858
  ]
  edge [
    source 24
    target 859
  ]
  edge [
    source 24
    target 860
  ]
  edge [
    source 24
    target 861
  ]
  edge [
    source 24
    target 862
  ]
  edge [
    source 24
    target 863
  ]
  edge [
    source 24
    target 864
  ]
  edge [
    source 24
    target 865
  ]
  edge [
    source 24
    target 866
  ]
  edge [
    source 24
    target 867
  ]
  edge [
    source 24
    target 868
  ]
  edge [
    source 24
    target 869
  ]
  edge [
    source 24
    target 870
  ]
  edge [
    source 24
    target 871
  ]
  edge [
    source 24
    target 872
  ]
  edge [
    source 24
    target 873
  ]
  edge [
    source 24
    target 874
  ]
  edge [
    source 24
    target 875
  ]
  edge [
    source 24
    target 876
  ]
  edge [
    source 24
    target 877
  ]
  edge [
    source 24
    target 878
  ]
  edge [
    source 24
    target 879
  ]
  edge [
    source 24
    target 880
  ]
  edge [
    source 24
    target 881
  ]
  edge [
    source 24
    target 509
  ]
  edge [
    source 24
    target 882
  ]
  edge [
    source 24
    target 883
  ]
  edge [
    source 24
    target 884
  ]
  edge [
    source 24
    target 885
  ]
  edge [
    source 24
    target 886
  ]
  edge [
    source 24
    target 887
  ]
  edge [
    source 24
    target 888
  ]
  edge [
    source 24
    target 86
  ]
  edge [
    source 24
    target 296
  ]
  edge [
    source 24
    target 889
  ]
  edge [
    source 24
    target 890
  ]
  edge [
    source 24
    target 891
  ]
  edge [
    source 24
    target 892
  ]
  edge [
    source 24
    target 893
  ]
  edge [
    source 24
    target 894
  ]
  edge [
    source 24
    target 895
  ]
  edge [
    source 24
    target 896
  ]
  edge [
    source 24
    target 897
  ]
  edge [
    source 24
    target 898
  ]
  edge [
    source 24
    target 899
  ]
  edge [
    source 24
    target 900
  ]
  edge [
    source 24
    target 901
  ]
  edge [
    source 24
    target 902
  ]
  edge [
    source 24
    target 903
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 904
  ]
  edge [
    source 24
    target 905
  ]
  edge [
    source 24
    target 906
  ]
  edge [
    source 24
    target 907
  ]
  edge [
    source 24
    target 908
  ]
  edge [
    source 24
    target 909
  ]
  edge [
    source 24
    target 910
  ]
  edge [
    source 24
    target 160
  ]
  edge [
    source 24
    target 911
  ]
  edge [
    source 24
    target 912
  ]
  edge [
    source 24
    target 913
  ]
  edge [
    source 24
    target 914
  ]
  edge [
    source 24
    target 915
  ]
  edge [
    source 24
    target 916
  ]
  edge [
    source 24
    target 917
  ]
  edge [
    source 24
    target 918
  ]
  edge [
    source 24
    target 469
  ]
  edge [
    source 24
    target 470
  ]
  edge [
    source 24
    target 471
  ]
  edge [
    source 24
    target 472
  ]
  edge [
    source 24
    target 473
  ]
  edge [
    source 24
    target 474
  ]
  edge [
    source 24
    target 475
  ]
  edge [
    source 24
    target 476
  ]
  edge [
    source 24
    target 477
  ]
  edge [
    source 24
    target 478
  ]
  edge [
    source 24
    target 479
  ]
  edge [
    source 24
    target 467
  ]
  edge [
    source 24
    target 288
  ]
  edge [
    source 24
    target 480
  ]
  edge [
    source 24
    target 481
  ]
  edge [
    source 24
    target 482
  ]
  edge [
    source 24
    target 483
  ]
  edge [
    source 24
    target 484
  ]
  edge [
    source 24
    target 485
  ]
  edge [
    source 24
    target 486
  ]
  edge [
    source 24
    target 487
  ]
  edge [
    source 24
    target 488
  ]
  edge [
    source 24
    target 489
  ]
  edge [
    source 24
    target 490
  ]
  edge [
    source 24
    target 491
  ]
  edge [
    source 24
    target 919
  ]
  edge [
    source 24
    target 920
  ]
  edge [
    source 24
    target 921
  ]
  edge [
    source 24
    target 922
  ]
  edge [
    source 24
    target 923
  ]
  edge [
    source 24
    target 924
  ]
  edge [
    source 24
    target 925
  ]
  edge [
    source 24
    target 926
  ]
  edge [
    source 24
    target 927
  ]
  edge [
    source 24
    target 928
  ]
  edge [
    source 24
    target 929
  ]
  edge [
    source 24
    target 930
  ]
  edge [
    source 24
    target 931
  ]
  edge [
    source 24
    target 932
  ]
  edge [
    source 24
    target 933
  ]
  edge [
    source 24
    target 934
  ]
  edge [
    source 24
    target 935
  ]
  edge [
    source 24
    target 936
  ]
  edge [
    source 24
    target 937
  ]
  edge [
    source 24
    target 938
  ]
  edge [
    source 24
    target 939
  ]
  edge [
    source 24
    target 940
  ]
  edge [
    source 24
    target 941
  ]
  edge [
    source 24
    target 942
  ]
  edge [
    source 24
    target 943
  ]
  edge [
    source 24
    target 944
  ]
  edge [
    source 24
    target 945
  ]
  edge [
    source 24
    target 946
  ]
  edge [
    source 24
    target 947
  ]
  edge [
    source 24
    target 948
  ]
  edge [
    source 24
    target 949
  ]
  edge [
    source 24
    target 950
  ]
  edge [
    source 24
    target 951
  ]
  edge [
    source 24
    target 952
  ]
  edge [
    source 24
    target 953
  ]
  edge [
    source 24
    target 954
  ]
  edge [
    source 24
    target 955
  ]
  edge [
    source 24
    target 956
  ]
  edge [
    source 24
    target 957
  ]
  edge [
    source 24
    target 958
  ]
  edge [
    source 24
    target 959
  ]
  edge [
    source 24
    target 960
  ]
  edge [
    source 24
    target 961
  ]
  edge [
    source 24
    target 962
  ]
  edge [
    source 24
    target 963
  ]
  edge [
    source 24
    target 964
  ]
  edge [
    source 24
    target 965
  ]
  edge [
    source 24
    target 966
  ]
  edge [
    source 24
    target 967
  ]
  edge [
    source 24
    target 968
  ]
  edge [
    source 24
    target 969
  ]
  edge [
    source 24
    target 970
  ]
  edge [
    source 24
    target 971
  ]
  edge [
    source 24
    target 972
  ]
  edge [
    source 24
    target 973
  ]
  edge [
    source 24
    target 974
  ]
  edge [
    source 24
    target 975
  ]
  edge [
    source 24
    target 976
  ]
  edge [
    source 24
    target 977
  ]
  edge [
    source 24
    target 978
  ]
  edge [
    source 24
    target 979
  ]
  edge [
    source 24
    target 980
  ]
  edge [
    source 24
    target 981
  ]
  edge [
    source 24
    target 982
  ]
  edge [
    source 24
    target 983
  ]
  edge [
    source 24
    target 984
  ]
  edge [
    source 24
    target 811
  ]
  edge [
    source 24
    target 985
  ]
  edge [
    source 24
    target 986
  ]
  edge [
    source 24
    target 987
  ]
  edge [
    source 24
    target 988
  ]
  edge [
    source 24
    target 989
  ]
  edge [
    source 24
    target 990
  ]
  edge [
    source 24
    target 507
  ]
  edge [
    source 24
    target 991
  ]
  edge [
    source 24
    target 992
  ]
  edge [
    source 24
    target 44
  ]
  edge [
    source 24
    target 993
  ]
  edge [
    source 24
    target 994
  ]
  edge [
    source 24
    target 995
  ]
  edge [
    source 24
    target 996
  ]
  edge [
    source 24
    target 997
  ]
  edge [
    source 24
    target 998
  ]
  edge [
    source 24
    target 999
  ]
  edge [
    source 24
    target 1000
  ]
  edge [
    source 24
    target 1001
  ]
  edge [
    source 24
    target 1002
  ]
  edge [
    source 24
    target 1003
  ]
  edge [
    source 24
    target 1004
  ]
  edge [
    source 24
    target 1005
  ]
  edge [
    source 24
    target 1006
  ]
  edge [
    source 24
    target 1007
  ]
  edge [
    source 24
    target 1008
  ]
  edge [
    source 24
    target 1009
  ]
  edge [
    source 24
    target 1010
  ]
  edge [
    source 24
    target 1011
  ]
  edge [
    source 24
    target 1012
  ]
  edge [
    source 24
    target 1013
  ]
  edge [
    source 24
    target 1014
  ]
  edge [
    source 24
    target 1015
  ]
  edge [
    source 24
    target 1016
  ]
  edge [
    source 24
    target 99
  ]
  edge [
    source 24
    target 1017
  ]
  edge [
    source 24
    target 1018
  ]
  edge [
    source 24
    target 1019
  ]
  edge [
    source 24
    target 97
  ]
  edge [
    source 24
    target 1020
  ]
  edge [
    source 24
    target 1021
  ]
  edge [
    source 24
    target 801
  ]
  edge [
    source 24
    target 1022
  ]
  edge [
    source 24
    target 1023
  ]
  edge [
    source 24
    target 1024
  ]
  edge [
    source 24
    target 1025
  ]
  edge [
    source 24
    target 1026
  ]
  edge [
    source 24
    target 1027
  ]
  edge [
    source 24
    target 1028
  ]
  edge [
    source 24
    target 1029
  ]
  edge [
    source 24
    target 686
  ]
  edge [
    source 24
    target 1030
  ]
  edge [
    source 24
    target 1031
  ]
  edge [
    source 24
    target 1032
  ]
  edge [
    source 24
    target 1033
  ]
  edge [
    source 24
    target 1034
  ]
  edge [
    source 24
    target 1035
  ]
  edge [
    source 24
    target 1036
  ]
  edge [
    source 24
    target 1037
  ]
  edge [
    source 24
    target 1038
  ]
  edge [
    source 24
    target 1039
  ]
  edge [
    source 24
    target 1040
  ]
  edge [
    source 24
    target 1041
  ]
  edge [
    source 24
    target 1042
  ]
  edge [
    source 24
    target 1043
  ]
  edge [
    source 24
    target 1044
  ]
  edge [
    source 24
    target 1045
  ]
  edge [
    source 24
    target 1046
  ]
  edge [
    source 24
    target 1047
  ]
  edge [
    source 24
    target 1048
  ]
  edge [
    source 24
    target 1049
  ]
  edge [
    source 24
    target 1050
  ]
  edge [
    source 24
    target 1051
  ]
  edge [
    source 24
    target 1052
  ]
  edge [
    source 24
    target 1053
  ]
  edge [
    source 24
    target 602
  ]
  edge [
    source 24
    target 1054
  ]
  edge [
    source 24
    target 1055
  ]
  edge [
    source 24
    target 1056
  ]
  edge [
    source 24
    target 256
  ]
  edge [
    source 24
    target 1057
  ]
  edge [
    source 24
    target 414
  ]
  edge [
    source 24
    target 385
  ]
  edge [
    source 24
    target 1058
  ]
  edge [
    source 24
    target 1059
  ]
  edge [
    source 24
    target 1060
  ]
  edge [
    source 24
    target 1061
  ]
  edge [
    source 24
    target 416
  ]
  edge [
    source 24
    target 417
  ]
  edge [
    source 24
    target 349
  ]
  edge [
    source 24
    target 577
  ]
  edge [
    source 24
    target 418
  ]
  edge [
    source 24
    target 1062
  ]
  edge [
    source 24
    target 1063
  ]
  edge [
    source 24
    target 1064
  ]
  edge [
    source 24
    target 1065
  ]
  edge [
    source 24
    target 1066
  ]
  edge [
    source 24
    target 1067
  ]
  edge [
    source 24
    target 1068
  ]
  edge [
    source 24
    target 287
  ]
  edge [
    source 24
    target 1069
  ]
  edge [
    source 24
    target 1070
  ]
  edge [
    source 24
    target 1071
  ]
  edge [
    source 24
    target 1072
  ]
  edge [
    source 24
    target 270
  ]
  edge [
    source 24
    target 1073
  ]
  edge [
    source 24
    target 1074
  ]
  edge [
    source 24
    target 1075
  ]
  edge [
    source 24
    target 1076
  ]
  edge [
    source 24
    target 1077
  ]
  edge [
    source 24
    target 1078
  ]
  edge [
    source 24
    target 1079
  ]
  edge [
    source 24
    target 1080
  ]
  edge [
    source 24
    target 1081
  ]
  edge [
    source 24
    target 404
  ]
  edge [
    source 24
    target 1082
  ]
  edge [
    source 24
    target 1083
  ]
  edge [
    source 24
    target 1084
  ]
  edge [
    source 24
    target 1085
  ]
  edge [
    source 24
    target 1086
  ]
  edge [
    source 24
    target 1087
  ]
  edge [
    source 24
    target 1088
  ]
  edge [
    source 24
    target 1089
  ]
  edge [
    source 24
    target 1090
  ]
  edge [
    source 24
    target 1091
  ]
  edge [
    source 24
    target 1092
  ]
  edge [
    source 24
    target 1093
  ]
  edge [
    source 24
    target 1094
  ]
  edge [
    source 24
    target 58
  ]
  edge [
    source 24
    target 1095
  ]
  edge [
    source 24
    target 1096
  ]
  edge [
    source 24
    target 1097
  ]
  edge [
    source 24
    target 1098
  ]
  edge [
    source 24
    target 1099
  ]
  edge [
    source 24
    target 1100
  ]
  edge [
    source 24
    target 403
  ]
  edge [
    source 24
    target 1101
  ]
  edge [
    source 24
    target 1102
  ]
  edge [
    source 24
    target 1103
  ]
  edge [
    source 24
    target 1104
  ]
  edge [
    source 24
    target 1105
  ]
  edge [
    source 24
    target 1106
  ]
  edge [
    source 24
    target 1107
  ]
  edge [
    source 24
    target 1108
  ]
  edge [
    source 24
    target 1109
  ]
  edge [
    source 24
    target 1110
  ]
  edge [
    source 24
    target 1111
  ]
  edge [
    source 24
    target 1112
  ]
  edge [
    source 24
    target 1113
  ]
  edge [
    source 24
    target 1114
  ]
  edge [
    source 24
    target 1115
  ]
  edge [
    source 24
    target 1116
  ]
  edge [
    source 24
    target 1117
  ]
  edge [
    source 24
    target 1118
  ]
  edge [
    source 24
    target 1119
  ]
  edge [
    source 24
    target 1120
  ]
  edge [
    source 24
    target 1121
  ]
  edge [
    source 24
    target 1122
  ]
  edge [
    source 24
    target 544
  ]
  edge [
    source 24
    target 1123
  ]
  edge [
    source 24
    target 1124
  ]
  edge [
    source 24
    target 1125
  ]
  edge [
    source 24
    target 1126
  ]
  edge [
    source 24
    target 1127
  ]
  edge [
    source 24
    target 1128
  ]
  edge [
    source 24
    target 1129
  ]
  edge [
    source 24
    target 1130
  ]
  edge [
    source 24
    target 1131
  ]
  edge [
    source 24
    target 1132
  ]
  edge [
    source 24
    target 1133
  ]
  edge [
    source 24
    target 1134
  ]
  edge [
    source 24
    target 1135
  ]
  edge [
    source 24
    target 1136
  ]
  edge [
    source 24
    target 1137
  ]
  edge [
    source 24
    target 1138
  ]
  edge [
    source 24
    target 1139
  ]
  edge [
    source 24
    target 1140
  ]
  edge [
    source 24
    target 1141
  ]
  edge [
    source 24
    target 1142
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 96
  ]
  edge [
    source 25
    target 97
  ]
  edge [
    source 25
    target 98
  ]
  edge [
    source 25
    target 99
  ]
  edge [
    source 25
    target 100
  ]
  edge [
    source 25
    target 101
  ]
  edge [
    source 25
    target 102
  ]
  edge [
    source 25
    target 103
  ]
  edge [
    source 25
    target 104
  ]
  edge [
    source 25
    target 105
  ]
  edge [
    source 25
    target 106
  ]
  edge [
    source 25
    target 107
  ]
  edge [
    source 25
    target 108
  ]
  edge [
    source 25
    target 109
  ]
  edge [
    source 25
    target 110
  ]
  edge [
    source 25
    target 111
  ]
  edge [
    source 25
    target 112
  ]
  edge [
    source 25
    target 113
  ]
  edge [
    source 25
    target 114
  ]
  edge [
    source 25
    target 115
  ]
  edge [
    source 25
    target 116
  ]
  edge [
    source 25
    target 117
  ]
  edge [
    source 25
    target 118
  ]
  edge [
    source 25
    target 119
  ]
  edge [
    source 25
    target 120
  ]
  edge [
    source 25
    target 1143
  ]
  edge [
    source 25
    target 1144
  ]
  edge [
    source 25
    target 1145
  ]
  edge [
    source 25
    target 142
  ]
  edge [
    source 25
    target 580
  ]
  edge [
    source 25
    target 1146
  ]
  edge [
    source 25
    target 1147
  ]
  edge [
    source 25
    target 1148
  ]
  edge [
    source 25
    target 1149
  ]
  edge [
    source 25
    target 1150
  ]
  edge [
    source 25
    target 1151
  ]
  edge [
    source 25
    target 1152
  ]
  edge [
    source 25
    target 1153
  ]
  edge [
    source 25
    target 1154
  ]
  edge [
    source 25
    target 1155
  ]
  edge [
    source 25
    target 1156
  ]
  edge [
    source 25
    target 1015
  ]
  edge [
    source 25
    target 1157
  ]
  edge [
    source 25
    target 1158
  ]
  edge [
    source 25
    target 1159
  ]
  edge [
    source 25
    target 1160
  ]
  edge [
    source 25
    target 1161
  ]
  edge [
    source 25
    target 1162
  ]
  edge [
    source 25
    target 1163
  ]
  edge [
    source 25
    target 1164
  ]
  edge [
    source 25
    target 1165
  ]
  edge [
    source 25
    target 1166
  ]
  edge [
    source 25
    target 1167
  ]
  edge [
    source 25
    target 1168
  ]
  edge [
    source 25
    target 489
  ]
  edge [
    source 25
    target 1169
  ]
  edge [
    source 25
    target 1170
  ]
  edge [
    source 25
    target 1171
  ]
  edge [
    source 25
    target 1172
  ]
  edge [
    source 25
    target 1173
  ]
  edge [
    source 25
    target 1174
  ]
  edge [
    source 25
    target 1175
  ]
  edge [
    source 25
    target 1176
  ]
  edge [
    source 25
    target 1177
  ]
  edge [
    source 25
    target 1178
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 1179
  ]
  edge [
    source 25
    target 1180
  ]
  edge [
    source 25
    target 1181
  ]
  edge [
    source 25
    target 1182
  ]
  edge [
    source 25
    target 1183
  ]
  edge [
    source 25
    target 1184
  ]
  edge [
    source 25
    target 1185
  ]
  edge [
    source 25
    target 1186
  ]
  edge [
    source 25
    target 1187
  ]
  edge [
    source 25
    target 1188
  ]
  edge [
    source 25
    target 1189
  ]
  edge [
    source 25
    target 1190
  ]
  edge [
    source 25
    target 1191
  ]
  edge [
    source 25
    target 256
  ]
  edge [
    source 25
    target 562
  ]
  edge [
    source 25
    target 1192
  ]
  edge [
    source 25
    target 1193
  ]
  edge [
    source 25
    target 829
  ]
  edge [
    source 25
    target 830
  ]
  edge [
    source 25
    target 831
  ]
  edge [
    source 25
    target 832
  ]
  edge [
    source 25
    target 833
  ]
  edge [
    source 25
    target 834
  ]
  edge [
    source 25
    target 73
  ]
  edge [
    source 25
    target 813
  ]
  edge [
    source 25
    target 835
  ]
  edge [
    source 25
    target 816
  ]
  edge [
    source 25
    target 488
  ]
  edge [
    source 25
    target 92
  ]
  edge [
    source 25
    target 818
  ]
  edge [
    source 25
    target 1194
  ]
  edge [
    source 25
    target 1195
  ]
  edge [
    source 25
    target 1196
  ]
  edge [
    source 25
    target 1197
  ]
  edge [
    source 25
    target 1198
  ]
  edge [
    source 25
    target 1199
  ]
  edge [
    source 25
    target 1200
  ]
  edge [
    source 25
    target 1201
  ]
  edge [
    source 25
    target 1202
  ]
  edge [
    source 25
    target 1203
  ]
  edge [
    source 25
    target 1204
  ]
  edge [
    source 25
    target 44
  ]
  edge [
    source 25
    target 1205
  ]
  edge [
    source 25
    target 1206
  ]
  edge [
    source 25
    target 1207
  ]
  edge [
    source 25
    target 1208
  ]
  edge [
    source 25
    target 1209
  ]
  edge [
    source 25
    target 924
  ]
  edge [
    source 25
    target 1210
  ]
  edge [
    source 25
    target 1211
  ]
  edge [
    source 25
    target 1212
  ]
  edge [
    source 25
    target 1213
  ]
  edge [
    source 25
    target 1214
  ]
  edge [
    source 25
    target 1215
  ]
  edge [
    source 25
    target 1216
  ]
  edge [
    source 25
    target 199
  ]
  edge [
    source 25
    target 1217
  ]
  edge [
    source 25
    target 662
  ]
  edge [
    source 25
    target 1218
  ]
  edge [
    source 25
    target 358
  ]
  edge [
    source 25
    target 1219
  ]
  edge [
    source 25
    target 1220
  ]
  edge [
    source 25
    target 1221
  ]
  edge [
    source 25
    target 1222
  ]
  edge [
    source 25
    target 869
  ]
  edge [
    source 25
    target 1223
  ]
  edge [
    source 25
    target 1224
  ]
  edge [
    source 25
    target 1225
  ]
  edge [
    source 25
    target 1226
  ]
  edge [
    source 25
    target 1227
  ]
  edge [
    source 25
    target 1228
  ]
  edge [
    source 25
    target 1229
  ]
  edge [
    source 25
    target 1230
  ]
  edge [
    source 25
    target 1231
  ]
  edge [
    source 25
    target 1232
  ]
  edge [
    source 25
    target 1233
  ]
  edge [
    source 25
    target 1234
  ]
  edge [
    source 25
    target 456
  ]
  edge [
    source 25
    target 537
  ]
  edge [
    source 25
    target 371
  ]
  edge [
    source 25
    target 1235
  ]
  edge [
    source 25
    target 1236
  ]
  edge [
    source 25
    target 1237
  ]
  edge [
    source 25
    target 1238
  ]
  edge [
    source 25
    target 1239
  ]
  edge [
    source 25
    target 763
  ]
  edge [
    source 25
    target 1240
  ]
  edge [
    source 25
    target 1241
  ]
  edge [
    source 25
    target 1242
  ]
  edge [
    source 25
    target 1243
  ]
  edge [
    source 25
    target 1244
  ]
  edge [
    source 25
    target 1245
  ]
  edge [
    source 25
    target 1246
  ]
  edge [
    source 25
    target 1247
  ]
  edge [
    source 25
    target 1248
  ]
  edge [
    source 25
    target 1249
  ]
  edge [
    source 25
    target 152
  ]
  edge [
    source 25
    target 1250
  ]
  edge [
    source 25
    target 1251
  ]
  edge [
    source 25
    target 1252
  ]
  edge [
    source 25
    target 1253
  ]
  edge [
    source 25
    target 1254
  ]
  edge [
    source 25
    target 1255
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 1256
  ]
  edge [
    source 25
    target 1257
  ]
  edge [
    source 25
    target 1258
  ]
  edge [
    source 25
    target 1259
  ]
  edge [
    source 25
    target 1260
  ]
  edge [
    source 25
    target 1261
  ]
  edge [
    source 25
    target 1262
  ]
  edge [
    source 25
    target 1263
  ]
  edge [
    source 25
    target 1264
  ]
  edge [
    source 25
    target 1265
  ]
  edge [
    source 25
    target 1266
  ]
  edge [
    source 25
    target 400
  ]
  edge [
    source 25
    target 1267
  ]
  edge [
    source 25
    target 1268
  ]
  edge [
    source 25
    target 1269
  ]
  edge [
    source 25
    target 1270
  ]
  edge [
    source 25
    target 1271
  ]
  edge [
    source 25
    target 1272
  ]
  edge [
    source 25
    target 123
  ]
  edge [
    source 25
    target 430
  ]
  edge [
    source 25
    target 840
  ]
  edge [
    source 25
    target 1273
  ]
  edge [
    source 25
    target 1274
  ]
  edge [
    source 25
    target 1275
  ]
  edge [
    source 25
    target 1022
  ]
  edge [
    source 25
    target 1276
  ]
  edge [
    source 25
    target 473
  ]
  edge [
    source 25
    target 1277
  ]
  edge [
    source 25
    target 1278
  ]
  edge [
    source 25
    target 1279
  ]
  edge [
    source 25
    target 1280
  ]
  edge [
    source 25
    target 138
  ]
  edge [
    source 25
    target 1281
  ]
  edge [
    source 25
    target 1282
  ]
  edge [
    source 25
    target 1283
  ]
  edge [
    source 25
    target 1284
  ]
  edge [
    source 25
    target 1285
  ]
  edge [
    source 25
    target 1286
  ]
  edge [
    source 25
    target 1287
  ]
  edge [
    source 25
    target 1288
  ]
  edge [
    source 25
    target 1289
  ]
  edge [
    source 25
    target 330
  ]
  edge [
    source 25
    target 1290
  ]
  edge [
    source 25
    target 1291
  ]
  edge [
    source 25
    target 1292
  ]
  edge [
    source 25
    target 1293
  ]
  edge [
    source 25
    target 1294
  ]
  edge [
    source 25
    target 1295
  ]
  edge [
    source 25
    target 1296
  ]
  edge [
    source 25
    target 576
  ]
  edge [
    source 25
    target 1297
  ]
  edge [
    source 25
    target 1298
  ]
  edge [
    source 25
    target 1299
  ]
  edge [
    source 25
    target 1300
  ]
  edge [
    source 25
    target 1301
  ]
  edge [
    source 25
    target 1302
  ]
  edge [
    source 25
    target 1303
  ]
  edge [
    source 25
    target 1304
  ]
  edge [
    source 25
    target 1305
  ]
  edge [
    source 25
    target 1306
  ]
  edge [
    source 25
    target 1307
  ]
  edge [
    source 25
    target 1308
  ]
  edge [
    source 25
    target 1309
  ]
  edge [
    source 25
    target 1310
  ]
  edge [
    source 25
    target 1311
  ]
  edge [
    source 25
    target 1312
  ]
  edge [
    source 25
    target 467
  ]
  edge [
    source 25
    target 1313
  ]
  edge [
    source 25
    target 1314
  ]
  edge [
    source 25
    target 1315
  ]
  edge [
    source 25
    target 1316
  ]
  edge [
    source 25
    target 709
  ]
  edge [
    source 25
    target 1317
  ]
  edge [
    source 25
    target 1318
  ]
  edge [
    source 25
    target 1319
  ]
  edge [
    source 25
    target 512
  ]
  edge [
    source 25
    target 482
  ]
  edge [
    source 25
    target 1320
  ]
  edge [
    source 25
    target 1321
  ]
  edge [
    source 25
    target 1322
  ]
  edge [
    source 25
    target 1323
  ]
  edge [
    source 25
    target 1324
  ]
  edge [
    source 25
    target 1325
  ]
  edge [
    source 25
    target 1326
  ]
  edge [
    source 25
    target 1327
  ]
  edge [
    source 25
    target 93
  ]
  edge [
    source 25
    target 1328
  ]
  edge [
    source 25
    target 1329
  ]
  edge [
    source 25
    target 1330
  ]
  edge [
    source 25
    target 1331
  ]
  edge [
    source 25
    target 1332
  ]
  edge [
    source 25
    target 1333
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1334
  ]
  edge [
    source 26
    target 1335
  ]
  edge [
    source 26
    target 1336
  ]
  edge [
    source 26
    target 722
  ]
  edge [
    source 26
    target 1337
  ]
  edge [
    source 26
    target 706
  ]
  edge [
    source 26
    target 1338
  ]
  edge [
    source 26
    target 1339
  ]
  edge [
    source 26
    target 1340
  ]
  edge [
    source 27
    target 823
  ]
  edge [
    source 27
    target 1341
  ]
  edge [
    source 27
    target 1342
  ]
  edge [
    source 27
    target 1343
  ]
  edge [
    source 27
    target 1344
  ]
  edge [
    source 27
    target 1345
  ]
  edge [
    source 27
    target 1346
  ]
  edge [
    source 27
    target 1347
  ]
  edge [
    source 27
    target 1348
  ]
  edge [
    source 27
    target 1349
  ]
  edge [
    source 27
    target 803
  ]
  edge [
    source 27
    target 1350
  ]
  edge [
    source 27
    target 1351
  ]
  edge [
    source 27
    target 1352
  ]
  edge [
    source 27
    target 1353
  ]
  edge [
    source 27
    target 1354
  ]
  edge [
    source 27
    target 1355
  ]
  edge [
    source 27
    target 1356
  ]
]
