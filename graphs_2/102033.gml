graph [
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "redakcja"
    origin "text"
  ]
  node [
    id 2
    label "qmama"
    origin "text"
  ]
  node [
    id 3
    label "autor"
    origin "text"
  ]
  node [
    id 4
    label "maja"
    origin "text"
  ]
  node [
    id 5
    label "szansa"
    origin "text"
  ]
  node [
    id 6
    label "wygrana"
    origin "text"
  ]
  node [
    id 7
    label "konkurs"
    origin "text"
  ]
  node [
    id 8
    label "fundacja"
    origin "text"
  ]
  node [
    id 9
    label "nowa"
    origin "text"
  ]
  node [
    id 10
    label "media"
    origin "text"
  ]
  node [
    id 11
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 12
    label "rozpoczyna&#263;"
    origin "text"
  ]
  node [
    id 13
    label "si&#281;"
    origin "text"
  ]
  node [
    id 14
    label "pierwsza"
    origin "text"
  ]
  node [
    id 15
    label "edycja"
    origin "text"
  ]
  node [
    id 16
    label "pocz&#261;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "maj"
    origin "text"
  ]
  node [
    id 18
    label "raz"
    origin "text"
  ]
  node [
    id 19
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 20
    label "jura"
    origin "text"
  ]
  node [
    id 21
    label "wskaza&#263;"
    origin "text"
  ]
  node [
    id 22
    label "ciekawy"
    origin "text"
  ]
  node [
    id 23
    label "materia&#322;"
    origin "text"
  ]
  node [
    id 24
    label "nagrodzi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "sprawny"
    origin "text"
  ]
  node [
    id 26
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 27
    label "projekt"
    origin "text"
  ]
  node [
    id 28
    label "czo&#322;owy"
    origin "text"
  ]
  node [
    id 29
    label "polski"
    origin "text"
  ]
  node [
    id 30
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 31
    label "przyznawa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "dobry"
    origin "text"
  ]
  node [
    id 33
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 34
    label "qmam&#243;w"
    origin "text"
  ]
  node [
    id 35
    label "rok"
    origin "text"
  ]
  node [
    id 36
    label "jaki&#347;"
  ]
  node [
    id 37
    label "przyzwoity"
  ]
  node [
    id 38
    label "jako&#347;"
  ]
  node [
    id 39
    label "jako_tako"
  ]
  node [
    id 40
    label "niez&#322;y"
  ]
  node [
    id 41
    label "dziwny"
  ]
  node [
    id 42
    label "charakterystyczny"
  ]
  node [
    id 43
    label "redaktor"
  ]
  node [
    id 44
    label "radio"
  ]
  node [
    id 45
    label "zesp&#243;&#322;"
  ]
  node [
    id 46
    label "siedziba"
  ]
  node [
    id 47
    label "composition"
  ]
  node [
    id 48
    label "wydawnictwo"
  ]
  node [
    id 49
    label "redaction"
  ]
  node [
    id 50
    label "tekst"
  ]
  node [
    id 51
    label "telewizja"
  ]
  node [
    id 52
    label "obr&#243;bka"
  ]
  node [
    id 53
    label "Mazowsze"
  ]
  node [
    id 54
    label "odm&#322;adzanie"
  ]
  node [
    id 55
    label "&#346;wietliki"
  ]
  node [
    id 56
    label "zbi&#243;r"
  ]
  node [
    id 57
    label "whole"
  ]
  node [
    id 58
    label "skupienie"
  ]
  node [
    id 59
    label "The_Beatles"
  ]
  node [
    id 60
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 61
    label "odm&#322;adza&#263;"
  ]
  node [
    id 62
    label "zabudowania"
  ]
  node [
    id 63
    label "group"
  ]
  node [
    id 64
    label "zespolik"
  ]
  node [
    id 65
    label "schorzenie"
  ]
  node [
    id 66
    label "ro&#347;lina"
  ]
  node [
    id 67
    label "grupa"
  ]
  node [
    id 68
    label "Depeche_Mode"
  ]
  node [
    id 69
    label "batch"
  ]
  node [
    id 70
    label "odm&#322;odzenie"
  ]
  node [
    id 71
    label "&#321;ubianka"
  ]
  node [
    id 72
    label "miejsce_pracy"
  ]
  node [
    id 73
    label "dzia&#322;_personalny"
  ]
  node [
    id 74
    label "Kreml"
  ]
  node [
    id 75
    label "Bia&#322;y_Dom"
  ]
  node [
    id 76
    label "budynek"
  ]
  node [
    id 77
    label "miejsce"
  ]
  node [
    id 78
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 79
    label "sadowisko"
  ]
  node [
    id 80
    label "proces_technologiczny"
  ]
  node [
    id 81
    label "czynno&#347;&#263;"
  ]
  node [
    id 82
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 83
    label "proces"
  ]
  node [
    id 84
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 85
    label "cz&#322;owiek"
  ]
  node [
    id 86
    label "bran&#380;owiec"
  ]
  node [
    id 87
    label "edytor"
  ]
  node [
    id 88
    label "debit"
  ]
  node [
    id 89
    label "druk"
  ]
  node [
    id 90
    label "publikacja"
  ]
  node [
    id 91
    label "szata_graficzna"
  ]
  node [
    id 92
    label "firma"
  ]
  node [
    id 93
    label "wydawa&#263;"
  ]
  node [
    id 94
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 95
    label "wyda&#263;"
  ]
  node [
    id 96
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 97
    label "poster"
  ]
  node [
    id 98
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 99
    label "paj&#281;czarz"
  ]
  node [
    id 100
    label "radiola"
  ]
  node [
    id 101
    label "programowiec"
  ]
  node [
    id 102
    label "spot"
  ]
  node [
    id 103
    label "stacja"
  ]
  node [
    id 104
    label "uk&#322;ad"
  ]
  node [
    id 105
    label "odbiornik"
  ]
  node [
    id 106
    label "eliminator"
  ]
  node [
    id 107
    label "radiolinia"
  ]
  node [
    id 108
    label "fala_radiowa"
  ]
  node [
    id 109
    label "radiofonia"
  ]
  node [
    id 110
    label "odbieranie"
  ]
  node [
    id 111
    label "studio"
  ]
  node [
    id 112
    label "dyskryminator"
  ]
  node [
    id 113
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 114
    label "odbiera&#263;"
  ]
  node [
    id 115
    label "telekomunikacja"
  ]
  node [
    id 116
    label "ekran"
  ]
  node [
    id 117
    label "BBC"
  ]
  node [
    id 118
    label "Interwizja"
  ]
  node [
    id 119
    label "instytucja"
  ]
  node [
    id 120
    label "Polsat"
  ]
  node [
    id 121
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 122
    label "muza"
  ]
  node [
    id 123
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 124
    label "technologia"
  ]
  node [
    id 125
    label "ekscerpcja"
  ]
  node [
    id 126
    label "j&#281;zykowo"
  ]
  node [
    id 127
    label "wypowied&#378;"
  ]
  node [
    id 128
    label "wytw&#243;r"
  ]
  node [
    id 129
    label "pomini&#281;cie"
  ]
  node [
    id 130
    label "dzie&#322;o"
  ]
  node [
    id 131
    label "preparacja"
  ]
  node [
    id 132
    label "odmianka"
  ]
  node [
    id 133
    label "opu&#347;ci&#263;"
  ]
  node [
    id 134
    label "koniektura"
  ]
  node [
    id 135
    label "pisa&#263;"
  ]
  node [
    id 136
    label "obelga"
  ]
  node [
    id 137
    label "kszta&#322;ciciel"
  ]
  node [
    id 138
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 139
    label "tworzyciel"
  ]
  node [
    id 140
    label "wykonawca"
  ]
  node [
    id 141
    label "pomys&#322;odawca"
  ]
  node [
    id 142
    label "&#347;w"
  ]
  node [
    id 143
    label "inicjator"
  ]
  node [
    id 144
    label "podmiot_gospodarczy"
  ]
  node [
    id 145
    label "artysta"
  ]
  node [
    id 146
    label "muzyk"
  ]
  node [
    id 147
    label "nauczyciel"
  ]
  node [
    id 148
    label "energia"
  ]
  node [
    id 149
    label "wedyzm"
  ]
  node [
    id 150
    label "buddyzm"
  ]
  node [
    id 151
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 152
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 153
    label "egzergia"
  ]
  node [
    id 154
    label "emitowa&#263;"
  ]
  node [
    id 155
    label "kwant_energii"
  ]
  node [
    id 156
    label "szwung"
  ]
  node [
    id 157
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 158
    label "power"
  ]
  node [
    id 159
    label "zjawisko"
  ]
  node [
    id 160
    label "cecha"
  ]
  node [
    id 161
    label "emitowanie"
  ]
  node [
    id 162
    label "energy"
  ]
  node [
    id 163
    label "kalpa"
  ]
  node [
    id 164
    label "lampka_ma&#347;lana"
  ]
  node [
    id 165
    label "Buddhism"
  ]
  node [
    id 166
    label "dana"
  ]
  node [
    id 167
    label "mahajana"
  ]
  node [
    id 168
    label "asura"
  ]
  node [
    id 169
    label "wad&#378;rajana"
  ]
  node [
    id 170
    label "bonzo"
  ]
  node [
    id 171
    label "therawada"
  ]
  node [
    id 172
    label "tantryzm"
  ]
  node [
    id 173
    label "hinajana"
  ]
  node [
    id 174
    label "bardo"
  ]
  node [
    id 175
    label "arahant"
  ]
  node [
    id 176
    label "religia"
  ]
  node [
    id 177
    label "ahinsa"
  ]
  node [
    id 178
    label "li"
  ]
  node [
    id 179
    label "hinduizm"
  ]
  node [
    id 180
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 181
    label "posiada&#263;"
  ]
  node [
    id 182
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 183
    label "wydarzenie"
  ]
  node [
    id 184
    label "egzekutywa"
  ]
  node [
    id 185
    label "potencja&#322;"
  ]
  node [
    id 186
    label "wyb&#243;r"
  ]
  node [
    id 187
    label "prospect"
  ]
  node [
    id 188
    label "ability"
  ]
  node [
    id 189
    label "obliczeniowo"
  ]
  node [
    id 190
    label "alternatywa"
  ]
  node [
    id 191
    label "operator_modalny"
  ]
  node [
    id 192
    label "puchar"
  ]
  node [
    id 193
    label "przedmiot"
  ]
  node [
    id 194
    label "korzy&#347;&#263;"
  ]
  node [
    id 195
    label "sukces"
  ]
  node [
    id 196
    label "conquest"
  ]
  node [
    id 197
    label "kobieta_sukcesu"
  ]
  node [
    id 198
    label "success"
  ]
  node [
    id 199
    label "rezultat"
  ]
  node [
    id 200
    label "passa"
  ]
  node [
    id 201
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 202
    label "zaleta"
  ]
  node [
    id 203
    label "dobro"
  ]
  node [
    id 204
    label "zboczenie"
  ]
  node [
    id 205
    label "om&#243;wienie"
  ]
  node [
    id 206
    label "sponiewieranie"
  ]
  node [
    id 207
    label "discipline"
  ]
  node [
    id 208
    label "rzecz"
  ]
  node [
    id 209
    label "omawia&#263;"
  ]
  node [
    id 210
    label "kr&#261;&#380;enie"
  ]
  node [
    id 211
    label "tre&#347;&#263;"
  ]
  node [
    id 212
    label "robienie"
  ]
  node [
    id 213
    label "sponiewiera&#263;"
  ]
  node [
    id 214
    label "element"
  ]
  node [
    id 215
    label "entity"
  ]
  node [
    id 216
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 217
    label "tematyka"
  ]
  node [
    id 218
    label "w&#261;tek"
  ]
  node [
    id 219
    label "charakter"
  ]
  node [
    id 220
    label "zbaczanie"
  ]
  node [
    id 221
    label "program_nauczania"
  ]
  node [
    id 222
    label "om&#243;wi&#263;"
  ]
  node [
    id 223
    label "omawianie"
  ]
  node [
    id 224
    label "thing"
  ]
  node [
    id 225
    label "kultura"
  ]
  node [
    id 226
    label "istota"
  ]
  node [
    id 227
    label "zbacza&#263;"
  ]
  node [
    id 228
    label "zboczy&#263;"
  ]
  node [
    id 229
    label "naczynie"
  ]
  node [
    id 230
    label "nagroda"
  ]
  node [
    id 231
    label "zwyci&#281;stwo"
  ]
  node [
    id 232
    label "zawody"
  ]
  node [
    id 233
    label "zawarto&#347;&#263;"
  ]
  node [
    id 234
    label "casting"
  ]
  node [
    id 235
    label "nab&#243;r"
  ]
  node [
    id 236
    label "Eurowizja"
  ]
  node [
    id 237
    label "eliminacje"
  ]
  node [
    id 238
    label "impreza"
  ]
  node [
    id 239
    label "emulation"
  ]
  node [
    id 240
    label "impra"
  ]
  node [
    id 241
    label "rozrywka"
  ]
  node [
    id 242
    label "przyj&#281;cie"
  ]
  node [
    id 243
    label "okazja"
  ]
  node [
    id 244
    label "party"
  ]
  node [
    id 245
    label "recruitment"
  ]
  node [
    id 246
    label "faza"
  ]
  node [
    id 247
    label "runda"
  ]
  node [
    id 248
    label "turniej"
  ]
  node [
    id 249
    label "retirement"
  ]
  node [
    id 250
    label "przes&#322;uchanie"
  ]
  node [
    id 251
    label "w&#281;dkarstwo"
  ]
  node [
    id 252
    label "darowizna"
  ]
  node [
    id 253
    label "foundation"
  ]
  node [
    id 254
    label "dar"
  ]
  node [
    id 255
    label "pocz&#261;tek"
  ]
  node [
    id 256
    label "osoba_prawna"
  ]
  node [
    id 257
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 258
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 259
    label "poj&#281;cie"
  ]
  node [
    id 260
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 261
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 262
    label "biuro"
  ]
  node [
    id 263
    label "organizacja"
  ]
  node [
    id 264
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 265
    label "Fundusze_Unijne"
  ]
  node [
    id 266
    label "zamyka&#263;"
  ]
  node [
    id 267
    label "establishment"
  ]
  node [
    id 268
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 269
    label "urz&#261;d"
  ]
  node [
    id 270
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 271
    label "afiliowa&#263;"
  ]
  node [
    id 272
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 273
    label "standard"
  ]
  node [
    id 274
    label "zamykanie"
  ]
  node [
    id 275
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 276
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 277
    label "pierworodztwo"
  ]
  node [
    id 278
    label "upgrade"
  ]
  node [
    id 279
    label "nast&#281;pstwo"
  ]
  node [
    id 280
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 281
    label "przeniesienie_praw"
  ]
  node [
    id 282
    label "zapomoga"
  ]
  node [
    id 283
    label "transakcja"
  ]
  node [
    id 284
    label "dyspozycja"
  ]
  node [
    id 285
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 286
    label "da&#324;"
  ]
  node [
    id 287
    label "faculty"
  ]
  node [
    id 288
    label "stygmat"
  ]
  node [
    id 289
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 290
    label "gwiazda"
  ]
  node [
    id 291
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 292
    label "Arktur"
  ]
  node [
    id 293
    label "kszta&#322;t"
  ]
  node [
    id 294
    label "Gwiazda_Polarna"
  ]
  node [
    id 295
    label "agregatka"
  ]
  node [
    id 296
    label "gromada"
  ]
  node [
    id 297
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 298
    label "S&#322;o&#324;ce"
  ]
  node [
    id 299
    label "Nibiru"
  ]
  node [
    id 300
    label "konstelacja"
  ]
  node [
    id 301
    label "ornament"
  ]
  node [
    id 302
    label "delta_Scuti"
  ]
  node [
    id 303
    label "&#347;wiat&#322;o"
  ]
  node [
    id 304
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 305
    label "obiekt"
  ]
  node [
    id 306
    label "s&#322;awa"
  ]
  node [
    id 307
    label "promie&#324;"
  ]
  node [
    id 308
    label "star"
  ]
  node [
    id 309
    label "gwiazdosz"
  ]
  node [
    id 310
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 311
    label "asocjacja_gwiazd"
  ]
  node [
    id 312
    label "supergrupa"
  ]
  node [
    id 313
    label "mass-media"
  ]
  node [
    id 314
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 315
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 316
    label "przekazior"
  ]
  node [
    id 317
    label "uzbrajanie"
  ]
  node [
    id 318
    label "medium"
  ]
  node [
    id 319
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 320
    label "&#347;rodek"
  ]
  node [
    id 321
    label "jasnowidz"
  ]
  node [
    id 322
    label "hipnoza"
  ]
  node [
    id 323
    label "spirytysta"
  ]
  node [
    id 324
    label "otoczenie"
  ]
  node [
    id 325
    label "publikator"
  ]
  node [
    id 326
    label "warunki"
  ]
  node [
    id 327
    label "strona"
  ]
  node [
    id 328
    label "przeka&#378;nik"
  ]
  node [
    id 329
    label "&#347;rodek_przekazu"
  ]
  node [
    id 330
    label "armament"
  ]
  node [
    id 331
    label "arming"
  ]
  node [
    id 332
    label "instalacja"
  ]
  node [
    id 333
    label "wyposa&#380;anie"
  ]
  node [
    id 334
    label "dozbrajanie"
  ]
  node [
    id 335
    label "dozbrojenie"
  ]
  node [
    id 336
    label "montowanie"
  ]
  node [
    id 337
    label "dok&#322;adnie"
  ]
  node [
    id 338
    label "meticulously"
  ]
  node [
    id 339
    label "punctiliously"
  ]
  node [
    id 340
    label "precyzyjnie"
  ]
  node [
    id 341
    label "dok&#322;adny"
  ]
  node [
    id 342
    label "rzetelnie"
  ]
  node [
    id 343
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 344
    label "start"
  ]
  node [
    id 345
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 346
    label "begin"
  ]
  node [
    id 347
    label "sprawowa&#263;"
  ]
  node [
    id 348
    label "prosecute"
  ]
  node [
    id 349
    label "by&#263;"
  ]
  node [
    id 350
    label "lot"
  ]
  node [
    id 351
    label "rozpocz&#281;cie"
  ]
  node [
    id 352
    label "uczestnictwo"
  ]
  node [
    id 353
    label "okno_startowe"
  ]
  node [
    id 354
    label "blok_startowy"
  ]
  node [
    id 355
    label "wy&#347;cig"
  ]
  node [
    id 356
    label "godzina"
  ]
  node [
    id 357
    label "time"
  ]
  node [
    id 358
    label "doba"
  ]
  node [
    id 359
    label "p&#243;&#322;godzina"
  ]
  node [
    id 360
    label "jednostka_czasu"
  ]
  node [
    id 361
    label "czas"
  ]
  node [
    id 362
    label "minuta"
  ]
  node [
    id 363
    label "kwadrans"
  ]
  node [
    id 364
    label "egzemplarz"
  ]
  node [
    id 365
    label "impression"
  ]
  node [
    id 366
    label "odmiana"
  ]
  node [
    id 367
    label "cykl"
  ]
  node [
    id 368
    label "notification"
  ]
  node [
    id 369
    label "zmiana"
  ]
  node [
    id 370
    label "produkcja"
  ]
  node [
    id 371
    label "mutant"
  ]
  node [
    id 372
    label "rewizja"
  ]
  node [
    id 373
    label "gramatyka"
  ]
  node [
    id 374
    label "typ"
  ]
  node [
    id 375
    label "paradygmat"
  ]
  node [
    id 376
    label "jednostka_systematyczna"
  ]
  node [
    id 377
    label "change"
  ]
  node [
    id 378
    label "podgatunek"
  ]
  node [
    id 379
    label "posta&#263;"
  ]
  node [
    id 380
    label "ferment"
  ]
  node [
    id 381
    label "rasa"
  ]
  node [
    id 382
    label "realizacja"
  ]
  node [
    id 383
    label "tingel-tangel"
  ]
  node [
    id 384
    label "numer"
  ]
  node [
    id 385
    label "monta&#380;"
  ]
  node [
    id 386
    label "postprodukcja"
  ]
  node [
    id 387
    label "performance"
  ]
  node [
    id 388
    label "fabrication"
  ]
  node [
    id 389
    label "product"
  ]
  node [
    id 390
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 391
    label "uzysk"
  ]
  node [
    id 392
    label "rozw&#243;j"
  ]
  node [
    id 393
    label "odtworzenie"
  ]
  node [
    id 394
    label "dorobek"
  ]
  node [
    id 395
    label "kreacja"
  ]
  node [
    id 396
    label "trema"
  ]
  node [
    id 397
    label "creation"
  ]
  node [
    id 398
    label "kooperowa&#263;"
  ]
  node [
    id 399
    label "czynnik_biotyczny"
  ]
  node [
    id 400
    label "wyewoluowanie"
  ]
  node [
    id 401
    label "reakcja"
  ]
  node [
    id 402
    label "individual"
  ]
  node [
    id 403
    label "przyswoi&#263;"
  ]
  node [
    id 404
    label "starzenie_si&#281;"
  ]
  node [
    id 405
    label "wyewoluowa&#263;"
  ]
  node [
    id 406
    label "okaz"
  ]
  node [
    id 407
    label "part"
  ]
  node [
    id 408
    label "ewoluowa&#263;"
  ]
  node [
    id 409
    label "przyswojenie"
  ]
  node [
    id 410
    label "ewoluowanie"
  ]
  node [
    id 411
    label "sztuka"
  ]
  node [
    id 412
    label "agent"
  ]
  node [
    id 413
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 414
    label "przyswaja&#263;"
  ]
  node [
    id 415
    label "nicpo&#324;"
  ]
  node [
    id 416
    label "przyswajanie"
  ]
  node [
    id 417
    label "passage"
  ]
  node [
    id 418
    label "oznaka"
  ]
  node [
    id 419
    label "komplet"
  ]
  node [
    id 420
    label "anatomopatolog"
  ]
  node [
    id 421
    label "zmianka"
  ]
  node [
    id 422
    label "amendment"
  ]
  node [
    id 423
    label "praca"
  ]
  node [
    id 424
    label "odmienianie"
  ]
  node [
    id 425
    label "tura"
  ]
  node [
    id 426
    label "set"
  ]
  node [
    id 427
    label "przebieg"
  ]
  node [
    id 428
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 429
    label "miesi&#261;czka"
  ]
  node [
    id 430
    label "okres"
  ]
  node [
    id 431
    label "owulacja"
  ]
  node [
    id 432
    label "sekwencja"
  ]
  node [
    id 433
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 434
    label "cycle"
  ]
  node [
    id 435
    label "zrobi&#263;"
  ]
  node [
    id 436
    label "do"
  ]
  node [
    id 437
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 438
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 439
    label "post&#261;pi&#263;"
  ]
  node [
    id 440
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 441
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 442
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 443
    label "zorganizowa&#263;"
  ]
  node [
    id 444
    label "appoint"
  ]
  node [
    id 445
    label "wystylizowa&#263;"
  ]
  node [
    id 446
    label "cause"
  ]
  node [
    id 447
    label "przerobi&#263;"
  ]
  node [
    id 448
    label "nabra&#263;"
  ]
  node [
    id 449
    label "make"
  ]
  node [
    id 450
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 451
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 452
    label "wydali&#263;"
  ]
  node [
    id 453
    label "ut"
  ]
  node [
    id 454
    label "d&#378;wi&#281;k"
  ]
  node [
    id 455
    label "C"
  ]
  node [
    id 456
    label "his"
  ]
  node [
    id 457
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 458
    label "tydzie&#324;"
  ]
  node [
    id 459
    label "miech"
  ]
  node [
    id 460
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 461
    label "kalendy"
  ]
  node [
    id 462
    label "cios"
  ]
  node [
    id 463
    label "chwila"
  ]
  node [
    id 464
    label "uderzenie"
  ]
  node [
    id 465
    label "blok"
  ]
  node [
    id 466
    label "shot"
  ]
  node [
    id 467
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 468
    label "struktura_geologiczna"
  ]
  node [
    id 469
    label "&#322;upliwo&#347;&#263;"
  ]
  node [
    id 470
    label "pr&#243;ba"
  ]
  node [
    id 471
    label "r&#243;&#380;a_skalna"
  ]
  node [
    id 472
    label "coup"
  ]
  node [
    id 473
    label "siekacz"
  ]
  node [
    id 474
    label "instrumentalizacja"
  ]
  node [
    id 475
    label "trafienie"
  ]
  node [
    id 476
    label "walka"
  ]
  node [
    id 477
    label "zdarzenie_si&#281;"
  ]
  node [
    id 478
    label "wdarcie_si&#281;"
  ]
  node [
    id 479
    label "pogorszenie"
  ]
  node [
    id 480
    label "poczucie"
  ]
  node [
    id 481
    label "contact"
  ]
  node [
    id 482
    label "stukni&#281;cie"
  ]
  node [
    id 483
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 484
    label "bat"
  ]
  node [
    id 485
    label "spowodowanie"
  ]
  node [
    id 486
    label "rush"
  ]
  node [
    id 487
    label "odbicie"
  ]
  node [
    id 488
    label "dawka"
  ]
  node [
    id 489
    label "zadanie"
  ]
  node [
    id 490
    label "&#347;ci&#281;cie"
  ]
  node [
    id 491
    label "st&#322;uczenie"
  ]
  node [
    id 492
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 493
    label "odbicie_si&#281;"
  ]
  node [
    id 494
    label "dotkni&#281;cie"
  ]
  node [
    id 495
    label "charge"
  ]
  node [
    id 496
    label "dostanie"
  ]
  node [
    id 497
    label "skrytykowanie"
  ]
  node [
    id 498
    label "zagrywka"
  ]
  node [
    id 499
    label "manewr"
  ]
  node [
    id 500
    label "nast&#261;pienie"
  ]
  node [
    id 501
    label "uderzanie"
  ]
  node [
    id 502
    label "pogoda"
  ]
  node [
    id 503
    label "stroke"
  ]
  node [
    id 504
    label "pobicie"
  ]
  node [
    id 505
    label "ruch"
  ]
  node [
    id 506
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 507
    label "flap"
  ]
  node [
    id 508
    label "dotyk"
  ]
  node [
    id 509
    label "zrobienie"
  ]
  node [
    id 510
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 511
    label "satelita"
  ]
  node [
    id 512
    label "peryselenium"
  ]
  node [
    id 513
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 514
    label "aposelenium"
  ]
  node [
    id 515
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 516
    label "Tytan"
  ]
  node [
    id 517
    label "moon"
  ]
  node [
    id 518
    label "aparat_fotograficzny"
  ]
  node [
    id 519
    label "bag"
  ]
  node [
    id 520
    label "sakwa"
  ]
  node [
    id 521
    label "torba"
  ]
  node [
    id 522
    label "przyrz&#261;d"
  ]
  node [
    id 523
    label "w&#243;r"
  ]
  node [
    id 524
    label "poprzedzanie"
  ]
  node [
    id 525
    label "czasoprzestrze&#324;"
  ]
  node [
    id 526
    label "laba"
  ]
  node [
    id 527
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 528
    label "chronometria"
  ]
  node [
    id 529
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 530
    label "rachuba_czasu"
  ]
  node [
    id 531
    label "przep&#322;ywanie"
  ]
  node [
    id 532
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 533
    label "czasokres"
  ]
  node [
    id 534
    label "odczyt"
  ]
  node [
    id 535
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 536
    label "dzieje"
  ]
  node [
    id 537
    label "kategoria_gramatyczna"
  ]
  node [
    id 538
    label "poprzedzenie"
  ]
  node [
    id 539
    label "trawienie"
  ]
  node [
    id 540
    label "pochodzi&#263;"
  ]
  node [
    id 541
    label "period"
  ]
  node [
    id 542
    label "okres_czasu"
  ]
  node [
    id 543
    label "poprzedza&#263;"
  ]
  node [
    id 544
    label "schy&#322;ek"
  ]
  node [
    id 545
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 546
    label "odwlekanie_si&#281;"
  ]
  node [
    id 547
    label "zegar"
  ]
  node [
    id 548
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 549
    label "czwarty_wymiar"
  ]
  node [
    id 550
    label "pochodzenie"
  ]
  node [
    id 551
    label "koniugacja"
  ]
  node [
    id 552
    label "Zeitgeist"
  ]
  node [
    id 553
    label "trawi&#263;"
  ]
  node [
    id 554
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 555
    label "poprzedzi&#263;"
  ]
  node [
    id 556
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 557
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 558
    label "time_period"
  ]
  node [
    id 559
    label "weekend"
  ]
  node [
    id 560
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 561
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 562
    label "p&#243;&#322;rocze"
  ]
  node [
    id 563
    label "martwy_sezon"
  ]
  node [
    id 564
    label "kalendarz"
  ]
  node [
    id 565
    label "cykl_astronomiczny"
  ]
  node [
    id 566
    label "lata"
  ]
  node [
    id 567
    label "pora_roku"
  ]
  node [
    id 568
    label "stulecie"
  ]
  node [
    id 569
    label "kurs"
  ]
  node [
    id 570
    label "jubileusz"
  ]
  node [
    id 571
    label "kwarta&#322;"
  ]
  node [
    id 572
    label "jura_wczesna"
  ]
  node [
    id 573
    label "era_mezozoiczna"
  ]
  node [
    id 574
    label "dogger"
  ]
  node [
    id 575
    label "plezjozaur"
  ]
  node [
    id 576
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 577
    label "euoplocefal"
  ]
  node [
    id 578
    label "formacja_geologiczna"
  ]
  node [
    id 579
    label "jura_&#347;rodkowa"
  ]
  node [
    id 580
    label "plezjozaury"
  ]
  node [
    id 581
    label "gad_morski"
  ]
  node [
    id 582
    label "ankylozaury"
  ]
  node [
    id 583
    label "tyreofory"
  ]
  node [
    id 584
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 585
    label "point"
  ]
  node [
    id 586
    label "pokaza&#263;"
  ]
  node [
    id 587
    label "poda&#263;"
  ]
  node [
    id 588
    label "picture"
  ]
  node [
    id 589
    label "aim"
  ]
  node [
    id 590
    label "wybra&#263;"
  ]
  node [
    id 591
    label "podkre&#347;li&#263;"
  ]
  node [
    id 592
    label "indicate"
  ]
  node [
    id 593
    label "clear"
  ]
  node [
    id 594
    label "przedstawi&#263;"
  ]
  node [
    id 595
    label "explain"
  ]
  node [
    id 596
    label "poja&#347;ni&#263;"
  ]
  node [
    id 597
    label "tenis"
  ]
  node [
    id 598
    label "supply"
  ]
  node [
    id 599
    label "da&#263;"
  ]
  node [
    id 600
    label "ustawi&#263;"
  ]
  node [
    id 601
    label "siatk&#243;wka"
  ]
  node [
    id 602
    label "give"
  ]
  node [
    id 603
    label "zagra&#263;"
  ]
  node [
    id 604
    label "jedzenie"
  ]
  node [
    id 605
    label "poinformowa&#263;"
  ]
  node [
    id 606
    label "introduce"
  ]
  node [
    id 607
    label "nafaszerowa&#263;"
  ]
  node [
    id 608
    label "zaserwowa&#263;"
  ]
  node [
    id 609
    label "powo&#322;a&#263;"
  ]
  node [
    id 610
    label "sie&#263;_rybacka"
  ]
  node [
    id 611
    label "zu&#380;y&#263;"
  ]
  node [
    id 612
    label "wyj&#261;&#263;"
  ]
  node [
    id 613
    label "ustali&#263;"
  ]
  node [
    id 614
    label "distill"
  ]
  node [
    id 615
    label "pick"
  ]
  node [
    id 616
    label "kotwica"
  ]
  node [
    id 617
    label "testify"
  ]
  node [
    id 618
    label "udowodni&#263;"
  ]
  node [
    id 619
    label "spowodowa&#263;"
  ]
  node [
    id 620
    label "wyrazi&#263;"
  ]
  node [
    id 621
    label "przeszkoli&#263;"
  ]
  node [
    id 622
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 623
    label "try"
  ]
  node [
    id 624
    label "kreska"
  ]
  node [
    id 625
    label "narysowa&#263;"
  ]
  node [
    id 626
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 627
    label "nietuzinkowy"
  ]
  node [
    id 628
    label "intryguj&#261;cy"
  ]
  node [
    id 629
    label "ch&#281;tny"
  ]
  node [
    id 630
    label "swoisty"
  ]
  node [
    id 631
    label "interesowanie"
  ]
  node [
    id 632
    label "interesuj&#261;cy"
  ]
  node [
    id 633
    label "ciekawie"
  ]
  node [
    id 634
    label "indagator"
  ]
  node [
    id 635
    label "interesuj&#261;co"
  ]
  node [
    id 636
    label "atrakcyjny"
  ]
  node [
    id 637
    label "intryguj&#261;co"
  ]
  node [
    id 638
    label "niespotykany"
  ]
  node [
    id 639
    label "nietuzinkowo"
  ]
  node [
    id 640
    label "ch&#281;tliwy"
  ]
  node [
    id 641
    label "ch&#281;tnie"
  ]
  node [
    id 642
    label "napalony"
  ]
  node [
    id 643
    label "chy&#380;y"
  ]
  node [
    id 644
    label "&#380;yczliwy"
  ]
  node [
    id 645
    label "przychylny"
  ]
  node [
    id 646
    label "gotowy"
  ]
  node [
    id 647
    label "ludzko&#347;&#263;"
  ]
  node [
    id 648
    label "asymilowanie"
  ]
  node [
    id 649
    label "wapniak"
  ]
  node [
    id 650
    label "asymilowa&#263;"
  ]
  node [
    id 651
    label "os&#322;abia&#263;"
  ]
  node [
    id 652
    label "hominid"
  ]
  node [
    id 653
    label "podw&#322;adny"
  ]
  node [
    id 654
    label "os&#322;abianie"
  ]
  node [
    id 655
    label "g&#322;owa"
  ]
  node [
    id 656
    label "figura"
  ]
  node [
    id 657
    label "portrecista"
  ]
  node [
    id 658
    label "dwun&#243;g"
  ]
  node [
    id 659
    label "profanum"
  ]
  node [
    id 660
    label "mikrokosmos"
  ]
  node [
    id 661
    label "nasada"
  ]
  node [
    id 662
    label "duch"
  ]
  node [
    id 663
    label "antropochoria"
  ]
  node [
    id 664
    label "osoba"
  ]
  node [
    id 665
    label "wz&#243;r"
  ]
  node [
    id 666
    label "senior"
  ]
  node [
    id 667
    label "oddzia&#322;ywanie"
  ]
  node [
    id 668
    label "Adam"
  ]
  node [
    id 669
    label "homo_sapiens"
  ]
  node [
    id 670
    label "polifag"
  ]
  node [
    id 671
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 672
    label "odr&#281;bny"
  ]
  node [
    id 673
    label "swoi&#347;cie"
  ]
  node [
    id 674
    label "dziwnie"
  ]
  node [
    id 675
    label "dziwy"
  ]
  node [
    id 676
    label "inny"
  ]
  node [
    id 677
    label "dobrze"
  ]
  node [
    id 678
    label "occupation"
  ]
  node [
    id 679
    label "bycie"
  ]
  node [
    id 680
    label "ciekawski"
  ]
  node [
    id 681
    label "materia"
  ]
  node [
    id 682
    label "nawil&#380;arka"
  ]
  node [
    id 683
    label "bielarnia"
  ]
  node [
    id 684
    label "dane"
  ]
  node [
    id 685
    label "tworzywo"
  ]
  node [
    id 686
    label "substancja"
  ]
  node [
    id 687
    label "kandydat"
  ]
  node [
    id 688
    label "archiwum"
  ]
  node [
    id 689
    label "krajka"
  ]
  node [
    id 690
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 691
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 692
    label "krajalno&#347;&#263;"
  ]
  node [
    id 693
    label "edytowa&#263;"
  ]
  node [
    id 694
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 695
    label "spakowanie"
  ]
  node [
    id 696
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 697
    label "pakowa&#263;"
  ]
  node [
    id 698
    label "rekord"
  ]
  node [
    id 699
    label "korelator"
  ]
  node [
    id 700
    label "wyci&#261;ganie"
  ]
  node [
    id 701
    label "pakowanie"
  ]
  node [
    id 702
    label "sekwencjonowa&#263;"
  ]
  node [
    id 703
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 704
    label "jednostka_informacji"
  ]
  node [
    id 705
    label "evidence"
  ]
  node [
    id 706
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 707
    label "rozpakowywanie"
  ]
  node [
    id 708
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 709
    label "rozpakowanie"
  ]
  node [
    id 710
    label "informacja"
  ]
  node [
    id 711
    label "rozpakowywa&#263;"
  ]
  node [
    id 712
    label "konwersja"
  ]
  node [
    id 713
    label "nap&#322;ywanie"
  ]
  node [
    id 714
    label "rozpakowa&#263;"
  ]
  node [
    id 715
    label "spakowa&#263;"
  ]
  node [
    id 716
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 717
    label "edytowanie"
  ]
  node [
    id 718
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 719
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 720
    label "sekwencjonowanie"
  ]
  node [
    id 721
    label "przenikanie"
  ]
  node [
    id 722
    label "byt"
  ]
  node [
    id 723
    label "cz&#261;steczka"
  ]
  node [
    id 724
    label "temperatura_krytyczna"
  ]
  node [
    id 725
    label "przenika&#263;"
  ]
  node [
    id 726
    label "smolisty"
  ]
  node [
    id 727
    label "obszycie"
  ]
  node [
    id 728
    label "okrajka"
  ]
  node [
    id 729
    label "wst&#261;&#380;ka"
  ]
  node [
    id 730
    label "pasek"
  ]
  node [
    id 731
    label "bardko"
  ]
  node [
    id 732
    label "pasmanteria"
  ]
  node [
    id 733
    label "temat"
  ]
  node [
    id 734
    label "szczeg&#243;&#322;"
  ]
  node [
    id 735
    label "ropa"
  ]
  node [
    id 736
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 737
    label "maszyna_w&#322;&#243;kiennicza"
  ]
  node [
    id 738
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 739
    label "szk&#322;o"
  ]
  node [
    id 740
    label "blacha"
  ]
  node [
    id 741
    label "lista_wyborcza"
  ]
  node [
    id 742
    label "aspirowanie"
  ]
  node [
    id 743
    label "bierne_prawo_wyborcze"
  ]
  node [
    id 744
    label "kolekcja"
  ]
  node [
    id 745
    label "dokumentacja"
  ]
  node [
    id 746
    label "archive"
  ]
  node [
    id 747
    label "plan"
  ]
  node [
    id 748
    label "kondycja"
  ]
  node [
    id 749
    label "polecenie"
  ]
  node [
    id 750
    label "samopoczucie"
  ]
  node [
    id 751
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 752
    label "zdolno&#347;&#263;"
  ]
  node [
    id 753
    label "capability"
  ]
  node [
    id 754
    label "prawo"
  ]
  node [
    id 755
    label "nadgrodzi&#263;"
  ]
  node [
    id 756
    label "przyzna&#263;"
  ]
  node [
    id 757
    label "recompense"
  ]
  node [
    id 758
    label "pay"
  ]
  node [
    id 759
    label "nada&#263;"
  ]
  node [
    id 760
    label "pozwoli&#263;"
  ]
  node [
    id 761
    label "stwierdzi&#263;"
  ]
  node [
    id 762
    label "powierzy&#263;"
  ]
  node [
    id 763
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 764
    label "obieca&#263;"
  ]
  node [
    id 765
    label "odst&#261;pi&#263;"
  ]
  node [
    id 766
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 767
    label "przywali&#263;"
  ]
  node [
    id 768
    label "wyrzec_si&#281;"
  ]
  node [
    id 769
    label "sztachn&#261;&#263;"
  ]
  node [
    id 770
    label "rap"
  ]
  node [
    id 771
    label "feed"
  ]
  node [
    id 772
    label "convey"
  ]
  node [
    id 773
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 774
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 775
    label "udost&#281;pni&#263;"
  ]
  node [
    id 776
    label "przeznaczy&#263;"
  ]
  node [
    id 777
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 778
    label "zada&#263;"
  ]
  node [
    id 779
    label "dress"
  ]
  node [
    id 780
    label "dostarczy&#263;"
  ]
  node [
    id 781
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 782
    label "przekaza&#263;"
  ]
  node [
    id 783
    label "doda&#263;"
  ]
  node [
    id 784
    label "zap&#322;aci&#263;"
  ]
  node [
    id 785
    label "wynagrodzi&#263;"
  ]
  node [
    id 786
    label "odrobi&#263;"
  ]
  node [
    id 787
    label "szybki"
  ]
  node [
    id 788
    label "letki"
  ]
  node [
    id 789
    label "umiej&#281;tny"
  ]
  node [
    id 790
    label "zdrowy"
  ]
  node [
    id 791
    label "dzia&#322;alny"
  ]
  node [
    id 792
    label "sprawnie"
  ]
  node [
    id 793
    label "dobroczynny"
  ]
  node [
    id 794
    label "czw&#243;rka"
  ]
  node [
    id 795
    label "spokojny"
  ]
  node [
    id 796
    label "skuteczny"
  ]
  node [
    id 797
    label "&#347;mieszny"
  ]
  node [
    id 798
    label "mi&#322;y"
  ]
  node [
    id 799
    label "grzeczny"
  ]
  node [
    id 800
    label "powitanie"
  ]
  node [
    id 801
    label "ca&#322;y"
  ]
  node [
    id 802
    label "zwrot"
  ]
  node [
    id 803
    label "pomy&#347;lny"
  ]
  node [
    id 804
    label "moralny"
  ]
  node [
    id 805
    label "drogi"
  ]
  node [
    id 806
    label "pozytywny"
  ]
  node [
    id 807
    label "odpowiedni"
  ]
  node [
    id 808
    label "korzystny"
  ]
  node [
    id 809
    label "pos&#322;uszny"
  ]
  node [
    id 810
    label "zdrowo"
  ]
  node [
    id 811
    label "wyzdrowienie"
  ]
  node [
    id 812
    label "wyleczenie_si&#281;"
  ]
  node [
    id 813
    label "uzdrowienie"
  ]
  node [
    id 814
    label "silny"
  ]
  node [
    id 815
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 816
    label "normalny"
  ]
  node [
    id 817
    label "rozs&#261;dny"
  ]
  node [
    id 818
    label "zdrowienie"
  ]
  node [
    id 819
    label "solidny"
  ]
  node [
    id 820
    label "uzdrawianie"
  ]
  node [
    id 821
    label "umiej&#281;tnie"
  ]
  node [
    id 822
    label "udany"
  ]
  node [
    id 823
    label "umny"
  ]
  node [
    id 824
    label "czynny"
  ]
  node [
    id 825
    label "lekki"
  ]
  node [
    id 826
    label "delikatny"
  ]
  node [
    id 827
    label "r&#261;czy"
  ]
  node [
    id 828
    label "&#322;atwy"
  ]
  node [
    id 829
    label "beztroski"
  ]
  node [
    id 830
    label "kompetentnie"
  ]
  node [
    id 831
    label "funkcjonalnie"
  ]
  node [
    id 832
    label "szybko"
  ]
  node [
    id 833
    label "udanie"
  ]
  node [
    id 834
    label "skutecznie"
  ]
  node [
    id 835
    label "intensywny"
  ]
  node [
    id 836
    label "prosty"
  ]
  node [
    id 837
    label "kr&#243;tki"
  ]
  node [
    id 838
    label "temperamentny"
  ]
  node [
    id 839
    label "bystrolotny"
  ]
  node [
    id 840
    label "dynamiczny"
  ]
  node [
    id 841
    label "bezpo&#347;redni"
  ]
  node [
    id 842
    label "energiczny"
  ]
  node [
    id 843
    label "coating"
  ]
  node [
    id 844
    label "conclusion"
  ]
  node [
    id 845
    label "koniec"
  ]
  node [
    id 846
    label "rozgrywka"
  ]
  node [
    id 847
    label "seria"
  ]
  node [
    id 848
    label "rhythm"
  ]
  node [
    id 849
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 850
    label "okr&#261;&#380;enie"
  ]
  node [
    id 851
    label "ostatnie_podrygi"
  ]
  node [
    id 852
    label "visitation"
  ]
  node [
    id 853
    label "agonia"
  ]
  node [
    id 854
    label "defenestracja"
  ]
  node [
    id 855
    label "punkt"
  ]
  node [
    id 856
    label "dzia&#322;anie"
  ]
  node [
    id 857
    label "kres"
  ]
  node [
    id 858
    label "mogi&#322;a"
  ]
  node [
    id 859
    label "kres_&#380;ycia"
  ]
  node [
    id 860
    label "szereg"
  ]
  node [
    id 861
    label "szeol"
  ]
  node [
    id 862
    label "pogrzebanie"
  ]
  node [
    id 863
    label "&#380;a&#322;oba"
  ]
  node [
    id 864
    label "zabicie"
  ]
  node [
    id 865
    label "intencja"
  ]
  node [
    id 866
    label "device"
  ]
  node [
    id 867
    label "program_u&#380;ytkowy"
  ]
  node [
    id 868
    label "pomys&#322;"
  ]
  node [
    id 869
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 870
    label "agreement"
  ]
  node [
    id 871
    label "dokument"
  ]
  node [
    id 872
    label "thinking"
  ]
  node [
    id 873
    label "zapis"
  ]
  node [
    id 874
    label "&#347;wiadectwo"
  ]
  node [
    id 875
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 876
    label "parafa"
  ]
  node [
    id 877
    label "plik"
  ]
  node [
    id 878
    label "raport&#243;wka"
  ]
  node [
    id 879
    label "utw&#243;r"
  ]
  node [
    id 880
    label "record"
  ]
  node [
    id 881
    label "registratura"
  ]
  node [
    id 882
    label "fascyku&#322;"
  ]
  node [
    id 883
    label "artyku&#322;"
  ]
  node [
    id 884
    label "writing"
  ]
  node [
    id 885
    label "sygnatariusz"
  ]
  node [
    id 886
    label "model"
  ]
  node [
    id 887
    label "rysunek"
  ]
  node [
    id 888
    label "przestrze&#324;"
  ]
  node [
    id 889
    label "obraz"
  ]
  node [
    id 890
    label "reprezentacja"
  ]
  node [
    id 891
    label "dekoracja"
  ]
  node [
    id 892
    label "perspektywa"
  ]
  node [
    id 893
    label "operat"
  ]
  node [
    id 894
    label "kosztorys"
  ]
  node [
    id 895
    label "idea"
  ]
  node [
    id 896
    label "pocz&#261;tki"
  ]
  node [
    id 897
    label "ukradzenie"
  ]
  node [
    id 898
    label "ukra&#347;&#263;"
  ]
  node [
    id 899
    label "system"
  ]
  node [
    id 900
    label "pierwszy"
  ]
  node [
    id 901
    label "przedni"
  ]
  node [
    id 902
    label "czo&#322;owo"
  ]
  node [
    id 903
    label "wybitny"
  ]
  node [
    id 904
    label "pr&#281;dki"
  ]
  node [
    id 905
    label "pocz&#261;tkowy"
  ]
  node [
    id 906
    label "najwa&#380;niejszy"
  ]
  node [
    id 907
    label "dzie&#324;"
  ]
  node [
    id 908
    label "wydatny"
  ]
  node [
    id 909
    label "wspania&#322;y"
  ]
  node [
    id 910
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 911
    label "&#347;wietny"
  ]
  node [
    id 912
    label "imponuj&#261;cy"
  ]
  node [
    id 913
    label "wybitnie"
  ]
  node [
    id 914
    label "celny"
  ]
  node [
    id 915
    label "przebrany"
  ]
  node [
    id 916
    label "przednio"
  ]
  node [
    id 917
    label "Polish"
  ]
  node [
    id 918
    label "goniony"
  ]
  node [
    id 919
    label "oberek"
  ]
  node [
    id 920
    label "ryba_po_grecku"
  ]
  node [
    id 921
    label "sztajer"
  ]
  node [
    id 922
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 923
    label "krakowiak"
  ]
  node [
    id 924
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 925
    label "pierogi_ruskie"
  ]
  node [
    id 926
    label "lacki"
  ]
  node [
    id 927
    label "polak"
  ]
  node [
    id 928
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 929
    label "chodzony"
  ]
  node [
    id 930
    label "po_polsku"
  ]
  node [
    id 931
    label "mazur"
  ]
  node [
    id 932
    label "polsko"
  ]
  node [
    id 933
    label "skoczny"
  ]
  node [
    id 934
    label "drabant"
  ]
  node [
    id 935
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 936
    label "j&#281;zyk"
  ]
  node [
    id 937
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 938
    label "artykulator"
  ]
  node [
    id 939
    label "kod"
  ]
  node [
    id 940
    label "kawa&#322;ek"
  ]
  node [
    id 941
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 942
    label "stylik"
  ]
  node [
    id 943
    label "przet&#322;umaczenie"
  ]
  node [
    id 944
    label "formalizowanie"
  ]
  node [
    id 945
    label "ssanie"
  ]
  node [
    id 946
    label "ssa&#263;"
  ]
  node [
    id 947
    label "language"
  ]
  node [
    id 948
    label "liza&#263;"
  ]
  node [
    id 949
    label "napisa&#263;"
  ]
  node [
    id 950
    label "konsonantyzm"
  ]
  node [
    id 951
    label "wokalizm"
  ]
  node [
    id 952
    label "fonetyka"
  ]
  node [
    id 953
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 954
    label "jeniec"
  ]
  node [
    id 955
    label "but"
  ]
  node [
    id 956
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 957
    label "po_koroniarsku"
  ]
  node [
    id 958
    label "kultura_duchowa"
  ]
  node [
    id 959
    label "t&#322;umaczenie"
  ]
  node [
    id 960
    label "m&#243;wienie"
  ]
  node [
    id 961
    label "pype&#263;"
  ]
  node [
    id 962
    label "lizanie"
  ]
  node [
    id 963
    label "pismo"
  ]
  node [
    id 964
    label "formalizowa&#263;"
  ]
  node [
    id 965
    label "rozumie&#263;"
  ]
  node [
    id 966
    label "organ"
  ]
  node [
    id 967
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 968
    label "rozumienie"
  ]
  node [
    id 969
    label "spos&#243;b"
  ]
  node [
    id 970
    label "makroglosja"
  ]
  node [
    id 971
    label "m&#243;wi&#263;"
  ]
  node [
    id 972
    label "jama_ustna"
  ]
  node [
    id 973
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 974
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 975
    label "natural_language"
  ]
  node [
    id 976
    label "s&#322;ownictwo"
  ]
  node [
    id 977
    label "urz&#261;dzenie"
  ]
  node [
    id 978
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 979
    label "wschodnioeuropejski"
  ]
  node [
    id 980
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 981
    label "poga&#324;ski"
  ]
  node [
    id 982
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 983
    label "topielec"
  ]
  node [
    id 984
    label "europejski"
  ]
  node [
    id 985
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 986
    label "langosz"
  ]
  node [
    id 987
    label "gwardzista"
  ]
  node [
    id 988
    label "melodia"
  ]
  node [
    id 989
    label "taniec"
  ]
  node [
    id 990
    label "taniec_ludowy"
  ]
  node [
    id 991
    label "&#347;redniowieczny"
  ]
  node [
    id 992
    label "specjalny"
  ]
  node [
    id 993
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 994
    label "weso&#322;y"
  ]
  node [
    id 995
    label "rytmiczny"
  ]
  node [
    id 996
    label "skocznie"
  ]
  node [
    id 997
    label "lendler"
  ]
  node [
    id 998
    label "austriacki"
  ]
  node [
    id 999
    label "polka"
  ]
  node [
    id 1000
    label "europejsko"
  ]
  node [
    id 1001
    label "przytup"
  ]
  node [
    id 1002
    label "ho&#322;ubiec"
  ]
  node [
    id 1003
    label "wodzi&#263;"
  ]
  node [
    id 1004
    label "ludowy"
  ]
  node [
    id 1005
    label "pie&#347;&#324;"
  ]
  node [
    id 1006
    label "mieszkaniec"
  ]
  node [
    id 1007
    label "centu&#347;"
  ]
  node [
    id 1008
    label "lalka"
  ]
  node [
    id 1009
    label "Ma&#322;opolanin"
  ]
  node [
    id 1010
    label "krakauer"
  ]
  node [
    id 1011
    label "publicysta"
  ]
  node [
    id 1012
    label "nowiniarz"
  ]
  node [
    id 1013
    label "akredytowanie"
  ]
  node [
    id 1014
    label "akredytowa&#263;"
  ]
  node [
    id 1015
    label "pracownik"
  ]
  node [
    id 1016
    label "fachowiec"
  ]
  node [
    id 1017
    label "zwi&#261;zkowiec"
  ]
  node [
    id 1018
    label "Korwin"
  ]
  node [
    id 1019
    label "Michnik"
  ]
  node [
    id 1020
    label "Conrad"
  ]
  node [
    id 1021
    label "intelektualista"
  ]
  node [
    id 1022
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 1023
    label "Gogol"
  ]
  node [
    id 1024
    label "nowinkarz"
  ]
  node [
    id 1025
    label "uwiarygodnia&#263;"
  ]
  node [
    id 1026
    label "attest"
  ]
  node [
    id 1027
    label "uwiarygodni&#263;"
  ]
  node [
    id 1028
    label "zezwala&#263;"
  ]
  node [
    id 1029
    label "upowa&#380;nia&#263;"
  ]
  node [
    id 1030
    label "upowa&#380;ni&#263;"
  ]
  node [
    id 1031
    label "zezwalanie"
  ]
  node [
    id 1032
    label "uwiarygodnienie"
  ]
  node [
    id 1033
    label "akredytowanie_si&#281;"
  ]
  node [
    id 1034
    label "upowa&#380;nianie"
  ]
  node [
    id 1035
    label "accreditation"
  ]
  node [
    id 1036
    label "pozwolenie"
  ]
  node [
    id 1037
    label "uwiarygodnianie"
  ]
  node [
    id 1038
    label "upowa&#380;nienie"
  ]
  node [
    id 1039
    label "dawa&#263;"
  ]
  node [
    id 1040
    label "confer"
  ]
  node [
    id 1041
    label "s&#261;dzi&#263;"
  ]
  node [
    id 1042
    label "distribute"
  ]
  node [
    id 1043
    label "stwierdza&#263;"
  ]
  node [
    id 1044
    label "nadawa&#263;"
  ]
  node [
    id 1045
    label "assign"
  ]
  node [
    id 1046
    label "gada&#263;"
  ]
  node [
    id 1047
    label "donosi&#263;"
  ]
  node [
    id 1048
    label "rekomendowa&#263;"
  ]
  node [
    id 1049
    label "za&#322;atwia&#263;"
  ]
  node [
    id 1050
    label "obgadywa&#263;"
  ]
  node [
    id 1051
    label "sprawia&#263;"
  ]
  node [
    id 1052
    label "przesy&#322;a&#263;"
  ]
  node [
    id 1053
    label "uznawa&#263;"
  ]
  node [
    id 1054
    label "oznajmia&#263;"
  ]
  node [
    id 1055
    label "wsp&#243;&#322;rz&#261;dzi&#263;"
  ]
  node [
    id 1056
    label "robi&#263;"
  ]
  node [
    id 1057
    label "my&#347;le&#263;"
  ]
  node [
    id 1058
    label "deliver"
  ]
  node [
    id 1059
    label "powodowa&#263;"
  ]
  node [
    id 1060
    label "hold"
  ]
  node [
    id 1061
    label "os&#261;dza&#263;"
  ]
  node [
    id 1062
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 1063
    label "zas&#261;dza&#263;"
  ]
  node [
    id 1064
    label "przekazywa&#263;"
  ]
  node [
    id 1065
    label "dostarcza&#263;"
  ]
  node [
    id 1066
    label "mie&#263;_miejsce"
  ]
  node [
    id 1067
    label "&#322;adowa&#263;"
  ]
  node [
    id 1068
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 1069
    label "przeznacza&#263;"
  ]
  node [
    id 1070
    label "surrender"
  ]
  node [
    id 1071
    label "traktowa&#263;"
  ]
  node [
    id 1072
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1073
    label "obiecywa&#263;"
  ]
  node [
    id 1074
    label "odst&#281;powa&#263;"
  ]
  node [
    id 1075
    label "tender"
  ]
  node [
    id 1076
    label "umieszcza&#263;"
  ]
  node [
    id 1077
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 1078
    label "t&#322;uc"
  ]
  node [
    id 1079
    label "powierza&#263;"
  ]
  node [
    id 1080
    label "render"
  ]
  node [
    id 1081
    label "wpiernicza&#263;"
  ]
  node [
    id 1082
    label "exsert"
  ]
  node [
    id 1083
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 1084
    label "train"
  ]
  node [
    id 1085
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1086
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 1087
    label "p&#322;aci&#263;"
  ]
  node [
    id 1088
    label "hold_out"
  ]
  node [
    id 1089
    label "nalewa&#263;"
  ]
  node [
    id 1090
    label "moralnie"
  ]
  node [
    id 1091
    label "warto&#347;ciowy"
  ]
  node [
    id 1092
    label "etycznie"
  ]
  node [
    id 1093
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1094
    label "nale&#380;ny"
  ]
  node [
    id 1095
    label "nale&#380;yty"
  ]
  node [
    id 1096
    label "typowy"
  ]
  node [
    id 1097
    label "uprawniony"
  ]
  node [
    id 1098
    label "zasadniczy"
  ]
  node [
    id 1099
    label "stosownie"
  ]
  node [
    id 1100
    label "taki"
  ]
  node [
    id 1101
    label "prawdziwy"
  ]
  node [
    id 1102
    label "ten"
  ]
  node [
    id 1103
    label "pozytywnie"
  ]
  node [
    id 1104
    label "fajny"
  ]
  node [
    id 1105
    label "dodatnio"
  ]
  node [
    id 1106
    label "przyjemny"
  ]
  node [
    id 1107
    label "po&#380;&#261;dany"
  ]
  node [
    id 1108
    label "niepowa&#380;ny"
  ]
  node [
    id 1109
    label "o&#347;mieszanie"
  ]
  node [
    id 1110
    label "&#347;miesznie"
  ]
  node [
    id 1111
    label "bawny"
  ]
  node [
    id 1112
    label "o&#347;mieszenie"
  ]
  node [
    id 1113
    label "nieadekwatny"
  ]
  node [
    id 1114
    label "wolny"
  ]
  node [
    id 1115
    label "uspokajanie_si&#281;"
  ]
  node [
    id 1116
    label "bezproblemowy"
  ]
  node [
    id 1117
    label "spokojnie"
  ]
  node [
    id 1118
    label "uspokojenie_si&#281;"
  ]
  node [
    id 1119
    label "cicho"
  ]
  node [
    id 1120
    label "uspokojenie"
  ]
  node [
    id 1121
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 1122
    label "nietrudny"
  ]
  node [
    id 1123
    label "uspokajanie"
  ]
  node [
    id 1124
    label "zale&#380;ny"
  ]
  node [
    id 1125
    label "uleg&#322;y"
  ]
  node [
    id 1126
    label "pos&#322;usznie"
  ]
  node [
    id 1127
    label "grzecznie"
  ]
  node [
    id 1128
    label "stosowny"
  ]
  node [
    id 1129
    label "niewinny"
  ]
  node [
    id 1130
    label "konserwatywny"
  ]
  node [
    id 1131
    label "nijaki"
  ]
  node [
    id 1132
    label "korzystnie"
  ]
  node [
    id 1133
    label "drogo"
  ]
  node [
    id 1134
    label "bliski"
  ]
  node [
    id 1135
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 1136
    label "przyjaciel"
  ]
  node [
    id 1137
    label "jedyny"
  ]
  node [
    id 1138
    label "du&#380;y"
  ]
  node [
    id 1139
    label "zdr&#243;w"
  ]
  node [
    id 1140
    label "calu&#347;ko"
  ]
  node [
    id 1141
    label "kompletny"
  ]
  node [
    id 1142
    label "&#380;ywy"
  ]
  node [
    id 1143
    label "pe&#322;ny"
  ]
  node [
    id 1144
    label "podobny"
  ]
  node [
    id 1145
    label "ca&#322;o"
  ]
  node [
    id 1146
    label "poskutkowanie"
  ]
  node [
    id 1147
    label "skutkowanie"
  ]
  node [
    id 1148
    label "pomy&#347;lnie"
  ]
  node [
    id 1149
    label "toto-lotek"
  ]
  node [
    id 1150
    label "arkusz_drukarski"
  ]
  node [
    id 1151
    label "&#322;&#243;dka"
  ]
  node [
    id 1152
    label "four"
  ]
  node [
    id 1153
    label "&#263;wiartka"
  ]
  node [
    id 1154
    label "hotel"
  ]
  node [
    id 1155
    label "cyfra"
  ]
  node [
    id 1156
    label "pok&#243;j"
  ]
  node [
    id 1157
    label "stopie&#324;"
  ]
  node [
    id 1158
    label "minialbum"
  ]
  node [
    id 1159
    label "osada"
  ]
  node [
    id 1160
    label "p&#322;yta_winylowa"
  ]
  node [
    id 1161
    label "blotka"
  ]
  node [
    id 1162
    label "zaprz&#281;g"
  ]
  node [
    id 1163
    label "przedtrzonowiec"
  ]
  node [
    id 1164
    label "turn"
  ]
  node [
    id 1165
    label "turning"
  ]
  node [
    id 1166
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 1167
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 1168
    label "skr&#281;t"
  ]
  node [
    id 1169
    label "obr&#243;t"
  ]
  node [
    id 1170
    label "fraza_czasownikowa"
  ]
  node [
    id 1171
    label "jednostka_leksykalna"
  ]
  node [
    id 1172
    label "wyra&#380;enie"
  ]
  node [
    id 1173
    label "welcome"
  ]
  node [
    id 1174
    label "spotkanie"
  ]
  node [
    id 1175
    label "pozdrowienie"
  ]
  node [
    id 1176
    label "zwyczaj"
  ]
  node [
    id 1177
    label "greeting"
  ]
  node [
    id 1178
    label "zdarzony"
  ]
  node [
    id 1179
    label "odpowiednio"
  ]
  node [
    id 1180
    label "odpowiadanie"
  ]
  node [
    id 1181
    label "kochanek"
  ]
  node [
    id 1182
    label "sk&#322;onny"
  ]
  node [
    id 1183
    label "wybranek"
  ]
  node [
    id 1184
    label "umi&#322;owany"
  ]
  node [
    id 1185
    label "przyjemnie"
  ]
  node [
    id 1186
    label "mi&#322;o"
  ]
  node [
    id 1187
    label "kochanie"
  ]
  node [
    id 1188
    label "dyplomata"
  ]
  node [
    id 1189
    label "dobroczynnie"
  ]
  node [
    id 1190
    label "lepiej"
  ]
  node [
    id 1191
    label "wiele"
  ]
  node [
    id 1192
    label "spo&#322;eczny"
  ]
  node [
    id 1193
    label "nadtytu&#322;"
  ]
  node [
    id 1194
    label "tytulatura"
  ]
  node [
    id 1195
    label "elevation"
  ]
  node [
    id 1196
    label "mianowaniec"
  ]
  node [
    id 1197
    label "nazwa"
  ]
  node [
    id 1198
    label "podtytu&#322;"
  ]
  node [
    id 1199
    label "technika"
  ]
  node [
    id 1200
    label "glif"
  ]
  node [
    id 1201
    label "dese&#324;"
  ]
  node [
    id 1202
    label "prohibita"
  ]
  node [
    id 1203
    label "cymelium"
  ]
  node [
    id 1204
    label "tkanina"
  ]
  node [
    id 1205
    label "zaproszenie"
  ]
  node [
    id 1206
    label "dwustronno&#347;&#263;"
  ]
  node [
    id 1207
    label "formatowa&#263;"
  ]
  node [
    id 1208
    label "formatowanie"
  ]
  node [
    id 1209
    label "zdobnik"
  ]
  node [
    id 1210
    label "character"
  ]
  node [
    id 1211
    label "printing"
  ]
  node [
    id 1212
    label "term"
  ]
  node [
    id 1213
    label "wezwanie"
  ]
  node [
    id 1214
    label "patron"
  ]
  node [
    id 1215
    label "leksem"
  ]
  node [
    id 1216
    label "pieni&#261;dze"
  ]
  node [
    id 1217
    label "plon"
  ]
  node [
    id 1218
    label "skojarzy&#263;"
  ]
  node [
    id 1219
    label "zadenuncjowa&#263;"
  ]
  node [
    id 1220
    label "impart"
  ]
  node [
    id 1221
    label "reszta"
  ]
  node [
    id 1222
    label "zapach"
  ]
  node [
    id 1223
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 1224
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 1225
    label "wiano"
  ]
  node [
    id 1226
    label "translate"
  ]
  node [
    id 1227
    label "wprowadzi&#263;"
  ]
  node [
    id 1228
    label "wytworzy&#263;"
  ]
  node [
    id 1229
    label "tajemnica"
  ]
  node [
    id 1230
    label "panna_na_wydaniu"
  ]
  node [
    id 1231
    label "ujawni&#263;"
  ]
  node [
    id 1232
    label "kojarzy&#263;"
  ]
  node [
    id 1233
    label "wprowadza&#263;"
  ]
  node [
    id 1234
    label "podawa&#263;"
  ]
  node [
    id 1235
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 1236
    label "ujawnia&#263;"
  ]
  node [
    id 1237
    label "placard"
  ]
  node [
    id 1238
    label "denuncjowa&#263;"
  ]
  node [
    id 1239
    label "wytwarza&#263;"
  ]
  node [
    id 1240
    label "stanowisko"
  ]
  node [
    id 1241
    label "mandatariusz"
  ]
  node [
    id 1242
    label "afisz"
  ]
  node [
    id 1243
    label "summer"
  ]
  node [
    id 1244
    label "liga"
  ]
  node [
    id 1245
    label "Entuzjastki"
  ]
  node [
    id 1246
    label "kompozycja"
  ]
  node [
    id 1247
    label "Terranie"
  ]
  node [
    id 1248
    label "category"
  ]
  node [
    id 1249
    label "pakiet_klimatyczny"
  ]
  node [
    id 1250
    label "oddzia&#322;"
  ]
  node [
    id 1251
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1252
    label "stage_set"
  ]
  node [
    id 1253
    label "type"
  ]
  node [
    id 1254
    label "specgrupa"
  ]
  node [
    id 1255
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 1256
    label "Eurogrupa"
  ]
  node [
    id 1257
    label "harcerze_starsi"
  ]
  node [
    id 1258
    label "rok_akademicki"
  ]
  node [
    id 1259
    label "rok_szkolny"
  ]
  node [
    id 1260
    label "semester"
  ]
  node [
    id 1261
    label "anniwersarz"
  ]
  node [
    id 1262
    label "rocznica"
  ]
  node [
    id 1263
    label "obszar"
  ]
  node [
    id 1264
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 1265
    label "long_time"
  ]
  node [
    id 1266
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 1267
    label "almanac"
  ]
  node [
    id 1268
    label "rozk&#322;ad"
  ]
  node [
    id 1269
    label "Juliusz_Cezar"
  ]
  node [
    id 1270
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1271
    label "zwy&#380;kowanie"
  ]
  node [
    id 1272
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1273
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1274
    label "zaj&#281;cia"
  ]
  node [
    id 1275
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1276
    label "trasa"
  ]
  node [
    id 1277
    label "przeorientowywanie"
  ]
  node [
    id 1278
    label "przejazd"
  ]
  node [
    id 1279
    label "kierunek"
  ]
  node [
    id 1280
    label "przeorientowywa&#263;"
  ]
  node [
    id 1281
    label "nauka"
  ]
  node [
    id 1282
    label "przeorientowanie"
  ]
  node [
    id 1283
    label "klasa"
  ]
  node [
    id 1284
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 1285
    label "przeorientowa&#263;"
  ]
  node [
    id 1286
    label "manner"
  ]
  node [
    id 1287
    label "course"
  ]
  node [
    id 1288
    label "zni&#380;kowanie"
  ]
  node [
    id 1289
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1290
    label "stawka"
  ]
  node [
    id 1291
    label "way"
  ]
  node [
    id 1292
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 1293
    label "deprecjacja"
  ]
  node [
    id 1294
    label "cedu&#322;a"
  ]
  node [
    id 1295
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 1296
    label "drive"
  ]
  node [
    id 1297
    label "bearing"
  ]
  node [
    id 1298
    label "Lira"
  ]
  node [
    id 1299
    label "nowy"
  ]
  node [
    id 1300
    label "Qmam"
  ]
  node [
    id 1301
    label "Amelia"
  ]
  node [
    id 1302
    label "&#321;ukasiak"
  ]
  node [
    id 1303
    label "Aleksandra"
  ]
  node [
    id 1304
    label "Zieleniewska"
  ]
  node [
    id 1305
    label "Renata"
  ]
  node [
    id 1306
    label "kto"
  ]
  node [
    id 1307
    label "Jan"
  ]
  node [
    id 1308
    label "wr&#243;bel"
  ]
  node [
    id 1309
    label "Mariusz"
  ]
  node [
    id 1310
    label "szczygie&#322;"
  ]
  node [
    id 1311
    label "format"
  ]
  node [
    id 1312
    label "gazeta"
  ]
  node [
    id 1313
    label "wyborczy"
  ]
  node [
    id 1314
    label "na"
  ]
  node [
    id 1315
    label "RMF"
  ]
  node [
    id 1316
    label "FM"
  ]
  node [
    id 1317
    label "plus"
  ]
  node [
    id 1318
    label "tv"
  ]
  node [
    id 1319
    label "puls"
  ]
  node [
    id 1320
    label "literacki"
  ]
  node [
    id 1321
    label "i"
  ]
  node [
    id 1322
    label "liceum"
  ]
  node [
    id 1323
    label "og&#243;lnokszta&#322;c&#261;cy"
  ]
  node [
    id 1324
    label "wyspa"
  ]
  node [
    id 1325
    label "warszawa"
  ]
  node [
    id 1326
    label "centrum"
  ]
  node [
    id 1327
    label "stosunki"
  ]
  node [
    id 1328
    label "mi&#281;dzynarodowy"
  ]
  node [
    id 1329
    label "rada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 1314
  ]
  edge [
    source 0
    target 733
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 1299
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 1329
  ]
  edge [
    source 8
    target 1326
  ]
  edge [
    source 8
    target 1327
  ]
  edge [
    source 8
    target 1328
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 10
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 255
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 356
  ]
  edge [
    source 14
    target 357
  ]
  edge [
    source 14
    target 358
  ]
  edge [
    source 14
    target 359
  ]
  edge [
    source 14
    target 360
  ]
  edge [
    source 14
    target 361
  ]
  edge [
    source 14
    target 362
  ]
  edge [
    source 14
    target 363
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 364
  ]
  edge [
    source 15
    target 365
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 15
    target 366
  ]
  edge [
    source 15
    target 367
  ]
  edge [
    source 15
    target 368
  ]
  edge [
    source 15
    target 369
  ]
  edge [
    source 15
    target 370
  ]
  edge [
    source 15
    target 371
  ]
  edge [
    source 15
    target 372
  ]
  edge [
    source 15
    target 373
  ]
  edge [
    source 15
    target 374
  ]
  edge [
    source 15
    target 375
  ]
  edge [
    source 15
    target 376
  ]
  edge [
    source 15
    target 377
  ]
  edge [
    source 15
    target 378
  ]
  edge [
    source 15
    target 379
  ]
  edge [
    source 15
    target 380
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 238
  ]
  edge [
    source 15
    target 382
  ]
  edge [
    source 15
    target 383
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 384
  ]
  edge [
    source 15
    target 385
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 386
  ]
  edge [
    source 15
    target 387
  ]
  edge [
    source 15
    target 388
  ]
  edge [
    source 15
    target 56
  ]
  edge [
    source 15
    target 389
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 391
  ]
  edge [
    source 15
    target 392
  ]
  edge [
    source 15
    target 393
  ]
  edge [
    source 15
    target 394
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 396
  ]
  edge [
    source 15
    target 397
  ]
  edge [
    source 15
    target 398
  ]
  edge [
    source 15
    target 399
  ]
  edge [
    source 15
    target 400
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 402
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 408
  ]
  edge [
    source 15
    target 409
  ]
  edge [
    source 15
    target 410
  ]
  edge [
    source 15
    target 305
  ]
  edge [
    source 15
    target 411
  ]
  edge [
    source 15
    target 412
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 15
    target 414
  ]
  edge [
    source 15
    target 415
  ]
  edge [
    source 15
    target 416
  ]
  edge [
    source 15
    target 417
  ]
  edge [
    source 15
    target 418
  ]
  edge [
    source 15
    target 419
  ]
  edge [
    source 15
    target 420
  ]
  edge [
    source 15
    target 421
  ]
  edge [
    source 15
    target 361
  ]
  edge [
    source 15
    target 422
  ]
  edge [
    source 15
    target 423
  ]
  edge [
    source 15
    target 424
  ]
  edge [
    source 15
    target 425
  ]
  edge [
    source 15
    target 426
  ]
  edge [
    source 15
    target 427
  ]
  edge [
    source 15
    target 428
  ]
  edge [
    source 15
    target 429
  ]
  edge [
    source 15
    target 430
  ]
  edge [
    source 15
    target 431
  ]
  edge [
    source 15
    target 432
  ]
  edge [
    source 15
    target 433
  ]
  edge [
    source 15
    target 434
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 16
    target 441
  ]
  edge [
    source 16
    target 442
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 16
    target 444
  ]
  edge [
    source 16
    target 445
  ]
  edge [
    source 16
    target 446
  ]
  edge [
    source 16
    target 447
  ]
  edge [
    source 16
    target 448
  ]
  edge [
    source 16
    target 449
  ]
  edge [
    source 16
    target 450
  ]
  edge [
    source 16
    target 451
  ]
  edge [
    source 16
    target 452
  ]
  edge [
    source 16
    target 453
  ]
  edge [
    source 16
    target 454
  ]
  edge [
    source 16
    target 455
  ]
  edge [
    source 16
    target 456
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 361
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 357
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 472
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 454
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 401
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 490
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 492
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 497
  ]
  edge [
    source 18
    target 498
  ]
  edge [
    source 18
    target 499
  ]
  edge [
    source 18
    target 500
  ]
  edge [
    source 18
    target 501
  ]
  edge [
    source 18
    target 502
  ]
  edge [
    source 18
    target 503
  ]
  edge [
    source 18
    target 504
  ]
  edge [
    source 18
    target 505
  ]
  edge [
    source 18
    target 506
  ]
  edge [
    source 18
    target 507
  ]
  edge [
    source 18
    target 508
  ]
  edge [
    source 18
    target 509
  ]
  edge [
    source 18
    target 361
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 458
  ]
  edge [
    source 19
    target 459
  ]
  edge [
    source 19
    target 460
  ]
  edge [
    source 19
    target 361
  ]
  edge [
    source 19
    target 35
  ]
  edge [
    source 19
    target 461
  ]
  edge [
    source 19
    target 510
  ]
  edge [
    source 19
    target 511
  ]
  edge [
    source 19
    target 512
  ]
  edge [
    source 19
    target 513
  ]
  edge [
    source 19
    target 303
  ]
  edge [
    source 19
    target 514
  ]
  edge [
    source 19
    target 515
  ]
  edge [
    source 19
    target 516
  ]
  edge [
    source 19
    target 517
  ]
  edge [
    source 19
    target 518
  ]
  edge [
    source 19
    target 519
  ]
  edge [
    source 19
    target 520
  ]
  edge [
    source 19
    target 521
  ]
  edge [
    source 19
    target 522
  ]
  edge [
    source 19
    target 523
  ]
  edge [
    source 19
    target 524
  ]
  edge [
    source 19
    target 525
  ]
  edge [
    source 19
    target 526
  ]
  edge [
    source 19
    target 527
  ]
  edge [
    source 19
    target 528
  ]
  edge [
    source 19
    target 529
  ]
  edge [
    source 19
    target 530
  ]
  edge [
    source 19
    target 531
  ]
  edge [
    source 19
    target 532
  ]
  edge [
    source 19
    target 533
  ]
  edge [
    source 19
    target 534
  ]
  edge [
    source 19
    target 463
  ]
  edge [
    source 19
    target 535
  ]
  edge [
    source 19
    target 536
  ]
  edge [
    source 19
    target 537
  ]
  edge [
    source 19
    target 538
  ]
  edge [
    source 19
    target 539
  ]
  edge [
    source 19
    target 540
  ]
  edge [
    source 19
    target 541
  ]
  edge [
    source 19
    target 542
  ]
  edge [
    source 19
    target 543
  ]
  edge [
    source 19
    target 544
  ]
  edge [
    source 19
    target 545
  ]
  edge [
    source 19
    target 546
  ]
  edge [
    source 19
    target 547
  ]
  edge [
    source 19
    target 548
  ]
  edge [
    source 19
    target 549
  ]
  edge [
    source 19
    target 550
  ]
  edge [
    source 19
    target 551
  ]
  edge [
    source 19
    target 552
  ]
  edge [
    source 19
    target 553
  ]
  edge [
    source 19
    target 502
  ]
  edge [
    source 19
    target 554
  ]
  edge [
    source 19
    target 555
  ]
  edge [
    source 19
    target 556
  ]
  edge [
    source 19
    target 557
  ]
  edge [
    source 19
    target 558
  ]
  edge [
    source 19
    target 358
  ]
  edge [
    source 19
    target 559
  ]
  edge [
    source 19
    target 560
  ]
  edge [
    source 19
    target 561
  ]
  edge [
    source 19
    target 562
  ]
  edge [
    source 19
    target 563
  ]
  edge [
    source 19
    target 564
  ]
  edge [
    source 19
    target 565
  ]
  edge [
    source 19
    target 566
  ]
  edge [
    source 19
    target 567
  ]
  edge [
    source 19
    target 568
  ]
  edge [
    source 19
    target 569
  ]
  edge [
    source 19
    target 570
  ]
  edge [
    source 19
    target 67
  ]
  edge [
    source 19
    target 571
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 572
  ]
  edge [
    source 20
    target 573
  ]
  edge [
    source 20
    target 574
  ]
  edge [
    source 20
    target 575
  ]
  edge [
    source 20
    target 576
  ]
  edge [
    source 20
    target 577
  ]
  edge [
    source 20
    target 578
  ]
  edge [
    source 20
    target 579
  ]
  edge [
    source 20
    target 580
  ]
  edge [
    source 20
    target 581
  ]
  edge [
    source 20
    target 582
  ]
  edge [
    source 20
    target 583
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 584
  ]
  edge [
    source 21
    target 585
  ]
  edge [
    source 21
    target 586
  ]
  edge [
    source 21
    target 587
  ]
  edge [
    source 21
    target 588
  ]
  edge [
    source 21
    target 589
  ]
  edge [
    source 21
    target 590
  ]
  edge [
    source 21
    target 591
  ]
  edge [
    source 21
    target 592
  ]
  edge [
    source 21
    target 593
  ]
  edge [
    source 21
    target 594
  ]
  edge [
    source 21
    target 595
  ]
  edge [
    source 21
    target 596
  ]
  edge [
    source 21
    target 597
  ]
  edge [
    source 21
    target 598
  ]
  edge [
    source 21
    target 599
  ]
  edge [
    source 21
    target 600
  ]
  edge [
    source 21
    target 601
  ]
  edge [
    source 21
    target 602
  ]
  edge [
    source 21
    target 603
  ]
  edge [
    source 21
    target 604
  ]
  edge [
    source 21
    target 605
  ]
  edge [
    source 21
    target 606
  ]
  edge [
    source 21
    target 607
  ]
  edge [
    source 21
    target 608
  ]
  edge [
    source 21
    target 609
  ]
  edge [
    source 21
    target 610
  ]
  edge [
    source 21
    target 611
  ]
  edge [
    source 21
    target 612
  ]
  edge [
    source 21
    target 613
  ]
  edge [
    source 21
    target 614
  ]
  edge [
    source 21
    target 615
  ]
  edge [
    source 21
    target 616
  ]
  edge [
    source 21
    target 617
  ]
  edge [
    source 21
    target 618
  ]
  edge [
    source 21
    target 619
  ]
  edge [
    source 21
    target 620
  ]
  edge [
    source 21
    target 621
  ]
  edge [
    source 21
    target 622
  ]
  edge [
    source 21
    target 623
  ]
  edge [
    source 21
    target 624
  ]
  edge [
    source 21
    target 625
  ]
  edge [
    source 21
    target 626
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 627
  ]
  edge [
    source 22
    target 85
  ]
  edge [
    source 22
    target 628
  ]
  edge [
    source 22
    target 629
  ]
  edge [
    source 22
    target 630
  ]
  edge [
    source 22
    target 631
  ]
  edge [
    source 22
    target 41
  ]
  edge [
    source 22
    target 632
  ]
  edge [
    source 22
    target 633
  ]
  edge [
    source 22
    target 634
  ]
  edge [
    source 22
    target 635
  ]
  edge [
    source 22
    target 636
  ]
  edge [
    source 22
    target 637
  ]
  edge [
    source 22
    target 638
  ]
  edge [
    source 22
    target 639
  ]
  edge [
    source 22
    target 640
  ]
  edge [
    source 22
    target 641
  ]
  edge [
    source 22
    target 642
  ]
  edge [
    source 22
    target 643
  ]
  edge [
    source 22
    target 644
  ]
  edge [
    source 22
    target 645
  ]
  edge [
    source 22
    target 646
  ]
  edge [
    source 22
    target 647
  ]
  edge [
    source 22
    target 648
  ]
  edge [
    source 22
    target 649
  ]
  edge [
    source 22
    target 650
  ]
  edge [
    source 22
    target 651
  ]
  edge [
    source 22
    target 379
  ]
  edge [
    source 22
    target 652
  ]
  edge [
    source 22
    target 653
  ]
  edge [
    source 22
    target 654
  ]
  edge [
    source 22
    target 655
  ]
  edge [
    source 22
    target 656
  ]
  edge [
    source 22
    target 657
  ]
  edge [
    source 22
    target 658
  ]
  edge [
    source 22
    target 659
  ]
  edge [
    source 22
    target 660
  ]
  edge [
    source 22
    target 661
  ]
  edge [
    source 22
    target 662
  ]
  edge [
    source 22
    target 663
  ]
  edge [
    source 22
    target 664
  ]
  edge [
    source 22
    target 665
  ]
  edge [
    source 22
    target 666
  ]
  edge [
    source 22
    target 667
  ]
  edge [
    source 22
    target 668
  ]
  edge [
    source 22
    target 669
  ]
  edge [
    source 22
    target 670
  ]
  edge [
    source 22
    target 671
  ]
  edge [
    source 22
    target 672
  ]
  edge [
    source 22
    target 673
  ]
  edge [
    source 22
    target 674
  ]
  edge [
    source 22
    target 675
  ]
  edge [
    source 22
    target 676
  ]
  edge [
    source 22
    target 677
  ]
  edge [
    source 22
    target 678
  ]
  edge [
    source 22
    target 679
  ]
  edge [
    source 22
    target 680
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 85
  ]
  edge [
    source 23
    target 681
  ]
  edge [
    source 23
    target 682
  ]
  edge [
    source 23
    target 683
  ]
  edge [
    source 23
    target 284
  ]
  edge [
    source 23
    target 684
  ]
  edge [
    source 23
    target 685
  ]
  edge [
    source 23
    target 686
  ]
  edge [
    source 23
    target 687
  ]
  edge [
    source 23
    target 688
  ]
  edge [
    source 23
    target 689
  ]
  edge [
    source 23
    target 690
  ]
  edge [
    source 23
    target 691
  ]
  edge [
    source 23
    target 692
  ]
  edge [
    source 23
    target 693
  ]
  edge [
    source 23
    target 694
  ]
  edge [
    source 23
    target 695
  ]
  edge [
    source 23
    target 696
  ]
  edge [
    source 23
    target 697
  ]
  edge [
    source 23
    target 698
  ]
  edge [
    source 23
    target 699
  ]
  edge [
    source 23
    target 700
  ]
  edge [
    source 23
    target 701
  ]
  edge [
    source 23
    target 702
  ]
  edge [
    source 23
    target 703
  ]
  edge [
    source 23
    target 704
  ]
  edge [
    source 23
    target 56
  ]
  edge [
    source 23
    target 705
  ]
  edge [
    source 23
    target 706
  ]
  edge [
    source 23
    target 707
  ]
  edge [
    source 23
    target 708
  ]
  edge [
    source 23
    target 709
  ]
  edge [
    source 23
    target 710
  ]
  edge [
    source 23
    target 711
  ]
  edge [
    source 23
    target 712
  ]
  edge [
    source 23
    target 713
  ]
  edge [
    source 23
    target 714
  ]
  edge [
    source 23
    target 715
  ]
  edge [
    source 23
    target 716
  ]
  edge [
    source 23
    target 717
  ]
  edge [
    source 23
    target 718
  ]
  edge [
    source 23
    target 719
  ]
  edge [
    source 23
    target 720
  ]
  edge [
    source 23
    target 721
  ]
  edge [
    source 23
    target 722
  ]
  edge [
    source 23
    target 723
  ]
  edge [
    source 23
    target 724
  ]
  edge [
    source 23
    target 725
  ]
  edge [
    source 23
    target 726
  ]
  edge [
    source 23
    target 647
  ]
  edge [
    source 23
    target 648
  ]
  edge [
    source 23
    target 649
  ]
  edge [
    source 23
    target 650
  ]
  edge [
    source 23
    target 651
  ]
  edge [
    source 23
    target 379
  ]
  edge [
    source 23
    target 652
  ]
  edge [
    source 23
    target 653
  ]
  edge [
    source 23
    target 654
  ]
  edge [
    source 23
    target 655
  ]
  edge [
    source 23
    target 656
  ]
  edge [
    source 23
    target 657
  ]
  edge [
    source 23
    target 658
  ]
  edge [
    source 23
    target 659
  ]
  edge [
    source 23
    target 660
  ]
  edge [
    source 23
    target 661
  ]
  edge [
    source 23
    target 662
  ]
  edge [
    source 23
    target 663
  ]
  edge [
    source 23
    target 664
  ]
  edge [
    source 23
    target 665
  ]
  edge [
    source 23
    target 666
  ]
  edge [
    source 23
    target 667
  ]
  edge [
    source 23
    target 668
  ]
  edge [
    source 23
    target 669
  ]
  edge [
    source 23
    target 670
  ]
  edge [
    source 23
    target 727
  ]
  edge [
    source 23
    target 728
  ]
  edge [
    source 23
    target 729
  ]
  edge [
    source 23
    target 730
  ]
  edge [
    source 23
    target 731
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 734
  ]
  edge [
    source 23
    target 735
  ]
  edge [
    source 23
    target 736
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 737
  ]
  edge [
    source 23
    target 738
  ]
  edge [
    source 23
    target 739
  ]
  edge [
    source 23
    target 740
  ]
  edge [
    source 23
    target 160
  ]
  edge [
    source 23
    target 741
  ]
  edge [
    source 23
    target 742
  ]
  edge [
    source 23
    target 743
  ]
  edge [
    source 23
    target 744
  ]
  edge [
    source 23
    target 119
  ]
  edge [
    source 23
    target 745
  ]
  edge [
    source 23
    target 746
  ]
  edge [
    source 23
    target 747
  ]
  edge [
    source 23
    target 748
  ]
  edge [
    source 23
    target 185
  ]
  edge [
    source 23
    target 749
  ]
  edge [
    source 23
    target 750
  ]
  edge [
    source 23
    target 751
  ]
  edge [
    source 23
    target 752
  ]
  edge [
    source 23
    target 753
  ]
  edge [
    source 23
    target 754
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 755
  ]
  edge [
    source 24
    target 599
  ]
  edge [
    source 24
    target 756
  ]
  edge [
    source 24
    target 757
  ]
  edge [
    source 24
    target 758
  ]
  edge [
    source 24
    target 759
  ]
  edge [
    source 24
    target 602
  ]
  edge [
    source 24
    target 760
  ]
  edge [
    source 24
    target 761
  ]
  edge [
    source 24
    target 762
  ]
  edge [
    source 24
    target 763
  ]
  edge [
    source 24
    target 764
  ]
  edge [
    source 24
    target 765
  ]
  edge [
    source 24
    target 766
  ]
  edge [
    source 24
    target 767
  ]
  edge [
    source 24
    target 768
  ]
  edge [
    source 24
    target 769
  ]
  edge [
    source 24
    target 770
  ]
  edge [
    source 24
    target 771
  ]
  edge [
    source 24
    target 435
  ]
  edge [
    source 24
    target 772
  ]
  edge [
    source 24
    target 773
  ]
  edge [
    source 24
    target 774
  ]
  edge [
    source 24
    target 617
  ]
  edge [
    source 24
    target 775
  ]
  edge [
    source 24
    target 776
  ]
  edge [
    source 24
    target 777
  ]
  edge [
    source 24
    target 588
  ]
  edge [
    source 24
    target 778
  ]
  edge [
    source 24
    target 779
  ]
  edge [
    source 24
    target 780
  ]
  edge [
    source 24
    target 781
  ]
  edge [
    source 24
    target 782
  ]
  edge [
    source 24
    target 598
  ]
  edge [
    source 24
    target 783
  ]
  edge [
    source 24
    target 784
  ]
  edge [
    source 24
    target 785
  ]
  edge [
    source 24
    target 786
  ]
  edge [
    source 25
    target 787
  ]
  edge [
    source 25
    target 788
  ]
  edge [
    source 25
    target 789
  ]
  edge [
    source 25
    target 790
  ]
  edge [
    source 25
    target 791
  ]
  edge [
    source 25
    target 32
  ]
  edge [
    source 25
    target 792
  ]
  edge [
    source 25
    target 793
  ]
  edge [
    source 25
    target 794
  ]
  edge [
    source 25
    target 795
  ]
  edge [
    source 25
    target 796
  ]
  edge [
    source 25
    target 797
  ]
  edge [
    source 25
    target 798
  ]
  edge [
    source 25
    target 799
  ]
  edge [
    source 25
    target 671
  ]
  edge [
    source 25
    target 800
  ]
  edge [
    source 25
    target 677
  ]
  edge [
    source 25
    target 801
  ]
  edge [
    source 25
    target 802
  ]
  edge [
    source 25
    target 803
  ]
  edge [
    source 25
    target 804
  ]
  edge [
    source 25
    target 805
  ]
  edge [
    source 25
    target 806
  ]
  edge [
    source 25
    target 807
  ]
  edge [
    source 25
    target 808
  ]
  edge [
    source 25
    target 809
  ]
  edge [
    source 25
    target 810
  ]
  edge [
    source 25
    target 811
  ]
  edge [
    source 25
    target 85
  ]
  edge [
    source 25
    target 812
  ]
  edge [
    source 25
    target 813
  ]
  edge [
    source 25
    target 814
  ]
  edge [
    source 25
    target 815
  ]
  edge [
    source 25
    target 816
  ]
  edge [
    source 25
    target 817
  ]
  edge [
    source 25
    target 818
  ]
  edge [
    source 25
    target 819
  ]
  edge [
    source 25
    target 820
  ]
  edge [
    source 25
    target 821
  ]
  edge [
    source 25
    target 822
  ]
  edge [
    source 25
    target 823
  ]
  edge [
    source 25
    target 824
  ]
  edge [
    source 25
    target 825
  ]
  edge [
    source 25
    target 826
  ]
  edge [
    source 25
    target 827
  ]
  edge [
    source 25
    target 828
  ]
  edge [
    source 25
    target 829
  ]
  edge [
    source 25
    target 830
  ]
  edge [
    source 25
    target 831
  ]
  edge [
    source 25
    target 832
  ]
  edge [
    source 25
    target 833
  ]
  edge [
    source 25
    target 834
  ]
  edge [
    source 25
    target 835
  ]
  edge [
    source 25
    target 836
  ]
  edge [
    source 25
    target 837
  ]
  edge [
    source 25
    target 838
  ]
  edge [
    source 25
    target 839
  ]
  edge [
    source 25
    target 840
  ]
  edge [
    source 25
    target 841
  ]
  edge [
    source 25
    target 842
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 843
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 844
  ]
  edge [
    source 26
    target 845
  ]
  edge [
    source 26
    target 846
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 847
  ]
  edge [
    source 26
    target 848
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 26
    target 361
  ]
  edge [
    source 26
    target 849
  ]
  edge [
    source 26
    target 850
  ]
  edge [
    source 26
    target 851
  ]
  edge [
    source 26
    target 852
  ]
  edge [
    source 26
    target 853
  ]
  edge [
    source 26
    target 854
  ]
  edge [
    source 26
    target 855
  ]
  edge [
    source 26
    target 856
  ]
  edge [
    source 26
    target 857
  ]
  edge [
    source 26
    target 183
  ]
  edge [
    source 26
    target 858
  ]
  edge [
    source 26
    target 859
  ]
  edge [
    source 26
    target 860
  ]
  edge [
    source 26
    target 861
  ]
  edge [
    source 26
    target 862
  ]
  edge [
    source 26
    target 77
  ]
  edge [
    source 26
    target 463
  ]
  edge [
    source 26
    target 863
  ]
  edge [
    source 26
    target 280
  ]
  edge [
    source 26
    target 864
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 865
  ]
  edge [
    source 27
    target 747
  ]
  edge [
    source 27
    target 866
  ]
  edge [
    source 27
    target 867
  ]
  edge [
    source 27
    target 868
  ]
  edge [
    source 27
    target 745
  ]
  edge [
    source 27
    target 869
  ]
  edge [
    source 27
    target 870
  ]
  edge [
    source 27
    target 871
  ]
  edge [
    source 27
    target 128
  ]
  edge [
    source 27
    target 872
  ]
  edge [
    source 27
    target 873
  ]
  edge [
    source 27
    target 874
  ]
  edge [
    source 27
    target 875
  ]
  edge [
    source 27
    target 876
  ]
  edge [
    source 27
    target 877
  ]
  edge [
    source 27
    target 878
  ]
  edge [
    source 27
    target 879
  ]
  edge [
    source 27
    target 880
  ]
  edge [
    source 27
    target 881
  ]
  edge [
    source 27
    target 882
  ]
  edge [
    source 27
    target 883
  ]
  edge [
    source 27
    target 884
  ]
  edge [
    source 27
    target 885
  ]
  edge [
    source 27
    target 886
  ]
  edge [
    source 27
    target 855
  ]
  edge [
    source 27
    target 887
  ]
  edge [
    source 27
    target 72
  ]
  edge [
    source 27
    target 888
  ]
  edge [
    source 27
    target 889
  ]
  edge [
    source 27
    target 890
  ]
  edge [
    source 27
    target 891
  ]
  edge [
    source 27
    target 892
  ]
  edge [
    source 27
    target 125
  ]
  edge [
    source 27
    target 893
  ]
  edge [
    source 27
    target 894
  ]
  edge [
    source 27
    target 895
  ]
  edge [
    source 27
    target 896
  ]
  edge [
    source 27
    target 897
  ]
  edge [
    source 27
    target 898
  ]
  edge [
    source 27
    target 899
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 900
  ]
  edge [
    source 28
    target 901
  ]
  edge [
    source 28
    target 902
  ]
  edge [
    source 28
    target 903
  ]
  edge [
    source 28
    target 904
  ]
  edge [
    source 28
    target 905
  ]
  edge [
    source 28
    target 906
  ]
  edge [
    source 28
    target 629
  ]
  edge [
    source 28
    target 907
  ]
  edge [
    source 28
    target 32
  ]
  edge [
    source 28
    target 638
  ]
  edge [
    source 28
    target 908
  ]
  edge [
    source 28
    target 909
  ]
  edge [
    source 28
    target 910
  ]
  edge [
    source 28
    target 911
  ]
  edge [
    source 28
    target 912
  ]
  edge [
    source 28
    target 913
  ]
  edge [
    source 28
    target 914
  ]
  edge [
    source 28
    target 915
  ]
  edge [
    source 28
    target 916
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 193
  ]
  edge [
    source 29
    target 917
  ]
  edge [
    source 29
    target 918
  ]
  edge [
    source 29
    target 919
  ]
  edge [
    source 29
    target 920
  ]
  edge [
    source 29
    target 921
  ]
  edge [
    source 29
    target 922
  ]
  edge [
    source 29
    target 923
  ]
  edge [
    source 29
    target 924
  ]
  edge [
    source 29
    target 925
  ]
  edge [
    source 29
    target 926
  ]
  edge [
    source 29
    target 927
  ]
  edge [
    source 29
    target 928
  ]
  edge [
    source 29
    target 929
  ]
  edge [
    source 29
    target 930
  ]
  edge [
    source 29
    target 931
  ]
  edge [
    source 29
    target 932
  ]
  edge [
    source 29
    target 933
  ]
  edge [
    source 29
    target 934
  ]
  edge [
    source 29
    target 935
  ]
  edge [
    source 29
    target 936
  ]
  edge [
    source 29
    target 937
  ]
  edge [
    source 29
    target 938
  ]
  edge [
    source 29
    target 939
  ]
  edge [
    source 29
    target 940
  ]
  edge [
    source 29
    target 941
  ]
  edge [
    source 29
    target 373
  ]
  edge [
    source 29
    target 942
  ]
  edge [
    source 29
    target 943
  ]
  edge [
    source 29
    target 944
  ]
  edge [
    source 29
    target 945
  ]
  edge [
    source 29
    target 946
  ]
  edge [
    source 29
    target 947
  ]
  edge [
    source 29
    target 948
  ]
  edge [
    source 29
    target 949
  ]
  edge [
    source 29
    target 950
  ]
  edge [
    source 29
    target 951
  ]
  edge [
    source 29
    target 135
  ]
  edge [
    source 29
    target 952
  ]
  edge [
    source 29
    target 953
  ]
  edge [
    source 29
    target 954
  ]
  edge [
    source 29
    target 955
  ]
  edge [
    source 29
    target 956
  ]
  edge [
    source 29
    target 957
  ]
  edge [
    source 29
    target 958
  ]
  edge [
    source 29
    target 959
  ]
  edge [
    source 29
    target 960
  ]
  edge [
    source 29
    target 961
  ]
  edge [
    source 29
    target 962
  ]
  edge [
    source 29
    target 963
  ]
  edge [
    source 29
    target 964
  ]
  edge [
    source 29
    target 965
  ]
  edge [
    source 29
    target 966
  ]
  edge [
    source 29
    target 967
  ]
  edge [
    source 29
    target 968
  ]
  edge [
    source 29
    target 969
  ]
  edge [
    source 29
    target 970
  ]
  edge [
    source 29
    target 971
  ]
  edge [
    source 29
    target 972
  ]
  edge [
    source 29
    target 973
  ]
  edge [
    source 29
    target 578
  ]
  edge [
    source 29
    target 974
  ]
  edge [
    source 29
    target 975
  ]
  edge [
    source 29
    target 976
  ]
  edge [
    source 29
    target 977
  ]
  edge [
    source 29
    target 978
  ]
  edge [
    source 29
    target 979
  ]
  edge [
    source 29
    target 980
  ]
  edge [
    source 29
    target 981
  ]
  edge [
    source 29
    target 982
  ]
  edge [
    source 29
    target 983
  ]
  edge [
    source 29
    target 984
  ]
  edge [
    source 29
    target 985
  ]
  edge [
    source 29
    target 986
  ]
  edge [
    source 29
    target 204
  ]
  edge [
    source 29
    target 205
  ]
  edge [
    source 29
    target 206
  ]
  edge [
    source 29
    target 207
  ]
  edge [
    source 29
    target 208
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 211
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 29
    target 213
  ]
  edge [
    source 29
    target 214
  ]
  edge [
    source 29
    target 215
  ]
  edge [
    source 29
    target 216
  ]
  edge [
    source 29
    target 217
  ]
  edge [
    source 29
    target 218
  ]
  edge [
    source 29
    target 219
  ]
  edge [
    source 29
    target 220
  ]
  edge [
    source 29
    target 221
  ]
  edge [
    source 29
    target 222
  ]
  edge [
    source 29
    target 223
  ]
  edge [
    source 29
    target 224
  ]
  edge [
    source 29
    target 225
  ]
  edge [
    source 29
    target 226
  ]
  edge [
    source 29
    target 227
  ]
  edge [
    source 29
    target 228
  ]
  edge [
    source 29
    target 987
  ]
  edge [
    source 29
    target 988
  ]
  edge [
    source 29
    target 989
  ]
  edge [
    source 29
    target 990
  ]
  edge [
    source 29
    target 991
  ]
  edge [
    source 29
    target 992
  ]
  edge [
    source 29
    target 993
  ]
  edge [
    source 29
    target 994
  ]
  edge [
    source 29
    target 995
  ]
  edge [
    source 29
    target 996
  ]
  edge [
    source 29
    target 842
  ]
  edge [
    source 29
    target 997
  ]
  edge [
    source 29
    target 998
  ]
  edge [
    source 29
    target 999
  ]
  edge [
    source 29
    target 1000
  ]
  edge [
    source 29
    target 1001
  ]
  edge [
    source 29
    target 1002
  ]
  edge [
    source 29
    target 1003
  ]
  edge [
    source 29
    target 1004
  ]
  edge [
    source 29
    target 1005
  ]
  edge [
    source 29
    target 1006
  ]
  edge [
    source 29
    target 1007
  ]
  edge [
    source 29
    target 1008
  ]
  edge [
    source 29
    target 1009
  ]
  edge [
    source 29
    target 1010
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1011
  ]
  edge [
    source 30
    target 1012
  ]
  edge [
    source 30
    target 86
  ]
  edge [
    source 30
    target 1013
  ]
  edge [
    source 30
    target 1014
  ]
  edge [
    source 30
    target 1015
  ]
  edge [
    source 30
    target 1016
  ]
  edge [
    source 30
    target 1017
  ]
  edge [
    source 30
    target 1018
  ]
  edge [
    source 30
    target 1019
  ]
  edge [
    source 30
    target 1020
  ]
  edge [
    source 30
    target 1021
  ]
  edge [
    source 30
    target 1022
  ]
  edge [
    source 30
    target 1023
  ]
  edge [
    source 30
    target 1024
  ]
  edge [
    source 30
    target 1025
  ]
  edge [
    source 30
    target 760
  ]
  edge [
    source 30
    target 1026
  ]
  edge [
    source 30
    target 1027
  ]
  edge [
    source 30
    target 1028
  ]
  edge [
    source 30
    target 1029
  ]
  edge [
    source 30
    target 1030
  ]
  edge [
    source 30
    target 1031
  ]
  edge [
    source 30
    target 1032
  ]
  edge [
    source 30
    target 1033
  ]
  edge [
    source 30
    target 1034
  ]
  edge [
    source 30
    target 1035
  ]
  edge [
    source 30
    target 1036
  ]
  edge [
    source 30
    target 1037
  ]
  edge [
    source 30
    target 1038
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1039
  ]
  edge [
    source 31
    target 1040
  ]
  edge [
    source 31
    target 1041
  ]
  edge [
    source 31
    target 1042
  ]
  edge [
    source 31
    target 1043
  ]
  edge [
    source 31
    target 1044
  ]
  edge [
    source 31
    target 1045
  ]
  edge [
    source 31
    target 1046
  ]
  edge [
    source 31
    target 602
  ]
  edge [
    source 31
    target 1047
  ]
  edge [
    source 31
    target 1048
  ]
  edge [
    source 31
    target 1049
  ]
  edge [
    source 31
    target 1050
  ]
  edge [
    source 31
    target 1051
  ]
  edge [
    source 31
    target 1052
  ]
  edge [
    source 31
    target 1026
  ]
  edge [
    source 31
    target 1053
  ]
  edge [
    source 31
    target 1054
  ]
  edge [
    source 31
    target 1055
  ]
  edge [
    source 31
    target 1056
  ]
  edge [
    source 31
    target 1057
  ]
  edge [
    source 31
    target 1058
  ]
  edge [
    source 31
    target 1059
  ]
  edge [
    source 31
    target 1060
  ]
  edge [
    source 31
    target 347
  ]
  edge [
    source 31
    target 1061
  ]
  edge [
    source 31
    target 1062
  ]
  edge [
    source 31
    target 1063
  ]
  edge [
    source 31
    target 1064
  ]
  edge [
    source 31
    target 1065
  ]
  edge [
    source 31
    target 1066
  ]
  edge [
    source 31
    target 1067
  ]
  edge [
    source 31
    target 1068
  ]
  edge [
    source 31
    target 1069
  ]
  edge [
    source 31
    target 1070
  ]
  edge [
    source 31
    target 1071
  ]
  edge [
    source 31
    target 1072
  ]
  edge [
    source 31
    target 1073
  ]
  edge [
    source 31
    target 1074
  ]
  edge [
    source 31
    target 1075
  ]
  edge [
    source 31
    target 770
  ]
  edge [
    source 31
    target 1076
  ]
  edge [
    source 31
    target 1077
  ]
  edge [
    source 31
    target 1078
  ]
  edge [
    source 31
    target 1079
  ]
  edge [
    source 31
    target 1080
  ]
  edge [
    source 31
    target 1081
  ]
  edge [
    source 31
    target 1082
  ]
  edge [
    source 31
    target 1083
  ]
  edge [
    source 31
    target 1084
  ]
  edge [
    source 31
    target 1085
  ]
  edge [
    source 31
    target 1086
  ]
  edge [
    source 31
    target 1087
  ]
  edge [
    source 31
    target 1088
  ]
  edge [
    source 31
    target 1089
  ]
  edge [
    source 31
    target 1028
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 793
  ]
  edge [
    source 32
    target 794
  ]
  edge [
    source 32
    target 795
  ]
  edge [
    source 32
    target 796
  ]
  edge [
    source 32
    target 797
  ]
  edge [
    source 32
    target 798
  ]
  edge [
    source 32
    target 799
  ]
  edge [
    source 32
    target 671
  ]
  edge [
    source 32
    target 800
  ]
  edge [
    source 32
    target 677
  ]
  edge [
    source 32
    target 801
  ]
  edge [
    source 32
    target 802
  ]
  edge [
    source 32
    target 803
  ]
  edge [
    source 32
    target 804
  ]
  edge [
    source 32
    target 805
  ]
  edge [
    source 32
    target 806
  ]
  edge [
    source 32
    target 807
  ]
  edge [
    source 32
    target 808
  ]
  edge [
    source 32
    target 809
  ]
  edge [
    source 32
    target 1090
  ]
  edge [
    source 32
    target 1091
  ]
  edge [
    source 32
    target 1092
  ]
  edge [
    source 32
    target 1093
  ]
  edge [
    source 32
    target 1094
  ]
  edge [
    source 32
    target 1095
  ]
  edge [
    source 32
    target 1096
  ]
  edge [
    source 32
    target 1097
  ]
  edge [
    source 32
    target 1098
  ]
  edge [
    source 32
    target 1099
  ]
  edge [
    source 32
    target 1100
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 1101
  ]
  edge [
    source 32
    target 1102
  ]
  edge [
    source 32
    target 1103
  ]
  edge [
    source 32
    target 1104
  ]
  edge [
    source 32
    target 1105
  ]
  edge [
    source 32
    target 1106
  ]
  edge [
    source 32
    target 1107
  ]
  edge [
    source 32
    target 1108
  ]
  edge [
    source 32
    target 1109
  ]
  edge [
    source 32
    target 1110
  ]
  edge [
    source 32
    target 1111
  ]
  edge [
    source 32
    target 1112
  ]
  edge [
    source 32
    target 41
  ]
  edge [
    source 32
    target 1113
  ]
  edge [
    source 32
    target 1114
  ]
  edge [
    source 32
    target 1115
  ]
  edge [
    source 32
    target 1116
  ]
  edge [
    source 32
    target 1117
  ]
  edge [
    source 32
    target 1118
  ]
  edge [
    source 32
    target 1119
  ]
  edge [
    source 32
    target 1120
  ]
  edge [
    source 32
    target 1121
  ]
  edge [
    source 32
    target 1122
  ]
  edge [
    source 32
    target 1123
  ]
  edge [
    source 32
    target 1124
  ]
  edge [
    source 32
    target 1125
  ]
  edge [
    source 32
    target 1126
  ]
  edge [
    source 32
    target 1127
  ]
  edge [
    source 32
    target 1128
  ]
  edge [
    source 32
    target 1129
  ]
  edge [
    source 32
    target 1130
  ]
  edge [
    source 32
    target 1131
  ]
  edge [
    source 32
    target 1132
  ]
  edge [
    source 32
    target 1133
  ]
  edge [
    source 32
    target 85
  ]
  edge [
    source 32
    target 1134
  ]
  edge [
    source 32
    target 1135
  ]
  edge [
    source 32
    target 1136
  ]
  edge [
    source 32
    target 1137
  ]
  edge [
    source 32
    target 1138
  ]
  edge [
    source 32
    target 1139
  ]
  edge [
    source 32
    target 1140
  ]
  edge [
    source 32
    target 1141
  ]
  edge [
    source 32
    target 1142
  ]
  edge [
    source 32
    target 1143
  ]
  edge [
    source 32
    target 1144
  ]
  edge [
    source 32
    target 1145
  ]
  edge [
    source 32
    target 1146
  ]
  edge [
    source 32
    target 834
  ]
  edge [
    source 32
    target 1147
  ]
  edge [
    source 32
    target 1148
  ]
  edge [
    source 32
    target 1149
  ]
  edge [
    source 32
    target 475
  ]
  edge [
    source 32
    target 56
  ]
  edge [
    source 32
    target 1150
  ]
  edge [
    source 32
    target 1151
  ]
  edge [
    source 32
    target 1152
  ]
  edge [
    source 32
    target 1153
  ]
  edge [
    source 32
    target 1154
  ]
  edge [
    source 32
    target 1155
  ]
  edge [
    source 32
    target 1156
  ]
  edge [
    source 32
    target 1157
  ]
  edge [
    source 32
    target 305
  ]
  edge [
    source 32
    target 1158
  ]
  edge [
    source 32
    target 1159
  ]
  edge [
    source 32
    target 1160
  ]
  edge [
    source 32
    target 1161
  ]
  edge [
    source 32
    target 1162
  ]
  edge [
    source 32
    target 1163
  ]
  edge [
    source 32
    target 855
  ]
  edge [
    source 32
    target 1164
  ]
  edge [
    source 32
    target 1165
  ]
  edge [
    source 32
    target 1166
  ]
  edge [
    source 32
    target 1167
  ]
  edge [
    source 32
    target 1168
  ]
  edge [
    source 32
    target 1169
  ]
  edge [
    source 32
    target 1170
  ]
  edge [
    source 32
    target 1171
  ]
  edge [
    source 32
    target 369
  ]
  edge [
    source 32
    target 1172
  ]
  edge [
    source 32
    target 1173
  ]
  edge [
    source 32
    target 1174
  ]
  edge [
    source 32
    target 1175
  ]
  edge [
    source 32
    target 1176
  ]
  edge [
    source 32
    target 1177
  ]
  edge [
    source 32
    target 1178
  ]
  edge [
    source 32
    target 1179
  ]
  edge [
    source 32
    target 1180
  ]
  edge [
    source 32
    target 992
  ]
  edge [
    source 32
    target 1181
  ]
  edge [
    source 32
    target 1182
  ]
  edge [
    source 32
    target 1183
  ]
  edge [
    source 32
    target 1184
  ]
  edge [
    source 32
    target 1185
  ]
  edge [
    source 32
    target 1186
  ]
  edge [
    source 32
    target 1187
  ]
  edge [
    source 32
    target 1188
  ]
  edge [
    source 32
    target 1189
  ]
  edge [
    source 32
    target 1190
  ]
  edge [
    source 32
    target 1191
  ]
  edge [
    source 32
    target 1192
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 88
  ]
  edge [
    source 33
    target 43
  ]
  edge [
    source 33
    target 89
  ]
  edge [
    source 33
    target 90
  ]
  edge [
    source 33
    target 1193
  ]
  edge [
    source 33
    target 91
  ]
  edge [
    source 33
    target 1194
  ]
  edge [
    source 33
    target 93
  ]
  edge [
    source 33
    target 1195
  ]
  edge [
    source 33
    target 95
  ]
  edge [
    source 33
    target 1196
  ]
  edge [
    source 33
    target 97
  ]
  edge [
    source 33
    target 1197
  ]
  edge [
    source 33
    target 1198
  ]
  edge [
    source 33
    target 1199
  ]
  edge [
    source 33
    target 365
  ]
  edge [
    source 33
    target 963
  ]
  edge [
    source 33
    target 1200
  ]
  edge [
    source 33
    target 1201
  ]
  edge [
    source 33
    target 1202
  ]
  edge [
    source 33
    target 1203
  ]
  edge [
    source 33
    target 128
  ]
  edge [
    source 33
    target 1204
  ]
  edge [
    source 33
    target 1205
  ]
  edge [
    source 33
    target 1206
  ]
  edge [
    source 33
    target 50
  ]
  edge [
    source 33
    target 1207
  ]
  edge [
    source 33
    target 1208
  ]
  edge [
    source 33
    target 1209
  ]
  edge [
    source 33
    target 1210
  ]
  edge [
    source 33
    target 1211
  ]
  edge [
    source 33
    target 370
  ]
  edge [
    source 33
    target 368
  ]
  edge [
    source 33
    target 1212
  ]
  edge [
    source 33
    target 1213
  ]
  edge [
    source 33
    target 1214
  ]
  edge [
    source 33
    target 1215
  ]
  edge [
    source 33
    target 84
  ]
  edge [
    source 33
    target 85
  ]
  edge [
    source 33
    target 48
  ]
  edge [
    source 33
    target 86
  ]
  edge [
    source 33
    target 87
  ]
  edge [
    source 33
    target 762
  ]
  edge [
    source 33
    target 1216
  ]
  edge [
    source 33
    target 1217
  ]
  edge [
    source 33
    target 602
  ]
  edge [
    source 33
    target 1218
  ]
  edge [
    source 33
    target 454
  ]
  edge [
    source 33
    target 1219
  ]
  edge [
    source 33
    target 1220
  ]
  edge [
    source 33
    target 599
  ]
  edge [
    source 33
    target 1221
  ]
  edge [
    source 33
    target 1222
  ]
  edge [
    source 33
    target 435
  ]
  edge [
    source 33
    target 1223
  ]
  edge [
    source 33
    target 1224
  ]
  edge [
    source 33
    target 1225
  ]
  edge [
    source 33
    target 1226
  ]
  edge [
    source 33
    target 588
  ]
  edge [
    source 33
    target 587
  ]
  edge [
    source 33
    target 1227
  ]
  edge [
    source 33
    target 1228
  ]
  edge [
    source 33
    target 779
  ]
  edge [
    source 33
    target 781
  ]
  edge [
    source 33
    target 1229
  ]
  edge [
    source 33
    target 1230
  ]
  edge [
    source 33
    target 598
  ]
  edge [
    source 33
    target 1231
  ]
  edge [
    source 33
    target 754
  ]
  edge [
    source 33
    target 1056
  ]
  edge [
    source 33
    target 1066
  ]
  edge [
    source 33
    target 1070
  ]
  edge [
    source 33
    target 1232
  ]
  edge [
    source 33
    target 1039
  ]
  edge [
    source 33
    target 1233
  ]
  edge [
    source 33
    target 1234
  ]
  edge [
    source 33
    target 1235
  ]
  edge [
    source 33
    target 1236
  ]
  edge [
    source 33
    target 1237
  ]
  edge [
    source 33
    target 1079
  ]
  edge [
    source 33
    target 1238
  ]
  edge [
    source 33
    target 1239
  ]
  edge [
    source 33
    target 1084
  ]
  edge [
    source 33
    target 269
  ]
  edge [
    source 33
    target 1240
  ]
  edge [
    source 33
    target 1241
  ]
  edge [
    source 33
    target 1242
  ]
  edge [
    source 33
    target 684
  ]
  edge [
    source 33
    target 56
  ]
  edge [
    source 35
    target 562
  ]
  edge [
    source 35
    target 563
  ]
  edge [
    source 35
    target 564
  ]
  edge [
    source 35
    target 565
  ]
  edge [
    source 35
    target 566
  ]
  edge [
    source 35
    target 567
  ]
  edge [
    source 35
    target 568
  ]
  edge [
    source 35
    target 569
  ]
  edge [
    source 35
    target 361
  ]
  edge [
    source 35
    target 570
  ]
  edge [
    source 35
    target 67
  ]
  edge [
    source 35
    target 571
  ]
  edge [
    source 35
    target 1243
  ]
  edge [
    source 35
    target 54
  ]
  edge [
    source 35
    target 1244
  ]
  edge [
    source 35
    target 376
  ]
  edge [
    source 35
    target 648
  ]
  edge [
    source 35
    target 296
  ]
  edge [
    source 35
    target 433
  ]
  edge [
    source 35
    target 650
  ]
  edge [
    source 35
    target 364
  ]
  edge [
    source 35
    target 1245
  ]
  edge [
    source 35
    target 56
  ]
  edge [
    source 35
    target 1246
  ]
  edge [
    source 35
    target 1247
  ]
  edge [
    source 35
    target 60
  ]
  edge [
    source 35
    target 1248
  ]
  edge [
    source 35
    target 1249
  ]
  edge [
    source 35
    target 1250
  ]
  edge [
    source 35
    target 1251
  ]
  edge [
    source 35
    target 723
  ]
  edge [
    source 35
    target 1252
  ]
  edge [
    source 35
    target 1253
  ]
  edge [
    source 35
    target 1254
  ]
  edge [
    source 35
    target 1255
  ]
  edge [
    source 35
    target 55
  ]
  edge [
    source 35
    target 70
  ]
  edge [
    source 35
    target 1256
  ]
  edge [
    source 35
    target 61
  ]
  edge [
    source 35
    target 578
  ]
  edge [
    source 35
    target 1257
  ]
  edge [
    source 35
    target 524
  ]
  edge [
    source 35
    target 525
  ]
  edge [
    source 35
    target 526
  ]
  edge [
    source 35
    target 527
  ]
  edge [
    source 35
    target 528
  ]
  edge [
    source 35
    target 529
  ]
  edge [
    source 35
    target 530
  ]
  edge [
    source 35
    target 531
  ]
  edge [
    source 35
    target 532
  ]
  edge [
    source 35
    target 533
  ]
  edge [
    source 35
    target 534
  ]
  edge [
    source 35
    target 463
  ]
  edge [
    source 35
    target 535
  ]
  edge [
    source 35
    target 536
  ]
  edge [
    source 35
    target 537
  ]
  edge [
    source 35
    target 538
  ]
  edge [
    source 35
    target 539
  ]
  edge [
    source 35
    target 540
  ]
  edge [
    source 35
    target 541
  ]
  edge [
    source 35
    target 542
  ]
  edge [
    source 35
    target 543
  ]
  edge [
    source 35
    target 544
  ]
  edge [
    source 35
    target 545
  ]
  edge [
    source 35
    target 546
  ]
  edge [
    source 35
    target 547
  ]
  edge [
    source 35
    target 548
  ]
  edge [
    source 35
    target 549
  ]
  edge [
    source 35
    target 550
  ]
  edge [
    source 35
    target 551
  ]
  edge [
    source 35
    target 552
  ]
  edge [
    source 35
    target 553
  ]
  edge [
    source 35
    target 502
  ]
  edge [
    source 35
    target 554
  ]
  edge [
    source 35
    target 555
  ]
  edge [
    source 35
    target 556
  ]
  edge [
    source 35
    target 557
  ]
  edge [
    source 35
    target 558
  ]
  edge [
    source 35
    target 1212
  ]
  edge [
    source 35
    target 1258
  ]
  edge [
    source 35
    target 1259
  ]
  edge [
    source 35
    target 1260
  ]
  edge [
    source 35
    target 1261
  ]
  edge [
    source 35
    target 1262
  ]
  edge [
    source 35
    target 1263
  ]
  edge [
    source 35
    target 458
  ]
  edge [
    source 35
    target 459
  ]
  edge [
    source 35
    target 460
  ]
  edge [
    source 35
    target 461
  ]
  edge [
    source 35
    target 1264
  ]
  edge [
    source 35
    target 1265
  ]
  edge [
    source 35
    target 1266
  ]
  edge [
    source 35
    target 1267
  ]
  edge [
    source 35
    target 1268
  ]
  edge [
    source 35
    target 48
  ]
  edge [
    source 35
    target 1269
  ]
  edge [
    source 35
    target 1270
  ]
  edge [
    source 35
    target 1271
  ]
  edge [
    source 35
    target 1272
  ]
  edge [
    source 35
    target 1273
  ]
  edge [
    source 35
    target 1274
  ]
  edge [
    source 35
    target 1275
  ]
  edge [
    source 35
    target 1276
  ]
  edge [
    source 35
    target 1277
  ]
  edge [
    source 35
    target 1278
  ]
  edge [
    source 35
    target 1279
  ]
  edge [
    source 35
    target 1280
  ]
  edge [
    source 35
    target 1281
  ]
  edge [
    source 35
    target 1282
  ]
  edge [
    source 35
    target 1283
  ]
  edge [
    source 35
    target 1284
  ]
  edge [
    source 35
    target 1285
  ]
  edge [
    source 35
    target 1286
  ]
  edge [
    source 35
    target 1287
  ]
  edge [
    source 35
    target 417
  ]
  edge [
    source 35
    target 1288
  ]
  edge [
    source 35
    target 1289
  ]
  edge [
    source 35
    target 847
  ]
  edge [
    source 35
    target 1290
  ]
  edge [
    source 35
    target 1291
  ]
  edge [
    source 35
    target 1292
  ]
  edge [
    source 35
    target 969
  ]
  edge [
    source 35
    target 1293
  ]
  edge [
    source 35
    target 1294
  ]
  edge [
    source 35
    target 1295
  ]
  edge [
    source 35
    target 1296
  ]
  edge [
    source 35
    target 1297
  ]
  edge [
    source 35
    target 1298
  ]
  edge [
    source 44
    target 1317
  ]
  edge [
    source 230
    target 984
  ]
  edge [
    source 230
    target 1320
  ]
  edge [
    source 318
    target 1299
  ]
  edge [
    source 318
    target 1300
  ]
  edge [
    source 318
    target 1329
  ]
  edge [
    source 733
    target 1314
  ]
  edge [
    source 984
    target 1320
  ]
  edge [
    source 1138
    target 1311
  ]
  edge [
    source 1192
    target 1321
  ]
  edge [
    source 1192
    target 1322
  ]
  edge [
    source 1192
    target 1323
  ]
  edge [
    source 1192
    target 1324
  ]
  edge [
    source 1192
    target 1325
  ]
  edge [
    source 1299
    target 1329
  ]
  edge [
    source 1301
    target 1302
  ]
  edge [
    source 1303
    target 1304
  ]
  edge [
    source 1305
    target 1306
  ]
  edge [
    source 1307
    target 1308
  ]
  edge [
    source 1309
    target 1310
  ]
  edge [
    source 1312
    target 1313
  ]
  edge [
    source 1315
    target 1316
  ]
  edge [
    source 1318
    target 1319
  ]
  edge [
    source 1321
    target 1322
  ]
  edge [
    source 1321
    target 1323
  ]
  edge [
    source 1321
    target 1324
  ]
  edge [
    source 1321
    target 1325
  ]
  edge [
    source 1322
    target 1323
  ]
  edge [
    source 1322
    target 1324
  ]
  edge [
    source 1322
    target 1325
  ]
  edge [
    source 1323
    target 1324
  ]
  edge [
    source 1323
    target 1325
  ]
  edge [
    source 1324
    target 1325
  ]
  edge [
    source 1326
    target 1327
  ]
  edge [
    source 1326
    target 1328
  ]
  edge [
    source 1326
    target 1329
  ]
  edge [
    source 1327
    target 1328
  ]
  edge [
    source 1327
    target 1329
  ]
  edge [
    source 1328
    target 1329
  ]
]
