graph [
  node [
    id 0
    label "ostatni"
    origin "text"
  ]
  node [
    id 1
    label "rozprawa"
    origin "text"
  ]
  node [
    id 2
    label "pow&#243;dztwo"
    origin "text"
  ]
  node [
    id 3
    label "cywilny"
    origin "text"
  ]
  node [
    id 4
    label "natalia"
    origin "text"
  ]
  node [
    id 5
    label "nitka"
    origin "text"
  ]
  node [
    id 6
    label "p&#322;a&#380;y&#324;skiej"
    origin "text"
  ]
  node [
    id 7
    label "przeciwko"
    origin "text"
  ]
  node [
    id 8
    label "niemiecki"
    origin "text"
  ]
  node [
    id 9
    label "biznesmen"
    origin "text"
  ]
  node [
    id 10
    label "hans"
    origin "text"
  ]
  node [
    id 11
    label "gram"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 14
    label "wobec"
    origin "text"
  ]
  node [
    id 15
    label "swoje"
    origin "text"
  ]
  node [
    id 16
    label "pracownik"
    origin "text"
  ]
  node [
    id 17
    label "u&#380;ywa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "sformu&#322;owanie"
    origin "text"
  ]
  node [
    id 19
    label "za&#347;"
    origin "text"
  ]
  node [
    id 20
    label "pow&#243;dka"
    origin "text"
  ]
  node [
    id 21
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 23
    label "gro&#378;ba"
    origin "text"
  ]
  node [
    id 24
    label "kolejny"
  ]
  node [
    id 25
    label "cz&#322;owiek"
  ]
  node [
    id 26
    label "niedawno"
  ]
  node [
    id 27
    label "poprzedni"
  ]
  node [
    id 28
    label "pozosta&#322;y"
  ]
  node [
    id 29
    label "ostatnio"
  ]
  node [
    id 30
    label "sko&#324;czony"
  ]
  node [
    id 31
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 32
    label "aktualny"
  ]
  node [
    id 33
    label "najgorszy"
  ]
  node [
    id 34
    label "istota_&#380;ywa"
  ]
  node [
    id 35
    label "w&#261;tpliwy"
  ]
  node [
    id 36
    label "nast&#281;pnie"
  ]
  node [
    id 37
    label "inny"
  ]
  node [
    id 38
    label "nastopny"
  ]
  node [
    id 39
    label "kolejno"
  ]
  node [
    id 40
    label "kt&#243;ry&#347;"
  ]
  node [
    id 41
    label "przesz&#322;y"
  ]
  node [
    id 42
    label "wcze&#347;niejszy"
  ]
  node [
    id 43
    label "poprzednio"
  ]
  node [
    id 44
    label "w&#261;tpliwie"
  ]
  node [
    id 45
    label "pozorny"
  ]
  node [
    id 46
    label "&#380;ywy"
  ]
  node [
    id 47
    label "ostateczny"
  ]
  node [
    id 48
    label "wa&#380;ny"
  ]
  node [
    id 49
    label "ludzko&#347;&#263;"
  ]
  node [
    id 50
    label "asymilowanie"
  ]
  node [
    id 51
    label "wapniak"
  ]
  node [
    id 52
    label "asymilowa&#263;"
  ]
  node [
    id 53
    label "os&#322;abia&#263;"
  ]
  node [
    id 54
    label "posta&#263;"
  ]
  node [
    id 55
    label "hominid"
  ]
  node [
    id 56
    label "podw&#322;adny"
  ]
  node [
    id 57
    label "os&#322;abianie"
  ]
  node [
    id 58
    label "g&#322;owa"
  ]
  node [
    id 59
    label "figura"
  ]
  node [
    id 60
    label "portrecista"
  ]
  node [
    id 61
    label "dwun&#243;g"
  ]
  node [
    id 62
    label "profanum"
  ]
  node [
    id 63
    label "mikrokosmos"
  ]
  node [
    id 64
    label "nasada"
  ]
  node [
    id 65
    label "duch"
  ]
  node [
    id 66
    label "antropochoria"
  ]
  node [
    id 67
    label "osoba"
  ]
  node [
    id 68
    label "wz&#243;r"
  ]
  node [
    id 69
    label "senior"
  ]
  node [
    id 70
    label "oddzia&#322;ywanie"
  ]
  node [
    id 71
    label "Adam"
  ]
  node [
    id 72
    label "homo_sapiens"
  ]
  node [
    id 73
    label "polifag"
  ]
  node [
    id 74
    label "wykszta&#322;cony"
  ]
  node [
    id 75
    label "dyplomowany"
  ]
  node [
    id 76
    label "wykwalifikowany"
  ]
  node [
    id 77
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 78
    label "kompletny"
  ]
  node [
    id 79
    label "sko&#324;czenie"
  ]
  node [
    id 80
    label "okre&#347;lony"
  ]
  node [
    id 81
    label "wielki"
  ]
  node [
    id 82
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 83
    label "aktualnie"
  ]
  node [
    id 84
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 85
    label "aktualizowanie"
  ]
  node [
    id 86
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 87
    label "uaktualnienie"
  ]
  node [
    id 88
    label "s&#261;d"
  ]
  node [
    id 89
    label "rozumowanie"
  ]
  node [
    id 90
    label "opracowanie"
  ]
  node [
    id 91
    label "proces"
  ]
  node [
    id 92
    label "obrady"
  ]
  node [
    id 93
    label "cytat"
  ]
  node [
    id 94
    label "tekst"
  ]
  node [
    id 95
    label "obja&#347;nienie"
  ]
  node [
    id 96
    label "s&#261;dzenie"
  ]
  node [
    id 97
    label "ekscerpcja"
  ]
  node [
    id 98
    label "j&#281;zykowo"
  ]
  node [
    id 99
    label "wypowied&#378;"
  ]
  node [
    id 100
    label "redakcja"
  ]
  node [
    id 101
    label "wytw&#243;r"
  ]
  node [
    id 102
    label "pomini&#281;cie"
  ]
  node [
    id 103
    label "dzie&#322;o"
  ]
  node [
    id 104
    label "preparacja"
  ]
  node [
    id 105
    label "odmianka"
  ]
  node [
    id 106
    label "opu&#347;ci&#263;"
  ]
  node [
    id 107
    label "koniektura"
  ]
  node [
    id 108
    label "pisa&#263;"
  ]
  node [
    id 109
    label "obelga"
  ]
  node [
    id 110
    label "dyskusja"
  ]
  node [
    id 111
    label "conference"
  ]
  node [
    id 112
    label "konsylium"
  ]
  node [
    id 113
    label "zesp&#243;&#322;"
  ]
  node [
    id 114
    label "podejrzany"
  ]
  node [
    id 115
    label "s&#261;downictwo"
  ]
  node [
    id 116
    label "system"
  ]
  node [
    id 117
    label "biuro"
  ]
  node [
    id 118
    label "court"
  ]
  node [
    id 119
    label "forum"
  ]
  node [
    id 120
    label "bronienie"
  ]
  node [
    id 121
    label "urz&#261;d"
  ]
  node [
    id 122
    label "wydarzenie"
  ]
  node [
    id 123
    label "oskar&#380;yciel"
  ]
  node [
    id 124
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 125
    label "skazany"
  ]
  node [
    id 126
    label "post&#281;powanie"
  ]
  node [
    id 127
    label "broni&#263;"
  ]
  node [
    id 128
    label "my&#347;l"
  ]
  node [
    id 129
    label "pods&#261;dny"
  ]
  node [
    id 130
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 131
    label "obrona"
  ]
  node [
    id 132
    label "instytucja"
  ]
  node [
    id 133
    label "antylogizm"
  ]
  node [
    id 134
    label "konektyw"
  ]
  node [
    id 135
    label "&#347;wiadek"
  ]
  node [
    id 136
    label "procesowicz"
  ]
  node [
    id 137
    label "strona"
  ]
  node [
    id 138
    label "alegacja"
  ]
  node [
    id 139
    label "wyimek"
  ]
  node [
    id 140
    label "konkordancja"
  ]
  node [
    id 141
    label "fragment"
  ]
  node [
    id 142
    label "ekscerptor"
  ]
  node [
    id 143
    label "proces_my&#347;lowy"
  ]
  node [
    id 144
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 145
    label "wnioskowanie"
  ]
  node [
    id 146
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 147
    label "robienie"
  ]
  node [
    id 148
    label "zinterpretowanie"
  ]
  node [
    id 149
    label "czynno&#347;&#263;"
  ]
  node [
    id 150
    label "judgment"
  ]
  node [
    id 151
    label "skupianie_si&#281;"
  ]
  node [
    id 152
    label "explanation"
  ]
  node [
    id 153
    label "remark"
  ]
  node [
    id 154
    label "report"
  ]
  node [
    id 155
    label "zrozumia&#322;y"
  ]
  node [
    id 156
    label "przedstawienie"
  ]
  node [
    id 157
    label "informacja"
  ]
  node [
    id 158
    label "poinformowanie"
  ]
  node [
    id 159
    label "przygotowanie"
  ]
  node [
    id 160
    label "paper"
  ]
  node [
    id 161
    label "kognicja"
  ]
  node [
    id 162
    label "przebieg"
  ]
  node [
    id 163
    label "legislacyjnie"
  ]
  node [
    id 164
    label "przes&#322;anka"
  ]
  node [
    id 165
    label "zjawisko"
  ]
  node [
    id 166
    label "nast&#281;pstwo"
  ]
  node [
    id 167
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 168
    label "powodowanie"
  ]
  node [
    id 169
    label "zas&#261;dzanie"
  ]
  node [
    id 170
    label "zarz&#261;dzanie"
  ]
  node [
    id 171
    label "traktowanie"
  ]
  node [
    id 172
    label "znajdowanie"
  ]
  node [
    id 173
    label "my&#347;lenie"
  ]
  node [
    id 174
    label "treatment"
  ]
  node [
    id 175
    label "dostawanie"
  ]
  node [
    id 176
    label "wyrokowanie"
  ]
  node [
    id 177
    label "skazanie"
  ]
  node [
    id 178
    label "orzekanie"
  ]
  node [
    id 179
    label "skazywanie"
  ]
  node [
    id 180
    label "appraisal"
  ]
  node [
    id 181
    label "orzekni&#281;cie"
  ]
  node [
    id 182
    label "zas&#261;dzenie"
  ]
  node [
    id 183
    label "zawyrokowanie"
  ]
  node [
    id 184
    label "s&#281;dziowanie"
  ]
  node [
    id 185
    label "wsp&#243;&#322;rz&#261;dzenie"
  ]
  node [
    id 186
    label "pozew"
  ]
  node [
    id 187
    label "wniosek"
  ]
  node [
    id 188
    label "pismo"
  ]
  node [
    id 189
    label "prayer"
  ]
  node [
    id 190
    label "twierdzenie"
  ]
  node [
    id 191
    label "propozycja"
  ]
  node [
    id 192
    label "motion"
  ]
  node [
    id 193
    label "nieoficjalny"
  ]
  node [
    id 194
    label "cywilnie"
  ]
  node [
    id 195
    label "nieoficjalnie"
  ]
  node [
    id 196
    label "nieformalny"
  ]
  node [
    id 197
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 198
    label "nawijad&#322;o"
  ]
  node [
    id 199
    label "sznur"
  ]
  node [
    id 200
    label "sie&#263;"
  ]
  node [
    id 201
    label "motowid&#322;o"
  ]
  node [
    id 202
    label "makaron"
  ]
  node [
    id 203
    label "pastafarianizm"
  ]
  node [
    id 204
    label "zjadacz"
  ]
  node [
    id 205
    label "wa&#322;ek"
  ]
  node [
    id 206
    label "p&#322;ywaczek"
  ]
  node [
    id 207
    label "jedzenie"
  ]
  node [
    id 208
    label "produkt"
  ]
  node [
    id 209
    label "kluski"
  ]
  node [
    id 210
    label "porcja"
  ]
  node [
    id 211
    label "rurka"
  ]
  node [
    id 212
    label "W&#322;och"
  ]
  node [
    id 213
    label "potrawa"
  ]
  node [
    id 214
    label "szpaler"
  ]
  node [
    id 215
    label "uporz&#261;dkowanie"
  ]
  node [
    id 216
    label "lina"
  ]
  node [
    id 217
    label "przew&#243;d"
  ]
  node [
    id 218
    label "glan"
  ]
  node [
    id 219
    label "tract"
  ]
  node [
    id 220
    label "prz&#281;dza"
  ]
  node [
    id 221
    label "przyrz&#261;d_mechaniczny"
  ]
  node [
    id 222
    label "ni&#263;"
  ]
  node [
    id 223
    label "kombajn"
  ]
  node [
    id 224
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 225
    label "snopowi&#261;za&#322;ka"
  ]
  node [
    id 226
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 227
    label "bobbin"
  ]
  node [
    id 228
    label "kszta&#322;t"
  ]
  node [
    id 229
    label "provider"
  ]
  node [
    id 230
    label "biznes_elektroniczny"
  ]
  node [
    id 231
    label "zasadzka"
  ]
  node [
    id 232
    label "mesh"
  ]
  node [
    id 233
    label "plecionka"
  ]
  node [
    id 234
    label "gauze"
  ]
  node [
    id 235
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 236
    label "struktura"
  ]
  node [
    id 237
    label "web"
  ]
  node [
    id 238
    label "organizacja"
  ]
  node [
    id 239
    label "gra_sieciowa"
  ]
  node [
    id 240
    label "net"
  ]
  node [
    id 241
    label "media"
  ]
  node [
    id 242
    label "sie&#263;_komputerowa"
  ]
  node [
    id 243
    label "snu&#263;"
  ]
  node [
    id 244
    label "vane"
  ]
  node [
    id 245
    label "instalacja"
  ]
  node [
    id 246
    label "wysnu&#263;"
  ]
  node [
    id 247
    label "organization"
  ]
  node [
    id 248
    label "obiekt"
  ]
  node [
    id 249
    label "us&#322;uga_internetowa"
  ]
  node [
    id 250
    label "rozmieszczenie"
  ]
  node [
    id 251
    label "podcast"
  ]
  node [
    id 252
    label "hipertekst"
  ]
  node [
    id 253
    label "cyberprzestrze&#324;"
  ]
  node [
    id 254
    label "mem"
  ]
  node [
    id 255
    label "grooming"
  ]
  node [
    id 256
    label "punkt_dost&#281;pu"
  ]
  node [
    id 257
    label "netbook"
  ]
  node [
    id 258
    label "e-hazard"
  ]
  node [
    id 259
    label "po_niemiecku"
  ]
  node [
    id 260
    label "German"
  ]
  node [
    id 261
    label "niemiecko"
  ]
  node [
    id 262
    label "cenar"
  ]
  node [
    id 263
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 264
    label "europejski"
  ]
  node [
    id 265
    label "strudel"
  ]
  node [
    id 266
    label "niemiec"
  ]
  node [
    id 267
    label "pionier"
  ]
  node [
    id 268
    label "zachodnioeuropejski"
  ]
  node [
    id 269
    label "j&#281;zyk"
  ]
  node [
    id 270
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 271
    label "junkers"
  ]
  node [
    id 272
    label "szwabski"
  ]
  node [
    id 273
    label "szwabsko"
  ]
  node [
    id 274
    label "j&#281;zyk_niemiecki"
  ]
  node [
    id 275
    label "po_szwabsku"
  ]
  node [
    id 276
    label "platt"
  ]
  node [
    id 277
    label "europejsko"
  ]
  node [
    id 278
    label "&#380;o&#322;nierz"
  ]
  node [
    id 279
    label "saper"
  ]
  node [
    id 280
    label "prekursor"
  ]
  node [
    id 281
    label "osadnik"
  ]
  node [
    id 282
    label "skaut"
  ]
  node [
    id 283
    label "g&#322;osiciel"
  ]
  node [
    id 284
    label "ciasto"
  ]
  node [
    id 285
    label "taniec_ludowy"
  ]
  node [
    id 286
    label "melodia"
  ]
  node [
    id 287
    label "taniec"
  ]
  node [
    id 288
    label "podgrzewacz"
  ]
  node [
    id 289
    label "samolot_wojskowy"
  ]
  node [
    id 290
    label "moreska"
  ]
  node [
    id 291
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 292
    label "zachodni"
  ]
  node [
    id 293
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 294
    label "langosz"
  ]
  node [
    id 295
    label "po_europejsku"
  ]
  node [
    id 296
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 297
    label "European"
  ]
  node [
    id 298
    label "typowy"
  ]
  node [
    id 299
    label "charakterystyczny"
  ]
  node [
    id 300
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 301
    label "artykulator"
  ]
  node [
    id 302
    label "kod"
  ]
  node [
    id 303
    label "kawa&#322;ek"
  ]
  node [
    id 304
    label "przedmiot"
  ]
  node [
    id 305
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 306
    label "gramatyka"
  ]
  node [
    id 307
    label "stylik"
  ]
  node [
    id 308
    label "przet&#322;umaczenie"
  ]
  node [
    id 309
    label "formalizowanie"
  ]
  node [
    id 310
    label "ssa&#263;"
  ]
  node [
    id 311
    label "ssanie"
  ]
  node [
    id 312
    label "language"
  ]
  node [
    id 313
    label "liza&#263;"
  ]
  node [
    id 314
    label "napisa&#263;"
  ]
  node [
    id 315
    label "konsonantyzm"
  ]
  node [
    id 316
    label "wokalizm"
  ]
  node [
    id 317
    label "fonetyka"
  ]
  node [
    id 318
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 319
    label "jeniec"
  ]
  node [
    id 320
    label "but"
  ]
  node [
    id 321
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 322
    label "po_koroniarsku"
  ]
  node [
    id 323
    label "kultura_duchowa"
  ]
  node [
    id 324
    label "t&#322;umaczenie"
  ]
  node [
    id 325
    label "m&#243;wienie"
  ]
  node [
    id 326
    label "pype&#263;"
  ]
  node [
    id 327
    label "lizanie"
  ]
  node [
    id 328
    label "formalizowa&#263;"
  ]
  node [
    id 329
    label "rozumie&#263;"
  ]
  node [
    id 330
    label "organ"
  ]
  node [
    id 331
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 332
    label "rozumienie"
  ]
  node [
    id 333
    label "spos&#243;b"
  ]
  node [
    id 334
    label "makroglosja"
  ]
  node [
    id 335
    label "m&#243;wi&#263;"
  ]
  node [
    id 336
    label "jama_ustna"
  ]
  node [
    id 337
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 338
    label "formacja_geologiczna"
  ]
  node [
    id 339
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 340
    label "natural_language"
  ]
  node [
    id 341
    label "s&#322;ownictwo"
  ]
  node [
    id 342
    label "urz&#261;dzenie"
  ]
  node [
    id 343
    label "przedsi&#281;biorca"
  ]
  node [
    id 344
    label "wydawca"
  ]
  node [
    id 345
    label "wsp&#243;lnik"
  ]
  node [
    id 346
    label "kapitalista"
  ]
  node [
    id 347
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 348
    label "klasa_&#347;rednia"
  ]
  node [
    id 349
    label "osoba_fizyczna"
  ]
  node [
    id 350
    label "centygram"
  ]
  node [
    id 351
    label "megagram"
  ]
  node [
    id 352
    label "metryczna_jednostka_masy"
  ]
  node [
    id 353
    label "miligram"
  ]
  node [
    id 354
    label "dekagram"
  ]
  node [
    id 355
    label "decygram"
  ]
  node [
    id 356
    label "mikrogram"
  ]
  node [
    id 357
    label "hektogram"
  ]
  node [
    id 358
    label "kilogram"
  ]
  node [
    id 359
    label "proszek"
  ]
  node [
    id 360
    label "tablet"
  ]
  node [
    id 361
    label "dawka"
  ]
  node [
    id 362
    label "blister"
  ]
  node [
    id 363
    label "lekarstwo"
  ]
  node [
    id 364
    label "cecha"
  ]
  node [
    id 365
    label "salariat"
  ]
  node [
    id 366
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 367
    label "delegowanie"
  ]
  node [
    id 368
    label "pracu&#347;"
  ]
  node [
    id 369
    label "r&#281;ka"
  ]
  node [
    id 370
    label "delegowa&#263;"
  ]
  node [
    id 371
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 372
    label "warstwa"
  ]
  node [
    id 373
    label "p&#322;aca"
  ]
  node [
    id 374
    label "krzy&#380;"
  ]
  node [
    id 375
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 376
    label "handwriting"
  ]
  node [
    id 377
    label "d&#322;o&#324;"
  ]
  node [
    id 378
    label "gestykulowa&#263;"
  ]
  node [
    id 379
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 380
    label "palec"
  ]
  node [
    id 381
    label "przedrami&#281;"
  ]
  node [
    id 382
    label "hand"
  ]
  node [
    id 383
    label "&#322;okie&#263;"
  ]
  node [
    id 384
    label "hazena"
  ]
  node [
    id 385
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 386
    label "bramkarz"
  ]
  node [
    id 387
    label "nadgarstek"
  ]
  node [
    id 388
    label "graba"
  ]
  node [
    id 389
    label "r&#261;czyna"
  ]
  node [
    id 390
    label "k&#322;&#261;b"
  ]
  node [
    id 391
    label "pi&#322;ka"
  ]
  node [
    id 392
    label "chwyta&#263;"
  ]
  node [
    id 393
    label "cmoknonsens"
  ]
  node [
    id 394
    label "pomocnik"
  ]
  node [
    id 395
    label "gestykulowanie"
  ]
  node [
    id 396
    label "chwytanie"
  ]
  node [
    id 397
    label "obietnica"
  ]
  node [
    id 398
    label "zagrywka"
  ]
  node [
    id 399
    label "kroki"
  ]
  node [
    id 400
    label "hasta"
  ]
  node [
    id 401
    label "wykroczenie"
  ]
  node [
    id 402
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 403
    label "czerwona_kartka"
  ]
  node [
    id 404
    label "paw"
  ]
  node [
    id 405
    label "rami&#281;"
  ]
  node [
    id 406
    label "wysy&#322;a&#263;"
  ]
  node [
    id 407
    label "air"
  ]
  node [
    id 408
    label "wys&#322;a&#263;"
  ]
  node [
    id 409
    label "oddelegowa&#263;"
  ]
  node [
    id 410
    label "oddelegowywa&#263;"
  ]
  node [
    id 411
    label "zapaleniec"
  ]
  node [
    id 412
    label "wysy&#322;anie"
  ]
  node [
    id 413
    label "wys&#322;anie"
  ]
  node [
    id 414
    label "delegacy"
  ]
  node [
    id 415
    label "oddelegowywanie"
  ]
  node [
    id 416
    label "oddelegowanie"
  ]
  node [
    id 417
    label "korzysta&#263;"
  ]
  node [
    id 418
    label "distribute"
  ]
  node [
    id 419
    label "give"
  ]
  node [
    id 420
    label "bash"
  ]
  node [
    id 421
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 422
    label "doznawa&#263;"
  ]
  node [
    id 423
    label "use"
  ]
  node [
    id 424
    label "uzyskiwa&#263;"
  ]
  node [
    id 425
    label "mutant"
  ]
  node [
    id 426
    label "doznanie"
  ]
  node [
    id 427
    label "dobrostan"
  ]
  node [
    id 428
    label "u&#380;ycie"
  ]
  node [
    id 429
    label "u&#380;y&#263;"
  ]
  node [
    id 430
    label "bawienie"
  ]
  node [
    id 431
    label "lubo&#347;&#263;"
  ]
  node [
    id 432
    label "prze&#380;ycie"
  ]
  node [
    id 433
    label "u&#380;ywanie"
  ]
  node [
    id 434
    label "hurt"
  ]
  node [
    id 435
    label "wording"
  ]
  node [
    id 436
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 437
    label "statement"
  ]
  node [
    id 438
    label "zapisanie"
  ]
  node [
    id 439
    label "rzucenie"
  ]
  node [
    id 440
    label "pos&#322;uchanie"
  ]
  node [
    id 441
    label "sparafrazowanie"
  ]
  node [
    id 442
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 443
    label "strawestowa&#263;"
  ]
  node [
    id 444
    label "sparafrazowa&#263;"
  ]
  node [
    id 445
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 446
    label "trawestowa&#263;"
  ]
  node [
    id 447
    label "parafrazowanie"
  ]
  node [
    id 448
    label "ozdobnik"
  ]
  node [
    id 449
    label "delimitacja"
  ]
  node [
    id 450
    label "parafrazowa&#263;"
  ]
  node [
    id 451
    label "stylizacja"
  ]
  node [
    id 452
    label "komunikat"
  ]
  node [
    id 453
    label "trawestowanie"
  ]
  node [
    id 454
    label "strawestowanie"
  ]
  node [
    id 455
    label "rezultat"
  ]
  node [
    id 456
    label "konwulsja"
  ]
  node [
    id 457
    label "ruszenie"
  ]
  node [
    id 458
    label "pierdolni&#281;cie"
  ]
  node [
    id 459
    label "poruszenie"
  ]
  node [
    id 460
    label "opuszczenie"
  ]
  node [
    id 461
    label "most"
  ]
  node [
    id 462
    label "wywo&#322;anie"
  ]
  node [
    id 463
    label "odej&#347;cie"
  ]
  node [
    id 464
    label "przewr&#243;cenie"
  ]
  node [
    id 465
    label "wyzwanie"
  ]
  node [
    id 466
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 467
    label "skonstruowanie"
  ]
  node [
    id 468
    label "spowodowanie"
  ]
  node [
    id 469
    label "grzmotni&#281;cie"
  ]
  node [
    id 470
    label "zdecydowanie"
  ]
  node [
    id 471
    label "przeznaczenie"
  ]
  node [
    id 472
    label "&#347;wiat&#322;o"
  ]
  node [
    id 473
    label "przemieszczenie"
  ]
  node [
    id 474
    label "wyposa&#380;enie"
  ]
  node [
    id 475
    label "podejrzenie"
  ]
  node [
    id 476
    label "czar"
  ]
  node [
    id 477
    label "shy"
  ]
  node [
    id 478
    label "oddzia&#322;anie"
  ]
  node [
    id 479
    label "cie&#324;"
  ]
  node [
    id 480
    label "zrezygnowanie"
  ]
  node [
    id 481
    label "porzucenie"
  ]
  node [
    id 482
    label "atak"
  ]
  node [
    id 483
    label "powiedzenie"
  ]
  node [
    id 484
    label "towar"
  ]
  node [
    id 485
    label "rzucanie"
  ]
  node [
    id 486
    label "przekazanie"
  ]
  node [
    id 487
    label "arrangement"
  ]
  node [
    id 488
    label "rozpisanie"
  ]
  node [
    id 489
    label "preservation"
  ]
  node [
    id 490
    label "wype&#322;nienie"
  ]
  node [
    id 491
    label "utrwalenie"
  ]
  node [
    id 492
    label "record"
  ]
  node [
    id 493
    label "w&#322;&#261;czenie"
  ]
  node [
    id 494
    label "zalecenie"
  ]
  node [
    id 495
    label "telling"
  ]
  node [
    id 496
    label "zrobienie"
  ]
  node [
    id 497
    label "threat"
  ]
  node [
    id 498
    label "zawisa&#263;"
  ]
  node [
    id 499
    label "zagrozi&#263;"
  ]
  node [
    id 500
    label "postrach"
  ]
  node [
    id 501
    label "pogroza"
  ]
  node [
    id 502
    label "zawisanie"
  ]
  node [
    id 503
    label "czarny_punkt"
  ]
  node [
    id 504
    label "charakterystyka"
  ]
  node [
    id 505
    label "m&#322;ot"
  ]
  node [
    id 506
    label "znak"
  ]
  node [
    id 507
    label "drzewo"
  ]
  node [
    id 508
    label "pr&#243;ba"
  ]
  node [
    id 509
    label "attribute"
  ]
  node [
    id 510
    label "marka"
  ]
  node [
    id 511
    label "niebezpiecze&#324;stwo"
  ]
  node [
    id 512
    label "zaszachowa&#263;"
  ]
  node [
    id 513
    label "threaten"
  ]
  node [
    id 514
    label "szachowa&#263;"
  ]
  node [
    id 515
    label "zaistnie&#263;"
  ]
  node [
    id 516
    label "uprzedzi&#263;"
  ]
  node [
    id 517
    label "menace"
  ]
  node [
    id 518
    label "pojawianie_si&#281;"
  ]
  node [
    id 519
    label "umieranie"
  ]
  node [
    id 520
    label "nieruchomienie"
  ]
  node [
    id 521
    label "dzianie_si&#281;"
  ]
  node [
    id 522
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 523
    label "mie&#263;_miejsce"
  ]
  node [
    id 524
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 525
    label "nieruchomie&#263;"
  ]
  node [
    id 526
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 527
    label "ko&#324;czy&#263;"
  ]
  node [
    id 528
    label "przyczyna"
  ]
  node [
    id 529
    label "przera&#380;enie"
  ]
  node [
    id 530
    label "reakcja"
  ]
  node [
    id 531
    label "Natalia"
  ]
  node [
    id 532
    label "P&#322;a&#380;y&#324;skiej"
  ]
  node [
    id 533
    label "Hans"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 9
    target 346
  ]
  edge [
    source 9
    target 347
  ]
  edge [
    source 9
    target 348
  ]
  edge [
    source 9
    target 349
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 366
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 49
  ]
  edge [
    source 16
    target 50
  ]
  edge [
    source 16
    target 51
  ]
  edge [
    source 16
    target 52
  ]
  edge [
    source 16
    target 53
  ]
  edge [
    source 16
    target 54
  ]
  edge [
    source 16
    target 55
  ]
  edge [
    source 16
    target 56
  ]
  edge [
    source 16
    target 57
  ]
  edge [
    source 16
    target 58
  ]
  edge [
    source 16
    target 59
  ]
  edge [
    source 16
    target 60
  ]
  edge [
    source 16
    target 61
  ]
  edge [
    source 16
    target 62
  ]
  edge [
    source 16
    target 63
  ]
  edge [
    source 16
    target 64
  ]
  edge [
    source 16
    target 65
  ]
  edge [
    source 16
    target 66
  ]
  edge [
    source 16
    target 67
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 16
    target 71
  ]
  edge [
    source 16
    target 72
  ]
  edge [
    source 16
    target 73
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 394
  ]
  edge [
    source 16
    target 395
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 397
  ]
  edge [
    source 16
    target 333
  ]
  edge [
    source 16
    target 398
  ]
  edge [
    source 16
    target 399
  ]
  edge [
    source 16
    target 400
  ]
  edge [
    source 16
    target 401
  ]
  edge [
    source 16
    target 402
  ]
  edge [
    source 16
    target 403
  ]
  edge [
    source 16
    target 404
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 406
  ]
  edge [
    source 16
    target 407
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 412
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 417
  ]
  edge [
    source 17
    target 418
  ]
  edge [
    source 17
    target 419
  ]
  edge [
    source 17
    target 420
  ]
  edge [
    source 17
    target 421
  ]
  edge [
    source 17
    target 422
  ]
  edge [
    source 17
    target 423
  ]
  edge [
    source 17
    target 424
  ]
  edge [
    source 17
    target 425
  ]
  edge [
    source 17
    target 426
  ]
  edge [
    source 17
    target 427
  ]
  edge [
    source 17
    target 428
  ]
  edge [
    source 17
    target 429
  ]
  edge [
    source 17
    target 430
  ]
  edge [
    source 17
    target 431
  ]
  edge [
    source 17
    target 432
  ]
  edge [
    source 17
    target 433
  ]
  edge [
    source 17
    target 434
  ]
  edge [
    source 17
    target 21
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 435
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 436
  ]
  edge [
    source 18
    target 437
  ]
  edge [
    source 18
    target 438
  ]
  edge [
    source 18
    target 439
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 440
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 441
  ]
  edge [
    source 18
    target 442
  ]
  edge [
    source 18
    target 443
  ]
  edge [
    source 18
    target 444
  ]
  edge [
    source 18
    target 445
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 18
    target 447
  ]
  edge [
    source 18
    target 448
  ]
  edge [
    source 18
    target 449
  ]
  edge [
    source 18
    target 450
  ]
  edge [
    source 18
    target 451
  ]
  edge [
    source 18
    target 452
  ]
  edge [
    source 18
    target 453
  ]
  edge [
    source 18
    target 454
  ]
  edge [
    source 18
    target 455
  ]
  edge [
    source 18
    target 456
  ]
  edge [
    source 18
    target 457
  ]
  edge [
    source 18
    target 458
  ]
  edge [
    source 18
    target 459
  ]
  edge [
    source 18
    target 460
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 472
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 474
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 490
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 363
  ]
  edge [
    source 18
    target 492
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 417
  ]
  edge [
    source 21
    target 418
  ]
  edge [
    source 21
    target 419
  ]
  edge [
    source 21
    target 420
  ]
  edge [
    source 21
    target 421
  ]
  edge [
    source 21
    target 422
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 497
  ]
  edge [
    source 23
    target 498
  ]
  edge [
    source 23
    target 99
  ]
  edge [
    source 23
    target 364
  ]
  edge [
    source 23
    target 499
  ]
  edge [
    source 23
    target 500
  ]
  edge [
    source 23
    target 501
  ]
  edge [
    source 23
    target 502
  ]
  edge [
    source 23
    target 503
  ]
  edge [
    source 23
    target 504
  ]
  edge [
    source 23
    target 505
  ]
  edge [
    source 23
    target 506
  ]
  edge [
    source 23
    target 507
  ]
  edge [
    source 23
    target 508
  ]
  edge [
    source 23
    target 509
  ]
  edge [
    source 23
    target 510
  ]
  edge [
    source 23
    target 440
  ]
  edge [
    source 23
    target 88
  ]
  edge [
    source 23
    target 441
  ]
  edge [
    source 23
    target 442
  ]
  edge [
    source 23
    target 443
  ]
  edge [
    source 23
    target 444
  ]
  edge [
    source 23
    target 445
  ]
  edge [
    source 23
    target 446
  ]
  edge [
    source 23
    target 447
  ]
  edge [
    source 23
    target 448
  ]
  edge [
    source 23
    target 449
  ]
  edge [
    source 23
    target 450
  ]
  edge [
    source 23
    target 451
  ]
  edge [
    source 23
    target 452
  ]
  edge [
    source 23
    target 453
  ]
  edge [
    source 23
    target 454
  ]
  edge [
    source 23
    target 455
  ]
  edge [
    source 23
    target 511
  ]
  edge [
    source 23
    target 512
  ]
  edge [
    source 23
    target 513
  ]
  edge [
    source 23
    target 514
  ]
  edge [
    source 23
    target 515
  ]
  edge [
    source 23
    target 516
  ]
  edge [
    source 23
    target 517
  ]
  edge [
    source 23
    target 518
  ]
  edge [
    source 23
    target 519
  ]
  edge [
    source 23
    target 520
  ]
  edge [
    source 23
    target 521
  ]
  edge [
    source 23
    target 522
  ]
  edge [
    source 23
    target 523
  ]
  edge [
    source 23
    target 524
  ]
  edge [
    source 23
    target 525
  ]
  edge [
    source 23
    target 526
  ]
  edge [
    source 23
    target 527
  ]
  edge [
    source 23
    target 528
  ]
  edge [
    source 23
    target 529
  ]
  edge [
    source 23
    target 530
  ]
  edge [
    source 531
    target 532
  ]
]
