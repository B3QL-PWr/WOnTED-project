graph [
  node [
    id 0
    label "taormina"
    origin "text"
  ]
  node [
    id 1
    label "wroc&#322;aw"
    origin "text"
  ]
  node [
    id 2
    label "poleca&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zamawia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "pizza"
    origin "text"
  ]
  node [
    id 5
    label "tam"
    origin "text"
  ]
  node [
    id 6
    label "ordynowa&#263;"
  ]
  node [
    id 7
    label "doradza&#263;"
  ]
  node [
    id 8
    label "wydawa&#263;"
  ]
  node [
    id 9
    label "m&#243;wi&#263;"
  ]
  node [
    id 10
    label "control"
  ]
  node [
    id 11
    label "charge"
  ]
  node [
    id 12
    label "placard"
  ]
  node [
    id 13
    label "powierza&#263;"
  ]
  node [
    id 14
    label "zadawa&#263;"
  ]
  node [
    id 15
    label "robi&#263;"
  ]
  node [
    id 16
    label "mie&#263;_miejsce"
  ]
  node [
    id 17
    label "plon"
  ]
  node [
    id 18
    label "give"
  ]
  node [
    id 19
    label "surrender"
  ]
  node [
    id 20
    label "kojarzy&#263;"
  ]
  node [
    id 21
    label "d&#378;wi&#281;k"
  ]
  node [
    id 22
    label "impart"
  ]
  node [
    id 23
    label "dawa&#263;"
  ]
  node [
    id 24
    label "reszta"
  ]
  node [
    id 25
    label "zapach"
  ]
  node [
    id 26
    label "wydawnictwo"
  ]
  node [
    id 27
    label "wiano"
  ]
  node [
    id 28
    label "produkcja"
  ]
  node [
    id 29
    label "wprowadza&#263;"
  ]
  node [
    id 30
    label "podawa&#263;"
  ]
  node [
    id 31
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 32
    label "ujawnia&#263;"
  ]
  node [
    id 33
    label "denuncjowa&#263;"
  ]
  node [
    id 34
    label "tajemnica"
  ]
  node [
    id 35
    label "panna_na_wydaniu"
  ]
  node [
    id 36
    label "wytwarza&#263;"
  ]
  node [
    id 37
    label "train"
  ]
  node [
    id 38
    label "gaworzy&#263;"
  ]
  node [
    id 39
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 40
    label "remark"
  ]
  node [
    id 41
    label "rozmawia&#263;"
  ]
  node [
    id 42
    label "wyra&#380;a&#263;"
  ]
  node [
    id 43
    label "umie&#263;"
  ]
  node [
    id 44
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 45
    label "dziama&#263;"
  ]
  node [
    id 46
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 47
    label "formu&#322;owa&#263;"
  ]
  node [
    id 48
    label "dysfonia"
  ]
  node [
    id 49
    label "express"
  ]
  node [
    id 50
    label "talk"
  ]
  node [
    id 51
    label "u&#380;ywa&#263;"
  ]
  node [
    id 52
    label "prawi&#263;"
  ]
  node [
    id 53
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 54
    label "powiada&#263;"
  ]
  node [
    id 55
    label "tell"
  ]
  node [
    id 56
    label "chew_the_fat"
  ]
  node [
    id 57
    label "say"
  ]
  node [
    id 58
    label "j&#281;zyk"
  ]
  node [
    id 59
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 60
    label "informowa&#263;"
  ]
  node [
    id 61
    label "wydobywa&#263;"
  ]
  node [
    id 62
    label "okre&#347;la&#263;"
  ]
  node [
    id 63
    label "deal"
  ]
  node [
    id 64
    label "zajmowa&#263;"
  ]
  node [
    id 65
    label "karmi&#263;"
  ]
  node [
    id 66
    label "szkodzi&#263;"
  ]
  node [
    id 67
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 68
    label "inflict"
  ]
  node [
    id 69
    label "share"
  ]
  node [
    id 70
    label "d&#378;wiga&#263;"
  ]
  node [
    id 71
    label "pose"
  ]
  node [
    id 72
    label "zak&#322;ada&#263;"
  ]
  node [
    id 73
    label "wyznawa&#263;"
  ]
  node [
    id 74
    label "oddawa&#263;"
  ]
  node [
    id 75
    label "confide"
  ]
  node [
    id 76
    label "zleca&#263;"
  ]
  node [
    id 77
    label "ufa&#263;"
  ]
  node [
    id 78
    label "command"
  ]
  node [
    id 79
    label "grant"
  ]
  node [
    id 80
    label "rede"
  ]
  node [
    id 81
    label "radzi&#263;"
  ]
  node [
    id 82
    label "wyprawia&#263;"
  ]
  node [
    id 83
    label "kierowa&#263;"
  ]
  node [
    id 84
    label "save"
  ]
  node [
    id 85
    label "zaleca&#263;"
  ]
  node [
    id 86
    label "order"
  ]
  node [
    id 87
    label "klawisz"
  ]
  node [
    id 88
    label "zam&#243;wi&#263;"
  ]
  node [
    id 89
    label "rezerwowa&#263;"
  ]
  node [
    id 90
    label "indenture"
  ]
  node [
    id 91
    label "zam&#243;wienie"
  ]
  node [
    id 92
    label "umawia&#263;_si&#281;"
  ]
  node [
    id 93
    label "zamawianie"
  ]
  node [
    id 94
    label "zg&#322;asza&#263;"
  ]
  node [
    id 95
    label "bespeak"
  ]
  node [
    id 96
    label "czarowa&#263;"
  ]
  node [
    id 97
    label "szeptucha"
  ]
  node [
    id 98
    label "condition"
  ]
  node [
    id 99
    label "wymawia&#263;"
  ]
  node [
    id 100
    label "zapewnia&#263;"
  ]
  node [
    id 101
    label "prosecute"
  ]
  node [
    id 102
    label "zachowywa&#263;"
  ]
  node [
    id 103
    label "stalowa&#263;"
  ]
  node [
    id 104
    label "report"
  ]
  node [
    id 105
    label "write"
  ]
  node [
    id 106
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 107
    label "enthuse"
  ]
  node [
    id 108
    label "wzbudza&#263;"
  ]
  node [
    id 109
    label "odprawia&#263;"
  ]
  node [
    id 110
    label "delight"
  ]
  node [
    id 111
    label "usi&#322;owa&#263;"
  ]
  node [
    id 112
    label "odznaka"
  ]
  node [
    id 113
    label "kawaler"
  ]
  node [
    id 114
    label "zaczarowanie"
  ]
  node [
    id 115
    label "indent"
  ]
  node [
    id 116
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 117
    label "zlecenie"
  ]
  node [
    id 118
    label "portfel_zam&#243;wie&#324;"
  ]
  node [
    id 119
    label "polecenie"
  ]
  node [
    id 120
    label "rozdysponowanie"
  ]
  node [
    id 121
    label "perpetration"
  ]
  node [
    id 122
    label "transakcja"
  ]
  node [
    id 123
    label "zg&#322;oszenie"
  ]
  node [
    id 124
    label "zarezerwowanie"
  ]
  node [
    id 125
    label "engagement"
  ]
  node [
    id 126
    label "umawianie_si&#281;"
  ]
  node [
    id 127
    label "czarowanie"
  ]
  node [
    id 128
    label "zlecanie"
  ]
  node [
    id 129
    label "zg&#322;aszanie"
  ]
  node [
    id 130
    label "szeptun"
  ]
  node [
    id 131
    label "polecanie"
  ]
  node [
    id 132
    label "rezerwowanie"
  ]
  node [
    id 133
    label "poleci&#263;"
  ]
  node [
    id 134
    label "zaczarowa&#263;"
  ]
  node [
    id 135
    label "zg&#322;osi&#263;"
  ]
  node [
    id 136
    label "appoint"
  ]
  node [
    id 137
    label "zarezerwowa&#263;"
  ]
  node [
    id 138
    label "zleci&#263;"
  ]
  node [
    id 139
    label "um&#243;wi&#263;_si&#281;"
  ]
  node [
    id 140
    label "staruszka"
  ]
  node [
    id 141
    label "zamawiaczka"
  ]
  node [
    id 142
    label "wypiek"
  ]
  node [
    id 143
    label "sp&#243;d"
  ]
  node [
    id 144
    label "fast_food"
  ]
  node [
    id 145
    label "baking"
  ]
  node [
    id 146
    label "upiek"
  ]
  node [
    id 147
    label "jedzenie"
  ]
  node [
    id 148
    label "produkt"
  ]
  node [
    id 149
    label "pieczenie"
  ]
  node [
    id 150
    label "placek"
  ]
  node [
    id 151
    label "bielizna"
  ]
  node [
    id 152
    label "strona"
  ]
  node [
    id 153
    label "d&#243;&#322;"
  ]
  node [
    id 154
    label "tu"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 5
    target 154
  ]
]
