graph [
  node [
    id 0
    label "kupa"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "normalna"
    origin "text"
  ]
  node [
    id 3
    label "krok"
    origin "text"
  ]
  node [
    id 4
    label "zdrowy"
    origin "text"
  ]
  node [
    id 5
    label "wypr&#243;&#380;nia&#263;"
    origin "text"
  ]
  node [
    id 6
    label "kszta&#322;t"
  ]
  node [
    id 7
    label "tragedia"
  ]
  node [
    id 8
    label "wydalina"
  ]
  node [
    id 9
    label "stool"
  ]
  node [
    id 10
    label "koprofilia"
  ]
  node [
    id 11
    label "odchody"
  ]
  node [
    id 12
    label "mn&#243;stwo"
  ]
  node [
    id 13
    label "knoll"
  ]
  node [
    id 14
    label "balas"
  ]
  node [
    id 15
    label "g&#243;wno"
  ]
  node [
    id 16
    label "fekalia"
  ]
  node [
    id 17
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 18
    label "play"
  ]
  node [
    id 19
    label "dramat"
  ]
  node [
    id 20
    label "cios"
  ]
  node [
    id 21
    label "gatunek_literacki"
  ]
  node [
    id 22
    label "lipa"
  ]
  node [
    id 23
    label "formacja"
  ]
  node [
    id 24
    label "punkt_widzenia"
  ]
  node [
    id 25
    label "wygl&#261;d"
  ]
  node [
    id 26
    label "g&#322;owa"
  ]
  node [
    id 27
    label "spirala"
  ]
  node [
    id 28
    label "p&#322;at"
  ]
  node [
    id 29
    label "comeliness"
  ]
  node [
    id 30
    label "kielich"
  ]
  node [
    id 31
    label "face"
  ]
  node [
    id 32
    label "blaszka"
  ]
  node [
    id 33
    label "charakter"
  ]
  node [
    id 34
    label "p&#281;tla"
  ]
  node [
    id 35
    label "obiekt"
  ]
  node [
    id 36
    label "pasmo"
  ]
  node [
    id 37
    label "cecha"
  ]
  node [
    id 38
    label "linearno&#347;&#263;"
  ]
  node [
    id 39
    label "gwiazda"
  ]
  node [
    id 40
    label "miniatura"
  ]
  node [
    id 41
    label "integer"
  ]
  node [
    id 42
    label "liczba"
  ]
  node [
    id 43
    label "zlewanie_si&#281;"
  ]
  node [
    id 44
    label "ilo&#347;&#263;"
  ]
  node [
    id 45
    label "uk&#322;ad"
  ]
  node [
    id 46
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 47
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 48
    label "pe&#322;ny"
  ]
  node [
    id 49
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 50
    label "enormousness"
  ]
  node [
    id 51
    label "body_waste"
  ]
  node [
    id 52
    label "produkt"
  ]
  node [
    id 53
    label "nieczysto&#347;ci"
  ]
  node [
    id 54
    label "koprofag"
  ]
  node [
    id 55
    label "ka&#322;"
  ]
  node [
    id 56
    label "balustrada"
  ]
  node [
    id 57
    label "podpora"
  ]
  node [
    id 58
    label "element"
  ]
  node [
    id 59
    label "zdobienie"
  ]
  node [
    id 60
    label "tandeta"
  ]
  node [
    id 61
    label "zero"
  ]
  node [
    id 62
    label "drobiazg"
  ]
  node [
    id 63
    label "mocz"
  ]
  node [
    id 64
    label "nieczysto&#347;ci_p&#322;ynne"
  ]
  node [
    id 65
    label "fetyszyzm"
  ]
  node [
    id 66
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 67
    label "mie&#263;_miejsce"
  ]
  node [
    id 68
    label "equal"
  ]
  node [
    id 69
    label "trwa&#263;"
  ]
  node [
    id 70
    label "chodzi&#263;"
  ]
  node [
    id 71
    label "si&#281;ga&#263;"
  ]
  node [
    id 72
    label "stan"
  ]
  node [
    id 73
    label "obecno&#347;&#263;"
  ]
  node [
    id 74
    label "stand"
  ]
  node [
    id 75
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 76
    label "uczestniczy&#263;"
  ]
  node [
    id 77
    label "participate"
  ]
  node [
    id 78
    label "robi&#263;"
  ]
  node [
    id 79
    label "istnie&#263;"
  ]
  node [
    id 80
    label "pozostawa&#263;"
  ]
  node [
    id 81
    label "zostawa&#263;"
  ]
  node [
    id 82
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 83
    label "adhere"
  ]
  node [
    id 84
    label "compass"
  ]
  node [
    id 85
    label "korzysta&#263;"
  ]
  node [
    id 86
    label "appreciation"
  ]
  node [
    id 87
    label "osi&#261;ga&#263;"
  ]
  node [
    id 88
    label "dociera&#263;"
  ]
  node [
    id 89
    label "get"
  ]
  node [
    id 90
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 91
    label "mierzy&#263;"
  ]
  node [
    id 92
    label "u&#380;ywa&#263;"
  ]
  node [
    id 93
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 94
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 95
    label "exsert"
  ]
  node [
    id 96
    label "being"
  ]
  node [
    id 97
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 98
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 99
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 100
    label "p&#322;ywa&#263;"
  ]
  node [
    id 101
    label "run"
  ]
  node [
    id 102
    label "bangla&#263;"
  ]
  node [
    id 103
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 104
    label "przebiega&#263;"
  ]
  node [
    id 105
    label "wk&#322;ada&#263;"
  ]
  node [
    id 106
    label "proceed"
  ]
  node [
    id 107
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 108
    label "carry"
  ]
  node [
    id 109
    label "bywa&#263;"
  ]
  node [
    id 110
    label "dziama&#263;"
  ]
  node [
    id 111
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 112
    label "stara&#263;_si&#281;"
  ]
  node [
    id 113
    label "para"
  ]
  node [
    id 114
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 115
    label "str&#243;j"
  ]
  node [
    id 116
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 117
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 118
    label "tryb"
  ]
  node [
    id 119
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 120
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 121
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 122
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 123
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 124
    label "continue"
  ]
  node [
    id 125
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 126
    label "Ohio"
  ]
  node [
    id 127
    label "wci&#281;cie"
  ]
  node [
    id 128
    label "Nowy_York"
  ]
  node [
    id 129
    label "warstwa"
  ]
  node [
    id 130
    label "samopoczucie"
  ]
  node [
    id 131
    label "Illinois"
  ]
  node [
    id 132
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 133
    label "state"
  ]
  node [
    id 134
    label "Jukatan"
  ]
  node [
    id 135
    label "Kalifornia"
  ]
  node [
    id 136
    label "Wirginia"
  ]
  node [
    id 137
    label "wektor"
  ]
  node [
    id 138
    label "Goa"
  ]
  node [
    id 139
    label "Teksas"
  ]
  node [
    id 140
    label "Waszyngton"
  ]
  node [
    id 141
    label "miejsce"
  ]
  node [
    id 142
    label "Massachusetts"
  ]
  node [
    id 143
    label "Alaska"
  ]
  node [
    id 144
    label "Arakan"
  ]
  node [
    id 145
    label "Hawaje"
  ]
  node [
    id 146
    label "Maryland"
  ]
  node [
    id 147
    label "punkt"
  ]
  node [
    id 148
    label "Michigan"
  ]
  node [
    id 149
    label "Arizona"
  ]
  node [
    id 150
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 151
    label "Georgia"
  ]
  node [
    id 152
    label "poziom"
  ]
  node [
    id 153
    label "Pensylwania"
  ]
  node [
    id 154
    label "shape"
  ]
  node [
    id 155
    label "Luizjana"
  ]
  node [
    id 156
    label "Nowy_Meksyk"
  ]
  node [
    id 157
    label "Alabama"
  ]
  node [
    id 158
    label "Kansas"
  ]
  node [
    id 159
    label "Oregon"
  ]
  node [
    id 160
    label "Oklahoma"
  ]
  node [
    id 161
    label "Floryda"
  ]
  node [
    id 162
    label "jednostka_administracyjna"
  ]
  node [
    id 163
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 164
    label "prosta"
  ]
  node [
    id 165
    label "krzywa"
  ]
  node [
    id 166
    label "odcinek"
  ]
  node [
    id 167
    label "straight_line"
  ]
  node [
    id 168
    label "czas"
  ]
  node [
    id 169
    label "trasa"
  ]
  node [
    id 170
    label "proste_sko&#347;ne"
  ]
  node [
    id 171
    label "step"
  ]
  node [
    id 172
    label "tu&#322;&#243;w"
  ]
  node [
    id 173
    label "measurement"
  ]
  node [
    id 174
    label "action"
  ]
  node [
    id 175
    label "czyn"
  ]
  node [
    id 176
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 177
    label "ruch"
  ]
  node [
    id 178
    label "passus"
  ]
  node [
    id 179
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 180
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 181
    label "skejt"
  ]
  node [
    id 182
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 183
    label "pace"
  ]
  node [
    id 184
    label "mechanika"
  ]
  node [
    id 185
    label "utrzymywanie"
  ]
  node [
    id 186
    label "move"
  ]
  node [
    id 187
    label "poruszenie"
  ]
  node [
    id 188
    label "movement"
  ]
  node [
    id 189
    label "myk"
  ]
  node [
    id 190
    label "utrzyma&#263;"
  ]
  node [
    id 191
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 192
    label "zjawisko"
  ]
  node [
    id 193
    label "utrzymanie"
  ]
  node [
    id 194
    label "travel"
  ]
  node [
    id 195
    label "kanciasty"
  ]
  node [
    id 196
    label "commercial_enterprise"
  ]
  node [
    id 197
    label "model"
  ]
  node [
    id 198
    label "strumie&#324;"
  ]
  node [
    id 199
    label "proces"
  ]
  node [
    id 200
    label "aktywno&#347;&#263;"
  ]
  node [
    id 201
    label "kr&#243;tki"
  ]
  node [
    id 202
    label "taktyka"
  ]
  node [
    id 203
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 204
    label "apraksja"
  ]
  node [
    id 205
    label "natural_process"
  ]
  node [
    id 206
    label "utrzymywa&#263;"
  ]
  node [
    id 207
    label "d&#322;ugi"
  ]
  node [
    id 208
    label "wydarzenie"
  ]
  node [
    id 209
    label "dyssypacja_energii"
  ]
  node [
    id 210
    label "tumult"
  ]
  node [
    id 211
    label "stopek"
  ]
  node [
    id 212
    label "czynno&#347;&#263;"
  ]
  node [
    id 213
    label "zmiana"
  ]
  node [
    id 214
    label "manewr"
  ]
  node [
    id 215
    label "lokomocja"
  ]
  node [
    id 216
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 217
    label "komunikacja"
  ]
  node [
    id 218
    label "drift"
  ]
  node [
    id 219
    label "ton"
  ]
  node [
    id 220
    label "rozmiar"
  ]
  node [
    id 221
    label "ambitus"
  ]
  node [
    id 222
    label "skala"
  ]
  node [
    id 223
    label "funkcja"
  ]
  node [
    id 224
    label "act"
  ]
  node [
    id 225
    label "Rzym_Zachodni"
  ]
  node [
    id 226
    label "whole"
  ]
  node [
    id 227
    label "Rzym_Wschodni"
  ]
  node [
    id 228
    label "urz&#261;dzenie"
  ]
  node [
    id 229
    label "uzyskanie"
  ]
  node [
    id 230
    label "dochrapanie_si&#281;"
  ]
  node [
    id 231
    label "skill"
  ]
  node [
    id 232
    label "accomplishment"
  ]
  node [
    id 233
    label "zdarzenie_si&#281;"
  ]
  node [
    id 234
    label "sukces"
  ]
  node [
    id 235
    label "zaawansowanie"
  ]
  node [
    id 236
    label "dotarcie"
  ]
  node [
    id 237
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 238
    label "biom"
  ]
  node [
    id 239
    label "teren"
  ]
  node [
    id 240
    label "r&#243;wnina"
  ]
  node [
    id 241
    label "formacja_ro&#347;linna"
  ]
  node [
    id 242
    label "taniec"
  ]
  node [
    id 243
    label "trawa"
  ]
  node [
    id 244
    label "Abakan"
  ]
  node [
    id 245
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 246
    label "Wielki_Step"
  ]
  node [
    id 247
    label "krok_taneczny"
  ]
  node [
    id 248
    label "breakdance"
  ]
  node [
    id 249
    label "cz&#322;owiek"
  ]
  node [
    id 250
    label "orygina&#322;"
  ]
  node [
    id 251
    label "przedstawiciel"
  ]
  node [
    id 252
    label "passage"
  ]
  node [
    id 253
    label "dwukrok"
  ]
  node [
    id 254
    label "tekst"
  ]
  node [
    id 255
    label "urywek"
  ]
  node [
    id 256
    label "krocze"
  ]
  node [
    id 257
    label "klatka_piersiowa"
  ]
  node [
    id 258
    label "biodro"
  ]
  node [
    id 259
    label "pier&#347;"
  ]
  node [
    id 260
    label "pachwina"
  ]
  node [
    id 261
    label "pacha"
  ]
  node [
    id 262
    label "brzuch"
  ]
  node [
    id 263
    label "struktura_anatomiczna"
  ]
  node [
    id 264
    label "bok"
  ]
  node [
    id 265
    label "body"
  ]
  node [
    id 266
    label "pupa"
  ]
  node [
    id 267
    label "plecy"
  ]
  node [
    id 268
    label "stawon&#243;g"
  ]
  node [
    id 269
    label "dekolt"
  ]
  node [
    id 270
    label "zad"
  ]
  node [
    id 271
    label "zdrowo"
  ]
  node [
    id 272
    label "wyzdrowienie"
  ]
  node [
    id 273
    label "wyleczenie_si&#281;"
  ]
  node [
    id 274
    label "uzdrowienie"
  ]
  node [
    id 275
    label "silny"
  ]
  node [
    id 276
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 277
    label "normalny"
  ]
  node [
    id 278
    label "rozs&#261;dny"
  ]
  node [
    id 279
    label "korzystny"
  ]
  node [
    id 280
    label "zdrowienie"
  ]
  node [
    id 281
    label "solidny"
  ]
  node [
    id 282
    label "uzdrawianie"
  ]
  node [
    id 283
    label "dobry"
  ]
  node [
    id 284
    label "spokojny"
  ]
  node [
    id 285
    label "przemy&#347;lany"
  ]
  node [
    id 286
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 287
    label "rozs&#261;dnie"
  ]
  node [
    id 288
    label "rozumny"
  ]
  node [
    id 289
    label "m&#261;dry"
  ]
  node [
    id 290
    label "dobroczynny"
  ]
  node [
    id 291
    label "czw&#243;rka"
  ]
  node [
    id 292
    label "skuteczny"
  ]
  node [
    id 293
    label "&#347;mieszny"
  ]
  node [
    id 294
    label "mi&#322;y"
  ]
  node [
    id 295
    label "grzeczny"
  ]
  node [
    id 296
    label "powitanie"
  ]
  node [
    id 297
    label "dobrze"
  ]
  node [
    id 298
    label "ca&#322;y"
  ]
  node [
    id 299
    label "zwrot"
  ]
  node [
    id 300
    label "pomy&#347;lny"
  ]
  node [
    id 301
    label "moralny"
  ]
  node [
    id 302
    label "drogi"
  ]
  node [
    id 303
    label "pozytywny"
  ]
  node [
    id 304
    label "odpowiedni"
  ]
  node [
    id 305
    label "pos&#322;uszny"
  ]
  node [
    id 306
    label "ludzko&#347;&#263;"
  ]
  node [
    id 307
    label "asymilowanie"
  ]
  node [
    id 308
    label "wapniak"
  ]
  node [
    id 309
    label "asymilowa&#263;"
  ]
  node [
    id 310
    label "os&#322;abia&#263;"
  ]
  node [
    id 311
    label "posta&#263;"
  ]
  node [
    id 312
    label "hominid"
  ]
  node [
    id 313
    label "podw&#322;adny"
  ]
  node [
    id 314
    label "os&#322;abianie"
  ]
  node [
    id 315
    label "figura"
  ]
  node [
    id 316
    label "portrecista"
  ]
  node [
    id 317
    label "dwun&#243;g"
  ]
  node [
    id 318
    label "profanum"
  ]
  node [
    id 319
    label "mikrokosmos"
  ]
  node [
    id 320
    label "nasada"
  ]
  node [
    id 321
    label "duch"
  ]
  node [
    id 322
    label "antropochoria"
  ]
  node [
    id 323
    label "osoba"
  ]
  node [
    id 324
    label "wz&#243;r"
  ]
  node [
    id 325
    label "senior"
  ]
  node [
    id 326
    label "oddzia&#322;ywanie"
  ]
  node [
    id 327
    label "Adam"
  ]
  node [
    id 328
    label "homo_sapiens"
  ]
  node [
    id 329
    label "polifag"
  ]
  node [
    id 330
    label "solidnie"
  ]
  node [
    id 331
    label "porz&#261;dny"
  ]
  node [
    id 332
    label "niez&#322;y"
  ]
  node [
    id 333
    label "&#322;adny"
  ]
  node [
    id 334
    label "obowi&#261;zkowy"
  ]
  node [
    id 335
    label "rzetelny"
  ]
  node [
    id 336
    label "korzystnie"
  ]
  node [
    id 337
    label "cz&#281;sty"
  ]
  node [
    id 338
    label "przeci&#281;tny"
  ]
  node [
    id 339
    label "oczywisty"
  ]
  node [
    id 340
    label "zdr&#243;w"
  ]
  node [
    id 341
    label "zwykle"
  ]
  node [
    id 342
    label "zwyczajny"
  ]
  node [
    id 343
    label "zwyczajnie"
  ]
  node [
    id 344
    label "prawid&#322;owy"
  ]
  node [
    id 345
    label "normalnie"
  ]
  node [
    id 346
    label "okre&#347;lony"
  ]
  node [
    id 347
    label "odpowiednio"
  ]
  node [
    id 348
    label "sprawnie"
  ]
  node [
    id 349
    label "wylizywanie_si&#281;"
  ]
  node [
    id 350
    label "stawanie_si&#281;"
  ]
  node [
    id 351
    label "przywracanie"
  ]
  node [
    id 352
    label "powodowanie"
  ]
  node [
    id 353
    label "ulepszanie"
  ]
  node [
    id 354
    label "wylizanie_si&#281;"
  ]
  node [
    id 355
    label "stanie_si&#281;"
  ]
  node [
    id 356
    label "rehabilitation"
  ]
  node [
    id 357
    label "poprawa"
  ]
  node [
    id 358
    label "sanitation"
  ]
  node [
    id 359
    label "spowodowanie"
  ]
  node [
    id 360
    label "przywr&#243;cenie"
  ]
  node [
    id 361
    label "ulepszenie"
  ]
  node [
    id 362
    label "intensywny"
  ]
  node [
    id 363
    label "krzepienie"
  ]
  node [
    id 364
    label "&#380;ywotny"
  ]
  node [
    id 365
    label "mocny"
  ]
  node [
    id 366
    label "pokrzepienie"
  ]
  node [
    id 367
    label "zdecydowany"
  ]
  node [
    id 368
    label "niepodwa&#380;alny"
  ]
  node [
    id 369
    label "du&#380;y"
  ]
  node [
    id 370
    label "mocno"
  ]
  node [
    id 371
    label "przekonuj&#261;cy"
  ]
  node [
    id 372
    label "wytrzyma&#322;y"
  ]
  node [
    id 373
    label "konkretny"
  ]
  node [
    id 374
    label "silnie"
  ]
  node [
    id 375
    label "meflochina"
  ]
  node [
    id 376
    label "zajebisty"
  ]
  node [
    id 377
    label "wyjmowa&#263;"
  ]
  node [
    id 378
    label "zu&#380;ywa&#263;"
  ]
  node [
    id 379
    label "authorize"
  ]
  node [
    id 380
    label "wyklucza&#263;"
  ]
  node [
    id 381
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 382
    label "przemieszcza&#263;"
  ]
  node [
    id 383
    label "produce"
  ]
  node [
    id 384
    label "expand"
  ]
  node [
    id 385
    label "use"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
]
