graph [
  node [
    id 0
    label "silny"
    origin "text"
  ]
  node [
    id 1
    label "podmorski"
    origin "text"
  ]
  node [
    id 2
    label "trz&#281;sienie"
    origin "text"
  ]
  node [
    id 3
    label "ziemia"
    origin "text"
  ]
  node [
    id 4
    label "magnituda"
    origin "text"
  ]
  node [
    id 5
    label "nawiedzi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "obszar"
    origin "text"
  ]
  node [
    id 7
    label "wschodni"
    origin "text"
  ]
  node [
    id 8
    label "wybrze&#380;e"
    origin "text"
  ]
  node [
    id 9
    label "nowa"
    origin "text"
  ]
  node [
    id 10
    label "kaledonia"
    origin "text"
  ]
  node [
    id 11
    label "po&#322;udniowy"
    origin "text"
  ]
  node [
    id 12
    label "pacyfik"
    origin "text"
  ]
  node [
    id 13
    label "poinformowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "centrum"
    origin "text"
  ]
  node [
    id 15
    label "ostrzega&#263;"
    origin "text"
  ]
  node [
    id 16
    label "przed"
    origin "text"
  ]
  node [
    id 17
    label "tsunami"
    origin "text"
  ]
  node [
    id 18
    label "ptwc"
    origin "text"
  ]
  node [
    id 19
    label "intensywny"
  ]
  node [
    id 20
    label "krzepienie"
  ]
  node [
    id 21
    label "&#380;ywotny"
  ]
  node [
    id 22
    label "mocny"
  ]
  node [
    id 23
    label "pokrzepienie"
  ]
  node [
    id 24
    label "zdecydowany"
  ]
  node [
    id 25
    label "niepodwa&#380;alny"
  ]
  node [
    id 26
    label "du&#380;y"
  ]
  node [
    id 27
    label "mocno"
  ]
  node [
    id 28
    label "przekonuj&#261;cy"
  ]
  node [
    id 29
    label "wytrzyma&#322;y"
  ]
  node [
    id 30
    label "konkretny"
  ]
  node [
    id 31
    label "zdrowy"
  ]
  node [
    id 32
    label "silnie"
  ]
  node [
    id 33
    label "meflochina"
  ]
  node [
    id 34
    label "zajebisty"
  ]
  node [
    id 35
    label "przekonuj&#261;co"
  ]
  node [
    id 36
    label "niema&#322;o"
  ]
  node [
    id 37
    label "powerfully"
  ]
  node [
    id 38
    label "widocznie"
  ]
  node [
    id 39
    label "szczerze"
  ]
  node [
    id 40
    label "konkretnie"
  ]
  node [
    id 41
    label "niepodwa&#380;alnie"
  ]
  node [
    id 42
    label "stabilnie"
  ]
  node [
    id 43
    label "zdecydowanie"
  ]
  node [
    id 44
    label "strongly"
  ]
  node [
    id 45
    label "comfort"
  ]
  node [
    id 46
    label "refresher_course"
  ]
  node [
    id 47
    label "wzmocnienie"
  ]
  node [
    id 48
    label "pocieszenie"
  ]
  node [
    id 49
    label "ukojenie"
  ]
  node [
    id 50
    label "wzmacnianie"
  ]
  node [
    id 51
    label "pocieszanie"
  ]
  node [
    id 52
    label "boost"
  ]
  node [
    id 53
    label "zdrowo"
  ]
  node [
    id 54
    label "wyzdrowienie"
  ]
  node [
    id 55
    label "cz&#322;owiek"
  ]
  node [
    id 56
    label "uzdrowienie"
  ]
  node [
    id 57
    label "wyleczenie_si&#281;"
  ]
  node [
    id 58
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 59
    label "normalny"
  ]
  node [
    id 60
    label "rozs&#261;dny"
  ]
  node [
    id 61
    label "korzystny"
  ]
  node [
    id 62
    label "zdrowienie"
  ]
  node [
    id 63
    label "solidny"
  ]
  node [
    id 64
    label "uzdrawianie"
  ]
  node [
    id 65
    label "dobry"
  ]
  node [
    id 66
    label "szczery"
  ]
  node [
    id 67
    label "stabilny"
  ]
  node [
    id 68
    label "trudny"
  ]
  node [
    id 69
    label "krzepki"
  ]
  node [
    id 70
    label "wyrazisty"
  ]
  node [
    id 71
    label "widoczny"
  ]
  node [
    id 72
    label "wzmocni&#263;"
  ]
  node [
    id 73
    label "wzmacnia&#263;"
  ]
  node [
    id 74
    label "intensywnie"
  ]
  node [
    id 75
    label "zajebi&#347;cie"
  ]
  node [
    id 76
    label "dusznie"
  ]
  node [
    id 77
    label "doustny"
  ]
  node [
    id 78
    label "antymalaryczny"
  ]
  node [
    id 79
    label "antymalaryk"
  ]
  node [
    id 80
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 81
    label "uodparnianie_si&#281;"
  ]
  node [
    id 82
    label "utwardzanie"
  ]
  node [
    id 83
    label "wytrzymale"
  ]
  node [
    id 84
    label "uodpornienie_si&#281;"
  ]
  node [
    id 85
    label "uodparnianie"
  ]
  node [
    id 86
    label "hartowny"
  ]
  node [
    id 87
    label "twardnienie"
  ]
  node [
    id 88
    label "odporny"
  ]
  node [
    id 89
    label "zahartowanie"
  ]
  node [
    id 90
    label "uodpornienie"
  ]
  node [
    id 91
    label "biologicznie"
  ]
  node [
    id 92
    label "&#380;ywotnie"
  ]
  node [
    id 93
    label "aktualny"
  ]
  node [
    id 94
    label "wa&#380;ny"
  ]
  node [
    id 95
    label "pe&#322;ny"
  ]
  node [
    id 96
    label "pewny"
  ]
  node [
    id 97
    label "zauwa&#380;alny"
  ]
  node [
    id 98
    label "gotowy"
  ]
  node [
    id 99
    label "skuteczny"
  ]
  node [
    id 100
    label "po&#380;ywny"
  ]
  node [
    id 101
    label "solidnie"
  ]
  node [
    id 102
    label "niez&#322;y"
  ]
  node [
    id 103
    label "ogarni&#281;ty"
  ]
  node [
    id 104
    label "jaki&#347;"
  ]
  node [
    id 105
    label "posilny"
  ]
  node [
    id 106
    label "&#322;adny"
  ]
  node [
    id 107
    label "tre&#347;ciwy"
  ]
  node [
    id 108
    label "abstrakcyjny"
  ]
  node [
    id 109
    label "okre&#347;lony"
  ]
  node [
    id 110
    label "skupiony"
  ]
  node [
    id 111
    label "jasny"
  ]
  node [
    id 112
    label "doros&#322;y"
  ]
  node [
    id 113
    label "znaczny"
  ]
  node [
    id 114
    label "wiele"
  ]
  node [
    id 115
    label "rozwini&#281;ty"
  ]
  node [
    id 116
    label "dorodny"
  ]
  node [
    id 117
    label "prawdziwy"
  ]
  node [
    id 118
    label "du&#380;o"
  ]
  node [
    id 119
    label "szybki"
  ]
  node [
    id 120
    label "znacz&#261;cy"
  ]
  node [
    id 121
    label "zwarty"
  ]
  node [
    id 122
    label "efektywny"
  ]
  node [
    id 123
    label "ogrodnictwo"
  ]
  node [
    id 124
    label "dynamiczny"
  ]
  node [
    id 125
    label "nieproporcjonalny"
  ]
  node [
    id 126
    label "specjalny"
  ]
  node [
    id 127
    label "wspania&#322;y"
  ]
  node [
    id 128
    label "zadzier&#380;ysty"
  ]
  node [
    id 129
    label "podwodny"
  ]
  node [
    id 130
    label "jolt"
  ]
  node [
    id 131
    label "wytrz&#261;&#347;ni&#281;cie"
  ]
  node [
    id 132
    label "ruszanie"
  ]
  node [
    id 133
    label "agitation"
  ]
  node [
    id 134
    label "rz&#261;dzenie"
  ]
  node [
    id 135
    label "poruszanie"
  ]
  node [
    id 136
    label "roztrz&#261;sanie"
  ]
  node [
    id 137
    label "spin"
  ]
  node [
    id 138
    label "roztrzepywanie"
  ]
  node [
    id 139
    label "sk&#322;&#243;cenie"
  ]
  node [
    id 140
    label "podporz&#261;dkowywanie"
  ]
  node [
    id 141
    label "roztrz&#261;&#347;ni&#281;cie"
  ]
  node [
    id 142
    label "wytrz&#261;sanie"
  ]
  node [
    id 143
    label "powodowanie"
  ]
  node [
    id 144
    label "gesture"
  ]
  node [
    id 145
    label "animowanie"
  ]
  node [
    id 146
    label "dzianie_si&#281;"
  ]
  node [
    id 147
    label "robienie"
  ]
  node [
    id 148
    label "movement"
  ]
  node [
    id 149
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 150
    label "zaanimowanie"
  ]
  node [
    id 151
    label "podnoszenie"
  ]
  node [
    id 152
    label "poruszanie_si&#281;"
  ]
  node [
    id 153
    label "porobienie"
  ]
  node [
    id 154
    label "wzbudzanie"
  ]
  node [
    id 155
    label "czynno&#347;&#263;"
  ]
  node [
    id 156
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 157
    label "motion"
  ]
  node [
    id 158
    label "raise"
  ]
  node [
    id 159
    label "zabieranie"
  ]
  node [
    id 160
    label "zaczynanie"
  ]
  node [
    id 161
    label "misdemeanor"
  ]
  node [
    id 162
    label "sprawowanie"
  ]
  node [
    id 163
    label "dominion"
  ]
  node [
    id 164
    label "w&#322;adca"
  ]
  node [
    id 165
    label "dominowanie"
  ]
  node [
    id 166
    label "reign"
  ]
  node [
    id 167
    label "rule"
  ]
  node [
    id 168
    label "w&#322;adza"
  ]
  node [
    id 169
    label "moment_p&#281;du"
  ]
  node [
    id 170
    label "zdarzenie_si&#281;"
  ]
  node [
    id 171
    label "opr&#243;&#380;nienie"
  ]
  node [
    id 172
    label "wyrzucenie"
  ]
  node [
    id 173
    label "spowodowanie"
  ]
  node [
    id 174
    label "rozrzucenie"
  ]
  node [
    id 175
    label "canvas"
  ]
  node [
    id 176
    label "rozpatrywanie"
  ]
  node [
    id 177
    label "discussion"
  ]
  node [
    id 178
    label "rozrzucanie"
  ]
  node [
    id 179
    label "opr&#243;&#380;nianie"
  ]
  node [
    id 180
    label "wyrzucanie"
  ]
  node [
    id 181
    label "quarrel"
  ]
  node [
    id 182
    label "powa&#347;nienie"
  ]
  node [
    id 183
    label "sk&#322;&#243;cony"
  ]
  node [
    id 184
    label "zmieszanie"
  ]
  node [
    id 185
    label "mieszanie"
  ]
  node [
    id 186
    label "subordination"
  ]
  node [
    id 187
    label "podporz&#261;dkowywanie_si&#281;"
  ]
  node [
    id 188
    label "uzale&#380;nianie"
  ]
  node [
    id 189
    label "prowadzenie_na_pasku"
  ]
  node [
    id 190
    label "wchodzenie_na_&#322;eb"
  ]
  node [
    id 191
    label "je&#380;d&#380;enie_po_g&#322;owie"
  ]
  node [
    id 192
    label "dyrygowanie"
  ]
  node [
    id 193
    label "owijanie_wok&#243;&#322;_palca"
  ]
  node [
    id 194
    label "dopasowywanie"
  ]
  node [
    id 195
    label "Mazowsze"
  ]
  node [
    id 196
    label "Anglia"
  ]
  node [
    id 197
    label "Amazonia"
  ]
  node [
    id 198
    label "Bordeaux"
  ]
  node [
    id 199
    label "Naddniestrze"
  ]
  node [
    id 200
    label "plantowa&#263;"
  ]
  node [
    id 201
    label "Europa_Zachodnia"
  ]
  node [
    id 202
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 203
    label "Armagnac"
  ]
  node [
    id 204
    label "zapadnia"
  ]
  node [
    id 205
    label "Zamojszczyzna"
  ]
  node [
    id 206
    label "Amhara"
  ]
  node [
    id 207
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 208
    label "budynek"
  ]
  node [
    id 209
    label "skorupa_ziemska"
  ]
  node [
    id 210
    label "Ma&#322;opolska"
  ]
  node [
    id 211
    label "Turkiestan"
  ]
  node [
    id 212
    label "Noworosja"
  ]
  node [
    id 213
    label "Mezoameryka"
  ]
  node [
    id 214
    label "glinowanie"
  ]
  node [
    id 215
    label "Lubelszczyzna"
  ]
  node [
    id 216
    label "Ba&#322;kany"
  ]
  node [
    id 217
    label "Kurdystan"
  ]
  node [
    id 218
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 219
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 220
    label "martwica"
  ]
  node [
    id 221
    label "Baszkiria"
  ]
  node [
    id 222
    label "Szkocja"
  ]
  node [
    id 223
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 224
    label "Tonkin"
  ]
  node [
    id 225
    label "Maghreb"
  ]
  node [
    id 226
    label "teren"
  ]
  node [
    id 227
    label "litosfera"
  ]
  node [
    id 228
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 229
    label "penetrator"
  ]
  node [
    id 230
    label "Nadrenia"
  ]
  node [
    id 231
    label "glinowa&#263;"
  ]
  node [
    id 232
    label "Wielkopolska"
  ]
  node [
    id 233
    label "Zabajkale"
  ]
  node [
    id 234
    label "Apulia"
  ]
  node [
    id 235
    label "domain"
  ]
  node [
    id 236
    label "Bojkowszczyzna"
  ]
  node [
    id 237
    label "podglebie"
  ]
  node [
    id 238
    label "kompleks_sorpcyjny"
  ]
  node [
    id 239
    label "Liguria"
  ]
  node [
    id 240
    label "Pamir"
  ]
  node [
    id 241
    label "Indochiny"
  ]
  node [
    id 242
    label "miejsce"
  ]
  node [
    id 243
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 244
    label "Polinezja"
  ]
  node [
    id 245
    label "Kurpie"
  ]
  node [
    id 246
    label "Podlasie"
  ]
  node [
    id 247
    label "S&#261;decczyzna"
  ]
  node [
    id 248
    label "Umbria"
  ]
  node [
    id 249
    label "Karaiby"
  ]
  node [
    id 250
    label "Ukraina_Zachodnia"
  ]
  node [
    id 251
    label "Kielecczyzna"
  ]
  node [
    id 252
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 253
    label "kort"
  ]
  node [
    id 254
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 255
    label "czynnik_produkcji"
  ]
  node [
    id 256
    label "Skandynawia"
  ]
  node [
    id 257
    label "Kujawy"
  ]
  node [
    id 258
    label "Tyrol"
  ]
  node [
    id 259
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 260
    label "Huculszczyzna"
  ]
  node [
    id 261
    label "pojazd"
  ]
  node [
    id 262
    label "Turyngia"
  ]
  node [
    id 263
    label "powierzchnia"
  ]
  node [
    id 264
    label "jednostka_administracyjna"
  ]
  node [
    id 265
    label "Podhale"
  ]
  node [
    id 266
    label "Toskania"
  ]
  node [
    id 267
    label "Bory_Tucholskie"
  ]
  node [
    id 268
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 269
    label "Kalabria"
  ]
  node [
    id 270
    label "pr&#243;chnica"
  ]
  node [
    id 271
    label "Hercegowina"
  ]
  node [
    id 272
    label "Lotaryngia"
  ]
  node [
    id 273
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 274
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 275
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 276
    label "Walia"
  ]
  node [
    id 277
    label "pomieszczenie"
  ]
  node [
    id 278
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 279
    label "Opolskie"
  ]
  node [
    id 280
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 281
    label "Kampania"
  ]
  node [
    id 282
    label "Sand&#380;ak"
  ]
  node [
    id 283
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 284
    label "Syjon"
  ]
  node [
    id 285
    label "Kabylia"
  ]
  node [
    id 286
    label "ryzosfera"
  ]
  node [
    id 287
    label "Lombardia"
  ]
  node [
    id 288
    label "Warmia"
  ]
  node [
    id 289
    label "Kaszmir"
  ]
  node [
    id 290
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 291
    label "&#321;&#243;dzkie"
  ]
  node [
    id 292
    label "Kaukaz"
  ]
  node [
    id 293
    label "Europa_Wschodnia"
  ]
  node [
    id 294
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 295
    label "Biskupizna"
  ]
  node [
    id 296
    label "Afryka_Wschodnia"
  ]
  node [
    id 297
    label "Podkarpacie"
  ]
  node [
    id 298
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 299
    label "Afryka_Zachodnia"
  ]
  node [
    id 300
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 301
    label "Bo&#347;nia"
  ]
  node [
    id 302
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 303
    label "p&#322;aszczyzna"
  ]
  node [
    id 304
    label "dotleni&#263;"
  ]
  node [
    id 305
    label "Oceania"
  ]
  node [
    id 306
    label "Pomorze_Zachodnie"
  ]
  node [
    id 307
    label "Powi&#347;le"
  ]
  node [
    id 308
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 309
    label "Opolszczyzna"
  ]
  node [
    id 310
    label "&#321;emkowszczyzna"
  ]
  node [
    id 311
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 312
    label "Podbeskidzie"
  ]
  node [
    id 313
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 314
    label "Kaszuby"
  ]
  node [
    id 315
    label "Ko&#322;yma"
  ]
  node [
    id 316
    label "Szlezwik"
  ]
  node [
    id 317
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 318
    label "glej"
  ]
  node [
    id 319
    label "Mikronezja"
  ]
  node [
    id 320
    label "pa&#324;stwo"
  ]
  node [
    id 321
    label "posadzka"
  ]
  node [
    id 322
    label "Polesie"
  ]
  node [
    id 323
    label "Kerala"
  ]
  node [
    id 324
    label "Mazury"
  ]
  node [
    id 325
    label "Palestyna"
  ]
  node [
    id 326
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 327
    label "Lauda"
  ]
  node [
    id 328
    label "Azja_Wschodnia"
  ]
  node [
    id 329
    label "Galicja"
  ]
  node [
    id 330
    label "Zakarpacie"
  ]
  node [
    id 331
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 332
    label "Lubuskie"
  ]
  node [
    id 333
    label "Laponia"
  ]
  node [
    id 334
    label "Yorkshire"
  ]
  node [
    id 335
    label "Bawaria"
  ]
  node [
    id 336
    label "Zag&#243;rze"
  ]
  node [
    id 337
    label "geosystem"
  ]
  node [
    id 338
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 339
    label "Andaluzja"
  ]
  node [
    id 340
    label "&#379;ywiecczyzna"
  ]
  node [
    id 341
    label "przestrze&#324;"
  ]
  node [
    id 342
    label "Oksytania"
  ]
  node [
    id 343
    label "Kociewie"
  ]
  node [
    id 344
    label "Lasko"
  ]
  node [
    id 345
    label "warunek_lokalowy"
  ]
  node [
    id 346
    label "plac"
  ]
  node [
    id 347
    label "location"
  ]
  node [
    id 348
    label "uwaga"
  ]
  node [
    id 349
    label "status"
  ]
  node [
    id 350
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 351
    label "chwila"
  ]
  node [
    id 352
    label "cia&#322;o"
  ]
  node [
    id 353
    label "cecha"
  ]
  node [
    id 354
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 355
    label "praca"
  ]
  node [
    id 356
    label "rz&#261;d"
  ]
  node [
    id 357
    label "tkanina_we&#322;niana"
  ]
  node [
    id 358
    label "boisko"
  ]
  node [
    id 359
    label "siatka"
  ]
  node [
    id 360
    label "ubrani&#243;wka"
  ]
  node [
    id 361
    label "p&#243;&#322;noc"
  ]
  node [
    id 362
    label "Kosowo"
  ]
  node [
    id 363
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 364
    label "Zab&#322;ocie"
  ]
  node [
    id 365
    label "zach&#243;d"
  ]
  node [
    id 366
    label "po&#322;udnie"
  ]
  node [
    id 367
    label "Pow&#261;zki"
  ]
  node [
    id 368
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 369
    label "Piotrowo"
  ]
  node [
    id 370
    label "Olszanica"
  ]
  node [
    id 371
    label "zbi&#243;r"
  ]
  node [
    id 372
    label "Ruda_Pabianicka"
  ]
  node [
    id 373
    label "holarktyka"
  ]
  node [
    id 374
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 375
    label "Ludwin&#243;w"
  ]
  node [
    id 376
    label "Arktyka"
  ]
  node [
    id 377
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 378
    label "Zabu&#380;e"
  ]
  node [
    id 379
    label "antroposfera"
  ]
  node [
    id 380
    label "Neogea"
  ]
  node [
    id 381
    label "terytorium"
  ]
  node [
    id 382
    label "Syberia_Zachodnia"
  ]
  node [
    id 383
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 384
    label "zakres"
  ]
  node [
    id 385
    label "pas_planetoid"
  ]
  node [
    id 386
    label "Syberia_Wschodnia"
  ]
  node [
    id 387
    label "Antarktyka"
  ]
  node [
    id 388
    label "Rakowice"
  ]
  node [
    id 389
    label "akrecja"
  ]
  node [
    id 390
    label "wymiar"
  ]
  node [
    id 391
    label "&#321;&#281;g"
  ]
  node [
    id 392
    label "Kresy_Zachodnie"
  ]
  node [
    id 393
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 394
    label "wsch&#243;d"
  ]
  node [
    id 395
    label "Notogea"
  ]
  node [
    id 396
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 397
    label "mienie"
  ]
  node [
    id 398
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 399
    label "stan"
  ]
  node [
    id 400
    label "rzecz"
  ]
  node [
    id 401
    label "immoblizacja"
  ]
  node [
    id 402
    label "&#347;ciana"
  ]
  node [
    id 403
    label "surface"
  ]
  node [
    id 404
    label "kwadrant"
  ]
  node [
    id 405
    label "degree"
  ]
  node [
    id 406
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 407
    label "ukszta&#322;towanie"
  ]
  node [
    id 408
    label "p&#322;aszczak"
  ]
  node [
    id 409
    label "rozmiar"
  ]
  node [
    id 410
    label "poj&#281;cie"
  ]
  node [
    id 411
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 412
    label "zwierciad&#322;o"
  ]
  node [
    id 413
    label "capacity"
  ]
  node [
    id 414
    label "plane"
  ]
  node [
    id 415
    label "kontekst"
  ]
  node [
    id 416
    label "miejsce_pracy"
  ]
  node [
    id 417
    label "nation"
  ]
  node [
    id 418
    label "krajobraz"
  ]
  node [
    id 419
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 420
    label "przyroda"
  ]
  node [
    id 421
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 422
    label "rozdzielanie"
  ]
  node [
    id 423
    label "bezbrze&#380;e"
  ]
  node [
    id 424
    label "punkt"
  ]
  node [
    id 425
    label "czasoprzestrze&#324;"
  ]
  node [
    id 426
    label "niezmierzony"
  ]
  node [
    id 427
    label "przedzielenie"
  ]
  node [
    id 428
    label "nielito&#347;ciwy"
  ]
  node [
    id 429
    label "rozdziela&#263;"
  ]
  node [
    id 430
    label "oktant"
  ]
  node [
    id 431
    label "przedzieli&#263;"
  ]
  node [
    id 432
    label "przestw&#243;r"
  ]
  node [
    id 433
    label "gleba"
  ]
  node [
    id 434
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 435
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 436
    label "warstwa"
  ]
  node [
    id 437
    label "Ziemia"
  ]
  node [
    id 438
    label "sialma"
  ]
  node [
    id 439
    label "warstwa_perydotytowa"
  ]
  node [
    id 440
    label "warstwa_granitowa"
  ]
  node [
    id 441
    label "powietrze"
  ]
  node [
    id 442
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 443
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 444
    label "fauna"
  ]
  node [
    id 445
    label "balkon"
  ]
  node [
    id 446
    label "budowla"
  ]
  node [
    id 447
    label "pod&#322;oga"
  ]
  node [
    id 448
    label "kondygnacja"
  ]
  node [
    id 449
    label "skrzyd&#322;o"
  ]
  node [
    id 450
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 451
    label "dach"
  ]
  node [
    id 452
    label "strop"
  ]
  node [
    id 453
    label "klatka_schodowa"
  ]
  node [
    id 454
    label "przedpro&#380;e"
  ]
  node [
    id 455
    label "Pentagon"
  ]
  node [
    id 456
    label "alkierz"
  ]
  node [
    id 457
    label "front"
  ]
  node [
    id 458
    label "amfilada"
  ]
  node [
    id 459
    label "apartment"
  ]
  node [
    id 460
    label "udost&#281;pnienie"
  ]
  node [
    id 461
    label "sklepienie"
  ]
  node [
    id 462
    label "sufit"
  ]
  node [
    id 463
    label "umieszczenie"
  ]
  node [
    id 464
    label "zakamarek"
  ]
  node [
    id 465
    label "odholowa&#263;"
  ]
  node [
    id 466
    label "przestrze&#324;_pozaziemska"
  ]
  node [
    id 467
    label "tabor"
  ]
  node [
    id 468
    label "przyholowywanie"
  ]
  node [
    id 469
    label "przyholowa&#263;"
  ]
  node [
    id 470
    label "przyholowanie"
  ]
  node [
    id 471
    label "fukni&#281;cie"
  ]
  node [
    id 472
    label "l&#261;d"
  ]
  node [
    id 473
    label "zielona_karta"
  ]
  node [
    id 474
    label "fukanie"
  ]
  node [
    id 475
    label "przyholowywa&#263;"
  ]
  node [
    id 476
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 477
    label "woda"
  ]
  node [
    id 478
    label "przeszklenie"
  ]
  node [
    id 479
    label "test_zderzeniowy"
  ]
  node [
    id 480
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 481
    label "odzywka"
  ]
  node [
    id 482
    label "nadwozie"
  ]
  node [
    id 483
    label "odholowanie"
  ]
  node [
    id 484
    label "prowadzenie_si&#281;"
  ]
  node [
    id 485
    label "odholowywa&#263;"
  ]
  node [
    id 486
    label "odholowywanie"
  ]
  node [
    id 487
    label "hamulec"
  ]
  node [
    id 488
    label "podwozie"
  ]
  node [
    id 489
    label "nasyci&#263;"
  ]
  node [
    id 490
    label "od&#347;wie&#380;y&#263;_si&#281;"
  ]
  node [
    id 491
    label "dostarczy&#263;"
  ]
  node [
    id 492
    label "metalizowa&#263;"
  ]
  node [
    id 493
    label "wzbogaca&#263;"
  ]
  node [
    id 494
    label "pokrywa&#263;"
  ]
  node [
    id 495
    label "aluminize"
  ]
  node [
    id 496
    label "zabezpiecza&#263;"
  ]
  node [
    id 497
    label "wzbogacanie"
  ]
  node [
    id 498
    label "zabezpieczanie"
  ]
  node [
    id 499
    label "pokrywanie"
  ]
  node [
    id 500
    label "metalizowanie"
  ]
  node [
    id 501
    label "level"
  ]
  node [
    id 502
    label "r&#243;wna&#263;"
  ]
  node [
    id 503
    label "uprawia&#263;"
  ]
  node [
    id 504
    label "urz&#261;dzenie"
  ]
  node [
    id 505
    label "Judea"
  ]
  node [
    id 506
    label "moszaw"
  ]
  node [
    id 507
    label "Kanaan"
  ]
  node [
    id 508
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 509
    label "Anglosas"
  ]
  node [
    id 510
    label "Jerozolima"
  ]
  node [
    id 511
    label "Etiopia"
  ]
  node [
    id 512
    label "Beskidy_Zachodnie"
  ]
  node [
    id 513
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 514
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 515
    label "Wiktoria"
  ]
  node [
    id 516
    label "Wielka_Brytania"
  ]
  node [
    id 517
    label "Guernsey"
  ]
  node [
    id 518
    label "Conrad"
  ]
  node [
    id 519
    label "funt_szterling"
  ]
  node [
    id 520
    label "Unia_Europejska"
  ]
  node [
    id 521
    label "Portland"
  ]
  node [
    id 522
    label "NATO"
  ]
  node [
    id 523
    label "El&#380;bieta_I"
  ]
  node [
    id 524
    label "Kornwalia"
  ]
  node [
    id 525
    label "Dolna_Frankonia"
  ]
  node [
    id 526
    label "Niemcy"
  ]
  node [
    id 527
    label "W&#322;ochy"
  ]
  node [
    id 528
    label "Ukraina"
  ]
  node [
    id 529
    label "Wyspy_Marshalla"
  ]
  node [
    id 530
    label "Nauru"
  ]
  node [
    id 531
    label "Mariany"
  ]
  node [
    id 532
    label "dolar"
  ]
  node [
    id 533
    label "Karpaty"
  ]
  node [
    id 534
    label "Beskid_Niski"
  ]
  node [
    id 535
    label "Polska"
  ]
  node [
    id 536
    label "Warszawa"
  ]
  node [
    id 537
    label "Mariensztat"
  ]
  node [
    id 538
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 539
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 540
    label "Paj&#281;czno"
  ]
  node [
    id 541
    label "Mogielnica"
  ]
  node [
    id 542
    label "Gop&#322;o"
  ]
  node [
    id 543
    label "Francja"
  ]
  node [
    id 544
    label "Moza"
  ]
  node [
    id 545
    label "Poprad"
  ]
  node [
    id 546
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 547
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 548
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 549
    label "Bojanowo"
  ]
  node [
    id 550
    label "Obra"
  ]
  node [
    id 551
    label "Wilkowo_Polskie"
  ]
  node [
    id 552
    label "Dobra"
  ]
  node [
    id 553
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 554
    label "Samoa"
  ]
  node [
    id 555
    label "Tonga"
  ]
  node [
    id 556
    label "Tuwalu"
  ]
  node [
    id 557
    label "Hawaje"
  ]
  node [
    id 558
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 559
    label "Rosja"
  ]
  node [
    id 560
    label "Etruria"
  ]
  node [
    id 561
    label "Rumelia"
  ]
  node [
    id 562
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 563
    label "Nowa_Zelandia"
  ]
  node [
    id 564
    label "Ocean_Spokojny"
  ]
  node [
    id 565
    label "Palau"
  ]
  node [
    id 566
    label "Melanezja"
  ]
  node [
    id 567
    label "Nowy_&#346;wiat"
  ]
  node [
    id 568
    label "Tar&#322;&#243;w"
  ]
  node [
    id 569
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 570
    label "Czeczenia"
  ]
  node [
    id 571
    label "Inguszetia"
  ]
  node [
    id 572
    label "Abchazja"
  ]
  node [
    id 573
    label "Sarmata"
  ]
  node [
    id 574
    label "Dagestan"
  ]
  node [
    id 575
    label "Eurazja"
  ]
  node [
    id 576
    label "Indie"
  ]
  node [
    id 577
    label "Pakistan"
  ]
  node [
    id 578
    label "Czarnog&#243;ra"
  ]
  node [
    id 579
    label "Serbia"
  ]
  node [
    id 580
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 581
    label "Tatry"
  ]
  node [
    id 582
    label "Podtatrze"
  ]
  node [
    id 583
    label "Imperium_Rosyjskie"
  ]
  node [
    id 584
    label "jezioro"
  ]
  node [
    id 585
    label "&#346;l&#261;sk"
  ]
  node [
    id 586
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 587
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 588
    label "Mo&#322;dawia"
  ]
  node [
    id 589
    label "Podole"
  ]
  node [
    id 590
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 591
    label "Hiszpania"
  ]
  node [
    id 592
    label "Austro-W&#281;gry"
  ]
  node [
    id 593
    label "Algieria"
  ]
  node [
    id 594
    label "funt_szkocki"
  ]
  node [
    id 595
    label "Kaledonia"
  ]
  node [
    id 596
    label "Libia"
  ]
  node [
    id 597
    label "Maroko"
  ]
  node [
    id 598
    label "Tunezja"
  ]
  node [
    id 599
    label "Mauretania"
  ]
  node [
    id 600
    label "Sahara_Zachodnia"
  ]
  node [
    id 601
    label "Biskupice"
  ]
  node [
    id 602
    label "Iwanowice"
  ]
  node [
    id 603
    label "Ziemia_Sandomierska"
  ]
  node [
    id 604
    label "Rogo&#378;nik"
  ]
  node [
    id 605
    label "Ropa"
  ]
  node [
    id 606
    label "Buriacja"
  ]
  node [
    id 607
    label "Rozewie"
  ]
  node [
    id 608
    label "Norwegia"
  ]
  node [
    id 609
    label "Szwecja"
  ]
  node [
    id 610
    label "Finlandia"
  ]
  node [
    id 611
    label "Antigua_i_Barbuda"
  ]
  node [
    id 612
    label "Kuba"
  ]
  node [
    id 613
    label "Jamajka"
  ]
  node [
    id 614
    label "Aruba"
  ]
  node [
    id 615
    label "Haiti"
  ]
  node [
    id 616
    label "Kajmany"
  ]
  node [
    id 617
    label "Portoryko"
  ]
  node [
    id 618
    label "Anguilla"
  ]
  node [
    id 619
    label "Bahamy"
  ]
  node [
    id 620
    label "Antyle"
  ]
  node [
    id 621
    label "Czechy"
  ]
  node [
    id 622
    label "Amazonka"
  ]
  node [
    id 623
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 624
    label "Wietnam"
  ]
  node [
    id 625
    label "Austria"
  ]
  node [
    id 626
    label "Alpy"
  ]
  node [
    id 627
    label "Katar"
  ]
  node [
    id 628
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 629
    label "Gwatemala"
  ]
  node [
    id 630
    label "Afganistan"
  ]
  node [
    id 631
    label "Ekwador"
  ]
  node [
    id 632
    label "Tad&#380;ykistan"
  ]
  node [
    id 633
    label "Bhutan"
  ]
  node [
    id 634
    label "Argentyna"
  ]
  node [
    id 635
    label "D&#380;ibuti"
  ]
  node [
    id 636
    label "Wenezuela"
  ]
  node [
    id 637
    label "Gabon"
  ]
  node [
    id 638
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 639
    label "Rwanda"
  ]
  node [
    id 640
    label "Liechtenstein"
  ]
  node [
    id 641
    label "organizacja"
  ]
  node [
    id 642
    label "Sri_Lanka"
  ]
  node [
    id 643
    label "Madagaskar"
  ]
  node [
    id 644
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 645
    label "Kongo"
  ]
  node [
    id 646
    label "Bangladesz"
  ]
  node [
    id 647
    label "Kanada"
  ]
  node [
    id 648
    label "Wehrlen"
  ]
  node [
    id 649
    label "Surinam"
  ]
  node [
    id 650
    label "Chile"
  ]
  node [
    id 651
    label "Uganda"
  ]
  node [
    id 652
    label "W&#281;gry"
  ]
  node [
    id 653
    label "Birma"
  ]
  node [
    id 654
    label "Kazachstan"
  ]
  node [
    id 655
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 656
    label "Armenia"
  ]
  node [
    id 657
    label "Timor_Wschodni"
  ]
  node [
    id 658
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 659
    label "Izrael"
  ]
  node [
    id 660
    label "Estonia"
  ]
  node [
    id 661
    label "Komory"
  ]
  node [
    id 662
    label "Kamerun"
  ]
  node [
    id 663
    label "Belize"
  ]
  node [
    id 664
    label "Sierra_Leone"
  ]
  node [
    id 665
    label "Luksemburg"
  ]
  node [
    id 666
    label "USA"
  ]
  node [
    id 667
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 668
    label "Barbados"
  ]
  node [
    id 669
    label "San_Marino"
  ]
  node [
    id 670
    label "Bu&#322;garia"
  ]
  node [
    id 671
    label "Indonezja"
  ]
  node [
    id 672
    label "Malawi"
  ]
  node [
    id 673
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 674
    label "partia"
  ]
  node [
    id 675
    label "Zambia"
  ]
  node [
    id 676
    label "Angola"
  ]
  node [
    id 677
    label "Grenada"
  ]
  node [
    id 678
    label "Nepal"
  ]
  node [
    id 679
    label "Panama"
  ]
  node [
    id 680
    label "Rumunia"
  ]
  node [
    id 681
    label "Malediwy"
  ]
  node [
    id 682
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 683
    label "S&#322;owacja"
  ]
  node [
    id 684
    label "para"
  ]
  node [
    id 685
    label "Egipt"
  ]
  node [
    id 686
    label "zwrot"
  ]
  node [
    id 687
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 688
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 689
    label "Kolumbia"
  ]
  node [
    id 690
    label "Mozambik"
  ]
  node [
    id 691
    label "Laos"
  ]
  node [
    id 692
    label "Burundi"
  ]
  node [
    id 693
    label "Suazi"
  ]
  node [
    id 694
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 695
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 696
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 697
    label "Trynidad_i_Tobago"
  ]
  node [
    id 698
    label "Dominika"
  ]
  node [
    id 699
    label "Syria"
  ]
  node [
    id 700
    label "Gwinea_Bissau"
  ]
  node [
    id 701
    label "Liberia"
  ]
  node [
    id 702
    label "Zimbabwe"
  ]
  node [
    id 703
    label "Dominikana"
  ]
  node [
    id 704
    label "Senegal"
  ]
  node [
    id 705
    label "Gruzja"
  ]
  node [
    id 706
    label "Togo"
  ]
  node [
    id 707
    label "Chorwacja"
  ]
  node [
    id 708
    label "Meksyk"
  ]
  node [
    id 709
    label "Macedonia"
  ]
  node [
    id 710
    label "Gujana"
  ]
  node [
    id 711
    label "Zair"
  ]
  node [
    id 712
    label "Albania"
  ]
  node [
    id 713
    label "Kambod&#380;a"
  ]
  node [
    id 714
    label "Mauritius"
  ]
  node [
    id 715
    label "Monako"
  ]
  node [
    id 716
    label "Gwinea"
  ]
  node [
    id 717
    label "Mali"
  ]
  node [
    id 718
    label "Nigeria"
  ]
  node [
    id 719
    label "Kostaryka"
  ]
  node [
    id 720
    label "Hanower"
  ]
  node [
    id 721
    label "Paragwaj"
  ]
  node [
    id 722
    label "Wyspy_Salomona"
  ]
  node [
    id 723
    label "Seszele"
  ]
  node [
    id 724
    label "Boliwia"
  ]
  node [
    id 725
    label "Kirgistan"
  ]
  node [
    id 726
    label "Irlandia"
  ]
  node [
    id 727
    label "Czad"
  ]
  node [
    id 728
    label "Irak"
  ]
  node [
    id 729
    label "Lesoto"
  ]
  node [
    id 730
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 731
    label "Malta"
  ]
  node [
    id 732
    label "Andora"
  ]
  node [
    id 733
    label "Chiny"
  ]
  node [
    id 734
    label "Filipiny"
  ]
  node [
    id 735
    label "Antarktis"
  ]
  node [
    id 736
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 737
    label "Brazylia"
  ]
  node [
    id 738
    label "Nikaragua"
  ]
  node [
    id 739
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 740
    label "Kenia"
  ]
  node [
    id 741
    label "Niger"
  ]
  node [
    id 742
    label "Portugalia"
  ]
  node [
    id 743
    label "Fid&#380;i"
  ]
  node [
    id 744
    label "Botswana"
  ]
  node [
    id 745
    label "Tajlandia"
  ]
  node [
    id 746
    label "Australia"
  ]
  node [
    id 747
    label "Burkina_Faso"
  ]
  node [
    id 748
    label "interior"
  ]
  node [
    id 749
    label "Benin"
  ]
  node [
    id 750
    label "Tanzania"
  ]
  node [
    id 751
    label "&#321;otwa"
  ]
  node [
    id 752
    label "Kiribati"
  ]
  node [
    id 753
    label "Rodezja"
  ]
  node [
    id 754
    label "Cypr"
  ]
  node [
    id 755
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 756
    label "Peru"
  ]
  node [
    id 757
    label "Urugwaj"
  ]
  node [
    id 758
    label "Jordania"
  ]
  node [
    id 759
    label "Grecja"
  ]
  node [
    id 760
    label "Azerbejd&#380;an"
  ]
  node [
    id 761
    label "Turcja"
  ]
  node [
    id 762
    label "Sudan"
  ]
  node [
    id 763
    label "Oman"
  ]
  node [
    id 764
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 765
    label "Uzbekistan"
  ]
  node [
    id 766
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 767
    label "Honduras"
  ]
  node [
    id 768
    label "Mongolia"
  ]
  node [
    id 769
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 770
    label "Tajwan"
  ]
  node [
    id 771
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 772
    label "Liban"
  ]
  node [
    id 773
    label "Japonia"
  ]
  node [
    id 774
    label "Ghana"
  ]
  node [
    id 775
    label "Bahrajn"
  ]
  node [
    id 776
    label "Belgia"
  ]
  node [
    id 777
    label "Kuwejt"
  ]
  node [
    id 778
    label "grupa"
  ]
  node [
    id 779
    label "Litwa"
  ]
  node [
    id 780
    label "S&#322;owenia"
  ]
  node [
    id 781
    label "Szwajcaria"
  ]
  node [
    id 782
    label "Erytrea"
  ]
  node [
    id 783
    label "Arabia_Saudyjska"
  ]
  node [
    id 784
    label "granica_pa&#324;stwa"
  ]
  node [
    id 785
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 786
    label "Malezja"
  ]
  node [
    id 787
    label "Korea"
  ]
  node [
    id 788
    label "Jemen"
  ]
  node [
    id 789
    label "Namibia"
  ]
  node [
    id 790
    label "holoarktyka"
  ]
  node [
    id 791
    label "Brunei"
  ]
  node [
    id 792
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 793
    label "Khitai"
  ]
  node [
    id 794
    label "Iran"
  ]
  node [
    id 795
    label "Gambia"
  ]
  node [
    id 796
    label "Somalia"
  ]
  node [
    id 797
    label "Holandia"
  ]
  node [
    id 798
    label "Turkmenistan"
  ]
  node [
    id 799
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 800
    label "Salwador"
  ]
  node [
    id 801
    label "substancja_szara"
  ]
  node [
    id 802
    label "tkanka"
  ]
  node [
    id 803
    label "kom&#243;rka_glejowa"
  ]
  node [
    id 804
    label "neuroglia"
  ]
  node [
    id 805
    label "ubytek"
  ]
  node [
    id 806
    label "fleczer"
  ]
  node [
    id 807
    label "choroba_bakteryjna"
  ]
  node [
    id 808
    label "schorzenie"
  ]
  node [
    id 809
    label "kwas_huminowy"
  ]
  node [
    id 810
    label "kamfenol"
  ]
  node [
    id 811
    label "&#322;yko"
  ]
  node [
    id 812
    label "necrosis"
  ]
  node [
    id 813
    label "odle&#380;yna"
  ]
  node [
    id 814
    label "zanikni&#281;cie"
  ]
  node [
    id 815
    label "zmiana_wsteczna"
  ]
  node [
    id 816
    label "ska&#322;a_osadowa"
  ]
  node [
    id 817
    label "korek"
  ]
  node [
    id 818
    label "system_korzeniowy"
  ]
  node [
    id 819
    label "bakteria"
  ]
  node [
    id 820
    label "pu&#322;apka"
  ]
  node [
    id 821
    label "moment_magnitude_scale"
  ]
  node [
    id 822
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 823
    label "trz&#281;sienie_ziemi"
  ]
  node [
    id 824
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 825
    label "afflict"
  ]
  node [
    id 826
    label "spotka&#263;"
  ]
  node [
    id 827
    label "odwiedzi&#263;"
  ]
  node [
    id 828
    label "inflict"
  ]
  node [
    id 829
    label "visit"
  ]
  node [
    id 830
    label "ogarn&#261;&#263;"
  ]
  node [
    id 831
    label "visualize"
  ]
  node [
    id 832
    label "zawita&#263;"
  ]
  node [
    id 833
    label "manipulate"
  ]
  node [
    id 834
    label "otoczy&#263;"
  ]
  node [
    id 835
    label "spowodowa&#263;"
  ]
  node [
    id 836
    label "involve"
  ]
  node [
    id 837
    label "environment"
  ]
  node [
    id 838
    label "dotkn&#261;&#263;"
  ]
  node [
    id 839
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 840
    label "insert"
  ]
  node [
    id 841
    label "pozna&#263;"
  ]
  node [
    id 842
    label "befall"
  ]
  node [
    id 843
    label "go_steady"
  ]
  node [
    id 844
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 845
    label "znale&#378;&#263;"
  ]
  node [
    id 846
    label "egzemplarz"
  ]
  node [
    id 847
    label "series"
  ]
  node [
    id 848
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 849
    label "uprawianie"
  ]
  node [
    id 850
    label "praca_rolnicza"
  ]
  node [
    id 851
    label "collection"
  ]
  node [
    id 852
    label "dane"
  ]
  node [
    id 853
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 854
    label "pakiet_klimatyczny"
  ]
  node [
    id 855
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 856
    label "sum"
  ]
  node [
    id 857
    label "gathering"
  ]
  node [
    id 858
    label "album"
  ]
  node [
    id 859
    label "Wile&#324;szczyzna"
  ]
  node [
    id 860
    label "Jukon"
  ]
  node [
    id 861
    label "integer"
  ]
  node [
    id 862
    label "liczba"
  ]
  node [
    id 863
    label "zlewanie_si&#281;"
  ]
  node [
    id 864
    label "ilo&#347;&#263;"
  ]
  node [
    id 865
    label "uk&#322;ad"
  ]
  node [
    id 866
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 867
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 868
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 869
    label "parametr"
  ]
  node [
    id 870
    label "poziom"
  ]
  node [
    id 871
    label "znaczenie"
  ]
  node [
    id 872
    label "wielko&#347;&#263;"
  ]
  node [
    id 873
    label "dymensja"
  ]
  node [
    id 874
    label "strona"
  ]
  node [
    id 875
    label "wiecz&#243;r"
  ]
  node [
    id 876
    label "sunset"
  ]
  node [
    id 877
    label "szar&#243;wka"
  ]
  node [
    id 878
    label "usi&#322;owanie"
  ]
  node [
    id 879
    label "strona_&#347;wiata"
  ]
  node [
    id 880
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 881
    label "zjawisko"
  ]
  node [
    id 882
    label "pora"
  ]
  node [
    id 883
    label "trud"
  ]
  node [
    id 884
    label "s&#322;o&#324;ce"
  ]
  node [
    id 885
    label "brzask"
  ]
  node [
    id 886
    label "pocz&#261;tek"
  ]
  node [
    id 887
    label "szabas"
  ]
  node [
    id 888
    label "rano"
  ]
  node [
    id 889
    label "&#347;rodek"
  ]
  node [
    id 890
    label "dzie&#324;"
  ]
  node [
    id 891
    label "dwunasta"
  ]
  node [
    id 892
    label "godzina"
  ]
  node [
    id 893
    label "Boreasz"
  ]
  node [
    id 894
    label "noc"
  ]
  node [
    id 895
    label "&#347;wiat"
  ]
  node [
    id 896
    label "p&#243;&#322;nocek"
  ]
  node [
    id 897
    label "Bie&#380;an&#243;w-Prokocim"
  ]
  node [
    id 898
    label "Pr&#261;dnik_Czerwony"
  ]
  node [
    id 899
    label "Czy&#380;yny"
  ]
  node [
    id 900
    label "Zwierzyniec"
  ]
  node [
    id 901
    label "euro"
  ]
  node [
    id 902
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 903
    label "lodowiec_kontynentalny"
  ]
  node [
    id 904
    label "Antarktyda"
  ]
  node [
    id 905
    label "Rataje"
  ]
  node [
    id 906
    label "G&#322;uszyna"
  ]
  node [
    id 907
    label "Kaw&#281;czyn"
  ]
  node [
    id 908
    label "Podg&#243;rze"
  ]
  node [
    id 909
    label "D&#281;bniki"
  ]
  node [
    id 910
    label "Kresy"
  ]
  node [
    id 911
    label "palearktyka"
  ]
  node [
    id 912
    label "nearktyka"
  ]
  node [
    id 913
    label "biosfera"
  ]
  node [
    id 914
    label "rozrost"
  ]
  node [
    id 915
    label "wzrost"
  ]
  node [
    id 916
    label "dysk_akrecyjny"
  ]
  node [
    id 917
    label "proces_biologiczny"
  ]
  node [
    id 918
    label "accretion"
  ]
  node [
    id 919
    label "sfera"
  ]
  node [
    id 920
    label "granica"
  ]
  node [
    id 921
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 922
    label "podzakres"
  ]
  node [
    id 923
    label "dziedzina"
  ]
  node [
    id 924
    label "desygnat"
  ]
  node [
    id 925
    label "circle"
  ]
  node [
    id 926
    label "syrniki"
  ]
  node [
    id 927
    label "placek"
  ]
  node [
    id 928
    label "linia"
  ]
  node [
    id 929
    label "ekoton"
  ]
  node [
    id 930
    label "str&#261;d"
  ]
  node [
    id 931
    label "ekosystem"
  ]
  node [
    id 932
    label "kszta&#322;t"
  ]
  node [
    id 933
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 934
    label "armia"
  ]
  node [
    id 935
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 936
    label "poprowadzi&#263;"
  ]
  node [
    id 937
    label "cord"
  ]
  node [
    id 938
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 939
    label "trasa"
  ]
  node [
    id 940
    label "po&#322;&#261;czenie"
  ]
  node [
    id 941
    label "tract"
  ]
  node [
    id 942
    label "materia&#322;_zecerski"
  ]
  node [
    id 943
    label "przeorientowywanie"
  ]
  node [
    id 944
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 945
    label "curve"
  ]
  node [
    id 946
    label "figura_geometryczna"
  ]
  node [
    id 947
    label "wygl&#261;d"
  ]
  node [
    id 948
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 949
    label "jard"
  ]
  node [
    id 950
    label "szczep"
  ]
  node [
    id 951
    label "phreaker"
  ]
  node [
    id 952
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 953
    label "grupa_organizm&#243;w"
  ]
  node [
    id 954
    label "prowadzi&#263;"
  ]
  node [
    id 955
    label "przeorientowywa&#263;"
  ]
  node [
    id 956
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 957
    label "access"
  ]
  node [
    id 958
    label "przeorientowanie"
  ]
  node [
    id 959
    label "przeorientowa&#263;"
  ]
  node [
    id 960
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 961
    label "billing"
  ]
  node [
    id 962
    label "szpaler"
  ]
  node [
    id 963
    label "sztrych"
  ]
  node [
    id 964
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 965
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 966
    label "drzewo_genealogiczne"
  ]
  node [
    id 967
    label "transporter"
  ]
  node [
    id 968
    label "line"
  ]
  node [
    id 969
    label "fragment"
  ]
  node [
    id 970
    label "kompleksja"
  ]
  node [
    id 971
    label "przew&#243;d"
  ]
  node [
    id 972
    label "budowa"
  ]
  node [
    id 973
    label "granice"
  ]
  node [
    id 974
    label "kontakt"
  ]
  node [
    id 975
    label "przewo&#378;nik"
  ]
  node [
    id 976
    label "przystanek"
  ]
  node [
    id 977
    label "linijka"
  ]
  node [
    id 978
    label "spos&#243;b"
  ]
  node [
    id 979
    label "uporz&#261;dkowanie"
  ]
  node [
    id 980
    label "coalescence"
  ]
  node [
    id 981
    label "Ural"
  ]
  node [
    id 982
    label "point"
  ]
  node [
    id 983
    label "bearing"
  ]
  node [
    id 984
    label "prowadzenie"
  ]
  node [
    id 985
    label "tekst"
  ]
  node [
    id 986
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 987
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 988
    label "koniec"
  ]
  node [
    id 989
    label "spi&#281;trza&#263;"
  ]
  node [
    id 990
    label "spi&#281;trzenie"
  ]
  node [
    id 991
    label "utylizator"
  ]
  node [
    id 992
    label "obiekt_naturalny"
  ]
  node [
    id 993
    label "p&#322;ycizna"
  ]
  node [
    id 994
    label "nabranie"
  ]
  node [
    id 995
    label "Waruna"
  ]
  node [
    id 996
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 997
    label "przybieranie"
  ]
  node [
    id 998
    label "uci&#261;g"
  ]
  node [
    id 999
    label "bombast"
  ]
  node [
    id 1000
    label "fala"
  ]
  node [
    id 1001
    label "kryptodepresja"
  ]
  node [
    id 1002
    label "water"
  ]
  node [
    id 1003
    label "wysi&#281;k"
  ]
  node [
    id 1004
    label "pustka"
  ]
  node [
    id 1005
    label "ciecz"
  ]
  node [
    id 1006
    label "przybrze&#380;e"
  ]
  node [
    id 1007
    label "nap&#243;j"
  ]
  node [
    id 1008
    label "spi&#281;trzanie"
  ]
  node [
    id 1009
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 1010
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 1011
    label "bicie"
  ]
  node [
    id 1012
    label "klarownik"
  ]
  node [
    id 1013
    label "chlastanie"
  ]
  node [
    id 1014
    label "woda_s&#322;odka"
  ]
  node [
    id 1015
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 1016
    label "nabra&#263;"
  ]
  node [
    id 1017
    label "chlasta&#263;"
  ]
  node [
    id 1018
    label "uj&#281;cie_wody"
  ]
  node [
    id 1019
    label "zrzut"
  ]
  node [
    id 1020
    label "wypowied&#378;"
  ]
  node [
    id 1021
    label "wodnik"
  ]
  node [
    id 1022
    label "l&#243;d"
  ]
  node [
    id 1023
    label "deklamacja"
  ]
  node [
    id 1024
    label "tlenek"
  ]
  node [
    id 1025
    label "gwiazda"
  ]
  node [
    id 1026
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 1027
    label "Arktur"
  ]
  node [
    id 1028
    label "Gwiazda_Polarna"
  ]
  node [
    id 1029
    label "agregatka"
  ]
  node [
    id 1030
    label "gromada"
  ]
  node [
    id 1031
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 1032
    label "S&#322;o&#324;ce"
  ]
  node [
    id 1033
    label "Nibiru"
  ]
  node [
    id 1034
    label "konstelacja"
  ]
  node [
    id 1035
    label "ornament"
  ]
  node [
    id 1036
    label "delta_Scuti"
  ]
  node [
    id 1037
    label "&#347;wiat&#322;o"
  ]
  node [
    id 1038
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 1039
    label "obiekt"
  ]
  node [
    id 1040
    label "s&#322;awa"
  ]
  node [
    id 1041
    label "promie&#324;"
  ]
  node [
    id 1042
    label "star"
  ]
  node [
    id 1043
    label "gwiazdosz"
  ]
  node [
    id 1044
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 1045
    label "asocjacja_gwiazd"
  ]
  node [
    id 1046
    label "supergrupa"
  ]
  node [
    id 1047
    label "gor&#261;cy"
  ]
  node [
    id 1048
    label "s&#322;oneczny"
  ]
  node [
    id 1049
    label "po&#322;udniowo"
  ]
  node [
    id 1050
    label "charakterystycznie"
  ]
  node [
    id 1051
    label "stresogenny"
  ]
  node [
    id 1052
    label "rozpalenie_si&#281;"
  ]
  node [
    id 1053
    label "sensacyjny"
  ]
  node [
    id 1054
    label "rozpalanie_si&#281;"
  ]
  node [
    id 1055
    label "na_gor&#261;co"
  ]
  node [
    id 1056
    label "&#380;arki"
  ]
  node [
    id 1057
    label "serdeczny"
  ]
  node [
    id 1058
    label "ciep&#322;y"
  ]
  node [
    id 1059
    label "g&#322;&#281;boki"
  ]
  node [
    id 1060
    label "gor&#261;co"
  ]
  node [
    id 1061
    label "seksowny"
  ]
  node [
    id 1062
    label "&#347;wie&#380;y"
  ]
  node [
    id 1063
    label "s&#322;onecznie"
  ]
  node [
    id 1064
    label "letni"
  ]
  node [
    id 1065
    label "weso&#322;y"
  ]
  node [
    id 1066
    label "bezdeszczowy"
  ]
  node [
    id 1067
    label "bezchmurny"
  ]
  node [
    id 1068
    label "pogodny"
  ]
  node [
    id 1069
    label "fotowoltaiczny"
  ]
  node [
    id 1070
    label "inform"
  ]
  node [
    id 1071
    label "zakomunikowa&#263;"
  ]
  node [
    id 1072
    label "blok"
  ]
  node [
    id 1073
    label "Hollywood"
  ]
  node [
    id 1074
    label "centrolew"
  ]
  node [
    id 1075
    label "sejm"
  ]
  node [
    id 1076
    label "o&#347;rodek"
  ]
  node [
    id 1077
    label "centroprawica"
  ]
  node [
    id 1078
    label "core"
  ]
  node [
    id 1079
    label "skupisko"
  ]
  node [
    id 1080
    label "zal&#261;&#380;ek"
  ]
  node [
    id 1081
    label "instytucja"
  ]
  node [
    id 1082
    label "otoczenie"
  ]
  node [
    id 1083
    label "warunki"
  ]
  node [
    id 1084
    label "center"
  ]
  node [
    id 1085
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 1086
    label "bajt"
  ]
  node [
    id 1087
    label "bloking"
  ]
  node [
    id 1088
    label "j&#261;kanie"
  ]
  node [
    id 1089
    label "przeszkoda"
  ]
  node [
    id 1090
    label "zesp&#243;&#322;"
  ]
  node [
    id 1091
    label "blokada"
  ]
  node [
    id 1092
    label "bry&#322;a"
  ]
  node [
    id 1093
    label "dzia&#322;"
  ]
  node [
    id 1094
    label "kontynent"
  ]
  node [
    id 1095
    label "nastawnia"
  ]
  node [
    id 1096
    label "blockage"
  ]
  node [
    id 1097
    label "block"
  ]
  node [
    id 1098
    label "start"
  ]
  node [
    id 1099
    label "program"
  ]
  node [
    id 1100
    label "zeszyt"
  ]
  node [
    id 1101
    label "blokowisko"
  ]
  node [
    id 1102
    label "artyku&#322;"
  ]
  node [
    id 1103
    label "barak"
  ]
  node [
    id 1104
    label "stok_kontynentalny"
  ]
  node [
    id 1105
    label "whole"
  ]
  node [
    id 1106
    label "square"
  ]
  node [
    id 1107
    label "siatk&#243;wka"
  ]
  node [
    id 1108
    label "kr&#261;g"
  ]
  node [
    id 1109
    label "ram&#243;wka"
  ]
  node [
    id 1110
    label "zamek"
  ]
  node [
    id 1111
    label "obrona"
  ]
  node [
    id 1112
    label "ok&#322;adka"
  ]
  node [
    id 1113
    label "bie&#380;nia"
  ]
  node [
    id 1114
    label "referat"
  ]
  node [
    id 1115
    label "dom_wielorodzinny"
  ]
  node [
    id 1116
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 1117
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1118
    label "sprawa"
  ]
  node [
    id 1119
    label "ust&#281;p"
  ]
  node [
    id 1120
    label "plan"
  ]
  node [
    id 1121
    label "obiekt_matematyczny"
  ]
  node [
    id 1122
    label "problemat"
  ]
  node [
    id 1123
    label "plamka"
  ]
  node [
    id 1124
    label "stopie&#324;_pisma"
  ]
  node [
    id 1125
    label "jednostka"
  ]
  node [
    id 1126
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1127
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1128
    label "mark"
  ]
  node [
    id 1129
    label "prosta"
  ]
  node [
    id 1130
    label "problematyka"
  ]
  node [
    id 1131
    label "zapunktowa&#263;"
  ]
  node [
    id 1132
    label "podpunkt"
  ]
  node [
    id 1133
    label "wojsko"
  ]
  node [
    id 1134
    label "kres"
  ]
  node [
    id 1135
    label "pozycja"
  ]
  node [
    id 1136
    label "koalicja"
  ]
  node [
    id 1137
    label "parlament"
  ]
  node [
    id 1138
    label "izba_ni&#380;sza"
  ]
  node [
    id 1139
    label "lewica"
  ]
  node [
    id 1140
    label "siedziba"
  ]
  node [
    id 1141
    label "parliament"
  ]
  node [
    id 1142
    label "obrady"
  ]
  node [
    id 1143
    label "prawica"
  ]
  node [
    id 1144
    label "zgromadzenie"
  ]
  node [
    id 1145
    label "Los_Angeles"
  ]
  node [
    id 1146
    label "caution"
  ]
  node [
    id 1147
    label "uprzedza&#263;"
  ]
  node [
    id 1148
    label "robi&#263;"
  ]
  node [
    id 1149
    label "og&#322;asza&#263;"
  ]
  node [
    id 1150
    label "post"
  ]
  node [
    id 1151
    label "informowa&#263;"
  ]
  node [
    id 1152
    label "anticipate"
  ]
  node [
    id 1153
    label "fala_morska"
  ]
  node [
    id 1154
    label "kl&#281;ska_&#380;ywio&#322;owa"
  ]
  node [
    id 1155
    label "Pacyfik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 3
    target 600
  ]
  edge [
    source 3
    target 601
  ]
  edge [
    source 3
    target 602
  ]
  edge [
    source 3
    target 603
  ]
  edge [
    source 3
    target 604
  ]
  edge [
    source 3
    target 605
  ]
  edge [
    source 3
    target 606
  ]
  edge [
    source 3
    target 607
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 612
  ]
  edge [
    source 3
    target 613
  ]
  edge [
    source 3
    target 614
  ]
  edge [
    source 3
    target 615
  ]
  edge [
    source 3
    target 616
  ]
  edge [
    source 3
    target 617
  ]
  edge [
    source 3
    target 618
  ]
  edge [
    source 3
    target 619
  ]
  edge [
    source 3
    target 620
  ]
  edge [
    source 3
    target 621
  ]
  edge [
    source 3
    target 622
  ]
  edge [
    source 3
    target 623
  ]
  edge [
    source 3
    target 624
  ]
  edge [
    source 3
    target 625
  ]
  edge [
    source 3
    target 626
  ]
  edge [
    source 3
    target 627
  ]
  edge [
    source 3
    target 628
  ]
  edge [
    source 3
    target 629
  ]
  edge [
    source 3
    target 630
  ]
  edge [
    source 3
    target 631
  ]
  edge [
    source 3
    target 632
  ]
  edge [
    source 3
    target 633
  ]
  edge [
    source 3
    target 634
  ]
  edge [
    source 3
    target 635
  ]
  edge [
    source 3
    target 636
  ]
  edge [
    source 3
    target 637
  ]
  edge [
    source 3
    target 638
  ]
  edge [
    source 3
    target 639
  ]
  edge [
    source 3
    target 640
  ]
  edge [
    source 3
    target 641
  ]
  edge [
    source 3
    target 642
  ]
  edge [
    source 3
    target 643
  ]
  edge [
    source 3
    target 644
  ]
  edge [
    source 3
    target 645
  ]
  edge [
    source 3
    target 646
  ]
  edge [
    source 3
    target 647
  ]
  edge [
    source 3
    target 648
  ]
  edge [
    source 3
    target 649
  ]
  edge [
    source 3
    target 650
  ]
  edge [
    source 3
    target 651
  ]
  edge [
    source 3
    target 652
  ]
  edge [
    source 3
    target 653
  ]
  edge [
    source 3
    target 654
  ]
  edge [
    source 3
    target 655
  ]
  edge [
    source 3
    target 656
  ]
  edge [
    source 3
    target 657
  ]
  edge [
    source 3
    target 658
  ]
  edge [
    source 3
    target 659
  ]
  edge [
    source 3
    target 660
  ]
  edge [
    source 3
    target 661
  ]
  edge [
    source 3
    target 662
  ]
  edge [
    source 3
    target 663
  ]
  edge [
    source 3
    target 664
  ]
  edge [
    source 3
    target 665
  ]
  edge [
    source 3
    target 666
  ]
  edge [
    source 3
    target 667
  ]
  edge [
    source 3
    target 668
  ]
  edge [
    source 3
    target 669
  ]
  edge [
    source 3
    target 670
  ]
  edge [
    source 3
    target 671
  ]
  edge [
    source 3
    target 672
  ]
  edge [
    source 3
    target 673
  ]
  edge [
    source 3
    target 674
  ]
  edge [
    source 3
    target 675
  ]
  edge [
    source 3
    target 676
  ]
  edge [
    source 3
    target 677
  ]
  edge [
    source 3
    target 678
  ]
  edge [
    source 3
    target 679
  ]
  edge [
    source 3
    target 680
  ]
  edge [
    source 3
    target 681
  ]
  edge [
    source 3
    target 682
  ]
  edge [
    source 3
    target 683
  ]
  edge [
    source 3
    target 684
  ]
  edge [
    source 3
    target 685
  ]
  edge [
    source 3
    target 686
  ]
  edge [
    source 3
    target 687
  ]
  edge [
    source 3
    target 688
  ]
  edge [
    source 3
    target 689
  ]
  edge [
    source 3
    target 690
  ]
  edge [
    source 3
    target 691
  ]
  edge [
    source 3
    target 692
  ]
  edge [
    source 3
    target 693
  ]
  edge [
    source 3
    target 694
  ]
  edge [
    source 3
    target 695
  ]
  edge [
    source 3
    target 696
  ]
  edge [
    source 3
    target 697
  ]
  edge [
    source 3
    target 698
  ]
  edge [
    source 3
    target 699
  ]
  edge [
    source 3
    target 700
  ]
  edge [
    source 3
    target 701
  ]
  edge [
    source 3
    target 702
  ]
  edge [
    source 3
    target 703
  ]
  edge [
    source 3
    target 704
  ]
  edge [
    source 3
    target 705
  ]
  edge [
    source 3
    target 706
  ]
  edge [
    source 3
    target 707
  ]
  edge [
    source 3
    target 708
  ]
  edge [
    source 3
    target 709
  ]
  edge [
    source 3
    target 710
  ]
  edge [
    source 3
    target 711
  ]
  edge [
    source 3
    target 712
  ]
  edge [
    source 3
    target 713
  ]
  edge [
    source 3
    target 714
  ]
  edge [
    source 3
    target 715
  ]
  edge [
    source 3
    target 716
  ]
  edge [
    source 3
    target 717
  ]
  edge [
    source 3
    target 718
  ]
  edge [
    source 3
    target 719
  ]
  edge [
    source 3
    target 720
  ]
  edge [
    source 3
    target 721
  ]
  edge [
    source 3
    target 722
  ]
  edge [
    source 3
    target 723
  ]
  edge [
    source 3
    target 724
  ]
  edge [
    source 3
    target 725
  ]
  edge [
    source 3
    target 726
  ]
  edge [
    source 3
    target 727
  ]
  edge [
    source 3
    target 728
  ]
  edge [
    source 3
    target 729
  ]
  edge [
    source 3
    target 730
  ]
  edge [
    source 3
    target 731
  ]
  edge [
    source 3
    target 732
  ]
  edge [
    source 3
    target 733
  ]
  edge [
    source 3
    target 734
  ]
  edge [
    source 3
    target 735
  ]
  edge [
    source 3
    target 736
  ]
  edge [
    source 3
    target 737
  ]
  edge [
    source 3
    target 738
  ]
  edge [
    source 3
    target 739
  ]
  edge [
    source 3
    target 740
  ]
  edge [
    source 3
    target 741
  ]
  edge [
    source 3
    target 742
  ]
  edge [
    source 3
    target 743
  ]
  edge [
    source 3
    target 744
  ]
  edge [
    source 3
    target 745
  ]
  edge [
    source 3
    target 746
  ]
  edge [
    source 3
    target 747
  ]
  edge [
    source 3
    target 748
  ]
  edge [
    source 3
    target 749
  ]
  edge [
    source 3
    target 750
  ]
  edge [
    source 3
    target 751
  ]
  edge [
    source 3
    target 752
  ]
  edge [
    source 3
    target 753
  ]
  edge [
    source 3
    target 754
  ]
  edge [
    source 3
    target 755
  ]
  edge [
    source 3
    target 756
  ]
  edge [
    source 3
    target 757
  ]
  edge [
    source 3
    target 758
  ]
  edge [
    source 3
    target 759
  ]
  edge [
    source 3
    target 760
  ]
  edge [
    source 3
    target 761
  ]
  edge [
    source 3
    target 762
  ]
  edge [
    source 3
    target 763
  ]
  edge [
    source 3
    target 764
  ]
  edge [
    source 3
    target 765
  ]
  edge [
    source 3
    target 766
  ]
  edge [
    source 3
    target 767
  ]
  edge [
    source 3
    target 768
  ]
  edge [
    source 3
    target 769
  ]
  edge [
    source 3
    target 770
  ]
  edge [
    source 3
    target 771
  ]
  edge [
    source 3
    target 772
  ]
  edge [
    source 3
    target 773
  ]
  edge [
    source 3
    target 774
  ]
  edge [
    source 3
    target 775
  ]
  edge [
    source 3
    target 776
  ]
  edge [
    source 3
    target 777
  ]
  edge [
    source 3
    target 778
  ]
  edge [
    source 3
    target 779
  ]
  edge [
    source 3
    target 780
  ]
  edge [
    source 3
    target 781
  ]
  edge [
    source 3
    target 782
  ]
  edge [
    source 3
    target 783
  ]
  edge [
    source 3
    target 784
  ]
  edge [
    source 3
    target 785
  ]
  edge [
    source 3
    target 786
  ]
  edge [
    source 3
    target 787
  ]
  edge [
    source 3
    target 788
  ]
  edge [
    source 3
    target 789
  ]
  edge [
    source 3
    target 790
  ]
  edge [
    source 3
    target 791
  ]
  edge [
    source 3
    target 792
  ]
  edge [
    source 3
    target 793
  ]
  edge [
    source 3
    target 794
  ]
  edge [
    source 3
    target 795
  ]
  edge [
    source 3
    target 796
  ]
  edge [
    source 3
    target 797
  ]
  edge [
    source 3
    target 798
  ]
  edge [
    source 3
    target 799
  ]
  edge [
    source 3
    target 800
  ]
  edge [
    source 3
    target 801
  ]
  edge [
    source 3
    target 802
  ]
  edge [
    source 3
    target 803
  ]
  edge [
    source 3
    target 804
  ]
  edge [
    source 3
    target 805
  ]
  edge [
    source 3
    target 806
  ]
  edge [
    source 3
    target 807
  ]
  edge [
    source 3
    target 808
  ]
  edge [
    source 3
    target 809
  ]
  edge [
    source 3
    target 810
  ]
  edge [
    source 3
    target 811
  ]
  edge [
    source 3
    target 812
  ]
  edge [
    source 3
    target 813
  ]
  edge [
    source 3
    target 814
  ]
  edge [
    source 3
    target 815
  ]
  edge [
    source 3
    target 816
  ]
  edge [
    source 3
    target 817
  ]
  edge [
    source 3
    target 818
  ]
  edge [
    source 3
    target 819
  ]
  edge [
    source 3
    target 820
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 821
  ]
  edge [
    source 4
    target 822
  ]
  edge [
    source 4
    target 823
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 824
  ]
  edge [
    source 5
    target 825
  ]
  edge [
    source 5
    target 826
  ]
  edge [
    source 5
    target 827
  ]
  edge [
    source 5
    target 828
  ]
  edge [
    source 5
    target 829
  ]
  edge [
    source 5
    target 830
  ]
  edge [
    source 5
    target 831
  ]
  edge [
    source 5
    target 832
  ]
  edge [
    source 5
    target 833
  ]
  edge [
    source 5
    target 834
  ]
  edge [
    source 5
    target 835
  ]
  edge [
    source 5
    target 836
  ]
  edge [
    source 5
    target 837
  ]
  edge [
    source 5
    target 838
  ]
  edge [
    source 5
    target 839
  ]
  edge [
    source 5
    target 840
  ]
  edge [
    source 5
    target 841
  ]
  edge [
    source 5
    target 842
  ]
  edge [
    source 5
    target 843
  ]
  edge [
    source 5
    target 844
  ]
  edge [
    source 5
    target 845
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 846
  ]
  edge [
    source 6
    target 847
  ]
  edge [
    source 6
    target 848
  ]
  edge [
    source 6
    target 849
  ]
  edge [
    source 6
    target 850
  ]
  edge [
    source 6
    target 851
  ]
  edge [
    source 6
    target 852
  ]
  edge [
    source 6
    target 853
  ]
  edge [
    source 6
    target 854
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 855
  ]
  edge [
    source 6
    target 856
  ]
  edge [
    source 6
    target 857
  ]
  edge [
    source 6
    target 858
  ]
  edge [
    source 6
    target 859
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 860
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 861
  ]
  edge [
    source 6
    target 862
  ]
  edge [
    source 6
    target 863
  ]
  edge [
    source 6
    target 864
  ]
  edge [
    source 6
    target 865
  ]
  edge [
    source 6
    target 866
  ]
  edge [
    source 6
    target 867
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 868
  ]
  edge [
    source 6
    target 869
  ]
  edge [
    source 6
    target 870
  ]
  edge [
    source 6
    target 871
  ]
  edge [
    source 6
    target 872
  ]
  edge [
    source 6
    target 873
  ]
  edge [
    source 6
    target 874
  ]
  edge [
    source 6
    target 875
  ]
  edge [
    source 6
    target 876
  ]
  edge [
    source 6
    target 877
  ]
  edge [
    source 6
    target 878
  ]
  edge [
    source 6
    target 879
  ]
  edge [
    source 6
    target 880
  ]
  edge [
    source 6
    target 881
  ]
  edge [
    source 6
    target 882
  ]
  edge [
    source 6
    target 883
  ]
  edge [
    source 6
    target 884
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 6
    target 892
  ]
  edge [
    source 6
    target 893
  ]
  edge [
    source 6
    target 894
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 896
  ]
  edge [
    source 6
    target 897
  ]
  edge [
    source 6
    target 898
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 904
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 918
  ]
  edge [
    source 6
    target 919
  ]
  edge [
    source 6
    target 920
  ]
  edge [
    source 6
    target 921
  ]
  edge [
    source 6
    target 922
  ]
  edge [
    source 6
    target 923
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 925
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 926
  ]
  edge [
    source 7
    target 927
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 928
  ]
  edge [
    source 8
    target 929
  ]
  edge [
    source 8
    target 930
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 931
  ]
  edge [
    source 8
    target 932
  ]
  edge [
    source 8
    target 933
  ]
  edge [
    source 8
    target 934
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 935
  ]
  edge [
    source 8
    target 936
  ]
  edge [
    source 8
    target 937
  ]
  edge [
    source 8
    target 938
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 939
  ]
  edge [
    source 8
    target 940
  ]
  edge [
    source 8
    target 941
  ]
  edge [
    source 8
    target 942
  ]
  edge [
    source 8
    target 943
  ]
  edge [
    source 8
    target 944
  ]
  edge [
    source 8
    target 945
  ]
  edge [
    source 8
    target 946
  ]
  edge [
    source 8
    target 947
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 948
  ]
  edge [
    source 8
    target 949
  ]
  edge [
    source 8
    target 950
  ]
  edge [
    source 8
    target 951
  ]
  edge [
    source 8
    target 952
  ]
  edge [
    source 8
    target 953
  ]
  edge [
    source 8
    target 954
  ]
  edge [
    source 8
    target 955
  ]
  edge [
    source 8
    target 956
  ]
  edge [
    source 8
    target 957
  ]
  edge [
    source 8
    target 958
  ]
  edge [
    source 8
    target 959
  ]
  edge [
    source 8
    target 960
  ]
  edge [
    source 8
    target 961
  ]
  edge [
    source 8
    target 920
  ]
  edge [
    source 8
    target 962
  ]
  edge [
    source 8
    target 963
  ]
  edge [
    source 8
    target 964
  ]
  edge [
    source 8
    target 965
  ]
  edge [
    source 8
    target 966
  ]
  edge [
    source 8
    target 967
  ]
  edge [
    source 8
    target 968
  ]
  edge [
    source 8
    target 969
  ]
  edge [
    source 8
    target 970
  ]
  edge [
    source 8
    target 971
  ]
  edge [
    source 8
    target 972
  ]
  edge [
    source 8
    target 973
  ]
  edge [
    source 8
    target 974
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 975
  ]
  edge [
    source 8
    target 976
  ]
  edge [
    source 8
    target 977
  ]
  edge [
    source 8
    target 978
  ]
  edge [
    source 8
    target 979
  ]
  edge [
    source 8
    target 980
  ]
  edge [
    source 8
    target 981
  ]
  edge [
    source 8
    target 982
  ]
  edge [
    source 8
    target 983
  ]
  edge [
    source 8
    target 984
  ]
  edge [
    source 8
    target 985
  ]
  edge [
    source 8
    target 986
  ]
  edge [
    source 8
    target 987
  ]
  edge [
    source 8
    target 988
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 989
  ]
  edge [
    source 8
    target 990
  ]
  edge [
    source 8
    target 991
  ]
  edge [
    source 8
    target 992
  ]
  edge [
    source 8
    target 993
  ]
  edge [
    source 8
    target 994
  ]
  edge [
    source 8
    target 995
  ]
  edge [
    source 8
    target 996
  ]
  edge [
    source 8
    target 997
  ]
  edge [
    source 8
    target 998
  ]
  edge [
    source 8
    target 999
  ]
  edge [
    source 8
    target 1000
  ]
  edge [
    source 8
    target 1001
  ]
  edge [
    source 8
    target 1002
  ]
  edge [
    source 8
    target 1003
  ]
  edge [
    source 8
    target 1004
  ]
  edge [
    source 8
    target 1005
  ]
  edge [
    source 8
    target 1006
  ]
  edge [
    source 8
    target 1007
  ]
  edge [
    source 8
    target 1008
  ]
  edge [
    source 8
    target 1009
  ]
  edge [
    source 8
    target 1010
  ]
  edge [
    source 8
    target 1011
  ]
  edge [
    source 8
    target 1012
  ]
  edge [
    source 8
    target 1013
  ]
  edge [
    source 8
    target 1014
  ]
  edge [
    source 8
    target 1015
  ]
  edge [
    source 8
    target 1016
  ]
  edge [
    source 8
    target 1017
  ]
  edge [
    source 8
    target 1018
  ]
  edge [
    source 8
    target 1019
  ]
  edge [
    source 8
    target 1020
  ]
  edge [
    source 8
    target 1021
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 1022
  ]
  edge [
    source 8
    target 1023
  ]
  edge [
    source 8
    target 1024
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 1025
  ]
  edge [
    source 9
    target 1026
  ]
  edge [
    source 9
    target 1027
  ]
  edge [
    source 9
    target 932
  ]
  edge [
    source 9
    target 1028
  ]
  edge [
    source 9
    target 1029
  ]
  edge [
    source 9
    target 1030
  ]
  edge [
    source 9
    target 1031
  ]
  edge [
    source 9
    target 1032
  ]
  edge [
    source 9
    target 1033
  ]
  edge [
    source 9
    target 1034
  ]
  edge [
    source 9
    target 1035
  ]
  edge [
    source 9
    target 1036
  ]
  edge [
    source 9
    target 1037
  ]
  edge [
    source 9
    target 1038
  ]
  edge [
    source 9
    target 1039
  ]
  edge [
    source 9
    target 1040
  ]
  edge [
    source 9
    target 1041
  ]
  edge [
    source 9
    target 1042
  ]
  edge [
    source 9
    target 1043
  ]
  edge [
    source 9
    target 1044
  ]
  edge [
    source 9
    target 1045
  ]
  edge [
    source 9
    target 1046
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1047
  ]
  edge [
    source 11
    target 1048
  ]
  edge [
    source 11
    target 1049
  ]
  edge [
    source 11
    target 1050
  ]
  edge [
    source 11
    target 1051
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 1052
  ]
  edge [
    source 11
    target 24
  ]
  edge [
    source 11
    target 1053
  ]
  edge [
    source 11
    target 1054
  ]
  edge [
    source 11
    target 1055
  ]
  edge [
    source 11
    target 1056
  ]
  edge [
    source 11
    target 1057
  ]
  edge [
    source 11
    target 1058
  ]
  edge [
    source 11
    target 1059
  ]
  edge [
    source 11
    target 1060
  ]
  edge [
    source 11
    target 1061
  ]
  edge [
    source 11
    target 1062
  ]
  edge [
    source 11
    target 1063
  ]
  edge [
    source 11
    target 1064
  ]
  edge [
    source 11
    target 1065
  ]
  edge [
    source 11
    target 1066
  ]
  edge [
    source 11
    target 1067
  ]
  edge [
    source 11
    target 1068
  ]
  edge [
    source 11
    target 1069
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 1155
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1070
  ]
  edge [
    source 13
    target 1071
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 424
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 242
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 889
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 371
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 641
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 778
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 351
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 1134
  ]
  edge [
    source 14
    target 341
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 1135
  ]
  edge [
    source 14
    target 345
  ]
  edge [
    source 14
    target 346
  ]
  edge [
    source 14
    target 347
  ]
  edge [
    source 14
    target 348
  ]
  edge [
    source 14
    target 349
  ]
  edge [
    source 14
    target 350
  ]
  edge [
    source 14
    target 352
  ]
  edge [
    source 14
    target 353
  ]
  edge [
    source 14
    target 354
  ]
  edge [
    source 14
    target 355
  ]
  edge [
    source 14
    target 356
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 1139
  ]
  edge [
    source 14
    target 1140
  ]
  edge [
    source 14
    target 1141
  ]
  edge [
    source 14
    target 1142
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1144
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1146
  ]
  edge [
    source 15
    target 1147
  ]
  edge [
    source 15
    target 1148
  ]
  edge [
    source 15
    target 1149
  ]
  edge [
    source 15
    target 1150
  ]
  edge [
    source 15
    target 1151
  ]
  edge [
    source 15
    target 1152
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 1153
  ]
  edge [
    source 17
    target 1154
  ]
]
