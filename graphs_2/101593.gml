graph [
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "j&#243;zef"
    origin "text"
  ]
  node [
    id 2
    label "rojek"
    origin "text"
  ]
  node [
    id 3
    label "klub"
    origin "text"
  ]
  node [
    id 4
    label "parlamentarny"
    origin "text"
  ]
  node [
    id 5
    label "prawo"
    origin "text"
  ]
  node [
    id 6
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ablegat"
  ]
  node [
    id 8
    label "izba_ni&#380;sza"
  ]
  node [
    id 9
    label "Korwin"
  ]
  node [
    id 10
    label "dyscyplina_partyjna"
  ]
  node [
    id 11
    label "Miko&#322;ajczyk"
  ]
  node [
    id 12
    label "kurier_dyplomatyczny"
  ]
  node [
    id 13
    label "wys&#322;annik"
  ]
  node [
    id 14
    label "poselstwo"
  ]
  node [
    id 15
    label "parlamentarzysta"
  ]
  node [
    id 16
    label "przedstawiciel"
  ]
  node [
    id 17
    label "dyplomata"
  ]
  node [
    id 18
    label "klubista"
  ]
  node [
    id 19
    label "reprezentacja"
  ]
  node [
    id 20
    label "cz&#322;onek"
  ]
  node [
    id 21
    label "mandatariusz"
  ]
  node [
    id 22
    label "grupa_bilateralna"
  ]
  node [
    id 23
    label "polityk"
  ]
  node [
    id 24
    label "parlament"
  ]
  node [
    id 25
    label "mi&#322;y"
  ]
  node [
    id 26
    label "cz&#322;owiek"
  ]
  node [
    id 27
    label "korpus_dyplomatyczny"
  ]
  node [
    id 28
    label "dyplomatyczny"
  ]
  node [
    id 29
    label "takt"
  ]
  node [
    id 30
    label "Metternich"
  ]
  node [
    id 31
    label "dostojnik"
  ]
  node [
    id 32
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 33
    label "przyk&#322;ad"
  ]
  node [
    id 34
    label "substytuowa&#263;"
  ]
  node [
    id 35
    label "substytuowanie"
  ]
  node [
    id 36
    label "zast&#281;pca"
  ]
  node [
    id 37
    label "wir"
  ]
  node [
    id 38
    label "pr&#261;d"
  ]
  node [
    id 39
    label "nawa&#322;"
  ]
  node [
    id 40
    label "nurt"
  ]
  node [
    id 41
    label "whirl"
  ]
  node [
    id 42
    label "obr&#243;t"
  ]
  node [
    id 43
    label "zjawisko"
  ]
  node [
    id 44
    label "kr&#261;&#380;el"
  ]
  node [
    id 45
    label "od&#322;am"
  ]
  node [
    id 46
    label "siedziba"
  ]
  node [
    id 47
    label "society"
  ]
  node [
    id 48
    label "bar"
  ]
  node [
    id 49
    label "jakobini"
  ]
  node [
    id 50
    label "lokal"
  ]
  node [
    id 51
    label "stowarzyszenie"
  ]
  node [
    id 52
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 53
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 54
    label "Chewra_Kadisza"
  ]
  node [
    id 55
    label "organizacja"
  ]
  node [
    id 56
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 57
    label "fabianie"
  ]
  node [
    id 58
    label "Rotary_International"
  ]
  node [
    id 59
    label "Eleusis"
  ]
  node [
    id 60
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 61
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 62
    label "Monar"
  ]
  node [
    id 63
    label "grupa"
  ]
  node [
    id 64
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 65
    label "&#321;ubianka"
  ]
  node [
    id 66
    label "miejsce_pracy"
  ]
  node [
    id 67
    label "dzia&#322;_personalny"
  ]
  node [
    id 68
    label "Kreml"
  ]
  node [
    id 69
    label "Bia&#322;y_Dom"
  ]
  node [
    id 70
    label "budynek"
  ]
  node [
    id 71
    label "miejsce"
  ]
  node [
    id 72
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 73
    label "sadowisko"
  ]
  node [
    id 74
    label "kawa&#322;"
  ]
  node [
    id 75
    label "bry&#322;a"
  ]
  node [
    id 76
    label "fragment"
  ]
  node [
    id 77
    label "struktura_geologiczna"
  ]
  node [
    id 78
    label "dzia&#322;"
  ]
  node [
    id 79
    label "section"
  ]
  node [
    id 80
    label "gastronomia"
  ]
  node [
    id 81
    label "zak&#322;ad"
  ]
  node [
    id 82
    label "lada"
  ]
  node [
    id 83
    label "blat"
  ]
  node [
    id 84
    label "berylowiec"
  ]
  node [
    id 85
    label "milibar"
  ]
  node [
    id 86
    label "kawiarnia"
  ]
  node [
    id 87
    label "buffet"
  ]
  node [
    id 88
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 89
    label "mikrobar"
  ]
  node [
    id 90
    label "demokratyczny"
  ]
  node [
    id 91
    label "parlamentarnie"
  ]
  node [
    id 92
    label "parlamentowy"
  ]
  node [
    id 93
    label "stosowny"
  ]
  node [
    id 94
    label "demokratycznie"
  ]
  node [
    id 95
    label "uczciwy"
  ]
  node [
    id 96
    label "zdemokratyzowanie_si&#281;"
  ]
  node [
    id 97
    label "demokratyzowanie"
  ]
  node [
    id 98
    label "zdemokratyzowanie"
  ]
  node [
    id 99
    label "demokratyzowanie_si&#281;"
  ]
  node [
    id 100
    label "post&#281;powy"
  ]
  node [
    id 101
    label "nale&#380;yty"
  ]
  node [
    id 102
    label "stosownie"
  ]
  node [
    id 103
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 104
    label "umocowa&#263;"
  ]
  node [
    id 105
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 106
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 107
    label "procesualistyka"
  ]
  node [
    id 108
    label "regu&#322;a_Allena"
  ]
  node [
    id 109
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 110
    label "kryminalistyka"
  ]
  node [
    id 111
    label "struktura"
  ]
  node [
    id 112
    label "szko&#322;a"
  ]
  node [
    id 113
    label "kierunek"
  ]
  node [
    id 114
    label "zasada_d'Alemberta"
  ]
  node [
    id 115
    label "obserwacja"
  ]
  node [
    id 116
    label "normatywizm"
  ]
  node [
    id 117
    label "jurisprudence"
  ]
  node [
    id 118
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 119
    label "kultura_duchowa"
  ]
  node [
    id 120
    label "przepis"
  ]
  node [
    id 121
    label "prawo_karne_procesowe"
  ]
  node [
    id 122
    label "criterion"
  ]
  node [
    id 123
    label "kazuistyka"
  ]
  node [
    id 124
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 125
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 126
    label "kryminologia"
  ]
  node [
    id 127
    label "opis"
  ]
  node [
    id 128
    label "regu&#322;a_Glogera"
  ]
  node [
    id 129
    label "prawo_Mendla"
  ]
  node [
    id 130
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 131
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 132
    label "prawo_karne"
  ]
  node [
    id 133
    label "legislacyjnie"
  ]
  node [
    id 134
    label "twierdzenie"
  ]
  node [
    id 135
    label "cywilistyka"
  ]
  node [
    id 136
    label "judykatura"
  ]
  node [
    id 137
    label "kanonistyka"
  ]
  node [
    id 138
    label "standard"
  ]
  node [
    id 139
    label "nauka_prawa"
  ]
  node [
    id 140
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 141
    label "podmiot"
  ]
  node [
    id 142
    label "law"
  ]
  node [
    id 143
    label "qualification"
  ]
  node [
    id 144
    label "dominion"
  ]
  node [
    id 145
    label "wykonawczy"
  ]
  node [
    id 146
    label "zasada"
  ]
  node [
    id 147
    label "normalizacja"
  ]
  node [
    id 148
    label "wypowied&#378;"
  ]
  node [
    id 149
    label "exposition"
  ]
  node [
    id 150
    label "czynno&#347;&#263;"
  ]
  node [
    id 151
    label "obja&#347;nienie"
  ]
  node [
    id 152
    label "model"
  ]
  node [
    id 153
    label "organizowa&#263;"
  ]
  node [
    id 154
    label "ordinariness"
  ]
  node [
    id 155
    label "instytucja"
  ]
  node [
    id 156
    label "zorganizowa&#263;"
  ]
  node [
    id 157
    label "taniec_towarzyski"
  ]
  node [
    id 158
    label "organizowanie"
  ]
  node [
    id 159
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 160
    label "zorganizowanie"
  ]
  node [
    id 161
    label "mechanika"
  ]
  node [
    id 162
    label "o&#347;"
  ]
  node [
    id 163
    label "usenet"
  ]
  node [
    id 164
    label "rozprz&#261;c"
  ]
  node [
    id 165
    label "zachowanie"
  ]
  node [
    id 166
    label "cybernetyk"
  ]
  node [
    id 167
    label "podsystem"
  ]
  node [
    id 168
    label "system"
  ]
  node [
    id 169
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 170
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 171
    label "sk&#322;ad"
  ]
  node [
    id 172
    label "systemat"
  ]
  node [
    id 173
    label "cecha"
  ]
  node [
    id 174
    label "konstrukcja"
  ]
  node [
    id 175
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 176
    label "konstelacja"
  ]
  node [
    id 177
    label "przebieg"
  ]
  node [
    id 178
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 179
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 180
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 181
    label "praktyka"
  ]
  node [
    id 182
    label "przeorientowywanie"
  ]
  node [
    id 183
    label "studia"
  ]
  node [
    id 184
    label "linia"
  ]
  node [
    id 185
    label "bok"
  ]
  node [
    id 186
    label "skr&#281;canie"
  ]
  node [
    id 187
    label "skr&#281;ca&#263;"
  ]
  node [
    id 188
    label "przeorientowywa&#263;"
  ]
  node [
    id 189
    label "orientowanie"
  ]
  node [
    id 190
    label "skr&#281;ci&#263;"
  ]
  node [
    id 191
    label "przeorientowanie"
  ]
  node [
    id 192
    label "zorientowanie"
  ]
  node [
    id 193
    label "przeorientowa&#263;"
  ]
  node [
    id 194
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 195
    label "metoda"
  ]
  node [
    id 196
    label "ty&#322;"
  ]
  node [
    id 197
    label "zorientowa&#263;"
  ]
  node [
    id 198
    label "g&#243;ra"
  ]
  node [
    id 199
    label "orientowa&#263;"
  ]
  node [
    id 200
    label "spos&#243;b"
  ]
  node [
    id 201
    label "ideologia"
  ]
  node [
    id 202
    label "orientacja"
  ]
  node [
    id 203
    label "prz&#243;d"
  ]
  node [
    id 204
    label "bearing"
  ]
  node [
    id 205
    label "skr&#281;cenie"
  ]
  node [
    id 206
    label "do&#347;wiadczenie"
  ]
  node [
    id 207
    label "teren_szko&#322;y"
  ]
  node [
    id 208
    label "wiedza"
  ]
  node [
    id 209
    label "Mickiewicz"
  ]
  node [
    id 210
    label "kwalifikacje"
  ]
  node [
    id 211
    label "podr&#281;cznik"
  ]
  node [
    id 212
    label "absolwent"
  ]
  node [
    id 213
    label "school"
  ]
  node [
    id 214
    label "zda&#263;"
  ]
  node [
    id 215
    label "gabinet"
  ]
  node [
    id 216
    label "urszulanki"
  ]
  node [
    id 217
    label "sztuba"
  ]
  node [
    id 218
    label "&#322;awa_szkolna"
  ]
  node [
    id 219
    label "nauka"
  ]
  node [
    id 220
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 221
    label "przepisa&#263;"
  ]
  node [
    id 222
    label "muzyka"
  ]
  node [
    id 223
    label "form"
  ]
  node [
    id 224
    label "klasa"
  ]
  node [
    id 225
    label "lekcja"
  ]
  node [
    id 226
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 227
    label "przepisanie"
  ]
  node [
    id 228
    label "czas"
  ]
  node [
    id 229
    label "skolaryzacja"
  ]
  node [
    id 230
    label "zdanie"
  ]
  node [
    id 231
    label "stopek"
  ]
  node [
    id 232
    label "sekretariat"
  ]
  node [
    id 233
    label "lesson"
  ]
  node [
    id 234
    label "niepokalanki"
  ]
  node [
    id 235
    label "szkolenie"
  ]
  node [
    id 236
    label "kara"
  ]
  node [
    id 237
    label "tablica"
  ]
  node [
    id 238
    label "posiada&#263;"
  ]
  node [
    id 239
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 240
    label "wydarzenie"
  ]
  node [
    id 241
    label "egzekutywa"
  ]
  node [
    id 242
    label "potencja&#322;"
  ]
  node [
    id 243
    label "wyb&#243;r"
  ]
  node [
    id 244
    label "prospect"
  ]
  node [
    id 245
    label "ability"
  ]
  node [
    id 246
    label "obliczeniowo"
  ]
  node [
    id 247
    label "alternatywa"
  ]
  node [
    id 248
    label "operator_modalny"
  ]
  node [
    id 249
    label "badanie"
  ]
  node [
    id 250
    label "proces_my&#347;lowy"
  ]
  node [
    id 251
    label "remark"
  ]
  node [
    id 252
    label "stwierdzenie"
  ]
  node [
    id 253
    label "observation"
  ]
  node [
    id 254
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 255
    label "alternatywa_Fredholma"
  ]
  node [
    id 256
    label "oznajmianie"
  ]
  node [
    id 257
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 258
    label "teoria"
  ]
  node [
    id 259
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 260
    label "paradoks_Leontiefa"
  ]
  node [
    id 261
    label "s&#261;d"
  ]
  node [
    id 262
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 263
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 264
    label "teza"
  ]
  node [
    id 265
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 266
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 267
    label "twierdzenie_Pettisa"
  ]
  node [
    id 268
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 269
    label "twierdzenie_Maya"
  ]
  node [
    id 270
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 271
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 272
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 273
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 274
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 275
    label "zapewnianie"
  ]
  node [
    id 276
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 277
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 278
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 279
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 280
    label "twierdzenie_Stokesa"
  ]
  node [
    id 281
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 282
    label "twierdzenie_Cevy"
  ]
  node [
    id 283
    label "twierdzenie_Pascala"
  ]
  node [
    id 284
    label "proposition"
  ]
  node [
    id 285
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 286
    label "komunikowanie"
  ]
  node [
    id 287
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 288
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 289
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 290
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 291
    label "relacja"
  ]
  node [
    id 292
    label "calibration"
  ]
  node [
    id 293
    label "operacja"
  ]
  node [
    id 294
    label "proces"
  ]
  node [
    id 295
    label "porz&#261;dek"
  ]
  node [
    id 296
    label "dominance"
  ]
  node [
    id 297
    label "zabieg"
  ]
  node [
    id 298
    label "standardization"
  ]
  node [
    id 299
    label "zmiana"
  ]
  node [
    id 300
    label "orzecznictwo"
  ]
  node [
    id 301
    label "wykonawczo"
  ]
  node [
    id 302
    label "byt"
  ]
  node [
    id 303
    label "osobowo&#347;&#263;"
  ]
  node [
    id 304
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 305
    label "set"
  ]
  node [
    id 306
    label "nada&#263;"
  ]
  node [
    id 307
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 308
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 309
    label "cook"
  ]
  node [
    id 310
    label "status"
  ]
  node [
    id 311
    label "procedura"
  ]
  node [
    id 312
    label "norma_prawna"
  ]
  node [
    id 313
    label "przedawnienie_si&#281;"
  ]
  node [
    id 314
    label "przedawnianie_si&#281;"
  ]
  node [
    id 315
    label "porada"
  ]
  node [
    id 316
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 317
    label "regulation"
  ]
  node [
    id 318
    label "recepta"
  ]
  node [
    id 319
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 320
    label "kodeks"
  ]
  node [
    id 321
    label "base"
  ]
  node [
    id 322
    label "umowa"
  ]
  node [
    id 323
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 324
    label "moralno&#347;&#263;"
  ]
  node [
    id 325
    label "occupation"
  ]
  node [
    id 326
    label "podstawa"
  ]
  node [
    id 327
    label "substancja"
  ]
  node [
    id 328
    label "prawid&#322;o"
  ]
  node [
    id 329
    label "casuistry"
  ]
  node [
    id 330
    label "manipulacja"
  ]
  node [
    id 331
    label "probabilizm"
  ]
  node [
    id 332
    label "dermatoglifika"
  ]
  node [
    id 333
    label "mikro&#347;lad"
  ]
  node [
    id 334
    label "technika_&#347;ledcza"
  ]
  node [
    id 335
    label "nemezis"
  ]
  node [
    id 336
    label "konsekwencja"
  ]
  node [
    id 337
    label "punishment"
  ]
  node [
    id 338
    label "righteousness"
  ]
  node [
    id 339
    label "roboty_przymusowe"
  ]
  node [
    id 340
    label "odczuwa&#263;"
  ]
  node [
    id 341
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 342
    label "skrupienie_si&#281;"
  ]
  node [
    id 343
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 344
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 345
    label "odczucie"
  ]
  node [
    id 346
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 347
    label "koszula_Dejaniry"
  ]
  node [
    id 348
    label "odczuwanie"
  ]
  node [
    id 349
    label "event"
  ]
  node [
    id 350
    label "rezultat"
  ]
  node [
    id 351
    label "skrupianie_si&#281;"
  ]
  node [
    id 352
    label "odczu&#263;"
  ]
  node [
    id 353
    label "charakterystyka"
  ]
  node [
    id 354
    label "m&#322;ot"
  ]
  node [
    id 355
    label "znak"
  ]
  node [
    id 356
    label "drzewo"
  ]
  node [
    id 357
    label "pr&#243;ba"
  ]
  node [
    id 358
    label "attribute"
  ]
  node [
    id 359
    label "marka"
  ]
  node [
    id 360
    label "J&#243;zefa"
  ]
  node [
    id 361
    label "i"
  ]
  node [
    id 362
    label "platforma"
  ]
  node [
    id 363
    label "obywatelski"
  ]
  node [
    id 364
    label "afera"
  ]
  node [
    id 365
    label "hazardowy"
  ]
  node [
    id 366
    label "komisja"
  ]
  node [
    id 367
    label "nadzwyczajny"
  ]
  node [
    id 368
    label "&#8222;"
  ]
  node [
    id 369
    label "przyjazny"
  ]
  node [
    id 370
    label "pa&#324;stwo"
  ]
  node [
    id 371
    label "&#8221;"
  ]
  node [
    id 372
    label "TVP"
  ]
  node [
    id 373
    label "info"
  ]
  node [
    id 374
    label "Andrzej"
  ]
  node [
    id 375
    label "szlachta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 362
    target 363
  ]
  edge [
    source 364
    target 365
  ]
  edge [
    source 366
    target 367
  ]
  edge [
    source 366
    target 368
  ]
  edge [
    source 366
    target 369
  ]
  edge [
    source 366
    target 370
  ]
  edge [
    source 366
    target 371
  ]
  edge [
    source 367
    target 368
  ]
  edge [
    source 367
    target 369
  ]
  edge [
    source 367
    target 370
  ]
  edge [
    source 367
    target 371
  ]
  edge [
    source 368
    target 369
  ]
  edge [
    source 368
    target 370
  ]
  edge [
    source 368
    target 371
  ]
  edge [
    source 369
    target 370
  ]
  edge [
    source 369
    target 371
  ]
  edge [
    source 370
    target 371
  ]
  edge [
    source 372
    target 373
  ]
  edge [
    source 374
    target 375
  ]
]
