graph [
  node [
    id 0
    label "kiedy"
    origin "text"
  ]
  node [
    id 1
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "co&#347;"
    origin "text"
  ]
  node [
    id 3
    label "kraj"
    origin "text"
  ]
  node [
    id 4
    label "zareklamowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "heheszki"
    origin "text"
  ]
  node [
    id 6
    label "takaprawda"
    origin "text"
  ]
  node [
    id 7
    label "stara&#263;_si&#281;"
  ]
  node [
    id 8
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 9
    label "sprawdza&#263;"
  ]
  node [
    id 10
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 11
    label "feel"
  ]
  node [
    id 12
    label "try"
  ]
  node [
    id 13
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 14
    label "przedstawienie"
  ]
  node [
    id 15
    label "kosztowa&#263;"
  ]
  node [
    id 16
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 17
    label "examine"
  ]
  node [
    id 18
    label "robi&#263;"
  ]
  node [
    id 19
    label "szpiegowa&#263;"
  ]
  node [
    id 20
    label "konsumowa&#263;"
  ]
  node [
    id 21
    label "by&#263;"
  ]
  node [
    id 22
    label "savor"
  ]
  node [
    id 23
    label "cena"
  ]
  node [
    id 24
    label "doznawa&#263;"
  ]
  node [
    id 25
    label "essay"
  ]
  node [
    id 26
    label "pr&#243;bowanie"
  ]
  node [
    id 27
    label "zademonstrowanie"
  ]
  node [
    id 28
    label "report"
  ]
  node [
    id 29
    label "obgadanie"
  ]
  node [
    id 30
    label "realizacja"
  ]
  node [
    id 31
    label "scena"
  ]
  node [
    id 32
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 33
    label "narration"
  ]
  node [
    id 34
    label "cyrk"
  ]
  node [
    id 35
    label "wytw&#243;r"
  ]
  node [
    id 36
    label "posta&#263;"
  ]
  node [
    id 37
    label "theatrical_performance"
  ]
  node [
    id 38
    label "opisanie"
  ]
  node [
    id 39
    label "malarstwo"
  ]
  node [
    id 40
    label "scenografia"
  ]
  node [
    id 41
    label "teatr"
  ]
  node [
    id 42
    label "ukazanie"
  ]
  node [
    id 43
    label "zapoznanie"
  ]
  node [
    id 44
    label "pokaz"
  ]
  node [
    id 45
    label "podanie"
  ]
  node [
    id 46
    label "spos&#243;b"
  ]
  node [
    id 47
    label "ods&#322;ona"
  ]
  node [
    id 48
    label "exhibit"
  ]
  node [
    id 49
    label "pokazanie"
  ]
  node [
    id 50
    label "wyst&#261;pienie"
  ]
  node [
    id 51
    label "przedstawi&#263;"
  ]
  node [
    id 52
    label "przedstawianie"
  ]
  node [
    id 53
    label "przedstawia&#263;"
  ]
  node [
    id 54
    label "rola"
  ]
  node [
    id 55
    label "thing"
  ]
  node [
    id 56
    label "cosik"
  ]
  node [
    id 57
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 58
    label "Katar"
  ]
  node [
    id 59
    label "Mazowsze"
  ]
  node [
    id 60
    label "Libia"
  ]
  node [
    id 61
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 62
    label "Gwatemala"
  ]
  node [
    id 63
    label "Anglia"
  ]
  node [
    id 64
    label "Amazonia"
  ]
  node [
    id 65
    label "Ekwador"
  ]
  node [
    id 66
    label "Afganistan"
  ]
  node [
    id 67
    label "Bordeaux"
  ]
  node [
    id 68
    label "Tad&#380;ykistan"
  ]
  node [
    id 69
    label "Bhutan"
  ]
  node [
    id 70
    label "Argentyna"
  ]
  node [
    id 71
    label "D&#380;ibuti"
  ]
  node [
    id 72
    label "Wenezuela"
  ]
  node [
    id 73
    label "Gabon"
  ]
  node [
    id 74
    label "Ukraina"
  ]
  node [
    id 75
    label "Naddniestrze"
  ]
  node [
    id 76
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 77
    label "Europa_Zachodnia"
  ]
  node [
    id 78
    label "Armagnac"
  ]
  node [
    id 79
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 80
    label "Rwanda"
  ]
  node [
    id 81
    label "Liechtenstein"
  ]
  node [
    id 82
    label "Amhara"
  ]
  node [
    id 83
    label "organizacja"
  ]
  node [
    id 84
    label "Sri_Lanka"
  ]
  node [
    id 85
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 86
    label "Zamojszczyzna"
  ]
  node [
    id 87
    label "Madagaskar"
  ]
  node [
    id 88
    label "Kongo"
  ]
  node [
    id 89
    label "Tonga"
  ]
  node [
    id 90
    label "Bangladesz"
  ]
  node [
    id 91
    label "Kanada"
  ]
  node [
    id 92
    label "Turkiestan"
  ]
  node [
    id 93
    label "Wehrlen"
  ]
  node [
    id 94
    label "Ma&#322;opolska"
  ]
  node [
    id 95
    label "Algieria"
  ]
  node [
    id 96
    label "Noworosja"
  ]
  node [
    id 97
    label "Uganda"
  ]
  node [
    id 98
    label "Surinam"
  ]
  node [
    id 99
    label "Sahara_Zachodnia"
  ]
  node [
    id 100
    label "Chile"
  ]
  node [
    id 101
    label "Lubelszczyzna"
  ]
  node [
    id 102
    label "W&#281;gry"
  ]
  node [
    id 103
    label "Mezoameryka"
  ]
  node [
    id 104
    label "Birma"
  ]
  node [
    id 105
    label "Ba&#322;kany"
  ]
  node [
    id 106
    label "Kurdystan"
  ]
  node [
    id 107
    label "Kazachstan"
  ]
  node [
    id 108
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 109
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 110
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 111
    label "Armenia"
  ]
  node [
    id 112
    label "Tuwalu"
  ]
  node [
    id 113
    label "Timor_Wschodni"
  ]
  node [
    id 114
    label "Baszkiria"
  ]
  node [
    id 115
    label "Szkocja"
  ]
  node [
    id 116
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 117
    label "Tonkin"
  ]
  node [
    id 118
    label "Maghreb"
  ]
  node [
    id 119
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 120
    label "Izrael"
  ]
  node [
    id 121
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 122
    label "Nadrenia"
  ]
  node [
    id 123
    label "Estonia"
  ]
  node [
    id 124
    label "Komory"
  ]
  node [
    id 125
    label "Podhale"
  ]
  node [
    id 126
    label "Wielkopolska"
  ]
  node [
    id 127
    label "Zabajkale"
  ]
  node [
    id 128
    label "Kamerun"
  ]
  node [
    id 129
    label "Haiti"
  ]
  node [
    id 130
    label "Belize"
  ]
  node [
    id 131
    label "Sierra_Leone"
  ]
  node [
    id 132
    label "Apulia"
  ]
  node [
    id 133
    label "Luksemburg"
  ]
  node [
    id 134
    label "brzeg"
  ]
  node [
    id 135
    label "USA"
  ]
  node [
    id 136
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 137
    label "Barbados"
  ]
  node [
    id 138
    label "San_Marino"
  ]
  node [
    id 139
    label "Bu&#322;garia"
  ]
  node [
    id 140
    label "Indonezja"
  ]
  node [
    id 141
    label "Wietnam"
  ]
  node [
    id 142
    label "Bojkowszczyzna"
  ]
  node [
    id 143
    label "Malawi"
  ]
  node [
    id 144
    label "Francja"
  ]
  node [
    id 145
    label "Zambia"
  ]
  node [
    id 146
    label "Kujawy"
  ]
  node [
    id 147
    label "Angola"
  ]
  node [
    id 148
    label "Liguria"
  ]
  node [
    id 149
    label "Grenada"
  ]
  node [
    id 150
    label "Pamir"
  ]
  node [
    id 151
    label "Nepal"
  ]
  node [
    id 152
    label "Panama"
  ]
  node [
    id 153
    label "Rumunia"
  ]
  node [
    id 154
    label "Indochiny"
  ]
  node [
    id 155
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 156
    label "Polinezja"
  ]
  node [
    id 157
    label "Kurpie"
  ]
  node [
    id 158
    label "Podlasie"
  ]
  node [
    id 159
    label "S&#261;decczyzna"
  ]
  node [
    id 160
    label "Umbria"
  ]
  node [
    id 161
    label "Czarnog&#243;ra"
  ]
  node [
    id 162
    label "Malediwy"
  ]
  node [
    id 163
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 164
    label "S&#322;owacja"
  ]
  node [
    id 165
    label "Karaiby"
  ]
  node [
    id 166
    label "Ukraina_Zachodnia"
  ]
  node [
    id 167
    label "Kielecczyzna"
  ]
  node [
    id 168
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 169
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 170
    label "Egipt"
  ]
  node [
    id 171
    label "Kalabria"
  ]
  node [
    id 172
    label "Kolumbia"
  ]
  node [
    id 173
    label "Mozambik"
  ]
  node [
    id 174
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 175
    label "Laos"
  ]
  node [
    id 176
    label "Burundi"
  ]
  node [
    id 177
    label "Suazi"
  ]
  node [
    id 178
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 179
    label "Czechy"
  ]
  node [
    id 180
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 181
    label "Wyspy_Marshalla"
  ]
  node [
    id 182
    label "Dominika"
  ]
  node [
    id 183
    label "Trynidad_i_Tobago"
  ]
  node [
    id 184
    label "Syria"
  ]
  node [
    id 185
    label "Palau"
  ]
  node [
    id 186
    label "Skandynawia"
  ]
  node [
    id 187
    label "Gwinea_Bissau"
  ]
  node [
    id 188
    label "Liberia"
  ]
  node [
    id 189
    label "Jamajka"
  ]
  node [
    id 190
    label "Zimbabwe"
  ]
  node [
    id 191
    label "Polska"
  ]
  node [
    id 192
    label "Bory_Tucholskie"
  ]
  node [
    id 193
    label "Huculszczyzna"
  ]
  node [
    id 194
    label "Tyrol"
  ]
  node [
    id 195
    label "Turyngia"
  ]
  node [
    id 196
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 197
    label "Dominikana"
  ]
  node [
    id 198
    label "Senegal"
  ]
  node [
    id 199
    label "Togo"
  ]
  node [
    id 200
    label "Gujana"
  ]
  node [
    id 201
    label "jednostka_administracyjna"
  ]
  node [
    id 202
    label "Albania"
  ]
  node [
    id 203
    label "Zair"
  ]
  node [
    id 204
    label "Meksyk"
  ]
  node [
    id 205
    label "Gruzja"
  ]
  node [
    id 206
    label "Macedonia"
  ]
  node [
    id 207
    label "Kambod&#380;a"
  ]
  node [
    id 208
    label "Chorwacja"
  ]
  node [
    id 209
    label "Monako"
  ]
  node [
    id 210
    label "Mauritius"
  ]
  node [
    id 211
    label "Gwinea"
  ]
  node [
    id 212
    label "Mali"
  ]
  node [
    id 213
    label "Nigeria"
  ]
  node [
    id 214
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 215
    label "Hercegowina"
  ]
  node [
    id 216
    label "Kostaryka"
  ]
  node [
    id 217
    label "Lotaryngia"
  ]
  node [
    id 218
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 219
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 220
    label "Hanower"
  ]
  node [
    id 221
    label "Paragwaj"
  ]
  node [
    id 222
    label "W&#322;ochy"
  ]
  node [
    id 223
    label "Seszele"
  ]
  node [
    id 224
    label "Wyspy_Salomona"
  ]
  node [
    id 225
    label "Hiszpania"
  ]
  node [
    id 226
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 227
    label "Walia"
  ]
  node [
    id 228
    label "Boliwia"
  ]
  node [
    id 229
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 230
    label "Opolskie"
  ]
  node [
    id 231
    label "Kirgistan"
  ]
  node [
    id 232
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 233
    label "Irlandia"
  ]
  node [
    id 234
    label "Kampania"
  ]
  node [
    id 235
    label "Czad"
  ]
  node [
    id 236
    label "Irak"
  ]
  node [
    id 237
    label "Lesoto"
  ]
  node [
    id 238
    label "Malta"
  ]
  node [
    id 239
    label "Andora"
  ]
  node [
    id 240
    label "Sand&#380;ak"
  ]
  node [
    id 241
    label "Chiny"
  ]
  node [
    id 242
    label "Filipiny"
  ]
  node [
    id 243
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 244
    label "Syjon"
  ]
  node [
    id 245
    label "Niemcy"
  ]
  node [
    id 246
    label "Kabylia"
  ]
  node [
    id 247
    label "Lombardia"
  ]
  node [
    id 248
    label "Warmia"
  ]
  node [
    id 249
    label "Nikaragua"
  ]
  node [
    id 250
    label "Pakistan"
  ]
  node [
    id 251
    label "Brazylia"
  ]
  node [
    id 252
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 253
    label "Kaszmir"
  ]
  node [
    id 254
    label "Maroko"
  ]
  node [
    id 255
    label "Portugalia"
  ]
  node [
    id 256
    label "Niger"
  ]
  node [
    id 257
    label "Kenia"
  ]
  node [
    id 258
    label "Botswana"
  ]
  node [
    id 259
    label "Fid&#380;i"
  ]
  node [
    id 260
    label "Tunezja"
  ]
  node [
    id 261
    label "Australia"
  ]
  node [
    id 262
    label "Tajlandia"
  ]
  node [
    id 263
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 264
    label "&#321;&#243;dzkie"
  ]
  node [
    id 265
    label "Kaukaz"
  ]
  node [
    id 266
    label "Burkina_Faso"
  ]
  node [
    id 267
    label "Tanzania"
  ]
  node [
    id 268
    label "Benin"
  ]
  node [
    id 269
    label "Europa_Wschodnia"
  ]
  node [
    id 270
    label "interior"
  ]
  node [
    id 271
    label "Indie"
  ]
  node [
    id 272
    label "&#321;otwa"
  ]
  node [
    id 273
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 274
    label "Biskupizna"
  ]
  node [
    id 275
    label "Kiribati"
  ]
  node [
    id 276
    label "Antigua_i_Barbuda"
  ]
  node [
    id 277
    label "Rodezja"
  ]
  node [
    id 278
    label "Afryka_Wschodnia"
  ]
  node [
    id 279
    label "Cypr"
  ]
  node [
    id 280
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 281
    label "Podkarpacie"
  ]
  node [
    id 282
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 283
    label "obszar"
  ]
  node [
    id 284
    label "Peru"
  ]
  node [
    id 285
    label "Afryka_Zachodnia"
  ]
  node [
    id 286
    label "Toskania"
  ]
  node [
    id 287
    label "Austria"
  ]
  node [
    id 288
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 289
    label "Urugwaj"
  ]
  node [
    id 290
    label "Podbeskidzie"
  ]
  node [
    id 291
    label "Jordania"
  ]
  node [
    id 292
    label "Bo&#347;nia"
  ]
  node [
    id 293
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 294
    label "Grecja"
  ]
  node [
    id 295
    label "Azerbejd&#380;an"
  ]
  node [
    id 296
    label "Oceania"
  ]
  node [
    id 297
    label "Turcja"
  ]
  node [
    id 298
    label "Pomorze_Zachodnie"
  ]
  node [
    id 299
    label "Samoa"
  ]
  node [
    id 300
    label "Powi&#347;le"
  ]
  node [
    id 301
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 302
    label "ziemia"
  ]
  node [
    id 303
    label "Sudan"
  ]
  node [
    id 304
    label "Oman"
  ]
  node [
    id 305
    label "&#321;emkowszczyzna"
  ]
  node [
    id 306
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 307
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 308
    label "Uzbekistan"
  ]
  node [
    id 309
    label "Portoryko"
  ]
  node [
    id 310
    label "Honduras"
  ]
  node [
    id 311
    label "Mongolia"
  ]
  node [
    id 312
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 313
    label "Kaszuby"
  ]
  node [
    id 314
    label "Ko&#322;yma"
  ]
  node [
    id 315
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 316
    label "Szlezwik"
  ]
  node [
    id 317
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 318
    label "Serbia"
  ]
  node [
    id 319
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 320
    label "Tajwan"
  ]
  node [
    id 321
    label "Wielka_Brytania"
  ]
  node [
    id 322
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 323
    label "Liban"
  ]
  node [
    id 324
    label "Japonia"
  ]
  node [
    id 325
    label "Ghana"
  ]
  node [
    id 326
    label "Belgia"
  ]
  node [
    id 327
    label "Bahrajn"
  ]
  node [
    id 328
    label "Mikronezja"
  ]
  node [
    id 329
    label "Etiopia"
  ]
  node [
    id 330
    label "Polesie"
  ]
  node [
    id 331
    label "Kuwejt"
  ]
  node [
    id 332
    label "Kerala"
  ]
  node [
    id 333
    label "Mazury"
  ]
  node [
    id 334
    label "Bahamy"
  ]
  node [
    id 335
    label "Rosja"
  ]
  node [
    id 336
    label "Mo&#322;dawia"
  ]
  node [
    id 337
    label "Palestyna"
  ]
  node [
    id 338
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 339
    label "Lauda"
  ]
  node [
    id 340
    label "Azja_Wschodnia"
  ]
  node [
    id 341
    label "Litwa"
  ]
  node [
    id 342
    label "S&#322;owenia"
  ]
  node [
    id 343
    label "Szwajcaria"
  ]
  node [
    id 344
    label "Erytrea"
  ]
  node [
    id 345
    label "Zakarpacie"
  ]
  node [
    id 346
    label "Arabia_Saudyjska"
  ]
  node [
    id 347
    label "Kuba"
  ]
  node [
    id 348
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 349
    label "Galicja"
  ]
  node [
    id 350
    label "Lubuskie"
  ]
  node [
    id 351
    label "Laponia"
  ]
  node [
    id 352
    label "granica_pa&#324;stwa"
  ]
  node [
    id 353
    label "Malezja"
  ]
  node [
    id 354
    label "Korea"
  ]
  node [
    id 355
    label "Yorkshire"
  ]
  node [
    id 356
    label "Bawaria"
  ]
  node [
    id 357
    label "Zag&#243;rze"
  ]
  node [
    id 358
    label "Jemen"
  ]
  node [
    id 359
    label "Nowa_Zelandia"
  ]
  node [
    id 360
    label "Andaluzja"
  ]
  node [
    id 361
    label "Namibia"
  ]
  node [
    id 362
    label "Nauru"
  ]
  node [
    id 363
    label "&#379;ywiecczyzna"
  ]
  node [
    id 364
    label "Brunei"
  ]
  node [
    id 365
    label "Oksytania"
  ]
  node [
    id 366
    label "Opolszczyzna"
  ]
  node [
    id 367
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 368
    label "Kociewie"
  ]
  node [
    id 369
    label "Khitai"
  ]
  node [
    id 370
    label "Mauretania"
  ]
  node [
    id 371
    label "Iran"
  ]
  node [
    id 372
    label "Gambia"
  ]
  node [
    id 373
    label "Somalia"
  ]
  node [
    id 374
    label "Holandia"
  ]
  node [
    id 375
    label "Lasko"
  ]
  node [
    id 376
    label "Turkmenistan"
  ]
  node [
    id 377
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 378
    label "Salwador"
  ]
  node [
    id 379
    label "woda"
  ]
  node [
    id 380
    label "linia"
  ]
  node [
    id 381
    label "zbi&#243;r"
  ]
  node [
    id 382
    label "ekoton"
  ]
  node [
    id 383
    label "str&#261;d"
  ]
  node [
    id 384
    label "koniec"
  ]
  node [
    id 385
    label "plantowa&#263;"
  ]
  node [
    id 386
    label "zapadnia"
  ]
  node [
    id 387
    label "budynek"
  ]
  node [
    id 388
    label "skorupa_ziemska"
  ]
  node [
    id 389
    label "glinowanie"
  ]
  node [
    id 390
    label "martwica"
  ]
  node [
    id 391
    label "teren"
  ]
  node [
    id 392
    label "litosfera"
  ]
  node [
    id 393
    label "penetrator"
  ]
  node [
    id 394
    label "glinowa&#263;"
  ]
  node [
    id 395
    label "domain"
  ]
  node [
    id 396
    label "podglebie"
  ]
  node [
    id 397
    label "kompleks_sorpcyjny"
  ]
  node [
    id 398
    label "miejsce"
  ]
  node [
    id 399
    label "kort"
  ]
  node [
    id 400
    label "czynnik_produkcji"
  ]
  node [
    id 401
    label "pojazd"
  ]
  node [
    id 402
    label "powierzchnia"
  ]
  node [
    id 403
    label "pr&#243;chnica"
  ]
  node [
    id 404
    label "pomieszczenie"
  ]
  node [
    id 405
    label "ryzosfera"
  ]
  node [
    id 406
    label "p&#322;aszczyzna"
  ]
  node [
    id 407
    label "dotleni&#263;"
  ]
  node [
    id 408
    label "glej"
  ]
  node [
    id 409
    label "pa&#324;stwo"
  ]
  node [
    id 410
    label "posadzka"
  ]
  node [
    id 411
    label "geosystem"
  ]
  node [
    id 412
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 413
    label "przestrze&#324;"
  ]
  node [
    id 414
    label "podmiot"
  ]
  node [
    id 415
    label "jednostka_organizacyjna"
  ]
  node [
    id 416
    label "struktura"
  ]
  node [
    id 417
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 418
    label "TOPR"
  ]
  node [
    id 419
    label "endecki"
  ]
  node [
    id 420
    label "zesp&#243;&#322;"
  ]
  node [
    id 421
    label "od&#322;am"
  ]
  node [
    id 422
    label "przedstawicielstwo"
  ]
  node [
    id 423
    label "Cepelia"
  ]
  node [
    id 424
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 425
    label "ZBoWiD"
  ]
  node [
    id 426
    label "organization"
  ]
  node [
    id 427
    label "centrala"
  ]
  node [
    id 428
    label "GOPR"
  ]
  node [
    id 429
    label "ZOMO"
  ]
  node [
    id 430
    label "ZMP"
  ]
  node [
    id 431
    label "komitet_koordynacyjny"
  ]
  node [
    id 432
    label "przybud&#243;wka"
  ]
  node [
    id 433
    label "boj&#243;wka"
  ]
  node [
    id 434
    label "p&#243;&#322;noc"
  ]
  node [
    id 435
    label "Kosowo"
  ]
  node [
    id 436
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 437
    label "Zab&#322;ocie"
  ]
  node [
    id 438
    label "zach&#243;d"
  ]
  node [
    id 439
    label "po&#322;udnie"
  ]
  node [
    id 440
    label "Pow&#261;zki"
  ]
  node [
    id 441
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 442
    label "Piotrowo"
  ]
  node [
    id 443
    label "Olszanica"
  ]
  node [
    id 444
    label "Ruda_Pabianicka"
  ]
  node [
    id 445
    label "holarktyka"
  ]
  node [
    id 446
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 447
    label "Ludwin&#243;w"
  ]
  node [
    id 448
    label "Arktyka"
  ]
  node [
    id 449
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 450
    label "Zabu&#380;e"
  ]
  node [
    id 451
    label "antroposfera"
  ]
  node [
    id 452
    label "Neogea"
  ]
  node [
    id 453
    label "terytorium"
  ]
  node [
    id 454
    label "Syberia_Zachodnia"
  ]
  node [
    id 455
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 456
    label "zakres"
  ]
  node [
    id 457
    label "pas_planetoid"
  ]
  node [
    id 458
    label "Syberia_Wschodnia"
  ]
  node [
    id 459
    label "Antarktyka"
  ]
  node [
    id 460
    label "Rakowice"
  ]
  node [
    id 461
    label "akrecja"
  ]
  node [
    id 462
    label "wymiar"
  ]
  node [
    id 463
    label "&#321;&#281;g"
  ]
  node [
    id 464
    label "Kresy_Zachodnie"
  ]
  node [
    id 465
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 466
    label "wsch&#243;d"
  ]
  node [
    id 467
    label "Notogea"
  ]
  node [
    id 468
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 469
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 470
    label "Pend&#380;ab"
  ]
  node [
    id 471
    label "funt_liba&#324;ski"
  ]
  node [
    id 472
    label "strefa_euro"
  ]
  node [
    id 473
    label "Pozna&#324;"
  ]
  node [
    id 474
    label "lira_malta&#324;ska"
  ]
  node [
    id 475
    label "Gozo"
  ]
  node [
    id 476
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 477
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 478
    label "dolar_namibijski"
  ]
  node [
    id 479
    label "milrejs"
  ]
  node [
    id 480
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 481
    label "NATO"
  ]
  node [
    id 482
    label "escudo_portugalskie"
  ]
  node [
    id 483
    label "dolar_bahamski"
  ]
  node [
    id 484
    label "Wielka_Bahama"
  ]
  node [
    id 485
    label "dolar_liberyjski"
  ]
  node [
    id 486
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 487
    label "riel"
  ]
  node [
    id 488
    label "Karelia"
  ]
  node [
    id 489
    label "Mari_El"
  ]
  node [
    id 490
    label "Inguszetia"
  ]
  node [
    id 491
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 492
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 493
    label "Udmurcja"
  ]
  node [
    id 494
    label "Newa"
  ]
  node [
    id 495
    label "&#321;adoga"
  ]
  node [
    id 496
    label "Czeczenia"
  ]
  node [
    id 497
    label "Anadyr"
  ]
  node [
    id 498
    label "Syberia"
  ]
  node [
    id 499
    label "Tatarstan"
  ]
  node [
    id 500
    label "Wszechrosja"
  ]
  node [
    id 501
    label "Azja"
  ]
  node [
    id 502
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 503
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 504
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 505
    label "Witim"
  ]
  node [
    id 506
    label "Kamczatka"
  ]
  node [
    id 507
    label "Jama&#322;"
  ]
  node [
    id 508
    label "Dagestan"
  ]
  node [
    id 509
    label "Tuwa"
  ]
  node [
    id 510
    label "car"
  ]
  node [
    id 511
    label "Komi"
  ]
  node [
    id 512
    label "Czuwaszja"
  ]
  node [
    id 513
    label "Chakasja"
  ]
  node [
    id 514
    label "Perm"
  ]
  node [
    id 515
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 516
    label "Ajon"
  ]
  node [
    id 517
    label "Adygeja"
  ]
  node [
    id 518
    label "Dniepr"
  ]
  node [
    id 519
    label "rubel_rosyjski"
  ]
  node [
    id 520
    label "Don"
  ]
  node [
    id 521
    label "Mordowia"
  ]
  node [
    id 522
    label "s&#322;owianofilstwo"
  ]
  node [
    id 523
    label "lew"
  ]
  node [
    id 524
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 525
    label "Dobrud&#380;a"
  ]
  node [
    id 526
    label "Unia_Europejska"
  ]
  node [
    id 527
    label "lira_izraelska"
  ]
  node [
    id 528
    label "szekel"
  ]
  node [
    id 529
    label "Galilea"
  ]
  node [
    id 530
    label "Judea"
  ]
  node [
    id 531
    label "Luksemburgia"
  ]
  node [
    id 532
    label "frank_belgijski"
  ]
  node [
    id 533
    label "Limburgia"
  ]
  node [
    id 534
    label "Walonia"
  ]
  node [
    id 535
    label "Brabancja"
  ]
  node [
    id 536
    label "Flandria"
  ]
  node [
    id 537
    label "Niderlandy"
  ]
  node [
    id 538
    label "dinar_iracki"
  ]
  node [
    id 539
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 540
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 541
    label "szyling_ugandyjski"
  ]
  node [
    id 542
    label "dolar_jamajski"
  ]
  node [
    id 543
    label "kafar"
  ]
  node [
    id 544
    label "ringgit"
  ]
  node [
    id 545
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 546
    label "Borneo"
  ]
  node [
    id 547
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 548
    label "dolar_surinamski"
  ]
  node [
    id 549
    label "funt_suda&#324;ski"
  ]
  node [
    id 550
    label "dolar_guja&#324;ski"
  ]
  node [
    id 551
    label "Manica"
  ]
  node [
    id 552
    label "escudo_mozambickie"
  ]
  node [
    id 553
    label "Cabo_Delgado"
  ]
  node [
    id 554
    label "Inhambane"
  ]
  node [
    id 555
    label "Maputo"
  ]
  node [
    id 556
    label "Gaza"
  ]
  node [
    id 557
    label "Niasa"
  ]
  node [
    id 558
    label "Nampula"
  ]
  node [
    id 559
    label "metical"
  ]
  node [
    id 560
    label "Sahara"
  ]
  node [
    id 561
    label "inti"
  ]
  node [
    id 562
    label "sol"
  ]
  node [
    id 563
    label "kip"
  ]
  node [
    id 564
    label "Pireneje"
  ]
  node [
    id 565
    label "euro"
  ]
  node [
    id 566
    label "kwacha_zambijska"
  ]
  node [
    id 567
    label "tugrik"
  ]
  node [
    id 568
    label "Buriaci"
  ]
  node [
    id 569
    label "ajmak"
  ]
  node [
    id 570
    label "balboa"
  ]
  node [
    id 571
    label "Ameryka_Centralna"
  ]
  node [
    id 572
    label "dolar"
  ]
  node [
    id 573
    label "gulden"
  ]
  node [
    id 574
    label "Zelandia"
  ]
  node [
    id 575
    label "manat_turkme&#324;ski"
  ]
  node [
    id 576
    label "dolar_Tuvalu"
  ]
  node [
    id 577
    label "zair"
  ]
  node [
    id 578
    label "Katanga"
  ]
  node [
    id 579
    label "frank_szwajcarski"
  ]
  node [
    id 580
    label "Jukatan"
  ]
  node [
    id 581
    label "dolar_Belize"
  ]
  node [
    id 582
    label "colon"
  ]
  node [
    id 583
    label "Dyja"
  ]
  node [
    id 584
    label "korona_czeska"
  ]
  node [
    id 585
    label "Izera"
  ]
  node [
    id 586
    label "ugija"
  ]
  node [
    id 587
    label "szyling_kenijski"
  ]
  node [
    id 588
    label "Nachiczewan"
  ]
  node [
    id 589
    label "manat_azerski"
  ]
  node [
    id 590
    label "Karabach"
  ]
  node [
    id 591
    label "Bengal"
  ]
  node [
    id 592
    label "taka"
  ]
  node [
    id 593
    label "Ocean_Spokojny"
  ]
  node [
    id 594
    label "dolar_Kiribati"
  ]
  node [
    id 595
    label "peso_filipi&#324;skie"
  ]
  node [
    id 596
    label "Cebu"
  ]
  node [
    id 597
    label "Atlantyk"
  ]
  node [
    id 598
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 599
    label "Ulster"
  ]
  node [
    id 600
    label "funt_irlandzki"
  ]
  node [
    id 601
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 602
    label "cedi"
  ]
  node [
    id 603
    label "ariary"
  ]
  node [
    id 604
    label "Ocean_Indyjski"
  ]
  node [
    id 605
    label "frank_malgaski"
  ]
  node [
    id 606
    label "Estremadura"
  ]
  node [
    id 607
    label "Kastylia"
  ]
  node [
    id 608
    label "Rzym_Zachodni"
  ]
  node [
    id 609
    label "Aragonia"
  ]
  node [
    id 610
    label "hacjender"
  ]
  node [
    id 611
    label "Asturia"
  ]
  node [
    id 612
    label "Baskonia"
  ]
  node [
    id 613
    label "Majorka"
  ]
  node [
    id 614
    label "Walencja"
  ]
  node [
    id 615
    label "peseta"
  ]
  node [
    id 616
    label "Katalonia"
  ]
  node [
    id 617
    label "peso_chilijskie"
  ]
  node [
    id 618
    label "Indie_Zachodnie"
  ]
  node [
    id 619
    label "Sikkim"
  ]
  node [
    id 620
    label "Asam"
  ]
  node [
    id 621
    label "rupia_indyjska"
  ]
  node [
    id 622
    label "Indie_Portugalskie"
  ]
  node [
    id 623
    label "Indie_Wschodnie"
  ]
  node [
    id 624
    label "Bollywood"
  ]
  node [
    id 625
    label "jen"
  ]
  node [
    id 626
    label "jinja"
  ]
  node [
    id 627
    label "Okinawa"
  ]
  node [
    id 628
    label "Japonica"
  ]
  node [
    id 629
    label "Rugia"
  ]
  node [
    id 630
    label "Saksonia"
  ]
  node [
    id 631
    label "Dolna_Saksonia"
  ]
  node [
    id 632
    label "Anglosas"
  ]
  node [
    id 633
    label "Hesja"
  ]
  node [
    id 634
    label "Wirtembergia"
  ]
  node [
    id 635
    label "Po&#322;abie"
  ]
  node [
    id 636
    label "Germania"
  ]
  node [
    id 637
    label "Frankonia"
  ]
  node [
    id 638
    label "Badenia"
  ]
  node [
    id 639
    label "Holsztyn"
  ]
  node [
    id 640
    label "marka"
  ]
  node [
    id 641
    label "Brandenburgia"
  ]
  node [
    id 642
    label "Szwabia"
  ]
  node [
    id 643
    label "Niemcy_Zachodnie"
  ]
  node [
    id 644
    label "Westfalia"
  ]
  node [
    id 645
    label "Helgoland"
  ]
  node [
    id 646
    label "Karlsbad"
  ]
  node [
    id 647
    label "Niemcy_Wschodnie"
  ]
  node [
    id 648
    label "Piemont"
  ]
  node [
    id 649
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 650
    label "Sardynia"
  ]
  node [
    id 651
    label "Italia"
  ]
  node [
    id 652
    label "Ok&#281;cie"
  ]
  node [
    id 653
    label "Karyntia"
  ]
  node [
    id 654
    label "Romania"
  ]
  node [
    id 655
    label "Warszawa"
  ]
  node [
    id 656
    label "lir"
  ]
  node [
    id 657
    label "Sycylia"
  ]
  node [
    id 658
    label "Dacja"
  ]
  node [
    id 659
    label "lej_rumu&#324;ski"
  ]
  node [
    id 660
    label "Siedmiogr&#243;d"
  ]
  node [
    id 661
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 662
    label "funt_syryjski"
  ]
  node [
    id 663
    label "alawizm"
  ]
  node [
    id 664
    label "frank_rwandyjski"
  ]
  node [
    id 665
    label "dinar_Bahrajnu"
  ]
  node [
    id 666
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 667
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 668
    label "frank_luksemburski"
  ]
  node [
    id 669
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 670
    label "peso_kuba&#324;skie"
  ]
  node [
    id 671
    label "frank_monakijski"
  ]
  node [
    id 672
    label "dinar_algierski"
  ]
  node [
    id 673
    label "Wojwodina"
  ]
  node [
    id 674
    label "dinar_serbski"
  ]
  node [
    id 675
    label "Orinoko"
  ]
  node [
    id 676
    label "boliwar"
  ]
  node [
    id 677
    label "tenge"
  ]
  node [
    id 678
    label "para"
  ]
  node [
    id 679
    label "frank_alba&#324;ski"
  ]
  node [
    id 680
    label "lek"
  ]
  node [
    id 681
    label "dolar_Barbadosu"
  ]
  node [
    id 682
    label "Antyle"
  ]
  node [
    id 683
    label "kyat"
  ]
  node [
    id 684
    label "Arakan"
  ]
  node [
    id 685
    label "c&#243;rdoba"
  ]
  node [
    id 686
    label "Paros"
  ]
  node [
    id 687
    label "Epir"
  ]
  node [
    id 688
    label "panhellenizm"
  ]
  node [
    id 689
    label "Eubea"
  ]
  node [
    id 690
    label "Rodos"
  ]
  node [
    id 691
    label "Achaja"
  ]
  node [
    id 692
    label "Termopile"
  ]
  node [
    id 693
    label "Attyka"
  ]
  node [
    id 694
    label "Hellada"
  ]
  node [
    id 695
    label "Etolia"
  ]
  node [
    id 696
    label "palestra"
  ]
  node [
    id 697
    label "Kreta"
  ]
  node [
    id 698
    label "drachma"
  ]
  node [
    id 699
    label "Olimp"
  ]
  node [
    id 700
    label "Tesalia"
  ]
  node [
    id 701
    label "Peloponez"
  ]
  node [
    id 702
    label "Eolia"
  ]
  node [
    id 703
    label "Beocja"
  ]
  node [
    id 704
    label "Parnas"
  ]
  node [
    id 705
    label "Lesbos"
  ]
  node [
    id 706
    label "Mariany"
  ]
  node [
    id 707
    label "Salzburg"
  ]
  node [
    id 708
    label "Rakuzy"
  ]
  node [
    id 709
    label "konsulent"
  ]
  node [
    id 710
    label "szyling_austryjacki"
  ]
  node [
    id 711
    label "birr"
  ]
  node [
    id 712
    label "negus"
  ]
  node [
    id 713
    label "Jawa"
  ]
  node [
    id 714
    label "Sumatra"
  ]
  node [
    id 715
    label "rupia_indonezyjska"
  ]
  node [
    id 716
    label "Nowa_Gwinea"
  ]
  node [
    id 717
    label "Moluki"
  ]
  node [
    id 718
    label "boliviano"
  ]
  node [
    id 719
    label "Pikardia"
  ]
  node [
    id 720
    label "Masyw_Centralny"
  ]
  node [
    id 721
    label "Akwitania"
  ]
  node [
    id 722
    label "Alzacja"
  ]
  node [
    id 723
    label "Sekwana"
  ]
  node [
    id 724
    label "Langwedocja"
  ]
  node [
    id 725
    label "Martynika"
  ]
  node [
    id 726
    label "Bretania"
  ]
  node [
    id 727
    label "Sabaudia"
  ]
  node [
    id 728
    label "Korsyka"
  ]
  node [
    id 729
    label "Normandia"
  ]
  node [
    id 730
    label "Gaskonia"
  ]
  node [
    id 731
    label "Burgundia"
  ]
  node [
    id 732
    label "frank_francuski"
  ]
  node [
    id 733
    label "Wandea"
  ]
  node [
    id 734
    label "Prowansja"
  ]
  node [
    id 735
    label "Gwadelupa"
  ]
  node [
    id 736
    label "somoni"
  ]
  node [
    id 737
    label "Melanezja"
  ]
  node [
    id 738
    label "dolar_Fid&#380;i"
  ]
  node [
    id 739
    label "funt_cypryjski"
  ]
  node [
    id 740
    label "Afrodyzje"
  ]
  node [
    id 741
    label "peso_dominika&#324;skie"
  ]
  node [
    id 742
    label "Fryburg"
  ]
  node [
    id 743
    label "Bazylea"
  ]
  node [
    id 744
    label "Alpy"
  ]
  node [
    id 745
    label "Helwecja"
  ]
  node [
    id 746
    label "Berno"
  ]
  node [
    id 747
    label "sum"
  ]
  node [
    id 748
    label "Karaka&#322;pacja"
  ]
  node [
    id 749
    label "Windawa"
  ]
  node [
    id 750
    label "&#322;at"
  ]
  node [
    id 751
    label "Kurlandia"
  ]
  node [
    id 752
    label "Liwonia"
  ]
  node [
    id 753
    label "rubel_&#322;otewski"
  ]
  node [
    id 754
    label "Inflanty"
  ]
  node [
    id 755
    label "Wile&#324;szczyzna"
  ]
  node [
    id 756
    label "&#379;mud&#378;"
  ]
  node [
    id 757
    label "lit"
  ]
  node [
    id 758
    label "frank_tunezyjski"
  ]
  node [
    id 759
    label "dinar_tunezyjski"
  ]
  node [
    id 760
    label "lempira"
  ]
  node [
    id 761
    label "korona_w&#281;gierska"
  ]
  node [
    id 762
    label "forint"
  ]
  node [
    id 763
    label "Lipt&#243;w"
  ]
  node [
    id 764
    label "dong"
  ]
  node [
    id 765
    label "Annam"
  ]
  node [
    id 766
    label "lud"
  ]
  node [
    id 767
    label "frank_kongijski"
  ]
  node [
    id 768
    label "szyling_somalijski"
  ]
  node [
    id 769
    label "cruzado"
  ]
  node [
    id 770
    label "real"
  ]
  node [
    id 771
    label "Podole"
  ]
  node [
    id 772
    label "Wsch&#243;d"
  ]
  node [
    id 773
    label "Naddnieprze"
  ]
  node [
    id 774
    label "Ma&#322;orosja"
  ]
  node [
    id 775
    label "Wo&#322;y&#324;"
  ]
  node [
    id 776
    label "Nadbu&#380;e"
  ]
  node [
    id 777
    label "hrywna"
  ]
  node [
    id 778
    label "Zaporo&#380;e"
  ]
  node [
    id 779
    label "Krym"
  ]
  node [
    id 780
    label "Dniestr"
  ]
  node [
    id 781
    label "Przykarpacie"
  ]
  node [
    id 782
    label "Kozaczyzna"
  ]
  node [
    id 783
    label "karbowaniec"
  ]
  node [
    id 784
    label "Tasmania"
  ]
  node [
    id 785
    label "Nowy_&#346;wiat"
  ]
  node [
    id 786
    label "dolar_australijski"
  ]
  node [
    id 787
    label "gourde"
  ]
  node [
    id 788
    label "escudo_angolskie"
  ]
  node [
    id 789
    label "kwanza"
  ]
  node [
    id 790
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 791
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 792
    label "Ad&#380;aria"
  ]
  node [
    id 793
    label "lari"
  ]
  node [
    id 794
    label "naira"
  ]
  node [
    id 795
    label "Ohio"
  ]
  node [
    id 796
    label "P&#243;&#322;noc"
  ]
  node [
    id 797
    label "Nowy_York"
  ]
  node [
    id 798
    label "Illinois"
  ]
  node [
    id 799
    label "Po&#322;udnie"
  ]
  node [
    id 800
    label "Kalifornia"
  ]
  node [
    id 801
    label "Wirginia"
  ]
  node [
    id 802
    label "Teksas"
  ]
  node [
    id 803
    label "Waszyngton"
  ]
  node [
    id 804
    label "zielona_karta"
  ]
  node [
    id 805
    label "Massachusetts"
  ]
  node [
    id 806
    label "Alaska"
  ]
  node [
    id 807
    label "Hawaje"
  ]
  node [
    id 808
    label "Maryland"
  ]
  node [
    id 809
    label "Michigan"
  ]
  node [
    id 810
    label "Arizona"
  ]
  node [
    id 811
    label "Georgia"
  ]
  node [
    id 812
    label "stan_wolny"
  ]
  node [
    id 813
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 814
    label "Pensylwania"
  ]
  node [
    id 815
    label "Luizjana"
  ]
  node [
    id 816
    label "Nowy_Meksyk"
  ]
  node [
    id 817
    label "Wuj_Sam"
  ]
  node [
    id 818
    label "Alabama"
  ]
  node [
    id 819
    label "Kansas"
  ]
  node [
    id 820
    label "Oregon"
  ]
  node [
    id 821
    label "Zach&#243;d"
  ]
  node [
    id 822
    label "Floryda"
  ]
  node [
    id 823
    label "Oklahoma"
  ]
  node [
    id 824
    label "Hudson"
  ]
  node [
    id 825
    label "som"
  ]
  node [
    id 826
    label "peso_urugwajskie"
  ]
  node [
    id 827
    label "denar_macedo&#324;ski"
  ]
  node [
    id 828
    label "dolar_Brunei"
  ]
  node [
    id 829
    label "rial_ira&#324;ski"
  ]
  node [
    id 830
    label "mu&#322;&#322;a"
  ]
  node [
    id 831
    label "Persja"
  ]
  node [
    id 832
    label "d&#380;amahirijja"
  ]
  node [
    id 833
    label "dinar_libijski"
  ]
  node [
    id 834
    label "nakfa"
  ]
  node [
    id 835
    label "rial_katarski"
  ]
  node [
    id 836
    label "quetzal"
  ]
  node [
    id 837
    label "won"
  ]
  node [
    id 838
    label "rial_jeme&#324;ski"
  ]
  node [
    id 839
    label "peso_argenty&#324;skie"
  ]
  node [
    id 840
    label "guarani"
  ]
  node [
    id 841
    label "perper"
  ]
  node [
    id 842
    label "dinar_kuwejcki"
  ]
  node [
    id 843
    label "dalasi"
  ]
  node [
    id 844
    label "dolar_Zimbabwe"
  ]
  node [
    id 845
    label "Szantung"
  ]
  node [
    id 846
    label "Chiny_Zachodnie"
  ]
  node [
    id 847
    label "Kuantung"
  ]
  node [
    id 848
    label "D&#380;ungaria"
  ]
  node [
    id 849
    label "yuan"
  ]
  node [
    id 850
    label "Hongkong"
  ]
  node [
    id 851
    label "Chiny_Wschodnie"
  ]
  node [
    id 852
    label "Guangdong"
  ]
  node [
    id 853
    label "Junnan"
  ]
  node [
    id 854
    label "Mand&#380;uria"
  ]
  node [
    id 855
    label "Syczuan"
  ]
  node [
    id 856
    label "Pa&#322;uki"
  ]
  node [
    id 857
    label "Wolin"
  ]
  node [
    id 858
    label "z&#322;oty"
  ]
  node [
    id 859
    label "So&#322;a"
  ]
  node [
    id 860
    label "Krajna"
  ]
  node [
    id 861
    label "Suwalszczyzna"
  ]
  node [
    id 862
    label "barwy_polskie"
  ]
  node [
    id 863
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 864
    label "Kaczawa"
  ]
  node [
    id 865
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 866
    label "Wis&#322;a"
  ]
  node [
    id 867
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 868
    label "lira_turecka"
  ]
  node [
    id 869
    label "Azja_Mniejsza"
  ]
  node [
    id 870
    label "Ujgur"
  ]
  node [
    id 871
    label "kuna"
  ]
  node [
    id 872
    label "dram"
  ]
  node [
    id 873
    label "tala"
  ]
  node [
    id 874
    label "korona_s&#322;owacka"
  ]
  node [
    id 875
    label "Turiec"
  ]
  node [
    id 876
    label "Himalaje"
  ]
  node [
    id 877
    label "rupia_nepalska"
  ]
  node [
    id 878
    label "frank_gwinejski"
  ]
  node [
    id 879
    label "korona_esto&#324;ska"
  ]
  node [
    id 880
    label "marka_esto&#324;ska"
  ]
  node [
    id 881
    label "Quebec"
  ]
  node [
    id 882
    label "dolar_kanadyjski"
  ]
  node [
    id 883
    label "Nowa_Fundlandia"
  ]
  node [
    id 884
    label "Zanzibar"
  ]
  node [
    id 885
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 886
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 887
    label "&#346;wite&#378;"
  ]
  node [
    id 888
    label "peso_kolumbijskie"
  ]
  node [
    id 889
    label "Synaj"
  ]
  node [
    id 890
    label "paraszyt"
  ]
  node [
    id 891
    label "funt_egipski"
  ]
  node [
    id 892
    label "szach"
  ]
  node [
    id 893
    label "Baktria"
  ]
  node [
    id 894
    label "afgani"
  ]
  node [
    id 895
    label "baht"
  ]
  node [
    id 896
    label "tolar"
  ]
  node [
    id 897
    label "lej_mo&#322;dawski"
  ]
  node [
    id 898
    label "Gagauzja"
  ]
  node [
    id 899
    label "moszaw"
  ]
  node [
    id 900
    label "Kanaan"
  ]
  node [
    id 901
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 902
    label "Jerozolima"
  ]
  node [
    id 903
    label "Beskidy_Zachodnie"
  ]
  node [
    id 904
    label "Wiktoria"
  ]
  node [
    id 905
    label "Guernsey"
  ]
  node [
    id 906
    label "Conrad"
  ]
  node [
    id 907
    label "funt_szterling"
  ]
  node [
    id 908
    label "Portland"
  ]
  node [
    id 909
    label "El&#380;bieta_I"
  ]
  node [
    id 910
    label "Kornwalia"
  ]
  node [
    id 911
    label "Dolna_Frankonia"
  ]
  node [
    id 912
    label "Karpaty"
  ]
  node [
    id 913
    label "Beskid_Niski"
  ]
  node [
    id 914
    label "Mariensztat"
  ]
  node [
    id 915
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 916
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 917
    label "Paj&#281;czno"
  ]
  node [
    id 918
    label "Mogielnica"
  ]
  node [
    id 919
    label "Gop&#322;o"
  ]
  node [
    id 920
    label "Moza"
  ]
  node [
    id 921
    label "Poprad"
  ]
  node [
    id 922
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 923
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 924
    label "Bojanowo"
  ]
  node [
    id 925
    label "Obra"
  ]
  node [
    id 926
    label "Wilkowo_Polskie"
  ]
  node [
    id 927
    label "Dobra"
  ]
  node [
    id 928
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 929
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 930
    label "Etruria"
  ]
  node [
    id 931
    label "Rumelia"
  ]
  node [
    id 932
    label "Tar&#322;&#243;w"
  ]
  node [
    id 933
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 934
    label "Abchazja"
  ]
  node [
    id 935
    label "Sarmata"
  ]
  node [
    id 936
    label "Eurazja"
  ]
  node [
    id 937
    label "Tatry"
  ]
  node [
    id 938
    label "Podtatrze"
  ]
  node [
    id 939
    label "Imperium_Rosyjskie"
  ]
  node [
    id 940
    label "jezioro"
  ]
  node [
    id 941
    label "&#346;l&#261;sk"
  ]
  node [
    id 942
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 943
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 944
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 945
    label "Austro-W&#281;gry"
  ]
  node [
    id 946
    label "funt_szkocki"
  ]
  node [
    id 947
    label "Kaledonia"
  ]
  node [
    id 948
    label "Biskupice"
  ]
  node [
    id 949
    label "Iwanowice"
  ]
  node [
    id 950
    label "Ziemia_Sandomierska"
  ]
  node [
    id 951
    label "Rogo&#378;nik"
  ]
  node [
    id 952
    label "Ropa"
  ]
  node [
    id 953
    label "Buriacja"
  ]
  node [
    id 954
    label "Rozewie"
  ]
  node [
    id 955
    label "Norwegia"
  ]
  node [
    id 956
    label "Szwecja"
  ]
  node [
    id 957
    label "Finlandia"
  ]
  node [
    id 958
    label "Aruba"
  ]
  node [
    id 959
    label "Kajmany"
  ]
  node [
    id 960
    label "Anguilla"
  ]
  node [
    id 961
    label "Amazonka"
  ]
  node [
    id 962
    label "pochwali&#263;"
  ]
  node [
    id 963
    label "zg&#322;osi&#263;"
  ]
  node [
    id 964
    label "zarekomendowa&#263;"
  ]
  node [
    id 965
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 966
    label "advertise"
  ]
  node [
    id 967
    label "boast"
  ]
  node [
    id 968
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 969
    label "poinformowa&#263;"
  ]
  node [
    id 970
    label "write"
  ]
  node [
    id 971
    label "announce"
  ]
  node [
    id 972
    label "opracowa&#263;"
  ]
  node [
    id 973
    label "give"
  ]
  node [
    id 974
    label "note"
  ]
  node [
    id 975
    label "da&#263;"
  ]
  node [
    id 976
    label "marshal"
  ]
  node [
    id 977
    label "zmieni&#263;"
  ]
  node [
    id 978
    label "przy&#322;o&#380;y&#263;"
  ]
  node [
    id 979
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 980
    label "fold"
  ]
  node [
    id 981
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 982
    label "zebra&#263;"
  ]
  node [
    id 983
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 984
    label "spowodowa&#263;"
  ]
  node [
    id 985
    label "jell"
  ]
  node [
    id 986
    label "frame"
  ]
  node [
    id 987
    label "przekaza&#263;"
  ]
  node [
    id 988
    label "set"
  ]
  node [
    id 989
    label "scali&#263;"
  ]
  node [
    id 990
    label "odda&#263;"
  ]
  node [
    id 991
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 992
    label "pay"
  ]
  node [
    id 993
    label "zestaw"
  ]
  node [
    id 994
    label "praise"
  ]
  node [
    id 995
    label "nagrodzi&#263;"
  ]
  node [
    id 996
    label "wywy&#380;szy&#263;"
  ]
  node [
    id 997
    label "doradzi&#263;"
  ]
  node [
    id 998
    label "commend"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 3
    target 430
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 432
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 434
  ]
  edge [
    source 3
    target 435
  ]
  edge [
    source 3
    target 436
  ]
  edge [
    source 3
    target 437
  ]
  edge [
    source 3
    target 438
  ]
  edge [
    source 3
    target 439
  ]
  edge [
    source 3
    target 440
  ]
  edge [
    source 3
    target 441
  ]
  edge [
    source 3
    target 442
  ]
  edge [
    source 3
    target 443
  ]
  edge [
    source 3
    target 444
  ]
  edge [
    source 3
    target 445
  ]
  edge [
    source 3
    target 446
  ]
  edge [
    source 3
    target 447
  ]
  edge [
    source 3
    target 448
  ]
  edge [
    source 3
    target 449
  ]
  edge [
    source 3
    target 450
  ]
  edge [
    source 3
    target 451
  ]
  edge [
    source 3
    target 452
  ]
  edge [
    source 3
    target 453
  ]
  edge [
    source 3
    target 454
  ]
  edge [
    source 3
    target 455
  ]
  edge [
    source 3
    target 456
  ]
  edge [
    source 3
    target 457
  ]
  edge [
    source 3
    target 458
  ]
  edge [
    source 3
    target 459
  ]
  edge [
    source 3
    target 460
  ]
  edge [
    source 3
    target 461
  ]
  edge [
    source 3
    target 462
  ]
  edge [
    source 3
    target 463
  ]
  edge [
    source 3
    target 464
  ]
  edge [
    source 3
    target 465
  ]
  edge [
    source 3
    target 466
  ]
  edge [
    source 3
    target 467
  ]
  edge [
    source 3
    target 468
  ]
  edge [
    source 3
    target 469
  ]
  edge [
    source 3
    target 470
  ]
  edge [
    source 3
    target 471
  ]
  edge [
    source 3
    target 472
  ]
  edge [
    source 3
    target 473
  ]
  edge [
    source 3
    target 474
  ]
  edge [
    source 3
    target 475
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 3
    target 477
  ]
  edge [
    source 3
    target 478
  ]
  edge [
    source 3
    target 479
  ]
  edge [
    source 3
    target 480
  ]
  edge [
    source 3
    target 481
  ]
  edge [
    source 3
    target 482
  ]
  edge [
    source 3
    target 483
  ]
  edge [
    source 3
    target 484
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 486
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 488
  ]
  edge [
    source 3
    target 489
  ]
  edge [
    source 3
    target 490
  ]
  edge [
    source 3
    target 491
  ]
  edge [
    source 3
    target 492
  ]
  edge [
    source 3
    target 493
  ]
  edge [
    source 3
    target 494
  ]
  edge [
    source 3
    target 495
  ]
  edge [
    source 3
    target 496
  ]
  edge [
    source 3
    target 497
  ]
  edge [
    source 3
    target 498
  ]
  edge [
    source 3
    target 499
  ]
  edge [
    source 3
    target 500
  ]
  edge [
    source 3
    target 501
  ]
  edge [
    source 3
    target 502
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 3
    target 600
  ]
  edge [
    source 3
    target 601
  ]
  edge [
    source 3
    target 602
  ]
  edge [
    source 3
    target 603
  ]
  edge [
    source 3
    target 604
  ]
  edge [
    source 3
    target 605
  ]
  edge [
    source 3
    target 606
  ]
  edge [
    source 3
    target 607
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 612
  ]
  edge [
    source 3
    target 613
  ]
  edge [
    source 3
    target 614
  ]
  edge [
    source 3
    target 615
  ]
  edge [
    source 3
    target 616
  ]
  edge [
    source 3
    target 617
  ]
  edge [
    source 3
    target 618
  ]
  edge [
    source 3
    target 619
  ]
  edge [
    source 3
    target 620
  ]
  edge [
    source 3
    target 621
  ]
  edge [
    source 3
    target 622
  ]
  edge [
    source 3
    target 623
  ]
  edge [
    source 3
    target 624
  ]
  edge [
    source 3
    target 625
  ]
  edge [
    source 3
    target 626
  ]
  edge [
    source 3
    target 627
  ]
  edge [
    source 3
    target 628
  ]
  edge [
    source 3
    target 629
  ]
  edge [
    source 3
    target 630
  ]
  edge [
    source 3
    target 631
  ]
  edge [
    source 3
    target 632
  ]
  edge [
    source 3
    target 633
  ]
  edge [
    source 3
    target 634
  ]
  edge [
    source 3
    target 635
  ]
  edge [
    source 3
    target 636
  ]
  edge [
    source 3
    target 637
  ]
  edge [
    source 3
    target 638
  ]
  edge [
    source 3
    target 639
  ]
  edge [
    source 3
    target 640
  ]
  edge [
    source 3
    target 641
  ]
  edge [
    source 3
    target 642
  ]
  edge [
    source 3
    target 643
  ]
  edge [
    source 3
    target 644
  ]
  edge [
    source 3
    target 645
  ]
  edge [
    source 3
    target 646
  ]
  edge [
    source 3
    target 647
  ]
  edge [
    source 3
    target 648
  ]
  edge [
    source 3
    target 649
  ]
  edge [
    source 3
    target 650
  ]
  edge [
    source 3
    target 651
  ]
  edge [
    source 3
    target 652
  ]
  edge [
    source 3
    target 653
  ]
  edge [
    source 3
    target 654
  ]
  edge [
    source 3
    target 655
  ]
  edge [
    source 3
    target 656
  ]
  edge [
    source 3
    target 657
  ]
  edge [
    source 3
    target 658
  ]
  edge [
    source 3
    target 659
  ]
  edge [
    source 3
    target 660
  ]
  edge [
    source 3
    target 661
  ]
  edge [
    source 3
    target 662
  ]
  edge [
    source 3
    target 663
  ]
  edge [
    source 3
    target 664
  ]
  edge [
    source 3
    target 665
  ]
  edge [
    source 3
    target 666
  ]
  edge [
    source 3
    target 667
  ]
  edge [
    source 3
    target 668
  ]
  edge [
    source 3
    target 669
  ]
  edge [
    source 3
    target 670
  ]
  edge [
    source 3
    target 671
  ]
  edge [
    source 3
    target 672
  ]
  edge [
    source 3
    target 673
  ]
  edge [
    source 3
    target 674
  ]
  edge [
    source 3
    target 675
  ]
  edge [
    source 3
    target 676
  ]
  edge [
    source 3
    target 677
  ]
  edge [
    source 3
    target 678
  ]
  edge [
    source 3
    target 679
  ]
  edge [
    source 3
    target 680
  ]
  edge [
    source 3
    target 681
  ]
  edge [
    source 3
    target 682
  ]
  edge [
    source 3
    target 683
  ]
  edge [
    source 3
    target 684
  ]
  edge [
    source 3
    target 685
  ]
  edge [
    source 3
    target 686
  ]
  edge [
    source 3
    target 687
  ]
  edge [
    source 3
    target 688
  ]
  edge [
    source 3
    target 689
  ]
  edge [
    source 3
    target 690
  ]
  edge [
    source 3
    target 691
  ]
  edge [
    source 3
    target 692
  ]
  edge [
    source 3
    target 693
  ]
  edge [
    source 3
    target 694
  ]
  edge [
    source 3
    target 695
  ]
  edge [
    source 3
    target 696
  ]
  edge [
    source 3
    target 697
  ]
  edge [
    source 3
    target 698
  ]
  edge [
    source 3
    target 699
  ]
  edge [
    source 3
    target 700
  ]
  edge [
    source 3
    target 701
  ]
  edge [
    source 3
    target 702
  ]
  edge [
    source 3
    target 703
  ]
  edge [
    source 3
    target 704
  ]
  edge [
    source 3
    target 705
  ]
  edge [
    source 3
    target 706
  ]
  edge [
    source 3
    target 707
  ]
  edge [
    source 3
    target 708
  ]
  edge [
    source 3
    target 709
  ]
  edge [
    source 3
    target 710
  ]
  edge [
    source 3
    target 711
  ]
  edge [
    source 3
    target 712
  ]
  edge [
    source 3
    target 713
  ]
  edge [
    source 3
    target 714
  ]
  edge [
    source 3
    target 715
  ]
  edge [
    source 3
    target 716
  ]
  edge [
    source 3
    target 717
  ]
  edge [
    source 3
    target 718
  ]
  edge [
    source 3
    target 719
  ]
  edge [
    source 3
    target 720
  ]
  edge [
    source 3
    target 721
  ]
  edge [
    source 3
    target 722
  ]
  edge [
    source 3
    target 723
  ]
  edge [
    source 3
    target 724
  ]
  edge [
    source 3
    target 725
  ]
  edge [
    source 3
    target 726
  ]
  edge [
    source 3
    target 727
  ]
  edge [
    source 3
    target 728
  ]
  edge [
    source 3
    target 729
  ]
  edge [
    source 3
    target 730
  ]
  edge [
    source 3
    target 731
  ]
  edge [
    source 3
    target 732
  ]
  edge [
    source 3
    target 733
  ]
  edge [
    source 3
    target 734
  ]
  edge [
    source 3
    target 735
  ]
  edge [
    source 3
    target 736
  ]
  edge [
    source 3
    target 737
  ]
  edge [
    source 3
    target 738
  ]
  edge [
    source 3
    target 739
  ]
  edge [
    source 3
    target 740
  ]
  edge [
    source 3
    target 741
  ]
  edge [
    source 3
    target 742
  ]
  edge [
    source 3
    target 743
  ]
  edge [
    source 3
    target 744
  ]
  edge [
    source 3
    target 745
  ]
  edge [
    source 3
    target 746
  ]
  edge [
    source 3
    target 747
  ]
  edge [
    source 3
    target 748
  ]
  edge [
    source 3
    target 749
  ]
  edge [
    source 3
    target 750
  ]
  edge [
    source 3
    target 751
  ]
  edge [
    source 3
    target 752
  ]
  edge [
    source 3
    target 753
  ]
  edge [
    source 3
    target 754
  ]
  edge [
    source 3
    target 755
  ]
  edge [
    source 3
    target 756
  ]
  edge [
    source 3
    target 757
  ]
  edge [
    source 3
    target 758
  ]
  edge [
    source 3
    target 759
  ]
  edge [
    source 3
    target 760
  ]
  edge [
    source 3
    target 761
  ]
  edge [
    source 3
    target 762
  ]
  edge [
    source 3
    target 763
  ]
  edge [
    source 3
    target 764
  ]
  edge [
    source 3
    target 765
  ]
  edge [
    source 3
    target 766
  ]
  edge [
    source 3
    target 767
  ]
  edge [
    source 3
    target 768
  ]
  edge [
    source 3
    target 769
  ]
  edge [
    source 3
    target 770
  ]
  edge [
    source 3
    target 771
  ]
  edge [
    source 3
    target 772
  ]
  edge [
    source 3
    target 773
  ]
  edge [
    source 3
    target 774
  ]
  edge [
    source 3
    target 775
  ]
  edge [
    source 3
    target 776
  ]
  edge [
    source 3
    target 777
  ]
  edge [
    source 3
    target 778
  ]
  edge [
    source 3
    target 779
  ]
  edge [
    source 3
    target 780
  ]
  edge [
    source 3
    target 781
  ]
  edge [
    source 3
    target 782
  ]
  edge [
    source 3
    target 783
  ]
  edge [
    source 3
    target 784
  ]
  edge [
    source 3
    target 785
  ]
  edge [
    source 3
    target 786
  ]
  edge [
    source 3
    target 787
  ]
  edge [
    source 3
    target 788
  ]
  edge [
    source 3
    target 789
  ]
  edge [
    source 3
    target 790
  ]
  edge [
    source 3
    target 791
  ]
  edge [
    source 3
    target 792
  ]
  edge [
    source 3
    target 793
  ]
  edge [
    source 3
    target 794
  ]
  edge [
    source 3
    target 795
  ]
  edge [
    source 3
    target 796
  ]
  edge [
    source 3
    target 797
  ]
  edge [
    source 3
    target 798
  ]
  edge [
    source 3
    target 799
  ]
  edge [
    source 3
    target 800
  ]
  edge [
    source 3
    target 801
  ]
  edge [
    source 3
    target 802
  ]
  edge [
    source 3
    target 803
  ]
  edge [
    source 3
    target 804
  ]
  edge [
    source 3
    target 805
  ]
  edge [
    source 3
    target 806
  ]
  edge [
    source 3
    target 807
  ]
  edge [
    source 3
    target 808
  ]
  edge [
    source 3
    target 809
  ]
  edge [
    source 3
    target 810
  ]
  edge [
    source 3
    target 811
  ]
  edge [
    source 3
    target 812
  ]
  edge [
    source 3
    target 813
  ]
  edge [
    source 3
    target 814
  ]
  edge [
    source 3
    target 815
  ]
  edge [
    source 3
    target 816
  ]
  edge [
    source 3
    target 817
  ]
  edge [
    source 3
    target 818
  ]
  edge [
    source 3
    target 819
  ]
  edge [
    source 3
    target 820
  ]
  edge [
    source 3
    target 821
  ]
  edge [
    source 3
    target 822
  ]
  edge [
    source 3
    target 823
  ]
  edge [
    source 3
    target 824
  ]
  edge [
    source 3
    target 825
  ]
  edge [
    source 3
    target 826
  ]
  edge [
    source 3
    target 827
  ]
  edge [
    source 3
    target 828
  ]
  edge [
    source 3
    target 829
  ]
  edge [
    source 3
    target 830
  ]
  edge [
    source 3
    target 831
  ]
  edge [
    source 3
    target 832
  ]
  edge [
    source 3
    target 833
  ]
  edge [
    source 3
    target 834
  ]
  edge [
    source 3
    target 835
  ]
  edge [
    source 3
    target 836
  ]
  edge [
    source 3
    target 837
  ]
  edge [
    source 3
    target 838
  ]
  edge [
    source 3
    target 839
  ]
  edge [
    source 3
    target 840
  ]
  edge [
    source 3
    target 841
  ]
  edge [
    source 3
    target 842
  ]
  edge [
    source 3
    target 843
  ]
  edge [
    source 3
    target 844
  ]
  edge [
    source 3
    target 845
  ]
  edge [
    source 3
    target 846
  ]
  edge [
    source 3
    target 847
  ]
  edge [
    source 3
    target 848
  ]
  edge [
    source 3
    target 849
  ]
  edge [
    source 3
    target 850
  ]
  edge [
    source 3
    target 851
  ]
  edge [
    source 3
    target 852
  ]
  edge [
    source 3
    target 853
  ]
  edge [
    source 3
    target 854
  ]
  edge [
    source 3
    target 855
  ]
  edge [
    source 3
    target 856
  ]
  edge [
    source 3
    target 857
  ]
  edge [
    source 3
    target 858
  ]
  edge [
    source 3
    target 859
  ]
  edge [
    source 3
    target 860
  ]
  edge [
    source 3
    target 861
  ]
  edge [
    source 3
    target 862
  ]
  edge [
    source 3
    target 863
  ]
  edge [
    source 3
    target 864
  ]
  edge [
    source 3
    target 865
  ]
  edge [
    source 3
    target 866
  ]
  edge [
    source 3
    target 867
  ]
  edge [
    source 3
    target 868
  ]
  edge [
    source 3
    target 869
  ]
  edge [
    source 3
    target 870
  ]
  edge [
    source 3
    target 871
  ]
  edge [
    source 3
    target 872
  ]
  edge [
    source 3
    target 873
  ]
  edge [
    source 3
    target 874
  ]
  edge [
    source 3
    target 875
  ]
  edge [
    source 3
    target 876
  ]
  edge [
    source 3
    target 877
  ]
  edge [
    source 3
    target 878
  ]
  edge [
    source 3
    target 879
  ]
  edge [
    source 3
    target 880
  ]
  edge [
    source 3
    target 881
  ]
  edge [
    source 3
    target 882
  ]
  edge [
    source 3
    target 883
  ]
  edge [
    source 3
    target 884
  ]
  edge [
    source 3
    target 885
  ]
  edge [
    source 3
    target 886
  ]
  edge [
    source 3
    target 887
  ]
  edge [
    source 3
    target 888
  ]
  edge [
    source 3
    target 889
  ]
  edge [
    source 3
    target 890
  ]
  edge [
    source 3
    target 891
  ]
  edge [
    source 3
    target 892
  ]
  edge [
    source 3
    target 893
  ]
  edge [
    source 3
    target 894
  ]
  edge [
    source 3
    target 895
  ]
  edge [
    source 3
    target 896
  ]
  edge [
    source 3
    target 897
  ]
  edge [
    source 3
    target 898
  ]
  edge [
    source 3
    target 899
  ]
  edge [
    source 3
    target 900
  ]
  edge [
    source 3
    target 901
  ]
  edge [
    source 3
    target 902
  ]
  edge [
    source 3
    target 903
  ]
  edge [
    source 3
    target 904
  ]
  edge [
    source 3
    target 905
  ]
  edge [
    source 3
    target 906
  ]
  edge [
    source 3
    target 907
  ]
  edge [
    source 3
    target 908
  ]
  edge [
    source 3
    target 909
  ]
  edge [
    source 3
    target 910
  ]
  edge [
    source 3
    target 911
  ]
  edge [
    source 3
    target 912
  ]
  edge [
    source 3
    target 913
  ]
  edge [
    source 3
    target 914
  ]
  edge [
    source 3
    target 915
  ]
  edge [
    source 3
    target 916
  ]
  edge [
    source 3
    target 917
  ]
  edge [
    source 3
    target 918
  ]
  edge [
    source 3
    target 919
  ]
  edge [
    source 3
    target 920
  ]
  edge [
    source 3
    target 921
  ]
  edge [
    source 3
    target 922
  ]
  edge [
    source 3
    target 923
  ]
  edge [
    source 3
    target 924
  ]
  edge [
    source 3
    target 925
  ]
  edge [
    source 3
    target 926
  ]
  edge [
    source 3
    target 927
  ]
  edge [
    source 3
    target 928
  ]
  edge [
    source 3
    target 929
  ]
  edge [
    source 3
    target 930
  ]
  edge [
    source 3
    target 931
  ]
  edge [
    source 3
    target 932
  ]
  edge [
    source 3
    target 933
  ]
  edge [
    source 3
    target 934
  ]
  edge [
    source 3
    target 935
  ]
  edge [
    source 3
    target 936
  ]
  edge [
    source 3
    target 937
  ]
  edge [
    source 3
    target 938
  ]
  edge [
    source 3
    target 939
  ]
  edge [
    source 3
    target 940
  ]
  edge [
    source 3
    target 941
  ]
  edge [
    source 3
    target 942
  ]
  edge [
    source 3
    target 943
  ]
  edge [
    source 3
    target 944
  ]
  edge [
    source 3
    target 945
  ]
  edge [
    source 3
    target 946
  ]
  edge [
    source 3
    target 947
  ]
  edge [
    source 3
    target 948
  ]
  edge [
    source 3
    target 949
  ]
  edge [
    source 3
    target 950
  ]
  edge [
    source 3
    target 951
  ]
  edge [
    source 3
    target 952
  ]
  edge [
    source 3
    target 953
  ]
  edge [
    source 3
    target 954
  ]
  edge [
    source 3
    target 955
  ]
  edge [
    source 3
    target 956
  ]
  edge [
    source 3
    target 957
  ]
  edge [
    source 3
    target 958
  ]
  edge [
    source 3
    target 959
  ]
  edge [
    source 3
    target 960
  ]
  edge [
    source 3
    target 961
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 962
  ]
  edge [
    source 4
    target 963
  ]
  edge [
    source 4
    target 964
  ]
  edge [
    source 4
    target 965
  ]
  edge [
    source 4
    target 966
  ]
  edge [
    source 4
    target 967
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 968
  ]
  edge [
    source 4
    target 969
  ]
  edge [
    source 4
    target 970
  ]
  edge [
    source 4
    target 971
  ]
  edge [
    source 4
    target 972
  ]
  edge [
    source 4
    target 973
  ]
  edge [
    source 4
    target 974
  ]
  edge [
    source 4
    target 975
  ]
  edge [
    source 4
    target 976
  ]
  edge [
    source 4
    target 977
  ]
  edge [
    source 4
    target 978
  ]
  edge [
    source 4
    target 979
  ]
  edge [
    source 4
    target 980
  ]
  edge [
    source 4
    target 981
  ]
  edge [
    source 4
    target 982
  ]
  edge [
    source 4
    target 983
  ]
  edge [
    source 4
    target 984
  ]
  edge [
    source 4
    target 985
  ]
  edge [
    source 4
    target 986
  ]
  edge [
    source 4
    target 987
  ]
  edge [
    source 4
    target 988
  ]
  edge [
    source 4
    target 989
  ]
  edge [
    source 4
    target 990
  ]
  edge [
    source 4
    target 991
  ]
  edge [
    source 4
    target 992
  ]
  edge [
    source 4
    target 993
  ]
  edge [
    source 4
    target 994
  ]
  edge [
    source 4
    target 995
  ]
  edge [
    source 4
    target 996
  ]
  edge [
    source 4
    target 997
  ]
  edge [
    source 4
    target 998
  ]
  edge [
    source 5
    target 6
  ]
]
