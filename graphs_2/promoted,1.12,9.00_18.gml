graph [
  node [
    id 0
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 1
    label "statystyk"
    origin "text"
  ]
  node [
    id 2
    label "departament"
    origin "text"
  ]
  node [
    id 3
    label "praca"
    origin "text"
  ]
  node [
    id 4
    label "usa"
    origin "text"
  ]
  node [
    id 5
    label "dostawca"
    origin "text"
  ]
  node [
    id 6
    label "pizza"
    origin "text"
  ]
  node [
    id 7
    label "og&#243;lnie"
    origin "text"
  ]
  node [
    id 8
    label "driver"
    origin "text"
  ]
  node [
    id 9
    label "sales"
    origin "text"
  ]
  node [
    id 10
    label "workers"
    origin "text"
  ]
  node [
    id 11
    label "pi&#261;ty"
    origin "text"
  ]
  node [
    id 12
    label "najbardziej"
    origin "text"
  ]
  node [
    id 13
    label "niebezpieczny"
    origin "text"
  ]
  node [
    id 14
    label "zaw&#243;d"
    origin "text"
  ]
  node [
    id 15
    label "ameryka"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "zabija&#263;"
    origin "text"
  ]
  node [
    id 18
    label "bity"
    origin "text"
  ]
  node [
    id 19
    label "nieprzytomno&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "okrada&#263;"
    origin "text"
  ]
  node [
    id 21
    label "matematyk"
  ]
  node [
    id 22
    label "nauczyciel"
  ]
  node [
    id 23
    label "Kartezjusz"
  ]
  node [
    id 24
    label "Berkeley"
  ]
  node [
    id 25
    label "Biot"
  ]
  node [
    id 26
    label "Archimedes"
  ]
  node [
    id 27
    label "Ptolemeusz"
  ]
  node [
    id 28
    label "Gauss"
  ]
  node [
    id 29
    label "Newton"
  ]
  node [
    id 30
    label "Kepler"
  ]
  node [
    id 31
    label "Fourier"
  ]
  node [
    id 32
    label "Bayes"
  ]
  node [
    id 33
    label "Doppler"
  ]
  node [
    id 34
    label "Laplace"
  ]
  node [
    id 35
    label "Euklides"
  ]
  node [
    id 36
    label "Borel"
  ]
  node [
    id 37
    label "naukowiec"
  ]
  node [
    id 38
    label "Galileusz"
  ]
  node [
    id 39
    label "Pitagoras"
  ]
  node [
    id 40
    label "Pascal"
  ]
  node [
    id 41
    label "Maxwell"
  ]
  node [
    id 42
    label "jednostka_organizacyjna"
  ]
  node [
    id 43
    label "relation"
  ]
  node [
    id 44
    label "Martynika"
  ]
  node [
    id 45
    label "podsekcja"
  ]
  node [
    id 46
    label "ministerstwo"
  ]
  node [
    id 47
    label "Gwadelupa"
  ]
  node [
    id 48
    label "Moza"
  ]
  node [
    id 49
    label "jednostka_administracyjna"
  ]
  node [
    id 50
    label "urz&#261;d"
  ]
  node [
    id 51
    label "NKWD"
  ]
  node [
    id 52
    label "ministerium"
  ]
  node [
    id 53
    label "Ministerstwo_Obrony_Narodowej"
  ]
  node [
    id 54
    label "MSW"
  ]
  node [
    id 55
    label "resort"
  ]
  node [
    id 56
    label "Francja"
  ]
  node [
    id 57
    label "Lotaryngia"
  ]
  node [
    id 58
    label "Europa"
  ]
  node [
    id 59
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 60
    label "najem"
  ]
  node [
    id 61
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 62
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 63
    label "zak&#322;ad"
  ]
  node [
    id 64
    label "stosunek_pracy"
  ]
  node [
    id 65
    label "benedykty&#324;ski"
  ]
  node [
    id 66
    label "poda&#380;_pracy"
  ]
  node [
    id 67
    label "pracowanie"
  ]
  node [
    id 68
    label "tyrka"
  ]
  node [
    id 69
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 70
    label "wytw&#243;r"
  ]
  node [
    id 71
    label "miejsce"
  ]
  node [
    id 72
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 73
    label "tynkarski"
  ]
  node [
    id 74
    label "pracowa&#263;"
  ]
  node [
    id 75
    label "czynno&#347;&#263;"
  ]
  node [
    id 76
    label "zmiana"
  ]
  node [
    id 77
    label "czynnik_produkcji"
  ]
  node [
    id 78
    label "zobowi&#261;zanie"
  ]
  node [
    id 79
    label "kierownictwo"
  ]
  node [
    id 80
    label "siedziba"
  ]
  node [
    id 81
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 82
    label "przedmiot"
  ]
  node [
    id 83
    label "p&#322;&#243;d"
  ]
  node [
    id 84
    label "work"
  ]
  node [
    id 85
    label "rezultat"
  ]
  node [
    id 86
    label "activity"
  ]
  node [
    id 87
    label "bezproblemowy"
  ]
  node [
    id 88
    label "wydarzenie"
  ]
  node [
    id 89
    label "warunek_lokalowy"
  ]
  node [
    id 90
    label "plac"
  ]
  node [
    id 91
    label "location"
  ]
  node [
    id 92
    label "uwaga"
  ]
  node [
    id 93
    label "przestrze&#324;"
  ]
  node [
    id 94
    label "status"
  ]
  node [
    id 95
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 96
    label "chwila"
  ]
  node [
    id 97
    label "cia&#322;o"
  ]
  node [
    id 98
    label "cecha"
  ]
  node [
    id 99
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 100
    label "rz&#261;d"
  ]
  node [
    id 101
    label "stosunek_prawny"
  ]
  node [
    id 102
    label "oblig"
  ]
  node [
    id 103
    label "uregulowa&#263;"
  ]
  node [
    id 104
    label "oddzia&#322;anie"
  ]
  node [
    id 105
    label "occupation"
  ]
  node [
    id 106
    label "duty"
  ]
  node [
    id 107
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 108
    label "zapowied&#378;"
  ]
  node [
    id 109
    label "obowi&#261;zek"
  ]
  node [
    id 110
    label "statement"
  ]
  node [
    id 111
    label "zapewnienie"
  ]
  node [
    id 112
    label "miejsce_pracy"
  ]
  node [
    id 113
    label "zak&#322;adka"
  ]
  node [
    id 114
    label "instytucja"
  ]
  node [
    id 115
    label "wyko&#324;czenie"
  ]
  node [
    id 116
    label "firma"
  ]
  node [
    id 117
    label "czyn"
  ]
  node [
    id 118
    label "company"
  ]
  node [
    id 119
    label "instytut"
  ]
  node [
    id 120
    label "umowa"
  ]
  node [
    id 121
    label "&#321;ubianka"
  ]
  node [
    id 122
    label "dzia&#322;_personalny"
  ]
  node [
    id 123
    label "Kreml"
  ]
  node [
    id 124
    label "Bia&#322;y_Dom"
  ]
  node [
    id 125
    label "budynek"
  ]
  node [
    id 126
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 127
    label "sadowisko"
  ]
  node [
    id 128
    label "rewizja"
  ]
  node [
    id 129
    label "passage"
  ]
  node [
    id 130
    label "oznaka"
  ]
  node [
    id 131
    label "change"
  ]
  node [
    id 132
    label "ferment"
  ]
  node [
    id 133
    label "komplet"
  ]
  node [
    id 134
    label "anatomopatolog"
  ]
  node [
    id 135
    label "zmianka"
  ]
  node [
    id 136
    label "czas"
  ]
  node [
    id 137
    label "zjawisko"
  ]
  node [
    id 138
    label "amendment"
  ]
  node [
    id 139
    label "odmienianie"
  ]
  node [
    id 140
    label "tura"
  ]
  node [
    id 141
    label "cierpliwy"
  ]
  node [
    id 142
    label "mozolny"
  ]
  node [
    id 143
    label "wytrwa&#322;y"
  ]
  node [
    id 144
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 145
    label "benedykty&#324;sko"
  ]
  node [
    id 146
    label "typowy"
  ]
  node [
    id 147
    label "po_benedykty&#324;sku"
  ]
  node [
    id 148
    label "endeavor"
  ]
  node [
    id 149
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 150
    label "mie&#263;_miejsce"
  ]
  node [
    id 151
    label "podejmowa&#263;"
  ]
  node [
    id 152
    label "dziama&#263;"
  ]
  node [
    id 153
    label "do"
  ]
  node [
    id 154
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 155
    label "bangla&#263;"
  ]
  node [
    id 156
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 157
    label "maszyna"
  ]
  node [
    id 158
    label "dzia&#322;a&#263;"
  ]
  node [
    id 159
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 160
    label "tryb"
  ]
  node [
    id 161
    label "funkcjonowa&#263;"
  ]
  node [
    id 162
    label "zawodoznawstwo"
  ]
  node [
    id 163
    label "emocja"
  ]
  node [
    id 164
    label "office"
  ]
  node [
    id 165
    label "kwalifikacje"
  ]
  node [
    id 166
    label "craft"
  ]
  node [
    id 167
    label "przepracowanie_si&#281;"
  ]
  node [
    id 168
    label "zarz&#261;dzanie"
  ]
  node [
    id 169
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 170
    label "podlizanie_si&#281;"
  ]
  node [
    id 171
    label "dopracowanie"
  ]
  node [
    id 172
    label "podlizywanie_si&#281;"
  ]
  node [
    id 173
    label "uruchamianie"
  ]
  node [
    id 174
    label "dzia&#322;anie"
  ]
  node [
    id 175
    label "d&#261;&#380;enie"
  ]
  node [
    id 176
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 177
    label "uruchomienie"
  ]
  node [
    id 178
    label "nakr&#281;canie"
  ]
  node [
    id 179
    label "funkcjonowanie"
  ]
  node [
    id 180
    label "tr&#243;jstronny"
  ]
  node [
    id 181
    label "postaranie_si&#281;"
  ]
  node [
    id 182
    label "odpocz&#281;cie"
  ]
  node [
    id 183
    label "nakr&#281;cenie"
  ]
  node [
    id 184
    label "zatrzymanie"
  ]
  node [
    id 185
    label "spracowanie_si&#281;"
  ]
  node [
    id 186
    label "skakanie"
  ]
  node [
    id 187
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 188
    label "podtrzymywanie"
  ]
  node [
    id 189
    label "w&#322;&#261;czanie"
  ]
  node [
    id 190
    label "zaprz&#281;ganie"
  ]
  node [
    id 191
    label "podejmowanie"
  ]
  node [
    id 192
    label "wyrabianie"
  ]
  node [
    id 193
    label "dzianie_si&#281;"
  ]
  node [
    id 194
    label "use"
  ]
  node [
    id 195
    label "przepracowanie"
  ]
  node [
    id 196
    label "poruszanie_si&#281;"
  ]
  node [
    id 197
    label "funkcja"
  ]
  node [
    id 198
    label "impact"
  ]
  node [
    id 199
    label "przepracowywanie"
  ]
  node [
    id 200
    label "awansowanie"
  ]
  node [
    id 201
    label "courtship"
  ]
  node [
    id 202
    label "zapracowanie"
  ]
  node [
    id 203
    label "wyrobienie"
  ]
  node [
    id 204
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 205
    label "w&#322;&#261;czenie"
  ]
  node [
    id 206
    label "transakcja"
  ]
  node [
    id 207
    label "biuro"
  ]
  node [
    id 208
    label "lead"
  ]
  node [
    id 209
    label "zesp&#243;&#322;"
  ]
  node [
    id 210
    label "w&#322;adza"
  ]
  node [
    id 211
    label "transportowiec"
  ]
  node [
    id 212
    label "podmiot_gospodarczy"
  ]
  node [
    id 213
    label "us&#322;ugodawca"
  ]
  node [
    id 214
    label "podmiot"
  ]
  node [
    id 215
    label "kierowca"
  ]
  node [
    id 216
    label "pracownik"
  ]
  node [
    id 217
    label "statek_handlowy"
  ]
  node [
    id 218
    label "okr&#281;t_nawodny"
  ]
  node [
    id 219
    label "bran&#380;owiec"
  ]
  node [
    id 220
    label "wypiek"
  ]
  node [
    id 221
    label "sp&#243;d"
  ]
  node [
    id 222
    label "fast_food"
  ]
  node [
    id 223
    label "baking"
  ]
  node [
    id 224
    label "upiek"
  ]
  node [
    id 225
    label "jedzenie"
  ]
  node [
    id 226
    label "produkt"
  ]
  node [
    id 227
    label "pieczenie"
  ]
  node [
    id 228
    label "produkcja"
  ]
  node [
    id 229
    label "placek"
  ]
  node [
    id 230
    label "bielizna"
  ]
  node [
    id 231
    label "strona"
  ]
  node [
    id 232
    label "d&#243;&#322;"
  ]
  node [
    id 233
    label "&#322;&#261;cznie"
  ]
  node [
    id 234
    label "nadrz&#281;dnie"
  ]
  node [
    id 235
    label "og&#243;lny"
  ]
  node [
    id 236
    label "posp&#243;lnie"
  ]
  node [
    id 237
    label "zbiorowo"
  ]
  node [
    id 238
    label "generalny"
  ]
  node [
    id 239
    label "zbiorowy"
  ]
  node [
    id 240
    label "og&#243;&#322;owy"
  ]
  node [
    id 241
    label "nadrz&#281;dny"
  ]
  node [
    id 242
    label "ca&#322;y"
  ]
  node [
    id 243
    label "kompletny"
  ]
  node [
    id 244
    label "&#322;&#261;czny"
  ]
  node [
    id 245
    label "powszechnie"
  ]
  node [
    id 246
    label "zwierzchni"
  ]
  node [
    id 247
    label "porz&#261;dny"
  ]
  node [
    id 248
    label "podstawowy"
  ]
  node [
    id 249
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 250
    label "zasadniczy"
  ]
  node [
    id 251
    label "generalnie"
  ]
  node [
    id 252
    label "zbiorczo"
  ]
  node [
    id 253
    label "wsp&#243;lnie"
  ]
  node [
    id 254
    label "licznie"
  ]
  node [
    id 255
    label "istotnie"
  ]
  node [
    id 256
    label "posp&#243;lny"
  ]
  node [
    id 257
    label "wood"
  ]
  node [
    id 258
    label "program"
  ]
  node [
    id 259
    label "instalowa&#263;"
  ]
  node [
    id 260
    label "oprogramowanie"
  ]
  node [
    id 261
    label "odinstalowywa&#263;"
  ]
  node [
    id 262
    label "spis"
  ]
  node [
    id 263
    label "zaprezentowanie"
  ]
  node [
    id 264
    label "podprogram"
  ]
  node [
    id 265
    label "ogranicznik_referencyjny"
  ]
  node [
    id 266
    label "course_of_study"
  ]
  node [
    id 267
    label "booklet"
  ]
  node [
    id 268
    label "dzia&#322;"
  ]
  node [
    id 269
    label "odinstalowanie"
  ]
  node [
    id 270
    label "broszura"
  ]
  node [
    id 271
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 272
    label "kana&#322;"
  ]
  node [
    id 273
    label "teleferie"
  ]
  node [
    id 274
    label "zainstalowanie"
  ]
  node [
    id 275
    label "struktura_organizacyjna"
  ]
  node [
    id 276
    label "pirat"
  ]
  node [
    id 277
    label "zaprezentowa&#263;"
  ]
  node [
    id 278
    label "prezentowanie"
  ]
  node [
    id 279
    label "prezentowa&#263;"
  ]
  node [
    id 280
    label "interfejs"
  ]
  node [
    id 281
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 282
    label "okno"
  ]
  node [
    id 283
    label "blok"
  ]
  node [
    id 284
    label "punkt"
  ]
  node [
    id 285
    label "folder"
  ]
  node [
    id 286
    label "zainstalowa&#263;"
  ]
  node [
    id 287
    label "za&#322;o&#380;enie"
  ]
  node [
    id 288
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 289
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 290
    label "ram&#243;wka"
  ]
  node [
    id 291
    label "emitowa&#263;"
  ]
  node [
    id 292
    label "emitowanie"
  ]
  node [
    id 293
    label "odinstalowywanie"
  ]
  node [
    id 294
    label "instrukcja"
  ]
  node [
    id 295
    label "informatyka"
  ]
  node [
    id 296
    label "deklaracja"
  ]
  node [
    id 297
    label "menu"
  ]
  node [
    id 298
    label "sekcja_krytyczna"
  ]
  node [
    id 299
    label "furkacja"
  ]
  node [
    id 300
    label "podstawa"
  ]
  node [
    id 301
    label "instalowanie"
  ]
  node [
    id 302
    label "oferta"
  ]
  node [
    id 303
    label "odinstalowa&#263;"
  ]
  node [
    id 304
    label "cz&#322;owiek"
  ]
  node [
    id 305
    label "kij_golfowy"
  ]
  node [
    id 306
    label "dzie&#324;"
  ]
  node [
    id 307
    label "ranek"
  ]
  node [
    id 308
    label "doba"
  ]
  node [
    id 309
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 310
    label "noc"
  ]
  node [
    id 311
    label "podwiecz&#243;r"
  ]
  node [
    id 312
    label "po&#322;udnie"
  ]
  node [
    id 313
    label "godzina"
  ]
  node [
    id 314
    label "przedpo&#322;udnie"
  ]
  node [
    id 315
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 316
    label "long_time"
  ]
  node [
    id 317
    label "wiecz&#243;r"
  ]
  node [
    id 318
    label "t&#322;usty_czwartek"
  ]
  node [
    id 319
    label "popo&#322;udnie"
  ]
  node [
    id 320
    label "walentynki"
  ]
  node [
    id 321
    label "czynienie_si&#281;"
  ]
  node [
    id 322
    label "s&#322;o&#324;ce"
  ]
  node [
    id 323
    label "rano"
  ]
  node [
    id 324
    label "tydzie&#324;"
  ]
  node [
    id 325
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 326
    label "wzej&#347;cie"
  ]
  node [
    id 327
    label "wsta&#263;"
  ]
  node [
    id 328
    label "day"
  ]
  node [
    id 329
    label "termin"
  ]
  node [
    id 330
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 331
    label "wstanie"
  ]
  node [
    id 332
    label "przedwiecz&#243;r"
  ]
  node [
    id 333
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 334
    label "Sylwester"
  ]
  node [
    id 335
    label "niebezpiecznie"
  ]
  node [
    id 336
    label "gro&#378;ny"
  ]
  node [
    id 337
    label "k&#322;opotliwy"
  ]
  node [
    id 338
    label "k&#322;opotliwie"
  ]
  node [
    id 339
    label "gro&#378;nie"
  ]
  node [
    id 340
    label "nieprzyjemny"
  ]
  node [
    id 341
    label "niewygodny"
  ]
  node [
    id 342
    label "nad&#261;sany"
  ]
  node [
    id 343
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 344
    label "eliminacje"
  ]
  node [
    id 345
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 346
    label "ogrom"
  ]
  node [
    id 347
    label "iskrzy&#263;"
  ]
  node [
    id 348
    label "d&#322;awi&#263;"
  ]
  node [
    id 349
    label "ostygn&#261;&#263;"
  ]
  node [
    id 350
    label "stygn&#261;&#263;"
  ]
  node [
    id 351
    label "stan"
  ]
  node [
    id 352
    label "temperatura"
  ]
  node [
    id 353
    label "wpa&#347;&#263;"
  ]
  node [
    id 354
    label "afekt"
  ]
  node [
    id 355
    label "wpada&#263;"
  ]
  node [
    id 356
    label "wiedza"
  ]
  node [
    id 357
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 358
    label "equal"
  ]
  node [
    id 359
    label "trwa&#263;"
  ]
  node [
    id 360
    label "chodzi&#263;"
  ]
  node [
    id 361
    label "si&#281;ga&#263;"
  ]
  node [
    id 362
    label "obecno&#347;&#263;"
  ]
  node [
    id 363
    label "stand"
  ]
  node [
    id 364
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 365
    label "uczestniczy&#263;"
  ]
  node [
    id 366
    label "participate"
  ]
  node [
    id 367
    label "robi&#263;"
  ]
  node [
    id 368
    label "istnie&#263;"
  ]
  node [
    id 369
    label "pozostawa&#263;"
  ]
  node [
    id 370
    label "zostawa&#263;"
  ]
  node [
    id 371
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 372
    label "adhere"
  ]
  node [
    id 373
    label "compass"
  ]
  node [
    id 374
    label "korzysta&#263;"
  ]
  node [
    id 375
    label "appreciation"
  ]
  node [
    id 376
    label "osi&#261;ga&#263;"
  ]
  node [
    id 377
    label "dociera&#263;"
  ]
  node [
    id 378
    label "get"
  ]
  node [
    id 379
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 380
    label "mierzy&#263;"
  ]
  node [
    id 381
    label "u&#380;ywa&#263;"
  ]
  node [
    id 382
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 383
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 384
    label "exsert"
  ]
  node [
    id 385
    label "being"
  ]
  node [
    id 386
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 387
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 388
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 389
    label "p&#322;ywa&#263;"
  ]
  node [
    id 390
    label "run"
  ]
  node [
    id 391
    label "przebiega&#263;"
  ]
  node [
    id 392
    label "wk&#322;ada&#263;"
  ]
  node [
    id 393
    label "proceed"
  ]
  node [
    id 394
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 395
    label "carry"
  ]
  node [
    id 396
    label "bywa&#263;"
  ]
  node [
    id 397
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 398
    label "stara&#263;_si&#281;"
  ]
  node [
    id 399
    label "para"
  ]
  node [
    id 400
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 401
    label "str&#243;j"
  ]
  node [
    id 402
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 403
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 404
    label "krok"
  ]
  node [
    id 405
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 406
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 407
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 408
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 409
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 410
    label "continue"
  ]
  node [
    id 411
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 412
    label "Ohio"
  ]
  node [
    id 413
    label "wci&#281;cie"
  ]
  node [
    id 414
    label "Nowy_York"
  ]
  node [
    id 415
    label "warstwa"
  ]
  node [
    id 416
    label "samopoczucie"
  ]
  node [
    id 417
    label "Illinois"
  ]
  node [
    id 418
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 419
    label "state"
  ]
  node [
    id 420
    label "Jukatan"
  ]
  node [
    id 421
    label "Kalifornia"
  ]
  node [
    id 422
    label "Wirginia"
  ]
  node [
    id 423
    label "wektor"
  ]
  node [
    id 424
    label "Goa"
  ]
  node [
    id 425
    label "Teksas"
  ]
  node [
    id 426
    label "Waszyngton"
  ]
  node [
    id 427
    label "Massachusetts"
  ]
  node [
    id 428
    label "Alaska"
  ]
  node [
    id 429
    label "Arakan"
  ]
  node [
    id 430
    label "Hawaje"
  ]
  node [
    id 431
    label "Maryland"
  ]
  node [
    id 432
    label "Michigan"
  ]
  node [
    id 433
    label "Arizona"
  ]
  node [
    id 434
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 435
    label "Georgia"
  ]
  node [
    id 436
    label "poziom"
  ]
  node [
    id 437
    label "Pensylwania"
  ]
  node [
    id 438
    label "shape"
  ]
  node [
    id 439
    label "Luizjana"
  ]
  node [
    id 440
    label "Nowy_Meksyk"
  ]
  node [
    id 441
    label "Alabama"
  ]
  node [
    id 442
    label "ilo&#347;&#263;"
  ]
  node [
    id 443
    label "Kansas"
  ]
  node [
    id 444
    label "Oregon"
  ]
  node [
    id 445
    label "Oklahoma"
  ]
  node [
    id 446
    label "Floryda"
  ]
  node [
    id 447
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 448
    label "powodowa&#263;_&#347;mier&#263;"
  ]
  node [
    id 449
    label "dispatch"
  ]
  node [
    id 450
    label "krzywdzi&#263;"
  ]
  node [
    id 451
    label "beat"
  ]
  node [
    id 452
    label "rzuca&#263;_na_kolana"
  ]
  node [
    id 453
    label "os&#322;ania&#263;"
  ]
  node [
    id 454
    label "niszczy&#263;"
  ]
  node [
    id 455
    label "karci&#263;"
  ]
  node [
    id 456
    label "mordowa&#263;"
  ]
  node [
    id 457
    label "bi&#263;"
  ]
  node [
    id 458
    label "zako&#324;cza&#263;"
  ]
  node [
    id 459
    label "rozbraja&#263;"
  ]
  node [
    id 460
    label "przybija&#263;"
  ]
  node [
    id 461
    label "morzy&#263;"
  ]
  node [
    id 462
    label "zakrywa&#263;"
  ]
  node [
    id 463
    label "kill"
  ]
  node [
    id 464
    label "zwalcza&#263;"
  ]
  node [
    id 465
    label "strike"
  ]
  node [
    id 466
    label "&#322;adowa&#263;"
  ]
  node [
    id 467
    label "usuwa&#263;"
  ]
  node [
    id 468
    label "butcher"
  ]
  node [
    id 469
    label "murder"
  ]
  node [
    id 470
    label "take"
  ]
  node [
    id 471
    label "napierdziela&#263;"
  ]
  node [
    id 472
    label "t&#322;oczy&#263;"
  ]
  node [
    id 473
    label "rejestrowa&#263;"
  ]
  node [
    id 474
    label "traktowa&#263;"
  ]
  node [
    id 475
    label "skuwa&#263;"
  ]
  node [
    id 476
    label "przygotowywa&#263;"
  ]
  node [
    id 477
    label "macha&#263;"
  ]
  node [
    id 478
    label "zag&#322;&#281;bia&#263;"
  ]
  node [
    id 479
    label "dawa&#263;"
  ]
  node [
    id 480
    label "peddle"
  ]
  node [
    id 481
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 482
    label "&#380;&#322;obi&#263;"
  ]
  node [
    id 483
    label "rap"
  ]
  node [
    id 484
    label "emanowa&#263;"
  ]
  node [
    id 485
    label "dzwoni&#263;"
  ]
  node [
    id 486
    label "nalewa&#263;"
  ]
  node [
    id 487
    label "balansjerka"
  ]
  node [
    id 488
    label "wygrywa&#263;"
  ]
  node [
    id 489
    label "t&#322;uc"
  ]
  node [
    id 490
    label "uderza&#263;"
  ]
  node [
    id 491
    label "wpiernicza&#263;"
  ]
  node [
    id 492
    label "pra&#263;"
  ]
  node [
    id 493
    label "str&#261;ca&#263;"
  ]
  node [
    id 494
    label "przerabia&#263;"
  ]
  node [
    id 495
    label "powodowa&#263;"
  ]
  node [
    id 496
    label "tug"
  ]
  node [
    id 497
    label "chop"
  ]
  node [
    id 498
    label "satisfy"
  ]
  node [
    id 499
    label "dopracowywa&#263;"
  ]
  node [
    id 500
    label "elaborate"
  ]
  node [
    id 501
    label "determine"
  ]
  node [
    id 502
    label "finish_up"
  ]
  node [
    id 503
    label "przestawa&#263;"
  ]
  node [
    id 504
    label "stanowi&#263;"
  ]
  node [
    id 505
    label "rezygnowa&#263;"
  ]
  node [
    id 506
    label "nadawa&#263;"
  ]
  node [
    id 507
    label "destroy"
  ]
  node [
    id 508
    label "uszkadza&#263;"
  ]
  node [
    id 509
    label "os&#322;abia&#263;"
  ]
  node [
    id 510
    label "szkodzi&#263;"
  ]
  node [
    id 511
    label "zdrowie"
  ]
  node [
    id 512
    label "mar"
  ]
  node [
    id 513
    label "pamper"
  ]
  node [
    id 514
    label "pokonywa&#263;"
  ]
  node [
    id 515
    label "fight"
  ]
  node [
    id 516
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 517
    label "dobija&#263;"
  ]
  node [
    id 518
    label "ubija&#263;"
  ]
  node [
    id 519
    label "przygn&#281;bia&#263;"
  ]
  node [
    id 520
    label "przystawia&#263;"
  ]
  node [
    id 521
    label "unieruchamia&#263;"
  ]
  node [
    id 522
    label "statek"
  ]
  node [
    id 523
    label "parali&#380;owa&#263;"
  ]
  node [
    id 524
    label "ogarnia&#263;"
  ]
  node [
    id 525
    label "wbija&#263;"
  ]
  node [
    id 526
    label "akceptowa&#263;"
  ]
  node [
    id 527
    label "przybywa&#263;"
  ]
  node [
    id 528
    label "hit"
  ]
  node [
    id 529
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 530
    label "przymocowywa&#263;"
  ]
  node [
    id 531
    label "po&#347;wiadcza&#263;"
  ]
  node [
    id 532
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 533
    label "nail"
  ]
  node [
    id 534
    label "stuka&#263;"
  ]
  node [
    id 535
    label "przygniata&#263;"
  ]
  node [
    id 536
    label "uwi&#261;zywa&#263;"
  ]
  node [
    id 537
    label "m&#281;czy&#263;"
  ]
  node [
    id 538
    label "psu&#263;"
  ]
  node [
    id 539
    label "ora&#263;"
  ]
  node [
    id 540
    label "exterminate"
  ]
  node [
    id 541
    label "zabiera&#263;"
  ]
  node [
    id 542
    label "sk&#322;ada&#263;"
  ]
  node [
    id 543
    label "roz&#322;adowywa&#263;"
  ]
  node [
    id 544
    label "discharge"
  ]
  node [
    id 545
    label "bro&#324;"
  ]
  node [
    id 546
    label "ukrzywdza&#263;"
  ]
  node [
    id 547
    label "niesprawiedliwy"
  ]
  node [
    id 548
    label "wrong"
  ]
  node [
    id 549
    label "kara&#263;"
  ]
  node [
    id 550
    label "upomina&#263;"
  ]
  node [
    id 551
    label "warn"
  ]
  node [
    id 552
    label "odgradza&#263;"
  ]
  node [
    id 553
    label "report"
  ]
  node [
    id 554
    label "chroni&#263;"
  ]
  node [
    id 555
    label "champion"
  ]
  node [
    id 556
    label "broni&#263;"
  ]
  node [
    id 557
    label "cover"
  ]
  node [
    id 558
    label "zas&#322;ania&#263;"
  ]
  node [
    id 559
    label "zataja&#263;"
  ]
  node [
    id 560
    label "zamyka&#263;"
  ]
  node [
    id 561
    label "dr&#281;czy&#263;"
  ]
  node [
    id 562
    label "rytm"
  ]
  node [
    id 563
    label "uszkodzony"
  ]
  node [
    id 564
    label "utwardzony"
  ]
  node [
    id 565
    label "jedyny"
  ]
  node [
    id 566
    label "du&#380;y"
  ]
  node [
    id 567
    label "zdr&#243;w"
  ]
  node [
    id 568
    label "calu&#347;ko"
  ]
  node [
    id 569
    label "&#380;ywy"
  ]
  node [
    id 570
    label "pe&#322;ny"
  ]
  node [
    id 571
    label "podobny"
  ]
  node [
    id 572
    label "ca&#322;o"
  ]
  node [
    id 573
    label "twardy"
  ]
  node [
    id 574
    label "wyraz"
  ]
  node [
    id 575
    label "unconsciousness"
  ]
  node [
    id 576
    label "term"
  ]
  node [
    id 577
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 578
    label "leksem"
  ]
  node [
    id 579
    label "posta&#263;"
  ]
  node [
    id 580
    label "element"
  ]
  node [
    id 581
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 582
    label "&#347;wiadczenie"
  ]
  node [
    id 583
    label "obskakiwa&#263;"
  ]
  node [
    id 584
    label "obdziera&#263;"
  ]
  node [
    id 585
    label "zajmowa&#263;"
  ]
  node [
    id 586
    label "poci&#261;ga&#263;"
  ]
  node [
    id 587
    label "fall"
  ]
  node [
    id 588
    label "liszy&#263;"
  ]
  node [
    id 589
    label "&#322;apa&#263;"
  ]
  node [
    id 590
    label "przesuwa&#263;"
  ]
  node [
    id 591
    label "prowadzi&#263;"
  ]
  node [
    id 592
    label "blurt_out"
  ]
  node [
    id 593
    label "konfiskowa&#263;"
  ]
  node [
    id 594
    label "deprive"
  ]
  node [
    id 595
    label "abstract"
  ]
  node [
    id 596
    label "przenosi&#263;"
  ]
  node [
    id 597
    label "pull"
  ]
  node [
    id 598
    label "bra&#263;"
  ]
  node [
    id 599
    label "tear"
  ]
  node [
    id 600
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 601
    label "zarabia&#263;"
  ]
  node [
    id 602
    label "strip"
  ]
  node [
    id 603
    label "odbiera&#263;"
  ]
  node [
    id 604
    label "obiega&#263;"
  ]
  node [
    id 605
    label "okr&#261;&#380;a&#263;"
  ]
  node [
    id 606
    label "op&#281;dza&#263;"
  ]
  node [
    id 607
    label "osacza&#263;"
  ]
  node [
    id 608
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 609
    label "dostawa&#263;"
  ]
  node [
    id 610
    label "environment"
  ]
  node [
    id 611
    label "radzi&#263;_sobie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 160
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 343
  ]
  edge [
    source 14
    target 344
  ]
  edge [
    source 14
    target 345
  ]
  edge [
    source 14
    target 346
  ]
  edge [
    source 14
    target 347
  ]
  edge [
    source 14
    target 348
  ]
  edge [
    source 14
    target 349
  ]
  edge [
    source 14
    target 350
  ]
  edge [
    source 14
    target 351
  ]
  edge [
    source 14
    target 352
  ]
  edge [
    source 14
    target 353
  ]
  edge [
    source 14
    target 354
  ]
  edge [
    source 14
    target 355
  ]
  edge [
    source 14
    target 356
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 68
  ]
  edge [
    source 14
    target 69
  ]
  edge [
    source 14
    target 70
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 14
    target 72
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 357
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 358
  ]
  edge [
    source 16
    target 359
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 16
    target 361
  ]
  edge [
    source 16
    target 351
  ]
  edge [
    source 16
    target 362
  ]
  edge [
    source 16
    target 363
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 366
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 394
  ]
  edge [
    source 16
    target 395
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 397
  ]
  edge [
    source 16
    target 398
  ]
  edge [
    source 16
    target 399
  ]
  edge [
    source 16
    target 400
  ]
  edge [
    source 16
    target 401
  ]
  edge [
    source 16
    target 402
  ]
  edge [
    source 16
    target 403
  ]
  edge [
    source 16
    target 404
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 406
  ]
  edge [
    source 16
    target 407
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 412
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 16
    target 418
  ]
  edge [
    source 16
    target 419
  ]
  edge [
    source 16
    target 420
  ]
  edge [
    source 16
    target 421
  ]
  edge [
    source 16
    target 422
  ]
  edge [
    source 16
    target 423
  ]
  edge [
    source 16
    target 424
  ]
  edge [
    source 16
    target 425
  ]
  edge [
    source 16
    target 426
  ]
  edge [
    source 16
    target 71
  ]
  edge [
    source 16
    target 427
  ]
  edge [
    source 16
    target 428
  ]
  edge [
    source 16
    target 429
  ]
  edge [
    source 16
    target 430
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 284
  ]
  edge [
    source 16
    target 432
  ]
  edge [
    source 16
    target 433
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 16
    target 441
  ]
  edge [
    source 16
    target 442
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 16
    target 444
  ]
  edge [
    source 16
    target 445
  ]
  edge [
    source 16
    target 446
  ]
  edge [
    source 16
    target 49
  ]
  edge [
    source 16
    target 447
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 448
  ]
  edge [
    source 17
    target 449
  ]
  edge [
    source 17
    target 450
  ]
  edge [
    source 17
    target 451
  ]
  edge [
    source 17
    target 452
  ]
  edge [
    source 17
    target 453
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 455
  ]
  edge [
    source 17
    target 456
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 465
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 386
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 474
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 17
    target 476
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 477
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 393
  ]
  edge [
    source 17
    target 479
  ]
  edge [
    source 17
    target 480
  ]
  edge [
    source 17
    target 481
  ]
  edge [
    source 17
    target 482
  ]
  edge [
    source 17
    target 483
  ]
  edge [
    source 17
    target 484
  ]
  edge [
    source 17
    target 485
  ]
  edge [
    source 17
    target 486
  ]
  edge [
    source 17
    target 487
  ]
  edge [
    source 17
    target 488
  ]
  edge [
    source 17
    target 489
  ]
  edge [
    source 17
    target 490
  ]
  edge [
    source 17
    target 491
  ]
  edge [
    source 17
    target 492
  ]
  edge [
    source 17
    target 493
  ]
  edge [
    source 17
    target 494
  ]
  edge [
    source 17
    target 495
  ]
  edge [
    source 17
    target 496
  ]
  edge [
    source 17
    target 497
  ]
  edge [
    source 17
    target 498
  ]
  edge [
    source 17
    target 499
  ]
  edge [
    source 17
    target 500
  ]
  edge [
    source 17
    target 501
  ]
  edge [
    source 17
    target 502
  ]
  edge [
    source 17
    target 503
  ]
  edge [
    source 17
    target 504
  ]
  edge [
    source 17
    target 505
  ]
  edge [
    source 17
    target 506
  ]
  edge [
    source 17
    target 507
  ]
  edge [
    source 17
    target 508
  ]
  edge [
    source 17
    target 509
  ]
  edge [
    source 17
    target 510
  ]
  edge [
    source 17
    target 511
  ]
  edge [
    source 17
    target 512
  ]
  edge [
    source 17
    target 513
  ]
  edge [
    source 17
    target 514
  ]
  edge [
    source 17
    target 515
  ]
  edge [
    source 17
    target 516
  ]
  edge [
    source 17
    target 517
  ]
  edge [
    source 17
    target 518
  ]
  edge [
    source 17
    target 519
  ]
  edge [
    source 17
    target 520
  ]
  edge [
    source 17
    target 521
  ]
  edge [
    source 17
    target 522
  ]
  edge [
    source 17
    target 523
  ]
  edge [
    source 17
    target 524
  ]
  edge [
    source 17
    target 525
  ]
  edge [
    source 17
    target 526
  ]
  edge [
    source 17
    target 527
  ]
  edge [
    source 17
    target 528
  ]
  edge [
    source 17
    target 529
  ]
  edge [
    source 17
    target 530
  ]
  edge [
    source 17
    target 531
  ]
  edge [
    source 17
    target 532
  ]
  edge [
    source 17
    target 533
  ]
  edge [
    source 17
    target 534
  ]
  edge [
    source 17
    target 535
  ]
  edge [
    source 17
    target 536
  ]
  edge [
    source 17
    target 537
  ]
  edge [
    source 17
    target 367
  ]
  edge [
    source 17
    target 538
  ]
  edge [
    source 17
    target 539
  ]
  edge [
    source 17
    target 540
  ]
  edge [
    source 17
    target 541
  ]
  edge [
    source 17
    target 542
  ]
  edge [
    source 17
    target 543
  ]
  edge [
    source 17
    target 544
  ]
  edge [
    source 17
    target 545
  ]
  edge [
    source 17
    target 546
  ]
  edge [
    source 17
    target 547
  ]
  edge [
    source 17
    target 548
  ]
  edge [
    source 17
    target 549
  ]
  edge [
    source 17
    target 550
  ]
  edge [
    source 17
    target 551
  ]
  edge [
    source 17
    target 552
  ]
  edge [
    source 17
    target 553
  ]
  edge [
    source 17
    target 554
  ]
  edge [
    source 17
    target 555
  ]
  edge [
    source 17
    target 556
  ]
  edge [
    source 17
    target 557
  ]
  edge [
    source 17
    target 558
  ]
  edge [
    source 17
    target 559
  ]
  edge [
    source 17
    target 560
  ]
  edge [
    source 17
    target 561
  ]
  edge [
    source 17
    target 562
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 563
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 242
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 567
  ]
  edge [
    source 18
    target 568
  ]
  edge [
    source 18
    target 243
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 351
  ]
  edge [
    source 19
    target 574
  ]
  edge [
    source 19
    target 575
  ]
  edge [
    source 19
    target 412
  ]
  edge [
    source 19
    target 413
  ]
  edge [
    source 19
    target 414
  ]
  edge [
    source 19
    target 415
  ]
  edge [
    source 19
    target 416
  ]
  edge [
    source 19
    target 417
  ]
  edge [
    source 19
    target 418
  ]
  edge [
    source 19
    target 419
  ]
  edge [
    source 19
    target 420
  ]
  edge [
    source 19
    target 421
  ]
  edge [
    source 19
    target 422
  ]
  edge [
    source 19
    target 423
  ]
  edge [
    source 19
    target 425
  ]
  edge [
    source 19
    target 424
  ]
  edge [
    source 19
    target 426
  ]
  edge [
    source 19
    target 71
  ]
  edge [
    source 19
    target 427
  ]
  edge [
    source 19
    target 428
  ]
  edge [
    source 19
    target 429
  ]
  edge [
    source 19
    target 430
  ]
  edge [
    source 19
    target 431
  ]
  edge [
    source 19
    target 284
  ]
  edge [
    source 19
    target 432
  ]
  edge [
    source 19
    target 433
  ]
  edge [
    source 19
    target 434
  ]
  edge [
    source 19
    target 435
  ]
  edge [
    source 19
    target 436
  ]
  edge [
    source 19
    target 437
  ]
  edge [
    source 19
    target 438
  ]
  edge [
    source 19
    target 439
  ]
  edge [
    source 19
    target 440
  ]
  edge [
    source 19
    target 441
  ]
  edge [
    source 19
    target 442
  ]
  edge [
    source 19
    target 443
  ]
  edge [
    source 19
    target 444
  ]
  edge [
    source 19
    target 446
  ]
  edge [
    source 19
    target 445
  ]
  edge [
    source 19
    target 49
  ]
  edge [
    source 19
    target 447
  ]
  edge [
    source 19
    target 576
  ]
  edge [
    source 19
    target 130
  ]
  edge [
    source 19
    target 577
  ]
  edge [
    source 19
    target 578
  ]
  edge [
    source 19
    target 579
  ]
  edge [
    source 19
    target 580
  ]
  edge [
    source 19
    target 98
  ]
  edge [
    source 19
    target 581
  ]
  edge [
    source 19
    target 582
  ]
  edge [
    source 20
    target 513
  ]
  edge [
    source 20
    target 541
  ]
  edge [
    source 20
    target 583
  ]
  edge [
    source 20
    target 584
  ]
  edge [
    source 20
    target 585
  ]
  edge [
    source 20
    target 586
  ]
  edge [
    source 20
    target 407
  ]
  edge [
    source 20
    target 587
  ]
  edge [
    source 20
    target 588
  ]
  edge [
    source 20
    target 589
  ]
  edge [
    source 20
    target 590
  ]
  edge [
    source 20
    target 591
  ]
  edge [
    source 20
    target 592
  ]
  edge [
    source 20
    target 593
  ]
  edge [
    source 20
    target 594
  ]
  edge [
    source 20
    target 595
  ]
  edge [
    source 20
    target 596
  ]
  edge [
    source 20
    target 597
  ]
  edge [
    source 20
    target 598
  ]
  edge [
    source 20
    target 599
  ]
  edge [
    source 20
    target 600
  ]
  edge [
    source 20
    target 601
  ]
  edge [
    source 20
    target 602
  ]
  edge [
    source 20
    target 603
  ]
  edge [
    source 20
    target 604
  ]
  edge [
    source 20
    target 605
  ]
  edge [
    source 20
    target 606
  ]
  edge [
    source 20
    target 607
  ]
  edge [
    source 20
    target 608
  ]
  edge [
    source 20
    target 609
  ]
  edge [
    source 20
    target 610
  ]
  edge [
    source 20
    target 611
  ]
]
