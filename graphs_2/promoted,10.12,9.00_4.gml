graph [
  node [
    id 0
    label "materia&#322;"
    origin "text"
  ]
  node [
    id 1
    label "wearechange"
    origin "text"
  ]
  node [
    id 2
    label "cz&#322;owiek"
  ]
  node [
    id 3
    label "materia"
  ]
  node [
    id 4
    label "nawil&#380;arka"
  ]
  node [
    id 5
    label "bielarnia"
  ]
  node [
    id 6
    label "dyspozycja"
  ]
  node [
    id 7
    label "dane"
  ]
  node [
    id 8
    label "tworzywo"
  ]
  node [
    id 9
    label "substancja"
  ]
  node [
    id 10
    label "kandydat"
  ]
  node [
    id 11
    label "archiwum"
  ]
  node [
    id 12
    label "krajka"
  ]
  node [
    id 13
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 14
    label "rozrywa&#263;_si&#281;"
  ]
  node [
    id 15
    label "krajalno&#347;&#263;"
  ]
  node [
    id 16
    label "edytowa&#263;"
  ]
  node [
    id 17
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 18
    label "spakowanie"
  ]
  node [
    id 19
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 20
    label "pakowa&#263;"
  ]
  node [
    id 21
    label "rekord"
  ]
  node [
    id 22
    label "korelator"
  ]
  node [
    id 23
    label "wyci&#261;ganie"
  ]
  node [
    id 24
    label "pakowanie"
  ]
  node [
    id 25
    label "sekwencjonowa&#263;"
  ]
  node [
    id 26
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 27
    label "jednostka_informacji"
  ]
  node [
    id 28
    label "zbi&#243;r"
  ]
  node [
    id 29
    label "evidence"
  ]
  node [
    id 30
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 31
    label "rozpakowywanie"
  ]
  node [
    id 32
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 33
    label "rozpakowanie"
  ]
  node [
    id 34
    label "informacja"
  ]
  node [
    id 35
    label "rozpakowywa&#263;"
  ]
  node [
    id 36
    label "konwersja"
  ]
  node [
    id 37
    label "nap&#322;ywanie"
  ]
  node [
    id 38
    label "rozpakowa&#263;"
  ]
  node [
    id 39
    label "spakowa&#263;"
  ]
  node [
    id 40
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 41
    label "edytowanie"
  ]
  node [
    id 42
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 43
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 44
    label "sekwencjonowanie"
  ]
  node [
    id 45
    label "przenikanie"
  ]
  node [
    id 46
    label "byt"
  ]
  node [
    id 47
    label "cz&#261;steczka"
  ]
  node [
    id 48
    label "temperatura_krytyczna"
  ]
  node [
    id 49
    label "przenika&#263;"
  ]
  node [
    id 50
    label "smolisty"
  ]
  node [
    id 51
    label "ludzko&#347;&#263;"
  ]
  node [
    id 52
    label "asymilowanie"
  ]
  node [
    id 53
    label "wapniak"
  ]
  node [
    id 54
    label "asymilowa&#263;"
  ]
  node [
    id 55
    label "os&#322;abia&#263;"
  ]
  node [
    id 56
    label "posta&#263;"
  ]
  node [
    id 57
    label "hominid"
  ]
  node [
    id 58
    label "podw&#322;adny"
  ]
  node [
    id 59
    label "os&#322;abianie"
  ]
  node [
    id 60
    label "g&#322;owa"
  ]
  node [
    id 61
    label "figura"
  ]
  node [
    id 62
    label "portrecista"
  ]
  node [
    id 63
    label "dwun&#243;g"
  ]
  node [
    id 64
    label "profanum"
  ]
  node [
    id 65
    label "mikrokosmos"
  ]
  node [
    id 66
    label "nasada"
  ]
  node [
    id 67
    label "duch"
  ]
  node [
    id 68
    label "antropochoria"
  ]
  node [
    id 69
    label "osoba"
  ]
  node [
    id 70
    label "wz&#243;r"
  ]
  node [
    id 71
    label "senior"
  ]
  node [
    id 72
    label "oddzia&#322;ywanie"
  ]
  node [
    id 73
    label "Adam"
  ]
  node [
    id 74
    label "homo_sapiens"
  ]
  node [
    id 75
    label "polifag"
  ]
  node [
    id 76
    label "obszycie"
  ]
  node [
    id 77
    label "okrajka"
  ]
  node [
    id 78
    label "wst&#261;&#380;ka"
  ]
  node [
    id 79
    label "pasek"
  ]
  node [
    id 80
    label "bardko"
  ]
  node [
    id 81
    label "pasmanteria"
  ]
  node [
    id 82
    label "temat"
  ]
  node [
    id 83
    label "szczeg&#243;&#322;"
  ]
  node [
    id 84
    label "ropa"
  ]
  node [
    id 85
    label "dualizm_falowo-korpuskularny"
  ]
  node [
    id 86
    label "rzecz"
  ]
  node [
    id 87
    label "szk&#322;o"
  ]
  node [
    id 88
    label "blacha"
  ]
  node [
    id 89
    label "cecha"
  ]
  node [
    id 90
    label "maszyna_w&#322;&#243;kiennicza"
  ]
  node [
    id 91
    label "zak&#322;ad_przemys&#322;owy"
  ]
  node [
    id 92
    label "lista_wyborcza"
  ]
  node [
    id 93
    label "aspirowanie"
  ]
  node [
    id 94
    label "bierne_prawo_wyborcze"
  ]
  node [
    id 95
    label "kolekcja"
  ]
  node [
    id 96
    label "instytucja"
  ]
  node [
    id 97
    label "dokumentacja"
  ]
  node [
    id 98
    label "archive"
  ]
  node [
    id 99
    label "plan"
  ]
  node [
    id 100
    label "kondycja"
  ]
  node [
    id 101
    label "potencja&#322;"
  ]
  node [
    id 102
    label "polecenie"
  ]
  node [
    id 103
    label "samopoczucie"
  ]
  node [
    id 104
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 105
    label "zdolno&#347;&#263;"
  ]
  node [
    id 106
    label "capability"
  ]
  node [
    id 107
    label "prawo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
]
