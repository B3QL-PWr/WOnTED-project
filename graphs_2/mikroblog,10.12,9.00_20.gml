graph [
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wysy&#322;a&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nosacz"
    origin "text"
  ]
  node [
    id 3
    label "zanim"
    origin "text"
  ]
  node [
    id 4
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 5
    label "modny"
    origin "text"
  ]
  node [
    id 6
    label "fejsbuk"
    origin "text"
  ]
  node [
    id 7
    label "poza"
    origin "text"
  ]
  node [
    id 8
    label "wykop"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "pyta&#263;"
    origin "text"
  ]
  node [
    id 11
    label "chuj"
    origin "text"
  ]
  node [
    id 12
    label "chodzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "ma&#322;pa"
    origin "text"
  ]
  node [
    id 14
    label "echo"
  ]
  node [
    id 15
    label "pilnowa&#263;"
  ]
  node [
    id 16
    label "robi&#263;"
  ]
  node [
    id 17
    label "recall"
  ]
  node [
    id 18
    label "si&#281;ga&#263;"
  ]
  node [
    id 19
    label "take_care"
  ]
  node [
    id 20
    label "troska&#263;_si&#281;"
  ]
  node [
    id 21
    label "chowa&#263;"
  ]
  node [
    id 22
    label "zachowywa&#263;"
  ]
  node [
    id 23
    label "zna&#263;"
  ]
  node [
    id 24
    label "think"
  ]
  node [
    id 25
    label "report"
  ]
  node [
    id 26
    label "hide"
  ]
  node [
    id 27
    label "znosi&#263;"
  ]
  node [
    id 28
    label "czu&#263;"
  ]
  node [
    id 29
    label "train"
  ]
  node [
    id 30
    label "przetrzymywa&#263;"
  ]
  node [
    id 31
    label "hodowa&#263;"
  ]
  node [
    id 32
    label "meliniarz"
  ]
  node [
    id 33
    label "umieszcza&#263;"
  ]
  node [
    id 34
    label "ukrywa&#263;"
  ]
  node [
    id 35
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 36
    label "continue"
  ]
  node [
    id 37
    label "wk&#322;ada&#263;"
  ]
  node [
    id 38
    label "tajemnica"
  ]
  node [
    id 39
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 40
    label "zdyscyplinowanie"
  ]
  node [
    id 41
    label "podtrzymywa&#263;"
  ]
  node [
    id 42
    label "post"
  ]
  node [
    id 43
    label "control"
  ]
  node [
    id 44
    label "przechowywa&#263;"
  ]
  node [
    id 45
    label "behave"
  ]
  node [
    id 46
    label "dieta"
  ]
  node [
    id 47
    label "hold"
  ]
  node [
    id 48
    label "post&#281;powa&#263;"
  ]
  node [
    id 49
    label "compass"
  ]
  node [
    id 50
    label "korzysta&#263;"
  ]
  node [
    id 51
    label "appreciation"
  ]
  node [
    id 52
    label "osi&#261;ga&#263;"
  ]
  node [
    id 53
    label "dociera&#263;"
  ]
  node [
    id 54
    label "get"
  ]
  node [
    id 55
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 56
    label "mierzy&#263;"
  ]
  node [
    id 57
    label "u&#380;ywa&#263;"
  ]
  node [
    id 58
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 59
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 60
    label "exsert"
  ]
  node [
    id 61
    label "organizowa&#263;"
  ]
  node [
    id 62
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 63
    label "czyni&#263;"
  ]
  node [
    id 64
    label "give"
  ]
  node [
    id 65
    label "stylizowa&#263;"
  ]
  node [
    id 66
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 67
    label "falowa&#263;"
  ]
  node [
    id 68
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 69
    label "peddle"
  ]
  node [
    id 70
    label "praca"
  ]
  node [
    id 71
    label "wydala&#263;"
  ]
  node [
    id 72
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 73
    label "tentegowa&#263;"
  ]
  node [
    id 74
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 75
    label "urz&#261;dza&#263;"
  ]
  node [
    id 76
    label "oszukiwa&#263;"
  ]
  node [
    id 77
    label "work"
  ]
  node [
    id 78
    label "ukazywa&#263;"
  ]
  node [
    id 79
    label "przerabia&#263;"
  ]
  node [
    id 80
    label "act"
  ]
  node [
    id 81
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 82
    label "cognizance"
  ]
  node [
    id 83
    label "wiedzie&#263;"
  ]
  node [
    id 84
    label "resonance"
  ]
  node [
    id 85
    label "zjawisko"
  ]
  node [
    id 86
    label "punkt"
  ]
  node [
    id 87
    label "ma&#322;pa_ogoniasta"
  ]
  node [
    id 88
    label "nadnosie"
  ]
  node [
    id 89
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 90
    label "gerezy"
  ]
  node [
    id 91
    label "po&#322;o&#380;enie"
  ]
  node [
    id 92
    label "sprawa"
  ]
  node [
    id 93
    label "ust&#281;p"
  ]
  node [
    id 94
    label "plan"
  ]
  node [
    id 95
    label "obiekt_matematyczny"
  ]
  node [
    id 96
    label "problemat"
  ]
  node [
    id 97
    label "plamka"
  ]
  node [
    id 98
    label "stopie&#324;_pisma"
  ]
  node [
    id 99
    label "jednostka"
  ]
  node [
    id 100
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 101
    label "miejsce"
  ]
  node [
    id 102
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 103
    label "mark"
  ]
  node [
    id 104
    label "chwila"
  ]
  node [
    id 105
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 106
    label "prosta"
  ]
  node [
    id 107
    label "problematyka"
  ]
  node [
    id 108
    label "obiekt"
  ]
  node [
    id 109
    label "zapunktowa&#263;"
  ]
  node [
    id 110
    label "podpunkt"
  ]
  node [
    id 111
    label "wojsko"
  ]
  node [
    id 112
    label "kres"
  ]
  node [
    id 113
    label "przestrze&#324;"
  ]
  node [
    id 114
    label "point"
  ]
  node [
    id 115
    label "pozycja"
  ]
  node [
    id 116
    label "fitofag"
  ]
  node [
    id 117
    label "herbivore"
  ]
  node [
    id 118
    label "zwierz&#281;"
  ]
  node [
    id 119
    label "makakowate"
  ]
  node [
    id 120
    label "dawny"
  ]
  node [
    id 121
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 122
    label "eksprezydent"
  ]
  node [
    id 123
    label "partner"
  ]
  node [
    id 124
    label "rozw&#243;d"
  ]
  node [
    id 125
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 126
    label "wcze&#347;niejszy"
  ]
  node [
    id 127
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 128
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 129
    label "pracownik"
  ]
  node [
    id 130
    label "przedsi&#281;biorca"
  ]
  node [
    id 131
    label "cz&#322;owiek"
  ]
  node [
    id 132
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 133
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 134
    label "kolaborator"
  ]
  node [
    id 135
    label "prowadzi&#263;"
  ]
  node [
    id 136
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 137
    label "sp&#243;lnik"
  ]
  node [
    id 138
    label "aktor"
  ]
  node [
    id 139
    label "uczestniczenie"
  ]
  node [
    id 140
    label "wcze&#347;niej"
  ]
  node [
    id 141
    label "przestarza&#322;y"
  ]
  node [
    id 142
    label "odleg&#322;y"
  ]
  node [
    id 143
    label "przesz&#322;y"
  ]
  node [
    id 144
    label "od_dawna"
  ]
  node [
    id 145
    label "poprzedni"
  ]
  node [
    id 146
    label "dawno"
  ]
  node [
    id 147
    label "d&#322;ugoletni"
  ]
  node [
    id 148
    label "anachroniczny"
  ]
  node [
    id 149
    label "dawniej"
  ]
  node [
    id 150
    label "niegdysiejszy"
  ]
  node [
    id 151
    label "kombatant"
  ]
  node [
    id 152
    label "stary"
  ]
  node [
    id 153
    label "rozstanie"
  ]
  node [
    id 154
    label "ekspartner"
  ]
  node [
    id 155
    label "rozbita_rodzina"
  ]
  node [
    id 156
    label "uniewa&#380;nienie"
  ]
  node [
    id 157
    label "separation"
  ]
  node [
    id 158
    label "prezydent"
  ]
  node [
    id 159
    label "popularny"
  ]
  node [
    id 160
    label "modnie"
  ]
  node [
    id 161
    label "dobry"
  ]
  node [
    id 162
    label "przyst&#281;pny"
  ]
  node [
    id 163
    label "znany"
  ]
  node [
    id 164
    label "popularnie"
  ]
  node [
    id 165
    label "&#322;atwy"
  ]
  node [
    id 166
    label "dobroczynny"
  ]
  node [
    id 167
    label "czw&#243;rka"
  ]
  node [
    id 168
    label "spokojny"
  ]
  node [
    id 169
    label "skuteczny"
  ]
  node [
    id 170
    label "&#347;mieszny"
  ]
  node [
    id 171
    label "mi&#322;y"
  ]
  node [
    id 172
    label "grzeczny"
  ]
  node [
    id 173
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 174
    label "powitanie"
  ]
  node [
    id 175
    label "dobrze"
  ]
  node [
    id 176
    label "ca&#322;y"
  ]
  node [
    id 177
    label "zwrot"
  ]
  node [
    id 178
    label "pomy&#347;lny"
  ]
  node [
    id 179
    label "moralny"
  ]
  node [
    id 180
    label "drogi"
  ]
  node [
    id 181
    label "pozytywny"
  ]
  node [
    id 182
    label "odpowiedni"
  ]
  node [
    id 183
    label "korzystny"
  ]
  node [
    id 184
    label "pos&#322;uszny"
  ]
  node [
    id 185
    label "ustawienie"
  ]
  node [
    id 186
    label "mode"
  ]
  node [
    id 187
    label "przesada"
  ]
  node [
    id 188
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 189
    label "gra"
  ]
  node [
    id 190
    label "u&#322;o&#380;enie"
  ]
  node [
    id 191
    label "ustalenie"
  ]
  node [
    id 192
    label "erection"
  ]
  node [
    id 193
    label "setup"
  ]
  node [
    id 194
    label "spowodowanie"
  ]
  node [
    id 195
    label "erecting"
  ]
  node [
    id 196
    label "rozmieszczenie"
  ]
  node [
    id 197
    label "poustawianie"
  ]
  node [
    id 198
    label "zinterpretowanie"
  ]
  node [
    id 199
    label "porozstawianie"
  ]
  node [
    id 200
    label "czynno&#347;&#263;"
  ]
  node [
    id 201
    label "rola"
  ]
  node [
    id 202
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 203
    label "zmienno&#347;&#263;"
  ]
  node [
    id 204
    label "play"
  ]
  node [
    id 205
    label "rozgrywka"
  ]
  node [
    id 206
    label "apparent_motion"
  ]
  node [
    id 207
    label "wydarzenie"
  ]
  node [
    id 208
    label "contest"
  ]
  node [
    id 209
    label "akcja"
  ]
  node [
    id 210
    label "komplet"
  ]
  node [
    id 211
    label "zabawa"
  ]
  node [
    id 212
    label "zasada"
  ]
  node [
    id 213
    label "rywalizacja"
  ]
  node [
    id 214
    label "zbijany"
  ]
  node [
    id 215
    label "post&#281;powanie"
  ]
  node [
    id 216
    label "game"
  ]
  node [
    id 217
    label "odg&#322;os"
  ]
  node [
    id 218
    label "Pok&#233;mon"
  ]
  node [
    id 219
    label "synteza"
  ]
  node [
    id 220
    label "odtworzenie"
  ]
  node [
    id 221
    label "rekwizyt_do_gry"
  ]
  node [
    id 222
    label "sztuczno&#347;&#263;"
  ]
  node [
    id 223
    label "nadmiar"
  ]
  node [
    id 224
    label "budowa"
  ]
  node [
    id 225
    label "zrzutowy"
  ]
  node [
    id 226
    label "odk&#322;ad"
  ]
  node [
    id 227
    label "chody"
  ]
  node [
    id 228
    label "szaniec"
  ]
  node [
    id 229
    label "wyrobisko"
  ]
  node [
    id 230
    label "kopniak"
  ]
  node [
    id 231
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 232
    label "odwa&#322;"
  ]
  node [
    id 233
    label "grodzisko"
  ]
  node [
    id 234
    label "cios"
  ]
  node [
    id 235
    label "kick"
  ]
  node [
    id 236
    label "kopni&#281;cie"
  ]
  node [
    id 237
    label "&#347;rodkowiec"
  ]
  node [
    id 238
    label "podsadzka"
  ]
  node [
    id 239
    label "obudowa"
  ]
  node [
    id 240
    label "sp&#261;g"
  ]
  node [
    id 241
    label "strop"
  ]
  node [
    id 242
    label "rabowarka"
  ]
  node [
    id 243
    label "opinka"
  ]
  node [
    id 244
    label "stojak_cierny"
  ]
  node [
    id 245
    label "kopalnia"
  ]
  node [
    id 246
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 247
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 248
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 249
    label "immersion"
  ]
  node [
    id 250
    label "umieszczenie"
  ]
  node [
    id 251
    label "las"
  ]
  node [
    id 252
    label "nora"
  ]
  node [
    id 253
    label "pies_my&#347;liwski"
  ]
  node [
    id 254
    label "trasa"
  ]
  node [
    id 255
    label "doj&#347;cie"
  ]
  node [
    id 256
    label "zesp&#243;&#322;"
  ]
  node [
    id 257
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 258
    label "horodyszcze"
  ]
  node [
    id 259
    label "Wyszogr&#243;d"
  ]
  node [
    id 260
    label "usypisko"
  ]
  node [
    id 261
    label "r&#243;w"
  ]
  node [
    id 262
    label "wa&#322;"
  ]
  node [
    id 263
    label "redoubt"
  ]
  node [
    id 264
    label "fortyfikacja"
  ]
  node [
    id 265
    label "mechanika"
  ]
  node [
    id 266
    label "struktura"
  ]
  node [
    id 267
    label "figura"
  ]
  node [
    id 268
    label "miejsce_pracy"
  ]
  node [
    id 269
    label "cecha"
  ]
  node [
    id 270
    label "organ"
  ]
  node [
    id 271
    label "kreacja"
  ]
  node [
    id 272
    label "posesja"
  ]
  node [
    id 273
    label "konstrukcja"
  ]
  node [
    id 274
    label "wjazd"
  ]
  node [
    id 275
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 276
    label "constitution"
  ]
  node [
    id 277
    label "gleba"
  ]
  node [
    id 278
    label "p&#281;d"
  ]
  node [
    id 279
    label "zbi&#243;r"
  ]
  node [
    id 280
    label "ablegier"
  ]
  node [
    id 281
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 282
    label "layer"
  ]
  node [
    id 283
    label "r&#243;j"
  ]
  node [
    id 284
    label "mrowisko"
  ]
  node [
    id 285
    label "sprawdza&#263;"
  ]
  node [
    id 286
    label "ankieter"
  ]
  node [
    id 287
    label "inspect"
  ]
  node [
    id 288
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 289
    label "przes&#322;uchiwa&#263;"
  ]
  node [
    id 290
    label "question"
  ]
  node [
    id 291
    label "examine"
  ]
  node [
    id 292
    label "szpiegowa&#263;"
  ]
  node [
    id 293
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 294
    label "przepytywa&#263;"
  ]
  node [
    id 295
    label "interrogate"
  ]
  node [
    id 296
    label "wypytywa&#263;"
  ]
  node [
    id 297
    label "s&#322;ucha&#263;"
  ]
  node [
    id 298
    label "penis"
  ]
  node [
    id 299
    label "ciul"
  ]
  node [
    id 300
    label "wyzwisko"
  ]
  node [
    id 301
    label "skurwysyn"
  ]
  node [
    id 302
    label "przekle&#324;stwo"
  ]
  node [
    id 303
    label "dupek"
  ]
  node [
    id 304
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 305
    label "ptaszek"
  ]
  node [
    id 306
    label "przyrodzenie"
  ]
  node [
    id 307
    label "shaft"
  ]
  node [
    id 308
    label "fiut"
  ]
  node [
    id 309
    label "idiota"
  ]
  node [
    id 310
    label "leszcz"
  ]
  node [
    id 311
    label "&#322;ajdak"
  ]
  node [
    id 312
    label "fakt"
  ]
  node [
    id 313
    label "strapienie"
  ]
  node [
    id 314
    label "wykrzyknik"
  ]
  node [
    id 315
    label "wulgaryzm"
  ]
  node [
    id 316
    label "bluzg"
  ]
  node [
    id 317
    label "four-letter_word"
  ]
  node [
    id 318
    label "figura_my&#347;li"
  ]
  node [
    id 319
    label "figura_karciana"
  ]
  node [
    id 320
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 321
    label "istota_&#380;ywa"
  ]
  node [
    id 322
    label "cholera"
  ]
  node [
    id 323
    label "wypowied&#378;"
  ]
  node [
    id 324
    label "pies"
  ]
  node [
    id 325
    label "chujowy"
  ]
  node [
    id 326
    label "obelga"
  ]
  node [
    id 327
    label "szmata"
  ]
  node [
    id 328
    label "mie&#263;_miejsce"
  ]
  node [
    id 329
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 330
    label "p&#322;ywa&#263;"
  ]
  node [
    id 331
    label "run"
  ]
  node [
    id 332
    label "bangla&#263;"
  ]
  node [
    id 333
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 334
    label "przebiega&#263;"
  ]
  node [
    id 335
    label "proceed"
  ]
  node [
    id 336
    label "by&#263;"
  ]
  node [
    id 337
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 338
    label "carry"
  ]
  node [
    id 339
    label "bywa&#263;"
  ]
  node [
    id 340
    label "dziama&#263;"
  ]
  node [
    id 341
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 342
    label "stara&#263;_si&#281;"
  ]
  node [
    id 343
    label "para"
  ]
  node [
    id 344
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 345
    label "str&#243;j"
  ]
  node [
    id 346
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 347
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 348
    label "krok"
  ]
  node [
    id 349
    label "tryb"
  ]
  node [
    id 350
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 351
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 352
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 353
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 354
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 355
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 356
    label "przedmiot"
  ]
  node [
    id 357
    label "kontrolowa&#263;"
  ]
  node [
    id 358
    label "sok"
  ]
  node [
    id 359
    label "krew"
  ]
  node [
    id 360
    label "wheel"
  ]
  node [
    id 361
    label "draw"
  ]
  node [
    id 362
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 363
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 364
    label "biec"
  ]
  node [
    id 365
    label "przebywa&#263;"
  ]
  node [
    id 366
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 367
    label "equal"
  ]
  node [
    id 368
    label "trwa&#263;"
  ]
  node [
    id 369
    label "stan"
  ]
  node [
    id 370
    label "obecno&#347;&#263;"
  ]
  node [
    id 371
    label "stand"
  ]
  node [
    id 372
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 373
    label "uczestniczy&#263;"
  ]
  node [
    id 374
    label "inflict"
  ]
  node [
    id 375
    label "&#380;egna&#263;"
  ]
  node [
    id 376
    label "pozosta&#263;"
  ]
  node [
    id 377
    label "pokazywa&#263;_si&#281;"
  ]
  node [
    id 378
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 379
    label "podr&#243;&#380;owa&#263;"
  ]
  node [
    id 380
    label "sterowa&#263;"
  ]
  node [
    id 381
    label "ciecz"
  ]
  node [
    id 382
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 383
    label "mie&#263;"
  ]
  node [
    id 384
    label "m&#243;wi&#263;"
  ]
  node [
    id 385
    label "lata&#263;"
  ]
  node [
    id 386
    label "statek"
  ]
  node [
    id 387
    label "swimming"
  ]
  node [
    id 388
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 389
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 390
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 391
    label "pracowa&#263;"
  ]
  node [
    id 392
    label "sink"
  ]
  node [
    id 393
    label "zanika&#263;"
  ]
  node [
    id 394
    label "pair"
  ]
  node [
    id 395
    label "odparowywanie"
  ]
  node [
    id 396
    label "gaz_cieplarniany"
  ]
  node [
    id 397
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 398
    label "poker"
  ]
  node [
    id 399
    label "moneta"
  ]
  node [
    id 400
    label "parowanie"
  ]
  node [
    id 401
    label "damp"
  ]
  node [
    id 402
    label "nale&#380;e&#263;"
  ]
  node [
    id 403
    label "sztuka"
  ]
  node [
    id 404
    label "odparowanie"
  ]
  node [
    id 405
    label "grupa"
  ]
  node [
    id 406
    label "odparowa&#263;"
  ]
  node [
    id 407
    label "dodatek"
  ]
  node [
    id 408
    label "jednostka_monetarna"
  ]
  node [
    id 409
    label "smoke"
  ]
  node [
    id 410
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 411
    label "odparowywa&#263;"
  ]
  node [
    id 412
    label "uk&#322;ad"
  ]
  node [
    id 413
    label "Albania"
  ]
  node [
    id 414
    label "gaz"
  ]
  node [
    id 415
    label "wyparowanie"
  ]
  node [
    id 416
    label "step"
  ]
  node [
    id 417
    label "tu&#322;&#243;w"
  ]
  node [
    id 418
    label "measurement"
  ]
  node [
    id 419
    label "action"
  ]
  node [
    id 420
    label "czyn"
  ]
  node [
    id 421
    label "ruch"
  ]
  node [
    id 422
    label "passus"
  ]
  node [
    id 423
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 424
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 425
    label "skejt"
  ]
  node [
    id 426
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 427
    label "pace"
  ]
  node [
    id 428
    label "ko&#322;o"
  ]
  node [
    id 429
    label "spos&#243;b"
  ]
  node [
    id 430
    label "modalno&#347;&#263;"
  ]
  node [
    id 431
    label "z&#261;b"
  ]
  node [
    id 432
    label "kategoria_gramatyczna"
  ]
  node [
    id 433
    label "skala"
  ]
  node [
    id 434
    label "funkcjonowa&#263;"
  ]
  node [
    id 435
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 436
    label "koniugacja"
  ]
  node [
    id 437
    label "gorset"
  ]
  node [
    id 438
    label "zrzucenie"
  ]
  node [
    id 439
    label "znoszenie"
  ]
  node [
    id 440
    label "kr&#243;j"
  ]
  node [
    id 441
    label "ubranie_si&#281;"
  ]
  node [
    id 442
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 443
    label "pochodzi&#263;"
  ]
  node [
    id 444
    label "zrzuci&#263;"
  ]
  node [
    id 445
    label "pasmanteria"
  ]
  node [
    id 446
    label "pochodzenie"
  ]
  node [
    id 447
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 448
    label "odzie&#380;"
  ]
  node [
    id 449
    label "wyko&#324;czenie"
  ]
  node [
    id 450
    label "nosi&#263;"
  ]
  node [
    id 451
    label "w&#322;o&#380;enie"
  ]
  node [
    id 452
    label "garderoba"
  ]
  node [
    id 453
    label "odziewek"
  ]
  node [
    id 454
    label "przekazywa&#263;"
  ]
  node [
    id 455
    label "ubiera&#263;"
  ]
  node [
    id 456
    label "odziewa&#263;"
  ]
  node [
    id 457
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 458
    label "obleka&#263;"
  ]
  node [
    id 459
    label "inspirowa&#263;"
  ]
  node [
    id 460
    label "pour"
  ]
  node [
    id 461
    label "introduce"
  ]
  node [
    id 462
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 463
    label "wzbudza&#263;"
  ]
  node [
    id 464
    label "place"
  ]
  node [
    id 465
    label "wpaja&#263;"
  ]
  node [
    id 466
    label "cover"
  ]
  node [
    id 467
    label "popyt"
  ]
  node [
    id 468
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 469
    label "rozumie&#263;"
  ]
  node [
    id 470
    label "szczeka&#263;"
  ]
  node [
    id 471
    label "rozmawia&#263;"
  ]
  node [
    id 472
    label "naczelne"
  ]
  node [
    id 473
    label "monkey"
  ]
  node [
    id 474
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 475
    label "prostytutka"
  ]
  node [
    id 476
    label "z&#322;o&#347;liwy"
  ]
  node [
    id 477
    label "humanoid"
  ]
  node [
    id 478
    label "dokucza&#263;"
  ]
  node [
    id 479
    label "kurwa"
  ]
  node [
    id 480
    label "pigalak"
  ]
  node [
    id 481
    label "dziewczyna_lekkich_obyczaj&#243;w"
  ]
  node [
    id 482
    label "rozpustnica"
  ]
  node [
    id 483
    label "diva"
  ]
  node [
    id 484
    label "jawnogrzesznica"
  ]
  node [
    id 485
    label "ludzko&#347;&#263;"
  ]
  node [
    id 486
    label "asymilowanie"
  ]
  node [
    id 487
    label "wapniak"
  ]
  node [
    id 488
    label "asymilowa&#263;"
  ]
  node [
    id 489
    label "os&#322;abia&#263;"
  ]
  node [
    id 490
    label "posta&#263;"
  ]
  node [
    id 491
    label "hominid"
  ]
  node [
    id 492
    label "podw&#322;adny"
  ]
  node [
    id 493
    label "os&#322;abianie"
  ]
  node [
    id 494
    label "g&#322;owa"
  ]
  node [
    id 495
    label "portrecista"
  ]
  node [
    id 496
    label "dwun&#243;g"
  ]
  node [
    id 497
    label "profanum"
  ]
  node [
    id 498
    label "mikrokosmos"
  ]
  node [
    id 499
    label "nasada"
  ]
  node [
    id 500
    label "duch"
  ]
  node [
    id 501
    label "antropochoria"
  ]
  node [
    id 502
    label "osoba"
  ]
  node [
    id 503
    label "wz&#243;r"
  ]
  node [
    id 504
    label "senior"
  ]
  node [
    id 505
    label "oddzia&#322;ywanie"
  ]
  node [
    id 506
    label "Adam"
  ]
  node [
    id 507
    label "homo_sapiens"
  ]
  node [
    id 508
    label "polifag"
  ]
  node [
    id 509
    label "ssak_&#380;yworodny"
  ]
  node [
    id 510
    label "ssaki_wy&#380;sze"
  ]
  node [
    id 511
    label "kuk&#322;a"
  ]
  node [
    id 512
    label "istota"
  ]
  node [
    id 513
    label "niemi&#322;y"
  ]
  node [
    id 514
    label "przekl&#281;ty"
  ]
  node [
    id 515
    label "ci&#281;&#380;ki"
  ]
  node [
    id 516
    label "celowy"
  ]
  node [
    id 517
    label "krytyczny"
  ]
  node [
    id 518
    label "prze&#347;miewczy"
  ]
  node [
    id 519
    label "z&#322;o&#347;liwie"
  ]
  node [
    id 520
    label "dojmowa&#263;"
  ]
  node [
    id 521
    label "gra&#263;_na_nerwach"
  ]
  node [
    id 522
    label "z&#322;o&#347;liwiec"
  ]
  node [
    id 523
    label "irytowa&#263;"
  ]
  node [
    id 524
    label "annoy"
  ]
  node [
    id 525
    label "przykrzy&#263;_si&#281;"
  ]
  node [
    id 526
    label "trouble_oneself"
  ]
  node [
    id 527
    label "kciuk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 11
    target 316
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 188
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 37
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 256
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 407
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 12
    target 411
  ]
  edge [
    source 12
    target 412
  ]
  edge [
    source 12
    target 413
  ]
  edge [
    source 12
    target 414
  ]
  edge [
    source 12
    target 415
  ]
  edge [
    source 12
    target 416
  ]
  edge [
    source 12
    target 417
  ]
  edge [
    source 12
    target 418
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 269
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 27
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 212
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
]
