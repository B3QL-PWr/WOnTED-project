graph [
  node [
    id 0
    label "niesamowity"
    origin "text"
  ]
  node [
    id 1
    label "rozlewisko"
    origin "text"
  ]
  node [
    id 2
    label "rzeczny"
    origin "text"
  ]
  node [
    id 3
    label "tajemniczy"
    origin "text"
  ]
  node [
    id 4
    label "ruina"
    origin "text"
  ]
  node [
    id 5
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 6
    label "niespotykany"
    origin "text"
  ]
  node [
    id 7
    label "nigdzie"
    origin "text"
  ]
  node [
    id 8
    label "indziej"
    origin "text"
  ]
  node [
    id 9
    label "zabytek"
    origin "text"
  ]
  node [
    id 10
    label "wiele"
    origin "text"
  ]
  node [
    id 11
    label "naturalny"
    origin "text"
  ]
  node [
    id 12
    label "zjawiskowy"
    origin "text"
  ]
  node [
    id 13
    label "atrakcja"
    origin "text"
  ]
  node [
    id 14
    label "niezwyk&#322;y"
  ]
  node [
    id 15
    label "niesamowicie"
  ]
  node [
    id 16
    label "inny"
  ]
  node [
    id 17
    label "niezwykle"
  ]
  node [
    id 18
    label "bardzo"
  ]
  node [
    id 19
    label "niesamowito"
  ]
  node [
    id 20
    label "zbiornik_wodny"
  ]
  node [
    id 21
    label "wodny"
  ]
  node [
    id 22
    label "specjalny"
  ]
  node [
    id 23
    label "nastrojowy"
  ]
  node [
    id 24
    label "niejednoznaczny"
  ]
  node [
    id 25
    label "skryty"
  ]
  node [
    id 26
    label "tajemniczo"
  ]
  node [
    id 27
    label "ciekawy"
  ]
  node [
    id 28
    label "dziwny"
  ]
  node [
    id 29
    label "intryguj&#261;cy"
  ]
  node [
    id 30
    label "nieznany"
  ]
  node [
    id 31
    label "niejednolity"
  ]
  node [
    id 32
    label "niejednoznacznie"
  ]
  node [
    id 33
    label "intryguj&#261;co"
  ]
  node [
    id 34
    label "introwertyczny"
  ]
  node [
    id 35
    label "kryjomy"
  ]
  node [
    id 36
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 37
    label "potajemny"
  ]
  node [
    id 38
    label "skrycie"
  ]
  node [
    id 39
    label "cz&#322;owiek"
  ]
  node [
    id 40
    label "indagator"
  ]
  node [
    id 41
    label "swoisty"
  ]
  node [
    id 42
    label "interesuj&#261;cy"
  ]
  node [
    id 43
    label "nietuzinkowy"
  ]
  node [
    id 44
    label "ciekawie"
  ]
  node [
    id 45
    label "interesowanie"
  ]
  node [
    id 46
    label "ch&#281;tny"
  ]
  node [
    id 47
    label "atmospheric"
  ]
  node [
    id 48
    label "nastrojowo"
  ]
  node [
    id 49
    label "dziwy"
  ]
  node [
    id 50
    label "dziwnie"
  ]
  node [
    id 51
    label "inscrutably"
  ]
  node [
    id 52
    label "cryptically"
  ]
  node [
    id 53
    label "stan"
  ]
  node [
    id 54
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 55
    label "descent"
  ]
  node [
    id 56
    label "trace"
  ]
  node [
    id 57
    label "obiekt"
  ]
  node [
    id 58
    label "&#347;wiadectwo"
  ]
  node [
    id 59
    label "reszta"
  ]
  node [
    id 60
    label "Arakan"
  ]
  node [
    id 61
    label "Teksas"
  ]
  node [
    id 62
    label "Georgia"
  ]
  node [
    id 63
    label "Maryland"
  ]
  node [
    id 64
    label "warstwa"
  ]
  node [
    id 65
    label "Luizjana"
  ]
  node [
    id 66
    label "Massachusetts"
  ]
  node [
    id 67
    label "Michigan"
  ]
  node [
    id 68
    label "by&#263;"
  ]
  node [
    id 69
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 70
    label "samopoczucie"
  ]
  node [
    id 71
    label "Floryda"
  ]
  node [
    id 72
    label "Ohio"
  ]
  node [
    id 73
    label "Alaska"
  ]
  node [
    id 74
    label "Nowy_Meksyk"
  ]
  node [
    id 75
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 76
    label "wci&#281;cie"
  ]
  node [
    id 77
    label "Kansas"
  ]
  node [
    id 78
    label "Alabama"
  ]
  node [
    id 79
    label "miejsce"
  ]
  node [
    id 80
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 81
    label "Kalifornia"
  ]
  node [
    id 82
    label "Wirginia"
  ]
  node [
    id 83
    label "punkt"
  ]
  node [
    id 84
    label "Nowy_York"
  ]
  node [
    id 85
    label "Waszyngton"
  ]
  node [
    id 86
    label "Pensylwania"
  ]
  node [
    id 87
    label "wektor"
  ]
  node [
    id 88
    label "Hawaje"
  ]
  node [
    id 89
    label "state"
  ]
  node [
    id 90
    label "poziom"
  ]
  node [
    id 91
    label "jednostka_administracyjna"
  ]
  node [
    id 92
    label "Illinois"
  ]
  node [
    id 93
    label "Oklahoma"
  ]
  node [
    id 94
    label "Jukatan"
  ]
  node [
    id 95
    label "Arizona"
  ]
  node [
    id 96
    label "ilo&#347;&#263;"
  ]
  node [
    id 97
    label "Oregon"
  ]
  node [
    id 98
    label "shape"
  ]
  node [
    id 99
    label "Goa"
  ]
  node [
    id 100
    label "nawa"
  ]
  node [
    id 101
    label "prezbiterium"
  ]
  node [
    id 102
    label "nerwica_eklezjogenna"
  ]
  node [
    id 103
    label "kropielnica"
  ]
  node [
    id 104
    label "ub&#322;agalnia"
  ]
  node [
    id 105
    label "zakrystia"
  ]
  node [
    id 106
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 107
    label "kult"
  ]
  node [
    id 108
    label "church"
  ]
  node [
    id 109
    label "organizacja_religijna"
  ]
  node [
    id 110
    label "wsp&#243;lnota"
  ]
  node [
    id 111
    label "kruchta"
  ]
  node [
    id 112
    label "dom"
  ]
  node [
    id 113
    label "Ska&#322;ka"
  ]
  node [
    id 114
    label "zwi&#261;zanie"
  ]
  node [
    id 115
    label "Walencja"
  ]
  node [
    id 116
    label "society"
  ]
  node [
    id 117
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 118
    label "partnership"
  ]
  node [
    id 119
    label "Ba&#322;kany"
  ]
  node [
    id 120
    label "zwi&#261;zek"
  ]
  node [
    id 121
    label "wi&#261;zanie"
  ]
  node [
    id 122
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 123
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 124
    label "zwi&#261;za&#263;"
  ]
  node [
    id 125
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 126
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 127
    label "marriage"
  ]
  node [
    id 128
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 129
    label "bratnia_dusza"
  ]
  node [
    id 130
    label "Skandynawia"
  ]
  node [
    id 131
    label "podobie&#324;stwo"
  ]
  node [
    id 132
    label "przybytek"
  ]
  node [
    id 133
    label "budynek"
  ]
  node [
    id 134
    label "siedlisko"
  ]
  node [
    id 135
    label "poj&#281;cie"
  ]
  node [
    id 136
    label "substancja_mieszkaniowa"
  ]
  node [
    id 137
    label "rodzina"
  ]
  node [
    id 138
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 139
    label "siedziba"
  ]
  node [
    id 140
    label "dom_rodzinny"
  ]
  node [
    id 141
    label "garderoba"
  ]
  node [
    id 142
    label "fratria"
  ]
  node [
    id 143
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 144
    label "stead"
  ]
  node [
    id 145
    label "instytucja"
  ]
  node [
    id 146
    label "grupa"
  ]
  node [
    id 147
    label "wiecha"
  ]
  node [
    id 148
    label "postawa"
  ]
  node [
    id 149
    label "translacja"
  ]
  node [
    id 150
    label "obrz&#281;d"
  ]
  node [
    id 151
    label "uwielbienie"
  ]
  node [
    id 152
    label "religia"
  ]
  node [
    id 153
    label "egzegeta"
  ]
  node [
    id 154
    label "worship"
  ]
  node [
    id 155
    label "przedsionek"
  ]
  node [
    id 156
    label "babiniec"
  ]
  node [
    id 157
    label "zesp&#243;&#322;"
  ]
  node [
    id 158
    label "korpus"
  ]
  node [
    id 159
    label "naczynie_na_wod&#281;_&#347;wi&#281;con&#261;"
  ]
  node [
    id 160
    label "paramenty"
  ]
  node [
    id 161
    label "pomieszczenie"
  ]
  node [
    id 162
    label "tabernakulum"
  ]
  node [
    id 163
    label "o&#322;tarz"
  ]
  node [
    id 164
    label "&#347;rodowisko"
  ]
  node [
    id 165
    label "stalle"
  ]
  node [
    id 166
    label "duchowie&#324;stwo"
  ]
  node [
    id 167
    label "lampka_wieczysta"
  ]
  node [
    id 168
    label "przedmiot"
  ]
  node [
    id 169
    label "starzyzna"
  ]
  node [
    id 170
    label "keepsake"
  ]
  node [
    id 171
    label "zbi&#243;r"
  ]
  node [
    id 172
    label "anachronism"
  ]
  node [
    id 173
    label "relikt"
  ]
  node [
    id 174
    label "discipline"
  ]
  node [
    id 175
    label "zboczy&#263;"
  ]
  node [
    id 176
    label "w&#261;tek"
  ]
  node [
    id 177
    label "kultura"
  ]
  node [
    id 178
    label "entity"
  ]
  node [
    id 179
    label "sponiewiera&#263;"
  ]
  node [
    id 180
    label "zboczenie"
  ]
  node [
    id 181
    label "zbaczanie"
  ]
  node [
    id 182
    label "charakter"
  ]
  node [
    id 183
    label "thing"
  ]
  node [
    id 184
    label "om&#243;wi&#263;"
  ]
  node [
    id 185
    label "tre&#347;&#263;"
  ]
  node [
    id 186
    label "element"
  ]
  node [
    id 187
    label "kr&#261;&#380;enie"
  ]
  node [
    id 188
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 189
    label "istota"
  ]
  node [
    id 190
    label "zbacza&#263;"
  ]
  node [
    id 191
    label "om&#243;wienie"
  ]
  node [
    id 192
    label "rzecz"
  ]
  node [
    id 193
    label "tematyka"
  ]
  node [
    id 194
    label "omawianie"
  ]
  node [
    id 195
    label "omawia&#263;"
  ]
  node [
    id 196
    label "robienie"
  ]
  node [
    id 197
    label "program_nauczania"
  ]
  node [
    id 198
    label "sponiewieranie"
  ]
  node [
    id 199
    label "certificate"
  ]
  node [
    id 200
    label "o&#347;wiadczenie"
  ]
  node [
    id 201
    label "dokument"
  ]
  node [
    id 202
    label "za&#347;wiadczenie"
  ]
  node [
    id 203
    label "dow&#243;d"
  ]
  node [
    id 204
    label "promocja"
  ]
  node [
    id 205
    label "du&#380;y"
  ]
  node [
    id 206
    label "wiela"
  ]
  node [
    id 207
    label "du&#380;o"
  ]
  node [
    id 208
    label "znaczny"
  ]
  node [
    id 209
    label "wa&#380;ny"
  ]
  node [
    id 210
    label "niema&#322;o"
  ]
  node [
    id 211
    label "prawdziwy"
  ]
  node [
    id 212
    label "rozwini&#281;ty"
  ]
  node [
    id 213
    label "doros&#322;y"
  ]
  node [
    id 214
    label "dorodny"
  ]
  node [
    id 215
    label "naturalnie"
  ]
  node [
    id 216
    label "zrozumia&#322;y"
  ]
  node [
    id 217
    label "prawy"
  ]
  node [
    id 218
    label "neutralny"
  ]
  node [
    id 219
    label "zwyczajny"
  ]
  node [
    id 220
    label "normalny"
  ]
  node [
    id 221
    label "immanentny"
  ]
  node [
    id 222
    label "rzeczywisty"
  ]
  node [
    id 223
    label "bezsporny"
  ]
  node [
    id 224
    label "szczery"
  ]
  node [
    id 225
    label "pierwotny"
  ]
  node [
    id 226
    label "organicznie"
  ]
  node [
    id 227
    label "zdr&#243;w"
  ]
  node [
    id 228
    label "zwykle"
  ]
  node [
    id 229
    label "prawid&#322;owy"
  ]
  node [
    id 230
    label "zwyczajnie"
  ]
  node [
    id 231
    label "cz&#281;sty"
  ]
  node [
    id 232
    label "normalnie"
  ]
  node [
    id 233
    label "przeci&#281;tny"
  ]
  node [
    id 234
    label "okre&#347;lony"
  ]
  node [
    id 235
    label "oczywisty"
  ]
  node [
    id 236
    label "sensowny"
  ]
  node [
    id 237
    label "pojmowalny"
  ]
  node [
    id 238
    label "prosty"
  ]
  node [
    id 239
    label "rozja&#347;nienie"
  ]
  node [
    id 240
    label "zrozumiale"
  ]
  node [
    id 241
    label "wyja&#347;nienie"
  ]
  node [
    id 242
    label "t&#322;umaczenie"
  ]
  node [
    id 243
    label "przeja&#347;nienie_si&#281;"
  ]
  node [
    id 244
    label "wyja&#347;nienie_si&#281;"
  ]
  node [
    id 245
    label "uzasadniony"
  ]
  node [
    id 246
    label "rozja&#347;nianie"
  ]
  node [
    id 247
    label "wyja&#347;nianie_si&#281;"
  ]
  node [
    id 248
    label "przeja&#347;nianie_si&#281;"
  ]
  node [
    id 249
    label "mo&#380;liwy"
  ]
  node [
    id 250
    label "realnie"
  ]
  node [
    id 251
    label "podobny"
  ]
  node [
    id 252
    label "ewidentny"
  ]
  node [
    id 253
    label "akceptowalny"
  ]
  node [
    id 254
    label "bezspornie"
  ]
  node [
    id 255
    label "obiektywny"
  ]
  node [
    id 256
    label "clear"
  ]
  node [
    id 257
    label "immanentnie"
  ]
  node [
    id 258
    label "nieod&#322;&#261;czny"
  ]
  node [
    id 259
    label "czysty"
  ]
  node [
    id 260
    label "prostoduszny"
  ]
  node [
    id 261
    label "s&#322;uszny"
  ]
  node [
    id 262
    label "uczciwy"
  ]
  node [
    id 263
    label "szczodry"
  ]
  node [
    id 264
    label "szczerze"
  ]
  node [
    id 265
    label "przekonuj&#261;cy"
  ]
  node [
    id 266
    label "szczyry"
  ]
  node [
    id 267
    label "neutralizowanie"
  ]
  node [
    id 268
    label "niestronniczy"
  ]
  node [
    id 269
    label "zneutralizowanie"
  ]
  node [
    id 270
    label "swobodny"
  ]
  node [
    id 271
    label "bezstronnie"
  ]
  node [
    id 272
    label "neutralnie"
  ]
  node [
    id 273
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 274
    label "bierny"
  ]
  node [
    id 275
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 276
    label "oswojony"
  ]
  node [
    id 277
    label "na&#322;o&#380;ny"
  ]
  node [
    id 278
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 279
    label "pierwotnie"
  ]
  node [
    id 280
    label "podstawowy"
  ]
  node [
    id 281
    label "g&#322;&#243;wny"
  ]
  node [
    id 282
    label "dziewiczy"
  ]
  node [
    id 283
    label "dziki"
  ]
  node [
    id 284
    label "pradawny"
  ]
  node [
    id 285
    label "pocz&#261;tkowy"
  ]
  node [
    id 286
    label "prymarnie"
  ]
  node [
    id 287
    label "bezpo&#347;redni"
  ]
  node [
    id 288
    label "podobnie"
  ]
  node [
    id 289
    label "na_prawo"
  ]
  node [
    id 290
    label "z_prawa"
  ]
  node [
    id 291
    label "w_prawo"
  ]
  node [
    id 292
    label "moralny"
  ]
  node [
    id 293
    label "legalny"
  ]
  node [
    id 294
    label "cnotliwy"
  ]
  node [
    id 295
    label "chwalebny"
  ]
  node [
    id 296
    label "prawicowy"
  ]
  node [
    id 297
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 298
    label "zacny"
  ]
  node [
    id 299
    label "zgodnie_z_prawem"
  ]
  node [
    id 300
    label "z_natury_rzeczy"
  ]
  node [
    id 301
    label "na&#347;ladowczo"
  ]
  node [
    id 302
    label "trwale"
  ]
  node [
    id 303
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 304
    label "organiczny"
  ]
  node [
    id 305
    label "nieodparcie"
  ]
  node [
    id 306
    label "pi&#281;kny"
  ]
  node [
    id 307
    label "zjawiskowo"
  ]
  node [
    id 308
    label "z&#322;y"
  ]
  node [
    id 309
    label "cudowny"
  ]
  node [
    id 310
    label "wzruszaj&#261;cy"
  ]
  node [
    id 311
    label "pi&#281;knienie"
  ]
  node [
    id 312
    label "dobry"
  ]
  node [
    id 313
    label "gor&#261;cy"
  ]
  node [
    id 314
    label "skandaliczny"
  ]
  node [
    id 315
    label "pi&#281;knie"
  ]
  node [
    id 316
    label "wypi&#281;knienie"
  ]
  node [
    id 317
    label "po&#380;&#261;dany"
  ]
  node [
    id 318
    label "wspania&#322;y"
  ]
  node [
    id 319
    label "szlachetnie"
  ]
  node [
    id 320
    label "okaza&#322;y"
  ]
  node [
    id 321
    label "urozmaicenie"
  ]
  node [
    id 322
    label "ciekawostka"
  ]
  node [
    id 323
    label "sensacja"
  ]
  node [
    id 324
    label "urok"
  ]
  node [
    id 325
    label "co&#347;"
  ]
  node [
    id 326
    label "differentiation"
  ]
  node [
    id 327
    label "podzielenie"
  ]
  node [
    id 328
    label "nadanie"
  ]
  node [
    id 329
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 330
    label "informacja"
  ]
  node [
    id 331
    label "novum"
  ]
  node [
    id 332
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 333
    label "disclosure"
  ]
  node [
    id 334
    label "zamieszanie"
  ]
  node [
    id 335
    label "podekscytowanie"
  ]
  node [
    id 336
    label "rozg&#322;os"
  ]
  node [
    id 337
    label "niespodzianka"
  ]
  node [
    id 338
    label "obiekt_matematyczny"
  ]
  node [
    id 339
    label "stopie&#324;_pisma"
  ]
  node [
    id 340
    label "pozycja"
  ]
  node [
    id 341
    label "problemat"
  ]
  node [
    id 342
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 343
    label "point"
  ]
  node [
    id 344
    label "plamka"
  ]
  node [
    id 345
    label "przestrze&#324;"
  ]
  node [
    id 346
    label "mark"
  ]
  node [
    id 347
    label "ust&#281;p"
  ]
  node [
    id 348
    label "po&#322;o&#380;enie"
  ]
  node [
    id 349
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 350
    label "kres"
  ]
  node [
    id 351
    label "plan"
  ]
  node [
    id 352
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 353
    label "chwila"
  ]
  node [
    id 354
    label "podpunkt"
  ]
  node [
    id 355
    label "jednostka"
  ]
  node [
    id 356
    label "sprawa"
  ]
  node [
    id 357
    label "problematyka"
  ]
  node [
    id 358
    label "prosta"
  ]
  node [
    id 359
    label "wojsko"
  ]
  node [
    id 360
    label "zapunktowa&#263;"
  ]
  node [
    id 361
    label "attraction"
  ]
  node [
    id 362
    label "cecha"
  ]
  node [
    id 363
    label "agreeableness"
  ]
  node [
    id 364
    label "czar"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 168
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 169
  ]
  edge [
    source 9
    target 170
  ]
  edge [
    source 9
    target 171
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 172
  ]
  edge [
    source 9
    target 173
  ]
  edge [
    source 9
    target 174
  ]
  edge [
    source 9
    target 175
  ]
  edge [
    source 9
    target 176
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 179
  ]
  edge [
    source 9
    target 180
  ]
  edge [
    source 9
    target 181
  ]
  edge [
    source 9
    target 182
  ]
  edge [
    source 9
    target 183
  ]
  edge [
    source 9
    target 184
  ]
  edge [
    source 9
    target 185
  ]
  edge [
    source 9
    target 186
  ]
  edge [
    source 9
    target 187
  ]
  edge [
    source 9
    target 188
  ]
  edge [
    source 9
    target 189
  ]
  edge [
    source 9
    target 190
  ]
  edge [
    source 9
    target 191
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 195
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 197
  ]
  edge [
    source 9
    target 198
  ]
  edge [
    source 9
    target 199
  ]
  edge [
    source 9
    target 200
  ]
  edge [
    source 9
    target 201
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 203
  ]
  edge [
    source 9
    target 204
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 215
  ]
  edge [
    source 11
    target 216
  ]
  edge [
    source 11
    target 217
  ]
  edge [
    source 11
    target 218
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 220
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 222
  ]
  edge [
    source 11
    target 223
  ]
  edge [
    source 11
    target 224
  ]
  edge [
    source 11
    target 225
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 211
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 16
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 57
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 13
    target 344
  ]
  edge [
    source 13
    target 345
  ]
  edge [
    source 13
    target 346
  ]
  edge [
    source 13
    target 347
  ]
  edge [
    source 13
    target 348
  ]
  edge [
    source 13
    target 349
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 350
  ]
  edge [
    source 13
    target 351
  ]
  edge [
    source 13
    target 352
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 354
  ]
  edge [
    source 13
    target 355
  ]
  edge [
    source 13
    target 356
  ]
  edge [
    source 13
    target 357
  ]
  edge [
    source 13
    target 358
  ]
  edge [
    source 13
    target 359
  ]
  edge [
    source 13
    target 360
  ]
  edge [
    source 13
    target 361
  ]
  edge [
    source 13
    target 362
  ]
  edge [
    source 13
    target 363
  ]
  edge [
    source 13
    target 364
  ]
]
