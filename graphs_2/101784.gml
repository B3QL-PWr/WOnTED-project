graph [
  node [
    id 0
    label "czeczenia"
    origin "text"
  ]
  node [
    id 1
    label "nazwa"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "cz&#281;sto"
    origin "text"
  ]
  node [
    id 4
    label "przewija&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "gazeta"
    origin "text"
  ]
  node [
    id 7
    label "telewizja"
    origin "text"
  ]
  node [
    id 8
    label "szary"
    origin "text"
  ]
  node [
    id 9
    label "obywatel"
    origin "text"
  ]
  node [
    id 10
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "niewiele"
    origin "text"
  ]
  node [
    id 12
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "polak"
    origin "text"
  ]
  node [
    id 14
    label "nawet"
    origin "text"
  ]
  node [
    id 15
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 16
    label "gdzie"
    origin "text"
  ]
  node [
    id 17
    label "ten"
    origin "text"
  ]
  node [
    id 18
    label "kraj"
    origin "text"
  ]
  node [
    id 19
    label "le&#380;"
    origin "text"
  ]
  node [
    id 20
    label "dlaczego"
    origin "text"
  ]
  node [
    id 21
    label "walczy&#263;"
    origin "text"
  ]
  node [
    id 22
    label "rosja"
    origin "text"
  ]
  node [
    id 23
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 24
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 25
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 26
    label "przybli&#380;y&#263;"
    origin "text"
  ]
  node [
    id 27
    label "problematyka"
    origin "text"
  ]
  node [
    id 28
    label "kaukaz"
    origin "text"
  ]
  node [
    id 29
    label "napi&#281;cie"
    origin "text"
  ]
  node [
    id 30
    label "tamtejszy"
    origin "text"
  ]
  node [
    id 31
    label "republika"
    origin "text"
  ]
  node [
    id 32
    label "term"
  ]
  node [
    id 33
    label "wezwanie"
  ]
  node [
    id 34
    label "patron"
  ]
  node [
    id 35
    label "leksem"
  ]
  node [
    id 36
    label "wordnet"
  ]
  node [
    id 37
    label "wypowiedzenie"
  ]
  node [
    id 38
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 39
    label "morfem"
  ]
  node [
    id 40
    label "s&#322;ownictwo"
  ]
  node [
    id 41
    label "wykrzyknik"
  ]
  node [
    id 42
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 43
    label "pole_semantyczne"
  ]
  node [
    id 44
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 45
    label "pisanie_si&#281;"
  ]
  node [
    id 46
    label "nag&#322;os"
  ]
  node [
    id 47
    label "wyg&#322;os"
  ]
  node [
    id 48
    label "jednostka_leksykalna"
  ]
  node [
    id 49
    label "nakaz"
  ]
  node [
    id 50
    label "pismo_urz&#281;dowe"
  ]
  node [
    id 51
    label "zwo&#322;anie_si&#281;"
  ]
  node [
    id 52
    label "wst&#281;p"
  ]
  node [
    id 53
    label "pro&#347;ba"
  ]
  node [
    id 54
    label "nakazanie"
  ]
  node [
    id 55
    label "admonition"
  ]
  node [
    id 56
    label "summons"
  ]
  node [
    id 57
    label "poproszenie"
  ]
  node [
    id 58
    label "bid"
  ]
  node [
    id 59
    label "apostrofa"
  ]
  node [
    id 60
    label "zach&#281;cenie"
  ]
  node [
    id 61
    label "poinformowanie"
  ]
  node [
    id 62
    label "&#322;uska"
  ]
  node [
    id 63
    label "opiekun"
  ]
  node [
    id 64
    label "patrycjusz"
  ]
  node [
    id 65
    label "prawnik"
  ]
  node [
    id 66
    label "nab&#243;j"
  ]
  node [
    id 67
    label "&#347;wi&#281;ty"
  ]
  node [
    id 68
    label "zmar&#322;y"
  ]
  node [
    id 69
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 70
    label "&#347;w"
  ]
  node [
    id 71
    label "szablon"
  ]
  node [
    id 72
    label "poj&#281;cie"
  ]
  node [
    id 73
    label "cz&#281;sty"
  ]
  node [
    id 74
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 75
    label "wielokrotnie"
  ]
  node [
    id 76
    label "robi&#263;"
  ]
  node [
    id 77
    label "owija&#263;"
  ]
  node [
    id 78
    label "pielucha"
  ]
  node [
    id 79
    label "powodowa&#263;"
  ]
  node [
    id 80
    label "kaseta"
  ]
  node [
    id 81
    label "przebiera&#263;"
  ]
  node [
    id 82
    label "swathe"
  ]
  node [
    id 83
    label "otacza&#263;"
  ]
  node [
    id 84
    label "extort"
  ]
  node [
    id 85
    label "mie&#263;_miejsce"
  ]
  node [
    id 86
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 87
    label "motywowa&#263;"
  ]
  node [
    id 88
    label "act"
  ]
  node [
    id 89
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 90
    label "organizowa&#263;"
  ]
  node [
    id 91
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 92
    label "czyni&#263;"
  ]
  node [
    id 93
    label "give"
  ]
  node [
    id 94
    label "stylizowa&#263;"
  ]
  node [
    id 95
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 96
    label "falowa&#263;"
  ]
  node [
    id 97
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 98
    label "peddle"
  ]
  node [
    id 99
    label "praca"
  ]
  node [
    id 100
    label "wydala&#263;"
  ]
  node [
    id 101
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 102
    label "tentegowa&#263;"
  ]
  node [
    id 103
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 104
    label "urz&#261;dza&#263;"
  ]
  node [
    id 105
    label "oszukiwa&#263;"
  ]
  node [
    id 106
    label "work"
  ]
  node [
    id 107
    label "ukazywa&#263;"
  ]
  node [
    id 108
    label "przerabia&#263;"
  ]
  node [
    id 109
    label "post&#281;powa&#263;"
  ]
  node [
    id 110
    label "dress"
  ]
  node [
    id 111
    label "przemienia&#263;"
  ]
  node [
    id 112
    label "trim"
  ]
  node [
    id 113
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 114
    label "ceregieli&#263;_si&#281;"
  ]
  node [
    id 115
    label "zmienia&#263;"
  ]
  node [
    id 116
    label "upodabnia&#263;"
  ]
  node [
    id 117
    label "cull"
  ]
  node [
    id 118
    label "zastanawia&#263;_si&#281;"
  ]
  node [
    id 119
    label "rusza&#263;"
  ]
  node [
    id 120
    label "przerzuca&#263;"
  ]
  node [
    id 121
    label "ta&#347;ma_magnetyczna"
  ]
  node [
    id 122
    label "skrzynka"
  ]
  node [
    id 123
    label "no&#347;nik_danych"
  ]
  node [
    id 124
    label "pude&#322;ko"
  ]
  node [
    id 125
    label "przewijanie"
  ]
  node [
    id 126
    label "coffin"
  ]
  node [
    id 127
    label "opakowanie"
  ]
  node [
    id 128
    label "wyprawka"
  ]
  node [
    id 129
    label "wytw&#243;r"
  ]
  node [
    id 130
    label "diaper"
  ]
  node [
    id 131
    label "powijak"
  ]
  node [
    id 132
    label "tytu&#322;"
  ]
  node [
    id 133
    label "redakcja"
  ]
  node [
    id 134
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 135
    label "czasopismo"
  ]
  node [
    id 136
    label "prasa"
  ]
  node [
    id 137
    label "egzemplarz"
  ]
  node [
    id 138
    label "psychotest"
  ]
  node [
    id 139
    label "pismo"
  ]
  node [
    id 140
    label "communication"
  ]
  node [
    id 141
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 142
    label "wk&#322;ad"
  ]
  node [
    id 143
    label "zajawka"
  ]
  node [
    id 144
    label "ok&#322;adka"
  ]
  node [
    id 145
    label "Zwrotnica"
  ]
  node [
    id 146
    label "dzia&#322;"
  ]
  node [
    id 147
    label "redaktor"
  ]
  node [
    id 148
    label "radio"
  ]
  node [
    id 149
    label "zesp&#243;&#322;"
  ]
  node [
    id 150
    label "siedziba"
  ]
  node [
    id 151
    label "composition"
  ]
  node [
    id 152
    label "wydawnictwo"
  ]
  node [
    id 153
    label "redaction"
  ]
  node [
    id 154
    label "tekst"
  ]
  node [
    id 155
    label "obr&#243;bka"
  ]
  node [
    id 156
    label "debit"
  ]
  node [
    id 157
    label "druk"
  ]
  node [
    id 158
    label "publikacja"
  ]
  node [
    id 159
    label "nadtytu&#322;"
  ]
  node [
    id 160
    label "szata_graficzna"
  ]
  node [
    id 161
    label "tytulatura"
  ]
  node [
    id 162
    label "wydawa&#263;"
  ]
  node [
    id 163
    label "elevation"
  ]
  node [
    id 164
    label "wyda&#263;"
  ]
  node [
    id 165
    label "mianowaniec"
  ]
  node [
    id 166
    label "poster"
  ]
  node [
    id 167
    label "podtytu&#322;"
  ]
  node [
    id 168
    label "centerfold"
  ]
  node [
    id 169
    label "t&#322;oczysko"
  ]
  node [
    id 170
    label "depesza"
  ]
  node [
    id 171
    label "maszyna"
  ]
  node [
    id 172
    label "media"
  ]
  node [
    id 173
    label "napisa&#263;"
  ]
  node [
    id 174
    label "dziennikarz_prasowy"
  ]
  node [
    id 175
    label "pisa&#263;"
  ]
  node [
    id 176
    label "kiosk"
  ]
  node [
    id 177
    label "maszyna_rolnicza"
  ]
  node [
    id 178
    label "telekomunikacja"
  ]
  node [
    id 179
    label "ekran"
  ]
  node [
    id 180
    label "BBC"
  ]
  node [
    id 181
    label "Interwizja"
  ]
  node [
    id 182
    label "paj&#281;czarz"
  ]
  node [
    id 183
    label "programowiec"
  ]
  node [
    id 184
    label "instytucja"
  ]
  node [
    id 185
    label "Polsat"
  ]
  node [
    id 186
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 187
    label "odbiornik"
  ]
  node [
    id 188
    label "muza"
  ]
  node [
    id 189
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 190
    label "odbieranie"
  ]
  node [
    id 191
    label "studio"
  ]
  node [
    id 192
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 193
    label "odbiera&#263;"
  ]
  node [
    id 194
    label "technologia"
  ]
  node [
    id 195
    label "technika"
  ]
  node [
    id 196
    label "teletransmisja"
  ]
  node [
    id 197
    label "telemetria"
  ]
  node [
    id 198
    label "telegrafia"
  ]
  node [
    id 199
    label "transmisja_danych"
  ]
  node [
    id 200
    label "kontrola_parzysto&#347;ci"
  ]
  node [
    id 201
    label "trunking"
  ]
  node [
    id 202
    label "komunikacja"
  ]
  node [
    id 203
    label "telekomutacja"
  ]
  node [
    id 204
    label "teletechnika"
  ]
  node [
    id 205
    label "teleks"
  ]
  node [
    id 206
    label "telematyka"
  ]
  node [
    id 207
    label "teleinformatyka"
  ]
  node [
    id 208
    label "telefonia"
  ]
  node [
    id 209
    label "mikrotechnologia"
  ]
  node [
    id 210
    label "spos&#243;b"
  ]
  node [
    id 211
    label "technologia_nieorganiczna"
  ]
  node [
    id 212
    label "engineering"
  ]
  node [
    id 213
    label "biotechnologia"
  ]
  node [
    id 214
    label "osoba_prawna"
  ]
  node [
    id 215
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 216
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 217
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 218
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 219
    label "biuro"
  ]
  node [
    id 220
    label "organizacja"
  ]
  node [
    id 221
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 222
    label "Fundusze_Unijne"
  ]
  node [
    id 223
    label "zamyka&#263;"
  ]
  node [
    id 224
    label "establishment"
  ]
  node [
    id 225
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 226
    label "urz&#261;d"
  ]
  node [
    id 227
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 228
    label "afiliowa&#263;"
  ]
  node [
    id 229
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 230
    label "standard"
  ]
  node [
    id 231
    label "zamykanie"
  ]
  node [
    id 232
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 233
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 234
    label "antena"
  ]
  node [
    id 235
    label "urz&#261;dzenie"
  ]
  node [
    id 236
    label "amplituner"
  ]
  node [
    id 237
    label "tuner"
  ]
  node [
    id 238
    label "mass-media"
  ]
  node [
    id 239
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 240
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 241
    label "przekazior"
  ]
  node [
    id 242
    label "uzbrajanie"
  ]
  node [
    id 243
    label "medium"
  ]
  node [
    id 244
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 245
    label "inspiratorka"
  ]
  node [
    id 246
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 247
    label "cz&#322;owiek"
  ]
  node [
    id 248
    label "banan"
  ]
  node [
    id 249
    label "talent"
  ]
  node [
    id 250
    label "kobieta"
  ]
  node [
    id 251
    label "Melpomena"
  ]
  node [
    id 252
    label "natchnienie"
  ]
  node [
    id 253
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 254
    label "bogini"
  ]
  node [
    id 255
    label "ro&#347;lina"
  ]
  node [
    id 256
    label "muzyka"
  ]
  node [
    id 257
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 258
    label "palma"
  ]
  node [
    id 259
    label "pomieszczenie"
  ]
  node [
    id 260
    label "p&#322;aszczyzna"
  ]
  node [
    id 261
    label "naszywka"
  ]
  node [
    id 262
    label "kominek"
  ]
  node [
    id 263
    label "zas&#322;ona"
  ]
  node [
    id 264
    label "os&#322;ona"
  ]
  node [
    id 265
    label "u&#380;ytkownik"
  ]
  node [
    id 266
    label "oszust"
  ]
  node [
    id 267
    label "telewizor"
  ]
  node [
    id 268
    label "pirat"
  ]
  node [
    id 269
    label "dochodzenie"
  ]
  node [
    id 270
    label "rozsi&#261;dni&#281;cie_si&#281;"
  ]
  node [
    id 271
    label "powodowanie"
  ]
  node [
    id 272
    label "wpadni&#281;cie"
  ]
  node [
    id 273
    label "collection"
  ]
  node [
    id 274
    label "konfiskowanie"
  ]
  node [
    id 275
    label "rozsiadanie_si&#281;"
  ]
  node [
    id 276
    label "zabieranie"
  ]
  node [
    id 277
    label "zlecenie"
  ]
  node [
    id 278
    label "przyjmowanie"
  ]
  node [
    id 279
    label "solicitation"
  ]
  node [
    id 280
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 281
    label "robienie"
  ]
  node [
    id 282
    label "zniewalanie"
  ]
  node [
    id 283
    label "doj&#347;cie"
  ]
  node [
    id 284
    label "przyp&#322;ywanie"
  ]
  node [
    id 285
    label "odzyskiwanie"
  ]
  node [
    id 286
    label "czynno&#347;&#263;"
  ]
  node [
    id 287
    label "branie"
  ]
  node [
    id 288
    label "perception"
  ]
  node [
    id 289
    label "odp&#322;ywanie"
  ]
  node [
    id 290
    label "wpadanie"
  ]
  node [
    id 291
    label "zabiera&#263;"
  ]
  node [
    id 292
    label "odzyskiwa&#263;"
  ]
  node [
    id 293
    label "przyjmowa&#263;"
  ]
  node [
    id 294
    label "bra&#263;"
  ]
  node [
    id 295
    label "fall"
  ]
  node [
    id 296
    label "liszy&#263;"
  ]
  node [
    id 297
    label "pozbawia&#263;"
  ]
  node [
    id 298
    label "konfiskowa&#263;"
  ]
  node [
    id 299
    label "deprive"
  ]
  node [
    id 300
    label "accept"
  ]
  node [
    id 301
    label "doznawa&#263;"
  ]
  node [
    id 302
    label "chmurnienie"
  ]
  node [
    id 303
    label "p&#322;owy"
  ]
  node [
    id 304
    label "niezabawny"
  ]
  node [
    id 305
    label "srebrny"
  ]
  node [
    id 306
    label "brzydki"
  ]
  node [
    id 307
    label "szarzenie"
  ]
  node [
    id 308
    label "blady"
  ]
  node [
    id 309
    label "zwyczajny"
  ]
  node [
    id 310
    label "pochmurno"
  ]
  node [
    id 311
    label "zielono"
  ]
  node [
    id 312
    label "oboj&#281;tny"
  ]
  node [
    id 313
    label "poszarzenie"
  ]
  node [
    id 314
    label "szaro"
  ]
  node [
    id 315
    label "ch&#322;odny"
  ]
  node [
    id 316
    label "spochmurnienie"
  ]
  node [
    id 317
    label "zwyk&#322;y"
  ]
  node [
    id 318
    label "bezbarwnie"
  ]
  node [
    id 319
    label "nieciekawy"
  ]
  node [
    id 320
    label "zbrzydni&#281;cie"
  ]
  node [
    id 321
    label "skandaliczny"
  ]
  node [
    id 322
    label "nieprzyjemny"
  ]
  node [
    id 323
    label "oszpecanie"
  ]
  node [
    id 324
    label "brzydni&#281;cie"
  ]
  node [
    id 325
    label "nie&#322;adny"
  ]
  node [
    id 326
    label "brzydko"
  ]
  node [
    id 327
    label "nieprzyzwoity"
  ]
  node [
    id 328
    label "oszpecenie"
  ]
  node [
    id 329
    label "przeci&#281;tny"
  ]
  node [
    id 330
    label "zwyczajnie"
  ]
  node [
    id 331
    label "zwykle"
  ]
  node [
    id 332
    label "okre&#347;lony"
  ]
  node [
    id 333
    label "nieatrakcyjny"
  ]
  node [
    id 334
    label "blado"
  ]
  node [
    id 335
    label "mizerny"
  ]
  node [
    id 336
    label "nienasycony"
  ]
  node [
    id 337
    label "s&#322;aby"
  ]
  node [
    id 338
    label "niewa&#380;ny"
  ]
  node [
    id 339
    label "jasny"
  ]
  node [
    id 340
    label "zi&#281;bienie"
  ]
  node [
    id 341
    label "niesympatyczny"
  ]
  node [
    id 342
    label "och&#322;odzenie"
  ]
  node [
    id 343
    label "opanowany"
  ]
  node [
    id 344
    label "ch&#322;odzenie_si&#281;"
  ]
  node [
    id 345
    label "rozs&#261;dny"
  ]
  node [
    id 346
    label "och&#322;odzenie_si&#281;"
  ]
  node [
    id 347
    label "sch&#322;adzanie"
  ]
  node [
    id 348
    label "ch&#322;odno"
  ]
  node [
    id 349
    label "oswojony"
  ]
  node [
    id 350
    label "na&#322;o&#380;ny"
  ]
  node [
    id 351
    label "nieciekawie"
  ]
  node [
    id 352
    label "z&#322;y"
  ]
  node [
    id 353
    label "zoboj&#281;tnienie"
  ]
  node [
    id 354
    label "nieszkodliwy"
  ]
  node [
    id 355
    label "&#347;ni&#281;ty"
  ]
  node [
    id 356
    label "oboj&#281;tnie"
  ]
  node [
    id 357
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 358
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 359
    label "neutralizowanie"
  ]
  node [
    id 360
    label "bierny"
  ]
  node [
    id 361
    label "zneutralizowanie"
  ]
  node [
    id 362
    label "neutralny"
  ]
  node [
    id 363
    label "zasnuwanie_si&#281;"
  ]
  node [
    id 364
    label "powa&#380;nienie"
  ]
  node [
    id 365
    label "pochmurny"
  ]
  node [
    id 366
    label "zasnucie_si&#281;"
  ]
  node [
    id 367
    label "zielony"
  ]
  node [
    id 368
    label "&#380;ywo"
  ]
  node [
    id 369
    label "&#347;wie&#380;o"
  ]
  node [
    id 370
    label "blandly"
  ]
  node [
    id 371
    label "nieefektownie"
  ]
  node [
    id 372
    label "nijaki"
  ]
  node [
    id 373
    label "zabarwienie_si&#281;"
  ]
  node [
    id 374
    label "rozwidnienie_si&#281;"
  ]
  node [
    id 375
    label "zmierzchni&#281;cie_si&#281;"
  ]
  node [
    id 376
    label "stanie_si&#281;"
  ]
  node [
    id 377
    label "rozwidnianie_si&#281;"
  ]
  node [
    id 378
    label "odcinanie_si&#281;"
  ]
  node [
    id 379
    label "zmierzchanie_si&#281;"
  ]
  node [
    id 380
    label "barwienie_si&#281;"
  ]
  node [
    id 381
    label "stawanie_si&#281;"
  ]
  node [
    id 382
    label "srebrzenie"
  ]
  node [
    id 383
    label "metaliczny"
  ]
  node [
    id 384
    label "srebrzy&#347;cie"
  ]
  node [
    id 385
    label "posrebrzenie"
  ]
  node [
    id 386
    label "srebrzenie_si&#281;"
  ]
  node [
    id 387
    label "utytu&#322;owany"
  ]
  node [
    id 388
    label "srebrno"
  ]
  node [
    id 389
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 390
    label "p&#322;owo"
  ]
  node [
    id 391
    label "&#380;&#243;&#322;tawy"
  ]
  node [
    id 392
    label "niedobrze"
  ]
  node [
    id 393
    label "wyblak&#322;y"
  ]
  node [
    id 394
    label "bezbarwny"
  ]
  node [
    id 395
    label "miastowy"
  ]
  node [
    id 396
    label "mieszkaniec"
  ]
  node [
    id 397
    label "pa&#324;stwo"
  ]
  node [
    id 398
    label "przedstawiciel"
  ]
  node [
    id 399
    label "mieszczanin"
  ]
  node [
    id 400
    label "miejski"
  ]
  node [
    id 401
    label "nowoczesny"
  ]
  node [
    id 402
    label "mieszcza&#324;stwo"
  ]
  node [
    id 403
    label "Katar"
  ]
  node [
    id 404
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 405
    label "Libia"
  ]
  node [
    id 406
    label "Gwatemala"
  ]
  node [
    id 407
    label "Afganistan"
  ]
  node [
    id 408
    label "Ekwador"
  ]
  node [
    id 409
    label "Tad&#380;ykistan"
  ]
  node [
    id 410
    label "Bhutan"
  ]
  node [
    id 411
    label "Argentyna"
  ]
  node [
    id 412
    label "D&#380;ibuti"
  ]
  node [
    id 413
    label "Wenezuela"
  ]
  node [
    id 414
    label "Ukraina"
  ]
  node [
    id 415
    label "Gabon"
  ]
  node [
    id 416
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 417
    label "Rwanda"
  ]
  node [
    id 418
    label "Liechtenstein"
  ]
  node [
    id 419
    label "Sri_Lanka"
  ]
  node [
    id 420
    label "Madagaskar"
  ]
  node [
    id 421
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 422
    label "Tonga"
  ]
  node [
    id 423
    label "Kongo"
  ]
  node [
    id 424
    label "Bangladesz"
  ]
  node [
    id 425
    label "Kanada"
  ]
  node [
    id 426
    label "Wehrlen"
  ]
  node [
    id 427
    label "Algieria"
  ]
  node [
    id 428
    label "Surinam"
  ]
  node [
    id 429
    label "Chile"
  ]
  node [
    id 430
    label "Sahara_Zachodnia"
  ]
  node [
    id 431
    label "Uganda"
  ]
  node [
    id 432
    label "W&#281;gry"
  ]
  node [
    id 433
    label "Birma"
  ]
  node [
    id 434
    label "Kazachstan"
  ]
  node [
    id 435
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 436
    label "Armenia"
  ]
  node [
    id 437
    label "Tuwalu"
  ]
  node [
    id 438
    label "Timor_Wschodni"
  ]
  node [
    id 439
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 440
    label "Izrael"
  ]
  node [
    id 441
    label "Estonia"
  ]
  node [
    id 442
    label "Komory"
  ]
  node [
    id 443
    label "Kamerun"
  ]
  node [
    id 444
    label "Haiti"
  ]
  node [
    id 445
    label "Belize"
  ]
  node [
    id 446
    label "Sierra_Leone"
  ]
  node [
    id 447
    label "Luksemburg"
  ]
  node [
    id 448
    label "USA"
  ]
  node [
    id 449
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 450
    label "Barbados"
  ]
  node [
    id 451
    label "San_Marino"
  ]
  node [
    id 452
    label "Bu&#322;garia"
  ]
  node [
    id 453
    label "Wietnam"
  ]
  node [
    id 454
    label "Indonezja"
  ]
  node [
    id 455
    label "Malawi"
  ]
  node [
    id 456
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 457
    label "Francja"
  ]
  node [
    id 458
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 459
    label "partia"
  ]
  node [
    id 460
    label "Zambia"
  ]
  node [
    id 461
    label "Angola"
  ]
  node [
    id 462
    label "Grenada"
  ]
  node [
    id 463
    label "Nepal"
  ]
  node [
    id 464
    label "Panama"
  ]
  node [
    id 465
    label "Rumunia"
  ]
  node [
    id 466
    label "Czarnog&#243;ra"
  ]
  node [
    id 467
    label "Malediwy"
  ]
  node [
    id 468
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 469
    label "S&#322;owacja"
  ]
  node [
    id 470
    label "para"
  ]
  node [
    id 471
    label "Egipt"
  ]
  node [
    id 472
    label "zwrot"
  ]
  node [
    id 473
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 474
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 475
    label "Kolumbia"
  ]
  node [
    id 476
    label "Mozambik"
  ]
  node [
    id 477
    label "Laos"
  ]
  node [
    id 478
    label "Burundi"
  ]
  node [
    id 479
    label "Suazi"
  ]
  node [
    id 480
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 481
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 482
    label "Czechy"
  ]
  node [
    id 483
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 484
    label "Wyspy_Marshalla"
  ]
  node [
    id 485
    label "Trynidad_i_Tobago"
  ]
  node [
    id 486
    label "Dominika"
  ]
  node [
    id 487
    label "Palau"
  ]
  node [
    id 488
    label "Syria"
  ]
  node [
    id 489
    label "Gwinea_Bissau"
  ]
  node [
    id 490
    label "Liberia"
  ]
  node [
    id 491
    label "Zimbabwe"
  ]
  node [
    id 492
    label "Polska"
  ]
  node [
    id 493
    label "Jamajka"
  ]
  node [
    id 494
    label "Dominikana"
  ]
  node [
    id 495
    label "Senegal"
  ]
  node [
    id 496
    label "Gruzja"
  ]
  node [
    id 497
    label "Togo"
  ]
  node [
    id 498
    label "Chorwacja"
  ]
  node [
    id 499
    label "Meksyk"
  ]
  node [
    id 500
    label "Macedonia"
  ]
  node [
    id 501
    label "Gujana"
  ]
  node [
    id 502
    label "Zair"
  ]
  node [
    id 503
    label "Albania"
  ]
  node [
    id 504
    label "Kambod&#380;a"
  ]
  node [
    id 505
    label "Mauritius"
  ]
  node [
    id 506
    label "Monako"
  ]
  node [
    id 507
    label "Gwinea"
  ]
  node [
    id 508
    label "Mali"
  ]
  node [
    id 509
    label "Nigeria"
  ]
  node [
    id 510
    label "Kostaryka"
  ]
  node [
    id 511
    label "Hanower"
  ]
  node [
    id 512
    label "Paragwaj"
  ]
  node [
    id 513
    label "W&#322;ochy"
  ]
  node [
    id 514
    label "Wyspy_Salomona"
  ]
  node [
    id 515
    label "Seszele"
  ]
  node [
    id 516
    label "Hiszpania"
  ]
  node [
    id 517
    label "Boliwia"
  ]
  node [
    id 518
    label "Kirgistan"
  ]
  node [
    id 519
    label "Irlandia"
  ]
  node [
    id 520
    label "Czad"
  ]
  node [
    id 521
    label "Irak"
  ]
  node [
    id 522
    label "Lesoto"
  ]
  node [
    id 523
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 524
    label "Malta"
  ]
  node [
    id 525
    label "Andora"
  ]
  node [
    id 526
    label "Chiny"
  ]
  node [
    id 527
    label "Filipiny"
  ]
  node [
    id 528
    label "Antarktis"
  ]
  node [
    id 529
    label "Niemcy"
  ]
  node [
    id 530
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 531
    label "Brazylia"
  ]
  node [
    id 532
    label "terytorium"
  ]
  node [
    id 533
    label "Nikaragua"
  ]
  node [
    id 534
    label "Pakistan"
  ]
  node [
    id 535
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 536
    label "Kenia"
  ]
  node [
    id 537
    label "Niger"
  ]
  node [
    id 538
    label "Tunezja"
  ]
  node [
    id 539
    label "Portugalia"
  ]
  node [
    id 540
    label "Fid&#380;i"
  ]
  node [
    id 541
    label "Maroko"
  ]
  node [
    id 542
    label "Botswana"
  ]
  node [
    id 543
    label "Tajlandia"
  ]
  node [
    id 544
    label "Australia"
  ]
  node [
    id 545
    label "Burkina_Faso"
  ]
  node [
    id 546
    label "interior"
  ]
  node [
    id 547
    label "Benin"
  ]
  node [
    id 548
    label "Tanzania"
  ]
  node [
    id 549
    label "Indie"
  ]
  node [
    id 550
    label "&#321;otwa"
  ]
  node [
    id 551
    label "Kiribati"
  ]
  node [
    id 552
    label "Antigua_i_Barbuda"
  ]
  node [
    id 553
    label "Rodezja"
  ]
  node [
    id 554
    label "Cypr"
  ]
  node [
    id 555
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 556
    label "Peru"
  ]
  node [
    id 557
    label "Austria"
  ]
  node [
    id 558
    label "Urugwaj"
  ]
  node [
    id 559
    label "Jordania"
  ]
  node [
    id 560
    label "Grecja"
  ]
  node [
    id 561
    label "Azerbejd&#380;an"
  ]
  node [
    id 562
    label "Turcja"
  ]
  node [
    id 563
    label "Samoa"
  ]
  node [
    id 564
    label "Sudan"
  ]
  node [
    id 565
    label "Oman"
  ]
  node [
    id 566
    label "ziemia"
  ]
  node [
    id 567
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 568
    label "Uzbekistan"
  ]
  node [
    id 569
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 570
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 571
    label "Honduras"
  ]
  node [
    id 572
    label "Mongolia"
  ]
  node [
    id 573
    label "Portoryko"
  ]
  node [
    id 574
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 575
    label "Serbia"
  ]
  node [
    id 576
    label "Tajwan"
  ]
  node [
    id 577
    label "Wielka_Brytania"
  ]
  node [
    id 578
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 579
    label "Liban"
  ]
  node [
    id 580
    label "Japonia"
  ]
  node [
    id 581
    label "Ghana"
  ]
  node [
    id 582
    label "Bahrajn"
  ]
  node [
    id 583
    label "Belgia"
  ]
  node [
    id 584
    label "Etiopia"
  ]
  node [
    id 585
    label "Mikronezja"
  ]
  node [
    id 586
    label "Kuwejt"
  ]
  node [
    id 587
    label "grupa"
  ]
  node [
    id 588
    label "Bahamy"
  ]
  node [
    id 589
    label "Rosja"
  ]
  node [
    id 590
    label "Mo&#322;dawia"
  ]
  node [
    id 591
    label "Litwa"
  ]
  node [
    id 592
    label "S&#322;owenia"
  ]
  node [
    id 593
    label "Szwajcaria"
  ]
  node [
    id 594
    label "Erytrea"
  ]
  node [
    id 595
    label "Kuba"
  ]
  node [
    id 596
    label "Arabia_Saudyjska"
  ]
  node [
    id 597
    label "granica_pa&#324;stwa"
  ]
  node [
    id 598
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 599
    label "Malezja"
  ]
  node [
    id 600
    label "Korea"
  ]
  node [
    id 601
    label "Jemen"
  ]
  node [
    id 602
    label "Nowa_Zelandia"
  ]
  node [
    id 603
    label "Namibia"
  ]
  node [
    id 604
    label "Nauru"
  ]
  node [
    id 605
    label "holoarktyka"
  ]
  node [
    id 606
    label "Brunei"
  ]
  node [
    id 607
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 608
    label "Khitai"
  ]
  node [
    id 609
    label "Mauretania"
  ]
  node [
    id 610
    label "Iran"
  ]
  node [
    id 611
    label "Gambia"
  ]
  node [
    id 612
    label "Somalia"
  ]
  node [
    id 613
    label "Holandia"
  ]
  node [
    id 614
    label "Turkmenistan"
  ]
  node [
    id 615
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 616
    label "Salwador"
  ]
  node [
    id 617
    label "ludno&#347;&#263;"
  ]
  node [
    id 618
    label "zwierz&#281;"
  ]
  node [
    id 619
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 620
    label "cz&#322;onek"
  ]
  node [
    id 621
    label "przyk&#322;ad"
  ]
  node [
    id 622
    label "substytuowa&#263;"
  ]
  node [
    id 623
    label "substytuowanie"
  ]
  node [
    id 624
    label "zast&#281;pca"
  ]
  node [
    id 625
    label "ludzko&#347;&#263;"
  ]
  node [
    id 626
    label "asymilowanie"
  ]
  node [
    id 627
    label "wapniak"
  ]
  node [
    id 628
    label "asymilowa&#263;"
  ]
  node [
    id 629
    label "os&#322;abia&#263;"
  ]
  node [
    id 630
    label "posta&#263;"
  ]
  node [
    id 631
    label "hominid"
  ]
  node [
    id 632
    label "podw&#322;adny"
  ]
  node [
    id 633
    label "os&#322;abianie"
  ]
  node [
    id 634
    label "g&#322;owa"
  ]
  node [
    id 635
    label "figura"
  ]
  node [
    id 636
    label "portrecista"
  ]
  node [
    id 637
    label "dwun&#243;g"
  ]
  node [
    id 638
    label "profanum"
  ]
  node [
    id 639
    label "mikrokosmos"
  ]
  node [
    id 640
    label "nasada"
  ]
  node [
    id 641
    label "duch"
  ]
  node [
    id 642
    label "antropochoria"
  ]
  node [
    id 643
    label "osoba"
  ]
  node [
    id 644
    label "wz&#243;r"
  ]
  node [
    id 645
    label "senior"
  ]
  node [
    id 646
    label "oddzia&#322;ywanie"
  ]
  node [
    id 647
    label "Adam"
  ]
  node [
    id 648
    label "homo_sapiens"
  ]
  node [
    id 649
    label "polifag"
  ]
  node [
    id 650
    label "gaworzy&#263;"
  ]
  node [
    id 651
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 652
    label "remark"
  ]
  node [
    id 653
    label "rozmawia&#263;"
  ]
  node [
    id 654
    label "wyra&#380;a&#263;"
  ]
  node [
    id 655
    label "umie&#263;"
  ]
  node [
    id 656
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 657
    label "dziama&#263;"
  ]
  node [
    id 658
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 659
    label "formu&#322;owa&#263;"
  ]
  node [
    id 660
    label "dysfonia"
  ]
  node [
    id 661
    label "express"
  ]
  node [
    id 662
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 663
    label "talk"
  ]
  node [
    id 664
    label "u&#380;ywa&#263;"
  ]
  node [
    id 665
    label "prawi&#263;"
  ]
  node [
    id 666
    label "powiada&#263;"
  ]
  node [
    id 667
    label "tell"
  ]
  node [
    id 668
    label "chew_the_fat"
  ]
  node [
    id 669
    label "say"
  ]
  node [
    id 670
    label "j&#281;zyk"
  ]
  node [
    id 671
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 672
    label "informowa&#263;"
  ]
  node [
    id 673
    label "wydobywa&#263;"
  ]
  node [
    id 674
    label "okre&#347;la&#263;"
  ]
  node [
    id 675
    label "korzysta&#263;"
  ]
  node [
    id 676
    label "distribute"
  ]
  node [
    id 677
    label "bash"
  ]
  node [
    id 678
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 679
    label "decydowa&#263;"
  ]
  node [
    id 680
    label "signify"
  ]
  node [
    id 681
    label "style"
  ]
  node [
    id 682
    label "komunikowa&#263;"
  ]
  node [
    id 683
    label "inform"
  ]
  node [
    id 684
    label "znaczy&#263;"
  ]
  node [
    id 685
    label "give_voice"
  ]
  node [
    id 686
    label "oznacza&#263;"
  ]
  node [
    id 687
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 688
    label "represent"
  ]
  node [
    id 689
    label "convey"
  ]
  node [
    id 690
    label "arouse"
  ]
  node [
    id 691
    label "determine"
  ]
  node [
    id 692
    label "reakcja_chemiczna"
  ]
  node [
    id 693
    label "uwydatnia&#263;"
  ]
  node [
    id 694
    label "eksploatowa&#263;"
  ]
  node [
    id 695
    label "uzyskiwa&#263;"
  ]
  node [
    id 696
    label "wydostawa&#263;"
  ]
  node [
    id 697
    label "wyjmowa&#263;"
  ]
  node [
    id 698
    label "train"
  ]
  node [
    id 699
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 700
    label "dobywa&#263;"
  ]
  node [
    id 701
    label "ocala&#263;"
  ]
  node [
    id 702
    label "excavate"
  ]
  node [
    id 703
    label "g&#243;rnictwo"
  ]
  node [
    id 704
    label "raise"
  ]
  node [
    id 705
    label "can"
  ]
  node [
    id 706
    label "m&#243;c"
  ]
  node [
    id 707
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 708
    label "rozumie&#263;"
  ]
  node [
    id 709
    label "szczeka&#263;"
  ]
  node [
    id 710
    label "funkcjonowa&#263;"
  ]
  node [
    id 711
    label "mawia&#263;"
  ]
  node [
    id 712
    label "opowiada&#263;"
  ]
  node [
    id 713
    label "chatter"
  ]
  node [
    id 714
    label "niemowl&#281;"
  ]
  node [
    id 715
    label "kosmetyk"
  ]
  node [
    id 716
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 717
    label "stanowisko_archeologiczne"
  ]
  node [
    id 718
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 719
    label "artykulator"
  ]
  node [
    id 720
    label "kod"
  ]
  node [
    id 721
    label "kawa&#322;ek"
  ]
  node [
    id 722
    label "przedmiot"
  ]
  node [
    id 723
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 724
    label "gramatyka"
  ]
  node [
    id 725
    label "stylik"
  ]
  node [
    id 726
    label "przet&#322;umaczenie"
  ]
  node [
    id 727
    label "formalizowanie"
  ]
  node [
    id 728
    label "ssanie"
  ]
  node [
    id 729
    label "ssa&#263;"
  ]
  node [
    id 730
    label "language"
  ]
  node [
    id 731
    label "liza&#263;"
  ]
  node [
    id 732
    label "konsonantyzm"
  ]
  node [
    id 733
    label "wokalizm"
  ]
  node [
    id 734
    label "fonetyka"
  ]
  node [
    id 735
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 736
    label "jeniec"
  ]
  node [
    id 737
    label "but"
  ]
  node [
    id 738
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 739
    label "po_koroniarsku"
  ]
  node [
    id 740
    label "kultura_duchowa"
  ]
  node [
    id 741
    label "t&#322;umaczenie"
  ]
  node [
    id 742
    label "m&#243;wienie"
  ]
  node [
    id 743
    label "pype&#263;"
  ]
  node [
    id 744
    label "lizanie"
  ]
  node [
    id 745
    label "formalizowa&#263;"
  ]
  node [
    id 746
    label "organ"
  ]
  node [
    id 747
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 748
    label "rozumienie"
  ]
  node [
    id 749
    label "makroglosja"
  ]
  node [
    id 750
    label "jama_ustna"
  ]
  node [
    id 751
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 752
    label "formacja_geologiczna"
  ]
  node [
    id 753
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 754
    label "natural_language"
  ]
  node [
    id 755
    label "dysphonia"
  ]
  node [
    id 756
    label "dysleksja"
  ]
  node [
    id 757
    label "pomiernie"
  ]
  node [
    id 758
    label "nieistotnie"
  ]
  node [
    id 759
    label "nieznaczny"
  ]
  node [
    id 760
    label "ma&#322;y"
  ]
  node [
    id 761
    label "niepowa&#380;nie"
  ]
  node [
    id 762
    label "nieznacznie"
  ]
  node [
    id 763
    label "drobnostkowy"
  ]
  node [
    id 764
    label "szybki"
  ]
  node [
    id 765
    label "wstydliwy"
  ]
  node [
    id 766
    label "ch&#322;opiec"
  ]
  node [
    id 767
    label "m&#322;ody"
  ]
  node [
    id 768
    label "ma&#322;o"
  ]
  node [
    id 769
    label "marny"
  ]
  node [
    id 770
    label "nieliczny"
  ]
  node [
    id 771
    label "n&#281;dznie"
  ]
  node [
    id 772
    label "licho"
  ]
  node [
    id 773
    label "proporcjonalnie"
  ]
  node [
    id 774
    label "pomierny"
  ]
  node [
    id 775
    label "miernie"
  ]
  node [
    id 776
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 777
    label "majority"
  ]
  node [
    id 778
    label "Rzym_Zachodni"
  ]
  node [
    id 779
    label "whole"
  ]
  node [
    id 780
    label "ilo&#347;&#263;"
  ]
  node [
    id 781
    label "element"
  ]
  node [
    id 782
    label "Rzym_Wschodni"
  ]
  node [
    id 783
    label "polski"
  ]
  node [
    id 784
    label "Polish"
  ]
  node [
    id 785
    label "goniony"
  ]
  node [
    id 786
    label "oberek"
  ]
  node [
    id 787
    label "ryba_po_grecku"
  ]
  node [
    id 788
    label "sztajer"
  ]
  node [
    id 789
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 790
    label "krakowiak"
  ]
  node [
    id 791
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 792
    label "pierogi_ruskie"
  ]
  node [
    id 793
    label "lacki"
  ]
  node [
    id 794
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 795
    label "chodzony"
  ]
  node [
    id 796
    label "po_polsku"
  ]
  node [
    id 797
    label "mazur"
  ]
  node [
    id 798
    label "polsko"
  ]
  node [
    id 799
    label "skoczny"
  ]
  node [
    id 800
    label "drabant"
  ]
  node [
    id 801
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 802
    label "cognizance"
  ]
  node [
    id 803
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 804
    label "wiadomy"
  ]
  node [
    id 805
    label "Mazowsze"
  ]
  node [
    id 806
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 807
    label "Anglia"
  ]
  node [
    id 808
    label "Amazonia"
  ]
  node [
    id 809
    label "Bordeaux"
  ]
  node [
    id 810
    label "Naddniestrze"
  ]
  node [
    id 811
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 812
    label "Europa_Zachodnia"
  ]
  node [
    id 813
    label "Armagnac"
  ]
  node [
    id 814
    label "Amhara"
  ]
  node [
    id 815
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 816
    label "Zamojszczyzna"
  ]
  node [
    id 817
    label "Turkiestan"
  ]
  node [
    id 818
    label "Ma&#322;opolska"
  ]
  node [
    id 819
    label "Noworosja"
  ]
  node [
    id 820
    label "Lubelszczyzna"
  ]
  node [
    id 821
    label "Mezoameryka"
  ]
  node [
    id 822
    label "Ba&#322;kany"
  ]
  node [
    id 823
    label "Kurdystan"
  ]
  node [
    id 824
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 825
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 826
    label "Baszkiria"
  ]
  node [
    id 827
    label "Szkocja"
  ]
  node [
    id 828
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 829
    label "Tonkin"
  ]
  node [
    id 830
    label "Maghreb"
  ]
  node [
    id 831
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 832
    label "Nadrenia"
  ]
  node [
    id 833
    label "Podhale"
  ]
  node [
    id 834
    label "Wielkopolska"
  ]
  node [
    id 835
    label "Zabajkale"
  ]
  node [
    id 836
    label "Apulia"
  ]
  node [
    id 837
    label "brzeg"
  ]
  node [
    id 838
    label "Bojkowszczyzna"
  ]
  node [
    id 839
    label "Kujawy"
  ]
  node [
    id 840
    label "Liguria"
  ]
  node [
    id 841
    label "Pamir"
  ]
  node [
    id 842
    label "Indochiny"
  ]
  node [
    id 843
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 844
    label "Polinezja"
  ]
  node [
    id 845
    label "Kurpie"
  ]
  node [
    id 846
    label "Podlasie"
  ]
  node [
    id 847
    label "S&#261;decczyzna"
  ]
  node [
    id 848
    label "Umbria"
  ]
  node [
    id 849
    label "Karaiby"
  ]
  node [
    id 850
    label "Ukraina_Zachodnia"
  ]
  node [
    id 851
    label "Kielecczyzna"
  ]
  node [
    id 852
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 853
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 854
    label "Kalabria"
  ]
  node [
    id 855
    label "Skandynawia"
  ]
  node [
    id 856
    label "Bory_Tucholskie"
  ]
  node [
    id 857
    label "Huculszczyzna"
  ]
  node [
    id 858
    label "Tyrol"
  ]
  node [
    id 859
    label "Turyngia"
  ]
  node [
    id 860
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 861
    label "jednostka_administracyjna"
  ]
  node [
    id 862
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 863
    label "Hercegowina"
  ]
  node [
    id 864
    label "Lotaryngia"
  ]
  node [
    id 865
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 866
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 867
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 868
    label "Walia"
  ]
  node [
    id 869
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 870
    label "Opolskie"
  ]
  node [
    id 871
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 872
    label "Kampania"
  ]
  node [
    id 873
    label "Sand&#380;ak"
  ]
  node [
    id 874
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 875
    label "Syjon"
  ]
  node [
    id 876
    label "Kabylia"
  ]
  node [
    id 877
    label "Lombardia"
  ]
  node [
    id 878
    label "Warmia"
  ]
  node [
    id 879
    label "Kaszmir"
  ]
  node [
    id 880
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 881
    label "&#321;&#243;dzkie"
  ]
  node [
    id 882
    label "Kaukaz"
  ]
  node [
    id 883
    label "Europa_Wschodnia"
  ]
  node [
    id 884
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 885
    label "Biskupizna"
  ]
  node [
    id 886
    label "Afryka_Wschodnia"
  ]
  node [
    id 887
    label "Podkarpacie"
  ]
  node [
    id 888
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 889
    label "obszar"
  ]
  node [
    id 890
    label "Afryka_Zachodnia"
  ]
  node [
    id 891
    label "Toskania"
  ]
  node [
    id 892
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 893
    label "Podbeskidzie"
  ]
  node [
    id 894
    label "Bo&#347;nia"
  ]
  node [
    id 895
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 896
    label "Oceania"
  ]
  node [
    id 897
    label "Pomorze_Zachodnie"
  ]
  node [
    id 898
    label "Powi&#347;le"
  ]
  node [
    id 899
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 900
    label "&#321;emkowszczyzna"
  ]
  node [
    id 901
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 902
    label "Kaszuby"
  ]
  node [
    id 903
    label "Ko&#322;yma"
  ]
  node [
    id 904
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 905
    label "Szlezwik"
  ]
  node [
    id 906
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 907
    label "Polesie"
  ]
  node [
    id 908
    label "Kerala"
  ]
  node [
    id 909
    label "Mazury"
  ]
  node [
    id 910
    label "Palestyna"
  ]
  node [
    id 911
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 912
    label "Lauda"
  ]
  node [
    id 913
    label "Azja_Wschodnia"
  ]
  node [
    id 914
    label "Zakarpacie"
  ]
  node [
    id 915
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 916
    label "Galicja"
  ]
  node [
    id 917
    label "Lubuskie"
  ]
  node [
    id 918
    label "Laponia"
  ]
  node [
    id 919
    label "Yorkshire"
  ]
  node [
    id 920
    label "Bawaria"
  ]
  node [
    id 921
    label "Zag&#243;rze"
  ]
  node [
    id 922
    label "Andaluzja"
  ]
  node [
    id 923
    label "&#379;ywiecczyzna"
  ]
  node [
    id 924
    label "Oksytania"
  ]
  node [
    id 925
    label "Opolszczyzna"
  ]
  node [
    id 926
    label "Kociewie"
  ]
  node [
    id 927
    label "Lasko"
  ]
  node [
    id 928
    label "woda"
  ]
  node [
    id 929
    label "linia"
  ]
  node [
    id 930
    label "zbi&#243;r"
  ]
  node [
    id 931
    label "ekoton"
  ]
  node [
    id 932
    label "str&#261;d"
  ]
  node [
    id 933
    label "koniec"
  ]
  node [
    id 934
    label "plantowa&#263;"
  ]
  node [
    id 935
    label "zapadnia"
  ]
  node [
    id 936
    label "budynek"
  ]
  node [
    id 937
    label "skorupa_ziemska"
  ]
  node [
    id 938
    label "glinowanie"
  ]
  node [
    id 939
    label "martwica"
  ]
  node [
    id 940
    label "teren"
  ]
  node [
    id 941
    label "litosfera"
  ]
  node [
    id 942
    label "penetrator"
  ]
  node [
    id 943
    label "glinowa&#263;"
  ]
  node [
    id 944
    label "domain"
  ]
  node [
    id 945
    label "podglebie"
  ]
  node [
    id 946
    label "kompleks_sorpcyjny"
  ]
  node [
    id 947
    label "miejsce"
  ]
  node [
    id 948
    label "kort"
  ]
  node [
    id 949
    label "czynnik_produkcji"
  ]
  node [
    id 950
    label "pojazd"
  ]
  node [
    id 951
    label "powierzchnia"
  ]
  node [
    id 952
    label "pr&#243;chnica"
  ]
  node [
    id 953
    label "ryzosfera"
  ]
  node [
    id 954
    label "dotleni&#263;"
  ]
  node [
    id 955
    label "glej"
  ]
  node [
    id 956
    label "posadzka"
  ]
  node [
    id 957
    label "geosystem"
  ]
  node [
    id 958
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 959
    label "przestrze&#324;"
  ]
  node [
    id 960
    label "podmiot"
  ]
  node [
    id 961
    label "jednostka_organizacyjna"
  ]
  node [
    id 962
    label "struktura"
  ]
  node [
    id 963
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 964
    label "TOPR"
  ]
  node [
    id 965
    label "endecki"
  ]
  node [
    id 966
    label "od&#322;am"
  ]
  node [
    id 967
    label "przedstawicielstwo"
  ]
  node [
    id 968
    label "Cepelia"
  ]
  node [
    id 969
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 970
    label "ZBoWiD"
  ]
  node [
    id 971
    label "organization"
  ]
  node [
    id 972
    label "centrala"
  ]
  node [
    id 973
    label "GOPR"
  ]
  node [
    id 974
    label "ZOMO"
  ]
  node [
    id 975
    label "ZMP"
  ]
  node [
    id 976
    label "komitet_koordynacyjny"
  ]
  node [
    id 977
    label "przybud&#243;wka"
  ]
  node [
    id 978
    label "boj&#243;wka"
  ]
  node [
    id 979
    label "p&#243;&#322;noc"
  ]
  node [
    id 980
    label "Kosowo"
  ]
  node [
    id 981
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 982
    label "Zab&#322;ocie"
  ]
  node [
    id 983
    label "zach&#243;d"
  ]
  node [
    id 984
    label "po&#322;udnie"
  ]
  node [
    id 985
    label "Pow&#261;zki"
  ]
  node [
    id 986
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 987
    label "Piotrowo"
  ]
  node [
    id 988
    label "Olszanica"
  ]
  node [
    id 989
    label "Ruda_Pabianicka"
  ]
  node [
    id 990
    label "holarktyka"
  ]
  node [
    id 991
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 992
    label "Ludwin&#243;w"
  ]
  node [
    id 993
    label "Arktyka"
  ]
  node [
    id 994
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 995
    label "Zabu&#380;e"
  ]
  node [
    id 996
    label "antroposfera"
  ]
  node [
    id 997
    label "Neogea"
  ]
  node [
    id 998
    label "Syberia_Zachodnia"
  ]
  node [
    id 999
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1000
    label "zakres"
  ]
  node [
    id 1001
    label "pas_planetoid"
  ]
  node [
    id 1002
    label "Syberia_Wschodnia"
  ]
  node [
    id 1003
    label "Antarktyka"
  ]
  node [
    id 1004
    label "Rakowice"
  ]
  node [
    id 1005
    label "akrecja"
  ]
  node [
    id 1006
    label "wymiar"
  ]
  node [
    id 1007
    label "&#321;&#281;g"
  ]
  node [
    id 1008
    label "Kresy_Zachodnie"
  ]
  node [
    id 1009
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1010
    label "wsch&#243;d"
  ]
  node [
    id 1011
    label "Notogea"
  ]
  node [
    id 1012
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 1013
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 1014
    label "Pend&#380;ab"
  ]
  node [
    id 1015
    label "funt_liba&#324;ski"
  ]
  node [
    id 1016
    label "strefa_euro"
  ]
  node [
    id 1017
    label "Pozna&#324;"
  ]
  node [
    id 1018
    label "lira_malta&#324;ska"
  ]
  node [
    id 1019
    label "Gozo"
  ]
  node [
    id 1020
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 1021
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 1022
    label "dolar_namibijski"
  ]
  node [
    id 1023
    label "milrejs"
  ]
  node [
    id 1024
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 1025
    label "NATO"
  ]
  node [
    id 1026
    label "escudo_portugalskie"
  ]
  node [
    id 1027
    label "dolar_bahamski"
  ]
  node [
    id 1028
    label "Wielka_Bahama"
  ]
  node [
    id 1029
    label "dolar_liberyjski"
  ]
  node [
    id 1030
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 1031
    label "riel"
  ]
  node [
    id 1032
    label "Karelia"
  ]
  node [
    id 1033
    label "Mari_El"
  ]
  node [
    id 1034
    label "Inguszetia"
  ]
  node [
    id 1035
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 1036
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 1037
    label "Udmurcja"
  ]
  node [
    id 1038
    label "Newa"
  ]
  node [
    id 1039
    label "&#321;adoga"
  ]
  node [
    id 1040
    label "Czeczenia"
  ]
  node [
    id 1041
    label "Anadyr"
  ]
  node [
    id 1042
    label "Syberia"
  ]
  node [
    id 1043
    label "Tatarstan"
  ]
  node [
    id 1044
    label "Wszechrosja"
  ]
  node [
    id 1045
    label "Azja"
  ]
  node [
    id 1046
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 1047
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 1048
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 1049
    label "Witim"
  ]
  node [
    id 1050
    label "Kamczatka"
  ]
  node [
    id 1051
    label "Jama&#322;"
  ]
  node [
    id 1052
    label "Dagestan"
  ]
  node [
    id 1053
    label "Tuwa"
  ]
  node [
    id 1054
    label "car"
  ]
  node [
    id 1055
    label "Komi"
  ]
  node [
    id 1056
    label "Czuwaszja"
  ]
  node [
    id 1057
    label "Chakasja"
  ]
  node [
    id 1058
    label "Perm"
  ]
  node [
    id 1059
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 1060
    label "Ajon"
  ]
  node [
    id 1061
    label "Adygeja"
  ]
  node [
    id 1062
    label "Dniepr"
  ]
  node [
    id 1063
    label "rubel_rosyjski"
  ]
  node [
    id 1064
    label "Don"
  ]
  node [
    id 1065
    label "Mordowia"
  ]
  node [
    id 1066
    label "s&#322;owianofilstwo"
  ]
  node [
    id 1067
    label "lew"
  ]
  node [
    id 1068
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 1069
    label "Dobrud&#380;a"
  ]
  node [
    id 1070
    label "Unia_Europejska"
  ]
  node [
    id 1071
    label "lira_izraelska"
  ]
  node [
    id 1072
    label "szekel"
  ]
  node [
    id 1073
    label "Galilea"
  ]
  node [
    id 1074
    label "Judea"
  ]
  node [
    id 1075
    label "Luksemburgia"
  ]
  node [
    id 1076
    label "frank_belgijski"
  ]
  node [
    id 1077
    label "Limburgia"
  ]
  node [
    id 1078
    label "Walonia"
  ]
  node [
    id 1079
    label "Brabancja"
  ]
  node [
    id 1080
    label "Flandria"
  ]
  node [
    id 1081
    label "Niderlandy"
  ]
  node [
    id 1082
    label "dinar_iracki"
  ]
  node [
    id 1083
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 1084
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 1085
    label "szyling_ugandyjski"
  ]
  node [
    id 1086
    label "dolar_jamajski"
  ]
  node [
    id 1087
    label "kafar"
  ]
  node [
    id 1088
    label "ringgit"
  ]
  node [
    id 1089
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 1090
    label "Borneo"
  ]
  node [
    id 1091
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 1092
    label "dolar_surinamski"
  ]
  node [
    id 1093
    label "funt_suda&#324;ski"
  ]
  node [
    id 1094
    label "dolar_guja&#324;ski"
  ]
  node [
    id 1095
    label "Manica"
  ]
  node [
    id 1096
    label "escudo_mozambickie"
  ]
  node [
    id 1097
    label "Cabo_Delgado"
  ]
  node [
    id 1098
    label "Inhambane"
  ]
  node [
    id 1099
    label "Maputo"
  ]
  node [
    id 1100
    label "Gaza"
  ]
  node [
    id 1101
    label "Niasa"
  ]
  node [
    id 1102
    label "Nampula"
  ]
  node [
    id 1103
    label "metical"
  ]
  node [
    id 1104
    label "Sahara"
  ]
  node [
    id 1105
    label "inti"
  ]
  node [
    id 1106
    label "sol"
  ]
  node [
    id 1107
    label "kip"
  ]
  node [
    id 1108
    label "Pireneje"
  ]
  node [
    id 1109
    label "euro"
  ]
  node [
    id 1110
    label "kwacha_zambijska"
  ]
  node [
    id 1111
    label "tugrik"
  ]
  node [
    id 1112
    label "Buriaci"
  ]
  node [
    id 1113
    label "ajmak"
  ]
  node [
    id 1114
    label "balboa"
  ]
  node [
    id 1115
    label "Ameryka_Centralna"
  ]
  node [
    id 1116
    label "dolar"
  ]
  node [
    id 1117
    label "gulden"
  ]
  node [
    id 1118
    label "Zelandia"
  ]
  node [
    id 1119
    label "manat_turkme&#324;ski"
  ]
  node [
    id 1120
    label "dolar_Tuvalu"
  ]
  node [
    id 1121
    label "zair"
  ]
  node [
    id 1122
    label "Katanga"
  ]
  node [
    id 1123
    label "frank_szwajcarski"
  ]
  node [
    id 1124
    label "Jukatan"
  ]
  node [
    id 1125
    label "dolar_Belize"
  ]
  node [
    id 1126
    label "colon"
  ]
  node [
    id 1127
    label "Dyja"
  ]
  node [
    id 1128
    label "korona_czeska"
  ]
  node [
    id 1129
    label "Izera"
  ]
  node [
    id 1130
    label "ugija"
  ]
  node [
    id 1131
    label "szyling_kenijski"
  ]
  node [
    id 1132
    label "Nachiczewan"
  ]
  node [
    id 1133
    label "manat_azerski"
  ]
  node [
    id 1134
    label "Karabach"
  ]
  node [
    id 1135
    label "Bengal"
  ]
  node [
    id 1136
    label "taka"
  ]
  node [
    id 1137
    label "Ocean_Spokojny"
  ]
  node [
    id 1138
    label "dolar_Kiribati"
  ]
  node [
    id 1139
    label "peso_filipi&#324;skie"
  ]
  node [
    id 1140
    label "Cebu"
  ]
  node [
    id 1141
    label "Atlantyk"
  ]
  node [
    id 1142
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 1143
    label "Ulster"
  ]
  node [
    id 1144
    label "funt_irlandzki"
  ]
  node [
    id 1145
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 1146
    label "cedi"
  ]
  node [
    id 1147
    label "ariary"
  ]
  node [
    id 1148
    label "Ocean_Indyjski"
  ]
  node [
    id 1149
    label "frank_malgaski"
  ]
  node [
    id 1150
    label "Estremadura"
  ]
  node [
    id 1151
    label "Kastylia"
  ]
  node [
    id 1152
    label "Aragonia"
  ]
  node [
    id 1153
    label "hacjender"
  ]
  node [
    id 1154
    label "Asturia"
  ]
  node [
    id 1155
    label "Baskonia"
  ]
  node [
    id 1156
    label "Majorka"
  ]
  node [
    id 1157
    label "Walencja"
  ]
  node [
    id 1158
    label "peseta"
  ]
  node [
    id 1159
    label "Katalonia"
  ]
  node [
    id 1160
    label "peso_chilijskie"
  ]
  node [
    id 1161
    label "Indie_Zachodnie"
  ]
  node [
    id 1162
    label "Sikkim"
  ]
  node [
    id 1163
    label "Asam"
  ]
  node [
    id 1164
    label "rupia_indyjska"
  ]
  node [
    id 1165
    label "Indie_Portugalskie"
  ]
  node [
    id 1166
    label "Indie_Wschodnie"
  ]
  node [
    id 1167
    label "Bollywood"
  ]
  node [
    id 1168
    label "jen"
  ]
  node [
    id 1169
    label "jinja"
  ]
  node [
    id 1170
    label "Okinawa"
  ]
  node [
    id 1171
    label "Japonica"
  ]
  node [
    id 1172
    label "Rugia"
  ]
  node [
    id 1173
    label "Saksonia"
  ]
  node [
    id 1174
    label "Dolna_Saksonia"
  ]
  node [
    id 1175
    label "Anglosas"
  ]
  node [
    id 1176
    label "Hesja"
  ]
  node [
    id 1177
    label "Wirtembergia"
  ]
  node [
    id 1178
    label "Po&#322;abie"
  ]
  node [
    id 1179
    label "Germania"
  ]
  node [
    id 1180
    label "Frankonia"
  ]
  node [
    id 1181
    label "Badenia"
  ]
  node [
    id 1182
    label "Holsztyn"
  ]
  node [
    id 1183
    label "marka"
  ]
  node [
    id 1184
    label "Brandenburgia"
  ]
  node [
    id 1185
    label "Szwabia"
  ]
  node [
    id 1186
    label "Niemcy_Zachodnie"
  ]
  node [
    id 1187
    label "Westfalia"
  ]
  node [
    id 1188
    label "Helgoland"
  ]
  node [
    id 1189
    label "Karlsbad"
  ]
  node [
    id 1190
    label "Niemcy_Wschodnie"
  ]
  node [
    id 1191
    label "Piemont"
  ]
  node [
    id 1192
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 1193
    label "Italia"
  ]
  node [
    id 1194
    label "Sardynia"
  ]
  node [
    id 1195
    label "Ok&#281;cie"
  ]
  node [
    id 1196
    label "Karyntia"
  ]
  node [
    id 1197
    label "Romania"
  ]
  node [
    id 1198
    label "Sycylia"
  ]
  node [
    id 1199
    label "Warszawa"
  ]
  node [
    id 1200
    label "lir"
  ]
  node [
    id 1201
    label "Dacja"
  ]
  node [
    id 1202
    label "lej_rumu&#324;ski"
  ]
  node [
    id 1203
    label "Siedmiogr&#243;d"
  ]
  node [
    id 1204
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 1205
    label "funt_syryjski"
  ]
  node [
    id 1206
    label "alawizm"
  ]
  node [
    id 1207
    label "frank_rwandyjski"
  ]
  node [
    id 1208
    label "dinar_Bahrajnu"
  ]
  node [
    id 1209
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 1210
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 1211
    label "frank_luksemburski"
  ]
  node [
    id 1212
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 1213
    label "peso_kuba&#324;skie"
  ]
  node [
    id 1214
    label "frank_monakijski"
  ]
  node [
    id 1215
    label "dinar_algierski"
  ]
  node [
    id 1216
    label "Wojwodina"
  ]
  node [
    id 1217
    label "dinar_serbski"
  ]
  node [
    id 1218
    label "boliwar"
  ]
  node [
    id 1219
    label "Orinoko"
  ]
  node [
    id 1220
    label "tenge"
  ]
  node [
    id 1221
    label "lek"
  ]
  node [
    id 1222
    label "frank_alba&#324;ski"
  ]
  node [
    id 1223
    label "dolar_Barbadosu"
  ]
  node [
    id 1224
    label "Antyle"
  ]
  node [
    id 1225
    label "kyat"
  ]
  node [
    id 1226
    label "Arakan"
  ]
  node [
    id 1227
    label "c&#243;rdoba"
  ]
  node [
    id 1228
    label "Paros"
  ]
  node [
    id 1229
    label "Epir"
  ]
  node [
    id 1230
    label "panhellenizm"
  ]
  node [
    id 1231
    label "Eubea"
  ]
  node [
    id 1232
    label "Rodos"
  ]
  node [
    id 1233
    label "Achaja"
  ]
  node [
    id 1234
    label "Termopile"
  ]
  node [
    id 1235
    label "Attyka"
  ]
  node [
    id 1236
    label "Hellada"
  ]
  node [
    id 1237
    label "Etolia"
  ]
  node [
    id 1238
    label "palestra"
  ]
  node [
    id 1239
    label "Kreta"
  ]
  node [
    id 1240
    label "drachma"
  ]
  node [
    id 1241
    label "Olimp"
  ]
  node [
    id 1242
    label "Tesalia"
  ]
  node [
    id 1243
    label "Peloponez"
  ]
  node [
    id 1244
    label "Eolia"
  ]
  node [
    id 1245
    label "Beocja"
  ]
  node [
    id 1246
    label "Parnas"
  ]
  node [
    id 1247
    label "Lesbos"
  ]
  node [
    id 1248
    label "Mariany"
  ]
  node [
    id 1249
    label "Salzburg"
  ]
  node [
    id 1250
    label "Rakuzy"
  ]
  node [
    id 1251
    label "konsulent"
  ]
  node [
    id 1252
    label "szyling_austryjacki"
  ]
  node [
    id 1253
    label "birr"
  ]
  node [
    id 1254
    label "negus"
  ]
  node [
    id 1255
    label "Jawa"
  ]
  node [
    id 1256
    label "Sumatra"
  ]
  node [
    id 1257
    label "rupia_indonezyjska"
  ]
  node [
    id 1258
    label "Nowa_Gwinea"
  ]
  node [
    id 1259
    label "Moluki"
  ]
  node [
    id 1260
    label "boliviano"
  ]
  node [
    id 1261
    label "Pikardia"
  ]
  node [
    id 1262
    label "Alzacja"
  ]
  node [
    id 1263
    label "Masyw_Centralny"
  ]
  node [
    id 1264
    label "Akwitania"
  ]
  node [
    id 1265
    label "Sekwana"
  ]
  node [
    id 1266
    label "Langwedocja"
  ]
  node [
    id 1267
    label "Martynika"
  ]
  node [
    id 1268
    label "Bretania"
  ]
  node [
    id 1269
    label "Sabaudia"
  ]
  node [
    id 1270
    label "Korsyka"
  ]
  node [
    id 1271
    label "Normandia"
  ]
  node [
    id 1272
    label "Gaskonia"
  ]
  node [
    id 1273
    label "Burgundia"
  ]
  node [
    id 1274
    label "frank_francuski"
  ]
  node [
    id 1275
    label "Wandea"
  ]
  node [
    id 1276
    label "Prowansja"
  ]
  node [
    id 1277
    label "Gwadelupa"
  ]
  node [
    id 1278
    label "somoni"
  ]
  node [
    id 1279
    label "Melanezja"
  ]
  node [
    id 1280
    label "dolar_Fid&#380;i"
  ]
  node [
    id 1281
    label "funt_cypryjski"
  ]
  node [
    id 1282
    label "Afrodyzje"
  ]
  node [
    id 1283
    label "peso_dominika&#324;skie"
  ]
  node [
    id 1284
    label "Fryburg"
  ]
  node [
    id 1285
    label "Bazylea"
  ]
  node [
    id 1286
    label "Alpy"
  ]
  node [
    id 1287
    label "Helwecja"
  ]
  node [
    id 1288
    label "Berno"
  ]
  node [
    id 1289
    label "sum"
  ]
  node [
    id 1290
    label "Karaka&#322;pacja"
  ]
  node [
    id 1291
    label "Kurlandia"
  ]
  node [
    id 1292
    label "Windawa"
  ]
  node [
    id 1293
    label "&#322;at"
  ]
  node [
    id 1294
    label "Liwonia"
  ]
  node [
    id 1295
    label "rubel_&#322;otewski"
  ]
  node [
    id 1296
    label "Inflanty"
  ]
  node [
    id 1297
    label "Wile&#324;szczyzna"
  ]
  node [
    id 1298
    label "&#379;mud&#378;"
  ]
  node [
    id 1299
    label "lit"
  ]
  node [
    id 1300
    label "frank_tunezyjski"
  ]
  node [
    id 1301
    label "dinar_tunezyjski"
  ]
  node [
    id 1302
    label "lempira"
  ]
  node [
    id 1303
    label "korona_w&#281;gierska"
  ]
  node [
    id 1304
    label "forint"
  ]
  node [
    id 1305
    label "Lipt&#243;w"
  ]
  node [
    id 1306
    label "dong"
  ]
  node [
    id 1307
    label "Annam"
  ]
  node [
    id 1308
    label "lud"
  ]
  node [
    id 1309
    label "frank_kongijski"
  ]
  node [
    id 1310
    label "szyling_somalijski"
  ]
  node [
    id 1311
    label "cruzado"
  ]
  node [
    id 1312
    label "real"
  ]
  node [
    id 1313
    label "Podole"
  ]
  node [
    id 1314
    label "Wsch&#243;d"
  ]
  node [
    id 1315
    label "Naddnieprze"
  ]
  node [
    id 1316
    label "Ma&#322;orosja"
  ]
  node [
    id 1317
    label "Wo&#322;y&#324;"
  ]
  node [
    id 1318
    label "Nadbu&#380;e"
  ]
  node [
    id 1319
    label "hrywna"
  ]
  node [
    id 1320
    label "Zaporo&#380;e"
  ]
  node [
    id 1321
    label "Krym"
  ]
  node [
    id 1322
    label "Dniestr"
  ]
  node [
    id 1323
    label "Przykarpacie"
  ]
  node [
    id 1324
    label "Kozaczyzna"
  ]
  node [
    id 1325
    label "karbowaniec"
  ]
  node [
    id 1326
    label "Tasmania"
  ]
  node [
    id 1327
    label "Nowy_&#346;wiat"
  ]
  node [
    id 1328
    label "dolar_australijski"
  ]
  node [
    id 1329
    label "gourde"
  ]
  node [
    id 1330
    label "escudo_angolskie"
  ]
  node [
    id 1331
    label "kwanza"
  ]
  node [
    id 1332
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1333
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 1334
    label "Ad&#380;aria"
  ]
  node [
    id 1335
    label "lari"
  ]
  node [
    id 1336
    label "naira"
  ]
  node [
    id 1337
    label "Ohio"
  ]
  node [
    id 1338
    label "P&#243;&#322;noc"
  ]
  node [
    id 1339
    label "Nowy_York"
  ]
  node [
    id 1340
    label "Illinois"
  ]
  node [
    id 1341
    label "Po&#322;udnie"
  ]
  node [
    id 1342
    label "Kalifornia"
  ]
  node [
    id 1343
    label "Wirginia"
  ]
  node [
    id 1344
    label "Teksas"
  ]
  node [
    id 1345
    label "Waszyngton"
  ]
  node [
    id 1346
    label "zielona_karta"
  ]
  node [
    id 1347
    label "Massachusetts"
  ]
  node [
    id 1348
    label "Alaska"
  ]
  node [
    id 1349
    label "Hawaje"
  ]
  node [
    id 1350
    label "Maryland"
  ]
  node [
    id 1351
    label "Michigan"
  ]
  node [
    id 1352
    label "Arizona"
  ]
  node [
    id 1353
    label "Georgia"
  ]
  node [
    id 1354
    label "stan_wolny"
  ]
  node [
    id 1355
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 1356
    label "Pensylwania"
  ]
  node [
    id 1357
    label "Luizjana"
  ]
  node [
    id 1358
    label "Nowy_Meksyk"
  ]
  node [
    id 1359
    label "Wuj_Sam"
  ]
  node [
    id 1360
    label "Alabama"
  ]
  node [
    id 1361
    label "Kansas"
  ]
  node [
    id 1362
    label "Oregon"
  ]
  node [
    id 1363
    label "Zach&#243;d"
  ]
  node [
    id 1364
    label "Floryda"
  ]
  node [
    id 1365
    label "Oklahoma"
  ]
  node [
    id 1366
    label "Hudson"
  ]
  node [
    id 1367
    label "som"
  ]
  node [
    id 1368
    label "peso_urugwajskie"
  ]
  node [
    id 1369
    label "denar_macedo&#324;ski"
  ]
  node [
    id 1370
    label "dolar_Brunei"
  ]
  node [
    id 1371
    label "rial_ira&#324;ski"
  ]
  node [
    id 1372
    label "mu&#322;&#322;a"
  ]
  node [
    id 1373
    label "Persja"
  ]
  node [
    id 1374
    label "d&#380;amahirijja"
  ]
  node [
    id 1375
    label "dinar_libijski"
  ]
  node [
    id 1376
    label "nakfa"
  ]
  node [
    id 1377
    label "rial_katarski"
  ]
  node [
    id 1378
    label "quetzal"
  ]
  node [
    id 1379
    label "won"
  ]
  node [
    id 1380
    label "rial_jeme&#324;ski"
  ]
  node [
    id 1381
    label "peso_argenty&#324;skie"
  ]
  node [
    id 1382
    label "guarani"
  ]
  node [
    id 1383
    label "perper"
  ]
  node [
    id 1384
    label "dinar_kuwejcki"
  ]
  node [
    id 1385
    label "dalasi"
  ]
  node [
    id 1386
    label "dolar_Zimbabwe"
  ]
  node [
    id 1387
    label "Szantung"
  ]
  node [
    id 1388
    label "Chiny_Zachodnie"
  ]
  node [
    id 1389
    label "Kuantung"
  ]
  node [
    id 1390
    label "D&#380;ungaria"
  ]
  node [
    id 1391
    label "yuan"
  ]
  node [
    id 1392
    label "Hongkong"
  ]
  node [
    id 1393
    label "Chiny_Wschodnie"
  ]
  node [
    id 1394
    label "Guangdong"
  ]
  node [
    id 1395
    label "Junnan"
  ]
  node [
    id 1396
    label "Mand&#380;uria"
  ]
  node [
    id 1397
    label "Syczuan"
  ]
  node [
    id 1398
    label "Pa&#322;uki"
  ]
  node [
    id 1399
    label "Wolin"
  ]
  node [
    id 1400
    label "z&#322;oty"
  ]
  node [
    id 1401
    label "So&#322;a"
  ]
  node [
    id 1402
    label "Krajna"
  ]
  node [
    id 1403
    label "Suwalszczyzna"
  ]
  node [
    id 1404
    label "barwy_polskie"
  ]
  node [
    id 1405
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 1406
    label "Kaczawa"
  ]
  node [
    id 1407
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 1408
    label "Wis&#322;a"
  ]
  node [
    id 1409
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 1410
    label "lira_turecka"
  ]
  node [
    id 1411
    label "Azja_Mniejsza"
  ]
  node [
    id 1412
    label "Ujgur"
  ]
  node [
    id 1413
    label "kuna"
  ]
  node [
    id 1414
    label "dram"
  ]
  node [
    id 1415
    label "tala"
  ]
  node [
    id 1416
    label "korona_s&#322;owacka"
  ]
  node [
    id 1417
    label "Turiec"
  ]
  node [
    id 1418
    label "Himalaje"
  ]
  node [
    id 1419
    label "rupia_nepalska"
  ]
  node [
    id 1420
    label "frank_gwinejski"
  ]
  node [
    id 1421
    label "korona_esto&#324;ska"
  ]
  node [
    id 1422
    label "marka_esto&#324;ska"
  ]
  node [
    id 1423
    label "Quebec"
  ]
  node [
    id 1424
    label "dolar_kanadyjski"
  ]
  node [
    id 1425
    label "Nowa_Fundlandia"
  ]
  node [
    id 1426
    label "Zanzibar"
  ]
  node [
    id 1427
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 1428
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 1429
    label "&#346;wite&#378;"
  ]
  node [
    id 1430
    label "peso_kolumbijskie"
  ]
  node [
    id 1431
    label "Synaj"
  ]
  node [
    id 1432
    label "paraszyt"
  ]
  node [
    id 1433
    label "funt_egipski"
  ]
  node [
    id 1434
    label "szach"
  ]
  node [
    id 1435
    label "Baktria"
  ]
  node [
    id 1436
    label "afgani"
  ]
  node [
    id 1437
    label "baht"
  ]
  node [
    id 1438
    label "tolar"
  ]
  node [
    id 1439
    label "lej_mo&#322;dawski"
  ]
  node [
    id 1440
    label "Gagauzja"
  ]
  node [
    id 1441
    label "moszaw"
  ]
  node [
    id 1442
    label "Kanaan"
  ]
  node [
    id 1443
    label "P&#243;&#322;wysep_Skandynawski"
  ]
  node [
    id 1444
    label "Jerozolima"
  ]
  node [
    id 1445
    label "Beskidy_Zachodnie"
  ]
  node [
    id 1446
    label "Wiktoria"
  ]
  node [
    id 1447
    label "Guernsey"
  ]
  node [
    id 1448
    label "Conrad"
  ]
  node [
    id 1449
    label "funt_szterling"
  ]
  node [
    id 1450
    label "Portland"
  ]
  node [
    id 1451
    label "El&#380;bieta_I"
  ]
  node [
    id 1452
    label "Kornwalia"
  ]
  node [
    id 1453
    label "Dolna_Frankonia"
  ]
  node [
    id 1454
    label "Karpaty"
  ]
  node [
    id 1455
    label "Beskid_Niski"
  ]
  node [
    id 1456
    label "Mariensztat"
  ]
  node [
    id 1457
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 1458
    label "Ziemia_Wielu&#324;ska"
  ]
  node [
    id 1459
    label "Paj&#281;czno"
  ]
  node [
    id 1460
    label "Mogielnica"
  ]
  node [
    id 1461
    label "Gop&#322;o"
  ]
  node [
    id 1462
    label "Moza"
  ]
  node [
    id 1463
    label "Poprad"
  ]
  node [
    id 1464
    label "Beskid_&#379;ywiecki"
  ]
  node [
    id 1465
    label "Nowe_Skalmierzyce"
  ]
  node [
    id 1466
    label "Bojanowo"
  ]
  node [
    id 1467
    label "Obra"
  ]
  node [
    id 1468
    label "Wilkowo_Polskie"
  ]
  node [
    id 1469
    label "Dobra"
  ]
  node [
    id 1470
    label "Pas&#322;&#281;ka"
  ]
  node [
    id 1471
    label "&#321;&#243;d&#378;"
  ]
  node [
    id 1472
    label "Etruria"
  ]
  node [
    id 1473
    label "Rumelia"
  ]
  node [
    id 1474
    label "Tar&#322;&#243;w"
  ]
  node [
    id 1475
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 1476
    label "Abchazja"
  ]
  node [
    id 1477
    label "Sarmata"
  ]
  node [
    id 1478
    label "Eurazja"
  ]
  node [
    id 1479
    label "Tatry"
  ]
  node [
    id 1480
    label "Podtatrze"
  ]
  node [
    id 1481
    label "Imperium_Rosyjskie"
  ]
  node [
    id 1482
    label "jezioro"
  ]
  node [
    id 1483
    label "&#346;l&#261;sk"
  ]
  node [
    id 1484
    label "Nysa_K&#322;odzka"
  ]
  node [
    id 1485
    label "rubel_naddniestrza&#324;ski"
  ]
  node [
    id 1486
    label "Ma&#322;opolska_Wschodnia"
  ]
  node [
    id 1487
    label "Austro-W&#281;gry"
  ]
  node [
    id 1488
    label "funt_szkocki"
  ]
  node [
    id 1489
    label "Kaledonia"
  ]
  node [
    id 1490
    label "Biskupice"
  ]
  node [
    id 1491
    label "Iwanowice"
  ]
  node [
    id 1492
    label "Ziemia_Sandomierska"
  ]
  node [
    id 1493
    label "Rogo&#378;nik"
  ]
  node [
    id 1494
    label "Ropa"
  ]
  node [
    id 1495
    label "Buriacja"
  ]
  node [
    id 1496
    label "Rozewie"
  ]
  node [
    id 1497
    label "Norwegia"
  ]
  node [
    id 1498
    label "Szwecja"
  ]
  node [
    id 1499
    label "Finlandia"
  ]
  node [
    id 1500
    label "Aruba"
  ]
  node [
    id 1501
    label "Kajmany"
  ]
  node [
    id 1502
    label "Anguilla"
  ]
  node [
    id 1503
    label "Amazonka"
  ]
  node [
    id 1504
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1505
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 1506
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 1507
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 1508
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 1509
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1510
    label "fight"
  ]
  node [
    id 1511
    label "wrestle"
  ]
  node [
    id 1512
    label "zawody"
  ]
  node [
    id 1513
    label "cope"
  ]
  node [
    id 1514
    label "contend"
  ]
  node [
    id 1515
    label "argue"
  ]
  node [
    id 1516
    label "my&#347;lenie"
  ]
  node [
    id 1517
    label "istnie&#263;"
  ]
  node [
    id 1518
    label "function"
  ]
  node [
    id 1519
    label "bangla&#263;"
  ]
  node [
    id 1520
    label "tryb"
  ]
  node [
    id 1521
    label "commit"
  ]
  node [
    id 1522
    label "proces_my&#347;lowy"
  ]
  node [
    id 1523
    label "domy&#347;lanie_si&#281;"
  ]
  node [
    id 1524
    label "pilnowanie"
  ]
  node [
    id 1525
    label "zjawisko"
  ]
  node [
    id 1526
    label "s&#261;dzenie"
  ]
  node [
    id 1527
    label "judgment"
  ]
  node [
    id 1528
    label "troskanie_si&#281;"
  ]
  node [
    id 1529
    label "wnioskowanie"
  ]
  node [
    id 1530
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 1531
    label "proces"
  ]
  node [
    id 1532
    label "treatment"
  ]
  node [
    id 1533
    label "walczenie"
  ]
  node [
    id 1534
    label "zinterpretowanie"
  ]
  node [
    id 1535
    label "skupianie_si&#281;"
  ]
  node [
    id 1536
    label "reflection"
  ]
  node [
    id 1537
    label "wzlecie&#263;"
  ]
  node [
    id 1538
    label "wzlecenie"
  ]
  node [
    id 1539
    label "impreza"
  ]
  node [
    id 1540
    label "contest"
  ]
  node [
    id 1541
    label "champion"
  ]
  node [
    id 1542
    label "rywalizacja"
  ]
  node [
    id 1543
    label "tysi&#281;cznik"
  ]
  node [
    id 1544
    label "spadochroniarstwo"
  ]
  node [
    id 1545
    label "kategoria_open"
  ]
  node [
    id 1546
    label "professional_wrestling"
  ]
  node [
    id 1547
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 1548
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 1549
    label "set_about"
  ]
  node [
    id 1550
    label "zapozna&#263;"
  ]
  node [
    id 1551
    label "approach"
  ]
  node [
    id 1552
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1553
    label "heat"
  ]
  node [
    id 1554
    label "deepen"
  ]
  node [
    id 1555
    label "put"
  ]
  node [
    id 1556
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 1557
    label "transfer"
  ]
  node [
    id 1558
    label "translate"
  ]
  node [
    id 1559
    label "picture"
  ]
  node [
    id 1560
    label "przedstawi&#263;"
  ]
  node [
    id 1561
    label "zrobi&#263;"
  ]
  node [
    id 1562
    label "zmieni&#263;"
  ]
  node [
    id 1563
    label "uzna&#263;"
  ]
  node [
    id 1564
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 1565
    label "przenie&#347;&#263;"
  ]
  node [
    id 1566
    label "prym"
  ]
  node [
    id 1567
    label "go"
  ]
  node [
    id 1568
    label "spowodowa&#263;"
  ]
  node [
    id 1569
    label "travel"
  ]
  node [
    id 1570
    label "zjednoczy&#263;"
  ]
  node [
    id 1571
    label "stworzy&#263;"
  ]
  node [
    id 1572
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1573
    label "incorporate"
  ]
  node [
    id 1574
    label "connect"
  ]
  node [
    id 1575
    label "relate"
  ]
  node [
    id 1576
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1577
    label "insert"
  ]
  node [
    id 1578
    label "obznajomi&#263;"
  ]
  node [
    id 1579
    label "zawrze&#263;"
  ]
  node [
    id 1580
    label "pozna&#263;"
  ]
  node [
    id 1581
    label "poinformowa&#263;"
  ]
  node [
    id 1582
    label "teach"
  ]
  node [
    id 1583
    label "problem"
  ]
  node [
    id 1584
    label "series"
  ]
  node [
    id 1585
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 1586
    label "uprawianie"
  ]
  node [
    id 1587
    label "praca_rolnicza"
  ]
  node [
    id 1588
    label "dane"
  ]
  node [
    id 1589
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1590
    label "pakiet_klimatyczny"
  ]
  node [
    id 1591
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 1592
    label "gathering"
  ]
  node [
    id 1593
    label "album"
  ]
  node [
    id 1594
    label "sprawa"
  ]
  node [
    id 1595
    label "subiekcja"
  ]
  node [
    id 1596
    label "problemat"
  ]
  node [
    id 1597
    label "jajko_Kolumba"
  ]
  node [
    id 1598
    label "obstruction"
  ]
  node [
    id 1599
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1600
    label "trudno&#347;&#263;"
  ]
  node [
    id 1601
    label "pierepa&#322;ka"
  ]
  node [
    id 1602
    label "ambaras"
  ]
  node [
    id 1603
    label "pies_policyjny"
  ]
  node [
    id 1604
    label "molos_typu_g&#243;rskiego"
  ]
  node [
    id 1605
    label "pies_str&#243;&#380;uj&#261;cy"
  ]
  node [
    id 1606
    label "owczarek"
  ]
  node [
    id 1607
    label "pies_pasterski"
  ]
  node [
    id 1608
    label "owczarz"
  ]
  node [
    id 1609
    label "pastuszek"
  ]
  node [
    id 1610
    label "tension"
  ]
  node [
    id 1611
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1612
    label "usztywnienie"
  ]
  node [
    id 1613
    label "striving"
  ]
  node [
    id 1614
    label "nastr&#243;j"
  ]
  node [
    id 1615
    label "napr&#281;&#380;enie"
  ]
  node [
    id 1616
    label "state"
  ]
  node [
    id 1617
    label "klimat"
  ]
  node [
    id 1618
    label "stan"
  ]
  node [
    id 1619
    label "samopoczucie"
  ]
  node [
    id 1620
    label "charakter"
  ]
  node [
    id 1621
    label "cecha"
  ]
  node [
    id 1622
    label "kwas"
  ]
  node [
    id 1623
    label "web"
  ]
  node [
    id 1624
    label "twardy"
  ]
  node [
    id 1625
    label "spowodowanie"
  ]
  node [
    id 1626
    label "unieruchomienie"
  ]
  node [
    id 1627
    label "stiffening"
  ]
  node [
    id 1628
    label "nietutejszy"
  ]
  node [
    id 1629
    label "obcy"
  ]
  node [
    id 1630
    label "Ka&#322;mucja"
  ]
  node [
    id 1631
    label "Singapur"
  ]
  node [
    id 1632
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 1633
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 1634
    label "Jakucja"
  ]
  node [
    id 1635
    label "ustr&#243;j"
  ]
  node [
    id 1636
    label "porz&#261;dek"
  ]
  node [
    id 1637
    label "podstawa"
  ]
  node [
    id 1638
    label "cia&#322;o"
  ]
  node [
    id 1639
    label "dolar_singapurski"
  ]
  node [
    id 1640
    label "Federacja_Rosyjska"
  ]
  node [
    id 1641
    label "Wepska_Gmina_Narodowa"
  ]
  node [
    id 1642
    label "Zyrianka"
  ]
  node [
    id 1643
    label "polityczny"
  ]
  node [
    id 1644
    label "sekretarz"
  ]
  node [
    id 1645
    label "generalny"
  ]
  node [
    id 1646
    label "KC"
  ]
  node [
    id 1647
    label "PZPR"
  ]
  node [
    id 1648
    label "Michai&#322;"
  ]
  node [
    id 1649
    label "Gorbaczow"
  ]
  node [
    id 1650
    label "stowarzyszy&#263;"
  ]
  node [
    id 1651
    label "&#8222;"
  ]
  node [
    id 1652
    label "&#8221;"
  ]
  node [
    id 1653
    label "zwi&#261;zka"
  ]
  node [
    id 1654
    label "poprze&#263;"
  ]
  node [
    id 1655
    label "pierestrojka"
  ]
  node [
    id 1656
    label "CzI"
  ]
  node [
    id 1657
    label "ASRR"
  ]
  node [
    id 1658
    label "front"
  ]
  node [
    id 1659
    label "ludowy"
  ]
  node [
    id 1660
    label "dok"
  ]
  node [
    id 1661
    label "Zawgajew"
  ]
  node [
    id 1662
    label "Czeczeno"
  ]
  node [
    id 1663
    label "Inguski"
  ]
  node [
    id 1664
    label "muzu&#322;ma&#324;ski"
  ]
  node [
    id 1665
    label "zarz&#261;d"
  ]
  node [
    id 1666
    label "duchowny"
  ]
  node [
    id 1667
    label "inicjatywa"
  ]
  node [
    id 1668
    label "demokratyczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 10
    target 753
  ]
  edge [
    source 10
    target 754
  ]
  edge [
    source 10
    target 40
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 755
  ]
  edge [
    source 10
    target 756
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 762
  ]
  edge [
    source 11
    target 763
  ]
  edge [
    source 11
    target 764
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 765
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 766
  ]
  edge [
    source 11
    target 767
  ]
  edge [
    source 11
    target 768
  ]
  edge [
    source 11
    target 769
  ]
  edge [
    source 11
    target 770
  ]
  edge [
    source 11
    target 771
  ]
  edge [
    source 11
    target 772
  ]
  edge [
    source 11
    target 773
  ]
  edge [
    source 11
    target 774
  ]
  edge [
    source 11
    target 775
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 235
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 783
  ]
  edge [
    source 13
    target 722
  ]
  edge [
    source 13
    target 784
  ]
  edge [
    source 13
    target 785
  ]
  edge [
    source 13
    target 786
  ]
  edge [
    source 13
    target 787
  ]
  edge [
    source 13
    target 788
  ]
  edge [
    source 13
    target 789
  ]
  edge [
    source 13
    target 790
  ]
  edge [
    source 13
    target 791
  ]
  edge [
    source 13
    target 792
  ]
  edge [
    source 13
    target 793
  ]
  edge [
    source 13
    target 794
  ]
  edge [
    source 13
    target 795
  ]
  edge [
    source 13
    target 796
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 332
  ]
  edge [
    source 17
    target 803
  ]
  edge [
    source 17
    target 804
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 404
  ]
  edge [
    source 18
    target 403
  ]
  edge [
    source 18
    target 805
  ]
  edge [
    source 18
    target 405
  ]
  edge [
    source 18
    target 806
  ]
  edge [
    source 18
    target 406
  ]
  edge [
    source 18
    target 807
  ]
  edge [
    source 18
    target 808
  ]
  edge [
    source 18
    target 408
  ]
  edge [
    source 18
    target 407
  ]
  edge [
    source 18
    target 809
  ]
  edge [
    source 18
    target 409
  ]
  edge [
    source 18
    target 410
  ]
  edge [
    source 18
    target 411
  ]
  edge [
    source 18
    target 412
  ]
  edge [
    source 18
    target 413
  ]
  edge [
    source 18
    target 415
  ]
  edge [
    source 18
    target 414
  ]
  edge [
    source 18
    target 810
  ]
  edge [
    source 18
    target 811
  ]
  edge [
    source 18
    target 812
  ]
  edge [
    source 18
    target 813
  ]
  edge [
    source 18
    target 416
  ]
  edge [
    source 18
    target 417
  ]
  edge [
    source 18
    target 418
  ]
  edge [
    source 18
    target 814
  ]
  edge [
    source 18
    target 220
  ]
  edge [
    source 18
    target 419
  ]
  edge [
    source 18
    target 815
  ]
  edge [
    source 18
    target 816
  ]
  edge [
    source 18
    target 420
  ]
  edge [
    source 18
    target 423
  ]
  edge [
    source 18
    target 422
  ]
  edge [
    source 18
    target 424
  ]
  edge [
    source 18
    target 425
  ]
  edge [
    source 18
    target 817
  ]
  edge [
    source 18
    target 426
  ]
  edge [
    source 18
    target 818
  ]
  edge [
    source 18
    target 427
  ]
  edge [
    source 18
    target 819
  ]
  edge [
    source 18
    target 431
  ]
  edge [
    source 18
    target 428
  ]
  edge [
    source 18
    target 430
  ]
  edge [
    source 18
    target 429
  ]
  edge [
    source 18
    target 820
  ]
  edge [
    source 18
    target 432
  ]
  edge [
    source 18
    target 821
  ]
  edge [
    source 18
    target 433
  ]
  edge [
    source 18
    target 822
  ]
  edge [
    source 18
    target 823
  ]
  edge [
    source 18
    target 434
  ]
  edge [
    source 18
    target 435
  ]
  edge [
    source 18
    target 824
  ]
  edge [
    source 18
    target 825
  ]
  edge [
    source 18
    target 436
  ]
  edge [
    source 18
    target 437
  ]
  edge [
    source 18
    target 438
  ]
  edge [
    source 18
    target 826
  ]
  edge [
    source 18
    target 827
  ]
  edge [
    source 18
    target 828
  ]
  edge [
    source 18
    target 829
  ]
  edge [
    source 18
    target 830
  ]
  edge [
    source 18
    target 439
  ]
  edge [
    source 18
    target 440
  ]
  edge [
    source 18
    target 831
  ]
  edge [
    source 18
    target 832
  ]
  edge [
    source 18
    target 441
  ]
  edge [
    source 18
    target 442
  ]
  edge [
    source 18
    target 833
  ]
  edge [
    source 18
    target 834
  ]
  edge [
    source 18
    target 835
  ]
  edge [
    source 18
    target 443
  ]
  edge [
    source 18
    target 444
  ]
  edge [
    source 18
    target 445
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 18
    target 836
  ]
  edge [
    source 18
    target 447
  ]
  edge [
    source 18
    target 837
  ]
  edge [
    source 18
    target 448
  ]
  edge [
    source 18
    target 449
  ]
  edge [
    source 18
    target 450
  ]
  edge [
    source 18
    target 451
  ]
  edge [
    source 18
    target 452
  ]
  edge [
    source 18
    target 454
  ]
  edge [
    source 18
    target 453
  ]
  edge [
    source 18
    target 838
  ]
  edge [
    source 18
    target 455
  ]
  edge [
    source 18
    target 457
  ]
  edge [
    source 18
    target 460
  ]
  edge [
    source 18
    target 839
  ]
  edge [
    source 18
    target 461
  ]
  edge [
    source 18
    target 840
  ]
  edge [
    source 18
    target 462
  ]
  edge [
    source 18
    target 841
  ]
  edge [
    source 18
    target 463
  ]
  edge [
    source 18
    target 464
  ]
  edge [
    source 18
    target 465
  ]
  edge [
    source 18
    target 842
  ]
  edge [
    source 18
    target 843
  ]
  edge [
    source 18
    target 844
  ]
  edge [
    source 18
    target 845
  ]
  edge [
    source 18
    target 846
  ]
  edge [
    source 18
    target 847
  ]
  edge [
    source 18
    target 848
  ]
  edge [
    source 18
    target 466
  ]
  edge [
    source 18
    target 467
  ]
  edge [
    source 18
    target 468
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 849
  ]
  edge [
    source 18
    target 850
  ]
  edge [
    source 18
    target 851
  ]
  edge [
    source 18
    target 852
  ]
  edge [
    source 18
    target 853
  ]
  edge [
    source 18
    target 471
  ]
  edge [
    source 18
    target 854
  ]
  edge [
    source 18
    target 475
  ]
  edge [
    source 18
    target 476
  ]
  edge [
    source 18
    target 473
  ]
  edge [
    source 18
    target 477
  ]
  edge [
    source 18
    target 478
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 484
  ]
  edge [
    source 18
    target 486
  ]
  edge [
    source 18
    target 485
  ]
  edge [
    source 18
    target 488
  ]
  edge [
    source 18
    target 487
  ]
  edge [
    source 18
    target 855
  ]
  edge [
    source 18
    target 489
  ]
  edge [
    source 18
    target 490
  ]
  edge [
    source 18
    target 493
  ]
  edge [
    source 18
    target 491
  ]
  edge [
    source 18
    target 492
  ]
  edge [
    source 18
    target 856
  ]
  edge [
    source 18
    target 857
  ]
  edge [
    source 18
    target 858
  ]
  edge [
    source 18
    target 859
  ]
  edge [
    source 18
    target 860
  ]
  edge [
    source 18
    target 494
  ]
  edge [
    source 18
    target 495
  ]
  edge [
    source 18
    target 497
  ]
  edge [
    source 18
    target 501
  ]
  edge [
    source 18
    target 861
  ]
  edge [
    source 18
    target 503
  ]
  edge [
    source 18
    target 502
  ]
  edge [
    source 18
    target 499
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 500
  ]
  edge [
    source 18
    target 504
  ]
  edge [
    source 18
    target 498
  ]
  edge [
    source 18
    target 506
  ]
  edge [
    source 18
    target 505
  ]
  edge [
    source 18
    target 507
  ]
  edge [
    source 18
    target 508
  ]
  edge [
    source 18
    target 509
  ]
  edge [
    source 18
    target 862
  ]
  edge [
    source 18
    target 863
  ]
  edge [
    source 18
    target 510
  ]
  edge [
    source 18
    target 864
  ]
  edge [
    source 18
    target 865
  ]
  edge [
    source 18
    target 866
  ]
  edge [
    source 18
    target 511
  ]
  edge [
    source 18
    target 512
  ]
  edge [
    source 18
    target 513
  ]
  edge [
    source 18
    target 515
  ]
  edge [
    source 18
    target 514
  ]
  edge [
    source 18
    target 516
  ]
  edge [
    source 18
    target 867
  ]
  edge [
    source 18
    target 868
  ]
  edge [
    source 18
    target 517
  ]
  edge [
    source 18
    target 869
  ]
  edge [
    source 18
    target 870
  ]
  edge [
    source 18
    target 518
  ]
  edge [
    source 18
    target 871
  ]
  edge [
    source 18
    target 519
  ]
  edge [
    source 18
    target 872
  ]
  edge [
    source 18
    target 520
  ]
  edge [
    source 18
    target 521
  ]
  edge [
    source 18
    target 522
  ]
  edge [
    source 18
    target 524
  ]
  edge [
    source 18
    target 525
  ]
  edge [
    source 18
    target 873
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 527
  ]
  edge [
    source 18
    target 874
  ]
  edge [
    source 18
    target 875
  ]
  edge [
    source 18
    target 529
  ]
  edge [
    source 18
    target 876
  ]
  edge [
    source 18
    target 877
  ]
  edge [
    source 18
    target 878
  ]
  edge [
    source 18
    target 533
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 531
  ]
  edge [
    source 18
    target 535
  ]
  edge [
    source 18
    target 879
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 539
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 542
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 544
  ]
  edge [
    source 18
    target 543
  ]
  edge [
    source 18
    target 880
  ]
  edge [
    source 18
    target 881
  ]
  edge [
    source 18
    target 882
  ]
  edge [
    source 18
    target 545
  ]
  edge [
    source 18
    target 548
  ]
  edge [
    source 18
    target 547
  ]
  edge [
    source 18
    target 883
  ]
  edge [
    source 18
    target 546
  ]
  edge [
    source 18
    target 549
  ]
  edge [
    source 18
    target 550
  ]
  edge [
    source 18
    target 884
  ]
  edge [
    source 18
    target 885
  ]
  edge [
    source 18
    target 551
  ]
  edge [
    source 18
    target 552
  ]
  edge [
    source 18
    target 553
  ]
  edge [
    source 18
    target 886
  ]
  edge [
    source 18
    target 554
  ]
  edge [
    source 18
    target 555
  ]
  edge [
    source 18
    target 887
  ]
  edge [
    source 18
    target 888
  ]
  edge [
    source 18
    target 889
  ]
  edge [
    source 18
    target 556
  ]
  edge [
    source 18
    target 890
  ]
  edge [
    source 18
    target 891
  ]
  edge [
    source 18
    target 557
  ]
  edge [
    source 18
    target 892
  ]
  edge [
    source 18
    target 558
  ]
  edge [
    source 18
    target 893
  ]
  edge [
    source 18
    target 559
  ]
  edge [
    source 18
    target 894
  ]
  edge [
    source 18
    target 895
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 561
  ]
  edge [
    source 18
    target 896
  ]
  edge [
    source 18
    target 562
  ]
  edge [
    source 18
    target 897
  ]
  edge [
    source 18
    target 563
  ]
  edge [
    source 18
    target 898
  ]
  edge [
    source 18
    target 899
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 900
  ]
  edge [
    source 18
    target 901
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 568
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 902
  ]
  edge [
    source 18
    target 903
  ]
  edge [
    source 18
    target 904
  ]
  edge [
    source 18
    target 905
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 906
  ]
  edge [
    source 18
    target 576
  ]
  edge [
    source 18
    target 577
  ]
  edge [
    source 18
    target 578
  ]
  edge [
    source 18
    target 579
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 907
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 908
  ]
  edge [
    source 18
    target 909
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 910
  ]
  edge [
    source 18
    target 911
  ]
  edge [
    source 18
    target 912
  ]
  edge [
    source 18
    target 913
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 914
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 915
  ]
  edge [
    source 18
    target 916
  ]
  edge [
    source 18
    target 917
  ]
  edge [
    source 18
    target 918
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 919
  ]
  edge [
    source 18
    target 920
  ]
  edge [
    source 18
    target 921
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 922
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 923
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 924
  ]
  edge [
    source 18
    target 925
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 926
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 613
  ]
  edge [
    source 18
    target 927
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 615
  ]
  edge [
    source 18
    target 616
  ]
  edge [
    source 18
    target 928
  ]
  edge [
    source 18
    target 929
  ]
  edge [
    source 18
    target 930
  ]
  edge [
    source 18
    target 931
  ]
  edge [
    source 18
    target 932
  ]
  edge [
    source 18
    target 933
  ]
  edge [
    source 18
    target 934
  ]
  edge [
    source 18
    target 935
  ]
  edge [
    source 18
    target 936
  ]
  edge [
    source 18
    target 937
  ]
  edge [
    source 18
    target 938
  ]
  edge [
    source 18
    target 939
  ]
  edge [
    source 18
    target 940
  ]
  edge [
    source 18
    target 941
  ]
  edge [
    source 18
    target 942
  ]
  edge [
    source 18
    target 943
  ]
  edge [
    source 18
    target 944
  ]
  edge [
    source 18
    target 945
  ]
  edge [
    source 18
    target 946
  ]
  edge [
    source 18
    target 947
  ]
  edge [
    source 18
    target 948
  ]
  edge [
    source 18
    target 949
  ]
  edge [
    source 18
    target 950
  ]
  edge [
    source 18
    target 951
  ]
  edge [
    source 18
    target 952
  ]
  edge [
    source 18
    target 259
  ]
  edge [
    source 18
    target 953
  ]
  edge [
    source 18
    target 260
  ]
  edge [
    source 18
    target 954
  ]
  edge [
    source 18
    target 955
  ]
  edge [
    source 18
    target 397
  ]
  edge [
    source 18
    target 956
  ]
  edge [
    source 18
    target 957
  ]
  edge [
    source 18
    target 958
  ]
  edge [
    source 18
    target 959
  ]
  edge [
    source 18
    target 960
  ]
  edge [
    source 18
    target 961
  ]
  edge [
    source 18
    target 962
  ]
  edge [
    source 18
    target 963
  ]
  edge [
    source 18
    target 964
  ]
  edge [
    source 18
    target 965
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 966
  ]
  edge [
    source 18
    target 967
  ]
  edge [
    source 18
    target 968
  ]
  edge [
    source 18
    target 969
  ]
  edge [
    source 18
    target 970
  ]
  edge [
    source 18
    target 971
  ]
  edge [
    source 18
    target 972
  ]
  edge [
    source 18
    target 973
  ]
  edge [
    source 18
    target 974
  ]
  edge [
    source 18
    target 975
  ]
  edge [
    source 18
    target 976
  ]
  edge [
    source 18
    target 977
  ]
  edge [
    source 18
    target 978
  ]
  edge [
    source 18
    target 979
  ]
  edge [
    source 18
    target 980
  ]
  edge [
    source 18
    target 981
  ]
  edge [
    source 18
    target 982
  ]
  edge [
    source 18
    target 983
  ]
  edge [
    source 18
    target 984
  ]
  edge [
    source 18
    target 985
  ]
  edge [
    source 18
    target 986
  ]
  edge [
    source 18
    target 987
  ]
  edge [
    source 18
    target 988
  ]
  edge [
    source 18
    target 989
  ]
  edge [
    source 18
    target 990
  ]
  edge [
    source 18
    target 991
  ]
  edge [
    source 18
    target 992
  ]
  edge [
    source 18
    target 993
  ]
  edge [
    source 18
    target 994
  ]
  edge [
    source 18
    target 995
  ]
  edge [
    source 18
    target 996
  ]
  edge [
    source 18
    target 997
  ]
  edge [
    source 18
    target 532
  ]
  edge [
    source 18
    target 998
  ]
  edge [
    source 18
    target 999
  ]
  edge [
    source 18
    target 1000
  ]
  edge [
    source 18
    target 1001
  ]
  edge [
    source 18
    target 1002
  ]
  edge [
    source 18
    target 1003
  ]
  edge [
    source 18
    target 1004
  ]
  edge [
    source 18
    target 1005
  ]
  edge [
    source 18
    target 1006
  ]
  edge [
    source 18
    target 1007
  ]
  edge [
    source 18
    target 1008
  ]
  edge [
    source 18
    target 1009
  ]
  edge [
    source 18
    target 1010
  ]
  edge [
    source 18
    target 1011
  ]
  edge [
    source 18
    target 1012
  ]
  edge [
    source 18
    target 1013
  ]
  edge [
    source 18
    target 1014
  ]
  edge [
    source 18
    target 1015
  ]
  edge [
    source 18
    target 1016
  ]
  edge [
    source 18
    target 1017
  ]
  edge [
    source 18
    target 1018
  ]
  edge [
    source 18
    target 1019
  ]
  edge [
    source 18
    target 1020
  ]
  edge [
    source 18
    target 1021
  ]
  edge [
    source 18
    target 1022
  ]
  edge [
    source 18
    target 1023
  ]
  edge [
    source 18
    target 1024
  ]
  edge [
    source 18
    target 1025
  ]
  edge [
    source 18
    target 1026
  ]
  edge [
    source 18
    target 1027
  ]
  edge [
    source 18
    target 1028
  ]
  edge [
    source 18
    target 1029
  ]
  edge [
    source 18
    target 1030
  ]
  edge [
    source 18
    target 1031
  ]
  edge [
    source 18
    target 1032
  ]
  edge [
    source 18
    target 1033
  ]
  edge [
    source 18
    target 1034
  ]
  edge [
    source 18
    target 1035
  ]
  edge [
    source 18
    target 1036
  ]
  edge [
    source 18
    target 1037
  ]
  edge [
    source 18
    target 1038
  ]
  edge [
    source 18
    target 1039
  ]
  edge [
    source 18
    target 1040
  ]
  edge [
    source 18
    target 1041
  ]
  edge [
    source 18
    target 1042
  ]
  edge [
    source 18
    target 1043
  ]
  edge [
    source 18
    target 1044
  ]
  edge [
    source 18
    target 1045
  ]
  edge [
    source 18
    target 1046
  ]
  edge [
    source 18
    target 1047
  ]
  edge [
    source 18
    target 1048
  ]
  edge [
    source 18
    target 1049
  ]
  edge [
    source 18
    target 1050
  ]
  edge [
    source 18
    target 1051
  ]
  edge [
    source 18
    target 1052
  ]
  edge [
    source 18
    target 1053
  ]
  edge [
    source 18
    target 1054
  ]
  edge [
    source 18
    target 1055
  ]
  edge [
    source 18
    target 1056
  ]
  edge [
    source 18
    target 1057
  ]
  edge [
    source 18
    target 1058
  ]
  edge [
    source 18
    target 1059
  ]
  edge [
    source 18
    target 1060
  ]
  edge [
    source 18
    target 1061
  ]
  edge [
    source 18
    target 1062
  ]
  edge [
    source 18
    target 1063
  ]
  edge [
    source 18
    target 1064
  ]
  edge [
    source 18
    target 1065
  ]
  edge [
    source 18
    target 1066
  ]
  edge [
    source 18
    target 1067
  ]
  edge [
    source 18
    target 1068
  ]
  edge [
    source 18
    target 1069
  ]
  edge [
    source 18
    target 1070
  ]
  edge [
    source 18
    target 1071
  ]
  edge [
    source 18
    target 1072
  ]
  edge [
    source 18
    target 1073
  ]
  edge [
    source 18
    target 1074
  ]
  edge [
    source 18
    target 1075
  ]
  edge [
    source 18
    target 1076
  ]
  edge [
    source 18
    target 1077
  ]
  edge [
    source 18
    target 1078
  ]
  edge [
    source 18
    target 1079
  ]
  edge [
    source 18
    target 1080
  ]
  edge [
    source 18
    target 1081
  ]
  edge [
    source 18
    target 1082
  ]
  edge [
    source 18
    target 1083
  ]
  edge [
    source 18
    target 1084
  ]
  edge [
    source 18
    target 1085
  ]
  edge [
    source 18
    target 1086
  ]
  edge [
    source 18
    target 1087
  ]
  edge [
    source 18
    target 1088
  ]
  edge [
    source 18
    target 1089
  ]
  edge [
    source 18
    target 1090
  ]
  edge [
    source 18
    target 1091
  ]
  edge [
    source 18
    target 1092
  ]
  edge [
    source 18
    target 1093
  ]
  edge [
    source 18
    target 1094
  ]
  edge [
    source 18
    target 1095
  ]
  edge [
    source 18
    target 1096
  ]
  edge [
    source 18
    target 1097
  ]
  edge [
    source 18
    target 1098
  ]
  edge [
    source 18
    target 1099
  ]
  edge [
    source 18
    target 1100
  ]
  edge [
    source 18
    target 1101
  ]
  edge [
    source 18
    target 1102
  ]
  edge [
    source 18
    target 1103
  ]
  edge [
    source 18
    target 1104
  ]
  edge [
    source 18
    target 1105
  ]
  edge [
    source 18
    target 1106
  ]
  edge [
    source 18
    target 1107
  ]
  edge [
    source 18
    target 1108
  ]
  edge [
    source 18
    target 1109
  ]
  edge [
    source 18
    target 1110
  ]
  edge [
    source 18
    target 1111
  ]
  edge [
    source 18
    target 1112
  ]
  edge [
    source 18
    target 1113
  ]
  edge [
    source 18
    target 1114
  ]
  edge [
    source 18
    target 1115
  ]
  edge [
    source 18
    target 1116
  ]
  edge [
    source 18
    target 1117
  ]
  edge [
    source 18
    target 1118
  ]
  edge [
    source 18
    target 1119
  ]
  edge [
    source 18
    target 1120
  ]
  edge [
    source 18
    target 1121
  ]
  edge [
    source 18
    target 1122
  ]
  edge [
    source 18
    target 1123
  ]
  edge [
    source 18
    target 1124
  ]
  edge [
    source 18
    target 1125
  ]
  edge [
    source 18
    target 1126
  ]
  edge [
    source 18
    target 1127
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 18
    target 1145
  ]
  edge [
    source 18
    target 1146
  ]
  edge [
    source 18
    target 1147
  ]
  edge [
    source 18
    target 1148
  ]
  edge [
    source 18
    target 1149
  ]
  edge [
    source 18
    target 1150
  ]
  edge [
    source 18
    target 1151
  ]
  edge [
    source 18
    target 778
  ]
  edge [
    source 18
    target 1152
  ]
  edge [
    source 18
    target 1153
  ]
  edge [
    source 18
    target 1154
  ]
  edge [
    source 18
    target 1155
  ]
  edge [
    source 18
    target 1156
  ]
  edge [
    source 18
    target 1157
  ]
  edge [
    source 18
    target 1158
  ]
  edge [
    source 18
    target 1159
  ]
  edge [
    source 18
    target 1160
  ]
  edge [
    source 18
    target 1161
  ]
  edge [
    source 18
    target 1162
  ]
  edge [
    source 18
    target 1163
  ]
  edge [
    source 18
    target 1164
  ]
  edge [
    source 18
    target 1165
  ]
  edge [
    source 18
    target 1166
  ]
  edge [
    source 18
    target 1167
  ]
  edge [
    source 18
    target 1168
  ]
  edge [
    source 18
    target 1169
  ]
  edge [
    source 18
    target 1170
  ]
  edge [
    source 18
    target 1171
  ]
  edge [
    source 18
    target 1172
  ]
  edge [
    source 18
    target 1173
  ]
  edge [
    source 18
    target 1174
  ]
  edge [
    source 18
    target 1175
  ]
  edge [
    source 18
    target 1176
  ]
  edge [
    source 18
    target 1177
  ]
  edge [
    source 18
    target 1178
  ]
  edge [
    source 18
    target 1179
  ]
  edge [
    source 18
    target 1180
  ]
  edge [
    source 18
    target 1181
  ]
  edge [
    source 18
    target 1182
  ]
  edge [
    source 18
    target 1183
  ]
  edge [
    source 18
    target 1184
  ]
  edge [
    source 18
    target 1185
  ]
  edge [
    source 18
    target 1186
  ]
  edge [
    source 18
    target 1187
  ]
  edge [
    source 18
    target 1188
  ]
  edge [
    source 18
    target 1189
  ]
  edge [
    source 18
    target 1190
  ]
  edge [
    source 18
    target 1191
  ]
  edge [
    source 18
    target 1192
  ]
  edge [
    source 18
    target 1193
  ]
  edge [
    source 18
    target 1194
  ]
  edge [
    source 18
    target 1195
  ]
  edge [
    source 18
    target 1196
  ]
  edge [
    source 18
    target 1197
  ]
  edge [
    source 18
    target 1198
  ]
  edge [
    source 18
    target 1199
  ]
  edge [
    source 18
    target 1200
  ]
  edge [
    source 18
    target 1201
  ]
  edge [
    source 18
    target 1202
  ]
  edge [
    source 18
    target 1203
  ]
  edge [
    source 18
    target 1204
  ]
  edge [
    source 18
    target 1205
  ]
  edge [
    source 18
    target 1206
  ]
  edge [
    source 18
    target 1207
  ]
  edge [
    source 18
    target 1208
  ]
  edge [
    source 18
    target 1209
  ]
  edge [
    source 18
    target 1210
  ]
  edge [
    source 18
    target 1211
  ]
  edge [
    source 18
    target 1212
  ]
  edge [
    source 18
    target 1213
  ]
  edge [
    source 18
    target 1214
  ]
  edge [
    source 18
    target 1215
  ]
  edge [
    source 18
    target 1216
  ]
  edge [
    source 18
    target 1217
  ]
  edge [
    source 18
    target 1218
  ]
  edge [
    source 18
    target 1219
  ]
  edge [
    source 18
    target 1220
  ]
  edge [
    source 18
    target 470
  ]
  edge [
    source 18
    target 1221
  ]
  edge [
    source 18
    target 1222
  ]
  edge [
    source 18
    target 1223
  ]
  edge [
    source 18
    target 1224
  ]
  edge [
    source 18
    target 1225
  ]
  edge [
    source 18
    target 1226
  ]
  edge [
    source 18
    target 1227
  ]
  edge [
    source 18
    target 1228
  ]
  edge [
    source 18
    target 1229
  ]
  edge [
    source 18
    target 1230
  ]
  edge [
    source 18
    target 1231
  ]
  edge [
    source 18
    target 1232
  ]
  edge [
    source 18
    target 1233
  ]
  edge [
    source 18
    target 1234
  ]
  edge [
    source 18
    target 1235
  ]
  edge [
    source 18
    target 1236
  ]
  edge [
    source 18
    target 1237
  ]
  edge [
    source 18
    target 1238
  ]
  edge [
    source 18
    target 1239
  ]
  edge [
    source 18
    target 1240
  ]
  edge [
    source 18
    target 1241
  ]
  edge [
    source 18
    target 1242
  ]
  edge [
    source 18
    target 1243
  ]
  edge [
    source 18
    target 1244
  ]
  edge [
    source 18
    target 1245
  ]
  edge [
    source 18
    target 1246
  ]
  edge [
    source 18
    target 1247
  ]
  edge [
    source 18
    target 1248
  ]
  edge [
    source 18
    target 1249
  ]
  edge [
    source 18
    target 1250
  ]
  edge [
    source 18
    target 1251
  ]
  edge [
    source 18
    target 1252
  ]
  edge [
    source 18
    target 1253
  ]
  edge [
    source 18
    target 1254
  ]
  edge [
    source 18
    target 1255
  ]
  edge [
    source 18
    target 1256
  ]
  edge [
    source 18
    target 1257
  ]
  edge [
    source 18
    target 1258
  ]
  edge [
    source 18
    target 1259
  ]
  edge [
    source 18
    target 1260
  ]
  edge [
    source 18
    target 1261
  ]
  edge [
    source 18
    target 1262
  ]
  edge [
    source 18
    target 1263
  ]
  edge [
    source 18
    target 1264
  ]
  edge [
    source 18
    target 1265
  ]
  edge [
    source 18
    target 1266
  ]
  edge [
    source 18
    target 1267
  ]
  edge [
    source 18
    target 1268
  ]
  edge [
    source 18
    target 1269
  ]
  edge [
    source 18
    target 1270
  ]
  edge [
    source 18
    target 1271
  ]
  edge [
    source 18
    target 1272
  ]
  edge [
    source 18
    target 1273
  ]
  edge [
    source 18
    target 1274
  ]
  edge [
    source 18
    target 1275
  ]
  edge [
    source 18
    target 1276
  ]
  edge [
    source 18
    target 1277
  ]
  edge [
    source 18
    target 1278
  ]
  edge [
    source 18
    target 1279
  ]
  edge [
    source 18
    target 1280
  ]
  edge [
    source 18
    target 1281
  ]
  edge [
    source 18
    target 1282
  ]
  edge [
    source 18
    target 1283
  ]
  edge [
    source 18
    target 1284
  ]
  edge [
    source 18
    target 1285
  ]
  edge [
    source 18
    target 1286
  ]
  edge [
    source 18
    target 1287
  ]
  edge [
    source 18
    target 1288
  ]
  edge [
    source 18
    target 1289
  ]
  edge [
    source 18
    target 1290
  ]
  edge [
    source 18
    target 1291
  ]
  edge [
    source 18
    target 1292
  ]
  edge [
    source 18
    target 1293
  ]
  edge [
    source 18
    target 1294
  ]
  edge [
    source 18
    target 1295
  ]
  edge [
    source 18
    target 1296
  ]
  edge [
    source 18
    target 1297
  ]
  edge [
    source 18
    target 1298
  ]
  edge [
    source 18
    target 1299
  ]
  edge [
    source 18
    target 1300
  ]
  edge [
    source 18
    target 1301
  ]
  edge [
    source 18
    target 1302
  ]
  edge [
    source 18
    target 1303
  ]
  edge [
    source 18
    target 1304
  ]
  edge [
    source 18
    target 1305
  ]
  edge [
    source 18
    target 1306
  ]
  edge [
    source 18
    target 1307
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1309
  ]
  edge [
    source 18
    target 1310
  ]
  edge [
    source 18
    target 1311
  ]
  edge [
    source 18
    target 1312
  ]
  edge [
    source 18
    target 1313
  ]
  edge [
    source 18
    target 1314
  ]
  edge [
    source 18
    target 1315
  ]
  edge [
    source 18
    target 1316
  ]
  edge [
    source 18
    target 1317
  ]
  edge [
    source 18
    target 1318
  ]
  edge [
    source 18
    target 1319
  ]
  edge [
    source 18
    target 1320
  ]
  edge [
    source 18
    target 1321
  ]
  edge [
    source 18
    target 1322
  ]
  edge [
    source 18
    target 1323
  ]
  edge [
    source 18
    target 1324
  ]
  edge [
    source 18
    target 1325
  ]
  edge [
    source 18
    target 1326
  ]
  edge [
    source 18
    target 1327
  ]
  edge [
    source 18
    target 1328
  ]
  edge [
    source 18
    target 1329
  ]
  edge [
    source 18
    target 1330
  ]
  edge [
    source 18
    target 1331
  ]
  edge [
    source 18
    target 1332
  ]
  edge [
    source 18
    target 1333
  ]
  edge [
    source 18
    target 1334
  ]
  edge [
    source 18
    target 1335
  ]
  edge [
    source 18
    target 1336
  ]
  edge [
    source 18
    target 1337
  ]
  edge [
    source 18
    target 1338
  ]
  edge [
    source 18
    target 1339
  ]
  edge [
    source 18
    target 1340
  ]
  edge [
    source 18
    target 1341
  ]
  edge [
    source 18
    target 1342
  ]
  edge [
    source 18
    target 1343
  ]
  edge [
    source 18
    target 1344
  ]
  edge [
    source 18
    target 1345
  ]
  edge [
    source 18
    target 1346
  ]
  edge [
    source 18
    target 1347
  ]
  edge [
    source 18
    target 1348
  ]
  edge [
    source 18
    target 1349
  ]
  edge [
    source 18
    target 1350
  ]
  edge [
    source 18
    target 1351
  ]
  edge [
    source 18
    target 1352
  ]
  edge [
    source 18
    target 1353
  ]
  edge [
    source 18
    target 1354
  ]
  edge [
    source 18
    target 1355
  ]
  edge [
    source 18
    target 1356
  ]
  edge [
    source 18
    target 1357
  ]
  edge [
    source 18
    target 1358
  ]
  edge [
    source 18
    target 1359
  ]
  edge [
    source 18
    target 1360
  ]
  edge [
    source 18
    target 1361
  ]
  edge [
    source 18
    target 1362
  ]
  edge [
    source 18
    target 1363
  ]
  edge [
    source 18
    target 1364
  ]
  edge [
    source 18
    target 1365
  ]
  edge [
    source 18
    target 1366
  ]
  edge [
    source 18
    target 1367
  ]
  edge [
    source 18
    target 1368
  ]
  edge [
    source 18
    target 1369
  ]
  edge [
    source 18
    target 1370
  ]
  edge [
    source 18
    target 1371
  ]
  edge [
    source 18
    target 1372
  ]
  edge [
    source 18
    target 1373
  ]
  edge [
    source 18
    target 1374
  ]
  edge [
    source 18
    target 1375
  ]
  edge [
    source 18
    target 1376
  ]
  edge [
    source 18
    target 1377
  ]
  edge [
    source 18
    target 1378
  ]
  edge [
    source 18
    target 1379
  ]
  edge [
    source 18
    target 1380
  ]
  edge [
    source 18
    target 1381
  ]
  edge [
    source 18
    target 1382
  ]
  edge [
    source 18
    target 1383
  ]
  edge [
    source 18
    target 1384
  ]
  edge [
    source 18
    target 1385
  ]
  edge [
    source 18
    target 1386
  ]
  edge [
    source 18
    target 1387
  ]
  edge [
    source 18
    target 1388
  ]
  edge [
    source 18
    target 1389
  ]
  edge [
    source 18
    target 1390
  ]
  edge [
    source 18
    target 1391
  ]
  edge [
    source 18
    target 1392
  ]
  edge [
    source 18
    target 1393
  ]
  edge [
    source 18
    target 1394
  ]
  edge [
    source 18
    target 1395
  ]
  edge [
    source 18
    target 1396
  ]
  edge [
    source 18
    target 1397
  ]
  edge [
    source 18
    target 1398
  ]
  edge [
    source 18
    target 1399
  ]
  edge [
    source 18
    target 1400
  ]
  edge [
    source 18
    target 1401
  ]
  edge [
    source 18
    target 1402
  ]
  edge [
    source 18
    target 1403
  ]
  edge [
    source 18
    target 1404
  ]
  edge [
    source 18
    target 1405
  ]
  edge [
    source 18
    target 1406
  ]
  edge [
    source 18
    target 1407
  ]
  edge [
    source 18
    target 1408
  ]
  edge [
    source 18
    target 1409
  ]
  edge [
    source 18
    target 1410
  ]
  edge [
    source 18
    target 1411
  ]
  edge [
    source 18
    target 1412
  ]
  edge [
    source 18
    target 1413
  ]
  edge [
    source 18
    target 1414
  ]
  edge [
    source 18
    target 1415
  ]
  edge [
    source 18
    target 1416
  ]
  edge [
    source 18
    target 1417
  ]
  edge [
    source 18
    target 1418
  ]
  edge [
    source 18
    target 1419
  ]
  edge [
    source 18
    target 1420
  ]
  edge [
    source 18
    target 1421
  ]
  edge [
    source 18
    target 1422
  ]
  edge [
    source 18
    target 1423
  ]
  edge [
    source 18
    target 1424
  ]
  edge [
    source 18
    target 1425
  ]
  edge [
    source 18
    target 1426
  ]
  edge [
    source 18
    target 1427
  ]
  edge [
    source 18
    target 1428
  ]
  edge [
    source 18
    target 1429
  ]
  edge [
    source 18
    target 1430
  ]
  edge [
    source 18
    target 1431
  ]
  edge [
    source 18
    target 1432
  ]
  edge [
    source 18
    target 1433
  ]
  edge [
    source 18
    target 1434
  ]
  edge [
    source 18
    target 1435
  ]
  edge [
    source 18
    target 1436
  ]
  edge [
    source 18
    target 1437
  ]
  edge [
    source 18
    target 1438
  ]
  edge [
    source 18
    target 1439
  ]
  edge [
    source 18
    target 1440
  ]
  edge [
    source 18
    target 1441
  ]
  edge [
    source 18
    target 1442
  ]
  edge [
    source 18
    target 1443
  ]
  edge [
    source 18
    target 1444
  ]
  edge [
    source 18
    target 1445
  ]
  edge [
    source 18
    target 1446
  ]
  edge [
    source 18
    target 1447
  ]
  edge [
    source 18
    target 1448
  ]
  edge [
    source 18
    target 1449
  ]
  edge [
    source 18
    target 1450
  ]
  edge [
    source 18
    target 1451
  ]
  edge [
    source 18
    target 1452
  ]
  edge [
    source 18
    target 1453
  ]
  edge [
    source 18
    target 1454
  ]
  edge [
    source 18
    target 1455
  ]
  edge [
    source 18
    target 1456
  ]
  edge [
    source 18
    target 1457
  ]
  edge [
    source 18
    target 1458
  ]
  edge [
    source 18
    target 1459
  ]
  edge [
    source 18
    target 1460
  ]
  edge [
    source 18
    target 1461
  ]
  edge [
    source 18
    target 1462
  ]
  edge [
    source 18
    target 1463
  ]
  edge [
    source 18
    target 1464
  ]
  edge [
    source 18
    target 1465
  ]
  edge [
    source 18
    target 1466
  ]
  edge [
    source 18
    target 1467
  ]
  edge [
    source 18
    target 1468
  ]
  edge [
    source 18
    target 1469
  ]
  edge [
    source 18
    target 1470
  ]
  edge [
    source 18
    target 1471
  ]
  edge [
    source 18
    target 1472
  ]
  edge [
    source 18
    target 1473
  ]
  edge [
    source 18
    target 1474
  ]
  edge [
    source 18
    target 1475
  ]
  edge [
    source 18
    target 1476
  ]
  edge [
    source 18
    target 1477
  ]
  edge [
    source 18
    target 1478
  ]
  edge [
    source 18
    target 1479
  ]
  edge [
    source 18
    target 1480
  ]
  edge [
    source 18
    target 1481
  ]
  edge [
    source 18
    target 1482
  ]
  edge [
    source 18
    target 1483
  ]
  edge [
    source 18
    target 1484
  ]
  edge [
    source 18
    target 1485
  ]
  edge [
    source 18
    target 1486
  ]
  edge [
    source 18
    target 1487
  ]
  edge [
    source 18
    target 1488
  ]
  edge [
    source 18
    target 1489
  ]
  edge [
    source 18
    target 1490
  ]
  edge [
    source 18
    target 1491
  ]
  edge [
    source 18
    target 1492
  ]
  edge [
    source 18
    target 1493
  ]
  edge [
    source 18
    target 1494
  ]
  edge [
    source 18
    target 1495
  ]
  edge [
    source 18
    target 1496
  ]
  edge [
    source 18
    target 1497
  ]
  edge [
    source 18
    target 1498
  ]
  edge [
    source 18
    target 1499
  ]
  edge [
    source 18
    target 1500
  ]
  edge [
    source 18
    target 1501
  ]
  edge [
    source 18
    target 1502
  ]
  edge [
    source 18
    target 1503
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1504
  ]
  edge [
    source 21
    target 1505
  ]
  edge [
    source 21
    target 76
  ]
  edge [
    source 21
    target 1506
  ]
  edge [
    source 21
    target 1507
  ]
  edge [
    source 21
    target 1508
  ]
  edge [
    source 21
    target 1509
  ]
  edge [
    source 21
    target 1510
  ]
  edge [
    source 21
    target 1511
  ]
  edge [
    source 21
    target 1512
  ]
  edge [
    source 21
    target 1513
  ]
  edge [
    source 21
    target 1514
  ]
  edge [
    source 21
    target 1515
  ]
  edge [
    source 21
    target 1516
  ]
  edge [
    source 21
    target 90
  ]
  edge [
    source 21
    target 91
  ]
  edge [
    source 21
    target 92
  ]
  edge [
    source 21
    target 93
  ]
  edge [
    source 21
    target 94
  ]
  edge [
    source 21
    target 95
  ]
  edge [
    source 21
    target 96
  ]
  edge [
    source 21
    target 97
  ]
  edge [
    source 21
    target 98
  ]
  edge [
    source 21
    target 99
  ]
  edge [
    source 21
    target 100
  ]
  edge [
    source 21
    target 101
  ]
  edge [
    source 21
    target 102
  ]
  edge [
    source 21
    target 103
  ]
  edge [
    source 21
    target 104
  ]
  edge [
    source 21
    target 105
  ]
  edge [
    source 21
    target 106
  ]
  edge [
    source 21
    target 107
  ]
  edge [
    source 21
    target 108
  ]
  edge [
    source 21
    target 88
  ]
  edge [
    source 21
    target 109
  ]
  edge [
    source 21
    target 85
  ]
  edge [
    source 21
    target 1517
  ]
  edge [
    source 21
    target 1518
  ]
  edge [
    source 21
    target 691
  ]
  edge [
    source 21
    target 1519
  ]
  edge [
    source 21
    target 1520
  ]
  edge [
    source 21
    target 79
  ]
  edge [
    source 21
    target 692
  ]
  edge [
    source 21
    target 1521
  ]
  edge [
    source 21
    target 657
  ]
  edge [
    source 21
    target 1522
  ]
  edge [
    source 21
    target 1523
  ]
  edge [
    source 21
    target 1524
  ]
  edge [
    source 21
    target 1525
  ]
  edge [
    source 21
    target 1526
  ]
  edge [
    source 21
    target 1527
  ]
  edge [
    source 21
    target 1528
  ]
  edge [
    source 21
    target 1529
  ]
  edge [
    source 21
    target 1530
  ]
  edge [
    source 21
    target 1531
  ]
  edge [
    source 21
    target 1532
  ]
  edge [
    source 21
    target 281
  ]
  edge [
    source 21
    target 738
  ]
  edge [
    source 21
    target 1533
  ]
  edge [
    source 21
    target 1534
  ]
  edge [
    source 21
    target 286
  ]
  edge [
    source 21
    target 1535
  ]
  edge [
    source 21
    target 1536
  ]
  edge [
    source 21
    target 1537
  ]
  edge [
    source 21
    target 1538
  ]
  edge [
    source 21
    target 1539
  ]
  edge [
    source 21
    target 1540
  ]
  edge [
    source 21
    target 1541
  ]
  edge [
    source 21
    target 1542
  ]
  edge [
    source 21
    target 1543
  ]
  edge [
    source 21
    target 1544
  ]
  edge [
    source 21
    target 1545
  ]
  edge [
    source 21
    target 1546
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1547
  ]
  edge [
    source 26
    target 1548
  ]
  edge [
    source 26
    target 1549
  ]
  edge [
    source 26
    target 1550
  ]
  edge [
    source 26
    target 1551
  ]
  edge [
    source 26
    target 1552
  ]
  edge [
    source 26
    target 1553
  ]
  edge [
    source 26
    target 1554
  ]
  edge [
    source 26
    target 1555
  ]
  edge [
    source 26
    target 1556
  ]
  edge [
    source 26
    target 1557
  ]
  edge [
    source 26
    target 1558
  ]
  edge [
    source 26
    target 93
  ]
  edge [
    source 26
    target 1559
  ]
  edge [
    source 26
    target 1560
  ]
  edge [
    source 26
    target 1561
  ]
  edge [
    source 26
    target 1562
  ]
  edge [
    source 26
    target 1563
  ]
  edge [
    source 26
    target 1564
  ]
  edge [
    source 26
    target 1565
  ]
  edge [
    source 26
    target 670
  ]
  edge [
    source 26
    target 1566
  ]
  edge [
    source 26
    target 1567
  ]
  edge [
    source 26
    target 1568
  ]
  edge [
    source 26
    target 1569
  ]
  edge [
    source 26
    target 1570
  ]
  edge [
    source 26
    target 1571
  ]
  edge [
    source 26
    target 1572
  ]
  edge [
    source 26
    target 1573
  ]
  edge [
    source 26
    target 1574
  ]
  edge [
    source 26
    target 1575
  ]
  edge [
    source 26
    target 1576
  ]
  edge [
    source 26
    target 1577
  ]
  edge [
    source 26
    target 1578
  ]
  edge [
    source 26
    target 1579
  ]
  edge [
    source 26
    target 1580
  ]
  edge [
    source 26
    target 1581
  ]
  edge [
    source 26
    target 1582
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1583
  ]
  edge [
    source 27
    target 930
  ]
  edge [
    source 27
    target 137
  ]
  edge [
    source 27
    target 1584
  ]
  edge [
    source 27
    target 1585
  ]
  edge [
    source 27
    target 1586
  ]
  edge [
    source 27
    target 1587
  ]
  edge [
    source 27
    target 273
  ]
  edge [
    source 27
    target 1588
  ]
  edge [
    source 27
    target 1589
  ]
  edge [
    source 27
    target 1590
  ]
  edge [
    source 27
    target 72
  ]
  edge [
    source 27
    target 1591
  ]
  edge [
    source 27
    target 1289
  ]
  edge [
    source 27
    target 1592
  ]
  edge [
    source 27
    target 986
  ]
  edge [
    source 27
    target 1593
  ]
  edge [
    source 27
    target 1594
  ]
  edge [
    source 27
    target 1595
  ]
  edge [
    source 27
    target 1596
  ]
  edge [
    source 27
    target 1597
  ]
  edge [
    source 27
    target 1598
  ]
  edge [
    source 27
    target 1599
  ]
  edge [
    source 27
    target 1600
  ]
  edge [
    source 27
    target 1601
  ]
  edge [
    source 27
    target 1602
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1603
  ]
  edge [
    source 28
    target 1604
  ]
  edge [
    source 28
    target 1605
  ]
  edge [
    source 28
    target 1606
  ]
  edge [
    source 28
    target 1607
  ]
  edge [
    source 28
    target 1608
  ]
  edge [
    source 28
    target 1609
  ]
  edge [
    source 28
    target 1650
  ]
  edge [
    source 28
    target 1651
  ]
  edge [
    source 28
    target 1652
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 1610
  ]
  edge [
    source 29
    target 1611
  ]
  edge [
    source 29
    target 1612
  ]
  edge [
    source 29
    target 1613
  ]
  edge [
    source 29
    target 1614
  ]
  edge [
    source 29
    target 1615
  ]
  edge [
    source 29
    target 1616
  ]
  edge [
    source 29
    target 1617
  ]
  edge [
    source 29
    target 1618
  ]
  edge [
    source 29
    target 1619
  ]
  edge [
    source 29
    target 1620
  ]
  edge [
    source 29
    target 1621
  ]
  edge [
    source 29
    target 1622
  ]
  edge [
    source 29
    target 1623
  ]
  edge [
    source 29
    target 722
  ]
  edge [
    source 29
    target 1624
  ]
  edge [
    source 29
    target 1625
  ]
  edge [
    source 29
    target 1626
  ]
  edge [
    source 29
    target 1627
  ]
  edge [
    source 29
    target 286
  ]
  edge [
    source 29
    target 1525
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1628
  ]
  edge [
    source 30
    target 1629
  ]
  edge [
    source 31
    target 1032
  ]
  edge [
    source 31
    target 1630
  ]
  edge [
    source 31
    target 1033
  ]
  edge [
    source 31
    target 1034
  ]
  edge [
    source 31
    target 1036
  ]
  edge [
    source 31
    target 1037
  ]
  edge [
    source 31
    target 1631
  ]
  edge [
    source 31
    target 1334
  ]
  edge [
    source 31
    target 1290
  ]
  edge [
    source 31
    target 1040
  ]
  edge [
    source 31
    target 1476
  ]
  edge [
    source 31
    target 1632
  ]
  edge [
    source 31
    target 1043
  ]
  edge [
    source 31
    target 1046
  ]
  edge [
    source 31
    target 397
  ]
  edge [
    source 31
    target 1633
  ]
  edge [
    source 31
    target 826
  ]
  edge [
    source 31
    target 1634
  ]
  edge [
    source 31
    target 1052
  ]
  edge [
    source 31
    target 1495
  ]
  edge [
    source 31
    target 1053
  ]
  edge [
    source 31
    target 1635
  ]
  edge [
    source 31
    target 1055
  ]
  edge [
    source 31
    target 1056
  ]
  edge [
    source 31
    target 1057
  ]
  edge [
    source 31
    target 1132
  ]
  edge [
    source 31
    target 1065
  ]
  edge [
    source 31
    target 1636
  ]
  edge [
    source 31
    target 1637
  ]
  edge [
    source 31
    target 1638
  ]
  edge [
    source 31
    target 404
  ]
  edge [
    source 31
    target 403
  ]
  edge [
    source 31
    target 405
  ]
  edge [
    source 31
    target 406
  ]
  edge [
    source 31
    target 408
  ]
  edge [
    source 31
    target 407
  ]
  edge [
    source 31
    target 409
  ]
  edge [
    source 31
    target 410
  ]
  edge [
    source 31
    target 411
  ]
  edge [
    source 31
    target 412
  ]
  edge [
    source 31
    target 413
  ]
  edge [
    source 31
    target 415
  ]
  edge [
    source 31
    target 414
  ]
  edge [
    source 31
    target 416
  ]
  edge [
    source 31
    target 417
  ]
  edge [
    source 31
    target 418
  ]
  edge [
    source 31
    target 220
  ]
  edge [
    source 31
    target 419
  ]
  edge [
    source 31
    target 420
  ]
  edge [
    source 31
    target 421
  ]
  edge [
    source 31
    target 423
  ]
  edge [
    source 31
    target 422
  ]
  edge [
    source 31
    target 424
  ]
  edge [
    source 31
    target 425
  ]
  edge [
    source 31
    target 426
  ]
  edge [
    source 31
    target 427
  ]
  edge [
    source 31
    target 431
  ]
  edge [
    source 31
    target 428
  ]
  edge [
    source 31
    target 430
  ]
  edge [
    source 31
    target 429
  ]
  edge [
    source 31
    target 432
  ]
  edge [
    source 31
    target 433
  ]
  edge [
    source 31
    target 434
  ]
  edge [
    source 31
    target 435
  ]
  edge [
    source 31
    target 436
  ]
  edge [
    source 31
    target 437
  ]
  edge [
    source 31
    target 438
  ]
  edge [
    source 31
    target 439
  ]
  edge [
    source 31
    target 440
  ]
  edge [
    source 31
    target 441
  ]
  edge [
    source 31
    target 442
  ]
  edge [
    source 31
    target 443
  ]
  edge [
    source 31
    target 444
  ]
  edge [
    source 31
    target 445
  ]
  edge [
    source 31
    target 446
  ]
  edge [
    source 31
    target 447
  ]
  edge [
    source 31
    target 448
  ]
  edge [
    source 31
    target 449
  ]
  edge [
    source 31
    target 450
  ]
  edge [
    source 31
    target 451
  ]
  edge [
    source 31
    target 452
  ]
  edge [
    source 31
    target 454
  ]
  edge [
    source 31
    target 453
  ]
  edge [
    source 31
    target 455
  ]
  edge [
    source 31
    target 456
  ]
  edge [
    source 31
    target 457
  ]
  edge [
    source 31
    target 458
  ]
  edge [
    source 31
    target 459
  ]
  edge [
    source 31
    target 460
  ]
  edge [
    source 31
    target 461
  ]
  edge [
    source 31
    target 462
  ]
  edge [
    source 31
    target 463
  ]
  edge [
    source 31
    target 464
  ]
  edge [
    source 31
    target 465
  ]
  edge [
    source 31
    target 466
  ]
  edge [
    source 31
    target 467
  ]
  edge [
    source 31
    target 468
  ]
  edge [
    source 31
    target 469
  ]
  edge [
    source 31
    target 470
  ]
  edge [
    source 31
    target 471
  ]
  edge [
    source 31
    target 472
  ]
  edge [
    source 31
    target 473
  ]
  edge [
    source 31
    target 474
  ]
  edge [
    source 31
    target 476
  ]
  edge [
    source 31
    target 475
  ]
  edge [
    source 31
    target 477
  ]
  edge [
    source 31
    target 478
  ]
  edge [
    source 31
    target 479
  ]
  edge [
    source 31
    target 480
  ]
  edge [
    source 31
    target 481
  ]
  edge [
    source 31
    target 482
  ]
  edge [
    source 31
    target 483
  ]
  edge [
    source 31
    target 484
  ]
  edge [
    source 31
    target 486
  ]
  edge [
    source 31
    target 485
  ]
  edge [
    source 31
    target 488
  ]
  edge [
    source 31
    target 487
  ]
  edge [
    source 31
    target 489
  ]
  edge [
    source 31
    target 490
  ]
  edge [
    source 31
    target 493
  ]
  edge [
    source 31
    target 491
  ]
  edge [
    source 31
    target 492
  ]
  edge [
    source 31
    target 494
  ]
  edge [
    source 31
    target 495
  ]
  edge [
    source 31
    target 497
  ]
  edge [
    source 31
    target 501
  ]
  edge [
    source 31
    target 496
  ]
  edge [
    source 31
    target 503
  ]
  edge [
    source 31
    target 502
  ]
  edge [
    source 31
    target 499
  ]
  edge [
    source 31
    target 500
  ]
  edge [
    source 31
    target 498
  ]
  edge [
    source 31
    target 504
  ]
  edge [
    source 31
    target 506
  ]
  edge [
    source 31
    target 505
  ]
  edge [
    source 31
    target 507
  ]
  edge [
    source 31
    target 508
  ]
  edge [
    source 31
    target 509
  ]
  edge [
    source 31
    target 510
  ]
  edge [
    source 31
    target 511
  ]
  edge [
    source 31
    target 512
  ]
  edge [
    source 31
    target 513
  ]
  edge [
    source 31
    target 515
  ]
  edge [
    source 31
    target 514
  ]
  edge [
    source 31
    target 516
  ]
  edge [
    source 31
    target 517
  ]
  edge [
    source 31
    target 518
  ]
  edge [
    source 31
    target 519
  ]
  edge [
    source 31
    target 520
  ]
  edge [
    source 31
    target 521
  ]
  edge [
    source 31
    target 522
  ]
  edge [
    source 31
    target 523
  ]
  edge [
    source 31
    target 524
  ]
  edge [
    source 31
    target 525
  ]
  edge [
    source 31
    target 526
  ]
  edge [
    source 31
    target 527
  ]
  edge [
    source 31
    target 528
  ]
  edge [
    source 31
    target 529
  ]
  edge [
    source 31
    target 530
  ]
  edge [
    source 31
    target 534
  ]
  edge [
    source 31
    target 532
  ]
  edge [
    source 31
    target 533
  ]
  edge [
    source 31
    target 531
  ]
  edge [
    source 31
    target 535
  ]
  edge [
    source 31
    target 541
  ]
  edge [
    source 31
    target 539
  ]
  edge [
    source 31
    target 537
  ]
  edge [
    source 31
    target 536
  ]
  edge [
    source 31
    target 542
  ]
  edge [
    source 31
    target 540
  ]
  edge [
    source 31
    target 538
  ]
  edge [
    source 31
    target 544
  ]
  edge [
    source 31
    target 543
  ]
  edge [
    source 31
    target 545
  ]
  edge [
    source 31
    target 546
  ]
  edge [
    source 31
    target 548
  ]
  edge [
    source 31
    target 547
  ]
  edge [
    source 31
    target 549
  ]
  edge [
    source 31
    target 550
  ]
  edge [
    source 31
    target 551
  ]
  edge [
    source 31
    target 552
  ]
  edge [
    source 31
    target 553
  ]
  edge [
    source 31
    target 554
  ]
  edge [
    source 31
    target 555
  ]
  edge [
    source 31
    target 556
  ]
  edge [
    source 31
    target 557
  ]
  edge [
    source 31
    target 558
  ]
  edge [
    source 31
    target 559
  ]
  edge [
    source 31
    target 560
  ]
  edge [
    source 31
    target 561
  ]
  edge [
    source 31
    target 562
  ]
  edge [
    source 31
    target 563
  ]
  edge [
    source 31
    target 564
  ]
  edge [
    source 31
    target 565
  ]
  edge [
    source 31
    target 566
  ]
  edge [
    source 31
    target 567
  ]
  edge [
    source 31
    target 568
  ]
  edge [
    source 31
    target 573
  ]
  edge [
    source 31
    target 571
  ]
  edge [
    source 31
    target 572
  ]
  edge [
    source 31
    target 570
  ]
  edge [
    source 31
    target 569
  ]
  edge [
    source 31
    target 574
  ]
  edge [
    source 31
    target 575
  ]
  edge [
    source 31
    target 576
  ]
  edge [
    source 31
    target 577
  ]
  edge [
    source 31
    target 578
  ]
  edge [
    source 31
    target 579
  ]
  edge [
    source 31
    target 580
  ]
  edge [
    source 31
    target 581
  ]
  edge [
    source 31
    target 583
  ]
  edge [
    source 31
    target 582
  ]
  edge [
    source 31
    target 585
  ]
  edge [
    source 31
    target 584
  ]
  edge [
    source 31
    target 586
  ]
  edge [
    source 31
    target 587
  ]
  edge [
    source 31
    target 588
  ]
  edge [
    source 31
    target 589
  ]
  edge [
    source 31
    target 590
  ]
  edge [
    source 31
    target 591
  ]
  edge [
    source 31
    target 592
  ]
  edge [
    source 31
    target 593
  ]
  edge [
    source 31
    target 594
  ]
  edge [
    source 31
    target 596
  ]
  edge [
    source 31
    target 595
  ]
  edge [
    source 31
    target 597
  ]
  edge [
    source 31
    target 598
  ]
  edge [
    source 31
    target 599
  ]
  edge [
    source 31
    target 600
  ]
  edge [
    source 31
    target 601
  ]
  edge [
    source 31
    target 602
  ]
  edge [
    source 31
    target 603
  ]
  edge [
    source 31
    target 604
  ]
  edge [
    source 31
    target 605
  ]
  edge [
    source 31
    target 606
  ]
  edge [
    source 31
    target 607
  ]
  edge [
    source 31
    target 608
  ]
  edge [
    source 31
    target 609
  ]
  edge [
    source 31
    target 610
  ]
  edge [
    source 31
    target 611
  ]
  edge [
    source 31
    target 612
  ]
  edge [
    source 31
    target 613
  ]
  edge [
    source 31
    target 614
  ]
  edge [
    source 31
    target 615
  ]
  edge [
    source 31
    target 616
  ]
  edge [
    source 31
    target 1012
  ]
  edge [
    source 31
    target 1639
  ]
  edge [
    source 31
    target 862
  ]
  edge [
    source 31
    target 882
  ]
  edge [
    source 31
    target 1640
  ]
  edge [
    source 31
    target 1641
  ]
  edge [
    source 31
    target 1642
  ]
  edge [
    source 31
    target 1002
  ]
  edge [
    source 31
    target 835
  ]
  edge [
    source 219
    target 1643
  ]
  edge [
    source 1644
    target 1645
  ]
  edge [
    source 1644
    target 1646
  ]
  edge [
    source 1644
    target 1647
  ]
  edge [
    source 1645
    target 1646
  ]
  edge [
    source 1645
    target 1647
  ]
  edge [
    source 1646
    target 1647
  ]
  edge [
    source 1648
    target 1649
  ]
  edge [
    source 1650
    target 1651
  ]
  edge [
    source 1650
    target 1652
  ]
  edge [
    source 1651
    target 1652
  ]
  edge [
    source 1653
    target 1654
  ]
  edge [
    source 1653
    target 1655
  ]
  edge [
    source 1654
    target 1655
  ]
  edge [
    source 1656
    target 1657
  ]
  edge [
    source 1656
    target 1658
  ]
  edge [
    source 1656
    target 1659
  ]
  edge [
    source 1657
    target 1658
  ]
  edge [
    source 1657
    target 1659
  ]
  edge [
    source 1658
    target 1659
  ]
  edge [
    source 1660
    target 1661
  ]
  edge [
    source 1662
    target 1663
  ]
  edge [
    source 1662
    target 1664
  ]
  edge [
    source 1662
    target 1665
  ]
  edge [
    source 1662
    target 1666
  ]
  edge [
    source 1663
    target 1664
  ]
  edge [
    source 1663
    target 1665
  ]
  edge [
    source 1663
    target 1666
  ]
  edge [
    source 1664
    target 1665
  ]
  edge [
    source 1664
    target 1666
  ]
  edge [
    source 1665
    target 1666
  ]
  edge [
    source 1667
    target 1668
  ]
]
