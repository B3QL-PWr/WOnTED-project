graph [
  node [
    id 0
    label "obarczy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wina"
    origin "text"
  ]
  node [
    id 2
    label "fiasko"
    origin "text"
  ]
  node [
    id 3
    label "operacja"
    origin "text"
  ]
  node [
    id 4
    label "garden"
    origin "text"
  ]
  node [
    id 5
    label "oskar&#380;y&#263;"
  ]
  node [
    id 6
    label "blame"
  ]
  node [
    id 7
    label "odpowiedzialno&#347;&#263;"
  ]
  node [
    id 8
    label "charge"
  ]
  node [
    id 9
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 10
    label "obowi&#261;zek"
  ]
  node [
    id 11
    label "load"
  ]
  node [
    id 12
    label "obci&#261;&#380;y&#263;"
  ]
  node [
    id 13
    label "disapprove"
  ]
  node [
    id 14
    label "zakomunikowa&#263;"
  ]
  node [
    id 15
    label "obowi&#261;za&#263;"
  ]
  node [
    id 16
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 17
    label "perpetrate"
  ]
  node [
    id 18
    label "zaszkodzi&#263;"
  ]
  node [
    id 19
    label "na&#322;o&#380;y&#263;"
  ]
  node [
    id 20
    label "duty"
  ]
  node [
    id 21
    label "wym&#243;g"
  ]
  node [
    id 22
    label "powinno&#347;&#263;"
  ]
  node [
    id 23
    label "zadanie"
  ]
  node [
    id 24
    label "konsekwencja"
  ]
  node [
    id 25
    label "wstyd"
  ]
  node [
    id 26
    label "liability"
  ]
  node [
    id 27
    label "cecha"
  ]
  node [
    id 28
    label "guilt"
  ]
  node [
    id 29
    label "lutnia"
  ]
  node [
    id 30
    label "odczuwa&#263;"
  ]
  node [
    id 31
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 32
    label "skrupienie_si&#281;"
  ]
  node [
    id 33
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 34
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 35
    label "odczucie"
  ]
  node [
    id 36
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 37
    label "koszula_Dejaniry"
  ]
  node [
    id 38
    label "odczuwanie"
  ]
  node [
    id 39
    label "event"
  ]
  node [
    id 40
    label "rezultat"
  ]
  node [
    id 41
    label "skrupianie_si&#281;"
  ]
  node [
    id 42
    label "odczu&#263;"
  ]
  node [
    id 43
    label "chordofon_szarpany"
  ]
  node [
    id 44
    label "srom"
  ]
  node [
    id 45
    label "emocja"
  ]
  node [
    id 46
    label "dishonor"
  ]
  node [
    id 47
    label "konfuzja"
  ]
  node [
    id 48
    label "shame"
  ]
  node [
    id 49
    label "g&#322;upio"
  ]
  node [
    id 50
    label "visitation"
  ]
  node [
    id 51
    label "wydarzenie"
  ]
  node [
    id 52
    label "przebiec"
  ]
  node [
    id 53
    label "charakter"
  ]
  node [
    id 54
    label "czynno&#347;&#263;"
  ]
  node [
    id 55
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 56
    label "motyw"
  ]
  node [
    id 57
    label "przebiegni&#281;cie"
  ]
  node [
    id 58
    label "fabu&#322;a"
  ]
  node [
    id 59
    label "proces_my&#347;lowy"
  ]
  node [
    id 60
    label "liczenie"
  ]
  node [
    id 61
    label "czyn"
  ]
  node [
    id 62
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 63
    label "supremum"
  ]
  node [
    id 64
    label "laparotomia"
  ]
  node [
    id 65
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 66
    label "jednostka"
  ]
  node [
    id 67
    label "matematyka"
  ]
  node [
    id 68
    label "rzut"
  ]
  node [
    id 69
    label "liczy&#263;"
  ]
  node [
    id 70
    label "strategia"
  ]
  node [
    id 71
    label "torakotomia"
  ]
  node [
    id 72
    label "chirurg"
  ]
  node [
    id 73
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 74
    label "zabieg"
  ]
  node [
    id 75
    label "szew"
  ]
  node [
    id 76
    label "funkcja"
  ]
  node [
    id 77
    label "mathematical_process"
  ]
  node [
    id 78
    label "infimum"
  ]
  node [
    id 79
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 80
    label "plan"
  ]
  node [
    id 81
    label "metoda"
  ]
  node [
    id 82
    label "gra"
  ]
  node [
    id 83
    label "pocz&#261;tki"
  ]
  node [
    id 84
    label "wzorzec_projektowy"
  ]
  node [
    id 85
    label "dziedzina"
  ]
  node [
    id 86
    label "program"
  ]
  node [
    id 87
    label "doktryna"
  ]
  node [
    id 88
    label "wrinkle"
  ]
  node [
    id 89
    label "dokument"
  ]
  node [
    id 90
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 91
    label "rachunek_operatorowy"
  ]
  node [
    id 92
    label "przedmiot"
  ]
  node [
    id 93
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 94
    label "kryptologia"
  ]
  node [
    id 95
    label "logicyzm"
  ]
  node [
    id 96
    label "logika"
  ]
  node [
    id 97
    label "matematyka_czysta"
  ]
  node [
    id 98
    label "forsing"
  ]
  node [
    id 99
    label "modelowanie_matematyczne"
  ]
  node [
    id 100
    label "matma"
  ]
  node [
    id 101
    label "teoria_katastrof"
  ]
  node [
    id 102
    label "kierunek"
  ]
  node [
    id 103
    label "fizyka_matematyczna"
  ]
  node [
    id 104
    label "teoria_graf&#243;w"
  ]
  node [
    id 105
    label "rachunki"
  ]
  node [
    id 106
    label "topologia_algebraiczna"
  ]
  node [
    id 107
    label "matematyka_stosowana"
  ]
  node [
    id 108
    label "leczenie"
  ]
  node [
    id 109
    label "operation"
  ]
  node [
    id 110
    label "act"
  ]
  node [
    id 111
    label "thoracotomy"
  ]
  node [
    id 112
    label "medycyna"
  ]
  node [
    id 113
    label "otwarcie"
  ]
  node [
    id 114
    label "jama_brzuszna"
  ]
  node [
    id 115
    label "laparotomy"
  ]
  node [
    id 116
    label "ryba"
  ]
  node [
    id 117
    label "pokolcowate"
  ]
  node [
    id 118
    label "specjalista"
  ]
  node [
    id 119
    label "spawanie"
  ]
  node [
    id 120
    label "chirurgia"
  ]
  node [
    id 121
    label "nitowanie"
  ]
  node [
    id 122
    label "kokon"
  ]
  node [
    id 123
    label "zszycie"
  ]
  node [
    id 124
    label "z&#322;&#261;czenie"
  ]
  node [
    id 125
    label "blizna"
  ]
  node [
    id 126
    label "suture"
  ]
  node [
    id 127
    label "badanie"
  ]
  node [
    id 128
    label "rachowanie"
  ]
  node [
    id 129
    label "dyskalkulia"
  ]
  node [
    id 130
    label "wynagrodzenie"
  ]
  node [
    id 131
    label "rozliczanie"
  ]
  node [
    id 132
    label "wymienianie"
  ]
  node [
    id 133
    label "oznaczanie"
  ]
  node [
    id 134
    label "wychodzenie"
  ]
  node [
    id 135
    label "naliczenie_si&#281;"
  ]
  node [
    id 136
    label "wyznaczanie"
  ]
  node [
    id 137
    label "dodawanie"
  ]
  node [
    id 138
    label "charakteryzowanie_si&#281;"
  ]
  node [
    id 139
    label "bang"
  ]
  node [
    id 140
    label "spodziewanie_si&#281;"
  ]
  node [
    id 141
    label "rozliczenie"
  ]
  node [
    id 142
    label "kwotowanie"
  ]
  node [
    id 143
    label "mierzenie"
  ]
  node [
    id 144
    label "count"
  ]
  node [
    id 145
    label "wycenianie"
  ]
  node [
    id 146
    label "branie"
  ]
  node [
    id 147
    label "sprowadzanie"
  ]
  node [
    id 148
    label "przeliczanie"
  ]
  node [
    id 149
    label "uwzgl&#281;dnianie"
  ]
  node [
    id 150
    label "odliczanie"
  ]
  node [
    id 151
    label "przeliczenie"
  ]
  node [
    id 152
    label "przyswoi&#263;"
  ]
  node [
    id 153
    label "ludzko&#347;&#263;"
  ]
  node [
    id 154
    label "one"
  ]
  node [
    id 155
    label "poj&#281;cie"
  ]
  node [
    id 156
    label "ewoluowanie"
  ]
  node [
    id 157
    label "skala"
  ]
  node [
    id 158
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 159
    label "przyswajanie"
  ]
  node [
    id 160
    label "wyewoluowanie"
  ]
  node [
    id 161
    label "reakcja"
  ]
  node [
    id 162
    label "przeliczy&#263;"
  ]
  node [
    id 163
    label "wyewoluowa&#263;"
  ]
  node [
    id 164
    label "ewoluowa&#263;"
  ]
  node [
    id 165
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 166
    label "liczba_naturalna"
  ]
  node [
    id 167
    label "czynnik_biotyczny"
  ]
  node [
    id 168
    label "g&#322;owa"
  ]
  node [
    id 169
    label "figura"
  ]
  node [
    id 170
    label "individual"
  ]
  node [
    id 171
    label "portrecista"
  ]
  node [
    id 172
    label "obiekt"
  ]
  node [
    id 173
    label "przyswaja&#263;"
  ]
  node [
    id 174
    label "przyswojenie"
  ]
  node [
    id 175
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 176
    label "profanum"
  ]
  node [
    id 177
    label "mikrokosmos"
  ]
  node [
    id 178
    label "starzenie_si&#281;"
  ]
  node [
    id 179
    label "duch"
  ]
  node [
    id 180
    label "osoba"
  ]
  node [
    id 181
    label "oddzia&#322;ywanie"
  ]
  node [
    id 182
    label "antropochoria"
  ]
  node [
    id 183
    label "homo_sapiens"
  ]
  node [
    id 184
    label "przelicza&#263;"
  ]
  node [
    id 185
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 186
    label "ograniczenie"
  ]
  node [
    id 187
    label "addytywno&#347;&#263;"
  ]
  node [
    id 188
    label "function"
  ]
  node [
    id 189
    label "zastosowanie"
  ]
  node [
    id 190
    label "funkcjonowanie"
  ]
  node [
    id 191
    label "praca"
  ]
  node [
    id 192
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 193
    label "powierzanie"
  ]
  node [
    id 194
    label "cel"
  ]
  node [
    id 195
    label "przeciwdziedzina"
  ]
  node [
    id 196
    label "awansowa&#263;"
  ]
  node [
    id 197
    label "stawia&#263;"
  ]
  node [
    id 198
    label "wakowa&#263;"
  ]
  node [
    id 199
    label "znaczenie"
  ]
  node [
    id 200
    label "postawi&#263;"
  ]
  node [
    id 201
    label "awansowanie"
  ]
  node [
    id 202
    label "armia"
  ]
  node [
    id 203
    label "nawr&#243;t_choroby"
  ]
  node [
    id 204
    label "potomstwo"
  ]
  node [
    id 205
    label "odwzorowanie"
  ]
  node [
    id 206
    label "rysunek"
  ]
  node [
    id 207
    label "scene"
  ]
  node [
    id 208
    label "throw"
  ]
  node [
    id 209
    label "float"
  ]
  node [
    id 210
    label "punkt"
  ]
  node [
    id 211
    label "projection"
  ]
  node [
    id 212
    label "injection"
  ]
  node [
    id 213
    label "blow"
  ]
  node [
    id 214
    label "pomys&#322;"
  ]
  node [
    id 215
    label "ruch"
  ]
  node [
    id 216
    label "k&#322;ad"
  ]
  node [
    id 217
    label "mold"
  ]
  node [
    id 218
    label "report"
  ]
  node [
    id 219
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 220
    label "osi&#261;ga&#263;"
  ]
  node [
    id 221
    label "wymienia&#263;"
  ]
  node [
    id 222
    label "posiada&#263;"
  ]
  node [
    id 223
    label "wycenia&#263;"
  ]
  node [
    id 224
    label "bra&#263;"
  ]
  node [
    id 225
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 226
    label "mierzy&#263;"
  ]
  node [
    id 227
    label "rachowa&#263;"
  ]
  node [
    id 228
    label "tell"
  ]
  node [
    id 229
    label "odlicza&#263;"
  ]
  node [
    id 230
    label "dodawa&#263;"
  ]
  node [
    id 231
    label "wyznacza&#263;"
  ]
  node [
    id 232
    label "admit"
  ]
  node [
    id 233
    label "policza&#263;"
  ]
  node [
    id 234
    label "okre&#347;la&#263;"
  ]
  node [
    id 235
    label "market"
  ]
  node [
    id 236
    label "Garden"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 235
    target 236
  ]
]
