graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "sejm"
    origin "text"
  ]
  node [
    id 4
    label "rocznica"
    origin "text"
  ]
  node [
    id 5
    label "urodziny"
    origin "text"
  ]
  node [
    id 6
    label "pragn&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przedstawi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "posta&#263;"
    origin "text"
  ]
  node [
    id 9
    label "piotr"
    origin "text"
  ]
  node [
    id 10
    label "wo&#378;niak"
    origin "text"
  ]
  node [
    id 11
    label "&#380;o&#322;nierz"
    origin "text"
  ]
  node [
    id 12
    label "armia"
    origin "text"
  ]
  node [
    id 13
    label "krajowy"
    origin "text"
  ]
  node [
    id 14
    label "ostatni"
    origin "text"
  ]
  node [
    id 15
    label "komendant"
    origin "text"
  ]
  node [
    id 16
    label "rzeszowskie"
    origin "text"
  ]
  node [
    id 17
    label "okr&#261;g"
    origin "text"
  ]
  node [
    id 18
    label "narodowy"
    origin "text"
  ]
  node [
    id 19
    label "zjednoczenie"
    origin "text"
  ]
  node [
    id 20
    label "wojskowy"
    origin "text"
  ]
  node [
    id 21
    label "aresztowany"
    origin "text"
  ]
  node [
    id 22
    label "przez"
    origin "text"
  ]
  node [
    id 23
    label "urz&#261;d"
    origin "text"
  ]
  node [
    id 24
    label "bezpiecze&#324;stwo"
    origin "text"
  ]
  node [
    id 25
    label "rocznik"
    origin "text"
  ]
  node [
    id 26
    label "skazany"
    origin "text"
  ]
  node [
    id 27
    label "kara"
    origin "text"
  ]
  node [
    id 28
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 29
    label "belfer"
  ]
  node [
    id 30
    label "murza"
  ]
  node [
    id 31
    label "cz&#322;owiek"
  ]
  node [
    id 32
    label "ojciec"
  ]
  node [
    id 33
    label "samiec"
  ]
  node [
    id 34
    label "androlog"
  ]
  node [
    id 35
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 36
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 37
    label "efendi"
  ]
  node [
    id 38
    label "opiekun"
  ]
  node [
    id 39
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 40
    label "pa&#324;stwo"
  ]
  node [
    id 41
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 42
    label "bratek"
  ]
  node [
    id 43
    label "Mieszko_I"
  ]
  node [
    id 44
    label "Midas"
  ]
  node [
    id 45
    label "m&#261;&#380;"
  ]
  node [
    id 46
    label "bogaty"
  ]
  node [
    id 47
    label "popularyzator"
  ]
  node [
    id 48
    label "pracodawca"
  ]
  node [
    id 49
    label "kszta&#322;ciciel"
  ]
  node [
    id 50
    label "preceptor"
  ]
  node [
    id 51
    label "nabab"
  ]
  node [
    id 52
    label "pupil"
  ]
  node [
    id 53
    label "andropauza"
  ]
  node [
    id 54
    label "zwrot"
  ]
  node [
    id 55
    label "przyw&#243;dca"
  ]
  node [
    id 56
    label "doros&#322;y"
  ]
  node [
    id 57
    label "pedagog"
  ]
  node [
    id 58
    label "rz&#261;dzenie"
  ]
  node [
    id 59
    label "jegomo&#347;&#263;"
  ]
  node [
    id 60
    label "szkolnik"
  ]
  node [
    id 61
    label "ch&#322;opina"
  ]
  node [
    id 62
    label "w&#322;odarz"
  ]
  node [
    id 63
    label "profesor"
  ]
  node [
    id 64
    label "gra_w_karty"
  ]
  node [
    id 65
    label "w&#322;adza"
  ]
  node [
    id 66
    label "Fidel_Castro"
  ]
  node [
    id 67
    label "Anders"
  ]
  node [
    id 68
    label "Ko&#347;ciuszko"
  ]
  node [
    id 69
    label "Tito"
  ]
  node [
    id 70
    label "Miko&#322;ajczyk"
  ]
  node [
    id 71
    label "lider"
  ]
  node [
    id 72
    label "Mao"
  ]
  node [
    id 73
    label "Sabataj_Cwi"
  ]
  node [
    id 74
    label "p&#322;atnik"
  ]
  node [
    id 75
    label "zwierzchnik"
  ]
  node [
    id 76
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 77
    label "nadzorca"
  ]
  node [
    id 78
    label "funkcjonariusz"
  ]
  node [
    id 79
    label "podmiot"
  ]
  node [
    id 80
    label "wykupienie"
  ]
  node [
    id 81
    label "bycie_w_posiadaniu"
  ]
  node [
    id 82
    label "wykupywanie"
  ]
  node [
    id 83
    label "rozszerzyciel"
  ]
  node [
    id 84
    label "ludzko&#347;&#263;"
  ]
  node [
    id 85
    label "asymilowanie"
  ]
  node [
    id 86
    label "wapniak"
  ]
  node [
    id 87
    label "asymilowa&#263;"
  ]
  node [
    id 88
    label "os&#322;abia&#263;"
  ]
  node [
    id 89
    label "hominid"
  ]
  node [
    id 90
    label "podw&#322;adny"
  ]
  node [
    id 91
    label "os&#322;abianie"
  ]
  node [
    id 92
    label "g&#322;owa"
  ]
  node [
    id 93
    label "figura"
  ]
  node [
    id 94
    label "portrecista"
  ]
  node [
    id 95
    label "dwun&#243;g"
  ]
  node [
    id 96
    label "profanum"
  ]
  node [
    id 97
    label "mikrokosmos"
  ]
  node [
    id 98
    label "nasada"
  ]
  node [
    id 99
    label "duch"
  ]
  node [
    id 100
    label "antropochoria"
  ]
  node [
    id 101
    label "osoba"
  ]
  node [
    id 102
    label "wz&#243;r"
  ]
  node [
    id 103
    label "senior"
  ]
  node [
    id 104
    label "oddzia&#322;ywanie"
  ]
  node [
    id 105
    label "Adam"
  ]
  node [
    id 106
    label "homo_sapiens"
  ]
  node [
    id 107
    label "polifag"
  ]
  node [
    id 108
    label "wydoro&#347;lenie"
  ]
  node [
    id 109
    label "du&#380;y"
  ]
  node [
    id 110
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 111
    label "doro&#347;lenie"
  ]
  node [
    id 112
    label "&#378;ra&#322;y"
  ]
  node [
    id 113
    label "doro&#347;le"
  ]
  node [
    id 114
    label "dojrzale"
  ]
  node [
    id 115
    label "dojrza&#322;y"
  ]
  node [
    id 116
    label "m&#261;dry"
  ]
  node [
    id 117
    label "doletni"
  ]
  node [
    id 118
    label "punkt"
  ]
  node [
    id 119
    label "turn"
  ]
  node [
    id 120
    label "turning"
  ]
  node [
    id 121
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 122
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 123
    label "skr&#281;t"
  ]
  node [
    id 124
    label "obr&#243;t"
  ]
  node [
    id 125
    label "fraza_czasownikowa"
  ]
  node [
    id 126
    label "jednostka_leksykalna"
  ]
  node [
    id 127
    label "zmiana"
  ]
  node [
    id 128
    label "wyra&#380;enie"
  ]
  node [
    id 129
    label "starosta"
  ]
  node [
    id 130
    label "zarz&#261;dca"
  ]
  node [
    id 131
    label "w&#322;adca"
  ]
  node [
    id 132
    label "nauczyciel"
  ]
  node [
    id 133
    label "stopie&#324;_naukowy"
  ]
  node [
    id 134
    label "nauczyciel_akademicki"
  ]
  node [
    id 135
    label "tytu&#322;"
  ]
  node [
    id 136
    label "profesura"
  ]
  node [
    id 137
    label "konsulent"
  ]
  node [
    id 138
    label "wirtuoz"
  ]
  node [
    id 139
    label "autor"
  ]
  node [
    id 140
    label "wyprawka"
  ]
  node [
    id 141
    label "mundurek"
  ]
  node [
    id 142
    label "szko&#322;a"
  ]
  node [
    id 143
    label "tarcza"
  ]
  node [
    id 144
    label "elew"
  ]
  node [
    id 145
    label "absolwent"
  ]
  node [
    id 146
    label "klasa"
  ]
  node [
    id 147
    label "ekspert"
  ]
  node [
    id 148
    label "ochotnik"
  ]
  node [
    id 149
    label "pomocnik"
  ]
  node [
    id 150
    label "student"
  ]
  node [
    id 151
    label "nauczyciel_muzyki"
  ]
  node [
    id 152
    label "zakonnik"
  ]
  node [
    id 153
    label "urz&#281;dnik"
  ]
  node [
    id 154
    label "bogacz"
  ]
  node [
    id 155
    label "dostojnik"
  ]
  node [
    id 156
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 157
    label "kuwada"
  ]
  node [
    id 158
    label "tworzyciel"
  ]
  node [
    id 159
    label "rodzice"
  ]
  node [
    id 160
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 161
    label "&#347;w"
  ]
  node [
    id 162
    label "pomys&#322;odawca"
  ]
  node [
    id 163
    label "rodzic"
  ]
  node [
    id 164
    label "wykonawca"
  ]
  node [
    id 165
    label "ojczym"
  ]
  node [
    id 166
    label "przodek"
  ]
  node [
    id 167
    label "papa"
  ]
  node [
    id 168
    label "stary"
  ]
  node [
    id 169
    label "kochanek"
  ]
  node [
    id 170
    label "fio&#322;ek"
  ]
  node [
    id 171
    label "facet"
  ]
  node [
    id 172
    label "brat"
  ]
  node [
    id 173
    label "zwierz&#281;"
  ]
  node [
    id 174
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 175
    label "ma&#322;&#380;onek"
  ]
  node [
    id 176
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 177
    label "m&#243;j"
  ]
  node [
    id 178
    label "ch&#322;op"
  ]
  node [
    id 179
    label "pan_m&#322;ody"
  ]
  node [
    id 180
    label "&#347;lubny"
  ]
  node [
    id 181
    label "pan_domu"
  ]
  node [
    id 182
    label "pan_i_w&#322;adca"
  ]
  node [
    id 183
    label "mo&#347;&#263;"
  ]
  node [
    id 184
    label "Frygia"
  ]
  node [
    id 185
    label "sprawowanie"
  ]
  node [
    id 186
    label "dominion"
  ]
  node [
    id 187
    label "dominowanie"
  ]
  node [
    id 188
    label "reign"
  ]
  node [
    id 189
    label "rule"
  ]
  node [
    id 190
    label "zwierz&#281;_domowe"
  ]
  node [
    id 191
    label "J&#281;drzejewicz"
  ]
  node [
    id 192
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 193
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 194
    label "John_Dewey"
  ]
  node [
    id 195
    label "specjalista"
  ]
  node [
    id 196
    label "&#380;ycie"
  ]
  node [
    id 197
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 198
    label "Turek"
  ]
  node [
    id 199
    label "effendi"
  ]
  node [
    id 200
    label "obfituj&#261;cy"
  ]
  node [
    id 201
    label "r&#243;&#380;norodny"
  ]
  node [
    id 202
    label "spania&#322;y"
  ]
  node [
    id 203
    label "obficie"
  ]
  node [
    id 204
    label "sytuowany"
  ]
  node [
    id 205
    label "och&#281;do&#380;ny"
  ]
  node [
    id 206
    label "forsiasty"
  ]
  node [
    id 207
    label "zapa&#347;ny"
  ]
  node [
    id 208
    label "bogato"
  ]
  node [
    id 209
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 210
    label "Katar"
  ]
  node [
    id 211
    label "Libia"
  ]
  node [
    id 212
    label "Gwatemala"
  ]
  node [
    id 213
    label "Ekwador"
  ]
  node [
    id 214
    label "Afganistan"
  ]
  node [
    id 215
    label "Tad&#380;ykistan"
  ]
  node [
    id 216
    label "Bhutan"
  ]
  node [
    id 217
    label "Argentyna"
  ]
  node [
    id 218
    label "D&#380;ibuti"
  ]
  node [
    id 219
    label "Wenezuela"
  ]
  node [
    id 220
    label "Gabon"
  ]
  node [
    id 221
    label "Ukraina"
  ]
  node [
    id 222
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 223
    label "Rwanda"
  ]
  node [
    id 224
    label "Liechtenstein"
  ]
  node [
    id 225
    label "organizacja"
  ]
  node [
    id 226
    label "Sri_Lanka"
  ]
  node [
    id 227
    label "Madagaskar"
  ]
  node [
    id 228
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 229
    label "Kongo"
  ]
  node [
    id 230
    label "Tonga"
  ]
  node [
    id 231
    label "Bangladesz"
  ]
  node [
    id 232
    label "Kanada"
  ]
  node [
    id 233
    label "Wehrlen"
  ]
  node [
    id 234
    label "Algieria"
  ]
  node [
    id 235
    label "Uganda"
  ]
  node [
    id 236
    label "Surinam"
  ]
  node [
    id 237
    label "Sahara_Zachodnia"
  ]
  node [
    id 238
    label "Chile"
  ]
  node [
    id 239
    label "W&#281;gry"
  ]
  node [
    id 240
    label "Birma"
  ]
  node [
    id 241
    label "Kazachstan"
  ]
  node [
    id 242
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 243
    label "Armenia"
  ]
  node [
    id 244
    label "Tuwalu"
  ]
  node [
    id 245
    label "Timor_Wschodni"
  ]
  node [
    id 246
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 247
    label "Izrael"
  ]
  node [
    id 248
    label "Estonia"
  ]
  node [
    id 249
    label "Komory"
  ]
  node [
    id 250
    label "Kamerun"
  ]
  node [
    id 251
    label "Haiti"
  ]
  node [
    id 252
    label "Belize"
  ]
  node [
    id 253
    label "Sierra_Leone"
  ]
  node [
    id 254
    label "Luksemburg"
  ]
  node [
    id 255
    label "USA"
  ]
  node [
    id 256
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 257
    label "Barbados"
  ]
  node [
    id 258
    label "San_Marino"
  ]
  node [
    id 259
    label "Bu&#322;garia"
  ]
  node [
    id 260
    label "Indonezja"
  ]
  node [
    id 261
    label "Wietnam"
  ]
  node [
    id 262
    label "Malawi"
  ]
  node [
    id 263
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 264
    label "Francja"
  ]
  node [
    id 265
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 266
    label "partia"
  ]
  node [
    id 267
    label "Zambia"
  ]
  node [
    id 268
    label "Angola"
  ]
  node [
    id 269
    label "Grenada"
  ]
  node [
    id 270
    label "Nepal"
  ]
  node [
    id 271
    label "Panama"
  ]
  node [
    id 272
    label "Rumunia"
  ]
  node [
    id 273
    label "Czarnog&#243;ra"
  ]
  node [
    id 274
    label "Malediwy"
  ]
  node [
    id 275
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 276
    label "S&#322;owacja"
  ]
  node [
    id 277
    label "para"
  ]
  node [
    id 278
    label "Egipt"
  ]
  node [
    id 279
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 280
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 281
    label "Mozambik"
  ]
  node [
    id 282
    label "Kolumbia"
  ]
  node [
    id 283
    label "Laos"
  ]
  node [
    id 284
    label "Burundi"
  ]
  node [
    id 285
    label "Suazi"
  ]
  node [
    id 286
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 287
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 288
    label "Czechy"
  ]
  node [
    id 289
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 290
    label "Wyspy_Marshalla"
  ]
  node [
    id 291
    label "Dominika"
  ]
  node [
    id 292
    label "Trynidad_i_Tobago"
  ]
  node [
    id 293
    label "Syria"
  ]
  node [
    id 294
    label "Palau"
  ]
  node [
    id 295
    label "Gwinea_Bissau"
  ]
  node [
    id 296
    label "Liberia"
  ]
  node [
    id 297
    label "Jamajka"
  ]
  node [
    id 298
    label "Zimbabwe"
  ]
  node [
    id 299
    label "Polska"
  ]
  node [
    id 300
    label "Dominikana"
  ]
  node [
    id 301
    label "Senegal"
  ]
  node [
    id 302
    label "Togo"
  ]
  node [
    id 303
    label "Gujana"
  ]
  node [
    id 304
    label "Gruzja"
  ]
  node [
    id 305
    label "Albania"
  ]
  node [
    id 306
    label "Zair"
  ]
  node [
    id 307
    label "Meksyk"
  ]
  node [
    id 308
    label "Macedonia"
  ]
  node [
    id 309
    label "Chorwacja"
  ]
  node [
    id 310
    label "Kambod&#380;a"
  ]
  node [
    id 311
    label "Monako"
  ]
  node [
    id 312
    label "Mauritius"
  ]
  node [
    id 313
    label "Gwinea"
  ]
  node [
    id 314
    label "Mali"
  ]
  node [
    id 315
    label "Nigeria"
  ]
  node [
    id 316
    label "Kostaryka"
  ]
  node [
    id 317
    label "Hanower"
  ]
  node [
    id 318
    label "Paragwaj"
  ]
  node [
    id 319
    label "W&#322;ochy"
  ]
  node [
    id 320
    label "Seszele"
  ]
  node [
    id 321
    label "Wyspy_Salomona"
  ]
  node [
    id 322
    label "Hiszpania"
  ]
  node [
    id 323
    label "Boliwia"
  ]
  node [
    id 324
    label "Kirgistan"
  ]
  node [
    id 325
    label "Irlandia"
  ]
  node [
    id 326
    label "Czad"
  ]
  node [
    id 327
    label "Irak"
  ]
  node [
    id 328
    label "Lesoto"
  ]
  node [
    id 329
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 330
    label "Malta"
  ]
  node [
    id 331
    label "Andora"
  ]
  node [
    id 332
    label "Chiny"
  ]
  node [
    id 333
    label "Filipiny"
  ]
  node [
    id 334
    label "Antarktis"
  ]
  node [
    id 335
    label "Niemcy"
  ]
  node [
    id 336
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 337
    label "Pakistan"
  ]
  node [
    id 338
    label "terytorium"
  ]
  node [
    id 339
    label "Nikaragua"
  ]
  node [
    id 340
    label "Brazylia"
  ]
  node [
    id 341
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 342
    label "Maroko"
  ]
  node [
    id 343
    label "Portugalia"
  ]
  node [
    id 344
    label "Niger"
  ]
  node [
    id 345
    label "Kenia"
  ]
  node [
    id 346
    label "Botswana"
  ]
  node [
    id 347
    label "Fid&#380;i"
  ]
  node [
    id 348
    label "Tunezja"
  ]
  node [
    id 349
    label "Australia"
  ]
  node [
    id 350
    label "Tajlandia"
  ]
  node [
    id 351
    label "Burkina_Faso"
  ]
  node [
    id 352
    label "interior"
  ]
  node [
    id 353
    label "Tanzania"
  ]
  node [
    id 354
    label "Benin"
  ]
  node [
    id 355
    label "Indie"
  ]
  node [
    id 356
    label "&#321;otwa"
  ]
  node [
    id 357
    label "Kiribati"
  ]
  node [
    id 358
    label "Antigua_i_Barbuda"
  ]
  node [
    id 359
    label "Rodezja"
  ]
  node [
    id 360
    label "Cypr"
  ]
  node [
    id 361
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 362
    label "Peru"
  ]
  node [
    id 363
    label "Austria"
  ]
  node [
    id 364
    label "Urugwaj"
  ]
  node [
    id 365
    label "Jordania"
  ]
  node [
    id 366
    label "Grecja"
  ]
  node [
    id 367
    label "Azerbejd&#380;an"
  ]
  node [
    id 368
    label "Turcja"
  ]
  node [
    id 369
    label "Samoa"
  ]
  node [
    id 370
    label "Sudan"
  ]
  node [
    id 371
    label "Oman"
  ]
  node [
    id 372
    label "ziemia"
  ]
  node [
    id 373
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 374
    label "Uzbekistan"
  ]
  node [
    id 375
    label "Portoryko"
  ]
  node [
    id 376
    label "Honduras"
  ]
  node [
    id 377
    label "Mongolia"
  ]
  node [
    id 378
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 379
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 380
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 381
    label "Serbia"
  ]
  node [
    id 382
    label "Tajwan"
  ]
  node [
    id 383
    label "Wielka_Brytania"
  ]
  node [
    id 384
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 385
    label "Liban"
  ]
  node [
    id 386
    label "Japonia"
  ]
  node [
    id 387
    label "Ghana"
  ]
  node [
    id 388
    label "Belgia"
  ]
  node [
    id 389
    label "Bahrajn"
  ]
  node [
    id 390
    label "Mikronezja"
  ]
  node [
    id 391
    label "Etiopia"
  ]
  node [
    id 392
    label "Kuwejt"
  ]
  node [
    id 393
    label "grupa"
  ]
  node [
    id 394
    label "Bahamy"
  ]
  node [
    id 395
    label "Rosja"
  ]
  node [
    id 396
    label "Mo&#322;dawia"
  ]
  node [
    id 397
    label "Litwa"
  ]
  node [
    id 398
    label "S&#322;owenia"
  ]
  node [
    id 399
    label "Szwajcaria"
  ]
  node [
    id 400
    label "Erytrea"
  ]
  node [
    id 401
    label "Arabia_Saudyjska"
  ]
  node [
    id 402
    label "Kuba"
  ]
  node [
    id 403
    label "granica_pa&#324;stwa"
  ]
  node [
    id 404
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 405
    label "Malezja"
  ]
  node [
    id 406
    label "Korea"
  ]
  node [
    id 407
    label "Jemen"
  ]
  node [
    id 408
    label "Nowa_Zelandia"
  ]
  node [
    id 409
    label "Namibia"
  ]
  node [
    id 410
    label "Nauru"
  ]
  node [
    id 411
    label "holoarktyka"
  ]
  node [
    id 412
    label "Brunei"
  ]
  node [
    id 413
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 414
    label "Khitai"
  ]
  node [
    id 415
    label "Mauretania"
  ]
  node [
    id 416
    label "Iran"
  ]
  node [
    id 417
    label "Gambia"
  ]
  node [
    id 418
    label "Somalia"
  ]
  node [
    id 419
    label "Holandia"
  ]
  node [
    id 420
    label "Turkmenistan"
  ]
  node [
    id 421
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 422
    label "Salwador"
  ]
  node [
    id 423
    label "Pi&#322;sudski"
  ]
  node [
    id 424
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 425
    label "parlamentarzysta"
  ]
  node [
    id 426
    label "oficer"
  ]
  node [
    id 427
    label "podchor&#261;&#380;y"
  ]
  node [
    id 428
    label "podoficer"
  ]
  node [
    id 429
    label "mundurowy"
  ]
  node [
    id 430
    label "mandatariusz"
  ]
  node [
    id 431
    label "grupa_bilateralna"
  ]
  node [
    id 432
    label "polityk"
  ]
  node [
    id 433
    label "parlament"
  ]
  node [
    id 434
    label "notabl"
  ]
  node [
    id 435
    label "oficja&#322;"
  ]
  node [
    id 436
    label "Komendant"
  ]
  node [
    id 437
    label "kasztanka"
  ]
  node [
    id 438
    label "wyrafinowany"
  ]
  node [
    id 439
    label "niepo&#347;ledni"
  ]
  node [
    id 440
    label "chwalebny"
  ]
  node [
    id 441
    label "z_wysoka"
  ]
  node [
    id 442
    label "wznios&#322;y"
  ]
  node [
    id 443
    label "daleki"
  ]
  node [
    id 444
    label "wysoce"
  ]
  node [
    id 445
    label "szczytnie"
  ]
  node [
    id 446
    label "znaczny"
  ]
  node [
    id 447
    label "warto&#347;ciowy"
  ]
  node [
    id 448
    label "wysoko"
  ]
  node [
    id 449
    label "uprzywilejowany"
  ]
  node [
    id 450
    label "niema&#322;o"
  ]
  node [
    id 451
    label "wiele"
  ]
  node [
    id 452
    label "rozwini&#281;ty"
  ]
  node [
    id 453
    label "dorodny"
  ]
  node [
    id 454
    label "wa&#380;ny"
  ]
  node [
    id 455
    label "prawdziwy"
  ]
  node [
    id 456
    label "du&#380;o"
  ]
  node [
    id 457
    label "szczeg&#243;lny"
  ]
  node [
    id 458
    label "lekki"
  ]
  node [
    id 459
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 460
    label "znacznie"
  ]
  node [
    id 461
    label "zauwa&#380;alny"
  ]
  node [
    id 462
    label "niez&#322;y"
  ]
  node [
    id 463
    label "niepo&#347;lednio"
  ]
  node [
    id 464
    label "wyj&#261;tkowy"
  ]
  node [
    id 465
    label "pochwalny"
  ]
  node [
    id 466
    label "wspania&#322;y"
  ]
  node [
    id 467
    label "szlachetny"
  ]
  node [
    id 468
    label "powa&#380;ny"
  ]
  node [
    id 469
    label "chwalebnie"
  ]
  node [
    id 470
    label "podnios&#322;y"
  ]
  node [
    id 471
    label "wznio&#347;le"
  ]
  node [
    id 472
    label "oderwany"
  ]
  node [
    id 473
    label "pi&#281;kny"
  ]
  node [
    id 474
    label "rewaluowanie"
  ]
  node [
    id 475
    label "warto&#347;ciowo"
  ]
  node [
    id 476
    label "drogi"
  ]
  node [
    id 477
    label "u&#380;yteczny"
  ]
  node [
    id 478
    label "zrewaluowanie"
  ]
  node [
    id 479
    label "dobry"
  ]
  node [
    id 480
    label "obyty"
  ]
  node [
    id 481
    label "wykwintny"
  ]
  node [
    id 482
    label "wyrafinowanie"
  ]
  node [
    id 483
    label "wymy&#347;lny"
  ]
  node [
    id 484
    label "dawny"
  ]
  node [
    id 485
    label "ogl&#281;dny"
  ]
  node [
    id 486
    label "d&#322;ugi"
  ]
  node [
    id 487
    label "daleko"
  ]
  node [
    id 488
    label "odleg&#322;y"
  ]
  node [
    id 489
    label "zwi&#261;zany"
  ]
  node [
    id 490
    label "r&#243;&#380;ny"
  ]
  node [
    id 491
    label "s&#322;aby"
  ]
  node [
    id 492
    label "odlegle"
  ]
  node [
    id 493
    label "oddalony"
  ]
  node [
    id 494
    label "g&#322;&#281;boki"
  ]
  node [
    id 495
    label "obcy"
  ]
  node [
    id 496
    label "nieobecny"
  ]
  node [
    id 497
    label "przysz&#322;y"
  ]
  node [
    id 498
    label "g&#243;rno"
  ]
  node [
    id 499
    label "szczytny"
  ]
  node [
    id 500
    label "intensywnie"
  ]
  node [
    id 501
    label "wielki"
  ]
  node [
    id 502
    label "niezmiernie"
  ]
  node [
    id 503
    label "izba_ni&#380;sza"
  ]
  node [
    id 504
    label "lewica"
  ]
  node [
    id 505
    label "siedziba"
  ]
  node [
    id 506
    label "parliament"
  ]
  node [
    id 507
    label "obrady"
  ]
  node [
    id 508
    label "prawica"
  ]
  node [
    id 509
    label "zgromadzenie"
  ]
  node [
    id 510
    label "centrum"
  ]
  node [
    id 511
    label "&#321;ubianka"
  ]
  node [
    id 512
    label "miejsce_pracy"
  ]
  node [
    id 513
    label "dzia&#322;_personalny"
  ]
  node [
    id 514
    label "Kreml"
  ]
  node [
    id 515
    label "Bia&#322;y_Dom"
  ]
  node [
    id 516
    label "budynek"
  ]
  node [
    id 517
    label "miejsce"
  ]
  node [
    id 518
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 519
    label "sadowisko"
  ]
  node [
    id 520
    label "odm&#322;adzanie"
  ]
  node [
    id 521
    label "liga"
  ]
  node [
    id 522
    label "jednostka_systematyczna"
  ]
  node [
    id 523
    label "gromada"
  ]
  node [
    id 524
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 525
    label "egzemplarz"
  ]
  node [
    id 526
    label "Entuzjastki"
  ]
  node [
    id 527
    label "zbi&#243;r"
  ]
  node [
    id 528
    label "kompozycja"
  ]
  node [
    id 529
    label "Terranie"
  ]
  node [
    id 530
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 531
    label "category"
  ]
  node [
    id 532
    label "pakiet_klimatyczny"
  ]
  node [
    id 533
    label "oddzia&#322;"
  ]
  node [
    id 534
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 535
    label "cz&#261;steczka"
  ]
  node [
    id 536
    label "stage_set"
  ]
  node [
    id 537
    label "type"
  ]
  node [
    id 538
    label "specgrupa"
  ]
  node [
    id 539
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 540
    label "&#346;wietliki"
  ]
  node [
    id 541
    label "odm&#322;odzenie"
  ]
  node [
    id 542
    label "Eurogrupa"
  ]
  node [
    id 543
    label "odm&#322;adza&#263;"
  ]
  node [
    id 544
    label "formacja_geologiczna"
  ]
  node [
    id 545
    label "harcerze_starsi"
  ]
  node [
    id 546
    label "dyskusja"
  ]
  node [
    id 547
    label "conference"
  ]
  node [
    id 548
    label "konsylium"
  ]
  node [
    id 549
    label "concourse"
  ]
  node [
    id 550
    label "gathering"
  ]
  node [
    id 551
    label "skupienie"
  ]
  node [
    id 552
    label "wsp&#243;lnota"
  ]
  node [
    id 553
    label "spowodowanie"
  ]
  node [
    id 554
    label "spotkanie"
  ]
  node [
    id 555
    label "organ"
  ]
  node [
    id 556
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 557
    label "gromadzenie"
  ]
  node [
    id 558
    label "templum"
  ]
  node [
    id 559
    label "konwentykiel"
  ]
  node [
    id 560
    label "klasztor"
  ]
  node [
    id 561
    label "caucus"
  ]
  node [
    id 562
    label "czynno&#347;&#263;"
  ]
  node [
    id 563
    label "pozyskanie"
  ]
  node [
    id 564
    label "kongregacja"
  ]
  node [
    id 565
    label "blok"
  ]
  node [
    id 566
    label "Hollywood"
  ]
  node [
    id 567
    label "centrolew"
  ]
  node [
    id 568
    label "o&#347;rodek"
  ]
  node [
    id 569
    label "centroprawica"
  ]
  node [
    id 570
    label "core"
  ]
  node [
    id 571
    label "hand"
  ]
  node [
    id 572
    label "left"
  ]
  node [
    id 573
    label "europarlament"
  ]
  node [
    id 574
    label "plankton_polityczny"
  ]
  node [
    id 575
    label "ustawodawca"
  ]
  node [
    id 576
    label "termin"
  ]
  node [
    id 577
    label "obchody"
  ]
  node [
    id 578
    label "nazewnictwo"
  ]
  node [
    id 579
    label "term"
  ]
  node [
    id 580
    label "przypadni&#281;cie"
  ]
  node [
    id 581
    label "ekspiracja"
  ]
  node [
    id 582
    label "przypa&#347;&#263;"
  ]
  node [
    id 583
    label "chronogram"
  ]
  node [
    id 584
    label "czas"
  ]
  node [
    id 585
    label "praktyka"
  ]
  node [
    id 586
    label "nazwa"
  ]
  node [
    id 587
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 588
    label "fest"
  ]
  node [
    id 589
    label "celebration"
  ]
  node [
    id 590
    label "pocz&#261;tek"
  ]
  node [
    id 591
    label "impreza"
  ]
  node [
    id 592
    label "&#347;wi&#281;to"
  ]
  node [
    id 593
    label "jubileusz"
  ]
  node [
    id 594
    label "pierworodztwo"
  ]
  node [
    id 595
    label "faza"
  ]
  node [
    id 596
    label "upgrade"
  ]
  node [
    id 597
    label "nast&#281;pstwo"
  ]
  node [
    id 598
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 599
    label "impra"
  ]
  node [
    id 600
    label "rozrywka"
  ]
  node [
    id 601
    label "przyj&#281;cie"
  ]
  node [
    id 602
    label "okazja"
  ]
  node [
    id 603
    label "party"
  ]
  node [
    id 604
    label "anniwersarz"
  ]
  node [
    id 605
    label "rok"
  ]
  node [
    id 606
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 607
    label "ramadan"
  ]
  node [
    id 608
    label "Godowe_&#346;wi&#281;to"
  ]
  node [
    id 609
    label "Nowy_Rok"
  ]
  node [
    id 610
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 611
    label "Barb&#243;rka"
  ]
  node [
    id 612
    label "czu&#263;"
  ]
  node [
    id 613
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 614
    label "&#322;akn&#261;&#263;"
  ]
  node [
    id 615
    label "t&#281;skni&#263;"
  ]
  node [
    id 616
    label "desire"
  ]
  node [
    id 617
    label "chcie&#263;"
  ]
  node [
    id 618
    label "kcie&#263;"
  ]
  node [
    id 619
    label "try"
  ]
  node [
    id 620
    label "post&#281;powa&#263;"
  ]
  node [
    id 621
    label "postrzega&#263;"
  ]
  node [
    id 622
    label "przewidywa&#263;"
  ]
  node [
    id 623
    label "by&#263;"
  ]
  node [
    id 624
    label "smell"
  ]
  node [
    id 625
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 626
    label "uczuwa&#263;"
  ]
  node [
    id 627
    label "spirit"
  ]
  node [
    id 628
    label "doznawa&#263;"
  ]
  node [
    id 629
    label "anticipate"
  ]
  node [
    id 630
    label "miss"
  ]
  node [
    id 631
    label "wymaga&#263;"
  ]
  node [
    id 632
    label "ukaza&#263;"
  ]
  node [
    id 633
    label "przedstawienie"
  ]
  node [
    id 634
    label "pokaza&#263;"
  ]
  node [
    id 635
    label "poda&#263;"
  ]
  node [
    id 636
    label "zapozna&#263;"
  ]
  node [
    id 637
    label "express"
  ]
  node [
    id 638
    label "represent"
  ]
  node [
    id 639
    label "zaproponowa&#263;"
  ]
  node [
    id 640
    label "zademonstrowa&#263;"
  ]
  node [
    id 641
    label "typify"
  ]
  node [
    id 642
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 643
    label "opisa&#263;"
  ]
  node [
    id 644
    label "tenis"
  ]
  node [
    id 645
    label "supply"
  ]
  node [
    id 646
    label "da&#263;"
  ]
  node [
    id 647
    label "ustawi&#263;"
  ]
  node [
    id 648
    label "siatk&#243;wka"
  ]
  node [
    id 649
    label "give"
  ]
  node [
    id 650
    label "zagra&#263;"
  ]
  node [
    id 651
    label "jedzenie"
  ]
  node [
    id 652
    label "poinformowa&#263;"
  ]
  node [
    id 653
    label "introduce"
  ]
  node [
    id 654
    label "nafaszerowa&#263;"
  ]
  node [
    id 655
    label "zaserwowa&#263;"
  ]
  node [
    id 656
    label "testify"
  ]
  node [
    id 657
    label "point"
  ]
  node [
    id 658
    label "udowodni&#263;"
  ]
  node [
    id 659
    label "spowodowa&#263;"
  ]
  node [
    id 660
    label "wyrazi&#263;"
  ]
  node [
    id 661
    label "przeszkoli&#263;"
  ]
  node [
    id 662
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 663
    label "indicate"
  ]
  node [
    id 664
    label "zach&#281;ci&#263;"
  ]
  node [
    id 665
    label "volunteer"
  ]
  node [
    id 666
    label "kandydatura"
  ]
  node [
    id 667
    label "announce"
  ]
  node [
    id 668
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 669
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 670
    label "perform"
  ]
  node [
    id 671
    label "wyj&#347;&#263;"
  ]
  node [
    id 672
    label "zrezygnowa&#263;"
  ]
  node [
    id 673
    label "odst&#261;pi&#263;"
  ]
  node [
    id 674
    label "nak&#322;oni&#263;"
  ]
  node [
    id 675
    label "appear"
  ]
  node [
    id 676
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 677
    label "zacz&#261;&#263;"
  ]
  node [
    id 678
    label "happen"
  ]
  node [
    id 679
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 680
    label "unwrap"
  ]
  node [
    id 681
    label "relate"
  ]
  node [
    id 682
    label "zinterpretowa&#263;"
  ]
  node [
    id 683
    label "delineate"
  ]
  node [
    id 684
    label "attest"
  ]
  node [
    id 685
    label "insert"
  ]
  node [
    id 686
    label "obznajomi&#263;"
  ]
  node [
    id 687
    label "zawrze&#263;"
  ]
  node [
    id 688
    label "pozna&#263;"
  ]
  node [
    id 689
    label "teach"
  ]
  node [
    id 690
    label "pr&#243;bowanie"
  ]
  node [
    id 691
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 692
    label "zademonstrowanie"
  ]
  node [
    id 693
    label "report"
  ]
  node [
    id 694
    label "obgadanie"
  ]
  node [
    id 695
    label "realizacja"
  ]
  node [
    id 696
    label "scena"
  ]
  node [
    id 697
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 698
    label "narration"
  ]
  node [
    id 699
    label "cyrk"
  ]
  node [
    id 700
    label "wytw&#243;r"
  ]
  node [
    id 701
    label "theatrical_performance"
  ]
  node [
    id 702
    label "opisanie"
  ]
  node [
    id 703
    label "malarstwo"
  ]
  node [
    id 704
    label "scenografia"
  ]
  node [
    id 705
    label "teatr"
  ]
  node [
    id 706
    label "ukazanie"
  ]
  node [
    id 707
    label "zapoznanie"
  ]
  node [
    id 708
    label "pokaz"
  ]
  node [
    id 709
    label "podanie"
  ]
  node [
    id 710
    label "spos&#243;b"
  ]
  node [
    id 711
    label "ods&#322;ona"
  ]
  node [
    id 712
    label "exhibit"
  ]
  node [
    id 713
    label "pokazanie"
  ]
  node [
    id 714
    label "wyst&#261;pienie"
  ]
  node [
    id 715
    label "przedstawianie"
  ]
  node [
    id 716
    label "przedstawia&#263;"
  ]
  node [
    id 717
    label "rola"
  ]
  node [
    id 718
    label "charakterystyka"
  ]
  node [
    id 719
    label "zaistnie&#263;"
  ]
  node [
    id 720
    label "Osjan"
  ]
  node [
    id 721
    label "cecha"
  ]
  node [
    id 722
    label "kto&#347;"
  ]
  node [
    id 723
    label "wygl&#261;d"
  ]
  node [
    id 724
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 725
    label "osobowo&#347;&#263;"
  ]
  node [
    id 726
    label "trim"
  ]
  node [
    id 727
    label "poby&#263;"
  ]
  node [
    id 728
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 729
    label "Aspazja"
  ]
  node [
    id 730
    label "punkt_widzenia"
  ]
  node [
    id 731
    label "kompleksja"
  ]
  node [
    id 732
    label "wytrzyma&#263;"
  ]
  node [
    id 733
    label "budowa"
  ]
  node [
    id 734
    label "formacja"
  ]
  node [
    id 735
    label "pozosta&#263;"
  ]
  node [
    id 736
    label "go&#347;&#263;"
  ]
  node [
    id 737
    label "zmusi&#263;"
  ]
  node [
    id 738
    label "digest"
  ]
  node [
    id 739
    label "proceed"
  ]
  node [
    id 740
    label "mentalno&#347;&#263;"
  ]
  node [
    id 741
    label "byt"
  ]
  node [
    id 742
    label "superego"
  ]
  node [
    id 743
    label "psychika"
  ]
  node [
    id 744
    label "charakter"
  ]
  node [
    id 745
    label "wn&#281;trze"
  ]
  node [
    id 746
    label "self"
  ]
  node [
    id 747
    label "odwiedziny"
  ]
  node [
    id 748
    label "klient"
  ]
  node [
    id 749
    label "restauracja"
  ]
  node [
    id 750
    label "przybysz"
  ]
  node [
    id 751
    label "uczestnik"
  ]
  node [
    id 752
    label "hotel"
  ]
  node [
    id 753
    label "sztuka"
  ]
  node [
    id 754
    label "m&#322;ot"
  ]
  node [
    id 755
    label "znak"
  ]
  node [
    id 756
    label "drzewo"
  ]
  node [
    id 757
    label "pr&#243;ba"
  ]
  node [
    id 758
    label "attribute"
  ]
  node [
    id 759
    label "marka"
  ]
  node [
    id 760
    label "przedmiot"
  ]
  node [
    id 761
    label "p&#322;&#243;d"
  ]
  node [
    id 762
    label "work"
  ]
  node [
    id 763
    label "rezultat"
  ]
  node [
    id 764
    label "postarzenie"
  ]
  node [
    id 765
    label "kszta&#322;t"
  ]
  node [
    id 766
    label "postarzanie"
  ]
  node [
    id 767
    label "brzydota"
  ]
  node [
    id 768
    label "postarza&#263;"
  ]
  node [
    id 769
    label "nadawanie"
  ]
  node [
    id 770
    label "postarzy&#263;"
  ]
  node [
    id 771
    label "widok"
  ]
  node [
    id 772
    label "prostota"
  ]
  node [
    id 773
    label "ubarwienie"
  ]
  node [
    id 774
    label "shape"
  ]
  node [
    id 775
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 776
    label "osta&#263;_si&#281;"
  ]
  node [
    id 777
    label "catch"
  ]
  node [
    id 778
    label "support"
  ]
  node [
    id 779
    label "prze&#380;y&#263;"
  ]
  node [
    id 780
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 781
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 782
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 783
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 784
    label "stay"
  ]
  node [
    id 785
    label "znaczenie"
  ]
  node [
    id 786
    label "opis"
  ]
  node [
    id 787
    label "parametr"
  ]
  node [
    id 788
    label "analiza"
  ]
  node [
    id 789
    label "specyfikacja"
  ]
  node [
    id 790
    label "wykres"
  ]
  node [
    id 791
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 792
    label "interpretacja"
  ]
  node [
    id 793
    label "mechanika"
  ]
  node [
    id 794
    label "struktura"
  ]
  node [
    id 795
    label "kreacja"
  ]
  node [
    id 796
    label "r&#243;w"
  ]
  node [
    id 797
    label "posesja"
  ]
  node [
    id 798
    label "konstrukcja"
  ]
  node [
    id 799
    label "wjazd"
  ]
  node [
    id 800
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 801
    label "praca"
  ]
  node [
    id 802
    label "constitution"
  ]
  node [
    id 803
    label "Bund"
  ]
  node [
    id 804
    label "Mazowsze"
  ]
  node [
    id 805
    label "PPR"
  ]
  node [
    id 806
    label "Jakobici"
  ]
  node [
    id 807
    label "zesp&#243;&#322;"
  ]
  node [
    id 808
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 809
    label "leksem"
  ]
  node [
    id 810
    label "SLD"
  ]
  node [
    id 811
    label "zespolik"
  ]
  node [
    id 812
    label "Razem"
  ]
  node [
    id 813
    label "PiS"
  ]
  node [
    id 814
    label "zjawisko"
  ]
  node [
    id 815
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 816
    label "Kuomintang"
  ]
  node [
    id 817
    label "ZSL"
  ]
  node [
    id 818
    label "jednostka"
  ]
  node [
    id 819
    label "proces"
  ]
  node [
    id 820
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 821
    label "rugby"
  ]
  node [
    id 822
    label "AWS"
  ]
  node [
    id 823
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 824
    label "PO"
  ]
  node [
    id 825
    label "si&#322;a"
  ]
  node [
    id 826
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 827
    label "Federali&#347;ci"
  ]
  node [
    id 828
    label "PSL"
  ]
  node [
    id 829
    label "wojsko"
  ]
  node [
    id 830
    label "Wigowie"
  ]
  node [
    id 831
    label "ZChN"
  ]
  node [
    id 832
    label "egzekutywa"
  ]
  node [
    id 833
    label "The_Beatles"
  ]
  node [
    id 834
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 835
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 836
    label "unit"
  ]
  node [
    id 837
    label "Depeche_Mode"
  ]
  node [
    id 838
    label "forma"
  ]
  node [
    id 839
    label "Perykles"
  ]
  node [
    id 840
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 841
    label "harcap"
  ]
  node [
    id 842
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 843
    label "demobilizowanie"
  ]
  node [
    id 844
    label "demobilizowa&#263;"
  ]
  node [
    id 845
    label "zdemobilizowanie"
  ]
  node [
    id 846
    label "Gurkha"
  ]
  node [
    id 847
    label "so&#322;dat"
  ]
  node [
    id 848
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 849
    label "rota"
  ]
  node [
    id 850
    label "zdemobilizowa&#263;"
  ]
  node [
    id 851
    label "walcz&#261;cy"
  ]
  node [
    id 852
    label "&#380;o&#322;dowy"
  ]
  node [
    id 853
    label "piecz&#261;tka"
  ]
  node [
    id 854
    label "s&#261;d_ko&#347;cielny"
  ]
  node [
    id 855
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 856
    label "&#322;ama&#263;"
  ]
  node [
    id 857
    label "stra&#380;_po&#380;arna"
  ]
  node [
    id 858
    label "tortury"
  ]
  node [
    id 859
    label "papie&#380;"
  ]
  node [
    id 860
    label "chordofon_szarpany"
  ]
  node [
    id 861
    label "przysi&#281;ga"
  ]
  node [
    id 862
    label "&#322;amanie"
  ]
  node [
    id 863
    label "szyk"
  ]
  node [
    id 864
    label "s&#261;d_apelacyjny"
  ]
  node [
    id 865
    label "whip"
  ]
  node [
    id 866
    label "Rota"
  ]
  node [
    id 867
    label "instrument_strunowy"
  ]
  node [
    id 868
    label "formu&#322;a"
  ]
  node [
    id 869
    label "zrejterowanie"
  ]
  node [
    id 870
    label "zmobilizowa&#263;"
  ]
  node [
    id 871
    label "dezerter"
  ]
  node [
    id 872
    label "oddzia&#322;_karny"
  ]
  node [
    id 873
    label "rezerwa"
  ]
  node [
    id 874
    label "tabor"
  ]
  node [
    id 875
    label "wermacht"
  ]
  node [
    id 876
    label "cofni&#281;cie"
  ]
  node [
    id 877
    label "potencja"
  ]
  node [
    id 878
    label "fala"
  ]
  node [
    id 879
    label "korpus"
  ]
  node [
    id 880
    label "soldateska"
  ]
  node [
    id 881
    label "ods&#322;ugiwanie"
  ]
  node [
    id 882
    label "werbowanie_si&#281;"
  ]
  node [
    id 883
    label "&#380;o&#322;nierz_niezawodowy"
  ]
  node [
    id 884
    label "s&#322;u&#380;ba"
  ]
  node [
    id 885
    label "or&#281;&#380;"
  ]
  node [
    id 886
    label "Legia_Cudzoziemska"
  ]
  node [
    id 887
    label "Armia_Czerwona"
  ]
  node [
    id 888
    label "ods&#322;ugiwa&#263;"
  ]
  node [
    id 889
    label "rejterowanie"
  ]
  node [
    id 890
    label "Czerwona_Gwardia"
  ]
  node [
    id 891
    label "zrejterowa&#263;"
  ]
  node [
    id 892
    label "sztabslekarz"
  ]
  node [
    id 893
    label "zmobilizowanie"
  ]
  node [
    id 894
    label "wojo"
  ]
  node [
    id 895
    label "pospolite_ruszenie"
  ]
  node [
    id 896
    label "Eurokorpus"
  ]
  node [
    id 897
    label "mobilizowanie"
  ]
  node [
    id 898
    label "rejterowa&#263;"
  ]
  node [
    id 899
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 900
    label "mobilizowa&#263;"
  ]
  node [
    id 901
    label "Armia_Krajowa"
  ]
  node [
    id 902
    label "obrona"
  ]
  node [
    id 903
    label "dryl"
  ]
  node [
    id 904
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 905
    label "petarda"
  ]
  node [
    id 906
    label "pozycja"
  ]
  node [
    id 907
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 908
    label "zaw&#243;d"
  ]
  node [
    id 909
    label "ucze&#324;"
  ]
  node [
    id 910
    label "odstr&#281;czenie"
  ]
  node [
    id 911
    label "zreorganizowanie"
  ]
  node [
    id 912
    label "odprawienie"
  ]
  node [
    id 913
    label "zniech&#281;cenie_si&#281;"
  ]
  node [
    id 914
    label "zniech&#281;ca&#263;"
  ]
  node [
    id 915
    label "zwalnia&#263;"
  ]
  node [
    id 916
    label "przebudowywa&#263;"
  ]
  node [
    id 917
    label "peruka"
  ]
  node [
    id 918
    label "warkocz"
  ]
  node [
    id 919
    label "zwalnianie"
  ]
  node [
    id 920
    label "zniech&#281;canie"
  ]
  node [
    id 921
    label "przebudowywanie"
  ]
  node [
    id 922
    label "zniech&#281;canie_si&#281;"
  ]
  node [
    id 923
    label "nosiciel"
  ]
  node [
    id 924
    label "wojownik"
  ]
  node [
    id 925
    label "zniech&#281;ci&#263;"
  ]
  node [
    id 926
    label "zreorganizowa&#263;"
  ]
  node [
    id 927
    label "odprawi&#263;"
  ]
  node [
    id 928
    label "dywizjon_artylerii"
  ]
  node [
    id 929
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 930
    label "military"
  ]
  node [
    id 931
    label "wojska_pancerne"
  ]
  node [
    id 932
    label "linia"
  ]
  node [
    id 933
    label "legia"
  ]
  node [
    id 934
    label "piechota"
  ]
  node [
    id 935
    label "rzut"
  ]
  node [
    id 936
    label "zwi&#261;zek_operacyjny"
  ]
  node [
    id 937
    label "artyleria"
  ]
  node [
    id 938
    label "t&#322;um"
  ]
  node [
    id 939
    label "szlak_bojowy"
  ]
  node [
    id 940
    label "dywizjon_kawaleryjski"
  ]
  node [
    id 941
    label "milicja"
  ]
  node [
    id 942
    label "kawaleria_powietrzna"
  ]
  node [
    id 943
    label "brygada"
  ]
  node [
    id 944
    label "bateria"
  ]
  node [
    id 945
    label "o&#347;"
  ]
  node [
    id 946
    label "usenet"
  ]
  node [
    id 947
    label "rozprz&#261;c"
  ]
  node [
    id 948
    label "zachowanie"
  ]
  node [
    id 949
    label "cybernetyk"
  ]
  node [
    id 950
    label "podsystem"
  ]
  node [
    id 951
    label "system"
  ]
  node [
    id 952
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 953
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 954
    label "sk&#322;ad"
  ]
  node [
    id 955
    label "systemat"
  ]
  node [
    id 956
    label "konstelacja"
  ]
  node [
    id 957
    label "demofobia"
  ]
  node [
    id 958
    label "lud"
  ]
  node [
    id 959
    label "najazd"
  ]
  node [
    id 960
    label "pachwina"
  ]
  node [
    id 961
    label "obudowa"
  ]
  node [
    id 962
    label "corpus"
  ]
  node [
    id 963
    label "brzuch"
  ]
  node [
    id 964
    label "budowla"
  ]
  node [
    id 965
    label "sto&#380;ek_wzrostu"
  ]
  node [
    id 966
    label "dywizja"
  ]
  node [
    id 967
    label "mi&#281;so"
  ]
  node [
    id 968
    label "dekolt"
  ]
  node [
    id 969
    label "zad"
  ]
  node [
    id 970
    label "documentation"
  ]
  node [
    id 971
    label "zasadzi&#263;"
  ]
  node [
    id 972
    label "zasadzenie"
  ]
  node [
    id 973
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 974
    label "struktura_anatomiczna"
  ]
  node [
    id 975
    label "bok"
  ]
  node [
    id 976
    label "podstawowy"
  ]
  node [
    id 977
    label "pupa"
  ]
  node [
    id 978
    label "krocze"
  ]
  node [
    id 979
    label "pier&#347;"
  ]
  node [
    id 980
    label "za&#322;o&#380;enie"
  ]
  node [
    id 981
    label "tuszka"
  ]
  node [
    id 982
    label "punkt_odniesienia"
  ]
  node [
    id 983
    label "konkordancja"
  ]
  node [
    id 984
    label "plecy"
  ]
  node [
    id 985
    label "klatka_piersiowa"
  ]
  node [
    id 986
    label "dr&#243;b"
  ]
  node [
    id 987
    label "biodro"
  ]
  node [
    id 988
    label "pacha"
  ]
  node [
    id 989
    label "despotyzm"
  ]
  node [
    id 990
    label "lias"
  ]
  node [
    id 991
    label "dzia&#322;"
  ]
  node [
    id 992
    label "pi&#281;tro"
  ]
  node [
    id 993
    label "jednostka_geologiczna"
  ]
  node [
    id 994
    label "filia"
  ]
  node [
    id 995
    label "malm"
  ]
  node [
    id 996
    label "whole"
  ]
  node [
    id 997
    label "dogger"
  ]
  node [
    id 998
    label "poziom"
  ]
  node [
    id 999
    label "promocja"
  ]
  node [
    id 1000
    label "kurs"
  ]
  node [
    id 1001
    label "bank"
  ]
  node [
    id 1002
    label "ajencja"
  ]
  node [
    id 1003
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 1004
    label "agencja"
  ]
  node [
    id 1005
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 1006
    label "szpital"
  ]
  node [
    id 1007
    label "egzamin"
  ]
  node [
    id 1008
    label "walka"
  ]
  node [
    id 1009
    label "gracz"
  ]
  node [
    id 1010
    label "poj&#281;cie"
  ]
  node [
    id 1011
    label "protection"
  ]
  node [
    id 1012
    label "poparcie"
  ]
  node [
    id 1013
    label "mecz"
  ]
  node [
    id 1014
    label "reakcja"
  ]
  node [
    id 1015
    label "defense"
  ]
  node [
    id 1016
    label "s&#261;d"
  ]
  node [
    id 1017
    label "auspices"
  ]
  node [
    id 1018
    label "gra"
  ]
  node [
    id 1019
    label "ochrona"
  ]
  node [
    id 1020
    label "sp&#243;r"
  ]
  node [
    id 1021
    label "post&#281;powanie"
  ]
  node [
    id 1022
    label "manewr"
  ]
  node [
    id 1023
    label "defensive_structure"
  ]
  node [
    id 1024
    label "guard_duty"
  ]
  node [
    id 1025
    label "strona"
  ]
  node [
    id 1026
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 1027
    label "zas&#243;b"
  ]
  node [
    id 1028
    label "nieufno&#347;&#263;"
  ]
  node [
    id 1029
    label "zapasy"
  ]
  node [
    id 1030
    label "ostro&#380;no&#347;&#263;"
  ]
  node [
    id 1031
    label "resource"
  ]
  node [
    id 1032
    label "host"
  ]
  node [
    id 1033
    label "supremum"
  ]
  node [
    id 1034
    label "nawr&#243;t_choroby"
  ]
  node [
    id 1035
    label "potomstwo"
  ]
  node [
    id 1036
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 1037
    label "odwzorowanie"
  ]
  node [
    id 1038
    label "rysunek"
  ]
  node [
    id 1039
    label "scene"
  ]
  node [
    id 1040
    label "throw"
  ]
  node [
    id 1041
    label "matematyka"
  ]
  node [
    id 1042
    label "float"
  ]
  node [
    id 1043
    label "projection"
  ]
  node [
    id 1044
    label "injection"
  ]
  node [
    id 1045
    label "blow"
  ]
  node [
    id 1046
    label "funkcja"
  ]
  node [
    id 1047
    label "pomys&#322;"
  ]
  node [
    id 1048
    label "ruch"
  ]
  node [
    id 1049
    label "k&#322;ad"
  ]
  node [
    id 1050
    label "infimum"
  ]
  node [
    id 1051
    label "mold"
  ]
  node [
    id 1052
    label "crew"
  ]
  node [
    id 1053
    label "batalion"
  ]
  node [
    id 1054
    label "D&#261;browszczacy"
  ]
  node [
    id 1055
    label "za&#322;oga"
  ]
  node [
    id 1056
    label "kompania_honorowa"
  ]
  node [
    id 1057
    label "infantry"
  ]
  node [
    id 1058
    label "falanga"
  ]
  node [
    id 1059
    label "policja"
  ]
  node [
    id 1060
    label "formacja_paramilitarna"
  ]
  node [
    id 1061
    label "artillery"
  ]
  node [
    id 1062
    label "uzbrojenie"
  ]
  node [
    id 1063
    label "munition"
  ]
  node [
    id 1064
    label "dzia&#322;o"
  ]
  node [
    id 1065
    label "coalescence"
  ]
  node [
    id 1066
    label "bearing"
  ]
  node [
    id 1067
    label "komunikacja"
  ]
  node [
    id 1068
    label "dodawanie"
  ]
  node [
    id 1069
    label "mno&#380;enie"
  ]
  node [
    id 1070
    label "kontakt"
  ]
  node [
    id 1071
    label "zaw&#243;r"
  ]
  node [
    id 1072
    label "kolekcja"
  ]
  node [
    id 1073
    label "oficer_ogniowy"
  ]
  node [
    id 1074
    label "kran"
  ]
  node [
    id 1075
    label "pododdzia&#322;"
  ]
  node [
    id 1076
    label "cell"
  ]
  node [
    id 1077
    label "pluton"
  ]
  node [
    id 1078
    label "odkr&#281;ca&#263;_wod&#281;"
  ]
  node [
    id 1079
    label "odkr&#281;ci&#263;_wod&#281;"
  ]
  node [
    id 1080
    label "dzia&#322;obitnia"
  ]
  node [
    id 1081
    label "urz&#261;dzenie"
  ]
  node [
    id 1082
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1083
    label "potency"
  ]
  node [
    id 1084
    label "tomizm"
  ]
  node [
    id 1085
    label "wydolno&#347;&#263;"
  ]
  node [
    id 1086
    label "zdolno&#347;&#263;"
  ]
  node [
    id 1087
    label "arystotelizm"
  ]
  node [
    id 1088
    label "gotowo&#347;&#263;"
  ]
  node [
    id 1089
    label "element_anatomiczny"
  ]
  node [
    id 1090
    label "poro&#380;e"
  ]
  node [
    id 1091
    label "heraldyka"
  ]
  node [
    id 1092
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 1093
    label "bro&#324;"
  ]
  node [
    id 1094
    label "call"
  ]
  node [
    id 1095
    label "stawia&#263;_w_stan_pogotowia"
  ]
  node [
    id 1096
    label "usposabia&#263;"
  ]
  node [
    id 1097
    label "powo&#322;ywa&#263;"
  ]
  node [
    id 1098
    label "pobudza&#263;"
  ]
  node [
    id 1099
    label "boost"
  ]
  node [
    id 1100
    label "przygotowywa&#263;"
  ]
  node [
    id 1101
    label "revocation"
  ]
  node [
    id 1102
    label "cofni&#281;cie_si&#281;"
  ]
  node [
    id 1103
    label "uniewa&#380;nienie"
  ]
  node [
    id 1104
    label "przemieszczenie"
  ]
  node [
    id 1105
    label "coitus_interruptus"
  ]
  node [
    id 1106
    label "retraction"
  ]
  node [
    id 1107
    label "wycofywanie_si&#281;"
  ]
  node [
    id 1108
    label "uciekanie"
  ]
  node [
    id 1109
    label "rezygnowanie"
  ]
  node [
    id 1110
    label "unikanie"
  ]
  node [
    id 1111
    label "energia"
  ]
  node [
    id 1112
    label "rozwi&#261;zanie"
  ]
  node [
    id 1113
    label "wuchta"
  ]
  node [
    id 1114
    label "zaleta"
  ]
  node [
    id 1115
    label "moment_si&#322;y"
  ]
  node [
    id 1116
    label "mn&#243;stwo"
  ]
  node [
    id 1117
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 1118
    label "capacity"
  ]
  node [
    id 1119
    label "magnitude"
  ]
  node [
    id 1120
    label "przemoc"
  ]
  node [
    id 1121
    label "nastawi&#263;"
  ]
  node [
    id 1122
    label "wake_up"
  ]
  node [
    id 1123
    label "powo&#322;a&#263;"
  ]
  node [
    id 1124
    label "postawi&#263;_w_stan_pogotowia"
  ]
  node [
    id 1125
    label "pool"
  ]
  node [
    id 1126
    label "pobudzi&#263;"
  ]
  node [
    id 1127
    label "przygotowa&#263;"
  ]
  node [
    id 1128
    label "zrezygnowanie"
  ]
  node [
    id 1129
    label "wycofanie_si&#281;"
  ]
  node [
    id 1130
    label "przestraszenie_si&#281;"
  ]
  node [
    id 1131
    label "uciekni&#281;cie"
  ]
  node [
    id 1132
    label "powo&#322;anie"
  ]
  node [
    id 1133
    label "pobudzenie"
  ]
  node [
    id 1134
    label "postawienie_"
  ]
  node [
    id 1135
    label "zebranie_si&#322;"
  ]
  node [
    id 1136
    label "przygotowanie"
  ]
  node [
    id 1137
    label "nastawienie"
  ]
  node [
    id 1138
    label "vivification"
  ]
  node [
    id 1139
    label "powo&#322;ywanie"
  ]
  node [
    id 1140
    label "vitalization"
  ]
  node [
    id 1141
    label "usposabianie"
  ]
  node [
    id 1142
    label "stawianie_"
  ]
  node [
    id 1143
    label "pobudzanie"
  ]
  node [
    id 1144
    label "przygotowywanie"
  ]
  node [
    id 1145
    label "uciec"
  ]
  node [
    id 1146
    label "stch&#243;rzy&#263;"
  ]
  node [
    id 1147
    label "wycofa&#263;_si&#281;"
  ]
  node [
    id 1148
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 1149
    label "retreat"
  ]
  node [
    id 1150
    label "ucieka&#263;"
  ]
  node [
    id 1151
    label "rezygnowa&#263;"
  ]
  node [
    id 1152
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1153
    label "debit"
  ]
  node [
    id 1154
    label "druk"
  ]
  node [
    id 1155
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1156
    label "szata_graficzna"
  ]
  node [
    id 1157
    label "wydawa&#263;"
  ]
  node [
    id 1158
    label "szermierka"
  ]
  node [
    id 1159
    label "spis"
  ]
  node [
    id 1160
    label "wyda&#263;"
  ]
  node [
    id 1161
    label "ustawienie"
  ]
  node [
    id 1162
    label "publikacja"
  ]
  node [
    id 1163
    label "status"
  ]
  node [
    id 1164
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 1165
    label "adres"
  ]
  node [
    id 1166
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 1167
    label "rozmieszczenie"
  ]
  node [
    id 1168
    label "sytuacja"
  ]
  node [
    id 1169
    label "rz&#261;d"
  ]
  node [
    id 1170
    label "redaktor"
  ]
  node [
    id 1171
    label "awansowa&#263;"
  ]
  node [
    id 1172
    label "awans"
  ]
  node [
    id 1173
    label "awansowanie"
  ]
  node [
    id 1174
    label "poster"
  ]
  node [
    id 1175
    label "le&#380;e&#263;"
  ]
  node [
    id 1176
    label "Cygan"
  ]
  node [
    id 1177
    label "dostawa"
  ]
  node [
    id 1178
    label "transport"
  ]
  node [
    id 1179
    label "pojazd"
  ]
  node [
    id 1180
    label "ob&#243;z"
  ]
  node [
    id 1181
    label "park"
  ]
  node [
    id 1182
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 1183
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 1184
    label "poprowadzi&#263;"
  ]
  node [
    id 1185
    label "cord"
  ]
  node [
    id 1186
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 1187
    label "trasa"
  ]
  node [
    id 1188
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1189
    label "tract"
  ]
  node [
    id 1190
    label "materia&#322;_zecerski"
  ]
  node [
    id 1191
    label "przeorientowywanie"
  ]
  node [
    id 1192
    label "curve"
  ]
  node [
    id 1193
    label "figura_geometryczna"
  ]
  node [
    id 1194
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 1195
    label "jard"
  ]
  node [
    id 1196
    label "szczep"
  ]
  node [
    id 1197
    label "phreaker"
  ]
  node [
    id 1198
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 1199
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1200
    label "prowadzi&#263;"
  ]
  node [
    id 1201
    label "przeorientowywa&#263;"
  ]
  node [
    id 1202
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 1203
    label "access"
  ]
  node [
    id 1204
    label "przeorientowanie"
  ]
  node [
    id 1205
    label "przeorientowa&#263;"
  ]
  node [
    id 1206
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 1207
    label "billing"
  ]
  node [
    id 1208
    label "granica"
  ]
  node [
    id 1209
    label "szpaler"
  ]
  node [
    id 1210
    label "sztrych"
  ]
  node [
    id 1211
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1212
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 1213
    label "drzewo_genealogiczne"
  ]
  node [
    id 1214
    label "transporter"
  ]
  node [
    id 1215
    label "line"
  ]
  node [
    id 1216
    label "fragment"
  ]
  node [
    id 1217
    label "przew&#243;d"
  ]
  node [
    id 1218
    label "granice"
  ]
  node [
    id 1219
    label "przewo&#378;nik"
  ]
  node [
    id 1220
    label "przystanek"
  ]
  node [
    id 1221
    label "linijka"
  ]
  node [
    id 1222
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1223
    label "Ural"
  ]
  node [
    id 1224
    label "prowadzenie"
  ]
  node [
    id 1225
    label "tekst"
  ]
  node [
    id 1226
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1227
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1228
    label "koniec"
  ]
  node [
    id 1229
    label "rodzimy"
  ]
  node [
    id 1230
    label "w&#322;asny"
  ]
  node [
    id 1231
    label "tutejszy"
  ]
  node [
    id 1232
    label "kolejny"
  ]
  node [
    id 1233
    label "niedawno"
  ]
  node [
    id 1234
    label "poprzedni"
  ]
  node [
    id 1235
    label "pozosta&#322;y"
  ]
  node [
    id 1236
    label "ostatnio"
  ]
  node [
    id 1237
    label "sko&#324;czony"
  ]
  node [
    id 1238
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 1239
    label "aktualny"
  ]
  node [
    id 1240
    label "najgorszy"
  ]
  node [
    id 1241
    label "istota_&#380;ywa"
  ]
  node [
    id 1242
    label "w&#261;tpliwy"
  ]
  node [
    id 1243
    label "nast&#281;pnie"
  ]
  node [
    id 1244
    label "inny"
  ]
  node [
    id 1245
    label "nastopny"
  ]
  node [
    id 1246
    label "kolejno"
  ]
  node [
    id 1247
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1248
    label "przesz&#322;y"
  ]
  node [
    id 1249
    label "wcze&#347;niejszy"
  ]
  node [
    id 1250
    label "poprzednio"
  ]
  node [
    id 1251
    label "w&#261;tpliwie"
  ]
  node [
    id 1252
    label "pozorny"
  ]
  node [
    id 1253
    label "&#380;ywy"
  ]
  node [
    id 1254
    label "ostateczny"
  ]
  node [
    id 1255
    label "wykszta&#322;cony"
  ]
  node [
    id 1256
    label "dyplomowany"
  ]
  node [
    id 1257
    label "wykwalifikowany"
  ]
  node [
    id 1258
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 1259
    label "kompletny"
  ]
  node [
    id 1260
    label "sko&#324;czenie"
  ]
  node [
    id 1261
    label "okre&#347;lony"
  ]
  node [
    id 1262
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 1263
    label "aktualnie"
  ]
  node [
    id 1264
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1265
    label "aktualizowanie"
  ]
  node [
    id 1266
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 1267
    label "uaktualnienie"
  ]
  node [
    id 1268
    label "wicekomendant"
  ]
  node [
    id 1269
    label "naczelnik"
  ]
  node [
    id 1270
    label "dow&#243;dca"
  ]
  node [
    id 1271
    label "instruktor"
  ]
  node [
    id 1272
    label "dow&#243;dztwo"
  ]
  node [
    id 1273
    label "naczelnikostwo"
  ]
  node [
    id 1274
    label "harcerz"
  ]
  node [
    id 1275
    label "trener"
  ]
  node [
    id 1276
    label "organizacja_harcerska"
  ]
  node [
    id 1277
    label "pryncypa&#322;"
  ]
  node [
    id 1278
    label "kierowa&#263;"
  ]
  node [
    id 1279
    label "kierownictwo"
  ]
  node [
    id 1280
    label "zast&#281;pca"
  ]
  node [
    id 1281
    label "figura_p&#322;aska"
  ]
  node [
    id 1282
    label "ko&#322;o"
  ]
  node [
    id 1283
    label "circumference"
  ]
  node [
    id 1284
    label "&#322;uk"
  ]
  node [
    id 1285
    label "circle"
  ]
  node [
    id 1286
    label "gang"
  ]
  node [
    id 1287
    label "zabawa"
  ]
  node [
    id 1288
    label "obr&#281;cz"
  ]
  node [
    id 1289
    label "piasta"
  ]
  node [
    id 1290
    label "lap"
  ]
  node [
    id 1291
    label "sphere"
  ]
  node [
    id 1292
    label "kolokwium"
  ]
  node [
    id 1293
    label "pi"
  ]
  node [
    id 1294
    label "zwolnica"
  ]
  node [
    id 1295
    label "p&#243;&#322;kole"
  ]
  node [
    id 1296
    label "sejmik"
  ]
  node [
    id 1297
    label "figura_ograniczona"
  ]
  node [
    id 1298
    label "odcinek_ko&#322;a"
  ]
  node [
    id 1299
    label "stowarzyszenie"
  ]
  node [
    id 1300
    label "podwozie"
  ]
  node [
    id 1301
    label "ewolucja_narciarska"
  ]
  node [
    id 1302
    label "bro&#324;_sportowa"
  ]
  node [
    id 1303
    label "strza&#322;ka"
  ]
  node [
    id 1304
    label "affiliation"
  ]
  node [
    id 1305
    label "graf"
  ]
  node [
    id 1306
    label "ci&#281;ciwa"
  ]
  node [
    id 1307
    label "&#322;&#281;k"
  ]
  node [
    id 1308
    label "bow_and_arrow"
  ]
  node [
    id 1309
    label "element"
  ]
  node [
    id 1310
    label "arkada"
  ]
  node [
    id 1311
    label "&#322;&#281;czysko"
  ]
  node [
    id 1312
    label "&#322;ubia"
  ]
  node [
    id 1313
    label "end"
  ]
  node [
    id 1314
    label "pod&#322;ucze"
  ]
  node [
    id 1315
    label "bro&#324;_miotaj&#261;ca"
  ]
  node [
    id 1316
    label "znak_muzyczny"
  ]
  node [
    id 1317
    label "ligature"
  ]
  node [
    id 1318
    label "nacjonalistyczny"
  ]
  node [
    id 1319
    label "narodowo"
  ]
  node [
    id 1320
    label "wynios&#322;y"
  ]
  node [
    id 1321
    label "dono&#347;ny"
  ]
  node [
    id 1322
    label "silny"
  ]
  node [
    id 1323
    label "wa&#380;nie"
  ]
  node [
    id 1324
    label "istotnie"
  ]
  node [
    id 1325
    label "eksponowany"
  ]
  node [
    id 1326
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1327
    label "nale&#380;ny"
  ]
  node [
    id 1328
    label "nale&#380;yty"
  ]
  node [
    id 1329
    label "typowy"
  ]
  node [
    id 1330
    label "uprawniony"
  ]
  node [
    id 1331
    label "zasadniczy"
  ]
  node [
    id 1332
    label "stosownie"
  ]
  node [
    id 1333
    label "taki"
  ]
  node [
    id 1334
    label "charakterystyczny"
  ]
  node [
    id 1335
    label "ten"
  ]
  node [
    id 1336
    label "polityczny"
  ]
  node [
    id 1337
    label "nacjonalistycznie"
  ]
  node [
    id 1338
    label "narodowo&#347;ciowy"
  ]
  node [
    id 1339
    label "zjednoczenie_si&#281;"
  ]
  node [
    id 1340
    label "zgoda"
  ]
  node [
    id 1341
    label "integration"
  ]
  node [
    id 1342
    label "association"
  ]
  node [
    id 1343
    label "zwi&#261;zek"
  ]
  node [
    id 1344
    label "odwadnia&#263;"
  ]
  node [
    id 1345
    label "wi&#261;zanie"
  ]
  node [
    id 1346
    label "odwodni&#263;"
  ]
  node [
    id 1347
    label "bratnia_dusza"
  ]
  node [
    id 1348
    label "powi&#261;zanie"
  ]
  node [
    id 1349
    label "zwi&#261;zanie"
  ]
  node [
    id 1350
    label "konstytucja"
  ]
  node [
    id 1351
    label "marriage"
  ]
  node [
    id 1352
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 1353
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 1354
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 1355
    label "zwi&#261;za&#263;"
  ]
  node [
    id 1356
    label "odwadnianie"
  ]
  node [
    id 1357
    label "odwodnienie"
  ]
  node [
    id 1358
    label "marketing_afiliacyjny"
  ]
  node [
    id 1359
    label "substancja_chemiczna"
  ]
  node [
    id 1360
    label "koligacja"
  ]
  node [
    id 1361
    label "lokant"
  ]
  node [
    id 1362
    label "azeotrop"
  ]
  node [
    id 1363
    label "decyzja"
  ]
  node [
    id 1364
    label "wiedza"
  ]
  node [
    id 1365
    label "consensus"
  ]
  node [
    id 1366
    label "zwalnianie_si&#281;"
  ]
  node [
    id 1367
    label "odpowied&#378;"
  ]
  node [
    id 1368
    label "zwolni&#263;_si&#281;"
  ]
  node [
    id 1369
    label "jednomy&#347;lno&#347;&#263;"
  ]
  node [
    id 1370
    label "spok&#243;j"
  ]
  node [
    id 1371
    label "license"
  ]
  node [
    id 1372
    label "agreement"
  ]
  node [
    id 1373
    label "zwalnia&#263;_si&#281;"
  ]
  node [
    id 1374
    label "zwolnienie_si&#281;"
  ]
  node [
    id 1375
    label "entity"
  ]
  node [
    id 1376
    label "pozwole&#324;stwo"
  ]
  node [
    id 1377
    label "stworzenie"
  ]
  node [
    id 1378
    label "zespolenie"
  ]
  node [
    id 1379
    label "dressing"
  ]
  node [
    id 1380
    label "pomy&#347;lenie"
  ]
  node [
    id 1381
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 1382
    label "alliance"
  ]
  node [
    id 1383
    label "joining"
  ]
  node [
    id 1384
    label "umo&#380;liwienie"
  ]
  node [
    id 1385
    label "akt_p&#322;ciowy"
  ]
  node [
    id 1386
    label "mention"
  ]
  node [
    id 1387
    label "port"
  ]
  node [
    id 1388
    label "rzucenie"
  ]
  node [
    id 1389
    label "zgrzeina"
  ]
  node [
    id 1390
    label "zestawienie"
  ]
  node [
    id 1391
    label "jednostka_organizacyjna"
  ]
  node [
    id 1392
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1393
    label "TOPR"
  ]
  node [
    id 1394
    label "endecki"
  ]
  node [
    id 1395
    label "przedstawicielstwo"
  ]
  node [
    id 1396
    label "od&#322;am"
  ]
  node [
    id 1397
    label "Cepelia"
  ]
  node [
    id 1398
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1399
    label "ZBoWiD"
  ]
  node [
    id 1400
    label "organization"
  ]
  node [
    id 1401
    label "centrala"
  ]
  node [
    id 1402
    label "GOPR"
  ]
  node [
    id 1403
    label "ZOMO"
  ]
  node [
    id 1404
    label "ZMP"
  ]
  node [
    id 1405
    label "komitet_koordynacyjny"
  ]
  node [
    id 1406
    label "przybud&#243;wka"
  ]
  node [
    id 1407
    label "boj&#243;wka"
  ]
  node [
    id 1408
    label "militarnie"
  ]
  node [
    id 1409
    label "wojskowo"
  ]
  node [
    id 1410
    label "antybalistyczny"
  ]
  node [
    id 1411
    label "podleg&#322;y"
  ]
  node [
    id 1412
    label "specjalny"
  ]
  node [
    id 1413
    label "intencjonalny"
  ]
  node [
    id 1414
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1415
    label "niedorozw&#243;j"
  ]
  node [
    id 1416
    label "specjalnie"
  ]
  node [
    id 1417
    label "nieetatowy"
  ]
  node [
    id 1418
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1419
    label "nienormalny"
  ]
  node [
    id 1420
    label "umy&#347;lnie"
  ]
  node [
    id 1421
    label "odpowiedni"
  ]
  node [
    id 1422
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 1423
    label "zwyczajny"
  ]
  node [
    id 1424
    label "typowo"
  ]
  node [
    id 1425
    label "cz&#281;sty"
  ]
  node [
    id 1426
    label "zwyk&#322;y"
  ]
  node [
    id 1427
    label "zale&#380;ny"
  ]
  node [
    id 1428
    label "podlegle"
  ]
  node [
    id 1429
    label "militarny"
  ]
  node [
    id 1430
    label "antyrakietowy"
  ]
  node [
    id 1431
    label "stanowisko"
  ]
  node [
    id 1432
    label "position"
  ]
  node [
    id 1433
    label "instytucja"
  ]
  node [
    id 1434
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1435
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1436
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1437
    label "mianowaniec"
  ]
  node [
    id 1438
    label "okienko"
  ]
  node [
    id 1439
    label "prawo"
  ]
  node [
    id 1440
    label "panowanie"
  ]
  node [
    id 1441
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 1442
    label "pogl&#261;d"
  ]
  node [
    id 1443
    label "stawia&#263;"
  ]
  node [
    id 1444
    label "uprawianie"
  ]
  node [
    id 1445
    label "wakowa&#263;"
  ]
  node [
    id 1446
    label "powierzanie"
  ]
  node [
    id 1447
    label "postawi&#263;"
  ]
  node [
    id 1448
    label "tkanka"
  ]
  node [
    id 1449
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1450
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 1451
    label "tw&#243;r"
  ]
  node [
    id 1452
    label "organogeneza"
  ]
  node [
    id 1453
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 1454
    label "uk&#322;ad"
  ]
  node [
    id 1455
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 1456
    label "dekortykacja"
  ]
  node [
    id 1457
    label "Izba_Konsyliarska"
  ]
  node [
    id 1458
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1459
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1460
    label "stomia"
  ]
  node [
    id 1461
    label "okolica"
  ]
  node [
    id 1462
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1463
    label "ekran"
  ]
  node [
    id 1464
    label "interfejs"
  ]
  node [
    id 1465
    label "okno"
  ]
  node [
    id 1466
    label "tabela"
  ]
  node [
    id 1467
    label "poczta"
  ]
  node [
    id 1468
    label "rubryka"
  ]
  node [
    id 1469
    label "ram&#243;wka"
  ]
  node [
    id 1470
    label "czas_wolny"
  ]
  node [
    id 1471
    label "wype&#322;nianie"
  ]
  node [
    id 1472
    label "program"
  ]
  node [
    id 1473
    label "wype&#322;nienie"
  ]
  node [
    id 1474
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 1475
    label "pulpit"
  ]
  node [
    id 1476
    label "menad&#380;er_okien"
  ]
  node [
    id 1477
    label "otw&#243;r"
  ]
  node [
    id 1478
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1479
    label "sfera"
  ]
  node [
    id 1480
    label "zakres"
  ]
  node [
    id 1481
    label "insourcing"
  ]
  node [
    id 1482
    label "column"
  ]
  node [
    id 1483
    label "distribution"
  ]
  node [
    id 1484
    label "stopie&#324;"
  ]
  node [
    id 1485
    label "competence"
  ]
  node [
    id 1486
    label "bezdro&#380;e"
  ]
  node [
    id 1487
    label "poddzia&#322;"
  ]
  node [
    id 1488
    label "osoba_prawna"
  ]
  node [
    id 1489
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1490
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1491
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1492
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1493
    label "biuro"
  ]
  node [
    id 1494
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1495
    label "Fundusze_Unijne"
  ]
  node [
    id 1496
    label "zamyka&#263;"
  ]
  node [
    id 1497
    label "establishment"
  ]
  node [
    id 1498
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1499
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1500
    label "afiliowa&#263;"
  ]
  node [
    id 1501
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1502
    label "standard"
  ]
  node [
    id 1503
    label "zamykanie"
  ]
  node [
    id 1504
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1505
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1506
    label "test_zderzeniowy"
  ]
  node [
    id 1507
    label "porz&#261;dek"
  ]
  node [
    id 1508
    label "katapultowa&#263;"
  ]
  node [
    id 1509
    label "BHP"
  ]
  node [
    id 1510
    label "ubezpieczy&#263;"
  ]
  node [
    id 1511
    label "ubezpieczenie"
  ]
  node [
    id 1512
    label "katapultowanie"
  ]
  node [
    id 1513
    label "stan"
  ]
  node [
    id 1514
    label "ubezpiecza&#263;"
  ]
  node [
    id 1515
    label "safety"
  ]
  node [
    id 1516
    label "ubezpieczanie"
  ]
  node [
    id 1517
    label "relacja"
  ]
  node [
    id 1518
    label "zasada"
  ]
  node [
    id 1519
    label "styl_architektoniczny"
  ]
  node [
    id 1520
    label "normalizacja"
  ]
  node [
    id 1521
    label "Ohio"
  ]
  node [
    id 1522
    label "wci&#281;cie"
  ]
  node [
    id 1523
    label "Nowy_York"
  ]
  node [
    id 1524
    label "warstwa"
  ]
  node [
    id 1525
    label "samopoczucie"
  ]
  node [
    id 1526
    label "Illinois"
  ]
  node [
    id 1527
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1528
    label "state"
  ]
  node [
    id 1529
    label "Jukatan"
  ]
  node [
    id 1530
    label "Kalifornia"
  ]
  node [
    id 1531
    label "Wirginia"
  ]
  node [
    id 1532
    label "wektor"
  ]
  node [
    id 1533
    label "Goa"
  ]
  node [
    id 1534
    label "Teksas"
  ]
  node [
    id 1535
    label "Waszyngton"
  ]
  node [
    id 1536
    label "Massachusetts"
  ]
  node [
    id 1537
    label "Alaska"
  ]
  node [
    id 1538
    label "Arakan"
  ]
  node [
    id 1539
    label "Hawaje"
  ]
  node [
    id 1540
    label "Maryland"
  ]
  node [
    id 1541
    label "Michigan"
  ]
  node [
    id 1542
    label "Arizona"
  ]
  node [
    id 1543
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1544
    label "Georgia"
  ]
  node [
    id 1545
    label "Pensylwania"
  ]
  node [
    id 1546
    label "Luizjana"
  ]
  node [
    id 1547
    label "Nowy_Meksyk"
  ]
  node [
    id 1548
    label "Alabama"
  ]
  node [
    id 1549
    label "ilo&#347;&#263;"
  ]
  node [
    id 1550
    label "Kansas"
  ]
  node [
    id 1551
    label "Oregon"
  ]
  node [
    id 1552
    label "Oklahoma"
  ]
  node [
    id 1553
    label "Floryda"
  ]
  node [
    id 1554
    label "jednostka_administracyjna"
  ]
  node [
    id 1555
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1556
    label "op&#322;ata"
  ]
  node [
    id 1557
    label "ubezpieczalnia"
  ]
  node [
    id 1558
    label "insurance"
  ]
  node [
    id 1559
    label "cover"
  ]
  node [
    id 1560
    label "franszyza"
  ]
  node [
    id 1561
    label "screen"
  ]
  node [
    id 1562
    label "suma_ubezpieczenia"
  ]
  node [
    id 1563
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 1564
    label "uprz&#261;&#380;_wspinaczkowa"
  ]
  node [
    id 1565
    label "zapewnienie"
  ]
  node [
    id 1566
    label "przyznanie"
  ]
  node [
    id 1567
    label "umowa"
  ]
  node [
    id 1568
    label "uchronienie"
  ]
  node [
    id 1569
    label "wyrzuca&#263;"
  ]
  node [
    id 1570
    label "wyrzuci&#263;"
  ]
  node [
    id 1571
    label "wylatywa&#263;"
  ]
  node [
    id 1572
    label "awaria"
  ]
  node [
    id 1573
    label "asekurowanie"
  ]
  node [
    id 1574
    label "chronienie"
  ]
  node [
    id 1575
    label "zabezpieczanie"
  ]
  node [
    id 1576
    label "ubezpieczyciel"
  ]
  node [
    id 1577
    label "zapewnianie"
  ]
  node [
    id 1578
    label "uchroni&#263;"
  ]
  node [
    id 1579
    label "ubezpieczy&#263;_si&#281;"
  ]
  node [
    id 1580
    label "ubezpieczony"
  ]
  node [
    id 1581
    label "zapewni&#263;"
  ]
  node [
    id 1582
    label "chroni&#263;"
  ]
  node [
    id 1583
    label "zapewnia&#263;"
  ]
  node [
    id 1584
    label "asekurowa&#263;"
  ]
  node [
    id 1585
    label "zabezpiecza&#263;"
  ]
  node [
    id 1586
    label "wyrzucenie"
  ]
  node [
    id 1587
    label "wylatywanie"
  ]
  node [
    id 1588
    label "wyrzucanie"
  ]
  node [
    id 1589
    label "katapultowanie_si&#281;"
  ]
  node [
    id 1590
    label "yearbook"
  ]
  node [
    id 1591
    label "czasopismo"
  ]
  node [
    id 1592
    label "kronika"
  ]
  node [
    id 1593
    label "zapis"
  ]
  node [
    id 1594
    label "chronograf"
  ]
  node [
    id 1595
    label "latopis"
  ]
  node [
    id 1596
    label "ksi&#281;ga"
  ]
  node [
    id 1597
    label "psychotest"
  ]
  node [
    id 1598
    label "pismo"
  ]
  node [
    id 1599
    label "communication"
  ]
  node [
    id 1600
    label "wk&#322;ad"
  ]
  node [
    id 1601
    label "zajawka"
  ]
  node [
    id 1602
    label "ok&#322;adka"
  ]
  node [
    id 1603
    label "Zwrotnica"
  ]
  node [
    id 1604
    label "prasa"
  ]
  node [
    id 1605
    label "dysponowanie"
  ]
  node [
    id 1606
    label "dysponowa&#263;"
  ]
  node [
    id 1607
    label "podejrzany"
  ]
  node [
    id 1608
    label "s&#261;downictwo"
  ]
  node [
    id 1609
    label "court"
  ]
  node [
    id 1610
    label "forum"
  ]
  node [
    id 1611
    label "bronienie"
  ]
  node [
    id 1612
    label "wydarzenie"
  ]
  node [
    id 1613
    label "oskar&#380;yciel"
  ]
  node [
    id 1614
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1615
    label "broni&#263;"
  ]
  node [
    id 1616
    label "my&#347;l"
  ]
  node [
    id 1617
    label "pods&#261;dny"
  ]
  node [
    id 1618
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1619
    label "wypowied&#378;"
  ]
  node [
    id 1620
    label "antylogizm"
  ]
  node [
    id 1621
    label "konektyw"
  ]
  node [
    id 1622
    label "&#347;wiadek"
  ]
  node [
    id 1623
    label "procesowicz"
  ]
  node [
    id 1624
    label "dispose"
  ]
  node [
    id 1625
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 1626
    label "namaszczenie_chorych"
  ]
  node [
    id 1627
    label "control"
  ]
  node [
    id 1628
    label "rozporz&#261;dza&#263;"
  ]
  node [
    id 1629
    label "command"
  ]
  node [
    id 1630
    label "ekonomia"
  ]
  node [
    id 1631
    label "dysponowanie_si&#281;"
  ]
  node [
    id 1632
    label "rozdysponowywanie"
  ]
  node [
    id 1633
    label "zarz&#261;dzanie"
  ]
  node [
    id 1634
    label "disposal"
  ]
  node [
    id 1635
    label "rozporz&#261;dzanie"
  ]
  node [
    id 1636
    label "kwota"
  ]
  node [
    id 1637
    label "nemezis"
  ]
  node [
    id 1638
    label "konsekwencja"
  ]
  node [
    id 1639
    label "punishment"
  ]
  node [
    id 1640
    label "klacz"
  ]
  node [
    id 1641
    label "forfeit"
  ]
  node [
    id 1642
    label "roboty_przymusowe"
  ]
  node [
    id 1643
    label "odczuwa&#263;"
  ]
  node [
    id 1644
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 1645
    label "skrupienie_si&#281;"
  ]
  node [
    id 1646
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 1647
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 1648
    label "odczucie"
  ]
  node [
    id 1649
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 1650
    label "koszula_Dejaniry"
  ]
  node [
    id 1651
    label "odczuwanie"
  ]
  node [
    id 1652
    label "event"
  ]
  node [
    id 1653
    label "skrupianie_si&#281;"
  ]
  node [
    id 1654
    label "odczu&#263;"
  ]
  node [
    id 1655
    label "ko&#324;"
  ]
  node [
    id 1656
    label "mare"
  ]
  node [
    id 1657
    label "samica"
  ]
  node [
    id 1658
    label "wynie&#347;&#263;"
  ]
  node [
    id 1659
    label "pieni&#261;dze"
  ]
  node [
    id 1660
    label "limit"
  ]
  node [
    id 1661
    label "wynosi&#263;"
  ]
  node [
    id 1662
    label "defenestracja"
  ]
  node [
    id 1663
    label "agonia"
  ]
  node [
    id 1664
    label "kres"
  ]
  node [
    id 1665
    label "mogi&#322;a"
  ]
  node [
    id 1666
    label "kres_&#380;ycia"
  ]
  node [
    id 1667
    label "upadek"
  ]
  node [
    id 1668
    label "szeol"
  ]
  node [
    id 1669
    label "pogrzebanie"
  ]
  node [
    id 1670
    label "istota_nadprzyrodzona"
  ]
  node [
    id 1671
    label "&#380;a&#322;oba"
  ]
  node [
    id 1672
    label "pogrzeb"
  ]
  node [
    id 1673
    label "zabicie"
  ]
  node [
    id 1674
    label "ostatnie_podrygi"
  ]
  node [
    id 1675
    label "dzia&#322;anie"
  ]
  node [
    id 1676
    label "chwila"
  ]
  node [
    id 1677
    label "gleba"
  ]
  node [
    id 1678
    label "kondycja"
  ]
  node [
    id 1679
    label "pogorszenie"
  ]
  node [
    id 1680
    label "inclination"
  ]
  node [
    id 1681
    label "death"
  ]
  node [
    id 1682
    label "zmierzch"
  ]
  node [
    id 1683
    label "nieuleczalnie_chory"
  ]
  node [
    id 1684
    label "spocz&#261;&#263;"
  ]
  node [
    id 1685
    label "spocz&#281;cie"
  ]
  node [
    id 1686
    label "pochowanie"
  ]
  node [
    id 1687
    label "spoczywa&#263;"
  ]
  node [
    id 1688
    label "chowanie"
  ]
  node [
    id 1689
    label "park_sztywnych"
  ]
  node [
    id 1690
    label "pomnik"
  ]
  node [
    id 1691
    label "nagrobek"
  ]
  node [
    id 1692
    label "prochowisko"
  ]
  node [
    id 1693
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 1694
    label "spoczywanie"
  ]
  node [
    id 1695
    label "za&#347;wiaty"
  ]
  node [
    id 1696
    label "piek&#322;o"
  ]
  node [
    id 1697
    label "judaizm"
  ]
  node [
    id 1698
    label "defenestration"
  ]
  node [
    id 1699
    label "zaj&#347;cie"
  ]
  node [
    id 1700
    label "&#380;al"
  ]
  node [
    id 1701
    label "paznokie&#263;"
  ]
  node [
    id 1702
    label "symbol"
  ]
  node [
    id 1703
    label "kir"
  ]
  node [
    id 1704
    label "brud"
  ]
  node [
    id 1705
    label "burying"
  ]
  node [
    id 1706
    label "zasypanie"
  ]
  node [
    id 1707
    label "zw&#322;oki"
  ]
  node [
    id 1708
    label "burial"
  ]
  node [
    id 1709
    label "w&#322;o&#380;enie"
  ]
  node [
    id 1710
    label "porobienie"
  ]
  node [
    id 1711
    label "gr&#243;b"
  ]
  node [
    id 1712
    label "uniemo&#380;liwienie"
  ]
  node [
    id 1713
    label "destruction"
  ]
  node [
    id 1714
    label "zabrzmienie"
  ]
  node [
    id 1715
    label "skrzywdzenie"
  ]
  node [
    id 1716
    label "pozabijanie"
  ]
  node [
    id 1717
    label "zniszczenie"
  ]
  node [
    id 1718
    label "zaszkodzenie"
  ]
  node [
    id 1719
    label "usuni&#281;cie"
  ]
  node [
    id 1720
    label "killing"
  ]
  node [
    id 1721
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1722
    label "czyn"
  ]
  node [
    id 1723
    label "umarcie"
  ]
  node [
    id 1724
    label "granie"
  ]
  node [
    id 1725
    label "zamkni&#281;cie"
  ]
  node [
    id 1726
    label "compaction"
  ]
  node [
    id 1727
    label "niepowodzenie"
  ]
  node [
    id 1728
    label "stypa"
  ]
  node [
    id 1729
    label "pusta_noc"
  ]
  node [
    id 1730
    label "grabarz"
  ]
  node [
    id 1731
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 1732
    label "obrz&#281;d"
  ]
  node [
    id 1733
    label "raj_utracony"
  ]
  node [
    id 1734
    label "umieranie"
  ]
  node [
    id 1735
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 1736
    label "prze&#380;ywanie"
  ]
  node [
    id 1737
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 1738
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 1739
    label "po&#322;&#243;g"
  ]
  node [
    id 1740
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1741
    label "subsistence"
  ]
  node [
    id 1742
    label "power"
  ]
  node [
    id 1743
    label "okres_noworodkowy"
  ]
  node [
    id 1744
    label "prze&#380;ycie"
  ]
  node [
    id 1745
    label "wiek_matuzalemowy"
  ]
  node [
    id 1746
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 1747
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 1748
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 1749
    label "do&#380;ywanie"
  ]
  node [
    id 1750
    label "dzieci&#324;stwo"
  ]
  node [
    id 1751
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1752
    label "rozw&#243;j"
  ]
  node [
    id 1753
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 1754
    label "menopauza"
  ]
  node [
    id 1755
    label "koleje_losu"
  ]
  node [
    id 1756
    label "bycie"
  ]
  node [
    id 1757
    label "zegar_biologiczny"
  ]
  node [
    id 1758
    label "szwung"
  ]
  node [
    id 1759
    label "przebywanie"
  ]
  node [
    id 1760
    label "warunki"
  ]
  node [
    id 1761
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 1762
    label "niemowl&#281;ctwo"
  ]
  node [
    id 1763
    label "life"
  ]
  node [
    id 1764
    label "staro&#347;&#263;"
  ]
  node [
    id 1765
    label "energy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 423
  ]
  edge [
    source 1
    target 424
  ]
  edge [
    source 1
    target 425
  ]
  edge [
    source 1
    target 426
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 427
  ]
  edge [
    source 1
    target 428
  ]
  edge [
    source 1
    target 429
  ]
  edge [
    source 1
    target 430
  ]
  edge [
    source 1
    target 431
  ]
  edge [
    source 1
    target 432
  ]
  edge [
    source 1
    target 433
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 434
  ]
  edge [
    source 1
    target 435
  ]
  edge [
    source 1
    target 436
  ]
  edge [
    source 1
    target 437
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 433
  ]
  edge [
    source 3
    target 503
  ]
  edge [
    source 3
    target 504
  ]
  edge [
    source 3
    target 505
  ]
  edge [
    source 3
    target 506
  ]
  edge [
    source 3
    target 507
  ]
  edge [
    source 3
    target 508
  ]
  edge [
    source 3
    target 509
  ]
  edge [
    source 3
    target 510
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 511
  ]
  edge [
    source 3
    target 512
  ]
  edge [
    source 3
    target 513
  ]
  edge [
    source 3
    target 514
  ]
  edge [
    source 3
    target 515
  ]
  edge [
    source 3
    target 516
  ]
  edge [
    source 3
    target 517
  ]
  edge [
    source 3
    target 518
  ]
  edge [
    source 3
    target 519
  ]
  edge [
    source 3
    target 520
  ]
  edge [
    source 3
    target 521
  ]
  edge [
    source 3
    target 522
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 523
  ]
  edge [
    source 3
    target 524
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 525
  ]
  edge [
    source 3
    target 526
  ]
  edge [
    source 3
    target 527
  ]
  edge [
    source 3
    target 528
  ]
  edge [
    source 3
    target 529
  ]
  edge [
    source 3
    target 530
  ]
  edge [
    source 3
    target 531
  ]
  edge [
    source 3
    target 532
  ]
  edge [
    source 3
    target 533
  ]
  edge [
    source 3
    target 534
  ]
  edge [
    source 3
    target 535
  ]
  edge [
    source 3
    target 536
  ]
  edge [
    source 3
    target 537
  ]
  edge [
    source 3
    target 538
  ]
  edge [
    source 3
    target 539
  ]
  edge [
    source 3
    target 540
  ]
  edge [
    source 3
    target 541
  ]
  edge [
    source 3
    target 542
  ]
  edge [
    source 3
    target 543
  ]
  edge [
    source 3
    target 544
  ]
  edge [
    source 3
    target 545
  ]
  edge [
    source 3
    target 546
  ]
  edge [
    source 3
    target 547
  ]
  edge [
    source 3
    target 548
  ]
  edge [
    source 3
    target 549
  ]
  edge [
    source 3
    target 550
  ]
  edge [
    source 3
    target 551
  ]
  edge [
    source 3
    target 552
  ]
  edge [
    source 3
    target 553
  ]
  edge [
    source 3
    target 554
  ]
  edge [
    source 3
    target 555
  ]
  edge [
    source 3
    target 556
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 431
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 590
  ]
  edge [
    source 5
    target 591
  ]
  edge [
    source 5
    target 592
  ]
  edge [
    source 5
    target 593
  ]
  edge [
    source 5
    target 594
  ]
  edge [
    source 5
    target 595
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 596
  ]
  edge [
    source 5
    target 597
  ]
  edge [
    source 5
    target 598
  ]
  edge [
    source 5
    target 599
  ]
  edge [
    source 5
    target 600
  ]
  edge [
    source 5
    target 601
  ]
  edge [
    source 5
    target 602
  ]
  edge [
    source 5
    target 603
  ]
  edge [
    source 5
    target 604
  ]
  edge [
    source 5
    target 605
  ]
  edge [
    source 5
    target 606
  ]
  edge [
    source 5
    target 607
  ]
  edge [
    source 5
    target 608
  ]
  edge [
    source 5
    target 609
  ]
  edge [
    source 5
    target 587
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 610
  ]
  edge [
    source 5
    target 611
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 632
  ]
  edge [
    source 7
    target 633
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 635
  ]
  edge [
    source 7
    target 636
  ]
  edge [
    source 7
    target 637
  ]
  edge [
    source 7
    target 638
  ]
  edge [
    source 7
    target 639
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 7
    target 679
  ]
  edge [
    source 7
    target 680
  ]
  edge [
    source 7
    target 681
  ]
  edge [
    source 7
    target 682
  ]
  edge [
    source 7
    target 683
  ]
  edge [
    source 7
    target 684
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 693
  ]
  edge [
    source 7
    target 694
  ]
  edge [
    source 7
    target 695
  ]
  edge [
    source 7
    target 696
  ]
  edge [
    source 7
    target 697
  ]
  edge [
    source 7
    target 698
  ]
  edge [
    source 7
    target 699
  ]
  edge [
    source 7
    target 700
  ]
  edge [
    source 7
    target 701
  ]
  edge [
    source 7
    target 702
  ]
  edge [
    source 7
    target 703
  ]
  edge [
    source 7
    target 704
  ]
  edge [
    source 7
    target 705
  ]
  edge [
    source 7
    target 706
  ]
  edge [
    source 7
    target 707
  ]
  edge [
    source 7
    target 708
  ]
  edge [
    source 7
    target 709
  ]
  edge [
    source 7
    target 710
  ]
  edge [
    source 7
    target 711
  ]
  edge [
    source 7
    target 712
  ]
  edge [
    source 7
    target 713
  ]
  edge [
    source 7
    target 714
  ]
  edge [
    source 7
    target 715
  ]
  edge [
    source 7
    target 716
  ]
  edge [
    source 7
    target 717
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 8
    target 745
  ]
  edge [
    source 8
    target 746
  ]
  edge [
    source 8
    target 747
  ]
  edge [
    source 8
    target 748
  ]
  edge [
    source 8
    target 749
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 751
  ]
  edge [
    source 8
    target 752
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 753
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 754
  ]
  edge [
    source 8
    target 755
  ]
  edge [
    source 8
    target 756
  ]
  edge [
    source 8
    target 757
  ]
  edge [
    source 8
    target 758
  ]
  edge [
    source 8
    target 759
  ]
  edge [
    source 8
    target 760
  ]
  edge [
    source 8
    target 761
  ]
  edge [
    source 8
    target 762
  ]
  edge [
    source 8
    target 763
  ]
  edge [
    source 8
    target 764
  ]
  edge [
    source 8
    target 765
  ]
  edge [
    source 8
    target 766
  ]
  edge [
    source 8
    target 767
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 768
  ]
  edge [
    source 8
    target 769
  ]
  edge [
    source 8
    target 770
  ]
  edge [
    source 8
    target 771
  ]
  edge [
    source 8
    target 772
  ]
  edge [
    source 8
    target 773
  ]
  edge [
    source 8
    target 774
  ]
  edge [
    source 8
    target 775
  ]
  edge [
    source 8
    target 776
  ]
  edge [
    source 8
    target 777
  ]
  edge [
    source 8
    target 778
  ]
  edge [
    source 8
    target 779
  ]
  edge [
    source 8
    target 780
  ]
  edge [
    source 8
    target 781
  ]
  edge [
    source 8
    target 782
  ]
  edge [
    source 8
    target 783
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 784
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 785
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 786
  ]
  edge [
    source 8
    target 787
  ]
  edge [
    source 8
    target 788
  ]
  edge [
    source 8
    target 789
  ]
  edge [
    source 8
    target 790
  ]
  edge [
    source 8
    target 791
  ]
  edge [
    source 8
    target 792
  ]
  edge [
    source 8
    target 690
  ]
  edge [
    source 8
    target 691
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 793
  ]
  edge [
    source 8
    target 794
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 795
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 796
  ]
  edge [
    source 8
    target 797
  ]
  edge [
    source 8
    target 798
  ]
  edge [
    source 8
    target 799
  ]
  edge [
    source 8
    target 800
  ]
  edge [
    source 8
    target 801
  ]
  edge [
    source 8
    target 802
  ]
  edge [
    source 8
    target 803
  ]
  edge [
    source 8
    target 804
  ]
  edge [
    source 8
    target 805
  ]
  edge [
    source 8
    target 806
  ]
  edge [
    source 8
    target 807
  ]
  edge [
    source 8
    target 808
  ]
  edge [
    source 8
    target 809
  ]
  edge [
    source 8
    target 810
  ]
  edge [
    source 8
    target 811
  ]
  edge [
    source 8
    target 812
  ]
  edge [
    source 8
    target 813
  ]
  edge [
    source 8
    target 814
  ]
  edge [
    source 8
    target 815
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 816
  ]
  edge [
    source 8
    target 817
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 818
  ]
  edge [
    source 8
    target 819
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 820
  ]
  edge [
    source 8
    target 821
  ]
  edge [
    source 8
    target 822
  ]
  edge [
    source 8
    target 823
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 824
  ]
  edge [
    source 8
    target 825
  ]
  edge [
    source 8
    target 826
  ]
  edge [
    source 8
    target 827
  ]
  edge [
    source 8
    target 828
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 829
  ]
  edge [
    source 8
    target 830
  ]
  edge [
    source 8
    target 831
  ]
  edge [
    source 8
    target 832
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 833
  ]
  edge [
    source 8
    target 834
  ]
  edge [
    source 8
    target 835
  ]
  edge [
    source 8
    target 836
  ]
  edge [
    source 8
    target 837
  ]
  edge [
    source 8
    target 838
  ]
  edge [
    source 8
    target 839
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 840
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 829
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 794
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 881
  ]
  edge [
    source 11
    target 882
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 883
  ]
  edge [
    source 11
    target 884
  ]
  edge [
    source 11
    target 885
  ]
  edge [
    source 11
    target 886
  ]
  edge [
    source 11
    target 887
  ]
  edge [
    source 11
    target 888
  ]
  edge [
    source 11
    target 889
  ]
  edge [
    source 11
    target 890
  ]
  edge [
    source 11
    target 825
  ]
  edge [
    source 11
    target 891
  ]
  edge [
    source 11
    target 892
  ]
  edge [
    source 11
    target 893
  ]
  edge [
    source 11
    target 894
  ]
  edge [
    source 11
    target 895
  ]
  edge [
    source 11
    target 896
  ]
  edge [
    source 11
    target 897
  ]
  edge [
    source 11
    target 898
  ]
  edge [
    source 11
    target 899
  ]
  edge [
    source 11
    target 900
  ]
  edge [
    source 11
    target 901
  ]
  edge [
    source 11
    target 902
  ]
  edge [
    source 11
    target 903
  ]
  edge [
    source 11
    target 904
  ]
  edge [
    source 11
    target 905
  ]
  edge [
    source 11
    target 906
  ]
  edge [
    source 11
    target 907
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 908
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 909
  ]
  edge [
    source 11
    target 910
  ]
  edge [
    source 11
    target 911
  ]
  edge [
    source 11
    target 912
  ]
  edge [
    source 11
    target 913
  ]
  edge [
    source 11
    target 914
  ]
  edge [
    source 11
    target 915
  ]
  edge [
    source 11
    target 916
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 917
  ]
  edge [
    source 11
    target 918
  ]
  edge [
    source 11
    target 919
  ]
  edge [
    source 11
    target 920
  ]
  edge [
    source 11
    target 921
  ]
  edge [
    source 11
    target 922
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 923
  ]
  edge [
    source 11
    target 924
  ]
  edge [
    source 11
    target 925
  ]
  edge [
    source 11
    target 926
  ]
  edge [
    source 11
    target 927
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 869
  ]
  edge [
    source 12
    target 870
  ]
  edge [
    source 12
    target 928
  ]
  edge [
    source 12
    target 872
  ]
  edge [
    source 12
    target 929
  ]
  edge [
    source 12
    target 930
  ]
  edge [
    source 12
    target 873
  ]
  edge [
    source 12
    target 874
  ]
  edge [
    source 12
    target 931
  ]
  edge [
    source 12
    target 875
  ]
  edge [
    source 12
    target 876
  ]
  edge [
    source 12
    target 877
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 932
  ]
  edge [
    source 12
    target 879
  ]
  edge [
    source 12
    target 880
  ]
  edge [
    source 12
    target 933
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 845
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 934
  ]
  edge [
    source 12
    target 935
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 936
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 937
  ]
  edge [
    source 12
    target 889
  ]
  edge [
    source 12
    target 938
  ]
  edge [
    source 12
    target 890
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 891
  ]
  edge [
    source 12
    target 893
  ]
  edge [
    source 12
    target 895
  ]
  edge [
    source 12
    target 896
  ]
  edge [
    source 12
    target 897
  ]
  edge [
    source 12
    target 939
  ]
  edge [
    source 12
    target 898
  ]
  edge [
    source 12
    target 899
  ]
  edge [
    source 12
    target 900
  ]
  edge [
    source 12
    target 940
  ]
  edge [
    source 12
    target 901
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 902
  ]
  edge [
    source 12
    target 941
  ]
  edge [
    source 12
    target 904
  ]
  edge [
    source 12
    target 942
  ]
  edge [
    source 12
    target 906
  ]
  edge [
    source 12
    target 943
  ]
  edge [
    source 12
    target 944
  ]
  edge [
    source 12
    target 850
  ]
  edge [
    source 12
    target 907
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 945
  ]
  edge [
    source 12
    target 946
  ]
  edge [
    source 12
    target 947
  ]
  edge [
    source 12
    target 948
  ]
  edge [
    source 12
    target 949
  ]
  edge [
    source 12
    target 950
  ]
  edge [
    source 12
    target 951
  ]
  edge [
    source 12
    target 952
  ]
  edge [
    source 12
    target 953
  ]
  edge [
    source 12
    target 954
  ]
  edge [
    source 12
    target 955
  ]
  edge [
    source 12
    target 721
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 956
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 957
  ]
  edge [
    source 12
    target 958
  ]
  edge [
    source 12
    target 959
  ]
  edge [
    source 12
    target 960
  ]
  edge [
    source 12
    target 961
  ]
  edge [
    source 12
    target 962
  ]
  edge [
    source 12
    target 963
  ]
  edge [
    source 12
    target 964
  ]
  edge [
    source 12
    target 965
  ]
  edge [
    source 12
    target 966
  ]
  edge [
    source 12
    target 967
  ]
  edge [
    source 12
    target 968
  ]
  edge [
    source 12
    target 969
  ]
  edge [
    source 12
    target 970
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 971
  ]
  edge [
    source 12
    target 972
  ]
  edge [
    source 12
    target 973
  ]
  edge [
    source 12
    target 974
  ]
  edge [
    source 12
    target 975
  ]
  edge [
    source 12
    target 976
  ]
  edge [
    source 12
    target 977
  ]
  edge [
    source 12
    target 978
  ]
  edge [
    source 12
    target 979
  ]
  edge [
    source 12
    target 980
  ]
  edge [
    source 12
    target 981
  ]
  edge [
    source 12
    target 982
  ]
  edge [
    source 12
    target 983
  ]
  edge [
    source 12
    target 984
  ]
  edge [
    source 12
    target 985
  ]
  edge [
    source 12
    target 986
  ]
  edge [
    source 12
    target 987
  ]
  edge [
    source 12
    target 988
  ]
  edge [
    source 12
    target 598
  ]
  edge [
    source 12
    target 989
  ]
  edge [
    source 12
    target 840
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 990
  ]
  edge [
    source 12
    target 991
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 992
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 993
  ]
  edge [
    source 12
    target 994
  ]
  edge [
    source 12
    target 995
  ]
  edge [
    source 12
    target 996
  ]
  edge [
    source 12
    target 997
  ]
  edge [
    source 12
    target 998
  ]
  edge [
    source 12
    target 999
  ]
  edge [
    source 12
    target 1000
  ]
  edge [
    source 12
    target 1001
  ]
  edge [
    source 12
    target 734
  ]
  edge [
    source 12
    target 1002
  ]
  edge [
    source 12
    target 505
  ]
  edge [
    source 12
    target 1003
  ]
  edge [
    source 12
    target 1004
  ]
  edge [
    source 12
    target 1005
  ]
  edge [
    source 12
    target 1006
  ]
  edge [
    source 12
    target 1007
  ]
  edge [
    source 12
    target 1008
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 1009
  ]
  edge [
    source 12
    target 1010
  ]
  edge [
    source 12
    target 1011
  ]
  edge [
    source 12
    target 1012
  ]
  edge [
    source 12
    target 1013
  ]
  edge [
    source 12
    target 1014
  ]
  edge [
    source 12
    target 1015
  ]
  edge [
    source 12
    target 1016
  ]
  edge [
    source 12
    target 1017
  ]
  edge [
    source 12
    target 1018
  ]
  edge [
    source 12
    target 1019
  ]
  edge [
    source 12
    target 1020
  ]
  edge [
    source 12
    target 1021
  ]
  edge [
    source 12
    target 1022
  ]
  edge [
    source 12
    target 1023
  ]
  edge [
    source 12
    target 1024
  ]
  edge [
    source 12
    target 1025
  ]
  edge [
    source 12
    target 1026
  ]
  edge [
    source 12
    target 1027
  ]
  edge [
    source 12
    target 1028
  ]
  edge [
    source 12
    target 1029
  ]
  edge [
    source 12
    target 1030
  ]
  edge [
    source 12
    target 1031
  ]
  edge [
    source 12
    target 1032
  ]
  edge [
    source 12
    target 1033
  ]
  edge [
    source 12
    target 1034
  ]
  edge [
    source 12
    target 1035
  ]
  edge [
    source 12
    target 1036
  ]
  edge [
    source 12
    target 1037
  ]
  edge [
    source 12
    target 1038
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 12
    target 1052
  ]
  edge [
    source 12
    target 1053
  ]
  edge [
    source 12
    target 1054
  ]
  edge [
    source 12
    target 1055
  ]
  edge [
    source 12
    target 1056
  ]
  edge [
    source 12
    target 1057
  ]
  edge [
    source 12
    target 1058
  ]
  edge [
    source 12
    target 1059
  ]
  edge [
    source 12
    target 1060
  ]
  edge [
    source 12
    target 1061
  ]
  edge [
    source 12
    target 1062
  ]
  edge [
    source 12
    target 1063
  ]
  edge [
    source 12
    target 1064
  ]
  edge [
    source 12
    target 1065
  ]
  edge [
    source 12
    target 1066
  ]
  edge [
    source 12
    target 1067
  ]
  edge [
    source 12
    target 1068
  ]
  edge [
    source 12
    target 1069
  ]
  edge [
    source 12
    target 1070
  ]
  edge [
    source 12
    target 1071
  ]
  edge [
    source 12
    target 1072
  ]
  edge [
    source 12
    target 1073
  ]
  edge [
    source 12
    target 1074
  ]
  edge [
    source 12
    target 1075
  ]
  edge [
    source 12
    target 1076
  ]
  edge [
    source 12
    target 1077
  ]
  edge [
    source 12
    target 1078
  ]
  edge [
    source 12
    target 1079
  ]
  edge [
    source 12
    target 1080
  ]
  edge [
    source 12
    target 1081
  ]
  edge [
    source 12
    target 1082
  ]
  edge [
    source 12
    target 1083
  ]
  edge [
    source 12
    target 741
  ]
  edge [
    source 12
    target 1084
  ]
  edge [
    source 12
    target 1085
  ]
  edge [
    source 12
    target 1086
  ]
  edge [
    source 12
    target 1087
  ]
  edge [
    source 12
    target 1088
  ]
  edge [
    source 12
    target 1089
  ]
  edge [
    source 12
    target 1090
  ]
  edge [
    source 12
    target 1091
  ]
  edge [
    source 12
    target 1092
  ]
  edge [
    source 12
    target 1093
  ]
  edge [
    source 12
    target 1094
  ]
  edge [
    source 12
    target 1095
  ]
  edge [
    source 12
    target 1096
  ]
  edge [
    source 12
    target 1097
  ]
  edge [
    source 12
    target 1098
  ]
  edge [
    source 12
    target 1099
  ]
  edge [
    source 12
    target 1100
  ]
  edge [
    source 12
    target 1101
  ]
  edge [
    source 12
    target 1102
  ]
  edge [
    source 12
    target 553
  ]
  edge [
    source 12
    target 1103
  ]
  edge [
    source 12
    target 1104
  ]
  edge [
    source 12
    target 1105
  ]
  edge [
    source 12
    target 1106
  ]
  edge [
    source 12
    target 1107
  ]
  edge [
    source 12
    target 1108
  ]
  edge [
    source 12
    target 1109
  ]
  edge [
    source 12
    target 1110
  ]
  edge [
    source 12
    target 1111
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 1112
  ]
  edge [
    source 12
    target 1113
  ]
  edge [
    source 12
    target 1114
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 1115
  ]
  edge [
    source 12
    target 1116
  ]
  edge [
    source 12
    target 1117
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 1118
  ]
  edge [
    source 12
    target 1119
  ]
  edge [
    source 12
    target 1120
  ]
  edge [
    source 12
    target 1121
  ]
  edge [
    source 12
    target 1122
  ]
  edge [
    source 12
    target 1123
  ]
  edge [
    source 12
    target 1124
  ]
  edge [
    source 12
    target 1125
  ]
  edge [
    source 12
    target 1126
  ]
  edge [
    source 12
    target 1127
  ]
  edge [
    source 12
    target 1128
  ]
  edge [
    source 12
    target 1129
  ]
  edge [
    source 12
    target 1130
  ]
  edge [
    source 12
    target 1131
  ]
  edge [
    source 12
    target 925
  ]
  edge [
    source 12
    target 926
  ]
  edge [
    source 12
    target 927
  ]
  edge [
    source 12
    target 1132
  ]
  edge [
    source 12
    target 1133
  ]
  edge [
    source 12
    target 1134
  ]
  edge [
    source 12
    target 1135
  ]
  edge [
    source 12
    target 1136
  ]
  edge [
    source 12
    target 1137
  ]
  edge [
    source 12
    target 1138
  ]
  edge [
    source 12
    target 1139
  ]
  edge [
    source 12
    target 1140
  ]
  edge [
    source 12
    target 1141
  ]
  edge [
    source 12
    target 1142
  ]
  edge [
    source 12
    target 1143
  ]
  edge [
    source 12
    target 1144
  ]
  edge [
    source 12
    target 1145
  ]
  edge [
    source 12
    target 1146
  ]
  edge [
    source 12
    target 1147
  ]
  edge [
    source 12
    target 1148
  ]
  edge [
    source 12
    target 1149
  ]
  edge [
    source 12
    target 1150
  ]
  edge [
    source 12
    target 1151
  ]
  edge [
    source 12
    target 1152
  ]
  edge [
    source 12
    target 1153
  ]
  edge [
    source 12
    target 1154
  ]
  edge [
    source 12
    target 1155
  ]
  edge [
    source 12
    target 1156
  ]
  edge [
    source 12
    target 1157
  ]
  edge [
    source 12
    target 1158
  ]
  edge [
    source 12
    target 1159
  ]
  edge [
    source 12
    target 1160
  ]
  edge [
    source 12
    target 1161
  ]
  edge [
    source 12
    target 1162
  ]
  edge [
    source 12
    target 1163
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 1164
  ]
  edge [
    source 12
    target 1165
  ]
  edge [
    source 12
    target 1166
  ]
  edge [
    source 12
    target 1167
  ]
  edge [
    source 12
    target 1168
  ]
  edge [
    source 12
    target 1169
  ]
  edge [
    source 12
    target 1170
  ]
  edge [
    source 12
    target 1171
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 1172
  ]
  edge [
    source 12
    target 1173
  ]
  edge [
    source 12
    target 1174
  ]
  edge [
    source 12
    target 1175
  ]
  edge [
    source 12
    target 910
  ]
  edge [
    source 12
    target 911
  ]
  edge [
    source 12
    target 912
  ]
  edge [
    source 12
    target 913
  ]
  edge [
    source 12
    target 1176
  ]
  edge [
    source 12
    target 1177
  ]
  edge [
    source 12
    target 1178
  ]
  edge [
    source 12
    target 1179
  ]
  edge [
    source 12
    target 1180
  ]
  edge [
    source 12
    target 1181
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 1182
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 1183
  ]
  edge [
    source 12
    target 1184
  ]
  edge [
    source 12
    target 1185
  ]
  edge [
    source 12
    target 1186
  ]
  edge [
    source 12
    target 1187
  ]
  edge [
    source 12
    target 1188
  ]
  edge [
    source 12
    target 1189
  ]
  edge [
    source 12
    target 1190
  ]
  edge [
    source 12
    target 1191
  ]
  edge [
    source 12
    target 1192
  ]
  edge [
    source 12
    target 1193
  ]
  edge [
    source 12
    target 723
  ]
  edge [
    source 12
    target 1194
  ]
  edge [
    source 12
    target 1195
  ]
  edge [
    source 12
    target 1196
  ]
  edge [
    source 12
    target 1197
  ]
  edge [
    source 12
    target 1198
  ]
  edge [
    source 12
    target 1199
  ]
  edge [
    source 12
    target 1200
  ]
  edge [
    source 12
    target 1201
  ]
  edge [
    source 12
    target 1202
  ]
  edge [
    source 12
    target 1203
  ]
  edge [
    source 12
    target 1204
  ]
  edge [
    source 12
    target 1205
  ]
  edge [
    source 12
    target 1206
  ]
  edge [
    source 12
    target 1207
  ]
  edge [
    source 12
    target 1208
  ]
  edge [
    source 12
    target 1209
  ]
  edge [
    source 12
    target 1210
  ]
  edge [
    source 12
    target 1211
  ]
  edge [
    source 12
    target 1212
  ]
  edge [
    source 12
    target 1213
  ]
  edge [
    source 12
    target 1214
  ]
  edge [
    source 12
    target 1215
  ]
  edge [
    source 12
    target 1216
  ]
  edge [
    source 12
    target 731
  ]
  edge [
    source 12
    target 1217
  ]
  edge [
    source 12
    target 733
  ]
  edge [
    source 12
    target 1218
  ]
  edge [
    source 12
    target 1219
  ]
  edge [
    source 12
    target 1220
  ]
  edge [
    source 12
    target 1221
  ]
  edge [
    source 12
    target 710
  ]
  edge [
    source 12
    target 1222
  ]
  edge [
    source 12
    target 1223
  ]
  edge [
    source 12
    target 657
  ]
  edge [
    source 12
    target 1224
  ]
  edge [
    source 12
    target 1225
  ]
  edge [
    source 12
    target 1226
  ]
  edge [
    source 12
    target 1227
  ]
  edge [
    source 12
    target 1228
  ]
  edge [
    source 12
    target 760
  ]
  edge [
    source 12
    target 871
  ]
  edge [
    source 12
    target 878
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 892
  ]
  edge [
    source 12
    target 894
  ]
  edge [
    source 12
    target 903
  ]
  edge [
    source 12
    target 905
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 1229
  ]
  edge [
    source 13
    target 1230
  ]
  edge [
    source 13
    target 1231
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 1232
  ]
  edge [
    source 14
    target 31
  ]
  edge [
    source 14
    target 1233
  ]
  edge [
    source 14
    target 1234
  ]
  edge [
    source 14
    target 1235
  ]
  edge [
    source 14
    target 1236
  ]
  edge [
    source 14
    target 1237
  ]
  edge [
    source 14
    target 1238
  ]
  edge [
    source 14
    target 1239
  ]
  edge [
    source 14
    target 1240
  ]
  edge [
    source 14
    target 1241
  ]
  edge [
    source 14
    target 1242
  ]
  edge [
    source 14
    target 1243
  ]
  edge [
    source 14
    target 1244
  ]
  edge [
    source 14
    target 1245
  ]
  edge [
    source 14
    target 1246
  ]
  edge [
    source 14
    target 1247
  ]
  edge [
    source 14
    target 1248
  ]
  edge [
    source 14
    target 1249
  ]
  edge [
    source 14
    target 1250
  ]
  edge [
    source 14
    target 1251
  ]
  edge [
    source 14
    target 1252
  ]
  edge [
    source 14
    target 1253
  ]
  edge [
    source 14
    target 1254
  ]
  edge [
    source 14
    target 454
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 93
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 95
  ]
  edge [
    source 14
    target 96
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 100
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 1255
  ]
  edge [
    source 14
    target 1256
  ]
  edge [
    source 14
    target 1257
  ]
  edge [
    source 14
    target 1258
  ]
  edge [
    source 14
    target 1259
  ]
  edge [
    source 14
    target 1260
  ]
  edge [
    source 14
    target 1261
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 1262
  ]
  edge [
    source 14
    target 1263
  ]
  edge [
    source 14
    target 1264
  ]
  edge [
    source 14
    target 1265
  ]
  edge [
    source 14
    target 1266
  ]
  edge [
    source 14
    target 1267
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1268
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 1269
  ]
  edge [
    source 15
    target 1270
  ]
  edge [
    source 15
    target 1271
  ]
  edge [
    source 15
    target 1272
  ]
  edge [
    source 15
    target 1273
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 1274
  ]
  edge [
    source 15
    target 38
  ]
  edge [
    source 15
    target 1275
  ]
  edge [
    source 15
    target 1276
  ]
  edge [
    source 15
    target 1277
  ]
  edge [
    source 15
    target 1278
  ]
  edge [
    source 15
    target 1279
  ]
  edge [
    source 15
    target 31
  ]
  edge [
    source 15
    target 1280
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1281
  ]
  edge [
    source 17
    target 1282
  ]
  edge [
    source 17
    target 1193
  ]
  edge [
    source 17
    target 1283
  ]
  edge [
    source 17
    target 1284
  ]
  edge [
    source 17
    target 1285
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 1286
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 1287
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 1288
  ]
  edge [
    source 17
    target 1289
  ]
  edge [
    source 17
    target 1290
  ]
  edge [
    source 17
    target 1291
  ]
  edge [
    source 17
    target 393
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 1292
  ]
  edge [
    source 17
    target 1293
  ]
  edge [
    source 17
    target 1294
  ]
  edge [
    source 17
    target 1295
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 1296
  ]
  edge [
    source 17
    target 1179
  ]
  edge [
    source 17
    target 1297
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 598
  ]
  edge [
    source 17
    target 1298
  ]
  edge [
    source 17
    target 1299
  ]
  edge [
    source 17
    target 1300
  ]
  edge [
    source 17
    target 765
  ]
  edge [
    source 17
    target 1301
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1302
  ]
  edge [
    source 17
    target 1303
  ]
  edge [
    source 17
    target 1304
  ]
  edge [
    source 17
    target 1305
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1306
  ]
  edge [
    source 17
    target 1307
  ]
  edge [
    source 17
    target 1308
  ]
  edge [
    source 17
    target 1309
  ]
  edge [
    source 17
    target 1310
  ]
  edge [
    source 17
    target 1311
  ]
  edge [
    source 17
    target 1312
  ]
  edge [
    source 17
    target 1313
  ]
  edge [
    source 17
    target 1314
  ]
  edge [
    source 17
    target 277
  ]
  edge [
    source 17
    target 1315
  ]
  edge [
    source 17
    target 1316
  ]
  edge [
    source 17
    target 1317
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 1318
  ]
  edge [
    source 18
    target 1319
  ]
  edge [
    source 18
    target 454
  ]
  edge [
    source 18
    target 1320
  ]
  edge [
    source 18
    target 1321
  ]
  edge [
    source 18
    target 1322
  ]
  edge [
    source 18
    target 1323
  ]
  edge [
    source 18
    target 1324
  ]
  edge [
    source 18
    target 446
  ]
  edge [
    source 18
    target 1325
  ]
  edge [
    source 18
    target 479
  ]
  edge [
    source 18
    target 1326
  ]
  edge [
    source 18
    target 1327
  ]
  edge [
    source 18
    target 1328
  ]
  edge [
    source 18
    target 1329
  ]
  edge [
    source 18
    target 1330
  ]
  edge [
    source 18
    target 1331
  ]
  edge [
    source 18
    target 1332
  ]
  edge [
    source 18
    target 1333
  ]
  edge [
    source 18
    target 1334
  ]
  edge [
    source 18
    target 455
  ]
  edge [
    source 18
    target 1335
  ]
  edge [
    source 18
    target 1336
  ]
  edge [
    source 18
    target 1337
  ]
  edge [
    source 18
    target 1338
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1339
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 1340
  ]
  edge [
    source 19
    target 1341
  ]
  edge [
    source 19
    target 1342
  ]
  edge [
    source 19
    target 1343
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1344
  ]
  edge [
    source 19
    target 1345
  ]
  edge [
    source 19
    target 1346
  ]
  edge [
    source 19
    target 1347
  ]
  edge [
    source 19
    target 1348
  ]
  edge [
    source 19
    target 1349
  ]
  edge [
    source 19
    target 1350
  ]
  edge [
    source 19
    target 1351
  ]
  edge [
    source 19
    target 1352
  ]
  edge [
    source 19
    target 1353
  ]
  edge [
    source 19
    target 1354
  ]
  edge [
    source 19
    target 1355
  ]
  edge [
    source 19
    target 1356
  ]
  edge [
    source 19
    target 1357
  ]
  edge [
    source 19
    target 1358
  ]
  edge [
    source 19
    target 1359
  ]
  edge [
    source 19
    target 1360
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1361
  ]
  edge [
    source 19
    target 1362
  ]
  edge [
    source 19
    target 1363
  ]
  edge [
    source 19
    target 1364
  ]
  edge [
    source 19
    target 1365
  ]
  edge [
    source 19
    target 1366
  ]
  edge [
    source 19
    target 1367
  ]
  edge [
    source 19
    target 1368
  ]
  edge [
    source 19
    target 1369
  ]
  edge [
    source 19
    target 1370
  ]
  edge [
    source 19
    target 1371
  ]
  edge [
    source 19
    target 1372
  ]
  edge [
    source 19
    target 1373
  ]
  edge [
    source 19
    target 1374
  ]
  edge [
    source 19
    target 1375
  ]
  edge [
    source 19
    target 1376
  ]
  edge [
    source 19
    target 1377
  ]
  edge [
    source 19
    target 1378
  ]
  edge [
    source 19
    target 1379
  ]
  edge [
    source 19
    target 1380
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 553
  ]
  edge [
    source 19
    target 1381
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1309
  ]
  edge [
    source 19
    target 1382
  ]
  edge [
    source 19
    target 1383
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1384
  ]
  edge [
    source 19
    target 1385
  ]
  edge [
    source 19
    target 562
  ]
  edge [
    source 19
    target 1386
  ]
  edge [
    source 19
    target 1070
  ]
  edge [
    source 19
    target 489
  ]
  edge [
    source 19
    target 1065
  ]
  edge [
    source 19
    target 1387
  ]
  edge [
    source 19
    target 1067
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1388
  ]
  edge [
    source 19
    target 1389
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1390
  ]
  edge [
    source 19
    target 79
  ]
  edge [
    source 19
    target 1391
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 1392
  ]
  edge [
    source 19
    target 1393
  ]
  edge [
    source 19
    target 1394
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 1395
  ]
  edge [
    source 19
    target 1396
  ]
  edge [
    source 19
    target 1397
  ]
  edge [
    source 19
    target 1398
  ]
  edge [
    source 19
    target 1399
  ]
  edge [
    source 19
    target 1400
  ]
  edge [
    source 19
    target 1401
  ]
  edge [
    source 19
    target 1402
  ]
  edge [
    source 19
    target 1403
  ]
  edge [
    source 19
    target 1404
  ]
  edge [
    source 19
    target 1405
  ]
  edge [
    source 19
    target 1406
  ]
  edge [
    source 19
    target 1407
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 20
    target 847
  ]
  edge [
    source 20
    target 849
  ]
  edge [
    source 20
    target 1408
  ]
  edge [
    source 20
    target 144
  ]
  edge [
    source 20
    target 1409
  ]
  edge [
    source 20
    target 846
  ]
  edge [
    source 20
    target 845
  ]
  edge [
    source 20
    target 848
  ]
  edge [
    source 20
    target 851
  ]
  edge [
    source 20
    target 852
  ]
  edge [
    source 20
    target 840
  ]
  edge [
    source 20
    target 842
  ]
  edge [
    source 20
    target 1410
  ]
  edge [
    source 20
    target 844
  ]
  edge [
    source 20
    target 1411
  ]
  edge [
    source 20
    target 1412
  ]
  edge [
    source 20
    target 841
  ]
  edge [
    source 20
    target 829
  ]
  edge [
    source 20
    target 843
  ]
  edge [
    source 20
    target 1329
  ]
  edge [
    source 20
    target 429
  ]
  edge [
    source 20
    target 850
  ]
  edge [
    source 20
    target 1413
  ]
  edge [
    source 20
    target 1414
  ]
  edge [
    source 20
    target 1415
  ]
  edge [
    source 20
    target 457
  ]
  edge [
    source 20
    target 1416
  ]
  edge [
    source 20
    target 1417
  ]
  edge [
    source 20
    target 1418
  ]
  edge [
    source 20
    target 1419
  ]
  edge [
    source 20
    target 1420
  ]
  edge [
    source 20
    target 1421
  ]
  edge [
    source 20
    target 1422
  ]
  edge [
    source 20
    target 110
  ]
  edge [
    source 20
    target 1423
  ]
  edge [
    source 20
    target 1424
  ]
  edge [
    source 20
    target 1425
  ]
  edge [
    source 20
    target 1426
  ]
  edge [
    source 20
    target 1427
  ]
  edge [
    source 20
    target 1428
  ]
  edge [
    source 20
    target 84
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 20
    target 86
  ]
  edge [
    source 20
    target 87
  ]
  edge [
    source 20
    target 88
  ]
  edge [
    source 20
    target 89
  ]
  edge [
    source 20
    target 90
  ]
  edge [
    source 20
    target 91
  ]
  edge [
    source 20
    target 92
  ]
  edge [
    source 20
    target 93
  ]
  edge [
    source 20
    target 94
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 96
  ]
  edge [
    source 20
    target 97
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 20
    target 99
  ]
  edge [
    source 20
    target 100
  ]
  edge [
    source 20
    target 101
  ]
  edge [
    source 20
    target 102
  ]
  edge [
    source 20
    target 103
  ]
  edge [
    source 20
    target 104
  ]
  edge [
    source 20
    target 105
  ]
  edge [
    source 20
    target 106
  ]
  edge [
    source 20
    target 107
  ]
  edge [
    source 20
    target 1429
  ]
  edge [
    source 20
    target 1430
  ]
  edge [
    source 20
    target 924
  ]
  edge [
    source 20
    target 270
  ]
  edge [
    source 20
    target 910
  ]
  edge [
    source 20
    target 911
  ]
  edge [
    source 20
    target 912
  ]
  edge [
    source 20
    target 913
  ]
  edge [
    source 20
    target 919
  ]
  edge [
    source 20
    target 920
  ]
  edge [
    source 20
    target 921
  ]
  edge [
    source 20
    target 922
  ]
  edge [
    source 20
    target 914
  ]
  edge [
    source 20
    target 915
  ]
  edge [
    source 20
    target 916
  ]
  edge [
    source 20
    target 925
  ]
  edge [
    source 20
    target 926
  ]
  edge [
    source 20
    target 927
  ]
  edge [
    source 20
    target 78
  ]
  edge [
    source 20
    target 923
  ]
  edge [
    source 20
    target 909
  ]
  edge [
    source 20
    target 917
  ]
  edge [
    source 20
    target 918
  ]
  edge [
    source 20
    target 869
  ]
  edge [
    source 20
    target 870
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 871
  ]
  edge [
    source 20
    target 872
  ]
  edge [
    source 20
    target 873
  ]
  edge [
    source 20
    target 874
  ]
  edge [
    source 20
    target 875
  ]
  edge [
    source 20
    target 876
  ]
  edge [
    source 20
    target 877
  ]
  edge [
    source 20
    target 878
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 142
  ]
  edge [
    source 20
    target 879
  ]
  edge [
    source 20
    target 880
  ]
  edge [
    source 20
    target 881
  ]
  edge [
    source 20
    target 882
  ]
  edge [
    source 20
    target 533
  ]
  edge [
    source 20
    target 883
  ]
  edge [
    source 20
    target 884
  ]
  edge [
    source 20
    target 885
  ]
  edge [
    source 20
    target 886
  ]
  edge [
    source 20
    target 887
  ]
  edge [
    source 20
    target 888
  ]
  edge [
    source 20
    target 889
  ]
  edge [
    source 20
    target 890
  ]
  edge [
    source 20
    target 825
  ]
  edge [
    source 20
    target 891
  ]
  edge [
    source 20
    target 892
  ]
  edge [
    source 20
    target 893
  ]
  edge [
    source 20
    target 894
  ]
  edge [
    source 20
    target 895
  ]
  edge [
    source 20
    target 896
  ]
  edge [
    source 20
    target 897
  ]
  edge [
    source 20
    target 898
  ]
  edge [
    source 20
    target 899
  ]
  edge [
    source 20
    target 900
  ]
  edge [
    source 20
    target 901
  ]
  edge [
    source 20
    target 902
  ]
  edge [
    source 20
    target 903
  ]
  edge [
    source 20
    target 904
  ]
  edge [
    source 20
    target 905
  ]
  edge [
    source 20
    target 906
  ]
  edge [
    source 20
    target 907
  ]
  edge [
    source 20
    target 853
  ]
  edge [
    source 20
    target 854
  ]
  edge [
    source 20
    target 855
  ]
  edge [
    source 20
    target 856
  ]
  edge [
    source 20
    target 857
  ]
  edge [
    source 20
    target 858
  ]
  edge [
    source 20
    target 859
  ]
  edge [
    source 20
    target 860
  ]
  edge [
    source 20
    target 861
  ]
  edge [
    source 20
    target 862
  ]
  edge [
    source 20
    target 863
  ]
  edge [
    source 20
    target 864
  ]
  edge [
    source 20
    target 865
  ]
  edge [
    source 20
    target 866
  ]
  edge [
    source 20
    target 867
  ]
  edge [
    source 20
    target 868
  ]
  edge [
    source 20
    target 393
  ]
  edge [
    source 20
    target 908
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 31
  ]
  edge [
    source 21
    target 84
  ]
  edge [
    source 21
    target 85
  ]
  edge [
    source 21
    target 86
  ]
  edge [
    source 21
    target 87
  ]
  edge [
    source 21
    target 88
  ]
  edge [
    source 21
    target 89
  ]
  edge [
    source 21
    target 90
  ]
  edge [
    source 21
    target 91
  ]
  edge [
    source 21
    target 92
  ]
  edge [
    source 21
    target 93
  ]
  edge [
    source 21
    target 94
  ]
  edge [
    source 21
    target 95
  ]
  edge [
    source 21
    target 96
  ]
  edge [
    source 21
    target 97
  ]
  edge [
    source 21
    target 98
  ]
  edge [
    source 21
    target 99
  ]
  edge [
    source 21
    target 100
  ]
  edge [
    source 21
    target 101
  ]
  edge [
    source 21
    target 102
  ]
  edge [
    source 21
    target 103
  ]
  edge [
    source 21
    target 104
  ]
  edge [
    source 21
    target 105
  ]
  edge [
    source 21
    target 106
  ]
  edge [
    source 21
    target 107
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1431
  ]
  edge [
    source 23
    target 1432
  ]
  edge [
    source 23
    target 1433
  ]
  edge [
    source 23
    target 505
  ]
  edge [
    source 23
    target 555
  ]
  edge [
    source 23
    target 1434
  ]
  edge [
    source 23
    target 1435
  ]
  edge [
    source 23
    target 1436
  ]
  edge [
    source 23
    target 1437
  ]
  edge [
    source 23
    target 991
  ]
  edge [
    source 23
    target 1438
  ]
  edge [
    source 23
    target 65
  ]
  edge [
    source 23
    target 511
  ]
  edge [
    source 23
    target 512
  ]
  edge [
    source 23
    target 513
  ]
  edge [
    source 23
    target 514
  ]
  edge [
    source 23
    target 515
  ]
  edge [
    source 23
    target 516
  ]
  edge [
    source 23
    target 517
  ]
  edge [
    source 23
    target 518
  ]
  edge [
    source 23
    target 519
  ]
  edge [
    source 23
    target 794
  ]
  edge [
    source 23
    target 1439
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 58
  ]
  edge [
    source 23
    target 1440
  ]
  edge [
    source 23
    target 1441
  ]
  edge [
    source 23
    target 1085
  ]
  edge [
    source 23
    target 393
  ]
  edge [
    source 23
    target 1169
  ]
  edge [
    source 23
    target 1152
  ]
  edge [
    source 23
    target 118
  ]
  edge [
    source 23
    target 1442
  ]
  edge [
    source 23
    target 829
  ]
  edge [
    source 23
    target 1171
  ]
  edge [
    source 23
    target 1443
  ]
  edge [
    source 23
    target 1444
  ]
  edge [
    source 23
    target 1445
  ]
  edge [
    source 23
    target 1446
  ]
  edge [
    source 23
    target 1447
  ]
  edge [
    source 23
    target 1173
  ]
  edge [
    source 23
    target 801
  ]
  edge [
    source 23
    target 1448
  ]
  edge [
    source 23
    target 1391
  ]
  edge [
    source 23
    target 1449
  ]
  edge [
    source 23
    target 1450
  ]
  edge [
    source 23
    target 1451
  ]
  edge [
    source 23
    target 1452
  ]
  edge [
    source 23
    target 807
  ]
  edge [
    source 23
    target 1453
  ]
  edge [
    source 23
    target 974
  ]
  edge [
    source 23
    target 1454
  ]
  edge [
    source 23
    target 1455
  ]
  edge [
    source 23
    target 1456
  ]
  edge [
    source 23
    target 1457
  ]
  edge [
    source 23
    target 1458
  ]
  edge [
    source 23
    target 1459
  ]
  edge [
    source 23
    target 1460
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 1461
  ]
  edge [
    source 23
    target 598
  ]
  edge [
    source 23
    target 1462
  ]
  edge [
    source 23
    target 1463
  ]
  edge [
    source 23
    target 1464
  ]
  edge [
    source 23
    target 1465
  ]
  edge [
    source 23
    target 1466
  ]
  edge [
    source 23
    target 1467
  ]
  edge [
    source 23
    target 1468
  ]
  edge [
    source 23
    target 906
  ]
  edge [
    source 23
    target 1469
  ]
  edge [
    source 23
    target 1470
  ]
  edge [
    source 23
    target 1471
  ]
  edge [
    source 23
    target 1472
  ]
  edge [
    source 23
    target 1473
  ]
  edge [
    source 23
    target 1474
  ]
  edge [
    source 23
    target 1475
  ]
  edge [
    source 23
    target 1476
  ]
  edge [
    source 23
    target 1477
  ]
  edge [
    source 23
    target 1478
  ]
  edge [
    source 23
    target 1479
  ]
  edge [
    source 23
    target 1480
  ]
  edge [
    source 23
    target 1481
  ]
  edge [
    source 23
    target 996
  ]
  edge [
    source 23
    target 700
  ]
  edge [
    source 23
    target 1482
  ]
  edge [
    source 23
    target 1483
  ]
  edge [
    source 23
    target 1484
  ]
  edge [
    source 23
    target 1485
  ]
  edge [
    source 23
    target 1486
  ]
  edge [
    source 23
    target 1487
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 23
    target 430
  ]
  edge [
    source 23
    target 1488
  ]
  edge [
    source 23
    target 1489
  ]
  edge [
    source 23
    target 1490
  ]
  edge [
    source 23
    target 1010
  ]
  edge [
    source 23
    target 1491
  ]
  edge [
    source 23
    target 1492
  ]
  edge [
    source 23
    target 1493
  ]
  edge [
    source 23
    target 225
  ]
  edge [
    source 23
    target 1494
  ]
  edge [
    source 23
    target 1495
  ]
  edge [
    source 23
    target 1496
  ]
  edge [
    source 23
    target 1497
  ]
  edge [
    source 23
    target 1498
  ]
  edge [
    source 23
    target 1499
  ]
  edge [
    source 23
    target 1500
  ]
  edge [
    source 23
    target 1501
  ]
  edge [
    source 23
    target 1502
  ]
  edge [
    source 23
    target 1503
  ]
  edge [
    source 23
    target 1504
  ]
  edge [
    source 23
    target 1505
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1506
  ]
  edge [
    source 24
    target 1507
  ]
  edge [
    source 24
    target 1508
  ]
  edge [
    source 24
    target 1509
  ]
  edge [
    source 24
    target 1510
  ]
  edge [
    source 24
    target 1511
  ]
  edge [
    source 24
    target 1512
  ]
  edge [
    source 24
    target 1513
  ]
  edge [
    source 24
    target 1514
  ]
  edge [
    source 24
    target 1515
  ]
  edge [
    source 24
    target 721
  ]
  edge [
    source 24
    target 1516
  ]
  edge [
    source 24
    target 794
  ]
  edge [
    source 24
    target 1517
  ]
  edge [
    source 24
    target 1454
  ]
  edge [
    source 24
    target 1518
  ]
  edge [
    source 24
    target 1519
  ]
  edge [
    source 24
    target 1520
  ]
  edge [
    source 24
    target 718
  ]
  edge [
    source 24
    target 754
  ]
  edge [
    source 24
    target 755
  ]
  edge [
    source 24
    target 756
  ]
  edge [
    source 24
    target 757
  ]
  edge [
    source 24
    target 758
  ]
  edge [
    source 24
    target 759
  ]
  edge [
    source 24
    target 1521
  ]
  edge [
    source 24
    target 1522
  ]
  edge [
    source 24
    target 1523
  ]
  edge [
    source 24
    target 1524
  ]
  edge [
    source 24
    target 1525
  ]
  edge [
    source 24
    target 1526
  ]
  edge [
    source 24
    target 1527
  ]
  edge [
    source 24
    target 1528
  ]
  edge [
    source 24
    target 1529
  ]
  edge [
    source 24
    target 1530
  ]
  edge [
    source 24
    target 1531
  ]
  edge [
    source 24
    target 1532
  ]
  edge [
    source 24
    target 623
  ]
  edge [
    source 24
    target 1533
  ]
  edge [
    source 24
    target 1534
  ]
  edge [
    source 24
    target 1535
  ]
  edge [
    source 24
    target 517
  ]
  edge [
    source 24
    target 1536
  ]
  edge [
    source 24
    target 1537
  ]
  edge [
    source 24
    target 1538
  ]
  edge [
    source 24
    target 1539
  ]
  edge [
    source 24
    target 1540
  ]
  edge [
    source 24
    target 118
  ]
  edge [
    source 24
    target 1541
  ]
  edge [
    source 24
    target 1542
  ]
  edge [
    source 24
    target 1543
  ]
  edge [
    source 24
    target 1544
  ]
  edge [
    source 24
    target 998
  ]
  edge [
    source 24
    target 1545
  ]
  edge [
    source 24
    target 774
  ]
  edge [
    source 24
    target 1546
  ]
  edge [
    source 24
    target 1547
  ]
  edge [
    source 24
    target 1548
  ]
  edge [
    source 24
    target 1549
  ]
  edge [
    source 24
    target 1550
  ]
  edge [
    source 24
    target 1551
  ]
  edge [
    source 24
    target 1552
  ]
  edge [
    source 24
    target 1553
  ]
  edge [
    source 24
    target 1554
  ]
  edge [
    source 24
    target 1555
  ]
  edge [
    source 24
    target 1556
  ]
  edge [
    source 24
    target 1557
  ]
  edge [
    source 24
    target 1558
  ]
  edge [
    source 24
    target 1559
  ]
  edge [
    source 24
    target 1560
  ]
  edge [
    source 24
    target 1561
  ]
  edge [
    source 24
    target 1562
  ]
  edge [
    source 24
    target 1563
  ]
  edge [
    source 24
    target 1564
  ]
  edge [
    source 24
    target 533
  ]
  edge [
    source 24
    target 1565
  ]
  edge [
    source 24
    target 1566
  ]
  edge [
    source 24
    target 1019
  ]
  edge [
    source 24
    target 1567
  ]
  edge [
    source 24
    target 1568
  ]
  edge [
    source 24
    target 527
  ]
  edge [
    source 24
    target 1569
  ]
  edge [
    source 24
    target 1570
  ]
  edge [
    source 24
    target 1571
  ]
  edge [
    source 24
    target 1572
  ]
  edge [
    source 24
    target 1573
  ]
  edge [
    source 24
    target 1574
  ]
  edge [
    source 24
    target 1575
  ]
  edge [
    source 24
    target 1576
  ]
  edge [
    source 24
    target 1577
  ]
  edge [
    source 24
    target 1578
  ]
  edge [
    source 24
    target 1579
  ]
  edge [
    source 24
    target 693
  ]
  edge [
    source 24
    target 687
  ]
  edge [
    source 24
    target 659
  ]
  edge [
    source 24
    target 1580
  ]
  edge [
    source 24
    target 1581
  ]
  edge [
    source 24
    target 1582
  ]
  edge [
    source 24
    target 1583
  ]
  edge [
    source 24
    target 1584
  ]
  edge [
    source 24
    target 1585
  ]
  edge [
    source 24
    target 1586
  ]
  edge [
    source 24
    target 1587
  ]
  edge [
    source 24
    target 1588
  ]
  edge [
    source 24
    target 1589
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 734
  ]
  edge [
    source 25
    target 1590
  ]
  edge [
    source 25
    target 1591
  ]
  edge [
    source 25
    target 1592
  ]
  edge [
    source 25
    target 803
  ]
  edge [
    source 25
    target 804
  ]
  edge [
    source 25
    target 805
  ]
  edge [
    source 25
    target 806
  ]
  edge [
    source 25
    target 807
  ]
  edge [
    source 25
    target 808
  ]
  edge [
    source 25
    target 809
  ]
  edge [
    source 25
    target 810
  ]
  edge [
    source 25
    target 811
  ]
  edge [
    source 25
    target 812
  ]
  edge [
    source 25
    target 813
  ]
  edge [
    source 25
    target 814
  ]
  edge [
    source 25
    target 815
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 25
    target 816
  ]
  edge [
    source 25
    target 817
  ]
  edge [
    source 25
    target 142
  ]
  edge [
    source 25
    target 818
  ]
  edge [
    source 25
    target 819
  ]
  edge [
    source 25
    target 225
  ]
  edge [
    source 25
    target 820
  ]
  edge [
    source 25
    target 821
  ]
  edge [
    source 25
    target 822
  ]
  edge [
    source 25
    target 823
  ]
  edge [
    source 25
    target 565
  ]
  edge [
    source 25
    target 824
  ]
  edge [
    source 25
    target 825
  ]
  edge [
    source 25
    target 826
  ]
  edge [
    source 25
    target 827
  ]
  edge [
    source 25
    target 828
  ]
  edge [
    source 25
    target 562
  ]
  edge [
    source 25
    target 829
  ]
  edge [
    source 25
    target 830
  ]
  edge [
    source 25
    target 831
  ]
  edge [
    source 25
    target 832
  ]
  edge [
    source 25
    target 833
  ]
  edge [
    source 25
    target 834
  ]
  edge [
    source 25
    target 835
  ]
  edge [
    source 25
    target 836
  ]
  edge [
    source 25
    target 837
  ]
  edge [
    source 25
    target 838
  ]
  edge [
    source 25
    target 1593
  ]
  edge [
    source 25
    target 1594
  ]
  edge [
    source 25
    target 1595
  ]
  edge [
    source 25
    target 1596
  ]
  edge [
    source 25
    target 525
  ]
  edge [
    source 25
    target 1597
  ]
  edge [
    source 25
    target 1598
  ]
  edge [
    source 25
    target 1599
  ]
  edge [
    source 25
    target 1450
  ]
  edge [
    source 25
    target 1600
  ]
  edge [
    source 25
    target 1601
  ]
  edge [
    source 25
    target 1602
  ]
  edge [
    source 25
    target 1603
  ]
  edge [
    source 25
    target 991
  ]
  edge [
    source 25
    target 1604
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1016
  ]
  edge [
    source 26
    target 1605
  ]
  edge [
    source 26
    target 1606
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 26
    target 84
  ]
  edge [
    source 26
    target 85
  ]
  edge [
    source 26
    target 86
  ]
  edge [
    source 26
    target 87
  ]
  edge [
    source 26
    target 88
  ]
  edge [
    source 26
    target 89
  ]
  edge [
    source 26
    target 90
  ]
  edge [
    source 26
    target 91
  ]
  edge [
    source 26
    target 92
  ]
  edge [
    source 26
    target 93
  ]
  edge [
    source 26
    target 94
  ]
  edge [
    source 26
    target 95
  ]
  edge [
    source 26
    target 96
  ]
  edge [
    source 26
    target 97
  ]
  edge [
    source 26
    target 98
  ]
  edge [
    source 26
    target 99
  ]
  edge [
    source 26
    target 100
  ]
  edge [
    source 26
    target 101
  ]
  edge [
    source 26
    target 102
  ]
  edge [
    source 26
    target 103
  ]
  edge [
    source 26
    target 104
  ]
  edge [
    source 26
    target 105
  ]
  edge [
    source 26
    target 106
  ]
  edge [
    source 26
    target 107
  ]
  edge [
    source 26
    target 807
  ]
  edge [
    source 26
    target 1607
  ]
  edge [
    source 26
    target 1608
  ]
  edge [
    source 26
    target 951
  ]
  edge [
    source 26
    target 1493
  ]
  edge [
    source 26
    target 700
  ]
  edge [
    source 26
    target 1609
  ]
  edge [
    source 26
    target 1610
  ]
  edge [
    source 26
    target 1611
  ]
  edge [
    source 26
    target 1612
  ]
  edge [
    source 26
    target 1613
  ]
  edge [
    source 26
    target 1614
  ]
  edge [
    source 26
    target 1021
  ]
  edge [
    source 26
    target 1615
  ]
  edge [
    source 26
    target 1616
  ]
  edge [
    source 26
    target 1617
  ]
  edge [
    source 26
    target 1618
  ]
  edge [
    source 26
    target 902
  ]
  edge [
    source 26
    target 1619
  ]
  edge [
    source 26
    target 1433
  ]
  edge [
    source 26
    target 1620
  ]
  edge [
    source 26
    target 1621
  ]
  edge [
    source 26
    target 1622
  ]
  edge [
    source 26
    target 1623
  ]
  edge [
    source 26
    target 1025
  ]
  edge [
    source 26
    target 1624
  ]
  edge [
    source 26
    target 1625
  ]
  edge [
    source 26
    target 1626
  ]
  edge [
    source 26
    target 1627
  ]
  edge [
    source 26
    target 1628
  ]
  edge [
    source 26
    target 1100
  ]
  edge [
    source 26
    target 1629
  ]
  edge [
    source 26
    target 1630
  ]
  edge [
    source 26
    target 1631
  ]
  edge [
    source 26
    target 1632
  ]
  edge [
    source 26
    target 1633
  ]
  edge [
    source 26
    target 1634
  ]
  edge [
    source 26
    target 1635
  ]
  edge [
    source 26
    target 1144
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 1636
  ]
  edge [
    source 27
    target 1637
  ]
  edge [
    source 27
    target 1638
  ]
  edge [
    source 27
    target 1639
  ]
  edge [
    source 27
    target 1640
  ]
  edge [
    source 27
    target 1641
  ]
  edge [
    source 27
    target 1642
  ]
  edge [
    source 27
    target 1643
  ]
  edge [
    source 27
    target 1644
  ]
  edge [
    source 27
    target 1645
  ]
  edge [
    source 27
    target 1646
  ]
  edge [
    source 27
    target 1647
  ]
  edge [
    source 27
    target 1648
  ]
  edge [
    source 27
    target 1649
  ]
  edge [
    source 27
    target 1650
  ]
  edge [
    source 27
    target 1651
  ]
  edge [
    source 27
    target 1652
  ]
  edge [
    source 27
    target 763
  ]
  edge [
    source 27
    target 1653
  ]
  edge [
    source 27
    target 1654
  ]
  edge [
    source 27
    target 1655
  ]
  edge [
    source 27
    target 1656
  ]
  edge [
    source 27
    target 1657
  ]
  edge [
    source 27
    target 1658
  ]
  edge [
    source 27
    target 1659
  ]
  edge [
    source 27
    target 1549
  ]
  edge [
    source 27
    target 1660
  ]
  edge [
    source 27
    target 1661
  ]
  edge [
    source 28
    target 1662
  ]
  edge [
    source 28
    target 1663
  ]
  edge [
    source 28
    target 1664
  ]
  edge [
    source 28
    target 1665
  ]
  edge [
    source 28
    target 196
  ]
  edge [
    source 28
    target 1666
  ]
  edge [
    source 28
    target 1667
  ]
  edge [
    source 28
    target 1668
  ]
  edge [
    source 28
    target 1669
  ]
  edge [
    source 28
    target 1670
  ]
  edge [
    source 28
    target 1671
  ]
  edge [
    source 28
    target 1672
  ]
  edge [
    source 28
    target 1673
  ]
  edge [
    source 28
    target 1674
  ]
  edge [
    source 28
    target 118
  ]
  edge [
    source 28
    target 1675
  ]
  edge [
    source 28
    target 1676
  ]
  edge [
    source 28
    target 1228
  ]
  edge [
    source 28
    target 1677
  ]
  edge [
    source 28
    target 1678
  ]
  edge [
    source 28
    target 1048
  ]
  edge [
    source 28
    target 1679
  ]
  edge [
    source 28
    target 1680
  ]
  edge [
    source 28
    target 1681
  ]
  edge [
    source 28
    target 1682
  ]
  edge [
    source 28
    target 1513
  ]
  edge [
    source 28
    target 1683
  ]
  edge [
    source 28
    target 1684
  ]
  edge [
    source 28
    target 1685
  ]
  edge [
    source 28
    target 1686
  ]
  edge [
    source 28
    target 1687
  ]
  edge [
    source 28
    target 1688
  ]
  edge [
    source 28
    target 1689
  ]
  edge [
    source 28
    target 1690
  ]
  edge [
    source 28
    target 1691
  ]
  edge [
    source 28
    target 1692
  ]
  edge [
    source 28
    target 1693
  ]
  edge [
    source 28
    target 1694
  ]
  edge [
    source 28
    target 1695
  ]
  edge [
    source 28
    target 1696
  ]
  edge [
    source 28
    target 1697
  ]
  edge [
    source 28
    target 1586
  ]
  edge [
    source 28
    target 1698
  ]
  edge [
    source 28
    target 1699
  ]
  edge [
    source 28
    target 1700
  ]
  edge [
    source 28
    target 1701
  ]
  edge [
    source 28
    target 1702
  ]
  edge [
    source 28
    target 584
  ]
  edge [
    source 28
    target 1703
  ]
  edge [
    source 28
    target 1704
  ]
  edge [
    source 28
    target 1705
  ]
  edge [
    source 28
    target 1706
  ]
  edge [
    source 28
    target 1707
  ]
  edge [
    source 28
    target 1708
  ]
  edge [
    source 28
    target 1709
  ]
  edge [
    source 28
    target 1710
  ]
  edge [
    source 28
    target 1711
  ]
  edge [
    source 28
    target 1712
  ]
  edge [
    source 28
    target 1713
  ]
  edge [
    source 28
    target 1714
  ]
  edge [
    source 28
    target 1715
  ]
  edge [
    source 28
    target 1716
  ]
  edge [
    source 28
    target 1717
  ]
  edge [
    source 28
    target 1718
  ]
  edge [
    source 28
    target 1719
  ]
  edge [
    source 28
    target 553
  ]
  edge [
    source 28
    target 1720
  ]
  edge [
    source 28
    target 1721
  ]
  edge [
    source 28
    target 1722
  ]
  edge [
    source 28
    target 1723
  ]
  edge [
    source 28
    target 1724
  ]
  edge [
    source 28
    target 1725
  ]
  edge [
    source 28
    target 1726
  ]
  edge [
    source 28
    target 1727
  ]
  edge [
    source 28
    target 1728
  ]
  edge [
    source 28
    target 1729
  ]
  edge [
    source 28
    target 1730
  ]
  edge [
    source 28
    target 1731
  ]
  edge [
    source 28
    target 1732
  ]
  edge [
    source 28
    target 1733
  ]
  edge [
    source 28
    target 1734
  ]
  edge [
    source 28
    target 1735
  ]
  edge [
    source 28
    target 1736
  ]
  edge [
    source 28
    target 1737
  ]
  edge [
    source 28
    target 1738
  ]
  edge [
    source 28
    target 1739
  ]
  edge [
    source 28
    target 1740
  ]
  edge [
    source 28
    target 1741
  ]
  edge [
    source 28
    target 1742
  ]
  edge [
    source 28
    target 1743
  ]
  edge [
    source 28
    target 1744
  ]
  edge [
    source 28
    target 1745
  ]
  edge [
    source 28
    target 1746
  ]
  edge [
    source 28
    target 1375
  ]
  edge [
    source 28
    target 1747
  ]
  edge [
    source 28
    target 1748
  ]
  edge [
    source 28
    target 1749
  ]
  edge [
    source 28
    target 741
  ]
  edge [
    source 28
    target 53
  ]
  edge [
    source 28
    target 1750
  ]
  edge [
    source 28
    target 1751
  ]
  edge [
    source 28
    target 1752
  ]
  edge [
    source 28
    target 1753
  ]
  edge [
    source 28
    target 1754
  ]
  edge [
    source 28
    target 1755
  ]
  edge [
    source 28
    target 1756
  ]
  edge [
    source 28
    target 1757
  ]
  edge [
    source 28
    target 1758
  ]
  edge [
    source 28
    target 1759
  ]
  edge [
    source 28
    target 1760
  ]
  edge [
    source 28
    target 1761
  ]
  edge [
    source 28
    target 1762
  ]
  edge [
    source 28
    target 1253
  ]
  edge [
    source 28
    target 1763
  ]
  edge [
    source 28
    target 1764
  ]
  edge [
    source 28
    target 1765
  ]
]
