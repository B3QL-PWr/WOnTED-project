graph [
  node [
    id 0
    label "droga"
    origin "text"
  ]
  node [
    id 1
    label "krajowy"
    origin "text"
  ]
  node [
    id 2
    label "ekskursja"
  ]
  node [
    id 3
    label "bezsilnikowy"
  ]
  node [
    id 4
    label "budowla"
  ]
  node [
    id 5
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 6
    label "trasa"
  ]
  node [
    id 7
    label "podbieg"
  ]
  node [
    id 8
    label "turystyka"
  ]
  node [
    id 9
    label "nawierzchnia"
  ]
  node [
    id 10
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 11
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 12
    label "rajza"
  ]
  node [
    id 13
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 14
    label "korona_drogi"
  ]
  node [
    id 15
    label "passage"
  ]
  node [
    id 16
    label "wylot"
  ]
  node [
    id 17
    label "ekwipunek"
  ]
  node [
    id 18
    label "zbior&#243;wka"
  ]
  node [
    id 19
    label "marszrutyzacja"
  ]
  node [
    id 20
    label "wyb&#243;j"
  ]
  node [
    id 21
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 22
    label "drogowskaz"
  ]
  node [
    id 23
    label "spos&#243;b"
  ]
  node [
    id 24
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "pobocze"
  ]
  node [
    id 26
    label "journey"
  ]
  node [
    id 27
    label "ruch"
  ]
  node [
    id 28
    label "przebieg"
  ]
  node [
    id 29
    label "infrastruktura"
  ]
  node [
    id 30
    label "w&#281;ze&#322;"
  ]
  node [
    id 31
    label "obudowanie"
  ]
  node [
    id 32
    label "obudowywa&#263;"
  ]
  node [
    id 33
    label "zbudowa&#263;"
  ]
  node [
    id 34
    label "obudowa&#263;"
  ]
  node [
    id 35
    label "kolumnada"
  ]
  node [
    id 36
    label "korpus"
  ]
  node [
    id 37
    label "Sukiennice"
  ]
  node [
    id 38
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 39
    label "fundament"
  ]
  node [
    id 40
    label "postanie"
  ]
  node [
    id 41
    label "obudowywanie"
  ]
  node [
    id 42
    label "zbudowanie"
  ]
  node [
    id 43
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 44
    label "stan_surowy"
  ]
  node [
    id 45
    label "konstrukcja"
  ]
  node [
    id 46
    label "rzecz"
  ]
  node [
    id 47
    label "model"
  ]
  node [
    id 48
    label "narz&#281;dzie"
  ]
  node [
    id 49
    label "zbi&#243;r"
  ]
  node [
    id 50
    label "tryb"
  ]
  node [
    id 51
    label "nature"
  ]
  node [
    id 52
    label "ton"
  ]
  node [
    id 53
    label "rozmiar"
  ]
  node [
    id 54
    label "odcinek"
  ]
  node [
    id 55
    label "ambitus"
  ]
  node [
    id 56
    label "czas"
  ]
  node [
    id 57
    label "skala"
  ]
  node [
    id 58
    label "mechanika"
  ]
  node [
    id 59
    label "utrzymywanie"
  ]
  node [
    id 60
    label "move"
  ]
  node [
    id 61
    label "poruszenie"
  ]
  node [
    id 62
    label "movement"
  ]
  node [
    id 63
    label "myk"
  ]
  node [
    id 64
    label "utrzyma&#263;"
  ]
  node [
    id 65
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 66
    label "zjawisko"
  ]
  node [
    id 67
    label "utrzymanie"
  ]
  node [
    id 68
    label "travel"
  ]
  node [
    id 69
    label "kanciasty"
  ]
  node [
    id 70
    label "commercial_enterprise"
  ]
  node [
    id 71
    label "strumie&#324;"
  ]
  node [
    id 72
    label "proces"
  ]
  node [
    id 73
    label "aktywno&#347;&#263;"
  ]
  node [
    id 74
    label "kr&#243;tki"
  ]
  node [
    id 75
    label "taktyka"
  ]
  node [
    id 76
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 77
    label "apraksja"
  ]
  node [
    id 78
    label "natural_process"
  ]
  node [
    id 79
    label "utrzymywa&#263;"
  ]
  node [
    id 80
    label "d&#322;ugi"
  ]
  node [
    id 81
    label "wydarzenie"
  ]
  node [
    id 82
    label "dyssypacja_energii"
  ]
  node [
    id 83
    label "tumult"
  ]
  node [
    id 84
    label "stopek"
  ]
  node [
    id 85
    label "czynno&#347;&#263;"
  ]
  node [
    id 86
    label "zmiana"
  ]
  node [
    id 87
    label "manewr"
  ]
  node [
    id 88
    label "lokomocja"
  ]
  node [
    id 89
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 90
    label "komunikacja"
  ]
  node [
    id 91
    label "drift"
  ]
  node [
    id 92
    label "warstwa"
  ]
  node [
    id 93
    label "pokrycie"
  ]
  node [
    id 94
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 95
    label "fingerpost"
  ]
  node [
    id 96
    label "tablica"
  ]
  node [
    id 97
    label "r&#281;kaw"
  ]
  node [
    id 98
    label "kontusz"
  ]
  node [
    id 99
    label "koniec"
  ]
  node [
    id 100
    label "otw&#243;r"
  ]
  node [
    id 101
    label "przydro&#380;e"
  ]
  node [
    id 102
    label "autostrada"
  ]
  node [
    id 103
    label "operacja"
  ]
  node [
    id 104
    label "bieg"
  ]
  node [
    id 105
    label "podr&#243;&#380;"
  ]
  node [
    id 106
    label "digress"
  ]
  node [
    id 107
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 108
    label "pozostawa&#263;"
  ]
  node [
    id 109
    label "s&#261;dzi&#263;"
  ]
  node [
    id 110
    label "chodzi&#263;"
  ]
  node [
    id 111
    label "b&#322;&#261;ka&#263;_si&#281;"
  ]
  node [
    id 112
    label "stray"
  ]
  node [
    id 113
    label "mieszanie_si&#281;"
  ]
  node [
    id 114
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 115
    label "chodzenie"
  ]
  node [
    id 116
    label "beznap&#281;dowy"
  ]
  node [
    id 117
    label "dormitorium"
  ]
  node [
    id 118
    label "sk&#322;adanka"
  ]
  node [
    id 119
    label "wyprawa"
  ]
  node [
    id 120
    label "polowanie"
  ]
  node [
    id 121
    label "spis"
  ]
  node [
    id 122
    label "pomieszczenie"
  ]
  node [
    id 123
    label "fotografia"
  ]
  node [
    id 124
    label "kocher"
  ]
  node [
    id 125
    label "wyposa&#380;enie"
  ]
  node [
    id 126
    label "nie&#347;miertelnik"
  ]
  node [
    id 127
    label "moderunek"
  ]
  node [
    id 128
    label "cz&#322;owiek"
  ]
  node [
    id 129
    label "ukochanie"
  ]
  node [
    id 130
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 131
    label "feblik"
  ]
  node [
    id 132
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 133
    label "podnieci&#263;"
  ]
  node [
    id 134
    label "numer"
  ]
  node [
    id 135
    label "po&#380;ycie"
  ]
  node [
    id 136
    label "tendency"
  ]
  node [
    id 137
    label "podniecenie"
  ]
  node [
    id 138
    label "afekt"
  ]
  node [
    id 139
    label "zakochanie"
  ]
  node [
    id 140
    label "zajawka"
  ]
  node [
    id 141
    label "seks"
  ]
  node [
    id 142
    label "podniecanie"
  ]
  node [
    id 143
    label "imisja"
  ]
  node [
    id 144
    label "love"
  ]
  node [
    id 145
    label "rozmna&#380;anie"
  ]
  node [
    id 146
    label "ruch_frykcyjny"
  ]
  node [
    id 147
    label "na_pieska"
  ]
  node [
    id 148
    label "serce"
  ]
  node [
    id 149
    label "pozycja_misjonarska"
  ]
  node [
    id 150
    label "wi&#281;&#378;"
  ]
  node [
    id 151
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 152
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 153
    label "z&#322;&#261;czenie"
  ]
  node [
    id 154
    label "gra_wst&#281;pna"
  ]
  node [
    id 155
    label "erotyka"
  ]
  node [
    id 156
    label "emocja"
  ]
  node [
    id 157
    label "baraszki"
  ]
  node [
    id 158
    label "drogi"
  ]
  node [
    id 159
    label "po&#380;&#261;danie"
  ]
  node [
    id 160
    label "wzw&#243;d"
  ]
  node [
    id 161
    label "podnieca&#263;"
  ]
  node [
    id 162
    label "psiapsi&#243;&#322;ka"
  ]
  node [
    id 163
    label "kochanka"
  ]
  node [
    id 164
    label "kultura_fizyczna"
  ]
  node [
    id 165
    label "turyzm"
  ]
  node [
    id 166
    label "rodzimy"
  ]
  node [
    id 167
    label "w&#322;asny"
  ]
  node [
    id 168
    label "tutejszy"
  ]
  node [
    id 169
    label "nr"
  ]
  node [
    id 170
    label "44"
  ]
  node [
    id 171
    label "g&#243;rny"
  ]
  node [
    id 172
    label "&#346;l&#261;sk"
  ]
  node [
    id 173
    label "konurbacja"
  ]
  node [
    id 174
    label "katowicki"
  ]
  node [
    id 175
    label "g&#243;rno&#347;l&#261;ski"
  ]
  node [
    id 176
    label "okr&#261;g"
  ]
  node [
    id 177
    label "przemys&#322;owy"
  ]
  node [
    id 178
    label "kompania"
  ]
  node [
    id 179
    label "piwowarski"
  ]
  node [
    id 180
    label "tyski"
  ]
  node [
    id 181
    label "browar"
  ]
  node [
    id 182
    label "ksi&#261;&#380;&#281;cy"
  ]
  node [
    id 183
    label "fiat"
  ]
  node [
    id 184
    label "auto"
  ]
  node [
    id 185
    label "Poland"
  ]
  node [
    id 186
    label "&#347;l&#261;ski"
  ]
  node [
    id 187
    label "gie&#322;da"
  ]
  node [
    id 188
    label "kwiatowy"
  ]
  node [
    id 189
    label "81"
  ]
  node [
    id 190
    label "28"
  ]
  node [
    id 191
    label "1"
  ]
  node [
    id 192
    label "Bierunia"
  ]
  node [
    id 193
    label "stary"
  ]
  node [
    id 194
    label "Tychy"
  ]
  node [
    id 195
    label "wilkowyj"
  ]
  node [
    id 196
    label "Bieru&#324;"
  ]
  node [
    id 197
    label "nowy"
  ]
  node [
    id 198
    label "bielsko"
  ]
  node [
    id 199
    label "bia&#322;y"
  ]
  node [
    id 200
    label "Urbanowic"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 158
    target 169
  ]
  edge [
    source 158
    target 170
  ]
  edge [
    source 158
    target 189
  ]
  edge [
    source 158
    target 190
  ]
  edge [
    source 158
    target 191
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 169
    target 189
  ]
  edge [
    source 169
    target 190
  ]
  edge [
    source 169
    target 191
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 175
    target 176
  ]
  edge [
    source 175
    target 177
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 180
    target 182
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 183
    target 185
  ]
  edge [
    source 184
    target 185
  ]
  edge [
    source 186
    target 187
  ]
  edge [
    source 186
    target 188
  ]
  edge [
    source 187
    target 188
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 192
    target 197
  ]
  edge [
    source 193
    target 196
  ]
  edge [
    source 194
    target 195
  ]
  edge [
    source 194
    target 200
  ]
  edge [
    source 198
    target 199
  ]
]
