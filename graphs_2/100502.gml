graph [
  node [
    id 0
    label "smard"
    origin "text"
  ]
  node [
    id 1
    label "dolny"
    origin "text"
  ]
  node [
    id 2
    label "wolny_ch&#322;op"
  ]
  node [
    id 3
    label "istota_&#380;ywa"
  ]
  node [
    id 4
    label "cz&#322;owiek"
  ]
  node [
    id 5
    label "ludzko&#347;&#263;"
  ]
  node [
    id 6
    label "asymilowanie"
  ]
  node [
    id 7
    label "wapniak"
  ]
  node [
    id 8
    label "asymilowa&#263;"
  ]
  node [
    id 9
    label "os&#322;abia&#263;"
  ]
  node [
    id 10
    label "posta&#263;"
  ]
  node [
    id 11
    label "hominid"
  ]
  node [
    id 12
    label "podw&#322;adny"
  ]
  node [
    id 13
    label "os&#322;abianie"
  ]
  node [
    id 14
    label "g&#322;owa"
  ]
  node [
    id 15
    label "figura"
  ]
  node [
    id 16
    label "portrecista"
  ]
  node [
    id 17
    label "dwun&#243;g"
  ]
  node [
    id 18
    label "profanum"
  ]
  node [
    id 19
    label "mikrokosmos"
  ]
  node [
    id 20
    label "nasada"
  ]
  node [
    id 21
    label "duch"
  ]
  node [
    id 22
    label "antropochoria"
  ]
  node [
    id 23
    label "osoba"
  ]
  node [
    id 24
    label "wz&#243;r"
  ]
  node [
    id 25
    label "senior"
  ]
  node [
    id 26
    label "oddzia&#322;ywanie"
  ]
  node [
    id 27
    label "Adam"
  ]
  node [
    id 28
    label "homo_sapiens"
  ]
  node [
    id 29
    label "polifag"
  ]
  node [
    id 30
    label "nisko"
  ]
  node [
    id 31
    label "zminimalizowanie"
  ]
  node [
    id 32
    label "minimalnie"
  ]
  node [
    id 33
    label "graniczny"
  ]
  node [
    id 34
    label "minimalizowanie"
  ]
  node [
    id 35
    label "uni&#380;enie"
  ]
  node [
    id 36
    label "pospolicie"
  ]
  node [
    id 37
    label "blisko"
  ]
  node [
    id 38
    label "wstydliwie"
  ]
  node [
    id 39
    label "ma&#322;o"
  ]
  node [
    id 40
    label "vilely"
  ]
  node [
    id 41
    label "despicably"
  ]
  node [
    id 42
    label "niski"
  ]
  node [
    id 43
    label "po&#347;lednio"
  ]
  node [
    id 44
    label "ma&#322;y"
  ]
  node [
    id 45
    label "granicznie"
  ]
  node [
    id 46
    label "minimalny"
  ]
  node [
    id 47
    label "zmniejszanie"
  ]
  node [
    id 48
    label "umniejszanie"
  ]
  node [
    id 49
    label "zmniejszenie"
  ]
  node [
    id 50
    label "umniejszenie"
  ]
  node [
    id 51
    label "skrajny"
  ]
  node [
    id 52
    label "przyleg&#322;y"
  ]
  node [
    id 53
    label "wa&#380;ny"
  ]
  node [
    id 54
    label "ostateczny"
  ]
  node [
    id 55
    label "brama"
  ]
  node [
    id 56
    label "brandenburski"
  ]
  node [
    id 57
    label "Carlo"
  ]
  node [
    id 58
    label "Gotharda"
  ]
  node [
    id 59
    label "Langhansa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 58
    target 59
  ]
]
