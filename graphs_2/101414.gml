graph [
  node [
    id 0
    label "pompa"
    origin "text"
  ]
  node [
    id 1
    label "&#347;mig&#322;owy"
    origin "text"
  ]
  node [
    id 2
    label "podnios&#322;o&#347;&#263;"
  ]
  node [
    id 3
    label "maszyna_hydrauliczna"
  ]
  node [
    id 4
    label "ulewa"
  ]
  node [
    id 5
    label "nurnik"
  ]
  node [
    id 6
    label "smok"
  ]
  node [
    id 7
    label "rozmach"
  ]
  node [
    id 8
    label "przepompownia"
  ]
  node [
    id 9
    label "deszcz"
  ]
  node [
    id 10
    label "precipitation"
  ]
  node [
    id 11
    label "egzaltacja"
  ]
  node [
    id 12
    label "patos"
  ]
  node [
    id 13
    label "cecha"
  ]
  node [
    id 14
    label "atmosfera"
  ]
  node [
    id 15
    label "energia"
  ]
  node [
    id 16
    label "wyobra&#378;nia"
  ]
  node [
    id 17
    label "potw&#243;r"
  ]
  node [
    id 18
    label "sito"
  ]
  node [
    id 19
    label "dragon"
  ]
  node [
    id 20
    label "&#322;apczywiec"
  ]
  node [
    id 21
    label "brzydula"
  ]
  node [
    id 22
    label "po&#380;eracz"
  ]
  node [
    id 23
    label "urwis"
  ]
  node [
    id 24
    label "benzyniak"
  ]
  node [
    id 25
    label "smok_wawelski"
  ]
  node [
    id 26
    label "alki"
  ]
  node [
    id 27
    label "ptak_wodny"
  ]
  node [
    id 28
    label "t&#322;ok"
  ]
  node [
    id 29
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
]
