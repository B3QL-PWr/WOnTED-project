graph [
  node [
    id 0
    label "le&#380;a"
    origin "text"
  ]
  node [
    id 1
    label "pot&#281;&#380;nie"
    origin "text"
  ]
  node [
    id 2
    label "chcie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "rucha&#263;"
    origin "text"
  ]
  node [
    id 5
    label "mama"
    origin "text"
  ]
  node [
    id 6
    label "kac"
    origin "text"
  ]
  node [
    id 7
    label "drewno"
  ]
  node [
    id 8
    label "legowisko"
  ]
  node [
    id 9
    label "screen"
  ]
  node [
    id 10
    label "gniazdo"
  ]
  node [
    id 11
    label "pos&#322;anie"
  ]
  node [
    id 12
    label "ost&#281;p"
  ]
  node [
    id 13
    label "surowiec"
  ]
  node [
    id 14
    label "parzelnia"
  ]
  node [
    id 15
    label "drewniany"
  ]
  node [
    id 16
    label "&#380;ywica"
  ]
  node [
    id 17
    label "trachej"
  ]
  node [
    id 18
    label "aktorzyna"
  ]
  node [
    id 19
    label "ksylofag"
  ]
  node [
    id 20
    label "tkanka_sta&#322;a"
  ]
  node [
    id 21
    label "zacios"
  ]
  node [
    id 22
    label "mi&#281;kisz_drzewny"
  ]
  node [
    id 23
    label "pot&#281;&#380;ny"
  ]
  node [
    id 24
    label "okazale"
  ]
  node [
    id 25
    label "konkretnie"
  ]
  node [
    id 26
    label "olbrzymi"
  ]
  node [
    id 27
    label "ogromnie"
  ]
  node [
    id 28
    label "bardzo"
  ]
  node [
    id 29
    label "intensywnie"
  ]
  node [
    id 30
    label "imponuj&#261;co"
  ]
  node [
    id 31
    label "obficie"
  ]
  node [
    id 32
    label "okaza&#322;y"
  ]
  node [
    id 33
    label "bogato"
  ]
  node [
    id 34
    label "jasno"
  ]
  node [
    id 35
    label "posilnie"
  ]
  node [
    id 36
    label "dok&#322;adnie"
  ]
  node [
    id 37
    label "tre&#347;ciwie"
  ]
  node [
    id 38
    label "po&#380;ywnie"
  ]
  node [
    id 39
    label "konkretny"
  ]
  node [
    id 40
    label "solidny"
  ]
  node [
    id 41
    label "&#322;adnie"
  ]
  node [
    id 42
    label "nie&#378;le"
  ]
  node [
    id 43
    label "ogromny"
  ]
  node [
    id 44
    label "dono&#347;nie"
  ]
  node [
    id 45
    label "intensywny"
  ]
  node [
    id 46
    label "g&#281;sto"
  ]
  node [
    id 47
    label "dynamicznie"
  ]
  node [
    id 48
    label "w_chuj"
  ]
  node [
    id 49
    label "mocny"
  ]
  node [
    id 50
    label "wielow&#322;adny"
  ]
  node [
    id 51
    label "ros&#322;y"
  ]
  node [
    id 52
    label "wielki"
  ]
  node [
    id 53
    label "jebitny"
  ]
  node [
    id 54
    label "olbrzymio"
  ]
  node [
    id 55
    label "czu&#263;"
  ]
  node [
    id 56
    label "desire"
  ]
  node [
    id 57
    label "kcie&#263;"
  ]
  node [
    id 58
    label "postrzega&#263;"
  ]
  node [
    id 59
    label "przewidywa&#263;"
  ]
  node [
    id 60
    label "by&#263;"
  ]
  node [
    id 61
    label "smell"
  ]
  node [
    id 62
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 63
    label "uczuwa&#263;"
  ]
  node [
    id 64
    label "spirit"
  ]
  node [
    id 65
    label "doznawa&#263;"
  ]
  node [
    id 66
    label "anticipate"
  ]
  node [
    id 67
    label "bra&#263;"
  ]
  node [
    id 68
    label "robi&#263;"
  ]
  node [
    id 69
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 70
    label "porywa&#263;"
  ]
  node [
    id 71
    label "korzysta&#263;"
  ]
  node [
    id 72
    label "take"
  ]
  node [
    id 73
    label "wchodzi&#263;"
  ]
  node [
    id 74
    label "poczytywa&#263;"
  ]
  node [
    id 75
    label "levy"
  ]
  node [
    id 76
    label "wk&#322;ada&#263;"
  ]
  node [
    id 77
    label "raise"
  ]
  node [
    id 78
    label "pokonywa&#263;"
  ]
  node [
    id 79
    label "przyjmowa&#263;"
  ]
  node [
    id 80
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 81
    label "prowadzi&#263;"
  ]
  node [
    id 82
    label "za&#380;ywa&#263;"
  ]
  node [
    id 83
    label "get"
  ]
  node [
    id 84
    label "otrzymywa&#263;"
  ]
  node [
    id 85
    label "&#263;pa&#263;"
  ]
  node [
    id 86
    label "interpretowa&#263;"
  ]
  node [
    id 87
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 88
    label "dostawa&#263;"
  ]
  node [
    id 89
    label "rusza&#263;"
  ]
  node [
    id 90
    label "chwyta&#263;"
  ]
  node [
    id 91
    label "grza&#263;"
  ]
  node [
    id 92
    label "wch&#322;ania&#263;"
  ]
  node [
    id 93
    label "wygrywa&#263;"
  ]
  node [
    id 94
    label "u&#380;ywa&#263;"
  ]
  node [
    id 95
    label "ucieka&#263;"
  ]
  node [
    id 96
    label "arise"
  ]
  node [
    id 97
    label "uprawia&#263;_seks"
  ]
  node [
    id 98
    label "abstract"
  ]
  node [
    id 99
    label "towarzystwo"
  ]
  node [
    id 100
    label "atakowa&#263;"
  ]
  node [
    id 101
    label "branie"
  ]
  node [
    id 102
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 103
    label "zalicza&#263;"
  ]
  node [
    id 104
    label "open"
  ]
  node [
    id 105
    label "wzi&#261;&#263;"
  ]
  node [
    id 106
    label "&#322;apa&#263;"
  ]
  node [
    id 107
    label "przewa&#380;a&#263;"
  ]
  node [
    id 108
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 109
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 110
    label "przodkini"
  ]
  node [
    id 111
    label "matka_zast&#281;pcza"
  ]
  node [
    id 112
    label "matczysko"
  ]
  node [
    id 113
    label "rodzice"
  ]
  node [
    id 114
    label "stara"
  ]
  node [
    id 115
    label "macierz"
  ]
  node [
    id 116
    label "rodzic"
  ]
  node [
    id 117
    label "Matka_Boska"
  ]
  node [
    id 118
    label "macocha"
  ]
  node [
    id 119
    label "starzy"
  ]
  node [
    id 120
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 121
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 122
    label "pokolenie"
  ]
  node [
    id 123
    label "wapniaki"
  ]
  node [
    id 124
    label "krewna"
  ]
  node [
    id 125
    label "opiekun"
  ]
  node [
    id 126
    label "wapniak"
  ]
  node [
    id 127
    label "rodzic_chrzestny"
  ]
  node [
    id 128
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 129
    label "matka"
  ]
  node [
    id 130
    label "&#380;ona"
  ]
  node [
    id 131
    label "kobieta"
  ]
  node [
    id 132
    label "partnerka"
  ]
  node [
    id 133
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 134
    label "matuszka"
  ]
  node [
    id 135
    label "parametryzacja"
  ]
  node [
    id 136
    label "pa&#324;stwo"
  ]
  node [
    id 137
    label "poj&#281;cie"
  ]
  node [
    id 138
    label "mod"
  ]
  node [
    id 139
    label "patriota"
  ]
  node [
    id 140
    label "m&#281;&#380;atka"
  ]
  node [
    id 141
    label "aldehyd_octowy"
  ]
  node [
    id 142
    label "doznanie"
  ]
  node [
    id 143
    label "klin"
  ]
  node [
    id 144
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 145
    label "wy&#347;wiadczenie"
  ]
  node [
    id 146
    label "zmys&#322;"
  ]
  node [
    id 147
    label "przeczulica"
  ]
  node [
    id 148
    label "spotkanie"
  ]
  node [
    id 149
    label "czucie"
  ]
  node [
    id 150
    label "poczucie"
  ]
  node [
    id 151
    label "narz&#281;dzie"
  ]
  node [
    id 152
    label "wielo&#347;cian"
  ]
  node [
    id 153
    label "g&#322;&#281;bszy"
  ]
  node [
    id 154
    label "podci&#261;gnik_klinowy"
  ]
  node [
    id 155
    label "wszywka"
  ]
  node [
    id 156
    label "problem"
  ]
  node [
    id 157
    label "szyk"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
]
