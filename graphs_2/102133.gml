graph [
  node [
    id 0
    label "wygrana"
    origin "text"
  ]
  node [
    id 1
    label "loteria"
    origin "text"
  ]
  node [
    id 2
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 3
    label "odmieni&#263;by"
    origin "text"
  ]
  node [
    id 4
    label "nasz"
    origin "text"
  ]
  node [
    id 5
    label "los"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 7
    label "mebel"
    origin "text"
  ]
  node [
    id 8
    label "rozrywka"
    origin "text"
  ]
  node [
    id 9
    label "lub"
    origin "text"
  ]
  node [
    id 10
    label "got&#243;wka"
    origin "text"
  ]
  node [
    id 11
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 12
    label "podnie&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "poziom"
    origin "text"
  ]
  node [
    id 14
    label "odczuwa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "przez"
    origin "text"
  ]
  node [
    id 16
    label "nasa"
    origin "text"
  ]
  node [
    id 17
    label "szcz&#281;&#347;cie"
    origin "text"
  ]
  node [
    id 18
    label "raczej"
    origin "text"
  ]
  node [
    id 19
    label "ale"
    origin "text"
  ]
  node [
    id 20
    label "inny"
    origin "text"
  ]
  node [
    id 21
    label "prze&#380;ycie"
    origin "text"
  ]
  node [
    id 22
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 23
    label "uszcz&#281;&#347;liwi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 25
    label "raport"
    origin "text"
  ]
  node [
    id 26
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "przegl&#261;d"
    origin "text"
  ]
  node [
    id 28
    label "journal"
    origin "text"
  ]
  node [
    id 29
    label "personality"
    origin "text"
  ]
  node [
    id 30
    label "anda"
    origin "text"
  ]
  node [
    id 31
    label "social"
    origin "text"
  ]
  node [
    id 32
    label "psychology"
    origin "text"
  ]
  node [
    id 33
    label "przedmiot"
  ]
  node [
    id 34
    label "korzy&#347;&#263;"
  ]
  node [
    id 35
    label "sukces"
  ]
  node [
    id 36
    label "conquest"
  ]
  node [
    id 37
    label "puchar"
  ]
  node [
    id 38
    label "success"
  ]
  node [
    id 39
    label "passa"
  ]
  node [
    id 40
    label "rezultat"
  ]
  node [
    id 41
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 42
    label "kobieta_sukcesu"
  ]
  node [
    id 43
    label "dobro"
  ]
  node [
    id 44
    label "zaleta"
  ]
  node [
    id 45
    label "discipline"
  ]
  node [
    id 46
    label "zboczy&#263;"
  ]
  node [
    id 47
    label "w&#261;tek"
  ]
  node [
    id 48
    label "kultura"
  ]
  node [
    id 49
    label "entity"
  ]
  node [
    id 50
    label "sponiewiera&#263;"
  ]
  node [
    id 51
    label "zboczenie"
  ]
  node [
    id 52
    label "zbaczanie"
  ]
  node [
    id 53
    label "charakter"
  ]
  node [
    id 54
    label "thing"
  ]
  node [
    id 55
    label "om&#243;wi&#263;"
  ]
  node [
    id 56
    label "tre&#347;&#263;"
  ]
  node [
    id 57
    label "element"
  ]
  node [
    id 58
    label "kr&#261;&#380;enie"
  ]
  node [
    id 59
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 60
    label "istota"
  ]
  node [
    id 61
    label "zbacza&#263;"
  ]
  node [
    id 62
    label "om&#243;wienie"
  ]
  node [
    id 63
    label "rzecz"
  ]
  node [
    id 64
    label "tematyka"
  ]
  node [
    id 65
    label "omawianie"
  ]
  node [
    id 66
    label "omawia&#263;"
  ]
  node [
    id 67
    label "robienie"
  ]
  node [
    id 68
    label "program_nauczania"
  ]
  node [
    id 69
    label "sponiewieranie"
  ]
  node [
    id 70
    label "nagroda"
  ]
  node [
    id 71
    label "zawarto&#347;&#263;"
  ]
  node [
    id 72
    label "naczynie"
  ]
  node [
    id 73
    label "zawody"
  ]
  node [
    id 74
    label "zwyci&#281;stwo"
  ]
  node [
    id 75
    label "gra_losowa"
  ]
  node [
    id 76
    label "realny"
  ]
  node [
    id 77
    label "naprawd&#281;"
  ]
  node [
    id 78
    label "prawdziwy"
  ]
  node [
    id 79
    label "mo&#380;liwy"
  ]
  node [
    id 80
    label "realnie"
  ]
  node [
    id 81
    label "podobny"
  ]
  node [
    id 82
    label "czyj&#347;"
  ]
  node [
    id 83
    label "prywatny"
  ]
  node [
    id 84
    label "bilet"
  ]
  node [
    id 85
    label "destiny"
  ]
  node [
    id 86
    label "przymus"
  ]
  node [
    id 87
    label "si&#322;a"
  ]
  node [
    id 88
    label "rzuci&#263;"
  ]
  node [
    id 89
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 90
    label "&#380;ycie"
  ]
  node [
    id 91
    label "hazard"
  ]
  node [
    id 92
    label "rzucenie"
  ]
  node [
    id 93
    label "przebieg_&#380;ycia"
  ]
  node [
    id 94
    label "cedu&#322;a"
  ]
  node [
    id 95
    label "passe-partout"
  ]
  node [
    id 96
    label "karta_wst&#281;pu"
  ]
  node [
    id 97
    label "konik"
  ]
  node [
    id 98
    label "capacity"
  ]
  node [
    id 99
    label "zdolno&#347;&#263;"
  ]
  node [
    id 100
    label "rozwi&#261;zanie"
  ]
  node [
    id 101
    label "cecha"
  ]
  node [
    id 102
    label "zjawisko"
  ]
  node [
    id 103
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 104
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 105
    label "energia"
  ]
  node [
    id 106
    label "parametr"
  ]
  node [
    id 107
    label "wojsko"
  ]
  node [
    id 108
    label "przemoc"
  ]
  node [
    id 109
    label "mn&#243;stwo"
  ]
  node [
    id 110
    label "moment_si&#322;y"
  ]
  node [
    id 111
    label "wuchta"
  ]
  node [
    id 112
    label "magnitude"
  ]
  node [
    id 113
    label "potencja"
  ]
  node [
    id 114
    label "presja"
  ]
  node [
    id 115
    label "potrzeba"
  ]
  node [
    id 116
    label "okres_noworodkowy"
  ]
  node [
    id 117
    label "umarcie"
  ]
  node [
    id 118
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 119
    label "dzieci&#324;stwo"
  ]
  node [
    id 120
    label "&#347;mier&#263;"
  ]
  node [
    id 121
    label "menopauza"
  ]
  node [
    id 122
    label "warunki"
  ]
  node [
    id 123
    label "do&#380;ywanie"
  ]
  node [
    id 124
    label "power"
  ]
  node [
    id 125
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 126
    label "byt"
  ]
  node [
    id 127
    label "zegar_biologiczny"
  ]
  node [
    id 128
    label "wiek_matuzalemowy"
  ]
  node [
    id 129
    label "koleje_losu"
  ]
  node [
    id 130
    label "life"
  ]
  node [
    id 131
    label "subsistence"
  ]
  node [
    id 132
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 133
    label "umieranie"
  ]
  node [
    id 134
    label "bycie"
  ]
  node [
    id 135
    label "staro&#347;&#263;"
  ]
  node [
    id 136
    label "rozw&#243;j"
  ]
  node [
    id 137
    label "przebywanie"
  ]
  node [
    id 138
    label "niemowl&#281;ctwo"
  ]
  node [
    id 139
    label "raj_utracony"
  ]
  node [
    id 140
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 141
    label "prze&#380;ywanie"
  ]
  node [
    id 142
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 143
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 144
    label "po&#322;&#243;g"
  ]
  node [
    id 145
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 146
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 147
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 148
    label "energy"
  ]
  node [
    id 149
    label "&#380;ywy"
  ]
  node [
    id 150
    label "andropauza"
  ]
  node [
    id 151
    label "czas"
  ]
  node [
    id 152
    label "szwung"
  ]
  node [
    id 153
    label "zmieni&#263;"
  ]
  node [
    id 154
    label "zdecydowa&#263;"
  ]
  node [
    id 155
    label "poruszy&#263;"
  ]
  node [
    id 156
    label "wywo&#322;a&#263;"
  ]
  node [
    id 157
    label "atak"
  ]
  node [
    id 158
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 159
    label "cie&#324;"
  ]
  node [
    id 160
    label "majdn&#261;&#263;"
  ]
  node [
    id 161
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 162
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 163
    label "ruszy&#263;"
  ]
  node [
    id 164
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 165
    label "rush"
  ]
  node [
    id 166
    label "czar"
  ]
  node [
    id 167
    label "skonstruowa&#263;"
  ]
  node [
    id 168
    label "przeznaczenie"
  ]
  node [
    id 169
    label "bewilder"
  ]
  node [
    id 170
    label "towar"
  ]
  node [
    id 171
    label "odej&#347;&#263;"
  ]
  node [
    id 172
    label "podejrzenie"
  ]
  node [
    id 173
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 174
    label "project"
  ]
  node [
    id 175
    label "wyzwanie"
  ]
  node [
    id 176
    label "spowodowa&#263;"
  ]
  node [
    id 177
    label "da&#263;"
  ]
  node [
    id 178
    label "peddle"
  ]
  node [
    id 179
    label "powiedzie&#263;"
  ]
  node [
    id 180
    label "most"
  ]
  node [
    id 181
    label "frame"
  ]
  node [
    id 182
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 183
    label "opu&#347;ci&#263;"
  ]
  node [
    id 184
    label "sygn&#261;&#263;"
  ]
  node [
    id 185
    label "konwulsja"
  ]
  node [
    id 186
    label "&#347;wiat&#322;o"
  ]
  node [
    id 187
    label "wyposa&#380;enie"
  ]
  node [
    id 188
    label "shy"
  ]
  node [
    id 189
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 190
    label "spowodowanie"
  ]
  node [
    id 191
    label "porzucenie"
  ]
  node [
    id 192
    label "odej&#347;cie"
  ]
  node [
    id 193
    label "oddzia&#322;anie"
  ]
  node [
    id 194
    label "powiedzenie"
  ]
  node [
    id 195
    label "skonstruowanie"
  ]
  node [
    id 196
    label "przewr&#243;cenie"
  ]
  node [
    id 197
    label "ruszenie"
  ]
  node [
    id 198
    label "rzucanie"
  ]
  node [
    id 199
    label "czynno&#347;&#263;"
  ]
  node [
    id 200
    label "zrezygnowanie"
  ]
  node [
    id 201
    label "przemieszczenie"
  ]
  node [
    id 202
    label "grzmotni&#281;cie"
  ]
  node [
    id 203
    label "zdecydowanie"
  ]
  node [
    id 204
    label "opuszczenie"
  ]
  node [
    id 205
    label "wywo&#322;anie"
  ]
  node [
    id 206
    label "poruszenie"
  ]
  node [
    id 207
    label "pierdolni&#281;cie"
  ]
  node [
    id 208
    label "poj&#281;cie"
  ]
  node [
    id 209
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 210
    label "wideoloteria"
  ]
  node [
    id 211
    label "prawdopodobie&#324;stwo"
  ]
  node [
    id 212
    label "play"
  ]
  node [
    id 213
    label "mocno"
  ]
  node [
    id 214
    label "du&#380;y"
  ]
  node [
    id 215
    label "cz&#281;sto"
  ]
  node [
    id 216
    label "bardzo"
  ]
  node [
    id 217
    label "wiela"
  ]
  node [
    id 218
    label "wiele"
  ]
  node [
    id 219
    label "znaczny"
  ]
  node [
    id 220
    label "wa&#380;ny"
  ]
  node [
    id 221
    label "niema&#322;o"
  ]
  node [
    id 222
    label "rozwini&#281;ty"
  ]
  node [
    id 223
    label "doros&#322;y"
  ]
  node [
    id 224
    label "dorodny"
  ]
  node [
    id 225
    label "silny"
  ]
  node [
    id 226
    label "stabilnie"
  ]
  node [
    id 227
    label "widocznie"
  ]
  node [
    id 228
    label "przekonuj&#261;co"
  ]
  node [
    id 229
    label "powerfully"
  ]
  node [
    id 230
    label "niepodwa&#380;alnie"
  ]
  node [
    id 231
    label "konkretnie"
  ]
  node [
    id 232
    label "intensywny"
  ]
  node [
    id 233
    label "szczerze"
  ]
  node [
    id 234
    label "strongly"
  ]
  node [
    id 235
    label "mocny"
  ]
  node [
    id 236
    label "silnie"
  ]
  node [
    id 237
    label "w_chuj"
  ]
  node [
    id 238
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 239
    label "cz&#281;sty"
  ]
  node [
    id 240
    label "umeblowanie"
  ]
  node [
    id 241
    label "przeszklenie"
  ]
  node [
    id 242
    label "gzyms"
  ]
  node [
    id 243
    label "nadstawa"
  ]
  node [
    id 244
    label "obudowywanie"
  ]
  node [
    id 245
    label "obudowywa&#263;"
  ]
  node [
    id 246
    label "element_wyposa&#380;enia"
  ]
  node [
    id 247
    label "sprz&#281;t"
  ]
  node [
    id 248
    label "ramiak"
  ]
  node [
    id 249
    label "obudowa&#263;"
  ]
  node [
    id 250
    label "obudowanie"
  ]
  node [
    id 251
    label "wn&#281;trze"
  ]
  node [
    id 252
    label "urz&#261;dzenie"
  ]
  node [
    id 253
    label "wyposa&#380;enie_wn&#281;trz"
  ]
  node [
    id 254
    label "zbi&#243;r"
  ]
  node [
    id 255
    label "sprz&#281;cik"
  ]
  node [
    id 256
    label "equipment"
  ]
  node [
    id 257
    label "furniture"
  ]
  node [
    id 258
    label "kolekcja"
  ]
  node [
    id 259
    label "sprz&#281;cior"
  ]
  node [
    id 260
    label "penis"
  ]
  node [
    id 261
    label "pojazd"
  ]
  node [
    id 262
    label "ochrona"
  ]
  node [
    id 263
    label "listwa"
  ]
  node [
    id 264
    label "wyst&#281;p"
  ]
  node [
    id 265
    label "mur"
  ]
  node [
    id 266
    label "belkowanie"
  ]
  node [
    id 267
    label "karnisz"
  ]
  node [
    id 268
    label "g&#243;ra"
  ]
  node [
    id 269
    label "case"
  ]
  node [
    id 270
    label "smother"
  ]
  node [
    id 271
    label "za&#322;&#261;czy&#263;"
  ]
  node [
    id 272
    label "otoczy&#263;"
  ]
  node [
    id 273
    label "zabezpieczy&#263;"
  ]
  node [
    id 274
    label "budowla"
  ]
  node [
    id 275
    label "zakry&#263;"
  ]
  node [
    id 276
    label "otaczanie"
  ]
  node [
    id 277
    label "zakrywanie"
  ]
  node [
    id 278
    label "za&#322;&#261;czanie"
  ]
  node [
    id 279
    label "zabezpieczanie"
  ]
  node [
    id 280
    label "wyposa&#380;anie"
  ]
  node [
    id 281
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 282
    label "otacza&#263;"
  ]
  node [
    id 283
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 284
    label "zakrywa&#263;"
  ]
  node [
    id 285
    label "zabezpiecza&#263;"
  ]
  node [
    id 286
    label "za&#322;&#261;czenie"
  ]
  node [
    id 287
    label "zabezpieczenie"
  ]
  node [
    id 288
    label "zakrycie"
  ]
  node [
    id 289
    label "otoczenie"
  ]
  node [
    id 290
    label "game"
  ]
  node [
    id 291
    label "odpoczynek"
  ]
  node [
    id 292
    label "czasoumilacz"
  ]
  node [
    id 293
    label "urozmaicenie"
  ]
  node [
    id 294
    label "stan"
  ]
  node [
    id 295
    label "wczas"
  ]
  node [
    id 296
    label "wyraj"
  ]
  node [
    id 297
    label "diversion"
  ]
  node [
    id 298
    label "brz&#281;cz&#261;ca_moneta"
  ]
  node [
    id 299
    label "money"
  ]
  node [
    id 300
    label "pieni&#261;dze"
  ]
  node [
    id 301
    label "kapanie"
  ]
  node [
    id 302
    label "rozej&#347;&#263;_si&#281;"
  ]
  node [
    id 303
    label "rozej&#347;cie_si&#281;"
  ]
  node [
    id 304
    label "kapn&#261;&#263;"
  ]
  node [
    id 305
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 306
    label "portfel"
  ]
  node [
    id 307
    label "sypni&#281;cie_si&#281;"
  ]
  node [
    id 308
    label "forsa"
  ]
  node [
    id 309
    label "kapa&#263;"
  ]
  node [
    id 310
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 311
    label "kwota"
  ]
  node [
    id 312
    label "kapita&#322;"
  ]
  node [
    id 313
    label "kapni&#281;cie"
  ]
  node [
    id 314
    label "wyda&#263;"
  ]
  node [
    id 315
    label "hajs"
  ]
  node [
    id 316
    label "dydki"
  ]
  node [
    id 317
    label "za&#322;apa&#263;"
  ]
  node [
    id 318
    label "pochwali&#263;"
  ]
  node [
    id 319
    label "sorb"
  ]
  node [
    id 320
    label "resume"
  ]
  node [
    id 321
    label "raise"
  ]
  node [
    id 322
    label "surface"
  ]
  node [
    id 323
    label "ulepszy&#263;"
  ]
  node [
    id 324
    label "zacz&#261;&#263;"
  ]
  node [
    id 325
    label "os&#322;awi&#263;"
  ]
  node [
    id 326
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 327
    label "laud"
  ]
  node [
    id 328
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 329
    label "pom&#243;c"
  ]
  node [
    id 330
    label "heft"
  ]
  node [
    id 331
    label "policzy&#263;"
  ]
  node [
    id 332
    label "float"
  ]
  node [
    id 333
    label "przybli&#380;y&#263;"
  ]
  node [
    id 334
    label "allude"
  ]
  node [
    id 335
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 336
    label "better"
  ]
  node [
    id 337
    label "ascend"
  ]
  node [
    id 338
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 339
    label "odbudowa&#263;"
  ]
  node [
    id 340
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 341
    label "heat"
  ]
  node [
    id 342
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 343
    label "zapozna&#263;"
  ]
  node [
    id 344
    label "set_about"
  ]
  node [
    id 345
    label "approach"
  ]
  node [
    id 346
    label "go"
  ]
  node [
    id 347
    label "travel"
  ]
  node [
    id 348
    label "przeznaczy&#263;"
  ]
  node [
    id 349
    label "regenerate"
  ]
  node [
    id 350
    label "wydali&#263;"
  ]
  node [
    id 351
    label "ustawi&#263;"
  ]
  node [
    id 352
    label "set"
  ]
  node [
    id 353
    label "direct"
  ]
  node [
    id 354
    label "przekaza&#263;"
  ]
  node [
    id 355
    label "give"
  ]
  node [
    id 356
    label "return"
  ]
  node [
    id 357
    label "rzygn&#261;&#263;"
  ]
  node [
    id 358
    label "shaftowa&#263;_si&#281;"
  ]
  node [
    id 359
    label "z_powrotem"
  ]
  node [
    id 360
    label "zespawa&#263;_si&#281;"
  ]
  node [
    id 361
    label "modify"
  ]
  node [
    id 362
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 363
    label "zrobi&#263;"
  ]
  node [
    id 364
    label "come_up"
  ]
  node [
    id 365
    label "sprawi&#263;"
  ]
  node [
    id 366
    label "zyska&#263;"
  ]
  node [
    id 367
    label "change"
  ]
  node [
    id 368
    label "straci&#263;"
  ]
  node [
    id 369
    label "zast&#261;pi&#263;"
  ]
  node [
    id 370
    label "przej&#347;&#263;"
  ]
  node [
    id 371
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 372
    label "odj&#261;&#263;"
  ]
  node [
    id 373
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 374
    label "introduce"
  ]
  node [
    id 375
    label "do"
  ]
  node [
    id 376
    label "post&#261;pi&#263;"
  ]
  node [
    id 377
    label "cause"
  ]
  node [
    id 378
    label "begin"
  ]
  node [
    id 379
    label "wyceni&#263;"
  ]
  node [
    id 380
    label "okre&#347;li&#263;"
  ]
  node [
    id 381
    label "zakwalifikowa&#263;"
  ]
  node [
    id 382
    label "wyrachowa&#263;"
  ]
  node [
    id 383
    label "wynagrodzenie"
  ]
  node [
    id 384
    label "wzi&#261;&#263;"
  ]
  node [
    id 385
    label "wyznaczy&#263;"
  ]
  node [
    id 386
    label "charge"
  ]
  node [
    id 387
    label "zaskutkowa&#263;"
  ]
  node [
    id 388
    label "help"
  ]
  node [
    id 389
    label "u&#322;atwi&#263;"
  ]
  node [
    id 390
    label "concur"
  ]
  node [
    id 391
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 392
    label "aid"
  ]
  node [
    id 393
    label "wywy&#380;szy&#263;"
  ]
  node [
    id 394
    label "nagrodzi&#263;"
  ]
  node [
    id 395
    label "praise"
  ]
  node [
    id 396
    label "przywr&#243;ci&#263;"
  ]
  node [
    id 397
    label "rebuild"
  ]
  node [
    id 398
    label "odtworzy&#263;"
  ]
  node [
    id 399
    label "pozyska&#263;"
  ]
  node [
    id 400
    label "zagarn&#261;&#263;"
  ]
  node [
    id 401
    label "zrozumie&#263;"
  ]
  node [
    id 402
    label "z&#322;apa&#263;"
  ]
  node [
    id 403
    label "zarazi&#263;_si&#281;"
  ]
  node [
    id 404
    label "rozpowszechni&#263;"
  ]
  node [
    id 405
    label "wys&#322;awi&#263;"
  ]
  node [
    id 406
    label "wyrz&#261;dzi&#263;"
  ]
  node [
    id 407
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 408
    label "szczebel"
  ]
  node [
    id 409
    label "punkt_widzenia"
  ]
  node [
    id 410
    label "budynek"
  ]
  node [
    id 411
    label "jako&#347;&#263;"
  ]
  node [
    id 412
    label "p&#322;aszczyzna"
  ]
  node [
    id 413
    label "po&#322;o&#380;enie"
  ]
  node [
    id 414
    label "ranga"
  ]
  node [
    id 415
    label "wyk&#322;adnik"
  ]
  node [
    id 416
    label "kierunek"
  ]
  node [
    id 417
    label "wysoko&#347;&#263;"
  ]
  node [
    id 418
    label "faza"
  ]
  node [
    id 419
    label "p&#322;aszczak"
  ]
  node [
    id 420
    label "zakres"
  ]
  node [
    id 421
    label "&#347;ciana"
  ]
  node [
    id 422
    label "cia&#322;o"
  ]
  node [
    id 423
    label "ukszta&#322;towanie"
  ]
  node [
    id 424
    label "degree"
  ]
  node [
    id 425
    label "wymiar"
  ]
  node [
    id 426
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 427
    label "powierzchnia"
  ]
  node [
    id 428
    label "kwadrant"
  ]
  node [
    id 429
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 430
    label "wielko&#347;&#263;"
  ]
  node [
    id 431
    label "altitude"
  ]
  node [
    id 432
    label "brzmienie"
  ]
  node [
    id 433
    label "odcinek"
  ]
  node [
    id 434
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 435
    label "rozmiar"
  ]
  node [
    id 436
    label "tallness"
  ]
  node [
    id 437
    label "sum"
  ]
  node [
    id 438
    label "k&#261;t"
  ]
  node [
    id 439
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 440
    label "reading"
  ]
  node [
    id 441
    label "trim"
  ]
  node [
    id 442
    label "pora&#380;ka"
  ]
  node [
    id 443
    label "zbudowanie"
  ]
  node [
    id 444
    label "ugoszczenie"
  ]
  node [
    id 445
    label "sytuacja"
  ]
  node [
    id 446
    label "pouk&#322;adanie"
  ]
  node [
    id 447
    label "miejsce"
  ]
  node [
    id 448
    label "ustawienie"
  ]
  node [
    id 449
    label "le&#380;e&#263;"
  ]
  node [
    id 450
    label "wygranie"
  ]
  node [
    id 451
    label "le&#380;enie"
  ]
  node [
    id 452
    label "presentation"
  ]
  node [
    id 453
    label "przenocowanie"
  ]
  node [
    id 454
    label "adres"
  ]
  node [
    id 455
    label "zepsucie"
  ]
  node [
    id 456
    label "zabicie"
  ]
  node [
    id 457
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 458
    label "umieszczenie"
  ]
  node [
    id 459
    label "pokrycie"
  ]
  node [
    id 460
    label "nak&#322;adzenie"
  ]
  node [
    id 461
    label "liczba"
  ]
  node [
    id 462
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 463
    label "exponent"
  ]
  node [
    id 464
    label "wska&#378;nik"
  ]
  node [
    id 465
    label "pot&#281;ga"
  ]
  node [
    id 466
    label "quality"
  ]
  node [
    id 467
    label "co&#347;"
  ]
  node [
    id 468
    label "state"
  ]
  node [
    id 469
    label "warto&#347;&#263;"
  ]
  node [
    id 470
    label "syf"
  ]
  node [
    id 471
    label "stopie&#324;"
  ]
  node [
    id 472
    label "drabina"
  ]
  node [
    id 473
    label "gradation"
  ]
  node [
    id 474
    label "linia"
  ]
  node [
    id 475
    label "przebieg"
  ]
  node [
    id 476
    label "orientowa&#263;"
  ]
  node [
    id 477
    label "zorientowa&#263;"
  ]
  node [
    id 478
    label "praktyka"
  ]
  node [
    id 479
    label "skr&#281;cenie"
  ]
  node [
    id 480
    label "skr&#281;ci&#263;"
  ]
  node [
    id 481
    label "przeorientowanie"
  ]
  node [
    id 482
    label "orientowanie"
  ]
  node [
    id 483
    label "zorientowanie"
  ]
  node [
    id 484
    label "ty&#322;"
  ]
  node [
    id 485
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 486
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 487
    label "przeorientowywanie"
  ]
  node [
    id 488
    label "bok"
  ]
  node [
    id 489
    label "ideologia"
  ]
  node [
    id 490
    label "skr&#281;canie"
  ]
  node [
    id 491
    label "orientacja"
  ]
  node [
    id 492
    label "metoda"
  ]
  node [
    id 493
    label "studia"
  ]
  node [
    id 494
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 495
    label "przeorientowa&#263;"
  ]
  node [
    id 496
    label "bearing"
  ]
  node [
    id 497
    label "spos&#243;b"
  ]
  node [
    id 498
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 499
    label "prz&#243;d"
  ]
  node [
    id 500
    label "skr&#281;ca&#263;"
  ]
  node [
    id 501
    label "system"
  ]
  node [
    id 502
    label "przeorientowywa&#263;"
  ]
  node [
    id 503
    label "komutowanie"
  ]
  node [
    id 504
    label "dw&#243;jnik"
  ]
  node [
    id 505
    label "przerywacz"
  ]
  node [
    id 506
    label "przew&#243;d"
  ]
  node [
    id 507
    label "obsesja"
  ]
  node [
    id 508
    label "nastr&#243;j"
  ]
  node [
    id 509
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 510
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 511
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 512
    label "cykl_astronomiczny"
  ]
  node [
    id 513
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 514
    label "coil"
  ]
  node [
    id 515
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 516
    label "stan_skupienia"
  ]
  node [
    id 517
    label "komutowa&#263;"
  ]
  node [
    id 518
    label "obw&#243;d"
  ]
  node [
    id 519
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 520
    label "fotoelement"
  ]
  node [
    id 521
    label "okres"
  ]
  node [
    id 522
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 523
    label "kraw&#281;d&#378;"
  ]
  node [
    id 524
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 525
    label "Rzym_Zachodni"
  ]
  node [
    id 526
    label "Rzym_Wschodni"
  ]
  node [
    id 527
    label "ilo&#347;&#263;"
  ]
  node [
    id 528
    label "whole"
  ]
  node [
    id 529
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 530
    label "stopie&#324;_s&#322;u&#380;bowy"
  ]
  node [
    id 531
    label "status"
  ]
  node [
    id 532
    label "numer"
  ]
  node [
    id 533
    label "kondygnacja"
  ]
  node [
    id 534
    label "skrzyd&#322;o"
  ]
  node [
    id 535
    label "klatka_schodowa"
  ]
  node [
    id 536
    label "front"
  ]
  node [
    id 537
    label "przedpro&#380;e"
  ]
  node [
    id 538
    label "dach"
  ]
  node [
    id 539
    label "alkierz"
  ]
  node [
    id 540
    label "strop"
  ]
  node [
    id 541
    label "pod&#322;oga"
  ]
  node [
    id 542
    label "Pentagon"
  ]
  node [
    id 543
    label "balkon"
  ]
  node [
    id 544
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 545
    label "konsekwencja"
  ]
  node [
    id 546
    label "uczuwa&#263;"
  ]
  node [
    id 547
    label "notice"
  ]
  node [
    id 548
    label "smell"
  ]
  node [
    id 549
    label "doznawa&#263;"
  ]
  node [
    id 550
    label "postrzega&#263;"
  ]
  node [
    id 551
    label "widzie&#263;"
  ]
  node [
    id 552
    label "feel"
  ]
  node [
    id 553
    label "hurt"
  ]
  node [
    id 554
    label "dochodzi&#263;"
  ]
  node [
    id 555
    label "perceive"
  ]
  node [
    id 556
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 557
    label "doj&#347;&#263;"
  ]
  node [
    id 558
    label "obacza&#263;"
  ]
  node [
    id 559
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 560
    label "os&#261;dza&#263;"
  ]
  node [
    id 561
    label "ogl&#261;da&#263;"
  ]
  node [
    id 562
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 563
    label "zmale&#263;"
  ]
  node [
    id 564
    label "male&#263;"
  ]
  node [
    id 565
    label "go_steady"
  ]
  node [
    id 566
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 567
    label "dostrzega&#263;"
  ]
  node [
    id 568
    label "aprobowa&#263;"
  ]
  node [
    id 569
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 570
    label "spotka&#263;"
  ]
  node [
    id 571
    label "reagowa&#263;"
  ]
  node [
    id 572
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 573
    label "wzrok"
  ]
  node [
    id 574
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 575
    label "skrupienie_si&#281;"
  ]
  node [
    id 576
    label "odczu&#263;"
  ]
  node [
    id 577
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 578
    label "odczucie"
  ]
  node [
    id 579
    label "skrupianie_si&#281;"
  ]
  node [
    id 580
    label "event"
  ]
  node [
    id 581
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 582
    label "koszula_Dejaniry"
  ]
  node [
    id 583
    label "odczuwanie"
  ]
  node [
    id 584
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 585
    label "czu&#263;"
  ]
  node [
    id 586
    label "przypadek"
  ]
  node [
    id 587
    label "wra&#380;enie"
  ]
  node [
    id 588
    label "dobrodziejstwo"
  ]
  node [
    id 589
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 590
    label "happening"
  ]
  node [
    id 591
    label "pacjent"
  ]
  node [
    id 592
    label "wydarzenie"
  ]
  node [
    id 593
    label "przyk&#322;ad"
  ]
  node [
    id 594
    label "kategoria_gramatyczna"
  ]
  node [
    id 595
    label "schorzenie"
  ]
  node [
    id 596
    label "krzywa_Engla"
  ]
  node [
    id 597
    label "kalokagatia"
  ]
  node [
    id 598
    label "dobra"
  ]
  node [
    id 599
    label "dobro&#263;"
  ]
  node [
    id 600
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 601
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 602
    label "cel"
  ]
  node [
    id 603
    label "go&#322;&#261;bek"
  ]
  node [
    id 604
    label "despond"
  ]
  node [
    id 605
    label "g&#322;agolica"
  ]
  node [
    id 606
    label "litera"
  ]
  node [
    id 607
    label "wybranie"
  ]
  node [
    id 608
    label "p&#243;j&#347;cie"
  ]
  node [
    id 609
    label "przydzielenie"
  ]
  node [
    id 610
    label "oblat"
  ]
  node [
    id 611
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 612
    label "obowi&#261;zek"
  ]
  node [
    id 613
    label "zrobienie"
  ]
  node [
    id 614
    label "ustalenie"
  ]
  node [
    id 615
    label "przeczulica"
  ]
  node [
    id 616
    label "czucie"
  ]
  node [
    id 617
    label "proces"
  ]
  node [
    id 618
    label "zmys&#322;"
  ]
  node [
    id 619
    label "poczucie"
  ]
  node [
    id 620
    label "odczucia"
  ]
  node [
    id 621
    label "reakcja"
  ]
  node [
    id 622
    label "czyn"
  ]
  node [
    id 623
    label "bogactwo"
  ]
  node [
    id 624
    label "benevolence"
  ]
  node [
    id 625
    label "dobroczynno&#347;&#263;"
  ]
  node [
    id 626
    label "piwo"
  ]
  node [
    id 627
    label "warzy&#263;"
  ]
  node [
    id 628
    label "wyj&#347;cie"
  ]
  node [
    id 629
    label "browarnia"
  ]
  node [
    id 630
    label "nawarzenie"
  ]
  node [
    id 631
    label "uwarzy&#263;"
  ]
  node [
    id 632
    label "uwarzenie"
  ]
  node [
    id 633
    label "bacik"
  ]
  node [
    id 634
    label "warzenie"
  ]
  node [
    id 635
    label "alkohol"
  ]
  node [
    id 636
    label "birofilia"
  ]
  node [
    id 637
    label "nap&#243;j"
  ]
  node [
    id 638
    label "nawarzy&#263;"
  ]
  node [
    id 639
    label "anta&#322;"
  ]
  node [
    id 640
    label "inszy"
  ]
  node [
    id 641
    label "inaczej"
  ]
  node [
    id 642
    label "osobno"
  ]
  node [
    id 643
    label "r&#243;&#380;ny"
  ]
  node [
    id 644
    label "kolejny"
  ]
  node [
    id 645
    label "odr&#281;bny"
  ]
  node [
    id 646
    label "nast&#281;pnie"
  ]
  node [
    id 647
    label "kolejno"
  ]
  node [
    id 648
    label "kt&#243;ry&#347;"
  ]
  node [
    id 649
    label "nastopny"
  ]
  node [
    id 650
    label "jaki&#347;"
  ]
  node [
    id 651
    label "r&#243;&#380;nie"
  ]
  node [
    id 652
    label "niestandardowo"
  ]
  node [
    id 653
    label "osobny"
  ]
  node [
    id 654
    label "osobnie"
  ]
  node [
    id 655
    label "odr&#281;bnie"
  ]
  node [
    id 656
    label "udzielnie"
  ]
  node [
    id 657
    label "individually"
  ]
  node [
    id 658
    label "doznanie"
  ]
  node [
    id 659
    label "poradzenie_sobie"
  ]
  node [
    id 660
    label "survival"
  ]
  node [
    id 661
    label "przetrwanie"
  ]
  node [
    id 662
    label "przej&#347;cie"
  ]
  node [
    id 663
    label "spotkanie"
  ]
  node [
    id 664
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 665
    label "wy&#347;wiadczenie"
  ]
  node [
    id 666
    label "pozostanie"
  ]
  node [
    id 667
    label "experience"
  ]
  node [
    id 668
    label "ustawa"
  ]
  node [
    id 669
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 670
    label "przewy&#380;szenie"
  ]
  node [
    id 671
    label "przemokni&#281;cie"
  ]
  node [
    id 672
    label "wydeptywanie"
  ]
  node [
    id 673
    label "offense"
  ]
  node [
    id 674
    label "traversal"
  ]
  node [
    id 675
    label "trwanie"
  ]
  node [
    id 676
    label "przepojenie"
  ]
  node [
    id 677
    label "przedostanie_si&#281;"
  ]
  node [
    id 678
    label "mini&#281;cie"
  ]
  node [
    id 679
    label "przestanie"
  ]
  node [
    id 680
    label "stanie_si&#281;"
  ]
  node [
    id 681
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 682
    label "nas&#261;czenie"
  ]
  node [
    id 683
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 684
    label "przebycie"
  ]
  node [
    id 685
    label "wymienienie"
  ]
  node [
    id 686
    label "nasycenie_si&#281;"
  ]
  node [
    id 687
    label "strain"
  ]
  node [
    id 688
    label "wytyczenie"
  ]
  node [
    id 689
    label "przerobienie"
  ]
  node [
    id 690
    label "zdarzenie_si&#281;"
  ]
  node [
    id 691
    label "uznanie"
  ]
  node [
    id 692
    label "przepuszczenie"
  ]
  node [
    id 693
    label "dostanie_si&#281;"
  ]
  node [
    id 694
    label "nale&#380;enie"
  ]
  node [
    id 695
    label "odmienienie"
  ]
  node [
    id 696
    label "wydeptanie"
  ]
  node [
    id 697
    label "mienie"
  ]
  node [
    id 698
    label "zaliczenie"
  ]
  node [
    id 699
    label "wstawka"
  ]
  node [
    id 700
    label "crack"
  ]
  node [
    id 701
    label "zacz&#281;cie"
  ]
  node [
    id 702
    label "szkolenie"
  ]
  node [
    id 703
    label "sport"
  ]
  node [
    id 704
    label "by&#263;"
  ]
  node [
    id 705
    label "uprawi&#263;"
  ]
  node [
    id 706
    label "gotowy"
  ]
  node [
    id 707
    label "might"
  ]
  node [
    id 708
    label "pole"
  ]
  node [
    id 709
    label "public_treasury"
  ]
  node [
    id 710
    label "obrobi&#263;"
  ]
  node [
    id 711
    label "nietrze&#378;wy"
  ]
  node [
    id 712
    label "gotowo"
  ]
  node [
    id 713
    label "przygotowywanie"
  ]
  node [
    id 714
    label "dyspozycyjny"
  ]
  node [
    id 715
    label "przygotowanie"
  ]
  node [
    id 716
    label "bliski"
  ]
  node [
    id 717
    label "martwy"
  ]
  node [
    id 718
    label "zalany"
  ]
  node [
    id 719
    label "doj&#347;cie"
  ]
  node [
    id 720
    label "nieuchronny"
  ]
  node [
    id 721
    label "czekanie"
  ]
  node [
    id 722
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 723
    label "stand"
  ]
  node [
    id 724
    label "trwa&#263;"
  ]
  node [
    id 725
    label "equal"
  ]
  node [
    id 726
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 727
    label "chodzi&#263;"
  ]
  node [
    id 728
    label "uczestniczy&#263;"
  ]
  node [
    id 729
    label "obecno&#347;&#263;"
  ]
  node [
    id 730
    label "si&#281;ga&#263;"
  ]
  node [
    id 731
    label "mie&#263;_miejsce"
  ]
  node [
    id 732
    label "wzbudzi&#263;"
  ]
  node [
    id 733
    label "arouse"
  ]
  node [
    id 734
    label "rise"
  ]
  node [
    id 735
    label "appear"
  ]
  node [
    id 736
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 737
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 738
    label "relacja"
  ]
  node [
    id 739
    label "statement"
  ]
  node [
    id 740
    label "raport_Beveridge'a"
  ]
  node [
    id 741
    label "raport_Fischlera"
  ]
  node [
    id 742
    label "zwi&#261;zanie"
  ]
  node [
    id 743
    label "ustosunkowywa&#263;"
  ]
  node [
    id 744
    label "podzbi&#243;r"
  ]
  node [
    id 745
    label "zwi&#261;zek"
  ]
  node [
    id 746
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 747
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 748
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 749
    label "marriage"
  ]
  node [
    id 750
    label "bratnia_dusza"
  ]
  node [
    id 751
    label "ustosunkowywanie"
  ]
  node [
    id 752
    label "zwi&#261;za&#263;"
  ]
  node [
    id 753
    label "sprawko"
  ]
  node [
    id 754
    label "korespondent"
  ]
  node [
    id 755
    label "wi&#261;zanie"
  ]
  node [
    id 756
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 757
    label "message"
  ]
  node [
    id 758
    label "trasa"
  ]
  node [
    id 759
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 760
    label "ustosunkowanie"
  ]
  node [
    id 761
    label "ustosunkowa&#263;"
  ]
  node [
    id 762
    label "wypowied&#378;"
  ]
  node [
    id 763
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 764
    label "wprowadzi&#263;"
  ]
  node [
    id 765
    label "wydawnictwo"
  ]
  node [
    id 766
    label "upubliczni&#263;"
  ]
  node [
    id 767
    label "picture"
  ]
  node [
    id 768
    label "testify"
  ]
  node [
    id 769
    label "wej&#347;&#263;"
  ]
  node [
    id 770
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 771
    label "doprowadzi&#263;"
  ]
  node [
    id 772
    label "rynek"
  ]
  node [
    id 773
    label "zej&#347;&#263;"
  ]
  node [
    id 774
    label "wpisa&#263;"
  ]
  node [
    id 775
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 776
    label "insert"
  ]
  node [
    id 777
    label "umie&#347;ci&#263;"
  ]
  node [
    id 778
    label "indicate"
  ]
  node [
    id 779
    label "udost&#281;pni&#263;"
  ]
  node [
    id 780
    label "redakcja"
  ]
  node [
    id 781
    label "druk"
  ]
  node [
    id 782
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 783
    label "poster"
  ]
  node [
    id 784
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 785
    label "publikacja"
  ]
  node [
    id 786
    label "redaktor"
  ]
  node [
    id 787
    label "szata_graficzna"
  ]
  node [
    id 788
    label "debit"
  ]
  node [
    id 789
    label "wydawa&#263;"
  ]
  node [
    id 790
    label "firma"
  ]
  node [
    id 791
    label "kontrola"
  ]
  node [
    id 792
    label "inspection"
  ]
  node [
    id 793
    label "impreza"
  ]
  node [
    id 794
    label "party"
  ]
  node [
    id 795
    label "impra"
  ]
  node [
    id 796
    label "przyj&#281;cie"
  ]
  node [
    id 797
    label "okazja"
  ]
  node [
    id 798
    label "perlustracja"
  ]
  node [
    id 799
    label "legalizacja_ponowna"
  ]
  node [
    id 800
    label "legalizacja_pierwotna"
  ]
  node [
    id 801
    label "examination"
  ]
  node [
    id 802
    label "w&#322;adza"
  ]
  node [
    id 803
    label "instytucja"
  ]
  node [
    id 804
    label "Journal"
  ]
  node [
    id 805
    label "of"
  ]
  node [
    id 806
    label "Personality"
  ]
  node [
    id 807
    label "Anda"
  ]
  node [
    id 808
    label "Social"
  ]
  node [
    id 809
    label "Psychology"
  ]
  node [
    id 810
    label "Leaf"
  ]
  node [
    id 811
    label "van"
  ]
  node [
    id 812
    label "Boven"
  ]
  node [
    id 813
    label "Thomas"
  ]
  node [
    id 814
    label "Gilovich"
  ]
  node [
    id 815
    label "uniwersytet"
  ]
  node [
    id 816
    label "Colorado"
  ]
  node [
    id 817
    label "wyspa"
  ]
  node [
    id 818
    label "Boulder"
  ]
  node [
    id 819
    label "Cornell"
  ]
  node [
    id 820
    label "Mount"
  ]
  node [
    id 821
    label "Everest"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 335
  ]
  edge [
    source 12
    target 336
  ]
  edge [
    source 12
    target 337
  ]
  edge [
    source 12
    target 338
  ]
  edge [
    source 12
    target 339
  ]
  edge [
    source 12
    target 340
  ]
  edge [
    source 12
    target 341
  ]
  edge [
    source 12
    target 342
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 344
  ]
  edge [
    source 12
    target 345
  ]
  edge [
    source 12
    target 346
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 347
  ]
  edge [
    source 12
    target 348
  ]
  edge [
    source 12
    target 349
  ]
  edge [
    source 12
    target 350
  ]
  edge [
    source 12
    target 351
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 12
    target 361
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 364
  ]
  edge [
    source 12
    target 365
  ]
  edge [
    source 12
    target 366
  ]
  edge [
    source 12
    target 367
  ]
  edge [
    source 12
    target 368
  ]
  edge [
    source 12
    target 369
  ]
  edge [
    source 12
    target 370
  ]
  edge [
    source 12
    target 371
  ]
  edge [
    source 12
    target 372
  ]
  edge [
    source 12
    target 373
  ]
  edge [
    source 12
    target 374
  ]
  edge [
    source 12
    target 375
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 382
  ]
  edge [
    source 12
    target 383
  ]
  edge [
    source 12
    target 384
  ]
  edge [
    source 12
    target 385
  ]
  edge [
    source 12
    target 386
  ]
  edge [
    source 12
    target 387
  ]
  edge [
    source 12
    target 388
  ]
  edge [
    source 12
    target 389
  ]
  edge [
    source 12
    target 390
  ]
  edge [
    source 12
    target 391
  ]
  edge [
    source 12
    target 392
  ]
  edge [
    source 12
    target 393
  ]
  edge [
    source 12
    target 394
  ]
  edge [
    source 12
    target 395
  ]
  edge [
    source 12
    target 396
  ]
  edge [
    source 12
    target 397
  ]
  edge [
    source 12
    target 398
  ]
  edge [
    source 12
    target 399
  ]
  edge [
    source 12
    target 400
  ]
  edge [
    source 12
    target 401
  ]
  edge [
    source 12
    target 402
  ]
  edge [
    source 12
    target 403
  ]
  edge [
    source 12
    target 404
  ]
  edge [
    source 12
    target 405
  ]
  edge [
    source 12
    target 406
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 57
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 274
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 409
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 14
    target 577
  ]
  edge [
    source 14
    target 40
  ]
  edge [
    source 14
    target 578
  ]
  edge [
    source 14
    target 579
  ]
  edge [
    source 14
    target 580
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 582
  ]
  edge [
    source 14
    target 583
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 585
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 17
    target 168
  ]
  edge [
    source 17
    target 586
  ]
  edge [
    source 17
    target 587
  ]
  edge [
    source 17
    target 43
  ]
  edge [
    source 17
    target 588
  ]
  edge [
    source 17
    target 589
  ]
  edge [
    source 17
    target 590
  ]
  edge [
    source 17
    target 591
  ]
  edge [
    source 17
    target 592
  ]
  edge [
    source 17
    target 593
  ]
  edge [
    source 17
    target 594
  ]
  edge [
    source 17
    target 595
  ]
  edge [
    source 17
    target 63
  ]
  edge [
    source 17
    target 596
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 598
  ]
  edge [
    source 17
    target 599
  ]
  edge [
    source 17
    target 600
  ]
  edge [
    source 17
    target 601
  ]
  edge [
    source 17
    target 602
  ]
  edge [
    source 17
    target 603
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 604
  ]
  edge [
    source 17
    target 605
  ]
  edge [
    source 17
    target 606
  ]
  edge [
    source 17
    target 85
  ]
  edge [
    source 17
    target 86
  ]
  edge [
    source 17
    target 607
  ]
  edge [
    source 17
    target 88
  ]
  edge [
    source 17
    target 87
  ]
  edge [
    source 17
    target 608
  ]
  edge [
    source 17
    target 609
  ]
  edge [
    source 17
    target 610
  ]
  edge [
    source 17
    target 611
  ]
  edge [
    source 17
    target 612
  ]
  edge [
    source 17
    target 613
  ]
  edge [
    source 17
    target 92
  ]
  edge [
    source 17
    target 614
  ]
  edge [
    source 17
    target 615
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 618
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 619
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 621
  ]
  edge [
    source 17
    target 622
  ]
  edge [
    source 17
    target 34
  ]
  edge [
    source 17
    target 623
  ]
  edge [
    source 17
    target 624
  ]
  edge [
    source 17
    target 625
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 626
  ]
  edge [
    source 19
    target 627
  ]
  edge [
    source 19
    target 628
  ]
  edge [
    source 19
    target 629
  ]
  edge [
    source 19
    target 630
  ]
  edge [
    source 19
    target 631
  ]
  edge [
    source 19
    target 632
  ]
  edge [
    source 19
    target 633
  ]
  edge [
    source 19
    target 634
  ]
  edge [
    source 19
    target 635
  ]
  edge [
    source 19
    target 636
  ]
  edge [
    source 19
    target 637
  ]
  edge [
    source 19
    target 638
  ]
  edge [
    source 19
    target 639
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 640
  ]
  edge [
    source 20
    target 641
  ]
  edge [
    source 20
    target 642
  ]
  edge [
    source 20
    target 643
  ]
  edge [
    source 20
    target 644
  ]
  edge [
    source 20
    target 645
  ]
  edge [
    source 20
    target 646
  ]
  edge [
    source 20
    target 647
  ]
  edge [
    source 20
    target 648
  ]
  edge [
    source 20
    target 649
  ]
  edge [
    source 20
    target 650
  ]
  edge [
    source 20
    target 651
  ]
  edge [
    source 20
    target 652
  ]
  edge [
    source 20
    target 653
  ]
  edge [
    source 20
    target 654
  ]
  edge [
    source 20
    target 655
  ]
  edge [
    source 20
    target 656
  ]
  edge [
    source 20
    target 657
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 658
  ]
  edge [
    source 21
    target 587
  ]
  edge [
    source 21
    target 659
  ]
  edge [
    source 21
    target 660
  ]
  edge [
    source 21
    target 661
  ]
  edge [
    source 21
    target 662
  ]
  edge [
    source 21
    target 615
  ]
  edge [
    source 21
    target 663
  ]
  edge [
    source 21
    target 616
  ]
  edge [
    source 21
    target 618
  ]
  edge [
    source 21
    target 619
  ]
  edge [
    source 21
    target 664
  ]
  edge [
    source 21
    target 665
  ]
  edge [
    source 21
    target 666
  ]
  edge [
    source 21
    target 667
  ]
  edge [
    source 21
    target 90
  ]
  edge [
    source 21
    target 617
  ]
  edge [
    source 21
    target 102
  ]
  edge [
    source 21
    target 620
  ]
  edge [
    source 21
    target 621
  ]
  edge [
    source 21
    target 668
  ]
  edge [
    source 21
    target 669
  ]
  edge [
    source 21
    target 670
  ]
  edge [
    source 21
    target 671
  ]
  edge [
    source 21
    target 672
  ]
  edge [
    source 21
    target 673
  ]
  edge [
    source 21
    target 674
  ]
  edge [
    source 21
    target 675
  ]
  edge [
    source 21
    target 676
  ]
  edge [
    source 21
    target 677
  ]
  edge [
    source 21
    target 678
  ]
  edge [
    source 21
    target 679
  ]
  edge [
    source 21
    target 680
  ]
  edge [
    source 21
    target 447
  ]
  edge [
    source 21
    target 681
  ]
  edge [
    source 21
    target 682
  ]
  edge [
    source 21
    target 683
  ]
  edge [
    source 21
    target 684
  ]
  edge [
    source 21
    target 685
  ]
  edge [
    source 21
    target 686
  ]
  edge [
    source 21
    target 687
  ]
  edge [
    source 21
    target 688
  ]
  edge [
    source 21
    target 689
  ]
  edge [
    source 21
    target 690
  ]
  edge [
    source 21
    target 691
  ]
  edge [
    source 21
    target 692
  ]
  edge [
    source 21
    target 693
  ]
  edge [
    source 21
    target 694
  ]
  edge [
    source 21
    target 695
  ]
  edge [
    source 21
    target 696
  ]
  edge [
    source 21
    target 697
  ]
  edge [
    source 21
    target 698
  ]
  edge [
    source 21
    target 699
  ]
  edge [
    source 21
    target 418
  ]
  edge [
    source 21
    target 700
  ]
  edge [
    source 21
    target 701
  ]
  edge [
    source 21
    target 702
  ]
  edge [
    source 21
    target 703
  ]
  edge [
    source 22
    target 704
  ]
  edge [
    source 22
    target 705
  ]
  edge [
    source 22
    target 706
  ]
  edge [
    source 22
    target 707
  ]
  edge [
    source 22
    target 708
  ]
  edge [
    source 22
    target 709
  ]
  edge [
    source 22
    target 710
  ]
  edge [
    source 22
    target 711
  ]
  edge [
    source 22
    target 712
  ]
  edge [
    source 22
    target 713
  ]
  edge [
    source 22
    target 714
  ]
  edge [
    source 22
    target 715
  ]
  edge [
    source 22
    target 716
  ]
  edge [
    source 22
    target 717
  ]
  edge [
    source 22
    target 718
  ]
  edge [
    source 22
    target 719
  ]
  edge [
    source 22
    target 720
  ]
  edge [
    source 22
    target 721
  ]
  edge [
    source 22
    target 722
  ]
  edge [
    source 22
    target 294
  ]
  edge [
    source 22
    target 723
  ]
  edge [
    source 22
    target 724
  ]
  edge [
    source 22
    target 725
  ]
  edge [
    source 22
    target 726
  ]
  edge [
    source 22
    target 727
  ]
  edge [
    source 22
    target 728
  ]
  edge [
    source 22
    target 729
  ]
  edge [
    source 22
    target 730
  ]
  edge [
    source 22
    target 731
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 732
  ]
  edge [
    source 23
    target 733
  ]
  edge [
    source 23
    target 156
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 734
  ]
  edge [
    source 24
    target 735
  ]
  edge [
    source 24
    target 736
  ]
  edge [
    source 24
    target 737
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 738
  ]
  edge [
    source 25
    target 739
  ]
  edge [
    source 25
    target 740
  ]
  edge [
    source 25
    target 741
  ]
  edge [
    source 25
    target 742
  ]
  edge [
    source 25
    target 743
  ]
  edge [
    source 25
    target 744
  ]
  edge [
    source 25
    target 745
  ]
  edge [
    source 25
    target 746
  ]
  edge [
    source 25
    target 747
  ]
  edge [
    source 25
    target 748
  ]
  edge [
    source 25
    target 749
  ]
  edge [
    source 25
    target 750
  ]
  edge [
    source 25
    target 751
  ]
  edge [
    source 25
    target 752
  ]
  edge [
    source 25
    target 753
  ]
  edge [
    source 25
    target 754
  ]
  edge [
    source 25
    target 755
  ]
  edge [
    source 25
    target 756
  ]
  edge [
    source 25
    target 757
  ]
  edge [
    source 25
    target 758
  ]
  edge [
    source 25
    target 759
  ]
  edge [
    source 25
    target 760
  ]
  edge [
    source 25
    target 761
  ]
  edge [
    source 25
    target 762
  ]
  edge [
    source 25
    target 763
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 764
  ]
  edge [
    source 26
    target 765
  ]
  edge [
    source 26
    target 766
  ]
  edge [
    source 26
    target 767
  ]
  edge [
    source 26
    target 768
  ]
  edge [
    source 26
    target 769
  ]
  edge [
    source 26
    target 324
  ]
  edge [
    source 26
    target 363
  ]
  edge [
    source 26
    target 770
  ]
  edge [
    source 26
    target 771
  ]
  edge [
    source 26
    target 772
  ]
  edge [
    source 26
    target 773
  ]
  edge [
    source 26
    target 774
  ]
  edge [
    source 26
    target 176
  ]
  edge [
    source 26
    target 343
  ]
  edge [
    source 26
    target 775
  ]
  edge [
    source 26
    target 776
  ]
  edge [
    source 26
    target 777
  ]
  edge [
    source 26
    target 778
  ]
  edge [
    source 26
    target 779
  ]
  edge [
    source 26
    target 780
  ]
  edge [
    source 26
    target 781
  ]
  edge [
    source 26
    target 782
  ]
  edge [
    source 26
    target 783
  ]
  edge [
    source 26
    target 784
  ]
  edge [
    source 26
    target 785
  ]
  edge [
    source 26
    target 786
  ]
  edge [
    source 26
    target 787
  ]
  edge [
    source 26
    target 788
  ]
  edge [
    source 26
    target 314
  ]
  edge [
    source 26
    target 789
  ]
  edge [
    source 26
    target 790
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 791
  ]
  edge [
    source 27
    target 792
  ]
  edge [
    source 27
    target 793
  ]
  edge [
    source 27
    target 794
  ]
  edge [
    source 27
    target 795
  ]
  edge [
    source 27
    target 796
  ]
  edge [
    source 27
    target 797
  ]
  edge [
    source 27
    target 798
  ]
  edge [
    source 27
    target 799
  ]
  edge [
    source 27
    target 800
  ]
  edge [
    source 27
    target 801
  ]
  edge [
    source 27
    target 802
  ]
  edge [
    source 27
    target 803
  ]
  edge [
    source 27
    target 199
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 804
    target 805
  ]
  edge [
    source 804
    target 806
  ]
  edge [
    source 804
    target 807
  ]
  edge [
    source 804
    target 808
  ]
  edge [
    source 804
    target 809
  ]
  edge [
    source 805
    target 806
  ]
  edge [
    source 805
    target 807
  ]
  edge [
    source 805
    target 808
  ]
  edge [
    source 805
    target 809
  ]
  edge [
    source 806
    target 807
  ]
  edge [
    source 806
    target 808
  ]
  edge [
    source 806
    target 809
  ]
  edge [
    source 807
    target 808
  ]
  edge [
    source 807
    target 809
  ]
  edge [
    source 808
    target 809
  ]
  edge [
    source 810
    target 811
  ]
  edge [
    source 810
    target 812
  ]
  edge [
    source 811
    target 812
  ]
  edge [
    source 813
    target 814
  ]
  edge [
    source 815
    target 816
  ]
  edge [
    source 815
    target 817
  ]
  edge [
    source 815
    target 818
  ]
  edge [
    source 815
    target 819
  ]
  edge [
    source 816
    target 817
  ]
  edge [
    source 816
    target 818
  ]
  edge [
    source 817
    target 818
  ]
  edge [
    source 817
    target 819
  ]
  edge [
    source 820
    target 821
  ]
]
