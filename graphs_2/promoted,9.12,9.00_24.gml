graph [
  node [
    id 0
    label "brytyjski"
    origin "text"
  ]
  node [
    id 1
    label "parlament"
    origin "text"
  ]
  node [
    id 2
    label "upubliczni&#263;"
    origin "text"
  ]
  node [
    id 3
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wewn&#281;trzny"
    origin "text"
  ]
  node [
    id 5
    label "komunikacja"
    origin "text"
  ]
  node [
    id 6
    label "facebook"
    origin "text"
  ]
  node [
    id 7
    label "angielski"
  ]
  node [
    id 8
    label "morris"
  ]
  node [
    id 9
    label "j&#281;zyk_angielski"
  ]
  node [
    id 10
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 11
    label "anglosaski"
  ]
  node [
    id 12
    label "angielsko"
  ]
  node [
    id 13
    label "brytyjsko"
  ]
  node [
    id 14
    label "europejski"
  ]
  node [
    id 15
    label "zachodnioeuropejski"
  ]
  node [
    id 16
    label "po_brytyjsku"
  ]
  node [
    id 17
    label "j&#281;zyk_martwy"
  ]
  node [
    id 18
    label "po_europejsku"
  ]
  node [
    id 19
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 20
    label "European"
  ]
  node [
    id 21
    label "typowy"
  ]
  node [
    id 22
    label "charakterystyczny"
  ]
  node [
    id 23
    label "europejsko"
  ]
  node [
    id 24
    label "po_anglosasku"
  ]
  node [
    id 25
    label "anglosasko"
  ]
  node [
    id 26
    label "angol"
  ]
  node [
    id 27
    label "po_angielsku"
  ]
  node [
    id 28
    label "English"
  ]
  node [
    id 29
    label "anglicki"
  ]
  node [
    id 30
    label "j&#281;zyk"
  ]
  node [
    id 31
    label "j&#281;zyki_angielskie"
  ]
  node [
    id 32
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 33
    label "moreska"
  ]
  node [
    id 34
    label "po_zachodnioeuropejsku"
  ]
  node [
    id 35
    label "zachodni"
  ]
  node [
    id 36
    label "characteristically"
  ]
  node [
    id 37
    label "taniec_ludowy"
  ]
  node [
    id 38
    label "urz&#261;d"
  ]
  node [
    id 39
    label "europarlament"
  ]
  node [
    id 40
    label "plankton_polityczny"
  ]
  node [
    id 41
    label "grupa"
  ]
  node [
    id 42
    label "grupa_bilateralna"
  ]
  node [
    id 43
    label "ustawodawca"
  ]
  node [
    id 44
    label "legislature"
  ]
  node [
    id 45
    label "ustanowiciel"
  ]
  node [
    id 46
    label "organ"
  ]
  node [
    id 47
    label "autor"
  ]
  node [
    id 48
    label "stanowisko"
  ]
  node [
    id 49
    label "position"
  ]
  node [
    id 50
    label "instytucja"
  ]
  node [
    id 51
    label "siedziba"
  ]
  node [
    id 52
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 53
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 54
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 55
    label "mianowaniec"
  ]
  node [
    id 56
    label "dzia&#322;"
  ]
  node [
    id 57
    label "okienko"
  ]
  node [
    id 58
    label "w&#322;adza"
  ]
  node [
    id 59
    label "odm&#322;adzanie"
  ]
  node [
    id 60
    label "liga"
  ]
  node [
    id 61
    label "jednostka_systematyczna"
  ]
  node [
    id 62
    label "asymilowanie"
  ]
  node [
    id 63
    label "gromada"
  ]
  node [
    id 64
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 65
    label "asymilowa&#263;"
  ]
  node [
    id 66
    label "egzemplarz"
  ]
  node [
    id 67
    label "Entuzjastki"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "kompozycja"
  ]
  node [
    id 70
    label "Terranie"
  ]
  node [
    id 71
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 72
    label "category"
  ]
  node [
    id 73
    label "pakiet_klimatyczny"
  ]
  node [
    id 74
    label "oddzia&#322;"
  ]
  node [
    id 75
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 76
    label "cz&#261;steczka"
  ]
  node [
    id 77
    label "stage_set"
  ]
  node [
    id 78
    label "type"
  ]
  node [
    id 79
    label "specgrupa"
  ]
  node [
    id 80
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 81
    label "&#346;wietliki"
  ]
  node [
    id 82
    label "odm&#322;odzenie"
  ]
  node [
    id 83
    label "Eurogrupa"
  ]
  node [
    id 84
    label "odm&#322;adza&#263;"
  ]
  node [
    id 85
    label "formacja_geologiczna"
  ]
  node [
    id 86
    label "harcerze_starsi"
  ]
  node [
    id 87
    label "Bruksela"
  ]
  node [
    id 88
    label "eurowybory"
  ]
  node [
    id 89
    label "udost&#281;pni&#263;"
  ]
  node [
    id 90
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 91
    label "open"
  ]
  node [
    id 92
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 93
    label "temat"
  ]
  node [
    id 94
    label "istota"
  ]
  node [
    id 95
    label "informacja"
  ]
  node [
    id 96
    label "zawarto&#347;&#263;"
  ]
  node [
    id 97
    label "ilo&#347;&#263;"
  ]
  node [
    id 98
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 99
    label "wn&#281;trze"
  ]
  node [
    id 100
    label "punkt"
  ]
  node [
    id 101
    label "publikacja"
  ]
  node [
    id 102
    label "wiedza"
  ]
  node [
    id 103
    label "doj&#347;cie"
  ]
  node [
    id 104
    label "obiega&#263;"
  ]
  node [
    id 105
    label "powzi&#281;cie"
  ]
  node [
    id 106
    label "dane"
  ]
  node [
    id 107
    label "obiegni&#281;cie"
  ]
  node [
    id 108
    label "sygna&#322;"
  ]
  node [
    id 109
    label "obieganie"
  ]
  node [
    id 110
    label "powzi&#261;&#263;"
  ]
  node [
    id 111
    label "obiec"
  ]
  node [
    id 112
    label "doj&#347;&#263;"
  ]
  node [
    id 113
    label "mentalno&#347;&#263;"
  ]
  node [
    id 114
    label "superego"
  ]
  node [
    id 115
    label "psychika"
  ]
  node [
    id 116
    label "znaczenie"
  ]
  node [
    id 117
    label "charakter"
  ]
  node [
    id 118
    label "cecha"
  ]
  node [
    id 119
    label "sprawa"
  ]
  node [
    id 120
    label "wyraz_pochodny"
  ]
  node [
    id 121
    label "zboczenie"
  ]
  node [
    id 122
    label "om&#243;wienie"
  ]
  node [
    id 123
    label "rzecz"
  ]
  node [
    id 124
    label "omawia&#263;"
  ]
  node [
    id 125
    label "fraza"
  ]
  node [
    id 126
    label "entity"
  ]
  node [
    id 127
    label "forum"
  ]
  node [
    id 128
    label "topik"
  ]
  node [
    id 129
    label "tematyka"
  ]
  node [
    id 130
    label "w&#261;tek"
  ]
  node [
    id 131
    label "zbaczanie"
  ]
  node [
    id 132
    label "forma"
  ]
  node [
    id 133
    label "om&#243;wi&#263;"
  ]
  node [
    id 134
    label "omawianie"
  ]
  node [
    id 135
    label "melodia"
  ]
  node [
    id 136
    label "otoczka"
  ]
  node [
    id 137
    label "zbacza&#263;"
  ]
  node [
    id 138
    label "zboczy&#263;"
  ]
  node [
    id 139
    label "wewn&#281;trznie"
  ]
  node [
    id 140
    label "numer"
  ]
  node [
    id 141
    label "wn&#281;trzny"
  ]
  node [
    id 142
    label "psychiczny"
  ]
  node [
    id 143
    label "zesp&#243;&#322;"
  ]
  node [
    id 144
    label "lias"
  ]
  node [
    id 145
    label "system"
  ]
  node [
    id 146
    label "jednostka"
  ]
  node [
    id 147
    label "pi&#281;tro"
  ]
  node [
    id 148
    label "klasa"
  ]
  node [
    id 149
    label "jednostka_geologiczna"
  ]
  node [
    id 150
    label "filia"
  ]
  node [
    id 151
    label "malm"
  ]
  node [
    id 152
    label "whole"
  ]
  node [
    id 153
    label "dogger"
  ]
  node [
    id 154
    label "poziom"
  ]
  node [
    id 155
    label "promocja"
  ]
  node [
    id 156
    label "kurs"
  ]
  node [
    id 157
    label "bank"
  ]
  node [
    id 158
    label "formacja"
  ]
  node [
    id 159
    label "ajencja"
  ]
  node [
    id 160
    label "wojsko"
  ]
  node [
    id 161
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 162
    label "agencja"
  ]
  node [
    id 163
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 164
    label "szpital"
  ]
  node [
    id 165
    label "niezr&#243;wnowa&#380;ony"
  ]
  node [
    id 166
    label "psychicznie"
  ]
  node [
    id 167
    label "odciele&#347;nianie_si&#281;"
  ]
  node [
    id 168
    label "nienormalny"
  ]
  node [
    id 169
    label "nerwowo_chory"
  ]
  node [
    id 170
    label "niematerialny"
  ]
  node [
    id 171
    label "odciele&#347;nienie_si&#281;"
  ]
  node [
    id 172
    label "psychiatra"
  ]
  node [
    id 173
    label "turn"
  ]
  node [
    id 174
    label "liczba"
  ]
  node [
    id 175
    label "&#380;art"
  ]
  node [
    id 176
    label "zi&#243;&#322;ko"
  ]
  node [
    id 177
    label "manewr"
  ]
  node [
    id 178
    label "impression"
  ]
  node [
    id 179
    label "wyst&#281;p"
  ]
  node [
    id 180
    label "sztos"
  ]
  node [
    id 181
    label "oznaczenie"
  ]
  node [
    id 182
    label "hotel"
  ]
  node [
    id 183
    label "pok&#243;j"
  ]
  node [
    id 184
    label "czasopismo"
  ]
  node [
    id 185
    label "akt_p&#322;ciowy"
  ]
  node [
    id 186
    label "orygina&#322;"
  ]
  node [
    id 187
    label "facet"
  ]
  node [
    id 188
    label "wn&#281;trzowy"
  ]
  node [
    id 189
    label "transportation_system"
  ]
  node [
    id 190
    label "explicite"
  ]
  node [
    id 191
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 192
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 193
    label "wydeptywanie"
  ]
  node [
    id 194
    label "miejsce"
  ]
  node [
    id 195
    label "wydeptanie"
  ]
  node [
    id 196
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 197
    label "implicite"
  ]
  node [
    id 198
    label "ekspedytor"
  ]
  node [
    id 199
    label "warunek_lokalowy"
  ]
  node [
    id 200
    label "plac"
  ]
  node [
    id 201
    label "location"
  ]
  node [
    id 202
    label "uwaga"
  ]
  node [
    id 203
    label "przestrze&#324;"
  ]
  node [
    id 204
    label "status"
  ]
  node [
    id 205
    label "chwila"
  ]
  node [
    id 206
    label "cia&#322;o"
  ]
  node [
    id 207
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 208
    label "praca"
  ]
  node [
    id 209
    label "rz&#261;d"
  ]
  node [
    id 210
    label "przej&#347;cie"
  ]
  node [
    id 211
    label "zniszczenie"
  ]
  node [
    id 212
    label "egress"
  ]
  node [
    id 213
    label "ukszta&#322;towanie"
  ]
  node [
    id 214
    label "skombinowanie"
  ]
  node [
    id 215
    label "niszczenie"
  ]
  node [
    id 216
    label "kszta&#322;towanie"
  ]
  node [
    id 217
    label "pozyskiwanie"
  ]
  node [
    id 218
    label "pracownik"
  ]
  node [
    id 219
    label "weryfikator"
  ]
  node [
    id 220
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 221
    label "przekaz"
  ]
  node [
    id 222
    label "po&#347;rednio"
  ]
  node [
    id 223
    label "bezpo&#347;rednio"
  ]
  node [
    id 224
    label "konto"
  ]
  node [
    id 225
    label "wall"
  ]
  node [
    id 226
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 227
    label "dorobek"
  ]
  node [
    id 228
    label "mienie"
  ]
  node [
    id 229
    label "subkonto"
  ]
  node [
    id 230
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 231
    label "debet"
  ]
  node [
    id 232
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 233
    label "kariera"
  ]
  node [
    id 234
    label "reprezentacja"
  ]
  node [
    id 235
    label "dost&#281;p"
  ]
  node [
    id 236
    label "rachunek"
  ]
  node [
    id 237
    label "kredyt"
  ]
  node [
    id 238
    label "profil"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
]
