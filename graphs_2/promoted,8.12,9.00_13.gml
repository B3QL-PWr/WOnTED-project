graph [
  node [
    id 0
    label "sie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "nagranie"
    origin "text"
  ]
  node [
    id 4
    label "wideo"
    origin "text"
  ]
  node [
    id 5
    label "zdj&#281;cia"
    origin "text"
  ]
  node [
    id 6
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 7
    label "wida&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dziesi&#261;tek"
    origin "text"
  ]
  node [
    id 9
    label "zatrzymana"
    origin "text"
  ]
  node [
    id 10
    label "ucze&#324;"
    origin "text"
  ]
  node [
    id 11
    label "kl&#281;cz&#261;cy"
    origin "text"
  ]
  node [
    id 12
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 13
    label "r&#281;ka"
    origin "text"
  ]
  node [
    id 14
    label "sku&#263;"
    origin "text"
  ]
  node [
    id 15
    label "kajdanki"
    origin "text"
  ]
  node [
    id 16
    label "trzyma&#263;"
    origin "text"
  ]
  node [
    id 17
    label "g&#322;owa"
    origin "text"
  ]
  node [
    id 18
    label "kszta&#322;t"
  ]
  node [
    id 19
    label "provider"
  ]
  node [
    id 20
    label "biznes_elektroniczny"
  ]
  node [
    id 21
    label "zasadzka"
  ]
  node [
    id 22
    label "mesh"
  ]
  node [
    id 23
    label "plecionka"
  ]
  node [
    id 24
    label "gauze"
  ]
  node [
    id 25
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 26
    label "struktura"
  ]
  node [
    id 27
    label "web"
  ]
  node [
    id 28
    label "organizacja"
  ]
  node [
    id 29
    label "gra_sieciowa"
  ]
  node [
    id 30
    label "net"
  ]
  node [
    id 31
    label "media"
  ]
  node [
    id 32
    label "sie&#263;_komputerowa"
  ]
  node [
    id 33
    label "nitka"
  ]
  node [
    id 34
    label "snu&#263;"
  ]
  node [
    id 35
    label "vane"
  ]
  node [
    id 36
    label "instalacja"
  ]
  node [
    id 37
    label "wysnu&#263;"
  ]
  node [
    id 38
    label "organization"
  ]
  node [
    id 39
    label "obiekt"
  ]
  node [
    id 40
    label "us&#322;uga_internetowa"
  ]
  node [
    id 41
    label "rozmieszczenie"
  ]
  node [
    id 42
    label "podcast"
  ]
  node [
    id 43
    label "hipertekst"
  ]
  node [
    id 44
    label "cyberprzestrze&#324;"
  ]
  node [
    id 45
    label "mem"
  ]
  node [
    id 46
    label "grooming"
  ]
  node [
    id 47
    label "punkt_dost&#281;pu"
  ]
  node [
    id 48
    label "netbook"
  ]
  node [
    id 49
    label "e-hazard"
  ]
  node [
    id 50
    label "strona"
  ]
  node [
    id 51
    label "zastawia&#263;"
  ]
  node [
    id 52
    label "miejsce"
  ]
  node [
    id 53
    label "zastawi&#263;"
  ]
  node [
    id 54
    label "ambush"
  ]
  node [
    id 55
    label "atak"
  ]
  node [
    id 56
    label "podst&#281;p"
  ]
  node [
    id 57
    label "sytuacja"
  ]
  node [
    id 58
    label "formacja"
  ]
  node [
    id 59
    label "punkt_widzenia"
  ]
  node [
    id 60
    label "wygl&#261;d"
  ]
  node [
    id 61
    label "spirala"
  ]
  node [
    id 62
    label "p&#322;at"
  ]
  node [
    id 63
    label "comeliness"
  ]
  node [
    id 64
    label "kielich"
  ]
  node [
    id 65
    label "face"
  ]
  node [
    id 66
    label "blaszka"
  ]
  node [
    id 67
    label "charakter"
  ]
  node [
    id 68
    label "p&#281;tla"
  ]
  node [
    id 69
    label "pasmo"
  ]
  node [
    id 70
    label "cecha"
  ]
  node [
    id 71
    label "linearno&#347;&#263;"
  ]
  node [
    id 72
    label "gwiazda"
  ]
  node [
    id 73
    label "miniatura"
  ]
  node [
    id 74
    label "integer"
  ]
  node [
    id 75
    label "liczba"
  ]
  node [
    id 76
    label "zlewanie_si&#281;"
  ]
  node [
    id 77
    label "ilo&#347;&#263;"
  ]
  node [
    id 78
    label "uk&#322;ad"
  ]
  node [
    id 79
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 80
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 81
    label "pe&#322;ny"
  ]
  node [
    id 82
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 83
    label "u&#322;o&#380;enie"
  ]
  node [
    id 84
    label "porozmieszczanie"
  ]
  node [
    id 85
    label "wyst&#281;powanie"
  ]
  node [
    id 86
    label "layout"
  ]
  node [
    id 87
    label "umieszczenie"
  ]
  node [
    id 88
    label "mechanika"
  ]
  node [
    id 89
    label "o&#347;"
  ]
  node [
    id 90
    label "usenet"
  ]
  node [
    id 91
    label "rozprz&#261;c"
  ]
  node [
    id 92
    label "zachowanie"
  ]
  node [
    id 93
    label "cybernetyk"
  ]
  node [
    id 94
    label "podsystem"
  ]
  node [
    id 95
    label "system"
  ]
  node [
    id 96
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 97
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 98
    label "sk&#322;ad"
  ]
  node [
    id 99
    label "systemat"
  ]
  node [
    id 100
    label "konstrukcja"
  ]
  node [
    id 101
    label "konstelacja"
  ]
  node [
    id 102
    label "podmiot"
  ]
  node [
    id 103
    label "jednostka_organizacyjna"
  ]
  node [
    id 104
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 105
    label "TOPR"
  ]
  node [
    id 106
    label "endecki"
  ]
  node [
    id 107
    label "zesp&#243;&#322;"
  ]
  node [
    id 108
    label "przedstawicielstwo"
  ]
  node [
    id 109
    label "od&#322;am"
  ]
  node [
    id 110
    label "Cepelia"
  ]
  node [
    id 111
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 112
    label "ZBoWiD"
  ]
  node [
    id 113
    label "centrala"
  ]
  node [
    id 114
    label "GOPR"
  ]
  node [
    id 115
    label "ZOMO"
  ]
  node [
    id 116
    label "ZMP"
  ]
  node [
    id 117
    label "komitet_koordynacyjny"
  ]
  node [
    id 118
    label "przybud&#243;wka"
  ]
  node [
    id 119
    label "boj&#243;wka"
  ]
  node [
    id 120
    label "zmiana_po&#322;o&#380;enia"
  ]
  node [
    id 121
    label "proces"
  ]
  node [
    id 122
    label "kompozycja"
  ]
  node [
    id 123
    label "uzbrajanie"
  ]
  node [
    id 124
    label "czynno&#347;&#263;"
  ]
  node [
    id 125
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 126
    label "co&#347;"
  ]
  node [
    id 127
    label "budynek"
  ]
  node [
    id 128
    label "thing"
  ]
  node [
    id 129
    label "poj&#281;cie"
  ]
  node [
    id 130
    label "program"
  ]
  node [
    id 131
    label "rzecz"
  ]
  node [
    id 132
    label "mass-media"
  ]
  node [
    id 133
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 134
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 135
    label "przekazior"
  ]
  node [
    id 136
    label "medium"
  ]
  node [
    id 137
    label "tekst"
  ]
  node [
    id 138
    label "ornament"
  ]
  node [
    id 139
    label "przedmiot"
  ]
  node [
    id 140
    label "splot"
  ]
  node [
    id 141
    label "braid"
  ]
  node [
    id 142
    label "szachulec"
  ]
  node [
    id 143
    label "b&#322;&#261;d"
  ]
  node [
    id 144
    label "wyr&#243;b_w&#322;&#243;kienniczy"
  ]
  node [
    id 145
    label "nawijad&#322;o"
  ]
  node [
    id 146
    label "sznur"
  ]
  node [
    id 147
    label "motowid&#322;o"
  ]
  node [
    id 148
    label "makaron"
  ]
  node [
    id 149
    label "internet"
  ]
  node [
    id 150
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 151
    label "kartka"
  ]
  node [
    id 152
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 153
    label "logowanie"
  ]
  node [
    id 154
    label "plik"
  ]
  node [
    id 155
    label "s&#261;d"
  ]
  node [
    id 156
    label "adres_internetowy"
  ]
  node [
    id 157
    label "linia"
  ]
  node [
    id 158
    label "serwis_internetowy"
  ]
  node [
    id 159
    label "posta&#263;"
  ]
  node [
    id 160
    label "bok"
  ]
  node [
    id 161
    label "skr&#281;canie"
  ]
  node [
    id 162
    label "skr&#281;ca&#263;"
  ]
  node [
    id 163
    label "orientowanie"
  ]
  node [
    id 164
    label "skr&#281;ci&#263;"
  ]
  node [
    id 165
    label "uj&#281;cie"
  ]
  node [
    id 166
    label "zorientowanie"
  ]
  node [
    id 167
    label "ty&#322;"
  ]
  node [
    id 168
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 169
    label "fragment"
  ]
  node [
    id 170
    label "zorientowa&#263;"
  ]
  node [
    id 171
    label "pagina"
  ]
  node [
    id 172
    label "g&#243;ra"
  ]
  node [
    id 173
    label "orientowa&#263;"
  ]
  node [
    id 174
    label "voice"
  ]
  node [
    id 175
    label "orientacja"
  ]
  node [
    id 176
    label "prz&#243;d"
  ]
  node [
    id 177
    label "powierzchnia"
  ]
  node [
    id 178
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 179
    label "forma"
  ]
  node [
    id 180
    label "skr&#281;cenie"
  ]
  node [
    id 181
    label "paj&#261;k"
  ]
  node [
    id 182
    label "devise"
  ]
  node [
    id 183
    label "wyjmowa&#263;"
  ]
  node [
    id 184
    label "project"
  ]
  node [
    id 185
    label "my&#347;le&#263;"
  ]
  node [
    id 186
    label "produkowa&#263;"
  ]
  node [
    id 187
    label "uk&#322;ada&#263;"
  ]
  node [
    id 188
    label "tworzy&#263;"
  ]
  node [
    id 189
    label "wyj&#261;&#263;"
  ]
  node [
    id 190
    label "stworzy&#263;"
  ]
  node [
    id 191
    label "zasadzi&#263;"
  ]
  node [
    id 192
    label "dostawca"
  ]
  node [
    id 193
    label "telefonia"
  ]
  node [
    id 194
    label "wydawnictwo"
  ]
  node [
    id 195
    label "meme"
  ]
  node [
    id 196
    label "hazard"
  ]
  node [
    id 197
    label "molestowanie_seksualne"
  ]
  node [
    id 198
    label "piel&#281;gnacja"
  ]
  node [
    id 199
    label "zwierz&#281;_domowe"
  ]
  node [
    id 200
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 201
    label "ma&#322;y"
  ]
  node [
    id 202
    label "wytw&#243;r"
  ]
  node [
    id 203
    label "wys&#322;uchanie"
  ]
  node [
    id 204
    label "utrwalenie"
  ]
  node [
    id 205
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 206
    label "recording"
  ]
  node [
    id 207
    label "ustalenie"
  ]
  node [
    id 208
    label "trwalszy"
  ]
  node [
    id 209
    label "confirmation"
  ]
  node [
    id 210
    label "p&#322;&#243;d"
  ]
  node [
    id 211
    label "work"
  ]
  node [
    id 212
    label "rezultat"
  ]
  node [
    id 213
    label "pos&#322;uchanie"
  ]
  node [
    id 214
    label "spe&#322;nienie"
  ]
  node [
    id 215
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 216
    label "hearing"
  ]
  node [
    id 217
    label "muzyka"
  ]
  node [
    id 218
    label "spe&#322;ni&#263;"
  ]
  node [
    id 219
    label "technika"
  ]
  node [
    id 220
    label "film"
  ]
  node [
    id 221
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 222
    label "wideokaseta"
  ]
  node [
    id 223
    label "odtwarzacz"
  ]
  node [
    id 224
    label "telekomunikacja"
  ]
  node [
    id 225
    label "cywilizacja"
  ]
  node [
    id 226
    label "spos&#243;b"
  ]
  node [
    id 227
    label "wiedza"
  ]
  node [
    id 228
    label "sprawno&#347;&#263;"
  ]
  node [
    id 229
    label "engineering"
  ]
  node [
    id 230
    label "fotowoltaika"
  ]
  node [
    id 231
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 232
    label "teletechnika"
  ]
  node [
    id 233
    label "mechanika_precyzyjna"
  ]
  node [
    id 234
    label "technologia"
  ]
  node [
    id 235
    label "animatronika"
  ]
  node [
    id 236
    label "odczulenie"
  ]
  node [
    id 237
    label "odczula&#263;"
  ]
  node [
    id 238
    label "blik"
  ]
  node [
    id 239
    label "odczuli&#263;"
  ]
  node [
    id 240
    label "scena"
  ]
  node [
    id 241
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 242
    label "muza"
  ]
  node [
    id 243
    label "postprodukcja"
  ]
  node [
    id 244
    label "block"
  ]
  node [
    id 245
    label "trawiarnia"
  ]
  node [
    id 246
    label "sklejarka"
  ]
  node [
    id 247
    label "sztuka"
  ]
  node [
    id 248
    label "filmoteka"
  ]
  node [
    id 249
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 250
    label "klatka"
  ]
  node [
    id 251
    label "rozbieg&#243;wka"
  ]
  node [
    id 252
    label "napisy"
  ]
  node [
    id 253
    label "ta&#347;ma"
  ]
  node [
    id 254
    label "odczulanie"
  ]
  node [
    id 255
    label "anamorfoza"
  ]
  node [
    id 256
    label "dorobek"
  ]
  node [
    id 257
    label "ty&#322;&#243;wka"
  ]
  node [
    id 258
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 259
    label "b&#322;ona"
  ]
  node [
    id 260
    label "emulsja_fotograficzna"
  ]
  node [
    id 261
    label "photograph"
  ]
  node [
    id 262
    label "czo&#322;&#243;wka"
  ]
  node [
    id 263
    label "rola"
  ]
  node [
    id 264
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 265
    label "sprz&#281;t_audio"
  ]
  node [
    id 266
    label "magnetowid"
  ]
  node [
    id 267
    label "wideoteka"
  ]
  node [
    id 268
    label "wypo&#380;yczalnia_wideo"
  ]
  node [
    id 269
    label "kaseta"
  ]
  node [
    id 270
    label "dark_lantern"
  ]
  node [
    id 271
    label "kopa"
  ]
  node [
    id 272
    label "zbi&#243;r"
  ]
  node [
    id 273
    label "egzemplarz"
  ]
  node [
    id 274
    label "series"
  ]
  node [
    id 275
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 276
    label "uprawianie"
  ]
  node [
    id 277
    label "praca_rolnicza"
  ]
  node [
    id 278
    label "collection"
  ]
  node [
    id 279
    label "dane"
  ]
  node [
    id 280
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 281
    label "pakiet_klimatyczny"
  ]
  node [
    id 282
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 283
    label "sum"
  ]
  node [
    id 284
    label "gathering"
  ]
  node [
    id 285
    label "album"
  ]
  node [
    id 286
    label "kuczka"
  ]
  node [
    id 287
    label "sterta"
  ]
  node [
    id 288
    label "rzemie&#347;lnik"
  ]
  node [
    id 289
    label "wyprawka"
  ]
  node [
    id 290
    label "mundurek"
  ]
  node [
    id 291
    label "szko&#322;a"
  ]
  node [
    id 292
    label "cz&#322;owiek"
  ]
  node [
    id 293
    label "tarcza"
  ]
  node [
    id 294
    label "elew"
  ]
  node [
    id 295
    label "kontynuator"
  ]
  node [
    id 296
    label "absolwent"
  ]
  node [
    id 297
    label "zwolennik"
  ]
  node [
    id 298
    label "praktykant"
  ]
  node [
    id 299
    label "klasa"
  ]
  node [
    id 300
    label "czeladnik"
  ]
  node [
    id 301
    label "wagon"
  ]
  node [
    id 302
    label "mecz_mistrzowski"
  ]
  node [
    id 303
    label "arrangement"
  ]
  node [
    id 304
    label "class"
  ]
  node [
    id 305
    label "&#322;awka"
  ]
  node [
    id 306
    label "wykrzyknik"
  ]
  node [
    id 307
    label "zaleta"
  ]
  node [
    id 308
    label "jednostka_systematyczna"
  ]
  node [
    id 309
    label "programowanie_obiektowe"
  ]
  node [
    id 310
    label "tablica"
  ]
  node [
    id 311
    label "warstwa"
  ]
  node [
    id 312
    label "rezerwa"
  ]
  node [
    id 313
    label "gromada"
  ]
  node [
    id 314
    label "Ekwici"
  ]
  node [
    id 315
    label "&#347;rodowisko"
  ]
  node [
    id 316
    label "sala"
  ]
  node [
    id 317
    label "pomoc"
  ]
  node [
    id 318
    label "form"
  ]
  node [
    id 319
    label "grupa"
  ]
  node [
    id 320
    label "przepisa&#263;"
  ]
  node [
    id 321
    label "jako&#347;&#263;"
  ]
  node [
    id 322
    label "znak_jako&#347;ci"
  ]
  node [
    id 323
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 324
    label "poziom"
  ]
  node [
    id 325
    label "type"
  ]
  node [
    id 326
    label "promocja"
  ]
  node [
    id 327
    label "przepisanie"
  ]
  node [
    id 328
    label "kurs"
  ]
  node [
    id 329
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 330
    label "dziennik_lekcyjny"
  ]
  node [
    id 331
    label "typ"
  ]
  node [
    id 332
    label "fakcja"
  ]
  node [
    id 333
    label "obrona"
  ]
  node [
    id 334
    label "botanika"
  ]
  node [
    id 335
    label "do&#347;wiadczenie"
  ]
  node [
    id 336
    label "teren_szko&#322;y"
  ]
  node [
    id 337
    label "Mickiewicz"
  ]
  node [
    id 338
    label "kwalifikacje"
  ]
  node [
    id 339
    label "podr&#281;cznik"
  ]
  node [
    id 340
    label "praktyka"
  ]
  node [
    id 341
    label "school"
  ]
  node [
    id 342
    label "zda&#263;"
  ]
  node [
    id 343
    label "gabinet"
  ]
  node [
    id 344
    label "urszulanki"
  ]
  node [
    id 345
    label "sztuba"
  ]
  node [
    id 346
    label "&#322;awa_szkolna"
  ]
  node [
    id 347
    label "nauka"
  ]
  node [
    id 348
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 349
    label "lekcja"
  ]
  node [
    id 350
    label "metoda"
  ]
  node [
    id 351
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 352
    label "czas"
  ]
  node [
    id 353
    label "skolaryzacja"
  ]
  node [
    id 354
    label "zdanie"
  ]
  node [
    id 355
    label "stopek"
  ]
  node [
    id 356
    label "sekretariat"
  ]
  node [
    id 357
    label "ideologia"
  ]
  node [
    id 358
    label "lesson"
  ]
  node [
    id 359
    label "instytucja"
  ]
  node [
    id 360
    label "niepokalanki"
  ]
  node [
    id 361
    label "siedziba"
  ]
  node [
    id 362
    label "szkolenie"
  ]
  node [
    id 363
    label "kara"
  ]
  node [
    id 364
    label "ludzko&#347;&#263;"
  ]
  node [
    id 365
    label "asymilowanie"
  ]
  node [
    id 366
    label "wapniak"
  ]
  node [
    id 367
    label "asymilowa&#263;"
  ]
  node [
    id 368
    label "os&#322;abia&#263;"
  ]
  node [
    id 369
    label "hominid"
  ]
  node [
    id 370
    label "podw&#322;adny"
  ]
  node [
    id 371
    label "os&#322;abianie"
  ]
  node [
    id 372
    label "figura"
  ]
  node [
    id 373
    label "portrecista"
  ]
  node [
    id 374
    label "dwun&#243;g"
  ]
  node [
    id 375
    label "profanum"
  ]
  node [
    id 376
    label "mikrokosmos"
  ]
  node [
    id 377
    label "nasada"
  ]
  node [
    id 378
    label "duch"
  ]
  node [
    id 379
    label "antropochoria"
  ]
  node [
    id 380
    label "osoba"
  ]
  node [
    id 381
    label "wz&#243;r"
  ]
  node [
    id 382
    label "senior"
  ]
  node [
    id 383
    label "oddzia&#322;ywanie"
  ]
  node [
    id 384
    label "Adam"
  ]
  node [
    id 385
    label "homo_sapiens"
  ]
  node [
    id 386
    label "polifag"
  ]
  node [
    id 387
    label "remiecha"
  ]
  node [
    id 388
    label "nowicjusz"
  ]
  node [
    id 389
    label "nast&#281;pca"
  ]
  node [
    id 390
    label "na&#347;ladowca"
  ]
  node [
    id 391
    label "&#380;o&#322;nierz"
  ]
  node [
    id 392
    label "student"
  ]
  node [
    id 393
    label "harcerz"
  ]
  node [
    id 394
    label "uniform"
  ]
  node [
    id 395
    label "naszywka"
  ]
  node [
    id 396
    label "wskaz&#243;wka"
  ]
  node [
    id 397
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 398
    label "obro&#324;ca"
  ]
  node [
    id 399
    label "bro&#324;_ochronna"
  ]
  node [
    id 400
    label "odznaka"
  ]
  node [
    id 401
    label "bro&#324;"
  ]
  node [
    id 402
    label "denture"
  ]
  node [
    id 403
    label "p&#322;yta_kontynentalna"
  ]
  node [
    id 404
    label "telefon"
  ]
  node [
    id 405
    label "or&#281;&#380;"
  ]
  node [
    id 406
    label "ochrona"
  ]
  node [
    id 407
    label "target"
  ]
  node [
    id 408
    label "cel"
  ]
  node [
    id 409
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 410
    label "maszyna"
  ]
  node [
    id 411
    label "obszar"
  ]
  node [
    id 412
    label "zapomoga"
  ]
  node [
    id 413
    label "komplet"
  ]
  node [
    id 414
    label "layette"
  ]
  node [
    id 415
    label "wyprawa"
  ]
  node [
    id 416
    label "niemowl&#281;"
  ]
  node [
    id 417
    label "wiano"
  ]
  node [
    id 418
    label "rzemie&#347;lniczek"
  ]
  node [
    id 419
    label "przybli&#380;enie"
  ]
  node [
    id 420
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 421
    label "kategoria"
  ]
  node [
    id 422
    label "szpaler"
  ]
  node [
    id 423
    label "lon&#380;a"
  ]
  node [
    id 424
    label "uporz&#261;dkowanie"
  ]
  node [
    id 425
    label "egzekutywa"
  ]
  node [
    id 426
    label "premier"
  ]
  node [
    id 427
    label "Londyn"
  ]
  node [
    id 428
    label "gabinet_cieni"
  ]
  node [
    id 429
    label "number"
  ]
  node [
    id 430
    label "Konsulat"
  ]
  node [
    id 431
    label "tract"
  ]
  node [
    id 432
    label "w&#322;adza"
  ]
  node [
    id 433
    label "spowodowanie"
  ]
  node [
    id 434
    label "structure"
  ]
  node [
    id 435
    label "sequence"
  ]
  node [
    id 436
    label "succession"
  ]
  node [
    id 437
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 438
    label "zapoznanie"
  ]
  node [
    id 439
    label "podanie"
  ]
  node [
    id 440
    label "bliski"
  ]
  node [
    id 441
    label "wyja&#347;nienie"
  ]
  node [
    id 442
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 443
    label "przemieszczenie"
  ]
  node [
    id 444
    label "approach"
  ]
  node [
    id 445
    label "pickup"
  ]
  node [
    id 446
    label "estimate"
  ]
  node [
    id 447
    label "po&#322;&#261;czenie"
  ]
  node [
    id 448
    label "ocena"
  ]
  node [
    id 449
    label "teoria"
  ]
  node [
    id 450
    label "osoba_prawna"
  ]
  node [
    id 451
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 452
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 453
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 454
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 455
    label "biuro"
  ]
  node [
    id 456
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 457
    label "Fundusze_Unijne"
  ]
  node [
    id 458
    label "zamyka&#263;"
  ]
  node [
    id 459
    label "establishment"
  ]
  node [
    id 460
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 461
    label "urz&#261;d"
  ]
  node [
    id 462
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 463
    label "afiliowa&#263;"
  ]
  node [
    id 464
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 465
    label "standard"
  ]
  node [
    id 466
    label "zamykanie"
  ]
  node [
    id 467
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 468
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 469
    label "organ"
  ]
  node [
    id 470
    label "obrady"
  ]
  node [
    id 471
    label "executive"
  ]
  node [
    id 472
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 473
    label "partia"
  ]
  node [
    id 474
    label "federacja"
  ]
  node [
    id 475
    label "przej&#347;cie"
  ]
  node [
    id 476
    label "espalier"
  ]
  node [
    id 477
    label "aleja"
  ]
  node [
    id 478
    label "szyk"
  ]
  node [
    id 479
    label "jednostka_administracyjna"
  ]
  node [
    id 480
    label "zoologia"
  ]
  node [
    id 481
    label "skupienie"
  ]
  node [
    id 482
    label "kr&#243;lestwo"
  ]
  node [
    id 483
    label "stage_set"
  ]
  node [
    id 484
    label "tribe"
  ]
  node [
    id 485
    label "hurma"
  ]
  node [
    id 486
    label "lina"
  ]
  node [
    id 487
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 488
    label "Bismarck"
  ]
  node [
    id 489
    label "zwierzchnik"
  ]
  node [
    id 490
    label "Sto&#322;ypin"
  ]
  node [
    id 491
    label "Miko&#322;ajczyk"
  ]
  node [
    id 492
    label "Chruszczow"
  ]
  node [
    id 493
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 494
    label "Jelcyn"
  ]
  node [
    id 495
    label "dostojnik"
  ]
  node [
    id 496
    label "prawo"
  ]
  node [
    id 497
    label "rz&#261;dzenie"
  ]
  node [
    id 498
    label "panowanie"
  ]
  node [
    id 499
    label "Kreml"
  ]
  node [
    id 500
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 501
    label "wydolno&#347;&#263;"
  ]
  node [
    id 502
    label "Wimbledon"
  ]
  node [
    id 503
    label "Westminster"
  ]
  node [
    id 504
    label "Londek"
  ]
  node [
    id 505
    label "krzy&#380;"
  ]
  node [
    id 506
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 507
    label "handwriting"
  ]
  node [
    id 508
    label "d&#322;o&#324;"
  ]
  node [
    id 509
    label "gestykulowa&#263;"
  ]
  node [
    id 510
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 511
    label "palec"
  ]
  node [
    id 512
    label "przedrami&#281;"
  ]
  node [
    id 513
    label "hand"
  ]
  node [
    id 514
    label "&#322;okie&#263;"
  ]
  node [
    id 515
    label "hazena"
  ]
  node [
    id 516
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 517
    label "bramkarz"
  ]
  node [
    id 518
    label "nadgarstek"
  ]
  node [
    id 519
    label "graba"
  ]
  node [
    id 520
    label "pracownik"
  ]
  node [
    id 521
    label "r&#261;czyna"
  ]
  node [
    id 522
    label "k&#322;&#261;b"
  ]
  node [
    id 523
    label "pi&#322;ka"
  ]
  node [
    id 524
    label "chwyta&#263;"
  ]
  node [
    id 525
    label "cmoknonsens"
  ]
  node [
    id 526
    label "pomocnik"
  ]
  node [
    id 527
    label "gestykulowanie"
  ]
  node [
    id 528
    label "chwytanie"
  ]
  node [
    id 529
    label "obietnica"
  ]
  node [
    id 530
    label "zagrywka"
  ]
  node [
    id 531
    label "kroki"
  ]
  node [
    id 532
    label "hasta"
  ]
  node [
    id 533
    label "wykroczenie"
  ]
  node [
    id 534
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 535
    label "czerwona_kartka"
  ]
  node [
    id 536
    label "paw"
  ]
  node [
    id 537
    label "rami&#281;"
  ]
  node [
    id 538
    label "kula"
  ]
  node [
    id 539
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 540
    label "do&#347;rodkowywa&#263;"
  ]
  node [
    id 541
    label "do&#347;rodkowywanie"
  ]
  node [
    id 542
    label "odbicie"
  ]
  node [
    id 543
    label "gra"
  ]
  node [
    id 544
    label "musket_ball"
  ]
  node [
    id 545
    label "aut"
  ]
  node [
    id 546
    label "serwowa&#263;"
  ]
  node [
    id 547
    label "sport_zespo&#322;owy"
  ]
  node [
    id 548
    label "sport"
  ]
  node [
    id 549
    label "serwowanie"
  ]
  node [
    id 550
    label "orb"
  ]
  node [
    id 551
    label "&#347;wieca"
  ]
  node [
    id 552
    label "zaserwowanie"
  ]
  node [
    id 553
    label "zaserwowa&#263;"
  ]
  node [
    id 554
    label "rzucanka"
  ]
  node [
    id 555
    label "charakterystyka"
  ]
  node [
    id 556
    label "m&#322;ot"
  ]
  node [
    id 557
    label "znak"
  ]
  node [
    id 558
    label "drzewo"
  ]
  node [
    id 559
    label "pr&#243;ba"
  ]
  node [
    id 560
    label "attribute"
  ]
  node [
    id 561
    label "marka"
  ]
  node [
    id 562
    label "model"
  ]
  node [
    id 563
    label "narz&#281;dzie"
  ]
  node [
    id 564
    label "tryb"
  ]
  node [
    id 565
    label "nature"
  ]
  node [
    id 566
    label "discourtesy"
  ]
  node [
    id 567
    label "post&#281;pek"
  ]
  node [
    id 568
    label "transgresja"
  ]
  node [
    id 569
    label "zrobienie"
  ]
  node [
    id 570
    label "gambit"
  ]
  node [
    id 571
    label "rozgrywka"
  ]
  node [
    id 572
    label "move"
  ]
  node [
    id 573
    label "manewr"
  ]
  node [
    id 574
    label "uderzenie"
  ]
  node [
    id 575
    label "posuni&#281;cie"
  ]
  node [
    id 576
    label "myk"
  ]
  node [
    id 577
    label "gra_w_karty"
  ]
  node [
    id 578
    label "mecz"
  ]
  node [
    id 579
    label "travel"
  ]
  node [
    id 580
    label "zapowied&#378;"
  ]
  node [
    id 581
    label "statement"
  ]
  node [
    id 582
    label "zapewnienie"
  ]
  node [
    id 583
    label "hokej"
  ]
  node [
    id 584
    label "zawodnik"
  ]
  node [
    id 585
    label "gracz"
  ]
  node [
    id 586
    label "pi&#322;ka_r&#281;czna"
  ]
  node [
    id 587
    label "bileter"
  ]
  node [
    id 588
    label "wykidaj&#322;o"
  ]
  node [
    id 589
    label "d&#378;wi&#281;k"
  ]
  node [
    id 590
    label "koszyk&#243;wka"
  ]
  node [
    id 591
    label "traverse"
  ]
  node [
    id 592
    label "kara_&#347;mierci"
  ]
  node [
    id 593
    label "cierpienie"
  ]
  node [
    id 594
    label "symbol"
  ]
  node [
    id 595
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 596
    label "biblizm"
  ]
  node [
    id 597
    label "order"
  ]
  node [
    id 598
    label "gest"
  ]
  node [
    id 599
    label "ujmowa&#263;"
  ]
  node [
    id 600
    label "zabiera&#263;"
  ]
  node [
    id 601
    label "bra&#263;"
  ]
  node [
    id 602
    label "rozumie&#263;"
  ]
  node [
    id 603
    label "get"
  ]
  node [
    id 604
    label "dochodzi&#263;"
  ]
  node [
    id 605
    label "cope"
  ]
  node [
    id 606
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 607
    label "ogarnia&#263;"
  ]
  node [
    id 608
    label "doj&#347;&#263;"
  ]
  node [
    id 609
    label "perceive"
  ]
  node [
    id 610
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 611
    label "w&#322;&#243;cznia"
  ]
  node [
    id 612
    label "triarius"
  ]
  node [
    id 613
    label "ca&#322;us"
  ]
  node [
    id 614
    label "dochodzenie"
  ]
  node [
    id 615
    label "rozumienie"
  ]
  node [
    id 616
    label "branie"
  ]
  node [
    id 617
    label "perception"
  ]
  node [
    id 618
    label "wpadni&#281;cie"
  ]
  node [
    id 619
    label "catch"
  ]
  node [
    id 620
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 621
    label "odp&#322;ywanie"
  ]
  node [
    id 622
    label "ogarnianie"
  ]
  node [
    id 623
    label "ci&#261;gni&#281;cie"
  ]
  node [
    id 624
    label "porywanie"
  ]
  node [
    id 625
    label "wpadanie"
  ]
  node [
    id 626
    label "doj&#347;cie"
  ]
  node [
    id 627
    label "przyp&#322;ywanie"
  ]
  node [
    id 628
    label "pokazanie"
  ]
  node [
    id 629
    label "ruszanie"
  ]
  node [
    id 630
    label "pokazywanie"
  ]
  node [
    id 631
    label "gesticulate"
  ]
  node [
    id 632
    label "rusza&#263;"
  ]
  node [
    id 633
    label "linia_mi&#322;o&#347;ci"
  ]
  node [
    id 634
    label "wyklepanie"
  ]
  node [
    id 635
    label "chiromancja"
  ]
  node [
    id 636
    label "klepanie"
  ]
  node [
    id 637
    label "wyklepa&#263;"
  ]
  node [
    id 638
    label "szeroko&#347;&#263;_d&#322;oni"
  ]
  node [
    id 639
    label "dotykanie"
  ]
  node [
    id 640
    label "klepa&#263;"
  ]
  node [
    id 641
    label "linia_&#380;ycia"
  ]
  node [
    id 642
    label "linia_rozumu"
  ]
  node [
    id 643
    label "poduszka"
  ]
  node [
    id 644
    label "dotyka&#263;"
  ]
  node [
    id 645
    label "kostka"
  ]
  node [
    id 646
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 647
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 648
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 649
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 650
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 651
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 652
    label "powerball"
  ]
  node [
    id 653
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 654
    label "polidaktylia"
  ]
  node [
    id 655
    label "dzia&#322;anie"
  ]
  node [
    id 656
    label "koniuszek_palca"
  ]
  node [
    id 657
    label "paznokie&#263;"
  ]
  node [
    id 658
    label "pazur"
  ]
  node [
    id 659
    label "element_anatomiczny"
  ]
  node [
    id 660
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 661
    label "zap&#322;on"
  ]
  node [
    id 662
    label "knykie&#263;"
  ]
  node [
    id 663
    label "palpacja"
  ]
  node [
    id 664
    label "zgi&#281;cie_&#322;okciowe"
  ]
  node [
    id 665
    label "r&#281;kaw"
  ]
  node [
    id 666
    label "miara"
  ]
  node [
    id 667
    label "d&#243;&#322;_&#322;okciowy"
  ]
  node [
    id 668
    label "listewka"
  ]
  node [
    id 669
    label "narz&#261;d_ruchu"
  ]
  node [
    id 670
    label "mi&#281;sie&#324;_&#322;okciowy"
  ]
  node [
    id 671
    label "ko&#347;&#263;_ramieniowa"
  ]
  node [
    id 672
    label "triceps"
  ]
  node [
    id 673
    label "element"
  ]
  node [
    id 674
    label "biceps"
  ]
  node [
    id 675
    label "robot_przemys&#322;owy"
  ]
  node [
    id 676
    label "ko&#347;&#263;_promieniowa"
  ]
  node [
    id 677
    label "ko&#347;&#263;_&#322;okciowa"
  ]
  node [
    id 678
    label "ko&#347;&#263;_czworoboczna_mniejsza"
  ]
  node [
    id 679
    label "metacarpus"
  ]
  node [
    id 680
    label "ko&#347;&#263;_czworoboczna_wi&#281;ksza"
  ]
  node [
    id 681
    label "cloud"
  ]
  node [
    id 682
    label "chmura"
  ]
  node [
    id 683
    label "p&#281;d"
  ]
  node [
    id 684
    label "grzbiet"
  ]
  node [
    id 685
    label "ob&#322;ok_Oorta"
  ]
  node [
    id 686
    label "pl&#261;tanina"
  ]
  node [
    id 687
    label "zjawisko"
  ]
  node [
    id 688
    label "oberwanie_si&#281;"
  ]
  node [
    id 689
    label "oberwa&#263;_si&#281;"
  ]
  node [
    id 690
    label "powderpuff"
  ]
  node [
    id 691
    label "burza"
  ]
  node [
    id 692
    label "r&#261;cz&#281;ta"
  ]
  node [
    id 693
    label "salariat"
  ]
  node [
    id 694
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 695
    label "delegowanie"
  ]
  node [
    id 696
    label "pracu&#347;"
  ]
  node [
    id 697
    label "delegowa&#263;"
  ]
  node [
    id 698
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 699
    label "kredens"
  ]
  node [
    id 700
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 701
    label "bylina"
  ]
  node [
    id 702
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 703
    label "wrzosowate"
  ]
  node [
    id 704
    label "pomagacz"
  ]
  node [
    id 705
    label "korona"
  ]
  node [
    id 706
    label "wymiociny"
  ]
  node [
    id 707
    label "ba&#380;anty"
  ]
  node [
    id 708
    label "ptak"
  ]
  node [
    id 709
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 710
    label "scali&#263;"
  ]
  node [
    id 711
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 712
    label "usun&#261;&#263;"
  ]
  node [
    id 713
    label "spowodowa&#263;"
  ]
  node [
    id 714
    label "fetter"
  ]
  node [
    id 715
    label "porazi&#263;"
  ]
  node [
    id 716
    label "os&#322;abi&#263;"
  ]
  node [
    id 717
    label "unieruchomi&#263;"
  ]
  node [
    id 718
    label "overwhelm"
  ]
  node [
    id 719
    label "act"
  ]
  node [
    id 720
    label "zjednoczy&#263;"
  ]
  node [
    id 721
    label "powi&#261;za&#263;"
  ]
  node [
    id 722
    label "ally"
  ]
  node [
    id 723
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 724
    label "connect"
  ]
  node [
    id 725
    label "withdraw"
  ]
  node [
    id 726
    label "motivate"
  ]
  node [
    id 727
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 728
    label "wyrugowa&#263;"
  ]
  node [
    id 729
    label "go"
  ]
  node [
    id 730
    label "undo"
  ]
  node [
    id 731
    label "zabi&#263;"
  ]
  node [
    id 732
    label "przenie&#347;&#263;"
  ]
  node [
    id 733
    label "przesun&#261;&#263;"
  ]
  node [
    id 734
    label "handcuff"
  ]
  node [
    id 735
    label "narz&#281;dzie_wymierzania_kary"
  ]
  node [
    id 736
    label "treat"
  ]
  node [
    id 737
    label "wychowywa&#263;"
  ]
  node [
    id 738
    label "robi&#263;"
  ]
  node [
    id 739
    label "pozostawa&#263;"
  ]
  node [
    id 740
    label "podtrzymywa&#263;"
  ]
  node [
    id 741
    label "dzier&#380;y&#263;"
  ]
  node [
    id 742
    label "zmusza&#263;"
  ]
  node [
    id 743
    label "continue"
  ]
  node [
    id 744
    label "przetrzymywa&#263;"
  ]
  node [
    id 745
    label "utrzymywa&#263;_si&#281;"
  ]
  node [
    id 746
    label "hodowa&#263;"
  ]
  node [
    id 747
    label "administrowa&#263;"
  ]
  node [
    id 748
    label "sympatyzowa&#263;"
  ]
  node [
    id 749
    label "argue"
  ]
  node [
    id 750
    label "adhere"
  ]
  node [
    id 751
    label "sprawowa&#263;"
  ]
  node [
    id 752
    label "zachowywa&#263;"
  ]
  node [
    id 753
    label "utrzymywa&#263;"
  ]
  node [
    id 754
    label "tajemnica"
  ]
  node [
    id 755
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 756
    label "zdyscyplinowanie"
  ]
  node [
    id 757
    label "post"
  ]
  node [
    id 758
    label "control"
  ]
  node [
    id 759
    label "przechowywa&#263;"
  ]
  node [
    id 760
    label "behave"
  ]
  node [
    id 761
    label "dieta"
  ]
  node [
    id 762
    label "hold"
  ]
  node [
    id 763
    label "post&#281;powa&#263;"
  ]
  node [
    id 764
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 765
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 766
    label "sandbag"
  ]
  node [
    id 767
    label "powodowa&#263;"
  ]
  node [
    id 768
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 769
    label "stay"
  ]
  node [
    id 770
    label "przeszkadza&#263;"
  ]
  node [
    id 771
    label "anticipate"
  ]
  node [
    id 772
    label "byt"
  ]
  node [
    id 773
    label "s&#261;dzi&#263;"
  ]
  node [
    id 774
    label "twierdzi&#263;"
  ]
  node [
    id 775
    label "zapewnia&#263;"
  ]
  node [
    id 776
    label "corroborate"
  ]
  node [
    id 777
    label "panowa&#263;"
  ]
  node [
    id 778
    label "defy"
  ]
  node [
    id 779
    label "broni&#263;"
  ]
  node [
    id 780
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 781
    label "prosecute"
  ]
  node [
    id 782
    label "by&#263;"
  ]
  node [
    id 783
    label "pociesza&#263;"
  ]
  node [
    id 784
    label "patronize"
  ]
  node [
    id 785
    label "reinforce"
  ]
  node [
    id 786
    label "back"
  ]
  node [
    id 787
    label "organizowa&#263;"
  ]
  node [
    id 788
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 789
    label "czyni&#263;"
  ]
  node [
    id 790
    label "give"
  ]
  node [
    id 791
    label "stylizowa&#263;"
  ]
  node [
    id 792
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 793
    label "falowa&#263;"
  ]
  node [
    id 794
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 795
    label "peddle"
  ]
  node [
    id 796
    label "praca"
  ]
  node [
    id 797
    label "wydala&#263;"
  ]
  node [
    id 798
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 799
    label "tentegowa&#263;"
  ]
  node [
    id 800
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 801
    label "urz&#261;dza&#263;"
  ]
  node [
    id 802
    label "oszukiwa&#263;"
  ]
  node [
    id 803
    label "ukazywa&#263;"
  ]
  node [
    id 804
    label "przerabia&#263;"
  ]
  node [
    id 805
    label "raise"
  ]
  node [
    id 806
    label "zapuszcza&#263;"
  ]
  node [
    id 807
    label "sprzyja&#263;"
  ]
  node [
    id 808
    label "blend"
  ]
  node [
    id 809
    label "stop"
  ]
  node [
    id 810
    label "przebywa&#263;"
  ]
  node [
    id 811
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 812
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 813
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 814
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 815
    label "support"
  ]
  node [
    id 816
    label "szkoli&#263;"
  ]
  node [
    id 817
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 818
    label "train"
  ]
  node [
    id 819
    label "cover"
  ]
  node [
    id 820
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 821
    label "dysponowa&#263;"
  ]
  node [
    id 822
    label "dzier&#380;e&#263;"
  ]
  node [
    id 823
    label "mie&#263;"
  ]
  node [
    id 824
    label "pryncypa&#322;"
  ]
  node [
    id 825
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 826
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 827
    label "kierowa&#263;"
  ]
  node [
    id 828
    label "alkohol"
  ]
  node [
    id 829
    label "zdolno&#347;&#263;"
  ]
  node [
    id 830
    label "&#380;ycie"
  ]
  node [
    id 831
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 832
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 833
    label "dekiel"
  ]
  node [
    id 834
    label "ro&#347;lina"
  ]
  node [
    id 835
    label "&#347;ci&#281;cie"
  ]
  node [
    id 836
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 837
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 838
    label "&#347;ci&#281;gno"
  ]
  node [
    id 839
    label "noosfera"
  ]
  node [
    id 840
    label "byd&#322;o"
  ]
  node [
    id 841
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 842
    label "makrocefalia"
  ]
  node [
    id 843
    label "ucho"
  ]
  node [
    id 844
    label "m&#243;zg"
  ]
  node [
    id 845
    label "kierownictwo"
  ]
  node [
    id 846
    label "fryzura"
  ]
  node [
    id 847
    label "umys&#322;"
  ]
  node [
    id 848
    label "cia&#322;o"
  ]
  node [
    id 849
    label "cz&#322;onek"
  ]
  node [
    id 850
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 851
    label "czaszka"
  ]
  node [
    id 852
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 853
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 854
    label "ptaszek"
  ]
  node [
    id 855
    label "przyrodzenie"
  ]
  node [
    id 856
    label "fiut"
  ]
  node [
    id 857
    label "shaft"
  ]
  node [
    id 858
    label "wchodzenie"
  ]
  node [
    id 859
    label "przedstawiciel"
  ]
  node [
    id 860
    label "wej&#347;cie"
  ]
  node [
    id 861
    label "przelezienie"
  ]
  node [
    id 862
    label "&#347;piew"
  ]
  node [
    id 863
    label "Synaj"
  ]
  node [
    id 864
    label "kierunek"
  ]
  node [
    id 865
    label "wysoki"
  ]
  node [
    id 866
    label "wzniesienie"
  ]
  node [
    id 867
    label "pi&#281;tro"
  ]
  node [
    id 868
    label "Ropa"
  ]
  node [
    id 869
    label "kupa"
  ]
  node [
    id 870
    label "przele&#378;&#263;"
  ]
  node [
    id 871
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 872
    label "karczek"
  ]
  node [
    id 873
    label "rami&#261;czko"
  ]
  node [
    id 874
    label "Jaworze"
  ]
  node [
    id 875
    label "przedzia&#322;ek"
  ]
  node [
    id 876
    label "pasemko"
  ]
  node [
    id 877
    label "fryz"
  ]
  node [
    id 878
    label "w&#322;osy"
  ]
  node [
    id 879
    label "grzywka"
  ]
  node [
    id 880
    label "egreta"
  ]
  node [
    id 881
    label "falownica"
  ]
  node [
    id 882
    label "fonta&#378;"
  ]
  node [
    id 883
    label "fryzura_intymna"
  ]
  node [
    id 884
    label "ozdoba"
  ]
  node [
    id 885
    label "pr&#243;bowanie"
  ]
  node [
    id 886
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 887
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 888
    label "realizacja"
  ]
  node [
    id 889
    label "didaskalia"
  ]
  node [
    id 890
    label "czyn"
  ]
  node [
    id 891
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 892
    label "environment"
  ]
  node [
    id 893
    label "head"
  ]
  node [
    id 894
    label "scenariusz"
  ]
  node [
    id 895
    label "jednostka"
  ]
  node [
    id 896
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 897
    label "utw&#243;r"
  ]
  node [
    id 898
    label "kultura_duchowa"
  ]
  node [
    id 899
    label "fortel"
  ]
  node [
    id 900
    label "theatrical_performance"
  ]
  node [
    id 901
    label "ambala&#380;"
  ]
  node [
    id 902
    label "kobieta"
  ]
  node [
    id 903
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 904
    label "Faust"
  ]
  node [
    id 905
    label "scenografia"
  ]
  node [
    id 906
    label "ods&#322;ona"
  ]
  node [
    id 907
    label "turn"
  ]
  node [
    id 908
    label "pokaz"
  ]
  node [
    id 909
    label "przedstawienie"
  ]
  node [
    id 910
    label "przedstawi&#263;"
  ]
  node [
    id 911
    label "Apollo"
  ]
  node [
    id 912
    label "kultura"
  ]
  node [
    id 913
    label "przedstawianie"
  ]
  node [
    id 914
    label "przedstawia&#263;"
  ]
  node [
    id 915
    label "towar"
  ]
  node [
    id 916
    label "posiada&#263;"
  ]
  node [
    id 917
    label "potencja&#322;"
  ]
  node [
    id 918
    label "zapomnienie"
  ]
  node [
    id 919
    label "zapomina&#263;"
  ]
  node [
    id 920
    label "zapominanie"
  ]
  node [
    id 921
    label "ability"
  ]
  node [
    id 922
    label "obliczeniowo"
  ]
  node [
    id 923
    label "zapomnie&#263;"
  ]
  node [
    id 924
    label "raj_utracony"
  ]
  node [
    id 925
    label "umieranie"
  ]
  node [
    id 926
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 927
    label "prze&#380;ywanie"
  ]
  node [
    id 928
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 929
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 930
    label "po&#322;&#243;g"
  ]
  node [
    id 931
    label "umarcie"
  ]
  node [
    id 932
    label "subsistence"
  ]
  node [
    id 933
    label "power"
  ]
  node [
    id 934
    label "okres_noworodkowy"
  ]
  node [
    id 935
    label "prze&#380;ycie"
  ]
  node [
    id 936
    label "wiek_matuzalemowy"
  ]
  node [
    id 937
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 938
    label "entity"
  ]
  node [
    id 939
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 940
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 941
    label "do&#380;ywanie"
  ]
  node [
    id 942
    label "dzieci&#324;stwo"
  ]
  node [
    id 943
    label "andropauza"
  ]
  node [
    id 944
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 945
    label "rozw&#243;j"
  ]
  node [
    id 946
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 947
    label "menopauza"
  ]
  node [
    id 948
    label "&#347;mier&#263;"
  ]
  node [
    id 949
    label "koleje_losu"
  ]
  node [
    id 950
    label "bycie"
  ]
  node [
    id 951
    label "zegar_biologiczny"
  ]
  node [
    id 952
    label "szwung"
  ]
  node [
    id 953
    label "przebywanie"
  ]
  node [
    id 954
    label "warunki"
  ]
  node [
    id 955
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 956
    label "niemowl&#281;ctwo"
  ]
  node [
    id 957
    label "&#380;ywy"
  ]
  node [
    id 958
    label "life"
  ]
  node [
    id 959
    label "staro&#347;&#263;"
  ]
  node [
    id 960
    label "energy"
  ]
  node [
    id 961
    label "mi&#281;sie&#324;"
  ]
  node [
    id 962
    label "napinacz"
  ]
  node [
    id 963
    label "czapka"
  ]
  node [
    id 964
    label "ucho_zewn&#281;trzne"
  ]
  node [
    id 965
    label "elektronystagmografia"
  ]
  node [
    id 966
    label "handle"
  ]
  node [
    id 967
    label "ochraniacz"
  ]
  node [
    id 968
    label "ma&#322;&#380;owina"
  ]
  node [
    id 969
    label "twarz"
  ]
  node [
    id 970
    label "ucho_wewn&#281;trzne"
  ]
  node [
    id 971
    label "uchwyt"
  ]
  node [
    id 972
    label "ucho_&#347;rodkowe"
  ]
  node [
    id 973
    label "przew&#243;d_s&#322;uchowy"
  ]
  node [
    id 974
    label "otw&#243;r"
  ]
  node [
    id 975
    label "szew_kostny"
  ]
  node [
    id 976
    label "trzewioczaszka"
  ]
  node [
    id 977
    label "siode&#322;ko_tureckie"
  ]
  node [
    id 978
    label "wa&#322;_nadoczodo&#322;owy"
  ]
  node [
    id 979
    label "m&#243;zgoczaszka"
  ]
  node [
    id 980
    label "ciemi&#281;"
  ]
  node [
    id 981
    label "ko&#347;&#263;_czo&#322;owa"
  ]
  node [
    id 982
    label "dynia"
  ]
  node [
    id 983
    label "ko&#347;&#263;_podniebienna"
  ]
  node [
    id 984
    label "rozszczep_czaszki"
  ]
  node [
    id 985
    label "szew_strza&#322;kowy"
  ]
  node [
    id 986
    label "puszka_m&#243;zgowa"
  ]
  node [
    id 987
    label "mak&#243;wka"
  ]
  node [
    id 988
    label "&#322;uk_jarzmowy"
  ]
  node [
    id 989
    label "ko&#347;&#263;_klinowa"
  ]
  node [
    id 990
    label "szkielet"
  ]
  node [
    id 991
    label "zatoka"
  ]
  node [
    id 992
    label "ma&#322;og&#322;owie"
  ]
  node [
    id 993
    label "oczod&#243;&#322;"
  ]
  node [
    id 994
    label "potylica"
  ]
  node [
    id 995
    label "lemiesz"
  ]
  node [
    id 996
    label "&#380;uchwa"
  ]
  node [
    id 997
    label "szczelina_oczodo&#322;owa"
  ]
  node [
    id 998
    label "po&#347;redniog&#322;owo&#347;&#263;"
  ]
  node [
    id 999
    label "diafanoskopia"
  ]
  node [
    id 1000
    label "&#322;eb"
  ]
  node [
    id 1001
    label "substancja_szara"
  ]
  node [
    id 1002
    label "encefalografia"
  ]
  node [
    id 1003
    label "przedmurze"
  ]
  node [
    id 1004
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 1005
    label "bruzda"
  ]
  node [
    id 1006
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 1007
    label "most"
  ]
  node [
    id 1008
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 1009
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 1010
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 1011
    label "podwzg&#243;rze"
  ]
  node [
    id 1012
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 1013
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 1014
    label "wzg&#243;rze"
  ]
  node [
    id 1015
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 1016
    label "elektroencefalogram"
  ]
  node [
    id 1017
    label "przodom&#243;zgowie"
  ]
  node [
    id 1018
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 1019
    label "projektodawca"
  ]
  node [
    id 1020
    label "przysadka"
  ]
  node [
    id 1021
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 1022
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 1023
    label "zw&#243;j"
  ]
  node [
    id 1024
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 1025
    label "kora_m&#243;zgowa"
  ]
  node [
    id 1026
    label "kresom&#243;zgowie"
  ]
  node [
    id 1027
    label "intelekt"
  ]
  node [
    id 1028
    label "lid"
  ]
  node [
    id 1029
    label "ko&#322;o"
  ]
  node [
    id 1030
    label "pokrywa"
  ]
  node [
    id 1031
    label "dekielek"
  ]
  node [
    id 1032
    label "os&#322;ona"
  ]
  node [
    id 1033
    label "g&#322;upek"
  ]
  node [
    id 1034
    label "g&#322;os"
  ]
  node [
    id 1035
    label "ekshumowanie"
  ]
  node [
    id 1036
    label "p&#322;aszczyzna"
  ]
  node [
    id 1037
    label "odwadnia&#263;"
  ]
  node [
    id 1038
    label "zabalsamowanie"
  ]
  node [
    id 1039
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 1040
    label "odwodni&#263;"
  ]
  node [
    id 1041
    label "sk&#243;ra"
  ]
  node [
    id 1042
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 1043
    label "staw"
  ]
  node [
    id 1044
    label "ow&#322;osienie"
  ]
  node [
    id 1045
    label "mi&#281;so"
  ]
  node [
    id 1046
    label "zabalsamowa&#263;"
  ]
  node [
    id 1047
    label "Izba_Konsyliarska"
  ]
  node [
    id 1048
    label "unerwienie"
  ]
  node [
    id 1049
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 1050
    label "kremacja"
  ]
  node [
    id 1051
    label "biorytm"
  ]
  node [
    id 1052
    label "sekcja"
  ]
  node [
    id 1053
    label "istota_&#380;ywa"
  ]
  node [
    id 1054
    label "otworzy&#263;"
  ]
  node [
    id 1055
    label "otwiera&#263;"
  ]
  node [
    id 1056
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 1057
    label "otworzenie"
  ]
  node [
    id 1058
    label "materia"
  ]
  node [
    id 1059
    label "pochowanie"
  ]
  node [
    id 1060
    label "otwieranie"
  ]
  node [
    id 1061
    label "tanatoplastyk"
  ]
  node [
    id 1062
    label "odwadnianie"
  ]
  node [
    id 1063
    label "Komitet_Region&#243;w"
  ]
  node [
    id 1064
    label "odwodnienie"
  ]
  node [
    id 1065
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 1066
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 1067
    label "pochowa&#263;"
  ]
  node [
    id 1068
    label "tanatoplastyka"
  ]
  node [
    id 1069
    label "balsamowa&#263;"
  ]
  node [
    id 1070
    label "nieumar&#322;y"
  ]
  node [
    id 1071
    label "temperatura"
  ]
  node [
    id 1072
    label "balsamowanie"
  ]
  node [
    id 1073
    label "ekshumowa&#263;"
  ]
  node [
    id 1074
    label "l&#281;d&#378;wie"
  ]
  node [
    id 1075
    label "pogrzeb"
  ]
  node [
    id 1076
    label "zbiorowisko"
  ]
  node [
    id 1077
    label "ro&#347;liny"
  ]
  node [
    id 1078
    label "wegetowanie"
  ]
  node [
    id 1079
    label "zadziorek"
  ]
  node [
    id 1080
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 1081
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 1082
    label "do&#322;owa&#263;"
  ]
  node [
    id 1083
    label "wegetacja"
  ]
  node [
    id 1084
    label "owoc"
  ]
  node [
    id 1085
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 1086
    label "strzyc"
  ]
  node [
    id 1087
    label "w&#322;&#243;kno"
  ]
  node [
    id 1088
    label "g&#322;uszenie"
  ]
  node [
    id 1089
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 1090
    label "fitotron"
  ]
  node [
    id 1091
    label "bulwka"
  ]
  node [
    id 1092
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 1093
    label "odn&#243;&#380;ka"
  ]
  node [
    id 1094
    label "epiderma"
  ]
  node [
    id 1095
    label "gumoza"
  ]
  node [
    id 1096
    label "strzy&#380;enie"
  ]
  node [
    id 1097
    label "wypotnik"
  ]
  node [
    id 1098
    label "flawonoid"
  ]
  node [
    id 1099
    label "wyro&#347;le"
  ]
  node [
    id 1100
    label "do&#322;owanie"
  ]
  node [
    id 1101
    label "g&#322;uszy&#263;"
  ]
  node [
    id 1102
    label "pora&#380;a&#263;"
  ]
  node [
    id 1103
    label "fitocenoza"
  ]
  node [
    id 1104
    label "hodowla"
  ]
  node [
    id 1105
    label "fotoautotrof"
  ]
  node [
    id 1106
    label "nieuleczalnie_chory"
  ]
  node [
    id 1107
    label "wegetowa&#263;"
  ]
  node [
    id 1108
    label "pochewka"
  ]
  node [
    id 1109
    label "sok"
  ]
  node [
    id 1110
    label "system_korzeniowy"
  ]
  node [
    id 1111
    label "zawi&#261;zek"
  ]
  node [
    id 1112
    label "pami&#281;&#263;"
  ]
  node [
    id 1113
    label "pomieszanie_si&#281;"
  ]
  node [
    id 1114
    label "wn&#281;trze"
  ]
  node [
    id 1115
    label "wyobra&#378;nia"
  ]
  node [
    id 1116
    label "obci&#281;cie"
  ]
  node [
    id 1117
    label "decapitation"
  ]
  node [
    id 1118
    label "zdarzenie_si&#281;"
  ]
  node [
    id 1119
    label "opitolenie"
  ]
  node [
    id 1120
    label "poobcinanie"
  ]
  node [
    id 1121
    label "zmro&#380;enie"
  ]
  node [
    id 1122
    label "snub"
  ]
  node [
    id 1123
    label "kr&#243;j"
  ]
  node [
    id 1124
    label "oblanie"
  ]
  node [
    id 1125
    label "przeegzaminowanie"
  ]
  node [
    id 1126
    label "ping-pong"
  ]
  node [
    id 1127
    label "cut"
  ]
  node [
    id 1128
    label "gilotyna"
  ]
  node [
    id 1129
    label "szafot"
  ]
  node [
    id 1130
    label "skr&#243;cenie"
  ]
  node [
    id 1131
    label "zniszczenie"
  ]
  node [
    id 1132
    label "siatk&#243;wka"
  ]
  node [
    id 1133
    label "k&#322;&#243;tnia"
  ]
  node [
    id 1134
    label "ukszta&#322;towanie"
  ]
  node [
    id 1135
    label "splay"
  ]
  node [
    id 1136
    label "zabicie"
  ]
  node [
    id 1137
    label "tenis"
  ]
  node [
    id 1138
    label "usuni&#281;cie"
  ]
  node [
    id 1139
    label "odci&#281;cie"
  ]
  node [
    id 1140
    label "st&#281;&#380;enie"
  ]
  node [
    id 1141
    label "chop"
  ]
  node [
    id 1142
    label "wada_wrodzona"
  ]
  node [
    id 1143
    label "decapitate"
  ]
  node [
    id 1144
    label "obci&#261;&#263;"
  ]
  node [
    id 1145
    label "naruszy&#263;"
  ]
  node [
    id 1146
    label "obni&#380;y&#263;"
  ]
  node [
    id 1147
    label "okroi&#263;"
  ]
  node [
    id 1148
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1149
    label "zaci&#261;&#263;"
  ]
  node [
    id 1150
    label "uderzy&#263;"
  ]
  node [
    id 1151
    label "obla&#263;"
  ]
  node [
    id 1152
    label "odbi&#263;"
  ]
  node [
    id 1153
    label "skr&#243;ci&#263;"
  ]
  node [
    id 1154
    label "pozbawi&#263;"
  ]
  node [
    id 1155
    label "opitoli&#263;"
  ]
  node [
    id 1156
    label "wywo&#322;a&#263;"
  ]
  node [
    id 1157
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 1158
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 1159
    label "odci&#261;&#263;"
  ]
  node [
    id 1160
    label "write_out"
  ]
  node [
    id 1161
    label "kr&#281;torogie"
  ]
  node [
    id 1162
    label "czochrad&#322;o"
  ]
  node [
    id 1163
    label "posp&#243;lstwo"
  ]
  node [
    id 1164
    label "kraal"
  ]
  node [
    id 1165
    label "livestock"
  ]
  node [
    id 1166
    label "u&#380;ywka"
  ]
  node [
    id 1167
    label "najebka"
  ]
  node [
    id 1168
    label "upajanie"
  ]
  node [
    id 1169
    label "szk&#322;o"
  ]
  node [
    id 1170
    label "wypicie"
  ]
  node [
    id 1171
    label "rozgrzewacz"
  ]
  node [
    id 1172
    label "nap&#243;j"
  ]
  node [
    id 1173
    label "alko"
  ]
  node [
    id 1174
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 1175
    label "picie"
  ]
  node [
    id 1176
    label "upojenie"
  ]
  node [
    id 1177
    label "upija&#263;"
  ]
  node [
    id 1178
    label "likwor"
  ]
  node [
    id 1179
    label "poniewierca"
  ]
  node [
    id 1180
    label "grupa_hydroksylowa"
  ]
  node [
    id 1181
    label "spirytualia"
  ]
  node [
    id 1182
    label "le&#380;akownia"
  ]
  node [
    id 1183
    label "upi&#263;"
  ]
  node [
    id 1184
    label "piwniczka"
  ]
  node [
    id 1185
    label "gorzelnia_rolnicza"
  ]
  node [
    id 1186
    label "sterowa&#263;"
  ]
  node [
    id 1187
    label "wysy&#322;a&#263;"
  ]
  node [
    id 1188
    label "manipulate"
  ]
  node [
    id 1189
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 1190
    label "ustawia&#263;"
  ]
  node [
    id 1191
    label "przeznacza&#263;"
  ]
  node [
    id 1192
    label "match"
  ]
  node [
    id 1193
    label "motywowa&#263;"
  ]
  node [
    id 1194
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 1195
    label "indicate"
  ]
  node [
    id 1196
    label "cognition"
  ]
  node [
    id 1197
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 1198
    label "pozwolenie"
  ]
  node [
    id 1199
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1200
    label "zaawansowanie"
  ]
  node [
    id 1201
    label "wykszta&#322;cenie"
  ]
  node [
    id 1202
    label "lead"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 17
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 18
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 419
  ]
  edge [
    source 12
    target 420
  ]
  edge [
    source 12
    target 421
  ]
  edge [
    source 12
    target 422
  ]
  edge [
    source 12
    target 423
  ]
  edge [
    source 12
    target 424
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 308
  ]
  edge [
    source 12
    target 425
  ]
  edge [
    source 12
    target 426
  ]
  edge [
    source 12
    target 427
  ]
  edge [
    source 12
    target 428
  ]
  edge [
    source 12
    target 313
  ]
  edge [
    source 12
    target 429
  ]
  edge [
    source 12
    target 430
  ]
  edge [
    source 12
    target 431
  ]
  edge [
    source 12
    target 299
  ]
  edge [
    source 12
    target 432
  ]
  edge [
    source 12
    target 26
  ]
  edge [
    source 12
    target 207
  ]
  edge [
    source 12
    target 433
  ]
  edge [
    source 12
    target 434
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 435
  ]
  edge [
    source 12
    target 436
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 202
  ]
  edge [
    source 12
    target 325
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 331
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 334
  ]
  edge [
    source 12
    target 301
  ]
  edge [
    source 12
    target 302
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 303
  ]
  edge [
    source 12
    target 304
  ]
  edge [
    source 12
    target 305
  ]
  edge [
    source 12
    target 306
  ]
  edge [
    source 12
    target 307
  ]
  edge [
    source 12
    target 309
  ]
  edge [
    source 12
    target 310
  ]
  edge [
    source 12
    target 311
  ]
  edge [
    source 12
    target 312
  ]
  edge [
    source 12
    target 314
  ]
  edge [
    source 12
    target 315
  ]
  edge [
    source 12
    target 291
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 12
    target 322
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 324
  ]
  edge [
    source 12
    target 326
  ]
  edge [
    source 12
    target 327
  ]
  edge [
    source 12
    target 328
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 329
  ]
  edge [
    source 12
    target 330
  ]
  edge [
    source 12
    target 332
  ]
  edge [
    source 12
    target 333
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 12
    target 492
  ]
  edge [
    source 12
    target 493
  ]
  edge [
    source 12
    target 494
  ]
  edge [
    source 12
    target 495
  ]
  edge [
    source 12
    target 496
  ]
  edge [
    source 12
    target 292
  ]
  edge [
    source 12
    target 497
  ]
  edge [
    source 12
    target 498
  ]
  edge [
    source 12
    target 499
  ]
  edge [
    source 12
    target 500
  ]
  edge [
    source 12
    target 501
  ]
  edge [
    source 12
    target 502
  ]
  edge [
    source 12
    target 503
  ]
  edge [
    source 12
    target 504
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 226
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 13
    target 588
  ]
  edge [
    source 13
    target 589
  ]
  edge [
    source 13
    target 590
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 591
  ]
  edge [
    source 13
    target 592
  ]
  edge [
    source 13
    target 593
  ]
  edge [
    source 13
    target 594
  ]
  edge [
    source 13
    target 595
  ]
  edge [
    source 13
    target 596
  ]
  edge [
    source 13
    target 597
  ]
  edge [
    source 13
    target 598
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 655
  ]
  edge [
    source 13
    target 656
  ]
  edge [
    source 13
    target 657
  ]
  edge [
    source 13
    target 658
  ]
  edge [
    source 13
    target 659
  ]
  edge [
    source 13
    target 660
  ]
  edge [
    source 13
    target 661
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 662
  ]
  edge [
    source 13
    target 663
  ]
  edge [
    source 13
    target 664
  ]
  edge [
    source 13
    target 665
  ]
  edge [
    source 13
    target 666
  ]
  edge [
    source 13
    target 667
  ]
  edge [
    source 13
    target 668
  ]
  edge [
    source 13
    target 669
  ]
  edge [
    source 13
    target 670
  ]
  edge [
    source 13
    target 671
  ]
  edge [
    source 13
    target 672
  ]
  edge [
    source 13
    target 673
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 674
  ]
  edge [
    source 13
    target 675
  ]
  edge [
    source 13
    target 676
  ]
  edge [
    source 13
    target 677
  ]
  edge [
    source 13
    target 678
  ]
  edge [
    source 13
    target 679
  ]
  edge [
    source 13
    target 680
  ]
  edge [
    source 13
    target 681
  ]
  edge [
    source 13
    target 682
  ]
  edge [
    source 13
    target 683
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 684
  ]
  edge [
    source 13
    target 685
  ]
  edge [
    source 13
    target 686
  ]
  edge [
    source 13
    target 687
  ]
  edge [
    source 13
    target 688
  ]
  edge [
    source 13
    target 689
  ]
  edge [
    source 13
    target 690
  ]
  edge [
    source 13
    target 691
  ]
  edge [
    source 13
    target 692
  ]
  edge [
    source 13
    target 693
  ]
  edge [
    source 13
    target 694
  ]
  edge [
    source 13
    target 292
  ]
  edge [
    source 13
    target 695
  ]
  edge [
    source 13
    target 696
  ]
  edge [
    source 13
    target 697
  ]
  edge [
    source 13
    target 698
  ]
  edge [
    source 13
    target 699
  ]
  edge [
    source 13
    target 700
  ]
  edge [
    source 13
    target 701
  ]
  edge [
    source 13
    target 702
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 703
  ]
  edge [
    source 13
    target 704
  ]
  edge [
    source 13
    target 705
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 707
  ]
  edge [
    source 13
    target 708
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 709
  ]
  edge [
    source 14
    target 710
  ]
  edge [
    source 14
    target 711
  ]
  edge [
    source 14
    target 712
  ]
  edge [
    source 14
    target 713
  ]
  edge [
    source 14
    target 714
  ]
  edge [
    source 14
    target 715
  ]
  edge [
    source 14
    target 716
  ]
  edge [
    source 14
    target 717
  ]
  edge [
    source 14
    target 718
  ]
  edge [
    source 14
    target 719
  ]
  edge [
    source 14
    target 720
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 722
  ]
  edge [
    source 14
    target 723
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 725
  ]
  edge [
    source 14
    target 726
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 728
  ]
  edge [
    source 14
    target 729
  ]
  edge [
    source 14
    target 730
  ]
  edge [
    source 14
    target 731
  ]
  edge [
    source 14
    target 732
  ]
  edge [
    source 14
    target 733
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 573
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 17
    target 824
  ]
  edge [
    source 17
    target 825
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 826
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 292
  ]
  edge [
    source 17
    target 827
  ]
  edge [
    source 17
    target 828
  ]
  edge [
    source 17
    target 829
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 17
    target 830
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 832
  ]
  edge [
    source 17
    target 660
  ]
  edge [
    source 17
    target 247
  ]
  edge [
    source 17
    target 833
  ]
  edge [
    source 17
    target 834
  ]
  edge [
    source 17
    target 835
  ]
  edge [
    source 17
    target 836
  ]
  edge [
    source 17
    target 837
  ]
  edge [
    source 17
    target 838
  ]
  edge [
    source 17
    target 839
  ]
  edge [
    source 17
    target 840
  ]
  edge [
    source 17
    target 841
  ]
  edge [
    source 17
    target 842
  ]
  edge [
    source 17
    target 39
  ]
  edge [
    source 17
    target 843
  ]
  edge [
    source 17
    target 172
  ]
  edge [
    source 17
    target 844
  ]
  edge [
    source 17
    target 845
  ]
  edge [
    source 17
    target 846
  ]
  edge [
    source 17
    target 847
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 850
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 852
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 853
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 854
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 659
  ]
  edge [
    source 17
    target 855
  ]
  edge [
    source 17
    target 856
  ]
  edge [
    source 17
    target 857
  ]
  edge [
    source 17
    target 858
  ]
  edge [
    source 17
    target 319
  ]
  edge [
    source 17
    target 859
  ]
  edge [
    source 17
    target 860
  ]
  edge [
    source 17
    target 364
  ]
  edge [
    source 17
    target 365
  ]
  edge [
    source 17
    target 366
  ]
  edge [
    source 17
    target 367
  ]
  edge [
    source 17
    target 368
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 369
  ]
  edge [
    source 17
    target 370
  ]
  edge [
    source 17
    target 371
  ]
  edge [
    source 17
    target 372
  ]
  edge [
    source 17
    target 373
  ]
  edge [
    source 17
    target 374
  ]
  edge [
    source 17
    target 375
  ]
  edge [
    source 17
    target 376
  ]
  edge [
    source 17
    target 377
  ]
  edge [
    source 17
    target 378
  ]
  edge [
    source 17
    target 379
  ]
  edge [
    source 17
    target 380
  ]
  edge [
    source 17
    target 381
  ]
  edge [
    source 17
    target 382
  ]
  edge [
    source 17
    target 383
  ]
  edge [
    source 17
    target 384
  ]
  edge [
    source 17
    target 385
  ]
  edge [
    source 17
    target 386
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 861
  ]
  edge [
    source 17
    target 862
  ]
  edge [
    source 17
    target 863
  ]
  edge [
    source 17
    target 499
  ]
  edge [
    source 17
    target 589
  ]
  edge [
    source 17
    target 864
  ]
  edge [
    source 17
    target 865
  ]
  edge [
    source 17
    target 673
  ]
  edge [
    source 17
    target 866
  ]
  edge [
    source 17
    target 867
  ]
  edge [
    source 17
    target 868
  ]
  edge [
    source 17
    target 869
  ]
  edge [
    source 17
    target 870
  ]
  edge [
    source 17
    target 871
  ]
  edge [
    source 17
    target 872
  ]
  edge [
    source 17
    target 873
  ]
  edge [
    source 17
    target 874
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 875
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 876
  ]
  edge [
    source 17
    target 877
  ]
  edge [
    source 17
    target 878
  ]
  edge [
    source 17
    target 879
  ]
  edge [
    source 17
    target 880
  ]
  edge [
    source 17
    target 881
  ]
  edge [
    source 17
    target 882
  ]
  edge [
    source 17
    target 883
  ]
  edge [
    source 17
    target 884
  ]
  edge [
    source 17
    target 885
  ]
  edge [
    source 17
    target 263
  ]
  edge [
    source 17
    target 886
  ]
  edge [
    source 17
    target 887
  ]
  edge [
    source 17
    target 888
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 890
  ]
  edge [
    source 17
    target 891
  ]
  edge [
    source 17
    target 892
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 894
  ]
  edge [
    source 17
    target 273
  ]
  edge [
    source 17
    target 895
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 897
  ]
  edge [
    source 17
    target 898
  ]
  edge [
    source 17
    target 899
  ]
  edge [
    source 17
    target 900
  ]
  edge [
    source 17
    target 901
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 902
  ]
  edge [
    source 17
    target 903
  ]
  edge [
    source 17
    target 904
  ]
  edge [
    source 17
    target 905
  ]
  edge [
    source 17
    target 906
  ]
  edge [
    source 17
    target 907
  ]
  edge [
    source 17
    target 908
  ]
  edge [
    source 17
    target 77
  ]
  edge [
    source 17
    target 909
  ]
  edge [
    source 17
    target 910
  ]
  edge [
    source 17
    target 911
  ]
  edge [
    source 17
    target 912
  ]
  edge [
    source 17
    target 913
  ]
  edge [
    source 17
    target 914
  ]
  edge [
    source 17
    target 915
  ]
  edge [
    source 17
    target 916
  ]
  edge [
    source 17
    target 917
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 919
  ]
  edge [
    source 17
    target 920
  ]
  edge [
    source 17
    target 921
  ]
  edge [
    source 17
    target 922
  ]
  edge [
    source 17
    target 923
  ]
  edge [
    source 17
    target 924
  ]
  edge [
    source 17
    target 925
  ]
  edge [
    source 17
    target 926
  ]
  edge [
    source 17
    target 927
  ]
  edge [
    source 17
    target 928
  ]
  edge [
    source 17
    target 929
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 231
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 772
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 352
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 555
  ]
  edge [
    source 17
    target 556
  ]
  edge [
    source 17
    target 557
  ]
  edge [
    source 17
    target 558
  ]
  edge [
    source 17
    target 559
  ]
  edge [
    source 17
    target 560
  ]
  edge [
    source 17
    target 561
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 489
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 78
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 272
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 52
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 167
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 683
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 17
    target 1115
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1118
  ]
  edge [
    source 17
    target 1119
  ]
  edge [
    source 17
    target 1120
  ]
  edge [
    source 17
    target 1121
  ]
  edge [
    source 17
    target 1122
  ]
  edge [
    source 17
    target 1123
  ]
  edge [
    source 17
    target 1124
  ]
  edge [
    source 17
    target 1125
  ]
  edge [
    source 17
    target 542
  ]
  edge [
    source 17
    target 433
  ]
  edge [
    source 17
    target 574
  ]
  edge [
    source 17
    target 1126
  ]
  edge [
    source 17
    target 1127
  ]
  edge [
    source 17
    target 1128
  ]
  edge [
    source 17
    target 1129
  ]
  edge [
    source 17
    target 1130
  ]
  edge [
    source 17
    target 1131
  ]
  edge [
    source 17
    target 592
  ]
  edge [
    source 17
    target 1132
  ]
  edge [
    source 17
    target 1133
  ]
  edge [
    source 17
    target 1134
  ]
  edge [
    source 17
    target 1135
  ]
  edge [
    source 17
    target 1136
  ]
  edge [
    source 17
    target 1137
  ]
  edge [
    source 17
    target 1138
  ]
  edge [
    source 17
    target 1139
  ]
  edge [
    source 17
    target 1140
  ]
  edge [
    source 17
    target 1141
  ]
  edge [
    source 17
    target 1142
  ]
  edge [
    source 17
    target 1143
  ]
  edge [
    source 17
    target 712
  ]
  edge [
    source 17
    target 1144
  ]
  edge [
    source 17
    target 1145
  ]
  edge [
    source 17
    target 1146
  ]
  edge [
    source 17
    target 1147
  ]
  edge [
    source 17
    target 1148
  ]
  edge [
    source 17
    target 1149
  ]
  edge [
    source 17
    target 1150
  ]
  edge [
    source 17
    target 1151
  ]
  edge [
    source 17
    target 1152
  ]
  edge [
    source 17
    target 1153
  ]
  edge [
    source 17
    target 1154
  ]
  edge [
    source 17
    target 1155
  ]
  edge [
    source 17
    target 731
  ]
  edge [
    source 17
    target 713
  ]
  edge [
    source 17
    target 1156
  ]
  edge [
    source 17
    target 717
  ]
  edge [
    source 17
    target 1157
  ]
  edge [
    source 17
    target 1158
  ]
  edge [
    source 17
    target 1159
  ]
  edge [
    source 17
    target 1160
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 59
  ]
  edge [
    source 17
    target 60
  ]
  edge [
    source 17
    target 61
  ]
  edge [
    source 17
    target 62
  ]
  edge [
    source 17
    target 63
  ]
  edge [
    source 17
    target 64
  ]
  edge [
    source 17
    target 65
  ]
  edge [
    source 17
    target 66
  ]
  edge [
    source 17
    target 67
  ]
  edge [
    source 17
    target 68
  ]
  edge [
    source 17
    target 69
  ]
  edge [
    source 17
    target 71
  ]
  edge [
    source 17
    target 72
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 17
    target 1161
  ]
  edge [
    source 17
    target 1162
  ]
  edge [
    source 17
    target 1163
  ]
  edge [
    source 17
    target 1164
  ]
  edge [
    source 17
    target 1165
  ]
  edge [
    source 17
    target 1166
  ]
  edge [
    source 17
    target 1167
  ]
  edge [
    source 17
    target 1168
  ]
  edge [
    source 17
    target 1169
  ]
  edge [
    source 17
    target 1170
  ]
  edge [
    source 17
    target 1171
  ]
  edge [
    source 17
    target 1172
  ]
  edge [
    source 17
    target 1173
  ]
  edge [
    source 17
    target 1174
  ]
  edge [
    source 17
    target 1175
  ]
  edge [
    source 17
    target 1176
  ]
  edge [
    source 17
    target 1177
  ]
  edge [
    source 17
    target 1178
  ]
  edge [
    source 17
    target 1179
  ]
  edge [
    source 17
    target 1180
  ]
  edge [
    source 17
    target 1181
  ]
  edge [
    source 17
    target 1182
  ]
  edge [
    source 17
    target 1183
  ]
  edge [
    source 17
    target 1184
  ]
  edge [
    source 17
    target 1185
  ]
  edge [
    source 17
    target 1186
  ]
  edge [
    source 17
    target 1187
  ]
  edge [
    source 17
    target 1188
  ]
  edge [
    source 17
    target 764
  ]
  edge [
    source 17
    target 1189
  ]
  edge [
    source 17
    target 1190
  ]
  edge [
    source 17
    target 790
  ]
  edge [
    source 17
    target 1191
  ]
  edge [
    source 17
    target 758
  ]
  edge [
    source 17
    target 1192
  ]
  edge [
    source 17
    target 1193
  ]
  edge [
    source 17
    target 747
  ]
  edge [
    source 17
    target 820
  ]
  edge [
    source 17
    target 1194
  ]
  edge [
    source 17
    target 597
  ]
  edge [
    source 17
    target 1195
  ]
  edge [
    source 17
    target 1196
  ]
  edge [
    source 17
    target 1197
  ]
  edge [
    source 17
    target 1198
  ]
  edge [
    source 17
    target 1199
  ]
  edge [
    source 17
    target 1200
  ]
  edge [
    source 17
    target 1201
  ]
  edge [
    source 17
    target 25
  ]
  edge [
    source 17
    target 455
  ]
  edge [
    source 17
    target 1202
  ]
  edge [
    source 17
    target 361
  ]
  edge [
    source 17
    target 796
  ]
  edge [
    source 17
    target 432
  ]
]
