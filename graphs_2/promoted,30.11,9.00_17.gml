graph [
  node [
    id 0
    label "kultura"
    origin "text"
  ]
  node [
    id 1
    label "wspiera&#263;"
    origin "text"
  ]
  node [
    id 2
    label "lokalny"
    origin "text"
  ]
  node [
    id 3
    label "marek"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "obecna"
    origin "text"
  ]
  node [
    id 6
    label "niemcy"
    origin "text"
  ]
  node [
    id 7
    label "usa"
    origin "text"
  ]
  node [
    id 8
    label "francja"
    origin "text"
  ]
  node [
    id 9
    label "japonia"
    origin "text"
  ]
  node [
    id 10
    label "lato"
    origin "text"
  ]
  node [
    id 11
    label "asymilowanie_si&#281;"
  ]
  node [
    id 12
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 13
    label "Wsch&#243;d"
  ]
  node [
    id 14
    label "przedmiot"
  ]
  node [
    id 15
    label "praca_rolnicza"
  ]
  node [
    id 16
    label "przejmowanie"
  ]
  node [
    id 17
    label "zjawisko"
  ]
  node [
    id 18
    label "cecha"
  ]
  node [
    id 19
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 20
    label "makrokosmos"
  ]
  node [
    id 21
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 22
    label "konwencja"
  ]
  node [
    id 23
    label "rzecz"
  ]
  node [
    id 24
    label "propriety"
  ]
  node [
    id 25
    label "przejmowa&#263;"
  ]
  node [
    id 26
    label "brzoskwiniarnia"
  ]
  node [
    id 27
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 28
    label "sztuka"
  ]
  node [
    id 29
    label "zwyczaj"
  ]
  node [
    id 30
    label "jako&#347;&#263;"
  ]
  node [
    id 31
    label "kuchnia"
  ]
  node [
    id 32
    label "tradycja"
  ]
  node [
    id 33
    label "populace"
  ]
  node [
    id 34
    label "hodowla"
  ]
  node [
    id 35
    label "religia"
  ]
  node [
    id 36
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 37
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 38
    label "przej&#281;cie"
  ]
  node [
    id 39
    label "przej&#261;&#263;"
  ]
  node [
    id 40
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 42
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 43
    label "warto&#347;&#263;"
  ]
  node [
    id 44
    label "quality"
  ]
  node [
    id 45
    label "co&#347;"
  ]
  node [
    id 46
    label "state"
  ]
  node [
    id 47
    label "syf"
  ]
  node [
    id 48
    label "absolutorium"
  ]
  node [
    id 49
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 50
    label "dzia&#322;anie"
  ]
  node [
    id 51
    label "activity"
  ]
  node [
    id 52
    label "proces"
  ]
  node [
    id 53
    label "boski"
  ]
  node [
    id 54
    label "krajobraz"
  ]
  node [
    id 55
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 56
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 57
    label "przywidzenie"
  ]
  node [
    id 58
    label "presence"
  ]
  node [
    id 59
    label "charakter"
  ]
  node [
    id 60
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 61
    label "potrzymanie"
  ]
  node [
    id 62
    label "rolnictwo"
  ]
  node [
    id 63
    label "pod&#243;j"
  ]
  node [
    id 64
    label "filiacja"
  ]
  node [
    id 65
    label "licencjonowanie"
  ]
  node [
    id 66
    label "opasa&#263;"
  ]
  node [
    id 67
    label "ch&#243;w"
  ]
  node [
    id 68
    label "licencja"
  ]
  node [
    id 69
    label "sokolarnia"
  ]
  node [
    id 70
    label "potrzyma&#263;"
  ]
  node [
    id 71
    label "rozp&#322;&#243;d"
  ]
  node [
    id 72
    label "grupa_organizm&#243;w"
  ]
  node [
    id 73
    label "wypas"
  ]
  node [
    id 74
    label "wychowalnia"
  ]
  node [
    id 75
    label "pstr&#261;garnia"
  ]
  node [
    id 76
    label "krzy&#380;owanie"
  ]
  node [
    id 77
    label "licencjonowa&#263;"
  ]
  node [
    id 78
    label "odch&#243;w"
  ]
  node [
    id 79
    label "tucz"
  ]
  node [
    id 80
    label "ud&#243;j"
  ]
  node [
    id 81
    label "klatka"
  ]
  node [
    id 82
    label "opasienie"
  ]
  node [
    id 83
    label "wych&#243;w"
  ]
  node [
    id 84
    label "obrz&#261;dek"
  ]
  node [
    id 85
    label "opasanie"
  ]
  node [
    id 86
    label "polish"
  ]
  node [
    id 87
    label "akwarium"
  ]
  node [
    id 88
    label "biotechnika"
  ]
  node [
    id 89
    label "Europejska_Konwencja_Praw_Cz&#322;owieka"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  node [
    id 91
    label "uk&#322;ad"
  ]
  node [
    id 92
    label "styl"
  ]
  node [
    id 93
    label "line"
  ]
  node [
    id 94
    label "kanon"
  ]
  node [
    id 95
    label "zjazd"
  ]
  node [
    id 96
    label "charakterystyka"
  ]
  node [
    id 97
    label "m&#322;ot"
  ]
  node [
    id 98
    label "znak"
  ]
  node [
    id 99
    label "drzewo"
  ]
  node [
    id 100
    label "pr&#243;ba"
  ]
  node [
    id 101
    label "attribute"
  ]
  node [
    id 102
    label "marka"
  ]
  node [
    id 103
    label "biom"
  ]
  node [
    id 104
    label "szata_ro&#347;linna"
  ]
  node [
    id 105
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 106
    label "formacja_ro&#347;linna"
  ]
  node [
    id 107
    label "przyroda"
  ]
  node [
    id 108
    label "zielono&#347;&#263;"
  ]
  node [
    id 109
    label "pi&#281;tro"
  ]
  node [
    id 110
    label "plant"
  ]
  node [
    id 111
    label "ro&#347;lina"
  ]
  node [
    id 112
    label "geosystem"
  ]
  node [
    id 113
    label "kult"
  ]
  node [
    id 114
    label "mitologia"
  ]
  node [
    id 115
    label "wyznanie"
  ]
  node [
    id 116
    label "ideologia"
  ]
  node [
    id 117
    label "nawraca&#263;_si&#281;"
  ]
  node [
    id 118
    label "zwi&#261;zek_wyznaniowy"
  ]
  node [
    id 119
    label "nawracanie_si&#281;"
  ]
  node [
    id 120
    label "duchowny"
  ]
  node [
    id 121
    label "rela"
  ]
  node [
    id 122
    label "kultura_duchowa"
  ]
  node [
    id 123
    label "kosmologia"
  ]
  node [
    id 124
    label "kosmogonia"
  ]
  node [
    id 125
    label "nawraca&#263;"
  ]
  node [
    id 126
    label "mistyka"
  ]
  node [
    id 127
    label "pr&#243;bowanie"
  ]
  node [
    id 128
    label "rola"
  ]
  node [
    id 129
    label "cz&#322;owiek"
  ]
  node [
    id 130
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 131
    label "realizacja"
  ]
  node [
    id 132
    label "scena"
  ]
  node [
    id 133
    label "didaskalia"
  ]
  node [
    id 134
    label "czyn"
  ]
  node [
    id 135
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 136
    label "environment"
  ]
  node [
    id 137
    label "head"
  ]
  node [
    id 138
    label "scenariusz"
  ]
  node [
    id 139
    label "egzemplarz"
  ]
  node [
    id 140
    label "jednostka"
  ]
  node [
    id 141
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 142
    label "utw&#243;r"
  ]
  node [
    id 143
    label "fortel"
  ]
  node [
    id 144
    label "theatrical_performance"
  ]
  node [
    id 145
    label "ambala&#380;"
  ]
  node [
    id 146
    label "sprawno&#347;&#263;"
  ]
  node [
    id 147
    label "kobieta"
  ]
  node [
    id 148
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 149
    label "Faust"
  ]
  node [
    id 150
    label "scenografia"
  ]
  node [
    id 151
    label "ods&#322;ona"
  ]
  node [
    id 152
    label "turn"
  ]
  node [
    id 153
    label "pokaz"
  ]
  node [
    id 154
    label "ilo&#347;&#263;"
  ]
  node [
    id 155
    label "przedstawienie"
  ]
  node [
    id 156
    label "przedstawi&#263;"
  ]
  node [
    id 157
    label "Apollo"
  ]
  node [
    id 158
    label "przedstawianie"
  ]
  node [
    id 159
    label "przedstawia&#263;"
  ]
  node [
    id 160
    label "towar"
  ]
  node [
    id 161
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 162
    label "zachowanie"
  ]
  node [
    id 163
    label "ceremony"
  ]
  node [
    id 164
    label "dorobek"
  ]
  node [
    id 165
    label "tworzenie"
  ]
  node [
    id 166
    label "kreacja"
  ]
  node [
    id 167
    label "creation"
  ]
  node [
    id 168
    label "staro&#347;cina_weselna"
  ]
  node [
    id 169
    label "folklor"
  ]
  node [
    id 170
    label "objawienie"
  ]
  node [
    id 171
    label "zaj&#281;cie"
  ]
  node [
    id 172
    label "instytucja"
  ]
  node [
    id 173
    label "tajniki"
  ]
  node [
    id 174
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 175
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 176
    label "jedzenie"
  ]
  node [
    id 177
    label "zaplecze"
  ]
  node [
    id 178
    label "pomieszczenie"
  ]
  node [
    id 179
    label "zlewozmywak"
  ]
  node [
    id 180
    label "gotowa&#263;"
  ]
  node [
    id 181
    label "ciemna_materia"
  ]
  node [
    id 182
    label "planeta"
  ]
  node [
    id 183
    label "mikrokosmos"
  ]
  node [
    id 184
    label "ekosfera"
  ]
  node [
    id 185
    label "przestrze&#324;"
  ]
  node [
    id 186
    label "czarna_dziura"
  ]
  node [
    id 187
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 188
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 189
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 190
    label "kosmos"
  ]
  node [
    id 191
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 192
    label "poprawno&#347;&#263;"
  ]
  node [
    id 193
    label "og&#322;ada"
  ]
  node [
    id 194
    label "service"
  ]
  node [
    id 195
    label "stosowno&#347;&#263;"
  ]
  node [
    id 196
    label "uleg&#322;o&#347;&#263;"
  ]
  node [
    id 197
    label "Ukraina"
  ]
  node [
    id 198
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 199
    label "blok_wschodni"
  ]
  node [
    id 200
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 201
    label "wsch&#243;d"
  ]
  node [
    id 202
    label "Europa_Wschodnia"
  ]
  node [
    id 203
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 204
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 205
    label "wra&#380;enie"
  ]
  node [
    id 206
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 207
    label "interception"
  ]
  node [
    id 208
    label "wzbudzenie"
  ]
  node [
    id 209
    label "emotion"
  ]
  node [
    id 210
    label "movement"
  ]
  node [
    id 211
    label "zaczerpni&#281;cie"
  ]
  node [
    id 212
    label "wzi&#281;cie"
  ]
  node [
    id 213
    label "bang"
  ]
  node [
    id 214
    label "wzi&#261;&#263;"
  ]
  node [
    id 215
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 216
    label "stimulate"
  ]
  node [
    id 217
    label "ogarn&#261;&#263;"
  ]
  node [
    id 218
    label "wzbudzi&#263;"
  ]
  node [
    id 219
    label "thrill"
  ]
  node [
    id 220
    label "treat"
  ]
  node [
    id 221
    label "czerpa&#263;"
  ]
  node [
    id 222
    label "bra&#263;"
  ]
  node [
    id 223
    label "go"
  ]
  node [
    id 224
    label "handle"
  ]
  node [
    id 225
    label "wzbudza&#263;"
  ]
  node [
    id 226
    label "ogarnia&#263;"
  ]
  node [
    id 227
    label "czerpanie"
  ]
  node [
    id 228
    label "acquisition"
  ]
  node [
    id 229
    label "branie"
  ]
  node [
    id 230
    label "caparison"
  ]
  node [
    id 231
    label "wzbudzanie"
  ]
  node [
    id 232
    label "czynno&#347;&#263;"
  ]
  node [
    id 233
    label "ogarnianie"
  ]
  node [
    id 234
    label "object"
  ]
  node [
    id 235
    label "temat"
  ]
  node [
    id 236
    label "wpadni&#281;cie"
  ]
  node [
    id 237
    label "mienie"
  ]
  node [
    id 238
    label "istota"
  ]
  node [
    id 239
    label "obiekt"
  ]
  node [
    id 240
    label "wpa&#347;&#263;"
  ]
  node [
    id 241
    label "wpadanie"
  ]
  node [
    id 242
    label "wpada&#263;"
  ]
  node [
    id 243
    label "zboczenie"
  ]
  node [
    id 244
    label "om&#243;wienie"
  ]
  node [
    id 245
    label "sponiewieranie"
  ]
  node [
    id 246
    label "discipline"
  ]
  node [
    id 247
    label "omawia&#263;"
  ]
  node [
    id 248
    label "kr&#261;&#380;enie"
  ]
  node [
    id 249
    label "tre&#347;&#263;"
  ]
  node [
    id 250
    label "robienie"
  ]
  node [
    id 251
    label "sponiewiera&#263;"
  ]
  node [
    id 252
    label "element"
  ]
  node [
    id 253
    label "entity"
  ]
  node [
    id 254
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 255
    label "tematyka"
  ]
  node [
    id 256
    label "w&#261;tek"
  ]
  node [
    id 257
    label "zbaczanie"
  ]
  node [
    id 258
    label "program_nauczania"
  ]
  node [
    id 259
    label "om&#243;wi&#263;"
  ]
  node [
    id 260
    label "omawianie"
  ]
  node [
    id 261
    label "thing"
  ]
  node [
    id 262
    label "zbacza&#263;"
  ]
  node [
    id 263
    label "zboczy&#263;"
  ]
  node [
    id 264
    label "miejsce"
  ]
  node [
    id 265
    label "uprawa"
  ]
  node [
    id 266
    label "pociesza&#263;"
  ]
  node [
    id 267
    label "u&#322;atwia&#263;"
  ]
  node [
    id 268
    label "sprzyja&#263;"
  ]
  node [
    id 269
    label "opiera&#263;"
  ]
  node [
    id 270
    label "Warszawa"
  ]
  node [
    id 271
    label "back"
  ]
  node [
    id 272
    label "pomaga&#263;"
  ]
  node [
    id 273
    label "&#322;atwi&#263;"
  ]
  node [
    id 274
    label "powodowa&#263;"
  ]
  node [
    id 275
    label "ease"
  ]
  node [
    id 276
    label "robi&#263;"
  ]
  node [
    id 277
    label "czu&#263;"
  ]
  node [
    id 278
    label "stanowi&#263;"
  ]
  node [
    id 279
    label "chowa&#263;"
  ]
  node [
    id 280
    label "osnowywa&#263;"
  ]
  node [
    id 281
    label "stawia&#263;"
  ]
  node [
    id 282
    label "digest"
  ]
  node [
    id 283
    label "cover"
  ]
  node [
    id 284
    label "warszawa"
  ]
  node [
    id 285
    label "Powi&#347;le"
  ]
  node [
    id 286
    label "Wawa"
  ]
  node [
    id 287
    label "syreni_gr&#243;d"
  ]
  node [
    id 288
    label "Wawer"
  ]
  node [
    id 289
    label "W&#322;ochy"
  ]
  node [
    id 290
    label "Ursyn&#243;w"
  ]
  node [
    id 291
    label "Bielany"
  ]
  node [
    id 292
    label "Weso&#322;a"
  ]
  node [
    id 293
    label "Bia&#322;o&#322;&#281;ka"
  ]
  node [
    id 294
    label "Targ&#243;wek"
  ]
  node [
    id 295
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 296
    label "Muran&#243;w"
  ]
  node [
    id 297
    label "Warsiawa"
  ]
  node [
    id 298
    label "Ursus"
  ]
  node [
    id 299
    label "Ochota"
  ]
  node [
    id 300
    label "Marymont"
  ]
  node [
    id 301
    label "Ujazd&#243;w"
  ]
  node [
    id 302
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 303
    label "Solec"
  ]
  node [
    id 304
    label "Bemowo"
  ]
  node [
    id 305
    label "Mokot&#243;w"
  ]
  node [
    id 306
    label "Wilan&#243;w"
  ]
  node [
    id 307
    label "warszawka"
  ]
  node [
    id 308
    label "varsaviana"
  ]
  node [
    id 309
    label "Wola"
  ]
  node [
    id 310
    label "Rembert&#243;w"
  ]
  node [
    id 311
    label "Praga"
  ]
  node [
    id 312
    label "&#379;oliborz"
  ]
  node [
    id 313
    label "lokalnie"
  ]
  node [
    id 314
    label "hygrofit"
  ]
  node [
    id 315
    label "bylina"
  ]
  node [
    id 316
    label "selerowate"
  ]
  node [
    id 317
    label "higrofil"
  ]
  node [
    id 318
    label "ludowy"
  ]
  node [
    id 319
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 320
    label "utw&#243;r_epicki"
  ]
  node [
    id 321
    label "pie&#347;&#324;"
  ]
  node [
    id 322
    label "selerowce"
  ]
  node [
    id 323
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 324
    label "mie&#263;_miejsce"
  ]
  node [
    id 325
    label "equal"
  ]
  node [
    id 326
    label "trwa&#263;"
  ]
  node [
    id 327
    label "chodzi&#263;"
  ]
  node [
    id 328
    label "si&#281;ga&#263;"
  ]
  node [
    id 329
    label "stan"
  ]
  node [
    id 330
    label "obecno&#347;&#263;"
  ]
  node [
    id 331
    label "stand"
  ]
  node [
    id 332
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 333
    label "uczestniczy&#263;"
  ]
  node [
    id 334
    label "participate"
  ]
  node [
    id 335
    label "istnie&#263;"
  ]
  node [
    id 336
    label "pozostawa&#263;"
  ]
  node [
    id 337
    label "zostawa&#263;"
  ]
  node [
    id 338
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 339
    label "adhere"
  ]
  node [
    id 340
    label "compass"
  ]
  node [
    id 341
    label "korzysta&#263;"
  ]
  node [
    id 342
    label "appreciation"
  ]
  node [
    id 343
    label "osi&#261;ga&#263;"
  ]
  node [
    id 344
    label "dociera&#263;"
  ]
  node [
    id 345
    label "get"
  ]
  node [
    id 346
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 347
    label "mierzy&#263;"
  ]
  node [
    id 348
    label "u&#380;ywa&#263;"
  ]
  node [
    id 349
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 350
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 351
    label "exsert"
  ]
  node [
    id 352
    label "being"
  ]
  node [
    id 353
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 354
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 355
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 356
    label "p&#322;ywa&#263;"
  ]
  node [
    id 357
    label "run"
  ]
  node [
    id 358
    label "bangla&#263;"
  ]
  node [
    id 359
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 360
    label "przebiega&#263;"
  ]
  node [
    id 361
    label "wk&#322;ada&#263;"
  ]
  node [
    id 362
    label "proceed"
  ]
  node [
    id 363
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 364
    label "carry"
  ]
  node [
    id 365
    label "bywa&#263;"
  ]
  node [
    id 366
    label "dziama&#263;"
  ]
  node [
    id 367
    label "stara&#263;_si&#281;"
  ]
  node [
    id 368
    label "para"
  ]
  node [
    id 369
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 370
    label "str&#243;j"
  ]
  node [
    id 371
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 372
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 373
    label "krok"
  ]
  node [
    id 374
    label "tryb"
  ]
  node [
    id 375
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 376
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 377
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 378
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 379
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 380
    label "continue"
  ]
  node [
    id 381
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 382
    label "Ohio"
  ]
  node [
    id 383
    label "wci&#281;cie"
  ]
  node [
    id 384
    label "Nowy_York"
  ]
  node [
    id 385
    label "warstwa"
  ]
  node [
    id 386
    label "samopoczucie"
  ]
  node [
    id 387
    label "Illinois"
  ]
  node [
    id 388
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 389
    label "Jukatan"
  ]
  node [
    id 390
    label "Kalifornia"
  ]
  node [
    id 391
    label "Wirginia"
  ]
  node [
    id 392
    label "wektor"
  ]
  node [
    id 393
    label "Goa"
  ]
  node [
    id 394
    label "Teksas"
  ]
  node [
    id 395
    label "Waszyngton"
  ]
  node [
    id 396
    label "Massachusetts"
  ]
  node [
    id 397
    label "Alaska"
  ]
  node [
    id 398
    label "Arakan"
  ]
  node [
    id 399
    label "Hawaje"
  ]
  node [
    id 400
    label "Maryland"
  ]
  node [
    id 401
    label "punkt"
  ]
  node [
    id 402
    label "Michigan"
  ]
  node [
    id 403
    label "Arizona"
  ]
  node [
    id 404
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 405
    label "Georgia"
  ]
  node [
    id 406
    label "poziom"
  ]
  node [
    id 407
    label "Pensylwania"
  ]
  node [
    id 408
    label "shape"
  ]
  node [
    id 409
    label "Luizjana"
  ]
  node [
    id 410
    label "Nowy_Meksyk"
  ]
  node [
    id 411
    label "Alabama"
  ]
  node [
    id 412
    label "Kansas"
  ]
  node [
    id 413
    label "Oregon"
  ]
  node [
    id 414
    label "Oklahoma"
  ]
  node [
    id 415
    label "Floryda"
  ]
  node [
    id 416
    label "jednostka_administracyjna"
  ]
  node [
    id 417
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 418
    label "pora_roku"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 418
  ]
]
