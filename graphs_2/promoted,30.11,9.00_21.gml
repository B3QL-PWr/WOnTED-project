graph [
  node [
    id 0
    label "napastnik"
    origin "text"
  ]
  node [
    id 1
    label "kilka"
    origin "text"
  ]
  node [
    id 2
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "temu"
    origin "text"
  ]
  node [
    id 4
    label "wyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wi&#281;zienie"
    origin "text"
  ]
  node [
    id 6
    label "Messi"
  ]
  node [
    id 7
    label "zawodnik"
  ]
  node [
    id 8
    label "napadzior"
  ]
  node [
    id 9
    label "gracz"
  ]
  node [
    id 10
    label "atak"
  ]
  node [
    id 11
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 12
    label "walka"
  ]
  node [
    id 13
    label "liga"
  ]
  node [
    id 14
    label "oznaka"
  ]
  node [
    id 15
    label "pogorszenie"
  ]
  node [
    id 16
    label "przemoc"
  ]
  node [
    id 17
    label "krytyka"
  ]
  node [
    id 18
    label "bat"
  ]
  node [
    id 19
    label "kaszel"
  ]
  node [
    id 20
    label "fit"
  ]
  node [
    id 21
    label "rzuci&#263;"
  ]
  node [
    id 22
    label "spasm"
  ]
  node [
    id 23
    label "zagrywka"
  ]
  node [
    id 24
    label "wypowied&#378;"
  ]
  node [
    id 25
    label "&#380;&#261;danie"
  ]
  node [
    id 26
    label "manewr"
  ]
  node [
    id 27
    label "przyp&#322;yw"
  ]
  node [
    id 28
    label "ofensywa"
  ]
  node [
    id 29
    label "pogoda"
  ]
  node [
    id 30
    label "stroke"
  ]
  node [
    id 31
    label "pozycja"
  ]
  node [
    id 32
    label "rzucenie"
  ]
  node [
    id 33
    label "knock"
  ]
  node [
    id 34
    label "zi&#243;&#322;ko"
  ]
  node [
    id 35
    label "czo&#322;&#243;wka"
  ]
  node [
    id 36
    label "uczestnik"
  ]
  node [
    id 37
    label "lista_startowa"
  ]
  node [
    id 38
    label "sportowiec"
  ]
  node [
    id 39
    label "orygina&#322;"
  ]
  node [
    id 40
    label "facet"
  ]
  node [
    id 41
    label "cz&#322;owiek"
  ]
  node [
    id 42
    label "posta&#263;"
  ]
  node [
    id 43
    label "zwierz&#281;"
  ]
  node [
    id 44
    label "bohater"
  ]
  node [
    id 45
    label "spryciarz"
  ]
  node [
    id 46
    label "rozdawa&#263;_karty"
  ]
  node [
    id 47
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 48
    label "sprawca"
  ]
  node [
    id 49
    label "Atomowa_Pch&#322;a"
  ]
  node [
    id 50
    label "ryba"
  ]
  node [
    id 51
    label "&#347;ledziowate"
  ]
  node [
    id 52
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 53
    label "kr&#281;gowiec"
  ]
  node [
    id 54
    label "systemik"
  ]
  node [
    id 55
    label "doniczkowiec"
  ]
  node [
    id 56
    label "mi&#281;so"
  ]
  node [
    id 57
    label "system"
  ]
  node [
    id 58
    label "patroszy&#263;"
  ]
  node [
    id 59
    label "rakowato&#347;&#263;"
  ]
  node [
    id 60
    label "w&#281;dkarstwo"
  ]
  node [
    id 61
    label "ryby"
  ]
  node [
    id 62
    label "fish"
  ]
  node [
    id 63
    label "linia_boczna"
  ]
  node [
    id 64
    label "tar&#322;o"
  ]
  node [
    id 65
    label "wyrostek_filtracyjny"
  ]
  node [
    id 66
    label "m&#281;tnooki"
  ]
  node [
    id 67
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 68
    label "pokrywa_skrzelowa"
  ]
  node [
    id 69
    label "ikra"
  ]
  node [
    id 70
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 71
    label "szczelina_skrzelowa"
  ]
  node [
    id 72
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 73
    label "ranek"
  ]
  node [
    id 74
    label "doba"
  ]
  node [
    id 75
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 76
    label "noc"
  ]
  node [
    id 77
    label "podwiecz&#243;r"
  ]
  node [
    id 78
    label "po&#322;udnie"
  ]
  node [
    id 79
    label "godzina"
  ]
  node [
    id 80
    label "przedpo&#322;udnie"
  ]
  node [
    id 81
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 82
    label "long_time"
  ]
  node [
    id 83
    label "wiecz&#243;r"
  ]
  node [
    id 84
    label "t&#322;usty_czwartek"
  ]
  node [
    id 85
    label "popo&#322;udnie"
  ]
  node [
    id 86
    label "walentynki"
  ]
  node [
    id 87
    label "czynienie_si&#281;"
  ]
  node [
    id 88
    label "s&#322;o&#324;ce"
  ]
  node [
    id 89
    label "rano"
  ]
  node [
    id 90
    label "tydzie&#324;"
  ]
  node [
    id 91
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 92
    label "wzej&#347;cie"
  ]
  node [
    id 93
    label "czas"
  ]
  node [
    id 94
    label "wsta&#263;"
  ]
  node [
    id 95
    label "day"
  ]
  node [
    id 96
    label "termin"
  ]
  node [
    id 97
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 98
    label "wstanie"
  ]
  node [
    id 99
    label "przedwiecz&#243;r"
  ]
  node [
    id 100
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 101
    label "Sylwester"
  ]
  node [
    id 102
    label "poprzedzanie"
  ]
  node [
    id 103
    label "czasoprzestrze&#324;"
  ]
  node [
    id 104
    label "laba"
  ]
  node [
    id 105
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 106
    label "chronometria"
  ]
  node [
    id 107
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 108
    label "rachuba_czasu"
  ]
  node [
    id 109
    label "przep&#322;ywanie"
  ]
  node [
    id 110
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 111
    label "czasokres"
  ]
  node [
    id 112
    label "odczyt"
  ]
  node [
    id 113
    label "chwila"
  ]
  node [
    id 114
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 115
    label "dzieje"
  ]
  node [
    id 116
    label "kategoria_gramatyczna"
  ]
  node [
    id 117
    label "poprzedzenie"
  ]
  node [
    id 118
    label "trawienie"
  ]
  node [
    id 119
    label "pochodzi&#263;"
  ]
  node [
    id 120
    label "period"
  ]
  node [
    id 121
    label "okres_czasu"
  ]
  node [
    id 122
    label "poprzedza&#263;"
  ]
  node [
    id 123
    label "schy&#322;ek"
  ]
  node [
    id 124
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 125
    label "odwlekanie_si&#281;"
  ]
  node [
    id 126
    label "zegar"
  ]
  node [
    id 127
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 128
    label "czwarty_wymiar"
  ]
  node [
    id 129
    label "pochodzenie"
  ]
  node [
    id 130
    label "koniugacja"
  ]
  node [
    id 131
    label "Zeitgeist"
  ]
  node [
    id 132
    label "trawi&#263;"
  ]
  node [
    id 133
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 134
    label "poprzedzi&#263;"
  ]
  node [
    id 135
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 136
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 137
    label "time_period"
  ]
  node [
    id 138
    label "nazewnictwo"
  ]
  node [
    id 139
    label "term"
  ]
  node [
    id 140
    label "przypadni&#281;cie"
  ]
  node [
    id 141
    label "ekspiracja"
  ]
  node [
    id 142
    label "przypa&#347;&#263;"
  ]
  node [
    id 143
    label "chronogram"
  ]
  node [
    id 144
    label "praktyka"
  ]
  node [
    id 145
    label "nazwa"
  ]
  node [
    id 146
    label "odwieczerz"
  ]
  node [
    id 147
    label "pora"
  ]
  node [
    id 148
    label "przyj&#281;cie"
  ]
  node [
    id 149
    label "spotkanie"
  ]
  node [
    id 150
    label "night"
  ]
  node [
    id 151
    label "zach&#243;d"
  ]
  node [
    id 152
    label "vesper"
  ]
  node [
    id 153
    label "aurora"
  ]
  node [
    id 154
    label "wsch&#243;d"
  ]
  node [
    id 155
    label "zjawisko"
  ]
  node [
    id 156
    label "&#347;rodek"
  ]
  node [
    id 157
    label "obszar"
  ]
  node [
    id 158
    label "Ziemia"
  ]
  node [
    id 159
    label "dwunasta"
  ]
  node [
    id 160
    label "strona_&#347;wiata"
  ]
  node [
    id 161
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 162
    label "dopo&#322;udnie"
  ]
  node [
    id 163
    label "blady_&#347;wit"
  ]
  node [
    id 164
    label "podkurek"
  ]
  node [
    id 165
    label "time"
  ]
  node [
    id 166
    label "p&#243;&#322;godzina"
  ]
  node [
    id 167
    label "jednostka_czasu"
  ]
  node [
    id 168
    label "minuta"
  ]
  node [
    id 169
    label "kwadrans"
  ]
  node [
    id 170
    label "p&#243;&#322;noc"
  ]
  node [
    id 171
    label "nokturn"
  ]
  node [
    id 172
    label "jednostka_geologiczna"
  ]
  node [
    id 173
    label "weekend"
  ]
  node [
    id 174
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 175
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 176
    label "miesi&#261;c"
  ]
  node [
    id 177
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 178
    label "mount"
  ]
  node [
    id 179
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 180
    label "wzej&#347;&#263;"
  ]
  node [
    id 181
    label "ascend"
  ]
  node [
    id 182
    label "kuca&#263;"
  ]
  node [
    id 183
    label "wyzdrowie&#263;"
  ]
  node [
    id 184
    label "opu&#347;ci&#263;"
  ]
  node [
    id 185
    label "rise"
  ]
  node [
    id 186
    label "arise"
  ]
  node [
    id 187
    label "stan&#261;&#263;"
  ]
  node [
    id 188
    label "przesta&#263;"
  ]
  node [
    id 189
    label "wyzdrowienie"
  ]
  node [
    id 190
    label "le&#380;enie"
  ]
  node [
    id 191
    label "kl&#281;czenie"
  ]
  node [
    id 192
    label "opuszczenie"
  ]
  node [
    id 193
    label "uniesienie_si&#281;"
  ]
  node [
    id 194
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 195
    label "siedzenie"
  ]
  node [
    id 196
    label "beginning"
  ]
  node [
    id 197
    label "przestanie"
  ]
  node [
    id 198
    label "S&#322;o&#324;ce"
  ]
  node [
    id 199
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 200
    label "&#347;wiat&#322;o"
  ]
  node [
    id 201
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 202
    label "kochanie"
  ]
  node [
    id 203
    label "sunlight"
  ]
  node [
    id 204
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 205
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 206
    label "grudzie&#324;"
  ]
  node [
    id 207
    label "luty"
  ]
  node [
    id 208
    label "ograniczenie"
  ]
  node [
    id 209
    label "drop"
  ]
  node [
    id 210
    label "ruszy&#263;"
  ]
  node [
    id 211
    label "zademonstrowa&#263;"
  ]
  node [
    id 212
    label "leave"
  ]
  node [
    id 213
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 214
    label "uko&#324;czy&#263;"
  ]
  node [
    id 215
    label "ukaza&#263;_si&#281;_drukiem"
  ]
  node [
    id 216
    label "uwolni&#263;_si&#281;"
  ]
  node [
    id 217
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 218
    label "get"
  ]
  node [
    id 219
    label "moderate"
  ]
  node [
    id 220
    label "wywie&#347;&#263;_si&#281;"
  ]
  node [
    id 221
    label "sko&#324;czy&#263;"
  ]
  node [
    id 222
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 223
    label "zej&#347;&#263;"
  ]
  node [
    id 224
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 225
    label "wystarczy&#263;"
  ]
  node [
    id 226
    label "perform"
  ]
  node [
    id 227
    label "open"
  ]
  node [
    id 228
    label "drive"
  ]
  node [
    id 229
    label "zagra&#263;"
  ]
  node [
    id 230
    label "uzyska&#263;"
  ]
  node [
    id 231
    label "wypa&#347;&#263;"
  ]
  node [
    id 232
    label "motivate"
  ]
  node [
    id 233
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 234
    label "zabra&#263;"
  ]
  node [
    id 235
    label "go"
  ]
  node [
    id 236
    label "zrobi&#263;"
  ]
  node [
    id 237
    label "allude"
  ]
  node [
    id 238
    label "cut"
  ]
  node [
    id 239
    label "spowodowa&#263;"
  ]
  node [
    id 240
    label "stimulate"
  ]
  node [
    id 241
    label "zacz&#261;&#263;"
  ]
  node [
    id 242
    label "wzbudzi&#263;"
  ]
  node [
    id 243
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 244
    label "end"
  ]
  node [
    id 245
    label "zako&#324;czy&#263;"
  ]
  node [
    id 246
    label "communicate"
  ]
  node [
    id 247
    label "przedstawi&#263;"
  ]
  node [
    id 248
    label "pokaza&#263;"
  ]
  node [
    id 249
    label "wyrazi&#263;"
  ]
  node [
    id 250
    label "attest"
  ]
  node [
    id 251
    label "indicate"
  ]
  node [
    id 252
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 253
    label "realize"
  ]
  node [
    id 254
    label "promocja"
  ]
  node [
    id 255
    label "make"
  ]
  node [
    id 256
    label "wytworzy&#263;"
  ]
  node [
    id 257
    label "give_birth"
  ]
  node [
    id 258
    label "wynikn&#261;&#263;"
  ]
  node [
    id 259
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 260
    label "fall"
  ]
  node [
    id 261
    label "condescend"
  ]
  node [
    id 262
    label "proceed"
  ]
  node [
    id 263
    label "become"
  ]
  node [
    id 264
    label "temat"
  ]
  node [
    id 265
    label "uby&#263;"
  ]
  node [
    id 266
    label "umrze&#263;"
  ]
  node [
    id 267
    label "za&#347;piewa&#263;"
  ]
  node [
    id 268
    label "obni&#380;y&#263;"
  ]
  node [
    id 269
    label "przenie&#347;&#263;_si&#281;"
  ]
  node [
    id 270
    label "distract"
  ]
  node [
    id 271
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 272
    label "zu&#380;y&#263;_si&#281;"
  ]
  node [
    id 273
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 274
    label "obni&#380;y&#263;_si&#281;"
  ]
  node [
    id 275
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 276
    label "odpa&#347;&#263;"
  ]
  node [
    id 277
    label "die"
  ]
  node [
    id 278
    label "zboczy&#263;"
  ]
  node [
    id 279
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 280
    label "wprowadzi&#263;"
  ]
  node [
    id 281
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 282
    label "odej&#347;&#263;"
  ]
  node [
    id 283
    label "zgin&#261;&#263;"
  ]
  node [
    id 284
    label "write_down"
  ]
  node [
    id 285
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 286
    label "pozostawi&#263;"
  ]
  node [
    id 287
    label "zostawi&#263;"
  ]
  node [
    id 288
    label "potani&#263;"
  ]
  node [
    id 289
    label "evacuate"
  ]
  node [
    id 290
    label "humiliate"
  ]
  node [
    id 291
    label "tekst"
  ]
  node [
    id 292
    label "straci&#263;"
  ]
  node [
    id 293
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 294
    label "authorize"
  ]
  node [
    id 295
    label "omin&#261;&#263;"
  ]
  node [
    id 296
    label "suffice"
  ]
  node [
    id 297
    label "zaspokoi&#263;"
  ]
  node [
    id 298
    label "dosta&#263;"
  ]
  node [
    id 299
    label "play"
  ]
  node [
    id 300
    label "zabrzmie&#263;"
  ]
  node [
    id 301
    label "instrument_muzyczny"
  ]
  node [
    id 302
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 303
    label "flare"
  ]
  node [
    id 304
    label "rozegra&#263;"
  ]
  node [
    id 305
    label "zaszczeka&#263;"
  ]
  node [
    id 306
    label "sound"
  ]
  node [
    id 307
    label "represent"
  ]
  node [
    id 308
    label "wykorzysta&#263;"
  ]
  node [
    id 309
    label "zatokowa&#263;"
  ]
  node [
    id 310
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 311
    label "uda&#263;_si&#281;"
  ]
  node [
    id 312
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 313
    label "wykona&#263;"
  ]
  node [
    id 314
    label "typify"
  ]
  node [
    id 315
    label "rola"
  ]
  node [
    id 316
    label "profit"
  ]
  node [
    id 317
    label "score"
  ]
  node [
    id 318
    label "dotrze&#263;"
  ]
  node [
    id 319
    label "dropiowate"
  ]
  node [
    id 320
    label "kania"
  ]
  node [
    id 321
    label "bustard"
  ]
  node [
    id 322
    label "ptak"
  ]
  node [
    id 323
    label "g&#322;upstwo"
  ]
  node [
    id 324
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 325
    label "prevention"
  ]
  node [
    id 326
    label "pomiarkowanie"
  ]
  node [
    id 327
    label "przeszkoda"
  ]
  node [
    id 328
    label "intelekt"
  ]
  node [
    id 329
    label "zmniejszenie"
  ]
  node [
    id 330
    label "reservation"
  ]
  node [
    id 331
    label "przekroczenie"
  ]
  node [
    id 332
    label "finlandyzacja"
  ]
  node [
    id 333
    label "otoczenie"
  ]
  node [
    id 334
    label "osielstwo"
  ]
  node [
    id 335
    label "zdyskryminowanie"
  ]
  node [
    id 336
    label "warunek"
  ]
  node [
    id 337
    label "limitation"
  ]
  node [
    id 338
    label "cecha"
  ]
  node [
    id 339
    label "przekroczy&#263;"
  ]
  node [
    id 340
    label "przekraczanie"
  ]
  node [
    id 341
    label "przekracza&#263;"
  ]
  node [
    id 342
    label "barrier"
  ]
  node [
    id 343
    label "pierdel"
  ]
  node [
    id 344
    label "&#321;ubianka"
  ]
  node [
    id 345
    label "przerwa_w_&#380;yciorysie"
  ]
  node [
    id 346
    label "ciupa"
  ]
  node [
    id 347
    label "reedukator"
  ]
  node [
    id 348
    label "pozbawienie_wolno&#347;ci"
  ]
  node [
    id 349
    label "Butyrki"
  ]
  node [
    id 350
    label "imprisonment"
  ]
  node [
    id 351
    label "miejsce_odosobnienia"
  ]
  node [
    id 352
    label "ogranicza&#263;"
  ]
  node [
    id 353
    label "uniemo&#380;liwianie"
  ]
  node [
    id 354
    label "sytuacja"
  ]
  node [
    id 355
    label "przeszkadzanie"
  ]
  node [
    id 356
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 357
    label "warunki"
  ]
  node [
    id 358
    label "szczeg&#243;&#322;"
  ]
  node [
    id 359
    label "state"
  ]
  node [
    id 360
    label "motyw"
  ]
  node [
    id 361
    label "realia"
  ]
  node [
    id 362
    label "alfabet_wi&#281;zienny"
  ]
  node [
    id 363
    label "odsiadka"
  ]
  node [
    id 364
    label "nauczyciel"
  ]
  node [
    id 365
    label "pracownik"
  ]
  node [
    id 366
    label "dom_poprawczy"
  ]
  node [
    id 367
    label "Monar"
  ]
  node [
    id 368
    label "rehabilitant"
  ]
  node [
    id 369
    label "suppress"
  ]
  node [
    id 370
    label "wytycza&#263;"
  ]
  node [
    id 371
    label "zmniejsza&#263;"
  ]
  node [
    id 372
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 373
    label "environment"
  ]
  node [
    id 374
    label "bound"
  ]
  node [
    id 375
    label "stanowi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
]
