graph [
  node [
    id 0
    label "miejski"
    origin "text"
  ]
  node [
    id 1
    label "strona"
    origin "text"
  ]
  node [
    id 2
    label "ksi&#281;&#380;yc"
    origin "text"
  ]
  node [
    id 3
    label "publiczny"
  ]
  node [
    id 4
    label "typowy"
  ]
  node [
    id 5
    label "miastowy"
  ]
  node [
    id 6
    label "miejsko"
  ]
  node [
    id 7
    label "upublicznianie"
  ]
  node [
    id 8
    label "jawny"
  ]
  node [
    id 9
    label "upublicznienie"
  ]
  node [
    id 10
    label "publicznie"
  ]
  node [
    id 11
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 12
    label "zwyczajny"
  ]
  node [
    id 13
    label "typowo"
  ]
  node [
    id 14
    label "cz&#281;sty"
  ]
  node [
    id 15
    label "zwyk&#322;y"
  ]
  node [
    id 16
    label "obywatel"
  ]
  node [
    id 17
    label "mieszczanin"
  ]
  node [
    id 18
    label "nowoczesny"
  ]
  node [
    id 19
    label "mieszcza&#324;stwo"
  ]
  node [
    id 20
    label "charakterystycznie"
  ]
  node [
    id 21
    label "kartka"
  ]
  node [
    id 22
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 23
    label "logowanie"
  ]
  node [
    id 24
    label "plik"
  ]
  node [
    id 25
    label "s&#261;d"
  ]
  node [
    id 26
    label "adres_internetowy"
  ]
  node [
    id 27
    label "linia"
  ]
  node [
    id 28
    label "serwis_internetowy"
  ]
  node [
    id 29
    label "posta&#263;"
  ]
  node [
    id 30
    label "bok"
  ]
  node [
    id 31
    label "skr&#281;canie"
  ]
  node [
    id 32
    label "skr&#281;ca&#263;"
  ]
  node [
    id 33
    label "orientowanie"
  ]
  node [
    id 34
    label "skr&#281;ci&#263;"
  ]
  node [
    id 35
    label "uj&#281;cie"
  ]
  node [
    id 36
    label "zorientowanie"
  ]
  node [
    id 37
    label "ty&#322;"
  ]
  node [
    id 38
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 39
    label "fragment"
  ]
  node [
    id 40
    label "layout"
  ]
  node [
    id 41
    label "obiekt"
  ]
  node [
    id 42
    label "zorientowa&#263;"
  ]
  node [
    id 43
    label "pagina"
  ]
  node [
    id 44
    label "podmiot"
  ]
  node [
    id 45
    label "g&#243;ra"
  ]
  node [
    id 46
    label "orientowa&#263;"
  ]
  node [
    id 47
    label "voice"
  ]
  node [
    id 48
    label "orientacja"
  ]
  node [
    id 49
    label "prz&#243;d"
  ]
  node [
    id 50
    label "internet"
  ]
  node [
    id 51
    label "powierzchnia"
  ]
  node [
    id 52
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 53
    label "forma"
  ]
  node [
    id 54
    label "skr&#281;cenie"
  ]
  node [
    id 55
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 56
    label "byt"
  ]
  node [
    id 57
    label "cz&#322;owiek"
  ]
  node [
    id 58
    label "osobowo&#347;&#263;"
  ]
  node [
    id 59
    label "organizacja"
  ]
  node [
    id 60
    label "prawo"
  ]
  node [
    id 61
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 62
    label "nauka_prawa"
  ]
  node [
    id 63
    label "utw&#243;r"
  ]
  node [
    id 64
    label "charakterystyka"
  ]
  node [
    id 65
    label "zaistnie&#263;"
  ]
  node [
    id 66
    label "cecha"
  ]
  node [
    id 67
    label "Osjan"
  ]
  node [
    id 68
    label "kto&#347;"
  ]
  node [
    id 69
    label "wygl&#261;d"
  ]
  node [
    id 70
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 71
    label "wytw&#243;r"
  ]
  node [
    id 72
    label "trim"
  ]
  node [
    id 73
    label "poby&#263;"
  ]
  node [
    id 74
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 75
    label "Aspazja"
  ]
  node [
    id 76
    label "punkt_widzenia"
  ]
  node [
    id 77
    label "kompleksja"
  ]
  node [
    id 78
    label "wytrzyma&#263;"
  ]
  node [
    id 79
    label "budowa"
  ]
  node [
    id 80
    label "formacja"
  ]
  node [
    id 81
    label "pozosta&#263;"
  ]
  node [
    id 82
    label "point"
  ]
  node [
    id 83
    label "przedstawienie"
  ]
  node [
    id 84
    label "go&#347;&#263;"
  ]
  node [
    id 85
    label "kszta&#322;t"
  ]
  node [
    id 86
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 87
    label "armia"
  ]
  node [
    id 88
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 89
    label "poprowadzi&#263;"
  ]
  node [
    id 90
    label "cord"
  ]
  node [
    id 91
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 92
    label "trasa"
  ]
  node [
    id 93
    label "po&#322;&#261;czenie"
  ]
  node [
    id 94
    label "tract"
  ]
  node [
    id 95
    label "materia&#322;_zecerski"
  ]
  node [
    id 96
    label "przeorientowywanie"
  ]
  node [
    id 97
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 98
    label "curve"
  ]
  node [
    id 99
    label "figura_geometryczna"
  ]
  node [
    id 100
    label "zbi&#243;r"
  ]
  node [
    id 101
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 102
    label "jard"
  ]
  node [
    id 103
    label "szczep"
  ]
  node [
    id 104
    label "phreaker"
  ]
  node [
    id 105
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 106
    label "grupa_organizm&#243;w"
  ]
  node [
    id 107
    label "prowadzi&#263;"
  ]
  node [
    id 108
    label "przeorientowywa&#263;"
  ]
  node [
    id 109
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 110
    label "access"
  ]
  node [
    id 111
    label "przeorientowanie"
  ]
  node [
    id 112
    label "przeorientowa&#263;"
  ]
  node [
    id 113
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 114
    label "billing"
  ]
  node [
    id 115
    label "granica"
  ]
  node [
    id 116
    label "szpaler"
  ]
  node [
    id 117
    label "sztrych"
  ]
  node [
    id 118
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 119
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 120
    label "drzewo_genealogiczne"
  ]
  node [
    id 121
    label "transporter"
  ]
  node [
    id 122
    label "line"
  ]
  node [
    id 123
    label "przew&#243;d"
  ]
  node [
    id 124
    label "granice"
  ]
  node [
    id 125
    label "kontakt"
  ]
  node [
    id 126
    label "rz&#261;d"
  ]
  node [
    id 127
    label "przewo&#378;nik"
  ]
  node [
    id 128
    label "przystanek"
  ]
  node [
    id 129
    label "linijka"
  ]
  node [
    id 130
    label "spos&#243;b"
  ]
  node [
    id 131
    label "uporz&#261;dkowanie"
  ]
  node [
    id 132
    label "coalescence"
  ]
  node [
    id 133
    label "Ural"
  ]
  node [
    id 134
    label "bearing"
  ]
  node [
    id 135
    label "prowadzenie"
  ]
  node [
    id 136
    label "tekst"
  ]
  node [
    id 137
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 138
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 139
    label "koniec"
  ]
  node [
    id 140
    label "podkatalog"
  ]
  node [
    id 141
    label "nadpisa&#263;"
  ]
  node [
    id 142
    label "nadpisanie"
  ]
  node [
    id 143
    label "bundle"
  ]
  node [
    id 144
    label "folder"
  ]
  node [
    id 145
    label "nadpisywanie"
  ]
  node [
    id 146
    label "paczka"
  ]
  node [
    id 147
    label "nadpisywa&#263;"
  ]
  node [
    id 148
    label "dokument"
  ]
  node [
    id 149
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 150
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 151
    label "Rzym_Zachodni"
  ]
  node [
    id 152
    label "whole"
  ]
  node [
    id 153
    label "ilo&#347;&#263;"
  ]
  node [
    id 154
    label "element"
  ]
  node [
    id 155
    label "Rzym_Wschodni"
  ]
  node [
    id 156
    label "urz&#261;dzenie"
  ]
  node [
    id 157
    label "rozmiar"
  ]
  node [
    id 158
    label "obszar"
  ]
  node [
    id 159
    label "poj&#281;cie"
  ]
  node [
    id 160
    label "liofilowo&#347;&#263;"
  ]
  node [
    id 161
    label "zwierciad&#322;o"
  ]
  node [
    id 162
    label "capacity"
  ]
  node [
    id 163
    label "plane"
  ]
  node [
    id 164
    label "temat"
  ]
  node [
    id 165
    label "jednostka_systematyczna"
  ]
  node [
    id 166
    label "poznanie"
  ]
  node [
    id 167
    label "leksem"
  ]
  node [
    id 168
    label "dzie&#322;o"
  ]
  node [
    id 169
    label "stan"
  ]
  node [
    id 170
    label "blaszka"
  ]
  node [
    id 171
    label "kantyzm"
  ]
  node [
    id 172
    label "zdolno&#347;&#263;"
  ]
  node [
    id 173
    label "do&#322;ek"
  ]
  node [
    id 174
    label "zawarto&#347;&#263;"
  ]
  node [
    id 175
    label "gwiazda"
  ]
  node [
    id 176
    label "formality"
  ]
  node [
    id 177
    label "struktura"
  ]
  node [
    id 178
    label "mode"
  ]
  node [
    id 179
    label "morfem"
  ]
  node [
    id 180
    label "rdze&#324;"
  ]
  node [
    id 181
    label "kielich"
  ]
  node [
    id 182
    label "ornamentyka"
  ]
  node [
    id 183
    label "pasmo"
  ]
  node [
    id 184
    label "zwyczaj"
  ]
  node [
    id 185
    label "g&#322;owa"
  ]
  node [
    id 186
    label "naczynie"
  ]
  node [
    id 187
    label "p&#322;at"
  ]
  node [
    id 188
    label "maszyna_drukarska"
  ]
  node [
    id 189
    label "style"
  ]
  node [
    id 190
    label "linearno&#347;&#263;"
  ]
  node [
    id 191
    label "wyra&#380;enie"
  ]
  node [
    id 192
    label "spirala"
  ]
  node [
    id 193
    label "dyspozycja"
  ]
  node [
    id 194
    label "odmiana"
  ]
  node [
    id 195
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 196
    label "wz&#243;r"
  ]
  node [
    id 197
    label "October"
  ]
  node [
    id 198
    label "creation"
  ]
  node [
    id 199
    label "p&#281;tla"
  ]
  node [
    id 200
    label "arystotelizm"
  ]
  node [
    id 201
    label "szablon"
  ]
  node [
    id 202
    label "miniatura"
  ]
  node [
    id 203
    label "zesp&#243;&#322;"
  ]
  node [
    id 204
    label "podejrzany"
  ]
  node [
    id 205
    label "s&#261;downictwo"
  ]
  node [
    id 206
    label "system"
  ]
  node [
    id 207
    label "biuro"
  ]
  node [
    id 208
    label "court"
  ]
  node [
    id 209
    label "forum"
  ]
  node [
    id 210
    label "bronienie"
  ]
  node [
    id 211
    label "urz&#261;d"
  ]
  node [
    id 212
    label "wydarzenie"
  ]
  node [
    id 213
    label "oskar&#380;yciel"
  ]
  node [
    id 214
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 215
    label "skazany"
  ]
  node [
    id 216
    label "post&#281;powanie"
  ]
  node [
    id 217
    label "broni&#263;"
  ]
  node [
    id 218
    label "my&#347;l"
  ]
  node [
    id 219
    label "pods&#261;dny"
  ]
  node [
    id 220
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 221
    label "obrona"
  ]
  node [
    id 222
    label "wypowied&#378;"
  ]
  node [
    id 223
    label "instytucja"
  ]
  node [
    id 224
    label "antylogizm"
  ]
  node [
    id 225
    label "konektyw"
  ]
  node [
    id 226
    label "&#347;wiadek"
  ]
  node [
    id 227
    label "procesowicz"
  ]
  node [
    id 228
    label "pochwytanie"
  ]
  node [
    id 229
    label "wording"
  ]
  node [
    id 230
    label "wzbudzenie"
  ]
  node [
    id 231
    label "withdrawal"
  ]
  node [
    id 232
    label "capture"
  ]
  node [
    id 233
    label "podniesienie"
  ]
  node [
    id 234
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 235
    label "film"
  ]
  node [
    id 236
    label "scena"
  ]
  node [
    id 237
    label "zapisanie"
  ]
  node [
    id 238
    label "prezentacja"
  ]
  node [
    id 239
    label "rzucenie"
  ]
  node [
    id 240
    label "zamkni&#281;cie"
  ]
  node [
    id 241
    label "zabranie"
  ]
  node [
    id 242
    label "poinformowanie"
  ]
  node [
    id 243
    label "zaaresztowanie"
  ]
  node [
    id 244
    label "wzi&#281;cie"
  ]
  node [
    id 245
    label "kierunek"
  ]
  node [
    id 246
    label "wyznaczenie"
  ]
  node [
    id 247
    label "przyczynienie_si&#281;"
  ]
  node [
    id 248
    label "zwr&#243;cenie"
  ]
  node [
    id 249
    label "zrozumienie"
  ]
  node [
    id 250
    label "tu&#322;&#243;w"
  ]
  node [
    id 251
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 252
    label "wielok&#261;t"
  ]
  node [
    id 253
    label "odcinek"
  ]
  node [
    id 254
    label "strzelba"
  ]
  node [
    id 255
    label "lufa"
  ]
  node [
    id 256
    label "&#347;ciana"
  ]
  node [
    id 257
    label "set"
  ]
  node [
    id 258
    label "orient"
  ]
  node [
    id 259
    label "eastern_hemisphere"
  ]
  node [
    id 260
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 261
    label "aim"
  ]
  node [
    id 262
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 263
    label "wyznaczy&#263;"
  ]
  node [
    id 264
    label "wrench"
  ]
  node [
    id 265
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 266
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 267
    label "sple&#347;&#263;"
  ]
  node [
    id 268
    label "os&#322;abi&#263;"
  ]
  node [
    id 269
    label "nawin&#261;&#263;"
  ]
  node [
    id 270
    label "scali&#263;"
  ]
  node [
    id 271
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 272
    label "twist"
  ]
  node [
    id 273
    label "splay"
  ]
  node [
    id 274
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 275
    label "uszkodzi&#263;"
  ]
  node [
    id 276
    label "break"
  ]
  node [
    id 277
    label "flex"
  ]
  node [
    id 278
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 279
    label "os&#322;abia&#263;"
  ]
  node [
    id 280
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 281
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 282
    label "splata&#263;"
  ]
  node [
    id 283
    label "throw"
  ]
  node [
    id 284
    label "screw"
  ]
  node [
    id 285
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 286
    label "scala&#263;"
  ]
  node [
    id 287
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 288
    label "przedmiot"
  ]
  node [
    id 289
    label "przelezienie"
  ]
  node [
    id 290
    label "&#347;piew"
  ]
  node [
    id 291
    label "Synaj"
  ]
  node [
    id 292
    label "Kreml"
  ]
  node [
    id 293
    label "d&#378;wi&#281;k"
  ]
  node [
    id 294
    label "wysoki"
  ]
  node [
    id 295
    label "wzniesienie"
  ]
  node [
    id 296
    label "grupa"
  ]
  node [
    id 297
    label "pi&#281;tro"
  ]
  node [
    id 298
    label "Ropa"
  ]
  node [
    id 299
    label "kupa"
  ]
  node [
    id 300
    label "przele&#378;&#263;"
  ]
  node [
    id 301
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 302
    label "karczek"
  ]
  node [
    id 303
    label "rami&#261;czko"
  ]
  node [
    id 304
    label "Jaworze"
  ]
  node [
    id 305
    label "odchylanie_si&#281;"
  ]
  node [
    id 306
    label "kszta&#322;towanie"
  ]
  node [
    id 307
    label "os&#322;abianie"
  ]
  node [
    id 308
    label "uprz&#281;dzenie"
  ]
  node [
    id 309
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 310
    label "scalanie"
  ]
  node [
    id 311
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 312
    label "snucie"
  ]
  node [
    id 313
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 314
    label "tortuosity"
  ]
  node [
    id 315
    label "odbijanie"
  ]
  node [
    id 316
    label "contortion"
  ]
  node [
    id 317
    label "splatanie"
  ]
  node [
    id 318
    label "turn"
  ]
  node [
    id 319
    label "nawini&#281;cie"
  ]
  node [
    id 320
    label "os&#322;abienie"
  ]
  node [
    id 321
    label "uszkodzenie"
  ]
  node [
    id 322
    label "odbicie"
  ]
  node [
    id 323
    label "poskr&#281;canie"
  ]
  node [
    id 324
    label "uraz"
  ]
  node [
    id 325
    label "odchylenie_si&#281;"
  ]
  node [
    id 326
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 327
    label "z&#322;&#261;czenie"
  ]
  node [
    id 328
    label "splecenie"
  ]
  node [
    id 329
    label "turning"
  ]
  node [
    id 330
    label "kierowa&#263;"
  ]
  node [
    id 331
    label "inform"
  ]
  node [
    id 332
    label "marshal"
  ]
  node [
    id 333
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 334
    label "wyznacza&#263;"
  ]
  node [
    id 335
    label "pomaga&#263;"
  ]
  node [
    id 336
    label "pomaganie"
  ]
  node [
    id 337
    label "orientation"
  ]
  node [
    id 338
    label "przyczynianie_si&#281;"
  ]
  node [
    id 339
    label "zwracanie"
  ]
  node [
    id 340
    label "rozeznawanie"
  ]
  node [
    id 341
    label "oznaczanie"
  ]
  node [
    id 342
    label "przestrze&#324;"
  ]
  node [
    id 343
    label "cia&#322;o"
  ]
  node [
    id 344
    label "po&#322;o&#380;enie"
  ]
  node [
    id 345
    label "seksualno&#347;&#263;"
  ]
  node [
    id 346
    label "wiedza"
  ]
  node [
    id 347
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 348
    label "zorientowanie_si&#281;"
  ]
  node [
    id 349
    label "pogubienie_si&#281;"
  ]
  node [
    id 350
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 351
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 352
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 353
    label "gubienie_si&#281;"
  ]
  node [
    id 354
    label "zaty&#322;"
  ]
  node [
    id 355
    label "pupa"
  ]
  node [
    id 356
    label "figura"
  ]
  node [
    id 357
    label "graficzny_interfejs_u&#380;ytkownika"
  ]
  node [
    id 358
    label "uk&#322;ad_graficzny"
  ]
  node [
    id 359
    label "uwierzytelnienie"
  ]
  node [
    id 360
    label "liczba"
  ]
  node [
    id 361
    label "circumference"
  ]
  node [
    id 362
    label "cyrkumferencja"
  ]
  node [
    id 363
    label "miejsce"
  ]
  node [
    id 364
    label "provider"
  ]
  node [
    id 365
    label "hipertekst"
  ]
  node [
    id 366
    label "cyberprzestrze&#324;"
  ]
  node [
    id 367
    label "mem"
  ]
  node [
    id 368
    label "grooming"
  ]
  node [
    id 369
    label "gra_sieciowa"
  ]
  node [
    id 370
    label "media"
  ]
  node [
    id 371
    label "biznes_elektroniczny"
  ]
  node [
    id 372
    label "sie&#263;_komputerowa"
  ]
  node [
    id 373
    label "punkt_dost&#281;pu"
  ]
  node [
    id 374
    label "us&#322;uga_internetowa"
  ]
  node [
    id 375
    label "netbook"
  ]
  node [
    id 376
    label "e-hazard"
  ]
  node [
    id 377
    label "podcast"
  ]
  node [
    id 378
    label "co&#347;"
  ]
  node [
    id 379
    label "budynek"
  ]
  node [
    id 380
    label "thing"
  ]
  node [
    id 381
    label "program"
  ]
  node [
    id 382
    label "rzecz"
  ]
  node [
    id 383
    label "faul"
  ]
  node [
    id 384
    label "wk&#322;ad"
  ]
  node [
    id 385
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 386
    label "s&#281;dzia"
  ]
  node [
    id 387
    label "bon"
  ]
  node [
    id 388
    label "ticket"
  ]
  node [
    id 389
    label "arkusz"
  ]
  node [
    id 390
    label "kartonik"
  ]
  node [
    id 391
    label "kara"
  ]
  node [
    id 392
    label "pagination"
  ]
  node [
    id 393
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 394
    label "numer"
  ]
  node [
    id 395
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 396
    label "satelita"
  ]
  node [
    id 397
    label "peryselenium"
  ]
  node [
    id 398
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 399
    label "&#347;wiat&#322;o"
  ]
  node [
    id 400
    label "aposelenium"
  ]
  node [
    id 401
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 402
    label "Tytan"
  ]
  node [
    id 403
    label "moon"
  ]
  node [
    id 404
    label "miesi&#261;c"
  ]
  node [
    id 405
    label "statek_kosmiczny"
  ]
  node [
    id 406
    label "satellite"
  ]
  node [
    id 407
    label "antena"
  ]
  node [
    id 408
    label "towarzysz"
  ]
  node [
    id 409
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 410
    label "telewizja"
  ]
  node [
    id 411
    label "antena_satelitarna"
  ]
  node [
    id 412
    label "kapsu&#322;a"
  ]
  node [
    id 413
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 414
    label "energia"
  ]
  node [
    id 415
    label "&#347;wieci&#263;"
  ]
  node [
    id 416
    label "odst&#281;p"
  ]
  node [
    id 417
    label "wpadni&#281;cie"
  ]
  node [
    id 418
    label "interpretacja"
  ]
  node [
    id 419
    label "zjawisko"
  ]
  node [
    id 420
    label "fotokataliza"
  ]
  node [
    id 421
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 422
    label "wpa&#347;&#263;"
  ]
  node [
    id 423
    label "rzuca&#263;"
  ]
  node [
    id 424
    label "obsadnik"
  ]
  node [
    id 425
    label "promieniowanie_optyczne"
  ]
  node [
    id 426
    label "lampa"
  ]
  node [
    id 427
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 428
    label "ja&#347;nia"
  ]
  node [
    id 429
    label "light"
  ]
  node [
    id 430
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 431
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 432
    label "wpada&#263;"
  ]
  node [
    id 433
    label "rzuci&#263;"
  ]
  node [
    id 434
    label "o&#347;wietlenie"
  ]
  node [
    id 435
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 436
    label "przy&#263;mienie"
  ]
  node [
    id 437
    label "instalacja"
  ]
  node [
    id 438
    label "&#347;wiecenie"
  ]
  node [
    id 439
    label "radiance"
  ]
  node [
    id 440
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 441
    label "przy&#263;mi&#263;"
  ]
  node [
    id 442
    label "b&#322;ysk"
  ]
  node [
    id 443
    label "&#347;wiat&#322;y"
  ]
  node [
    id 444
    label "promie&#324;"
  ]
  node [
    id 445
    label "m&#261;drze"
  ]
  node [
    id 446
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 447
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 448
    label "lighting"
  ]
  node [
    id 449
    label "lighter"
  ]
  node [
    id 450
    label "plama"
  ]
  node [
    id 451
    label "&#347;rednica"
  ]
  node [
    id 452
    label "wpadanie"
  ]
  node [
    id 453
    label "przy&#263;miewanie"
  ]
  node [
    id 454
    label "rzucanie"
  ]
  node [
    id 455
    label "Ziemia"
  ]
  node [
    id 456
    label "aspekt"
  ]
  node [
    id 457
    label "Jowisz"
  ]
  node [
    id 458
    label "orbita"
  ]
  node [
    id 459
    label "tydzie&#324;"
  ]
  node [
    id 460
    label "miech"
  ]
  node [
    id 461
    label "czas"
  ]
  node [
    id 462
    label "rok"
  ]
  node [
    id 463
    label "kalendy"
  ]
  node [
    id 464
    label "stary"
  ]
  node [
    id 465
    label "dobry"
  ]
  node [
    id 466
    label "ma&#322;&#380;e&#324;stwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 1
    target 313
  ]
  edge [
    source 1
    target 314
  ]
  edge [
    source 1
    target 315
  ]
  edge [
    source 1
    target 316
  ]
  edge [
    source 1
    target 317
  ]
  edge [
    source 1
    target 318
  ]
  edge [
    source 1
    target 319
  ]
  edge [
    source 1
    target 320
  ]
  edge [
    source 1
    target 321
  ]
  edge [
    source 1
    target 322
  ]
  edge [
    source 1
    target 323
  ]
  edge [
    source 1
    target 324
  ]
  edge [
    source 1
    target 325
  ]
  edge [
    source 1
    target 326
  ]
  edge [
    source 1
    target 327
  ]
  edge [
    source 1
    target 328
  ]
  edge [
    source 1
    target 329
  ]
  edge [
    source 1
    target 330
  ]
  edge [
    source 1
    target 331
  ]
  edge [
    source 1
    target 332
  ]
  edge [
    source 1
    target 333
  ]
  edge [
    source 1
    target 334
  ]
  edge [
    source 1
    target 335
  ]
  edge [
    source 1
    target 336
  ]
  edge [
    source 1
    target 337
  ]
  edge [
    source 1
    target 338
  ]
  edge [
    source 1
    target 339
  ]
  edge [
    source 1
    target 340
  ]
  edge [
    source 1
    target 341
  ]
  edge [
    source 1
    target 342
  ]
  edge [
    source 1
    target 343
  ]
  edge [
    source 1
    target 344
  ]
  edge [
    source 1
    target 345
  ]
  edge [
    source 1
    target 346
  ]
  edge [
    source 1
    target 347
  ]
  edge [
    source 1
    target 348
  ]
  edge [
    source 1
    target 349
  ]
  edge [
    source 1
    target 350
  ]
  edge [
    source 1
    target 351
  ]
  edge [
    source 1
    target 352
  ]
  edge [
    source 1
    target 353
  ]
  edge [
    source 1
    target 354
  ]
  edge [
    source 1
    target 355
  ]
  edge [
    source 1
    target 356
  ]
  edge [
    source 1
    target 357
  ]
  edge [
    source 1
    target 358
  ]
  edge [
    source 1
    target 359
  ]
  edge [
    source 1
    target 360
  ]
  edge [
    source 1
    target 361
  ]
  edge [
    source 1
    target 362
  ]
  edge [
    source 1
    target 363
  ]
  edge [
    source 1
    target 364
  ]
  edge [
    source 1
    target 365
  ]
  edge [
    source 1
    target 366
  ]
  edge [
    source 1
    target 367
  ]
  edge [
    source 1
    target 368
  ]
  edge [
    source 1
    target 369
  ]
  edge [
    source 1
    target 370
  ]
  edge [
    source 1
    target 371
  ]
  edge [
    source 1
    target 372
  ]
  edge [
    source 1
    target 373
  ]
  edge [
    source 1
    target 374
  ]
  edge [
    source 1
    target 375
  ]
  edge [
    source 1
    target 376
  ]
  edge [
    source 1
    target 377
  ]
  edge [
    source 1
    target 378
  ]
  edge [
    source 1
    target 379
  ]
  edge [
    source 1
    target 380
  ]
  edge [
    source 1
    target 381
  ]
  edge [
    source 1
    target 382
  ]
  edge [
    source 1
    target 383
  ]
  edge [
    source 1
    target 384
  ]
  edge [
    source 1
    target 385
  ]
  edge [
    source 1
    target 386
  ]
  edge [
    source 1
    target 387
  ]
  edge [
    source 1
    target 388
  ]
  edge [
    source 1
    target 389
  ]
  edge [
    source 1
    target 390
  ]
  edge [
    source 1
    target 391
  ]
  edge [
    source 1
    target 392
  ]
  edge [
    source 1
    target 393
  ]
  edge [
    source 1
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 464
    target 465
  ]
  edge [
    source 464
    target 466
  ]
  edge [
    source 465
    target 466
  ]
]
