graph [
  node [
    id 0
    label "&#380;aden"
    origin "text"
  ]
  node [
    id 1
    label "biega&#263;"
    origin "text"
  ]
  node [
    id 2
    label "guz"
    origin "text"
  ]
  node [
    id 3
    label "dziwa"
    origin "text"
  ]
  node [
    id 4
    label "nijaki"
  ]
  node [
    id 5
    label "nijak"
  ]
  node [
    id 6
    label "niezabawny"
  ]
  node [
    id 7
    label "zwyczajny"
  ]
  node [
    id 8
    label "oboj&#281;tny"
  ]
  node [
    id 9
    label "poszarzenie"
  ]
  node [
    id 10
    label "neutralny"
  ]
  node [
    id 11
    label "szarzenie"
  ]
  node [
    id 12
    label "bezbarwnie"
  ]
  node [
    id 13
    label "nieciekawy"
  ]
  node [
    id 14
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 15
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 16
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "hula&#263;"
  ]
  node [
    id 18
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 19
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 20
    label "rozwolnienie"
  ]
  node [
    id 21
    label "uprawia&#263;"
  ]
  node [
    id 22
    label "&#347;ciga&#263;_si&#281;"
  ]
  node [
    id 23
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 24
    label "biec"
  ]
  node [
    id 25
    label "chodzi&#263;"
  ]
  node [
    id 26
    label "rush"
  ]
  node [
    id 27
    label "dash"
  ]
  node [
    id 28
    label "cieka&#263;"
  ]
  node [
    id 29
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 30
    label "ucieka&#263;"
  ]
  node [
    id 31
    label "chorowa&#263;"
  ]
  node [
    id 32
    label "mie&#263;_miejsce"
  ]
  node [
    id 33
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 34
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 35
    label "p&#322;ywa&#263;"
  ]
  node [
    id 36
    label "run"
  ]
  node [
    id 37
    label "bangla&#263;"
  ]
  node [
    id 38
    label "przebiega&#263;"
  ]
  node [
    id 39
    label "wk&#322;ada&#263;"
  ]
  node [
    id 40
    label "proceed"
  ]
  node [
    id 41
    label "by&#263;"
  ]
  node [
    id 42
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 43
    label "carry"
  ]
  node [
    id 44
    label "bywa&#263;"
  ]
  node [
    id 45
    label "dziama&#263;"
  ]
  node [
    id 46
    label "stara&#263;_si&#281;"
  ]
  node [
    id 47
    label "para"
  ]
  node [
    id 48
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 49
    label "str&#243;j"
  ]
  node [
    id 50
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 51
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 52
    label "krok"
  ]
  node [
    id 53
    label "tryb"
  ]
  node [
    id 54
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 55
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 56
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 57
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 58
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 59
    label "continue"
  ]
  node [
    id 60
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 61
    label "work"
  ]
  node [
    id 62
    label "hodowa&#263;"
  ]
  node [
    id 63
    label "plantator"
  ]
  node [
    id 64
    label "pain"
  ]
  node [
    id 65
    label "garlic"
  ]
  node [
    id 66
    label "pragn&#261;&#263;"
  ]
  node [
    id 67
    label "cierpie&#263;"
  ]
  node [
    id 68
    label "przedmiot"
  ]
  node [
    id 69
    label "kontrolowa&#263;"
  ]
  node [
    id 70
    label "sok"
  ]
  node [
    id 71
    label "krew"
  ]
  node [
    id 72
    label "wheel"
  ]
  node [
    id 73
    label "bra&#263;"
  ]
  node [
    id 74
    label "pali&#263;_wrotki"
  ]
  node [
    id 75
    label "wydostawa&#263;_si&#281;"
  ]
  node [
    id 76
    label "blow"
  ]
  node [
    id 77
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 78
    label "unika&#263;"
  ]
  node [
    id 79
    label "zwiewa&#263;"
  ]
  node [
    id 80
    label "spieprza&#263;"
  ]
  node [
    id 81
    label "wymyka&#263;_si&#281;"
  ]
  node [
    id 82
    label "w&#322;&#243;czy&#263;_si&#281;"
  ]
  node [
    id 83
    label "funkcjonowa&#263;"
  ]
  node [
    id 84
    label "lampartowa&#263;_si&#281;"
  ]
  node [
    id 85
    label "bomblowa&#263;"
  ]
  node [
    id 86
    label "rant"
  ]
  node [
    id 87
    label "rozrabia&#263;"
  ]
  node [
    id 88
    label "wia&#263;"
  ]
  node [
    id 89
    label "carouse"
  ]
  node [
    id 90
    label "storm"
  ]
  node [
    id 91
    label "bawi&#263;_si&#281;"
  ]
  node [
    id 92
    label "brzmie&#263;"
  ]
  node [
    id 93
    label "panoszy&#263;_si&#281;"
  ]
  node [
    id 94
    label "folgowa&#263;"
  ]
  node [
    id 95
    label "czyha&#263;"
  ]
  node [
    id 96
    label "szale&#263;"
  ]
  node [
    id 97
    label "lumpowa&#263;"
  ]
  node [
    id 98
    label "lumpowa&#263;_si&#281;"
  ]
  node [
    id 99
    label "startowa&#263;"
  ]
  node [
    id 100
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 101
    label "draw"
  ]
  node [
    id 102
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 103
    label "rozchodzi&#263;_si&#281;"
  ]
  node [
    id 104
    label "bie&#380;e&#263;"
  ]
  node [
    id 105
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 106
    label "przesuwa&#263;"
  ]
  node [
    id 107
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 108
    label "t&#322;oczy&#263;_si&#281;"
  ]
  node [
    id 109
    label "wie&#347;&#263;"
  ]
  node [
    id 110
    label "tent-fly"
  ]
  node [
    id 111
    label "przybywa&#263;"
  ]
  node [
    id 112
    label "i&#347;&#263;"
  ]
  node [
    id 113
    label "lecie&#263;"
  ]
  node [
    id 114
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 115
    label "zwierz&#281;"
  ]
  node [
    id 116
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 117
    label "rise"
  ]
  node [
    id 118
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 119
    label "oznaka"
  ]
  node [
    id 120
    label "przeczy&#347;ci&#263;"
  ]
  node [
    id 121
    label "przeczyszcza&#263;"
  ]
  node [
    id 122
    label "przeczyszczanie"
  ]
  node [
    id 123
    label "katar_kiszek"
  ]
  node [
    id 124
    label "rotawirus"
  ]
  node [
    id 125
    label "sraczka"
  ]
  node [
    id 126
    label "przeczyszczenie"
  ]
  node [
    id 127
    label "s&#281;k_zaro&#347;ni&#281;ty"
  ]
  node [
    id 128
    label "zmiana"
  ]
  node [
    id 129
    label "zgrubienie"
  ]
  node [
    id 130
    label "rewizja"
  ]
  node [
    id 131
    label "passage"
  ]
  node [
    id 132
    label "change"
  ]
  node [
    id 133
    label "ferment"
  ]
  node [
    id 134
    label "komplet"
  ]
  node [
    id 135
    label "anatomopatolog"
  ]
  node [
    id 136
    label "zmianka"
  ]
  node [
    id 137
    label "czas"
  ]
  node [
    id 138
    label "zjawisko"
  ]
  node [
    id 139
    label "amendment"
  ]
  node [
    id 140
    label "praca"
  ]
  node [
    id 141
    label "odmienianie"
  ]
  node [
    id 142
    label "tura"
  ]
  node [
    id 143
    label "gruby"
  ]
  node [
    id 144
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 145
    label "figura_stylistyczna"
  ]
  node [
    id 146
    label "leksem"
  ]
  node [
    id 147
    label "bulge"
  ]
  node [
    id 148
    label "powi&#281;kszenie_si&#281;"
  ]
  node [
    id 149
    label "kurwa"
  ]
  node [
    id 150
    label "dziewczyna"
  ]
  node [
    id 151
    label "pigalak"
  ]
  node [
    id 152
    label "ma&#322;pa"
  ]
  node [
    id 153
    label "dziewczyna_lekkich_obyczaj&#243;w"
  ]
  node [
    id 154
    label "rozpustnica"
  ]
  node [
    id 155
    label "diva"
  ]
  node [
    id 156
    label "jawnogrzesznica"
  ]
  node [
    id 157
    label "dziewka"
  ]
  node [
    id 158
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 159
    label "sikorka"
  ]
  node [
    id 160
    label "kora"
  ]
  node [
    id 161
    label "cz&#322;owiek"
  ]
  node [
    id 162
    label "dziewcz&#281;"
  ]
  node [
    id 163
    label "dziecina"
  ]
  node [
    id 164
    label "m&#322;&#243;dka"
  ]
  node [
    id 165
    label "sympatia"
  ]
  node [
    id 166
    label "dziunia"
  ]
  node [
    id 167
    label "dziewczynina"
  ]
  node [
    id 168
    label "partnerka"
  ]
  node [
    id 169
    label "siksa"
  ]
  node [
    id 170
    label "dziewoja"
  ]
  node [
    id 171
    label "puszczalska"
  ]
  node [
    id 172
    label "bezwstydnica"
  ]
  node [
    id 173
    label "&#347;piewaczka"
  ]
  node [
    id 174
    label "aktorka"
  ]
  node [
    id 175
    label "prostytutka"
  ]
  node [
    id 176
    label "tancerka"
  ]
  node [
    id 177
    label "gwiazda"
  ]
  node [
    id 178
    label "naczelne"
  ]
  node [
    id 179
    label "monkey"
  ]
  node [
    id 180
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 181
    label "z&#322;o&#347;liwy"
  ]
  node [
    id 182
    label "istota_&#380;ywa"
  ]
  node [
    id 183
    label "humanoid"
  ]
  node [
    id 184
    label "dokucza&#263;"
  ]
  node [
    id 185
    label "skurwienie_si&#281;"
  ]
  node [
    id 186
    label "zo&#322;za"
  ]
  node [
    id 187
    label "karze&#322;"
  ]
  node [
    id 188
    label "wyzwisko"
  ]
  node [
    id 189
    label "przekle&#324;stwo"
  ]
  node [
    id 190
    label "szmaciarz"
  ]
  node [
    id 191
    label "kurcz&#281;"
  ]
  node [
    id 192
    label "kurwienie_si&#281;"
  ]
  node [
    id 193
    label "miejsce"
  ]
  node [
    id 194
    label "kurwido&#322;ek"
  ]
  node [
    id 195
    label "grzesznica"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
]
