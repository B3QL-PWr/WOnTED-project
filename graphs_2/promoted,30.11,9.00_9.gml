graph [
  node [
    id 0
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 1
    label "luby"
    origin "text"
  ]
  node [
    id 2
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "sta&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "klient"
    origin "text"
  ]
  node [
    id 5
    label "kochanek"
  ]
  node [
    id 6
    label "wybranek"
  ]
  node [
    id 7
    label "umi&#322;owany"
  ]
  node [
    id 8
    label "drogi"
  ]
  node [
    id 9
    label "kochanie"
  ]
  node [
    id 10
    label "mi&#322;owanie"
  ]
  node [
    id 11
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 12
    label "love"
  ]
  node [
    id 13
    label "zwrot"
  ]
  node [
    id 14
    label "chowanie"
  ]
  node [
    id 15
    label "czucie"
  ]
  node [
    id 16
    label "patrzenie_"
  ]
  node [
    id 17
    label "mi&#322;y"
  ]
  node [
    id 18
    label "kocha&#347;"
  ]
  node [
    id 19
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 20
    label "partner"
  ]
  node [
    id 21
    label "przyjaciel"
  ]
  node [
    id 22
    label "bratek"
  ]
  node [
    id 23
    label "fagas"
  ]
  node [
    id 24
    label "drogo"
  ]
  node [
    id 25
    label "cz&#322;owiek"
  ]
  node [
    id 26
    label "bliski"
  ]
  node [
    id 27
    label "warto&#347;ciowy"
  ]
  node [
    id 28
    label "partia"
  ]
  node [
    id 29
    label "jedyny"
  ]
  node [
    id 30
    label "hide"
  ]
  node [
    id 31
    label "czu&#263;"
  ]
  node [
    id 32
    label "support"
  ]
  node [
    id 33
    label "need"
  ]
  node [
    id 34
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 35
    label "wykonawca"
  ]
  node [
    id 36
    label "interpretator"
  ]
  node [
    id 37
    label "cover"
  ]
  node [
    id 38
    label "postrzega&#263;"
  ]
  node [
    id 39
    label "przewidywa&#263;"
  ]
  node [
    id 40
    label "by&#263;"
  ]
  node [
    id 41
    label "smell"
  ]
  node [
    id 42
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 43
    label "uczuwa&#263;"
  ]
  node [
    id 44
    label "spirit"
  ]
  node [
    id 45
    label "doznawa&#263;"
  ]
  node [
    id 46
    label "anticipate"
  ]
  node [
    id 47
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 48
    label "element"
  ]
  node [
    id 49
    label "constant"
  ]
  node [
    id 50
    label "wielko&#347;&#263;"
  ]
  node [
    id 51
    label "r&#243;&#380;niczka"
  ]
  node [
    id 52
    label "&#347;rodowisko"
  ]
  node [
    id 53
    label "przedmiot"
  ]
  node [
    id 54
    label "materia"
  ]
  node [
    id 55
    label "szambo"
  ]
  node [
    id 56
    label "aspo&#322;eczny"
  ]
  node [
    id 57
    label "component"
  ]
  node [
    id 58
    label "szkodnik"
  ]
  node [
    id 59
    label "gangsterski"
  ]
  node [
    id 60
    label "poj&#281;cie"
  ]
  node [
    id 61
    label "underworld"
  ]
  node [
    id 62
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 63
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 64
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 65
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 66
    label "warunek_lokalowy"
  ]
  node [
    id 67
    label "rozmiar"
  ]
  node [
    id 68
    label "liczba"
  ]
  node [
    id 69
    label "cecha"
  ]
  node [
    id 70
    label "rzadko&#347;&#263;"
  ]
  node [
    id 71
    label "zaleta"
  ]
  node [
    id 72
    label "ilo&#347;&#263;"
  ]
  node [
    id 73
    label "measure"
  ]
  node [
    id 74
    label "znaczenie"
  ]
  node [
    id 75
    label "opinia"
  ]
  node [
    id 76
    label "dymensja"
  ]
  node [
    id 77
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 78
    label "zdolno&#347;&#263;"
  ]
  node [
    id 79
    label "potencja"
  ]
  node [
    id 80
    label "property"
  ]
  node [
    id 81
    label "agent_rozliczeniowy"
  ]
  node [
    id 82
    label "komputer_cyfrowy"
  ]
  node [
    id 83
    label "us&#322;ugobiorca"
  ]
  node [
    id 84
    label "Rzymianin"
  ]
  node [
    id 85
    label "szlachcic"
  ]
  node [
    id 86
    label "obywatel"
  ]
  node [
    id 87
    label "klientela"
  ]
  node [
    id 88
    label "program"
  ]
  node [
    id 89
    label "szlachciura"
  ]
  node [
    id 90
    label "przedstawiciel"
  ]
  node [
    id 91
    label "szlachta"
  ]
  node [
    id 92
    label "notabl"
  ]
  node [
    id 93
    label "Cyceron"
  ]
  node [
    id 94
    label "mieszkaniec"
  ]
  node [
    id 95
    label "Horacy"
  ]
  node [
    id 96
    label "W&#322;och"
  ]
  node [
    id 97
    label "miastowy"
  ]
  node [
    id 98
    label "pa&#324;stwo"
  ]
  node [
    id 99
    label "ludzko&#347;&#263;"
  ]
  node [
    id 100
    label "asymilowanie"
  ]
  node [
    id 101
    label "wapniak"
  ]
  node [
    id 102
    label "asymilowa&#263;"
  ]
  node [
    id 103
    label "os&#322;abia&#263;"
  ]
  node [
    id 104
    label "posta&#263;"
  ]
  node [
    id 105
    label "hominid"
  ]
  node [
    id 106
    label "podw&#322;adny"
  ]
  node [
    id 107
    label "os&#322;abianie"
  ]
  node [
    id 108
    label "g&#322;owa"
  ]
  node [
    id 109
    label "figura"
  ]
  node [
    id 110
    label "portrecista"
  ]
  node [
    id 111
    label "dwun&#243;g"
  ]
  node [
    id 112
    label "profanum"
  ]
  node [
    id 113
    label "mikrokosmos"
  ]
  node [
    id 114
    label "nasada"
  ]
  node [
    id 115
    label "duch"
  ]
  node [
    id 116
    label "antropochoria"
  ]
  node [
    id 117
    label "osoba"
  ]
  node [
    id 118
    label "wz&#243;r"
  ]
  node [
    id 119
    label "senior"
  ]
  node [
    id 120
    label "oddzia&#322;ywanie"
  ]
  node [
    id 121
    label "Adam"
  ]
  node [
    id 122
    label "homo_sapiens"
  ]
  node [
    id 123
    label "polifag"
  ]
  node [
    id 124
    label "podmiot"
  ]
  node [
    id 125
    label "instalowa&#263;"
  ]
  node [
    id 126
    label "oprogramowanie"
  ]
  node [
    id 127
    label "odinstalowywa&#263;"
  ]
  node [
    id 128
    label "spis"
  ]
  node [
    id 129
    label "zaprezentowanie"
  ]
  node [
    id 130
    label "podprogram"
  ]
  node [
    id 131
    label "ogranicznik_referencyjny"
  ]
  node [
    id 132
    label "course_of_study"
  ]
  node [
    id 133
    label "booklet"
  ]
  node [
    id 134
    label "dzia&#322;"
  ]
  node [
    id 135
    label "odinstalowanie"
  ]
  node [
    id 136
    label "broszura"
  ]
  node [
    id 137
    label "wytw&#243;r"
  ]
  node [
    id 138
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 139
    label "kana&#322;"
  ]
  node [
    id 140
    label "teleferie"
  ]
  node [
    id 141
    label "zainstalowanie"
  ]
  node [
    id 142
    label "struktura_organizacyjna"
  ]
  node [
    id 143
    label "pirat"
  ]
  node [
    id 144
    label "zaprezentowa&#263;"
  ]
  node [
    id 145
    label "prezentowanie"
  ]
  node [
    id 146
    label "prezentowa&#263;"
  ]
  node [
    id 147
    label "interfejs"
  ]
  node [
    id 148
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 149
    label "okno"
  ]
  node [
    id 150
    label "blok"
  ]
  node [
    id 151
    label "punkt"
  ]
  node [
    id 152
    label "folder"
  ]
  node [
    id 153
    label "zainstalowa&#263;"
  ]
  node [
    id 154
    label "za&#322;o&#380;enie"
  ]
  node [
    id 155
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 156
    label "ram&#243;wka"
  ]
  node [
    id 157
    label "tryb"
  ]
  node [
    id 158
    label "emitowa&#263;"
  ]
  node [
    id 159
    label "emitowanie"
  ]
  node [
    id 160
    label "odinstalowywanie"
  ]
  node [
    id 161
    label "instrukcja"
  ]
  node [
    id 162
    label "informatyka"
  ]
  node [
    id 163
    label "deklaracja"
  ]
  node [
    id 164
    label "menu"
  ]
  node [
    id 165
    label "sekcja_krytyczna"
  ]
  node [
    id 166
    label "furkacja"
  ]
  node [
    id 167
    label "podstawa"
  ]
  node [
    id 168
    label "instalowanie"
  ]
  node [
    id 169
    label "oferta"
  ]
  node [
    id 170
    label "odinstalowa&#263;"
  ]
  node [
    id 171
    label "fio&#322;ek"
  ]
  node [
    id 172
    label "facet"
  ]
  node [
    id 173
    label "brat"
  ]
  node [
    id 174
    label "clientele"
  ]
  node [
    id 175
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 176
    label "Samsungu"
  ]
  node [
    id 177
    label "Galaxy"
  ]
  node [
    id 178
    label "S8"
  ]
  node [
    id 179
    label "medium"
  ]
  node [
    id 180
    label "Markt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 176
    target 178
  ]
  edge [
    source 177
    target 178
  ]
  edge [
    source 179
    target 180
  ]
]
