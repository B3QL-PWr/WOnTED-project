graph [
  node [
    id 0
    label "kopiec"
    origin "text"
  ]
  node [
    id 1
    label "dwumianowy"
    origin "text"
  ]
  node [
    id 2
    label "kszta&#322;t"
  ]
  node [
    id 3
    label "struktura"
  ]
  node [
    id 4
    label "usypisko"
  ]
  node [
    id 5
    label "knoll"
  ]
  node [
    id 6
    label "Kopiec_Pi&#322;sudskiego"
  ]
  node [
    id 7
    label "pryzma"
  ]
  node [
    id 8
    label "mechanika"
  ]
  node [
    id 9
    label "o&#347;"
  ]
  node [
    id 10
    label "usenet"
  ]
  node [
    id 11
    label "rozprz&#261;c"
  ]
  node [
    id 12
    label "zachowanie"
  ]
  node [
    id 13
    label "cybernetyk"
  ]
  node [
    id 14
    label "podsystem"
  ]
  node [
    id 15
    label "system"
  ]
  node [
    id 16
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 17
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 18
    label "sk&#322;ad"
  ]
  node [
    id 19
    label "systemat"
  ]
  node [
    id 20
    label "cecha"
  ]
  node [
    id 21
    label "konstrukcja"
  ]
  node [
    id 22
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 23
    label "konstelacja"
  ]
  node [
    id 24
    label "formacja"
  ]
  node [
    id 25
    label "punkt_widzenia"
  ]
  node [
    id 26
    label "wygl&#261;d"
  ]
  node [
    id 27
    label "g&#322;owa"
  ]
  node [
    id 28
    label "spirala"
  ]
  node [
    id 29
    label "p&#322;at"
  ]
  node [
    id 30
    label "comeliness"
  ]
  node [
    id 31
    label "kielich"
  ]
  node [
    id 32
    label "face"
  ]
  node [
    id 33
    label "blaszka"
  ]
  node [
    id 34
    label "charakter"
  ]
  node [
    id 35
    label "p&#281;tla"
  ]
  node [
    id 36
    label "obiekt"
  ]
  node [
    id 37
    label "pasmo"
  ]
  node [
    id 38
    label "linearno&#347;&#263;"
  ]
  node [
    id 39
    label "gwiazda"
  ]
  node [
    id 40
    label "miniatura"
  ]
  node [
    id 41
    label "zwa&#322;"
  ]
  node [
    id 42
    label "sterta"
  ]
  node [
    id 43
    label "k&#322;oda"
  ]
  node [
    id 44
    label "sze&#347;cian"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
]
