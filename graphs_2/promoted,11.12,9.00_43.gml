graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "golf"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 3
    label "funkcja"
    origin "text"
  ]
  node [
    id 4
    label "pierwsze&#324;stwo"
    origin "text"
  ]
  node [
    id 5
    label "przodkini"
  ]
  node [
    id 6
    label "matka_zast&#281;pcza"
  ]
  node [
    id 7
    label "matczysko"
  ]
  node [
    id 8
    label "rodzice"
  ]
  node [
    id 9
    label "stara"
  ]
  node [
    id 10
    label "macierz"
  ]
  node [
    id 11
    label "rodzic"
  ]
  node [
    id 12
    label "Matka_Boska"
  ]
  node [
    id 13
    label "macocha"
  ]
  node [
    id 14
    label "starzy"
  ]
  node [
    id 15
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 16
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 17
    label "pokolenie"
  ]
  node [
    id 18
    label "wapniaki"
  ]
  node [
    id 19
    label "krewna"
  ]
  node [
    id 20
    label "opiekun"
  ]
  node [
    id 21
    label "wapniak"
  ]
  node [
    id 22
    label "rodzic_chrzestny"
  ]
  node [
    id 23
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 24
    label "matka"
  ]
  node [
    id 25
    label "&#380;ona"
  ]
  node [
    id 26
    label "kobieta"
  ]
  node [
    id 27
    label "partnerka"
  ]
  node [
    id 28
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 29
    label "matuszka"
  ]
  node [
    id 30
    label "parametryzacja"
  ]
  node [
    id 31
    label "pa&#324;stwo"
  ]
  node [
    id 32
    label "poj&#281;cie"
  ]
  node [
    id 33
    label "mod"
  ]
  node [
    id 34
    label "patriota"
  ]
  node [
    id 35
    label "m&#281;&#380;atka"
  ]
  node [
    id 36
    label "samoch&#243;d_osobowy"
  ]
  node [
    id 37
    label "bilard"
  ]
  node [
    id 38
    label "volkswagen"
  ]
  node [
    id 39
    label "sweter"
  ]
  node [
    id 40
    label "gra"
  ]
  node [
    id 41
    label "r&#281;kawica_golfowa"
  ]
  node [
    id 42
    label "samoch&#243;d"
  ]
  node [
    id 43
    label "sport"
  ]
  node [
    id 44
    label "Golf"
  ]
  node [
    id 45
    label "w&#243;zek_golfowy"
  ]
  node [
    id 46
    label "ko&#322;eczek_golfowy"
  ]
  node [
    id 47
    label "flaga_golfowa"
  ]
  node [
    id 48
    label "dekolt"
  ]
  node [
    id 49
    label "do&#322;ek"
  ]
  node [
    id 50
    label "zatoka"
  ]
  node [
    id 51
    label "Volkswagen"
  ]
  node [
    id 52
    label "pojazd_drogowy"
  ]
  node [
    id 53
    label "spryskiwacz"
  ]
  node [
    id 54
    label "most"
  ]
  node [
    id 55
    label "baga&#380;nik"
  ]
  node [
    id 56
    label "silnik"
  ]
  node [
    id 57
    label "dachowanie"
  ]
  node [
    id 58
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 59
    label "pompa_wodna"
  ]
  node [
    id 60
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 61
    label "poduszka_powietrzna"
  ]
  node [
    id 62
    label "tempomat"
  ]
  node [
    id 63
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 64
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 65
    label "deska_rozdzielcza"
  ]
  node [
    id 66
    label "immobilizer"
  ]
  node [
    id 67
    label "t&#322;umik"
  ]
  node [
    id 68
    label "ABS"
  ]
  node [
    id 69
    label "kierownica"
  ]
  node [
    id 70
    label "bak"
  ]
  node [
    id 71
    label "dwu&#347;lad"
  ]
  node [
    id 72
    label "poci&#261;g_drogowy"
  ]
  node [
    id 73
    label "wycieraczka"
  ]
  node [
    id 74
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 75
    label "st&#243;&#322;"
  ]
  node [
    id 76
    label "skiksowanie"
  ]
  node [
    id 77
    label "kiksowanie"
  ]
  node [
    id 78
    label "skiksowa&#263;"
  ]
  node [
    id 79
    label "sztos"
  ]
  node [
    id 80
    label "&#322;uza"
  ]
  node [
    id 81
    label "kiksowa&#263;"
  ]
  node [
    id 82
    label "banda"
  ]
  node [
    id 83
    label "rest"
  ]
  node [
    id 84
    label "g&#243;ra"
  ]
  node [
    id 85
    label "dzianina"
  ]
  node [
    id 86
    label "tu&#322;&#243;w"
  ]
  node [
    id 87
    label "wyko&#324;czenie"
  ]
  node [
    id 88
    label "prz&#243;d"
  ]
  node [
    id 89
    label "zmienno&#347;&#263;"
  ]
  node [
    id 90
    label "play"
  ]
  node [
    id 91
    label "rozgrywka"
  ]
  node [
    id 92
    label "apparent_motion"
  ]
  node [
    id 93
    label "wydarzenie"
  ]
  node [
    id 94
    label "contest"
  ]
  node [
    id 95
    label "akcja"
  ]
  node [
    id 96
    label "komplet"
  ]
  node [
    id 97
    label "zabawa"
  ]
  node [
    id 98
    label "zasada"
  ]
  node [
    id 99
    label "rywalizacja"
  ]
  node [
    id 100
    label "zbijany"
  ]
  node [
    id 101
    label "post&#281;powanie"
  ]
  node [
    id 102
    label "game"
  ]
  node [
    id 103
    label "odg&#322;os"
  ]
  node [
    id 104
    label "Pok&#233;mon"
  ]
  node [
    id 105
    label "czynno&#347;&#263;"
  ]
  node [
    id 106
    label "synteza"
  ]
  node [
    id 107
    label "odtworzenie"
  ]
  node [
    id 108
    label "rekwizyt_do_gry"
  ]
  node [
    id 109
    label "zgrupowanie"
  ]
  node [
    id 110
    label "kultura_fizyczna"
  ]
  node [
    id 111
    label "usportowienie"
  ]
  node [
    id 112
    label "atakowa&#263;"
  ]
  node [
    id 113
    label "zaatakowanie"
  ]
  node [
    id 114
    label "atakowanie"
  ]
  node [
    id 115
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 116
    label "zaatakowa&#263;"
  ]
  node [
    id 117
    label "usportowi&#263;"
  ]
  node [
    id 118
    label "sokolstwo"
  ]
  node [
    id 119
    label "areszt"
  ]
  node [
    id 120
    label "faza"
  ]
  node [
    id 121
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 122
    label "l&#261;d"
  ]
  node [
    id 123
    label "depressive_disorder"
  ]
  node [
    id 124
    label "bruzda"
  ]
  node [
    id 125
    label "obszar"
  ]
  node [
    id 126
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 127
    label "Pampa"
  ]
  node [
    id 128
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 129
    label "forma"
  ]
  node [
    id 130
    label "odlewnictwo"
  ]
  node [
    id 131
    label "za&#322;amanie"
  ]
  node [
    id 132
    label "woda"
  ]
  node [
    id 133
    label "przystanek"
  ]
  node [
    id 134
    label "korona_drogi"
  ]
  node [
    id 135
    label "zaburzenie"
  ]
  node [
    id 136
    label "jama"
  ]
  node [
    id 137
    label "Zatoka_Botnicka"
  ]
  node [
    id 138
    label "zbiornik_wodny"
  ]
  node [
    id 139
    label "jezdnia"
  ]
  node [
    id 140
    label "&#322;ysina"
  ]
  node [
    id 141
    label "nastawi&#263;"
  ]
  node [
    id 142
    label "draw"
  ]
  node [
    id 143
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 144
    label "incorporate"
  ]
  node [
    id 145
    label "obejrze&#263;"
  ]
  node [
    id 146
    label "impersonate"
  ]
  node [
    id 147
    label "dokoptowa&#263;"
  ]
  node [
    id 148
    label "prosecute"
  ]
  node [
    id 149
    label "uruchomi&#263;"
  ]
  node [
    id 150
    label "umie&#347;ci&#263;"
  ]
  node [
    id 151
    label "zacz&#261;&#263;"
  ]
  node [
    id 152
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 153
    label "spowodowa&#263;"
  ]
  node [
    id 154
    label "begin"
  ]
  node [
    id 155
    label "trip"
  ]
  node [
    id 156
    label "post&#261;pi&#263;"
  ]
  node [
    id 157
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 158
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 159
    label "odj&#261;&#263;"
  ]
  node [
    id 160
    label "zrobi&#263;"
  ]
  node [
    id 161
    label "cause"
  ]
  node [
    id 162
    label "introduce"
  ]
  node [
    id 163
    label "do"
  ]
  node [
    id 164
    label "set"
  ]
  node [
    id 165
    label "put"
  ]
  node [
    id 166
    label "uplasowa&#263;"
  ]
  node [
    id 167
    label "wpierniczy&#263;"
  ]
  node [
    id 168
    label "okre&#347;li&#263;"
  ]
  node [
    id 169
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 170
    label "zmieni&#263;"
  ]
  node [
    id 171
    label "umieszcza&#263;"
  ]
  node [
    id 172
    label "z&#322;amanie"
  ]
  node [
    id 173
    label "plant"
  ]
  node [
    id 174
    label "ustawi&#263;"
  ]
  node [
    id 175
    label "direct"
  ]
  node [
    id 176
    label "aim"
  ]
  node [
    id 177
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 178
    label "poprawi&#263;"
  ]
  node [
    id 179
    label "przyrz&#261;dzi&#263;"
  ]
  node [
    id 180
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 181
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 182
    label "wyznaczy&#263;"
  ]
  node [
    id 183
    label "visualize"
  ]
  node [
    id 184
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 185
    label "dokooptowa&#263;"
  ]
  node [
    id 186
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 187
    label "czyn"
  ]
  node [
    id 188
    label "supremum"
  ]
  node [
    id 189
    label "addytywno&#347;&#263;"
  ]
  node [
    id 190
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 191
    label "jednostka"
  ]
  node [
    id 192
    label "function"
  ]
  node [
    id 193
    label "zastosowanie"
  ]
  node [
    id 194
    label "matematyka"
  ]
  node [
    id 195
    label "funkcjonowanie"
  ]
  node [
    id 196
    label "praca"
  ]
  node [
    id 197
    label "rzut"
  ]
  node [
    id 198
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 199
    label "powierzanie"
  ]
  node [
    id 200
    label "cel"
  ]
  node [
    id 201
    label "dziedzina"
  ]
  node [
    id 202
    label "przeciwdziedzina"
  ]
  node [
    id 203
    label "awansowa&#263;"
  ]
  node [
    id 204
    label "stawia&#263;"
  ]
  node [
    id 205
    label "wakowa&#263;"
  ]
  node [
    id 206
    label "znaczenie"
  ]
  node [
    id 207
    label "postawi&#263;"
  ]
  node [
    id 208
    label "awansowanie"
  ]
  node [
    id 209
    label "infimum"
  ]
  node [
    id 210
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 211
    label "najem"
  ]
  node [
    id 212
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 213
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 214
    label "zak&#322;ad"
  ]
  node [
    id 215
    label "stosunek_pracy"
  ]
  node [
    id 216
    label "benedykty&#324;ski"
  ]
  node [
    id 217
    label "poda&#380;_pracy"
  ]
  node [
    id 218
    label "pracowanie"
  ]
  node [
    id 219
    label "tyrka"
  ]
  node [
    id 220
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 221
    label "wytw&#243;r"
  ]
  node [
    id 222
    label "miejsce"
  ]
  node [
    id 223
    label "zaw&#243;d"
  ]
  node [
    id 224
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 225
    label "tynkarski"
  ]
  node [
    id 226
    label "pracowa&#263;"
  ]
  node [
    id 227
    label "zmiana"
  ]
  node [
    id 228
    label "czynnik_produkcji"
  ]
  node [
    id 229
    label "zobowi&#261;zanie"
  ]
  node [
    id 230
    label "kierownictwo"
  ]
  node [
    id 231
    label "siedziba"
  ]
  node [
    id 232
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 233
    label "punkt"
  ]
  node [
    id 234
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 235
    label "rezultat"
  ]
  node [
    id 236
    label "thing"
  ]
  node [
    id 237
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 238
    label "rzecz"
  ]
  node [
    id 239
    label "odk&#322;adanie"
  ]
  node [
    id 240
    label "condition"
  ]
  node [
    id 241
    label "liczenie"
  ]
  node [
    id 242
    label "stawianie"
  ]
  node [
    id 243
    label "bycie"
  ]
  node [
    id 244
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 245
    label "assay"
  ]
  node [
    id 246
    label "wskazywanie"
  ]
  node [
    id 247
    label "wyraz"
  ]
  node [
    id 248
    label "gravity"
  ]
  node [
    id 249
    label "weight"
  ]
  node [
    id 250
    label "command"
  ]
  node [
    id 251
    label "odgrywanie_roli"
  ]
  node [
    id 252
    label "istota"
  ]
  node [
    id 253
    label "informacja"
  ]
  node [
    id 254
    label "cecha"
  ]
  node [
    id 255
    label "okre&#347;lanie"
  ]
  node [
    id 256
    label "kto&#347;"
  ]
  node [
    id 257
    label "wyra&#380;enie"
  ]
  node [
    id 258
    label "oddawanie"
  ]
  node [
    id 259
    label "stanowisko"
  ]
  node [
    id 260
    label "zlecanie"
  ]
  node [
    id 261
    label "ufanie"
  ]
  node [
    id 262
    label "wyznawanie"
  ]
  node [
    id 263
    label "zadanie"
  ]
  node [
    id 264
    label "przej&#347;cie"
  ]
  node [
    id 265
    label "przechodzenie"
  ]
  node [
    id 266
    label "przeniesienie"
  ]
  node [
    id 267
    label "status"
  ]
  node [
    id 268
    label "promowanie"
  ]
  node [
    id 269
    label "habilitowanie_si&#281;"
  ]
  node [
    id 270
    label "obj&#281;cie"
  ]
  node [
    id 271
    label "obejmowanie"
  ]
  node [
    id 272
    label "kariera"
  ]
  node [
    id 273
    label "przenoszenie"
  ]
  node [
    id 274
    label "pozyskiwanie"
  ]
  node [
    id 275
    label "pozyskanie"
  ]
  node [
    id 276
    label "pi&#281;cie_si&#281;"
  ]
  node [
    id 277
    label "zafundowa&#263;"
  ]
  node [
    id 278
    label "budowla"
  ]
  node [
    id 279
    label "wyda&#263;"
  ]
  node [
    id 280
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 281
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 282
    label "pozostawi&#263;"
  ]
  node [
    id 283
    label "obra&#263;"
  ]
  node [
    id 284
    label "peddle"
  ]
  node [
    id 285
    label "za&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 286
    label "obstawi&#263;"
  ]
  node [
    id 287
    label "post"
  ]
  node [
    id 288
    label "oceni&#263;"
  ]
  node [
    id 289
    label "u&#322;o&#380;y&#263;"
  ]
  node [
    id 290
    label "uczyni&#263;"
  ]
  node [
    id 291
    label "znak"
  ]
  node [
    id 292
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 293
    label "wytworzy&#263;"
  ]
  node [
    id 294
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 295
    label "przypu&#347;ci&#263;"
  ]
  node [
    id 296
    label "wskaza&#263;"
  ]
  node [
    id 297
    label "przyzna&#263;"
  ]
  node [
    id 298
    label "wydoby&#263;"
  ]
  node [
    id 299
    label "przedstawi&#263;"
  ]
  node [
    id 300
    label "establish"
  ]
  node [
    id 301
    label "stawi&#263;"
  ]
  node [
    id 302
    label "pozostawia&#263;"
  ]
  node [
    id 303
    label "czyni&#263;"
  ]
  node [
    id 304
    label "wydawa&#263;"
  ]
  node [
    id 305
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 306
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 307
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 308
    label "raise"
  ]
  node [
    id 309
    label "przewidywa&#263;"
  ]
  node [
    id 310
    label "przyznawa&#263;"
  ]
  node [
    id 311
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 312
    label "go"
  ]
  node [
    id 313
    label "obstawia&#263;"
  ]
  node [
    id 314
    label "ocenia&#263;"
  ]
  node [
    id 315
    label "zastawia&#263;"
  ]
  node [
    id 316
    label "wskazywa&#263;"
  ]
  node [
    id 317
    label "uruchamia&#263;"
  ]
  node [
    id 318
    label "wytwarza&#263;"
  ]
  node [
    id 319
    label "fundowa&#263;"
  ]
  node [
    id 320
    label "zmienia&#263;"
  ]
  node [
    id 321
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 322
    label "deliver"
  ]
  node [
    id 323
    label "powodowa&#263;"
  ]
  node [
    id 324
    label "wyznacza&#263;"
  ]
  node [
    id 325
    label "przedstawia&#263;"
  ]
  node [
    id 326
    label "wydobywa&#263;"
  ]
  node [
    id 327
    label "wolny"
  ]
  node [
    id 328
    label "by&#263;"
  ]
  node [
    id 329
    label "pozyska&#263;"
  ]
  node [
    id 330
    label "obejmowa&#263;"
  ]
  node [
    id 331
    label "pozyskiwa&#263;"
  ]
  node [
    id 332
    label "dawa&#263;_awans"
  ]
  node [
    id 333
    label "obj&#261;&#263;"
  ]
  node [
    id 334
    label "przej&#347;&#263;"
  ]
  node [
    id 335
    label "pi&#261;&#263;_si&#281;"
  ]
  node [
    id 336
    label "da&#263;_awans"
  ]
  node [
    id 337
    label "przechodzi&#263;"
  ]
  node [
    id 338
    label "przyswoi&#263;"
  ]
  node [
    id 339
    label "ludzko&#347;&#263;"
  ]
  node [
    id 340
    label "one"
  ]
  node [
    id 341
    label "ewoluowanie"
  ]
  node [
    id 342
    label "skala"
  ]
  node [
    id 343
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 344
    label "przyswajanie"
  ]
  node [
    id 345
    label "wyewoluowanie"
  ]
  node [
    id 346
    label "reakcja"
  ]
  node [
    id 347
    label "przeliczy&#263;"
  ]
  node [
    id 348
    label "wyewoluowa&#263;"
  ]
  node [
    id 349
    label "ewoluowa&#263;"
  ]
  node [
    id 350
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 351
    label "liczba_naturalna"
  ]
  node [
    id 352
    label "czynnik_biotyczny"
  ]
  node [
    id 353
    label "g&#322;owa"
  ]
  node [
    id 354
    label "figura"
  ]
  node [
    id 355
    label "individual"
  ]
  node [
    id 356
    label "portrecista"
  ]
  node [
    id 357
    label "obiekt"
  ]
  node [
    id 358
    label "przyswaja&#263;"
  ]
  node [
    id 359
    label "przyswojenie"
  ]
  node [
    id 360
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 361
    label "profanum"
  ]
  node [
    id 362
    label "mikrokosmos"
  ]
  node [
    id 363
    label "starzenie_si&#281;"
  ]
  node [
    id 364
    label "duch"
  ]
  node [
    id 365
    label "przeliczanie"
  ]
  node [
    id 366
    label "osoba"
  ]
  node [
    id 367
    label "oddzia&#322;ywanie"
  ]
  node [
    id 368
    label "antropochoria"
  ]
  node [
    id 369
    label "homo_sapiens"
  ]
  node [
    id 370
    label "przelicza&#263;"
  ]
  node [
    id 371
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 372
    label "przeliczenie"
  ]
  node [
    id 373
    label "ograniczenie"
  ]
  node [
    id 374
    label "armia"
  ]
  node [
    id 375
    label "nawr&#243;t_choroby"
  ]
  node [
    id 376
    label "potomstwo"
  ]
  node [
    id 377
    label "odwzorowanie"
  ]
  node [
    id 378
    label "rysunek"
  ]
  node [
    id 379
    label "scene"
  ]
  node [
    id 380
    label "throw"
  ]
  node [
    id 381
    label "float"
  ]
  node [
    id 382
    label "projection"
  ]
  node [
    id 383
    label "injection"
  ]
  node [
    id 384
    label "blow"
  ]
  node [
    id 385
    label "pomys&#322;"
  ]
  node [
    id 386
    label "ruch"
  ]
  node [
    id 387
    label "k&#322;ad"
  ]
  node [
    id 388
    label "mold"
  ]
  node [
    id 389
    label "rachunek_operatorowy"
  ]
  node [
    id 390
    label "przedmiot"
  ]
  node [
    id 391
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 392
    label "kryptologia"
  ]
  node [
    id 393
    label "logicyzm"
  ]
  node [
    id 394
    label "logika"
  ]
  node [
    id 395
    label "matematyka_czysta"
  ]
  node [
    id 396
    label "forsing"
  ]
  node [
    id 397
    label "modelowanie_matematyczne"
  ]
  node [
    id 398
    label "matma"
  ]
  node [
    id 399
    label "teoria_katastrof"
  ]
  node [
    id 400
    label "kierunek"
  ]
  node [
    id 401
    label "fizyka_matematyczna"
  ]
  node [
    id 402
    label "teoria_graf&#243;w"
  ]
  node [
    id 403
    label "rachunki"
  ]
  node [
    id 404
    label "topologia_algebraiczna"
  ]
  node [
    id 405
    label "matematyka_stosowana"
  ]
  node [
    id 406
    label "rozpoznawalno&#347;&#263;"
  ]
  node [
    id 407
    label "act"
  ]
  node [
    id 408
    label "stosowanie"
  ]
  node [
    id 409
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 410
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 411
    label "use"
  ]
  node [
    id 412
    label "zrobienie"
  ]
  node [
    id 413
    label "podtrzymywanie"
  ]
  node [
    id 414
    label "w&#322;&#261;czanie"
  ]
  node [
    id 415
    label "uruchamianie"
  ]
  node [
    id 416
    label "nakr&#281;cenie"
  ]
  node [
    id 417
    label "uruchomienie"
  ]
  node [
    id 418
    label "nakr&#281;canie"
  ]
  node [
    id 419
    label "impact"
  ]
  node [
    id 420
    label "tr&#243;jstronny"
  ]
  node [
    id 421
    label "dzianie_si&#281;"
  ]
  node [
    id 422
    label "w&#322;&#261;czenie"
  ]
  node [
    id 423
    label "zatrzymanie"
  ]
  node [
    id 424
    label "zbi&#243;r"
  ]
  node [
    id 425
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 426
    label "sfera"
  ]
  node [
    id 427
    label "zakres"
  ]
  node [
    id 428
    label "bezdro&#380;e"
  ]
  node [
    id 429
    label "poddzia&#322;"
  ]
  node [
    id 430
    label "przewaga"
  ]
  node [
    id 431
    label "laterality"
  ]
  node [
    id 432
    label "kolejno&#347;&#263;"
  ]
  node [
    id 433
    label "prym"
  ]
  node [
    id 434
    label "r&#243;&#380;nica"
  ]
  node [
    id 435
    label "advantage"
  ]
  node [
    id 436
    label "przemoc"
  ]
  node [
    id 437
    label "uk&#322;ad"
  ]
  node [
    id 438
    label "dominacja"
  ]
  node [
    id 439
    label "przek&#322;adanie"
  ]
  node [
    id 440
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 441
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 442
    label "predilection"
  ]
  node [
    id 443
    label "przek&#322;ada&#263;"
  ]
  node [
    id 444
    label "preponderencja"
  ]
  node [
    id 445
    label "supremacja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 389
  ]
  edge [
    source 3
    target 390
  ]
  edge [
    source 3
    target 391
  ]
  edge [
    source 3
    target 392
  ]
  edge [
    source 3
    target 393
  ]
  edge [
    source 3
    target 394
  ]
  edge [
    source 3
    target 395
  ]
  edge [
    source 3
    target 396
  ]
  edge [
    source 3
    target 397
  ]
  edge [
    source 3
    target 398
  ]
  edge [
    source 3
    target 399
  ]
  edge [
    source 3
    target 400
  ]
  edge [
    source 3
    target 401
  ]
  edge [
    source 3
    target 402
  ]
  edge [
    source 3
    target 403
  ]
  edge [
    source 3
    target 404
  ]
  edge [
    source 3
    target 405
  ]
  edge [
    source 3
    target 406
  ]
  edge [
    source 3
    target 407
  ]
  edge [
    source 3
    target 408
  ]
  edge [
    source 3
    target 409
  ]
  edge [
    source 3
    target 410
  ]
  edge [
    source 3
    target 411
  ]
  edge [
    source 3
    target 412
  ]
  edge [
    source 3
    target 413
  ]
  edge [
    source 3
    target 414
  ]
  edge [
    source 3
    target 415
  ]
  edge [
    source 3
    target 416
  ]
  edge [
    source 3
    target 417
  ]
  edge [
    source 3
    target 418
  ]
  edge [
    source 3
    target 419
  ]
  edge [
    source 3
    target 420
  ]
  edge [
    source 3
    target 421
  ]
  edge [
    source 3
    target 422
  ]
  edge [
    source 3
    target 423
  ]
  edge [
    source 3
    target 424
  ]
  edge [
    source 3
    target 425
  ]
  edge [
    source 3
    target 426
  ]
  edge [
    source 3
    target 427
  ]
  edge [
    source 3
    target 428
  ]
  edge [
    source 3
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
]
