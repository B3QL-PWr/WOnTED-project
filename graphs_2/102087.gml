graph [
  node [
    id 0
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 1
    label "czerwiec"
    origin "text"
  ]
  node [
    id 2
    label "filia"
    origin "text"
  ]
  node [
    id 3
    label "dla"
    origin "text"
  ]
  node [
    id 4
    label "dziecko"
    origin "text"
  ]
  node [
    id 5
    label "m&#322;odzie&#380;"
    origin "text"
  ]
  node [
    id 6
    label "przy"
    origin "text"
  ]
  node [
    id 7
    label "ula"
    origin "text"
  ]
  node [
    id 8
    label "mielczarski"
    origin "text"
  ]
  node [
    id 9
    label "pracownica"
    origin "text"
  ]
  node [
    id 10
    label "czerpalnia"
    origin "text"
  ]
  node [
    id 11
    label "papier"
    origin "text"
  ]
  node [
    id 12
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 13
    label "cykl"
    origin "text"
  ]
  node [
    id 14
    label "warsztat"
    origin "text"
  ]
  node [
    id 15
    label "ozorkowski"
    origin "text"
  ]
  node [
    id 16
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 17
    label "str&#243;j"
    origin "text"
  ]
  node [
    id 18
    label "epoka"
    origin "text"
  ]
  node [
    id 19
    label "wprowadzi&#263;"
    origin "text"
  ]
  node [
    id 20
    label "tajnik"
    origin "text"
  ]
  node [
    id 21
    label "powstawanie"
    origin "text"
  ]
  node [
    id 22
    label "ksi&#261;&#380;ka"
    origin "text"
  ]
  node [
    id 23
    label "sprzed"
    origin "text"
  ]
  node [
    id 24
    label "gutenberg"
    origin "text"
  ]
  node [
    id 25
    label "uczestnica"
    origin "text"
  ]
  node [
    id 26
    label "by&#263;"
    origin "text"
  ]
  node [
    id 27
    label "te&#380;"
    origin "text"
  ]
  node [
    id 28
    label "mogel"
    origin "text"
  ]
  node [
    id 29
    label "samodzielnie"
    origin "text"
  ]
  node [
    id 30
    label "ufarbowa&#263;"
    origin "text"
  ]
  node [
    id 31
    label "metoda"
    origin "text"
  ]
  node [
    id 32
    label "japo&#324;ski"
    origin "text"
  ]
  node [
    id 33
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 34
    label "g&#281;si"
    origin "text"
  ]
  node [
    id 35
    label "pi&#243;ro"
    origin "text"
  ]
  node [
    id 36
    label "odciska&#263;"
    origin "text"
  ]
  node [
    id 37
    label "lakowy"
    origin "text"
  ]
  node [
    id 38
    label "ranek"
  ]
  node [
    id 39
    label "doba"
  ]
  node [
    id 40
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 41
    label "noc"
  ]
  node [
    id 42
    label "podwiecz&#243;r"
  ]
  node [
    id 43
    label "po&#322;udnie"
  ]
  node [
    id 44
    label "godzina"
  ]
  node [
    id 45
    label "przedpo&#322;udnie"
  ]
  node [
    id 46
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 47
    label "long_time"
  ]
  node [
    id 48
    label "wiecz&#243;r"
  ]
  node [
    id 49
    label "t&#322;usty_czwartek"
  ]
  node [
    id 50
    label "popo&#322;udnie"
  ]
  node [
    id 51
    label "walentynki"
  ]
  node [
    id 52
    label "czynienie_si&#281;"
  ]
  node [
    id 53
    label "s&#322;o&#324;ce"
  ]
  node [
    id 54
    label "rano"
  ]
  node [
    id 55
    label "tydzie&#324;"
  ]
  node [
    id 56
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 57
    label "wzej&#347;cie"
  ]
  node [
    id 58
    label "czas"
  ]
  node [
    id 59
    label "wsta&#263;"
  ]
  node [
    id 60
    label "day"
  ]
  node [
    id 61
    label "termin"
  ]
  node [
    id 62
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 63
    label "wstanie"
  ]
  node [
    id 64
    label "przedwiecz&#243;r"
  ]
  node [
    id 65
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 66
    label "Sylwester"
  ]
  node [
    id 67
    label "poprzedzanie"
  ]
  node [
    id 68
    label "czasoprzestrze&#324;"
  ]
  node [
    id 69
    label "laba"
  ]
  node [
    id 70
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 71
    label "chronometria"
  ]
  node [
    id 72
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 73
    label "rachuba_czasu"
  ]
  node [
    id 74
    label "przep&#322;ywanie"
  ]
  node [
    id 75
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 76
    label "czasokres"
  ]
  node [
    id 77
    label "odczyt"
  ]
  node [
    id 78
    label "chwila"
  ]
  node [
    id 79
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 80
    label "dzieje"
  ]
  node [
    id 81
    label "kategoria_gramatyczna"
  ]
  node [
    id 82
    label "poprzedzenie"
  ]
  node [
    id 83
    label "trawienie"
  ]
  node [
    id 84
    label "pochodzi&#263;"
  ]
  node [
    id 85
    label "period"
  ]
  node [
    id 86
    label "okres_czasu"
  ]
  node [
    id 87
    label "poprzedza&#263;"
  ]
  node [
    id 88
    label "schy&#322;ek"
  ]
  node [
    id 89
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 90
    label "odwlekanie_si&#281;"
  ]
  node [
    id 91
    label "zegar"
  ]
  node [
    id 92
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 93
    label "czwarty_wymiar"
  ]
  node [
    id 94
    label "pochodzenie"
  ]
  node [
    id 95
    label "koniugacja"
  ]
  node [
    id 96
    label "Zeitgeist"
  ]
  node [
    id 97
    label "trawi&#263;"
  ]
  node [
    id 98
    label "pogoda"
  ]
  node [
    id 99
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 100
    label "poprzedzi&#263;"
  ]
  node [
    id 101
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 102
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 103
    label "time_period"
  ]
  node [
    id 104
    label "nazewnictwo"
  ]
  node [
    id 105
    label "term"
  ]
  node [
    id 106
    label "przypadni&#281;cie"
  ]
  node [
    id 107
    label "ekspiracja"
  ]
  node [
    id 108
    label "przypa&#347;&#263;"
  ]
  node [
    id 109
    label "chronogram"
  ]
  node [
    id 110
    label "praktyka"
  ]
  node [
    id 111
    label "nazwa"
  ]
  node [
    id 112
    label "przyj&#281;cie"
  ]
  node [
    id 113
    label "spotkanie"
  ]
  node [
    id 114
    label "night"
  ]
  node [
    id 115
    label "zach&#243;d"
  ]
  node [
    id 116
    label "vesper"
  ]
  node [
    id 117
    label "pora"
  ]
  node [
    id 118
    label "odwieczerz"
  ]
  node [
    id 119
    label "blady_&#347;wit"
  ]
  node [
    id 120
    label "podkurek"
  ]
  node [
    id 121
    label "aurora"
  ]
  node [
    id 122
    label "wsch&#243;d"
  ]
  node [
    id 123
    label "zjawisko"
  ]
  node [
    id 124
    label "&#347;rodek"
  ]
  node [
    id 125
    label "obszar"
  ]
  node [
    id 126
    label "Ziemia"
  ]
  node [
    id 127
    label "dwunasta"
  ]
  node [
    id 128
    label "strona_&#347;wiata"
  ]
  node [
    id 129
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 130
    label "dopo&#322;udnie"
  ]
  node [
    id 131
    label "p&#243;&#322;noc"
  ]
  node [
    id 132
    label "nokturn"
  ]
  node [
    id 133
    label "time"
  ]
  node [
    id 134
    label "p&#243;&#322;godzina"
  ]
  node [
    id 135
    label "jednostka_czasu"
  ]
  node [
    id 136
    label "minuta"
  ]
  node [
    id 137
    label "kwadrans"
  ]
  node [
    id 138
    label "jednostka_geologiczna"
  ]
  node [
    id 139
    label "weekend"
  ]
  node [
    id 140
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 141
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 142
    label "miesi&#261;c"
  ]
  node [
    id 143
    label "S&#322;o&#324;ce"
  ]
  node [
    id 144
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 145
    label "&#347;wiat&#322;o"
  ]
  node [
    id 146
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 147
    label "kochanie"
  ]
  node [
    id 148
    label "sunlight"
  ]
  node [
    id 149
    label "wy&#322;onienie_si&#281;"
  ]
  node [
    id 150
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 151
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 152
    label "podnie&#347;&#263;_si&#281;"
  ]
  node [
    id 153
    label "mount"
  ]
  node [
    id 154
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 155
    label "wzej&#347;&#263;"
  ]
  node [
    id 156
    label "ascend"
  ]
  node [
    id 157
    label "kuca&#263;"
  ]
  node [
    id 158
    label "wyzdrowie&#263;"
  ]
  node [
    id 159
    label "opu&#347;ci&#263;"
  ]
  node [
    id 160
    label "rise"
  ]
  node [
    id 161
    label "arise"
  ]
  node [
    id 162
    label "stan&#261;&#263;"
  ]
  node [
    id 163
    label "przesta&#263;"
  ]
  node [
    id 164
    label "wyzdrowienie"
  ]
  node [
    id 165
    label "le&#380;enie"
  ]
  node [
    id 166
    label "kl&#281;czenie"
  ]
  node [
    id 167
    label "opuszczenie"
  ]
  node [
    id 168
    label "uniesienie_si&#281;"
  ]
  node [
    id 169
    label "siedzenie"
  ]
  node [
    id 170
    label "beginning"
  ]
  node [
    id 171
    label "przestanie"
  ]
  node [
    id 172
    label "grudzie&#324;"
  ]
  node [
    id 173
    label "luty"
  ]
  node [
    id 174
    label "ro&#347;lina_zielna"
  ]
  node [
    id 175
    label "go&#378;dzikowate"
  ]
  node [
    id 176
    label "Bo&#380;e_Cia&#322;o"
  ]
  node [
    id 177
    label "miech"
  ]
  node [
    id 178
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 179
    label "rok"
  ]
  node [
    id 180
    label "kalendy"
  ]
  node [
    id 181
    label "go&#378;dzikowce"
  ]
  node [
    id 182
    label "agencja"
  ]
  node [
    id 183
    label "dzia&#322;"
  ]
  node [
    id 184
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 185
    label "jednostka_organizacyjna"
  ]
  node [
    id 186
    label "urz&#261;d"
  ]
  node [
    id 187
    label "sfera"
  ]
  node [
    id 188
    label "zakres"
  ]
  node [
    id 189
    label "miejsce_pracy"
  ]
  node [
    id 190
    label "zesp&#243;&#322;"
  ]
  node [
    id 191
    label "insourcing"
  ]
  node [
    id 192
    label "whole"
  ]
  node [
    id 193
    label "wytw&#243;r"
  ]
  node [
    id 194
    label "column"
  ]
  node [
    id 195
    label "distribution"
  ]
  node [
    id 196
    label "stopie&#324;"
  ]
  node [
    id 197
    label "competence"
  ]
  node [
    id 198
    label "bezdro&#380;e"
  ]
  node [
    id 199
    label "poddzia&#322;"
  ]
  node [
    id 200
    label "ajencja"
  ]
  node [
    id 201
    label "przedstawicielstwo"
  ]
  node [
    id 202
    label "instytucja"
  ]
  node [
    id 203
    label "firma"
  ]
  node [
    id 204
    label "siedziba"
  ]
  node [
    id 205
    label "NASA"
  ]
  node [
    id 206
    label "oddzia&#322;"
  ]
  node [
    id 207
    label "bank"
  ]
  node [
    id 208
    label "utulenie"
  ]
  node [
    id 209
    label "pediatra"
  ]
  node [
    id 210
    label "dzieciak"
  ]
  node [
    id 211
    label "utulanie"
  ]
  node [
    id 212
    label "dzieciarnia"
  ]
  node [
    id 213
    label "cz&#322;owiek"
  ]
  node [
    id 214
    label "niepe&#322;noletni"
  ]
  node [
    id 215
    label "organizm"
  ]
  node [
    id 216
    label "utula&#263;"
  ]
  node [
    id 217
    label "cz&#322;owieczek"
  ]
  node [
    id 218
    label "fledgling"
  ]
  node [
    id 219
    label "zwierz&#281;"
  ]
  node [
    id 220
    label "utuli&#263;"
  ]
  node [
    id 221
    label "m&#322;odzik"
  ]
  node [
    id 222
    label "pedofil"
  ]
  node [
    id 223
    label "m&#322;odziak"
  ]
  node [
    id 224
    label "potomek"
  ]
  node [
    id 225
    label "entliczek-pentliczek"
  ]
  node [
    id 226
    label "potomstwo"
  ]
  node [
    id 227
    label "sraluch"
  ]
  node [
    id 228
    label "zbi&#243;r"
  ]
  node [
    id 229
    label "czeladka"
  ]
  node [
    id 230
    label "dzietno&#347;&#263;"
  ]
  node [
    id 231
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 232
    label "bawienie_si&#281;"
  ]
  node [
    id 233
    label "pomiot"
  ]
  node [
    id 234
    label "grupa"
  ]
  node [
    id 235
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 236
    label "kinderbal"
  ]
  node [
    id 237
    label "krewny"
  ]
  node [
    id 238
    label "ludzko&#347;&#263;"
  ]
  node [
    id 239
    label "asymilowanie"
  ]
  node [
    id 240
    label "wapniak"
  ]
  node [
    id 241
    label "asymilowa&#263;"
  ]
  node [
    id 242
    label "os&#322;abia&#263;"
  ]
  node [
    id 243
    label "posta&#263;"
  ]
  node [
    id 244
    label "hominid"
  ]
  node [
    id 245
    label "podw&#322;adny"
  ]
  node [
    id 246
    label "os&#322;abianie"
  ]
  node [
    id 247
    label "g&#322;owa"
  ]
  node [
    id 248
    label "figura"
  ]
  node [
    id 249
    label "portrecista"
  ]
  node [
    id 250
    label "dwun&#243;g"
  ]
  node [
    id 251
    label "profanum"
  ]
  node [
    id 252
    label "mikrokosmos"
  ]
  node [
    id 253
    label "nasada"
  ]
  node [
    id 254
    label "duch"
  ]
  node [
    id 255
    label "antropochoria"
  ]
  node [
    id 256
    label "osoba"
  ]
  node [
    id 257
    label "wz&#243;r"
  ]
  node [
    id 258
    label "senior"
  ]
  node [
    id 259
    label "oddzia&#322;ywanie"
  ]
  node [
    id 260
    label "Adam"
  ]
  node [
    id 261
    label "homo_sapiens"
  ]
  node [
    id 262
    label "polifag"
  ]
  node [
    id 263
    label "ma&#322;oletny"
  ]
  node [
    id 264
    label "m&#322;ody"
  ]
  node [
    id 265
    label "degenerat"
  ]
  node [
    id 266
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 267
    label "zwyrol"
  ]
  node [
    id 268
    label "czerniak"
  ]
  node [
    id 269
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 270
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 271
    label "paszcza"
  ]
  node [
    id 272
    label "popapraniec"
  ]
  node [
    id 273
    label "skuba&#263;"
  ]
  node [
    id 274
    label "skubanie"
  ]
  node [
    id 275
    label "skubni&#281;cie"
  ]
  node [
    id 276
    label "agresja"
  ]
  node [
    id 277
    label "zwierz&#281;ta"
  ]
  node [
    id 278
    label "fukni&#281;cie"
  ]
  node [
    id 279
    label "farba"
  ]
  node [
    id 280
    label "fukanie"
  ]
  node [
    id 281
    label "istota_&#380;ywa"
  ]
  node [
    id 282
    label "gad"
  ]
  node [
    id 283
    label "siedzie&#263;"
  ]
  node [
    id 284
    label "oswaja&#263;"
  ]
  node [
    id 285
    label "tresowa&#263;"
  ]
  node [
    id 286
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 287
    label "poligamia"
  ]
  node [
    id 288
    label "oz&#243;r"
  ]
  node [
    id 289
    label "skubn&#261;&#263;"
  ]
  node [
    id 290
    label "wios&#322;owa&#263;"
  ]
  node [
    id 291
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 292
    label "niecz&#322;owiek"
  ]
  node [
    id 293
    label "wios&#322;owanie"
  ]
  node [
    id 294
    label "napasienie_si&#281;"
  ]
  node [
    id 295
    label "wiwarium"
  ]
  node [
    id 296
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 297
    label "animalista"
  ]
  node [
    id 298
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 299
    label "budowa"
  ]
  node [
    id 300
    label "hodowla"
  ]
  node [
    id 301
    label "pasienie_si&#281;"
  ]
  node [
    id 302
    label "sodomita"
  ]
  node [
    id 303
    label "monogamia"
  ]
  node [
    id 304
    label "przyssawka"
  ]
  node [
    id 305
    label "zachowanie"
  ]
  node [
    id 306
    label "budowa_cia&#322;a"
  ]
  node [
    id 307
    label "okrutnik"
  ]
  node [
    id 308
    label "grzbiet"
  ]
  node [
    id 309
    label "weterynarz"
  ]
  node [
    id 310
    label "&#322;eb"
  ]
  node [
    id 311
    label "wylinka"
  ]
  node [
    id 312
    label "bestia"
  ]
  node [
    id 313
    label "poskramia&#263;"
  ]
  node [
    id 314
    label "fauna"
  ]
  node [
    id 315
    label "treser"
  ]
  node [
    id 316
    label "le&#380;e&#263;"
  ]
  node [
    id 317
    label "p&#322;aszczyzna"
  ]
  node [
    id 318
    label "odwadnia&#263;"
  ]
  node [
    id 319
    label "przyswoi&#263;"
  ]
  node [
    id 320
    label "sk&#243;ra"
  ]
  node [
    id 321
    label "odwodni&#263;"
  ]
  node [
    id 322
    label "ewoluowanie"
  ]
  node [
    id 323
    label "staw"
  ]
  node [
    id 324
    label "ow&#322;osienie"
  ]
  node [
    id 325
    label "unerwienie"
  ]
  node [
    id 326
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 327
    label "reakcja"
  ]
  node [
    id 328
    label "wyewoluowanie"
  ]
  node [
    id 329
    label "przyswajanie"
  ]
  node [
    id 330
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 331
    label "wyewoluowa&#263;"
  ]
  node [
    id 332
    label "miejsce"
  ]
  node [
    id 333
    label "biorytm"
  ]
  node [
    id 334
    label "ewoluowa&#263;"
  ]
  node [
    id 335
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 336
    label "otworzy&#263;"
  ]
  node [
    id 337
    label "otwiera&#263;"
  ]
  node [
    id 338
    label "czynnik_biotyczny"
  ]
  node [
    id 339
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 340
    label "otworzenie"
  ]
  node [
    id 341
    label "otwieranie"
  ]
  node [
    id 342
    label "individual"
  ]
  node [
    id 343
    label "szkielet"
  ]
  node [
    id 344
    label "ty&#322;"
  ]
  node [
    id 345
    label "obiekt"
  ]
  node [
    id 346
    label "przyswaja&#263;"
  ]
  node [
    id 347
    label "przyswojenie"
  ]
  node [
    id 348
    label "odwadnianie"
  ]
  node [
    id 349
    label "odwodnienie"
  ]
  node [
    id 350
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 351
    label "starzenie_si&#281;"
  ]
  node [
    id 352
    label "prz&#243;d"
  ]
  node [
    id 353
    label "uk&#322;ad"
  ]
  node [
    id 354
    label "temperatura"
  ]
  node [
    id 355
    label "l&#281;d&#378;wie"
  ]
  node [
    id 356
    label "cia&#322;o"
  ]
  node [
    id 357
    label "cz&#322;onek"
  ]
  node [
    id 358
    label "utulanie_si&#281;"
  ]
  node [
    id 359
    label "usypianie"
  ]
  node [
    id 360
    label "pocieszanie"
  ]
  node [
    id 361
    label "uspokajanie"
  ]
  node [
    id 362
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 363
    label "uspokoi&#263;"
  ]
  node [
    id 364
    label "uspokojenie"
  ]
  node [
    id 365
    label "utulenie_si&#281;"
  ]
  node [
    id 366
    label "u&#347;pienie"
  ]
  node [
    id 367
    label "usypia&#263;"
  ]
  node [
    id 368
    label "uspokaja&#263;"
  ]
  node [
    id 369
    label "dewiant"
  ]
  node [
    id 370
    label "specjalista"
  ]
  node [
    id 371
    label "wyliczanka"
  ]
  node [
    id 372
    label "harcerz"
  ]
  node [
    id 373
    label "ch&#322;opta&#347;"
  ]
  node [
    id 374
    label "zawodnik"
  ]
  node [
    id 375
    label "go&#322;ow&#261;s"
  ]
  node [
    id 376
    label "m&#322;ode"
  ]
  node [
    id 377
    label "stopie&#324;_harcerski"
  ]
  node [
    id 378
    label "g&#243;wniarz"
  ]
  node [
    id 379
    label "beniaminek"
  ]
  node [
    id 380
    label "istotka"
  ]
  node [
    id 381
    label "bech"
  ]
  node [
    id 382
    label "dziecinny"
  ]
  node [
    id 383
    label "naiwniak"
  ]
  node [
    id 384
    label "smarkateria"
  ]
  node [
    id 385
    label "facylitacja"
  ]
  node [
    id 386
    label "papiernia"
  ]
  node [
    id 387
    label "wytw&#243;rnia"
  ]
  node [
    id 388
    label "nak&#322;uwacz"
  ]
  node [
    id 389
    label "parafa"
  ]
  node [
    id 390
    label "raport&#243;wka"
  ]
  node [
    id 391
    label "tworzywo"
  ]
  node [
    id 392
    label "papeteria"
  ]
  node [
    id 393
    label "fascyku&#322;"
  ]
  node [
    id 394
    label "dokumentacja"
  ]
  node [
    id 395
    label "registratura"
  ]
  node [
    id 396
    label "libra"
  ]
  node [
    id 397
    label "format_arkusza"
  ]
  node [
    id 398
    label "artyku&#322;"
  ]
  node [
    id 399
    label "writing"
  ]
  node [
    id 400
    label "sygnatariusz"
  ]
  node [
    id 401
    label "blok"
  ]
  node [
    id 402
    label "prawda"
  ]
  node [
    id 403
    label "znak_j&#281;zykowy"
  ]
  node [
    id 404
    label "nag&#322;&#243;wek"
  ]
  node [
    id 405
    label "szkic"
  ]
  node [
    id 406
    label "line"
  ]
  node [
    id 407
    label "fragment"
  ]
  node [
    id 408
    label "tekst"
  ]
  node [
    id 409
    label "wyr&#243;b"
  ]
  node [
    id 410
    label "rodzajnik"
  ]
  node [
    id 411
    label "dokument"
  ]
  node [
    id 412
    label "towar"
  ]
  node [
    id 413
    label "paragraf"
  ]
  node [
    id 414
    label "substancja"
  ]
  node [
    id 415
    label "przedmiot"
  ]
  node [
    id 416
    label "p&#322;&#243;d"
  ]
  node [
    id 417
    label "work"
  ]
  node [
    id 418
    label "rezultat"
  ]
  node [
    id 419
    label "stationery"
  ]
  node [
    id 420
    label "komplet"
  ]
  node [
    id 421
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 422
    label "szpikulec"
  ]
  node [
    id 423
    label "jednostka"
  ]
  node [
    id 424
    label "quire"
  ]
  node [
    id 425
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 426
    label "waga"
  ]
  node [
    id 427
    label "jednostka_masy"
  ]
  node [
    id 428
    label "przedstawiciel"
  ]
  node [
    id 429
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 430
    label "wydanie"
  ]
  node [
    id 431
    label "plik"
  ]
  node [
    id 432
    label "torba"
  ]
  node [
    id 433
    label "ekscerpcja"
  ]
  node [
    id 434
    label "materia&#322;"
  ]
  node [
    id 435
    label "operat"
  ]
  node [
    id 436
    label "kosztorys"
  ]
  node [
    id 437
    label "biuro"
  ]
  node [
    id 438
    label "wpis"
  ]
  node [
    id 439
    label "register"
  ]
  node [
    id 440
    label "paraph"
  ]
  node [
    id 441
    label "podpis"
  ]
  node [
    id 442
    label "wykona&#263;"
  ]
  node [
    id 443
    label "zbudowa&#263;"
  ]
  node [
    id 444
    label "draw"
  ]
  node [
    id 445
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 446
    label "carry"
  ]
  node [
    id 447
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 448
    label "leave"
  ]
  node [
    id 449
    label "przewie&#347;&#263;"
  ]
  node [
    id 450
    label "pom&#243;c"
  ]
  node [
    id 451
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 452
    label "profit"
  ]
  node [
    id 453
    label "score"
  ]
  node [
    id 454
    label "make"
  ]
  node [
    id 455
    label "dotrze&#263;"
  ]
  node [
    id 456
    label "uzyska&#263;"
  ]
  node [
    id 457
    label "wytworzy&#263;"
  ]
  node [
    id 458
    label "picture"
  ]
  node [
    id 459
    label "manufacture"
  ]
  node [
    id 460
    label "zrobi&#263;"
  ]
  node [
    id 461
    label "go"
  ]
  node [
    id 462
    label "spowodowa&#263;"
  ]
  node [
    id 463
    label "travel"
  ]
  node [
    id 464
    label "stworzy&#263;"
  ]
  node [
    id 465
    label "budowla"
  ]
  node [
    id 466
    label "establish"
  ]
  node [
    id 467
    label "evolve"
  ]
  node [
    id 468
    label "zaplanowa&#263;"
  ]
  node [
    id 469
    label "wear"
  ]
  node [
    id 470
    label "return"
  ]
  node [
    id 471
    label "plant"
  ]
  node [
    id 472
    label "pozostawi&#263;"
  ]
  node [
    id 473
    label "pokry&#263;"
  ]
  node [
    id 474
    label "znak"
  ]
  node [
    id 475
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 476
    label "przygotowa&#263;"
  ]
  node [
    id 477
    label "stagger"
  ]
  node [
    id 478
    label "zepsu&#263;"
  ]
  node [
    id 479
    label "zmieni&#263;"
  ]
  node [
    id 480
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 481
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 482
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 483
    label "umie&#347;ci&#263;"
  ]
  node [
    id 484
    label "zacz&#261;&#263;"
  ]
  node [
    id 485
    label "raise"
  ]
  node [
    id 486
    label "wygra&#263;"
  ]
  node [
    id 487
    label "aid"
  ]
  node [
    id 488
    label "concur"
  ]
  node [
    id 489
    label "help"
  ]
  node [
    id 490
    label "u&#322;atwi&#263;"
  ]
  node [
    id 491
    label "zaskutkowa&#263;"
  ]
  node [
    id 492
    label "set"
  ]
  node [
    id 493
    label "przebieg"
  ]
  node [
    id 494
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 495
    label "miesi&#261;czka"
  ]
  node [
    id 496
    label "okres"
  ]
  node [
    id 497
    label "owulacja"
  ]
  node [
    id 498
    label "sekwencja"
  ]
  node [
    id 499
    label "edycja"
  ]
  node [
    id 500
    label "cycle"
  ]
  node [
    id 501
    label "linia"
  ]
  node [
    id 502
    label "procedura"
  ]
  node [
    id 503
    label "proces"
  ]
  node [
    id 504
    label "room"
  ]
  node [
    id 505
    label "ilo&#347;&#263;"
  ]
  node [
    id 506
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 507
    label "sequence"
  ]
  node [
    id 508
    label "praca"
  ]
  node [
    id 509
    label "integer"
  ]
  node [
    id 510
    label "liczba"
  ]
  node [
    id 511
    label "zlewanie_si&#281;"
  ]
  node [
    id 512
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 513
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 514
    label "pe&#322;ny"
  ]
  node [
    id 515
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 516
    label "ci&#261;g"
  ]
  node [
    id 517
    label "kompozycja"
  ]
  node [
    id 518
    label "pie&#347;&#324;"
  ]
  node [
    id 519
    label "okres_amazo&#324;ski"
  ]
  node [
    id 520
    label "stater"
  ]
  node [
    id 521
    label "flow"
  ]
  node [
    id 522
    label "choroba_przyrodzona"
  ]
  node [
    id 523
    label "postglacja&#322;"
  ]
  node [
    id 524
    label "sylur"
  ]
  node [
    id 525
    label "kreda"
  ]
  node [
    id 526
    label "ordowik"
  ]
  node [
    id 527
    label "okres_hesperyjski"
  ]
  node [
    id 528
    label "paleogen"
  ]
  node [
    id 529
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 530
    label "okres_halsztacki"
  ]
  node [
    id 531
    label "riak"
  ]
  node [
    id 532
    label "czwartorz&#281;d"
  ]
  node [
    id 533
    label "podokres"
  ]
  node [
    id 534
    label "trzeciorz&#281;d"
  ]
  node [
    id 535
    label "kalim"
  ]
  node [
    id 536
    label "fala"
  ]
  node [
    id 537
    label "perm"
  ]
  node [
    id 538
    label "retoryka"
  ]
  node [
    id 539
    label "prekambr"
  ]
  node [
    id 540
    label "faza"
  ]
  node [
    id 541
    label "neogen"
  ]
  node [
    id 542
    label "pulsacja"
  ]
  node [
    id 543
    label "proces_fizjologiczny"
  ]
  node [
    id 544
    label "kambr"
  ]
  node [
    id 545
    label "kriogen"
  ]
  node [
    id 546
    label "ton"
  ]
  node [
    id 547
    label "orosir"
  ]
  node [
    id 548
    label "poprzednik"
  ]
  node [
    id 549
    label "spell"
  ]
  node [
    id 550
    label "interstadia&#322;"
  ]
  node [
    id 551
    label "ektas"
  ]
  node [
    id 552
    label "sider"
  ]
  node [
    id 553
    label "rok_akademicki"
  ]
  node [
    id 554
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 555
    label "ciota"
  ]
  node [
    id 556
    label "pierwszorz&#281;d"
  ]
  node [
    id 557
    label "okres_noachijski"
  ]
  node [
    id 558
    label "ediakar"
  ]
  node [
    id 559
    label "zdanie"
  ]
  node [
    id 560
    label "nast&#281;pnik"
  ]
  node [
    id 561
    label "condition"
  ]
  node [
    id 562
    label "jura"
  ]
  node [
    id 563
    label "glacja&#322;"
  ]
  node [
    id 564
    label "sten"
  ]
  node [
    id 565
    label "era"
  ]
  node [
    id 566
    label "trias"
  ]
  node [
    id 567
    label "p&#243;&#322;okres"
  ]
  node [
    id 568
    label "rok_szkolny"
  ]
  node [
    id 569
    label "dewon"
  ]
  node [
    id 570
    label "karbon"
  ]
  node [
    id 571
    label "izochronizm"
  ]
  node [
    id 572
    label "preglacja&#322;"
  ]
  node [
    id 573
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 574
    label "drugorz&#281;d"
  ]
  node [
    id 575
    label "semester"
  ]
  node [
    id 576
    label "gem"
  ]
  node [
    id 577
    label "runda"
  ]
  node [
    id 578
    label "muzyka"
  ]
  node [
    id 579
    label "zestaw"
  ]
  node [
    id 580
    label "egzemplarz"
  ]
  node [
    id 581
    label "impression"
  ]
  node [
    id 582
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 583
    label "odmiana"
  ]
  node [
    id 584
    label "notification"
  ]
  node [
    id 585
    label "zmiana"
  ]
  node [
    id 586
    label "produkcja"
  ]
  node [
    id 587
    label "proces_biologiczny"
  ]
  node [
    id 588
    label "sprawno&#347;&#263;"
  ]
  node [
    id 589
    label "st&#243;&#322;_roboczy"
  ]
  node [
    id 590
    label "wyposa&#380;enie"
  ]
  node [
    id 591
    label "pracownia"
  ]
  node [
    id 592
    label "warunek_lokalowy"
  ]
  node [
    id 593
    label "plac"
  ]
  node [
    id 594
    label "location"
  ]
  node [
    id 595
    label "uwaga"
  ]
  node [
    id 596
    label "przestrze&#324;"
  ]
  node [
    id 597
    label "status"
  ]
  node [
    id 598
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 599
    label "cecha"
  ]
  node [
    id 600
    label "rz&#261;d"
  ]
  node [
    id 601
    label "jako&#347;&#263;"
  ]
  node [
    id 602
    label "szybko&#347;&#263;"
  ]
  node [
    id 603
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 604
    label "kondycja_fizyczna"
  ]
  node [
    id 605
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 606
    label "zdrowie"
  ]
  node [
    id 607
    label "stan"
  ]
  node [
    id 608
    label "poj&#281;cie"
  ]
  node [
    id 609
    label "harcerski"
  ]
  node [
    id 610
    label "odznaka"
  ]
  node [
    id 611
    label "danie"
  ]
  node [
    id 612
    label "fixture"
  ]
  node [
    id 613
    label "zinformatyzowanie"
  ]
  node [
    id 614
    label "spowodowanie"
  ]
  node [
    id 615
    label "zainstalowanie"
  ]
  node [
    id 616
    label "urz&#261;dzenie"
  ]
  node [
    id 617
    label "zrobienie"
  ]
  node [
    id 618
    label "doznanie"
  ]
  node [
    id 619
    label "gathering"
  ]
  node [
    id 620
    label "zawarcie"
  ]
  node [
    id 621
    label "wydarzenie"
  ]
  node [
    id 622
    label "znajomy"
  ]
  node [
    id 623
    label "powitanie"
  ]
  node [
    id 624
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 625
    label "zdarzenie_si&#281;"
  ]
  node [
    id 626
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 627
    label "znalezienie"
  ]
  node [
    id 628
    label "match"
  ]
  node [
    id 629
    label "employment"
  ]
  node [
    id 630
    label "po&#380;egnanie"
  ]
  node [
    id 631
    label "gather"
  ]
  node [
    id 632
    label "spotykanie"
  ]
  node [
    id 633
    label "spotkanie_si&#281;"
  ]
  node [
    id 634
    label "pomieszczenie"
  ]
  node [
    id 635
    label "do&#347;wiadczenie"
  ]
  node [
    id 636
    label "teren_szko&#322;y"
  ]
  node [
    id 637
    label "wiedza"
  ]
  node [
    id 638
    label "Mickiewicz"
  ]
  node [
    id 639
    label "kwalifikacje"
  ]
  node [
    id 640
    label "podr&#281;cznik"
  ]
  node [
    id 641
    label "absolwent"
  ]
  node [
    id 642
    label "school"
  ]
  node [
    id 643
    label "system"
  ]
  node [
    id 644
    label "zda&#263;"
  ]
  node [
    id 645
    label "gabinet"
  ]
  node [
    id 646
    label "urszulanki"
  ]
  node [
    id 647
    label "sztuba"
  ]
  node [
    id 648
    label "&#322;awa_szkolna"
  ]
  node [
    id 649
    label "nauka"
  ]
  node [
    id 650
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 651
    label "przepisa&#263;"
  ]
  node [
    id 652
    label "form"
  ]
  node [
    id 653
    label "klasa"
  ]
  node [
    id 654
    label "lekcja"
  ]
  node [
    id 655
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 656
    label "przepisanie"
  ]
  node [
    id 657
    label "skolaryzacja"
  ]
  node [
    id 658
    label "stopek"
  ]
  node [
    id 659
    label "sekretariat"
  ]
  node [
    id 660
    label "ideologia"
  ]
  node [
    id 661
    label "lesson"
  ]
  node [
    id 662
    label "niepokalanki"
  ]
  node [
    id 663
    label "szkolenie"
  ]
  node [
    id 664
    label "kara"
  ]
  node [
    id 665
    label "tablica"
  ]
  node [
    id 666
    label "wyprawka"
  ]
  node [
    id 667
    label "pomoc_naukowa"
  ]
  node [
    id 668
    label "odm&#322;adzanie"
  ]
  node [
    id 669
    label "liga"
  ]
  node [
    id 670
    label "jednostka_systematyczna"
  ]
  node [
    id 671
    label "gromada"
  ]
  node [
    id 672
    label "Entuzjastki"
  ]
  node [
    id 673
    label "Terranie"
  ]
  node [
    id 674
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 675
    label "category"
  ]
  node [
    id 676
    label "pakiet_klimatyczny"
  ]
  node [
    id 677
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 678
    label "cz&#261;steczka"
  ]
  node [
    id 679
    label "stage_set"
  ]
  node [
    id 680
    label "type"
  ]
  node [
    id 681
    label "specgrupa"
  ]
  node [
    id 682
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 683
    label "&#346;wietliki"
  ]
  node [
    id 684
    label "odm&#322;odzenie"
  ]
  node [
    id 685
    label "Eurogrupa"
  ]
  node [
    id 686
    label "odm&#322;adza&#263;"
  ]
  node [
    id 687
    label "formacja_geologiczna"
  ]
  node [
    id 688
    label "harcerze_starsi"
  ]
  node [
    id 689
    label "course"
  ]
  node [
    id 690
    label "pomaganie"
  ]
  node [
    id 691
    label "training"
  ]
  node [
    id 692
    label "zapoznawanie"
  ]
  node [
    id 693
    label "seria"
  ]
  node [
    id 694
    label "zaj&#281;cia"
  ]
  node [
    id 695
    label "pouczenie"
  ]
  node [
    id 696
    label "o&#347;wiecanie"
  ]
  node [
    id 697
    label "Lira"
  ]
  node [
    id 698
    label "kliker"
  ]
  node [
    id 699
    label "miasteczko_rowerowe"
  ]
  node [
    id 700
    label "porada"
  ]
  node [
    id 701
    label "fotowoltaika"
  ]
  node [
    id 702
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 703
    label "przem&#243;wienie"
  ]
  node [
    id 704
    label "nauki_o_poznaniu"
  ]
  node [
    id 705
    label "nomotetyczny"
  ]
  node [
    id 706
    label "systematyka"
  ]
  node [
    id 707
    label "typologia"
  ]
  node [
    id 708
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 709
    label "kultura_duchowa"
  ]
  node [
    id 710
    label "nauki_penalne"
  ]
  node [
    id 711
    label "dziedzina"
  ]
  node [
    id 712
    label "imagineskopia"
  ]
  node [
    id 713
    label "teoria_naukowa"
  ]
  node [
    id 714
    label "inwentyka"
  ]
  node [
    id 715
    label "metodologia"
  ]
  node [
    id 716
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 717
    label "nauki_o_Ziemi"
  ]
  node [
    id 718
    label "eliminacje"
  ]
  node [
    id 719
    label "osoba_prawna"
  ]
  node [
    id 720
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 721
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 722
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 723
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 724
    label "organizacja"
  ]
  node [
    id 725
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 726
    label "Fundusze_Unijne"
  ]
  node [
    id 727
    label "zamyka&#263;"
  ]
  node [
    id 728
    label "establishment"
  ]
  node [
    id 729
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 730
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 731
    label "afiliowa&#263;"
  ]
  node [
    id 732
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 733
    label "standard"
  ]
  node [
    id 734
    label "zamykanie"
  ]
  node [
    id 735
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 736
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 737
    label "spos&#243;b"
  ]
  node [
    id 738
    label "obrz&#261;dek"
  ]
  node [
    id 739
    label "Biblia"
  ]
  node [
    id 740
    label "lektor"
  ]
  node [
    id 741
    label "kwota"
  ]
  node [
    id 742
    label "nemezis"
  ]
  node [
    id 743
    label "konsekwencja"
  ]
  node [
    id 744
    label "punishment"
  ]
  node [
    id 745
    label "klacz"
  ]
  node [
    id 746
    label "forfeit"
  ]
  node [
    id 747
    label "roboty_przymusowe"
  ]
  node [
    id 748
    label "j&#261;dro"
  ]
  node [
    id 749
    label "systemik"
  ]
  node [
    id 750
    label "rozprz&#261;c"
  ]
  node [
    id 751
    label "oprogramowanie"
  ]
  node [
    id 752
    label "systemat"
  ]
  node [
    id 753
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 754
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 755
    label "model"
  ]
  node [
    id 756
    label "struktura"
  ]
  node [
    id 757
    label "usenet"
  ]
  node [
    id 758
    label "s&#261;d"
  ]
  node [
    id 759
    label "porz&#261;dek"
  ]
  node [
    id 760
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 761
    label "przyn&#281;ta"
  ]
  node [
    id 762
    label "net"
  ]
  node [
    id 763
    label "w&#281;dkarstwo"
  ]
  node [
    id 764
    label "eratem"
  ]
  node [
    id 765
    label "doktryna"
  ]
  node [
    id 766
    label "pulpit"
  ]
  node [
    id 767
    label "konstelacja"
  ]
  node [
    id 768
    label "o&#347;"
  ]
  node [
    id 769
    label "podsystem"
  ]
  node [
    id 770
    label "ryba"
  ]
  node [
    id 771
    label "Leopard"
  ]
  node [
    id 772
    label "Android"
  ]
  node [
    id 773
    label "cybernetyk"
  ]
  node [
    id 774
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 775
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 776
    label "method"
  ]
  node [
    id 777
    label "sk&#322;ad"
  ]
  node [
    id 778
    label "podstawa"
  ]
  node [
    id 779
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 780
    label "practice"
  ]
  node [
    id 781
    label "znawstwo"
  ]
  node [
    id 782
    label "skill"
  ]
  node [
    id 783
    label "czyn"
  ]
  node [
    id 784
    label "zwyczaj"
  ]
  node [
    id 785
    label "eksperiencja"
  ]
  node [
    id 786
    label "&#321;ubianka"
  ]
  node [
    id 787
    label "dzia&#322;_personalny"
  ]
  node [
    id 788
    label "Kreml"
  ]
  node [
    id 789
    label "Bia&#322;y_Dom"
  ]
  node [
    id 790
    label "budynek"
  ]
  node [
    id 791
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 792
    label "sadowisko"
  ]
  node [
    id 793
    label "wokalistyka"
  ]
  node [
    id 794
    label "wykonywanie"
  ]
  node [
    id 795
    label "muza"
  ]
  node [
    id 796
    label "wykonywa&#263;"
  ]
  node [
    id 797
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 798
    label "beatbox"
  ]
  node [
    id 799
    label "komponowa&#263;"
  ]
  node [
    id 800
    label "komponowanie"
  ]
  node [
    id 801
    label "pasa&#380;"
  ]
  node [
    id 802
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 803
    label "notacja_muzyczna"
  ]
  node [
    id 804
    label "kontrapunkt"
  ]
  node [
    id 805
    label "sztuka"
  ]
  node [
    id 806
    label "instrumentalistyka"
  ]
  node [
    id 807
    label "harmonia"
  ]
  node [
    id 808
    label "wys&#322;uchanie"
  ]
  node [
    id 809
    label "kapela"
  ]
  node [
    id 810
    label "britpop"
  ]
  node [
    id 811
    label "badanie"
  ]
  node [
    id 812
    label "obserwowanie"
  ]
  node [
    id 813
    label "wy&#347;wiadczenie"
  ]
  node [
    id 814
    label "assay"
  ]
  node [
    id 815
    label "checkup"
  ]
  node [
    id 816
    label "do&#347;wiadczanie"
  ]
  node [
    id 817
    label "zbadanie"
  ]
  node [
    id 818
    label "potraktowanie"
  ]
  node [
    id 819
    label "poczucie"
  ]
  node [
    id 820
    label "proporcja"
  ]
  node [
    id 821
    label "wykszta&#322;cenie"
  ]
  node [
    id 822
    label "urszulanki_unii_rzymskiej"
  ]
  node [
    id 823
    label "Zgromadzenie_Si&#243;str_Urszulanek_Maryi_Panny_Niepokalanej_z_Gandino"
  ]
  node [
    id 824
    label "&#380;e&#324;skie_zgromadzenie_zakonne"
  ]
  node [
    id 825
    label "urszulanki_szare"
  ]
  node [
    id 826
    label "cognition"
  ]
  node [
    id 827
    label "intelekt"
  ]
  node [
    id 828
    label "pozwolenie"
  ]
  node [
    id 829
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 830
    label "zaawansowanie"
  ]
  node [
    id 831
    label "przekaza&#263;"
  ]
  node [
    id 832
    label "supply"
  ]
  node [
    id 833
    label "zaleci&#263;"
  ]
  node [
    id 834
    label "rewrite"
  ]
  node [
    id 835
    label "zrzec_si&#281;"
  ]
  node [
    id 836
    label "testament"
  ]
  node [
    id 837
    label "skopiowa&#263;"
  ]
  node [
    id 838
    label "zadanie"
  ]
  node [
    id 839
    label "lekarstwo"
  ]
  node [
    id 840
    label "przenie&#347;&#263;"
  ]
  node [
    id 841
    label "ucze&#324;"
  ]
  node [
    id 842
    label "student"
  ]
  node [
    id 843
    label "zaliczy&#263;"
  ]
  node [
    id 844
    label "powierzy&#263;"
  ]
  node [
    id 845
    label "zmusi&#263;"
  ]
  node [
    id 846
    label "translate"
  ]
  node [
    id 847
    label "give"
  ]
  node [
    id 848
    label "przedstawi&#263;"
  ]
  node [
    id 849
    label "z&#322;o&#380;y&#263;_egzamin"
  ]
  node [
    id 850
    label "convey"
  ]
  node [
    id 851
    label "przekazanie"
  ]
  node [
    id 852
    label "skopiowanie"
  ]
  node [
    id 853
    label "arrangement"
  ]
  node [
    id 854
    label "przeniesienie"
  ]
  node [
    id 855
    label "answer"
  ]
  node [
    id 856
    label "zrzekni&#281;cie_si&#281;"
  ]
  node [
    id 857
    label "transcription"
  ]
  node [
    id 858
    label "zalecenie"
  ]
  node [
    id 859
    label "fraza"
  ]
  node [
    id 860
    label "stanowisko"
  ]
  node [
    id 861
    label "wypowiedzenie"
  ]
  node [
    id 862
    label "prison_term"
  ]
  node [
    id 863
    label "przedstawienie"
  ]
  node [
    id 864
    label "wyra&#380;enie"
  ]
  node [
    id 865
    label "zaliczenie"
  ]
  node [
    id 866
    label "antylogizm"
  ]
  node [
    id 867
    label "zmuszenie"
  ]
  node [
    id 868
    label "konektyw"
  ]
  node [
    id 869
    label "attitude"
  ]
  node [
    id 870
    label "powierzenie"
  ]
  node [
    id 871
    label "adjudication"
  ]
  node [
    id 872
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 873
    label "pass"
  ]
  node [
    id 874
    label "political_orientation"
  ]
  node [
    id 875
    label "idea"
  ]
  node [
    id 876
    label "stra&#380;nik"
  ]
  node [
    id 877
    label "przedszkole"
  ]
  node [
    id 878
    label "opiekun"
  ]
  node [
    id 879
    label "ruch"
  ]
  node [
    id 880
    label "rozmiar&#243;wka"
  ]
  node [
    id 881
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 882
    label "tarcza"
  ]
  node [
    id 883
    label "kosz"
  ]
  node [
    id 884
    label "transparent"
  ]
  node [
    id 885
    label "rubryka"
  ]
  node [
    id 886
    label "kontener"
  ]
  node [
    id 887
    label "spis"
  ]
  node [
    id 888
    label "plate"
  ]
  node [
    id 889
    label "konstrukcja"
  ]
  node [
    id 890
    label "szachownica_Punnetta"
  ]
  node [
    id 891
    label "chart"
  ]
  node [
    id 892
    label "izba"
  ]
  node [
    id 893
    label "biurko"
  ]
  node [
    id 894
    label "boks"
  ]
  node [
    id 895
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 896
    label "egzekutywa"
  ]
  node [
    id 897
    label "premier"
  ]
  node [
    id 898
    label "Londyn"
  ]
  node [
    id 899
    label "palestra"
  ]
  node [
    id 900
    label "pok&#243;j"
  ]
  node [
    id 901
    label "gabinet_cieni"
  ]
  node [
    id 902
    label "Konsulat"
  ]
  node [
    id 903
    label "wagon"
  ]
  node [
    id 904
    label "mecz_mistrzowski"
  ]
  node [
    id 905
    label "class"
  ]
  node [
    id 906
    label "&#322;awka"
  ]
  node [
    id 907
    label "wykrzyknik"
  ]
  node [
    id 908
    label "zaleta"
  ]
  node [
    id 909
    label "programowanie_obiektowe"
  ]
  node [
    id 910
    label "warstwa"
  ]
  node [
    id 911
    label "rezerwa"
  ]
  node [
    id 912
    label "Ekwici"
  ]
  node [
    id 913
    label "&#347;rodowisko"
  ]
  node [
    id 914
    label "sala"
  ]
  node [
    id 915
    label "pomoc"
  ]
  node [
    id 916
    label "znak_jako&#347;ci"
  ]
  node [
    id 917
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 918
    label "poziom"
  ]
  node [
    id 919
    label "promocja"
  ]
  node [
    id 920
    label "kurs"
  ]
  node [
    id 921
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 922
    label "dziennik_lekcyjny"
  ]
  node [
    id 923
    label "typ"
  ]
  node [
    id 924
    label "fakcja"
  ]
  node [
    id 925
    label "obrona"
  ]
  node [
    id 926
    label "atak"
  ]
  node [
    id 927
    label "botanika"
  ]
  node [
    id 928
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 929
    label "Wallenrod"
  ]
  node [
    id 930
    label "gorset"
  ]
  node [
    id 931
    label "zrzucenie"
  ]
  node [
    id 932
    label "znoszenie"
  ]
  node [
    id 933
    label "kr&#243;j"
  ]
  node [
    id 934
    label "ubranie_si&#281;"
  ]
  node [
    id 935
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 936
    label "znosi&#263;"
  ]
  node [
    id 937
    label "zrzuci&#263;"
  ]
  node [
    id 938
    label "pasmanteria"
  ]
  node [
    id 939
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 940
    label "odzie&#380;"
  ]
  node [
    id 941
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 942
    label "wyko&#324;czenie"
  ]
  node [
    id 943
    label "nosi&#263;"
  ]
  node [
    id 944
    label "zasada"
  ]
  node [
    id 945
    label "w&#322;o&#380;enie"
  ]
  node [
    id 946
    label "garderoba"
  ]
  node [
    id 947
    label "odziewek"
  ]
  node [
    id 948
    label "szatnia"
  ]
  node [
    id 949
    label "szafa_ubraniowa"
  ]
  node [
    id 950
    label "dom"
  ]
  node [
    id 951
    label "rozmiar"
  ]
  node [
    id 952
    label "otulisko"
  ]
  node [
    id 953
    label "moda"
  ]
  node [
    id 954
    label "modniarka"
  ]
  node [
    id 955
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 956
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 957
    label "regu&#322;a_Allena"
  ]
  node [
    id 958
    label "base"
  ]
  node [
    id 959
    label "umowa"
  ]
  node [
    id 960
    label "obserwacja"
  ]
  node [
    id 961
    label "zasada_d'Alemberta"
  ]
  node [
    id 962
    label "normalizacja"
  ]
  node [
    id 963
    label "moralno&#347;&#263;"
  ]
  node [
    id 964
    label "criterion"
  ]
  node [
    id 965
    label "opis"
  ]
  node [
    id 966
    label "regu&#322;a_Glogera"
  ]
  node [
    id 967
    label "prawo_Mendla"
  ]
  node [
    id 968
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 969
    label "twierdzenie"
  ]
  node [
    id 970
    label "prawo"
  ]
  node [
    id 971
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 972
    label "qualification"
  ]
  node [
    id 973
    label "dominion"
  ]
  node [
    id 974
    label "occupation"
  ]
  node [
    id 975
    label "prawid&#322;o"
  ]
  node [
    id 976
    label "mechanika"
  ]
  node [
    id 977
    label "kamizelka"
  ]
  node [
    id 978
    label "opatrunek"
  ]
  node [
    id 979
    label "gotka"
  ]
  node [
    id 980
    label "bielizna"
  ]
  node [
    id 981
    label "przyrz&#261;d"
  ]
  node [
    id 982
    label "brykla"
  ]
  node [
    id 983
    label "back_brace"
  ]
  node [
    id 984
    label "przedmiot_ortopedyczny"
  ]
  node [
    id 985
    label "girdle"
  ]
  node [
    id 986
    label "zu&#380;ycie"
  ]
  node [
    id 987
    label "skonany"
  ]
  node [
    id 988
    label "zniszczenie"
  ]
  node [
    id 989
    label "os&#322;abienie"
  ]
  node [
    id 990
    label "wymordowanie"
  ]
  node [
    id 991
    label "murder"
  ]
  node [
    id 992
    label "pomordowanie"
  ]
  node [
    id 993
    label "znu&#380;enie"
  ]
  node [
    id 994
    label "ukszta&#322;towanie"
  ]
  node [
    id 995
    label "zm&#281;czenie"
  ]
  node [
    id 996
    label "adjustment"
  ]
  node [
    id 997
    label "zabicie"
  ]
  node [
    id 998
    label "haberdashery"
  ]
  node [
    id 999
    label "sklep"
  ]
  node [
    id 1000
    label "ozdoba"
  ]
  node [
    id 1001
    label "szmuklerz"
  ]
  node [
    id 1002
    label "pismo"
  ]
  node [
    id 1003
    label "glif"
  ]
  node [
    id 1004
    label "zdobnik"
  ]
  node [
    id 1005
    label "character"
  ]
  node [
    id 1006
    label "design"
  ]
  node [
    id 1007
    label "przemieszczenie"
  ]
  node [
    id 1008
    label "zgromadzenie"
  ]
  node [
    id 1009
    label "odprowadzenie"
  ]
  node [
    id 1010
    label "zdj&#281;cie"
  ]
  node [
    id 1011
    label "pozwalanie"
  ]
  node [
    id 1012
    label "invest"
  ]
  node [
    id 1013
    label "load"
  ]
  node [
    id 1014
    label "ubra&#263;"
  ]
  node [
    id 1015
    label "oblec_si&#281;"
  ]
  node [
    id 1016
    label "oblec"
  ]
  node [
    id 1017
    label "podwin&#261;&#263;"
  ]
  node [
    id 1018
    label "przewidzie&#263;"
  ]
  node [
    id 1019
    label "przyodzia&#263;"
  ]
  node [
    id 1020
    label "jell"
  ]
  node [
    id 1021
    label "insert"
  ]
  node [
    id 1022
    label "utworzy&#263;"
  ]
  node [
    id 1023
    label "zap&#322;aci&#263;"
  ]
  node [
    id 1024
    label "create"
  ]
  node [
    id 1025
    label "install"
  ]
  node [
    id 1026
    label "map"
  ]
  node [
    id 1027
    label "gromadzi&#263;"
  ]
  node [
    id 1028
    label "usuwa&#263;"
  ]
  node [
    id 1029
    label "porywa&#263;"
  ]
  node [
    id 1030
    label "sk&#322;ada&#263;"
  ]
  node [
    id 1031
    label "ranny"
  ]
  node [
    id 1032
    label "zbiera&#263;"
  ]
  node [
    id 1033
    label "behave"
  ]
  node [
    id 1034
    label "represent"
  ]
  node [
    id 1035
    label "podrze&#263;"
  ]
  node [
    id 1036
    label "przenosi&#263;"
  ]
  node [
    id 1037
    label "wytrzymywa&#263;"
  ]
  node [
    id 1038
    label "seclude"
  ]
  node [
    id 1039
    label "wygrywa&#263;"
  ]
  node [
    id 1040
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 1041
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 1042
    label "zu&#380;y&#263;"
  ]
  node [
    id 1043
    label "niszczy&#263;"
  ]
  node [
    id 1044
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 1045
    label "tolerowa&#263;"
  ]
  node [
    id 1046
    label "obleczenie_si&#281;"
  ]
  node [
    id 1047
    label "poubieranie"
  ]
  node [
    id 1048
    label "przyodzianie"
  ]
  node [
    id 1049
    label "powk&#322;adanie"
  ]
  node [
    id 1050
    label "infliction"
  ]
  node [
    id 1051
    label "obleczenie"
  ]
  node [
    id 1052
    label "przebranie"
  ]
  node [
    id 1053
    label "umieszczenie"
  ]
  node [
    id 1054
    label "przywdzianie"
  ]
  node [
    id 1055
    label "deposit"
  ]
  node [
    id 1056
    label "rozebranie"
  ]
  node [
    id 1057
    label "przymierzenie"
  ]
  node [
    id 1058
    label "spill"
  ]
  node [
    id 1059
    label "zgromadzi&#263;"
  ]
  node [
    id 1060
    label "drop"
  ]
  node [
    id 1061
    label "odprowadzi&#263;"
  ]
  node [
    id 1062
    label "zdj&#261;&#263;"
  ]
  node [
    id 1063
    label "toleration"
  ]
  node [
    id 1064
    label "collection"
  ]
  node [
    id 1065
    label "wytrzymywanie"
  ]
  node [
    id 1066
    label "take"
  ]
  node [
    id 1067
    label "jajko"
  ]
  node [
    id 1068
    label "usuwanie"
  ]
  node [
    id 1069
    label "porywanie"
  ]
  node [
    id 1070
    label "wygrywanie"
  ]
  node [
    id 1071
    label "abrogation"
  ]
  node [
    id 1072
    label "gromadzenie"
  ]
  node [
    id 1073
    label "przenoszenie"
  ]
  node [
    id 1074
    label "poddawanie_si&#281;"
  ]
  node [
    id 1075
    label "uniewa&#380;nianie"
  ]
  node [
    id 1076
    label "rodzenie"
  ]
  node [
    id 1077
    label "tolerowanie"
  ]
  node [
    id 1078
    label "niszczenie"
  ]
  node [
    id 1079
    label "stand"
  ]
  node [
    id 1080
    label "posiada&#263;"
  ]
  node [
    id 1081
    label "mie&#263;"
  ]
  node [
    id 1082
    label "przemieszcza&#263;"
  ]
  node [
    id 1083
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1084
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 1085
    label "zaczynanie_si&#281;"
  ]
  node [
    id 1086
    label "wynikanie"
  ]
  node [
    id 1087
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 1088
    label "origin"
  ]
  node [
    id 1089
    label "background"
  ]
  node [
    id 1090
    label "geneza"
  ]
  node [
    id 1091
    label "date"
  ]
  node [
    id 1092
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 1093
    label "wynika&#263;"
  ]
  node [
    id 1094
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1095
    label "fall"
  ]
  node [
    id 1096
    label "poby&#263;"
  ]
  node [
    id 1097
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 1098
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1099
    label "bolt"
  ]
  node [
    id 1100
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 1101
    label "uda&#263;_si&#281;"
  ]
  node [
    id 1102
    label "wpoi&#263;"
  ]
  node [
    id 1103
    label "pour"
  ]
  node [
    id 1104
    label "natchn&#261;&#263;"
  ]
  node [
    id 1105
    label "wzbudzi&#263;"
  ]
  node [
    id 1106
    label "aalen"
  ]
  node [
    id 1107
    label "jura_wczesna"
  ]
  node [
    id 1108
    label "holocen"
  ]
  node [
    id 1109
    label "pliocen"
  ]
  node [
    id 1110
    label "plejstocen"
  ]
  node [
    id 1111
    label "paleocen"
  ]
  node [
    id 1112
    label "bajos"
  ]
  node [
    id 1113
    label "kelowej"
  ]
  node [
    id 1114
    label "eocen"
  ]
  node [
    id 1115
    label "miocen"
  ]
  node [
    id 1116
    label "&#347;rodkowy_trias"
  ]
  node [
    id 1117
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 1118
    label "wczesny_trias"
  ]
  node [
    id 1119
    label "jura_&#347;rodkowa"
  ]
  node [
    id 1120
    label "oligocen"
  ]
  node [
    id 1121
    label "gelas"
  ]
  node [
    id 1122
    label "kalabr"
  ]
  node [
    id 1123
    label "megaterium"
  ]
  node [
    id 1124
    label "lutet"
  ]
  node [
    id 1125
    label "barton"
  ]
  node [
    id 1126
    label "iprez"
  ]
  node [
    id 1127
    label "priabon"
  ]
  node [
    id 1128
    label "aluwium"
  ]
  node [
    id 1129
    label "tanet"
  ]
  node [
    id 1130
    label "dan"
  ]
  node [
    id 1131
    label "zeland"
  ]
  node [
    id 1132
    label "szat"
  ]
  node [
    id 1133
    label "rupel"
  ]
  node [
    id 1134
    label "messyn"
  ]
  node [
    id 1135
    label "serrawal"
  ]
  node [
    id 1136
    label "torton"
  ]
  node [
    id 1137
    label "akwitan"
  ]
  node [
    id 1138
    label "lang"
  ]
  node [
    id 1139
    label "burdyga&#322;"
  ]
  node [
    id 1140
    label "zankl"
  ]
  node [
    id 1141
    label "piacent"
  ]
  node [
    id 1142
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 1143
    label "kres"
  ]
  node [
    id 1144
    label "charakter"
  ]
  node [
    id 1145
    label "rynek"
  ]
  node [
    id 1146
    label "doprowadzi&#263;"
  ]
  node [
    id 1147
    label "testify"
  ]
  node [
    id 1148
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 1149
    label "wpisa&#263;"
  ]
  node [
    id 1150
    label "zapozna&#263;"
  ]
  node [
    id 1151
    label "wej&#347;&#263;"
  ]
  node [
    id 1152
    label "zej&#347;&#263;"
  ]
  node [
    id 1153
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 1154
    label "indicate"
  ]
  node [
    id 1155
    label "post&#261;pi&#263;"
  ]
  node [
    id 1156
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 1157
    label "odj&#261;&#263;"
  ]
  node [
    id 1158
    label "cause"
  ]
  node [
    id 1159
    label "introduce"
  ]
  node [
    id 1160
    label "begin"
  ]
  node [
    id 1161
    label "do"
  ]
  node [
    id 1162
    label "permit"
  ]
  node [
    id 1163
    label "obznajomi&#263;"
  ]
  node [
    id 1164
    label "zawrze&#263;"
  ]
  node [
    id 1165
    label "pozna&#263;"
  ]
  node [
    id 1166
    label "poinformowa&#263;"
  ]
  node [
    id 1167
    label "teach"
  ]
  node [
    id 1168
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1169
    label "move"
  ]
  node [
    id 1170
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1171
    label "zaistnie&#263;"
  ]
  node [
    id 1172
    label "z&#322;oi&#263;"
  ]
  node [
    id 1173
    label "drapn&#261;&#263;_si&#281;"
  ]
  node [
    id 1174
    label "przekroczy&#263;"
  ]
  node [
    id 1175
    label "nast&#261;pi&#263;"
  ]
  node [
    id 1176
    label "zag&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 1177
    label "w&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 1178
    label "catch"
  ]
  node [
    id 1179
    label "intervene"
  ]
  node [
    id 1180
    label "get"
  ]
  node [
    id 1181
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 1182
    label "wnikn&#261;&#263;"
  ]
  node [
    id 1183
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 1184
    label "przenikn&#261;&#263;"
  ]
  node [
    id 1185
    label "doj&#347;&#263;"
  ]
  node [
    id 1186
    label "wzi&#261;&#263;"
  ]
  node [
    id 1187
    label "spotka&#263;"
  ]
  node [
    id 1188
    label "submit"
  ]
  node [
    id 1189
    label "become"
  ]
  node [
    id 1190
    label "act"
  ]
  node [
    id 1191
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 1192
    label "napisa&#263;"
  ]
  node [
    id 1193
    label "write"
  ]
  node [
    id 1194
    label "pos&#322;a&#263;"
  ]
  node [
    id 1195
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 1196
    label "poprowadzi&#263;"
  ]
  node [
    id 1197
    label "put"
  ]
  node [
    id 1198
    label "uplasowa&#263;"
  ]
  node [
    id 1199
    label "wpierniczy&#263;"
  ]
  node [
    id 1200
    label "okre&#347;li&#263;"
  ]
  node [
    id 1201
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 1202
    label "umieszcza&#263;"
  ]
  node [
    id 1203
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 1204
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 1205
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 1206
    label "zorganizowa&#263;"
  ]
  node [
    id 1207
    label "appoint"
  ]
  node [
    id 1208
    label "wystylizowa&#263;"
  ]
  node [
    id 1209
    label "przerobi&#263;"
  ]
  node [
    id 1210
    label "nabra&#263;"
  ]
  node [
    id 1211
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 1212
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 1213
    label "wydali&#263;"
  ]
  node [
    id 1214
    label "trouble"
  ]
  node [
    id 1215
    label "naruszy&#263;"
  ]
  node [
    id 1216
    label "temat"
  ]
  node [
    id 1217
    label "uby&#263;"
  ]
  node [
    id 1218
    label "umrze&#263;"
  ]
  node [
    id 1219
    label "za&#347;piewa&#263;"
  ]
  node [
    id 1220
    label "obni&#380;y&#263;"
  ]
  node [
    id 1221
    label "przenie&#347;&#263;_si&#281;"
  ]
  node [
    id 1222
    label "distract"
  ]
  node [
    id 1223
    label "zu&#380;y&#263;_si&#281;"
  ]
  node [
    id 1224
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 1225
    label "obni&#380;y&#263;_si&#281;"
  ]
  node [
    id 1226
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 1227
    label "odpa&#347;&#263;"
  ]
  node [
    id 1228
    label "die"
  ]
  node [
    id 1229
    label "zboczy&#263;"
  ]
  node [
    id 1230
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 1231
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 1232
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 1233
    label "odej&#347;&#263;"
  ]
  node [
    id 1234
    label "zgin&#261;&#263;"
  ]
  node [
    id 1235
    label "write_down"
  ]
  node [
    id 1236
    label "stoisko"
  ]
  node [
    id 1237
    label "rynek_podstawowy"
  ]
  node [
    id 1238
    label "rynek_r&#243;wnoleg&#322;y"
  ]
  node [
    id 1239
    label "konsument"
  ]
  node [
    id 1240
    label "pojawienie_si&#281;"
  ]
  node [
    id 1241
    label "obiekt_handlowy"
  ]
  node [
    id 1242
    label "wska&#378;nik_rynkowy"
  ]
  node [
    id 1243
    label "wytw&#243;rca"
  ]
  node [
    id 1244
    label "rynek_wt&#243;rny"
  ]
  node [
    id 1245
    label "wprowadzanie"
  ]
  node [
    id 1246
    label "wprowadza&#263;"
  ]
  node [
    id 1247
    label "kram"
  ]
  node [
    id 1248
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 1249
    label "emitowa&#263;"
  ]
  node [
    id 1250
    label "emitowanie"
  ]
  node [
    id 1251
    label "gospodarka"
  ]
  node [
    id 1252
    label "biznes"
  ]
  node [
    id 1253
    label "segment_rynku"
  ]
  node [
    id 1254
    label "wprowadzenie"
  ]
  node [
    id 1255
    label "targowica"
  ]
  node [
    id 1256
    label "formation"
  ]
  node [
    id 1257
    label "sprzeciwianie_si&#281;"
  ]
  node [
    id 1258
    label "rebellion"
  ]
  node [
    id 1259
    label "coevals"
  ]
  node [
    id 1260
    label "initiation"
  ]
  node [
    id 1261
    label "buntowanie_si&#281;"
  ]
  node [
    id 1262
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 1263
    label "unoszenie_si&#281;"
  ]
  node [
    id 1264
    label "rozdzia&#322;"
  ]
  node [
    id 1265
    label "wk&#322;ad"
  ]
  node [
    id 1266
    label "tytu&#322;"
  ]
  node [
    id 1267
    label "zak&#322;adka"
  ]
  node [
    id 1268
    label "nomina&#322;"
  ]
  node [
    id 1269
    label "ok&#322;adka"
  ]
  node [
    id 1270
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 1271
    label "wydawnictwo"
  ]
  node [
    id 1272
    label "ekslibris"
  ]
  node [
    id 1273
    label "przek&#322;adacz"
  ]
  node [
    id 1274
    label "bibliofilstwo"
  ]
  node [
    id 1275
    label "falc"
  ]
  node [
    id 1276
    label "pagina"
  ]
  node [
    id 1277
    label "zw&#243;j"
  ]
  node [
    id 1278
    label "j&#281;zykowo"
  ]
  node [
    id 1279
    label "wypowied&#378;"
  ]
  node [
    id 1280
    label "redakcja"
  ]
  node [
    id 1281
    label "pomini&#281;cie"
  ]
  node [
    id 1282
    label "dzie&#322;o"
  ]
  node [
    id 1283
    label "preparacja"
  ]
  node [
    id 1284
    label "odmianka"
  ]
  node [
    id 1285
    label "koniektura"
  ]
  node [
    id 1286
    label "obelga"
  ]
  node [
    id 1287
    label "okaz"
  ]
  node [
    id 1288
    label "part"
  ]
  node [
    id 1289
    label "agent"
  ]
  node [
    id 1290
    label "nicpo&#324;"
  ]
  node [
    id 1291
    label "debit"
  ]
  node [
    id 1292
    label "redaktor"
  ]
  node [
    id 1293
    label "druk"
  ]
  node [
    id 1294
    label "publikacja"
  ]
  node [
    id 1295
    label "szata_graficzna"
  ]
  node [
    id 1296
    label "wydawa&#263;"
  ]
  node [
    id 1297
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 1298
    label "wyda&#263;"
  ]
  node [
    id 1299
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 1300
    label "poster"
  ]
  node [
    id 1301
    label "interruption"
  ]
  node [
    id 1302
    label "podzia&#322;"
  ]
  node [
    id 1303
    label "podrozdzia&#322;"
  ]
  node [
    id 1304
    label "nadtytu&#322;"
  ]
  node [
    id 1305
    label "tytulatura"
  ]
  node [
    id 1306
    label "elevation"
  ]
  node [
    id 1307
    label "mianowaniec"
  ]
  node [
    id 1308
    label "podtytu&#322;"
  ]
  node [
    id 1309
    label "blacha"
  ]
  node [
    id 1310
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1311
    label "kartka"
  ]
  node [
    id 1312
    label "uczestnictwo"
  ]
  node [
    id 1313
    label "element"
  ]
  node [
    id 1314
    label "input"
  ]
  node [
    id 1315
    label "czasopismo"
  ]
  node [
    id 1316
    label "lokata"
  ]
  node [
    id 1317
    label "zeszyt"
  ]
  node [
    id 1318
    label "pagination"
  ]
  node [
    id 1319
    label "strona"
  ]
  node [
    id 1320
    label "numer"
  ]
  node [
    id 1321
    label "oprawa"
  ]
  node [
    id 1322
    label "boarding"
  ]
  node [
    id 1323
    label "oprawianie"
  ]
  node [
    id 1324
    label "os&#322;ona"
  ]
  node [
    id 1325
    label "oprawia&#263;"
  ]
  node [
    id 1326
    label "t&#322;umacz"
  ]
  node [
    id 1327
    label "kszta&#322;t"
  ]
  node [
    id 1328
    label "wrench"
  ]
  node [
    id 1329
    label "m&#243;zg"
  ]
  node [
    id 1330
    label "kink"
  ]
  node [
    id 1331
    label "manuskrypt"
  ]
  node [
    id 1332
    label "rolka"
  ]
  node [
    id 1333
    label "bookmark"
  ]
  node [
    id 1334
    label "fa&#322;da"
  ]
  node [
    id 1335
    label "znacznik"
  ]
  node [
    id 1336
    label "sp&#243;&#322;g&#322;oska"
  ]
  node [
    id 1337
    label "widok"
  ]
  node [
    id 1338
    label "program"
  ]
  node [
    id 1339
    label "warto&#347;&#263;"
  ]
  node [
    id 1340
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 1341
    label "pieni&#261;dz"
  ]
  node [
    id 1342
    label "par_value"
  ]
  node [
    id 1343
    label "cena"
  ]
  node [
    id 1344
    label "znaczek"
  ]
  node [
    id 1345
    label "kolekcjonerstwo"
  ]
  node [
    id 1346
    label "bibliomania"
  ]
  node [
    id 1347
    label "oznaczenie"
  ]
  node [
    id 1348
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1349
    label "mie&#263;_miejsce"
  ]
  node [
    id 1350
    label "equal"
  ]
  node [
    id 1351
    label "trwa&#263;"
  ]
  node [
    id 1352
    label "chodzi&#263;"
  ]
  node [
    id 1353
    label "si&#281;ga&#263;"
  ]
  node [
    id 1354
    label "obecno&#347;&#263;"
  ]
  node [
    id 1355
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1356
    label "uczestniczy&#263;"
  ]
  node [
    id 1357
    label "participate"
  ]
  node [
    id 1358
    label "robi&#263;"
  ]
  node [
    id 1359
    label "istnie&#263;"
  ]
  node [
    id 1360
    label "pozostawa&#263;"
  ]
  node [
    id 1361
    label "zostawa&#263;"
  ]
  node [
    id 1362
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1363
    label "adhere"
  ]
  node [
    id 1364
    label "compass"
  ]
  node [
    id 1365
    label "korzysta&#263;"
  ]
  node [
    id 1366
    label "appreciation"
  ]
  node [
    id 1367
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1368
    label "dociera&#263;"
  ]
  node [
    id 1369
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1370
    label "mierzy&#263;"
  ]
  node [
    id 1371
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1372
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1373
    label "exsert"
  ]
  node [
    id 1374
    label "being"
  ]
  node [
    id 1375
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1376
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1377
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1378
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1379
    label "run"
  ]
  node [
    id 1380
    label "bangla&#263;"
  ]
  node [
    id 1381
    label "przebiega&#263;"
  ]
  node [
    id 1382
    label "proceed"
  ]
  node [
    id 1383
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1384
    label "bywa&#263;"
  ]
  node [
    id 1385
    label "dziama&#263;"
  ]
  node [
    id 1386
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1387
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1388
    label "para"
  ]
  node [
    id 1389
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1390
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1391
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1392
    label "krok"
  ]
  node [
    id 1393
    label "tryb"
  ]
  node [
    id 1394
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1395
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1396
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1397
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1398
    label "continue"
  ]
  node [
    id 1399
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1400
    label "Ohio"
  ]
  node [
    id 1401
    label "wci&#281;cie"
  ]
  node [
    id 1402
    label "Nowy_York"
  ]
  node [
    id 1403
    label "samopoczucie"
  ]
  node [
    id 1404
    label "Illinois"
  ]
  node [
    id 1405
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1406
    label "state"
  ]
  node [
    id 1407
    label "Jukatan"
  ]
  node [
    id 1408
    label "Kalifornia"
  ]
  node [
    id 1409
    label "Wirginia"
  ]
  node [
    id 1410
    label "wektor"
  ]
  node [
    id 1411
    label "Teksas"
  ]
  node [
    id 1412
    label "Goa"
  ]
  node [
    id 1413
    label "Waszyngton"
  ]
  node [
    id 1414
    label "Massachusetts"
  ]
  node [
    id 1415
    label "Alaska"
  ]
  node [
    id 1416
    label "Arakan"
  ]
  node [
    id 1417
    label "Hawaje"
  ]
  node [
    id 1418
    label "Maryland"
  ]
  node [
    id 1419
    label "punkt"
  ]
  node [
    id 1420
    label "Michigan"
  ]
  node [
    id 1421
    label "Arizona"
  ]
  node [
    id 1422
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1423
    label "Georgia"
  ]
  node [
    id 1424
    label "Pensylwania"
  ]
  node [
    id 1425
    label "shape"
  ]
  node [
    id 1426
    label "Luizjana"
  ]
  node [
    id 1427
    label "Nowy_Meksyk"
  ]
  node [
    id 1428
    label "Alabama"
  ]
  node [
    id 1429
    label "Kansas"
  ]
  node [
    id 1430
    label "Oregon"
  ]
  node [
    id 1431
    label "Floryda"
  ]
  node [
    id 1432
    label "Oklahoma"
  ]
  node [
    id 1433
    label "jednostka_administracyjna"
  ]
  node [
    id 1434
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1435
    label "autonomiczny"
  ]
  node [
    id 1436
    label "samodzielny"
  ]
  node [
    id 1437
    label "osobno"
  ]
  node [
    id 1438
    label "niepodlegle"
  ]
  node [
    id 1439
    label "niepodleg&#322;y"
  ]
  node [
    id 1440
    label "autonomicznie"
  ]
  node [
    id 1441
    label "wolno"
  ]
  node [
    id 1442
    label "individually"
  ]
  node [
    id 1443
    label "odr&#281;bny"
  ]
  node [
    id 1444
    label "udzielnie"
  ]
  node [
    id 1445
    label "osobnie"
  ]
  node [
    id 1446
    label "odr&#281;bnie"
  ]
  node [
    id 1447
    label "osobny"
  ]
  node [
    id 1448
    label "autonomizowanie_si&#281;"
  ]
  node [
    id 1449
    label "niezale&#380;ny"
  ]
  node [
    id 1450
    label "zautonomizowanie_si&#281;"
  ]
  node [
    id 1451
    label "w&#322;asny"
  ]
  node [
    id 1452
    label "sw&#243;j"
  ]
  node [
    id 1453
    label "sobieradzki"
  ]
  node [
    id 1454
    label "czyj&#347;"
  ]
  node [
    id 1455
    label "indywidualny"
  ]
  node [
    id 1456
    label "zabarwi&#263;"
  ]
  node [
    id 1457
    label "dye"
  ]
  node [
    id 1458
    label "uatrakcyjni&#263;"
  ]
  node [
    id 1459
    label "nada&#263;"
  ]
  node [
    id 1460
    label "da&#263;"
  ]
  node [
    id 1461
    label "stain"
  ]
  node [
    id 1462
    label "blot"
  ]
  node [
    id 1463
    label "okrasi&#263;"
  ]
  node [
    id 1464
    label "narz&#281;dzie"
  ]
  node [
    id 1465
    label "nature"
  ]
  node [
    id 1466
    label "strategia"
  ]
  node [
    id 1467
    label "teoria"
  ]
  node [
    id 1468
    label "doctrine"
  ]
  node [
    id 1469
    label "futon"
  ]
  node [
    id 1470
    label "karate"
  ]
  node [
    id 1471
    label "japo&#324;sko"
  ]
  node [
    id 1472
    label "sh&#333;gi"
  ]
  node [
    id 1473
    label "ju-jitsu"
  ]
  node [
    id 1474
    label "hanafuda"
  ]
  node [
    id 1475
    label "ky&#363;d&#333;"
  ]
  node [
    id 1476
    label "j&#281;zyk_izolowany"
  ]
  node [
    id 1477
    label "Japanese"
  ]
  node [
    id 1478
    label "katsudon"
  ]
  node [
    id 1479
    label "azjatycki"
  ]
  node [
    id 1480
    label "po_japo&#324;sku"
  ]
  node [
    id 1481
    label "ikebana"
  ]
  node [
    id 1482
    label "j&#281;zyk"
  ]
  node [
    id 1483
    label "dalekowschodni"
  ]
  node [
    id 1484
    label "po_dalekowschodniemu"
  ]
  node [
    id 1485
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 1486
    label "artykulator"
  ]
  node [
    id 1487
    label "kod"
  ]
  node [
    id 1488
    label "kawa&#322;ek"
  ]
  node [
    id 1489
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 1490
    label "gramatyka"
  ]
  node [
    id 1491
    label "stylik"
  ]
  node [
    id 1492
    label "przet&#322;umaczenie"
  ]
  node [
    id 1493
    label "formalizowanie"
  ]
  node [
    id 1494
    label "ssa&#263;"
  ]
  node [
    id 1495
    label "ssanie"
  ]
  node [
    id 1496
    label "language"
  ]
  node [
    id 1497
    label "liza&#263;"
  ]
  node [
    id 1498
    label "konsonantyzm"
  ]
  node [
    id 1499
    label "wokalizm"
  ]
  node [
    id 1500
    label "fonetyka"
  ]
  node [
    id 1501
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 1502
    label "jeniec"
  ]
  node [
    id 1503
    label "but"
  ]
  node [
    id 1504
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 1505
    label "po_koroniarsku"
  ]
  node [
    id 1506
    label "t&#322;umaczenie"
  ]
  node [
    id 1507
    label "m&#243;wienie"
  ]
  node [
    id 1508
    label "pype&#263;"
  ]
  node [
    id 1509
    label "lizanie"
  ]
  node [
    id 1510
    label "formalizowa&#263;"
  ]
  node [
    id 1511
    label "rozumie&#263;"
  ]
  node [
    id 1512
    label "organ"
  ]
  node [
    id 1513
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 1514
    label "rozumienie"
  ]
  node [
    id 1515
    label "makroglosja"
  ]
  node [
    id 1516
    label "m&#243;wi&#263;"
  ]
  node [
    id 1517
    label "jama_ustna"
  ]
  node [
    id 1518
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 1519
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 1520
    label "natural_language"
  ]
  node [
    id 1521
    label "s&#322;ownictwo"
  ]
  node [
    id 1522
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1523
    label "kampong"
  ]
  node [
    id 1524
    label "typowy"
  ]
  node [
    id 1525
    label "ghaty"
  ]
  node [
    id 1526
    label "charakterystyczny"
  ]
  node [
    id 1527
    label "balut"
  ]
  node [
    id 1528
    label "ka&#322;mucki"
  ]
  node [
    id 1529
    label "azjatycko"
  ]
  node [
    id 1530
    label "naczynie"
  ]
  node [
    id 1531
    label "karcianka"
  ]
  node [
    id 1532
    label "gra_w_karty"
  ]
  node [
    id 1533
    label "sensei"
  ]
  node [
    id 1534
    label "sztuka_walki"
  ]
  node [
    id 1535
    label "materac"
  ]
  node [
    id 1536
    label "potrawa"
  ]
  node [
    id 1537
    label "kotlet"
  ]
  node [
    id 1538
    label "szachy"
  ]
  node [
    id 1539
    label "&#322;ucznictwo"
  ]
  node [
    id 1540
    label "samuraj"
  ]
  node [
    id 1541
    label "sport_walki"
  ]
  node [
    id 1542
    label "formu&#322;owa&#263;"
  ]
  node [
    id 1543
    label "ozdabia&#263;"
  ]
  node [
    id 1544
    label "stawia&#263;"
  ]
  node [
    id 1545
    label "styl"
  ]
  node [
    id 1546
    label "skryba"
  ]
  node [
    id 1547
    label "read"
  ]
  node [
    id 1548
    label "donosi&#263;"
  ]
  node [
    id 1549
    label "code"
  ]
  node [
    id 1550
    label "dysgrafia"
  ]
  node [
    id 1551
    label "dysortografia"
  ]
  node [
    id 1552
    label "tworzy&#263;"
  ]
  node [
    id 1553
    label "prasa"
  ]
  node [
    id 1554
    label "pope&#322;nia&#263;"
  ]
  node [
    id 1555
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1556
    label "wytwarza&#263;"
  ]
  node [
    id 1557
    label "consist"
  ]
  node [
    id 1558
    label "stanowi&#263;"
  ]
  node [
    id 1559
    label "spill_the_beans"
  ]
  node [
    id 1560
    label "przeby&#263;"
  ]
  node [
    id 1561
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 1562
    label "zanosi&#263;"
  ]
  node [
    id 1563
    label "inform"
  ]
  node [
    id 1564
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 1565
    label "render"
  ]
  node [
    id 1566
    label "ci&#261;&#380;a"
  ]
  node [
    id 1567
    label "informowa&#263;"
  ]
  node [
    id 1568
    label "komunikowa&#263;"
  ]
  node [
    id 1569
    label "pozostawia&#263;"
  ]
  node [
    id 1570
    label "czyni&#263;"
  ]
  node [
    id 1571
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 1572
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 1573
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 1574
    label "przewidywa&#263;"
  ]
  node [
    id 1575
    label "przyznawa&#263;"
  ]
  node [
    id 1576
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1577
    label "obstawia&#263;"
  ]
  node [
    id 1578
    label "ocenia&#263;"
  ]
  node [
    id 1579
    label "zastawia&#263;"
  ]
  node [
    id 1580
    label "wskazywa&#263;"
  ]
  node [
    id 1581
    label "uruchamia&#263;"
  ]
  node [
    id 1582
    label "fundowa&#263;"
  ]
  node [
    id 1583
    label "zmienia&#263;"
  ]
  node [
    id 1584
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 1585
    label "deliver"
  ]
  node [
    id 1586
    label "powodowa&#263;"
  ]
  node [
    id 1587
    label "wyznacza&#263;"
  ]
  node [
    id 1588
    label "przedstawia&#263;"
  ]
  node [
    id 1589
    label "wydobywa&#263;"
  ]
  node [
    id 1590
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 1591
    label "trim"
  ]
  node [
    id 1592
    label "gryzipi&#243;rek"
  ]
  node [
    id 1593
    label "pisarz"
  ]
  node [
    id 1594
    label "t&#322;oczysko"
  ]
  node [
    id 1595
    label "depesza"
  ]
  node [
    id 1596
    label "maszyna"
  ]
  node [
    id 1597
    label "media"
  ]
  node [
    id 1598
    label "dziennikarz_prasowy"
  ]
  node [
    id 1599
    label "kiosk"
  ]
  node [
    id 1600
    label "maszyna_rolnicza"
  ]
  node [
    id 1601
    label "gazeta"
  ]
  node [
    id 1602
    label "trzonek"
  ]
  node [
    id 1603
    label "dyscyplina_sportowa"
  ]
  node [
    id 1604
    label "handle"
  ]
  node [
    id 1605
    label "stroke"
  ]
  node [
    id 1606
    label "kanon"
  ]
  node [
    id 1607
    label "behawior"
  ]
  node [
    id 1608
    label "dysleksja"
  ]
  node [
    id 1609
    label "pisanie"
  ]
  node [
    id 1610
    label "dysgraphia"
  ]
  node [
    id 1611
    label "kaczkowate"
  ]
  node [
    id 1612
    label "blaszkodziobe"
  ]
  node [
    id 1613
    label "stal&#243;wka"
  ]
  node [
    id 1614
    label "wyrostek"
  ]
  node [
    id 1615
    label "stylo"
  ]
  node [
    id 1616
    label "przybory_do_pisania"
  ]
  node [
    id 1617
    label "obsadka"
  ]
  node [
    id 1618
    label "ptak"
  ]
  node [
    id 1619
    label "wypisanie"
  ]
  node [
    id 1620
    label "pir&#243;g"
  ]
  node [
    id 1621
    label "pierze"
  ]
  node [
    id 1622
    label "wypisa&#263;"
  ]
  node [
    id 1623
    label "pisarstwo"
  ]
  node [
    id 1624
    label "element_anatomiczny"
  ]
  node [
    id 1625
    label "autor"
  ]
  node [
    id 1626
    label "p&#322;askownik"
  ]
  node [
    id 1627
    label "dusza"
  ]
  node [
    id 1628
    label "upierzenie"
  ]
  node [
    id 1629
    label "atrament"
  ]
  node [
    id 1630
    label "magierka"
  ]
  node [
    id 1631
    label "quill"
  ]
  node [
    id 1632
    label "pi&#243;ropusz"
  ]
  node [
    id 1633
    label "stosina"
  ]
  node [
    id 1634
    label "wyst&#281;p"
  ]
  node [
    id 1635
    label "g&#322;ownia"
  ]
  node [
    id 1636
    label "resor_pi&#243;rowy"
  ]
  node [
    id 1637
    label "pen"
  ]
  node [
    id 1638
    label "literatura"
  ]
  node [
    id 1639
    label "kszta&#322;townik"
  ]
  node [
    id 1640
    label "punkt_McBurneya"
  ]
  node [
    id 1641
    label "narz&#261;d_limfoidalny"
  ]
  node [
    id 1642
    label "tw&#243;r"
  ]
  node [
    id 1643
    label "jelito_&#347;lepe"
  ]
  node [
    id 1644
    label "ch&#322;opiec"
  ]
  node [
    id 1645
    label "m&#322;okos"
  ]
  node [
    id 1646
    label "zako&#324;czenie"
  ]
  node [
    id 1647
    label "Rzym_Zachodni"
  ]
  node [
    id 1648
    label "Rzym_Wschodni"
  ]
  node [
    id 1649
    label "r&#243;&#380;niczka"
  ]
  node [
    id 1650
    label "materia"
  ]
  node [
    id 1651
    label "szambo"
  ]
  node [
    id 1652
    label "aspo&#322;eczny"
  ]
  node [
    id 1653
    label "component"
  ]
  node [
    id 1654
    label "szkodnik"
  ]
  node [
    id 1655
    label "gangsterski"
  ]
  node [
    id 1656
    label "underworld"
  ]
  node [
    id 1657
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 1658
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 1659
    label "kszta&#322;ciciel"
  ]
  node [
    id 1660
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 1661
    label "tworzyciel"
  ]
  node [
    id 1662
    label "wykonawca"
  ]
  node [
    id 1663
    label "pomys&#322;odawca"
  ]
  node [
    id 1664
    label "&#347;w"
  ]
  node [
    id 1665
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 1666
    label "performance"
  ]
  node [
    id 1667
    label "impreza"
  ]
  node [
    id 1668
    label "tingel-tangel"
  ]
  node [
    id 1669
    label "trema"
  ]
  node [
    id 1670
    label "odtworzenie"
  ]
  node [
    id 1671
    label "dziobni&#281;cie"
  ]
  node [
    id 1672
    label "wysiadywa&#263;"
  ]
  node [
    id 1673
    label "ko&#347;&#263;_potyliczna"
  ]
  node [
    id 1674
    label "dzioba&#263;"
  ]
  node [
    id 1675
    label "grzebie&#324;"
  ]
  node [
    id 1676
    label "ptaki"
  ]
  node [
    id 1677
    label "kuper"
  ]
  node [
    id 1678
    label "hukni&#281;cie"
  ]
  node [
    id 1679
    label "dziobn&#261;&#263;"
  ]
  node [
    id 1680
    label "ptasz&#281;"
  ]
  node [
    id 1681
    label "skrzyd&#322;o"
  ]
  node [
    id 1682
    label "kloaka"
  ]
  node [
    id 1683
    label "dzi&#243;bn&#261;&#263;"
  ]
  node [
    id 1684
    label "wysiedzie&#263;"
  ]
  node [
    id 1685
    label "bird"
  ]
  node [
    id 1686
    label "dziobanie"
  ]
  node [
    id 1687
    label "pogwizdywanie"
  ]
  node [
    id 1688
    label "dzi&#243;b"
  ]
  node [
    id 1689
    label "ptactwo"
  ]
  node [
    id 1690
    label "roz&#263;wierkanie_si&#281;"
  ]
  node [
    id 1691
    label "zaklekotanie"
  ]
  node [
    id 1692
    label "skok"
  ]
  node [
    id 1693
    label "owodniowiec"
  ]
  node [
    id 1694
    label "dzi&#243;bni&#281;cie"
  ]
  node [
    id 1695
    label "klapak"
  ]
  node [
    id 1696
    label "szlara"
  ]
  node [
    id 1697
    label "m&#322;&#243;dka"
  ]
  node [
    id 1698
    label "wype&#322;niacz"
  ]
  node [
    id 1699
    label "w&#322;osy"
  ]
  node [
    id 1700
    label "skrzyde&#322;ko"
  ]
  node [
    id 1701
    label "czapka"
  ]
  node [
    id 1702
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 1703
    label "polano"
  ]
  node [
    id 1704
    label "podstawczak"
  ]
  node [
    id 1705
    label "grzyb"
  ]
  node [
    id 1706
    label "g&#322;owniowate"
  ]
  node [
    id 1707
    label "bro&#324;_sieczna"
  ]
  node [
    id 1708
    label "paso&#380;yt"
  ]
  node [
    id 1709
    label "brand"
  ]
  node [
    id 1710
    label "g&#322;ownia_pyl&#261;ca"
  ]
  node [
    id 1711
    label "kapelusz"
  ]
  node [
    id 1712
    label "uniform"
  ]
  node [
    id 1713
    label "piek&#322;o"
  ]
  node [
    id 1714
    label "&#380;elazko"
  ]
  node [
    id 1715
    label "odwaga"
  ]
  node [
    id 1716
    label "sfera_afektywna"
  ]
  node [
    id 1717
    label "deformowanie"
  ]
  node [
    id 1718
    label "core"
  ]
  node [
    id 1719
    label "mind"
  ]
  node [
    id 1720
    label "sumienie"
  ]
  node [
    id 1721
    label "sztabka"
  ]
  node [
    id 1722
    label "deformowa&#263;"
  ]
  node [
    id 1723
    label "rdze&#324;"
  ]
  node [
    id 1724
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1725
    label "schody"
  ]
  node [
    id 1726
    label "pupa"
  ]
  node [
    id 1727
    label "klocek"
  ]
  node [
    id 1728
    label "instrument_smyczkowy"
  ]
  node [
    id 1729
    label "seksualno&#347;&#263;"
  ]
  node [
    id 1730
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1731
    label "byt"
  ]
  node [
    id 1732
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 1733
    label "lina"
  ]
  node [
    id 1734
    label "ego"
  ]
  node [
    id 1735
    label "kompleks"
  ]
  node [
    id 1736
    label "motor"
  ]
  node [
    id 1737
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1738
    label "mi&#281;kisz"
  ]
  node [
    id 1739
    label "marrow"
  ]
  node [
    id 1740
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1741
    label "dudka"
  ]
  node [
    id 1742
    label "sp&#322;ywak"
  ]
  node [
    id 1743
    label "inkaust"
  ]
  node [
    id 1744
    label "ka&#322;amarz"
  ]
  node [
    id 1745
    label "roztw&#243;r"
  ]
  node [
    id 1746
    label "hook"
  ]
  node [
    id 1747
    label "linijka"
  ]
  node [
    id 1748
    label "miara"
  ]
  node [
    id 1749
    label "ko&#324;c&#243;wka"
  ]
  node [
    id 1750
    label "holder"
  ]
  node [
    id 1751
    label "o&#322;&#243;wek"
  ]
  node [
    id 1752
    label "wykluczy&#263;"
  ]
  node [
    id 1753
    label "make_out"
  ]
  node [
    id 1754
    label "d&#322;ugopis"
  ]
  node [
    id 1755
    label "wymieni&#263;"
  ]
  node [
    id 1756
    label "discharge"
  ]
  node [
    id 1757
    label "distill"
  ]
  node [
    id 1758
    label "wymienienie"
  ]
  node [
    id 1759
    label "napisanie"
  ]
  node [
    id 1760
    label "wykluczenie"
  ]
  node [
    id 1761
    label "pull"
  ]
  node [
    id 1762
    label "press"
  ]
  node [
    id 1763
    label "oddziela&#263;"
  ]
  node [
    id 1764
    label "abstract"
  ]
  node [
    id 1765
    label "dzieli&#263;"
  ]
  node [
    id 1766
    label "odosobnia&#263;"
  ]
  node [
    id 1767
    label "transgress"
  ]
  node [
    id 1768
    label "specjalny"
  ]
  node [
    id 1769
    label "&#380;ywicowy"
  ]
  node [
    id 1770
    label "czerwony"
  ]
  node [
    id 1771
    label "kra&#347;ny"
  ]
  node [
    id 1772
    label "komuszek"
  ]
  node [
    id 1773
    label "czerwienienie_si&#281;"
  ]
  node [
    id 1774
    label "rozpalanie_si&#281;"
  ]
  node [
    id 1775
    label "zaczerwienienie_si&#281;"
  ]
  node [
    id 1776
    label "rozpalony"
  ]
  node [
    id 1777
    label "tubylec"
  ]
  node [
    id 1778
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 1779
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 1780
    label "radyka&#322;"
  ]
  node [
    id 1781
    label "dzia&#322;acz"
  ]
  node [
    id 1782
    label "demokrata"
  ]
  node [
    id 1783
    label "lewactwo"
  ]
  node [
    id 1784
    label "Gierek"
  ]
  node [
    id 1785
    label "Tito"
  ]
  node [
    id 1786
    label "lewicowy"
  ]
  node [
    id 1787
    label "Bre&#380;niew"
  ]
  node [
    id 1788
    label "Mao"
  ]
  node [
    id 1789
    label "czerwono"
  ]
  node [
    id 1790
    label "Polak"
  ]
  node [
    id 1791
    label "skomunizowanie"
  ]
  node [
    id 1792
    label "rozpalenie_si&#281;"
  ]
  node [
    id 1793
    label "lewicowiec"
  ]
  node [
    id 1794
    label "Amerykanin"
  ]
  node [
    id 1795
    label "rumiany"
  ]
  node [
    id 1796
    label "czerwienienie"
  ]
  node [
    id 1797
    label "sczerwienienie"
  ]
  node [
    id 1798
    label "Bierut"
  ]
  node [
    id 1799
    label "Fidel_Castro"
  ]
  node [
    id 1800
    label "rezerwat"
  ]
  node [
    id 1801
    label "zaczerwienienie"
  ]
  node [
    id 1802
    label "Stalin"
  ]
  node [
    id 1803
    label "Chruszczow"
  ]
  node [
    id 1804
    label "ciep&#322;y"
  ]
  node [
    id 1805
    label "dojrza&#322;y"
  ]
  node [
    id 1806
    label "Gomu&#322;ka"
  ]
  node [
    id 1807
    label "reformator"
  ]
  node [
    id 1808
    label "komunizowanie"
  ]
  node [
    id 1809
    label "intencjonalny"
  ]
  node [
    id 1810
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 1811
    label "niedorozw&#243;j"
  ]
  node [
    id 1812
    label "szczeg&#243;lny"
  ]
  node [
    id 1813
    label "specjalnie"
  ]
  node [
    id 1814
    label "nieetatowy"
  ]
  node [
    id 1815
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 1816
    label "nienormalny"
  ]
  node [
    id 1817
    label "umy&#347;lnie"
  ]
  node [
    id 1818
    label "odpowiedni"
  ]
  node [
    id 1819
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 1820
    label "naturalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 4
    target 273
  ]
  edge [
    source 4
    target 274
  ]
  edge [
    source 4
    target 275
  ]
  edge [
    source 4
    target 276
  ]
  edge [
    source 4
    target 277
  ]
  edge [
    source 4
    target 278
  ]
  edge [
    source 4
    target 279
  ]
  edge [
    source 4
    target 280
  ]
  edge [
    source 4
    target 281
  ]
  edge [
    source 4
    target 282
  ]
  edge [
    source 4
    target 283
  ]
  edge [
    source 4
    target 284
  ]
  edge [
    source 4
    target 285
  ]
  edge [
    source 4
    target 286
  ]
  edge [
    source 4
    target 287
  ]
  edge [
    source 4
    target 288
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 290
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 292
  ]
  edge [
    source 4
    target 293
  ]
  edge [
    source 4
    target 294
  ]
  edge [
    source 4
    target 295
  ]
  edge [
    source 4
    target 296
  ]
  edge [
    source 4
    target 297
  ]
  edge [
    source 4
    target 298
  ]
  edge [
    source 4
    target 299
  ]
  edge [
    source 4
    target 300
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 362
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
  edge [
    source 12
    target 473
  ]
  edge [
    source 12
    target 474
  ]
  edge [
    source 12
    target 475
  ]
  edge [
    source 12
    target 476
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 491
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 58
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 228
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 13
    target 507
  ]
  edge [
    source 13
    target 508
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 71
  ]
  edge [
    source 13
    target 72
  ]
  edge [
    source 13
    target 73
  ]
  edge [
    source 13
    target 74
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 509
  ]
  edge [
    source 13
    target 510
  ]
  edge [
    source 13
    target 511
  ]
  edge [
    source 13
    target 353
  ]
  edge [
    source 13
    target 512
  ]
  edge [
    source 13
    target 513
  ]
  edge [
    source 13
    target 514
  ]
  edge [
    source 13
    target 515
  ]
  edge [
    source 13
    target 516
  ]
  edge [
    source 13
    target 517
  ]
  edge [
    source 13
    target 518
  ]
  edge [
    source 13
    target 519
  ]
  edge [
    source 13
    target 520
  ]
  edge [
    source 13
    target 521
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 523
  ]
  edge [
    source 13
    target 524
  ]
  edge [
    source 13
    target 525
  ]
  edge [
    source 13
    target 526
  ]
  edge [
    source 13
    target 527
  ]
  edge [
    source 13
    target 528
  ]
  edge [
    source 13
    target 529
  ]
  edge [
    source 13
    target 530
  ]
  edge [
    source 13
    target 531
  ]
  edge [
    source 13
    target 532
  ]
  edge [
    source 13
    target 533
  ]
  edge [
    source 13
    target 534
  ]
  edge [
    source 13
    target 535
  ]
  edge [
    source 13
    target 536
  ]
  edge [
    source 13
    target 537
  ]
  edge [
    source 13
    target 538
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 540
  ]
  edge [
    source 13
    target 541
  ]
  edge [
    source 13
    target 542
  ]
  edge [
    source 13
    target 543
  ]
  edge [
    source 13
    target 544
  ]
  edge [
    source 13
    target 545
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 546
  ]
  edge [
    source 13
    target 547
  ]
  edge [
    source 13
    target 548
  ]
  edge [
    source 13
    target 549
  ]
  edge [
    source 13
    target 550
  ]
  edge [
    source 13
    target 551
  ]
  edge [
    source 13
    target 552
  ]
  edge [
    source 13
    target 18
  ]
  edge [
    source 13
    target 553
  ]
  edge [
    source 13
    target 554
  ]
  edge [
    source 13
    target 555
  ]
  edge [
    source 13
    target 556
  ]
  edge [
    source 13
    target 557
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 559
  ]
  edge [
    source 13
    target 560
  ]
  edge [
    source 13
    target 561
  ]
  edge [
    source 13
    target 562
  ]
  edge [
    source 13
    target 563
  ]
  edge [
    source 13
    target 564
  ]
  edge [
    source 13
    target 565
  ]
  edge [
    source 13
    target 566
  ]
  edge [
    source 13
    target 567
  ]
  edge [
    source 13
    target 568
  ]
  edge [
    source 13
    target 569
  ]
  edge [
    source 13
    target 570
  ]
  edge [
    source 13
    target 571
  ]
  edge [
    source 13
    target 572
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 574
  ]
  edge [
    source 13
    target 575
  ]
  edge [
    source 13
    target 576
  ]
  edge [
    source 13
    target 577
  ]
  edge [
    source 13
    target 578
  ]
  edge [
    source 13
    target 579
  ]
  edge [
    source 13
    target 580
  ]
  edge [
    source 13
    target 581
  ]
  edge [
    source 13
    target 582
  ]
  edge [
    source 13
    target 583
  ]
  edge [
    source 13
    target 584
  ]
  edge [
    source 13
    target 585
  ]
  edge [
    source 13
    target 586
  ]
  edge [
    source 13
    target 587
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 14
    target 26
  ]
  edge [
    source 14
    target 588
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 589
  ]
  edge [
    source 14
    target 332
  ]
  edge [
    source 14
    target 590
  ]
  edge [
    source 14
    target 591
  ]
  edge [
    source 14
    target 592
  ]
  edge [
    source 14
    target 593
  ]
  edge [
    source 14
    target 594
  ]
  edge [
    source 14
    target 595
  ]
  edge [
    source 14
    target 596
  ]
  edge [
    source 14
    target 597
  ]
  edge [
    source 14
    target 598
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 356
  ]
  edge [
    source 14
    target 599
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 600
  ]
  edge [
    source 14
    target 601
  ]
  edge [
    source 14
    target 602
  ]
  edge [
    source 14
    target 603
  ]
  edge [
    source 14
    target 604
  ]
  edge [
    source 14
    target 605
  ]
  edge [
    source 14
    target 606
  ]
  edge [
    source 14
    target 607
  ]
  edge [
    source 14
    target 608
  ]
  edge [
    source 14
    target 609
  ]
  edge [
    source 14
    target 610
  ]
  edge [
    source 14
    target 611
  ]
  edge [
    source 14
    target 612
  ]
  edge [
    source 14
    target 228
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 615
  ]
  edge [
    source 14
    target 616
  ]
  edge [
    source 14
    target 617
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 619
  ]
  edge [
    source 14
    target 620
  ]
  edge [
    source 14
    target 621
  ]
  edge [
    source 14
    target 622
  ]
  edge [
    source 14
    target 623
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 625
  ]
  edge [
    source 14
    target 626
  ]
  edge [
    source 14
    target 627
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 629
  ]
  edge [
    source 14
    target 630
  ]
  edge [
    source 14
    target 631
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 633
  ]
  edge [
    source 14
    target 634
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 635
  ]
  edge [
    source 16
    target 636
  ]
  edge [
    source 16
    target 637
  ]
  edge [
    source 16
    target 638
  ]
  edge [
    source 16
    target 639
  ]
  edge [
    source 16
    target 640
  ]
  edge [
    source 16
    target 641
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 642
  ]
  edge [
    source 16
    target 643
  ]
  edge [
    source 16
    target 644
  ]
  edge [
    source 16
    target 645
  ]
  edge [
    source 16
    target 646
  ]
  edge [
    source 16
    target 647
  ]
  edge [
    source 16
    target 648
  ]
  edge [
    source 16
    target 649
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 234
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 653
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 16
    target 655
  ]
  edge [
    source 16
    target 656
  ]
  edge [
    source 16
    target 58
  ]
  edge [
    source 16
    target 657
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 658
  ]
  edge [
    source 16
    target 659
  ]
  edge [
    source 16
    target 660
  ]
  edge [
    source 16
    target 661
  ]
  edge [
    source 16
    target 202
  ]
  edge [
    source 16
    target 662
  ]
  edge [
    source 16
    target 204
  ]
  edge [
    source 16
    target 663
  ]
  edge [
    source 16
    target 664
  ]
  edge [
    source 16
    target 665
  ]
  edge [
    source 16
    target 666
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 668
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 670
  ]
  edge [
    source 16
    target 239
  ]
  edge [
    source 16
    target 671
  ]
  edge [
    source 16
    target 326
  ]
  edge [
    source 16
    target 241
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 672
  ]
  edge [
    source 16
    target 228
  ]
  edge [
    source 16
    target 517
  ]
  edge [
    source 16
    target 673
  ]
  edge [
    source 16
    target 674
  ]
  edge [
    source 16
    target 675
  ]
  edge [
    source 16
    target 676
  ]
  edge [
    source 16
    target 206
  ]
  edge [
    source 16
    target 677
  ]
  edge [
    source 16
    target 678
  ]
  edge [
    source 16
    target 679
  ]
  edge [
    source 16
    target 680
  ]
  edge [
    source 16
    target 681
  ]
  edge [
    source 16
    target 682
  ]
  edge [
    source 16
    target 683
  ]
  edge [
    source 16
    target 684
  ]
  edge [
    source 16
    target 685
  ]
  edge [
    source 16
    target 686
  ]
  edge [
    source 16
    target 687
  ]
  edge [
    source 16
    target 688
  ]
  edge [
    source 16
    target 689
  ]
  edge [
    source 16
    target 690
  ]
  edge [
    source 16
    target 691
  ]
  edge [
    source 16
    target 692
  ]
  edge [
    source 16
    target 693
  ]
  edge [
    source 16
    target 694
  ]
  edge [
    source 16
    target 695
  ]
  edge [
    source 16
    target 696
  ]
  edge [
    source 16
    target 697
  ]
  edge [
    source 16
    target 698
  ]
  edge [
    source 16
    target 699
  ]
  edge [
    source 16
    target 700
  ]
  edge [
    source 16
    target 701
  ]
  edge [
    source 16
    target 702
  ]
  edge [
    source 16
    target 703
  ]
  edge [
    source 16
    target 704
  ]
  edge [
    source 16
    target 705
  ]
  edge [
    source 16
    target 706
  ]
  edge [
    source 16
    target 503
  ]
  edge [
    source 16
    target 707
  ]
  edge [
    source 16
    target 708
  ]
  edge [
    source 16
    target 709
  ]
  edge [
    source 16
    target 710
  ]
  edge [
    source 16
    target 711
  ]
  edge [
    source 16
    target 712
  ]
  edge [
    source 16
    target 713
  ]
  edge [
    source 16
    target 714
  ]
  edge [
    source 16
    target 715
  ]
  edge [
    source 16
    target 716
  ]
  edge [
    source 16
    target 717
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 718
  ]
  edge [
    source 16
    target 719
  ]
  edge [
    source 16
    target 720
  ]
  edge [
    source 16
    target 721
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 722
  ]
  edge [
    source 16
    target 723
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 724
  ]
  edge [
    source 16
    target 725
  ]
  edge [
    source 16
    target 726
  ]
  edge [
    source 16
    target 727
  ]
  edge [
    source 16
    target 728
  ]
  edge [
    source 16
    target 729
  ]
  edge [
    source 16
    target 186
  ]
  edge [
    source 16
    target 730
  ]
  edge [
    source 16
    target 731
  ]
  edge [
    source 16
    target 732
  ]
  edge [
    source 16
    target 733
  ]
  edge [
    source 16
    target 734
  ]
  edge [
    source 16
    target 735
  ]
  edge [
    source 16
    target 736
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 737
  ]
  edge [
    source 16
    target 738
  ]
  edge [
    source 16
    target 739
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 740
  ]
  edge [
    source 16
    target 741
  ]
  edge [
    source 16
    target 742
  ]
  edge [
    source 16
    target 743
  ]
  edge [
    source 16
    target 744
  ]
  edge [
    source 16
    target 745
  ]
  edge [
    source 16
    target 746
  ]
  edge [
    source 16
    target 747
  ]
  edge [
    source 16
    target 67
  ]
  edge [
    source 16
    target 68
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 16
    target 70
  ]
  edge [
    source 16
    target 71
  ]
  edge [
    source 16
    target 72
  ]
  edge [
    source 16
    target 73
  ]
  edge [
    source 16
    target 74
  ]
  edge [
    source 16
    target 75
  ]
  edge [
    source 16
    target 76
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 16
    target 84
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 16
    target 87
  ]
  edge [
    source 16
    target 88
  ]
  edge [
    source 16
    target 89
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 91
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 748
  ]
  edge [
    source 16
    target 749
  ]
  edge [
    source 16
    target 750
  ]
  edge [
    source 16
    target 751
  ]
  edge [
    source 16
    target 752
  ]
  edge [
    source 16
    target 753
  ]
  edge [
    source 16
    target 754
  ]
  edge [
    source 16
    target 755
  ]
  edge [
    source 16
    target 756
  ]
  edge [
    source 16
    target 757
  ]
  edge [
    source 16
    target 758
  ]
  edge [
    source 16
    target 759
  ]
  edge [
    source 16
    target 760
  ]
  edge [
    source 16
    target 761
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 762
  ]
  edge [
    source 16
    target 763
  ]
  edge [
    source 16
    target 764
  ]
  edge [
    source 16
    target 765
  ]
  edge [
    source 16
    target 766
  ]
  edge [
    source 16
    target 767
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 768
  ]
  edge [
    source 16
    target 769
  ]
  edge [
    source 16
    target 770
  ]
  edge [
    source 16
    target 771
  ]
  edge [
    source 16
    target 772
  ]
  edge [
    source 16
    target 305
  ]
  edge [
    source 16
    target 773
  ]
  edge [
    source 16
    target 774
  ]
  edge [
    source 16
    target 775
  ]
  edge [
    source 16
    target 776
  ]
  edge [
    source 16
    target 777
  ]
  edge [
    source 16
    target 778
  ]
  edge [
    source 16
    target 779
  ]
  edge [
    source 16
    target 780
  ]
  edge [
    source 16
    target 781
  ]
  edge [
    source 16
    target 782
  ]
  edge [
    source 16
    target 783
  ]
  edge [
    source 16
    target 784
  ]
  edge [
    source 16
    target 785
  ]
  edge [
    source 16
    target 508
  ]
  edge [
    source 16
    target 786
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 787
  ]
  edge [
    source 16
    target 788
  ]
  edge [
    source 16
    target 789
  ]
  edge [
    source 16
    target 790
  ]
  edge [
    source 16
    target 332
  ]
  edge [
    source 16
    target 791
  ]
  edge [
    source 16
    target 792
  ]
  edge [
    source 16
    target 793
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 794
  ]
  edge [
    source 16
    target 795
  ]
  edge [
    source 16
    target 796
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 797
  ]
  edge [
    source 16
    target 798
  ]
  edge [
    source 16
    target 799
  ]
  edge [
    source 16
    target 800
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 801
  ]
  edge [
    source 16
    target 802
  ]
  edge [
    source 16
    target 803
  ]
  edge [
    source 16
    target 804
  ]
  edge [
    source 16
    target 805
  ]
  edge [
    source 16
    target 806
  ]
  edge [
    source 16
    target 807
  ]
  edge [
    source 16
    target 492
  ]
  edge [
    source 16
    target 808
  ]
  edge [
    source 16
    target 809
  ]
  edge [
    source 16
    target 810
  ]
  edge [
    source 16
    target 811
  ]
  edge [
    source 16
    target 812
  ]
  edge [
    source 16
    target 813
  ]
  edge [
    source 16
    target 621
  ]
  edge [
    source 16
    target 814
  ]
  edge [
    source 16
    target 815
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 816
  ]
  edge [
    source 16
    target 817
  ]
  edge [
    source 16
    target 818
  ]
  edge [
    source 16
    target 819
  ]
  edge [
    source 16
    target 820
  ]
  edge [
    source 16
    target 821
  ]
  edge [
    source 16
    target 822
  ]
  edge [
    source 16
    target 823
  ]
  edge [
    source 16
    target 824
  ]
  edge [
    source 16
    target 825
  ]
  edge [
    source 16
    target 826
  ]
  edge [
    source 16
    target 827
  ]
  edge [
    source 16
    target 828
  ]
  edge [
    source 16
    target 829
  ]
  edge [
    source 16
    target 830
  ]
  edge [
    source 16
    target 831
  ]
  edge [
    source 16
    target 832
  ]
  edge [
    source 16
    target 833
  ]
  edge [
    source 16
    target 834
  ]
  edge [
    source 16
    target 835
  ]
  edge [
    source 16
    target 836
  ]
  edge [
    source 16
    target 837
  ]
  edge [
    source 16
    target 838
  ]
  edge [
    source 16
    target 839
  ]
  edge [
    source 16
    target 840
  ]
  edge [
    source 16
    target 841
  ]
  edge [
    source 16
    target 842
  ]
  edge [
    source 16
    target 213
  ]
  edge [
    source 16
    target 843
  ]
  edge [
    source 16
    target 844
  ]
  edge [
    source 16
    target 845
  ]
  edge [
    source 16
    target 846
  ]
  edge [
    source 16
    target 847
  ]
  edge [
    source 16
    target 458
  ]
  edge [
    source 16
    target 848
  ]
  edge [
    source 16
    target 849
  ]
  edge [
    source 16
    target 850
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 852
  ]
  edge [
    source 16
    target 853
  ]
  edge [
    source 16
    target 854
  ]
  edge [
    source 16
    target 855
  ]
  edge [
    source 16
    target 856
  ]
  edge [
    source 16
    target 857
  ]
  edge [
    source 16
    target 858
  ]
  edge [
    source 16
    target 859
  ]
  edge [
    source 16
    target 860
  ]
  edge [
    source 16
    target 861
  ]
  edge [
    source 16
    target 862
  ]
  edge [
    source 16
    target 496
  ]
  edge [
    source 16
    target 863
  ]
  edge [
    source 16
    target 864
  ]
  edge [
    source 16
    target 865
  ]
  edge [
    source 16
    target 866
  ]
  edge [
    source 16
    target 867
  ]
  edge [
    source 16
    target 868
  ]
  edge [
    source 16
    target 869
  ]
  edge [
    source 16
    target 870
  ]
  edge [
    source 16
    target 871
  ]
  edge [
    source 16
    target 872
  ]
  edge [
    source 16
    target 873
  ]
  edge [
    source 16
    target 874
  ]
  edge [
    source 16
    target 875
  ]
  edge [
    source 16
    target 876
  ]
  edge [
    source 16
    target 877
  ]
  edge [
    source 16
    target 878
  ]
  edge [
    source 16
    target 879
  ]
  edge [
    source 16
    target 880
  ]
  edge [
    source 16
    target 317
  ]
  edge [
    source 16
    target 881
  ]
  edge [
    source 16
    target 882
  ]
  edge [
    source 16
    target 883
  ]
  edge [
    source 16
    target 884
  ]
  edge [
    source 16
    target 353
  ]
  edge [
    source 16
    target 885
  ]
  edge [
    source 16
    target 886
  ]
  edge [
    source 16
    target 887
  ]
  edge [
    source 16
    target 888
  ]
  edge [
    source 16
    target 889
  ]
  edge [
    source 16
    target 890
  ]
  edge [
    source 16
    target 891
  ]
  edge [
    source 16
    target 892
  ]
  edge [
    source 16
    target 893
  ]
  edge [
    source 16
    target 894
  ]
  edge [
    source 16
    target 895
  ]
  edge [
    source 16
    target 896
  ]
  edge [
    source 16
    target 897
  ]
  edge [
    source 16
    target 898
  ]
  edge [
    source 16
    target 899
  ]
  edge [
    source 16
    target 900
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 16
    target 901
  ]
  edge [
    source 16
    target 634
  ]
  edge [
    source 16
    target 902
  ]
  edge [
    source 16
    target 903
  ]
  edge [
    source 16
    target 904
  ]
  edge [
    source 16
    target 905
  ]
  edge [
    source 16
    target 906
  ]
  edge [
    source 16
    target 907
  ]
  edge [
    source 16
    target 908
  ]
  edge [
    source 16
    target 909
  ]
  edge [
    source 16
    target 910
  ]
  edge [
    source 16
    target 911
  ]
  edge [
    source 16
    target 912
  ]
  edge [
    source 16
    target 913
  ]
  edge [
    source 16
    target 914
  ]
  edge [
    source 16
    target 915
  ]
  edge [
    source 16
    target 601
  ]
  edge [
    source 16
    target 916
  ]
  edge [
    source 16
    target 917
  ]
  edge [
    source 16
    target 918
  ]
  edge [
    source 16
    target 919
  ]
  edge [
    source 16
    target 920
  ]
  edge [
    source 16
    target 345
  ]
  edge [
    source 16
    target 921
  ]
  edge [
    source 16
    target 922
  ]
  edge [
    source 16
    target 923
  ]
  edge [
    source 16
    target 924
  ]
  edge [
    source 16
    target 925
  ]
  edge [
    source 16
    target 926
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 930
  ]
  edge [
    source 17
    target 931
  ]
  edge [
    source 17
    target 932
  ]
  edge [
    source 17
    target 933
  ]
  edge [
    source 17
    target 756
  ]
  edge [
    source 17
    target 934
  ]
  edge [
    source 17
    target 935
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 84
  ]
  edge [
    source 17
    target 937
  ]
  edge [
    source 17
    target 938
  ]
  edge [
    source 17
    target 94
  ]
  edge [
    source 17
    target 939
  ]
  edge [
    source 17
    target 940
  ]
  edge [
    source 17
    target 941
  ]
  edge [
    source 17
    target 942
  ]
  edge [
    source 17
    target 943
  ]
  edge [
    source 17
    target 944
  ]
  edge [
    source 17
    target 945
  ]
  edge [
    source 17
    target 946
  ]
  edge [
    source 17
    target 947
  ]
  edge [
    source 17
    target 948
  ]
  edge [
    source 17
    target 949
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 950
  ]
  edge [
    source 17
    target 951
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 17
    target 953
  ]
  edge [
    source 17
    target 954
  ]
  edge [
    source 17
    target 955
  ]
  edge [
    source 17
    target 956
  ]
  edge [
    source 17
    target 957
  ]
  edge [
    source 17
    target 958
  ]
  edge [
    source 17
    target 959
  ]
  edge [
    source 17
    target 960
  ]
  edge [
    source 17
    target 961
  ]
  edge [
    source 17
    target 760
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 963
  ]
  edge [
    source 17
    target 964
  ]
  edge [
    source 17
    target 965
  ]
  edge [
    source 17
    target 966
  ]
  edge [
    source 17
    target 967
  ]
  edge [
    source 17
    target 968
  ]
  edge [
    source 17
    target 969
  ]
  edge [
    source 17
    target 970
  ]
  edge [
    source 17
    target 733
  ]
  edge [
    source 17
    target 971
  ]
  edge [
    source 17
    target 737
  ]
  edge [
    source 17
    target 972
  ]
  edge [
    source 17
    target 973
  ]
  edge [
    source 17
    target 974
  ]
  edge [
    source 17
    target 778
  ]
  edge [
    source 17
    target 414
  ]
  edge [
    source 17
    target 975
  ]
  edge [
    source 17
    target 976
  ]
  edge [
    source 17
    target 768
  ]
  edge [
    source 17
    target 757
  ]
  edge [
    source 17
    target 750
  ]
  edge [
    source 17
    target 305
  ]
  edge [
    source 17
    target 773
  ]
  edge [
    source 17
    target 769
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 774
  ]
  edge [
    source 17
    target 775
  ]
  edge [
    source 17
    target 777
  ]
  edge [
    source 17
    target 752
  ]
  edge [
    source 17
    target 599
  ]
  edge [
    source 17
    target 889
  ]
  edge [
    source 17
    target 326
  ]
  edge [
    source 17
    target 767
  ]
  edge [
    source 17
    target 977
  ]
  edge [
    source 17
    target 978
  ]
  edge [
    source 17
    target 979
  ]
  edge [
    source 17
    target 980
  ]
  edge [
    source 17
    target 981
  ]
  edge [
    source 17
    target 982
  ]
  edge [
    source 17
    target 983
  ]
  edge [
    source 17
    target 984
  ]
  edge [
    source 17
    target 985
  ]
  edge [
    source 17
    target 616
  ]
  edge [
    source 17
    target 986
  ]
  edge [
    source 17
    target 987
  ]
  edge [
    source 17
    target 988
  ]
  edge [
    source 17
    target 989
  ]
  edge [
    source 17
    target 990
  ]
  edge [
    source 17
    target 991
  ]
  edge [
    source 17
    target 992
  ]
  edge [
    source 17
    target 993
  ]
  edge [
    source 17
    target 617
  ]
  edge [
    source 17
    target 994
  ]
  edge [
    source 17
    target 995
  ]
  edge [
    source 17
    target 996
  ]
  edge [
    source 17
    target 997
  ]
  edge [
    source 17
    target 998
  ]
  edge [
    source 17
    target 999
  ]
  edge [
    source 17
    target 1000
  ]
  edge [
    source 17
    target 1001
  ]
  edge [
    source 17
    target 755
  ]
  edge [
    source 17
    target 1002
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 483
  ]
  edge [
    source 17
    target 492
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 446
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 17
    target 1038
  ]
  edge [
    source 17
    target 1039
  ]
  edge [
    source 17
    target 1040
  ]
  edge [
    source 17
    target 1041
  ]
  edge [
    source 17
    target 1042
  ]
  edge [
    source 17
    target 1043
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1045
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 851
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 445
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 170
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 831
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 31
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1106
  ]
  edge [
    source 18
    target 1107
  ]
  edge [
    source 18
    target 1108
  ]
  edge [
    source 18
    target 1109
  ]
  edge [
    source 18
    target 1110
  ]
  edge [
    source 18
    target 1111
  ]
  edge [
    source 18
    target 80
  ]
  edge [
    source 18
    target 1112
  ]
  edge [
    source 18
    target 1113
  ]
  edge [
    source 18
    target 1114
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 496
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 1115
  ]
  edge [
    source 18
    target 1116
  ]
  edge [
    source 18
    target 58
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 96
  ]
  edge [
    source 18
    target 1117
  ]
  edge [
    source 18
    target 1118
  ]
  edge [
    source 18
    target 102
  ]
  edge [
    source 18
    target 1119
  ]
  edge [
    source 18
    target 1120
  ]
  edge [
    source 18
    target 67
  ]
  edge [
    source 18
    target 68
  ]
  edge [
    source 18
    target 69
  ]
  edge [
    source 18
    target 70
  ]
  edge [
    source 18
    target 71
  ]
  edge [
    source 18
    target 72
  ]
  edge [
    source 18
    target 73
  ]
  edge [
    source 18
    target 74
  ]
  edge [
    source 18
    target 75
  ]
  edge [
    source 18
    target 76
  ]
  edge [
    source 18
    target 77
  ]
  edge [
    source 18
    target 78
  ]
  edge [
    source 18
    target 79
  ]
  edge [
    source 18
    target 81
  ]
  edge [
    source 18
    target 82
  ]
  edge [
    source 18
    target 83
  ]
  edge [
    source 18
    target 84
  ]
  edge [
    source 18
    target 85
  ]
  edge [
    source 18
    target 86
  ]
  edge [
    source 18
    target 87
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 18
    target 90
  ]
  edge [
    source 18
    target 91
  ]
  edge [
    source 18
    target 92
  ]
  edge [
    source 18
    target 93
  ]
  edge [
    source 18
    target 94
  ]
  edge [
    source 18
    target 95
  ]
  edge [
    source 18
    target 97
  ]
  edge [
    source 18
    target 98
  ]
  edge [
    source 18
    target 99
  ]
  edge [
    source 18
    target 100
  ]
  edge [
    source 18
    target 101
  ]
  edge [
    source 18
    target 103
  ]
  edge [
    source 18
    target 1121
  ]
  edge [
    source 18
    target 1122
  ]
  edge [
    source 18
    target 1123
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 532
  ]
  edge [
    source 18
    target 1124
  ]
  edge [
    source 18
    target 1125
  ]
  edge [
    source 18
    target 1126
  ]
  edge [
    source 18
    target 528
  ]
  edge [
    source 18
    target 1127
  ]
  edge [
    source 18
    target 1128
  ]
  edge [
    source 18
    target 1129
  ]
  edge [
    source 18
    target 1130
  ]
  edge [
    source 18
    target 1131
  ]
  edge [
    source 18
    target 1132
  ]
  edge [
    source 18
    target 1133
  ]
  edge [
    source 18
    target 1134
  ]
  edge [
    source 18
    target 1135
  ]
  edge [
    source 18
    target 541
  ]
  edge [
    source 18
    target 1136
  ]
  edge [
    source 18
    target 1137
  ]
  edge [
    source 18
    target 1138
  ]
  edge [
    source 18
    target 1139
  ]
  edge [
    source 18
    target 1140
  ]
  edge [
    source 18
    target 1141
  ]
  edge [
    source 18
    target 519
  ]
  edge [
    source 18
    target 520
  ]
  edge [
    source 18
    target 521
  ]
  edge [
    source 18
    target 522
  ]
  edge [
    source 18
    target 523
  ]
  edge [
    source 18
    target 524
  ]
  edge [
    source 18
    target 525
  ]
  edge [
    source 18
    target 526
  ]
  edge [
    source 18
    target 527
  ]
  edge [
    source 18
    target 529
  ]
  edge [
    source 18
    target 530
  ]
  edge [
    source 18
    target 531
  ]
  edge [
    source 18
    target 533
  ]
  edge [
    source 18
    target 534
  ]
  edge [
    source 18
    target 535
  ]
  edge [
    source 18
    target 536
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 538
  ]
  edge [
    source 18
    target 539
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 542
  ]
  edge [
    source 18
    target 543
  ]
  edge [
    source 18
    target 544
  ]
  edge [
    source 18
    target 545
  ]
  edge [
    source 18
    target 546
  ]
  edge [
    source 18
    target 547
  ]
  edge [
    source 18
    target 548
  ]
  edge [
    source 18
    target 549
  ]
  edge [
    source 18
    target 550
  ]
  edge [
    source 18
    target 551
  ]
  edge [
    source 18
    target 552
  ]
  edge [
    source 18
    target 553
  ]
  edge [
    source 18
    target 554
  ]
  edge [
    source 18
    target 555
  ]
  edge [
    source 18
    target 556
  ]
  edge [
    source 18
    target 557
  ]
  edge [
    source 18
    target 558
  ]
  edge [
    source 18
    target 559
  ]
  edge [
    source 18
    target 560
  ]
  edge [
    source 18
    target 561
  ]
  edge [
    source 18
    target 562
  ]
  edge [
    source 18
    target 563
  ]
  edge [
    source 18
    target 564
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 566
  ]
  edge [
    source 18
    target 567
  ]
  edge [
    source 18
    target 568
  ]
  edge [
    source 18
    target 569
  ]
  edge [
    source 18
    target 570
  ]
  edge [
    source 18
    target 571
  ]
  edge [
    source 18
    target 572
  ]
  edge [
    source 18
    target 573
  ]
  edge [
    source 18
    target 574
  ]
  edge [
    source 18
    target 575
  ]
  edge [
    source 18
    target 1142
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 1143
  ]
  edge [
    source 18
    target 1144
  ]
  edge [
    source 19
    target 1145
  ]
  edge [
    source 19
    target 1146
  ]
  edge [
    source 19
    target 1147
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1148
  ]
  edge [
    source 19
    target 1149
  ]
  edge [
    source 19
    target 458
  ]
  edge [
    source 19
    target 1150
  ]
  edge [
    source 19
    target 460
  ]
  edge [
    source 19
    target 1151
  ]
  edge [
    source 19
    target 462
  ]
  edge [
    source 19
    target 1152
  ]
  edge [
    source 19
    target 1153
  ]
  edge [
    source 19
    target 483
  ]
  edge [
    source 19
    target 484
  ]
  edge [
    source 19
    target 1154
  ]
  edge [
    source 19
    target 1155
  ]
  edge [
    source 19
    target 1156
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 1157
  ]
  edge [
    source 19
    target 1158
  ]
  edge [
    source 19
    target 1159
  ]
  edge [
    source 19
    target 1160
  ]
  edge [
    source 19
    target 1161
  ]
  edge [
    source 19
    target 1162
  ]
  edge [
    source 19
    target 1163
  ]
  edge [
    source 19
    target 1164
  ]
  edge [
    source 19
    target 1165
  ]
  edge [
    source 19
    target 1166
  ]
  edge [
    source 19
    target 1167
  ]
  edge [
    source 19
    target 1168
  ]
  edge [
    source 19
    target 1169
  ]
  edge [
    source 19
    target 1170
  ]
  edge [
    source 19
    target 1171
  ]
  edge [
    source 19
    target 1172
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 1173
  ]
  edge [
    source 19
    target 1174
  ]
  edge [
    source 19
    target 1175
  ]
  edge [
    source 19
    target 1176
  ]
  edge [
    source 19
    target 1177
  ]
  edge [
    source 19
    target 1178
  ]
  edge [
    source 19
    target 1179
  ]
  edge [
    source 19
    target 1180
  ]
  edge [
    source 19
    target 1181
  ]
  edge [
    source 19
    target 447
  ]
  edge [
    source 19
    target 1182
  ]
  edge [
    source 19
    target 1183
  ]
  edge [
    source 19
    target 1184
  ]
  edge [
    source 19
    target 1185
  ]
  edge [
    source 19
    target 451
  ]
  edge [
    source 19
    target 1186
  ]
  edge [
    source 19
    target 1187
  ]
  edge [
    source 19
    target 1188
  ]
  edge [
    source 19
    target 1189
  ]
  edge [
    source 19
    target 1190
  ]
  edge [
    source 19
    target 444
  ]
  edge [
    source 19
    target 1191
  ]
  edge [
    source 19
    target 1192
  ]
  edge [
    source 19
    target 1193
  ]
  edge [
    source 19
    target 492
  ]
  edge [
    source 19
    target 442
  ]
  edge [
    source 19
    target 1194
  ]
  edge [
    source 19
    target 446
  ]
  edge [
    source 19
    target 1195
  ]
  edge [
    source 19
    target 1196
  ]
  edge [
    source 19
    target 1066
  ]
  edge [
    source 19
    target 1105
  ]
  edge [
    source 19
    target 1197
  ]
  edge [
    source 19
    target 1198
  ]
  edge [
    source 19
    target 1199
  ]
  edge [
    source 19
    target 1200
  ]
  edge [
    source 19
    target 1201
  ]
  edge [
    source 19
    target 479
  ]
  edge [
    source 19
    target 1202
  ]
  edge [
    source 19
    target 1203
  ]
  edge [
    source 19
    target 1204
  ]
  edge [
    source 19
    target 1205
  ]
  edge [
    source 19
    target 1206
  ]
  edge [
    source 19
    target 1207
  ]
  edge [
    source 19
    target 1208
  ]
  edge [
    source 19
    target 1209
  ]
  edge [
    source 19
    target 1210
  ]
  edge [
    source 19
    target 454
  ]
  edge [
    source 19
    target 1211
  ]
  edge [
    source 19
    target 1212
  ]
  edge [
    source 19
    target 1213
  ]
  edge [
    source 19
    target 1214
  ]
  edge [
    source 19
    target 1215
  ]
  edge [
    source 19
    target 1216
  ]
  edge [
    source 19
    target 1217
  ]
  edge [
    source 19
    target 1218
  ]
  edge [
    source 19
    target 1219
  ]
  edge [
    source 19
    target 1220
  ]
  edge [
    source 19
    target 1221
  ]
  edge [
    source 19
    target 1222
  ]
  edge [
    source 19
    target 1094
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 1223
  ]
  edge [
    source 19
    target 1224
  ]
  edge [
    source 19
    target 1225
  ]
  edge [
    source 19
    target 1226
  ]
  edge [
    source 19
    target 1227
  ]
  edge [
    source 19
    target 1228
  ]
  edge [
    source 19
    target 1229
  ]
  edge [
    source 19
    target 1230
  ]
  edge [
    source 19
    target 1231
  ]
  edge [
    source 19
    target 1232
  ]
  edge [
    source 19
    target 1233
  ]
  edge [
    source 19
    target 1234
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 1235
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 1236
  ]
  edge [
    source 19
    target 1237
  ]
  edge [
    source 19
    target 1238
  ]
  edge [
    source 19
    target 1239
  ]
  edge [
    source 19
    target 1240
  ]
  edge [
    source 19
    target 1241
  ]
  edge [
    source 19
    target 1242
  ]
  edge [
    source 19
    target 1243
  ]
  edge [
    source 19
    target 1244
  ]
  edge [
    source 19
    target 1245
  ]
  edge [
    source 19
    target 1246
  ]
  edge [
    source 19
    target 1247
  ]
  edge [
    source 19
    target 593
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1256
  ]
  edge [
    source 21
    target 1257
  ]
  edge [
    source 21
    target 1258
  ]
  edge [
    source 21
    target 1259
  ]
  edge [
    source 21
    target 1260
  ]
  edge [
    source 21
    target 1261
  ]
  edge [
    source 21
    target 1262
  ]
  edge [
    source 21
    target 1263
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 580
  ]
  edge [
    source 22
    target 1264
  ]
  edge [
    source 22
    target 1265
  ]
  edge [
    source 22
    target 1266
  ]
  edge [
    source 22
    target 1267
  ]
  edge [
    source 22
    target 1268
  ]
  edge [
    source 22
    target 1269
  ]
  edge [
    source 22
    target 1270
  ]
  edge [
    source 22
    target 1271
  ]
  edge [
    source 22
    target 1272
  ]
  edge [
    source 22
    target 408
  ]
  edge [
    source 22
    target 1273
  ]
  edge [
    source 22
    target 1274
  ]
  edge [
    source 22
    target 1275
  ]
  edge [
    source 22
    target 1276
  ]
  edge [
    source 22
    target 1277
  ]
  edge [
    source 22
    target 433
  ]
  edge [
    source 22
    target 1278
  ]
  edge [
    source 22
    target 1279
  ]
  edge [
    source 22
    target 1280
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 1281
  ]
  edge [
    source 22
    target 1282
  ]
  edge [
    source 22
    target 1283
  ]
  edge [
    source 22
    target 1284
  ]
  edge [
    source 22
    target 159
  ]
  edge [
    source 22
    target 1285
  ]
  edge [
    source 22
    target 33
  ]
  edge [
    source 22
    target 1286
  ]
  edge [
    source 22
    target 338
  ]
  edge [
    source 22
    target 328
  ]
  edge [
    source 22
    target 327
  ]
  edge [
    source 22
    target 342
  ]
  edge [
    source 22
    target 319
  ]
  edge [
    source 22
    target 351
  ]
  edge [
    source 22
    target 331
  ]
  edge [
    source 22
    target 1287
  ]
  edge [
    source 22
    target 1288
  ]
  edge [
    source 22
    target 334
  ]
  edge [
    source 22
    target 347
  ]
  edge [
    source 22
    target 322
  ]
  edge [
    source 22
    target 345
  ]
  edge [
    source 22
    target 805
  ]
  edge [
    source 22
    target 1289
  ]
  edge [
    source 22
    target 335
  ]
  edge [
    source 22
    target 346
  ]
  edge [
    source 22
    target 1290
  ]
  edge [
    source 22
    target 329
  ]
  edge [
    source 22
    target 1291
  ]
  edge [
    source 22
    target 1292
  ]
  edge [
    source 22
    target 1293
  ]
  edge [
    source 22
    target 1294
  ]
  edge [
    source 22
    target 1295
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 1296
  ]
  edge [
    source 22
    target 1297
  ]
  edge [
    source 22
    target 1298
  ]
  edge [
    source 22
    target 1299
  ]
  edge [
    source 22
    target 1300
  ]
  edge [
    source 22
    target 621
  ]
  edge [
    source 22
    target 540
  ]
  edge [
    source 22
    target 1301
  ]
  edge [
    source 22
    target 1302
  ]
  edge [
    source 22
    target 1303
  ]
  edge [
    source 22
    target 407
  ]
  edge [
    source 22
    target 1304
  ]
  edge [
    source 22
    target 1305
  ]
  edge [
    source 22
    target 1306
  ]
  edge [
    source 22
    target 1307
  ]
  edge [
    source 22
    target 111
  ]
  edge [
    source 22
    target 1308
  ]
  edge [
    source 22
    target 1309
  ]
  edge [
    source 22
    target 1310
  ]
  edge [
    source 22
    target 308
  ]
  edge [
    source 22
    target 1311
  ]
  edge [
    source 22
    target 741
  ]
  edge [
    source 22
    target 1312
  ]
  edge [
    source 22
    target 1313
  ]
  edge [
    source 22
    target 1314
  ]
  edge [
    source 22
    target 1315
  ]
  edge [
    source 22
    target 1316
  ]
  edge [
    source 22
    target 1317
  ]
  edge [
    source 22
    target 1318
  ]
  edge [
    source 22
    target 1319
  ]
  edge [
    source 22
    target 1320
  ]
  edge [
    source 22
    target 401
  ]
  edge [
    source 22
    target 1321
  ]
  edge [
    source 22
    target 1322
  ]
  edge [
    source 22
    target 1323
  ]
  edge [
    source 22
    target 1324
  ]
  edge [
    source 22
    target 1325
  ]
  edge [
    source 22
    target 1326
  ]
  edge [
    source 22
    target 616
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 1327
  ]
  edge [
    source 22
    target 1328
  ]
  edge [
    source 22
    target 1329
  ]
  edge [
    source 22
    target 1330
  ]
  edge [
    source 22
    target 431
  ]
  edge [
    source 22
    target 1331
  ]
  edge [
    source 22
    target 1332
  ]
  edge [
    source 22
    target 1333
  ]
  edge [
    source 22
    target 1334
  ]
  edge [
    source 22
    target 1335
  ]
  edge [
    source 22
    target 1336
  ]
  edge [
    source 22
    target 1337
  ]
  edge [
    source 22
    target 1338
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 1339
  ]
  edge [
    source 22
    target 1340
  ]
  edge [
    source 22
    target 1341
  ]
  edge [
    source 22
    target 1342
  ]
  edge [
    source 22
    target 1343
  ]
  edge [
    source 22
    target 1344
  ]
  edge [
    source 22
    target 1345
  ]
  edge [
    source 22
    target 1346
  ]
  edge [
    source 22
    target 1347
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 1348
  ]
  edge [
    source 26
    target 1349
  ]
  edge [
    source 26
    target 1350
  ]
  edge [
    source 26
    target 1351
  ]
  edge [
    source 26
    target 1352
  ]
  edge [
    source 26
    target 1353
  ]
  edge [
    source 26
    target 607
  ]
  edge [
    source 26
    target 1354
  ]
  edge [
    source 26
    target 1079
  ]
  edge [
    source 26
    target 1355
  ]
  edge [
    source 26
    target 1356
  ]
  edge [
    source 26
    target 1357
  ]
  edge [
    source 26
    target 1358
  ]
  edge [
    source 26
    target 1359
  ]
  edge [
    source 26
    target 1360
  ]
  edge [
    source 26
    target 1361
  ]
  edge [
    source 26
    target 1362
  ]
  edge [
    source 26
    target 1363
  ]
  edge [
    source 26
    target 1364
  ]
  edge [
    source 26
    target 1365
  ]
  edge [
    source 26
    target 1366
  ]
  edge [
    source 26
    target 1367
  ]
  edge [
    source 26
    target 1368
  ]
  edge [
    source 26
    target 1180
  ]
  edge [
    source 26
    target 1369
  ]
  edge [
    source 26
    target 1370
  ]
  edge [
    source 26
    target 1371
  ]
  edge [
    source 26
    target 72
  ]
  edge [
    source 26
    target 1372
  ]
  edge [
    source 26
    target 1373
  ]
  edge [
    source 26
    target 1374
  ]
  edge [
    source 26
    target 1375
  ]
  edge [
    source 26
    target 599
  ]
  edge [
    source 26
    target 1376
  ]
  edge [
    source 26
    target 1377
  ]
  edge [
    source 26
    target 1378
  ]
  edge [
    source 26
    target 1379
  ]
  edge [
    source 26
    target 1380
  ]
  edge [
    source 26
    target 1098
  ]
  edge [
    source 26
    target 1381
  ]
  edge [
    source 26
    target 1083
  ]
  edge [
    source 26
    target 1382
  ]
  edge [
    source 26
    target 1383
  ]
  edge [
    source 26
    target 446
  ]
  edge [
    source 26
    target 1384
  ]
  edge [
    source 26
    target 1385
  ]
  edge [
    source 26
    target 1386
  ]
  edge [
    source 26
    target 1387
  ]
  edge [
    source 26
    target 1388
  ]
  edge [
    source 26
    target 1389
  ]
  edge [
    source 26
    target 1390
  ]
  edge [
    source 26
    target 1391
  ]
  edge [
    source 26
    target 1392
  ]
  edge [
    source 26
    target 1393
  ]
  edge [
    source 26
    target 1394
  ]
  edge [
    source 26
    target 941
  ]
  edge [
    source 26
    target 1395
  ]
  edge [
    source 26
    target 1396
  ]
  edge [
    source 26
    target 1397
  ]
  edge [
    source 26
    target 1398
  ]
  edge [
    source 26
    target 1399
  ]
  edge [
    source 26
    target 1400
  ]
  edge [
    source 26
    target 1401
  ]
  edge [
    source 26
    target 1402
  ]
  edge [
    source 26
    target 910
  ]
  edge [
    source 26
    target 1403
  ]
  edge [
    source 26
    target 1404
  ]
  edge [
    source 26
    target 1405
  ]
  edge [
    source 26
    target 1406
  ]
  edge [
    source 26
    target 1407
  ]
  edge [
    source 26
    target 1408
  ]
  edge [
    source 26
    target 1409
  ]
  edge [
    source 26
    target 1410
  ]
  edge [
    source 26
    target 1411
  ]
  edge [
    source 26
    target 1412
  ]
  edge [
    source 26
    target 1413
  ]
  edge [
    source 26
    target 332
  ]
  edge [
    source 26
    target 1414
  ]
  edge [
    source 26
    target 1415
  ]
  edge [
    source 26
    target 1416
  ]
  edge [
    source 26
    target 1417
  ]
  edge [
    source 26
    target 1418
  ]
  edge [
    source 26
    target 1419
  ]
  edge [
    source 26
    target 1420
  ]
  edge [
    source 26
    target 1421
  ]
  edge [
    source 26
    target 1422
  ]
  edge [
    source 26
    target 1423
  ]
  edge [
    source 26
    target 918
  ]
  edge [
    source 26
    target 1424
  ]
  edge [
    source 26
    target 1425
  ]
  edge [
    source 26
    target 1426
  ]
  edge [
    source 26
    target 1427
  ]
  edge [
    source 26
    target 1428
  ]
  edge [
    source 26
    target 505
  ]
  edge [
    source 26
    target 1429
  ]
  edge [
    source 26
    target 1430
  ]
  edge [
    source 26
    target 1431
  ]
  edge [
    source 26
    target 1432
  ]
  edge [
    source 26
    target 1433
  ]
  edge [
    source 26
    target 1434
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 1435
  ]
  edge [
    source 29
    target 1436
  ]
  edge [
    source 29
    target 1437
  ]
  edge [
    source 29
    target 1438
  ]
  edge [
    source 29
    target 1439
  ]
  edge [
    source 29
    target 1440
  ]
  edge [
    source 29
    target 1441
  ]
  edge [
    source 29
    target 1442
  ]
  edge [
    source 29
    target 1443
  ]
  edge [
    source 29
    target 1444
  ]
  edge [
    source 29
    target 1445
  ]
  edge [
    source 29
    target 1446
  ]
  edge [
    source 29
    target 1447
  ]
  edge [
    source 29
    target 1448
  ]
  edge [
    source 29
    target 1449
  ]
  edge [
    source 29
    target 1450
  ]
  edge [
    source 29
    target 1451
  ]
  edge [
    source 29
    target 1452
  ]
  edge [
    source 29
    target 1453
  ]
  edge [
    source 29
    target 1454
  ]
  edge [
    source 29
    target 1455
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 1456
  ]
  edge [
    source 30
    target 1457
  ]
  edge [
    source 30
    target 1458
  ]
  edge [
    source 30
    target 1459
  ]
  edge [
    source 30
    target 1460
  ]
  edge [
    source 30
    target 1461
  ]
  edge [
    source 30
    target 1462
  ]
  edge [
    source 30
    target 1463
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 754
  ]
  edge [
    source 31
    target 776
  ]
  edge [
    source 31
    target 737
  ]
  edge [
    source 31
    target 765
  ]
  edge [
    source 31
    target 755
  ]
  edge [
    source 31
    target 1464
  ]
  edge [
    source 31
    target 228
  ]
  edge [
    source 31
    target 1393
  ]
  edge [
    source 31
    target 1465
  ]
  edge [
    source 31
    target 1466
  ]
  edge [
    source 31
    target 1467
  ]
  edge [
    source 31
    target 1468
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 1469
  ]
  edge [
    source 32
    target 1470
  ]
  edge [
    source 32
    target 1471
  ]
  edge [
    source 32
    target 1472
  ]
  edge [
    source 32
    target 1473
  ]
  edge [
    source 32
    target 1474
  ]
  edge [
    source 32
    target 1475
  ]
  edge [
    source 32
    target 1476
  ]
  edge [
    source 32
    target 1477
  ]
  edge [
    source 32
    target 1478
  ]
  edge [
    source 32
    target 1479
  ]
  edge [
    source 32
    target 1480
  ]
  edge [
    source 32
    target 1481
  ]
  edge [
    source 32
    target 1482
  ]
  edge [
    source 32
    target 1483
  ]
  edge [
    source 32
    target 1484
  ]
  edge [
    source 32
    target 1485
  ]
  edge [
    source 32
    target 1486
  ]
  edge [
    source 32
    target 1487
  ]
  edge [
    source 32
    target 1488
  ]
  edge [
    source 32
    target 415
  ]
  edge [
    source 32
    target 1489
  ]
  edge [
    source 32
    target 1490
  ]
  edge [
    source 32
    target 1491
  ]
  edge [
    source 32
    target 1492
  ]
  edge [
    source 32
    target 1493
  ]
  edge [
    source 32
    target 1494
  ]
  edge [
    source 32
    target 1495
  ]
  edge [
    source 32
    target 1496
  ]
  edge [
    source 32
    target 1497
  ]
  edge [
    source 32
    target 1192
  ]
  edge [
    source 32
    target 1498
  ]
  edge [
    source 32
    target 1499
  ]
  edge [
    source 32
    target 1500
  ]
  edge [
    source 32
    target 1501
  ]
  edge [
    source 32
    target 1502
  ]
  edge [
    source 32
    target 1503
  ]
  edge [
    source 32
    target 1504
  ]
  edge [
    source 32
    target 1505
  ]
  edge [
    source 32
    target 709
  ]
  edge [
    source 32
    target 1506
  ]
  edge [
    source 32
    target 1507
  ]
  edge [
    source 32
    target 1508
  ]
  edge [
    source 32
    target 1509
  ]
  edge [
    source 32
    target 1002
  ]
  edge [
    source 32
    target 1510
  ]
  edge [
    source 32
    target 1511
  ]
  edge [
    source 32
    target 1512
  ]
  edge [
    source 32
    target 1513
  ]
  edge [
    source 32
    target 1514
  ]
  edge [
    source 32
    target 737
  ]
  edge [
    source 32
    target 1515
  ]
  edge [
    source 32
    target 1516
  ]
  edge [
    source 32
    target 1517
  ]
  edge [
    source 32
    target 1518
  ]
  edge [
    source 32
    target 687
  ]
  edge [
    source 32
    target 1519
  ]
  edge [
    source 32
    target 1520
  ]
  edge [
    source 32
    target 1521
  ]
  edge [
    source 32
    target 616
  ]
  edge [
    source 32
    target 1522
  ]
  edge [
    source 32
    target 1523
  ]
  edge [
    source 32
    target 1524
  ]
  edge [
    source 32
    target 1525
  ]
  edge [
    source 32
    target 1526
  ]
  edge [
    source 32
    target 1527
  ]
  edge [
    source 32
    target 1528
  ]
  edge [
    source 32
    target 1529
  ]
  edge [
    source 32
    target 1530
  ]
  edge [
    source 32
    target 517
  ]
  edge [
    source 32
    target 805
  ]
  edge [
    source 32
    target 1531
  ]
  edge [
    source 32
    target 1532
  ]
  edge [
    source 32
    target 1533
  ]
  edge [
    source 32
    target 1534
  ]
  edge [
    source 32
    target 1535
  ]
  edge [
    source 32
    target 1536
  ]
  edge [
    source 32
    target 1537
  ]
  edge [
    source 32
    target 1538
  ]
  edge [
    source 32
    target 1539
  ]
  edge [
    source 32
    target 1540
  ]
  edge [
    source 32
    target 1541
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 1542
  ]
  edge [
    source 33
    target 1543
  ]
  edge [
    source 33
    target 1544
  ]
  edge [
    source 33
    target 549
  ]
  edge [
    source 33
    target 1545
  ]
  edge [
    source 33
    target 1546
  ]
  edge [
    source 33
    target 1547
  ]
  edge [
    source 33
    target 1548
  ]
  edge [
    source 33
    target 1549
  ]
  edge [
    source 33
    target 408
  ]
  edge [
    source 33
    target 1550
  ]
  edge [
    source 33
    target 1551
  ]
  edge [
    source 33
    target 1552
  ]
  edge [
    source 33
    target 1553
  ]
  edge [
    source 33
    target 1358
  ]
  edge [
    source 33
    target 1554
  ]
  edge [
    source 33
    target 1555
  ]
  edge [
    source 33
    target 1556
  ]
  edge [
    source 33
    target 1180
  ]
  edge [
    source 33
    target 1557
  ]
  edge [
    source 33
    target 1558
  ]
  edge [
    source 33
    target 485
  ]
  edge [
    source 33
    target 1559
  ]
  edge [
    source 33
    target 1560
  ]
  edge [
    source 33
    target 1561
  ]
  edge [
    source 33
    target 1562
  ]
  edge [
    source 33
    target 1563
  ]
  edge [
    source 33
    target 847
  ]
  edge [
    source 33
    target 1042
  ]
  edge [
    source 33
    target 1564
  ]
  edge [
    source 33
    target 1159
  ]
  edge [
    source 33
    target 1565
  ]
  edge [
    source 33
    target 1566
  ]
  edge [
    source 33
    target 1567
  ]
  edge [
    source 33
    target 1568
  ]
  edge [
    source 33
    target 850
  ]
  edge [
    source 33
    target 1569
  ]
  edge [
    source 33
    target 1570
  ]
  edge [
    source 33
    target 1296
  ]
  edge [
    source 33
    target 1571
  ]
  edge [
    source 33
    target 1572
  ]
  edge [
    source 33
    target 1573
  ]
  edge [
    source 33
    target 1574
  ]
  edge [
    source 33
    target 1575
  ]
  edge [
    source 33
    target 1576
  ]
  edge [
    source 33
    target 461
  ]
  edge [
    source 33
    target 1577
  ]
  edge [
    source 33
    target 1202
  ]
  edge [
    source 33
    target 1578
  ]
  edge [
    source 33
    target 1579
  ]
  edge [
    source 33
    target 860
  ]
  edge [
    source 33
    target 474
  ]
  edge [
    source 33
    target 1580
  ]
  edge [
    source 33
    target 1581
  ]
  edge [
    source 33
    target 1582
  ]
  edge [
    source 33
    target 1583
  ]
  edge [
    source 33
    target 1584
  ]
  edge [
    source 33
    target 1585
  ]
  edge [
    source 33
    target 1586
  ]
  edge [
    source 33
    target 1587
  ]
  edge [
    source 33
    target 1588
  ]
  edge [
    source 33
    target 1589
  ]
  edge [
    source 33
    target 1590
  ]
  edge [
    source 33
    target 1591
  ]
  edge [
    source 33
    target 1592
  ]
  edge [
    source 33
    target 213
  ]
  edge [
    source 33
    target 1593
  ]
  edge [
    source 33
    target 433
  ]
  edge [
    source 33
    target 1278
  ]
  edge [
    source 33
    target 1279
  ]
  edge [
    source 33
    target 1280
  ]
  edge [
    source 33
    target 193
  ]
  edge [
    source 33
    target 1281
  ]
  edge [
    source 33
    target 1282
  ]
  edge [
    source 33
    target 1283
  ]
  edge [
    source 33
    target 1284
  ]
  edge [
    source 33
    target 159
  ]
  edge [
    source 33
    target 1285
  ]
  edge [
    source 33
    target 1286
  ]
  edge [
    source 33
    target 190
  ]
  edge [
    source 33
    target 1594
  ]
  edge [
    source 33
    target 1595
  ]
  edge [
    source 33
    target 1596
  ]
  edge [
    source 33
    target 1597
  ]
  edge [
    source 33
    target 1192
  ]
  edge [
    source 33
    target 1315
  ]
  edge [
    source 33
    target 1598
  ]
  edge [
    source 33
    target 1599
  ]
  edge [
    source 33
    target 1600
  ]
  edge [
    source 33
    target 1601
  ]
  edge [
    source 33
    target 1602
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 1464
  ]
  edge [
    source 33
    target 737
  ]
  edge [
    source 33
    target 228
  ]
  edge [
    source 33
    target 1489
  ]
  edge [
    source 33
    target 305
  ]
  edge [
    source 33
    target 1491
  ]
  edge [
    source 33
    target 1603
  ]
  edge [
    source 33
    target 1604
  ]
  edge [
    source 33
    target 1605
  ]
  edge [
    source 33
    target 406
  ]
  edge [
    source 33
    target 1144
  ]
  edge [
    source 33
    target 1520
  ]
  edge [
    source 33
    target 1606
  ]
  edge [
    source 33
    target 1607
  ]
  edge [
    source 33
    target 1608
  ]
  edge [
    source 33
    target 1609
  ]
  edge [
    source 33
    target 1610
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1611
  ]
  edge [
    source 34
    target 1612
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1613
  ]
  edge [
    source 35
    target 1614
  ]
  edge [
    source 35
    target 1615
  ]
  edge [
    source 35
    target 1616
  ]
  edge [
    source 35
    target 1617
  ]
  edge [
    source 35
    target 1618
  ]
  edge [
    source 35
    target 1619
  ]
  edge [
    source 35
    target 1620
  ]
  edge [
    source 35
    target 1621
  ]
  edge [
    source 35
    target 1622
  ]
  edge [
    source 35
    target 1623
  ]
  edge [
    source 35
    target 1313
  ]
  edge [
    source 35
    target 1624
  ]
  edge [
    source 35
    target 1625
  ]
  edge [
    source 35
    target 398
  ]
  edge [
    source 35
    target 1626
  ]
  edge [
    source 35
    target 1627
  ]
  edge [
    source 35
    target 1628
  ]
  edge [
    source 35
    target 1629
  ]
  edge [
    source 35
    target 1630
  ]
  edge [
    source 35
    target 1631
  ]
  edge [
    source 35
    target 1632
  ]
  edge [
    source 35
    target 1633
  ]
  edge [
    source 35
    target 1634
  ]
  edge [
    source 35
    target 1635
  ]
  edge [
    source 35
    target 1636
  ]
  edge [
    source 35
    target 1637
  ]
  edge [
    source 35
    target 129
  ]
  edge [
    source 35
    target 928
  ]
  edge [
    source 35
    target 1638
  ]
  edge [
    source 35
    target 1639
  ]
  edge [
    source 35
    target 1640
  ]
  edge [
    source 35
    target 1641
  ]
  edge [
    source 35
    target 1642
  ]
  edge [
    source 35
    target 1643
  ]
  edge [
    source 35
    target 1644
  ]
  edge [
    source 35
    target 1645
  ]
  edge [
    source 35
    target 1646
  ]
  edge [
    source 35
    target 401
  ]
  edge [
    source 35
    target 402
  ]
  edge [
    source 35
    target 403
  ]
  edge [
    source 35
    target 404
  ]
  edge [
    source 35
    target 405
  ]
  edge [
    source 35
    target 406
  ]
  edge [
    source 35
    target 407
  ]
  edge [
    source 35
    target 408
  ]
  edge [
    source 35
    target 409
  ]
  edge [
    source 35
    target 410
  ]
  edge [
    source 35
    target 411
  ]
  edge [
    source 35
    target 412
  ]
  edge [
    source 35
    target 413
  ]
  edge [
    source 35
    target 1647
  ]
  edge [
    source 35
    target 192
  ]
  edge [
    source 35
    target 505
  ]
  edge [
    source 35
    target 1648
  ]
  edge [
    source 35
    target 616
  ]
  edge [
    source 35
    target 1649
  ]
  edge [
    source 35
    target 913
  ]
  edge [
    source 35
    target 415
  ]
  edge [
    source 35
    target 1650
  ]
  edge [
    source 35
    target 1651
  ]
  edge [
    source 35
    target 1652
  ]
  edge [
    source 35
    target 1653
  ]
  edge [
    source 35
    target 1654
  ]
  edge [
    source 35
    target 1655
  ]
  edge [
    source 35
    target 608
  ]
  edge [
    source 35
    target 1656
  ]
  edge [
    source 35
    target 1657
  ]
  edge [
    source 35
    target 1658
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 35
    target 1659
  ]
  edge [
    source 35
    target 1660
  ]
  edge [
    source 35
    target 1661
  ]
  edge [
    source 35
    target 1662
  ]
  edge [
    source 35
    target 1663
  ]
  edge [
    source 35
    target 1664
  ]
  edge [
    source 35
    target 1665
  ]
  edge [
    source 35
    target 1666
  ]
  edge [
    source 35
    target 1667
  ]
  edge [
    source 35
    target 1668
  ]
  edge [
    source 35
    target 1320
  ]
  edge [
    source 35
    target 1669
  ]
  edge [
    source 35
    target 1670
  ]
  edge [
    source 35
    target 1671
  ]
  edge [
    source 35
    target 1672
  ]
  edge [
    source 35
    target 1673
  ]
  edge [
    source 35
    target 1674
  ]
  edge [
    source 35
    target 1675
  ]
  edge [
    source 35
    target 1676
  ]
  edge [
    source 35
    target 1677
  ]
  edge [
    source 35
    target 1678
  ]
  edge [
    source 35
    target 1679
  ]
  edge [
    source 35
    target 1680
  ]
  edge [
    source 35
    target 1681
  ]
  edge [
    source 35
    target 1682
  ]
  edge [
    source 35
    target 1683
  ]
  edge [
    source 35
    target 1684
  ]
  edge [
    source 35
    target 1685
  ]
  edge [
    source 35
    target 1686
  ]
  edge [
    source 35
    target 1687
  ]
  edge [
    source 35
    target 1688
  ]
  edge [
    source 35
    target 1689
  ]
  edge [
    source 35
    target 1690
  ]
  edge [
    source 35
    target 1691
  ]
  edge [
    source 35
    target 1692
  ]
  edge [
    source 35
    target 1693
  ]
  edge [
    source 35
    target 1694
  ]
  edge [
    source 35
    target 1695
  ]
  edge [
    source 35
    target 1696
  ]
  edge [
    source 35
    target 1697
  ]
  edge [
    source 35
    target 1698
  ]
  edge [
    source 35
    target 1699
  ]
  edge [
    source 35
    target 599
  ]
  edge [
    source 35
    target 1700
  ]
  edge [
    source 35
    target 1701
  ]
  edge [
    source 35
    target 1702
  ]
  edge [
    source 35
    target 1703
  ]
  edge [
    source 35
    target 1704
  ]
  edge [
    source 35
    target 145
  ]
  edge [
    source 35
    target 1705
  ]
  edge [
    source 35
    target 1706
  ]
  edge [
    source 35
    target 1707
  ]
  edge [
    source 35
    target 1708
  ]
  edge [
    source 35
    target 1709
  ]
  edge [
    source 35
    target 1710
  ]
  edge [
    source 35
    target 1711
  ]
  edge [
    source 35
    target 1712
  ]
  edge [
    source 35
    target 1713
  ]
  edge [
    source 35
    target 1714
  ]
  edge [
    source 35
    target 213
  ]
  edge [
    source 35
    target 1715
  ]
  edge [
    source 35
    target 1716
  ]
  edge [
    source 35
    target 1717
  ]
  edge [
    source 35
    target 1718
  ]
  edge [
    source 35
    target 1719
  ]
  edge [
    source 35
    target 1720
  ]
  edge [
    source 35
    target 1721
  ]
  edge [
    source 35
    target 1722
  ]
  edge [
    source 35
    target 1723
  ]
  edge [
    source 35
    target 1724
  ]
  edge [
    source 35
    target 1725
  ]
  edge [
    source 35
    target 1726
  ]
  edge [
    source 35
    target 805
  ]
  edge [
    source 35
    target 1727
  ]
  edge [
    source 35
    target 1728
  ]
  edge [
    source 35
    target 1729
  ]
  edge [
    source 35
    target 1730
  ]
  edge [
    source 35
    target 1731
  ]
  edge [
    source 35
    target 1732
  ]
  edge [
    source 35
    target 1733
  ]
  edge [
    source 35
    target 1734
  ]
  edge [
    source 35
    target 1144
  ]
  edge [
    source 35
    target 1735
  ]
  edge [
    source 35
    target 1425
  ]
  edge [
    source 35
    target 1736
  ]
  edge [
    source 35
    target 252
  ]
  edge [
    source 35
    target 596
  ]
  edge [
    source 35
    target 1737
  ]
  edge [
    source 35
    target 1738
  ]
  edge [
    source 35
    target 1739
  ]
  edge [
    source 35
    target 1740
  ]
  edge [
    source 35
    target 1741
  ]
  edge [
    source 35
    target 1742
  ]
  edge [
    source 35
    target 1743
  ]
  edge [
    source 35
    target 279
  ]
  edge [
    source 35
    target 1744
  ]
  edge [
    source 35
    target 1745
  ]
  edge [
    source 35
    target 1746
  ]
  edge [
    source 35
    target 1747
  ]
  edge [
    source 35
    target 1748
  ]
  edge [
    source 35
    target 1749
  ]
  edge [
    source 35
    target 1750
  ]
  edge [
    source 35
    target 1751
  ]
  edge [
    source 35
    target 1752
  ]
  edge [
    source 35
    target 1753
  ]
  edge [
    source 35
    target 1754
  ]
  edge [
    source 35
    target 1042
  ]
  edge [
    source 35
    target 1755
  ]
  edge [
    source 35
    target 1192
  ]
  edge [
    source 35
    target 1756
  ]
  edge [
    source 35
    target 1757
  ]
  edge [
    source 35
    target 986
  ]
  edge [
    source 35
    target 1758
  ]
  edge [
    source 35
    target 1759
  ]
  edge [
    source 35
    target 1760
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 1761
  ]
  edge [
    source 36
    target 1552
  ]
  edge [
    source 36
    target 1762
  ]
  edge [
    source 36
    target 1763
  ]
  edge [
    source 36
    target 1358
  ]
  edge [
    source 36
    target 1554
  ]
  edge [
    source 36
    target 1555
  ]
  edge [
    source 36
    target 1556
  ]
  edge [
    source 36
    target 1180
  ]
  edge [
    source 36
    target 1557
  ]
  edge [
    source 36
    target 1558
  ]
  edge [
    source 36
    target 485
  ]
  edge [
    source 36
    target 1764
  ]
  edge [
    source 36
    target 1765
  ]
  edge [
    source 36
    target 1766
  ]
  edge [
    source 36
    target 1767
  ]
  edge [
    source 37
    target 1768
  ]
  edge [
    source 37
    target 1769
  ]
  edge [
    source 37
    target 1770
  ]
  edge [
    source 37
    target 1771
  ]
  edge [
    source 37
    target 213
  ]
  edge [
    source 37
    target 1772
  ]
  edge [
    source 37
    target 1773
  ]
  edge [
    source 37
    target 1774
  ]
  edge [
    source 37
    target 1775
  ]
  edge [
    source 37
    target 1776
  ]
  edge [
    source 37
    target 1777
  ]
  edge [
    source 37
    target 1778
  ]
  edge [
    source 37
    target 1779
  ]
  edge [
    source 37
    target 1780
  ]
  edge [
    source 37
    target 1781
  ]
  edge [
    source 37
    target 1782
  ]
  edge [
    source 37
    target 1783
  ]
  edge [
    source 37
    target 1784
  ]
  edge [
    source 37
    target 1785
  ]
  edge [
    source 37
    target 1786
  ]
  edge [
    source 37
    target 1787
  ]
  edge [
    source 37
    target 1788
  ]
  edge [
    source 37
    target 1789
  ]
  edge [
    source 37
    target 1790
  ]
  edge [
    source 37
    target 1791
  ]
  edge [
    source 37
    target 1792
  ]
  edge [
    source 37
    target 1793
  ]
  edge [
    source 37
    target 1794
  ]
  edge [
    source 37
    target 1795
  ]
  edge [
    source 37
    target 1796
  ]
  edge [
    source 37
    target 1797
  ]
  edge [
    source 37
    target 1798
  ]
  edge [
    source 37
    target 1799
  ]
  edge [
    source 37
    target 1800
  ]
  edge [
    source 37
    target 1801
  ]
  edge [
    source 37
    target 1802
  ]
  edge [
    source 37
    target 1803
  ]
  edge [
    source 37
    target 1804
  ]
  edge [
    source 37
    target 1805
  ]
  edge [
    source 37
    target 1806
  ]
  edge [
    source 37
    target 1807
  ]
  edge [
    source 37
    target 1808
  ]
  edge [
    source 37
    target 1809
  ]
  edge [
    source 37
    target 1810
  ]
  edge [
    source 37
    target 1811
  ]
  edge [
    source 37
    target 1812
  ]
  edge [
    source 37
    target 1813
  ]
  edge [
    source 37
    target 1814
  ]
  edge [
    source 37
    target 1815
  ]
  edge [
    source 37
    target 1816
  ]
  edge [
    source 37
    target 1817
  ]
  edge [
    source 37
    target 1818
  ]
  edge [
    source 37
    target 1819
  ]
  edge [
    source 37
    target 1820
  ]
]
