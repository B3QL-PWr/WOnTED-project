graph [
  node [
    id 0
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 2
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "przedszkole"
    origin "text"
  ]
  node [
    id 4
    label "dok&#322;adnie"
  ]
  node [
    id 5
    label "punctiliously"
  ]
  node [
    id 6
    label "meticulously"
  ]
  node [
    id 7
    label "precyzyjnie"
  ]
  node [
    id 8
    label "dok&#322;adny"
  ]
  node [
    id 9
    label "rzetelnie"
  ]
  node [
    id 10
    label "post&#261;pi&#263;"
  ]
  node [
    id 11
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 12
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 13
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 14
    label "zorganizowa&#263;"
  ]
  node [
    id 15
    label "appoint"
  ]
  node [
    id 16
    label "wystylizowa&#263;"
  ]
  node [
    id 17
    label "cause"
  ]
  node [
    id 18
    label "przerobi&#263;"
  ]
  node [
    id 19
    label "nabra&#263;"
  ]
  node [
    id 20
    label "make"
  ]
  node [
    id 21
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 22
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 23
    label "wydali&#263;"
  ]
  node [
    id 24
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 25
    label "advance"
  ]
  node [
    id 26
    label "act"
  ]
  node [
    id 27
    label "see"
  ]
  node [
    id 28
    label "usun&#261;&#263;"
  ]
  node [
    id 29
    label "sack"
  ]
  node [
    id 30
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 31
    label "restore"
  ]
  node [
    id 32
    label "dostosowa&#263;"
  ]
  node [
    id 33
    label "pozyska&#263;"
  ]
  node [
    id 34
    label "stworzy&#263;"
  ]
  node [
    id 35
    label "plan"
  ]
  node [
    id 36
    label "stage"
  ]
  node [
    id 37
    label "urobi&#263;"
  ]
  node [
    id 38
    label "ensnare"
  ]
  node [
    id 39
    label "wprowadzi&#263;"
  ]
  node [
    id 40
    label "zaplanowa&#263;"
  ]
  node [
    id 41
    label "przygotowa&#263;"
  ]
  node [
    id 42
    label "standard"
  ]
  node [
    id 43
    label "skupi&#263;"
  ]
  node [
    id 44
    label "podbi&#263;"
  ]
  node [
    id 45
    label "umocni&#263;"
  ]
  node [
    id 46
    label "doprowadzi&#263;"
  ]
  node [
    id 47
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 48
    label "zadowoli&#263;"
  ]
  node [
    id 49
    label "accommodate"
  ]
  node [
    id 50
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 51
    label "zabezpieczy&#263;"
  ]
  node [
    id 52
    label "wytworzy&#263;"
  ]
  node [
    id 53
    label "pomy&#347;le&#263;"
  ]
  node [
    id 54
    label "woda"
  ]
  node [
    id 55
    label "hoax"
  ]
  node [
    id 56
    label "deceive"
  ]
  node [
    id 57
    label "oszwabi&#263;"
  ]
  node [
    id 58
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 59
    label "objecha&#263;"
  ]
  node [
    id 60
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 61
    label "gull"
  ]
  node [
    id 62
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 63
    label "wzi&#261;&#263;"
  ]
  node [
    id 64
    label "naby&#263;"
  ]
  node [
    id 65
    label "fraud"
  ]
  node [
    id 66
    label "kupi&#263;"
  ]
  node [
    id 67
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 68
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 69
    label "zaliczy&#263;"
  ]
  node [
    id 70
    label "overwork"
  ]
  node [
    id 71
    label "zamieni&#263;"
  ]
  node [
    id 72
    label "zmodyfikowa&#263;"
  ]
  node [
    id 73
    label "change"
  ]
  node [
    id 74
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 75
    label "przej&#347;&#263;"
  ]
  node [
    id 76
    label "zmieni&#263;"
  ]
  node [
    id 77
    label "convert"
  ]
  node [
    id 78
    label "prze&#380;y&#263;"
  ]
  node [
    id 79
    label "przetworzy&#263;"
  ]
  node [
    id 80
    label "upora&#263;_si&#281;"
  ]
  node [
    id 81
    label "stylize"
  ]
  node [
    id 82
    label "nada&#263;"
  ]
  node [
    id 83
    label "upodobni&#263;"
  ]
  node [
    id 84
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 85
    label "sprawi&#263;"
  ]
  node [
    id 86
    label "farsa"
  ]
  node [
    id 87
    label "dziecinada"
  ]
  node [
    id 88
    label "siedziba"
  ]
  node [
    id 89
    label "grupa"
  ]
  node [
    id 90
    label "warunki"
  ]
  node [
    id 91
    label "plac_zabaw"
  ]
  node [
    id 92
    label "plac&#243;wka_opieku&#324;czo-wychowawcza"
  ]
  node [
    id 93
    label "stopek"
  ]
  node [
    id 94
    label "cyrk"
  ]
  node [
    id 95
    label "&#321;ubianka"
  ]
  node [
    id 96
    label "miejsce_pracy"
  ]
  node [
    id 97
    label "dzia&#322;_personalny"
  ]
  node [
    id 98
    label "Kreml"
  ]
  node [
    id 99
    label "Bia&#322;y_Dom"
  ]
  node [
    id 100
    label "budynek"
  ]
  node [
    id 101
    label "miejsce"
  ]
  node [
    id 102
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 103
    label "sadowisko"
  ]
  node [
    id 104
    label "wolty&#380;erka"
  ]
  node [
    id 105
    label "repryza"
  ]
  node [
    id 106
    label "ekwilibrystyka"
  ]
  node [
    id 107
    label "tresura"
  ]
  node [
    id 108
    label "nied&#378;wiednik"
  ]
  node [
    id 109
    label "skandal"
  ]
  node [
    id 110
    label "instytucja"
  ]
  node [
    id 111
    label "hipodrom"
  ]
  node [
    id 112
    label "przedstawienie"
  ]
  node [
    id 113
    label "namiot"
  ]
  node [
    id 114
    label "circus"
  ]
  node [
    id 115
    label "heca"
  ]
  node [
    id 116
    label "arena"
  ]
  node [
    id 117
    label "klownada"
  ]
  node [
    id 118
    label "akrobacja"
  ]
  node [
    id 119
    label "amfiteatr"
  ]
  node [
    id 120
    label "trybuna"
  ]
  node [
    id 121
    label "gag"
  ]
  node [
    id 122
    label "parody"
  ]
  node [
    id 123
    label "sytuacja"
  ]
  node [
    id 124
    label "komedia"
  ]
  node [
    id 125
    label "status"
  ]
  node [
    id 126
    label "odm&#322;adzanie"
  ]
  node [
    id 127
    label "liga"
  ]
  node [
    id 128
    label "jednostka_systematyczna"
  ]
  node [
    id 129
    label "asymilowanie"
  ]
  node [
    id 130
    label "gromada"
  ]
  node [
    id 131
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 132
    label "asymilowa&#263;"
  ]
  node [
    id 133
    label "egzemplarz"
  ]
  node [
    id 134
    label "Entuzjastki"
  ]
  node [
    id 135
    label "zbi&#243;r"
  ]
  node [
    id 136
    label "kompozycja"
  ]
  node [
    id 137
    label "Terranie"
  ]
  node [
    id 138
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 139
    label "category"
  ]
  node [
    id 140
    label "pakiet_klimatyczny"
  ]
  node [
    id 141
    label "oddzia&#322;"
  ]
  node [
    id 142
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 143
    label "cz&#261;steczka"
  ]
  node [
    id 144
    label "stage_set"
  ]
  node [
    id 145
    label "type"
  ]
  node [
    id 146
    label "specgrupa"
  ]
  node [
    id 147
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 148
    label "&#346;wietliki"
  ]
  node [
    id 149
    label "odm&#322;odzenie"
  ]
  node [
    id 150
    label "Eurogrupa"
  ]
  node [
    id 151
    label "odm&#322;adza&#263;"
  ]
  node [
    id 152
    label "formacja_geologiczna"
  ]
  node [
    id 153
    label "harcerze_starsi"
  ]
  node [
    id 154
    label "stra&#380;nik"
  ]
  node [
    id 155
    label "szko&#322;a"
  ]
  node [
    id 156
    label "opiekun"
  ]
  node [
    id 157
    label "ruch"
  ]
  node [
    id 158
    label "childhood"
  ]
  node [
    id 159
    label "zachowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
]
