graph [
  node [
    id 0
    label "swobodny"
    origin "text"
  ]
  node [
    id 1
    label "przep&#322;yw"
    origin "text"
  ]
  node [
    id 2
    label "kapita&#322;"
    origin "text"
  ]
  node [
    id 3
    label "naturalny"
  ]
  node [
    id 4
    label "bezpruderyjny"
  ]
  node [
    id 5
    label "dowolny"
  ]
  node [
    id 6
    label "wygodny"
  ]
  node [
    id 7
    label "niezale&#380;ny"
  ]
  node [
    id 8
    label "swobodnie"
  ]
  node [
    id 9
    label "wolnie"
  ]
  node [
    id 10
    label "leniwy"
  ]
  node [
    id 11
    label "dogodnie"
  ]
  node [
    id 12
    label "wygodnie"
  ]
  node [
    id 13
    label "odpowiedni"
  ]
  node [
    id 14
    label "przyjemny"
  ]
  node [
    id 15
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 16
    label "usamodzielnienie"
  ]
  node [
    id 17
    label "usamodzielnianie"
  ]
  node [
    id 18
    label "niezale&#380;nie"
  ]
  node [
    id 19
    label "szczery"
  ]
  node [
    id 20
    label "prawy"
  ]
  node [
    id 21
    label "zrozumia&#322;y"
  ]
  node [
    id 22
    label "immanentny"
  ]
  node [
    id 23
    label "zwyczajny"
  ]
  node [
    id 24
    label "bezsporny"
  ]
  node [
    id 25
    label "organicznie"
  ]
  node [
    id 26
    label "pierwotny"
  ]
  node [
    id 27
    label "neutralny"
  ]
  node [
    id 28
    label "normalny"
  ]
  node [
    id 29
    label "rzeczywisty"
  ]
  node [
    id 30
    label "naturalnie"
  ]
  node [
    id 31
    label "uwalnianie"
  ]
  node [
    id 32
    label "uwolnienie"
  ]
  node [
    id 33
    label "dowolnie"
  ]
  node [
    id 34
    label "nieskr&#281;powany"
  ]
  node [
    id 35
    label "bezpruderyjnie"
  ]
  node [
    id 36
    label "&#347;mia&#322;y"
  ]
  node [
    id 37
    label "wolny"
  ]
  node [
    id 38
    label "wolno"
  ]
  node [
    id 39
    label "lu&#378;no"
  ]
  node [
    id 40
    label "obieg"
  ]
  node [
    id 41
    label "ruch"
  ]
  node [
    id 42
    label "flux"
  ]
  node [
    id 43
    label "mechanika"
  ]
  node [
    id 44
    label "utrzymywanie"
  ]
  node [
    id 45
    label "move"
  ]
  node [
    id 46
    label "poruszenie"
  ]
  node [
    id 47
    label "movement"
  ]
  node [
    id 48
    label "myk"
  ]
  node [
    id 49
    label "utrzyma&#263;"
  ]
  node [
    id 50
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 51
    label "zjawisko"
  ]
  node [
    id 52
    label "utrzymanie"
  ]
  node [
    id 53
    label "travel"
  ]
  node [
    id 54
    label "kanciasty"
  ]
  node [
    id 55
    label "commercial_enterprise"
  ]
  node [
    id 56
    label "model"
  ]
  node [
    id 57
    label "strumie&#324;"
  ]
  node [
    id 58
    label "proces"
  ]
  node [
    id 59
    label "aktywno&#347;&#263;"
  ]
  node [
    id 60
    label "kr&#243;tki"
  ]
  node [
    id 61
    label "taktyka"
  ]
  node [
    id 62
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 63
    label "apraksja"
  ]
  node [
    id 64
    label "natural_process"
  ]
  node [
    id 65
    label "utrzymywa&#263;"
  ]
  node [
    id 66
    label "d&#322;ugi"
  ]
  node [
    id 67
    label "wydarzenie"
  ]
  node [
    id 68
    label "dyssypacja_energii"
  ]
  node [
    id 69
    label "tumult"
  ]
  node [
    id 70
    label "stopek"
  ]
  node [
    id 71
    label "czynno&#347;&#263;"
  ]
  node [
    id 72
    label "zmiana"
  ]
  node [
    id 73
    label "manewr"
  ]
  node [
    id 74
    label "lokomocja"
  ]
  node [
    id 75
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 76
    label "komunikacja"
  ]
  node [
    id 77
    label "drift"
  ]
  node [
    id 78
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 79
    label "tour"
  ]
  node [
    id 80
    label "circulation"
  ]
  node [
    id 81
    label "obecno&#347;&#263;"
  ]
  node [
    id 82
    label "absolutorium"
  ]
  node [
    id 83
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 84
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 85
    label "&#347;rodowisko"
  ]
  node [
    id 86
    label "nap&#322;ywanie"
  ]
  node [
    id 87
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 88
    label "zaleta"
  ]
  node [
    id 89
    label "mienie"
  ]
  node [
    id 90
    label "podupada&#263;"
  ]
  node [
    id 91
    label "podupadanie"
  ]
  node [
    id 92
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 93
    label "kwestor"
  ]
  node [
    id 94
    label "zas&#243;b"
  ]
  node [
    id 95
    label "supernadz&#243;r"
  ]
  node [
    id 96
    label "uruchomienie"
  ]
  node [
    id 97
    label "uruchamia&#263;"
  ]
  node [
    id 98
    label "kapitalista"
  ]
  node [
    id 99
    label "uruchamianie"
  ]
  node [
    id 100
    label "czynnik_produkcji"
  ]
  node [
    id 101
    label "przej&#347;cie"
  ]
  node [
    id 102
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 103
    label "rodowo&#347;&#263;"
  ]
  node [
    id 104
    label "patent"
  ]
  node [
    id 105
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 106
    label "dobra"
  ]
  node [
    id 107
    label "stan"
  ]
  node [
    id 108
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 109
    label "przej&#347;&#263;"
  ]
  node [
    id 110
    label "possession"
  ]
  node [
    id 111
    label "warto&#347;&#263;"
  ]
  node [
    id 112
    label "zrewaluowa&#263;"
  ]
  node [
    id 113
    label "rewaluowanie"
  ]
  node [
    id 114
    label "korzy&#347;&#263;"
  ]
  node [
    id 115
    label "zrewaluowanie"
  ]
  node [
    id 116
    label "rewaluowa&#263;"
  ]
  node [
    id 117
    label "wabik"
  ]
  node [
    id 118
    label "strona"
  ]
  node [
    id 119
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 120
    label "ilo&#347;&#263;"
  ]
  node [
    id 121
    label "zbi&#243;r"
  ]
  node [
    id 122
    label "class"
  ]
  node [
    id 123
    label "zesp&#243;&#322;"
  ]
  node [
    id 124
    label "obiekt_naturalny"
  ]
  node [
    id 125
    label "otoczenie"
  ]
  node [
    id 126
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 127
    label "environment"
  ]
  node [
    id 128
    label "rzecz"
  ]
  node [
    id 129
    label "huczek"
  ]
  node [
    id 130
    label "ekosystem"
  ]
  node [
    id 131
    label "wszechstworzenie"
  ]
  node [
    id 132
    label "grupa"
  ]
  node [
    id 133
    label "woda"
  ]
  node [
    id 134
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 135
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 136
    label "teren"
  ]
  node [
    id 137
    label "mikrokosmos"
  ]
  node [
    id 138
    label "stw&#243;r"
  ]
  node [
    id 139
    label "warunki"
  ]
  node [
    id 140
    label "Ziemia"
  ]
  node [
    id 141
    label "fauna"
  ]
  node [
    id 142
    label "biota"
  ]
  node [
    id 143
    label "begin"
  ]
  node [
    id 144
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 145
    label "zaczyna&#263;"
  ]
  node [
    id 146
    label "urz&#281;dnik"
  ]
  node [
    id 147
    label "ksi&#281;gowy"
  ]
  node [
    id 148
    label "kwestura"
  ]
  node [
    id 149
    label "Katon"
  ]
  node [
    id 150
    label "polityk"
  ]
  node [
    id 151
    label "decline"
  ]
  node [
    id 152
    label "traci&#263;"
  ]
  node [
    id 153
    label "fall"
  ]
  node [
    id 154
    label "s&#322;abn&#261;&#263;"
  ]
  node [
    id 155
    label "graduation"
  ]
  node [
    id 156
    label "uko&#324;czenie"
  ]
  node [
    id 157
    label "ocena"
  ]
  node [
    id 158
    label "powodowanie"
  ]
  node [
    id 159
    label "w&#322;&#261;czanie"
  ]
  node [
    id 160
    label "robienie"
  ]
  node [
    id 161
    label "zaczynanie"
  ]
  node [
    id 162
    label "funkcjonowanie"
  ]
  node [
    id 163
    label "upadanie"
  ]
  node [
    id 164
    label "s&#322;abni&#281;cie"
  ]
  node [
    id 165
    label "shoot"
  ]
  node [
    id 166
    label "pour"
  ]
  node [
    id 167
    label "dane"
  ]
  node [
    id 168
    label "zasila&#263;"
  ]
  node [
    id 169
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 170
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 171
    label "meet"
  ]
  node [
    id 172
    label "dociera&#263;"
  ]
  node [
    id 173
    label "zbiera&#263;_si&#281;"
  ]
  node [
    id 174
    label "wzbiera&#263;"
  ]
  node [
    id 175
    label "ogarnia&#263;"
  ]
  node [
    id 176
    label "wype&#322;nia&#263;"
  ]
  node [
    id 177
    label "gromadzenie_si&#281;"
  ]
  node [
    id 178
    label "zbieranie_si&#281;"
  ]
  node [
    id 179
    label "zasilanie"
  ]
  node [
    id 180
    label "docieranie"
  ]
  node [
    id 181
    label "t&#281;&#380;enie"
  ]
  node [
    id 182
    label "nawiewanie"
  ]
  node [
    id 183
    label "nadmuchanie"
  ]
  node [
    id 184
    label "ogarnianie"
  ]
  node [
    id 185
    label "zasilenie"
  ]
  node [
    id 186
    label "zgromadzenie_si&#281;"
  ]
  node [
    id 187
    label "opanowanie"
  ]
  node [
    id 188
    label "zebranie_si&#281;"
  ]
  node [
    id 189
    label "dotarcie"
  ]
  node [
    id 190
    label "nasilenie_si&#281;"
  ]
  node [
    id 191
    label "bulge"
  ]
  node [
    id 192
    label "bankowo&#347;&#263;"
  ]
  node [
    id 193
    label "nadz&#243;r"
  ]
  node [
    id 194
    label "spowodowanie"
  ]
  node [
    id 195
    label "zacz&#281;cie"
  ]
  node [
    id 196
    label "w&#322;&#261;czenie"
  ]
  node [
    id 197
    label "propulsion"
  ]
  node [
    id 198
    label "zrobienie"
  ]
  node [
    id 199
    label "wype&#322;ni&#263;"
  ]
  node [
    id 200
    label "mount"
  ]
  node [
    id 201
    label "zasili&#263;"
  ]
  node [
    id 202
    label "wax"
  ]
  node [
    id 203
    label "dotrze&#263;"
  ]
  node [
    id 204
    label "zebra&#263;_si&#281;"
  ]
  node [
    id 205
    label "zgromadzi&#263;_si&#281;"
  ]
  node [
    id 206
    label "rise"
  ]
  node [
    id 207
    label "ogarn&#261;&#263;"
  ]
  node [
    id 208
    label "saddle_horse"
  ]
  node [
    id 209
    label "wezbra&#263;"
  ]
  node [
    id 210
    label "bogacz"
  ]
  node [
    id 211
    label "przedsi&#281;biorca"
  ]
  node [
    id 212
    label "cz&#322;owiek"
  ]
  node [
    id 213
    label "industrializm"
  ]
  node [
    id 214
    label "rentier"
  ]
  node [
    id 215
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 216
    label "finansjera"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
]
