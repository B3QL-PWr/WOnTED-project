graph [
  node [
    id 0
    label "lenno"
    origin "text"
  ]
  node [
    id 1
    label "w&#322;ochy"
    origin "text"
  ]
  node [
    id 2
    label "serfdom"
  ]
  node [
    id 3
    label "feudalizm"
  ]
  node [
    id 4
    label "podleg&#322;o&#347;&#263;"
  ]
  node [
    id 5
    label "maj&#261;tek"
  ]
  node [
    id 6
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 7
    label "przej&#347;cie"
  ]
  node [
    id 8
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 9
    label "rodowo&#347;&#263;"
  ]
  node [
    id 10
    label "kwota"
  ]
  node [
    id 11
    label "Wilko"
  ]
  node [
    id 12
    label "patent"
  ]
  node [
    id 13
    label "mienie"
  ]
  node [
    id 14
    label "frymark"
  ]
  node [
    id 15
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 16
    label "dobra"
  ]
  node [
    id 17
    label "stan"
  ]
  node [
    id 18
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 19
    label "przej&#347;&#263;"
  ]
  node [
    id 20
    label "possession"
  ]
  node [
    id 21
    label "rezultat"
  ]
  node [
    id 22
    label "podporz&#261;dkowanie"
  ]
  node [
    id 23
    label "niesamodzielno&#347;&#263;"
  ]
  node [
    id 24
    label "feodalizm"
  ]
  node [
    id 25
    label "feuda&#322;"
  ]
  node [
    id 26
    label "senioria"
  ]
  node [
    id 27
    label "dominium"
  ]
  node [
    id 28
    label "feudalism"
  ]
  node [
    id 29
    label "system_ekonomiczny"
  ]
  node [
    id 30
    label "prawo_wychodu"
  ]
  node [
    id 31
    label "ustr&#243;j"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
]
