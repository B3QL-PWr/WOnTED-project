graph [
  node [
    id 0
    label "przenosi&#263;"
    origin "text"
  ]
  node [
    id 1
    label "baza"
    origin "text"
  ]
  node [
    id 2
    label "access"
    origin "text"
  ]
  node [
    id 3
    label "mysql"
    origin "text"
  ]
  node [
    id 4
    label "kopiowa&#263;"
  ]
  node [
    id 5
    label "dostosowywa&#263;"
  ]
  node [
    id 6
    label "estrange"
  ]
  node [
    id 7
    label "ponosi&#263;"
  ]
  node [
    id 8
    label "rozpowszechnia&#263;"
  ]
  node [
    id 9
    label "transfer"
  ]
  node [
    id 10
    label "move"
  ]
  node [
    id 11
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 12
    label "go"
  ]
  node [
    id 13
    label "circulate"
  ]
  node [
    id 14
    label "pocisk"
  ]
  node [
    id 15
    label "przemieszcza&#263;"
  ]
  node [
    id 16
    label "zmienia&#263;"
  ]
  node [
    id 17
    label "wytrzyma&#263;"
  ]
  node [
    id 18
    label "umieszcza&#263;"
  ]
  node [
    id 19
    label "przelatywa&#263;"
  ]
  node [
    id 20
    label "infest"
  ]
  node [
    id 21
    label "strzela&#263;"
  ]
  node [
    id 22
    label "translokowa&#263;"
  ]
  node [
    id 23
    label "powodowa&#263;"
  ]
  node [
    id 24
    label "robi&#263;"
  ]
  node [
    id 25
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 26
    label "wytwarza&#263;"
  ]
  node [
    id 27
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 28
    label "transcribe"
  ]
  node [
    id 29
    label "pirat"
  ]
  node [
    id 30
    label "mock"
  ]
  node [
    id 31
    label "lecie&#263;"
  ]
  node [
    id 32
    label "bra&#263;"
  ]
  node [
    id 33
    label "biec"
  ]
  node [
    id 34
    label "przebywa&#263;"
  ]
  node [
    id 35
    label "przedostawa&#263;_si&#281;"
  ]
  node [
    id 36
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 37
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 38
    label "tent-fly"
  ]
  node [
    id 39
    label "przebiega&#263;"
  ]
  node [
    id 40
    label "mija&#263;"
  ]
  node [
    id 41
    label "authorize"
  ]
  node [
    id 42
    label "sprawia&#263;"
  ]
  node [
    id 43
    label "generalize"
  ]
  node [
    id 44
    label "rozszerza&#263;"
  ]
  node [
    id 45
    label "exsert"
  ]
  node [
    id 46
    label "traci&#263;"
  ]
  node [
    id 47
    label "alternate"
  ]
  node [
    id 48
    label "change"
  ]
  node [
    id 49
    label "reengineering"
  ]
  node [
    id 50
    label "zast&#281;powa&#263;"
  ]
  node [
    id 51
    label "zyskiwa&#263;"
  ]
  node [
    id 52
    label "przechodzi&#263;"
  ]
  node [
    id 53
    label "kierowa&#263;"
  ]
  node [
    id 54
    label "plu&#263;"
  ]
  node [
    id 55
    label "train"
  ]
  node [
    id 56
    label "walczy&#263;"
  ]
  node [
    id 57
    label "napierdziela&#263;"
  ]
  node [
    id 58
    label "fight"
  ]
  node [
    id 59
    label "snap"
  ]
  node [
    id 60
    label "uderza&#263;"
  ]
  node [
    id 61
    label "odpala&#263;"
  ]
  node [
    id 62
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 63
    label "equal"
  ]
  node [
    id 64
    label "zmusi&#263;"
  ]
  node [
    id 65
    label "digest"
  ]
  node [
    id 66
    label "pozosta&#263;"
  ]
  node [
    id 67
    label "proceed"
  ]
  node [
    id 68
    label "wst&#281;powa&#263;"
  ]
  node [
    id 69
    label "mie&#263;_miejsce"
  ]
  node [
    id 70
    label "str&#243;j"
  ]
  node [
    id 71
    label "hurt"
  ]
  node [
    id 72
    label "make"
  ]
  node [
    id 73
    label "bolt"
  ]
  node [
    id 74
    label "odci&#261;ga&#263;"
  ]
  node [
    id 75
    label "doznawa&#263;"
  ]
  node [
    id 76
    label "plasowa&#263;"
  ]
  node [
    id 77
    label "umie&#347;ci&#263;"
  ]
  node [
    id 78
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 79
    label "pomieszcza&#263;"
  ]
  node [
    id 80
    label "accommodate"
  ]
  node [
    id 81
    label "venture"
  ]
  node [
    id 82
    label "wpiernicza&#263;"
  ]
  node [
    id 83
    label "okre&#347;la&#263;"
  ]
  node [
    id 84
    label "goban"
  ]
  node [
    id 85
    label "gra_planszowa"
  ]
  node [
    id 86
    label "sport_umys&#322;owy"
  ]
  node [
    id 87
    label "chi&#324;ski"
  ]
  node [
    id 88
    label "ilo&#347;&#263;"
  ]
  node [
    id 89
    label "przekaz"
  ]
  node [
    id 90
    label "zamiana"
  ]
  node [
    id 91
    label "release"
  ]
  node [
    id 92
    label "lista_transferowa"
  ]
  node [
    id 93
    label "amunicja"
  ]
  node [
    id 94
    label "g&#322;owica"
  ]
  node [
    id 95
    label "trafienie"
  ]
  node [
    id 96
    label "trafianie"
  ]
  node [
    id 97
    label "kulka"
  ]
  node [
    id 98
    label "rdze&#324;"
  ]
  node [
    id 99
    label "prochownia"
  ]
  node [
    id 100
    label "przeniesienie"
  ]
  node [
    id 101
    label "&#322;adunek_bojowy"
  ]
  node [
    id 102
    label "trafi&#263;"
  ]
  node [
    id 103
    label "przenoszenie"
  ]
  node [
    id 104
    label "przenie&#347;&#263;"
  ]
  node [
    id 105
    label "trafia&#263;"
  ]
  node [
    id 106
    label "bro&#324;"
  ]
  node [
    id 107
    label "rekord"
  ]
  node [
    id 108
    label "poj&#281;cie"
  ]
  node [
    id 109
    label "base"
  ]
  node [
    id 110
    label "stacjonowanie"
  ]
  node [
    id 111
    label "documentation"
  ]
  node [
    id 112
    label "pole"
  ]
  node [
    id 113
    label "zbi&#243;r"
  ]
  node [
    id 114
    label "zasadzi&#263;"
  ]
  node [
    id 115
    label "zasadzenie"
  ]
  node [
    id 116
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 117
    label "miejsce"
  ]
  node [
    id 118
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 119
    label "podstawowy"
  ]
  node [
    id 120
    label "baseball"
  ]
  node [
    id 121
    label "kolumna"
  ]
  node [
    id 122
    label "kosmetyk"
  ]
  node [
    id 123
    label "za&#322;o&#380;enie"
  ]
  node [
    id 124
    label "punkt_odniesienia"
  ]
  node [
    id 125
    label "boisko"
  ]
  node [
    id 126
    label "system_bazy_danych"
  ]
  node [
    id 127
    label "informatyka"
  ]
  node [
    id 128
    label "podstawa"
  ]
  node [
    id 129
    label "podwini&#281;cie"
  ]
  node [
    id 130
    label "zap&#322;acenie"
  ]
  node [
    id 131
    label "przyodzianie"
  ]
  node [
    id 132
    label "budowla"
  ]
  node [
    id 133
    label "pokrycie"
  ]
  node [
    id 134
    label "rozebranie"
  ]
  node [
    id 135
    label "zak&#322;adka"
  ]
  node [
    id 136
    label "struktura"
  ]
  node [
    id 137
    label "poubieranie"
  ]
  node [
    id 138
    label "infliction"
  ]
  node [
    id 139
    label "spowodowanie"
  ]
  node [
    id 140
    label "pozak&#322;adanie"
  ]
  node [
    id 141
    label "program"
  ]
  node [
    id 142
    label "przebranie"
  ]
  node [
    id 143
    label "przywdzianie"
  ]
  node [
    id 144
    label "obleczenie_si&#281;"
  ]
  node [
    id 145
    label "utworzenie"
  ]
  node [
    id 146
    label "twierdzenie"
  ]
  node [
    id 147
    label "obleczenie"
  ]
  node [
    id 148
    label "umieszczenie"
  ]
  node [
    id 149
    label "czynno&#347;&#263;"
  ]
  node [
    id 150
    label "przygotowywanie"
  ]
  node [
    id 151
    label "przymierzenie"
  ]
  node [
    id 152
    label "wyko&#324;czenie"
  ]
  node [
    id 153
    label "point"
  ]
  node [
    id 154
    label "przygotowanie"
  ]
  node [
    id 155
    label "proposition"
  ]
  node [
    id 156
    label "przewidzenie"
  ]
  node [
    id 157
    label "zrobienie"
  ]
  node [
    id 158
    label "preparat"
  ]
  node [
    id 159
    label "cosmetic"
  ]
  node [
    id 160
    label "olejek"
  ]
  node [
    id 161
    label "warunek_lokalowy"
  ]
  node [
    id 162
    label "plac"
  ]
  node [
    id 163
    label "location"
  ]
  node [
    id 164
    label "uwaga"
  ]
  node [
    id 165
    label "przestrze&#324;"
  ]
  node [
    id 166
    label "status"
  ]
  node [
    id 167
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 168
    label "chwila"
  ]
  node [
    id 169
    label "cia&#322;o"
  ]
  node [
    id 170
    label "cecha"
  ]
  node [
    id 171
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 172
    label "praca"
  ]
  node [
    id 173
    label "rz&#261;d"
  ]
  node [
    id 174
    label "egzemplarz"
  ]
  node [
    id 175
    label "series"
  ]
  node [
    id 176
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 177
    label "uprawianie"
  ]
  node [
    id 178
    label "praca_rolnicza"
  ]
  node [
    id 179
    label "collection"
  ]
  node [
    id 180
    label "dane"
  ]
  node [
    id 181
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 182
    label "pakiet_klimatyczny"
  ]
  node [
    id 183
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 184
    label "sum"
  ]
  node [
    id 185
    label "gathering"
  ]
  node [
    id 186
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 187
    label "album"
  ]
  node [
    id 188
    label "pot&#281;ga"
  ]
  node [
    id 189
    label "przedmiot"
  ]
  node [
    id 190
    label "column"
  ]
  node [
    id 191
    label "bok"
  ]
  node [
    id 192
    label "d&#243;&#322;"
  ]
  node [
    id 193
    label "dzieci&#281;ctwo"
  ]
  node [
    id 194
    label "background"
  ]
  node [
    id 195
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 196
    label "strategia"
  ]
  node [
    id 197
    label "pomys&#322;"
  ]
  node [
    id 198
    label "&#347;ciana"
  ]
  node [
    id 199
    label "pos&#322;uchanie"
  ]
  node [
    id 200
    label "skumanie"
  ]
  node [
    id 201
    label "orientacja"
  ]
  node [
    id 202
    label "wytw&#243;r"
  ]
  node [
    id 203
    label "teoria"
  ]
  node [
    id 204
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 205
    label "clasp"
  ]
  node [
    id 206
    label "przem&#243;wienie"
  ]
  node [
    id 207
    label "forma"
  ]
  node [
    id 208
    label "zorientowanie"
  ]
  node [
    id 209
    label "niezaawansowany"
  ]
  node [
    id 210
    label "najwa&#380;niejszy"
  ]
  node [
    id 211
    label "pocz&#261;tkowy"
  ]
  node [
    id 212
    label "podstawowo"
  ]
  node [
    id 213
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 214
    label "establish"
  ]
  node [
    id 215
    label "plant"
  ]
  node [
    id 216
    label "osnowa&#263;"
  ]
  node [
    id 217
    label "przymocowa&#263;"
  ]
  node [
    id 218
    label "wetkn&#261;&#263;"
  ]
  node [
    id 219
    label "wetkni&#281;cie"
  ]
  node [
    id 220
    label "przetkanie"
  ]
  node [
    id 221
    label "anchor"
  ]
  node [
    id 222
    label "przymocowanie"
  ]
  node [
    id 223
    label "zaczerpni&#281;cie"
  ]
  node [
    id 224
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 225
    label "interposition"
  ]
  node [
    id 226
    label "odm&#322;odzenie"
  ]
  node [
    id 227
    label "HP"
  ]
  node [
    id 228
    label "dost&#281;p"
  ]
  node [
    id 229
    label "infa"
  ]
  node [
    id 230
    label "kierunek"
  ]
  node [
    id 231
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 232
    label "kryptologia"
  ]
  node [
    id 233
    label "baza_danych"
  ]
  node [
    id 234
    label "przetwarzanie_informacji"
  ]
  node [
    id 235
    label "sztuczna_inteligencja"
  ]
  node [
    id 236
    label "gramatyka_formalna"
  ]
  node [
    id 237
    label "zamek"
  ]
  node [
    id 238
    label "dziedzina_informatyki"
  ]
  node [
    id 239
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 240
    label "artefakt"
  ]
  node [
    id 241
    label "przebywanie"
  ]
  node [
    id 242
    label "kij_baseballowy"
  ]
  node [
    id 243
    label "b&#322;&#261;d"
  ]
  node [
    id 244
    label "r&#281;kawica_baseballowa"
  ]
  node [
    id 245
    label "gra"
  ]
  node [
    id 246
    label "sport"
  ]
  node [
    id 247
    label "sport_zespo&#322;owy"
  ]
  node [
    id 248
    label "&#322;apacz"
  ]
  node [
    id 249
    label "uprawienie"
  ]
  node [
    id 250
    label "u&#322;o&#380;enie"
  ]
  node [
    id 251
    label "p&#322;osa"
  ]
  node [
    id 252
    label "ziemia"
  ]
  node [
    id 253
    label "t&#322;o"
  ]
  node [
    id 254
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 255
    label "gospodarstwo"
  ]
  node [
    id 256
    label "uprawi&#263;"
  ]
  node [
    id 257
    label "room"
  ]
  node [
    id 258
    label "dw&#243;r"
  ]
  node [
    id 259
    label "okazja"
  ]
  node [
    id 260
    label "rozmiar"
  ]
  node [
    id 261
    label "irygowanie"
  ]
  node [
    id 262
    label "compass"
  ]
  node [
    id 263
    label "square"
  ]
  node [
    id 264
    label "zmienna"
  ]
  node [
    id 265
    label "irygowa&#263;"
  ]
  node [
    id 266
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 267
    label "socjologia"
  ]
  node [
    id 268
    label "dziedzina"
  ]
  node [
    id 269
    label "region"
  ]
  node [
    id 270
    label "zagon"
  ]
  node [
    id 271
    label "obszar"
  ]
  node [
    id 272
    label "sk&#322;ad"
  ]
  node [
    id 273
    label "powierzchnia"
  ]
  node [
    id 274
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 275
    label "plane"
  ]
  node [
    id 276
    label "radlina"
  ]
  node [
    id 277
    label "szczyt"
  ]
  node [
    id 278
    label "record"
  ]
  node [
    id 279
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 280
    label "bojowisko"
  ]
  node [
    id 281
    label "bojo"
  ]
  node [
    id 282
    label "linia"
  ]
  node [
    id 283
    label "aut"
  ]
  node [
    id 284
    label "skrzyd&#322;o"
  ]
  node [
    id 285
    label "obiekt"
  ]
  node [
    id 286
    label "kszta&#322;t"
  ]
  node [
    id 287
    label "podpora"
  ]
  node [
    id 288
    label "zesp&#243;&#322;"
  ]
  node [
    id 289
    label "s&#322;up"
  ]
  node [
    id 290
    label "awangarda"
  ]
  node [
    id 291
    label "heading"
  ]
  node [
    id 292
    label "dzia&#322;"
  ]
  node [
    id 293
    label "wykres"
  ]
  node [
    id 294
    label "ogniwo_galwaniczne"
  ]
  node [
    id 295
    label "element"
  ]
  node [
    id 296
    label "pomnik"
  ]
  node [
    id 297
    label "artyku&#322;"
  ]
  node [
    id 298
    label "g&#322;o&#347;nik"
  ]
  node [
    id 299
    label "plinta"
  ]
  node [
    id 300
    label "tabela"
  ]
  node [
    id 301
    label "trzon"
  ]
  node [
    id 302
    label "maszyna"
  ]
  node [
    id 303
    label "szyk"
  ]
  node [
    id 304
    label "megaron"
  ]
  node [
    id 305
    label "macierz"
  ]
  node [
    id 306
    label "reprezentacja"
  ]
  node [
    id 307
    label "urz&#261;dzenie_przemys&#322;owe"
  ]
  node [
    id 308
    label "ariergarda"
  ]
  node [
    id 309
    label "&#322;am"
  ]
  node [
    id 310
    label "kierownica"
  ]
  node [
    id 311
    label "urz&#261;dzenie"
  ]
  node [
    id 312
    label "strona"
  ]
  node [
    id 313
    label "MS"
  ]
  node [
    id 314
    label "Access"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 284
  ]
  edge [
    source 1
    target 285
  ]
  edge [
    source 1
    target 286
  ]
  edge [
    source 1
    target 287
  ]
  edge [
    source 1
    target 288
  ]
  edge [
    source 1
    target 289
  ]
  edge [
    source 1
    target 290
  ]
  edge [
    source 1
    target 291
  ]
  edge [
    source 1
    target 292
  ]
  edge [
    source 1
    target 293
  ]
  edge [
    source 1
    target 294
  ]
  edge [
    source 1
    target 295
  ]
  edge [
    source 1
    target 296
  ]
  edge [
    source 1
    target 297
  ]
  edge [
    source 1
    target 298
  ]
  edge [
    source 1
    target 299
  ]
  edge [
    source 1
    target 300
  ]
  edge [
    source 1
    target 301
  ]
  edge [
    source 1
    target 302
  ]
  edge [
    source 1
    target 303
  ]
  edge [
    source 1
    target 304
  ]
  edge [
    source 1
    target 305
  ]
  edge [
    source 1
    target 306
  ]
  edge [
    source 1
    target 307
  ]
  edge [
    source 1
    target 308
  ]
  edge [
    source 1
    target 309
  ]
  edge [
    source 1
    target 310
  ]
  edge [
    source 1
    target 311
  ]
  edge [
    source 1
    target 312
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 313
    target 314
  ]
]
