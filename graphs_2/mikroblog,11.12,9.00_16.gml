graph [
  node [
    id 0
    label "stycze&#324;"
    origin "text"
  ]
  node [
    id 1
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "publiczny"
    origin "text"
  ]
  node [
    id 4
    label "rejestr"
    origin "text"
  ]
  node [
    id 5
    label "sprawca"
    origin "text"
  ]
  node [
    id 6
    label "przest&#281;pstwo"
    origin "text"
  ]
  node [
    id 7
    label "t&#322;o"
    origin "text"
  ]
  node [
    id 8
    label "seksualny"
    origin "text"
  ]
  node [
    id 9
    label "Nowy_Rok"
  ]
  node [
    id 10
    label "miesi&#261;c"
  ]
  node [
    id 11
    label "tydzie&#324;"
  ]
  node [
    id 12
    label "miech"
  ]
  node [
    id 13
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 14
    label "czas"
  ]
  node [
    id 15
    label "rok"
  ]
  node [
    id 16
    label "kalendy"
  ]
  node [
    id 17
    label "artyleria"
  ]
  node [
    id 18
    label "laweta"
  ]
  node [
    id 19
    label "cannon"
  ]
  node [
    id 20
    label "waln&#261;&#263;"
  ]
  node [
    id 21
    label "bateria_artylerii"
  ]
  node [
    id 22
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 23
    label "oporopowrotnik"
  ]
  node [
    id 24
    label "przedmuchiwacz"
  ]
  node [
    id 25
    label "bateria"
  ]
  node [
    id 26
    label "bro&#324;"
  ]
  node [
    id 27
    label "amunicja"
  ]
  node [
    id 28
    label "karta_przetargowa"
  ]
  node [
    id 29
    label "rozbroi&#263;"
  ]
  node [
    id 30
    label "rozbrojenie"
  ]
  node [
    id 31
    label "osprz&#281;t"
  ]
  node [
    id 32
    label "uzbrojenie"
  ]
  node [
    id 33
    label "przyrz&#261;d"
  ]
  node [
    id 34
    label "rozbrajanie"
  ]
  node [
    id 35
    label "rozbraja&#263;"
  ]
  node [
    id 36
    label "or&#281;&#380;"
  ]
  node [
    id 37
    label "urz&#261;dzenie"
  ]
  node [
    id 38
    label "przyczepa"
  ]
  node [
    id 39
    label "podstawa"
  ]
  node [
    id 40
    label "lemiesz"
  ]
  node [
    id 41
    label "powrotnik"
  ]
  node [
    id 42
    label "formacja"
  ]
  node [
    id 43
    label "armia"
  ]
  node [
    id 44
    label "artillery"
  ]
  node [
    id 45
    label "munition"
  ]
  node [
    id 46
    label "zaw&#243;r"
  ]
  node [
    id 47
    label "zbi&#243;r"
  ]
  node [
    id 48
    label "kolekcja"
  ]
  node [
    id 49
    label "oficer_ogniowy"
  ]
  node [
    id 50
    label "dywizjon_artylerii"
  ]
  node [
    id 51
    label "kran"
  ]
  node [
    id 52
    label "pododdzia&#322;"
  ]
  node [
    id 53
    label "cell"
  ]
  node [
    id 54
    label "pluton"
  ]
  node [
    id 55
    label "odkr&#281;ca&#263;_wod&#281;"
  ]
  node [
    id 56
    label "odkr&#281;ci&#263;_wod&#281;"
  ]
  node [
    id 57
    label "dzia&#322;obitnia"
  ]
  node [
    id 58
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 59
    label "sygn&#261;&#263;"
  ]
  node [
    id 60
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 61
    label "peddle"
  ]
  node [
    id 62
    label "jebn&#261;&#263;"
  ]
  node [
    id 63
    label "fall"
  ]
  node [
    id 64
    label "rap"
  ]
  node [
    id 65
    label "uderzy&#263;"
  ]
  node [
    id 66
    label "zrobi&#263;"
  ]
  node [
    id 67
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 68
    label "zmieni&#263;"
  ]
  node [
    id 69
    label "majdn&#261;&#263;"
  ]
  node [
    id 70
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 71
    label "paln&#261;&#263;"
  ]
  node [
    id 72
    label "strzeli&#263;"
  ]
  node [
    id 73
    label "lumber"
  ]
  node [
    id 74
    label "upublicznianie"
  ]
  node [
    id 75
    label "jawny"
  ]
  node [
    id 76
    label "upublicznienie"
  ]
  node [
    id 77
    label "publicznie"
  ]
  node [
    id 78
    label "jawnie"
  ]
  node [
    id 79
    label "udost&#281;pnianie"
  ]
  node [
    id 80
    label "udost&#281;pnienie"
  ]
  node [
    id 81
    label "ujawnienie_si&#281;"
  ]
  node [
    id 82
    label "ujawnianie_si&#281;"
  ]
  node [
    id 83
    label "zdecydowany"
  ]
  node [
    id 84
    label "znajomy"
  ]
  node [
    id 85
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 86
    label "ujawnienie"
  ]
  node [
    id 87
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 88
    label "ujawnianie"
  ]
  node [
    id 89
    label "ewidentny"
  ]
  node [
    id 90
    label "catalog"
  ]
  node [
    id 91
    label "uk&#322;ad_elektroniczny"
  ]
  node [
    id 92
    label "przycisk"
  ]
  node [
    id 93
    label "pozycja"
  ]
  node [
    id 94
    label "stock"
  ]
  node [
    id 95
    label "tekst"
  ]
  node [
    id 96
    label "regestr"
  ]
  node [
    id 97
    label "sumariusz"
  ]
  node [
    id 98
    label "procesor"
  ]
  node [
    id 99
    label "brzmienie"
  ]
  node [
    id 100
    label "figurowa&#263;"
  ]
  node [
    id 101
    label "skala"
  ]
  node [
    id 102
    label "book"
  ]
  node [
    id 103
    label "wyliczanka"
  ]
  node [
    id 104
    label "organy"
  ]
  node [
    id 105
    label "przedmiot"
  ]
  node [
    id 106
    label "kom&#243;rka"
  ]
  node [
    id 107
    label "furnishing"
  ]
  node [
    id 108
    label "zabezpieczenie"
  ]
  node [
    id 109
    label "zrobienie"
  ]
  node [
    id 110
    label "wyrz&#261;dzenie"
  ]
  node [
    id 111
    label "zagospodarowanie"
  ]
  node [
    id 112
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 113
    label "ig&#322;a"
  ]
  node [
    id 114
    label "narz&#281;dzie"
  ]
  node [
    id 115
    label "wirnik"
  ]
  node [
    id 116
    label "aparatura"
  ]
  node [
    id 117
    label "system_energetyczny"
  ]
  node [
    id 118
    label "impulsator"
  ]
  node [
    id 119
    label "mechanizm"
  ]
  node [
    id 120
    label "sprz&#281;t"
  ]
  node [
    id 121
    label "czynno&#347;&#263;"
  ]
  node [
    id 122
    label "blokowanie"
  ]
  node [
    id 123
    label "set"
  ]
  node [
    id 124
    label "zablokowanie"
  ]
  node [
    id 125
    label "przygotowanie"
  ]
  node [
    id 126
    label "komora"
  ]
  node [
    id 127
    label "j&#281;zyk"
  ]
  node [
    id 128
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 129
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 130
    label "interfejs"
  ]
  node [
    id 131
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 132
    label "pole"
  ]
  node [
    id 133
    label "prze&#322;&#261;cznik"
  ]
  node [
    id 134
    label "nacisk"
  ]
  node [
    id 135
    label "wymowa"
  ]
  node [
    id 136
    label "d&#378;wi&#281;k"
  ]
  node [
    id 137
    label "ekscerpcja"
  ]
  node [
    id 138
    label "j&#281;zykowo"
  ]
  node [
    id 139
    label "wypowied&#378;"
  ]
  node [
    id 140
    label "redakcja"
  ]
  node [
    id 141
    label "wytw&#243;r"
  ]
  node [
    id 142
    label "pomini&#281;cie"
  ]
  node [
    id 143
    label "dzie&#322;o"
  ]
  node [
    id 144
    label "preparacja"
  ]
  node [
    id 145
    label "odmianka"
  ]
  node [
    id 146
    label "opu&#347;ci&#263;"
  ]
  node [
    id 147
    label "koniektura"
  ]
  node [
    id 148
    label "pisa&#263;"
  ]
  node [
    id 149
    label "obelga"
  ]
  node [
    id 150
    label "egzemplarz"
  ]
  node [
    id 151
    label "series"
  ]
  node [
    id 152
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 153
    label "uprawianie"
  ]
  node [
    id 154
    label "praca_rolnicza"
  ]
  node [
    id 155
    label "collection"
  ]
  node [
    id 156
    label "dane"
  ]
  node [
    id 157
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 158
    label "pakiet_klimatyczny"
  ]
  node [
    id 159
    label "poj&#281;cie"
  ]
  node [
    id 160
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 161
    label "sum"
  ]
  node [
    id 162
    label "gathering"
  ]
  node [
    id 163
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 164
    label "album"
  ]
  node [
    id 165
    label "g&#322;os"
  ]
  node [
    id 166
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 167
    label "spis"
  ]
  node [
    id 168
    label "check"
  ]
  node [
    id 169
    label "entliczek"
  ]
  node [
    id 170
    label "zabawa"
  ]
  node [
    id 171
    label "wiersz"
  ]
  node [
    id 172
    label "pentliczek"
  ]
  node [
    id 173
    label "instrument_d&#281;ty_klawiszowy"
  ]
  node [
    id 174
    label "wiatrownica"
  ]
  node [
    id 175
    label "prospekt"
  ]
  node [
    id 176
    label "kalikant"
  ]
  node [
    id 177
    label "organy_Hammonda"
  ]
  node [
    id 178
    label "kolorysta"
  ]
  node [
    id 179
    label "ch&#243;r"
  ]
  node [
    id 180
    label "traktura"
  ]
  node [
    id 181
    label "instrument_klawiszowy"
  ]
  node [
    id 182
    label "masztab"
  ]
  node [
    id 183
    label "kreska"
  ]
  node [
    id 184
    label "podzia&#322;ka"
  ]
  node [
    id 185
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 186
    label "wielko&#347;&#263;"
  ]
  node [
    id 187
    label "zero"
  ]
  node [
    id 188
    label "interwa&#322;"
  ]
  node [
    id 189
    label "przymiar"
  ]
  node [
    id 190
    label "struktura"
  ]
  node [
    id 191
    label "sfera"
  ]
  node [
    id 192
    label "jednostka"
  ]
  node [
    id 193
    label "dominanta"
  ]
  node [
    id 194
    label "tetrachord"
  ]
  node [
    id 195
    label "scale"
  ]
  node [
    id 196
    label "przedzia&#322;"
  ]
  node [
    id 197
    label "podzakres"
  ]
  node [
    id 198
    label "proporcja"
  ]
  node [
    id 199
    label "dziedzina"
  ]
  node [
    id 200
    label "part"
  ]
  node [
    id 201
    label "subdominanta"
  ]
  node [
    id 202
    label "arytmometr"
  ]
  node [
    id 203
    label "uk&#322;ad_scalony"
  ]
  node [
    id 204
    label "pami&#281;&#263;_g&#243;rna"
  ]
  node [
    id 205
    label "rdze&#324;"
  ]
  node [
    id 206
    label "cooler"
  ]
  node [
    id 207
    label "komputer"
  ]
  node [
    id 208
    label "sumator"
  ]
  node [
    id 209
    label "wyra&#380;anie"
  ]
  node [
    id 210
    label "sound"
  ]
  node [
    id 211
    label "cecha"
  ]
  node [
    id 212
    label "tone"
  ]
  node [
    id 213
    label "wydawanie"
  ]
  node [
    id 214
    label "spirit"
  ]
  node [
    id 215
    label "kolorystyka"
  ]
  node [
    id 216
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 217
    label "po&#322;o&#380;enie"
  ]
  node [
    id 218
    label "debit"
  ]
  node [
    id 219
    label "druk"
  ]
  node [
    id 220
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 221
    label "szata_graficzna"
  ]
  node [
    id 222
    label "wydawa&#263;"
  ]
  node [
    id 223
    label "szermierka"
  ]
  node [
    id 224
    label "wyda&#263;"
  ]
  node [
    id 225
    label "ustawienie"
  ]
  node [
    id 226
    label "publikacja"
  ]
  node [
    id 227
    label "status"
  ]
  node [
    id 228
    label "miejsce"
  ]
  node [
    id 229
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 230
    label "adres"
  ]
  node [
    id 231
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 232
    label "rozmieszczenie"
  ]
  node [
    id 233
    label "sytuacja"
  ]
  node [
    id 234
    label "rz&#261;d"
  ]
  node [
    id 235
    label "redaktor"
  ]
  node [
    id 236
    label "awansowa&#263;"
  ]
  node [
    id 237
    label "wojsko"
  ]
  node [
    id 238
    label "bearing"
  ]
  node [
    id 239
    label "znaczenie"
  ]
  node [
    id 240
    label "awans"
  ]
  node [
    id 241
    label "awansowanie"
  ]
  node [
    id 242
    label "poster"
  ]
  node [
    id 243
    label "le&#380;e&#263;"
  ]
  node [
    id 244
    label "sprawiciel"
  ]
  node [
    id 245
    label "cz&#322;owiek"
  ]
  node [
    id 246
    label "ludzko&#347;&#263;"
  ]
  node [
    id 247
    label "asymilowanie"
  ]
  node [
    id 248
    label "wapniak"
  ]
  node [
    id 249
    label "asymilowa&#263;"
  ]
  node [
    id 250
    label "os&#322;abia&#263;"
  ]
  node [
    id 251
    label "posta&#263;"
  ]
  node [
    id 252
    label "hominid"
  ]
  node [
    id 253
    label "podw&#322;adny"
  ]
  node [
    id 254
    label "os&#322;abianie"
  ]
  node [
    id 255
    label "g&#322;owa"
  ]
  node [
    id 256
    label "figura"
  ]
  node [
    id 257
    label "portrecista"
  ]
  node [
    id 258
    label "dwun&#243;g"
  ]
  node [
    id 259
    label "profanum"
  ]
  node [
    id 260
    label "mikrokosmos"
  ]
  node [
    id 261
    label "nasada"
  ]
  node [
    id 262
    label "duch"
  ]
  node [
    id 263
    label "antropochoria"
  ]
  node [
    id 264
    label "osoba"
  ]
  node [
    id 265
    label "wz&#243;r"
  ]
  node [
    id 266
    label "senior"
  ]
  node [
    id 267
    label "oddzia&#322;ywanie"
  ]
  node [
    id 268
    label "Adam"
  ]
  node [
    id 269
    label "homo_sapiens"
  ]
  node [
    id 270
    label "polifag"
  ]
  node [
    id 271
    label "brudny"
  ]
  node [
    id 272
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 273
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 274
    label "crime"
  ]
  node [
    id 275
    label "sprawstwo"
  ]
  node [
    id 276
    label "nies&#322;uszno&#347;&#263;"
  ]
  node [
    id 277
    label "bias"
  ]
  node [
    id 278
    label "strata"
  ]
  node [
    id 279
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 280
    label "post&#281;pek"
  ]
  node [
    id 281
    label "injustice"
  ]
  node [
    id 282
    label "criminalism"
  ]
  node [
    id 283
    label "gang"
  ]
  node [
    id 284
    label "bezprawie"
  ]
  node [
    id 285
    label "problem_spo&#322;eczny"
  ]
  node [
    id 286
    label "patologia"
  ]
  node [
    id 287
    label "banda"
  ]
  node [
    id 288
    label "nieprzyjemny"
  ]
  node [
    id 289
    label "nielegalny"
  ]
  node [
    id 290
    label "brudno"
  ]
  node [
    id 291
    label "brudzenie_si&#281;"
  ]
  node [
    id 292
    label "z&#322;amany"
  ]
  node [
    id 293
    label "nieprzyzwoity"
  ]
  node [
    id 294
    label "nieporz&#261;dny"
  ]
  node [
    id 295
    label "brudzenie"
  ]
  node [
    id 296
    label "ciemny"
  ]
  node [
    id 297
    label "flejtuchowaty"
  ]
  node [
    id 298
    label "zabrudzenie_si&#281;"
  ]
  node [
    id 299
    label "nieczysty"
  ]
  node [
    id 300
    label "u&#380;ywany"
  ]
  node [
    id 301
    label "p&#322;aszczyzna"
  ]
  node [
    id 302
    label "&#347;rodowisko"
  ]
  node [
    id 303
    label "plan"
  ]
  node [
    id 304
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 305
    label "gestaltyzm"
  ]
  node [
    id 306
    label "background"
  ]
  node [
    id 307
    label "melodia"
  ]
  node [
    id 308
    label "causal_agent"
  ]
  node [
    id 309
    label "dalszoplanowy"
  ]
  node [
    id 310
    label "warunki"
  ]
  node [
    id 311
    label "pod&#322;o&#380;e"
  ]
  node [
    id 312
    label "obiekt"
  ]
  node [
    id 313
    label "informacja"
  ]
  node [
    id 314
    label "obraz"
  ]
  node [
    id 315
    label "layer"
  ]
  node [
    id 316
    label "lingwistyka_kognitywna"
  ]
  node [
    id 317
    label "partia"
  ]
  node [
    id 318
    label "ubarwienie"
  ]
  node [
    id 319
    label "model"
  ]
  node [
    id 320
    label "intencja"
  ]
  node [
    id 321
    label "punkt"
  ]
  node [
    id 322
    label "rysunek"
  ]
  node [
    id 323
    label "miejsce_pracy"
  ]
  node [
    id 324
    label "przestrze&#324;"
  ]
  node [
    id 325
    label "device"
  ]
  node [
    id 326
    label "pomys&#322;"
  ]
  node [
    id 327
    label "reprezentacja"
  ]
  node [
    id 328
    label "agreement"
  ]
  node [
    id 329
    label "dekoracja"
  ]
  node [
    id 330
    label "perspektywa"
  ]
  node [
    id 331
    label "wymiar"
  ]
  node [
    id 332
    label "&#347;ciana"
  ]
  node [
    id 333
    label "surface"
  ]
  node [
    id 334
    label "zakres"
  ]
  node [
    id 335
    label "kwadrant"
  ]
  node [
    id 336
    label "degree"
  ]
  node [
    id 337
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 338
    label "powierzchnia"
  ]
  node [
    id 339
    label "ukszta&#322;towanie"
  ]
  node [
    id 340
    label "cia&#322;o"
  ]
  node [
    id 341
    label "p&#322;aszczak"
  ]
  node [
    id 342
    label "wygl&#261;d"
  ]
  node [
    id 343
    label "r&#243;&#380;nokolorowy"
  ]
  node [
    id 344
    label "barwny"
  ]
  node [
    id 345
    label "przybranie"
  ]
  node [
    id 346
    label "color"
  ]
  node [
    id 347
    label "litosfera"
  ]
  node [
    id 348
    label "dotleni&#263;"
  ]
  node [
    id 349
    label "pr&#243;chnica"
  ]
  node [
    id 350
    label "glej"
  ]
  node [
    id 351
    label "martwica"
  ]
  node [
    id 352
    label "glinowa&#263;"
  ]
  node [
    id 353
    label "podglebie"
  ]
  node [
    id 354
    label "ryzosfera"
  ]
  node [
    id 355
    label "kompleks_sorpcyjny"
  ]
  node [
    id 356
    label "przyczyna"
  ]
  node [
    id 357
    label "geosystem"
  ]
  node [
    id 358
    label "glinowanie"
  ]
  node [
    id 359
    label "wiedza"
  ]
  node [
    id 360
    label "doj&#347;cie"
  ]
  node [
    id 361
    label "obiega&#263;"
  ]
  node [
    id 362
    label "powzi&#281;cie"
  ]
  node [
    id 363
    label "obiegni&#281;cie"
  ]
  node [
    id 364
    label "sygna&#322;"
  ]
  node [
    id 365
    label "obieganie"
  ]
  node [
    id 366
    label "powzi&#261;&#263;"
  ]
  node [
    id 367
    label "obiec"
  ]
  node [
    id 368
    label "doj&#347;&#263;"
  ]
  node [
    id 369
    label "co&#347;"
  ]
  node [
    id 370
    label "budynek"
  ]
  node [
    id 371
    label "thing"
  ]
  node [
    id 372
    label "program"
  ]
  node [
    id 373
    label "rzecz"
  ]
  node [
    id 374
    label "strona"
  ]
  node [
    id 375
    label "zanucenie"
  ]
  node [
    id 376
    label "nuta"
  ]
  node [
    id 377
    label "zakosztowa&#263;"
  ]
  node [
    id 378
    label "zajawka"
  ]
  node [
    id 379
    label "zanuci&#263;"
  ]
  node [
    id 380
    label "emocja"
  ]
  node [
    id 381
    label "oskoma"
  ]
  node [
    id 382
    label "melika"
  ]
  node [
    id 383
    label "nucenie"
  ]
  node [
    id 384
    label "nuci&#263;"
  ]
  node [
    id 385
    label "istota"
  ]
  node [
    id 386
    label "zjawisko"
  ]
  node [
    id 387
    label "taste"
  ]
  node [
    id 388
    label "muzyka"
  ]
  node [
    id 389
    label "inclination"
  ]
  node [
    id 390
    label "Bund"
  ]
  node [
    id 391
    label "PPR"
  ]
  node [
    id 392
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 393
    label "wybranek"
  ]
  node [
    id 394
    label "Jakobici"
  ]
  node [
    id 395
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 396
    label "SLD"
  ]
  node [
    id 397
    label "Razem"
  ]
  node [
    id 398
    label "PiS"
  ]
  node [
    id 399
    label "package"
  ]
  node [
    id 400
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 401
    label "Kuomintang"
  ]
  node [
    id 402
    label "ZSL"
  ]
  node [
    id 403
    label "organizacja"
  ]
  node [
    id 404
    label "AWS"
  ]
  node [
    id 405
    label "gra"
  ]
  node [
    id 406
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 407
    label "game"
  ]
  node [
    id 408
    label "grupa"
  ]
  node [
    id 409
    label "blok"
  ]
  node [
    id 410
    label "materia&#322;"
  ]
  node [
    id 411
    label "PO"
  ]
  node [
    id 412
    label "si&#322;a"
  ]
  node [
    id 413
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 414
    label "niedoczas"
  ]
  node [
    id 415
    label "Federali&#347;ci"
  ]
  node [
    id 416
    label "PSL"
  ]
  node [
    id 417
    label "Wigowie"
  ]
  node [
    id 418
    label "ZChN"
  ]
  node [
    id 419
    label "egzekutywa"
  ]
  node [
    id 420
    label "aktyw"
  ]
  node [
    id 421
    label "wybranka"
  ]
  node [
    id 422
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 423
    label "unit"
  ]
  node [
    id 424
    label "sk&#322;adnik"
  ]
  node [
    id 425
    label "wydarzenie"
  ]
  node [
    id 426
    label "class"
  ]
  node [
    id 427
    label "zesp&#243;&#322;"
  ]
  node [
    id 428
    label "obiekt_naturalny"
  ]
  node [
    id 429
    label "otoczenie"
  ]
  node [
    id 430
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 431
    label "environment"
  ]
  node [
    id 432
    label "huczek"
  ]
  node [
    id 433
    label "ekosystem"
  ]
  node [
    id 434
    label "wszechstworzenie"
  ]
  node [
    id 435
    label "woda"
  ]
  node [
    id 436
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 437
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 438
    label "teren"
  ]
  node [
    id 439
    label "stw&#243;r"
  ]
  node [
    id 440
    label "Ziemia"
  ]
  node [
    id 441
    label "fauna"
  ]
  node [
    id 442
    label "biota"
  ]
  node [
    id 443
    label "pocz&#261;tki"
  ]
  node [
    id 444
    label "pochodzenie"
  ]
  node [
    id 445
    label "kontekst"
  ]
  node [
    id 446
    label "representation"
  ]
  node [
    id 447
    label "effigy"
  ]
  node [
    id 448
    label "podobrazie"
  ]
  node [
    id 449
    label "scena"
  ]
  node [
    id 450
    label "human_body"
  ]
  node [
    id 451
    label "projekcja"
  ]
  node [
    id 452
    label "oprawia&#263;"
  ]
  node [
    id 453
    label "postprodukcja"
  ]
  node [
    id 454
    label "inning"
  ]
  node [
    id 455
    label "pulment"
  ]
  node [
    id 456
    label "pogl&#261;d"
  ]
  node [
    id 457
    label "plama_barwna"
  ]
  node [
    id 458
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 459
    label "oprawianie"
  ]
  node [
    id 460
    label "sztafa&#380;"
  ]
  node [
    id 461
    label "parkiet"
  ]
  node [
    id 462
    label "opinion"
  ]
  node [
    id 463
    label "uj&#281;cie"
  ]
  node [
    id 464
    label "zaj&#347;cie"
  ]
  node [
    id 465
    label "persona"
  ]
  node [
    id 466
    label "filmoteka"
  ]
  node [
    id 467
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 468
    label "ziarno"
  ]
  node [
    id 469
    label "picture"
  ]
  node [
    id 470
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 471
    label "wypunktowa&#263;"
  ]
  node [
    id 472
    label "ostro&#347;&#263;"
  ]
  node [
    id 473
    label "malarz"
  ]
  node [
    id 474
    label "napisy"
  ]
  node [
    id 475
    label "przeplot"
  ]
  node [
    id 476
    label "punktowa&#263;"
  ]
  node [
    id 477
    label "anamorfoza"
  ]
  node [
    id 478
    label "przedstawienie"
  ]
  node [
    id 479
    label "ty&#322;&#243;wka"
  ]
  node [
    id 480
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 481
    label "widok"
  ]
  node [
    id 482
    label "czo&#322;&#243;wka"
  ]
  node [
    id 483
    label "rola"
  ]
  node [
    id 484
    label "psychologia"
  ]
  node [
    id 485
    label "drugoplanowo"
  ]
  node [
    id 486
    label "nieznaczny"
  ]
  node [
    id 487
    label "poboczny"
  ]
  node [
    id 488
    label "dalszy"
  ]
  node [
    id 489
    label "p&#322;ciowo"
  ]
  node [
    id 490
    label "erotyczny"
  ]
  node [
    id 491
    label "seksowny"
  ]
  node [
    id 492
    label "seksualnie"
  ]
  node [
    id 493
    label "erotycznie"
  ]
  node [
    id 494
    label "mi&#322;o&#347;ny"
  ]
  node [
    id 495
    label "specjalny"
  ]
  node [
    id 496
    label "p&#322;ciowy"
  ]
  node [
    id 497
    label "powabny"
  ]
  node [
    id 498
    label "podniecaj&#261;cy"
  ]
  node [
    id 499
    label "atrakcyjny"
  ]
  node [
    id 500
    label "seksownie"
  ]
  node [
    id 501
    label "na"
  ]
  node [
    id 502
    label "Dariusz"
  ]
  node [
    id 503
    label "buda"
  ]
  node [
    id 504
    label "nowa"
  ]
  node [
    id 505
    label "dw&#243;r"
  ]
  node [
    id 506
    label "Gda&#324;skie"
  ]
  node [
    id 507
    label "Laskowice"
  ]
  node [
    id 508
    label "wielki"
  ]
  node [
    id 509
    label "s&#261;d"
  ]
  node [
    id 510
    label "wojew&#243;dzki"
  ]
  node [
    id 511
    label "apelacyjny"
  ]
  node [
    id 512
    label "w"
  ]
  node [
    id 513
    label "gda&#324;ski"
  ]
  node [
    id 514
    label "zak&#322;ad"
  ]
  node [
    id 515
    label "karny"
  ]
  node [
    id 516
    label "okr&#281;gowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 502
    target 503
  ]
  edge [
    source 504
    target 505
  ]
  edge [
    source 504
    target 506
  ]
  edge [
    source 505
    target 506
  ]
  edge [
    source 507
    target 508
  ]
  edge [
    source 509
    target 510
  ]
  edge [
    source 509
    target 511
  ]
  edge [
    source 509
    target 512
  ]
  edge [
    source 509
    target 513
  ]
  edge [
    source 509
    target 516
  ]
  edge [
    source 511
    target 512
  ]
  edge [
    source 511
    target 513
  ]
  edge [
    source 512
    target 513
  ]
  edge [
    source 514
    target 515
  ]
]
