graph [
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "&#347;ledzi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "tag"
    origin "text"
  ]
  node [
    id 3
    label "aptekarskiwabik"
    origin "text"
  ]
  node [
    id 4
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 5
    label "invite"
  ]
  node [
    id 6
    label "ask"
  ]
  node [
    id 7
    label "oferowa&#263;"
  ]
  node [
    id 8
    label "zach&#281;ca&#263;"
  ]
  node [
    id 9
    label "volunteer"
  ]
  node [
    id 10
    label "szperacz"
  ]
  node [
    id 11
    label "robi&#263;"
  ]
  node [
    id 12
    label "chase"
  ]
  node [
    id 13
    label "szuka&#263;"
  ]
  node [
    id 14
    label "examine"
  ]
  node [
    id 15
    label "&#322;owiectwo"
  ]
  node [
    id 16
    label "stara&#263;_si&#281;"
  ]
  node [
    id 17
    label "sprawdza&#263;"
  ]
  node [
    id 18
    label "try"
  ]
  node [
    id 19
    label "&#322;azi&#263;"
  ]
  node [
    id 20
    label "organizowa&#263;"
  ]
  node [
    id 21
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 22
    label "czyni&#263;"
  ]
  node [
    id 23
    label "give"
  ]
  node [
    id 24
    label "stylizowa&#263;"
  ]
  node [
    id 25
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 26
    label "falowa&#263;"
  ]
  node [
    id 27
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 28
    label "peddle"
  ]
  node [
    id 29
    label "praca"
  ]
  node [
    id 30
    label "wydala&#263;"
  ]
  node [
    id 31
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "tentegowa&#263;"
  ]
  node [
    id 33
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 34
    label "urz&#261;dza&#263;"
  ]
  node [
    id 35
    label "oszukiwa&#263;"
  ]
  node [
    id 36
    label "work"
  ]
  node [
    id 37
    label "ukazywa&#263;"
  ]
  node [
    id 38
    label "przerabia&#263;"
  ]
  node [
    id 39
    label "act"
  ]
  node [
    id 40
    label "post&#281;powa&#263;"
  ]
  node [
    id 41
    label "reflektor_punktowy"
  ]
  node [
    id 42
    label "patrolowiec"
  ]
  node [
    id 43
    label "poszukiwacz"
  ]
  node [
    id 44
    label "pies_my&#347;liwski"
  ]
  node [
    id 45
    label "tropi&#263;"
  ]
  node [
    id 46
    label "wytropienie"
  ]
  node [
    id 47
    label "podkurza&#263;"
  ]
  node [
    id 48
    label "rolnictwo"
  ]
  node [
    id 49
    label "wytropi&#263;"
  ]
  node [
    id 50
    label "polowanie"
  ]
  node [
    id 51
    label "tropienie"
  ]
  node [
    id 52
    label "blood_sport"
  ]
  node [
    id 53
    label "defilowa&#263;"
  ]
  node [
    id 54
    label "czynno&#347;&#263;"
  ]
  node [
    id 55
    label "gospodarka_le&#347;na"
  ]
  node [
    id 56
    label "napis"
  ]
  node [
    id 57
    label "nerd"
  ]
  node [
    id 58
    label "znacznik"
  ]
  node [
    id 59
    label "komnatowy"
  ]
  node [
    id 60
    label "sport_elektroniczny"
  ]
  node [
    id 61
    label "identyfikator"
  ]
  node [
    id 62
    label "znak"
  ]
  node [
    id 63
    label "oznaka"
  ]
  node [
    id 64
    label "mark"
  ]
  node [
    id 65
    label "marker"
  ]
  node [
    id 66
    label "substancja"
  ]
  node [
    id 67
    label "autografia"
  ]
  node [
    id 68
    label "tekst"
  ]
  node [
    id 69
    label "expressive_style"
  ]
  node [
    id 70
    label "informacja"
  ]
  node [
    id 71
    label "plakietka"
  ]
  node [
    id 72
    label "identifier"
  ]
  node [
    id 73
    label "symbol"
  ]
  node [
    id 74
    label "urz&#261;dzenie"
  ]
  node [
    id 75
    label "programowanie"
  ]
  node [
    id 76
    label "geek"
  ]
  node [
    id 77
    label "Gerhard"
  ]
  node [
    id 78
    label "Domagk"
  ]
  node [
    id 79
    label "i"
  ]
  node [
    id 80
    label "wojna"
  ]
  node [
    id 81
    label "&#347;wiatowy"
  ]
  node [
    id 82
    label "wielki"
  ]
  node [
    id 83
    label "Iga"
  ]
  node [
    id 84
    label "Farben"
  ]
  node [
    id 85
    label "Mietzscha"
  ]
  node [
    id 86
    label "Anda"
  ]
  node [
    id 87
    label "Klarera"
  ]
  node [
    id 88
    label "naukowiec"
  ]
  node [
    id 89
    label "Paul"
  ]
  node [
    id 90
    label "Gelmo"
  ]
  node [
    id 91
    label "uniwersytet"
  ]
  node [
    id 92
    label "wiede&#324;ski"
  ]
  node [
    id 93
    label "Franklin"
  ]
  node [
    id 94
    label "D"
  ]
  node [
    id 95
    label "Winstonowi"
  ]
  node [
    id 96
    label "Churchill"
  ]
  node [
    id 97
    label "Roosevelt"
  ]
  node [
    id 98
    label "nagroda"
  ]
  node [
    id 99
    label "Nobel"
  ]
  node [
    id 100
    label "o&#347;wiadczenie"
  ]
  node [
    id 101
    label "Ana"
  ]
  node [
    id 102
    label "V"
  ]
  node [
    id 103
    label "Diez"
  ]
  node [
    id 104
    label "Rouxb"
  ]
  node [
    id 105
    label "Rustam"
  ]
  node [
    id 106
    label "Aminov"
  ]
  node [
    id 107
    label "httpswwwsciencehistoryorghistoricalprofilegerharddomagk"
  ]
  node [
    id 108
    label "httpcontenttimecomtimemagazinearticle0917177190000html"
  ]
  node [
    id 109
    label "Jos&#233;"
  ]
  node [
    id 110
    label "a"
  ]
  node [
    id 111
    label "Tapia"
  ]
  node [
    id 112
    label "Granadosa"
  ]
  node [
    id 113
    label "proca"
  ]
  node [
    id 114
    label "Natl"
  ]
  node [
    id 115
    label "Acad"
  ]
  node [
    id 116
    label "Sci"
  ]
  node [
    id 117
    label "u"
  ]
  node [
    id 118
    label "sekunda"
  ]
  node [
    id 119
    label "Life"
  ]
  node [
    id 120
    label "death"
  ]
  node [
    id 121
    label "during"
  ]
  node [
    id 122
    label "the"
  ]
  node [
    id 123
    label "Great"
  ]
  node [
    id 124
    label "Depression"
  ]
  node [
    id 125
    label "future"
  ]
  node [
    id 126
    label "szczeg&#243;lny"
  ]
  node [
    id 127
    label "podzi&#281;kowanie"
  ]
  node [
    id 128
    label "dla"
  ]
  node [
    id 129
    label "shoterxd"
  ]
  node [
    id 130
    label "brant"
  ]
  node [
    id 131
    label "InformacjaNieprawdziwaCCCLVIII"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 100
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 81
  ]
  edge [
    source 79
    target 105
  ]
  edge [
    source 79
    target 106
  ]
  edge [
    source 79
    target 126
  ]
  edge [
    source 79
    target 127
  ]
  edge [
    source 79
    target 128
  ]
  edge [
    source 79
    target 129
  ]
  edge [
    source 79
    target 130
  ]
  edge [
    source 79
    target 131
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 82
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 85
    target 86
  ]
  edge [
    source 85
    target 87
  ]
  edge [
    source 85
    target 88
  ]
  edge [
    source 86
    target 87
  ]
  edge [
    source 86
    target 88
  ]
  edge [
    source 86
    target 119
  ]
  edge [
    source 86
    target 120
  ]
  edge [
    source 86
    target 121
  ]
  edge [
    source 86
    target 122
  ]
  edge [
    source 86
    target 123
  ]
  edge [
    source 86
    target 124
  ]
  edge [
    source 87
    target 88
  ]
  edge [
    source 89
    target 90
  ]
  edge [
    source 91
    target 92
  ]
  edge [
    source 93
    target 94
  ]
  edge [
    source 93
    target 97
  ]
  edge [
    source 94
    target 97
  ]
  edge [
    source 95
    target 96
  ]
  edge [
    source 98
    target 99
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 103
  ]
  edge [
    source 101
    target 104
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 104
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 107
    target 108
  ]
  edge [
    source 107
    target 122
  ]
  edge [
    source 107
    target 125
  ]
  edge [
    source 108
    target 122
  ]
  edge [
    source 108
    target 125
  ]
  edge [
    source 109
    target 110
  ]
  edge [
    source 109
    target 111
  ]
  edge [
    source 109
    target 112
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 110
    target 112
  ]
  edge [
    source 110
    target 113
  ]
  edge [
    source 110
    target 114
  ]
  edge [
    source 110
    target 115
  ]
  edge [
    source 110
    target 116
  ]
  edge [
    source 110
    target 117
  ]
  edge [
    source 110
    target 118
  ]
  edge [
    source 111
    target 112
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 113
    target 115
  ]
  edge [
    source 113
    target 116
  ]
  edge [
    source 113
    target 117
  ]
  edge [
    source 113
    target 118
  ]
  edge [
    source 114
    target 115
  ]
  edge [
    source 114
    target 116
  ]
  edge [
    source 114
    target 117
  ]
  edge [
    source 114
    target 118
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 117
  ]
  edge [
    source 115
    target 118
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 116
    target 118
  ]
  edge [
    source 117
    target 118
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 119
    target 121
  ]
  edge [
    source 119
    target 122
  ]
  edge [
    source 119
    target 123
  ]
  edge [
    source 119
    target 124
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 122
  ]
  edge [
    source 120
    target 123
  ]
  edge [
    source 120
    target 124
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 123
  ]
  edge [
    source 121
    target 124
  ]
  edge [
    source 122
    target 123
  ]
  edge [
    source 122
    target 124
  ]
  edge [
    source 122
    target 125
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 126
    target 127
  ]
  edge [
    source 126
    target 128
  ]
  edge [
    source 126
    target 129
  ]
  edge [
    source 126
    target 130
  ]
  edge [
    source 126
    target 131
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 129
  ]
  edge [
    source 127
    target 130
  ]
  edge [
    source 127
    target 131
  ]
  edge [
    source 128
    target 129
  ]
  edge [
    source 128
    target 130
  ]
  edge [
    source 128
    target 131
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 131
  ]
  edge [
    source 130
    target 131
  ]
]
