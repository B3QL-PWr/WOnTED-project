graph [
  node [
    id 0
    label "aleksander"
    origin "text"
  ]
  node [
    id 1
    label "rymkiewicz"
    origin "text"
  ]
  node [
    id 2
    label "Aleksandra"
  ]
  node [
    id 3
    label "Rymkiewicz"
  ]
  node [
    id 4
    label "zeszyt"
  ]
  node [
    id 5
    label "nar&#243;d"
  ]
  node [
    id 6
    label "polskie"
  ]
  node [
    id 7
    label "drzewo"
  ]
  node [
    id 8
    label "promie&#324;"
  ]
  node [
    id 9
    label "dla"
  ]
  node [
    id 10
    label "artysta"
  ]
  node [
    id 11
    label "przygoda"
  ]
  node [
    id 12
    label "Gucio"
  ]
  node [
    id 13
    label "pingwin"
  ]
  node [
    id 14
    label "wyb&#243;r"
  ]
  node [
    id 15
    label "poezje"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
]
