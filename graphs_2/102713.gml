graph [
  node [
    id 0
    label "filler"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "wiecz&#243;r"
    origin "text"
  ]
  node [
    id 3
    label "xxx"
    origin "text"
  ]
  node [
    id 4
    label "drzewo_owocowe"
  ]
  node [
    id 5
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 6
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 7
    label "skuteczny"
  ]
  node [
    id 8
    label "ca&#322;y"
  ]
  node [
    id 9
    label "czw&#243;rka"
  ]
  node [
    id 10
    label "spokojny"
  ]
  node [
    id 11
    label "pos&#322;uszny"
  ]
  node [
    id 12
    label "korzystny"
  ]
  node [
    id 13
    label "drogi"
  ]
  node [
    id 14
    label "pozytywny"
  ]
  node [
    id 15
    label "moralny"
  ]
  node [
    id 16
    label "pomy&#347;lny"
  ]
  node [
    id 17
    label "powitanie"
  ]
  node [
    id 18
    label "grzeczny"
  ]
  node [
    id 19
    label "&#347;mieszny"
  ]
  node [
    id 20
    label "odpowiedni"
  ]
  node [
    id 21
    label "zwrot"
  ]
  node [
    id 22
    label "dobrze"
  ]
  node [
    id 23
    label "dobroczynny"
  ]
  node [
    id 24
    label "mi&#322;y"
  ]
  node [
    id 25
    label "etycznie"
  ]
  node [
    id 26
    label "moralnie"
  ]
  node [
    id 27
    label "warto&#347;ciowy"
  ]
  node [
    id 28
    label "taki"
  ]
  node [
    id 29
    label "stosownie"
  ]
  node [
    id 30
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 31
    label "prawdziwy"
  ]
  node [
    id 32
    label "typowy"
  ]
  node [
    id 33
    label "zasadniczy"
  ]
  node [
    id 34
    label "charakterystyczny"
  ]
  node [
    id 35
    label "uprawniony"
  ]
  node [
    id 36
    label "nale&#380;yty"
  ]
  node [
    id 37
    label "ten"
  ]
  node [
    id 38
    label "nale&#380;ny"
  ]
  node [
    id 39
    label "pozytywnie"
  ]
  node [
    id 40
    label "fajny"
  ]
  node [
    id 41
    label "przyjemny"
  ]
  node [
    id 42
    label "po&#380;&#261;dany"
  ]
  node [
    id 43
    label "dodatnio"
  ]
  node [
    id 44
    label "o&#347;mieszenie"
  ]
  node [
    id 45
    label "o&#347;mieszanie"
  ]
  node [
    id 46
    label "&#347;miesznie"
  ]
  node [
    id 47
    label "nieadekwatny"
  ]
  node [
    id 48
    label "bawny"
  ]
  node [
    id 49
    label "niepowa&#380;ny"
  ]
  node [
    id 50
    label "dziwny"
  ]
  node [
    id 51
    label "uspokojenie_si&#281;"
  ]
  node [
    id 52
    label "wolny"
  ]
  node [
    id 53
    label "bezproblemowy"
  ]
  node [
    id 54
    label "uspokajanie_si&#281;"
  ]
  node [
    id 55
    label "spokojnie"
  ]
  node [
    id 56
    label "uspokojenie"
  ]
  node [
    id 57
    label "nietrudny"
  ]
  node [
    id 58
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 59
    label "cicho"
  ]
  node [
    id 60
    label "uspokajanie"
  ]
  node [
    id 61
    label "pos&#322;usznie"
  ]
  node [
    id 62
    label "zale&#380;ny"
  ]
  node [
    id 63
    label "uleg&#322;y"
  ]
  node [
    id 64
    label "konserwatywny"
  ]
  node [
    id 65
    label "stosowny"
  ]
  node [
    id 66
    label "grzecznie"
  ]
  node [
    id 67
    label "nijaki"
  ]
  node [
    id 68
    label "niewinny"
  ]
  node [
    id 69
    label "korzystnie"
  ]
  node [
    id 70
    label "cz&#322;owiek"
  ]
  node [
    id 71
    label "przyjaciel"
  ]
  node [
    id 72
    label "bliski"
  ]
  node [
    id 73
    label "drogo"
  ]
  node [
    id 74
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 75
    label "kompletny"
  ]
  node [
    id 76
    label "zdr&#243;w"
  ]
  node [
    id 77
    label "ca&#322;o"
  ]
  node [
    id 78
    label "du&#380;y"
  ]
  node [
    id 79
    label "calu&#347;ko"
  ]
  node [
    id 80
    label "podobny"
  ]
  node [
    id 81
    label "&#380;ywy"
  ]
  node [
    id 82
    label "pe&#322;ny"
  ]
  node [
    id 83
    label "jedyny"
  ]
  node [
    id 84
    label "sprawny"
  ]
  node [
    id 85
    label "skutkowanie"
  ]
  node [
    id 86
    label "poskutkowanie"
  ]
  node [
    id 87
    label "skutecznie"
  ]
  node [
    id 88
    label "pomy&#347;lnie"
  ]
  node [
    id 89
    label "zbi&#243;r"
  ]
  node [
    id 90
    label "przedtrzonowiec"
  ]
  node [
    id 91
    label "trafienie"
  ]
  node [
    id 92
    label "osada"
  ]
  node [
    id 93
    label "blotka"
  ]
  node [
    id 94
    label "p&#322;yta_winylowa"
  ]
  node [
    id 95
    label "cyfra"
  ]
  node [
    id 96
    label "pok&#243;j"
  ]
  node [
    id 97
    label "obiekt"
  ]
  node [
    id 98
    label "stopie&#324;"
  ]
  node [
    id 99
    label "arkusz_drukarski"
  ]
  node [
    id 100
    label "zaprz&#281;g"
  ]
  node [
    id 101
    label "toto-lotek"
  ]
  node [
    id 102
    label "&#263;wiartka"
  ]
  node [
    id 103
    label "&#322;&#243;dka"
  ]
  node [
    id 104
    label "four"
  ]
  node [
    id 105
    label "minialbum"
  ]
  node [
    id 106
    label "hotel"
  ]
  node [
    id 107
    label "punkt"
  ]
  node [
    id 108
    label "zmiana"
  ]
  node [
    id 109
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 110
    label "turn"
  ]
  node [
    id 111
    label "wyra&#380;enie"
  ]
  node [
    id 112
    label "fraza_czasownikowa"
  ]
  node [
    id 113
    label "turning"
  ]
  node [
    id 114
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 115
    label "skr&#281;t"
  ]
  node [
    id 116
    label "jednostka_leksykalna"
  ]
  node [
    id 117
    label "obr&#243;t"
  ]
  node [
    id 118
    label "spotkanie"
  ]
  node [
    id 119
    label "pozdrowienie"
  ]
  node [
    id 120
    label "welcome"
  ]
  node [
    id 121
    label "zwyczaj"
  ]
  node [
    id 122
    label "greeting"
  ]
  node [
    id 123
    label "zdarzony"
  ]
  node [
    id 124
    label "odpowiednio"
  ]
  node [
    id 125
    label "specjalny"
  ]
  node [
    id 126
    label "odpowiadanie"
  ]
  node [
    id 127
    label "wybranek"
  ]
  node [
    id 128
    label "sk&#322;onny"
  ]
  node [
    id 129
    label "kochanek"
  ]
  node [
    id 130
    label "mi&#322;o"
  ]
  node [
    id 131
    label "dyplomata"
  ]
  node [
    id 132
    label "umi&#322;owany"
  ]
  node [
    id 133
    label "kochanie"
  ]
  node [
    id 134
    label "przyjemnie"
  ]
  node [
    id 135
    label "wiele"
  ]
  node [
    id 136
    label "lepiej"
  ]
  node [
    id 137
    label "dobroczynnie"
  ]
  node [
    id 138
    label "spo&#322;eczny"
  ]
  node [
    id 139
    label "zach&#243;d"
  ]
  node [
    id 140
    label "night"
  ]
  node [
    id 141
    label "przyj&#281;cie"
  ]
  node [
    id 142
    label "dzie&#324;"
  ]
  node [
    id 143
    label "pora"
  ]
  node [
    id 144
    label "vesper"
  ]
  node [
    id 145
    label "wzi&#281;cie"
  ]
  node [
    id 146
    label "wpuszczenie"
  ]
  node [
    id 147
    label "w&#322;&#261;czenie"
  ]
  node [
    id 148
    label "stanie_si&#281;"
  ]
  node [
    id 149
    label "entertainment"
  ]
  node [
    id 150
    label "presumption"
  ]
  node [
    id 151
    label "zobowi&#261;zanie_si&#281;"
  ]
  node [
    id 152
    label "dopuszczenie"
  ]
  node [
    id 153
    label "impreza"
  ]
  node [
    id 154
    label "credence"
  ]
  node [
    id 155
    label "party"
  ]
  node [
    id 156
    label "poch&#322;oni&#281;cie"
  ]
  node [
    id 157
    label "uznanie"
  ]
  node [
    id 158
    label "reception"
  ]
  node [
    id 159
    label "zgodzenie_si&#281;"
  ]
  node [
    id 160
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 161
    label "przyj&#261;&#263;"
  ]
  node [
    id 162
    label "umieszczenie"
  ]
  node [
    id 163
    label "zrobienie"
  ]
  node [
    id 164
    label "zareagowanie"
  ]
  node [
    id 165
    label "match"
  ]
  node [
    id 166
    label "spotkanie_si&#281;"
  ]
  node [
    id 167
    label "gather"
  ]
  node [
    id 168
    label "spowodowanie"
  ]
  node [
    id 169
    label "zawarcie"
  ]
  node [
    id 170
    label "zdarzenie_si&#281;"
  ]
  node [
    id 171
    label "po&#380;egnanie"
  ]
  node [
    id 172
    label "spotykanie"
  ]
  node [
    id 173
    label "wydarzenie"
  ]
  node [
    id 174
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 175
    label "gathering"
  ]
  node [
    id 176
    label "doznanie"
  ]
  node [
    id 177
    label "znalezienie"
  ]
  node [
    id 178
    label "employment"
  ]
  node [
    id 179
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 180
    label "znajomy"
  ]
  node [
    id 181
    label "czas"
  ]
  node [
    id 182
    label "okres_czasu"
  ]
  node [
    id 183
    label "run"
  ]
  node [
    id 184
    label "sunset"
  ]
  node [
    id 185
    label "obszar"
  ]
  node [
    id 186
    label "trud"
  ]
  node [
    id 187
    label "zjawisko"
  ]
  node [
    id 188
    label "s&#322;o&#324;ce"
  ]
  node [
    id 189
    label "strona_&#347;wiata"
  ]
  node [
    id 190
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 191
    label "szar&#243;wka"
  ]
  node [
    id 192
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 193
    label "usi&#322;owanie"
  ]
  node [
    id 194
    label "long_time"
  ]
  node [
    id 195
    label "czynienie_si&#281;"
  ]
  node [
    id 196
    label "noc"
  ]
  node [
    id 197
    label "t&#322;usty_czwartek"
  ]
  node [
    id 198
    label "podwiecz&#243;r"
  ]
  node [
    id 199
    label "ranek"
  ]
  node [
    id 200
    label "po&#322;udnie"
  ]
  node [
    id 201
    label "Sylwester"
  ]
  node [
    id 202
    label "godzina"
  ]
  node [
    id 203
    label "popo&#322;udnie"
  ]
  node [
    id 204
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 205
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 206
    label "walentynki"
  ]
  node [
    id 207
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 208
    label "przedpo&#322;udnie"
  ]
  node [
    id 209
    label "wzej&#347;cie"
  ]
  node [
    id 210
    label "wstanie"
  ]
  node [
    id 211
    label "przedwiecz&#243;r"
  ]
  node [
    id 212
    label "rano"
  ]
  node [
    id 213
    label "termin"
  ]
  node [
    id 214
    label "tydzie&#324;"
  ]
  node [
    id 215
    label "day"
  ]
  node [
    id 216
    label "doba"
  ]
  node [
    id 217
    label "wsta&#263;"
  ]
  node [
    id 218
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 219
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
]
