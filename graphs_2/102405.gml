graph [
  node [
    id 0
    label "nasz"
    origin "text"
  ]
  node [
    id 1
    label "woodstock"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 3
    label "totalnie"
    origin "text"
  ]
  node [
    id 4
    label "przer&#261;ba&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "media"
    origin "text"
  ]
  node [
    id 7
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 8
    label "traktowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "nasa"
    origin "text"
  ]
  node [
    id 10
    label "jak"
    origin "text"
  ]
  node [
    id 11
    label "banda"
    origin "text"
  ]
  node [
    id 12
    label "oszo&#322;om"
    origin "text"
  ]
  node [
    id 13
    label "oburza&#263;"
    origin "text"
  ]
  node [
    id 14
    label "si&#281;"
    origin "text"
  ]
  node [
    id 15
    label "ch&#322;opak"
    origin "text"
  ]
  node [
    id 16
    label "dred"
    origin "text"
  ]
  node [
    id 17
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 18
    label "teraz"
    origin "text"
  ]
  node [
    id 19
    label "sprostowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "siada&#263;"
    origin "text"
  ]
  node [
    id 21
    label "pisz"
    origin "text"
  ]
  node [
    id 22
    label "gazeta"
    origin "text"
  ]
  node [
    id 23
    label "powiedzie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 25
    label "machn&#261;&#263;"
    origin "text"
  ]
  node [
    id 26
    label "fajny"
    origin "text"
  ]
  node [
    id 27
    label "tekst"
    origin "text"
  ]
  node [
    id 28
    label "opublikowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 29
    label "inny"
    origin "text"
  ]
  node [
    id 30
    label "qmamie"
    origin "text"
  ]
  node [
    id 31
    label "festiwalowy"
    origin "text"
  ]
  node [
    id 32
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 33
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 34
    label "serwis"
    origin "text"
  ]
  node [
    id 35
    label "mama"
    origin "text"
  ]
  node [
    id 36
    label "plac"
    origin "text"
  ]
  node [
    id 37
    label "czyj&#347;"
  ]
  node [
    id 38
    label "prywatny"
  ]
  node [
    id 39
    label "m&#261;&#380;"
  ]
  node [
    id 40
    label "ma&#322;&#380;onek"
  ]
  node [
    id 41
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 42
    label "ch&#322;op"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "pan_m&#322;ody"
  ]
  node [
    id 45
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 46
    label "&#347;lubny"
  ]
  node [
    id 47
    label "pan_domu"
  ]
  node [
    id 48
    label "pan_i_w&#322;adca"
  ]
  node [
    id 49
    label "stary"
  ]
  node [
    id 50
    label "totalitarny"
  ]
  node [
    id 51
    label "absolutnie"
  ]
  node [
    id 52
    label "totalny"
  ]
  node [
    id 53
    label "totalitarnie"
  ]
  node [
    id 54
    label "absolutny"
  ]
  node [
    id 55
    label "kompletny"
  ]
  node [
    id 56
    label "wielki"
  ]
  node [
    id 57
    label "sko&#324;czenie"
  ]
  node [
    id 58
    label "jednoznacznie"
  ]
  node [
    id 59
    label "wholly"
  ]
  node [
    id 60
    label "completely"
  ]
  node [
    id 61
    label "absolutystyczny"
  ]
  node [
    id 62
    label "bezwzgl&#281;dny"
  ]
  node [
    id 63
    label "bezspornie"
  ]
  node [
    id 64
    label "bezwzgl&#281;dnie"
  ]
  node [
    id 65
    label "zupe&#322;nie"
  ]
  node [
    id 66
    label "unambiguously"
  ]
  node [
    id 67
    label "nieograniczenie"
  ]
  node [
    id 68
    label "zdecydowanie"
  ]
  node [
    id 69
    label "niezale&#380;nie"
  ]
  node [
    id 70
    label "nieodwo&#322;alnie"
  ]
  node [
    id 71
    label "wyci&#261;&#263;"
  ]
  node [
    id 72
    label "przeci&#261;&#263;"
  ]
  node [
    id 73
    label "wyr&#261;ba&#263;"
  ]
  node [
    id 74
    label "wykona&#263;"
  ]
  node [
    id 75
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 76
    label "r&#261;bn&#261;&#263;"
  ]
  node [
    id 77
    label "fall"
  ]
  node [
    id 78
    label "oddzieli&#263;"
  ]
  node [
    id 79
    label "wymordowa&#263;"
  ]
  node [
    id 80
    label "strzeli&#263;"
  ]
  node [
    id 81
    label "wydzieli&#263;"
  ]
  node [
    id 82
    label "usun&#261;&#263;"
  ]
  node [
    id 83
    label "zrobi&#263;"
  ]
  node [
    id 84
    label "skopiowa&#263;"
  ]
  node [
    id 85
    label "uderzy&#263;"
  ]
  node [
    id 86
    label "cut"
  ]
  node [
    id 87
    label "write_out"
  ]
  node [
    id 88
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 89
    label "bro&#324;_palna"
  ]
  node [
    id 90
    label "paln&#261;&#263;"
  ]
  node [
    id 91
    label "wy&#380;&#322;obi&#263;"
  ]
  node [
    id 92
    label "traverse"
  ]
  node [
    id 93
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 94
    label "zablokowa&#263;"
  ]
  node [
    id 95
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 96
    label "uci&#261;&#263;"
  ]
  node [
    id 97
    label "traversal"
  ]
  node [
    id 98
    label "przej&#347;&#263;"
  ]
  node [
    id 99
    label "naruszy&#263;"
  ]
  node [
    id 100
    label "przebi&#263;"
  ]
  node [
    id 101
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 102
    label "przedzieli&#263;"
  ]
  node [
    id 103
    label "przerwa&#263;"
  ]
  node [
    id 104
    label "wydoby&#263;"
  ]
  node [
    id 105
    label "zniszczy&#263;"
  ]
  node [
    id 106
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 107
    label "majority"
  ]
  node [
    id 108
    label "Rzym_Zachodni"
  ]
  node [
    id 109
    label "whole"
  ]
  node [
    id 110
    label "ilo&#347;&#263;"
  ]
  node [
    id 111
    label "element"
  ]
  node [
    id 112
    label "Rzym_Wschodni"
  ]
  node [
    id 113
    label "urz&#261;dzenie"
  ]
  node [
    id 114
    label "mass-media"
  ]
  node [
    id 115
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 116
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 117
    label "przekazior"
  ]
  node [
    id 118
    label "uzbrajanie"
  ]
  node [
    id 119
    label "medium"
  ]
  node [
    id 120
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 121
    label "&#347;rodek"
  ]
  node [
    id 122
    label "jasnowidz"
  ]
  node [
    id 123
    label "hipnoza"
  ]
  node [
    id 124
    label "spirytysta"
  ]
  node [
    id 125
    label "otoczenie"
  ]
  node [
    id 126
    label "publikator"
  ]
  node [
    id 127
    label "warunki"
  ]
  node [
    id 128
    label "strona"
  ]
  node [
    id 129
    label "przeka&#378;nik"
  ]
  node [
    id 130
    label "&#347;rodek_przekazu"
  ]
  node [
    id 131
    label "armament"
  ]
  node [
    id 132
    label "arming"
  ]
  node [
    id 133
    label "instalacja"
  ]
  node [
    id 134
    label "wyposa&#380;anie"
  ]
  node [
    id 135
    label "dozbrajanie"
  ]
  node [
    id 136
    label "dozbrojenie"
  ]
  node [
    id 137
    label "montowanie"
  ]
  node [
    id 138
    label "publicysta"
  ]
  node [
    id 139
    label "nowiniarz"
  ]
  node [
    id 140
    label "bran&#380;owiec"
  ]
  node [
    id 141
    label "akredytowanie"
  ]
  node [
    id 142
    label "akredytowa&#263;"
  ]
  node [
    id 143
    label "Korwin"
  ]
  node [
    id 144
    label "Michnik"
  ]
  node [
    id 145
    label "Conrad"
  ]
  node [
    id 146
    label "intelektualista"
  ]
  node [
    id 147
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 148
    label "autor"
  ]
  node [
    id 149
    label "Gogol"
  ]
  node [
    id 150
    label "pracownik"
  ]
  node [
    id 151
    label "fachowiec"
  ]
  node [
    id 152
    label "zwi&#261;zkowiec"
  ]
  node [
    id 153
    label "nowinkarz"
  ]
  node [
    id 154
    label "uwiarygodnia&#263;"
  ]
  node [
    id 155
    label "pozwoli&#263;"
  ]
  node [
    id 156
    label "attest"
  ]
  node [
    id 157
    label "uwiarygodni&#263;"
  ]
  node [
    id 158
    label "zezwala&#263;"
  ]
  node [
    id 159
    label "upowa&#380;nia&#263;"
  ]
  node [
    id 160
    label "upowa&#380;ni&#263;"
  ]
  node [
    id 161
    label "zezwalanie"
  ]
  node [
    id 162
    label "uwiarygodnienie"
  ]
  node [
    id 163
    label "akredytowanie_si&#281;"
  ]
  node [
    id 164
    label "upowa&#380;nianie"
  ]
  node [
    id 165
    label "accreditation"
  ]
  node [
    id 166
    label "pozwolenie"
  ]
  node [
    id 167
    label "uwiarygodnianie"
  ]
  node [
    id 168
    label "upowa&#380;nienie"
  ]
  node [
    id 169
    label "robi&#263;"
  ]
  node [
    id 170
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 171
    label "poddawa&#263;"
  ]
  node [
    id 172
    label "dotyczy&#263;"
  ]
  node [
    id 173
    label "use"
  ]
  node [
    id 174
    label "treat"
  ]
  node [
    id 175
    label "oferowa&#263;"
  ]
  node [
    id 176
    label "introduce"
  ]
  node [
    id 177
    label "deliver"
  ]
  node [
    id 178
    label "opowiada&#263;"
  ]
  node [
    id 179
    label "krzywdzi&#263;"
  ]
  node [
    id 180
    label "organizowa&#263;"
  ]
  node [
    id 181
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 182
    label "czyni&#263;"
  ]
  node [
    id 183
    label "give"
  ]
  node [
    id 184
    label "stylizowa&#263;"
  ]
  node [
    id 185
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 186
    label "falowa&#263;"
  ]
  node [
    id 187
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 188
    label "peddle"
  ]
  node [
    id 189
    label "praca"
  ]
  node [
    id 190
    label "wydala&#263;"
  ]
  node [
    id 191
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 192
    label "tentegowa&#263;"
  ]
  node [
    id 193
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 194
    label "urz&#261;dza&#263;"
  ]
  node [
    id 195
    label "oszukiwa&#263;"
  ]
  node [
    id 196
    label "work"
  ]
  node [
    id 197
    label "ukazywa&#263;"
  ]
  node [
    id 198
    label "przerabia&#263;"
  ]
  node [
    id 199
    label "act"
  ]
  node [
    id 200
    label "post&#281;powa&#263;"
  ]
  node [
    id 201
    label "podpowiada&#263;"
  ]
  node [
    id 202
    label "render"
  ]
  node [
    id 203
    label "decydowa&#263;"
  ]
  node [
    id 204
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 205
    label "rezygnowa&#263;"
  ]
  node [
    id 206
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 207
    label "bargain"
  ]
  node [
    id 208
    label "tycze&#263;"
  ]
  node [
    id 209
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 210
    label "zobo"
  ]
  node [
    id 211
    label "yakalo"
  ]
  node [
    id 212
    label "byd&#322;o"
  ]
  node [
    id 213
    label "dzo"
  ]
  node [
    id 214
    label "kr&#281;torogie"
  ]
  node [
    id 215
    label "zbi&#243;r"
  ]
  node [
    id 216
    label "g&#322;owa"
  ]
  node [
    id 217
    label "czochrad&#322;o"
  ]
  node [
    id 218
    label "posp&#243;lstwo"
  ]
  node [
    id 219
    label "kraal"
  ]
  node [
    id 220
    label "livestock"
  ]
  node [
    id 221
    label "prze&#380;uwacz"
  ]
  node [
    id 222
    label "zebu"
  ]
  node [
    id 223
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 224
    label "bizon"
  ]
  node [
    id 225
    label "byd&#322;o_domowe"
  ]
  node [
    id 226
    label "towarzystwo"
  ]
  node [
    id 227
    label "gang"
  ]
  node [
    id 228
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 229
    label "crew"
  ]
  node [
    id 230
    label "granda"
  ]
  node [
    id 231
    label "organizacja"
  ]
  node [
    id 232
    label "ogrodzenie"
  ]
  node [
    id 233
    label "gromada"
  ]
  node [
    id 234
    label "band"
  ]
  node [
    id 235
    label "package"
  ]
  node [
    id 236
    label "st&#243;&#322;_bilardowy"
  ]
  node [
    id 237
    label "kraw&#281;d&#378;"
  ]
  node [
    id 238
    label "flight"
  ]
  node [
    id 239
    label "podmiot"
  ]
  node [
    id 240
    label "jednostka_organizacyjna"
  ]
  node [
    id 241
    label "struktura"
  ]
  node [
    id 242
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 243
    label "TOPR"
  ]
  node [
    id 244
    label "endecki"
  ]
  node [
    id 245
    label "zesp&#243;&#322;"
  ]
  node [
    id 246
    label "przedstawicielstwo"
  ]
  node [
    id 247
    label "od&#322;am"
  ]
  node [
    id 248
    label "Cepelia"
  ]
  node [
    id 249
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 250
    label "ZBoWiD"
  ]
  node [
    id 251
    label "organization"
  ]
  node [
    id 252
    label "centrala"
  ]
  node [
    id 253
    label "GOPR"
  ]
  node [
    id 254
    label "ZOMO"
  ]
  node [
    id 255
    label "ZMP"
  ]
  node [
    id 256
    label "komitet_koordynacyjny"
  ]
  node [
    id 257
    label "przybud&#243;wka"
  ]
  node [
    id 258
    label "boj&#243;wka"
  ]
  node [
    id 259
    label "graf"
  ]
  node [
    id 260
    label "para"
  ]
  node [
    id 261
    label "narta"
  ]
  node [
    id 262
    label "ochraniacz"
  ]
  node [
    id 263
    label "poj&#281;cie"
  ]
  node [
    id 264
    label "end"
  ]
  node [
    id 265
    label "koniec"
  ]
  node [
    id 266
    label "sytuacja"
  ]
  node [
    id 267
    label "grodza"
  ]
  node [
    id 268
    label "okalanie"
  ]
  node [
    id 269
    label "railing"
  ]
  node [
    id 270
    label "przeszkoda"
  ]
  node [
    id 271
    label "przestrze&#324;"
  ]
  node [
    id 272
    label "reservation"
  ]
  node [
    id 273
    label "wjazd"
  ]
  node [
    id 274
    label "wskok"
  ]
  node [
    id 275
    label "limitation"
  ]
  node [
    id 276
    label "bramka"
  ]
  node [
    id 277
    label "rzecz"
  ]
  node [
    id 278
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 279
    label "Chewra_Kadisza"
  ]
  node [
    id 280
    label "partnership"
  ]
  node [
    id 281
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 282
    label "asystencja"
  ]
  node [
    id 283
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 284
    label "wi&#281;&#378;"
  ]
  node [
    id 285
    label "Rotary_International"
  ]
  node [
    id 286
    label "fabianie"
  ]
  node [
    id 287
    label "Eleusis"
  ]
  node [
    id 288
    label "obecno&#347;&#263;"
  ]
  node [
    id 289
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 290
    label "Monar"
  ]
  node [
    id 291
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 292
    label "grupa"
  ]
  node [
    id 293
    label "grono"
  ]
  node [
    id 294
    label "typ"
  ]
  node [
    id 295
    label "jednostka_administracyjna"
  ]
  node [
    id 296
    label "zoologia"
  ]
  node [
    id 297
    label "skupienie"
  ]
  node [
    id 298
    label "jednostka_systematyczna"
  ]
  node [
    id 299
    label "kr&#243;lestwo"
  ]
  node [
    id 300
    label "stage_set"
  ]
  node [
    id 301
    label "tribe"
  ]
  node [
    id 302
    label "hurma"
  ]
  node [
    id 303
    label "botanika"
  ]
  node [
    id 304
    label "criminalism"
  ]
  node [
    id 305
    label "przest&#281;pstwo"
  ]
  node [
    id 306
    label "bezprawie"
  ]
  node [
    id 307
    label "problem_spo&#322;eczny"
  ]
  node [
    id 308
    label "patologia"
  ]
  node [
    id 309
    label "gangster"
  ]
  node [
    id 310
    label "smr&#243;d"
  ]
  node [
    id 311
    label "cyrk"
  ]
  node [
    id 312
    label "nadu&#380;ycie"
  ]
  node [
    id 313
    label "paczka"
  ]
  node [
    id 314
    label "argument"
  ]
  node [
    id 315
    label "sensacja"
  ]
  node [
    id 316
    label "chory_na_g&#322;ow&#281;"
  ]
  node [
    id 317
    label "fanatyk"
  ]
  node [
    id 318
    label "pasjonat"
  ]
  node [
    id 319
    label "obsesjonista"
  ]
  node [
    id 320
    label "zwolennik"
  ]
  node [
    id 321
    label "radyka&#322;"
  ]
  node [
    id 322
    label "dzia&#322;a&#263;_na_nerwy"
  ]
  node [
    id 323
    label "disgust"
  ]
  node [
    id 324
    label "porusza&#263;"
  ]
  node [
    id 325
    label "podnosi&#263;"
  ]
  node [
    id 326
    label "move"
  ]
  node [
    id 327
    label "go"
  ]
  node [
    id 328
    label "drive"
  ]
  node [
    id 329
    label "meet"
  ]
  node [
    id 330
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 331
    label "powodowa&#263;"
  ]
  node [
    id 332
    label "wzbudza&#263;"
  ]
  node [
    id 333
    label "porobi&#263;"
  ]
  node [
    id 334
    label "g&#243;wniarz"
  ]
  node [
    id 335
    label "synek"
  ]
  node [
    id 336
    label "boyfriend"
  ]
  node [
    id 337
    label "okrzos"
  ]
  node [
    id 338
    label "dziecko"
  ]
  node [
    id 339
    label "sympatia"
  ]
  node [
    id 340
    label "usynowienie"
  ]
  node [
    id 341
    label "pomocnik"
  ]
  node [
    id 342
    label "kawaler"
  ]
  node [
    id 343
    label "pederasta"
  ]
  node [
    id 344
    label "m&#322;odzieniec"
  ]
  node [
    id 345
    label "kajtek"
  ]
  node [
    id 346
    label "&#347;l&#261;ski"
  ]
  node [
    id 347
    label "usynawianie"
  ]
  node [
    id 348
    label "utulenie"
  ]
  node [
    id 349
    label "pediatra"
  ]
  node [
    id 350
    label "dzieciak"
  ]
  node [
    id 351
    label "utulanie"
  ]
  node [
    id 352
    label "dzieciarnia"
  ]
  node [
    id 353
    label "niepe&#322;noletni"
  ]
  node [
    id 354
    label "organizm"
  ]
  node [
    id 355
    label "utula&#263;"
  ]
  node [
    id 356
    label "cz&#322;owieczek"
  ]
  node [
    id 357
    label "fledgling"
  ]
  node [
    id 358
    label "zwierz&#281;"
  ]
  node [
    id 359
    label "utuli&#263;"
  ]
  node [
    id 360
    label "m&#322;odzik"
  ]
  node [
    id 361
    label "pedofil"
  ]
  node [
    id 362
    label "m&#322;odziak"
  ]
  node [
    id 363
    label "potomek"
  ]
  node [
    id 364
    label "entliczek-pentliczek"
  ]
  node [
    id 365
    label "potomstwo"
  ]
  node [
    id 366
    label "sraluch"
  ]
  node [
    id 367
    label "ludzko&#347;&#263;"
  ]
  node [
    id 368
    label "asymilowanie"
  ]
  node [
    id 369
    label "wapniak"
  ]
  node [
    id 370
    label "asymilowa&#263;"
  ]
  node [
    id 371
    label "os&#322;abia&#263;"
  ]
  node [
    id 372
    label "posta&#263;"
  ]
  node [
    id 373
    label "hominid"
  ]
  node [
    id 374
    label "podw&#322;adny"
  ]
  node [
    id 375
    label "os&#322;abianie"
  ]
  node [
    id 376
    label "figura"
  ]
  node [
    id 377
    label "portrecista"
  ]
  node [
    id 378
    label "dwun&#243;g"
  ]
  node [
    id 379
    label "profanum"
  ]
  node [
    id 380
    label "mikrokosmos"
  ]
  node [
    id 381
    label "nasada"
  ]
  node [
    id 382
    label "duch"
  ]
  node [
    id 383
    label "antropochoria"
  ]
  node [
    id 384
    label "osoba"
  ]
  node [
    id 385
    label "wz&#243;r"
  ]
  node [
    id 386
    label "senior"
  ]
  node [
    id 387
    label "oddzia&#322;ywanie"
  ]
  node [
    id 388
    label "Adam"
  ]
  node [
    id 389
    label "homo_sapiens"
  ]
  node [
    id 390
    label "polifag"
  ]
  node [
    id 391
    label "emocja"
  ]
  node [
    id 392
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 393
    label "partner"
  ]
  node [
    id 394
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 395
    label "love"
  ]
  node [
    id 396
    label "kredens"
  ]
  node [
    id 397
    label "zawodnik"
  ]
  node [
    id 398
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 399
    label "bylina"
  ]
  node [
    id 400
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 401
    label "gracz"
  ]
  node [
    id 402
    label "r&#281;ka"
  ]
  node [
    id 403
    label "pomoc"
  ]
  node [
    id 404
    label "wrzosowate"
  ]
  node [
    id 405
    label "pomagacz"
  ]
  node [
    id 406
    label "junior"
  ]
  node [
    id 407
    label "junak"
  ]
  node [
    id 408
    label "m&#322;odzie&#380;"
  ]
  node [
    id 409
    label "mo&#322;ojec"
  ]
  node [
    id 410
    label "m&#322;okos"
  ]
  node [
    id 411
    label "smarkateria"
  ]
  node [
    id 412
    label "ch&#322;opiec"
  ]
  node [
    id 413
    label "kawa&#322;ek"
  ]
  node [
    id 414
    label "gej"
  ]
  node [
    id 415
    label "cug"
  ]
  node [
    id 416
    label "krepel"
  ]
  node [
    id 417
    label "francuz"
  ]
  node [
    id 418
    label "mietlorz"
  ]
  node [
    id 419
    label "etnolekt"
  ]
  node [
    id 420
    label "sza&#322;ot"
  ]
  node [
    id 421
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 422
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 423
    label "regionalny"
  ]
  node [
    id 424
    label "polski"
  ]
  node [
    id 425
    label "halba"
  ]
  node [
    id 426
    label "buchta"
  ]
  node [
    id 427
    label "czarne_kluski"
  ]
  node [
    id 428
    label "szpajza"
  ]
  node [
    id 429
    label "szl&#261;ski"
  ]
  node [
    id 430
    label "&#347;lonski"
  ]
  node [
    id 431
    label "po_&#347;l&#261;sku"
  ]
  node [
    id 432
    label "waloszek"
  ]
  node [
    id 433
    label "tytu&#322;"
  ]
  node [
    id 434
    label "order"
  ]
  node [
    id 435
    label "zalotnik"
  ]
  node [
    id 436
    label "kawalerka"
  ]
  node [
    id 437
    label "rycerz"
  ]
  node [
    id 438
    label "odznaczenie"
  ]
  node [
    id 439
    label "nie&#380;onaty"
  ]
  node [
    id 440
    label "zakon_rycerski"
  ]
  node [
    id 441
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 442
    label "zakonnik"
  ]
  node [
    id 443
    label "przysposobienie"
  ]
  node [
    id 444
    label "syn"
  ]
  node [
    id 445
    label "adoption"
  ]
  node [
    id 446
    label "przysposabianie"
  ]
  node [
    id 447
    label "dredy"
  ]
  node [
    id 448
    label "pasemko"
  ]
  node [
    id 449
    label "fryzura"
  ]
  node [
    id 450
    label "pasmo"
  ]
  node [
    id 451
    label "rastaman"
  ]
  node [
    id 452
    label "by&#263;"
  ]
  node [
    id 453
    label "gotowy"
  ]
  node [
    id 454
    label "might"
  ]
  node [
    id 455
    label "uprawi&#263;"
  ]
  node [
    id 456
    label "public_treasury"
  ]
  node [
    id 457
    label "pole"
  ]
  node [
    id 458
    label "obrobi&#263;"
  ]
  node [
    id 459
    label "nietrze&#378;wy"
  ]
  node [
    id 460
    label "czekanie"
  ]
  node [
    id 461
    label "martwy"
  ]
  node [
    id 462
    label "bliski"
  ]
  node [
    id 463
    label "gotowo"
  ]
  node [
    id 464
    label "przygotowywanie"
  ]
  node [
    id 465
    label "przygotowanie"
  ]
  node [
    id 466
    label "dyspozycyjny"
  ]
  node [
    id 467
    label "zalany"
  ]
  node [
    id 468
    label "nieuchronny"
  ]
  node [
    id 469
    label "doj&#347;cie"
  ]
  node [
    id 470
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 471
    label "mie&#263;_miejsce"
  ]
  node [
    id 472
    label "equal"
  ]
  node [
    id 473
    label "trwa&#263;"
  ]
  node [
    id 474
    label "chodzi&#263;"
  ]
  node [
    id 475
    label "si&#281;ga&#263;"
  ]
  node [
    id 476
    label "stan"
  ]
  node [
    id 477
    label "stand"
  ]
  node [
    id 478
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 479
    label "uczestniczy&#263;"
  ]
  node [
    id 480
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 481
    label "chwila"
  ]
  node [
    id 482
    label "time"
  ]
  node [
    id 483
    label "czas"
  ]
  node [
    id 484
    label "skorygowa&#263;"
  ]
  node [
    id 485
    label "correct"
  ]
  node [
    id 486
    label "nastroi&#263;"
  ]
  node [
    id 487
    label "poprawi&#263;"
  ]
  node [
    id 488
    label "revise"
  ]
  node [
    id 489
    label "instrument_muzyczny"
  ]
  node [
    id 490
    label "perch"
  ]
  node [
    id 491
    label "sit"
  ]
  node [
    id 492
    label "zajmowa&#263;"
  ]
  node [
    id 493
    label "plasowa&#263;_si&#281;"
  ]
  node [
    id 494
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 495
    label "l&#261;dowa&#263;"
  ]
  node [
    id 496
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 497
    label "bring"
  ]
  node [
    id 498
    label "psu&#263;_si&#281;"
  ]
  node [
    id 499
    label "dostarcza&#263;"
  ]
  node [
    id 500
    label "korzysta&#263;"
  ]
  node [
    id 501
    label "schorzenie"
  ]
  node [
    id 502
    label "komornik"
  ]
  node [
    id 503
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 504
    label "return"
  ]
  node [
    id 505
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 506
    label "bra&#263;"
  ]
  node [
    id 507
    label "rozciekawia&#263;"
  ]
  node [
    id 508
    label "klasyfikacja"
  ]
  node [
    id 509
    label "zadawa&#263;"
  ]
  node [
    id 510
    label "fill"
  ]
  node [
    id 511
    label "zabiera&#263;"
  ]
  node [
    id 512
    label "topographic_point"
  ]
  node [
    id 513
    label "obejmowa&#263;"
  ]
  node [
    id 514
    label "pali&#263;_si&#281;"
  ]
  node [
    id 515
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 516
    label "aim"
  ]
  node [
    id 517
    label "anektowa&#263;"
  ]
  node [
    id 518
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 519
    label "prosecute"
  ]
  node [
    id 520
    label "sake"
  ]
  node [
    id 521
    label "do"
  ]
  node [
    id 522
    label "zaczyna&#263;"
  ]
  node [
    id 523
    label "set_about"
  ]
  node [
    id 524
    label "wchodzi&#263;"
  ]
  node [
    id 525
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 526
    label "submit"
  ]
  node [
    id 527
    label "land"
  ]
  node [
    id 528
    label "finish_up"
  ]
  node [
    id 529
    label "przybywa&#263;"
  ]
  node [
    id 530
    label "trafia&#263;"
  ]
  node [
    id 531
    label "radzi&#263;_sobie"
  ]
  node [
    id 532
    label "sitowate"
  ]
  node [
    id 533
    label "ro&#347;lina_zielna"
  ]
  node [
    id 534
    label "redakcja"
  ]
  node [
    id 535
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 536
    label "czasopismo"
  ]
  node [
    id 537
    label "prasa"
  ]
  node [
    id 538
    label "egzemplarz"
  ]
  node [
    id 539
    label "psychotest"
  ]
  node [
    id 540
    label "pismo"
  ]
  node [
    id 541
    label "communication"
  ]
  node [
    id 542
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 543
    label "wk&#322;ad"
  ]
  node [
    id 544
    label "zajawka"
  ]
  node [
    id 545
    label "ok&#322;adka"
  ]
  node [
    id 546
    label "Zwrotnica"
  ]
  node [
    id 547
    label "dzia&#322;"
  ]
  node [
    id 548
    label "redaktor"
  ]
  node [
    id 549
    label "radio"
  ]
  node [
    id 550
    label "siedziba"
  ]
  node [
    id 551
    label "composition"
  ]
  node [
    id 552
    label "wydawnictwo"
  ]
  node [
    id 553
    label "redaction"
  ]
  node [
    id 554
    label "telewizja"
  ]
  node [
    id 555
    label "obr&#243;bka"
  ]
  node [
    id 556
    label "debit"
  ]
  node [
    id 557
    label "druk"
  ]
  node [
    id 558
    label "publikacja"
  ]
  node [
    id 559
    label "nadtytu&#322;"
  ]
  node [
    id 560
    label "szata_graficzna"
  ]
  node [
    id 561
    label "tytulatura"
  ]
  node [
    id 562
    label "wydawa&#263;"
  ]
  node [
    id 563
    label "elevation"
  ]
  node [
    id 564
    label "wyda&#263;"
  ]
  node [
    id 565
    label "mianowaniec"
  ]
  node [
    id 566
    label "poster"
  ]
  node [
    id 567
    label "nazwa"
  ]
  node [
    id 568
    label "podtytu&#322;"
  ]
  node [
    id 569
    label "centerfold"
  ]
  node [
    id 570
    label "t&#322;oczysko"
  ]
  node [
    id 571
    label "depesza"
  ]
  node [
    id 572
    label "maszyna"
  ]
  node [
    id 573
    label "napisa&#263;"
  ]
  node [
    id 574
    label "dziennikarz_prasowy"
  ]
  node [
    id 575
    label "pisa&#263;"
  ]
  node [
    id 576
    label "kiosk"
  ]
  node [
    id 577
    label "maszyna_rolnicza"
  ]
  node [
    id 578
    label "take_care"
  ]
  node [
    id 579
    label "troska&#263;_si&#281;"
  ]
  node [
    id 580
    label "rozpatrywa&#263;"
  ]
  node [
    id 581
    label "zamierza&#263;"
  ]
  node [
    id 582
    label "argue"
  ]
  node [
    id 583
    label "os&#261;dza&#263;"
  ]
  node [
    id 584
    label "strike"
  ]
  node [
    id 585
    label "s&#261;dzi&#263;"
  ]
  node [
    id 586
    label "znajdowa&#263;"
  ]
  node [
    id 587
    label "hold"
  ]
  node [
    id 588
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 589
    label "przeprowadza&#263;"
  ]
  node [
    id 590
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 591
    label "consider"
  ]
  node [
    id 592
    label "volunteer"
  ]
  node [
    id 593
    label "waln&#261;&#263;"
  ]
  node [
    id 594
    label "rap"
  ]
  node [
    id 595
    label "merdn&#261;&#263;"
  ]
  node [
    id 596
    label "ruszy&#263;"
  ]
  node [
    id 597
    label "sway"
  ]
  node [
    id 598
    label "tick"
  ]
  node [
    id 599
    label "odpieprzy&#263;"
  ]
  node [
    id 600
    label "motivate"
  ]
  node [
    id 601
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 602
    label "zabra&#263;"
  ]
  node [
    id 603
    label "allude"
  ]
  node [
    id 604
    label "spowodowa&#263;"
  ]
  node [
    id 605
    label "stimulate"
  ]
  node [
    id 606
    label "zacz&#261;&#263;"
  ]
  node [
    id 607
    label "wzbudzi&#263;"
  ]
  node [
    id 608
    label "urazi&#263;"
  ]
  node [
    id 609
    label "wystartowa&#263;"
  ]
  node [
    id 610
    label "przywali&#263;"
  ]
  node [
    id 611
    label "dupn&#261;&#263;"
  ]
  node [
    id 612
    label "skrytykowa&#263;"
  ]
  node [
    id 613
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 614
    label "nast&#261;pi&#263;"
  ]
  node [
    id 615
    label "wstrzeli&#263;_si&#281;"
  ]
  node [
    id 616
    label "sztachn&#261;&#263;"
  ]
  node [
    id 617
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 618
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 619
    label "crush"
  ]
  node [
    id 620
    label "uderzy&#263;_do_panny"
  ]
  node [
    id 621
    label "postara&#263;_si&#281;"
  ]
  node [
    id 622
    label "hopn&#261;&#263;"
  ]
  node [
    id 623
    label "zada&#263;"
  ]
  node [
    id 624
    label "uda&#263;_si&#281;"
  ]
  node [
    id 625
    label "dotkn&#261;&#263;"
  ]
  node [
    id 626
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 627
    label "anoint"
  ]
  node [
    id 628
    label "transgress"
  ]
  node [
    id 629
    label "chop"
  ]
  node [
    id 630
    label "jebn&#261;&#263;"
  ]
  node [
    id 631
    label "lumber"
  ]
  node [
    id 632
    label "sygn&#261;&#263;"
  ]
  node [
    id 633
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 634
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 635
    label "zmieni&#263;"
  ]
  node [
    id 636
    label "dzia&#322;o"
  ]
  node [
    id 637
    label "majdn&#261;&#263;"
  ]
  node [
    id 638
    label "wydosta&#263;_si&#281;"
  ]
  node [
    id 639
    label "wag"
  ]
  node [
    id 640
    label "odb&#281;bni&#263;"
  ]
  node [
    id 641
    label "muzyka_rozrywkowa"
  ]
  node [
    id 642
    label "karpiowate"
  ]
  node [
    id 643
    label "ryba"
  ]
  node [
    id 644
    label "czarna_muzyka"
  ]
  node [
    id 645
    label "drapie&#380;nik"
  ]
  node [
    id 646
    label "asp"
  ]
  node [
    id 647
    label "byczy"
  ]
  node [
    id 648
    label "fajnie"
  ]
  node [
    id 649
    label "klawy"
  ]
  node [
    id 650
    label "dobry"
  ]
  node [
    id 651
    label "dobroczynny"
  ]
  node [
    id 652
    label "czw&#243;rka"
  ]
  node [
    id 653
    label "spokojny"
  ]
  node [
    id 654
    label "skuteczny"
  ]
  node [
    id 655
    label "&#347;mieszny"
  ]
  node [
    id 656
    label "mi&#322;y"
  ]
  node [
    id 657
    label "grzeczny"
  ]
  node [
    id 658
    label "powitanie"
  ]
  node [
    id 659
    label "dobrze"
  ]
  node [
    id 660
    label "ca&#322;y"
  ]
  node [
    id 661
    label "zwrot"
  ]
  node [
    id 662
    label "pomy&#347;lny"
  ]
  node [
    id 663
    label "moralny"
  ]
  node [
    id 664
    label "drogi"
  ]
  node [
    id 665
    label "pozytywny"
  ]
  node [
    id 666
    label "odpowiedni"
  ]
  node [
    id 667
    label "korzystny"
  ]
  node [
    id 668
    label "pos&#322;uszny"
  ]
  node [
    id 669
    label "na_schwa&#322;"
  ]
  node [
    id 670
    label "klawo"
  ]
  node [
    id 671
    label "byczo"
  ]
  node [
    id 672
    label "podobny"
  ]
  node [
    id 673
    label "du&#380;y"
  ]
  node [
    id 674
    label "ekscerpcja"
  ]
  node [
    id 675
    label "j&#281;zykowo"
  ]
  node [
    id 676
    label "wypowied&#378;"
  ]
  node [
    id 677
    label "wytw&#243;r"
  ]
  node [
    id 678
    label "pomini&#281;cie"
  ]
  node [
    id 679
    label "dzie&#322;o"
  ]
  node [
    id 680
    label "preparacja"
  ]
  node [
    id 681
    label "odmianka"
  ]
  node [
    id 682
    label "opu&#347;ci&#263;"
  ]
  node [
    id 683
    label "koniektura"
  ]
  node [
    id 684
    label "obelga"
  ]
  node [
    id 685
    label "przedmiot"
  ]
  node [
    id 686
    label "p&#322;&#243;d"
  ]
  node [
    id 687
    label "rezultat"
  ]
  node [
    id 688
    label "obrazowanie"
  ]
  node [
    id 689
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 690
    label "dorobek"
  ]
  node [
    id 691
    label "forma"
  ]
  node [
    id 692
    label "tre&#347;&#263;"
  ]
  node [
    id 693
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 694
    label "retrospektywa"
  ]
  node [
    id 695
    label "works"
  ]
  node [
    id 696
    label "creation"
  ]
  node [
    id 697
    label "tetralogia"
  ]
  node [
    id 698
    label "komunikat"
  ]
  node [
    id 699
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 700
    label "pos&#322;uchanie"
  ]
  node [
    id 701
    label "s&#261;d"
  ]
  node [
    id 702
    label "sparafrazowanie"
  ]
  node [
    id 703
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 704
    label "strawestowa&#263;"
  ]
  node [
    id 705
    label "sparafrazowa&#263;"
  ]
  node [
    id 706
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 707
    label "trawestowa&#263;"
  ]
  node [
    id 708
    label "sformu&#322;owanie"
  ]
  node [
    id 709
    label "parafrazowanie"
  ]
  node [
    id 710
    label "ozdobnik"
  ]
  node [
    id 711
    label "delimitacja"
  ]
  node [
    id 712
    label "parafrazowa&#263;"
  ]
  node [
    id 713
    label "stylizacja"
  ]
  node [
    id 714
    label "trawestowanie"
  ]
  node [
    id 715
    label "strawestowanie"
  ]
  node [
    id 716
    label "cholera"
  ]
  node [
    id 717
    label "ubliga"
  ]
  node [
    id 718
    label "niedorobek"
  ]
  node [
    id 719
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 720
    label "chuj"
  ]
  node [
    id 721
    label "bluzg"
  ]
  node [
    id 722
    label "wyzwisko"
  ]
  node [
    id 723
    label "indignation"
  ]
  node [
    id 724
    label "pies"
  ]
  node [
    id 725
    label "wrzuta"
  ]
  node [
    id 726
    label "chujowy"
  ]
  node [
    id 727
    label "krzywda"
  ]
  node [
    id 728
    label "szmata"
  ]
  node [
    id 729
    label "formu&#322;owa&#263;"
  ]
  node [
    id 730
    label "ozdabia&#263;"
  ]
  node [
    id 731
    label "stawia&#263;"
  ]
  node [
    id 732
    label "spell"
  ]
  node [
    id 733
    label "styl"
  ]
  node [
    id 734
    label "skryba"
  ]
  node [
    id 735
    label "read"
  ]
  node [
    id 736
    label "donosi&#263;"
  ]
  node [
    id 737
    label "code"
  ]
  node [
    id 738
    label "dysgrafia"
  ]
  node [
    id 739
    label "dysortografia"
  ]
  node [
    id 740
    label "tworzy&#263;"
  ]
  node [
    id 741
    label "odmiana"
  ]
  node [
    id 742
    label "preparation"
  ]
  node [
    id 743
    label "proces_technologiczny"
  ]
  node [
    id 744
    label "uj&#281;cie"
  ]
  node [
    id 745
    label "wyforowa&#263;_si&#281;"
  ]
  node [
    id 746
    label "pozostawi&#263;"
  ]
  node [
    id 747
    label "obni&#380;y&#263;"
  ]
  node [
    id 748
    label "zostawi&#263;"
  ]
  node [
    id 749
    label "przesta&#263;"
  ]
  node [
    id 750
    label "potani&#263;"
  ]
  node [
    id 751
    label "drop"
  ]
  node [
    id 752
    label "evacuate"
  ]
  node [
    id 753
    label "humiliate"
  ]
  node [
    id 754
    label "leave"
  ]
  node [
    id 755
    label "straci&#263;"
  ]
  node [
    id 756
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 757
    label "authorize"
  ]
  node [
    id 758
    label "omin&#261;&#263;"
  ]
  node [
    id 759
    label "u&#380;ytkownik"
  ]
  node [
    id 760
    label "komunikacyjnie"
  ]
  node [
    id 761
    label "przypuszczenie"
  ]
  node [
    id 762
    label "conjecture"
  ]
  node [
    id 763
    label "wniosek"
  ]
  node [
    id 764
    label "wyb&#243;r"
  ]
  node [
    id 765
    label "dokumentacja"
  ]
  node [
    id 766
    label "ellipsis"
  ]
  node [
    id 767
    label "wykluczenie"
  ]
  node [
    id 768
    label "figura_my&#347;li"
  ]
  node [
    id 769
    label "zrobienie"
  ]
  node [
    id 770
    label "kolejny"
  ]
  node [
    id 771
    label "osobno"
  ]
  node [
    id 772
    label "r&#243;&#380;ny"
  ]
  node [
    id 773
    label "inszy"
  ]
  node [
    id 774
    label "inaczej"
  ]
  node [
    id 775
    label "odr&#281;bny"
  ]
  node [
    id 776
    label "nast&#281;pnie"
  ]
  node [
    id 777
    label "nastopny"
  ]
  node [
    id 778
    label "kolejno"
  ]
  node [
    id 779
    label "kt&#243;ry&#347;"
  ]
  node [
    id 780
    label "jaki&#347;"
  ]
  node [
    id 781
    label "r&#243;&#380;nie"
  ]
  node [
    id 782
    label "niestandardowo"
  ]
  node [
    id 783
    label "individually"
  ]
  node [
    id 784
    label "udzielnie"
  ]
  node [
    id 785
    label "osobnie"
  ]
  node [
    id 786
    label "odr&#281;bnie"
  ]
  node [
    id 787
    label "osobny"
  ]
  node [
    id 788
    label "doba"
  ]
  node [
    id 789
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 790
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 791
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 792
    label "jednocze&#347;nie"
  ]
  node [
    id 793
    label "tydzie&#324;"
  ]
  node [
    id 794
    label "noc"
  ]
  node [
    id 795
    label "dzie&#324;"
  ]
  node [
    id 796
    label "godzina"
  ]
  node [
    id 797
    label "long_time"
  ]
  node [
    id 798
    label "jednostka_geologiczna"
  ]
  node [
    id 799
    label "punkt"
  ]
  node [
    id 800
    label "YouTube"
  ]
  node [
    id 801
    label "zak&#322;ad"
  ]
  node [
    id 802
    label "uderzenie"
  ]
  node [
    id 803
    label "service"
  ]
  node [
    id 804
    label "us&#322;uga"
  ]
  node [
    id 805
    label "porcja"
  ]
  node [
    id 806
    label "zastawa"
  ]
  node [
    id 807
    label "mecz"
  ]
  node [
    id 808
    label "doniesienie"
  ]
  node [
    id 809
    label "instrumentalizacja"
  ]
  node [
    id 810
    label "trafienie"
  ]
  node [
    id 811
    label "walka"
  ]
  node [
    id 812
    label "cios"
  ]
  node [
    id 813
    label "zdarzenie_si&#281;"
  ]
  node [
    id 814
    label "wdarcie_si&#281;"
  ]
  node [
    id 815
    label "pogorszenie"
  ]
  node [
    id 816
    label "d&#378;wi&#281;k"
  ]
  node [
    id 817
    label "poczucie"
  ]
  node [
    id 818
    label "coup"
  ]
  node [
    id 819
    label "reakcja"
  ]
  node [
    id 820
    label "contact"
  ]
  node [
    id 821
    label "stukni&#281;cie"
  ]
  node [
    id 822
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 823
    label "bat"
  ]
  node [
    id 824
    label "spowodowanie"
  ]
  node [
    id 825
    label "rush"
  ]
  node [
    id 826
    label "odbicie"
  ]
  node [
    id 827
    label "dawka"
  ]
  node [
    id 828
    label "zadanie"
  ]
  node [
    id 829
    label "&#347;ci&#281;cie"
  ]
  node [
    id 830
    label "st&#322;uczenie"
  ]
  node [
    id 831
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 832
    label "odbicie_si&#281;"
  ]
  node [
    id 833
    label "dotkni&#281;cie"
  ]
  node [
    id 834
    label "charge"
  ]
  node [
    id 835
    label "dostanie"
  ]
  node [
    id 836
    label "skrytykowanie"
  ]
  node [
    id 837
    label "zagrywka"
  ]
  node [
    id 838
    label "manewr"
  ]
  node [
    id 839
    label "nast&#261;pienie"
  ]
  node [
    id 840
    label "uderzanie"
  ]
  node [
    id 841
    label "pogoda"
  ]
  node [
    id 842
    label "stroke"
  ]
  node [
    id 843
    label "pobicie"
  ]
  node [
    id 844
    label "ruch"
  ]
  node [
    id 845
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 846
    label "flap"
  ]
  node [
    id 847
    label "dotyk"
  ]
  node [
    id 848
    label "produkt_gotowy"
  ]
  node [
    id 849
    label "asortyment"
  ]
  node [
    id 850
    label "czynno&#347;&#263;"
  ]
  node [
    id 851
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 852
    label "&#347;wiadczenie"
  ]
  node [
    id 853
    label "element_wyposa&#380;enia"
  ]
  node [
    id 854
    label "sto&#322;owizna"
  ]
  node [
    id 855
    label "zas&#243;b"
  ]
  node [
    id 856
    label "&#380;o&#322;d"
  ]
  node [
    id 857
    label "zak&#322;adka"
  ]
  node [
    id 858
    label "miejsce_pracy"
  ]
  node [
    id 859
    label "instytucja"
  ]
  node [
    id 860
    label "wyko&#324;czenie"
  ]
  node [
    id 861
    label "firma"
  ]
  node [
    id 862
    label "czyn"
  ]
  node [
    id 863
    label "company"
  ]
  node [
    id 864
    label "instytut"
  ]
  node [
    id 865
    label "umowa"
  ]
  node [
    id 866
    label "po&#322;o&#380;enie"
  ]
  node [
    id 867
    label "sprawa"
  ]
  node [
    id 868
    label "ust&#281;p"
  ]
  node [
    id 869
    label "plan"
  ]
  node [
    id 870
    label "obiekt_matematyczny"
  ]
  node [
    id 871
    label "problemat"
  ]
  node [
    id 872
    label "plamka"
  ]
  node [
    id 873
    label "stopie&#324;_pisma"
  ]
  node [
    id 874
    label "jednostka"
  ]
  node [
    id 875
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 876
    label "miejsce"
  ]
  node [
    id 877
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 878
    label "mark"
  ]
  node [
    id 879
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 880
    label "prosta"
  ]
  node [
    id 881
    label "problematyka"
  ]
  node [
    id 882
    label "obiekt"
  ]
  node [
    id 883
    label "zapunktowa&#263;"
  ]
  node [
    id 884
    label "podpunkt"
  ]
  node [
    id 885
    label "wojsko"
  ]
  node [
    id 886
    label "kres"
  ]
  node [
    id 887
    label "point"
  ]
  node [
    id 888
    label "pozycja"
  ]
  node [
    id 889
    label "obrona"
  ]
  node [
    id 890
    label "gra"
  ]
  node [
    id 891
    label "game"
  ]
  node [
    id 892
    label "serw"
  ]
  node [
    id 893
    label "dwumecz"
  ]
  node [
    id 894
    label "kartka"
  ]
  node [
    id 895
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 896
    label "logowanie"
  ]
  node [
    id 897
    label "plik"
  ]
  node [
    id 898
    label "adres_internetowy"
  ]
  node [
    id 899
    label "linia"
  ]
  node [
    id 900
    label "serwis_internetowy"
  ]
  node [
    id 901
    label "bok"
  ]
  node [
    id 902
    label "skr&#281;canie"
  ]
  node [
    id 903
    label "skr&#281;ca&#263;"
  ]
  node [
    id 904
    label "orientowanie"
  ]
  node [
    id 905
    label "skr&#281;ci&#263;"
  ]
  node [
    id 906
    label "zorientowanie"
  ]
  node [
    id 907
    label "ty&#322;"
  ]
  node [
    id 908
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 909
    label "fragment"
  ]
  node [
    id 910
    label "layout"
  ]
  node [
    id 911
    label "zorientowa&#263;"
  ]
  node [
    id 912
    label "pagina"
  ]
  node [
    id 913
    label "g&#243;ra"
  ]
  node [
    id 914
    label "orientowa&#263;"
  ]
  node [
    id 915
    label "voice"
  ]
  node [
    id 916
    label "orientacja"
  ]
  node [
    id 917
    label "prz&#243;d"
  ]
  node [
    id 918
    label "internet"
  ]
  node [
    id 919
    label "powierzchnia"
  ]
  node [
    id 920
    label "skr&#281;cenie"
  ]
  node [
    id 921
    label "do&#322;&#261;czenie"
  ]
  node [
    id 922
    label "message"
  ]
  node [
    id 923
    label "naznoszenie"
  ]
  node [
    id 924
    label "zawiadomienie"
  ]
  node [
    id 925
    label "zniesienie"
  ]
  node [
    id 926
    label "zaniesienie"
  ]
  node [
    id 927
    label "announcement"
  ]
  node [
    id 928
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 929
    label "fetch"
  ]
  node [
    id 930
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 931
    label "poinformowanie"
  ]
  node [
    id 932
    label "przodkini"
  ]
  node [
    id 933
    label "matka_zast&#281;pcza"
  ]
  node [
    id 934
    label "matczysko"
  ]
  node [
    id 935
    label "rodzice"
  ]
  node [
    id 936
    label "stara"
  ]
  node [
    id 937
    label "macierz"
  ]
  node [
    id 938
    label "rodzic"
  ]
  node [
    id 939
    label "Matka_Boska"
  ]
  node [
    id 940
    label "macocha"
  ]
  node [
    id 941
    label "starzy"
  ]
  node [
    id 942
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 943
    label "pokolenie"
  ]
  node [
    id 944
    label "wapniaki"
  ]
  node [
    id 945
    label "opiekun"
  ]
  node [
    id 946
    label "rodzic_chrzestny"
  ]
  node [
    id 947
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 948
    label "krewna"
  ]
  node [
    id 949
    label "matka"
  ]
  node [
    id 950
    label "&#380;ona"
  ]
  node [
    id 951
    label "kobieta"
  ]
  node [
    id 952
    label "partnerka"
  ]
  node [
    id 953
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 954
    label "matuszka"
  ]
  node [
    id 955
    label "parametryzacja"
  ]
  node [
    id 956
    label "pa&#324;stwo"
  ]
  node [
    id 957
    label "mod"
  ]
  node [
    id 958
    label "patriota"
  ]
  node [
    id 959
    label "m&#281;&#380;atka"
  ]
  node [
    id 960
    label "&#321;ubianka"
  ]
  node [
    id 961
    label "area"
  ]
  node [
    id 962
    label "Majdan"
  ]
  node [
    id 963
    label "pole_bitwy"
  ]
  node [
    id 964
    label "stoisko"
  ]
  node [
    id 965
    label "obszar"
  ]
  node [
    id 966
    label "pierzeja"
  ]
  node [
    id 967
    label "obiekt_handlowy"
  ]
  node [
    id 968
    label "zgromadzenie"
  ]
  node [
    id 969
    label "miasto"
  ]
  node [
    id 970
    label "targowica"
  ]
  node [
    id 971
    label "kram"
  ]
  node [
    id 972
    label "p&#243;&#322;noc"
  ]
  node [
    id 973
    label "Kosowo"
  ]
  node [
    id 974
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 975
    label "Zab&#322;ocie"
  ]
  node [
    id 976
    label "zach&#243;d"
  ]
  node [
    id 977
    label "po&#322;udnie"
  ]
  node [
    id 978
    label "Pow&#261;zki"
  ]
  node [
    id 979
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 980
    label "Piotrowo"
  ]
  node [
    id 981
    label "Olszanica"
  ]
  node [
    id 982
    label "holarktyka"
  ]
  node [
    id 983
    label "Ruda_Pabianicka"
  ]
  node [
    id 984
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 985
    label "Ludwin&#243;w"
  ]
  node [
    id 986
    label "Arktyka"
  ]
  node [
    id 987
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 988
    label "Zabu&#380;e"
  ]
  node [
    id 989
    label "antroposfera"
  ]
  node [
    id 990
    label "terytorium"
  ]
  node [
    id 991
    label "Neogea"
  ]
  node [
    id 992
    label "Syberia_Zachodnia"
  ]
  node [
    id 993
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 994
    label "zakres"
  ]
  node [
    id 995
    label "pas_planetoid"
  ]
  node [
    id 996
    label "Syberia_Wschodnia"
  ]
  node [
    id 997
    label "Antarktyka"
  ]
  node [
    id 998
    label "Rakowice"
  ]
  node [
    id 999
    label "akrecja"
  ]
  node [
    id 1000
    label "wymiar"
  ]
  node [
    id 1001
    label "&#321;&#281;g"
  ]
  node [
    id 1002
    label "Kresy_Zachodnie"
  ]
  node [
    id 1003
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 1004
    label "wsch&#243;d"
  ]
  node [
    id 1005
    label "Notogea"
  ]
  node [
    id 1006
    label "rozdzielanie"
  ]
  node [
    id 1007
    label "bezbrze&#380;e"
  ]
  node [
    id 1008
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1009
    label "niezmierzony"
  ]
  node [
    id 1010
    label "przedzielenie"
  ]
  node [
    id 1011
    label "nielito&#347;ciwy"
  ]
  node [
    id 1012
    label "rozdziela&#263;"
  ]
  node [
    id 1013
    label "oktant"
  ]
  node [
    id 1014
    label "przestw&#243;r"
  ]
  node [
    id 1015
    label "concourse"
  ]
  node [
    id 1016
    label "gathering"
  ]
  node [
    id 1017
    label "wsp&#243;lnota"
  ]
  node [
    id 1018
    label "spotkanie"
  ]
  node [
    id 1019
    label "organ"
  ]
  node [
    id 1020
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 1021
    label "gromadzenie"
  ]
  node [
    id 1022
    label "templum"
  ]
  node [
    id 1023
    label "konwentykiel"
  ]
  node [
    id 1024
    label "klasztor"
  ]
  node [
    id 1025
    label "caucus"
  ]
  node [
    id 1026
    label "pozyskanie"
  ]
  node [
    id 1027
    label "kongregacja"
  ]
  node [
    id 1028
    label "ulica"
  ]
  node [
    id 1029
    label "targ"
  ]
  node [
    id 1030
    label "szmartuz"
  ]
  node [
    id 1031
    label "kramnica"
  ]
  node [
    id 1032
    label "Brunszwik"
  ]
  node [
    id 1033
    label "Twer"
  ]
  node [
    id 1034
    label "Marki"
  ]
  node [
    id 1035
    label "Tarnopol"
  ]
  node [
    id 1036
    label "Czerkiesk"
  ]
  node [
    id 1037
    label "Johannesburg"
  ]
  node [
    id 1038
    label "Nowogr&#243;d"
  ]
  node [
    id 1039
    label "Heidelberg"
  ]
  node [
    id 1040
    label "Korsze"
  ]
  node [
    id 1041
    label "Chocim"
  ]
  node [
    id 1042
    label "Lenzen"
  ]
  node [
    id 1043
    label "Bie&#322;gorod"
  ]
  node [
    id 1044
    label "Hebron"
  ]
  node [
    id 1045
    label "Korynt"
  ]
  node [
    id 1046
    label "Pemba"
  ]
  node [
    id 1047
    label "Norfolk"
  ]
  node [
    id 1048
    label "Tarragona"
  ]
  node [
    id 1049
    label "Loreto"
  ]
  node [
    id 1050
    label "Wielki_Pres&#322;aw"
  ]
  node [
    id 1051
    label "Paczk&#243;w"
  ]
  node [
    id 1052
    label "Krasnodar"
  ]
  node [
    id 1053
    label "Hadziacz"
  ]
  node [
    id 1054
    label "Cymlansk"
  ]
  node [
    id 1055
    label "Efez"
  ]
  node [
    id 1056
    label "Kandahar"
  ]
  node [
    id 1057
    label "&#346;wiebodzice"
  ]
  node [
    id 1058
    label "Antwerpia"
  ]
  node [
    id 1059
    label "Baltimore"
  ]
  node [
    id 1060
    label "Eger"
  ]
  node [
    id 1061
    label "Cumana"
  ]
  node [
    id 1062
    label "Kanton"
  ]
  node [
    id 1063
    label "Sarat&#243;w"
  ]
  node [
    id 1064
    label "Siena"
  ]
  node [
    id 1065
    label "Dubno"
  ]
  node [
    id 1066
    label "Tyl&#380;a"
  ]
  node [
    id 1067
    label "Frankfurt_nad_Odr&#261;"
  ]
  node [
    id 1068
    label "Pi&#324;sk"
  ]
  node [
    id 1069
    label "Toledo"
  ]
  node [
    id 1070
    label "Piza"
  ]
  node [
    id 1071
    label "Triest"
  ]
  node [
    id 1072
    label "Struga"
  ]
  node [
    id 1073
    label "Gettysburg"
  ]
  node [
    id 1074
    label "Sierdobsk"
  ]
  node [
    id 1075
    label "Xai-Xai"
  ]
  node [
    id 1076
    label "Bristol"
  ]
  node [
    id 1077
    label "Katania"
  ]
  node [
    id 1078
    label "Parma"
  ]
  node [
    id 1079
    label "Tel_Awiw-Jafa"
  ]
  node [
    id 1080
    label "Dniepropetrowsk"
  ]
  node [
    id 1081
    label "Tours"
  ]
  node [
    id 1082
    label "Mohylew"
  ]
  node [
    id 1083
    label "Suzdal"
  ]
  node [
    id 1084
    label "Samara"
  ]
  node [
    id 1085
    label "Akerman"
  ]
  node [
    id 1086
    label "Szk&#322;&#243;w"
  ]
  node [
    id 1087
    label "Chimoio"
  ]
  node [
    id 1088
    label "Perm"
  ]
  node [
    id 1089
    label "Murma&#324;sk"
  ]
  node [
    id 1090
    label "Z&#322;oczew"
  ]
  node [
    id 1091
    label "Reda"
  ]
  node [
    id 1092
    label "Korsu&#324;_Szewczenkowski"
  ]
  node [
    id 1093
    label "Aleksandria"
  ]
  node [
    id 1094
    label "Kowel"
  ]
  node [
    id 1095
    label "Hamburg"
  ]
  node [
    id 1096
    label "Rudki"
  ]
  node [
    id 1097
    label "O&#322;omuniec"
  ]
  node [
    id 1098
    label "Kowno"
  ]
  node [
    id 1099
    label "Luksor"
  ]
  node [
    id 1100
    label "Cremona"
  ]
  node [
    id 1101
    label "Suczawa"
  ]
  node [
    id 1102
    label "M&#252;nster"
  ]
  node [
    id 1103
    label "Peszawar"
  ]
  node [
    id 1104
    label "Los_Angeles"
  ]
  node [
    id 1105
    label "Szawle"
  ]
  node [
    id 1106
    label "Winnica"
  ]
  node [
    id 1107
    label "I&#322;awka"
  ]
  node [
    id 1108
    label "Poniatowa"
  ]
  node [
    id 1109
    label "Ko&#322;omyja"
  ]
  node [
    id 1110
    label "Asy&#380;"
  ]
  node [
    id 1111
    label "Tolkmicko"
  ]
  node [
    id 1112
    label "Orlean"
  ]
  node [
    id 1113
    label "Koper"
  ]
  node [
    id 1114
    label "Le&#324;sk"
  ]
  node [
    id 1115
    label "Rostock"
  ]
  node [
    id 1116
    label "Mantua"
  ]
  node [
    id 1117
    label "Barcelona"
  ]
  node [
    id 1118
    label "Mo&#347;ciska"
  ]
  node [
    id 1119
    label "Koluszki"
  ]
  node [
    id 1120
    label "Stalingrad"
  ]
  node [
    id 1121
    label "Fergana"
  ]
  node [
    id 1122
    label "A&#322;czewsk"
  ]
  node [
    id 1123
    label "Kaszyn"
  ]
  node [
    id 1124
    label "S&#322;awia&#324;sk_nad_Kubaniem"
  ]
  node [
    id 1125
    label "B&#322;agowieszcze&#324;sk"
  ]
  node [
    id 1126
    label "D&#252;sseldorf"
  ]
  node [
    id 1127
    label "Mozyrz"
  ]
  node [
    id 1128
    label "Syrakuzy"
  ]
  node [
    id 1129
    label "Peszt"
  ]
  node [
    id 1130
    label "Lichinga"
  ]
  node [
    id 1131
    label "Bogusz&#243;w-Gorce"
  ]
  node [
    id 1132
    label "Choroszcz"
  ]
  node [
    id 1133
    label "Po&#322;ock"
  ]
  node [
    id 1134
    label "Cherso&#324;"
  ]
  node [
    id 1135
    label "Fryburg"
  ]
  node [
    id 1136
    label "Izmir"
  ]
  node [
    id 1137
    label "Jawor&#243;w"
  ]
  node [
    id 1138
    label "Wenecja"
  ]
  node [
    id 1139
    label "Kordoba"
  ]
  node [
    id 1140
    label "Mrocza"
  ]
  node [
    id 1141
    label "Solikamsk"
  ]
  node [
    id 1142
    label "Be&#322;z"
  ]
  node [
    id 1143
    label "Wo&#322;gograd"
  ]
  node [
    id 1144
    label "&#379;ar&#243;w"
  ]
  node [
    id 1145
    label "Brugia"
  ]
  node [
    id 1146
    label "Radk&#243;w"
  ]
  node [
    id 1147
    label "Wielkie_Tyrnowo"
  ]
  node [
    id 1148
    label "Harbin"
  ]
  node [
    id 1149
    label "&#379;&#243;&#322;kiew"
  ]
  node [
    id 1150
    label "Zaporo&#380;e"
  ]
  node [
    id 1151
    label "Smorgonie"
  ]
  node [
    id 1152
    label "Nowa_D&#281;ba"
  ]
  node [
    id 1153
    label "Aktobe"
  ]
  node [
    id 1154
    label "Ussuryjsk"
  ]
  node [
    id 1155
    label "Mo&#380;ajsk"
  ]
  node [
    id 1156
    label "Tanger"
  ]
  node [
    id 1157
    label "Nowogard"
  ]
  node [
    id 1158
    label "Utrecht"
  ]
  node [
    id 1159
    label "Czerniejewo"
  ]
  node [
    id 1160
    label "Bazylea"
  ]
  node [
    id 1161
    label "miejscowo&#347;&#263;"
  ]
  node [
    id 1162
    label "Nie&#347;wie&#380;"
  ]
  node [
    id 1163
    label "Tu&#322;a"
  ]
  node [
    id 1164
    label "Al-Kufa"
  ]
  node [
    id 1165
    label "Jutrosin"
  ]
  node [
    id 1166
    label "Czelabi&#324;sk"
  ]
  node [
    id 1167
    label "Stoczek_&#321;ukowski"
  ]
  node [
    id 1168
    label "Split"
  ]
  node [
    id 1169
    label "Czerniowce"
  ]
  node [
    id 1170
    label "Majsur"
  ]
  node [
    id 1171
    label "Poczdam"
  ]
  node [
    id 1172
    label "Troick"
  ]
  node [
    id 1173
    label "Minusi&#324;sk"
  ]
  node [
    id 1174
    label "Kostroma"
  ]
  node [
    id 1175
    label "Barwice"
  ]
  node [
    id 1176
    label "U&#322;an_Ude"
  ]
  node [
    id 1177
    label "Czeskie_Budziejowice"
  ]
  node [
    id 1178
    label "Getynga"
  ]
  node [
    id 1179
    label "Kercz"
  ]
  node [
    id 1180
    label "B&#322;aszki"
  ]
  node [
    id 1181
    label "Lipawa"
  ]
  node [
    id 1182
    label "Bujnaksk"
  ]
  node [
    id 1183
    label "Wittenberga"
  ]
  node [
    id 1184
    label "Gorycja"
  ]
  node [
    id 1185
    label "Fryburg_Bryzgowijski"
  ]
  node [
    id 1186
    label "Swatowe"
  ]
  node [
    id 1187
    label "Frankfurt_nad_Menem"
  ]
  node [
    id 1188
    label "Magadan"
  ]
  node [
    id 1189
    label "Rzg&#243;w"
  ]
  node [
    id 1190
    label "Bijsk"
  ]
  node [
    id 1191
    label "Norylsk"
  ]
  node [
    id 1192
    label "Mesyna"
  ]
  node [
    id 1193
    label "Berezyna"
  ]
  node [
    id 1194
    label "Stawropol"
  ]
  node [
    id 1195
    label "Kircholm"
  ]
  node [
    id 1196
    label "Hawana"
  ]
  node [
    id 1197
    label "Pardubice"
  ]
  node [
    id 1198
    label "Drezno"
  ]
  node [
    id 1199
    label "Zaklik&#243;w"
  ]
  node [
    id 1200
    label "Kozielsk"
  ]
  node [
    id 1201
    label "Paw&#322;owo"
  ]
  node [
    id 1202
    label "Kani&#243;w"
  ]
  node [
    id 1203
    label "Adana"
  ]
  node [
    id 1204
    label "Kleczew"
  ]
  node [
    id 1205
    label "Rybi&#324;sk"
  ]
  node [
    id 1206
    label "Dayton"
  ]
  node [
    id 1207
    label "Nowy_Orlean"
  ]
  node [
    id 1208
    label "Perejas&#322;aw"
  ]
  node [
    id 1209
    label "Jenisejsk"
  ]
  node [
    id 1210
    label "Bolonia"
  ]
  node [
    id 1211
    label "Bir&#380;e"
  ]
  node [
    id 1212
    label "Marsylia"
  ]
  node [
    id 1213
    label "Workuta"
  ]
  node [
    id 1214
    label "Sewilla"
  ]
  node [
    id 1215
    label "Megara"
  ]
  node [
    id 1216
    label "Gotha"
  ]
  node [
    id 1217
    label "Kiejdany"
  ]
  node [
    id 1218
    label "Zaleszczyki"
  ]
  node [
    id 1219
    label "Ja&#322;ta"
  ]
  node [
    id 1220
    label "Burgas"
  ]
  node [
    id 1221
    label "Essen"
  ]
  node [
    id 1222
    label "Czadca"
  ]
  node [
    id 1223
    label "Manchester"
  ]
  node [
    id 1224
    label "Dzier&#380;y&#324;sk"
  ]
  node [
    id 1225
    label "Schmalkalden"
  ]
  node [
    id 1226
    label "Oleszyce"
  ]
  node [
    id 1227
    label "Kie&#380;mark"
  ]
  node [
    id 1228
    label "Kleck"
  ]
  node [
    id 1229
    label "Suez"
  ]
  node [
    id 1230
    label "Brack"
  ]
  node [
    id 1231
    label "Symferopol"
  ]
  node [
    id 1232
    label "Michalovce"
  ]
  node [
    id 1233
    label "Tambow"
  ]
  node [
    id 1234
    label "Turkmenbaszy"
  ]
  node [
    id 1235
    label "Bogumin"
  ]
  node [
    id 1236
    label "Sambor"
  ]
  node [
    id 1237
    label "Kalwaria_Zebrzydowska"
  ]
  node [
    id 1238
    label "Milan&#243;wek"
  ]
  node [
    id 1239
    label "Nachiczewan"
  ]
  node [
    id 1240
    label "Cluny"
  ]
  node [
    id 1241
    label "Stalinogorsk"
  ]
  node [
    id 1242
    label "Lipsk"
  ]
  node [
    id 1243
    label "Karlsbad"
  ]
  node [
    id 1244
    label "Pietrozawodsk"
  ]
  node [
    id 1245
    label "Bar"
  ]
  node [
    id 1246
    label "Korfant&#243;w"
  ]
  node [
    id 1247
    label "Nieftiegorsk"
  ]
  node [
    id 1248
    label "Hanower"
  ]
  node [
    id 1249
    label "Windawa"
  ]
  node [
    id 1250
    label "&#346;niatyn"
  ]
  node [
    id 1251
    label "Dalton"
  ]
  node [
    id 1252
    label "tramwaj"
  ]
  node [
    id 1253
    label "Kaszgar"
  ]
  node [
    id 1254
    label "Berdia&#324;sk"
  ]
  node [
    id 1255
    label "Koprzywnica"
  ]
  node [
    id 1256
    label "Dnieprodzier&#380;y&#324;sk"
  ]
  node [
    id 1257
    label "Brno"
  ]
  node [
    id 1258
    label "Wia&#378;ma"
  ]
  node [
    id 1259
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 1260
    label "Starobielsk"
  ]
  node [
    id 1261
    label "Ostr&#243;g"
  ]
  node [
    id 1262
    label "Oran"
  ]
  node [
    id 1263
    label "Nowogr&#243;d_Wielki"
  ]
  node [
    id 1264
    label "Wyszehrad"
  ]
  node [
    id 1265
    label "Zb&#261;szy&#324;"
  ]
  node [
    id 1266
    label "Trembowla"
  ]
  node [
    id 1267
    label "Tobolsk"
  ]
  node [
    id 1268
    label "Liberec"
  ]
  node [
    id 1269
    label "Wo&#322;godo&#324;sk"
  ]
  node [
    id 1270
    label "Semipa&#322;aty&#324;sk"
  ]
  node [
    id 1271
    label "G&#322;uszyca"
  ]
  node [
    id 1272
    label "Akwileja"
  ]
  node [
    id 1273
    label "Kar&#322;owice"
  ]
  node [
    id 1274
    label "Borys&#243;w"
  ]
  node [
    id 1275
    label "Stryj"
  ]
  node [
    id 1276
    label "Czeski_Cieszyn"
  ]
  node [
    id 1277
    label "Rydu&#322;towy"
  ]
  node [
    id 1278
    label "Darmstadt"
  ]
  node [
    id 1279
    label "Opawa"
  ]
  node [
    id 1280
    label "Jerycho"
  ]
  node [
    id 1281
    label "&#321;ohojsk"
  ]
  node [
    id 1282
    label "Fatima"
  ]
  node [
    id 1283
    label "W&#322;odzimierz_Wo&#322;y&#324;ski"
  ]
  node [
    id 1284
    label "Sara&#324;sk"
  ]
  node [
    id 1285
    label "Lyon"
  ]
  node [
    id 1286
    label "Wormacja"
  ]
  node [
    id 1287
    label "Perwomajsk"
  ]
  node [
    id 1288
    label "Lubeka"
  ]
  node [
    id 1289
    label "Sura&#380;"
  ]
  node [
    id 1290
    label "Karaganda"
  ]
  node [
    id 1291
    label "Nazaret"
  ]
  node [
    id 1292
    label "Poniewie&#380;"
  ]
  node [
    id 1293
    label "Siewieromorsk"
  ]
  node [
    id 1294
    label "Greifswald"
  ]
  node [
    id 1295
    label "Trewir"
  ]
  node [
    id 1296
    label "Nitra"
  ]
  node [
    id 1297
    label "Karwina"
  ]
  node [
    id 1298
    label "Houston"
  ]
  node [
    id 1299
    label "Demmin"
  ]
  node [
    id 1300
    label "Szamocin"
  ]
  node [
    id 1301
    label "Kolkata"
  ]
  node [
    id 1302
    label "Brasz&#243;w"
  ]
  node [
    id 1303
    label "&#321;uck"
  ]
  node [
    id 1304
    label "Peczora"
  ]
  node [
    id 1305
    label "S&#322;onim"
  ]
  node [
    id 1306
    label "Mekka"
  ]
  node [
    id 1307
    label "Rzeczyca"
  ]
  node [
    id 1308
    label "Konstancja"
  ]
  node [
    id 1309
    label "Orenburg"
  ]
  node [
    id 1310
    label "Pittsburgh"
  ]
  node [
    id 1311
    label "Pietropaw&#322;owsk"
  ]
  node [
    id 1312
    label "Barabi&#324;sk"
  ]
  node [
    id 1313
    label "Mory&#324;"
  ]
  node [
    id 1314
    label "Hallstatt"
  ]
  node [
    id 1315
    label "Mannheim"
  ]
  node [
    id 1316
    label "Tarent"
  ]
  node [
    id 1317
    label "Dortmund"
  ]
  node [
    id 1318
    label "Jab&#322;onk&#243;w"
  ]
  node [
    id 1319
    label "Dodona"
  ]
  node [
    id 1320
    label "Trojan"
  ]
  node [
    id 1321
    label "Nankin"
  ]
  node [
    id 1322
    label "Weimar"
  ]
  node [
    id 1323
    label "Brac&#322;aw"
  ]
  node [
    id 1324
    label "Izbica_Kujawska"
  ]
  node [
    id 1325
    label "Sankt_Florian"
  ]
  node [
    id 1326
    label "Pilzno"
  ]
  node [
    id 1327
    label "&#321;uga&#324;sk"
  ]
  node [
    id 1328
    label "Sewastopol"
  ]
  node [
    id 1329
    label "Poczaj&#243;w"
  ]
  node [
    id 1330
    label "Pas&#322;&#281;k"
  ]
  node [
    id 1331
    label "Sulech&#243;w"
  ]
  node [
    id 1332
    label "Pr&#243;szk&#243;w"
  ]
  node [
    id 1333
    label "Norak"
  ]
  node [
    id 1334
    label "Filadelfia"
  ]
  node [
    id 1335
    label "Maribor"
  ]
  node [
    id 1336
    label "Detroit"
  ]
  node [
    id 1337
    label "Bobolice"
  ]
  node [
    id 1338
    label "K&#322;odawa"
  ]
  node [
    id 1339
    label "Radziech&#243;w"
  ]
  node [
    id 1340
    label "W&#322;odzimierz"
  ]
  node [
    id 1341
    label "Tartu"
  ]
  node [
    id 1342
    label "Drohobycz"
  ]
  node [
    id 1343
    label "Saloniki"
  ]
  node [
    id 1344
    label "G&#243;ra_Kalwaria"
  ]
  node [
    id 1345
    label "Ku&#378;nia_Raciborska"
  ]
  node [
    id 1346
    label "Miedwie&#380;jegorsk"
  ]
  node [
    id 1347
    label "Buchara"
  ]
  node [
    id 1348
    label "P&#322;owdiw"
  ]
  node [
    id 1349
    label "Koszyce"
  ]
  node [
    id 1350
    label "Brema"
  ]
  node [
    id 1351
    label "Wagram"
  ]
  node [
    id 1352
    label "Czarnobyl"
  ]
  node [
    id 1353
    label "Brze&#347;&#263;"
  ]
  node [
    id 1354
    label "S&#232;vres"
  ]
  node [
    id 1355
    label "Dubrownik"
  ]
  node [
    id 1356
    label "Grenada"
  ]
  node [
    id 1357
    label "Jekaterynburg"
  ]
  node [
    id 1358
    label "zabudowa"
  ]
  node [
    id 1359
    label "Inhambane"
  ]
  node [
    id 1360
    label "Konstantyn&#243;wka"
  ]
  node [
    id 1361
    label "Krajowa"
  ]
  node [
    id 1362
    label "Norymberga"
  ]
  node [
    id 1363
    label "Tarnogr&#243;d"
  ]
  node [
    id 1364
    label "Beresteczko"
  ]
  node [
    id 1365
    label "Chabarowsk"
  ]
  node [
    id 1366
    label "Boden"
  ]
  node [
    id 1367
    label "Bamberg"
  ]
  node [
    id 1368
    label "Podhajce"
  ]
  node [
    id 1369
    label "Lhasa"
  ]
  node [
    id 1370
    label "Oszmiana"
  ]
  node [
    id 1371
    label "Narbona"
  ]
  node [
    id 1372
    label "Carrara"
  ]
  node [
    id 1373
    label "Soleczniki"
  ]
  node [
    id 1374
    label "Ba&#324;ska_Bystrzyca"
  ]
  node [
    id 1375
    label "Malin"
  ]
  node [
    id 1376
    label "Gandawa"
  ]
  node [
    id 1377
    label "burmistrz"
  ]
  node [
    id 1378
    label "Lancaster"
  ]
  node [
    id 1379
    label "S&#322;uck"
  ]
  node [
    id 1380
    label "Kronsztad"
  ]
  node [
    id 1381
    label "Mosty"
  ]
  node [
    id 1382
    label "Budionnowsk"
  ]
  node [
    id 1383
    label "Oksford"
  ]
  node [
    id 1384
    label "Awinion"
  ]
  node [
    id 1385
    label "M&#347;cis&#322;aw"
  ]
  node [
    id 1386
    label "Edynburg"
  ]
  node [
    id 1387
    label "Zagorsk"
  ]
  node [
    id 1388
    label "Kaspijsk"
  ]
  node [
    id 1389
    label "Konotop"
  ]
  node [
    id 1390
    label "Nantes"
  ]
  node [
    id 1391
    label "Sydney"
  ]
  node [
    id 1392
    label "Orsza"
  ]
  node [
    id 1393
    label "Krzanowice"
  ]
  node [
    id 1394
    label "Tiume&#324;"
  ]
  node [
    id 1395
    label "Wyborg"
  ]
  node [
    id 1396
    label "Nerczy&#324;sk"
  ]
  node [
    id 1397
    label "Rost&#243;w"
  ]
  node [
    id 1398
    label "Halicz"
  ]
  node [
    id 1399
    label "Sumy"
  ]
  node [
    id 1400
    label "Locarno"
  ]
  node [
    id 1401
    label "Luboml"
  ]
  node [
    id 1402
    label "Mariupol"
  ]
  node [
    id 1403
    label "Bras&#322;aw"
  ]
  node [
    id 1404
    label "Witnica"
  ]
  node [
    id 1405
    label "Niko&#322;ajewsk_nad_Amurem"
  ]
  node [
    id 1406
    label "Orneta"
  ]
  node [
    id 1407
    label "Gr&#243;dek"
  ]
  node [
    id 1408
    label "Go&#347;cino"
  ]
  node [
    id 1409
    label "Cannes"
  ]
  node [
    id 1410
    label "Lw&#243;w"
  ]
  node [
    id 1411
    label "Ulm"
  ]
  node [
    id 1412
    label "Aczy&#324;sk"
  ]
  node [
    id 1413
    label "Stuttgart"
  ]
  node [
    id 1414
    label "weduta"
  ]
  node [
    id 1415
    label "Borowsk"
  ]
  node [
    id 1416
    label "Niko&#322;ajewsk"
  ]
  node [
    id 1417
    label "&#346;wi&#281;ciany"
  ]
  node [
    id 1418
    label "Worone&#380;"
  ]
  node [
    id 1419
    label "Delhi"
  ]
  node [
    id 1420
    label "Adrianopol"
  ]
  node [
    id 1421
    label "Byczyna"
  ]
  node [
    id 1422
    label "Obuch&#243;w"
  ]
  node [
    id 1423
    label "Tyraspol"
  ]
  node [
    id 1424
    label "Modena"
  ]
  node [
    id 1425
    label "Rajgr&#243;d"
  ]
  node [
    id 1426
    label "Wo&#322;kowysk"
  ]
  node [
    id 1427
    label "&#379;ylina"
  ]
  node [
    id 1428
    label "Zurych"
  ]
  node [
    id 1429
    label "Vukovar"
  ]
  node [
    id 1430
    label "Narwa"
  ]
  node [
    id 1431
    label "Neapol"
  ]
  node [
    id 1432
    label "Frydek-Mistek"
  ]
  node [
    id 1433
    label "W&#322;adywostok"
  ]
  node [
    id 1434
    label "Calais"
  ]
  node [
    id 1435
    label "&#379;ydacz&#243;w"
  ]
  node [
    id 1436
    label "Trydent"
  ]
  node [
    id 1437
    label "Magnitogorsk"
  ]
  node [
    id 1438
    label "Padwa"
  ]
  node [
    id 1439
    label "Isfahan"
  ]
  node [
    id 1440
    label "Mohyl&#243;w_Podolski"
  ]
  node [
    id 1441
    label "Marburg"
  ]
  node [
    id 1442
    label "Homel"
  ]
  node [
    id 1443
    label "Boston"
  ]
  node [
    id 1444
    label "W&#252;rzburg"
  ]
  node [
    id 1445
    label "Antiochia"
  ]
  node [
    id 1446
    label "Wotki&#324;sk"
  ]
  node [
    id 1447
    label "A&#322;apajewsk"
  ]
  node [
    id 1448
    label "Lejda"
  ]
  node [
    id 1449
    label "Nieder_Selters"
  ]
  node [
    id 1450
    label "Nicea"
  ]
  node [
    id 1451
    label "Dmitrow"
  ]
  node [
    id 1452
    label "Taganrog"
  ]
  node [
    id 1453
    label "Z&#322;ocz&#243;w"
  ]
  node [
    id 1454
    label "Nowomoskowsk"
  ]
  node [
    id 1455
    label "Koby&#322;ka"
  ]
  node [
    id 1456
    label "Iwano-Frankowsk"
  ]
  node [
    id 1457
    label "Kis&#322;owodzk"
  ]
  node [
    id 1458
    label "Tomsk"
  ]
  node [
    id 1459
    label "Ferrara"
  ]
  node [
    id 1460
    label "Edam"
  ]
  node [
    id 1461
    label "Suworow"
  ]
  node [
    id 1462
    label "Turka"
  ]
  node [
    id 1463
    label "Aralsk"
  ]
  node [
    id 1464
    label "Kobry&#324;"
  ]
  node [
    id 1465
    label "Rotterdam"
  ]
  node [
    id 1466
    label "Bordeaux"
  ]
  node [
    id 1467
    label "L&#252;neburg"
  ]
  node [
    id 1468
    label "Akwizgran"
  ]
  node [
    id 1469
    label "Liverpool"
  ]
  node [
    id 1470
    label "Asuan"
  ]
  node [
    id 1471
    label "Bonn"
  ]
  node [
    id 1472
    label "Teby"
  ]
  node [
    id 1473
    label "Szumsk"
  ]
  node [
    id 1474
    label "Ku&#378;nieck"
  ]
  node [
    id 1475
    label "Bia&#322;ogr&#243;d_nad_Dniestrem"
  ]
  node [
    id 1476
    label "Tyberiada"
  ]
  node [
    id 1477
    label "Turkiestan"
  ]
  node [
    id 1478
    label "Nanning"
  ]
  node [
    id 1479
    label "G&#322;uch&#243;w"
  ]
  node [
    id 1480
    label "Bajonna"
  ]
  node [
    id 1481
    label "Wo&#322;oko&#322;amsk"
  ]
  node [
    id 1482
    label "Orze&#322;"
  ]
  node [
    id 1483
    label "Opalenica"
  ]
  node [
    id 1484
    label "Buczacz"
  ]
  node [
    id 1485
    label "Armenia"
  ]
  node [
    id 1486
    label "Nowoku&#378;nieck"
  ]
  node [
    id 1487
    label "Wuppertal"
  ]
  node [
    id 1488
    label "Wuhan"
  ]
  node [
    id 1489
    label "Betlejem"
  ]
  node [
    id 1490
    label "Wi&#322;komierz"
  ]
  node [
    id 1491
    label "Podiebrady"
  ]
  node [
    id 1492
    label "Rawenna"
  ]
  node [
    id 1493
    label "Haarlem"
  ]
  node [
    id 1494
    label "Woskriesiensk"
  ]
  node [
    id 1495
    label "Pyskowice"
  ]
  node [
    id 1496
    label "Kilonia"
  ]
  node [
    id 1497
    label "Ruciane-Nida"
  ]
  node [
    id 1498
    label "Kursk"
  ]
  node [
    id 1499
    label "Wolgast"
  ]
  node [
    id 1500
    label "Stralsund"
  ]
  node [
    id 1501
    label "Sydon"
  ]
  node [
    id 1502
    label "Natal"
  ]
  node [
    id 1503
    label "Hradec_Kr&#225;lov&#233;"
  ]
  node [
    id 1504
    label "Baranowicze"
  ]
  node [
    id 1505
    label "Stara_Zagora"
  ]
  node [
    id 1506
    label "Regensburg"
  ]
  node [
    id 1507
    label "Kapsztad"
  ]
  node [
    id 1508
    label "Kemerowo"
  ]
  node [
    id 1509
    label "Mi&#347;nia"
  ]
  node [
    id 1510
    label "Stary_Sambor"
  ]
  node [
    id 1511
    label "Soligorsk"
  ]
  node [
    id 1512
    label "Ostaszk&#243;w"
  ]
  node [
    id 1513
    label "T&#322;uszcz"
  ]
  node [
    id 1514
    label "Uljanowsk"
  ]
  node [
    id 1515
    label "Tuluza"
  ]
  node [
    id 1516
    label "Miko&#322;aj&#243;w"
  ]
  node [
    id 1517
    label "Chicago"
  ]
  node [
    id 1518
    label "Kamieniec_Podolski"
  ]
  node [
    id 1519
    label "Dijon"
  ]
  node [
    id 1520
    label "Siedliszcze"
  ]
  node [
    id 1521
    label "Haga"
  ]
  node [
    id 1522
    label "Bobrujsk"
  ]
  node [
    id 1523
    label "Kokand"
  ]
  node [
    id 1524
    label "Windsor"
  ]
  node [
    id 1525
    label "Chmielnicki"
  ]
  node [
    id 1526
    label "Winchester"
  ]
  node [
    id 1527
    label "Bria&#324;sk"
  ]
  node [
    id 1528
    label "Uppsala"
  ]
  node [
    id 1529
    label "Paw&#322;odar"
  ]
  node [
    id 1530
    label "Canterbury"
  ]
  node [
    id 1531
    label "Omsk"
  ]
  node [
    id 1532
    label "Tyr"
  ]
  node [
    id 1533
    label "przestrze&#324;_publiczna"
  ]
  node [
    id 1534
    label "Kolonia"
  ]
  node [
    id 1535
    label "Nowa_Ruda"
  ]
  node [
    id 1536
    label "Oldenburg_in_Holstein"
  ]
  node [
    id 1537
    label "Czerkasy"
  ]
  node [
    id 1538
    label "Budziszyn"
  ]
  node [
    id 1539
    label "Rohatyn"
  ]
  node [
    id 1540
    label "Nowogr&#243;dek"
  ]
  node [
    id 1541
    label "Buda"
  ]
  node [
    id 1542
    label "Zbara&#380;"
  ]
  node [
    id 1543
    label "Korzec"
  ]
  node [
    id 1544
    label "Medyna"
  ]
  node [
    id 1545
    label "Piatigorsk"
  ]
  node [
    id 1546
    label "Monako"
  ]
  node [
    id 1547
    label "Chark&#243;w"
  ]
  node [
    id 1548
    label "Zadar"
  ]
  node [
    id 1549
    label "Brandenburg"
  ]
  node [
    id 1550
    label "&#379;ytawa"
  ]
  node [
    id 1551
    label "Konstantynopol"
  ]
  node [
    id 1552
    label "Wismar"
  ]
  node [
    id 1553
    label "Wielsk"
  ]
  node [
    id 1554
    label "U&#347;cie_nad_&#321;ab&#261;"
  ]
  node [
    id 1555
    label "Genewa"
  ]
  node [
    id 1556
    label "Merseburg"
  ]
  node [
    id 1557
    label "Lozanna"
  ]
  node [
    id 1558
    label "Azow"
  ]
  node [
    id 1559
    label "K&#322;ajpeda"
  ]
  node [
    id 1560
    label "Angarsk"
  ]
  node [
    id 1561
    label "Ostrawa"
  ]
  node [
    id 1562
    label "Jastarnia"
  ]
  node [
    id 1563
    label "Moguncja"
  ]
  node [
    id 1564
    label "Siewsk"
  ]
  node [
    id 1565
    label "Pasawa"
  ]
  node [
    id 1566
    label "Penza"
  ]
  node [
    id 1567
    label "Borys&#322;aw"
  ]
  node [
    id 1568
    label "Osaka"
  ]
  node [
    id 1569
    label "Eupatoria"
  ]
  node [
    id 1570
    label "Kalmar"
  ]
  node [
    id 1571
    label "Troki"
  ]
  node [
    id 1572
    label "Mosina"
  ]
  node [
    id 1573
    label "Orany"
  ]
  node [
    id 1574
    label "Zas&#322;aw"
  ]
  node [
    id 1575
    label "Dobrodzie&#324;"
  ]
  node [
    id 1576
    label "Kars"
  ]
  node [
    id 1577
    label "Poprad"
  ]
  node [
    id 1578
    label "Sajgon"
  ]
  node [
    id 1579
    label "Tulon"
  ]
  node [
    id 1580
    label "Kro&#347;niewice"
  ]
  node [
    id 1581
    label "Krzywi&#324;"
  ]
  node [
    id 1582
    label "Batumi"
  ]
  node [
    id 1583
    label "Werona"
  ]
  node [
    id 1584
    label "&#379;migr&#243;d"
  ]
  node [
    id 1585
    label "Ka&#322;uga"
  ]
  node [
    id 1586
    label "Rakoniewice"
  ]
  node [
    id 1587
    label "Trabzon"
  ]
  node [
    id 1588
    label "Debreczyn"
  ]
  node [
    id 1589
    label "Jena"
  ]
  node [
    id 1590
    label "Strzelno"
  ]
  node [
    id 1591
    label "Gwardiejsk"
  ]
  node [
    id 1592
    label "Wersal"
  ]
  node [
    id 1593
    label "Bych&#243;w"
  ]
  node [
    id 1594
    label "Ba&#322;tijsk"
  ]
  node [
    id 1595
    label "Trenczyn"
  ]
  node [
    id 1596
    label "Walencja"
  ]
  node [
    id 1597
    label "Warna"
  ]
  node [
    id 1598
    label "Swiet&#322;ogorsk"
  ]
  node [
    id 1599
    label "Huma&#324;"
  ]
  node [
    id 1600
    label "Wilejka"
  ]
  node [
    id 1601
    label "Ochryda"
  ]
  node [
    id 1602
    label "Berdycz&#243;w"
  ]
  node [
    id 1603
    label "Krasnogorsk"
  ]
  node [
    id 1604
    label "Bogus&#322;aw"
  ]
  node [
    id 1605
    label "Trzyniec"
  ]
  node [
    id 1606
    label "urz&#261;d"
  ]
  node [
    id 1607
    label "Mariampol"
  ]
  node [
    id 1608
    label "Ko&#322;omna"
  ]
  node [
    id 1609
    label "Chanty-Mansyjsk"
  ]
  node [
    id 1610
    label "Piast&#243;w"
  ]
  node [
    id 1611
    label "Jastrowie"
  ]
  node [
    id 1612
    label "Nampula"
  ]
  node [
    id 1613
    label "Kamionka_Strumi&#322;owa"
  ]
  node [
    id 1614
    label "Bor"
  ]
  node [
    id 1615
    label "Lengyel"
  ]
  node [
    id 1616
    label "Lubecz"
  ]
  node [
    id 1617
    label "Wierchoja&#324;sk"
  ]
  node [
    id 1618
    label "Barczewo"
  ]
  node [
    id 1619
    label "Madras"
  ]
  node [
    id 1620
    label "zdrada"
  ]
  node [
    id 1621
    label "warunek_lokalowy"
  ]
  node [
    id 1622
    label "location"
  ]
  node [
    id 1623
    label "uwaga"
  ]
  node [
    id 1624
    label "status"
  ]
  node [
    id 1625
    label "cia&#322;o"
  ]
  node [
    id 1626
    label "cecha"
  ]
  node [
    id 1627
    label "rz&#261;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 169
  ]
  edge [
    source 8
    target 170
  ]
  edge [
    source 8
    target 171
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 176
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 179
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 11
    target 228
  ]
  edge [
    source 11
    target 229
  ]
  edge [
    source 11
    target 230
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 234
  ]
  edge [
    source 11
    target 235
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 11
    target 245
  ]
  edge [
    source 11
    target 246
  ]
  edge [
    source 11
    target 247
  ]
  edge [
    source 11
    target 248
  ]
  edge [
    source 11
    target 249
  ]
  edge [
    source 11
    target 250
  ]
  edge [
    source 11
    target 251
  ]
  edge [
    source 11
    target 252
  ]
  edge [
    source 11
    target 253
  ]
  edge [
    source 11
    target 254
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 256
  ]
  edge [
    source 11
    target 257
  ]
  edge [
    source 11
    target 258
  ]
  edge [
    source 11
    target 259
  ]
  edge [
    source 11
    target 260
  ]
  edge [
    source 11
    target 261
  ]
  edge [
    source 11
    target 262
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 11
    target 271
  ]
  edge [
    source 11
    target 272
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 219
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 11
    target 285
  ]
  edge [
    source 11
    target 286
  ]
  edge [
    source 11
    target 287
  ]
  edge [
    source 11
    target 288
  ]
  edge [
    source 11
    target 289
  ]
  edge [
    source 11
    target 290
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 292
  ]
  edge [
    source 11
    target 293
  ]
  edge [
    source 11
    target 294
  ]
  edge [
    source 11
    target 295
  ]
  edge [
    source 11
    target 296
  ]
  edge [
    source 11
    target 297
  ]
  edge [
    source 11
    target 298
  ]
  edge [
    source 11
    target 299
  ]
  edge [
    source 11
    target 300
  ]
  edge [
    source 11
    target 301
  ]
  edge [
    source 11
    target 302
  ]
  edge [
    source 11
    target 303
  ]
  edge [
    source 11
    target 304
  ]
  edge [
    source 11
    target 305
  ]
  edge [
    source 11
    target 306
  ]
  edge [
    source 11
    target 307
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 309
  ]
  edge [
    source 11
    target 310
  ]
  edge [
    source 11
    target 311
  ]
  edge [
    source 11
    target 312
  ]
  edge [
    source 11
    target 313
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 315
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 316
  ]
  edge [
    source 12
    target 317
  ]
  edge [
    source 12
    target 318
  ]
  edge [
    source 12
    target 319
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 12
    target 320
  ]
  edge [
    source 12
    target 321
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 334
  ]
  edge [
    source 15
    target 335
  ]
  edge [
    source 15
    target 43
  ]
  edge [
    source 15
    target 336
  ]
  edge [
    source 15
    target 337
  ]
  edge [
    source 15
    target 338
  ]
  edge [
    source 15
    target 339
  ]
  edge [
    source 15
    target 340
  ]
  edge [
    source 15
    target 341
  ]
  edge [
    source 15
    target 342
  ]
  edge [
    source 15
    target 343
  ]
  edge [
    source 15
    target 344
  ]
  edge [
    source 15
    target 345
  ]
  edge [
    source 15
    target 346
  ]
  edge [
    source 15
    target 347
  ]
  edge [
    source 15
    target 348
  ]
  edge [
    source 15
    target 349
  ]
  edge [
    source 15
    target 350
  ]
  edge [
    source 15
    target 351
  ]
  edge [
    source 15
    target 352
  ]
  edge [
    source 15
    target 353
  ]
  edge [
    source 15
    target 354
  ]
  edge [
    source 15
    target 355
  ]
  edge [
    source 15
    target 356
  ]
  edge [
    source 15
    target 357
  ]
  edge [
    source 15
    target 358
  ]
  edge [
    source 15
    target 359
  ]
  edge [
    source 15
    target 360
  ]
  edge [
    source 15
    target 361
  ]
  edge [
    source 15
    target 362
  ]
  edge [
    source 15
    target 363
  ]
  edge [
    source 15
    target 364
  ]
  edge [
    source 15
    target 365
  ]
  edge [
    source 15
    target 366
  ]
  edge [
    source 15
    target 367
  ]
  edge [
    source 15
    target 368
  ]
  edge [
    source 15
    target 369
  ]
  edge [
    source 15
    target 370
  ]
  edge [
    source 15
    target 371
  ]
  edge [
    source 15
    target 372
  ]
  edge [
    source 15
    target 373
  ]
  edge [
    source 15
    target 374
  ]
  edge [
    source 15
    target 375
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 376
  ]
  edge [
    source 15
    target 377
  ]
  edge [
    source 15
    target 378
  ]
  edge [
    source 15
    target 379
  ]
  edge [
    source 15
    target 380
  ]
  edge [
    source 15
    target 381
  ]
  edge [
    source 15
    target 382
  ]
  edge [
    source 15
    target 383
  ]
  edge [
    source 15
    target 384
  ]
  edge [
    source 15
    target 385
  ]
  edge [
    source 15
    target 386
  ]
  edge [
    source 15
    target 387
  ]
  edge [
    source 15
    target 388
  ]
  edge [
    source 15
    target 389
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 391
  ]
  edge [
    source 15
    target 392
  ]
  edge [
    source 15
    target 393
  ]
  edge [
    source 15
    target 394
  ]
  edge [
    source 15
    target 395
  ]
  edge [
    source 15
    target 396
  ]
  edge [
    source 15
    target 397
  ]
  edge [
    source 15
    target 398
  ]
  edge [
    source 15
    target 399
  ]
  edge [
    source 15
    target 400
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 402
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 408
  ]
  edge [
    source 15
    target 409
  ]
  edge [
    source 15
    target 410
  ]
  edge [
    source 15
    target 411
  ]
  edge [
    source 15
    target 412
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 15
    target 414
  ]
  edge [
    source 15
    target 415
  ]
  edge [
    source 15
    target 416
  ]
  edge [
    source 15
    target 417
  ]
  edge [
    source 15
    target 418
  ]
  edge [
    source 15
    target 419
  ]
  edge [
    source 15
    target 420
  ]
  edge [
    source 15
    target 421
  ]
  edge [
    source 15
    target 422
  ]
  edge [
    source 15
    target 423
  ]
  edge [
    source 15
    target 424
  ]
  edge [
    source 15
    target 425
  ]
  edge [
    source 15
    target 426
  ]
  edge [
    source 15
    target 427
  ]
  edge [
    source 15
    target 428
  ]
  edge [
    source 15
    target 429
  ]
  edge [
    source 15
    target 430
  ]
  edge [
    source 15
    target 431
  ]
  edge [
    source 15
    target 432
  ]
  edge [
    source 15
    target 433
  ]
  edge [
    source 15
    target 434
  ]
  edge [
    source 15
    target 435
  ]
  edge [
    source 15
    target 436
  ]
  edge [
    source 15
    target 437
  ]
  edge [
    source 15
    target 438
  ]
  edge [
    source 15
    target 439
  ]
  edge [
    source 15
    target 440
  ]
  edge [
    source 15
    target 441
  ]
  edge [
    source 15
    target 442
  ]
  edge [
    source 15
    target 443
  ]
  edge [
    source 15
    target 444
  ]
  edge [
    source 15
    target 445
  ]
  edge [
    source 15
    target 446
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 447
  ]
  edge [
    source 16
    target 448
  ]
  edge [
    source 16
    target 449
  ]
  edge [
    source 16
    target 450
  ]
  edge [
    source 16
    target 451
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 452
  ]
  edge [
    source 17
    target 453
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 455
  ]
  edge [
    source 17
    target 456
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 465
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 17
    target 469
  ]
  edge [
    source 17
    target 470
  ]
  edge [
    source 17
    target 471
  ]
  edge [
    source 17
    target 472
  ]
  edge [
    source 17
    target 473
  ]
  edge [
    source 17
    target 474
  ]
  edge [
    source 17
    target 475
  ]
  edge [
    source 17
    target 476
  ]
  edge [
    source 17
    target 288
  ]
  edge [
    source 17
    target 477
  ]
  edge [
    source 17
    target 478
  ]
  edge [
    source 17
    target 479
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 480
  ]
  edge [
    source 18
    target 481
  ]
  edge [
    source 18
    target 482
  ]
  edge [
    source 18
    target 483
  ]
  edge [
    source 18
    target 33
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 484
  ]
  edge [
    source 19
    target 485
  ]
  edge [
    source 19
    target 486
  ]
  edge [
    source 19
    target 487
  ]
  edge [
    source 19
    target 488
  ]
  edge [
    source 19
    target 489
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 490
  ]
  edge [
    source 20
    target 491
  ]
  edge [
    source 20
    target 492
  ]
  edge [
    source 20
    target 493
  ]
  edge [
    source 20
    target 494
  ]
  edge [
    source 20
    target 495
  ]
  edge [
    source 20
    target 496
  ]
  edge [
    source 20
    target 497
  ]
  edge [
    source 20
    target 498
  ]
  edge [
    source 20
    target 499
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 500
  ]
  edge [
    source 20
    target 501
  ]
  edge [
    source 20
    target 502
  ]
  edge [
    source 20
    target 503
  ]
  edge [
    source 20
    target 504
  ]
  edge [
    source 20
    target 505
  ]
  edge [
    source 20
    target 473
  ]
  edge [
    source 20
    target 506
  ]
  edge [
    source 20
    target 507
  ]
  edge [
    source 20
    target 508
  ]
  edge [
    source 20
    target 509
  ]
  edge [
    source 20
    target 510
  ]
  edge [
    source 20
    target 511
  ]
  edge [
    source 20
    target 512
  ]
  edge [
    source 20
    target 513
  ]
  edge [
    source 20
    target 514
  ]
  edge [
    source 20
    target 515
  ]
  edge [
    source 20
    target 516
  ]
  edge [
    source 20
    target 517
  ]
  edge [
    source 20
    target 518
  ]
  edge [
    source 20
    target 519
  ]
  edge [
    source 20
    target 331
  ]
  edge [
    source 20
    target 520
  ]
  edge [
    source 20
    target 521
  ]
  edge [
    source 20
    target 522
  ]
  edge [
    source 20
    target 523
  ]
  edge [
    source 20
    target 524
  ]
  edge [
    source 20
    target 525
  ]
  edge [
    source 20
    target 526
  ]
  edge [
    source 20
    target 527
  ]
  edge [
    source 20
    target 528
  ]
  edge [
    source 20
    target 529
  ]
  edge [
    source 20
    target 530
  ]
  edge [
    source 20
    target 531
  ]
  edge [
    source 20
    target 532
  ]
  edge [
    source 20
    target 533
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 433
  ]
  edge [
    source 22
    target 534
  ]
  edge [
    source 22
    target 535
  ]
  edge [
    source 22
    target 536
  ]
  edge [
    source 22
    target 537
  ]
  edge [
    source 22
    target 538
  ]
  edge [
    source 22
    target 539
  ]
  edge [
    source 22
    target 540
  ]
  edge [
    source 22
    target 541
  ]
  edge [
    source 22
    target 542
  ]
  edge [
    source 22
    target 543
  ]
  edge [
    source 22
    target 544
  ]
  edge [
    source 22
    target 545
  ]
  edge [
    source 22
    target 546
  ]
  edge [
    source 22
    target 547
  ]
  edge [
    source 22
    target 548
  ]
  edge [
    source 22
    target 549
  ]
  edge [
    source 22
    target 245
  ]
  edge [
    source 22
    target 550
  ]
  edge [
    source 22
    target 551
  ]
  edge [
    source 22
    target 552
  ]
  edge [
    source 22
    target 553
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 554
  ]
  edge [
    source 22
    target 555
  ]
  edge [
    source 22
    target 556
  ]
  edge [
    source 22
    target 557
  ]
  edge [
    source 22
    target 558
  ]
  edge [
    source 22
    target 559
  ]
  edge [
    source 22
    target 560
  ]
  edge [
    source 22
    target 561
  ]
  edge [
    source 22
    target 562
  ]
  edge [
    source 22
    target 563
  ]
  edge [
    source 22
    target 564
  ]
  edge [
    source 22
    target 565
  ]
  edge [
    source 22
    target 566
  ]
  edge [
    source 22
    target 567
  ]
  edge [
    source 22
    target 568
  ]
  edge [
    source 22
    target 569
  ]
  edge [
    source 22
    target 570
  ]
  edge [
    source 22
    target 571
  ]
  edge [
    source 22
    target 572
  ]
  edge [
    source 22
    target 573
  ]
  edge [
    source 22
    target 574
  ]
  edge [
    source 22
    target 575
  ]
  edge [
    source 22
    target 576
  ]
  edge [
    source 22
    target 577
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 169
  ]
  edge [
    source 24
    target 578
  ]
  edge [
    source 24
    target 579
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 580
  ]
  edge [
    source 24
    target 581
  ]
  edge [
    source 24
    target 582
  ]
  edge [
    source 24
    target 583
  ]
  edge [
    source 24
    target 584
  ]
  edge [
    source 24
    target 585
  ]
  edge [
    source 24
    target 331
  ]
  edge [
    source 24
    target 586
  ]
  edge [
    source 24
    target 587
  ]
  edge [
    source 24
    target 588
  ]
  edge [
    source 24
    target 589
  ]
  edge [
    source 24
    target 590
  ]
  edge [
    source 24
    target 591
  ]
  edge [
    source 24
    target 592
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 181
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 24
    target 186
  ]
  edge [
    source 24
    target 187
  ]
  edge [
    source 24
    target 188
  ]
  edge [
    source 24
    target 189
  ]
  edge [
    source 24
    target 190
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 197
  ]
  edge [
    source 24
    target 198
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 593
  ]
  edge [
    source 25
    target 594
  ]
  edge [
    source 25
    target 85
  ]
  edge [
    source 25
    target 595
  ]
  edge [
    source 25
    target 596
  ]
  edge [
    source 25
    target 597
  ]
  edge [
    source 25
    target 598
  ]
  edge [
    source 25
    target 599
  ]
  edge [
    source 25
    target 600
  ]
  edge [
    source 25
    target 601
  ]
  edge [
    source 25
    target 602
  ]
  edge [
    source 25
    target 327
  ]
  edge [
    source 25
    target 83
  ]
  edge [
    source 25
    target 603
  ]
  edge [
    source 25
    target 86
  ]
  edge [
    source 25
    target 604
  ]
  edge [
    source 25
    target 605
  ]
  edge [
    source 25
    target 606
  ]
  edge [
    source 25
    target 607
  ]
  edge [
    source 25
    target 608
  ]
  edge [
    source 25
    target 584
  ]
  edge [
    source 25
    target 609
  ]
  edge [
    source 25
    target 610
  ]
  edge [
    source 25
    target 611
  ]
  edge [
    source 25
    target 612
  ]
  edge [
    source 25
    target 613
  ]
  edge [
    source 25
    target 614
  ]
  edge [
    source 25
    target 615
  ]
  edge [
    source 25
    target 616
  ]
  edge [
    source 25
    target 617
  ]
  edge [
    source 25
    target 618
  ]
  edge [
    source 25
    target 619
  ]
  edge [
    source 25
    target 620
  ]
  edge [
    source 25
    target 621
  ]
  edge [
    source 25
    target 77
  ]
  edge [
    source 25
    target 622
  ]
  edge [
    source 25
    target 623
  ]
  edge [
    source 25
    target 624
  ]
  edge [
    source 25
    target 625
  ]
  edge [
    source 25
    target 626
  ]
  edge [
    source 25
    target 627
  ]
  edge [
    source 25
    target 628
  ]
  edge [
    source 25
    target 629
  ]
  edge [
    source 25
    target 630
  ]
  edge [
    source 25
    target 631
  ]
  edge [
    source 25
    target 632
  ]
  edge [
    source 25
    target 633
  ]
  edge [
    source 25
    target 188
  ]
  edge [
    source 25
    target 634
  ]
  edge [
    source 25
    target 635
  ]
  edge [
    source 25
    target 636
  ]
  edge [
    source 25
    target 637
  ]
  edge [
    source 25
    target 638
  ]
  edge [
    source 25
    target 90
  ]
  edge [
    source 25
    target 80
  ]
  edge [
    source 25
    target 639
  ]
  edge [
    source 25
    target 640
  ]
  edge [
    source 25
    target 641
  ]
  edge [
    source 25
    target 642
  ]
  edge [
    source 25
    target 643
  ]
  edge [
    source 25
    target 644
  ]
  edge [
    source 25
    target 645
  ]
  edge [
    source 25
    target 646
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 647
  ]
  edge [
    source 26
    target 648
  ]
  edge [
    source 26
    target 649
  ]
  edge [
    source 26
    target 650
  ]
  edge [
    source 26
    target 651
  ]
  edge [
    source 26
    target 652
  ]
  edge [
    source 26
    target 653
  ]
  edge [
    source 26
    target 654
  ]
  edge [
    source 26
    target 655
  ]
  edge [
    source 26
    target 656
  ]
  edge [
    source 26
    target 657
  ]
  edge [
    source 26
    target 421
  ]
  edge [
    source 26
    target 658
  ]
  edge [
    source 26
    target 659
  ]
  edge [
    source 26
    target 660
  ]
  edge [
    source 26
    target 661
  ]
  edge [
    source 26
    target 662
  ]
  edge [
    source 26
    target 663
  ]
  edge [
    source 26
    target 664
  ]
  edge [
    source 26
    target 665
  ]
  edge [
    source 26
    target 666
  ]
  edge [
    source 26
    target 667
  ]
  edge [
    source 26
    target 668
  ]
  edge [
    source 26
    target 669
  ]
  edge [
    source 26
    target 670
  ]
  edge [
    source 26
    target 671
  ]
  edge [
    source 26
    target 672
  ]
  edge [
    source 26
    target 673
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 674
  ]
  edge [
    source 27
    target 675
  ]
  edge [
    source 27
    target 676
  ]
  edge [
    source 27
    target 534
  ]
  edge [
    source 27
    target 677
  ]
  edge [
    source 27
    target 678
  ]
  edge [
    source 27
    target 679
  ]
  edge [
    source 27
    target 680
  ]
  edge [
    source 27
    target 681
  ]
  edge [
    source 27
    target 682
  ]
  edge [
    source 27
    target 683
  ]
  edge [
    source 27
    target 575
  ]
  edge [
    source 27
    target 684
  ]
  edge [
    source 27
    target 685
  ]
  edge [
    source 27
    target 686
  ]
  edge [
    source 27
    target 196
  ]
  edge [
    source 27
    target 687
  ]
  edge [
    source 27
    target 688
  ]
  edge [
    source 27
    target 689
  ]
  edge [
    source 27
    target 690
  ]
  edge [
    source 27
    target 691
  ]
  edge [
    source 27
    target 692
  ]
  edge [
    source 27
    target 693
  ]
  edge [
    source 27
    target 694
  ]
  edge [
    source 27
    target 695
  ]
  edge [
    source 27
    target 696
  ]
  edge [
    source 27
    target 697
  ]
  edge [
    source 27
    target 698
  ]
  edge [
    source 27
    target 699
  ]
  edge [
    source 27
    target 189
  ]
  edge [
    source 27
    target 700
  ]
  edge [
    source 27
    target 701
  ]
  edge [
    source 27
    target 702
  ]
  edge [
    source 27
    target 703
  ]
  edge [
    source 27
    target 704
  ]
  edge [
    source 27
    target 705
  ]
  edge [
    source 27
    target 706
  ]
  edge [
    source 27
    target 707
  ]
  edge [
    source 27
    target 708
  ]
  edge [
    source 27
    target 709
  ]
  edge [
    source 27
    target 710
  ]
  edge [
    source 27
    target 711
  ]
  edge [
    source 27
    target 712
  ]
  edge [
    source 27
    target 713
  ]
  edge [
    source 27
    target 714
  ]
  edge [
    source 27
    target 715
  ]
  edge [
    source 27
    target 716
  ]
  edge [
    source 27
    target 717
  ]
  edge [
    source 27
    target 718
  ]
  edge [
    source 27
    target 719
  ]
  edge [
    source 27
    target 720
  ]
  edge [
    source 27
    target 721
  ]
  edge [
    source 27
    target 722
  ]
  edge [
    source 27
    target 723
  ]
  edge [
    source 27
    target 724
  ]
  edge [
    source 27
    target 725
  ]
  edge [
    source 27
    target 726
  ]
  edge [
    source 27
    target 727
  ]
  edge [
    source 27
    target 728
  ]
  edge [
    source 27
    target 729
  ]
  edge [
    source 27
    target 730
  ]
  edge [
    source 27
    target 731
  ]
  edge [
    source 27
    target 732
  ]
  edge [
    source 27
    target 733
  ]
  edge [
    source 27
    target 734
  ]
  edge [
    source 27
    target 735
  ]
  edge [
    source 27
    target 736
  ]
  edge [
    source 27
    target 737
  ]
  edge [
    source 27
    target 738
  ]
  edge [
    source 27
    target 739
  ]
  edge [
    source 27
    target 740
  ]
  edge [
    source 27
    target 537
  ]
  edge [
    source 27
    target 741
  ]
  edge [
    source 27
    target 742
  ]
  edge [
    source 27
    target 743
  ]
  edge [
    source 27
    target 744
  ]
  edge [
    source 27
    target 745
  ]
  edge [
    source 27
    target 746
  ]
  edge [
    source 27
    target 747
  ]
  edge [
    source 27
    target 748
  ]
  edge [
    source 27
    target 749
  ]
  edge [
    source 27
    target 750
  ]
  edge [
    source 27
    target 751
  ]
  edge [
    source 27
    target 752
  ]
  edge [
    source 27
    target 753
  ]
  edge [
    source 27
    target 754
  ]
  edge [
    source 27
    target 755
  ]
  edge [
    source 27
    target 756
  ]
  edge [
    source 27
    target 757
  ]
  edge [
    source 27
    target 758
  ]
  edge [
    source 27
    target 759
  ]
  edge [
    source 27
    target 760
  ]
  edge [
    source 27
    target 548
  ]
  edge [
    source 27
    target 549
  ]
  edge [
    source 27
    target 245
  ]
  edge [
    source 27
    target 550
  ]
  edge [
    source 27
    target 551
  ]
  edge [
    source 27
    target 552
  ]
  edge [
    source 27
    target 553
  ]
  edge [
    source 27
    target 554
  ]
  edge [
    source 27
    target 555
  ]
  edge [
    source 27
    target 761
  ]
  edge [
    source 27
    target 762
  ]
  edge [
    source 27
    target 763
  ]
  edge [
    source 27
    target 764
  ]
  edge [
    source 27
    target 765
  ]
  edge [
    source 27
    target 766
  ]
  edge [
    source 27
    target 767
  ]
  edge [
    source 27
    target 768
  ]
  edge [
    source 27
    target 769
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 770
  ]
  edge [
    source 29
    target 771
  ]
  edge [
    source 29
    target 772
  ]
  edge [
    source 29
    target 773
  ]
  edge [
    source 29
    target 774
  ]
  edge [
    source 29
    target 775
  ]
  edge [
    source 29
    target 776
  ]
  edge [
    source 29
    target 777
  ]
  edge [
    source 29
    target 778
  ]
  edge [
    source 29
    target 779
  ]
  edge [
    source 29
    target 780
  ]
  edge [
    source 29
    target 781
  ]
  edge [
    source 29
    target 782
  ]
  edge [
    source 29
    target 783
  ]
  edge [
    source 29
    target 784
  ]
  edge [
    source 29
    target 785
  ]
  edge [
    source 29
    target 786
  ]
  edge [
    source 29
    target 787
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 788
  ]
  edge [
    source 33
    target 480
  ]
  edge [
    source 33
    target 789
  ]
  edge [
    source 33
    target 790
  ]
  edge [
    source 33
    target 483
  ]
  edge [
    source 33
    target 791
  ]
  edge [
    source 33
    target 792
  ]
  edge [
    source 33
    target 793
  ]
  edge [
    source 33
    target 794
  ]
  edge [
    source 33
    target 795
  ]
  edge [
    source 33
    target 796
  ]
  edge [
    source 33
    target 797
  ]
  edge [
    source 33
    target 798
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 799
  ]
  edge [
    source 34
    target 800
  ]
  edge [
    source 34
    target 677
  ]
  edge [
    source 34
    target 801
  ]
  edge [
    source 34
    target 802
  ]
  edge [
    source 34
    target 803
  ]
  edge [
    source 34
    target 804
  ]
  edge [
    source 34
    target 805
  ]
  edge [
    source 34
    target 806
  ]
  edge [
    source 34
    target 807
  ]
  edge [
    source 34
    target 128
  ]
  edge [
    source 34
    target 808
  ]
  edge [
    source 34
    target 809
  ]
  edge [
    source 34
    target 810
  ]
  edge [
    source 34
    target 811
  ]
  edge [
    source 34
    target 812
  ]
  edge [
    source 34
    target 813
  ]
  edge [
    source 34
    target 814
  ]
  edge [
    source 34
    target 815
  ]
  edge [
    source 34
    target 816
  ]
  edge [
    source 34
    target 817
  ]
  edge [
    source 34
    target 818
  ]
  edge [
    source 34
    target 819
  ]
  edge [
    source 34
    target 820
  ]
  edge [
    source 34
    target 821
  ]
  edge [
    source 34
    target 822
  ]
  edge [
    source 34
    target 823
  ]
  edge [
    source 34
    target 824
  ]
  edge [
    source 34
    target 825
  ]
  edge [
    source 34
    target 826
  ]
  edge [
    source 34
    target 827
  ]
  edge [
    source 34
    target 828
  ]
  edge [
    source 34
    target 829
  ]
  edge [
    source 34
    target 830
  ]
  edge [
    source 34
    target 831
  ]
  edge [
    source 34
    target 482
  ]
  edge [
    source 34
    target 832
  ]
  edge [
    source 34
    target 833
  ]
  edge [
    source 34
    target 834
  ]
  edge [
    source 34
    target 835
  ]
  edge [
    source 34
    target 836
  ]
  edge [
    source 34
    target 837
  ]
  edge [
    source 34
    target 838
  ]
  edge [
    source 34
    target 839
  ]
  edge [
    source 34
    target 840
  ]
  edge [
    source 34
    target 841
  ]
  edge [
    source 34
    target 842
  ]
  edge [
    source 34
    target 843
  ]
  edge [
    source 34
    target 844
  ]
  edge [
    source 34
    target 845
  ]
  edge [
    source 34
    target 846
  ]
  edge [
    source 34
    target 847
  ]
  edge [
    source 34
    target 769
  ]
  edge [
    source 34
    target 848
  ]
  edge [
    source 34
    target 849
  ]
  edge [
    source 34
    target 850
  ]
  edge [
    source 34
    target 851
  ]
  edge [
    source 34
    target 852
  ]
  edge [
    source 34
    target 853
  ]
  edge [
    source 34
    target 854
  ]
  edge [
    source 34
    target 685
  ]
  edge [
    source 34
    target 686
  ]
  edge [
    source 34
    target 196
  ]
  edge [
    source 34
    target 687
  ]
  edge [
    source 34
    target 855
  ]
  edge [
    source 34
    target 110
  ]
  edge [
    source 34
    target 856
  ]
  edge [
    source 34
    target 857
  ]
  edge [
    source 34
    target 240
  ]
  edge [
    source 34
    target 858
  ]
  edge [
    source 34
    target 859
  ]
  edge [
    source 34
    target 860
  ]
  edge [
    source 34
    target 861
  ]
  edge [
    source 34
    target 862
  ]
  edge [
    source 34
    target 863
  ]
  edge [
    source 34
    target 864
  ]
  edge [
    source 34
    target 865
  ]
  edge [
    source 34
    target 866
  ]
  edge [
    source 34
    target 867
  ]
  edge [
    source 34
    target 868
  ]
  edge [
    source 34
    target 869
  ]
  edge [
    source 34
    target 870
  ]
  edge [
    source 34
    target 871
  ]
  edge [
    source 34
    target 872
  ]
  edge [
    source 34
    target 873
  ]
  edge [
    source 34
    target 874
  ]
  edge [
    source 34
    target 875
  ]
  edge [
    source 34
    target 876
  ]
  edge [
    source 34
    target 877
  ]
  edge [
    source 34
    target 878
  ]
  edge [
    source 34
    target 481
  ]
  edge [
    source 34
    target 879
  ]
  edge [
    source 34
    target 880
  ]
  edge [
    source 34
    target 881
  ]
  edge [
    source 34
    target 882
  ]
  edge [
    source 34
    target 883
  ]
  edge [
    source 34
    target 884
  ]
  edge [
    source 34
    target 885
  ]
  edge [
    source 34
    target 886
  ]
  edge [
    source 34
    target 271
  ]
  edge [
    source 34
    target 887
  ]
  edge [
    source 34
    target 888
  ]
  edge [
    source 34
    target 889
  ]
  edge [
    source 34
    target 890
  ]
  edge [
    source 34
    target 891
  ]
  edge [
    source 34
    target 892
  ]
  edge [
    source 34
    target 893
  ]
  edge [
    source 34
    target 894
  ]
  edge [
    source 34
    target 895
  ]
  edge [
    source 34
    target 896
  ]
  edge [
    source 34
    target 897
  ]
  edge [
    source 34
    target 701
  ]
  edge [
    source 34
    target 898
  ]
  edge [
    source 34
    target 899
  ]
  edge [
    source 34
    target 900
  ]
  edge [
    source 34
    target 372
  ]
  edge [
    source 34
    target 901
  ]
  edge [
    source 34
    target 902
  ]
  edge [
    source 34
    target 903
  ]
  edge [
    source 34
    target 904
  ]
  edge [
    source 34
    target 905
  ]
  edge [
    source 34
    target 744
  ]
  edge [
    source 34
    target 906
  ]
  edge [
    source 34
    target 907
  ]
  edge [
    source 34
    target 908
  ]
  edge [
    source 34
    target 909
  ]
  edge [
    source 34
    target 910
  ]
  edge [
    source 34
    target 911
  ]
  edge [
    source 34
    target 912
  ]
  edge [
    source 34
    target 239
  ]
  edge [
    source 34
    target 913
  ]
  edge [
    source 34
    target 914
  ]
  edge [
    source 34
    target 915
  ]
  edge [
    source 34
    target 916
  ]
  edge [
    source 34
    target 917
  ]
  edge [
    source 34
    target 918
  ]
  edge [
    source 34
    target 919
  ]
  edge [
    source 34
    target 106
  ]
  edge [
    source 34
    target 691
  ]
  edge [
    source 34
    target 920
  ]
  edge [
    source 34
    target 921
  ]
  edge [
    source 34
    target 922
  ]
  edge [
    source 34
    target 923
  ]
  edge [
    source 34
    target 924
  ]
  edge [
    source 34
    target 925
  ]
  edge [
    source 34
    target 926
  ]
  edge [
    source 34
    target 927
  ]
  edge [
    source 34
    target 928
  ]
  edge [
    source 34
    target 929
  ]
  edge [
    source 34
    target 930
  ]
  edge [
    source 34
    target 931
  ]
  edge [
    source 35
    target 932
  ]
  edge [
    source 35
    target 933
  ]
  edge [
    source 35
    target 934
  ]
  edge [
    source 35
    target 935
  ]
  edge [
    source 35
    target 936
  ]
  edge [
    source 35
    target 937
  ]
  edge [
    source 35
    target 938
  ]
  edge [
    source 35
    target 939
  ]
  edge [
    source 35
    target 940
  ]
  edge [
    source 35
    target 941
  ]
  edge [
    source 35
    target 41
  ]
  edge [
    source 35
    target 942
  ]
  edge [
    source 35
    target 943
  ]
  edge [
    source 35
    target 944
  ]
  edge [
    source 35
    target 945
  ]
  edge [
    source 35
    target 369
  ]
  edge [
    source 35
    target 946
  ]
  edge [
    source 35
    target 947
  ]
  edge [
    source 35
    target 948
  ]
  edge [
    source 35
    target 949
  ]
  edge [
    source 35
    target 950
  ]
  edge [
    source 35
    target 951
  ]
  edge [
    source 35
    target 952
  ]
  edge [
    source 35
    target 953
  ]
  edge [
    source 35
    target 954
  ]
  edge [
    source 35
    target 955
  ]
  edge [
    source 35
    target 956
  ]
  edge [
    source 35
    target 263
  ]
  edge [
    source 35
    target 957
  ]
  edge [
    source 35
    target 958
  ]
  edge [
    source 35
    target 959
  ]
  edge [
    source 36
    target 960
  ]
  edge [
    source 36
    target 961
  ]
  edge [
    source 36
    target 962
  ]
  edge [
    source 36
    target 963
  ]
  edge [
    source 36
    target 271
  ]
  edge [
    source 36
    target 964
  ]
  edge [
    source 36
    target 965
  ]
  edge [
    source 36
    target 966
  ]
  edge [
    source 36
    target 876
  ]
  edge [
    source 36
    target 967
  ]
  edge [
    source 36
    target 968
  ]
  edge [
    source 36
    target 969
  ]
  edge [
    source 36
    target 970
  ]
  edge [
    source 36
    target 971
  ]
  edge [
    source 36
    target 972
  ]
  edge [
    source 36
    target 973
  ]
  edge [
    source 36
    target 974
  ]
  edge [
    source 36
    target 975
  ]
  edge [
    source 36
    target 976
  ]
  edge [
    source 36
    target 977
  ]
  edge [
    source 36
    target 978
  ]
  edge [
    source 36
    target 979
  ]
  edge [
    source 36
    target 980
  ]
  edge [
    source 36
    target 981
  ]
  edge [
    source 36
    target 215
  ]
  edge [
    source 36
    target 982
  ]
  edge [
    source 36
    target 983
  ]
  edge [
    source 36
    target 984
  ]
  edge [
    source 36
    target 985
  ]
  edge [
    source 36
    target 986
  ]
  edge [
    source 36
    target 987
  ]
  edge [
    source 36
    target 988
  ]
  edge [
    source 36
    target 989
  ]
  edge [
    source 36
    target 990
  ]
  edge [
    source 36
    target 991
  ]
  edge [
    source 36
    target 992
  ]
  edge [
    source 36
    target 993
  ]
  edge [
    source 36
    target 994
  ]
  edge [
    source 36
    target 995
  ]
  edge [
    source 36
    target 996
  ]
  edge [
    source 36
    target 997
  ]
  edge [
    source 36
    target 998
  ]
  edge [
    source 36
    target 999
  ]
  edge [
    source 36
    target 1000
  ]
  edge [
    source 36
    target 1001
  ]
  edge [
    source 36
    target 1002
  ]
  edge [
    source 36
    target 1003
  ]
  edge [
    source 36
    target 1004
  ]
  edge [
    source 36
    target 1005
  ]
  edge [
    source 36
    target 1006
  ]
  edge [
    source 36
    target 1007
  ]
  edge [
    source 36
    target 799
  ]
  edge [
    source 36
    target 1008
  ]
  edge [
    source 36
    target 1009
  ]
  edge [
    source 36
    target 1010
  ]
  edge [
    source 36
    target 1011
  ]
  edge [
    source 36
    target 1012
  ]
  edge [
    source 36
    target 1013
  ]
  edge [
    source 36
    target 102
  ]
  edge [
    source 36
    target 1014
  ]
  edge [
    source 36
    target 1015
  ]
  edge [
    source 36
    target 1016
  ]
  edge [
    source 36
    target 297
  ]
  edge [
    source 36
    target 1017
  ]
  edge [
    source 36
    target 824
  ]
  edge [
    source 36
    target 1018
  ]
  edge [
    source 36
    target 1019
  ]
  edge [
    source 36
    target 1020
  ]
  edge [
    source 36
    target 292
  ]
  edge [
    source 36
    target 1021
  ]
  edge [
    source 36
    target 1022
  ]
  edge [
    source 36
    target 1023
  ]
  edge [
    source 36
    target 1024
  ]
  edge [
    source 36
    target 1025
  ]
  edge [
    source 36
    target 850
  ]
  edge [
    source 36
    target 1026
  ]
  edge [
    source 36
    target 1027
  ]
  edge [
    source 36
    target 1028
  ]
  edge [
    source 36
    target 1029
  ]
  edge [
    source 36
    target 1030
  ]
  edge [
    source 36
    target 1031
  ]
  edge [
    source 36
    target 1032
  ]
  edge [
    source 36
    target 1033
  ]
  edge [
    source 36
    target 1034
  ]
  edge [
    source 36
    target 1035
  ]
  edge [
    source 36
    target 1036
  ]
  edge [
    source 36
    target 1037
  ]
  edge [
    source 36
    target 1038
  ]
  edge [
    source 36
    target 1039
  ]
  edge [
    source 36
    target 1040
  ]
  edge [
    source 36
    target 1041
  ]
  edge [
    source 36
    target 1042
  ]
  edge [
    source 36
    target 1043
  ]
  edge [
    source 36
    target 1044
  ]
  edge [
    source 36
    target 1045
  ]
  edge [
    source 36
    target 1046
  ]
  edge [
    source 36
    target 1047
  ]
  edge [
    source 36
    target 1048
  ]
  edge [
    source 36
    target 1049
  ]
  edge [
    source 36
    target 1050
  ]
  edge [
    source 36
    target 1051
  ]
  edge [
    source 36
    target 1052
  ]
  edge [
    source 36
    target 1053
  ]
  edge [
    source 36
    target 1054
  ]
  edge [
    source 36
    target 1055
  ]
  edge [
    source 36
    target 1056
  ]
  edge [
    source 36
    target 1057
  ]
  edge [
    source 36
    target 1058
  ]
  edge [
    source 36
    target 1059
  ]
  edge [
    source 36
    target 1060
  ]
  edge [
    source 36
    target 1061
  ]
  edge [
    source 36
    target 1062
  ]
  edge [
    source 36
    target 1063
  ]
  edge [
    source 36
    target 1064
  ]
  edge [
    source 36
    target 1065
  ]
  edge [
    source 36
    target 1066
  ]
  edge [
    source 36
    target 1067
  ]
  edge [
    source 36
    target 1068
  ]
  edge [
    source 36
    target 1069
  ]
  edge [
    source 36
    target 1070
  ]
  edge [
    source 36
    target 1071
  ]
  edge [
    source 36
    target 1072
  ]
  edge [
    source 36
    target 1073
  ]
  edge [
    source 36
    target 1074
  ]
  edge [
    source 36
    target 1075
  ]
  edge [
    source 36
    target 1076
  ]
  edge [
    source 36
    target 1077
  ]
  edge [
    source 36
    target 1078
  ]
  edge [
    source 36
    target 1079
  ]
  edge [
    source 36
    target 1080
  ]
  edge [
    source 36
    target 1081
  ]
  edge [
    source 36
    target 1082
  ]
  edge [
    source 36
    target 1083
  ]
  edge [
    source 36
    target 1084
  ]
  edge [
    source 36
    target 1085
  ]
  edge [
    source 36
    target 1086
  ]
  edge [
    source 36
    target 1087
  ]
  edge [
    source 36
    target 1088
  ]
  edge [
    source 36
    target 1089
  ]
  edge [
    source 36
    target 1090
  ]
  edge [
    source 36
    target 1091
  ]
  edge [
    source 36
    target 1092
  ]
  edge [
    source 36
    target 1093
  ]
  edge [
    source 36
    target 1094
  ]
  edge [
    source 36
    target 1095
  ]
  edge [
    source 36
    target 1096
  ]
  edge [
    source 36
    target 1097
  ]
  edge [
    source 36
    target 1098
  ]
  edge [
    source 36
    target 1099
  ]
  edge [
    source 36
    target 1100
  ]
  edge [
    source 36
    target 1101
  ]
  edge [
    source 36
    target 1102
  ]
  edge [
    source 36
    target 1103
  ]
  edge [
    source 36
    target 1104
  ]
  edge [
    source 36
    target 1105
  ]
  edge [
    source 36
    target 1106
  ]
  edge [
    source 36
    target 1107
  ]
  edge [
    source 36
    target 1108
  ]
  edge [
    source 36
    target 1109
  ]
  edge [
    source 36
    target 1110
  ]
  edge [
    source 36
    target 1111
  ]
  edge [
    source 36
    target 1112
  ]
  edge [
    source 36
    target 1113
  ]
  edge [
    source 36
    target 1114
  ]
  edge [
    source 36
    target 1115
  ]
  edge [
    source 36
    target 1116
  ]
  edge [
    source 36
    target 1117
  ]
  edge [
    source 36
    target 1118
  ]
  edge [
    source 36
    target 1119
  ]
  edge [
    source 36
    target 1120
  ]
  edge [
    source 36
    target 1121
  ]
  edge [
    source 36
    target 1122
  ]
  edge [
    source 36
    target 1123
  ]
  edge [
    source 36
    target 1124
  ]
  edge [
    source 36
    target 1125
  ]
  edge [
    source 36
    target 1126
  ]
  edge [
    source 36
    target 1127
  ]
  edge [
    source 36
    target 1128
  ]
  edge [
    source 36
    target 1129
  ]
  edge [
    source 36
    target 1130
  ]
  edge [
    source 36
    target 1131
  ]
  edge [
    source 36
    target 1132
  ]
  edge [
    source 36
    target 1133
  ]
  edge [
    source 36
    target 1134
  ]
  edge [
    source 36
    target 1135
  ]
  edge [
    source 36
    target 1136
  ]
  edge [
    source 36
    target 1137
  ]
  edge [
    source 36
    target 1138
  ]
  edge [
    source 36
    target 1139
  ]
  edge [
    source 36
    target 1140
  ]
  edge [
    source 36
    target 1141
  ]
  edge [
    source 36
    target 1142
  ]
  edge [
    source 36
    target 1143
  ]
  edge [
    source 36
    target 1144
  ]
  edge [
    source 36
    target 1145
  ]
  edge [
    source 36
    target 1146
  ]
  edge [
    source 36
    target 1147
  ]
  edge [
    source 36
    target 1148
  ]
  edge [
    source 36
    target 1149
  ]
  edge [
    source 36
    target 1150
  ]
  edge [
    source 36
    target 1151
  ]
  edge [
    source 36
    target 1152
  ]
  edge [
    source 36
    target 1153
  ]
  edge [
    source 36
    target 1154
  ]
  edge [
    source 36
    target 1155
  ]
  edge [
    source 36
    target 1156
  ]
  edge [
    source 36
    target 1157
  ]
  edge [
    source 36
    target 1158
  ]
  edge [
    source 36
    target 1159
  ]
  edge [
    source 36
    target 1160
  ]
  edge [
    source 36
    target 1161
  ]
  edge [
    source 36
    target 1162
  ]
  edge [
    source 36
    target 1163
  ]
  edge [
    source 36
    target 1164
  ]
  edge [
    source 36
    target 1165
  ]
  edge [
    source 36
    target 1166
  ]
  edge [
    source 36
    target 1167
  ]
  edge [
    source 36
    target 1168
  ]
  edge [
    source 36
    target 1169
  ]
  edge [
    source 36
    target 1170
  ]
  edge [
    source 36
    target 1171
  ]
  edge [
    source 36
    target 1172
  ]
  edge [
    source 36
    target 1173
  ]
  edge [
    source 36
    target 1174
  ]
  edge [
    source 36
    target 1175
  ]
  edge [
    source 36
    target 1176
  ]
  edge [
    source 36
    target 1177
  ]
  edge [
    source 36
    target 1178
  ]
  edge [
    source 36
    target 1179
  ]
  edge [
    source 36
    target 1180
  ]
  edge [
    source 36
    target 1181
  ]
  edge [
    source 36
    target 1182
  ]
  edge [
    source 36
    target 1183
  ]
  edge [
    source 36
    target 1184
  ]
  edge [
    source 36
    target 1185
  ]
  edge [
    source 36
    target 1186
  ]
  edge [
    source 36
    target 1187
  ]
  edge [
    source 36
    target 1188
  ]
  edge [
    source 36
    target 1189
  ]
  edge [
    source 36
    target 1190
  ]
  edge [
    source 36
    target 1191
  ]
  edge [
    source 36
    target 1192
  ]
  edge [
    source 36
    target 1193
  ]
  edge [
    source 36
    target 1194
  ]
  edge [
    source 36
    target 1195
  ]
  edge [
    source 36
    target 1196
  ]
  edge [
    source 36
    target 1197
  ]
  edge [
    source 36
    target 1198
  ]
  edge [
    source 36
    target 1199
  ]
  edge [
    source 36
    target 1200
  ]
  edge [
    source 36
    target 1201
  ]
  edge [
    source 36
    target 1202
  ]
  edge [
    source 36
    target 1203
  ]
  edge [
    source 36
    target 1204
  ]
  edge [
    source 36
    target 1205
  ]
  edge [
    source 36
    target 1206
  ]
  edge [
    source 36
    target 1207
  ]
  edge [
    source 36
    target 1208
  ]
  edge [
    source 36
    target 1209
  ]
  edge [
    source 36
    target 1210
  ]
  edge [
    source 36
    target 1211
  ]
  edge [
    source 36
    target 1212
  ]
  edge [
    source 36
    target 1213
  ]
  edge [
    source 36
    target 1214
  ]
  edge [
    source 36
    target 1215
  ]
  edge [
    source 36
    target 1216
  ]
  edge [
    source 36
    target 1217
  ]
  edge [
    source 36
    target 1218
  ]
  edge [
    source 36
    target 1219
  ]
  edge [
    source 36
    target 1220
  ]
  edge [
    source 36
    target 1221
  ]
  edge [
    source 36
    target 1222
  ]
  edge [
    source 36
    target 1223
  ]
  edge [
    source 36
    target 1224
  ]
  edge [
    source 36
    target 1225
  ]
  edge [
    source 36
    target 1226
  ]
  edge [
    source 36
    target 1227
  ]
  edge [
    source 36
    target 1228
  ]
  edge [
    source 36
    target 1229
  ]
  edge [
    source 36
    target 1230
  ]
  edge [
    source 36
    target 1231
  ]
  edge [
    source 36
    target 1232
  ]
  edge [
    source 36
    target 1233
  ]
  edge [
    source 36
    target 1234
  ]
  edge [
    source 36
    target 1235
  ]
  edge [
    source 36
    target 1236
  ]
  edge [
    source 36
    target 1237
  ]
  edge [
    source 36
    target 1238
  ]
  edge [
    source 36
    target 1239
  ]
  edge [
    source 36
    target 1240
  ]
  edge [
    source 36
    target 1241
  ]
  edge [
    source 36
    target 1242
  ]
  edge [
    source 36
    target 1243
  ]
  edge [
    source 36
    target 1244
  ]
  edge [
    source 36
    target 1245
  ]
  edge [
    source 36
    target 1246
  ]
  edge [
    source 36
    target 1247
  ]
  edge [
    source 36
    target 1248
  ]
  edge [
    source 36
    target 1249
  ]
  edge [
    source 36
    target 1250
  ]
  edge [
    source 36
    target 1251
  ]
  edge [
    source 36
    target 1252
  ]
  edge [
    source 36
    target 1253
  ]
  edge [
    source 36
    target 1254
  ]
  edge [
    source 36
    target 1255
  ]
  edge [
    source 36
    target 1256
  ]
  edge [
    source 36
    target 1257
  ]
  edge [
    source 36
    target 1258
  ]
  edge [
    source 36
    target 1259
  ]
  edge [
    source 36
    target 1260
  ]
  edge [
    source 36
    target 1261
  ]
  edge [
    source 36
    target 1262
  ]
  edge [
    source 36
    target 1263
  ]
  edge [
    source 36
    target 1264
  ]
  edge [
    source 36
    target 1265
  ]
  edge [
    source 36
    target 1266
  ]
  edge [
    source 36
    target 1267
  ]
  edge [
    source 36
    target 1268
  ]
  edge [
    source 36
    target 1269
  ]
  edge [
    source 36
    target 1270
  ]
  edge [
    source 36
    target 1271
  ]
  edge [
    source 36
    target 1272
  ]
  edge [
    source 36
    target 1273
  ]
  edge [
    source 36
    target 1274
  ]
  edge [
    source 36
    target 1275
  ]
  edge [
    source 36
    target 1276
  ]
  edge [
    source 36
    target 1277
  ]
  edge [
    source 36
    target 1278
  ]
  edge [
    source 36
    target 1279
  ]
  edge [
    source 36
    target 1280
  ]
  edge [
    source 36
    target 1281
  ]
  edge [
    source 36
    target 1282
  ]
  edge [
    source 36
    target 1283
  ]
  edge [
    source 36
    target 1284
  ]
  edge [
    source 36
    target 1285
  ]
  edge [
    source 36
    target 1286
  ]
  edge [
    source 36
    target 1287
  ]
  edge [
    source 36
    target 1288
  ]
  edge [
    source 36
    target 1289
  ]
  edge [
    source 36
    target 1290
  ]
  edge [
    source 36
    target 1291
  ]
  edge [
    source 36
    target 1292
  ]
  edge [
    source 36
    target 1293
  ]
  edge [
    source 36
    target 1294
  ]
  edge [
    source 36
    target 1295
  ]
  edge [
    source 36
    target 1296
  ]
  edge [
    source 36
    target 1297
  ]
  edge [
    source 36
    target 1298
  ]
  edge [
    source 36
    target 1299
  ]
  edge [
    source 36
    target 1300
  ]
  edge [
    source 36
    target 1301
  ]
  edge [
    source 36
    target 1302
  ]
  edge [
    source 36
    target 1303
  ]
  edge [
    source 36
    target 1304
  ]
  edge [
    source 36
    target 1305
  ]
  edge [
    source 36
    target 1306
  ]
  edge [
    source 36
    target 1307
  ]
  edge [
    source 36
    target 1308
  ]
  edge [
    source 36
    target 1309
  ]
  edge [
    source 36
    target 1310
  ]
  edge [
    source 36
    target 1311
  ]
  edge [
    source 36
    target 1312
  ]
  edge [
    source 36
    target 1313
  ]
  edge [
    source 36
    target 1314
  ]
  edge [
    source 36
    target 1315
  ]
  edge [
    source 36
    target 1316
  ]
  edge [
    source 36
    target 1317
  ]
  edge [
    source 36
    target 1318
  ]
  edge [
    source 36
    target 1319
  ]
  edge [
    source 36
    target 1320
  ]
  edge [
    source 36
    target 1321
  ]
  edge [
    source 36
    target 1322
  ]
  edge [
    source 36
    target 1323
  ]
  edge [
    source 36
    target 1324
  ]
  edge [
    source 36
    target 1325
  ]
  edge [
    source 36
    target 1326
  ]
  edge [
    source 36
    target 1327
  ]
  edge [
    source 36
    target 1328
  ]
  edge [
    source 36
    target 1329
  ]
  edge [
    source 36
    target 1330
  ]
  edge [
    source 36
    target 1331
  ]
  edge [
    source 36
    target 1332
  ]
  edge [
    source 36
    target 1333
  ]
  edge [
    source 36
    target 1334
  ]
  edge [
    source 36
    target 1335
  ]
  edge [
    source 36
    target 1336
  ]
  edge [
    source 36
    target 1337
  ]
  edge [
    source 36
    target 1338
  ]
  edge [
    source 36
    target 1339
  ]
  edge [
    source 36
    target 287
  ]
  edge [
    source 36
    target 1340
  ]
  edge [
    source 36
    target 1341
  ]
  edge [
    source 36
    target 1342
  ]
  edge [
    source 36
    target 1343
  ]
  edge [
    source 36
    target 1344
  ]
  edge [
    source 36
    target 1345
  ]
  edge [
    source 36
    target 1346
  ]
  edge [
    source 36
    target 1347
  ]
  edge [
    source 36
    target 1348
  ]
  edge [
    source 36
    target 1349
  ]
  edge [
    source 36
    target 1350
  ]
  edge [
    source 36
    target 1351
  ]
  edge [
    source 36
    target 1352
  ]
  edge [
    source 36
    target 1353
  ]
  edge [
    source 36
    target 1354
  ]
  edge [
    source 36
    target 1355
  ]
  edge [
    source 36
    target 1356
  ]
  edge [
    source 36
    target 1357
  ]
  edge [
    source 36
    target 1358
  ]
  edge [
    source 36
    target 1359
  ]
  edge [
    source 36
    target 1360
  ]
  edge [
    source 36
    target 1361
  ]
  edge [
    source 36
    target 1362
  ]
  edge [
    source 36
    target 1363
  ]
  edge [
    source 36
    target 1364
  ]
  edge [
    source 36
    target 1365
  ]
  edge [
    source 36
    target 1366
  ]
  edge [
    source 36
    target 1367
  ]
  edge [
    source 36
    target 1368
  ]
  edge [
    source 36
    target 1369
  ]
  edge [
    source 36
    target 1370
  ]
  edge [
    source 36
    target 1371
  ]
  edge [
    source 36
    target 1372
  ]
  edge [
    source 36
    target 1373
  ]
  edge [
    source 36
    target 1374
  ]
  edge [
    source 36
    target 1375
  ]
  edge [
    source 36
    target 1376
  ]
  edge [
    source 36
    target 1377
  ]
  edge [
    source 36
    target 1378
  ]
  edge [
    source 36
    target 1379
  ]
  edge [
    source 36
    target 1380
  ]
  edge [
    source 36
    target 1381
  ]
  edge [
    source 36
    target 1382
  ]
  edge [
    source 36
    target 1383
  ]
  edge [
    source 36
    target 1384
  ]
  edge [
    source 36
    target 1385
  ]
  edge [
    source 36
    target 1386
  ]
  edge [
    source 36
    target 1387
  ]
  edge [
    source 36
    target 1388
  ]
  edge [
    source 36
    target 1389
  ]
  edge [
    source 36
    target 1390
  ]
  edge [
    source 36
    target 1391
  ]
  edge [
    source 36
    target 1392
  ]
  edge [
    source 36
    target 1393
  ]
  edge [
    source 36
    target 1394
  ]
  edge [
    source 36
    target 1395
  ]
  edge [
    source 36
    target 1396
  ]
  edge [
    source 36
    target 1397
  ]
  edge [
    source 36
    target 1398
  ]
  edge [
    source 36
    target 1399
  ]
  edge [
    source 36
    target 1400
  ]
  edge [
    source 36
    target 1401
  ]
  edge [
    source 36
    target 1402
  ]
  edge [
    source 36
    target 1403
  ]
  edge [
    source 36
    target 1404
  ]
  edge [
    source 36
    target 1405
  ]
  edge [
    source 36
    target 1406
  ]
  edge [
    source 36
    target 1407
  ]
  edge [
    source 36
    target 1408
  ]
  edge [
    source 36
    target 1409
  ]
  edge [
    source 36
    target 1410
  ]
  edge [
    source 36
    target 1411
  ]
  edge [
    source 36
    target 1412
  ]
  edge [
    source 36
    target 1413
  ]
  edge [
    source 36
    target 1414
  ]
  edge [
    source 36
    target 1415
  ]
  edge [
    source 36
    target 1416
  ]
  edge [
    source 36
    target 1417
  ]
  edge [
    source 36
    target 1418
  ]
  edge [
    source 36
    target 1419
  ]
  edge [
    source 36
    target 1420
  ]
  edge [
    source 36
    target 1421
  ]
  edge [
    source 36
    target 1422
  ]
  edge [
    source 36
    target 1423
  ]
  edge [
    source 36
    target 1424
  ]
  edge [
    source 36
    target 1425
  ]
  edge [
    source 36
    target 1426
  ]
  edge [
    source 36
    target 1427
  ]
  edge [
    source 36
    target 1428
  ]
  edge [
    source 36
    target 1429
  ]
  edge [
    source 36
    target 1430
  ]
  edge [
    source 36
    target 1431
  ]
  edge [
    source 36
    target 1432
  ]
  edge [
    source 36
    target 1433
  ]
  edge [
    source 36
    target 1434
  ]
  edge [
    source 36
    target 1435
  ]
  edge [
    source 36
    target 1436
  ]
  edge [
    source 36
    target 1437
  ]
  edge [
    source 36
    target 1438
  ]
  edge [
    source 36
    target 1439
  ]
  edge [
    source 36
    target 1440
  ]
  edge [
    source 36
    target 1441
  ]
  edge [
    source 36
    target 1442
  ]
  edge [
    source 36
    target 1443
  ]
  edge [
    source 36
    target 1444
  ]
  edge [
    source 36
    target 1445
  ]
  edge [
    source 36
    target 1446
  ]
  edge [
    source 36
    target 1447
  ]
  edge [
    source 36
    target 1448
  ]
  edge [
    source 36
    target 1449
  ]
  edge [
    source 36
    target 1450
  ]
  edge [
    source 36
    target 1451
  ]
  edge [
    source 36
    target 1452
  ]
  edge [
    source 36
    target 1453
  ]
  edge [
    source 36
    target 1454
  ]
  edge [
    source 36
    target 1455
  ]
  edge [
    source 36
    target 1456
  ]
  edge [
    source 36
    target 1457
  ]
  edge [
    source 36
    target 1458
  ]
  edge [
    source 36
    target 1459
  ]
  edge [
    source 36
    target 1460
  ]
  edge [
    source 36
    target 1461
  ]
  edge [
    source 36
    target 1462
  ]
  edge [
    source 36
    target 1463
  ]
  edge [
    source 36
    target 1464
  ]
  edge [
    source 36
    target 1465
  ]
  edge [
    source 36
    target 1466
  ]
  edge [
    source 36
    target 1467
  ]
  edge [
    source 36
    target 1468
  ]
  edge [
    source 36
    target 1469
  ]
  edge [
    source 36
    target 1470
  ]
  edge [
    source 36
    target 1471
  ]
  edge [
    source 36
    target 1472
  ]
  edge [
    source 36
    target 1473
  ]
  edge [
    source 36
    target 1474
  ]
  edge [
    source 36
    target 1475
  ]
  edge [
    source 36
    target 1476
  ]
  edge [
    source 36
    target 1477
  ]
  edge [
    source 36
    target 1478
  ]
  edge [
    source 36
    target 1479
  ]
  edge [
    source 36
    target 1480
  ]
  edge [
    source 36
    target 1481
  ]
  edge [
    source 36
    target 1482
  ]
  edge [
    source 36
    target 1483
  ]
  edge [
    source 36
    target 1484
  ]
  edge [
    source 36
    target 1485
  ]
  edge [
    source 36
    target 1486
  ]
  edge [
    source 36
    target 1487
  ]
  edge [
    source 36
    target 1488
  ]
  edge [
    source 36
    target 1489
  ]
  edge [
    source 36
    target 1490
  ]
  edge [
    source 36
    target 1491
  ]
  edge [
    source 36
    target 1492
  ]
  edge [
    source 36
    target 1493
  ]
  edge [
    source 36
    target 1494
  ]
  edge [
    source 36
    target 1495
  ]
  edge [
    source 36
    target 1496
  ]
  edge [
    source 36
    target 1497
  ]
  edge [
    source 36
    target 1498
  ]
  edge [
    source 36
    target 1499
  ]
  edge [
    source 36
    target 1500
  ]
  edge [
    source 36
    target 1501
  ]
  edge [
    source 36
    target 1502
  ]
  edge [
    source 36
    target 1503
  ]
  edge [
    source 36
    target 1504
  ]
  edge [
    source 36
    target 1505
  ]
  edge [
    source 36
    target 1506
  ]
  edge [
    source 36
    target 1507
  ]
  edge [
    source 36
    target 1508
  ]
  edge [
    source 36
    target 1509
  ]
  edge [
    source 36
    target 1510
  ]
  edge [
    source 36
    target 1511
  ]
  edge [
    source 36
    target 1512
  ]
  edge [
    source 36
    target 1513
  ]
  edge [
    source 36
    target 1514
  ]
  edge [
    source 36
    target 1515
  ]
  edge [
    source 36
    target 1516
  ]
  edge [
    source 36
    target 1517
  ]
  edge [
    source 36
    target 1518
  ]
  edge [
    source 36
    target 1519
  ]
  edge [
    source 36
    target 1520
  ]
  edge [
    source 36
    target 1521
  ]
  edge [
    source 36
    target 1522
  ]
  edge [
    source 36
    target 1523
  ]
  edge [
    source 36
    target 1524
  ]
  edge [
    source 36
    target 1525
  ]
  edge [
    source 36
    target 1526
  ]
  edge [
    source 36
    target 1527
  ]
  edge [
    source 36
    target 1528
  ]
  edge [
    source 36
    target 1529
  ]
  edge [
    source 36
    target 1530
  ]
  edge [
    source 36
    target 1531
  ]
  edge [
    source 36
    target 1532
  ]
  edge [
    source 36
    target 1533
  ]
  edge [
    source 36
    target 1534
  ]
  edge [
    source 36
    target 1535
  ]
  edge [
    source 36
    target 1536
  ]
  edge [
    source 36
    target 1537
  ]
  edge [
    source 36
    target 1538
  ]
  edge [
    source 36
    target 1539
  ]
  edge [
    source 36
    target 1540
  ]
  edge [
    source 36
    target 1541
  ]
  edge [
    source 36
    target 1542
  ]
  edge [
    source 36
    target 1543
  ]
  edge [
    source 36
    target 1544
  ]
  edge [
    source 36
    target 1545
  ]
  edge [
    source 36
    target 1546
  ]
  edge [
    source 36
    target 1547
  ]
  edge [
    source 36
    target 1548
  ]
  edge [
    source 36
    target 1549
  ]
  edge [
    source 36
    target 1550
  ]
  edge [
    source 36
    target 1551
  ]
  edge [
    source 36
    target 1552
  ]
  edge [
    source 36
    target 1553
  ]
  edge [
    source 36
    target 1554
  ]
  edge [
    source 36
    target 1555
  ]
  edge [
    source 36
    target 1556
  ]
  edge [
    source 36
    target 1557
  ]
  edge [
    source 36
    target 1558
  ]
  edge [
    source 36
    target 1559
  ]
  edge [
    source 36
    target 1560
  ]
  edge [
    source 36
    target 1561
  ]
  edge [
    source 36
    target 1562
  ]
  edge [
    source 36
    target 1563
  ]
  edge [
    source 36
    target 1564
  ]
  edge [
    source 36
    target 1565
  ]
  edge [
    source 36
    target 1566
  ]
  edge [
    source 36
    target 1567
  ]
  edge [
    source 36
    target 1568
  ]
  edge [
    source 36
    target 1569
  ]
  edge [
    source 36
    target 1570
  ]
  edge [
    source 36
    target 1571
  ]
  edge [
    source 36
    target 1572
  ]
  edge [
    source 36
    target 1573
  ]
  edge [
    source 36
    target 1574
  ]
  edge [
    source 36
    target 1575
  ]
  edge [
    source 36
    target 1576
  ]
  edge [
    source 36
    target 1577
  ]
  edge [
    source 36
    target 1578
  ]
  edge [
    source 36
    target 1579
  ]
  edge [
    source 36
    target 1580
  ]
  edge [
    source 36
    target 1581
  ]
  edge [
    source 36
    target 1582
  ]
  edge [
    source 36
    target 1583
  ]
  edge [
    source 36
    target 1584
  ]
  edge [
    source 36
    target 1585
  ]
  edge [
    source 36
    target 1586
  ]
  edge [
    source 36
    target 1587
  ]
  edge [
    source 36
    target 1588
  ]
  edge [
    source 36
    target 1589
  ]
  edge [
    source 36
    target 1590
  ]
  edge [
    source 36
    target 1591
  ]
  edge [
    source 36
    target 1592
  ]
  edge [
    source 36
    target 1593
  ]
  edge [
    source 36
    target 1594
  ]
  edge [
    source 36
    target 1595
  ]
  edge [
    source 36
    target 1596
  ]
  edge [
    source 36
    target 1597
  ]
  edge [
    source 36
    target 1598
  ]
  edge [
    source 36
    target 1599
  ]
  edge [
    source 36
    target 1600
  ]
  edge [
    source 36
    target 1601
  ]
  edge [
    source 36
    target 1602
  ]
  edge [
    source 36
    target 1603
  ]
  edge [
    source 36
    target 1604
  ]
  edge [
    source 36
    target 1605
  ]
  edge [
    source 36
    target 1606
  ]
  edge [
    source 36
    target 1607
  ]
  edge [
    source 36
    target 1608
  ]
  edge [
    source 36
    target 1609
  ]
  edge [
    source 36
    target 1610
  ]
  edge [
    source 36
    target 1611
  ]
  edge [
    source 36
    target 1612
  ]
  edge [
    source 36
    target 1613
  ]
  edge [
    source 36
    target 1614
  ]
  edge [
    source 36
    target 1615
  ]
  edge [
    source 36
    target 1616
  ]
  edge [
    source 36
    target 1617
  ]
  edge [
    source 36
    target 1618
  ]
  edge [
    source 36
    target 1619
  ]
  edge [
    source 36
    target 1620
  ]
  edge [
    source 36
    target 1621
  ]
  edge [
    source 36
    target 1622
  ]
  edge [
    source 36
    target 1623
  ]
  edge [
    source 36
    target 1624
  ]
  edge [
    source 36
    target 908
  ]
  edge [
    source 36
    target 481
  ]
  edge [
    source 36
    target 1625
  ]
  edge [
    source 36
    target 1626
  ]
  edge [
    source 36
    target 106
  ]
  edge [
    source 36
    target 189
  ]
  edge [
    source 36
    target 1627
  ]
]
