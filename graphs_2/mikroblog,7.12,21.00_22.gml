graph [
  node [
    id 0
    label "um&#243;wi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "wczoraj"
    origin "text"
  ]
  node [
    id 3
    label "galeria"
    origin "text"
  ]
  node [
    id 4
    label "krakowski"
    origin "text"
  ]
  node [
    id 5
    label "pizzahut"
    origin "text"
  ]
  node [
    id 6
    label "moja"
    origin "text"
  ]
  node [
    id 7
    label "kumpela"
    origin "text"
  ]
  node [
    id 8
    label "dawno"
  ]
  node [
    id 9
    label "doba"
  ]
  node [
    id 10
    label "niedawno"
  ]
  node [
    id 11
    label "aktualnie"
  ]
  node [
    id 12
    label "ostatni"
  ]
  node [
    id 13
    label "dawny"
  ]
  node [
    id 14
    label "d&#322;ugotrwale"
  ]
  node [
    id 15
    label "wcze&#347;niej"
  ]
  node [
    id 16
    label "ongi&#347;"
  ]
  node [
    id 17
    label "dawnie"
  ]
  node [
    id 18
    label "tydzie&#324;"
  ]
  node [
    id 19
    label "noc"
  ]
  node [
    id 20
    label "dzie&#324;"
  ]
  node [
    id 21
    label "czas"
  ]
  node [
    id 22
    label "godzina"
  ]
  node [
    id 23
    label "long_time"
  ]
  node [
    id 24
    label "jednostka_geologiczna"
  ]
  node [
    id 25
    label "balkon"
  ]
  node [
    id 26
    label "eskalator"
  ]
  node [
    id 27
    label "zbi&#243;r"
  ]
  node [
    id 28
    label "&#322;&#261;cznik"
  ]
  node [
    id 29
    label "wystawa"
  ]
  node [
    id 30
    label "sala"
  ]
  node [
    id 31
    label "publiczno&#347;&#263;"
  ]
  node [
    id 32
    label "Galeria_Arsena&#322;"
  ]
  node [
    id 33
    label "muzeum"
  ]
  node [
    id 34
    label "sklep"
  ]
  node [
    id 35
    label "centrum_handlowe"
  ]
  node [
    id 36
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 37
    label "ekspozycja"
  ]
  node [
    id 38
    label "instytucja"
  ]
  node [
    id 39
    label "kuratorstwo"
  ]
  node [
    id 40
    label "budynek"
  ]
  node [
    id 41
    label "widownia"
  ]
  node [
    id 42
    label "balustrada"
  ]
  node [
    id 43
    label "p&#243;&#322;ka"
  ]
  node [
    id 44
    label "firma"
  ]
  node [
    id 45
    label "stoisko"
  ]
  node [
    id 46
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 47
    label "sk&#322;ad"
  ]
  node [
    id 48
    label "obiekt_handlowy"
  ]
  node [
    id 49
    label "zaplecze"
  ]
  node [
    id 50
    label "witryna"
  ]
  node [
    id 51
    label "odbiorca"
  ]
  node [
    id 52
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 53
    label "audience"
  ]
  node [
    id 54
    label "publiczka"
  ]
  node [
    id 55
    label "widzownia"
  ]
  node [
    id 56
    label "droga"
  ]
  node [
    id 57
    label "kreska"
  ]
  node [
    id 58
    label "zwiadowca"
  ]
  node [
    id 59
    label "znak_pisarski"
  ]
  node [
    id 60
    label "czynnik"
  ]
  node [
    id 61
    label "fuga"
  ]
  node [
    id 62
    label "przew&#243;d_wiertniczy"
  ]
  node [
    id 63
    label "znak_muzyczny"
  ]
  node [
    id 64
    label "pogoniec"
  ]
  node [
    id 65
    label "S&#261;deczanka"
  ]
  node [
    id 66
    label "ligature"
  ]
  node [
    id 67
    label "orzeczenie"
  ]
  node [
    id 68
    label "rakieta"
  ]
  node [
    id 69
    label "napastnik"
  ]
  node [
    id 70
    label "po&#347;rednik"
  ]
  node [
    id 71
    label "hyphen"
  ]
  node [
    id 72
    label "kontakt"
  ]
  node [
    id 73
    label "dobud&#243;wka"
  ]
  node [
    id 74
    label "egzemplarz"
  ]
  node [
    id 75
    label "series"
  ]
  node [
    id 76
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 77
    label "uprawianie"
  ]
  node [
    id 78
    label "praca_rolnicza"
  ]
  node [
    id 79
    label "collection"
  ]
  node [
    id 80
    label "dane"
  ]
  node [
    id 81
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 82
    label "pakiet_klimatyczny"
  ]
  node [
    id 83
    label "poj&#281;cie"
  ]
  node [
    id 84
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 85
    label "sum"
  ]
  node [
    id 86
    label "gathering"
  ]
  node [
    id 87
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 88
    label "album"
  ]
  node [
    id 89
    label "zgromadzenie"
  ]
  node [
    id 90
    label "pomieszczenie"
  ]
  node [
    id 91
    label "szyba"
  ]
  node [
    id 92
    label "okno"
  ]
  node [
    id 93
    label "kolekcja"
  ]
  node [
    id 94
    label "impreza"
  ]
  node [
    id 95
    label "kustosz"
  ]
  node [
    id 96
    label "miejsce"
  ]
  node [
    id 97
    label "kurator"
  ]
  node [
    id 98
    label "Agropromocja"
  ]
  node [
    id 99
    label "wernisa&#380;"
  ]
  node [
    id 100
    label "Arsena&#322;"
  ]
  node [
    id 101
    label "schody"
  ]
  node [
    id 102
    label "g&#322;&#243;g"
  ]
  node [
    id 103
    label "ma&#322;opolski"
  ]
  node [
    id 104
    label "po_krakowsku"
  ]
  node [
    id 105
    label "polski"
  ]
  node [
    id 106
    label "regionalny"
  ]
  node [
    id 107
    label "sznycel"
  ]
  node [
    id 108
    label "po_ma&#322;opolsku"
  ]
  node [
    id 109
    label "r&#243;&#380;owate"
  ]
  node [
    id 110
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 111
    label "babia_m&#261;ka"
  ]
  node [
    id 112
    label "kwas_chlorogenowy"
  ]
  node [
    id 113
    label "hip"
  ]
  node [
    id 114
    label "drewno"
  ]
  node [
    id 115
    label "dzika_r&#243;&#380;a"
  ]
  node [
    id 116
    label "wielopestkowiec"
  ]
  node [
    id 117
    label "r&#243;&#380;a"
  ]
  node [
    id 118
    label "kuma"
  ]
  node [
    id 119
    label "kole&#380;anka"
  ]
  node [
    id 120
    label "kumostwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
]
