graph [
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "list"
    origin "text"
  ]
  node [
    id 2
    label "edycja"
    origin "text"
  ]
  node [
    id 3
    label "zapami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ten"
    origin "text"
  ]
  node [
    id 5
    label "wpis"
    origin "text"
  ]
  node [
    id 6
    label "formu&#322;owa&#263;"
  ]
  node [
    id 7
    label "ozdabia&#263;"
  ]
  node [
    id 8
    label "stawia&#263;"
  ]
  node [
    id 9
    label "spell"
  ]
  node [
    id 10
    label "styl"
  ]
  node [
    id 11
    label "skryba"
  ]
  node [
    id 12
    label "read"
  ]
  node [
    id 13
    label "donosi&#263;"
  ]
  node [
    id 14
    label "code"
  ]
  node [
    id 15
    label "tekst"
  ]
  node [
    id 16
    label "dysgrafia"
  ]
  node [
    id 17
    label "dysortografia"
  ]
  node [
    id 18
    label "tworzy&#263;"
  ]
  node [
    id 19
    label "prasa"
  ]
  node [
    id 20
    label "robi&#263;"
  ]
  node [
    id 21
    label "pope&#322;nia&#263;"
  ]
  node [
    id 22
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 23
    label "wytwarza&#263;"
  ]
  node [
    id 24
    label "get"
  ]
  node [
    id 25
    label "consist"
  ]
  node [
    id 26
    label "stanowi&#263;"
  ]
  node [
    id 27
    label "raise"
  ]
  node [
    id 28
    label "spill_the_beans"
  ]
  node [
    id 29
    label "przeby&#263;"
  ]
  node [
    id 30
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 31
    label "zanosi&#263;"
  ]
  node [
    id 32
    label "inform"
  ]
  node [
    id 33
    label "give"
  ]
  node [
    id 34
    label "zu&#380;y&#263;"
  ]
  node [
    id 35
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 36
    label "introduce"
  ]
  node [
    id 37
    label "render"
  ]
  node [
    id 38
    label "ci&#261;&#380;a"
  ]
  node [
    id 39
    label "informowa&#263;"
  ]
  node [
    id 40
    label "komunikowa&#263;"
  ]
  node [
    id 41
    label "convey"
  ]
  node [
    id 42
    label "pozostawia&#263;"
  ]
  node [
    id 43
    label "czyni&#263;"
  ]
  node [
    id 44
    label "wydawa&#263;"
  ]
  node [
    id 45
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 46
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 47
    label "zak&#322;ada&#263;_si&#281;"
  ]
  node [
    id 48
    label "przewidywa&#263;"
  ]
  node [
    id 49
    label "przyznawa&#263;"
  ]
  node [
    id 50
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 51
    label "go"
  ]
  node [
    id 52
    label "obstawia&#263;"
  ]
  node [
    id 53
    label "umieszcza&#263;"
  ]
  node [
    id 54
    label "ocenia&#263;"
  ]
  node [
    id 55
    label "zastawia&#263;"
  ]
  node [
    id 56
    label "stanowisko"
  ]
  node [
    id 57
    label "znak"
  ]
  node [
    id 58
    label "wskazywa&#263;"
  ]
  node [
    id 59
    label "uruchamia&#263;"
  ]
  node [
    id 60
    label "fundowa&#263;"
  ]
  node [
    id 61
    label "zmienia&#263;"
  ]
  node [
    id 62
    label "decydowa&#263;_si&#281;"
  ]
  node [
    id 63
    label "deliver"
  ]
  node [
    id 64
    label "powodowa&#263;"
  ]
  node [
    id 65
    label "wyznacza&#263;"
  ]
  node [
    id 66
    label "przedstawia&#263;"
  ]
  node [
    id 67
    label "wydobywa&#263;"
  ]
  node [
    id 68
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 69
    label "trim"
  ]
  node [
    id 70
    label "gryzipi&#243;rek"
  ]
  node [
    id 71
    label "cz&#322;owiek"
  ]
  node [
    id 72
    label "pisarz"
  ]
  node [
    id 73
    label "ekscerpcja"
  ]
  node [
    id 74
    label "j&#281;zykowo"
  ]
  node [
    id 75
    label "wypowied&#378;"
  ]
  node [
    id 76
    label "redakcja"
  ]
  node [
    id 77
    label "wytw&#243;r"
  ]
  node [
    id 78
    label "pomini&#281;cie"
  ]
  node [
    id 79
    label "dzie&#322;o"
  ]
  node [
    id 80
    label "preparacja"
  ]
  node [
    id 81
    label "odmianka"
  ]
  node [
    id 82
    label "opu&#347;ci&#263;"
  ]
  node [
    id 83
    label "koniektura"
  ]
  node [
    id 84
    label "obelga"
  ]
  node [
    id 85
    label "zesp&#243;&#322;"
  ]
  node [
    id 86
    label "t&#322;oczysko"
  ]
  node [
    id 87
    label "depesza"
  ]
  node [
    id 88
    label "maszyna"
  ]
  node [
    id 89
    label "media"
  ]
  node [
    id 90
    label "napisa&#263;"
  ]
  node [
    id 91
    label "czasopismo"
  ]
  node [
    id 92
    label "dziennikarz_prasowy"
  ]
  node [
    id 93
    label "kiosk"
  ]
  node [
    id 94
    label "maszyna_rolnicza"
  ]
  node [
    id 95
    label "gazeta"
  ]
  node [
    id 96
    label "trzonek"
  ]
  node [
    id 97
    label "reakcja"
  ]
  node [
    id 98
    label "narz&#281;dzie"
  ]
  node [
    id 99
    label "spos&#243;b"
  ]
  node [
    id 100
    label "zbi&#243;r"
  ]
  node [
    id 101
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 102
    label "zachowanie"
  ]
  node [
    id 103
    label "stylik"
  ]
  node [
    id 104
    label "dyscyplina_sportowa"
  ]
  node [
    id 105
    label "handle"
  ]
  node [
    id 106
    label "stroke"
  ]
  node [
    id 107
    label "line"
  ]
  node [
    id 108
    label "charakter"
  ]
  node [
    id 109
    label "natural_language"
  ]
  node [
    id 110
    label "kanon"
  ]
  node [
    id 111
    label "behawior"
  ]
  node [
    id 112
    label "dysleksja"
  ]
  node [
    id 113
    label "pisanie"
  ]
  node [
    id 114
    label "dysgraphia"
  ]
  node [
    id 115
    label "znaczek_pocztowy"
  ]
  node [
    id 116
    label "li&#347;&#263;"
  ]
  node [
    id 117
    label "epistolografia"
  ]
  node [
    id 118
    label "poczta"
  ]
  node [
    id 119
    label "poczta_elektroniczna"
  ]
  node [
    id 120
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 121
    label "przesy&#322;ka"
  ]
  node [
    id 122
    label "znoszenie"
  ]
  node [
    id 123
    label "nap&#322;ywanie"
  ]
  node [
    id 124
    label "communication"
  ]
  node [
    id 125
    label "signal"
  ]
  node [
    id 126
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 127
    label "znosi&#263;"
  ]
  node [
    id 128
    label "znie&#347;&#263;"
  ]
  node [
    id 129
    label "zniesienie"
  ]
  node [
    id 130
    label "zarys"
  ]
  node [
    id 131
    label "informacja"
  ]
  node [
    id 132
    label "komunikat"
  ]
  node [
    id 133
    label "depesza_emska"
  ]
  node [
    id 134
    label "dochodzenie"
  ]
  node [
    id 135
    label "przedmiot"
  ]
  node [
    id 136
    label "doj&#347;cie"
  ]
  node [
    id 137
    label "posy&#322;ka"
  ]
  node [
    id 138
    label "nadawca"
  ]
  node [
    id 139
    label "adres"
  ]
  node [
    id 140
    label "dochodzi&#263;"
  ]
  node [
    id 141
    label "doj&#347;&#263;"
  ]
  node [
    id 142
    label "zasada"
  ]
  node [
    id 143
    label "pi&#347;miennictwo"
  ]
  node [
    id 144
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 145
    label "skrytka_pocztowa"
  ]
  node [
    id 146
    label "miejscownik"
  ]
  node [
    id 147
    label "instytucja"
  ]
  node [
    id 148
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 149
    label "mail"
  ]
  node [
    id 150
    label "plac&#243;wka"
  ]
  node [
    id 151
    label "szybkow&#243;z"
  ]
  node [
    id 152
    label "okienko"
  ]
  node [
    id 153
    label "pi&#322;kowanie"
  ]
  node [
    id 154
    label "cz&#322;on_p&#281;dowy"
  ]
  node [
    id 155
    label "nasada"
  ]
  node [
    id 156
    label "nerwacja"
  ]
  node [
    id 157
    label "&#380;y&#322;kowanie"
  ]
  node [
    id 158
    label "ogonek"
  ]
  node [
    id 159
    label "organ_ro&#347;linny"
  ]
  node [
    id 160
    label "blaszka"
  ]
  node [
    id 161
    label "listowie"
  ]
  node [
    id 162
    label "foliofag"
  ]
  node [
    id 163
    label "ro&#347;lina"
  ]
  node [
    id 164
    label "egzemplarz"
  ]
  node [
    id 165
    label "impression"
  ]
  node [
    id 166
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 167
    label "odmiana"
  ]
  node [
    id 168
    label "cykl"
  ]
  node [
    id 169
    label "notification"
  ]
  node [
    id 170
    label "zmiana"
  ]
  node [
    id 171
    label "produkcja"
  ]
  node [
    id 172
    label "mutant"
  ]
  node [
    id 173
    label "rewizja"
  ]
  node [
    id 174
    label "gramatyka"
  ]
  node [
    id 175
    label "typ"
  ]
  node [
    id 176
    label "paradygmat"
  ]
  node [
    id 177
    label "jednostka_systematyczna"
  ]
  node [
    id 178
    label "change"
  ]
  node [
    id 179
    label "podgatunek"
  ]
  node [
    id 180
    label "posta&#263;"
  ]
  node [
    id 181
    label "ferment"
  ]
  node [
    id 182
    label "rasa"
  ]
  node [
    id 183
    label "zjawisko"
  ]
  node [
    id 184
    label "impreza"
  ]
  node [
    id 185
    label "realizacja"
  ]
  node [
    id 186
    label "tingel-tangel"
  ]
  node [
    id 187
    label "numer"
  ]
  node [
    id 188
    label "monta&#380;"
  ]
  node [
    id 189
    label "wyda&#263;"
  ]
  node [
    id 190
    label "postprodukcja"
  ]
  node [
    id 191
    label "performance"
  ]
  node [
    id 192
    label "fabrication"
  ]
  node [
    id 193
    label "product"
  ]
  node [
    id 194
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 195
    label "uzysk"
  ]
  node [
    id 196
    label "rozw&#243;j"
  ]
  node [
    id 197
    label "odtworzenie"
  ]
  node [
    id 198
    label "dorobek"
  ]
  node [
    id 199
    label "kreacja"
  ]
  node [
    id 200
    label "trema"
  ]
  node [
    id 201
    label "creation"
  ]
  node [
    id 202
    label "kooperowa&#263;"
  ]
  node [
    id 203
    label "czynnik_biotyczny"
  ]
  node [
    id 204
    label "wyewoluowanie"
  ]
  node [
    id 205
    label "individual"
  ]
  node [
    id 206
    label "przyswoi&#263;"
  ]
  node [
    id 207
    label "starzenie_si&#281;"
  ]
  node [
    id 208
    label "wyewoluowa&#263;"
  ]
  node [
    id 209
    label "okaz"
  ]
  node [
    id 210
    label "part"
  ]
  node [
    id 211
    label "ewoluowa&#263;"
  ]
  node [
    id 212
    label "przyswojenie"
  ]
  node [
    id 213
    label "ewoluowanie"
  ]
  node [
    id 214
    label "obiekt"
  ]
  node [
    id 215
    label "sztuka"
  ]
  node [
    id 216
    label "agent"
  ]
  node [
    id 217
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 218
    label "przyswaja&#263;"
  ]
  node [
    id 219
    label "nicpo&#324;"
  ]
  node [
    id 220
    label "przyswajanie"
  ]
  node [
    id 221
    label "passage"
  ]
  node [
    id 222
    label "oznaka"
  ]
  node [
    id 223
    label "komplet"
  ]
  node [
    id 224
    label "anatomopatolog"
  ]
  node [
    id 225
    label "zmianka"
  ]
  node [
    id 226
    label "czas"
  ]
  node [
    id 227
    label "amendment"
  ]
  node [
    id 228
    label "praca"
  ]
  node [
    id 229
    label "odmienianie"
  ]
  node [
    id 230
    label "tura"
  ]
  node [
    id 231
    label "set"
  ]
  node [
    id 232
    label "przebieg"
  ]
  node [
    id 233
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 234
    label "miesi&#261;czka"
  ]
  node [
    id 235
    label "okres"
  ]
  node [
    id 236
    label "owulacja"
  ]
  node [
    id 237
    label "sekwencja"
  ]
  node [
    id 238
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 239
    label "cycle"
  ]
  node [
    id 240
    label "zachowa&#263;"
  ]
  node [
    id 241
    label "mnemotechnika"
  ]
  node [
    id 242
    label "post&#261;pi&#263;"
  ]
  node [
    id 243
    label "tajemnica"
  ]
  node [
    id 244
    label "pami&#281;&#263;"
  ]
  node [
    id 245
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 246
    label "zdyscyplinowanie"
  ]
  node [
    id 247
    label "post"
  ]
  node [
    id 248
    label "zrobi&#263;"
  ]
  node [
    id 249
    label "przechowa&#263;"
  ]
  node [
    id 250
    label "preserve"
  ]
  node [
    id 251
    label "dieta"
  ]
  node [
    id 252
    label "bury"
  ]
  node [
    id 253
    label "podtrzyma&#263;"
  ]
  node [
    id 254
    label "technika"
  ]
  node [
    id 255
    label "gem"
  ]
  node [
    id 256
    label "kompozycja"
  ]
  node [
    id 257
    label "runda"
  ]
  node [
    id 258
    label "muzyka"
  ]
  node [
    id 259
    label "zestaw"
  ]
  node [
    id 260
    label "okre&#347;lony"
  ]
  node [
    id 261
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 262
    label "wiadomy"
  ]
  node [
    id 263
    label "inscription"
  ]
  node [
    id 264
    label "op&#322;ata"
  ]
  node [
    id 265
    label "akt"
  ]
  node [
    id 266
    label "czynno&#347;&#263;"
  ]
  node [
    id 267
    label "entrance"
  ]
  node [
    id 268
    label "kwota"
  ]
  node [
    id 269
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 270
    label "activity"
  ]
  node [
    id 271
    label "bezproblemowy"
  ]
  node [
    id 272
    label "wydarzenie"
  ]
  node [
    id 273
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 274
    label "podnieci&#263;"
  ]
  node [
    id 275
    label "scena"
  ]
  node [
    id 276
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 277
    label "po&#380;ycie"
  ]
  node [
    id 278
    label "poj&#281;cie"
  ]
  node [
    id 279
    label "podniecenie"
  ]
  node [
    id 280
    label "nago&#347;&#263;"
  ]
  node [
    id 281
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 282
    label "fascyku&#322;"
  ]
  node [
    id 283
    label "seks"
  ]
  node [
    id 284
    label "podniecanie"
  ]
  node [
    id 285
    label "imisja"
  ]
  node [
    id 286
    label "zwyczaj"
  ]
  node [
    id 287
    label "rozmna&#380;anie"
  ]
  node [
    id 288
    label "ruch_frykcyjny"
  ]
  node [
    id 289
    label "ontologia"
  ]
  node [
    id 290
    label "na_pieska"
  ]
  node [
    id 291
    label "pozycja_misjonarska"
  ]
  node [
    id 292
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 293
    label "fragment"
  ]
  node [
    id 294
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 295
    label "z&#322;&#261;czenie"
  ]
  node [
    id 296
    label "gra_wst&#281;pna"
  ]
  node [
    id 297
    label "erotyka"
  ]
  node [
    id 298
    label "urzeczywistnienie"
  ]
  node [
    id 299
    label "baraszki"
  ]
  node [
    id 300
    label "certificate"
  ]
  node [
    id 301
    label "po&#380;&#261;danie"
  ]
  node [
    id 302
    label "wzw&#243;d"
  ]
  node [
    id 303
    label "funkcja"
  ]
  node [
    id 304
    label "act"
  ]
  node [
    id 305
    label "dokument"
  ]
  node [
    id 306
    label "arystotelizm"
  ]
  node [
    id 307
    label "podnieca&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
]
