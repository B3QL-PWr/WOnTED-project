graph [
  node [
    id 0
    label "amerykanin"
    origin "text"
  ]
  node [
    id 1
    label "rosyjski"
    origin "text"
  ]
  node [
    id 2
    label "pochodzenie"
    origin "text"
  ]
  node [
    id 3
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 4
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 5
    label "kacapski"
  ]
  node [
    id 6
    label "po_rosyjsku"
  ]
  node [
    id 7
    label "wielkoruski"
  ]
  node [
    id 8
    label "Russian"
  ]
  node [
    id 9
    label "j&#281;zyk"
  ]
  node [
    id 10
    label "rusek"
  ]
  node [
    id 11
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 12
    label "artykulator"
  ]
  node [
    id 13
    label "kod"
  ]
  node [
    id 14
    label "kawa&#322;ek"
  ]
  node [
    id 15
    label "przedmiot"
  ]
  node [
    id 16
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 17
    label "gramatyka"
  ]
  node [
    id 18
    label "stylik"
  ]
  node [
    id 19
    label "przet&#322;umaczenie"
  ]
  node [
    id 20
    label "formalizowanie"
  ]
  node [
    id 21
    label "ssa&#263;"
  ]
  node [
    id 22
    label "ssanie"
  ]
  node [
    id 23
    label "language"
  ]
  node [
    id 24
    label "liza&#263;"
  ]
  node [
    id 25
    label "napisa&#263;"
  ]
  node [
    id 26
    label "konsonantyzm"
  ]
  node [
    id 27
    label "wokalizm"
  ]
  node [
    id 28
    label "pisa&#263;"
  ]
  node [
    id 29
    label "fonetyka"
  ]
  node [
    id 30
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 31
    label "jeniec"
  ]
  node [
    id 32
    label "but"
  ]
  node [
    id 33
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 34
    label "po_koroniarsku"
  ]
  node [
    id 35
    label "kultura_duchowa"
  ]
  node [
    id 36
    label "t&#322;umaczenie"
  ]
  node [
    id 37
    label "m&#243;wienie"
  ]
  node [
    id 38
    label "pype&#263;"
  ]
  node [
    id 39
    label "lizanie"
  ]
  node [
    id 40
    label "pismo"
  ]
  node [
    id 41
    label "formalizowa&#263;"
  ]
  node [
    id 42
    label "rozumie&#263;"
  ]
  node [
    id 43
    label "organ"
  ]
  node [
    id 44
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 45
    label "rozumienie"
  ]
  node [
    id 46
    label "spos&#243;b"
  ]
  node [
    id 47
    label "makroglosja"
  ]
  node [
    id 48
    label "m&#243;wi&#263;"
  ]
  node [
    id 49
    label "jama_ustna"
  ]
  node [
    id 50
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 51
    label "formacja_geologiczna"
  ]
  node [
    id 52
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 53
    label "natural_language"
  ]
  node [
    id 54
    label "s&#322;ownictwo"
  ]
  node [
    id 55
    label "urz&#261;dzenie"
  ]
  node [
    id 56
    label "wschodnioeuropejski"
  ]
  node [
    id 57
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 58
    label "poga&#324;ski"
  ]
  node [
    id 59
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 60
    label "topielec"
  ]
  node [
    id 61
    label "europejski"
  ]
  node [
    id 62
    label "j&#281;zyk_rosyjski"
  ]
  node [
    id 63
    label "po_kacapsku"
  ]
  node [
    id 64
    label "ruski"
  ]
  node [
    id 65
    label "imperialny"
  ]
  node [
    id 66
    label "po_wielkorusku"
  ]
  node [
    id 67
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 68
    label "zaczynanie_si&#281;"
  ]
  node [
    id 69
    label "str&#243;j"
  ]
  node [
    id 70
    label "wynikanie"
  ]
  node [
    id 71
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 72
    label "origin"
  ]
  node [
    id 73
    label "background"
  ]
  node [
    id 74
    label "czas"
  ]
  node [
    id 75
    label "geneza"
  ]
  node [
    id 76
    label "beginning"
  ]
  node [
    id 77
    label "okazywanie_si&#281;"
  ]
  node [
    id 78
    label "powstawanie"
  ]
  node [
    id 79
    label "implication"
  ]
  node [
    id 80
    label "flux"
  ]
  node [
    id 81
    label "czynnik"
  ]
  node [
    id 82
    label "proces"
  ]
  node [
    id 83
    label "zesp&#243;&#322;"
  ]
  node [
    id 84
    label "rodny"
  ]
  node [
    id 85
    label "powstanie"
  ]
  node [
    id 86
    label "monogeneza"
  ]
  node [
    id 87
    label "zaistnienie"
  ]
  node [
    id 88
    label "give"
  ]
  node [
    id 89
    label "pocz&#261;tek"
  ]
  node [
    id 90
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 91
    label "przyczyna"
  ]
  node [
    id 92
    label "poprzedzanie"
  ]
  node [
    id 93
    label "czasoprzestrze&#324;"
  ]
  node [
    id 94
    label "laba"
  ]
  node [
    id 95
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 96
    label "chronometria"
  ]
  node [
    id 97
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 98
    label "rachuba_czasu"
  ]
  node [
    id 99
    label "przep&#322;ywanie"
  ]
  node [
    id 100
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 101
    label "czasokres"
  ]
  node [
    id 102
    label "odczyt"
  ]
  node [
    id 103
    label "chwila"
  ]
  node [
    id 104
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 105
    label "dzieje"
  ]
  node [
    id 106
    label "kategoria_gramatyczna"
  ]
  node [
    id 107
    label "poprzedzenie"
  ]
  node [
    id 108
    label "trawienie"
  ]
  node [
    id 109
    label "pochodzi&#263;"
  ]
  node [
    id 110
    label "period"
  ]
  node [
    id 111
    label "okres_czasu"
  ]
  node [
    id 112
    label "poprzedza&#263;"
  ]
  node [
    id 113
    label "schy&#322;ek"
  ]
  node [
    id 114
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 115
    label "odwlekanie_si&#281;"
  ]
  node [
    id 116
    label "zegar"
  ]
  node [
    id 117
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 118
    label "czwarty_wymiar"
  ]
  node [
    id 119
    label "koniugacja"
  ]
  node [
    id 120
    label "Zeitgeist"
  ]
  node [
    id 121
    label "trawi&#263;"
  ]
  node [
    id 122
    label "pogoda"
  ]
  node [
    id 123
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 124
    label "poprzedzi&#263;"
  ]
  node [
    id 125
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 126
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 127
    label "time_period"
  ]
  node [
    id 128
    label "r&#243;&#380;norodno&#347;&#263;"
  ]
  node [
    id 129
    label "cecha"
  ]
  node [
    id 130
    label "gorset"
  ]
  node [
    id 131
    label "zrzucenie"
  ]
  node [
    id 132
    label "znoszenie"
  ]
  node [
    id 133
    label "kr&#243;j"
  ]
  node [
    id 134
    label "struktura"
  ]
  node [
    id 135
    label "ubranie_si&#281;"
  ]
  node [
    id 136
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 137
    label "znosi&#263;"
  ]
  node [
    id 138
    label "zrzuci&#263;"
  ]
  node [
    id 139
    label "pasmanteria"
  ]
  node [
    id 140
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 141
    label "odzie&#380;"
  ]
  node [
    id 142
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 143
    label "wyko&#324;czenie"
  ]
  node [
    id 144
    label "nosi&#263;"
  ]
  node [
    id 145
    label "zasada"
  ]
  node [
    id 146
    label "w&#322;o&#380;enie"
  ]
  node [
    id 147
    label "garderoba"
  ]
  node [
    id 148
    label "odziewek"
  ]
  node [
    id 149
    label "pocz&#261;tki"
  ]
  node [
    id 150
    label "kontekst"
  ]
  node [
    id 151
    label "stanowi&#263;"
  ]
  node [
    id 152
    label "zjednoczy&#263;"
  ]
  node [
    id 153
    label "zwi&#261;zek"
  ]
  node [
    id 154
    label "radziecki"
  ]
  node [
    id 155
    label "nowy"
  ]
  node [
    id 156
    label "Jork"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 155
    target 156
  ]
]
