graph [
  node [
    id 0
    label "by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "przez"
    origin "text"
  ]
  node [
    id 2
    label "lato"
    origin "text"
  ]
  node [
    id 3
    label "wolny"
    origin "text"
  ]
  node [
    id 4
    label "polska"
    origin "text"
  ]
  node [
    id 5
    label "taki"
    origin "text"
  ]
  node [
    id 6
    label "zbrodniarz"
    origin "text"
  ]
  node [
    id 7
    label "nie"
    origin "text"
  ]
  node [
    id 8
    label "powiedzie&#263;"
    origin "text"
  ]
  node [
    id 9
    label "jaros&#322;aw"
    origin "text"
  ]
  node [
    id 10
    label "szarek"
    origin "text"
  ]
  node [
    id 11
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 12
    label "mie&#263;_miejsce"
  ]
  node [
    id 13
    label "equal"
  ]
  node [
    id 14
    label "trwa&#263;"
  ]
  node [
    id 15
    label "chodzi&#263;"
  ]
  node [
    id 16
    label "si&#281;ga&#263;"
  ]
  node [
    id 17
    label "stan"
  ]
  node [
    id 18
    label "obecno&#347;&#263;"
  ]
  node [
    id 19
    label "stand"
  ]
  node [
    id 20
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 21
    label "uczestniczy&#263;"
  ]
  node [
    id 22
    label "participate"
  ]
  node [
    id 23
    label "robi&#263;"
  ]
  node [
    id 24
    label "istnie&#263;"
  ]
  node [
    id 25
    label "pozostawa&#263;"
  ]
  node [
    id 26
    label "zostawa&#263;"
  ]
  node [
    id 27
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 28
    label "adhere"
  ]
  node [
    id 29
    label "compass"
  ]
  node [
    id 30
    label "korzysta&#263;"
  ]
  node [
    id 31
    label "appreciation"
  ]
  node [
    id 32
    label "osi&#261;ga&#263;"
  ]
  node [
    id 33
    label "dociera&#263;"
  ]
  node [
    id 34
    label "get"
  ]
  node [
    id 35
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 36
    label "mierzy&#263;"
  ]
  node [
    id 37
    label "u&#380;ywa&#263;"
  ]
  node [
    id 38
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 39
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 40
    label "exsert"
  ]
  node [
    id 41
    label "being"
  ]
  node [
    id 42
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 43
    label "cecha"
  ]
  node [
    id 44
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 45
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 46
    label "p&#322;ywa&#263;"
  ]
  node [
    id 47
    label "run"
  ]
  node [
    id 48
    label "bangla&#263;"
  ]
  node [
    id 49
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 50
    label "przebiega&#263;"
  ]
  node [
    id 51
    label "wk&#322;ada&#263;"
  ]
  node [
    id 52
    label "proceed"
  ]
  node [
    id 53
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 54
    label "carry"
  ]
  node [
    id 55
    label "bywa&#263;"
  ]
  node [
    id 56
    label "dziama&#263;"
  ]
  node [
    id 57
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 58
    label "stara&#263;_si&#281;"
  ]
  node [
    id 59
    label "para"
  ]
  node [
    id 60
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 61
    label "str&#243;j"
  ]
  node [
    id 62
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 63
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 64
    label "krok"
  ]
  node [
    id 65
    label "tryb"
  ]
  node [
    id 66
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 67
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 68
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 69
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 70
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 71
    label "continue"
  ]
  node [
    id 72
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 73
    label "Ohio"
  ]
  node [
    id 74
    label "wci&#281;cie"
  ]
  node [
    id 75
    label "Nowy_York"
  ]
  node [
    id 76
    label "warstwa"
  ]
  node [
    id 77
    label "samopoczucie"
  ]
  node [
    id 78
    label "Illinois"
  ]
  node [
    id 79
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 80
    label "state"
  ]
  node [
    id 81
    label "Jukatan"
  ]
  node [
    id 82
    label "Kalifornia"
  ]
  node [
    id 83
    label "Wirginia"
  ]
  node [
    id 84
    label "wektor"
  ]
  node [
    id 85
    label "Teksas"
  ]
  node [
    id 86
    label "Goa"
  ]
  node [
    id 87
    label "Waszyngton"
  ]
  node [
    id 88
    label "miejsce"
  ]
  node [
    id 89
    label "Massachusetts"
  ]
  node [
    id 90
    label "Alaska"
  ]
  node [
    id 91
    label "Arakan"
  ]
  node [
    id 92
    label "Hawaje"
  ]
  node [
    id 93
    label "Maryland"
  ]
  node [
    id 94
    label "punkt"
  ]
  node [
    id 95
    label "Michigan"
  ]
  node [
    id 96
    label "Arizona"
  ]
  node [
    id 97
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 98
    label "Georgia"
  ]
  node [
    id 99
    label "poziom"
  ]
  node [
    id 100
    label "Pensylwania"
  ]
  node [
    id 101
    label "shape"
  ]
  node [
    id 102
    label "Luizjana"
  ]
  node [
    id 103
    label "Nowy_Meksyk"
  ]
  node [
    id 104
    label "Alabama"
  ]
  node [
    id 105
    label "ilo&#347;&#263;"
  ]
  node [
    id 106
    label "Kansas"
  ]
  node [
    id 107
    label "Oregon"
  ]
  node [
    id 108
    label "Floryda"
  ]
  node [
    id 109
    label "Oklahoma"
  ]
  node [
    id 110
    label "jednostka_administracyjna"
  ]
  node [
    id 111
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 112
    label "pora_roku"
  ]
  node [
    id 113
    label "rozrzedzenie"
  ]
  node [
    id 114
    label "rzedni&#281;cie"
  ]
  node [
    id 115
    label "niespieszny"
  ]
  node [
    id 116
    label "zwalnianie_si&#281;"
  ]
  node [
    id 117
    label "wakowa&#263;"
  ]
  node [
    id 118
    label "rozwadnianie"
  ]
  node [
    id 119
    label "niezale&#380;ny"
  ]
  node [
    id 120
    label "zrzedni&#281;cie"
  ]
  node [
    id 121
    label "swobodnie"
  ]
  node [
    id 122
    label "rozrzedzanie"
  ]
  node [
    id 123
    label "rozwodnienie"
  ]
  node [
    id 124
    label "strza&#322;"
  ]
  node [
    id 125
    label "wolnie"
  ]
  node [
    id 126
    label "zwolnienie_si&#281;"
  ]
  node [
    id 127
    label "wolno"
  ]
  node [
    id 128
    label "lu&#378;no"
  ]
  node [
    id 129
    label "niespiesznie"
  ]
  node [
    id 130
    label "spokojny"
  ]
  node [
    id 131
    label "trafny"
  ]
  node [
    id 132
    label "shot"
  ]
  node [
    id 133
    label "przykro&#347;&#263;"
  ]
  node [
    id 134
    label "huk"
  ]
  node [
    id 135
    label "bum-bum"
  ]
  node [
    id 136
    label "pi&#322;ka"
  ]
  node [
    id 137
    label "uderzenie"
  ]
  node [
    id 138
    label "eksplozja"
  ]
  node [
    id 139
    label "wyrzut"
  ]
  node [
    id 140
    label "usi&#322;owanie"
  ]
  node [
    id 141
    label "przypadek"
  ]
  node [
    id 142
    label "shooting"
  ]
  node [
    id 143
    label "odgadywanie"
  ]
  node [
    id 144
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 145
    label "usamodzielnienie"
  ]
  node [
    id 146
    label "usamodzielnianie"
  ]
  node [
    id 147
    label "niezale&#380;nie"
  ]
  node [
    id 148
    label "thinly"
  ]
  node [
    id 149
    label "wolniej"
  ]
  node [
    id 150
    label "swobodny"
  ]
  node [
    id 151
    label "free"
  ]
  node [
    id 152
    label "lu&#378;ny"
  ]
  node [
    id 153
    label "dowolnie"
  ]
  node [
    id 154
    label "naturalnie"
  ]
  node [
    id 155
    label "dilution"
  ]
  node [
    id 156
    label "powodowanie"
  ]
  node [
    id 157
    label "rozcie&#324;czanie"
  ]
  node [
    id 158
    label "chrzczenie"
  ]
  node [
    id 159
    label "rzadki"
  ]
  node [
    id 160
    label "stanie_si&#281;"
  ]
  node [
    id 161
    label "ochrzczenie"
  ]
  node [
    id 162
    label "rozcie&#324;czenie"
  ]
  node [
    id 163
    label "rarefaction"
  ]
  node [
    id 164
    label "czynno&#347;&#263;"
  ]
  node [
    id 165
    label "spowodowanie"
  ]
  node [
    id 166
    label "lekko"
  ]
  node [
    id 167
    label "&#322;atwo"
  ]
  node [
    id 168
    label "odlegle"
  ]
  node [
    id 169
    label "przyjemnie"
  ]
  node [
    id 170
    label "nieformalnie"
  ]
  node [
    id 171
    label "stawanie_si&#281;"
  ]
  node [
    id 172
    label "stanowisko"
  ]
  node [
    id 173
    label "okre&#347;lony"
  ]
  node [
    id 174
    label "jaki&#347;"
  ]
  node [
    id 175
    label "przyzwoity"
  ]
  node [
    id 176
    label "ciekawy"
  ]
  node [
    id 177
    label "jako&#347;"
  ]
  node [
    id 178
    label "jako_tako"
  ]
  node [
    id 179
    label "niez&#322;y"
  ]
  node [
    id 180
    label "dziwny"
  ]
  node [
    id 181
    label "charakterystyczny"
  ]
  node [
    id 182
    label "wiadomy"
  ]
  node [
    id 183
    label "przest&#281;pca"
  ]
  node [
    id 184
    label "pogwa&#322;ciciel"
  ]
  node [
    id 185
    label "z&#322;oczy&#324;ca"
  ]
  node [
    id 186
    label "sprzeciw"
  ]
  node [
    id 187
    label "czerwona_kartka"
  ]
  node [
    id 188
    label "protestacja"
  ]
  node [
    id 189
    label "reakcja"
  ]
  node [
    id 190
    label "discover"
  ]
  node [
    id 191
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 192
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 193
    label "wydoby&#263;"
  ]
  node [
    id 194
    label "poda&#263;"
  ]
  node [
    id 195
    label "okre&#347;li&#263;"
  ]
  node [
    id 196
    label "express"
  ]
  node [
    id 197
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 198
    label "wyrazi&#263;"
  ]
  node [
    id 199
    label "rzekn&#261;&#263;"
  ]
  node [
    id 200
    label "unwrap"
  ]
  node [
    id 201
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 202
    label "convey"
  ]
  node [
    id 203
    label "tenis"
  ]
  node [
    id 204
    label "supply"
  ]
  node [
    id 205
    label "da&#263;"
  ]
  node [
    id 206
    label "ustawi&#263;"
  ]
  node [
    id 207
    label "siatk&#243;wka"
  ]
  node [
    id 208
    label "give"
  ]
  node [
    id 209
    label "zagra&#263;"
  ]
  node [
    id 210
    label "jedzenie"
  ]
  node [
    id 211
    label "poinformowa&#263;"
  ]
  node [
    id 212
    label "introduce"
  ]
  node [
    id 213
    label "nafaszerowa&#263;"
  ]
  node [
    id 214
    label "zaserwowa&#263;"
  ]
  node [
    id 215
    label "draw"
  ]
  node [
    id 216
    label "doby&#263;"
  ]
  node [
    id 217
    label "g&#243;rnictwo"
  ]
  node [
    id 218
    label "wyeksploatowa&#263;"
  ]
  node [
    id 219
    label "extract"
  ]
  node [
    id 220
    label "obtain"
  ]
  node [
    id 221
    label "wyj&#261;&#263;"
  ]
  node [
    id 222
    label "ocali&#263;"
  ]
  node [
    id 223
    label "uzyska&#263;"
  ]
  node [
    id 224
    label "wyda&#263;"
  ]
  node [
    id 225
    label "wydosta&#263;"
  ]
  node [
    id 226
    label "uwydatni&#263;"
  ]
  node [
    id 227
    label "distill"
  ]
  node [
    id 228
    label "raise"
  ]
  node [
    id 229
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 230
    label "testify"
  ]
  node [
    id 231
    label "zakomunikowa&#263;"
  ]
  node [
    id 232
    label "oznaczy&#263;"
  ]
  node [
    id 233
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 234
    label "vent"
  ]
  node [
    id 235
    label "zdecydowa&#263;"
  ]
  node [
    id 236
    label "zrobi&#263;"
  ]
  node [
    id 237
    label "spowodowa&#263;"
  ]
  node [
    id 238
    label "situate"
  ]
  node [
    id 239
    label "nominate"
  ]
  node [
    id 240
    label "Jaros&#322;aw"
  ]
  node [
    id 241
    label "Szarek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 240
    target 241
  ]
]
