graph [
  node [
    id 0
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wpad&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 4
    label "mama"
    origin "text"
  ]
  node [
    id 5
    label "nadzieja"
    origin "text"
  ]
  node [
    id 6
    label "koniec"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "udo"
    origin "text"
  ]
  node [
    id 9
    label "nape&#322;ni&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wywrotka"
    origin "text"
  ]
  node [
    id 11
    label "cognizance"
  ]
  node [
    id 12
    label "okre&#347;lony"
  ]
  node [
    id 13
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 14
    label "wiadomy"
  ]
  node [
    id 15
    label "wytw&#243;r"
  ]
  node [
    id 16
    label "pocz&#261;tki"
  ]
  node [
    id 17
    label "ukra&#347;&#263;"
  ]
  node [
    id 18
    label "ukradzenie"
  ]
  node [
    id 19
    label "idea"
  ]
  node [
    id 20
    label "system"
  ]
  node [
    id 21
    label "przedmiot"
  ]
  node [
    id 22
    label "p&#322;&#243;d"
  ]
  node [
    id 23
    label "work"
  ]
  node [
    id 24
    label "rezultat"
  ]
  node [
    id 25
    label "strategia"
  ]
  node [
    id 26
    label "background"
  ]
  node [
    id 27
    label "dzieci&#281;ctwo"
  ]
  node [
    id 28
    label "podpierdoli&#263;"
  ]
  node [
    id 29
    label "dash_off"
  ]
  node [
    id 30
    label "zw&#281;dzi&#263;"
  ]
  node [
    id 31
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 32
    label "zabra&#263;"
  ]
  node [
    id 33
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 34
    label "overcharge"
  ]
  node [
    id 35
    label "podpierdolenie"
  ]
  node [
    id 36
    label "zgini&#281;cie"
  ]
  node [
    id 37
    label "przyw&#322;aszczenie"
  ]
  node [
    id 38
    label "larceny"
  ]
  node [
    id 39
    label "zaczerpni&#281;cie"
  ]
  node [
    id 40
    label "zw&#281;dzenie"
  ]
  node [
    id 41
    label "okradzenie"
  ]
  node [
    id 42
    label "nakradzenie"
  ]
  node [
    id 43
    label "ideologia"
  ]
  node [
    id 44
    label "byt"
  ]
  node [
    id 45
    label "intelekt"
  ]
  node [
    id 46
    label "Kant"
  ]
  node [
    id 47
    label "cel"
  ]
  node [
    id 48
    label "poj&#281;cie"
  ]
  node [
    id 49
    label "istota"
  ]
  node [
    id 50
    label "ideacja"
  ]
  node [
    id 51
    label "j&#261;dro"
  ]
  node [
    id 52
    label "systemik"
  ]
  node [
    id 53
    label "rozprz&#261;c"
  ]
  node [
    id 54
    label "oprogramowanie"
  ]
  node [
    id 55
    label "systemat"
  ]
  node [
    id 56
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 57
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 58
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 59
    label "model"
  ]
  node [
    id 60
    label "struktura"
  ]
  node [
    id 61
    label "usenet"
  ]
  node [
    id 62
    label "s&#261;d"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "porz&#261;dek"
  ]
  node [
    id 65
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 66
    label "przyn&#281;ta"
  ]
  node [
    id 67
    label "net"
  ]
  node [
    id 68
    label "w&#281;dkarstwo"
  ]
  node [
    id 69
    label "eratem"
  ]
  node [
    id 70
    label "oddzia&#322;"
  ]
  node [
    id 71
    label "doktryna"
  ]
  node [
    id 72
    label "pulpit"
  ]
  node [
    id 73
    label "konstelacja"
  ]
  node [
    id 74
    label "jednostka_geologiczna"
  ]
  node [
    id 75
    label "o&#347;"
  ]
  node [
    id 76
    label "podsystem"
  ]
  node [
    id 77
    label "metoda"
  ]
  node [
    id 78
    label "ryba"
  ]
  node [
    id 79
    label "Leopard"
  ]
  node [
    id 80
    label "spos&#243;b"
  ]
  node [
    id 81
    label "Android"
  ]
  node [
    id 82
    label "zachowanie"
  ]
  node [
    id 83
    label "cybernetyk"
  ]
  node [
    id 84
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 85
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 86
    label "method"
  ]
  node [
    id 87
    label "sk&#322;ad"
  ]
  node [
    id 88
    label "podstawa"
  ]
  node [
    id 89
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 90
    label "przodkini"
  ]
  node [
    id 91
    label "matka_zast&#281;pcza"
  ]
  node [
    id 92
    label "matczysko"
  ]
  node [
    id 93
    label "rodzice"
  ]
  node [
    id 94
    label "stara"
  ]
  node [
    id 95
    label "macierz"
  ]
  node [
    id 96
    label "rodzic"
  ]
  node [
    id 97
    label "Matka_Boska"
  ]
  node [
    id 98
    label "macocha"
  ]
  node [
    id 99
    label "starzy"
  ]
  node [
    id 100
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 101
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 102
    label "pokolenie"
  ]
  node [
    id 103
    label "wapniaki"
  ]
  node [
    id 104
    label "krewna"
  ]
  node [
    id 105
    label "opiekun"
  ]
  node [
    id 106
    label "wapniak"
  ]
  node [
    id 107
    label "rodzic_chrzestny"
  ]
  node [
    id 108
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 109
    label "matka"
  ]
  node [
    id 110
    label "&#380;ona"
  ]
  node [
    id 111
    label "kobieta"
  ]
  node [
    id 112
    label "partnerka"
  ]
  node [
    id 113
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 114
    label "matuszka"
  ]
  node [
    id 115
    label "parametryzacja"
  ]
  node [
    id 116
    label "pa&#324;stwo"
  ]
  node [
    id 117
    label "mod"
  ]
  node [
    id 118
    label "patriota"
  ]
  node [
    id 119
    label "m&#281;&#380;atka"
  ]
  node [
    id 120
    label "szansa"
  ]
  node [
    id 121
    label "spoczywa&#263;"
  ]
  node [
    id 122
    label "&#347;wiate&#322;ko_w_tunelu"
  ]
  node [
    id 123
    label "oczekiwanie"
  ]
  node [
    id 124
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 125
    label "wierzy&#263;"
  ]
  node [
    id 126
    label "posiada&#263;"
  ]
  node [
    id 127
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 128
    label "wydarzenie"
  ]
  node [
    id 129
    label "egzekutywa"
  ]
  node [
    id 130
    label "potencja&#322;"
  ]
  node [
    id 131
    label "wyb&#243;r"
  ]
  node [
    id 132
    label "prospect"
  ]
  node [
    id 133
    label "ability"
  ]
  node [
    id 134
    label "obliczeniowo"
  ]
  node [
    id 135
    label "alternatywa"
  ]
  node [
    id 136
    label "cecha"
  ]
  node [
    id 137
    label "operator_modalny"
  ]
  node [
    id 138
    label "wytrzymanie"
  ]
  node [
    id 139
    label "czekanie"
  ]
  node [
    id 140
    label "spodziewanie_si&#281;"
  ]
  node [
    id 141
    label "anticipation"
  ]
  node [
    id 142
    label "przewidywanie"
  ]
  node [
    id 143
    label "wytrzymywanie"
  ]
  node [
    id 144
    label "spotykanie"
  ]
  node [
    id 145
    label "wait"
  ]
  node [
    id 146
    label "wierza&#263;"
  ]
  node [
    id 147
    label "trust"
  ]
  node [
    id 148
    label "powierzy&#263;"
  ]
  node [
    id 149
    label "wyznawa&#263;"
  ]
  node [
    id 150
    label "czu&#263;"
  ]
  node [
    id 151
    label "faith"
  ]
  node [
    id 152
    label "chowa&#263;"
  ]
  node [
    id 153
    label "powierza&#263;"
  ]
  node [
    id 154
    label "uznawa&#263;"
  ]
  node [
    id 155
    label "by&#263;"
  ]
  node [
    id 156
    label "lie"
  ]
  node [
    id 157
    label "odpoczywa&#263;"
  ]
  node [
    id 158
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 159
    label "gr&#243;b"
  ]
  node [
    id 160
    label "ostatnie_podrygi"
  ]
  node [
    id 161
    label "visitation"
  ]
  node [
    id 162
    label "agonia"
  ]
  node [
    id 163
    label "defenestracja"
  ]
  node [
    id 164
    label "punkt"
  ]
  node [
    id 165
    label "dzia&#322;anie"
  ]
  node [
    id 166
    label "kres"
  ]
  node [
    id 167
    label "mogi&#322;a"
  ]
  node [
    id 168
    label "kres_&#380;ycia"
  ]
  node [
    id 169
    label "szereg"
  ]
  node [
    id 170
    label "szeol"
  ]
  node [
    id 171
    label "pogrzebanie"
  ]
  node [
    id 172
    label "miejsce"
  ]
  node [
    id 173
    label "chwila"
  ]
  node [
    id 174
    label "&#380;a&#322;oba"
  ]
  node [
    id 175
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 176
    label "zabicie"
  ]
  node [
    id 177
    label "przebiec"
  ]
  node [
    id 178
    label "charakter"
  ]
  node [
    id 179
    label "czynno&#347;&#263;"
  ]
  node [
    id 180
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 181
    label "motyw"
  ]
  node [
    id 182
    label "przebiegni&#281;cie"
  ]
  node [
    id 183
    label "fabu&#322;a"
  ]
  node [
    id 184
    label "Rzym_Zachodni"
  ]
  node [
    id 185
    label "whole"
  ]
  node [
    id 186
    label "ilo&#347;&#263;"
  ]
  node [
    id 187
    label "element"
  ]
  node [
    id 188
    label "Rzym_Wschodni"
  ]
  node [
    id 189
    label "urz&#261;dzenie"
  ]
  node [
    id 190
    label "warunek_lokalowy"
  ]
  node [
    id 191
    label "plac"
  ]
  node [
    id 192
    label "location"
  ]
  node [
    id 193
    label "uwaga"
  ]
  node [
    id 194
    label "przestrze&#324;"
  ]
  node [
    id 195
    label "status"
  ]
  node [
    id 196
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 197
    label "cia&#322;o"
  ]
  node [
    id 198
    label "praca"
  ]
  node [
    id 199
    label "rz&#261;d"
  ]
  node [
    id 200
    label "time"
  ]
  node [
    id 201
    label "czas"
  ]
  node [
    id 202
    label "&#347;mier&#263;"
  ]
  node [
    id 203
    label "death"
  ]
  node [
    id 204
    label "upadek"
  ]
  node [
    id 205
    label "zmierzch"
  ]
  node [
    id 206
    label "stan"
  ]
  node [
    id 207
    label "nieuleczalnie_chory"
  ]
  node [
    id 208
    label "spocz&#261;&#263;"
  ]
  node [
    id 209
    label "spocz&#281;cie"
  ]
  node [
    id 210
    label "pochowanie"
  ]
  node [
    id 211
    label "chowanie"
  ]
  node [
    id 212
    label "park_sztywnych"
  ]
  node [
    id 213
    label "pomnik"
  ]
  node [
    id 214
    label "nagrobek"
  ]
  node [
    id 215
    label "prochowisko"
  ]
  node [
    id 216
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 217
    label "spoczywanie"
  ]
  node [
    id 218
    label "za&#347;wiaty"
  ]
  node [
    id 219
    label "piek&#322;o"
  ]
  node [
    id 220
    label "judaizm"
  ]
  node [
    id 221
    label "destruction"
  ]
  node [
    id 222
    label "zabrzmienie"
  ]
  node [
    id 223
    label "skrzywdzenie"
  ]
  node [
    id 224
    label "pozabijanie"
  ]
  node [
    id 225
    label "zniszczenie"
  ]
  node [
    id 226
    label "zaszkodzenie"
  ]
  node [
    id 227
    label "usuni&#281;cie"
  ]
  node [
    id 228
    label "spowodowanie"
  ]
  node [
    id 229
    label "killing"
  ]
  node [
    id 230
    label "zdarzenie_si&#281;"
  ]
  node [
    id 231
    label "czyn"
  ]
  node [
    id 232
    label "umarcie"
  ]
  node [
    id 233
    label "granie"
  ]
  node [
    id 234
    label "zamkni&#281;cie"
  ]
  node [
    id 235
    label "compaction"
  ]
  node [
    id 236
    label "&#380;al"
  ]
  node [
    id 237
    label "paznokie&#263;"
  ]
  node [
    id 238
    label "symbol"
  ]
  node [
    id 239
    label "kir"
  ]
  node [
    id 240
    label "brud"
  ]
  node [
    id 241
    label "wyrzucenie"
  ]
  node [
    id 242
    label "defenestration"
  ]
  node [
    id 243
    label "zaj&#347;cie"
  ]
  node [
    id 244
    label "burying"
  ]
  node [
    id 245
    label "zasypanie"
  ]
  node [
    id 246
    label "zw&#322;oki"
  ]
  node [
    id 247
    label "burial"
  ]
  node [
    id 248
    label "w&#322;o&#380;enie"
  ]
  node [
    id 249
    label "porobienie"
  ]
  node [
    id 250
    label "uniemo&#380;liwienie"
  ]
  node [
    id 251
    label "po&#322;o&#380;enie"
  ]
  node [
    id 252
    label "sprawa"
  ]
  node [
    id 253
    label "ust&#281;p"
  ]
  node [
    id 254
    label "plan"
  ]
  node [
    id 255
    label "obiekt_matematyczny"
  ]
  node [
    id 256
    label "problemat"
  ]
  node [
    id 257
    label "plamka"
  ]
  node [
    id 258
    label "stopie&#324;_pisma"
  ]
  node [
    id 259
    label "jednostka"
  ]
  node [
    id 260
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 261
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 262
    label "mark"
  ]
  node [
    id 263
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 264
    label "prosta"
  ]
  node [
    id 265
    label "problematyka"
  ]
  node [
    id 266
    label "obiekt"
  ]
  node [
    id 267
    label "zapunktowa&#263;"
  ]
  node [
    id 268
    label "podpunkt"
  ]
  node [
    id 269
    label "wojsko"
  ]
  node [
    id 270
    label "point"
  ]
  node [
    id 271
    label "pozycja"
  ]
  node [
    id 272
    label "szpaler"
  ]
  node [
    id 273
    label "column"
  ]
  node [
    id 274
    label "uporz&#261;dkowanie"
  ]
  node [
    id 275
    label "mn&#243;stwo"
  ]
  node [
    id 276
    label "unit"
  ]
  node [
    id 277
    label "rozmieszczenie"
  ]
  node [
    id 278
    label "tract"
  ]
  node [
    id 279
    label "wyra&#380;enie"
  ]
  node [
    id 280
    label "infimum"
  ]
  node [
    id 281
    label "powodowanie"
  ]
  node [
    id 282
    label "liczenie"
  ]
  node [
    id 283
    label "cz&#322;owiek"
  ]
  node [
    id 284
    label "skutek"
  ]
  node [
    id 285
    label "podzia&#322;anie"
  ]
  node [
    id 286
    label "supremum"
  ]
  node [
    id 287
    label "kampania"
  ]
  node [
    id 288
    label "uruchamianie"
  ]
  node [
    id 289
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 290
    label "operacja"
  ]
  node [
    id 291
    label "hipnotyzowanie"
  ]
  node [
    id 292
    label "robienie"
  ]
  node [
    id 293
    label "uruchomienie"
  ]
  node [
    id 294
    label "nakr&#281;canie"
  ]
  node [
    id 295
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 296
    label "matematyka"
  ]
  node [
    id 297
    label "reakcja_chemiczna"
  ]
  node [
    id 298
    label "tr&#243;jstronny"
  ]
  node [
    id 299
    label "natural_process"
  ]
  node [
    id 300
    label "nakr&#281;cenie"
  ]
  node [
    id 301
    label "zatrzymanie"
  ]
  node [
    id 302
    label "wp&#322;yw"
  ]
  node [
    id 303
    label "rzut"
  ]
  node [
    id 304
    label "podtrzymywanie"
  ]
  node [
    id 305
    label "w&#322;&#261;czanie"
  ]
  node [
    id 306
    label "liczy&#263;"
  ]
  node [
    id 307
    label "operation"
  ]
  node [
    id 308
    label "dzianie_si&#281;"
  ]
  node [
    id 309
    label "zadzia&#322;anie"
  ]
  node [
    id 310
    label "priorytet"
  ]
  node [
    id 311
    label "bycie"
  ]
  node [
    id 312
    label "rozpocz&#281;cie"
  ]
  node [
    id 313
    label "docieranie"
  ]
  node [
    id 314
    label "funkcja"
  ]
  node [
    id 315
    label "czynny"
  ]
  node [
    id 316
    label "impact"
  ]
  node [
    id 317
    label "oferta"
  ]
  node [
    id 318
    label "zako&#324;czenie"
  ]
  node [
    id 319
    label "act"
  ]
  node [
    id 320
    label "wdzieranie_si&#281;"
  ]
  node [
    id 321
    label "w&#322;&#261;czenie"
  ]
  node [
    id 322
    label "mi&#281;sie&#324;_dwug&#322;owy_uda"
  ]
  node [
    id 323
    label "mi&#281;sie&#324;_czworog&#322;owy"
  ]
  node [
    id 324
    label "noga"
  ]
  node [
    id 325
    label "t&#281;tnica_udowa"
  ]
  node [
    id 326
    label "struktura_anatomiczna"
  ]
  node [
    id 327
    label "kr&#281;tarz"
  ]
  node [
    id 328
    label "odn&#243;&#380;e"
  ]
  node [
    id 329
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 330
    label "ko&#347;&#263;"
  ]
  node [
    id 331
    label "dogrywa&#263;"
  ]
  node [
    id 332
    label "s&#322;abeusz"
  ]
  node [
    id 333
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 334
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 335
    label "czpas"
  ]
  node [
    id 336
    label "nerw_udowy"
  ]
  node [
    id 337
    label "bezbramkowy"
  ]
  node [
    id 338
    label "podpora"
  ]
  node [
    id 339
    label "faulowa&#263;"
  ]
  node [
    id 340
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 341
    label "zamurowanie"
  ]
  node [
    id 342
    label "depta&#263;"
  ]
  node [
    id 343
    label "mi&#281;czak"
  ]
  node [
    id 344
    label "stopa"
  ]
  node [
    id 345
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 346
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 347
    label "mato&#322;"
  ]
  node [
    id 348
    label "ekstraklasa"
  ]
  node [
    id 349
    label "sfaulowa&#263;"
  ]
  node [
    id 350
    label "&#322;&#261;czyna"
  ]
  node [
    id 351
    label "lobowanie"
  ]
  node [
    id 352
    label "dogrywanie"
  ]
  node [
    id 353
    label "napinacz"
  ]
  node [
    id 354
    label "dublet"
  ]
  node [
    id 355
    label "sfaulowanie"
  ]
  node [
    id 356
    label "lobowa&#263;"
  ]
  node [
    id 357
    label "gira"
  ]
  node [
    id 358
    label "bramkarz"
  ]
  node [
    id 359
    label "zamurowywanie"
  ]
  node [
    id 360
    label "kopni&#281;cie"
  ]
  node [
    id 361
    label "faulowanie"
  ]
  node [
    id 362
    label "&#322;amaga"
  ]
  node [
    id 363
    label "kopn&#261;&#263;"
  ]
  node [
    id 364
    label "kopanie"
  ]
  node [
    id 365
    label "dogranie"
  ]
  node [
    id 366
    label "pi&#322;ka"
  ]
  node [
    id 367
    label "przelobowa&#263;"
  ]
  node [
    id 368
    label "mundial"
  ]
  node [
    id 369
    label "catenaccio"
  ]
  node [
    id 370
    label "r&#281;ka"
  ]
  node [
    id 371
    label "kopa&#263;"
  ]
  node [
    id 372
    label "dogra&#263;"
  ]
  node [
    id 373
    label "ko&#324;czyna"
  ]
  node [
    id 374
    label "tackle"
  ]
  node [
    id 375
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 376
    label "narz&#261;d_ruchu"
  ]
  node [
    id 377
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 378
    label "interliga"
  ]
  node [
    id 379
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 380
    label "zamurowywa&#263;"
  ]
  node [
    id 381
    label "przelobowanie"
  ]
  node [
    id 382
    label "czerwona_kartka"
  ]
  node [
    id 383
    label "Wis&#322;a"
  ]
  node [
    id 384
    label "zamurowa&#263;"
  ]
  node [
    id 385
    label "jedenastka"
  ]
  node [
    id 386
    label "sprawi&#263;"
  ]
  node [
    id 387
    label "perform"
  ]
  node [
    id 388
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 389
    label "wywo&#322;a&#263;"
  ]
  node [
    id 390
    label "do"
  ]
  node [
    id 391
    label "umie&#347;ci&#263;"
  ]
  node [
    id 392
    label "wzbudzi&#263;"
  ]
  node [
    id 393
    label "set"
  ]
  node [
    id 394
    label "put"
  ]
  node [
    id 395
    label "uplasowa&#263;"
  ]
  node [
    id 396
    label "wpierniczy&#263;"
  ]
  node [
    id 397
    label "okre&#347;li&#263;"
  ]
  node [
    id 398
    label "zrobi&#263;"
  ]
  node [
    id 399
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 400
    label "zmieni&#263;"
  ]
  node [
    id 401
    label "umieszcza&#263;"
  ]
  node [
    id 402
    label "arouse"
  ]
  node [
    id 403
    label "wyrobi&#263;"
  ]
  node [
    id 404
    label "wzi&#261;&#263;"
  ]
  node [
    id 405
    label "catch"
  ]
  node [
    id 406
    label "spowodowa&#263;"
  ]
  node [
    id 407
    label "frame"
  ]
  node [
    id 408
    label "przygotowa&#263;"
  ]
  node [
    id 409
    label "poleci&#263;"
  ]
  node [
    id 410
    label "train"
  ]
  node [
    id 411
    label "wezwa&#263;"
  ]
  node [
    id 412
    label "trip"
  ]
  node [
    id 413
    label "oznajmi&#263;"
  ]
  node [
    id 414
    label "revolutionize"
  ]
  node [
    id 415
    label "przetworzy&#263;"
  ]
  node [
    id 416
    label "wydali&#263;"
  ]
  node [
    id 417
    label "ut"
  ]
  node [
    id 418
    label "d&#378;wi&#281;k"
  ]
  node [
    id 419
    label "C"
  ]
  node [
    id 420
    label "his"
  ]
  node [
    id 421
    label "wagon"
  ]
  node [
    id 422
    label "ruch"
  ]
  node [
    id 423
    label "samoch&#243;d_ci&#281;&#380;arowy"
  ]
  node [
    id 424
    label "ci&#281;&#380;ki_sprz&#281;t"
  ]
  node [
    id 425
    label "przyczepa"
  ]
  node [
    id 426
    label "trailer"
  ]
  node [
    id 427
    label "pojazd_niemechaniczny"
  ]
  node [
    id 428
    label "poci&#261;g"
  ]
  node [
    id 429
    label "karton"
  ]
  node [
    id 430
    label "czo&#322;ownica"
  ]
  node [
    id 431
    label "harmonijka"
  ]
  node [
    id 432
    label "tramwaj"
  ]
  node [
    id 433
    label "klasa"
  ]
  node [
    id 434
    label "mechanika"
  ]
  node [
    id 435
    label "utrzymywanie"
  ]
  node [
    id 436
    label "move"
  ]
  node [
    id 437
    label "poruszenie"
  ]
  node [
    id 438
    label "movement"
  ]
  node [
    id 439
    label "myk"
  ]
  node [
    id 440
    label "utrzyma&#263;"
  ]
  node [
    id 441
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 442
    label "zjawisko"
  ]
  node [
    id 443
    label "utrzymanie"
  ]
  node [
    id 444
    label "travel"
  ]
  node [
    id 445
    label "kanciasty"
  ]
  node [
    id 446
    label "commercial_enterprise"
  ]
  node [
    id 447
    label "strumie&#324;"
  ]
  node [
    id 448
    label "proces"
  ]
  node [
    id 449
    label "aktywno&#347;&#263;"
  ]
  node [
    id 450
    label "kr&#243;tki"
  ]
  node [
    id 451
    label "taktyka"
  ]
  node [
    id 452
    label "apraksja"
  ]
  node [
    id 453
    label "utrzymywa&#263;"
  ]
  node [
    id 454
    label "d&#322;ugi"
  ]
  node [
    id 455
    label "dyssypacja_energii"
  ]
  node [
    id 456
    label "tumult"
  ]
  node [
    id 457
    label "stopek"
  ]
  node [
    id 458
    label "zmiana"
  ]
  node [
    id 459
    label "manewr"
  ]
  node [
    id 460
    label "lokomocja"
  ]
  node [
    id 461
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 462
    label "komunikacja"
  ]
  node [
    id 463
    label "drift"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 184
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 187
  ]
  edge [
    source 8
    target 188
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 386
  ]
  edge [
    source 9
    target 387
  ]
  edge [
    source 9
    target 388
  ]
  edge [
    source 9
    target 389
  ]
  edge [
    source 9
    target 390
  ]
  edge [
    source 9
    target 391
  ]
  edge [
    source 9
    target 392
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
]
