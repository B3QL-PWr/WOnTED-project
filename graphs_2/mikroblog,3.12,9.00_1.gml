graph [
  node [
    id 0
    label "do&#380;y&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "czas"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "empatia"
    origin "text"
  ]
  node [
    id 4
    label "nieznacz&#261;cy"
    origin "text"
  ]
  node [
    id 5
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 6
    label "przesz&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "chronometria"
  ]
  node [
    id 8
    label "odczyt"
  ]
  node [
    id 9
    label "laba"
  ]
  node [
    id 10
    label "czasoprzestrze&#324;"
  ]
  node [
    id 11
    label "time_period"
  ]
  node [
    id 12
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 13
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 14
    label "Zeitgeist"
  ]
  node [
    id 15
    label "pochodzenie"
  ]
  node [
    id 16
    label "przep&#322;ywanie"
  ]
  node [
    id 17
    label "schy&#322;ek"
  ]
  node [
    id 18
    label "czwarty_wymiar"
  ]
  node [
    id 19
    label "kategoria_gramatyczna"
  ]
  node [
    id 20
    label "poprzedzi&#263;"
  ]
  node [
    id 21
    label "pogoda"
  ]
  node [
    id 22
    label "czasokres"
  ]
  node [
    id 23
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 24
    label "poprzedzenie"
  ]
  node [
    id 25
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 26
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 27
    label "dzieje"
  ]
  node [
    id 28
    label "zegar"
  ]
  node [
    id 29
    label "koniugacja"
  ]
  node [
    id 30
    label "trawi&#263;"
  ]
  node [
    id 31
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 32
    label "poprzedza&#263;"
  ]
  node [
    id 33
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 34
    label "trawienie"
  ]
  node [
    id 35
    label "chwila"
  ]
  node [
    id 36
    label "rachuba_czasu"
  ]
  node [
    id 37
    label "poprzedzanie"
  ]
  node [
    id 38
    label "okres_czasu"
  ]
  node [
    id 39
    label "period"
  ]
  node [
    id 40
    label "odwlekanie_si&#281;"
  ]
  node [
    id 41
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 42
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 43
    label "pochodzi&#263;"
  ]
  node [
    id 44
    label "time"
  ]
  node [
    id 45
    label "blok"
  ]
  node [
    id 46
    label "reading"
  ]
  node [
    id 47
    label "handout"
  ]
  node [
    id 48
    label "podawanie"
  ]
  node [
    id 49
    label "wyk&#322;ad"
  ]
  node [
    id 50
    label "lecture"
  ]
  node [
    id 51
    label "pomiar"
  ]
  node [
    id 52
    label "meteorology"
  ]
  node [
    id 53
    label "warunki"
  ]
  node [
    id 54
    label "weather"
  ]
  node [
    id 55
    label "zjawisko"
  ]
  node [
    id 56
    label "pok&#243;j"
  ]
  node [
    id 57
    label "atak"
  ]
  node [
    id 58
    label "prognoza_meteorologiczna"
  ]
  node [
    id 59
    label "potrzyma&#263;"
  ]
  node [
    id 60
    label "program"
  ]
  node [
    id 61
    label "czas_wolny"
  ]
  node [
    id 62
    label "metrologia"
  ]
  node [
    id 63
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 64
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 65
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 66
    label "czasomierz"
  ]
  node [
    id 67
    label "tyka&#263;"
  ]
  node [
    id 68
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 69
    label "tykn&#261;&#263;"
  ]
  node [
    id 70
    label "nabicie"
  ]
  node [
    id 71
    label "bicie"
  ]
  node [
    id 72
    label "kotwica"
  ]
  node [
    id 73
    label "godzinnik"
  ]
  node [
    id 74
    label "werk"
  ]
  node [
    id 75
    label "urz&#261;dzenie"
  ]
  node [
    id 76
    label "wahad&#322;o"
  ]
  node [
    id 77
    label "kurant"
  ]
  node [
    id 78
    label "cyferblat"
  ]
  node [
    id 79
    label "liczba"
  ]
  node [
    id 80
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 81
    label "osoba"
  ]
  node [
    id 82
    label "czasownik"
  ]
  node [
    id 83
    label "tryb"
  ]
  node [
    id 84
    label "coupling"
  ]
  node [
    id 85
    label "fleksja"
  ]
  node [
    id 86
    label "orz&#281;sek"
  ]
  node [
    id 87
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 88
    label "background"
  ]
  node [
    id 89
    label "str&#243;j"
  ]
  node [
    id 90
    label "wynikanie"
  ]
  node [
    id 91
    label "origin"
  ]
  node [
    id 92
    label "zaczynanie_si&#281;"
  ]
  node [
    id 93
    label "beginning"
  ]
  node [
    id 94
    label "wielorasowo&#347;&#263;"
  ]
  node [
    id 95
    label "geneza"
  ]
  node [
    id 96
    label "marnowanie"
  ]
  node [
    id 97
    label "unicestwianie"
  ]
  node [
    id 98
    label "sp&#281;dzanie"
  ]
  node [
    id 99
    label "digestion"
  ]
  node [
    id 100
    label "perystaltyka"
  ]
  node [
    id 101
    label "proces_fizjologiczny"
  ]
  node [
    id 102
    label "rozk&#322;adanie"
  ]
  node [
    id 103
    label "przetrawianie"
  ]
  node [
    id 104
    label "contemplation"
  ]
  node [
    id 105
    label "proceed"
  ]
  node [
    id 106
    label "pour"
  ]
  node [
    id 107
    label "mija&#263;"
  ]
  node [
    id 108
    label "sail"
  ]
  node [
    id 109
    label "przebywa&#263;"
  ]
  node [
    id 110
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 111
    label "przy&#322;&#261;cza&#263;"
  ]
  node [
    id 112
    label "przy&#322;&#261;czy&#263;"
  ]
  node [
    id 113
    label "carry"
  ]
  node [
    id 114
    label "go&#347;ci&#263;"
  ]
  node [
    id 115
    label "zanikni&#281;cie"
  ]
  node [
    id 116
    label "departure"
  ]
  node [
    id 117
    label "odej&#347;cie"
  ]
  node [
    id 118
    label "opuszczenie"
  ]
  node [
    id 119
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 120
    label "os&#322;abni&#281;cie"
  ]
  node [
    id 121
    label "ciecz"
  ]
  node [
    id 122
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 123
    label "oddalenie_si&#281;"
  ]
  node [
    id 124
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 125
    label "cross"
  ]
  node [
    id 126
    label "swimming"
  ]
  node [
    id 127
    label "min&#261;&#263;"
  ]
  node [
    id 128
    label "przeby&#263;"
  ]
  node [
    id 129
    label "up&#322;yn&#261;&#263;"
  ]
  node [
    id 130
    label "zago&#347;ci&#263;"
  ]
  node [
    id 131
    label "nadp&#322;yn&#261;&#263;"
  ]
  node [
    id 132
    label "overwhelm"
  ]
  node [
    id 133
    label "zrobi&#263;"
  ]
  node [
    id 134
    label "opatrzy&#263;"
  ]
  node [
    id 135
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 136
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 137
    label "opatrywa&#263;"
  ]
  node [
    id 138
    label "poby&#263;"
  ]
  node [
    id 139
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 140
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 141
    label "bolt"
  ]
  node [
    id 142
    label "uda&#263;_si&#281;"
  ]
  node [
    id 143
    label "date"
  ]
  node [
    id 144
    label "fall"
  ]
  node [
    id 145
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 146
    label "spowodowa&#263;"
  ]
  node [
    id 147
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 148
    label "wynika&#263;"
  ]
  node [
    id 149
    label "zdarzenie_si&#281;"
  ]
  node [
    id 150
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 151
    label "progress"
  ]
  node [
    id 152
    label "opatrzenie"
  ]
  node [
    id 153
    label "opatrywanie"
  ]
  node [
    id 154
    label "przebycie"
  ]
  node [
    id 155
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 156
    label "mini&#281;cie"
  ]
  node [
    id 157
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 158
    label "zaistnienie"
  ]
  node [
    id 159
    label "doznanie"
  ]
  node [
    id 160
    label "cruise"
  ]
  node [
    id 161
    label "lutowa&#263;"
  ]
  node [
    id 162
    label "metal"
  ]
  node [
    id 163
    label "przetrawia&#263;"
  ]
  node [
    id 164
    label "poch&#322;ania&#263;"
  ]
  node [
    id 165
    label "marnowa&#263;"
  ]
  node [
    id 166
    label "digest"
  ]
  node [
    id 167
    label "usuwa&#263;"
  ]
  node [
    id 168
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 169
    label "sp&#281;dza&#263;"
  ]
  node [
    id 170
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 171
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 172
    label "zjawianie_si&#281;"
  ]
  node [
    id 173
    label "mijanie"
  ]
  node [
    id 174
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 175
    label "przebywanie"
  ]
  node [
    id 176
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 177
    label "flux"
  ]
  node [
    id 178
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 179
    label "zaznawanie"
  ]
  node [
    id 180
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 181
    label "charakter"
  ]
  node [
    id 182
    label "epoka"
  ]
  node [
    id 183
    label "ciota"
  ]
  node [
    id 184
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 185
    label "flow"
  ]
  node [
    id 186
    label "choroba_przyrodzona"
  ]
  node [
    id 187
    label "poprawno&#347;&#263;_j&#281;zykowa"
  ]
  node [
    id 188
    label "kres"
  ]
  node [
    id 189
    label "przestrze&#324;"
  ]
  node [
    id 190
    label "empathy"
  ]
  node [
    id 191
    label "wczuwa&#263;_si&#281;"
  ]
  node [
    id 192
    label "lito&#347;&#263;"
  ]
  node [
    id 193
    label "zrozumienie"
  ]
  node [
    id 194
    label "mi&#322;o&#347;&#263;_bli&#378;niego"
  ]
  node [
    id 195
    label "po&#380;a&#322;owa&#263;"
  ]
  node [
    id 196
    label "emocja"
  ]
  node [
    id 197
    label "sympathy"
  ]
  node [
    id 198
    label "po&#380;a&#322;owanie"
  ]
  node [
    id 199
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 200
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 201
    label "skumanie"
  ]
  node [
    id 202
    label "pos&#322;uchanie"
  ]
  node [
    id 203
    label "poczucie"
  ]
  node [
    id 204
    label "appreciation"
  ]
  node [
    id 205
    label "zorientowanie"
  ]
  node [
    id 206
    label "przem&#243;wienie"
  ]
  node [
    id 207
    label "clasp"
  ]
  node [
    id 208
    label "creation"
  ]
  node [
    id 209
    label "ocenienie"
  ]
  node [
    id 210
    label "ma&#322;owa&#380;ny"
  ]
  node [
    id 211
    label "nieistotnie"
  ]
  node [
    id 212
    label "nieznaczny"
  ]
  node [
    id 213
    label "niewa&#380;ny"
  ]
  node [
    id 214
    label "drobnostkowy"
  ]
  node [
    id 215
    label "ma&#322;y"
  ]
  node [
    id 216
    label "nieznacznie"
  ]
  node [
    id 217
    label "niepowa&#380;nie"
  ]
  node [
    id 218
    label "komunikat"
  ]
  node [
    id 219
    label "jednostka_informacji"
  ]
  node [
    id 220
    label "wykrzyknik"
  ]
  node [
    id 221
    label "bit"
  ]
  node [
    id 222
    label "wordnet"
  ]
  node [
    id 223
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 224
    label "obietnica"
  ]
  node [
    id 225
    label "wypowiedzenie"
  ]
  node [
    id 226
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 227
    label "nag&#322;os"
  ]
  node [
    id 228
    label "morfem"
  ]
  node [
    id 229
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 230
    label "wyg&#322;os"
  ]
  node [
    id 231
    label "s&#322;ownictwo"
  ]
  node [
    id 232
    label "jednostka_leksykalna"
  ]
  node [
    id 233
    label "pole_semantyczne"
  ]
  node [
    id 234
    label "pisanie_si&#281;"
  ]
  node [
    id 235
    label "roi&#263;_si&#281;"
  ]
  node [
    id 236
    label "kreacjonista"
  ]
  node [
    id 237
    label "communication"
  ]
  node [
    id 238
    label "wytw&#243;r"
  ]
  node [
    id 239
    label "uroi&#263;_si&#281;"
  ]
  node [
    id 240
    label "statement"
  ]
  node [
    id 241
    label "zapewnienie"
  ]
  node [
    id 242
    label "zapowied&#378;"
  ]
  node [
    id 243
    label "termin"
  ]
  node [
    id 244
    label "terminology"
  ]
  node [
    id 245
    label "j&#281;zyk"
  ]
  node [
    id 246
    label "rozwi&#261;zanie"
  ]
  node [
    id 247
    label "przepowiedzenie"
  ]
  node [
    id 248
    label "notice"
  ]
  node [
    id 249
    label "message"
  ]
  node [
    id 250
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 251
    label "wyra&#380;enie"
  ]
  node [
    id 252
    label "konwersja"
  ]
  node [
    id 253
    label "wydanie"
  ]
  node [
    id 254
    label "denunciation"
  ]
  node [
    id 255
    label "powiedzenie"
  ]
  node [
    id 256
    label "zwerbalizowanie"
  ]
  node [
    id 257
    label "wydobycie"
  ]
  node [
    id 258
    label "generowanie"
  ]
  node [
    id 259
    label "notification"
  ]
  node [
    id 260
    label "szyk"
  ]
  node [
    id 261
    label "generowa&#263;"
  ]
  node [
    id 262
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 263
    label "leksem"
  ]
  node [
    id 264
    label "&#347;rodek"
  ]
  node [
    id 265
    label "koniec"
  ]
  node [
    id 266
    label "pocz&#261;tek"
  ]
  node [
    id 267
    label "morpheme"
  ]
  node [
    id 268
    label "forma"
  ]
  node [
    id 269
    label "s&#322;owo_maszynowe"
  ]
  node [
    id 270
    label "rytm"
  ]
  node [
    id 271
    label "cyfra"
  ]
  node [
    id 272
    label "p&#243;&#322;bajt"
  ]
  node [
    id 273
    label "system_dw&#243;jkowy"
  ]
  node [
    id 274
    label "bajt"
  ]
  node [
    id 275
    label "oktet"
  ]
  node [
    id 276
    label "baza_danych"
  ]
  node [
    id 277
    label "WordNet"
  ]
  node [
    id 278
    label "S&#322;owosie&#263;"
  ]
  node [
    id 279
    label "exclamation_mark"
  ]
  node [
    id 280
    label "znak_interpunkcyjny"
  ]
  node [
    id 281
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 282
    label "cecha"
  ]
  node [
    id 283
    label "rozmiar"
  ]
  node [
    id 284
    label "miejsce"
  ]
  node [
    id 285
    label "circumference"
  ]
  node [
    id 286
    label "strona"
  ]
  node [
    id 287
    label "cyrkumferencja"
  ]
  node [
    id 288
    label "nieprzechodnio&#347;&#263;"
  ]
  node [
    id 289
    label "przechodnio&#347;&#263;"
  ]
  node [
    id 290
    label "&#380;ycie"
  ]
  node [
    id 291
    label "koleje_losu"
  ]
  node [
    id 292
    label "okres_noworodkowy"
  ]
  node [
    id 293
    label "umarcie"
  ]
  node [
    id 294
    label "entity"
  ]
  node [
    id 295
    label "prze&#380;ycie"
  ]
  node [
    id 296
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 297
    label "dzieci&#324;stwo"
  ]
  node [
    id 298
    label "&#347;mier&#263;"
  ]
  node [
    id 299
    label "menopauza"
  ]
  node [
    id 300
    label "do&#380;ywanie"
  ]
  node [
    id 301
    label "power"
  ]
  node [
    id 302
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 303
    label "byt"
  ]
  node [
    id 304
    label "zegar_biologiczny"
  ]
  node [
    id 305
    label "wiek_matuzalemowy"
  ]
  node [
    id 306
    label "life"
  ]
  node [
    id 307
    label "subsistence"
  ]
  node [
    id 308
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 309
    label "umieranie"
  ]
  node [
    id 310
    label "bycie"
  ]
  node [
    id 311
    label "staro&#347;&#263;"
  ]
  node [
    id 312
    label "rozw&#243;j"
  ]
  node [
    id 313
    label "niemowl&#281;ctwo"
  ]
  node [
    id 314
    label "raj_utracony"
  ]
  node [
    id 315
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 316
    label "prze&#380;ywanie"
  ]
  node [
    id 317
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 318
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 319
    label "po&#322;&#243;g"
  ]
  node [
    id 320
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 321
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 322
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 323
    label "energy"
  ]
  node [
    id 324
    label "&#380;ywy"
  ]
  node [
    id 325
    label "andropauza"
  ]
  node [
    id 326
    label "szwung"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
]
