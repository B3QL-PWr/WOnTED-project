graph [
  node [
    id 0
    label "mecz"
    origin "text"
  ]
  node [
    id 1
    label "obrona"
  ]
  node [
    id 2
    label "gra"
  ]
  node [
    id 3
    label "game"
  ]
  node [
    id 4
    label "serw"
  ]
  node [
    id 5
    label "dwumecz"
  ]
  node [
    id 6
    label "zmienno&#347;&#263;"
  ]
  node [
    id 7
    label "play"
  ]
  node [
    id 8
    label "rozgrywka"
  ]
  node [
    id 9
    label "apparent_motion"
  ]
  node [
    id 10
    label "wydarzenie"
  ]
  node [
    id 11
    label "contest"
  ]
  node [
    id 12
    label "akcja"
  ]
  node [
    id 13
    label "komplet"
  ]
  node [
    id 14
    label "zabawa"
  ]
  node [
    id 15
    label "zasada"
  ]
  node [
    id 16
    label "rywalizacja"
  ]
  node [
    id 17
    label "zbijany"
  ]
  node [
    id 18
    label "post&#281;powanie"
  ]
  node [
    id 19
    label "odg&#322;os"
  ]
  node [
    id 20
    label "Pok&#233;mon"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "synteza"
  ]
  node [
    id 23
    label "odtworzenie"
  ]
  node [
    id 24
    label "rekwizyt_do_gry"
  ]
  node [
    id 25
    label "egzamin"
  ]
  node [
    id 26
    label "walka"
  ]
  node [
    id 27
    label "liga"
  ]
  node [
    id 28
    label "gracz"
  ]
  node [
    id 29
    label "poj&#281;cie"
  ]
  node [
    id 30
    label "protection"
  ]
  node [
    id 31
    label "poparcie"
  ]
  node [
    id 32
    label "reakcja"
  ]
  node [
    id 33
    label "defense"
  ]
  node [
    id 34
    label "s&#261;d"
  ]
  node [
    id 35
    label "auspices"
  ]
  node [
    id 36
    label "ochrona"
  ]
  node [
    id 37
    label "sp&#243;r"
  ]
  node [
    id 38
    label "wojsko"
  ]
  node [
    id 39
    label "manewr"
  ]
  node [
    id 40
    label "defensive_structure"
  ]
  node [
    id 41
    label "guard_duty"
  ]
  node [
    id 42
    label "strona"
  ]
  node [
    id 43
    label "uderzenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
]
