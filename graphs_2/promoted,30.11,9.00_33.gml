graph [
  node [
    id 0
    label "krakowskie"
    origin "text"
  ]
  node [
    id 1
    label "szopkarstwo"
    origin "text"
  ]
  node [
    id 2
    label "trafi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "lista"
    origin "text"
  ]
  node [
    id 4
    label "reprezentatywny"
    origin "text"
  ]
  node [
    id 5
    label "niematerialny"
    origin "text"
  ]
  node [
    id 6
    label "dziedzictwo"
    origin "text"
  ]
  node [
    id 7
    label "kulturowy"
    origin "text"
  ]
  node [
    id 8
    label "ludzko&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "sztuka_ludowa"
  ]
  node [
    id 10
    label "dolecie&#263;"
  ]
  node [
    id 11
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 12
    label "spotka&#263;"
  ]
  node [
    id 13
    label "przypasowa&#263;"
  ]
  node [
    id 14
    label "hit"
  ]
  node [
    id 15
    label "pocisk"
  ]
  node [
    id 16
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 17
    label "stumble"
  ]
  node [
    id 18
    label "dotrze&#263;"
  ]
  node [
    id 19
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 20
    label "wpa&#347;&#263;"
  ]
  node [
    id 21
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 22
    label "znale&#378;&#263;"
  ]
  node [
    id 23
    label "happen"
  ]
  node [
    id 24
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 25
    label "insert"
  ]
  node [
    id 26
    label "visualize"
  ]
  node [
    id 27
    label "pozna&#263;"
  ]
  node [
    id 28
    label "befall"
  ]
  node [
    id 29
    label "spowodowa&#263;"
  ]
  node [
    id 30
    label "go_steady"
  ]
  node [
    id 31
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 32
    label "strike"
  ]
  node [
    id 33
    label "ulec"
  ]
  node [
    id 34
    label "collapse"
  ]
  node [
    id 35
    label "rzecz"
  ]
  node [
    id 36
    label "d&#378;wi&#281;k"
  ]
  node [
    id 37
    label "fall_upon"
  ]
  node [
    id 38
    label "ponie&#347;&#263;"
  ]
  node [
    id 39
    label "ogrom"
  ]
  node [
    id 40
    label "zapach"
  ]
  node [
    id 41
    label "zakocha&#263;_si&#281;"
  ]
  node [
    id 42
    label "uderzy&#263;"
  ]
  node [
    id 43
    label "wymy&#347;li&#263;"
  ]
  node [
    id 44
    label "wkl&#281;sn&#261;&#263;"
  ]
  node [
    id 45
    label "wpada&#263;"
  ]
  node [
    id 46
    label "decline"
  ]
  node [
    id 47
    label "&#347;wiat&#322;o"
  ]
  node [
    id 48
    label "fall"
  ]
  node [
    id 49
    label "wpieprzy&#263;_si&#281;"
  ]
  node [
    id 50
    label "zaj&#347;&#263;_w_ci&#261;&#380;&#281;"
  ]
  node [
    id 51
    label "emocja"
  ]
  node [
    id 52
    label "odwiedzi&#263;"
  ]
  node [
    id 53
    label "wp&#322;yn&#261;&#263;"
  ]
  node [
    id 54
    label "wpierdoli&#263;_si&#281;"
  ]
  node [
    id 55
    label "pozyska&#263;"
  ]
  node [
    id 56
    label "oceni&#263;"
  ]
  node [
    id 57
    label "devise"
  ]
  node [
    id 58
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 59
    label "dozna&#263;"
  ]
  node [
    id 60
    label "wykry&#263;"
  ]
  node [
    id 61
    label "odzyska&#263;"
  ]
  node [
    id 62
    label "znaj&#347;&#263;"
  ]
  node [
    id 63
    label "invent"
  ]
  node [
    id 64
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 65
    label "utrze&#263;"
  ]
  node [
    id 66
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 67
    label "silnik"
  ]
  node [
    id 68
    label "catch"
  ]
  node [
    id 69
    label "dopasowa&#263;"
  ]
  node [
    id 70
    label "advance"
  ]
  node [
    id 71
    label "get"
  ]
  node [
    id 72
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 73
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 74
    label "dorobi&#263;"
  ]
  node [
    id 75
    label "become"
  ]
  node [
    id 76
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 77
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 78
    label "range"
  ]
  node [
    id 79
    label "flow"
  ]
  node [
    id 80
    label "doj&#347;&#263;"
  ]
  node [
    id 81
    label "moda"
  ]
  node [
    id 82
    label "popularny"
  ]
  node [
    id 83
    label "utw&#243;r"
  ]
  node [
    id 84
    label "sensacja"
  ]
  node [
    id 85
    label "nowina"
  ]
  node [
    id 86
    label "odkrycie"
  ]
  node [
    id 87
    label "amunicja"
  ]
  node [
    id 88
    label "g&#322;owica"
  ]
  node [
    id 89
    label "trafienie"
  ]
  node [
    id 90
    label "trafianie"
  ]
  node [
    id 91
    label "kulka"
  ]
  node [
    id 92
    label "rdze&#324;"
  ]
  node [
    id 93
    label "prochownia"
  ]
  node [
    id 94
    label "przeniesienie"
  ]
  node [
    id 95
    label "&#322;adunek_bojowy"
  ]
  node [
    id 96
    label "przenoszenie"
  ]
  node [
    id 97
    label "przenie&#347;&#263;"
  ]
  node [
    id 98
    label "trafia&#263;"
  ]
  node [
    id 99
    label "przenosi&#263;"
  ]
  node [
    id 100
    label "bro&#324;"
  ]
  node [
    id 101
    label "zbi&#243;r"
  ]
  node [
    id 102
    label "catalog"
  ]
  node [
    id 103
    label "pozycja"
  ]
  node [
    id 104
    label "tekst"
  ]
  node [
    id 105
    label "sumariusz"
  ]
  node [
    id 106
    label "book"
  ]
  node [
    id 107
    label "stock"
  ]
  node [
    id 108
    label "figurowa&#263;"
  ]
  node [
    id 109
    label "wyliczanka"
  ]
  node [
    id 110
    label "ekscerpcja"
  ]
  node [
    id 111
    label "j&#281;zykowo"
  ]
  node [
    id 112
    label "wypowied&#378;"
  ]
  node [
    id 113
    label "redakcja"
  ]
  node [
    id 114
    label "wytw&#243;r"
  ]
  node [
    id 115
    label "pomini&#281;cie"
  ]
  node [
    id 116
    label "dzie&#322;o"
  ]
  node [
    id 117
    label "preparacja"
  ]
  node [
    id 118
    label "odmianka"
  ]
  node [
    id 119
    label "opu&#347;ci&#263;"
  ]
  node [
    id 120
    label "koniektura"
  ]
  node [
    id 121
    label "pisa&#263;"
  ]
  node [
    id 122
    label "obelga"
  ]
  node [
    id 123
    label "egzemplarz"
  ]
  node [
    id 124
    label "series"
  ]
  node [
    id 125
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 126
    label "uprawianie"
  ]
  node [
    id 127
    label "praca_rolnicza"
  ]
  node [
    id 128
    label "collection"
  ]
  node [
    id 129
    label "dane"
  ]
  node [
    id 130
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 131
    label "pakiet_klimatyczny"
  ]
  node [
    id 132
    label "poj&#281;cie"
  ]
  node [
    id 133
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 134
    label "sum"
  ]
  node [
    id 135
    label "gathering"
  ]
  node [
    id 136
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 137
    label "album"
  ]
  node [
    id 138
    label "po&#322;o&#380;enie"
  ]
  node [
    id 139
    label "debit"
  ]
  node [
    id 140
    label "druk"
  ]
  node [
    id 141
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 142
    label "szata_graficzna"
  ]
  node [
    id 143
    label "wydawa&#263;"
  ]
  node [
    id 144
    label "szermierka"
  ]
  node [
    id 145
    label "spis"
  ]
  node [
    id 146
    label "wyda&#263;"
  ]
  node [
    id 147
    label "ustawienie"
  ]
  node [
    id 148
    label "publikacja"
  ]
  node [
    id 149
    label "status"
  ]
  node [
    id 150
    label "miejsce"
  ]
  node [
    id 151
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 152
    label "adres"
  ]
  node [
    id 153
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 154
    label "rozmieszczenie"
  ]
  node [
    id 155
    label "sytuacja"
  ]
  node [
    id 156
    label "rz&#261;d"
  ]
  node [
    id 157
    label "redaktor"
  ]
  node [
    id 158
    label "awansowa&#263;"
  ]
  node [
    id 159
    label "wojsko"
  ]
  node [
    id 160
    label "bearing"
  ]
  node [
    id 161
    label "znaczenie"
  ]
  node [
    id 162
    label "awans"
  ]
  node [
    id 163
    label "awansowanie"
  ]
  node [
    id 164
    label "poster"
  ]
  node [
    id 165
    label "le&#380;e&#263;"
  ]
  node [
    id 166
    label "entliczek"
  ]
  node [
    id 167
    label "zabawa"
  ]
  node [
    id 168
    label "wiersz"
  ]
  node [
    id 169
    label "pentliczek"
  ]
  node [
    id 170
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 171
    label "typowy"
  ]
  node [
    id 172
    label "dobry"
  ]
  node [
    id 173
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 174
    label "zwyczajny"
  ]
  node [
    id 175
    label "typowo"
  ]
  node [
    id 176
    label "cz&#281;sty"
  ]
  node [
    id 177
    label "zwyk&#322;y"
  ]
  node [
    id 178
    label "dobroczynny"
  ]
  node [
    id 179
    label "czw&#243;rka"
  ]
  node [
    id 180
    label "spokojny"
  ]
  node [
    id 181
    label "skuteczny"
  ]
  node [
    id 182
    label "&#347;mieszny"
  ]
  node [
    id 183
    label "mi&#322;y"
  ]
  node [
    id 184
    label "grzeczny"
  ]
  node [
    id 185
    label "powitanie"
  ]
  node [
    id 186
    label "dobrze"
  ]
  node [
    id 187
    label "ca&#322;y"
  ]
  node [
    id 188
    label "zwrot"
  ]
  node [
    id 189
    label "pomy&#347;lny"
  ]
  node [
    id 190
    label "moralny"
  ]
  node [
    id 191
    label "drogi"
  ]
  node [
    id 192
    label "pozytywny"
  ]
  node [
    id 193
    label "odpowiedni"
  ]
  node [
    id 194
    label "korzystny"
  ]
  node [
    id 195
    label "pos&#322;uszny"
  ]
  node [
    id 196
    label "niematerialnie"
  ]
  node [
    id 197
    label "dematerializowanie"
  ]
  node [
    id 198
    label "zdematerializowanie"
  ]
  node [
    id 199
    label "usuwanie"
  ]
  node [
    id 200
    label "zast&#281;powanie"
  ]
  node [
    id 201
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 202
    label "usuni&#281;cie"
  ]
  node [
    id 203
    label "wymienienie"
  ]
  node [
    id 204
    label "zachowek"
  ]
  node [
    id 205
    label "mienie"
  ]
  node [
    id 206
    label "wydziedziczenie"
  ]
  node [
    id 207
    label "scheda_spadkowa"
  ]
  node [
    id 208
    label "sukcesja"
  ]
  node [
    id 209
    label "wydziedziczy&#263;"
  ]
  node [
    id 210
    label "prawo"
  ]
  node [
    id 211
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 212
    label "umocowa&#263;"
  ]
  node [
    id 213
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 214
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 215
    label "procesualistyka"
  ]
  node [
    id 216
    label "regu&#322;a_Allena"
  ]
  node [
    id 217
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 218
    label "kryminalistyka"
  ]
  node [
    id 219
    label "struktura"
  ]
  node [
    id 220
    label "szko&#322;a"
  ]
  node [
    id 221
    label "kierunek"
  ]
  node [
    id 222
    label "zasada_d'Alemberta"
  ]
  node [
    id 223
    label "obserwacja"
  ]
  node [
    id 224
    label "normatywizm"
  ]
  node [
    id 225
    label "jurisprudence"
  ]
  node [
    id 226
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 227
    label "kultura_duchowa"
  ]
  node [
    id 228
    label "przepis"
  ]
  node [
    id 229
    label "prawo_karne_procesowe"
  ]
  node [
    id 230
    label "criterion"
  ]
  node [
    id 231
    label "kazuistyka"
  ]
  node [
    id 232
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 233
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 234
    label "kryminologia"
  ]
  node [
    id 235
    label "opis"
  ]
  node [
    id 236
    label "regu&#322;a_Glogera"
  ]
  node [
    id 237
    label "prawo_Mendla"
  ]
  node [
    id 238
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 239
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 240
    label "prawo_karne"
  ]
  node [
    id 241
    label "legislacyjnie"
  ]
  node [
    id 242
    label "twierdzenie"
  ]
  node [
    id 243
    label "cywilistyka"
  ]
  node [
    id 244
    label "judykatura"
  ]
  node [
    id 245
    label "kanonistyka"
  ]
  node [
    id 246
    label "standard"
  ]
  node [
    id 247
    label "nauka_prawa"
  ]
  node [
    id 248
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 249
    label "podmiot"
  ]
  node [
    id 250
    label "law"
  ]
  node [
    id 251
    label "qualification"
  ]
  node [
    id 252
    label "dominion"
  ]
  node [
    id 253
    label "wykonawczy"
  ]
  node [
    id 254
    label "zasada"
  ]
  node [
    id 255
    label "normalizacja"
  ]
  node [
    id 256
    label "przej&#347;cie"
  ]
  node [
    id 257
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 258
    label "rodowo&#347;&#263;"
  ]
  node [
    id 259
    label "patent"
  ]
  node [
    id 260
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 261
    label "dobra"
  ]
  node [
    id 262
    label "stan"
  ]
  node [
    id 263
    label "w&#322;asno&#347;&#263;"
  ]
  node [
    id 264
    label "przej&#347;&#263;"
  ]
  node [
    id 265
    label "possession"
  ]
  node [
    id 266
    label "spadek"
  ]
  node [
    id 267
    label "zabra&#263;"
  ]
  node [
    id 268
    label "disinherit"
  ]
  node [
    id 269
    label "zabranie"
  ]
  node [
    id 270
    label "disinheritance"
  ]
  node [
    id 271
    label "proces_biologiczny"
  ]
  node [
    id 272
    label "seniorat"
  ]
  node [
    id 273
    label "kulturowo"
  ]
  node [
    id 274
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 275
    label "cz&#322;owiek"
  ]
  node [
    id 276
    label "facylitacja"
  ]
  node [
    id 277
    label "asymilowanie"
  ]
  node [
    id 278
    label "wapniak"
  ]
  node [
    id 279
    label "asymilowa&#263;"
  ]
  node [
    id 280
    label "os&#322;abia&#263;"
  ]
  node [
    id 281
    label "posta&#263;"
  ]
  node [
    id 282
    label "hominid"
  ]
  node [
    id 283
    label "podw&#322;adny"
  ]
  node [
    id 284
    label "os&#322;abianie"
  ]
  node [
    id 285
    label "g&#322;owa"
  ]
  node [
    id 286
    label "figura"
  ]
  node [
    id 287
    label "portrecista"
  ]
  node [
    id 288
    label "dwun&#243;g"
  ]
  node [
    id 289
    label "profanum"
  ]
  node [
    id 290
    label "mikrokosmos"
  ]
  node [
    id 291
    label "nasada"
  ]
  node [
    id 292
    label "duch"
  ]
  node [
    id 293
    label "antropochoria"
  ]
  node [
    id 294
    label "osoba"
  ]
  node [
    id 295
    label "wz&#243;r"
  ]
  node [
    id 296
    label "senior"
  ]
  node [
    id 297
    label "oddzia&#322;ywanie"
  ]
  node [
    id 298
    label "Adam"
  ]
  node [
    id 299
    label "homo_sapiens"
  ]
  node [
    id 300
    label "polifag"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
]
