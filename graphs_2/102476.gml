graph [
  node [
    id 0
    label "towarzystwo"
    origin "text"
  ]
  node [
    id 1
    label "przyjaciel"
    origin "text"
  ]
  node [
    id 2
    label "ozorkowy"
    origin "text"
  ]
  node [
    id 3
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "historyczny"
    origin "text"
  ]
  node [
    id 5
    label "spacer"
    origin "text"
  ]
  node [
    id 6
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 7
    label "tym"
    origin "text"
  ]
  node [
    id 8
    label "razem"
    origin "text"
  ]
  node [
    id 9
    label "pozna&#263;"
    origin "text"
  ]
  node [
    id 10
    label "historia"
    origin "text"
  ]
  node [
    id 11
    label "parafia"
    origin "text"
  ]
  node [
    id 12
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 13
    label "ewangelicko"
    origin "text"
  ]
  node [
    id 14
    label "augsburski"
    origin "text"
  ]
  node [
    id 15
    label "pan"
    origin "text"
  ]
  node [
    id 16
    label "aposto&#322;"
    origin "text"
  ]
  node [
    id 17
    label "jezus"
    origin "text"
  ]
  node [
    id 18
    label "chrystus"
    origin "text"
  ]
  node [
    id 19
    label "zbi&#243;rka"
    origin "text"
  ]
  node [
    id 20
    label "przed"
    origin "text"
  ]
  node [
    id 21
    label "ula"
    origin "text"
  ]
  node [
    id 22
    label "&#347;rednia"
    origin "text"
  ]
  node [
    id 23
    label "maj"
    origin "text"
  ]
  node [
    id 24
    label "godz"
    origin "text"
  ]
  node [
    id 25
    label "fabianie"
  ]
  node [
    id 26
    label "grono"
  ]
  node [
    id 27
    label "Rotary_International"
  ]
  node [
    id 28
    label "partnership"
  ]
  node [
    id 29
    label "Rzeczpospolita_babi&#324;ska"
  ]
  node [
    id 30
    label "asystencja"
  ]
  node [
    id 31
    label "Monar"
  ]
  node [
    id 32
    label "Krakowska_Kongregacja_Kupiecka"
  ]
  node [
    id 33
    label "Eleusis"
  ]
  node [
    id 34
    label "Chewra_Kadisza"
  ]
  node [
    id 35
    label "Polskie_Towarzystwo_Turystyczno-Krajoznawcze"
  ]
  node [
    id 36
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 37
    label "obecno&#347;&#263;"
  ]
  node [
    id 38
    label "organizacja"
  ]
  node [
    id 39
    label "grupa"
  ]
  node [
    id 40
    label "umowa_stowarzyszeniowa"
  ]
  node [
    id 41
    label "wi&#281;&#378;"
  ]
  node [
    id 42
    label "Ku&#378;nica_Ko&#322;&#322;&#261;tajowska"
  ]
  node [
    id 43
    label "zwi&#261;zanie"
  ]
  node [
    id 44
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 45
    label "zwi&#261;za&#263;"
  ]
  node [
    id 46
    label "wi&#261;zanie"
  ]
  node [
    id 47
    label "zwi&#261;zek"
  ]
  node [
    id 48
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 49
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 50
    label "marriage"
  ]
  node [
    id 51
    label "bratnia_dusza"
  ]
  node [
    id 52
    label "marketing_afiliacyjny"
  ]
  node [
    id 53
    label "zbi&#243;r"
  ]
  node [
    id 54
    label "owoc"
  ]
  node [
    id 55
    label "mirycetyna"
  ]
  node [
    id 56
    label "jagoda"
  ]
  node [
    id 57
    label "ki&#347;&#263;"
  ]
  node [
    id 58
    label "being"
  ]
  node [
    id 59
    label "cecha"
  ]
  node [
    id 60
    label "stan"
  ]
  node [
    id 61
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 62
    label "przybud&#243;wka"
  ]
  node [
    id 63
    label "struktura"
  ]
  node [
    id 64
    label "organization"
  ]
  node [
    id 65
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 66
    label "od&#322;am"
  ]
  node [
    id 67
    label "TOPR"
  ]
  node [
    id 68
    label "komitet_koordynacyjny"
  ]
  node [
    id 69
    label "przedstawicielstwo"
  ]
  node [
    id 70
    label "ZMP"
  ]
  node [
    id 71
    label "Cepelia"
  ]
  node [
    id 72
    label "GOPR"
  ]
  node [
    id 73
    label "endecki"
  ]
  node [
    id 74
    label "ZBoWiD"
  ]
  node [
    id 75
    label "podmiot"
  ]
  node [
    id 76
    label "boj&#243;wka"
  ]
  node [
    id 77
    label "ZOMO"
  ]
  node [
    id 78
    label "zesp&#243;&#322;"
  ]
  node [
    id 79
    label "jednostka_organizacyjna"
  ]
  node [
    id 80
    label "centrala"
  ]
  node [
    id 81
    label "asymilowa&#263;"
  ]
  node [
    id 82
    label "kompozycja"
  ]
  node [
    id 83
    label "pakiet_klimatyczny"
  ]
  node [
    id 84
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 85
    label "type"
  ]
  node [
    id 86
    label "cz&#261;steczka"
  ]
  node [
    id 87
    label "gromada"
  ]
  node [
    id 88
    label "specgrupa"
  ]
  node [
    id 89
    label "egzemplarz"
  ]
  node [
    id 90
    label "stage_set"
  ]
  node [
    id 91
    label "asymilowanie"
  ]
  node [
    id 92
    label "odm&#322;odzenie"
  ]
  node [
    id 93
    label "odm&#322;adza&#263;"
  ]
  node [
    id 94
    label "harcerze_starsi"
  ]
  node [
    id 95
    label "jednostka_systematyczna"
  ]
  node [
    id 96
    label "oddzia&#322;"
  ]
  node [
    id 97
    label "category"
  ]
  node [
    id 98
    label "liga"
  ]
  node [
    id 99
    label "&#346;wietliki"
  ]
  node [
    id 100
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 101
    label "formacja_geologiczna"
  ]
  node [
    id 102
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 103
    label "Eurogrupa"
  ]
  node [
    id 104
    label "Terranie"
  ]
  node [
    id 105
    label "odm&#322;adzanie"
  ]
  node [
    id 106
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 107
    label "Entuzjastki"
  ]
  node [
    id 108
    label "wym&#243;g"
  ]
  node [
    id 109
    label "asysta"
  ]
  node [
    id 110
    label "reedukator"
  ]
  node [
    id 111
    label "G&#322;osk&#243;w"
  ]
  node [
    id 112
    label "harcerstwo"
  ]
  node [
    id 113
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 114
    label "kochanek"
  ]
  node [
    id 115
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 116
    label "drogi"
  ]
  node [
    id 117
    label "sympatyk"
  ]
  node [
    id 118
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 119
    label "amikus"
  ]
  node [
    id 120
    label "pobratymiec"
  ]
  node [
    id 121
    label "kum"
  ]
  node [
    id 122
    label "fagas"
  ]
  node [
    id 123
    label "partner"
  ]
  node [
    id 124
    label "kocha&#347;"
  ]
  node [
    id 125
    label "mi&#322;y"
  ]
  node [
    id 126
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 127
    label "bratek"
  ]
  node [
    id 128
    label "zwrot"
  ]
  node [
    id 129
    label "chrzest"
  ]
  node [
    id 130
    label "cz&#322;owiek"
  ]
  node [
    id 131
    label "kumostwo"
  ]
  node [
    id 132
    label "plemiennik"
  ]
  node [
    id 133
    label "wsp&#243;&#322;obywatel"
  ]
  node [
    id 134
    label "pobratymca"
  ]
  node [
    id 135
    label "stronnik"
  ]
  node [
    id 136
    label "wsp&#243;&#322;ziomek"
  ]
  node [
    id 137
    label "brat"
  ]
  node [
    id 138
    label "ojczyc"
  ]
  node [
    id 139
    label "zwolennik"
  ]
  node [
    id 140
    label "warto&#347;ciowy"
  ]
  node [
    id 141
    label "bliski"
  ]
  node [
    id 142
    label "drogo"
  ]
  node [
    id 143
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 144
    label "mi&#281;sny"
  ]
  node [
    id 145
    label "sklep"
  ]
  node [
    id 146
    label "hodowlany"
  ]
  node [
    id 147
    label "bia&#322;kowy"
  ]
  node [
    id 148
    label "specjalny"
  ]
  node [
    id 149
    label "naturalny"
  ]
  node [
    id 150
    label "oferowa&#263;"
  ]
  node [
    id 151
    label "ask"
  ]
  node [
    id 152
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 153
    label "invite"
  ]
  node [
    id 154
    label "volunteer"
  ]
  node [
    id 155
    label "zach&#281;ca&#263;"
  ]
  node [
    id 156
    label "zgodny"
  ]
  node [
    id 157
    label "dziejowo"
  ]
  node [
    id 158
    label "prawdziwy"
  ]
  node [
    id 159
    label "wiekopomny"
  ]
  node [
    id 160
    label "historycznie"
  ]
  node [
    id 161
    label "dawny"
  ]
  node [
    id 162
    label "spokojny"
  ]
  node [
    id 163
    label "zgodnie"
  ]
  node [
    id 164
    label "zbie&#380;ny"
  ]
  node [
    id 165
    label "dobry"
  ]
  node [
    id 166
    label "prawdziwie"
  ]
  node [
    id 167
    label "podobny"
  ]
  node [
    id 168
    label "m&#261;dry"
  ]
  node [
    id 169
    label "szczery"
  ]
  node [
    id 170
    label "naprawd&#281;"
  ]
  node [
    id 171
    label "&#380;ywny"
  ]
  node [
    id 172
    label "realnie"
  ]
  node [
    id 173
    label "od_dawna"
  ]
  node [
    id 174
    label "anachroniczny"
  ]
  node [
    id 175
    label "dawniej"
  ]
  node [
    id 176
    label "odleg&#322;y"
  ]
  node [
    id 177
    label "przesz&#322;y"
  ]
  node [
    id 178
    label "d&#322;ugoletni"
  ]
  node [
    id 179
    label "poprzedni"
  ]
  node [
    id 180
    label "dawno"
  ]
  node [
    id 181
    label "przestarza&#322;y"
  ]
  node [
    id 182
    label "kombatant"
  ]
  node [
    id 183
    label "niegdysiejszy"
  ]
  node [
    id 184
    label "wcze&#347;niejszy"
  ]
  node [
    id 185
    label "stary"
  ]
  node [
    id 186
    label "donios&#322;y"
  ]
  node [
    id 187
    label "wielki"
  ]
  node [
    id 188
    label "pierwotnie"
  ]
  node [
    id 189
    label "natural_process"
  ]
  node [
    id 190
    label "ruch"
  ]
  node [
    id 191
    label "prezentacja"
  ]
  node [
    id 192
    label "czynno&#347;&#263;"
  ]
  node [
    id 193
    label "move"
  ]
  node [
    id 194
    label "zmiana"
  ]
  node [
    id 195
    label "model"
  ]
  node [
    id 196
    label "aktywno&#347;&#263;"
  ]
  node [
    id 197
    label "utrzymywanie"
  ]
  node [
    id 198
    label "utrzymywa&#263;"
  ]
  node [
    id 199
    label "taktyka"
  ]
  node [
    id 200
    label "d&#322;ugi"
  ]
  node [
    id 201
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 202
    label "kanciasty"
  ]
  node [
    id 203
    label "utrzyma&#263;"
  ]
  node [
    id 204
    label "myk"
  ]
  node [
    id 205
    label "manewr"
  ]
  node [
    id 206
    label "utrzymanie"
  ]
  node [
    id 207
    label "wydarzenie"
  ]
  node [
    id 208
    label "tumult"
  ]
  node [
    id 209
    label "stopek"
  ]
  node [
    id 210
    label "movement"
  ]
  node [
    id 211
    label "strumie&#324;"
  ]
  node [
    id 212
    label "komunikacja"
  ]
  node [
    id 213
    label "lokomocja"
  ]
  node [
    id 214
    label "drift"
  ]
  node [
    id 215
    label "commercial_enterprise"
  ]
  node [
    id 216
    label "zjawisko"
  ]
  node [
    id 217
    label "apraksja"
  ]
  node [
    id 218
    label "proces"
  ]
  node [
    id 219
    label "poruszenie"
  ]
  node [
    id 220
    label "mechanika"
  ]
  node [
    id 221
    label "travel"
  ]
  node [
    id 222
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 223
    label "dyssypacja_energii"
  ]
  node [
    id 224
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 225
    label "kr&#243;tki"
  ]
  node [
    id 226
    label "bezproblemowy"
  ]
  node [
    id 227
    label "activity"
  ]
  node [
    id 228
    label "komunikat"
  ]
  node [
    id 229
    label "grafika_u&#380;ytkowa"
  ]
  node [
    id 230
    label "show"
  ]
  node [
    id 231
    label "impreza"
  ]
  node [
    id 232
    label "pokaz&#243;wka"
  ]
  node [
    id 233
    label "informacja"
  ]
  node [
    id 234
    label "wypowied&#378;"
  ]
  node [
    id 235
    label "szkolenie"
  ]
  node [
    id 236
    label "prezenter"
  ]
  node [
    id 237
    label "&#322;&#261;cznie"
  ]
  node [
    id 238
    label "zbiorczo"
  ]
  node [
    id 239
    label "&#322;&#261;czny"
  ]
  node [
    id 240
    label "przyswoi&#263;"
  ]
  node [
    id 241
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 242
    label "teach"
  ]
  node [
    id 243
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 244
    label "zrozumie&#263;"
  ]
  node [
    id 245
    label "visualize"
  ]
  node [
    id 246
    label "experience"
  ]
  node [
    id 247
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 248
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 249
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 250
    label "topographic_point"
  ]
  node [
    id 251
    label "feel"
  ]
  node [
    id 252
    label "permit"
  ]
  node [
    id 253
    label "spowodowa&#263;"
  ]
  node [
    id 254
    label "dostrzec"
  ]
  node [
    id 255
    label "zauwa&#380;y&#263;"
  ]
  node [
    id 256
    label "pobra&#263;"
  ]
  node [
    id 257
    label "thrill"
  ]
  node [
    id 258
    label "organizm"
  ]
  node [
    id 259
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 260
    label "translate"
  ]
  node [
    id 261
    label "kultura"
  ]
  node [
    id 262
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 263
    label "think"
  ]
  node [
    id 264
    label "skuma&#263;"
  ]
  node [
    id 265
    label "zacz&#261;&#263;"
  ]
  node [
    id 266
    label "oceni&#263;"
  ]
  node [
    id 267
    label "do"
  ]
  node [
    id 268
    label "poczu&#263;"
  ]
  node [
    id 269
    label "varsavianistyka"
  ]
  node [
    id 270
    label "bizantynistyka"
  ]
  node [
    id 271
    label "genealogia"
  ]
  node [
    id 272
    label "epoka"
  ]
  node [
    id 273
    label "numizmatyka"
  ]
  node [
    id 274
    label "paleografia"
  ]
  node [
    id 275
    label "annalistyka"
  ]
  node [
    id 276
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 277
    label "historyka"
  ]
  node [
    id 278
    label "muzealnictwo"
  ]
  node [
    id 279
    label "nautologia"
  ]
  node [
    id 280
    label "filigranistyka"
  ]
  node [
    id 281
    label "hista"
  ]
  node [
    id 282
    label "weksylologia"
  ]
  node [
    id 283
    label "historiografia"
  ]
  node [
    id 284
    label "charakter"
  ]
  node [
    id 285
    label "przedmiot"
  ]
  node [
    id 286
    label "chronologia"
  ]
  node [
    id 287
    label "historia_sztuki"
  ]
  node [
    id 288
    label "przebiec"
  ]
  node [
    id 289
    label "przebiegni&#281;cie"
  ]
  node [
    id 290
    label "ikonografia"
  ]
  node [
    id 291
    label "prezentyzm"
  ]
  node [
    id 292
    label "mediewistyka"
  ]
  node [
    id 293
    label "dyplomatyka"
  ]
  node [
    id 294
    label "neografia"
  ]
  node [
    id 295
    label "ruralistyka"
  ]
  node [
    id 296
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 297
    label "prozopografia"
  ]
  node [
    id 298
    label "motyw"
  ]
  node [
    id 299
    label "epigrafika"
  ]
  node [
    id 300
    label "papirologia"
  ]
  node [
    id 301
    label "sfragistyka"
  ]
  node [
    id 302
    label "nauka_humanistyczna"
  ]
  node [
    id 303
    label "report"
  ]
  node [
    id 304
    label "zabytkoznawstwo"
  ]
  node [
    id 305
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 306
    label "archiwistyka"
  ]
  node [
    id 307
    label "heraldyka"
  ]
  node [
    id 308
    label "historia_gospodarcza"
  ]
  node [
    id 309
    label "kierunek"
  ]
  node [
    id 310
    label "fabu&#322;a"
  ]
  node [
    id 311
    label "czas"
  ]
  node [
    id 312
    label "&#380;ycie"
  ]
  node [
    id 313
    label "koleje_losu"
  ]
  node [
    id 314
    label "discipline"
  ]
  node [
    id 315
    label "zboczy&#263;"
  ]
  node [
    id 316
    label "w&#261;tek"
  ]
  node [
    id 317
    label "entity"
  ]
  node [
    id 318
    label "sponiewiera&#263;"
  ]
  node [
    id 319
    label "zboczenie"
  ]
  node [
    id 320
    label "zbaczanie"
  ]
  node [
    id 321
    label "thing"
  ]
  node [
    id 322
    label "om&#243;wi&#263;"
  ]
  node [
    id 323
    label "tre&#347;&#263;"
  ]
  node [
    id 324
    label "element"
  ]
  node [
    id 325
    label "kr&#261;&#380;enie"
  ]
  node [
    id 326
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 327
    label "istota"
  ]
  node [
    id 328
    label "zbacza&#263;"
  ]
  node [
    id 329
    label "om&#243;wienie"
  ]
  node [
    id 330
    label "rzecz"
  ]
  node [
    id 331
    label "tematyka"
  ]
  node [
    id 332
    label "omawianie"
  ]
  node [
    id 333
    label "omawia&#263;"
  ]
  node [
    id 334
    label "robienie"
  ]
  node [
    id 335
    label "program_nauczania"
  ]
  node [
    id 336
    label "sponiewieranie"
  ]
  node [
    id 337
    label "parafrazowanie"
  ]
  node [
    id 338
    label "stylizacja"
  ]
  node [
    id 339
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 340
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 341
    label "strawestowanie"
  ]
  node [
    id 342
    label "sparafrazowanie"
  ]
  node [
    id 343
    label "sformu&#322;owanie"
  ]
  node [
    id 344
    label "pos&#322;uchanie"
  ]
  node [
    id 345
    label "strawestowa&#263;"
  ]
  node [
    id 346
    label "parafrazowa&#263;"
  ]
  node [
    id 347
    label "delimitacja"
  ]
  node [
    id 348
    label "rezultat"
  ]
  node [
    id 349
    label "ozdobnik"
  ]
  node [
    id 350
    label "trawestowa&#263;"
  ]
  node [
    id 351
    label "s&#261;d"
  ]
  node [
    id 352
    label "sparafrazowa&#263;"
  ]
  node [
    id 353
    label "trawestowanie"
  ]
  node [
    id 354
    label "linia"
  ]
  node [
    id 355
    label "przebieg"
  ]
  node [
    id 356
    label "orientowa&#263;"
  ]
  node [
    id 357
    label "zorientowa&#263;"
  ]
  node [
    id 358
    label "praktyka"
  ]
  node [
    id 359
    label "skr&#281;cenie"
  ]
  node [
    id 360
    label "skr&#281;ci&#263;"
  ]
  node [
    id 361
    label "przeorientowanie"
  ]
  node [
    id 362
    label "g&#243;ra"
  ]
  node [
    id 363
    label "orientowanie"
  ]
  node [
    id 364
    label "zorientowanie"
  ]
  node [
    id 365
    label "ty&#322;"
  ]
  node [
    id 366
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 367
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 368
    label "przeorientowywanie"
  ]
  node [
    id 369
    label "bok"
  ]
  node [
    id 370
    label "ideologia"
  ]
  node [
    id 371
    label "skr&#281;canie"
  ]
  node [
    id 372
    label "orientacja"
  ]
  node [
    id 373
    label "metoda"
  ]
  node [
    id 374
    label "studia"
  ]
  node [
    id 375
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 376
    label "przeorientowa&#263;"
  ]
  node [
    id 377
    label "bearing"
  ]
  node [
    id 378
    label "spos&#243;b"
  ]
  node [
    id 379
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 380
    label "prz&#243;d"
  ]
  node [
    id 381
    label "skr&#281;ca&#263;"
  ]
  node [
    id 382
    label "system"
  ]
  node [
    id 383
    label "przeorientowywa&#263;"
  ]
  node [
    id 384
    label "bajos"
  ]
  node [
    id 385
    label "kelowej"
  ]
  node [
    id 386
    label "paleocen"
  ]
  node [
    id 387
    label "&#347;rodkowy_trias"
  ]
  node [
    id 388
    label "miocen"
  ]
  node [
    id 389
    label "Zeitgeist"
  ]
  node [
    id 390
    label "schy&#322;ek"
  ]
  node [
    id 391
    label "plejstocen"
  ]
  node [
    id 392
    label "aalen"
  ]
  node [
    id 393
    label "jura_&#347;rodkowa"
  ]
  node [
    id 394
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 395
    label "jura_wczesna"
  ]
  node [
    id 396
    label "jednostka_geologiczna"
  ]
  node [
    id 397
    label "eocen"
  ]
  node [
    id 398
    label "term"
  ]
  node [
    id 399
    label "dzieje"
  ]
  node [
    id 400
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 401
    label "wczesny_trias"
  ]
  node [
    id 402
    label "okres"
  ]
  node [
    id 403
    label "holocen"
  ]
  node [
    id 404
    label "pliocen"
  ]
  node [
    id 405
    label "oligocen"
  ]
  node [
    id 406
    label "perypetia"
  ]
  node [
    id 407
    label "w&#281;ze&#322;"
  ]
  node [
    id 408
    label "opowiadanie"
  ]
  node [
    id 409
    label "dendrochronologia"
  ]
  node [
    id 410
    label "kolejno&#347;&#263;"
  ]
  node [
    id 411
    label "datacja"
  ]
  node [
    id 412
    label "plastyka"
  ]
  node [
    id 413
    label "&#347;redniowiecze"
  ]
  node [
    id 414
    label "descendencja"
  ]
  node [
    id 415
    label "pochodzenie"
  ]
  node [
    id 416
    label "drzewo_genealogiczne"
  ]
  node [
    id 417
    label "procedencja"
  ]
  node [
    id 418
    label "numismatics"
  ]
  node [
    id 419
    label "kolekcjonerstwo"
  ]
  node [
    id 420
    label "medal"
  ]
  node [
    id 421
    label "archeologia"
  ]
  node [
    id 422
    label "archiwoznawstwo"
  ]
  node [
    id 423
    label "Byzantine_Empire"
  ]
  node [
    id 424
    label "brachygrafia"
  ]
  node [
    id 425
    label "pismo"
  ]
  node [
    id 426
    label "architektura"
  ]
  node [
    id 427
    label "nauka"
  ]
  node [
    id 428
    label "or&#281;&#380;"
  ]
  node [
    id 429
    label "s&#322;up"
  ]
  node [
    id 430
    label "pas"
  ]
  node [
    id 431
    label "barwa_heraldyczna"
  ]
  node [
    id 432
    label "herb"
  ]
  node [
    id 433
    label "oksza"
  ]
  node [
    id 434
    label "museum"
  ]
  node [
    id 435
    label "bibliologia"
  ]
  node [
    id 436
    label "pi&#347;miennictwo"
  ]
  node [
    id 437
    label "historiography"
  ]
  node [
    id 438
    label "metodologia"
  ]
  node [
    id 439
    label "fraza"
  ]
  node [
    id 440
    label "ozdoba"
  ]
  node [
    id 441
    label "przyczyna"
  ]
  node [
    id 442
    label "temat"
  ]
  node [
    id 443
    label "melodia"
  ]
  node [
    id 444
    label "sytuacja"
  ]
  node [
    id 445
    label "umowa"
  ]
  node [
    id 446
    label "cover"
  ]
  node [
    id 447
    label "fizjonomia"
  ]
  node [
    id 448
    label "kompleksja"
  ]
  node [
    id 449
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 450
    label "psychika"
  ]
  node [
    id 451
    label "posta&#263;"
  ]
  node [
    id 452
    label "osobowo&#347;&#263;"
  ]
  node [
    id 453
    label "proceed"
  ]
  node [
    id 454
    label "fly"
  ]
  node [
    id 455
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 456
    label "przeby&#263;"
  ]
  node [
    id 457
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 458
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 459
    label "przesun&#261;&#263;"
  ]
  node [
    id 460
    label "przemierzy&#263;"
  ]
  node [
    id 461
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 462
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 463
    label "run"
  ]
  node [
    id 464
    label "przebycie"
  ]
  node [
    id 465
    label "przemkni&#281;cie"
  ]
  node [
    id 466
    label "zdarzenie_si&#281;"
  ]
  node [
    id 467
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 468
    label "zabrzmienie"
  ]
  node [
    id 469
    label "parochia"
  ]
  node [
    id 470
    label "rada_parafialna"
  ]
  node [
    id 471
    label "dekanat"
  ]
  node [
    id 472
    label "parafianin"
  ]
  node [
    id 473
    label "diecezja"
  ]
  node [
    id 474
    label "zabudowania"
  ]
  node [
    id 475
    label "kompleks"
  ]
  node [
    id 476
    label "obszar"
  ]
  node [
    id 477
    label "nawa"
  ]
  node [
    id 478
    label "prezbiterium"
  ]
  node [
    id 479
    label "nerwica_eklezjogenna"
  ]
  node [
    id 480
    label "kropielnica"
  ]
  node [
    id 481
    label "ub&#322;agalnia"
  ]
  node [
    id 482
    label "zakrystia"
  ]
  node [
    id 483
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 484
    label "kult"
  ]
  node [
    id 485
    label "church"
  ]
  node [
    id 486
    label "organizacja_religijna"
  ]
  node [
    id 487
    label "wsp&#243;lnota"
  ]
  node [
    id 488
    label "kruchta"
  ]
  node [
    id 489
    label "dom"
  ]
  node [
    id 490
    label "Ska&#322;ka"
  ]
  node [
    id 491
    label "ciemniak"
  ]
  node [
    id 492
    label "prostaczek"
  ]
  node [
    id 493
    label "wstecznik"
  ]
  node [
    id 494
    label "parafia&#324;stwo"
  ]
  node [
    id 495
    label "za&#347;ciankowy"
  ]
  node [
    id 496
    label "owieczka"
  ]
  node [
    id 497
    label "katolik"
  ]
  node [
    id 498
    label "wierny"
  ]
  node [
    id 499
    label "jednostka_administracyjna"
  ]
  node [
    id 500
    label "rada_kap&#322;a&#324;ska"
  ]
  node [
    id 501
    label "metropolia"
  ]
  node [
    id 502
    label "Ko&#347;ci&#243;&#322;_katolicki"
  ]
  node [
    id 503
    label "archidiakonat"
  ]
  node [
    id 504
    label "Ko&#347;ci&#243;&#322;"
  ]
  node [
    id 505
    label "urz&#261;d"
  ]
  node [
    id 506
    label "deanship"
  ]
  node [
    id 507
    label "Walencja"
  ]
  node [
    id 508
    label "society"
  ]
  node [
    id 509
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 510
    label "Ba&#322;kany"
  ]
  node [
    id 511
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 512
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 513
    label "Skandynawia"
  ]
  node [
    id 514
    label "podobie&#324;stwo"
  ]
  node [
    id 515
    label "przybytek"
  ]
  node [
    id 516
    label "budynek"
  ]
  node [
    id 517
    label "siedlisko"
  ]
  node [
    id 518
    label "poj&#281;cie"
  ]
  node [
    id 519
    label "substancja_mieszkaniowa"
  ]
  node [
    id 520
    label "rodzina"
  ]
  node [
    id 521
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 522
    label "siedziba"
  ]
  node [
    id 523
    label "dom_rodzinny"
  ]
  node [
    id 524
    label "garderoba"
  ]
  node [
    id 525
    label "fratria"
  ]
  node [
    id 526
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 527
    label "stead"
  ]
  node [
    id 528
    label "instytucja"
  ]
  node [
    id 529
    label "wiecha"
  ]
  node [
    id 530
    label "postawa"
  ]
  node [
    id 531
    label "translacja"
  ]
  node [
    id 532
    label "obrz&#281;d"
  ]
  node [
    id 533
    label "uwielbienie"
  ]
  node [
    id 534
    label "religia"
  ]
  node [
    id 535
    label "egzegeta"
  ]
  node [
    id 536
    label "worship"
  ]
  node [
    id 537
    label "przedsionek"
  ]
  node [
    id 538
    label "babiniec"
  ]
  node [
    id 539
    label "korpus"
  ]
  node [
    id 540
    label "naczynie_na_wod&#281;_&#347;wi&#281;con&#261;"
  ]
  node [
    id 541
    label "paramenty"
  ]
  node [
    id 542
    label "pomieszczenie"
  ]
  node [
    id 543
    label "tabernakulum"
  ]
  node [
    id 544
    label "o&#322;tarz"
  ]
  node [
    id 545
    label "&#347;rodowisko"
  ]
  node [
    id 546
    label "stalle"
  ]
  node [
    id 547
    label "duchowie&#324;stwo"
  ]
  node [
    id 548
    label "lampka_wieczysta"
  ]
  node [
    id 549
    label "murza"
  ]
  node [
    id 550
    label "belfer"
  ]
  node [
    id 551
    label "szkolnik"
  ]
  node [
    id 552
    label "pupil"
  ]
  node [
    id 553
    label "ojciec"
  ]
  node [
    id 554
    label "kszta&#322;ciciel"
  ]
  node [
    id 555
    label "Midas"
  ]
  node [
    id 556
    label "przyw&#243;dca"
  ]
  node [
    id 557
    label "opiekun"
  ]
  node [
    id 558
    label "Mieszko_I"
  ]
  node [
    id 559
    label "doros&#322;y"
  ]
  node [
    id 560
    label "pracodawca"
  ]
  node [
    id 561
    label "profesor"
  ]
  node [
    id 562
    label "m&#261;&#380;"
  ]
  node [
    id 563
    label "rz&#261;dzenie"
  ]
  node [
    id 564
    label "bogaty"
  ]
  node [
    id 565
    label "pa&#324;stwo"
  ]
  node [
    id 566
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 567
    label "w&#322;odarz"
  ]
  node [
    id 568
    label "nabab"
  ]
  node [
    id 569
    label "preceptor"
  ]
  node [
    id 570
    label "samiec"
  ]
  node [
    id 571
    label "pedagog"
  ]
  node [
    id 572
    label "efendi"
  ]
  node [
    id 573
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 574
    label "popularyzator"
  ]
  node [
    id 575
    label "gra_w_karty"
  ]
  node [
    id 576
    label "jegomo&#347;&#263;"
  ]
  node [
    id 577
    label "androlog"
  ]
  node [
    id 578
    label "andropauza"
  ]
  node [
    id 579
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 580
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 581
    label "ch&#322;opina"
  ]
  node [
    id 582
    label "w&#322;adza"
  ]
  node [
    id 583
    label "Sabataj_Cwi"
  ]
  node [
    id 584
    label "lider"
  ]
  node [
    id 585
    label "Mao"
  ]
  node [
    id 586
    label "Anders"
  ]
  node [
    id 587
    label "Fidel_Castro"
  ]
  node [
    id 588
    label "Miko&#322;ajczyk"
  ]
  node [
    id 589
    label "Tito"
  ]
  node [
    id 590
    label "Ko&#347;ciuszko"
  ]
  node [
    id 591
    label "zwierzchnik"
  ]
  node [
    id 592
    label "p&#322;atnik"
  ]
  node [
    id 593
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 594
    label "nadzorca"
  ]
  node [
    id 595
    label "funkcjonariusz"
  ]
  node [
    id 596
    label "wykupywanie"
  ]
  node [
    id 597
    label "wykupienie"
  ]
  node [
    id 598
    label "bycie_w_posiadaniu"
  ]
  node [
    id 599
    label "rozszerzyciel"
  ]
  node [
    id 600
    label "nasada"
  ]
  node [
    id 601
    label "profanum"
  ]
  node [
    id 602
    label "wz&#243;r"
  ]
  node [
    id 603
    label "senior"
  ]
  node [
    id 604
    label "os&#322;abia&#263;"
  ]
  node [
    id 605
    label "homo_sapiens"
  ]
  node [
    id 606
    label "osoba"
  ]
  node [
    id 607
    label "ludzko&#347;&#263;"
  ]
  node [
    id 608
    label "Adam"
  ]
  node [
    id 609
    label "hominid"
  ]
  node [
    id 610
    label "portrecista"
  ]
  node [
    id 611
    label "polifag"
  ]
  node [
    id 612
    label "podw&#322;adny"
  ]
  node [
    id 613
    label "dwun&#243;g"
  ]
  node [
    id 614
    label "wapniak"
  ]
  node [
    id 615
    label "duch"
  ]
  node [
    id 616
    label "os&#322;abianie"
  ]
  node [
    id 617
    label "antropochoria"
  ]
  node [
    id 618
    label "figura"
  ]
  node [
    id 619
    label "g&#322;owa"
  ]
  node [
    id 620
    label "mikrokosmos"
  ]
  node [
    id 621
    label "oddzia&#322;ywanie"
  ]
  node [
    id 622
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 623
    label "du&#380;y"
  ]
  node [
    id 624
    label "dojrza&#322;y"
  ]
  node [
    id 625
    label "dojrzale"
  ]
  node [
    id 626
    label "wydoro&#347;lenie"
  ]
  node [
    id 627
    label "doro&#347;lenie"
  ]
  node [
    id 628
    label "&#378;ra&#322;y"
  ]
  node [
    id 629
    label "doletni"
  ]
  node [
    id 630
    label "doro&#347;le"
  ]
  node [
    id 631
    label "punkt"
  ]
  node [
    id 632
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 633
    label "turn"
  ]
  node [
    id 634
    label "wyra&#380;enie"
  ]
  node [
    id 635
    label "fraza_czasownikowa"
  ]
  node [
    id 636
    label "turning"
  ]
  node [
    id 637
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 638
    label "skr&#281;t"
  ]
  node [
    id 639
    label "jednostka_leksykalna"
  ]
  node [
    id 640
    label "obr&#243;t"
  ]
  node [
    id 641
    label "starosta"
  ]
  node [
    id 642
    label "w&#322;adca"
  ]
  node [
    id 643
    label "zarz&#261;dca"
  ]
  node [
    id 644
    label "nauczyciel"
  ]
  node [
    id 645
    label "autor"
  ]
  node [
    id 646
    label "szko&#322;a"
  ]
  node [
    id 647
    label "tarcza"
  ]
  node [
    id 648
    label "klasa"
  ]
  node [
    id 649
    label "elew"
  ]
  node [
    id 650
    label "mundurek"
  ]
  node [
    id 651
    label "absolwent"
  ]
  node [
    id 652
    label "wyprawka"
  ]
  node [
    id 653
    label "nauczyciel_akademicki"
  ]
  node [
    id 654
    label "tytu&#322;"
  ]
  node [
    id 655
    label "stopie&#324;_naukowy"
  ]
  node [
    id 656
    label "konsulent"
  ]
  node [
    id 657
    label "profesura"
  ]
  node [
    id 658
    label "wirtuoz"
  ]
  node [
    id 659
    label "ochotnik"
  ]
  node [
    id 660
    label "nauczyciel_muzyki"
  ]
  node [
    id 661
    label "pomocnik"
  ]
  node [
    id 662
    label "zakonnik"
  ]
  node [
    id 663
    label "student"
  ]
  node [
    id 664
    label "ekspert"
  ]
  node [
    id 665
    label "bogacz"
  ]
  node [
    id 666
    label "dostojnik"
  ]
  node [
    id 667
    label "urz&#281;dnik"
  ]
  node [
    id 668
    label "mo&#347;&#263;"
  ]
  node [
    id 669
    label "&#347;w"
  ]
  node [
    id 670
    label "rodzic"
  ]
  node [
    id 671
    label "pomys&#322;odawca"
  ]
  node [
    id 672
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 673
    label "rodzice"
  ]
  node [
    id 674
    label "wykonawca"
  ]
  node [
    id 675
    label "kuwada"
  ]
  node [
    id 676
    label "ojczym"
  ]
  node [
    id 677
    label "papa"
  ]
  node [
    id 678
    label "przodek"
  ]
  node [
    id 679
    label "tworzyciel"
  ]
  node [
    id 680
    label "zwierz&#281;"
  ]
  node [
    id 681
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 682
    label "facet"
  ]
  node [
    id 683
    label "fio&#322;ek"
  ]
  node [
    id 684
    label "pan_i_w&#322;adca"
  ]
  node [
    id 685
    label "pan_m&#322;ody"
  ]
  node [
    id 686
    label "ch&#322;op"
  ]
  node [
    id 687
    label "&#347;lubny"
  ]
  node [
    id 688
    label "m&#243;j"
  ]
  node [
    id 689
    label "pan_domu"
  ]
  node [
    id 690
    label "ma&#322;&#380;onek"
  ]
  node [
    id 691
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 692
    label "Frygia"
  ]
  node [
    id 693
    label "dominowanie"
  ]
  node [
    id 694
    label "reign"
  ]
  node [
    id 695
    label "sprawowanie"
  ]
  node [
    id 696
    label "dominion"
  ]
  node [
    id 697
    label "rule"
  ]
  node [
    id 698
    label "zwierz&#281;_domowe"
  ]
  node [
    id 699
    label "John_Dewey"
  ]
  node [
    id 700
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 701
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 702
    label "J&#281;drzejewicz"
  ]
  node [
    id 703
    label "specjalista"
  ]
  node [
    id 704
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 705
    label "Turek"
  ]
  node [
    id 706
    label "effendi"
  ]
  node [
    id 707
    label "och&#281;do&#380;ny"
  ]
  node [
    id 708
    label "zapa&#347;ny"
  ]
  node [
    id 709
    label "sytuowany"
  ]
  node [
    id 710
    label "obfituj&#261;cy"
  ]
  node [
    id 711
    label "forsiasty"
  ]
  node [
    id 712
    label "spania&#322;y"
  ]
  node [
    id 713
    label "obficie"
  ]
  node [
    id 714
    label "r&#243;&#380;norodny"
  ]
  node [
    id 715
    label "bogato"
  ]
  node [
    id 716
    label "Japonia"
  ]
  node [
    id 717
    label "Zair"
  ]
  node [
    id 718
    label "Belize"
  ]
  node [
    id 719
    label "San_Marino"
  ]
  node [
    id 720
    label "Tanzania"
  ]
  node [
    id 721
    label "Antigua_i_Barbuda"
  ]
  node [
    id 722
    label "granica_pa&#324;stwa"
  ]
  node [
    id 723
    label "Senegal"
  ]
  node [
    id 724
    label "Seszele"
  ]
  node [
    id 725
    label "Mauretania"
  ]
  node [
    id 726
    label "Indie"
  ]
  node [
    id 727
    label "Filipiny"
  ]
  node [
    id 728
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 729
    label "Zimbabwe"
  ]
  node [
    id 730
    label "Malezja"
  ]
  node [
    id 731
    label "Rumunia"
  ]
  node [
    id 732
    label "Surinam"
  ]
  node [
    id 733
    label "Ukraina"
  ]
  node [
    id 734
    label "Syria"
  ]
  node [
    id 735
    label "Wyspy_Marshalla"
  ]
  node [
    id 736
    label "Burkina_Faso"
  ]
  node [
    id 737
    label "Grecja"
  ]
  node [
    id 738
    label "Polska"
  ]
  node [
    id 739
    label "Wenezuela"
  ]
  node [
    id 740
    label "Suazi"
  ]
  node [
    id 741
    label "Nepal"
  ]
  node [
    id 742
    label "S&#322;owacja"
  ]
  node [
    id 743
    label "Algieria"
  ]
  node [
    id 744
    label "Chiny"
  ]
  node [
    id 745
    label "Grenada"
  ]
  node [
    id 746
    label "Barbados"
  ]
  node [
    id 747
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 748
    label "Pakistan"
  ]
  node [
    id 749
    label "Niemcy"
  ]
  node [
    id 750
    label "Bahrajn"
  ]
  node [
    id 751
    label "Komory"
  ]
  node [
    id 752
    label "Australia"
  ]
  node [
    id 753
    label "Rodezja"
  ]
  node [
    id 754
    label "Malawi"
  ]
  node [
    id 755
    label "Gwinea"
  ]
  node [
    id 756
    label "Wehrlen"
  ]
  node [
    id 757
    label "Meksyk"
  ]
  node [
    id 758
    label "Liechtenstein"
  ]
  node [
    id 759
    label "Czarnog&#243;ra"
  ]
  node [
    id 760
    label "Wielka_Brytania"
  ]
  node [
    id 761
    label "Kuwejt"
  ]
  node [
    id 762
    label "Monako"
  ]
  node [
    id 763
    label "Angola"
  ]
  node [
    id 764
    label "Jemen"
  ]
  node [
    id 765
    label "Etiopia"
  ]
  node [
    id 766
    label "Madagaskar"
  ]
  node [
    id 767
    label "terytorium"
  ]
  node [
    id 768
    label "Kolumbia"
  ]
  node [
    id 769
    label "Portoryko"
  ]
  node [
    id 770
    label "Mauritius"
  ]
  node [
    id 771
    label "Kostaryka"
  ]
  node [
    id 772
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 773
    label "Tajlandia"
  ]
  node [
    id 774
    label "Argentyna"
  ]
  node [
    id 775
    label "Zambia"
  ]
  node [
    id 776
    label "Sri_Lanka"
  ]
  node [
    id 777
    label "Gwatemala"
  ]
  node [
    id 778
    label "Kirgistan"
  ]
  node [
    id 779
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 780
    label "Hiszpania"
  ]
  node [
    id 781
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 782
    label "Salwador"
  ]
  node [
    id 783
    label "Korea"
  ]
  node [
    id 784
    label "Macedonia"
  ]
  node [
    id 785
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 786
    label "Brunei"
  ]
  node [
    id 787
    label "Mozambik"
  ]
  node [
    id 788
    label "Turcja"
  ]
  node [
    id 789
    label "Kambod&#380;a"
  ]
  node [
    id 790
    label "Benin"
  ]
  node [
    id 791
    label "Bhutan"
  ]
  node [
    id 792
    label "Tunezja"
  ]
  node [
    id 793
    label "Austria"
  ]
  node [
    id 794
    label "Izrael"
  ]
  node [
    id 795
    label "Sierra_Leone"
  ]
  node [
    id 796
    label "Jamajka"
  ]
  node [
    id 797
    label "Rosja"
  ]
  node [
    id 798
    label "Rwanda"
  ]
  node [
    id 799
    label "holoarktyka"
  ]
  node [
    id 800
    label "Nigeria"
  ]
  node [
    id 801
    label "USA"
  ]
  node [
    id 802
    label "Oman"
  ]
  node [
    id 803
    label "Luksemburg"
  ]
  node [
    id 804
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 805
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 806
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 807
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 808
    label "Dominikana"
  ]
  node [
    id 809
    label "Irlandia"
  ]
  node [
    id 810
    label "Liban"
  ]
  node [
    id 811
    label "Hanower"
  ]
  node [
    id 812
    label "Estonia"
  ]
  node [
    id 813
    label "Iran"
  ]
  node [
    id 814
    label "Nowa_Zelandia"
  ]
  node [
    id 815
    label "Gabon"
  ]
  node [
    id 816
    label "Samoa"
  ]
  node [
    id 817
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 818
    label "S&#322;owenia"
  ]
  node [
    id 819
    label "Kiribati"
  ]
  node [
    id 820
    label "Egipt"
  ]
  node [
    id 821
    label "Togo"
  ]
  node [
    id 822
    label "Mongolia"
  ]
  node [
    id 823
    label "Sudan"
  ]
  node [
    id 824
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 825
    label "Bahamy"
  ]
  node [
    id 826
    label "Bangladesz"
  ]
  node [
    id 827
    label "partia"
  ]
  node [
    id 828
    label "Serbia"
  ]
  node [
    id 829
    label "Czechy"
  ]
  node [
    id 830
    label "Holandia"
  ]
  node [
    id 831
    label "Birma"
  ]
  node [
    id 832
    label "Albania"
  ]
  node [
    id 833
    label "Mikronezja"
  ]
  node [
    id 834
    label "Gambia"
  ]
  node [
    id 835
    label "Kazachstan"
  ]
  node [
    id 836
    label "interior"
  ]
  node [
    id 837
    label "Uzbekistan"
  ]
  node [
    id 838
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 839
    label "Malta"
  ]
  node [
    id 840
    label "Lesoto"
  ]
  node [
    id 841
    label "para"
  ]
  node [
    id 842
    label "Antarktis"
  ]
  node [
    id 843
    label "Andora"
  ]
  node [
    id 844
    label "Nauru"
  ]
  node [
    id 845
    label "Kuba"
  ]
  node [
    id 846
    label "Wietnam"
  ]
  node [
    id 847
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 848
    label "ziemia"
  ]
  node [
    id 849
    label "Kamerun"
  ]
  node [
    id 850
    label "Chorwacja"
  ]
  node [
    id 851
    label "Urugwaj"
  ]
  node [
    id 852
    label "Niger"
  ]
  node [
    id 853
    label "Turkmenistan"
  ]
  node [
    id 854
    label "Szwajcaria"
  ]
  node [
    id 855
    label "Palau"
  ]
  node [
    id 856
    label "Litwa"
  ]
  node [
    id 857
    label "Gruzja"
  ]
  node [
    id 858
    label "Tajwan"
  ]
  node [
    id 859
    label "Kongo"
  ]
  node [
    id 860
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 861
    label "Honduras"
  ]
  node [
    id 862
    label "Boliwia"
  ]
  node [
    id 863
    label "Uganda"
  ]
  node [
    id 864
    label "Namibia"
  ]
  node [
    id 865
    label "Azerbejd&#380;an"
  ]
  node [
    id 866
    label "Erytrea"
  ]
  node [
    id 867
    label "Gujana"
  ]
  node [
    id 868
    label "Panama"
  ]
  node [
    id 869
    label "Somalia"
  ]
  node [
    id 870
    label "Burundi"
  ]
  node [
    id 871
    label "Tuwalu"
  ]
  node [
    id 872
    label "Libia"
  ]
  node [
    id 873
    label "Katar"
  ]
  node [
    id 874
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 875
    label "Sahara_Zachodnia"
  ]
  node [
    id 876
    label "Trynidad_i_Tobago"
  ]
  node [
    id 877
    label "Gwinea_Bissau"
  ]
  node [
    id 878
    label "Bu&#322;garia"
  ]
  node [
    id 879
    label "Fid&#380;i"
  ]
  node [
    id 880
    label "Nikaragua"
  ]
  node [
    id 881
    label "Tonga"
  ]
  node [
    id 882
    label "Timor_Wschodni"
  ]
  node [
    id 883
    label "Laos"
  ]
  node [
    id 884
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 885
    label "Ghana"
  ]
  node [
    id 886
    label "Brazylia"
  ]
  node [
    id 887
    label "Belgia"
  ]
  node [
    id 888
    label "Irak"
  ]
  node [
    id 889
    label "Peru"
  ]
  node [
    id 890
    label "Arabia_Saudyjska"
  ]
  node [
    id 891
    label "Indonezja"
  ]
  node [
    id 892
    label "Malediwy"
  ]
  node [
    id 893
    label "Afganistan"
  ]
  node [
    id 894
    label "Jordania"
  ]
  node [
    id 895
    label "Kenia"
  ]
  node [
    id 896
    label "Czad"
  ]
  node [
    id 897
    label "Liberia"
  ]
  node [
    id 898
    label "W&#281;gry"
  ]
  node [
    id 899
    label "Chile"
  ]
  node [
    id 900
    label "Mali"
  ]
  node [
    id 901
    label "Armenia"
  ]
  node [
    id 902
    label "Kanada"
  ]
  node [
    id 903
    label "Cypr"
  ]
  node [
    id 904
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 905
    label "Ekwador"
  ]
  node [
    id 906
    label "Mo&#322;dawia"
  ]
  node [
    id 907
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 908
    label "W&#322;ochy"
  ]
  node [
    id 909
    label "Wyspy_Salomona"
  ]
  node [
    id 910
    label "&#321;otwa"
  ]
  node [
    id 911
    label "D&#380;ibuti"
  ]
  node [
    id 912
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 913
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 914
    label "Portugalia"
  ]
  node [
    id 915
    label "Botswana"
  ]
  node [
    id 916
    label "Maroko"
  ]
  node [
    id 917
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 918
    label "Francja"
  ]
  node [
    id 919
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 920
    label "Dominika"
  ]
  node [
    id 921
    label "Paragwaj"
  ]
  node [
    id 922
    label "Tad&#380;ykistan"
  ]
  node [
    id 923
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 924
    label "Haiti"
  ]
  node [
    id 925
    label "Khitai"
  ]
  node [
    id 926
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 927
    label "ucze&#324;"
  ]
  node [
    id 928
    label "chor&#261;&#380;y"
  ]
  node [
    id 929
    label "g&#322;osiciel"
  ]
  node [
    id 930
    label "ptak"
  ]
  node [
    id 931
    label "ska&#322;owrony"
  ]
  node [
    id 932
    label "rozsiewca"
  ]
  node [
    id 933
    label "Judasz"
  ]
  node [
    id 934
    label "tuba"
  ]
  node [
    id 935
    label "kontynuator"
  ]
  node [
    id 936
    label "czeladnik"
  ]
  node [
    id 937
    label "praktykant"
  ]
  node [
    id 938
    label "rzemie&#347;lnik"
  ]
  node [
    id 939
    label "bird"
  ]
  node [
    id 940
    label "roz&#263;wierkanie_si&#281;"
  ]
  node [
    id 941
    label "hukni&#281;cie"
  ]
  node [
    id 942
    label "pi&#243;ro"
  ]
  node [
    id 943
    label "grzebie&#324;"
  ]
  node [
    id 944
    label "upierzenie"
  ]
  node [
    id 945
    label "skok"
  ]
  node [
    id 946
    label "zaklekotanie"
  ]
  node [
    id 947
    label "ptaki"
  ]
  node [
    id 948
    label "owodniowiec"
  ]
  node [
    id 949
    label "kloaka"
  ]
  node [
    id 950
    label "dzi&#243;bni&#281;cie"
  ]
  node [
    id 951
    label "kuper"
  ]
  node [
    id 952
    label "ko&#347;&#263;_potyliczna"
  ]
  node [
    id 953
    label "wysiadywa&#263;"
  ]
  node [
    id 954
    label "dziobn&#261;&#263;"
  ]
  node [
    id 955
    label "ptactwo"
  ]
  node [
    id 956
    label "ptasz&#281;"
  ]
  node [
    id 957
    label "dziobni&#281;cie"
  ]
  node [
    id 958
    label "dzi&#243;b"
  ]
  node [
    id 959
    label "dziobanie"
  ]
  node [
    id 960
    label "skrzyd&#322;o"
  ]
  node [
    id 961
    label "pogwizdywanie"
  ]
  node [
    id 962
    label "dzioba&#263;"
  ]
  node [
    id 963
    label "dzi&#243;bn&#261;&#263;"
  ]
  node [
    id 964
    label "wysiedzie&#263;"
  ]
  node [
    id 965
    label "&#347;wiadek_Jehowy"
  ]
  node [
    id 966
    label "kaznodzieja"
  ]
  node [
    id 967
    label "poczet_sztandarowy"
  ]
  node [
    id 968
    label "podoficer"
  ]
  node [
    id 969
    label "luzak"
  ]
  node [
    id 970
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 971
    label "&#380;o&#322;nierz"
  ]
  node [
    id 972
    label "oficer"
  ]
  node [
    id 973
    label "ziemianin"
  ]
  node [
    id 974
    label "odznaczenie"
  ]
  node [
    id 975
    label "rulon"
  ]
  node [
    id 976
    label "opakowanie"
  ]
  node [
    id 977
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 978
    label "sukienka"
  ]
  node [
    id 979
    label "wzmacniacz"
  ]
  node [
    id 980
    label "horn"
  ]
  node [
    id 981
    label "rura"
  ]
  node [
    id 982
    label "kszta&#322;t"
  ]
  node [
    id 983
    label "&#347;piewaj&#261;ce"
  ]
  node [
    id 984
    label "spotkanie"
  ]
  node [
    id 985
    label "koszyk&#243;wka"
  ]
  node [
    id 986
    label "kwestowanie"
  ]
  node [
    id 987
    label "recoil"
  ]
  node [
    id 988
    label "kwestarz"
  ]
  node [
    id 989
    label "apel"
  ]
  node [
    id 990
    label "collection"
  ]
  node [
    id 991
    label "chwyt"
  ]
  node [
    id 992
    label "match"
  ]
  node [
    id 993
    label "spotkanie_si&#281;"
  ]
  node [
    id 994
    label "gather"
  ]
  node [
    id 995
    label "spowodowanie"
  ]
  node [
    id 996
    label "zawarcie"
  ]
  node [
    id 997
    label "po&#380;egnanie"
  ]
  node [
    id 998
    label "spotykanie"
  ]
  node [
    id 999
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 1000
    label "gathering"
  ]
  node [
    id 1001
    label "powitanie"
  ]
  node [
    id 1002
    label "doznanie"
  ]
  node [
    id 1003
    label "znalezienie"
  ]
  node [
    id 1004
    label "employment"
  ]
  node [
    id 1005
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 1006
    label "znajomy"
  ]
  node [
    id 1007
    label "strategia"
  ]
  node [
    id 1008
    label "uchwyt"
  ]
  node [
    id 1009
    label "uj&#281;cie"
  ]
  node [
    id 1010
    label "zabieg"
  ]
  node [
    id 1011
    label "zacisk"
  ]
  node [
    id 1012
    label "wolontariusz"
  ]
  node [
    id 1013
    label "uczestnik"
  ]
  node [
    id 1014
    label "zakon_&#380;ebraczy"
  ]
  node [
    id 1015
    label "uczestniczenie"
  ]
  node [
    id 1016
    label "uderzenie"
  ]
  node [
    id 1017
    label "szermierka"
  ]
  node [
    id 1018
    label "pies_my&#347;liwski"
  ]
  node [
    id 1019
    label "bid"
  ]
  node [
    id 1020
    label "zdyscyplinowanie"
  ]
  node [
    id 1021
    label "sygna&#322;"
  ]
  node [
    id 1022
    label "pro&#347;ba"
  ]
  node [
    id 1023
    label "znak"
  ]
  node [
    id 1024
    label "koz&#322;owa&#263;"
  ]
  node [
    id 1025
    label "koz&#322;owanie"
  ]
  node [
    id 1026
    label "wsad"
  ]
  node [
    id 1027
    label "kosz"
  ]
  node [
    id 1028
    label "przelobowa&#263;"
  ]
  node [
    id 1029
    label "pi&#322;ka"
  ]
  node [
    id 1030
    label "strefa_podkoszowa"
  ]
  node [
    id 1031
    label "kroki"
  ]
  node [
    id 1032
    label "przelobowanie"
  ]
  node [
    id 1033
    label "dwutakt"
  ]
  node [
    id 1034
    label "miara_tendencji_centralnej"
  ]
  node [
    id 1035
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 1036
    label "miesi&#261;c"
  ]
  node [
    id 1037
    label "tydzie&#324;"
  ]
  node [
    id 1038
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 1039
    label "rok"
  ]
  node [
    id 1040
    label "miech"
  ]
  node [
    id 1041
    label "kalendy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 10
    target 273
  ]
  edge [
    source 10
    target 274
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 276
  ]
  edge [
    source 10
    target 277
  ]
  edge [
    source 10
    target 278
  ]
  edge [
    source 10
    target 279
  ]
  edge [
    source 10
    target 280
  ]
  edge [
    source 10
    target 281
  ]
  edge [
    source 10
    target 282
  ]
  edge [
    source 10
    target 283
  ]
  edge [
    source 10
    target 284
  ]
  edge [
    source 10
    target 285
  ]
  edge [
    source 10
    target 286
  ]
  edge [
    source 10
    target 287
  ]
  edge [
    source 10
    target 288
  ]
  edge [
    source 10
    target 289
  ]
  edge [
    source 10
    target 290
  ]
  edge [
    source 10
    target 291
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 53
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 469
  ]
  edge [
    source 11
    target 470
  ]
  edge [
    source 11
    target 471
  ]
  edge [
    source 11
    target 472
  ]
  edge [
    source 11
    target 473
  ]
  edge [
    source 11
    target 474
  ]
  edge [
    source 11
    target 475
  ]
  edge [
    source 11
    target 476
  ]
  edge [
    source 11
    target 477
  ]
  edge [
    source 11
    target 478
  ]
  edge [
    source 11
    target 479
  ]
  edge [
    source 11
    target 480
  ]
  edge [
    source 11
    target 481
  ]
  edge [
    source 11
    target 482
  ]
  edge [
    source 11
    target 483
  ]
  edge [
    source 11
    target 484
  ]
  edge [
    source 11
    target 485
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 487
  ]
  edge [
    source 11
    target 488
  ]
  edge [
    source 11
    target 489
  ]
  edge [
    source 11
    target 490
  ]
  edge [
    source 11
    target 491
  ]
  edge [
    source 11
    target 492
  ]
  edge [
    source 11
    target 493
  ]
  edge [
    source 11
    target 494
  ]
  edge [
    source 11
    target 495
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 497
  ]
  edge [
    source 11
    target 498
  ]
  edge [
    source 11
    target 499
  ]
  edge [
    source 11
    target 500
  ]
  edge [
    source 11
    target 501
  ]
  edge [
    source 11
    target 502
  ]
  edge [
    source 11
    target 503
  ]
  edge [
    source 11
    target 504
  ]
  edge [
    source 11
    target 505
  ]
  edge [
    source 11
    target 506
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 20
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 477
  ]
  edge [
    source 12
    target 478
  ]
  edge [
    source 12
    target 479
  ]
  edge [
    source 12
    target 480
  ]
  edge [
    source 12
    target 481
  ]
  edge [
    source 12
    target 482
  ]
  edge [
    source 12
    target 483
  ]
  edge [
    source 12
    target 484
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 486
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 488
  ]
  edge [
    source 12
    target 489
  ]
  edge [
    source 12
    target 490
  ]
  edge [
    source 12
    target 43
  ]
  edge [
    source 12
    target 507
  ]
  edge [
    source 12
    target 508
  ]
  edge [
    source 12
    target 509
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 12
    target 510
  ]
  edge [
    source 12
    target 47
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 511
  ]
  edge [
    source 12
    target 512
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 513
  ]
  edge [
    source 12
    target 514
  ]
  edge [
    source 12
    target 515
  ]
  edge [
    source 12
    target 516
  ]
  edge [
    source 12
    target 517
  ]
  edge [
    source 12
    target 518
  ]
  edge [
    source 12
    target 519
  ]
  edge [
    source 12
    target 520
  ]
  edge [
    source 12
    target 521
  ]
  edge [
    source 12
    target 522
  ]
  edge [
    source 12
    target 523
  ]
  edge [
    source 12
    target 524
  ]
  edge [
    source 12
    target 525
  ]
  edge [
    source 12
    target 526
  ]
  edge [
    source 12
    target 527
  ]
  edge [
    source 12
    target 528
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 529
  ]
  edge [
    source 12
    target 530
  ]
  edge [
    source 12
    target 531
  ]
  edge [
    source 12
    target 532
  ]
  edge [
    source 12
    target 533
  ]
  edge [
    source 12
    target 534
  ]
  edge [
    source 12
    target 535
  ]
  edge [
    source 12
    target 536
  ]
  edge [
    source 12
    target 537
  ]
  edge [
    source 12
    target 538
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 539
  ]
  edge [
    source 12
    target 540
  ]
  edge [
    source 12
    target 541
  ]
  edge [
    source 12
    target 542
  ]
  edge [
    source 12
    target 543
  ]
  edge [
    source 12
    target 544
  ]
  edge [
    source 12
    target 545
  ]
  edge [
    source 12
    target 546
  ]
  edge [
    source 12
    target 547
  ]
  edge [
    source 12
    target 548
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 550
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 552
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 570
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 573
  ]
  edge [
    source 15
    target 574
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 576
  ]
  edge [
    source 15
    target 577
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 578
  ]
  edge [
    source 15
    target 579
  ]
  edge [
    source 15
    target 580
  ]
  edge [
    source 15
    target 581
  ]
  edge [
    source 15
    target 582
  ]
  edge [
    source 15
    target 583
  ]
  edge [
    source 15
    target 584
  ]
  edge [
    source 15
    target 585
  ]
  edge [
    source 15
    target 586
  ]
  edge [
    source 15
    target 587
  ]
  edge [
    source 15
    target 588
  ]
  edge [
    source 15
    target 589
  ]
  edge [
    source 15
    target 590
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 75
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 81
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 602
  ]
  edge [
    source 15
    target 603
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 604
  ]
  edge [
    source 15
    target 605
  ]
  edge [
    source 15
    target 606
  ]
  edge [
    source 15
    target 607
  ]
  edge [
    source 15
    target 608
  ]
  edge [
    source 15
    target 609
  ]
  edge [
    source 15
    target 451
  ]
  edge [
    source 15
    target 610
  ]
  edge [
    source 15
    target 611
  ]
  edge [
    source 15
    target 612
  ]
  edge [
    source 15
    target 613
  ]
  edge [
    source 15
    target 614
  ]
  edge [
    source 15
    target 615
  ]
  edge [
    source 15
    target 616
  ]
  edge [
    source 15
    target 617
  ]
  edge [
    source 15
    target 618
  ]
  edge [
    source 15
    target 619
  ]
  edge [
    source 15
    target 620
  ]
  edge [
    source 15
    target 621
  ]
  edge [
    source 15
    target 622
  ]
  edge [
    source 15
    target 623
  ]
  edge [
    source 15
    target 624
  ]
  edge [
    source 15
    target 625
  ]
  edge [
    source 15
    target 626
  ]
  edge [
    source 15
    target 627
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 628
  ]
  edge [
    source 15
    target 629
  ]
  edge [
    source 15
    target 630
  ]
  edge [
    source 15
    target 631
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 632
  ]
  edge [
    source 15
    target 633
  ]
  edge [
    source 15
    target 634
  ]
  edge [
    source 15
    target 635
  ]
  edge [
    source 15
    target 636
  ]
  edge [
    source 15
    target 637
  ]
  edge [
    source 15
    target 638
  ]
  edge [
    source 15
    target 639
  ]
  edge [
    source 15
    target 640
  ]
  edge [
    source 15
    target 641
  ]
  edge [
    source 15
    target 642
  ]
  edge [
    source 15
    target 643
  ]
  edge [
    source 15
    target 644
  ]
  edge [
    source 15
    target 645
  ]
  edge [
    source 15
    target 646
  ]
  edge [
    source 15
    target 647
  ]
  edge [
    source 15
    target 648
  ]
  edge [
    source 15
    target 649
  ]
  edge [
    source 15
    target 650
  ]
  edge [
    source 15
    target 651
  ]
  edge [
    source 15
    target 652
  ]
  edge [
    source 15
    target 653
  ]
  edge [
    source 15
    target 654
  ]
  edge [
    source 15
    target 655
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 15
    target 657
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 659
  ]
  edge [
    source 15
    target 660
  ]
  edge [
    source 15
    target 661
  ]
  edge [
    source 15
    target 662
  ]
  edge [
    source 15
    target 663
  ]
  edge [
    source 15
    target 664
  ]
  edge [
    source 15
    target 665
  ]
  edge [
    source 15
    target 666
  ]
  edge [
    source 15
    target 667
  ]
  edge [
    source 15
    target 668
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 669
  ]
  edge [
    source 15
    target 670
  ]
  edge [
    source 15
    target 671
  ]
  edge [
    source 15
    target 672
  ]
  edge [
    source 15
    target 673
  ]
  edge [
    source 15
    target 674
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 675
  ]
  edge [
    source 15
    target 676
  ]
  edge [
    source 15
    target 677
  ]
  edge [
    source 15
    target 678
  ]
  edge [
    source 15
    target 679
  ]
  edge [
    source 15
    target 680
  ]
  edge [
    source 15
    target 681
  ]
  edge [
    source 15
    target 682
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 683
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 684
  ]
  edge [
    source 15
    target 685
  ]
  edge [
    source 15
    target 686
  ]
  edge [
    source 15
    target 687
  ]
  edge [
    source 15
    target 688
  ]
  edge [
    source 15
    target 689
  ]
  edge [
    source 15
    target 690
  ]
  edge [
    source 15
    target 691
  ]
  edge [
    source 15
    target 692
  ]
  edge [
    source 15
    target 693
  ]
  edge [
    source 15
    target 694
  ]
  edge [
    source 15
    target 695
  ]
  edge [
    source 15
    target 696
  ]
  edge [
    source 15
    target 697
  ]
  edge [
    source 15
    target 698
  ]
  edge [
    source 15
    target 699
  ]
  edge [
    source 15
    target 700
  ]
  edge [
    source 15
    target 701
  ]
  edge [
    source 15
    target 702
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 312
  ]
  edge [
    source 15
    target 704
  ]
  edge [
    source 15
    target 705
  ]
  edge [
    source 15
    target 706
  ]
  edge [
    source 15
    target 707
  ]
  edge [
    source 15
    target 708
  ]
  edge [
    source 15
    target 709
  ]
  edge [
    source 15
    target 710
  ]
  edge [
    source 15
    target 711
  ]
  edge [
    source 15
    target 712
  ]
  edge [
    source 15
    target 713
  ]
  edge [
    source 15
    target 714
  ]
  edge [
    source 15
    target 715
  ]
  edge [
    source 15
    target 716
  ]
  edge [
    source 15
    target 717
  ]
  edge [
    source 15
    target 718
  ]
  edge [
    source 15
    target 719
  ]
  edge [
    source 15
    target 720
  ]
  edge [
    source 15
    target 721
  ]
  edge [
    source 15
    target 722
  ]
  edge [
    source 15
    target 723
  ]
  edge [
    source 15
    target 724
  ]
  edge [
    source 15
    target 725
  ]
  edge [
    source 15
    target 726
  ]
  edge [
    source 15
    target 727
  ]
  edge [
    source 15
    target 728
  ]
  edge [
    source 15
    target 729
  ]
  edge [
    source 15
    target 730
  ]
  edge [
    source 15
    target 731
  ]
  edge [
    source 15
    target 732
  ]
  edge [
    source 15
    target 733
  ]
  edge [
    source 15
    target 734
  ]
  edge [
    source 15
    target 735
  ]
  edge [
    source 15
    target 736
  ]
  edge [
    source 15
    target 737
  ]
  edge [
    source 15
    target 738
  ]
  edge [
    source 15
    target 739
  ]
  edge [
    source 15
    target 740
  ]
  edge [
    source 15
    target 741
  ]
  edge [
    source 15
    target 742
  ]
  edge [
    source 15
    target 743
  ]
  edge [
    source 15
    target 744
  ]
  edge [
    source 15
    target 745
  ]
  edge [
    source 15
    target 746
  ]
  edge [
    source 15
    target 747
  ]
  edge [
    source 15
    target 748
  ]
  edge [
    source 15
    target 749
  ]
  edge [
    source 15
    target 750
  ]
  edge [
    source 15
    target 751
  ]
  edge [
    source 15
    target 752
  ]
  edge [
    source 15
    target 753
  ]
  edge [
    source 15
    target 754
  ]
  edge [
    source 15
    target 755
  ]
  edge [
    source 15
    target 756
  ]
  edge [
    source 15
    target 757
  ]
  edge [
    source 15
    target 758
  ]
  edge [
    source 15
    target 759
  ]
  edge [
    source 15
    target 760
  ]
  edge [
    source 15
    target 761
  ]
  edge [
    source 15
    target 762
  ]
  edge [
    source 15
    target 763
  ]
  edge [
    source 15
    target 764
  ]
  edge [
    source 15
    target 765
  ]
  edge [
    source 15
    target 766
  ]
  edge [
    source 15
    target 767
  ]
  edge [
    source 15
    target 768
  ]
  edge [
    source 15
    target 769
  ]
  edge [
    source 15
    target 770
  ]
  edge [
    source 15
    target 771
  ]
  edge [
    source 15
    target 772
  ]
  edge [
    source 15
    target 773
  ]
  edge [
    source 15
    target 774
  ]
  edge [
    source 15
    target 775
  ]
  edge [
    source 15
    target 776
  ]
  edge [
    source 15
    target 777
  ]
  edge [
    source 15
    target 778
  ]
  edge [
    source 15
    target 779
  ]
  edge [
    source 15
    target 780
  ]
  edge [
    source 15
    target 781
  ]
  edge [
    source 15
    target 782
  ]
  edge [
    source 15
    target 783
  ]
  edge [
    source 15
    target 784
  ]
  edge [
    source 15
    target 785
  ]
  edge [
    source 15
    target 786
  ]
  edge [
    source 15
    target 787
  ]
  edge [
    source 15
    target 788
  ]
  edge [
    source 15
    target 789
  ]
  edge [
    source 15
    target 790
  ]
  edge [
    source 15
    target 791
  ]
  edge [
    source 15
    target 792
  ]
  edge [
    source 15
    target 793
  ]
  edge [
    source 15
    target 794
  ]
  edge [
    source 15
    target 795
  ]
  edge [
    source 15
    target 796
  ]
  edge [
    source 15
    target 797
  ]
  edge [
    source 15
    target 798
  ]
  edge [
    source 15
    target 799
  ]
  edge [
    source 15
    target 800
  ]
  edge [
    source 15
    target 801
  ]
  edge [
    source 15
    target 802
  ]
  edge [
    source 15
    target 803
  ]
  edge [
    source 15
    target 804
  ]
  edge [
    source 15
    target 805
  ]
  edge [
    source 15
    target 806
  ]
  edge [
    source 15
    target 807
  ]
  edge [
    source 15
    target 808
  ]
  edge [
    source 15
    target 809
  ]
  edge [
    source 15
    target 810
  ]
  edge [
    source 15
    target 811
  ]
  edge [
    source 15
    target 812
  ]
  edge [
    source 15
    target 813
  ]
  edge [
    source 15
    target 814
  ]
  edge [
    source 15
    target 815
  ]
  edge [
    source 15
    target 816
  ]
  edge [
    source 15
    target 817
  ]
  edge [
    source 15
    target 818
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 820
  ]
  edge [
    source 15
    target 821
  ]
  edge [
    source 15
    target 822
  ]
  edge [
    source 15
    target 823
  ]
  edge [
    source 15
    target 824
  ]
  edge [
    source 15
    target 825
  ]
  edge [
    source 15
    target 826
  ]
  edge [
    source 15
    target 827
  ]
  edge [
    source 15
    target 828
  ]
  edge [
    source 15
    target 829
  ]
  edge [
    source 15
    target 830
  ]
  edge [
    source 15
    target 831
  ]
  edge [
    source 15
    target 832
  ]
  edge [
    source 15
    target 833
  ]
  edge [
    source 15
    target 834
  ]
  edge [
    source 15
    target 835
  ]
  edge [
    source 15
    target 836
  ]
  edge [
    source 15
    target 837
  ]
  edge [
    source 15
    target 838
  ]
  edge [
    source 15
    target 839
  ]
  edge [
    source 15
    target 840
  ]
  edge [
    source 15
    target 841
  ]
  edge [
    source 15
    target 842
  ]
  edge [
    source 15
    target 843
  ]
  edge [
    source 15
    target 844
  ]
  edge [
    source 15
    target 845
  ]
  edge [
    source 15
    target 846
  ]
  edge [
    source 15
    target 847
  ]
  edge [
    source 15
    target 848
  ]
  edge [
    source 15
    target 849
  ]
  edge [
    source 15
    target 850
  ]
  edge [
    source 15
    target 851
  ]
  edge [
    source 15
    target 852
  ]
  edge [
    source 15
    target 853
  ]
  edge [
    source 15
    target 854
  ]
  edge [
    source 15
    target 38
  ]
  edge [
    source 15
    target 39
  ]
  edge [
    source 15
    target 855
  ]
  edge [
    source 15
    target 856
  ]
  edge [
    source 15
    target 857
  ]
  edge [
    source 15
    target 858
  ]
  edge [
    source 15
    target 859
  ]
  edge [
    source 15
    target 860
  ]
  edge [
    source 15
    target 861
  ]
  edge [
    source 15
    target 862
  ]
  edge [
    source 15
    target 863
  ]
  edge [
    source 15
    target 864
  ]
  edge [
    source 15
    target 865
  ]
  edge [
    source 15
    target 866
  ]
  edge [
    source 15
    target 867
  ]
  edge [
    source 15
    target 868
  ]
  edge [
    source 15
    target 869
  ]
  edge [
    source 15
    target 870
  ]
  edge [
    source 15
    target 871
  ]
  edge [
    source 15
    target 872
  ]
  edge [
    source 15
    target 873
  ]
  edge [
    source 15
    target 874
  ]
  edge [
    source 15
    target 875
  ]
  edge [
    source 15
    target 876
  ]
  edge [
    source 15
    target 877
  ]
  edge [
    source 15
    target 878
  ]
  edge [
    source 15
    target 879
  ]
  edge [
    source 15
    target 880
  ]
  edge [
    source 15
    target 881
  ]
  edge [
    source 15
    target 882
  ]
  edge [
    source 15
    target 883
  ]
  edge [
    source 15
    target 884
  ]
  edge [
    source 15
    target 885
  ]
  edge [
    source 15
    target 886
  ]
  edge [
    source 15
    target 887
  ]
  edge [
    source 15
    target 888
  ]
  edge [
    source 15
    target 889
  ]
  edge [
    source 15
    target 890
  ]
  edge [
    source 15
    target 891
  ]
  edge [
    source 15
    target 892
  ]
  edge [
    source 15
    target 893
  ]
  edge [
    source 15
    target 894
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 896
  ]
  edge [
    source 15
    target 897
  ]
  edge [
    source 15
    target 898
  ]
  edge [
    source 15
    target 899
  ]
  edge [
    source 15
    target 900
  ]
  edge [
    source 15
    target 901
  ]
  edge [
    source 15
    target 902
  ]
  edge [
    source 15
    target 903
  ]
  edge [
    source 15
    target 904
  ]
  edge [
    source 15
    target 905
  ]
  edge [
    source 15
    target 906
  ]
  edge [
    source 15
    target 907
  ]
  edge [
    source 15
    target 908
  ]
  edge [
    source 15
    target 909
  ]
  edge [
    source 15
    target 910
  ]
  edge [
    source 15
    target 911
  ]
  edge [
    source 15
    target 912
  ]
  edge [
    source 15
    target 521
  ]
  edge [
    source 15
    target 913
  ]
  edge [
    source 15
    target 914
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 916
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 918
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 920
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 922
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 927
  ]
  edge [
    source 16
    target 669
  ]
  edge [
    source 16
    target 928
  ]
  edge [
    source 16
    target 929
  ]
  edge [
    source 16
    target 930
  ]
  edge [
    source 16
    target 931
  ]
  edge [
    source 16
    target 574
  ]
  edge [
    source 16
    target 932
  ]
  edge [
    source 16
    target 933
  ]
  edge [
    source 16
    target 934
  ]
  edge [
    source 16
    target 646
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 935
  ]
  edge [
    source 16
    target 647
  ]
  edge [
    source 16
    target 936
  ]
  edge [
    source 16
    target 648
  ]
  edge [
    source 16
    target 649
  ]
  edge [
    source 16
    target 937
  ]
  edge [
    source 16
    target 650
  ]
  edge [
    source 16
    target 651
  ]
  edge [
    source 16
    target 938
  ]
  edge [
    source 16
    target 652
  ]
  edge [
    source 16
    target 599
  ]
  edge [
    source 16
    target 939
  ]
  edge [
    source 16
    target 940
  ]
  edge [
    source 16
    target 941
  ]
  edge [
    source 16
    target 942
  ]
  edge [
    source 16
    target 943
  ]
  edge [
    source 16
    target 944
  ]
  edge [
    source 16
    target 945
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 947
  ]
  edge [
    source 16
    target 948
  ]
  edge [
    source 16
    target 949
  ]
  edge [
    source 16
    target 950
  ]
  edge [
    source 16
    target 951
  ]
  edge [
    source 16
    target 952
  ]
  edge [
    source 16
    target 953
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 654
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 667
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 16
    target 975
  ]
  edge [
    source 16
    target 976
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 16
    target 981
  ]
  edge [
    source 16
    target 982
  ]
  edge [
    source 16
    target 983
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 984
  ]
  edge [
    source 19
    target 985
  ]
  edge [
    source 19
    target 986
  ]
  edge [
    source 19
    target 987
  ]
  edge [
    source 19
    target 988
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 989
  ]
  edge [
    source 19
    target 990
  ]
  edge [
    source 19
    target 991
  ]
  edge [
    source 19
    target 992
  ]
  edge [
    source 19
    target 993
  ]
  edge [
    source 19
    target 994
  ]
  edge [
    source 19
    target 995
  ]
  edge [
    source 19
    target 996
  ]
  edge [
    source 19
    target 466
  ]
  edge [
    source 19
    target 997
  ]
  edge [
    source 19
    target 998
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 378
  ]
  edge [
    source 19
    target 82
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 662
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 1017
  ]
  edge [
    source 19
    target 1018
  ]
  edge [
    source 19
    target 1019
  ]
  edge [
    source 19
    target 1020
  ]
  edge [
    source 19
    target 1021
  ]
  edge [
    source 19
    target 1022
  ]
  edge [
    source 19
    target 1023
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1025
  ]
  edge [
    source 19
    target 1026
  ]
  edge [
    source 19
    target 1027
  ]
  edge [
    source 19
    target 1028
  ]
  edge [
    source 19
    target 1029
  ]
  edge [
    source 19
    target 1030
  ]
  edge [
    source 19
    target 1031
  ]
  edge [
    source 19
    target 1032
  ]
  edge [
    source 19
    target 1033
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1034
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1035
  ]
  edge [
    source 23
    target 1036
  ]
  edge [
    source 23
    target 1037
  ]
  edge [
    source 23
    target 1038
  ]
  edge [
    source 23
    target 1039
  ]
  edge [
    source 23
    target 1040
  ]
  edge [
    source 23
    target 1041
  ]
  edge [
    source 23
    target 311
  ]
]
