graph [
  node [
    id 0
    label "eksperymentowa&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 2
    label "kubizm"
    origin "text"
  ]
  node [
    id 3
    label "brzydota"
    origin "text"
  ]
  node [
    id 4
    label "szko&#322;a"
  ]
  node [
    id 5
    label "cubism"
  ]
  node [
    id 6
    label "do&#347;wiadczenie"
  ]
  node [
    id 7
    label "teren_szko&#322;y"
  ]
  node [
    id 8
    label "wiedza"
  ]
  node [
    id 9
    label "Mickiewicz"
  ]
  node [
    id 10
    label "kwalifikacje"
  ]
  node [
    id 11
    label "podr&#281;cznik"
  ]
  node [
    id 12
    label "absolwent"
  ]
  node [
    id 13
    label "praktyka"
  ]
  node [
    id 14
    label "school"
  ]
  node [
    id 15
    label "system"
  ]
  node [
    id 16
    label "zda&#263;"
  ]
  node [
    id 17
    label "gabinet"
  ]
  node [
    id 18
    label "urszulanki"
  ]
  node [
    id 19
    label "sztuba"
  ]
  node [
    id 20
    label "&#322;awa_szkolna"
  ]
  node [
    id 21
    label "nauka"
  ]
  node [
    id 22
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 23
    label "przepisa&#263;"
  ]
  node [
    id 24
    label "muzyka"
  ]
  node [
    id 25
    label "grupa"
  ]
  node [
    id 26
    label "form"
  ]
  node [
    id 27
    label "klasa"
  ]
  node [
    id 28
    label "lekcja"
  ]
  node [
    id 29
    label "metoda"
  ]
  node [
    id 30
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 31
    label "przepisanie"
  ]
  node [
    id 32
    label "czas"
  ]
  node [
    id 33
    label "skolaryzacja"
  ]
  node [
    id 34
    label "zdanie"
  ]
  node [
    id 35
    label "stopek"
  ]
  node [
    id 36
    label "sekretariat"
  ]
  node [
    id 37
    label "ideologia"
  ]
  node [
    id 38
    label "lesson"
  ]
  node [
    id 39
    label "instytucja"
  ]
  node [
    id 40
    label "niepokalanki"
  ]
  node [
    id 41
    label "siedziba"
  ]
  node [
    id 42
    label "szkolenie"
  ]
  node [
    id 43
    label "kara"
  ]
  node [
    id 44
    label "tablica"
  ]
  node [
    id 45
    label "dysproporcja"
  ]
  node [
    id 46
    label "koszmarek"
  ]
  node [
    id 47
    label "wygl&#261;d"
  ]
  node [
    id 48
    label "dysharmonia"
  ]
  node [
    id 49
    label "szkarada"
  ]
  node [
    id 50
    label "ugliness"
  ]
  node [
    id 51
    label "cecha"
  ]
  node [
    id 52
    label "charakterystyka"
  ]
  node [
    id 53
    label "m&#322;ot"
  ]
  node [
    id 54
    label "znak"
  ]
  node [
    id 55
    label "drzewo"
  ]
  node [
    id 56
    label "pr&#243;ba"
  ]
  node [
    id 57
    label "attribute"
  ]
  node [
    id 58
    label "marka"
  ]
  node [
    id 59
    label "owad_wodny"
  ]
  node [
    id 60
    label "chru&#347;cik"
  ]
  node [
    id 61
    label "pierzekowate"
  ]
  node [
    id 62
    label "rzecz"
  ]
  node [
    id 63
    label "r&#243;&#380;nica"
  ]
  node [
    id 64
    label "diafonia"
  ]
  node [
    id 65
    label "wsp&#243;&#322;brzmienie"
  ]
  node [
    id 66
    label "discordance"
  ]
  node [
    id 67
    label "sound"
  ]
  node [
    id 68
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 69
    label "disagreement"
  ]
  node [
    id 70
    label "brzydal"
  ]
  node [
    id 71
    label "postarzenie"
  ]
  node [
    id 72
    label "kszta&#322;t"
  ]
  node [
    id 73
    label "postarzanie"
  ]
  node [
    id 74
    label "portrecista"
  ]
  node [
    id 75
    label "postarza&#263;"
  ]
  node [
    id 76
    label "nadawanie"
  ]
  node [
    id 77
    label "postarzy&#263;"
  ]
  node [
    id 78
    label "widok"
  ]
  node [
    id 79
    label "prostota"
  ]
  node [
    id 80
    label "ubarwienie"
  ]
  node [
    id 81
    label "shape"
  ]
  node [
    id 82
    label "par"
  ]
  node [
    id 83
    label "pi&#281;kny"
  ]
  node [
    id 84
    label "kochankowie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 84
  ]
  edge [
    source 83
    target 84
  ]
]
