graph [
  node [
    id 0
    label "spotkanie"
    origin "text"
  ]
  node [
    id 1
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 2
    label "doznanie"
  ]
  node [
    id 3
    label "gathering"
  ]
  node [
    id 4
    label "zawarcie"
  ]
  node [
    id 5
    label "wydarzenie"
  ]
  node [
    id 6
    label "znajomy"
  ]
  node [
    id 7
    label "powitanie"
  ]
  node [
    id 8
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 9
    label "spowodowanie"
  ]
  node [
    id 10
    label "zdarzenie_si&#281;"
  ]
  node [
    id 11
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 12
    label "znalezienie"
  ]
  node [
    id 13
    label "match"
  ]
  node [
    id 14
    label "employment"
  ]
  node [
    id 15
    label "po&#380;egnanie"
  ]
  node [
    id 16
    label "gather"
  ]
  node [
    id 17
    label "spotykanie"
  ]
  node [
    id 18
    label "spotkanie_si&#281;"
  ]
  node [
    id 19
    label "dzianie_si&#281;"
  ]
  node [
    id 20
    label "zaznawanie"
  ]
  node [
    id 21
    label "znajdowanie"
  ]
  node [
    id 22
    label "zdarzanie_si&#281;"
  ]
  node [
    id 23
    label "merging"
  ]
  node [
    id 24
    label "meeting"
  ]
  node [
    id 25
    label "zawieranie"
  ]
  node [
    id 26
    label "czynno&#347;&#263;"
  ]
  node [
    id 27
    label "campaign"
  ]
  node [
    id 28
    label "causing"
  ]
  node [
    id 29
    label "przebiec"
  ]
  node [
    id 30
    label "charakter"
  ]
  node [
    id 31
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 32
    label "motyw"
  ]
  node [
    id 33
    label "przebiegni&#281;cie"
  ]
  node [
    id 34
    label "fabu&#322;a"
  ]
  node [
    id 35
    label "postaranie_si&#281;"
  ]
  node [
    id 36
    label "discovery"
  ]
  node [
    id 37
    label "wymy&#347;lenie"
  ]
  node [
    id 38
    label "determination"
  ]
  node [
    id 39
    label "dorwanie"
  ]
  node [
    id 40
    label "znalezienie_si&#281;"
  ]
  node [
    id 41
    label "wykrycie"
  ]
  node [
    id 42
    label "poszukanie"
  ]
  node [
    id 43
    label "invention"
  ]
  node [
    id 44
    label "pozyskanie"
  ]
  node [
    id 45
    label "zmieszczenie"
  ]
  node [
    id 46
    label "umawianie_si&#281;"
  ]
  node [
    id 47
    label "zapoznanie"
  ]
  node [
    id 48
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 49
    label "zapoznanie_si&#281;"
  ]
  node [
    id 50
    label "ustalenie"
  ]
  node [
    id 51
    label "dissolution"
  ]
  node [
    id 52
    label "przyskrzynienie"
  ]
  node [
    id 53
    label "uk&#322;ad"
  ]
  node [
    id 54
    label "pozamykanie"
  ]
  node [
    id 55
    label "inclusion"
  ]
  node [
    id 56
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 57
    label "uchwalenie"
  ]
  node [
    id 58
    label "umowa"
  ]
  node [
    id 59
    label "zrobienie"
  ]
  node [
    id 60
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 61
    label "wy&#347;wiadczenie"
  ]
  node [
    id 62
    label "zmys&#322;"
  ]
  node [
    id 63
    label "przeczulica"
  ]
  node [
    id 64
    label "czucie"
  ]
  node [
    id 65
    label "poczucie"
  ]
  node [
    id 66
    label "znany"
  ]
  node [
    id 67
    label "sw&#243;j"
  ]
  node [
    id 68
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 69
    label "znajomek"
  ]
  node [
    id 70
    label "zapoznawanie"
  ]
  node [
    id 71
    label "znajomo"
  ]
  node [
    id 72
    label "pewien"
  ]
  node [
    id 73
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 74
    label "przyj&#281;ty"
  ]
  node [
    id 75
    label "za_pan_brat"
  ]
  node [
    id 76
    label "rozstanie_si&#281;"
  ]
  node [
    id 77
    label "adieu"
  ]
  node [
    id 78
    label "pozdrowienie"
  ]
  node [
    id 79
    label "zwyczaj"
  ]
  node [
    id 80
    label "farewell"
  ]
  node [
    id 81
    label "welcome"
  ]
  node [
    id 82
    label "greeting"
  ]
  node [
    id 83
    label "wyra&#380;enie"
  ]
  node [
    id 84
    label "miesi&#261;c"
  ]
  node [
    id 85
    label "tydzie&#324;"
  ]
  node [
    id 86
    label "miech"
  ]
  node [
    id 87
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 88
    label "czas"
  ]
  node [
    id 89
    label "rok"
  ]
  node [
    id 90
    label "kalendy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
]
