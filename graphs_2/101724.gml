graph [
  node [
    id 0
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "liczba"
    origin "text"
  ]
  node [
    id 2
    label "student"
    origin "text"
  ]
  node [
    id 3
    label "studia"
    origin "text"
  ]
  node [
    id 4
    label "stacjonarny"
    origin "text"
  ]
  node [
    id 5
    label "wy&#322;&#261;czenie"
    origin "text"
  ]
  node [
    id 6
    label "osoba"
    origin "text"
  ]
  node [
    id 7
    label "ostatni"
    origin "text"
  ]
  node [
    id 8
    label "rok"
    origin "text"
  ]
  node [
    id 9
    label "bez"
    origin "text"
  ]
  node [
    id 10
    label "egzamin"
    origin "text"
  ]
  node [
    id 11
    label "dyplomowy"
    origin "text"
  ]
  node [
    id 12
    label "ten"
    origin "text"
  ]
  node [
    id 13
    label "grupa"
    origin "text"
  ]
  node [
    id 14
    label "kierunek"
    origin "text"
  ]
  node [
    id 15
    label "uczelnia"
    origin "text"
  ]
  node [
    id 16
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 17
    label "dana"
    origin "text"
  ]
  node [
    id 18
    label "nades&#322;a&#263;"
    origin "text"
  ]
  node [
    id 19
    label "przez"
    origin "text"
  ]
  node [
    id 20
    label "set"
  ]
  node [
    id 21
    label "by&#263;"
  ]
  node [
    id 22
    label "wyraz"
  ]
  node [
    id 23
    label "wskazywa&#263;"
  ]
  node [
    id 24
    label "signify"
  ]
  node [
    id 25
    label "represent"
  ]
  node [
    id 26
    label "ustala&#263;"
  ]
  node [
    id 27
    label "stanowi&#263;"
  ]
  node [
    id 28
    label "okre&#347;la&#263;"
  ]
  node [
    id 29
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 30
    label "mie&#263;_miejsce"
  ]
  node [
    id 31
    label "equal"
  ]
  node [
    id 32
    label "trwa&#263;"
  ]
  node [
    id 33
    label "chodzi&#263;"
  ]
  node [
    id 34
    label "si&#281;ga&#263;"
  ]
  node [
    id 35
    label "stan"
  ]
  node [
    id 36
    label "obecno&#347;&#263;"
  ]
  node [
    id 37
    label "stand"
  ]
  node [
    id 38
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 39
    label "uczestniczy&#263;"
  ]
  node [
    id 40
    label "warto&#347;&#263;"
  ]
  node [
    id 41
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 42
    label "podkre&#347;la&#263;"
  ]
  node [
    id 43
    label "podawa&#263;"
  ]
  node [
    id 44
    label "pokazywa&#263;"
  ]
  node [
    id 45
    label "wybiera&#263;"
  ]
  node [
    id 46
    label "indicate"
  ]
  node [
    id 47
    label "decydowa&#263;"
  ]
  node [
    id 48
    label "style"
  ]
  node [
    id 49
    label "powodowa&#263;"
  ]
  node [
    id 50
    label "robi&#263;"
  ]
  node [
    id 51
    label "peddle"
  ]
  node [
    id 52
    label "unwrap"
  ]
  node [
    id 53
    label "zmienia&#263;"
  ]
  node [
    id 54
    label "umacnia&#263;"
  ]
  node [
    id 55
    label "arrange"
  ]
  node [
    id 56
    label "decide"
  ]
  node [
    id 57
    label "pies_my&#347;liwski"
  ]
  node [
    id 58
    label "zatrzymywa&#263;"
  ]
  node [
    id 59
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 60
    label "typify"
  ]
  node [
    id 61
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 62
    label "gem"
  ]
  node [
    id 63
    label "kompozycja"
  ]
  node [
    id 64
    label "runda"
  ]
  node [
    id 65
    label "muzyka"
  ]
  node [
    id 66
    label "zestaw"
  ]
  node [
    id 67
    label "term"
  ]
  node [
    id 68
    label "oznaka"
  ]
  node [
    id 69
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 70
    label "leksem"
  ]
  node [
    id 71
    label "posta&#263;"
  ]
  node [
    id 72
    label "element"
  ]
  node [
    id 73
    label "cecha"
  ]
  node [
    id 74
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 75
    label "&#347;wiadczenie"
  ]
  node [
    id 76
    label "kategoria"
  ]
  node [
    id 77
    label "pierwiastek"
  ]
  node [
    id 78
    label "rozmiar"
  ]
  node [
    id 79
    label "wyra&#380;enie"
  ]
  node [
    id 80
    label "poj&#281;cie"
  ]
  node [
    id 81
    label "number"
  ]
  node [
    id 82
    label "kategoria_gramatyczna"
  ]
  node [
    id 83
    label "kwadrat_magiczny"
  ]
  node [
    id 84
    label "koniugacja"
  ]
  node [
    id 85
    label "odm&#322;adzanie"
  ]
  node [
    id 86
    label "liga"
  ]
  node [
    id 87
    label "jednostka_systematyczna"
  ]
  node [
    id 88
    label "asymilowanie"
  ]
  node [
    id 89
    label "gromada"
  ]
  node [
    id 90
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 91
    label "asymilowa&#263;"
  ]
  node [
    id 92
    label "egzemplarz"
  ]
  node [
    id 93
    label "Entuzjastki"
  ]
  node [
    id 94
    label "zbi&#243;r"
  ]
  node [
    id 95
    label "Terranie"
  ]
  node [
    id 96
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 97
    label "category"
  ]
  node [
    id 98
    label "pakiet_klimatyczny"
  ]
  node [
    id 99
    label "oddzia&#322;"
  ]
  node [
    id 100
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 101
    label "cz&#261;steczka"
  ]
  node [
    id 102
    label "stage_set"
  ]
  node [
    id 103
    label "type"
  ]
  node [
    id 104
    label "specgrupa"
  ]
  node [
    id 105
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 106
    label "&#346;wietliki"
  ]
  node [
    id 107
    label "odm&#322;odzenie"
  ]
  node [
    id 108
    label "Eurogrupa"
  ]
  node [
    id 109
    label "odm&#322;adza&#263;"
  ]
  node [
    id 110
    label "formacja_geologiczna"
  ]
  node [
    id 111
    label "harcerze_starsi"
  ]
  node [
    id 112
    label "wytw&#243;r"
  ]
  node [
    id 113
    label "teoria"
  ]
  node [
    id 114
    label "forma"
  ]
  node [
    id 115
    label "klasa"
  ]
  node [
    id 116
    label "charakterystyka"
  ]
  node [
    id 117
    label "m&#322;ot"
  ]
  node [
    id 118
    label "znak"
  ]
  node [
    id 119
    label "drzewo"
  ]
  node [
    id 120
    label "pr&#243;ba"
  ]
  node [
    id 121
    label "attribute"
  ]
  node [
    id 122
    label "marka"
  ]
  node [
    id 123
    label "pos&#322;uchanie"
  ]
  node [
    id 124
    label "skumanie"
  ]
  node [
    id 125
    label "orientacja"
  ]
  node [
    id 126
    label "zorientowanie"
  ]
  node [
    id 127
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 128
    label "clasp"
  ]
  node [
    id 129
    label "przem&#243;wienie"
  ]
  node [
    id 130
    label "warunek_lokalowy"
  ]
  node [
    id 131
    label "circumference"
  ]
  node [
    id 132
    label "odzie&#380;"
  ]
  node [
    id 133
    label "ilo&#347;&#263;"
  ]
  node [
    id 134
    label "znaczenie"
  ]
  node [
    id 135
    label "dymensja"
  ]
  node [
    id 136
    label "fleksja"
  ]
  node [
    id 137
    label "coupling"
  ]
  node [
    id 138
    label "tryb"
  ]
  node [
    id 139
    label "czas"
  ]
  node [
    id 140
    label "czasownik"
  ]
  node [
    id 141
    label "rozmna&#380;anie_p&#322;ciowe"
  ]
  node [
    id 142
    label "orz&#281;sek"
  ]
  node [
    id 143
    label "sformu&#322;owanie"
  ]
  node [
    id 144
    label "zdarzenie_si&#281;"
  ]
  node [
    id 145
    label "poinformowanie"
  ]
  node [
    id 146
    label "wording"
  ]
  node [
    id 147
    label "oznaczenie"
  ]
  node [
    id 148
    label "znak_j&#281;zykowy"
  ]
  node [
    id 149
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 150
    label "ozdobnik"
  ]
  node [
    id 151
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 152
    label "grupa_imienna"
  ]
  node [
    id 153
    label "jednostka_leksykalna"
  ]
  node [
    id 154
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 155
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 156
    label "ujawnienie"
  ]
  node [
    id 157
    label "affirmation"
  ]
  node [
    id 158
    label "zapisanie"
  ]
  node [
    id 159
    label "rzucenie"
  ]
  node [
    id 160
    label "substancja_chemiczna"
  ]
  node [
    id 161
    label "morfem"
  ]
  node [
    id 162
    label "sk&#322;adnik"
  ]
  node [
    id 163
    label "root"
  ]
  node [
    id 164
    label "indeks"
  ]
  node [
    id 165
    label "s&#322;uchacz"
  ]
  node [
    id 166
    label "immatrykulowanie"
  ]
  node [
    id 167
    label "absolwent"
  ]
  node [
    id 168
    label "immatrykulowa&#263;"
  ]
  node [
    id 169
    label "akademik"
  ]
  node [
    id 170
    label "tutor"
  ]
  node [
    id 171
    label "odbiorca"
  ]
  node [
    id 172
    label "cz&#322;owiek"
  ]
  node [
    id 173
    label "pracownik_naukowy"
  ]
  node [
    id 174
    label "akademia"
  ]
  node [
    id 175
    label "cz&#322;onek"
  ]
  node [
    id 176
    label "przedstawiciel"
  ]
  node [
    id 177
    label "artysta"
  ]
  node [
    id 178
    label "dom"
  ]
  node [
    id 179
    label "reprezentant"
  ]
  node [
    id 180
    label "znak_pisarski"
  ]
  node [
    id 181
    label "directory"
  ]
  node [
    id 182
    label "wska&#378;nik"
  ]
  node [
    id 183
    label "za&#347;wiadczenie"
  ]
  node [
    id 184
    label "indeks_Lernera"
  ]
  node [
    id 185
    label "spis"
  ]
  node [
    id 186
    label "album"
  ]
  node [
    id 187
    label "ucze&#324;"
  ]
  node [
    id 188
    label "szko&#322;a"
  ]
  node [
    id 189
    label "zapisywanie"
  ]
  node [
    id 190
    label "zapisywa&#263;"
  ]
  node [
    id 191
    label "zapisa&#263;"
  ]
  node [
    id 192
    label "nauczyciel"
  ]
  node [
    id 193
    label "nauczyciel_akademicki"
  ]
  node [
    id 194
    label "opiekun"
  ]
  node [
    id 195
    label "wychowawca"
  ]
  node [
    id 196
    label "mentor"
  ]
  node [
    id 197
    label "badanie"
  ]
  node [
    id 198
    label "nauka"
  ]
  node [
    id 199
    label "obserwowanie"
  ]
  node [
    id 200
    label "zrecenzowanie"
  ]
  node [
    id 201
    label "kontrola"
  ]
  node [
    id 202
    label "analysis"
  ]
  node [
    id 203
    label "rektalny"
  ]
  node [
    id 204
    label "ustalenie"
  ]
  node [
    id 205
    label "macanie"
  ]
  node [
    id 206
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 207
    label "usi&#322;owanie"
  ]
  node [
    id 208
    label "udowadnianie"
  ]
  node [
    id 209
    label "praca"
  ]
  node [
    id 210
    label "bia&#322;a_niedziela"
  ]
  node [
    id 211
    label "diagnostyka"
  ]
  node [
    id 212
    label "dociekanie"
  ]
  node [
    id 213
    label "rezultat"
  ]
  node [
    id 214
    label "sprawdzanie"
  ]
  node [
    id 215
    label "penetrowanie"
  ]
  node [
    id 216
    label "czynno&#347;&#263;"
  ]
  node [
    id 217
    label "krytykowanie"
  ]
  node [
    id 218
    label "omawianie"
  ]
  node [
    id 219
    label "ustalanie"
  ]
  node [
    id 220
    label "rozpatrywanie"
  ]
  node [
    id 221
    label "investigation"
  ]
  node [
    id 222
    label "wziernikowanie"
  ]
  node [
    id 223
    label "examination"
  ]
  node [
    id 224
    label "wiedza"
  ]
  node [
    id 225
    label "miasteczko_rowerowe"
  ]
  node [
    id 226
    label "porada"
  ]
  node [
    id 227
    label "fotowoltaika"
  ]
  node [
    id 228
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 229
    label "nauki_o_poznaniu"
  ]
  node [
    id 230
    label "nomotetyczny"
  ]
  node [
    id 231
    label "systematyka"
  ]
  node [
    id 232
    label "proces"
  ]
  node [
    id 233
    label "typologia"
  ]
  node [
    id 234
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 235
    label "kultura_duchowa"
  ]
  node [
    id 236
    label "&#322;awa_szkolna"
  ]
  node [
    id 237
    label "nauki_penalne"
  ]
  node [
    id 238
    label "dziedzina"
  ]
  node [
    id 239
    label "imagineskopia"
  ]
  node [
    id 240
    label "teoria_naukowa"
  ]
  node [
    id 241
    label "inwentyka"
  ]
  node [
    id 242
    label "metodologia"
  ]
  node [
    id 243
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 244
    label "nauki_o_Ziemi"
  ]
  node [
    id 245
    label "stacjonarnie"
  ]
  node [
    id 246
    label "nieruchomy"
  ]
  node [
    id 247
    label "cutoff"
  ]
  node [
    id 248
    label "wydzielenie"
  ]
  node [
    id 249
    label "debarment"
  ]
  node [
    id 250
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 251
    label "przerwanie"
  ]
  node [
    id 252
    label "nieprawdopodobie&#324;stwo"
  ]
  node [
    id 253
    label "odci&#281;cie"
  ]
  node [
    id 254
    label "closure"
  ]
  node [
    id 255
    label "wykluczenie"
  ]
  node [
    id 256
    label "release"
  ]
  node [
    id 257
    label "odrzucenie"
  ]
  node [
    id 258
    label "gotowanie"
  ]
  node [
    id 259
    label "przygaszenie"
  ]
  node [
    id 260
    label "od&#322;&#261;czenie"
  ]
  node [
    id 261
    label "w&#322;&#261;czenie"
  ]
  node [
    id 262
    label "zatrzymanie"
  ]
  node [
    id 263
    label "przestanie"
  ]
  node [
    id 264
    label "odseparowanie"
  ]
  node [
    id 265
    label "cut"
  ]
  node [
    id 266
    label "zamkni&#281;cie"
  ]
  node [
    id 267
    label "&#347;ci&#281;cie"
  ]
  node [
    id 268
    label "oddzielenie"
  ]
  node [
    id 269
    label "uniemo&#380;liwienie"
  ]
  node [
    id 270
    label "snub"
  ]
  node [
    id 271
    label "oddalenie"
  ]
  node [
    id 272
    label "defense"
  ]
  node [
    id 273
    label "odrzucanie"
  ]
  node [
    id 274
    label "rejection"
  ]
  node [
    id 275
    label "usuni&#281;cie"
  ]
  node [
    id 276
    label "spowodowanie"
  ]
  node [
    id 277
    label "elimination"
  ]
  node [
    id 278
    label "repudiation"
  ]
  node [
    id 279
    label "oddanie"
  ]
  node [
    id 280
    label "repulsion"
  ]
  node [
    id 281
    label "zrobienie"
  ]
  node [
    id 282
    label "powodowanie"
  ]
  node [
    id 283
    label "przefiltrowanie"
  ]
  node [
    id 284
    label "career"
  ]
  node [
    id 285
    label "zaaresztowanie"
  ]
  node [
    id 286
    label "przechowanie"
  ]
  node [
    id 287
    label "observation"
  ]
  node [
    id 288
    label "funkcjonowanie"
  ]
  node [
    id 289
    label "pochowanie"
  ]
  node [
    id 290
    label "discontinuance"
  ]
  node [
    id 291
    label "zaczepienie"
  ]
  node [
    id 292
    label "pozajmowanie"
  ]
  node [
    id 293
    label "hipostaza"
  ]
  node [
    id 294
    label "capture"
  ]
  node [
    id 295
    label "przetrzymanie"
  ]
  node [
    id 296
    label "oddzia&#322;anie"
  ]
  node [
    id 297
    label "&#322;apanie"
  ]
  node [
    id 298
    label "z&#322;apanie"
  ]
  node [
    id 299
    label "check"
  ]
  node [
    id 300
    label "unieruchomienie"
  ]
  node [
    id 301
    label "zabranie"
  ]
  node [
    id 302
    label "porozrywanie"
  ]
  node [
    id 303
    label "przerywa&#263;"
  ]
  node [
    id 304
    label "rozerwanie"
  ]
  node [
    id 305
    label "poprzerywanie"
  ]
  node [
    id 306
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 307
    label "severance"
  ]
  node [
    id 308
    label "przeszkodzenie"
  ]
  node [
    id 309
    label "wstrzymanie"
  ]
  node [
    id 310
    label "przerywanie"
  ]
  node [
    id 311
    label "przerzedzenie"
  ]
  node [
    id 312
    label "przerwa&#263;"
  ]
  node [
    id 313
    label "odpoczywanie"
  ]
  node [
    id 314
    label "przedziurawienie"
  ]
  node [
    id 315
    label "wada_wrodzona"
  ]
  node [
    id 316
    label "urwanie"
  ]
  node [
    id 317
    label "kultywar"
  ]
  node [
    id 318
    label "clang"
  ]
  node [
    id 319
    label "pacjent"
  ]
  node [
    id 320
    label "od&#322;&#261;czony"
  ]
  node [
    id 321
    label "odbicie"
  ]
  node [
    id 322
    label "ablation"
  ]
  node [
    id 323
    label "wykrojenie"
  ]
  node [
    id 324
    label "przydzielenie"
  ]
  node [
    id 325
    label "wyznaczenie"
  ]
  node [
    id 326
    label "division"
  ]
  node [
    id 327
    label "wytworzenie"
  ]
  node [
    id 328
    label "rozdzielenie"
  ]
  node [
    id 329
    label "osobny"
  ]
  node [
    id 330
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 331
    label "oduczenie"
  ]
  node [
    id 332
    label "disavowal"
  ]
  node [
    id 333
    label "zako&#324;czenie"
  ]
  node [
    id 334
    label "cessation"
  ]
  node [
    id 335
    label "przeczekanie"
  ]
  node [
    id 336
    label "impossibility"
  ]
  node [
    id 337
    label "wyklucza&#263;"
  ]
  node [
    id 338
    label "wykluczy&#263;"
  ]
  node [
    id 339
    label "wydarzenie"
  ]
  node [
    id 340
    label "rzadko&#347;&#263;"
  ]
  node [
    id 341
    label "wykluczanie"
  ]
  node [
    id 342
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 343
    label "obejrzenie"
  ]
  node [
    id 344
    label "involvement"
  ]
  node [
    id 345
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 346
    label "za&#347;wiecenie"
  ]
  node [
    id 347
    label "nastawienie"
  ]
  node [
    id 348
    label "uruchomienie"
  ]
  node [
    id 349
    label "zacz&#281;cie"
  ]
  node [
    id 350
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 351
    label "selekcja"
  ]
  node [
    id 352
    label "czyn"
  ]
  node [
    id 353
    label "izolacja"
  ]
  node [
    id 354
    label "wrz&#261;tek"
  ]
  node [
    id 355
    label "wygotowywanie"
  ]
  node [
    id 356
    label "cooking"
  ]
  node [
    id 357
    label "wy&#322;&#261;czanie"
  ]
  node [
    id 358
    label "po_kucharsku"
  ]
  node [
    id 359
    label "nagotowanie_si&#281;"
  ]
  node [
    id 360
    label "boiling"
  ]
  node [
    id 361
    label "rozgotowywanie"
  ]
  node [
    id 362
    label "oddzia&#322;ywanie"
  ]
  node [
    id 363
    label "przyrz&#261;dzanie"
  ]
  node [
    id 364
    label "rozgotowanie"
  ]
  node [
    id 365
    label "przygotowywanie"
  ]
  node [
    id 366
    label "os&#322;abienie"
  ]
  node [
    id 367
    label "zmniejszenie"
  ]
  node [
    id 368
    label "&#347;wiat&#322;o"
  ]
  node [
    id 369
    label "przewy&#380;szenie"
  ]
  node [
    id 370
    label "przy&#263;miony"
  ]
  node [
    id 371
    label "mystification"
  ]
  node [
    id 372
    label "Chocho&#322;"
  ]
  node [
    id 373
    label "Herkules_Poirot"
  ]
  node [
    id 374
    label "Edyp"
  ]
  node [
    id 375
    label "ludzko&#347;&#263;"
  ]
  node [
    id 376
    label "parali&#380;owa&#263;"
  ]
  node [
    id 377
    label "Harry_Potter"
  ]
  node [
    id 378
    label "Casanova"
  ]
  node [
    id 379
    label "Gargantua"
  ]
  node [
    id 380
    label "Zgredek"
  ]
  node [
    id 381
    label "Winnetou"
  ]
  node [
    id 382
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 383
    label "Dulcynea"
  ]
  node [
    id 384
    label "g&#322;owa"
  ]
  node [
    id 385
    label "figura"
  ]
  node [
    id 386
    label "portrecista"
  ]
  node [
    id 387
    label "person"
  ]
  node [
    id 388
    label "Sherlock_Holmes"
  ]
  node [
    id 389
    label "Quasimodo"
  ]
  node [
    id 390
    label "Plastu&#347;"
  ]
  node [
    id 391
    label "Faust"
  ]
  node [
    id 392
    label "Wallenrod"
  ]
  node [
    id 393
    label "Dwukwiat"
  ]
  node [
    id 394
    label "profanum"
  ]
  node [
    id 395
    label "Don_Juan"
  ]
  node [
    id 396
    label "Don_Kiszot"
  ]
  node [
    id 397
    label "mikrokosmos"
  ]
  node [
    id 398
    label "duch"
  ]
  node [
    id 399
    label "antropochoria"
  ]
  node [
    id 400
    label "Hamlet"
  ]
  node [
    id 401
    label "Werter"
  ]
  node [
    id 402
    label "istota"
  ]
  node [
    id 403
    label "Szwejk"
  ]
  node [
    id 404
    label "homo_sapiens"
  ]
  node [
    id 405
    label "mentalno&#347;&#263;"
  ]
  node [
    id 406
    label "superego"
  ]
  node [
    id 407
    label "psychika"
  ]
  node [
    id 408
    label "wn&#281;trze"
  ]
  node [
    id 409
    label "charakter"
  ]
  node [
    id 410
    label "zaistnie&#263;"
  ]
  node [
    id 411
    label "Osjan"
  ]
  node [
    id 412
    label "kto&#347;"
  ]
  node [
    id 413
    label "wygl&#261;d"
  ]
  node [
    id 414
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 415
    label "osobowo&#347;&#263;"
  ]
  node [
    id 416
    label "trim"
  ]
  node [
    id 417
    label "poby&#263;"
  ]
  node [
    id 418
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 419
    label "Aspazja"
  ]
  node [
    id 420
    label "punkt_widzenia"
  ]
  node [
    id 421
    label "kompleksja"
  ]
  node [
    id 422
    label "wytrzyma&#263;"
  ]
  node [
    id 423
    label "budowa"
  ]
  node [
    id 424
    label "formacja"
  ]
  node [
    id 425
    label "pozosta&#263;"
  ]
  node [
    id 426
    label "point"
  ]
  node [
    id 427
    label "przedstawienie"
  ]
  node [
    id 428
    label "go&#347;&#263;"
  ]
  node [
    id 429
    label "hamper"
  ]
  node [
    id 430
    label "spasm"
  ]
  node [
    id 431
    label "mrozi&#263;"
  ]
  node [
    id 432
    label "pora&#380;a&#263;"
  ]
  node [
    id 433
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 434
    label "pryncypa&#322;"
  ]
  node [
    id 435
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 436
    label "kszta&#322;t"
  ]
  node [
    id 437
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 438
    label "kierowa&#263;"
  ]
  node [
    id 439
    label "alkohol"
  ]
  node [
    id 440
    label "zdolno&#347;&#263;"
  ]
  node [
    id 441
    label "&#380;ycie"
  ]
  node [
    id 442
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 443
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 444
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 445
    label "sztuka"
  ]
  node [
    id 446
    label "dekiel"
  ]
  node [
    id 447
    label "ro&#347;lina"
  ]
  node [
    id 448
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 449
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 450
    label "&#347;ci&#281;gno"
  ]
  node [
    id 451
    label "noosfera"
  ]
  node [
    id 452
    label "byd&#322;o"
  ]
  node [
    id 453
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 454
    label "makrocefalia"
  ]
  node [
    id 455
    label "obiekt"
  ]
  node [
    id 456
    label "ucho"
  ]
  node [
    id 457
    label "g&#243;ra"
  ]
  node [
    id 458
    label "m&#243;zg"
  ]
  node [
    id 459
    label "kierownictwo"
  ]
  node [
    id 460
    label "fryzura"
  ]
  node [
    id 461
    label "umys&#322;"
  ]
  node [
    id 462
    label "cia&#322;o"
  ]
  node [
    id 463
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 464
    label "czaszka"
  ]
  node [
    id 465
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 466
    label "hipnotyzowanie"
  ]
  node [
    id 467
    label "&#347;lad"
  ]
  node [
    id 468
    label "docieranie"
  ]
  node [
    id 469
    label "natural_process"
  ]
  node [
    id 470
    label "reakcja_chemiczna"
  ]
  node [
    id 471
    label "wdzieranie_si&#281;"
  ]
  node [
    id 472
    label "zjawisko"
  ]
  node [
    id 473
    label "act"
  ]
  node [
    id 474
    label "lobbysta"
  ]
  node [
    id 475
    label "allochoria"
  ]
  node [
    id 476
    label "fotograf"
  ]
  node [
    id 477
    label "malarz"
  ]
  node [
    id 478
    label "p&#322;aszczyzna"
  ]
  node [
    id 479
    label "przedmiot"
  ]
  node [
    id 480
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 481
    label "bierka_szachowa"
  ]
  node [
    id 482
    label "obiekt_matematyczny"
  ]
  node [
    id 483
    label "gestaltyzm"
  ]
  node [
    id 484
    label "styl"
  ]
  node [
    id 485
    label "obraz"
  ]
  node [
    id 486
    label "rzecz"
  ]
  node [
    id 487
    label "d&#378;wi&#281;k"
  ]
  node [
    id 488
    label "character"
  ]
  node [
    id 489
    label "rze&#378;ba"
  ]
  node [
    id 490
    label "stylistyka"
  ]
  node [
    id 491
    label "figure"
  ]
  node [
    id 492
    label "miejsce"
  ]
  node [
    id 493
    label "antycypacja"
  ]
  node [
    id 494
    label "ornamentyka"
  ]
  node [
    id 495
    label "informacja"
  ]
  node [
    id 496
    label "facet"
  ]
  node [
    id 497
    label "popis"
  ]
  node [
    id 498
    label "wiersz"
  ]
  node [
    id 499
    label "symetria"
  ]
  node [
    id 500
    label "lingwistyka_kognitywna"
  ]
  node [
    id 501
    label "karta"
  ]
  node [
    id 502
    label "shape"
  ]
  node [
    id 503
    label "podzbi&#243;r"
  ]
  node [
    id 504
    label "perspektywa"
  ]
  node [
    id 505
    label "Szekspir"
  ]
  node [
    id 506
    label "Mickiewicz"
  ]
  node [
    id 507
    label "cierpienie"
  ]
  node [
    id 508
    label "piek&#322;o"
  ]
  node [
    id 509
    label "human_body"
  ]
  node [
    id 510
    label "ofiarowywanie"
  ]
  node [
    id 511
    label "sfera_afektywna"
  ]
  node [
    id 512
    label "nekromancja"
  ]
  node [
    id 513
    label "Po&#347;wist"
  ]
  node [
    id 514
    label "podekscytowanie"
  ]
  node [
    id 515
    label "deformowanie"
  ]
  node [
    id 516
    label "sumienie"
  ]
  node [
    id 517
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 518
    label "deformowa&#263;"
  ]
  node [
    id 519
    label "zjawa"
  ]
  node [
    id 520
    label "zmar&#322;y"
  ]
  node [
    id 521
    label "istota_nadprzyrodzona"
  ]
  node [
    id 522
    label "power"
  ]
  node [
    id 523
    label "entity"
  ]
  node [
    id 524
    label "ofiarowywa&#263;"
  ]
  node [
    id 525
    label "oddech"
  ]
  node [
    id 526
    label "seksualno&#347;&#263;"
  ]
  node [
    id 527
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 528
    label "byt"
  ]
  node [
    id 529
    label "si&#322;a"
  ]
  node [
    id 530
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 531
    label "ego"
  ]
  node [
    id 532
    label "ofiarowanie"
  ]
  node [
    id 533
    label "fizjonomia"
  ]
  node [
    id 534
    label "kompleks"
  ]
  node [
    id 535
    label "zapalno&#347;&#263;"
  ]
  node [
    id 536
    label "T&#281;sknica"
  ]
  node [
    id 537
    label "ofiarowa&#263;"
  ]
  node [
    id 538
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 539
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 540
    label "passion"
  ]
  node [
    id 541
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 542
    label "atom"
  ]
  node [
    id 543
    label "przyroda"
  ]
  node [
    id 544
    label "Ziemia"
  ]
  node [
    id 545
    label "kosmos"
  ]
  node [
    id 546
    label "miniatura"
  ]
  node [
    id 547
    label "kolejny"
  ]
  node [
    id 548
    label "niedawno"
  ]
  node [
    id 549
    label "poprzedni"
  ]
  node [
    id 550
    label "pozosta&#322;y"
  ]
  node [
    id 551
    label "ostatnio"
  ]
  node [
    id 552
    label "sko&#324;czony"
  ]
  node [
    id 553
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 554
    label "aktualny"
  ]
  node [
    id 555
    label "najgorszy"
  ]
  node [
    id 556
    label "istota_&#380;ywa"
  ]
  node [
    id 557
    label "w&#261;tpliwy"
  ]
  node [
    id 558
    label "nast&#281;pnie"
  ]
  node [
    id 559
    label "inny"
  ]
  node [
    id 560
    label "nastopny"
  ]
  node [
    id 561
    label "kolejno"
  ]
  node [
    id 562
    label "kt&#243;ry&#347;"
  ]
  node [
    id 563
    label "przesz&#322;y"
  ]
  node [
    id 564
    label "wcze&#347;niejszy"
  ]
  node [
    id 565
    label "poprzednio"
  ]
  node [
    id 566
    label "w&#261;tpliwie"
  ]
  node [
    id 567
    label "pozorny"
  ]
  node [
    id 568
    label "&#380;ywy"
  ]
  node [
    id 569
    label "ostateczny"
  ]
  node [
    id 570
    label "wa&#380;ny"
  ]
  node [
    id 571
    label "wapniak"
  ]
  node [
    id 572
    label "os&#322;abia&#263;"
  ]
  node [
    id 573
    label "hominid"
  ]
  node [
    id 574
    label "podw&#322;adny"
  ]
  node [
    id 575
    label "os&#322;abianie"
  ]
  node [
    id 576
    label "dwun&#243;g"
  ]
  node [
    id 577
    label "nasada"
  ]
  node [
    id 578
    label "wz&#243;r"
  ]
  node [
    id 579
    label "senior"
  ]
  node [
    id 580
    label "Adam"
  ]
  node [
    id 581
    label "polifag"
  ]
  node [
    id 582
    label "wykszta&#322;cony"
  ]
  node [
    id 583
    label "dyplomowany"
  ]
  node [
    id 584
    label "wykwalifikowany"
  ]
  node [
    id 585
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 586
    label "kompletny"
  ]
  node [
    id 587
    label "sko&#324;czenie"
  ]
  node [
    id 588
    label "okre&#347;lony"
  ]
  node [
    id 589
    label "wielki"
  ]
  node [
    id 590
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 591
    label "aktualnie"
  ]
  node [
    id 592
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 593
    label "aktualizowanie"
  ]
  node [
    id 594
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 595
    label "uaktualnienie"
  ]
  node [
    id 596
    label "p&#243;&#322;rocze"
  ]
  node [
    id 597
    label "martwy_sezon"
  ]
  node [
    id 598
    label "kalendarz"
  ]
  node [
    id 599
    label "cykl_astronomiczny"
  ]
  node [
    id 600
    label "lata"
  ]
  node [
    id 601
    label "pora_roku"
  ]
  node [
    id 602
    label "stulecie"
  ]
  node [
    id 603
    label "kurs"
  ]
  node [
    id 604
    label "jubileusz"
  ]
  node [
    id 605
    label "kwarta&#322;"
  ]
  node [
    id 606
    label "miesi&#261;c"
  ]
  node [
    id 607
    label "summer"
  ]
  node [
    id 608
    label "poprzedzanie"
  ]
  node [
    id 609
    label "czasoprzestrze&#324;"
  ]
  node [
    id 610
    label "laba"
  ]
  node [
    id 611
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 612
    label "chronometria"
  ]
  node [
    id 613
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 614
    label "rachuba_czasu"
  ]
  node [
    id 615
    label "przep&#322;ywanie"
  ]
  node [
    id 616
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 617
    label "czasokres"
  ]
  node [
    id 618
    label "odczyt"
  ]
  node [
    id 619
    label "chwila"
  ]
  node [
    id 620
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 621
    label "dzieje"
  ]
  node [
    id 622
    label "poprzedzenie"
  ]
  node [
    id 623
    label "trawienie"
  ]
  node [
    id 624
    label "pochodzi&#263;"
  ]
  node [
    id 625
    label "period"
  ]
  node [
    id 626
    label "okres_czasu"
  ]
  node [
    id 627
    label "poprzedza&#263;"
  ]
  node [
    id 628
    label "schy&#322;ek"
  ]
  node [
    id 629
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 630
    label "odwlekanie_si&#281;"
  ]
  node [
    id 631
    label "zegar"
  ]
  node [
    id 632
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 633
    label "czwarty_wymiar"
  ]
  node [
    id 634
    label "pochodzenie"
  ]
  node [
    id 635
    label "Zeitgeist"
  ]
  node [
    id 636
    label "trawi&#263;"
  ]
  node [
    id 637
    label "pogoda"
  ]
  node [
    id 638
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 639
    label "poprzedzi&#263;"
  ]
  node [
    id 640
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 641
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 642
    label "time_period"
  ]
  node [
    id 643
    label "tydzie&#324;"
  ]
  node [
    id 644
    label "miech"
  ]
  node [
    id 645
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 646
    label "kalendy"
  ]
  node [
    id 647
    label "rok_akademicki"
  ]
  node [
    id 648
    label "rok_szkolny"
  ]
  node [
    id 649
    label "semester"
  ]
  node [
    id 650
    label "anniwersarz"
  ]
  node [
    id 651
    label "rocznica"
  ]
  node [
    id 652
    label "obszar"
  ]
  node [
    id 653
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 654
    label "long_time"
  ]
  node [
    id 655
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 656
    label "almanac"
  ]
  node [
    id 657
    label "rozk&#322;ad"
  ]
  node [
    id 658
    label "wydawnictwo"
  ]
  node [
    id 659
    label "Juliusz_Cezar"
  ]
  node [
    id 660
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 661
    label "zwy&#380;kowanie"
  ]
  node [
    id 662
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 663
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 664
    label "zaj&#281;cia"
  ]
  node [
    id 665
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 666
    label "trasa"
  ]
  node [
    id 667
    label "przeorientowywanie"
  ]
  node [
    id 668
    label "przejazd"
  ]
  node [
    id 669
    label "przeorientowywa&#263;"
  ]
  node [
    id 670
    label "przeorientowanie"
  ]
  node [
    id 671
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 672
    label "przeorientowa&#263;"
  ]
  node [
    id 673
    label "manner"
  ]
  node [
    id 674
    label "course"
  ]
  node [
    id 675
    label "passage"
  ]
  node [
    id 676
    label "zni&#380;kowanie"
  ]
  node [
    id 677
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 678
    label "seria"
  ]
  node [
    id 679
    label "stawka"
  ]
  node [
    id 680
    label "way"
  ]
  node [
    id 681
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 682
    label "spos&#243;b"
  ]
  node [
    id 683
    label "deprecjacja"
  ]
  node [
    id 684
    label "cedu&#322;a"
  ]
  node [
    id 685
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 686
    label "drive"
  ]
  node [
    id 687
    label "bearing"
  ]
  node [
    id 688
    label "Lira"
  ]
  node [
    id 689
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 690
    label "krzew"
  ]
  node [
    id 691
    label "delfinidyna"
  ]
  node [
    id 692
    label "pi&#380;maczkowate"
  ]
  node [
    id 693
    label "ki&#347;&#263;"
  ]
  node [
    id 694
    label "hy&#263;ka"
  ]
  node [
    id 695
    label "pestkowiec"
  ]
  node [
    id 696
    label "kwiat"
  ]
  node [
    id 697
    label "owoc"
  ]
  node [
    id 698
    label "oliwkowate"
  ]
  node [
    id 699
    label "lilac"
  ]
  node [
    id 700
    label "kostka"
  ]
  node [
    id 701
    label "kita"
  ]
  node [
    id 702
    label "ko&#347;&#263;_haczykowata"
  ]
  node [
    id 703
    label "ko&#347;&#263;_&#322;&#243;deczkowata"
  ]
  node [
    id 704
    label "d&#322;o&#324;"
  ]
  node [
    id 705
    label "kana&#322;_nadgarstka"
  ]
  node [
    id 706
    label "powerball"
  ]
  node [
    id 707
    label "&#380;ubr"
  ]
  node [
    id 708
    label "ko&#347;&#263;_g&#322;&#243;wkowata"
  ]
  node [
    id 709
    label "p&#281;k"
  ]
  node [
    id 710
    label "r&#281;ka"
  ]
  node [
    id 711
    label "ogon"
  ]
  node [
    id 712
    label "ko&#347;&#263;_grochowata"
  ]
  node [
    id 713
    label "ko&#347;&#263;_ksi&#281;&#380;ycowata"
  ]
  node [
    id 714
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 715
    label "ko&#347;&#263;_tr&#243;jgraniasta"
  ]
  node [
    id 716
    label "flakon"
  ]
  node [
    id 717
    label "przykoronek"
  ]
  node [
    id 718
    label "kielich"
  ]
  node [
    id 719
    label "dno_kwiatowe"
  ]
  node [
    id 720
    label "organ_ro&#347;linny"
  ]
  node [
    id 721
    label "warga"
  ]
  node [
    id 722
    label "korona"
  ]
  node [
    id 723
    label "rurka"
  ]
  node [
    id 724
    label "ozdoba"
  ]
  node [
    id 725
    label "&#322;yko"
  ]
  node [
    id 726
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 727
    label "karczowa&#263;"
  ]
  node [
    id 728
    label "wykarczowanie"
  ]
  node [
    id 729
    label "skupina"
  ]
  node [
    id 730
    label "wykarczowa&#263;"
  ]
  node [
    id 731
    label "karczowanie"
  ]
  node [
    id 732
    label "fanerofit"
  ]
  node [
    id 733
    label "zbiorowisko"
  ]
  node [
    id 734
    label "ro&#347;liny"
  ]
  node [
    id 735
    label "p&#281;d"
  ]
  node [
    id 736
    label "wegetowanie"
  ]
  node [
    id 737
    label "zadziorek"
  ]
  node [
    id 738
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 739
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 740
    label "do&#322;owa&#263;"
  ]
  node [
    id 741
    label "wegetacja"
  ]
  node [
    id 742
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 743
    label "strzyc"
  ]
  node [
    id 744
    label "w&#322;&#243;kno"
  ]
  node [
    id 745
    label "g&#322;uszenie"
  ]
  node [
    id 746
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 747
    label "fitotron"
  ]
  node [
    id 748
    label "bulwka"
  ]
  node [
    id 749
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 750
    label "odn&#243;&#380;ka"
  ]
  node [
    id 751
    label "epiderma"
  ]
  node [
    id 752
    label "gumoza"
  ]
  node [
    id 753
    label "strzy&#380;enie"
  ]
  node [
    id 754
    label "wypotnik"
  ]
  node [
    id 755
    label "flawonoid"
  ]
  node [
    id 756
    label "wyro&#347;le"
  ]
  node [
    id 757
    label "do&#322;owanie"
  ]
  node [
    id 758
    label "g&#322;uszy&#263;"
  ]
  node [
    id 759
    label "fitocenoza"
  ]
  node [
    id 760
    label "hodowla"
  ]
  node [
    id 761
    label "fotoautotrof"
  ]
  node [
    id 762
    label "nieuleczalnie_chory"
  ]
  node [
    id 763
    label "wegetowa&#263;"
  ]
  node [
    id 764
    label "pochewka"
  ]
  node [
    id 765
    label "sok"
  ]
  node [
    id 766
    label "system_korzeniowy"
  ]
  node [
    id 767
    label "zawi&#261;zek"
  ]
  node [
    id 768
    label "pestka"
  ]
  node [
    id 769
    label "mi&#261;&#380;sz"
  ]
  node [
    id 770
    label "frukt"
  ]
  node [
    id 771
    label "drylowanie"
  ]
  node [
    id 772
    label "produkt"
  ]
  node [
    id 773
    label "owocnia"
  ]
  node [
    id 774
    label "fruktoza"
  ]
  node [
    id 775
    label "gniazdo_nasienne"
  ]
  node [
    id 776
    label "glukoza"
  ]
  node [
    id 777
    label "antocyjanidyn"
  ]
  node [
    id 778
    label "szczeciowce"
  ]
  node [
    id 779
    label "jasnotowce"
  ]
  node [
    id 780
    label "Oleaceae"
  ]
  node [
    id 781
    label "wielkopolski"
  ]
  node [
    id 782
    label "bez_czarny"
  ]
  node [
    id 783
    label "oblewanie"
  ]
  node [
    id 784
    label "faza"
  ]
  node [
    id 785
    label "sesja_egzaminacyjna"
  ]
  node [
    id 786
    label "oblewa&#263;"
  ]
  node [
    id 787
    label "praca_pisemna"
  ]
  node [
    id 788
    label "sprawdzian"
  ]
  node [
    id 789
    label "magiel"
  ]
  node [
    id 790
    label "arkusz"
  ]
  node [
    id 791
    label "podchodzi&#263;"
  ]
  node [
    id 792
    label "&#263;wiczenie"
  ]
  node [
    id 793
    label "pytanie"
  ]
  node [
    id 794
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 795
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 796
    label "dydaktyka"
  ]
  node [
    id 797
    label "coil"
  ]
  node [
    id 798
    label "fotoelement"
  ]
  node [
    id 799
    label "komutowanie"
  ]
  node [
    id 800
    label "stan_skupienia"
  ]
  node [
    id 801
    label "nastr&#243;j"
  ]
  node [
    id 802
    label "przerywacz"
  ]
  node [
    id 803
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 804
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 805
    label "kraw&#281;d&#378;"
  ]
  node [
    id 806
    label "obsesja"
  ]
  node [
    id 807
    label "dw&#243;jnik"
  ]
  node [
    id 808
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 809
    label "okres"
  ]
  node [
    id 810
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 811
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 812
    label "przew&#243;d"
  ]
  node [
    id 813
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 814
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 815
    label "obw&#243;d"
  ]
  node [
    id 816
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 817
    label "degree"
  ]
  node [
    id 818
    label "komutowa&#263;"
  ]
  node [
    id 819
    label "do&#347;wiadczenie"
  ]
  node [
    id 820
    label "spotkanie"
  ]
  node [
    id 821
    label "pobiera&#263;"
  ]
  node [
    id 822
    label "metal_szlachetny"
  ]
  node [
    id 823
    label "pobranie"
  ]
  node [
    id 824
    label "pobra&#263;"
  ]
  node [
    id 825
    label "pobieranie"
  ]
  node [
    id 826
    label "effort"
  ]
  node [
    id 827
    label "analiza_chemiczna"
  ]
  node [
    id 828
    label "item"
  ]
  node [
    id 829
    label "sytuacja"
  ]
  node [
    id 830
    label "probiernictwo"
  ]
  node [
    id 831
    label "test"
  ]
  node [
    id 832
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 833
    label "p&#322;at"
  ]
  node [
    id 834
    label "p&#243;&#322;arkusz"
  ]
  node [
    id 835
    label "spill"
  ]
  node [
    id 836
    label "ocenia&#263;"
  ]
  node [
    id 837
    label "moczy&#263;"
  ]
  node [
    id 838
    label "zalewa&#263;"
  ]
  node [
    id 839
    label "op&#322;ywa&#263;"
  ]
  node [
    id 840
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 841
    label "przegrywa&#263;"
  ]
  node [
    id 842
    label "powleka&#263;"
  ]
  node [
    id 843
    label "egzaminowa&#263;"
  ]
  node [
    id 844
    label "glaze"
  ]
  node [
    id 845
    label "la&#263;"
  ]
  node [
    id 846
    label "barwi&#263;"
  ]
  node [
    id 847
    label "p&#322;ukanie"
  ]
  node [
    id 848
    label "egzaminator"
  ]
  node [
    id 849
    label "przegrywanie"
  ]
  node [
    id 850
    label "pokrywanie"
  ]
  node [
    id 851
    label "otaczanie"
  ]
  node [
    id 852
    label "zdawanie"
  ]
  node [
    id 853
    label "&#347;wi&#281;towanie"
  ]
  node [
    id 854
    label "polew"
  ]
  node [
    id 855
    label "powlekanie"
  ]
  node [
    id 856
    label "perfusion"
  ]
  node [
    id 857
    label "lanie"
  ]
  node [
    id 858
    label "egzaminowanie"
  ]
  node [
    id 859
    label "punkt"
  ]
  node [
    id 860
    label "maglownik"
  ]
  node [
    id 861
    label "rozmowa"
  ]
  node [
    id 862
    label "zak&#322;ad"
  ]
  node [
    id 863
    label "t&#322;ok"
  ]
  node [
    id 864
    label "przes&#322;uchanie"
  ]
  node [
    id 865
    label "urz&#261;dzenie"
  ]
  node [
    id 866
    label "plotka"
  ]
  node [
    id 867
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 868
    label "wiadomy"
  ]
  node [
    id 869
    label "konfiguracja"
  ]
  node [
    id 870
    label "cz&#261;stka"
  ]
  node [
    id 871
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 872
    label "diadochia"
  ]
  node [
    id 873
    label "substancja"
  ]
  node [
    id 874
    label "grupa_funkcyjna"
  ]
  node [
    id 875
    label "integer"
  ]
  node [
    id 876
    label "zlewanie_si&#281;"
  ]
  node [
    id 877
    label "uk&#322;ad"
  ]
  node [
    id 878
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 879
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 880
    label "pe&#322;ny"
  ]
  node [
    id 881
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 882
    label "series"
  ]
  node [
    id 883
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 884
    label "uprawianie"
  ]
  node [
    id 885
    label "praca_rolnicza"
  ]
  node [
    id 886
    label "collection"
  ]
  node [
    id 887
    label "dane"
  ]
  node [
    id 888
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 889
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 890
    label "sum"
  ]
  node [
    id 891
    label "gathering"
  ]
  node [
    id 892
    label "zesp&#243;&#322;"
  ]
  node [
    id 893
    label "lias"
  ]
  node [
    id 894
    label "dzia&#322;"
  ]
  node [
    id 895
    label "system"
  ]
  node [
    id 896
    label "jednostka"
  ]
  node [
    id 897
    label "pi&#281;tro"
  ]
  node [
    id 898
    label "jednostka_geologiczna"
  ]
  node [
    id 899
    label "filia"
  ]
  node [
    id 900
    label "malm"
  ]
  node [
    id 901
    label "whole"
  ]
  node [
    id 902
    label "dogger"
  ]
  node [
    id 903
    label "poziom"
  ]
  node [
    id 904
    label "promocja"
  ]
  node [
    id 905
    label "bank"
  ]
  node [
    id 906
    label "ajencja"
  ]
  node [
    id 907
    label "wojsko"
  ]
  node [
    id 908
    label "siedziba"
  ]
  node [
    id 909
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 910
    label "agencja"
  ]
  node [
    id 911
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 912
    label "szpital"
  ]
  node [
    id 913
    label "blend"
  ]
  node [
    id 914
    label "struktura"
  ]
  node [
    id 915
    label "prawo_karne"
  ]
  node [
    id 916
    label "dzie&#322;o"
  ]
  node [
    id 917
    label "figuracja"
  ]
  node [
    id 918
    label "chwyt"
  ]
  node [
    id 919
    label "okup"
  ]
  node [
    id 920
    label "muzykologia"
  ]
  node [
    id 921
    label "&#347;redniowiecze"
  ]
  node [
    id 922
    label "czynnik_biotyczny"
  ]
  node [
    id 923
    label "wyewoluowanie"
  ]
  node [
    id 924
    label "reakcja"
  ]
  node [
    id 925
    label "individual"
  ]
  node [
    id 926
    label "przyswoi&#263;"
  ]
  node [
    id 927
    label "starzenie_si&#281;"
  ]
  node [
    id 928
    label "wyewoluowa&#263;"
  ]
  node [
    id 929
    label "okaz"
  ]
  node [
    id 930
    label "part"
  ]
  node [
    id 931
    label "przyswojenie"
  ]
  node [
    id 932
    label "ewoluowanie"
  ]
  node [
    id 933
    label "ewoluowa&#263;"
  ]
  node [
    id 934
    label "agent"
  ]
  node [
    id 935
    label "przyswaja&#263;"
  ]
  node [
    id 936
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 937
    label "nicpo&#324;"
  ]
  node [
    id 938
    label "przyswajanie"
  ]
  node [
    id 939
    label "feminizm"
  ]
  node [
    id 940
    label "Unia_Europejska"
  ]
  node [
    id 941
    label "uatrakcyjni&#263;"
  ]
  node [
    id 942
    label "przewietrzy&#263;"
  ]
  node [
    id 943
    label "regenerate"
  ]
  node [
    id 944
    label "odtworzy&#263;"
  ]
  node [
    id 945
    label "wymieni&#263;"
  ]
  node [
    id 946
    label "odbudowa&#263;"
  ]
  node [
    id 947
    label "odbudowywa&#263;"
  ]
  node [
    id 948
    label "m&#322;odzi&#263;"
  ]
  node [
    id 949
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 950
    label "przewietrza&#263;"
  ]
  node [
    id 951
    label "wymienia&#263;"
  ]
  node [
    id 952
    label "odtwarza&#263;"
  ]
  node [
    id 953
    label "odtwarzanie"
  ]
  node [
    id 954
    label "uatrakcyjnianie"
  ]
  node [
    id 955
    label "zast&#281;powanie"
  ]
  node [
    id 956
    label "odbudowywanie"
  ]
  node [
    id 957
    label "rejuvenation"
  ]
  node [
    id 958
    label "m&#322;odszy"
  ]
  node [
    id 959
    label "wymienienie"
  ]
  node [
    id 960
    label "uatrakcyjnienie"
  ]
  node [
    id 961
    label "odbudowanie"
  ]
  node [
    id 962
    label "odtworzenie"
  ]
  node [
    id 963
    label "asymilowanie_si&#281;"
  ]
  node [
    id 964
    label "absorption"
  ]
  node [
    id 965
    label "czerpanie"
  ]
  node [
    id 966
    label "acquisition"
  ]
  node [
    id 967
    label "zmienianie"
  ]
  node [
    id 968
    label "organizm"
  ]
  node [
    id 969
    label "assimilation"
  ]
  node [
    id 970
    label "upodabnianie"
  ]
  node [
    id 971
    label "g&#322;oska"
  ]
  node [
    id 972
    label "kultura"
  ]
  node [
    id 973
    label "podobny"
  ]
  node [
    id 974
    label "fonetyka"
  ]
  node [
    id 975
    label "mecz_mistrzowski"
  ]
  node [
    id 976
    label "&#347;rodowisko"
  ]
  node [
    id 977
    label "arrangement"
  ]
  node [
    id 978
    label "obrona"
  ]
  node [
    id 979
    label "pomoc"
  ]
  node [
    id 980
    label "organizacja"
  ]
  node [
    id 981
    label "rezerwa"
  ]
  node [
    id 982
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 983
    label "atak"
  ]
  node [
    id 984
    label "moneta"
  ]
  node [
    id 985
    label "union"
  ]
  node [
    id 986
    label "assimilate"
  ]
  node [
    id 987
    label "dostosowywa&#263;"
  ]
  node [
    id 988
    label "dostosowa&#263;"
  ]
  node [
    id 989
    label "przejmowa&#263;"
  ]
  node [
    id 990
    label "upodobni&#263;"
  ]
  node [
    id 991
    label "przej&#261;&#263;"
  ]
  node [
    id 992
    label "upodabnia&#263;"
  ]
  node [
    id 993
    label "typ"
  ]
  node [
    id 994
    label "jednostka_administracyjna"
  ]
  node [
    id 995
    label "zoologia"
  ]
  node [
    id 996
    label "skupienie"
  ]
  node [
    id 997
    label "kr&#243;lestwo"
  ]
  node [
    id 998
    label "tribe"
  ]
  node [
    id 999
    label "hurma"
  ]
  node [
    id 1000
    label "botanika"
  ]
  node [
    id 1001
    label "przebieg"
  ]
  node [
    id 1002
    label "praktyka"
  ]
  node [
    id 1003
    label "linia"
  ]
  node [
    id 1004
    label "bok"
  ]
  node [
    id 1005
    label "skr&#281;canie"
  ]
  node [
    id 1006
    label "skr&#281;ca&#263;"
  ]
  node [
    id 1007
    label "orientowanie"
  ]
  node [
    id 1008
    label "skr&#281;ci&#263;"
  ]
  node [
    id 1009
    label "metoda"
  ]
  node [
    id 1010
    label "ty&#322;"
  ]
  node [
    id 1011
    label "zorientowa&#263;"
  ]
  node [
    id 1012
    label "orientowa&#263;"
  ]
  node [
    id 1013
    label "ideologia"
  ]
  node [
    id 1014
    label "prz&#243;d"
  ]
  node [
    id 1015
    label "skr&#281;cenie"
  ]
  node [
    id 1016
    label "model"
  ]
  node [
    id 1017
    label "narz&#281;dzie"
  ]
  node [
    id 1018
    label "nature"
  ]
  node [
    id 1019
    label "intencja"
  ]
  node [
    id 1020
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 1021
    label "leaning"
  ]
  node [
    id 1022
    label "armia"
  ]
  node [
    id 1023
    label "poprowadzi&#263;"
  ]
  node [
    id 1024
    label "cord"
  ]
  node [
    id 1025
    label "po&#322;&#261;czenie"
  ]
  node [
    id 1026
    label "tract"
  ]
  node [
    id 1027
    label "materia&#322;_zecerski"
  ]
  node [
    id 1028
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1029
    label "curve"
  ]
  node [
    id 1030
    label "figura_geometryczna"
  ]
  node [
    id 1031
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 1032
    label "jard"
  ]
  node [
    id 1033
    label "szczep"
  ]
  node [
    id 1034
    label "phreaker"
  ]
  node [
    id 1035
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 1036
    label "grupa_organizm&#243;w"
  ]
  node [
    id 1037
    label "prowadzi&#263;"
  ]
  node [
    id 1038
    label "access"
  ]
  node [
    id 1039
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 1040
    label "billing"
  ]
  node [
    id 1041
    label "granica"
  ]
  node [
    id 1042
    label "szpaler"
  ]
  node [
    id 1043
    label "sztrych"
  ]
  node [
    id 1044
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 1045
    label "drzewo_genealogiczne"
  ]
  node [
    id 1046
    label "transporter"
  ]
  node [
    id 1047
    label "line"
  ]
  node [
    id 1048
    label "fragment"
  ]
  node [
    id 1049
    label "granice"
  ]
  node [
    id 1050
    label "kontakt"
  ]
  node [
    id 1051
    label "rz&#261;d"
  ]
  node [
    id 1052
    label "przewo&#378;nik"
  ]
  node [
    id 1053
    label "przystanek"
  ]
  node [
    id 1054
    label "linijka"
  ]
  node [
    id 1055
    label "uporz&#261;dkowanie"
  ]
  node [
    id 1056
    label "coalescence"
  ]
  node [
    id 1057
    label "Ural"
  ]
  node [
    id 1058
    label "prowadzenie"
  ]
  node [
    id 1059
    label "tekst"
  ]
  node [
    id 1060
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 1061
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 1062
    label "koniec"
  ]
  node [
    id 1063
    label "practice"
  ]
  node [
    id 1064
    label "znawstwo"
  ]
  node [
    id 1065
    label "skill"
  ]
  node [
    id 1066
    label "zwyczaj"
  ]
  node [
    id 1067
    label "eksperiencja"
  ]
  node [
    id 1068
    label "j&#261;dro"
  ]
  node [
    id 1069
    label "systemik"
  ]
  node [
    id 1070
    label "rozprz&#261;c"
  ]
  node [
    id 1071
    label "oprogramowanie"
  ]
  node [
    id 1072
    label "systemat"
  ]
  node [
    id 1073
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1074
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1075
    label "usenet"
  ]
  node [
    id 1076
    label "s&#261;d"
  ]
  node [
    id 1077
    label "porz&#261;dek"
  ]
  node [
    id 1078
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1079
    label "przyn&#281;ta"
  ]
  node [
    id 1080
    label "p&#322;&#243;d"
  ]
  node [
    id 1081
    label "net"
  ]
  node [
    id 1082
    label "w&#281;dkarstwo"
  ]
  node [
    id 1083
    label "eratem"
  ]
  node [
    id 1084
    label "doktryna"
  ]
  node [
    id 1085
    label "pulpit"
  ]
  node [
    id 1086
    label "konstelacja"
  ]
  node [
    id 1087
    label "o&#347;"
  ]
  node [
    id 1088
    label "podsystem"
  ]
  node [
    id 1089
    label "ryba"
  ]
  node [
    id 1090
    label "Leopard"
  ]
  node [
    id 1091
    label "Android"
  ]
  node [
    id 1092
    label "zachowanie"
  ]
  node [
    id 1093
    label "cybernetyk"
  ]
  node [
    id 1094
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1095
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1096
    label "method"
  ]
  node [
    id 1097
    label "sk&#322;ad"
  ]
  node [
    id 1098
    label "podstawa"
  ]
  node [
    id 1099
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1100
    label "procedura"
  ]
  node [
    id 1101
    label "room"
  ]
  node [
    id 1102
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 1103
    label "sequence"
  ]
  node [
    id 1104
    label "cycle"
  ]
  node [
    id 1105
    label "zmienienie"
  ]
  node [
    id 1106
    label "zmieni&#263;"
  ]
  node [
    id 1107
    label "eastern_hemisphere"
  ]
  node [
    id 1108
    label "inform"
  ]
  node [
    id 1109
    label "marshal"
  ]
  node [
    id 1110
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 1111
    label "wyznacza&#263;"
  ]
  node [
    id 1112
    label "pomaga&#263;"
  ]
  node [
    id 1113
    label "tu&#322;&#243;w"
  ]
  node [
    id 1114
    label "bro&#324;_my&#347;liwska"
  ]
  node [
    id 1115
    label "wielok&#261;t"
  ]
  node [
    id 1116
    label "odcinek"
  ]
  node [
    id 1117
    label "strzelba"
  ]
  node [
    id 1118
    label "lufa"
  ]
  node [
    id 1119
    label "&#347;ciana"
  ]
  node [
    id 1120
    label "strona"
  ]
  node [
    id 1121
    label "przyczynienie_si&#281;"
  ]
  node [
    id 1122
    label "zwr&#243;cenie"
  ]
  node [
    id 1123
    label "zrozumienie"
  ]
  node [
    id 1124
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1125
    label "&#347;wiatopogl&#261;d"
  ]
  node [
    id 1126
    label "zorientowanie_si&#281;"
  ]
  node [
    id 1127
    label "pogubienie_si&#281;"
  ]
  node [
    id 1128
    label "orientation"
  ]
  node [
    id 1129
    label "pogubi&#263;_si&#281;"
  ]
  node [
    id 1130
    label "gubi&#263;_si&#281;"
  ]
  node [
    id 1131
    label "gubienie_si&#281;"
  ]
  node [
    id 1132
    label "turn"
  ]
  node [
    id 1133
    label "wrench"
  ]
  node [
    id 1134
    label "nawini&#281;cie"
  ]
  node [
    id 1135
    label "uszkodzenie"
  ]
  node [
    id 1136
    label "poskr&#281;canie"
  ]
  node [
    id 1137
    label "uraz"
  ]
  node [
    id 1138
    label "odchylenie_si&#281;"
  ]
  node [
    id 1139
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 1140
    label "z&#322;&#261;czenie"
  ]
  node [
    id 1141
    label "splecenie"
  ]
  node [
    id 1142
    label "turning"
  ]
  node [
    id 1143
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 1144
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 1145
    label "sple&#347;&#263;"
  ]
  node [
    id 1146
    label "os&#322;abi&#263;"
  ]
  node [
    id 1147
    label "nawin&#261;&#263;"
  ]
  node [
    id 1148
    label "scali&#263;"
  ]
  node [
    id 1149
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 1150
    label "twist"
  ]
  node [
    id 1151
    label "splay"
  ]
  node [
    id 1152
    label "uszkodzi&#263;"
  ]
  node [
    id 1153
    label "break"
  ]
  node [
    id 1154
    label "flex"
  ]
  node [
    id 1155
    label "przestrze&#324;"
  ]
  node [
    id 1156
    label "zaty&#322;"
  ]
  node [
    id 1157
    label "pupa"
  ]
  node [
    id 1158
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1159
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 1160
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 1161
    label "splata&#263;"
  ]
  node [
    id 1162
    label "throw"
  ]
  node [
    id 1163
    label "screw"
  ]
  node [
    id 1164
    label "scala&#263;"
  ]
  node [
    id 1165
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 1166
    label "przelezienie"
  ]
  node [
    id 1167
    label "&#347;piew"
  ]
  node [
    id 1168
    label "Synaj"
  ]
  node [
    id 1169
    label "Kreml"
  ]
  node [
    id 1170
    label "wysoki"
  ]
  node [
    id 1171
    label "wzniesienie"
  ]
  node [
    id 1172
    label "Ropa"
  ]
  node [
    id 1173
    label "kupa"
  ]
  node [
    id 1174
    label "przele&#378;&#263;"
  ]
  node [
    id 1175
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 1176
    label "karczek"
  ]
  node [
    id 1177
    label "rami&#261;czko"
  ]
  node [
    id 1178
    label "Jaworze"
  ]
  node [
    id 1179
    label "orient"
  ]
  node [
    id 1180
    label "przyczyni&#263;_si&#281;"
  ]
  node [
    id 1181
    label "aim"
  ]
  node [
    id 1182
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 1183
    label "wyznaczy&#263;"
  ]
  node [
    id 1184
    label "pomaganie"
  ]
  node [
    id 1185
    label "przyczynianie_si&#281;"
  ]
  node [
    id 1186
    label "zwracanie"
  ]
  node [
    id 1187
    label "rozeznawanie"
  ]
  node [
    id 1188
    label "oznaczanie"
  ]
  node [
    id 1189
    label "odchylanie_si&#281;"
  ]
  node [
    id 1190
    label "kszta&#322;towanie"
  ]
  node [
    id 1191
    label "uprz&#281;dzenie"
  ]
  node [
    id 1192
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1193
    label "scalanie"
  ]
  node [
    id 1194
    label "lewoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 1195
    label "snucie"
  ]
  node [
    id 1196
    label "prawoskr&#281;tno&#347;&#263;"
  ]
  node [
    id 1197
    label "tortuosity"
  ]
  node [
    id 1198
    label "odbijanie"
  ]
  node [
    id 1199
    label "contortion"
  ]
  node [
    id 1200
    label "splatanie"
  ]
  node [
    id 1201
    label "political_orientation"
  ]
  node [
    id 1202
    label "idea"
  ]
  node [
    id 1203
    label "kanclerz"
  ]
  node [
    id 1204
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 1205
    label "podkanclerz"
  ]
  node [
    id 1206
    label "miasteczko_studenckie"
  ]
  node [
    id 1207
    label "kwestura"
  ]
  node [
    id 1208
    label "wyk&#322;adanie"
  ]
  node [
    id 1209
    label "rektorat"
  ]
  node [
    id 1210
    label "school"
  ]
  node [
    id 1211
    label "senat"
  ]
  node [
    id 1212
    label "promotorstwo"
  ]
  node [
    id 1213
    label "teren_szko&#322;y"
  ]
  node [
    id 1214
    label "kwalifikacje"
  ]
  node [
    id 1215
    label "podr&#281;cznik"
  ]
  node [
    id 1216
    label "zda&#263;"
  ]
  node [
    id 1217
    label "gabinet"
  ]
  node [
    id 1218
    label "urszulanki"
  ]
  node [
    id 1219
    label "sztuba"
  ]
  node [
    id 1220
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 1221
    label "przepisa&#263;"
  ]
  node [
    id 1222
    label "form"
  ]
  node [
    id 1223
    label "lekcja"
  ]
  node [
    id 1224
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 1225
    label "przepisanie"
  ]
  node [
    id 1226
    label "skolaryzacja"
  ]
  node [
    id 1227
    label "zdanie"
  ]
  node [
    id 1228
    label "stopek"
  ]
  node [
    id 1229
    label "sekretariat"
  ]
  node [
    id 1230
    label "lesson"
  ]
  node [
    id 1231
    label "instytucja"
  ]
  node [
    id 1232
    label "niepokalanki"
  ]
  node [
    id 1233
    label "szkolenie"
  ]
  node [
    id 1234
    label "kara"
  ]
  node [
    id 1235
    label "tablica"
  ]
  node [
    id 1236
    label "bursary"
  ]
  node [
    id 1237
    label "urz&#261;d"
  ]
  node [
    id 1238
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 1239
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 1240
    label "biuro"
  ]
  node [
    id 1241
    label "parlament"
  ]
  node [
    id 1242
    label "organ"
  ]
  node [
    id 1243
    label "deputation"
  ]
  node [
    id 1244
    label "kolegium"
  ]
  node [
    id 1245
    label "izba_wy&#380;sza"
  ]
  node [
    id 1246
    label "magistrat"
  ]
  node [
    id 1247
    label "uczy&#263;"
  ]
  node [
    id 1248
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 1249
    label "wyjmowa&#263;"
  ]
  node [
    id 1250
    label "translate"
  ]
  node [
    id 1251
    label "elaborate"
  ]
  node [
    id 1252
    label "give"
  ]
  node [
    id 1253
    label "urz&#281;dnik"
  ]
  node [
    id 1254
    label "puszczenie"
  ]
  node [
    id 1255
    label "issue"
  ]
  node [
    id 1256
    label "wyjmowanie"
  ]
  node [
    id 1257
    label "robienie"
  ]
  node [
    id 1258
    label "wystawianie"
  ]
  node [
    id 1259
    label "k&#322;adzenie"
  ]
  node [
    id 1260
    label "t&#322;umaczenie"
  ]
  node [
    id 1261
    label "uczenie"
  ]
  node [
    id 1262
    label "presentation"
  ]
  node [
    id 1263
    label "opieka"
  ]
  node [
    id 1264
    label "Bismarck"
  ]
  node [
    id 1265
    label "kuria"
  ]
  node [
    id 1266
    label "premier"
  ]
  node [
    id 1267
    label "Goebbels"
  ]
  node [
    id 1268
    label "duchowny"
  ]
  node [
    id 1269
    label "dostojnik"
  ]
  node [
    id 1270
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 1271
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 1272
    label "appear"
  ]
  node [
    id 1273
    label "rise"
  ]
  node [
    id 1274
    label "buddyzm"
  ]
  node [
    id 1275
    label "cnota"
  ]
  node [
    id 1276
    label "dar"
  ]
  node [
    id 1277
    label "dyspozycja"
  ]
  node [
    id 1278
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1279
    label "da&#324;"
  ]
  node [
    id 1280
    label "faculty"
  ]
  node [
    id 1281
    label "stygmat"
  ]
  node [
    id 1282
    label "dobro"
  ]
  node [
    id 1283
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 1284
    label "dobro&#263;"
  ]
  node [
    id 1285
    label "aretologia"
  ]
  node [
    id 1286
    label "zaleta"
  ]
  node [
    id 1287
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 1288
    label "honesty"
  ]
  node [
    id 1289
    label "panie&#324;stwo"
  ]
  node [
    id 1290
    label "kalpa"
  ]
  node [
    id 1291
    label "lampka_ma&#347;lana"
  ]
  node [
    id 1292
    label "Buddhism"
  ]
  node [
    id 1293
    label "mahajana"
  ]
  node [
    id 1294
    label "bardo"
  ]
  node [
    id 1295
    label "wad&#378;rajana"
  ]
  node [
    id 1296
    label "arahant"
  ]
  node [
    id 1297
    label "therawada"
  ]
  node [
    id 1298
    label "tantryzm"
  ]
  node [
    id 1299
    label "ahinsa"
  ]
  node [
    id 1300
    label "hinajana"
  ]
  node [
    id 1301
    label "bonzo"
  ]
  node [
    id 1302
    label "asura"
  ]
  node [
    id 1303
    label "religia"
  ]
  node [
    id 1304
    label "maja"
  ]
  node [
    id 1305
    label "li"
  ]
  node [
    id 1306
    label "air"
  ]
  node [
    id 1307
    label "dostarczy&#263;"
  ]
  node [
    id 1308
    label "wytworzy&#263;"
  ]
  node [
    id 1309
    label "spowodowa&#263;"
  ]
  node [
    id 1310
    label "picture"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 7
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
  edge [
    source 8
    target 662
  ]
  edge [
    source 8
    target 663
  ]
  edge [
    source 8
    target 664
  ]
  edge [
    source 8
    target 665
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 667
  ]
  edge [
    source 8
    target 668
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 669
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 670
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 671
  ]
  edge [
    source 8
    target 672
  ]
  edge [
    source 8
    target 673
  ]
  edge [
    source 8
    target 674
  ]
  edge [
    source 8
    target 675
  ]
  edge [
    source 8
    target 676
  ]
  edge [
    source 8
    target 677
  ]
  edge [
    source 8
    target 678
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 680
  ]
  edge [
    source 8
    target 681
  ]
  edge [
    source 8
    target 682
  ]
  edge [
    source 8
    target 683
  ]
  edge [
    source 8
    target 684
  ]
  edge [
    source 8
    target 685
  ]
  edge [
    source 8
    target 686
  ]
  edge [
    source 8
    target 687
  ]
  edge [
    source 8
    target 688
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 9
    target 704
  ]
  edge [
    source 9
    target 705
  ]
  edge [
    source 9
    target 706
  ]
  edge [
    source 9
    target 707
  ]
  edge [
    source 9
    target 708
  ]
  edge [
    source 9
    target 709
  ]
  edge [
    source 9
    target 710
  ]
  edge [
    source 9
    target 711
  ]
  edge [
    source 9
    target 333
  ]
  edge [
    source 9
    target 712
  ]
  edge [
    source 9
    target 713
  ]
  edge [
    source 9
    target 714
  ]
  edge [
    source 9
    target 715
  ]
  edge [
    source 9
    target 716
  ]
  edge [
    source 9
    target 717
  ]
  edge [
    source 9
    target 718
  ]
  edge [
    source 9
    target 719
  ]
  edge [
    source 9
    target 720
  ]
  edge [
    source 9
    target 721
  ]
  edge [
    source 9
    target 722
  ]
  edge [
    source 9
    target 723
  ]
  edge [
    source 9
    target 724
  ]
  edge [
    source 9
    target 725
  ]
  edge [
    source 9
    target 726
  ]
  edge [
    source 9
    target 727
  ]
  edge [
    source 9
    target 728
  ]
  edge [
    source 9
    target 729
  ]
  edge [
    source 9
    target 730
  ]
  edge [
    source 9
    target 731
  ]
  edge [
    source 9
    target 732
  ]
  edge [
    source 9
    target 733
  ]
  edge [
    source 9
    target 734
  ]
  edge [
    source 9
    target 735
  ]
  edge [
    source 9
    target 736
  ]
  edge [
    source 9
    target 737
  ]
  edge [
    source 9
    target 738
  ]
  edge [
    source 9
    target 739
  ]
  edge [
    source 9
    target 740
  ]
  edge [
    source 9
    target 741
  ]
  edge [
    source 9
    target 742
  ]
  edge [
    source 9
    target 743
  ]
  edge [
    source 9
    target 744
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 9
    target 770
  ]
  edge [
    source 9
    target 771
  ]
  edge [
    source 9
    target 772
  ]
  edge [
    source 9
    target 773
  ]
  edge [
    source 9
    target 774
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 775
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 776
  ]
  edge [
    source 9
    target 777
  ]
  edge [
    source 9
    target 778
  ]
  edge [
    source 9
    target 779
  ]
  edge [
    source 9
    target 780
  ]
  edge [
    source 9
    target 781
  ]
  edge [
    source 9
    target 782
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 599
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 10
    target 841
  ]
  edge [
    source 10
    target 842
  ]
  edge [
    source 10
    target 843
  ]
  edge [
    source 10
    target 844
  ]
  edge [
    source 10
    target 845
  ]
  edge [
    source 10
    target 846
  ]
  edge [
    source 10
    target 847
  ]
  edge [
    source 10
    target 848
  ]
  edge [
    source 10
    target 849
  ]
  edge [
    source 10
    target 850
  ]
  edge [
    source 10
    target 851
  ]
  edge [
    source 10
    target 852
  ]
  edge [
    source 10
    target 853
  ]
  edge [
    source 10
    target 854
  ]
  edge [
    source 10
    target 855
  ]
  edge [
    source 10
    target 856
  ]
  edge [
    source 10
    target 857
  ]
  edge [
    source 10
    target 858
  ]
  edge [
    source 10
    target 859
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 860
  ]
  edge [
    source 10
    target 861
  ]
  edge [
    source 10
    target 862
  ]
  edge [
    source 10
    target 863
  ]
  edge [
    source 10
    target 864
  ]
  edge [
    source 10
    target 865
  ]
  edge [
    source 10
    target 866
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 15
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 12
    target 588
  ]
  edge [
    source 12
    target 867
  ]
  edge [
    source 12
    target 868
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 13
    target 88
  ]
  edge [
    source 13
    target 89
  ]
  edge [
    source 13
    target 90
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 13
    target 92
  ]
  edge [
    source 13
    target 93
  ]
  edge [
    source 13
    target 94
  ]
  edge [
    source 13
    target 63
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 916
  ]
  edge [
    source 13
    target 917
  ]
  edge [
    source 13
    target 918
  ]
  edge [
    source 13
    target 919
  ]
  edge [
    source 13
    target 920
  ]
  edge [
    source 13
    target 921
  ]
  edge [
    source 13
    target 922
  ]
  edge [
    source 13
    target 923
  ]
  edge [
    source 13
    target 924
  ]
  edge [
    source 13
    target 925
  ]
  edge [
    source 13
    target 926
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 927
  ]
  edge [
    source 13
    target 928
  ]
  edge [
    source 13
    target 929
  ]
  edge [
    source 13
    target 930
  ]
  edge [
    source 13
    target 931
  ]
  edge [
    source 13
    target 932
  ]
  edge [
    source 13
    target 933
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 934
  ]
  edge [
    source 13
    target 935
  ]
  edge [
    source 13
    target 936
  ]
  edge [
    source 13
    target 937
  ]
  edge [
    source 13
    target 938
  ]
  edge [
    source 13
    target 939
  ]
  edge [
    source 13
    target 940
  ]
  edge [
    source 13
    target 941
  ]
  edge [
    source 13
    target 942
  ]
  edge [
    source 13
    target 943
  ]
  edge [
    source 13
    target 944
  ]
  edge [
    source 13
    target 945
  ]
  edge [
    source 13
    target 946
  ]
  edge [
    source 13
    target 947
  ]
  edge [
    source 13
    target 948
  ]
  edge [
    source 13
    target 949
  ]
  edge [
    source 13
    target 950
  ]
  edge [
    source 13
    target 951
  ]
  edge [
    source 13
    target 952
  ]
  edge [
    source 13
    target 953
  ]
  edge [
    source 13
    target 954
  ]
  edge [
    source 13
    target 955
  ]
  edge [
    source 13
    target 956
  ]
  edge [
    source 13
    target 957
  ]
  edge [
    source 13
    target 958
  ]
  edge [
    source 13
    target 959
  ]
  edge [
    source 13
    target 960
  ]
  edge [
    source 13
    target 961
  ]
  edge [
    source 13
    target 962
  ]
  edge [
    source 13
    target 963
  ]
  edge [
    source 13
    target 964
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 965
  ]
  edge [
    source 13
    target 966
  ]
  edge [
    source 13
    target 967
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 968
  ]
  edge [
    source 13
    target 969
  ]
  edge [
    source 13
    target 970
  ]
  edge [
    source 13
    target 971
  ]
  edge [
    source 13
    target 972
  ]
  edge [
    source 13
    target 973
  ]
  edge [
    source 13
    target 974
  ]
  edge [
    source 13
    target 975
  ]
  edge [
    source 13
    target 976
  ]
  edge [
    source 13
    target 977
  ]
  edge [
    source 13
    target 978
  ]
  edge [
    source 13
    target 979
  ]
  edge [
    source 13
    target 980
  ]
  edge [
    source 13
    target 981
  ]
  edge [
    source 13
    target 982
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 983
  ]
  edge [
    source 13
    target 984
  ]
  edge [
    source 13
    target 985
  ]
  edge [
    source 13
    target 986
  ]
  edge [
    source 13
    target 987
  ]
  edge [
    source 13
    target 988
  ]
  edge [
    source 13
    target 989
  ]
  edge [
    source 13
    target 990
  ]
  edge [
    source 13
    target 991
  ]
  edge [
    source 13
    target 992
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 993
  ]
  edge [
    source 13
    target 994
  ]
  edge [
    source 13
    target 995
  ]
  edge [
    source 13
    target 996
  ]
  edge [
    source 13
    target 997
  ]
  edge [
    source 13
    target 998
  ]
  edge [
    source 13
    target 999
  ]
  edge [
    source 13
    target 1000
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 1001
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 1002
  ]
  edge [
    source 14
    target 895
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 1003
  ]
  edge [
    source 14
    target 1004
  ]
  edge [
    source 14
    target 1005
  ]
  edge [
    source 14
    target 1006
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 1007
  ]
  edge [
    source 14
    target 1008
  ]
  edge [
    source 14
    target 670
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 672
  ]
  edge [
    source 14
    target 677
  ]
  edge [
    source 14
    target 1009
  ]
  edge [
    source 14
    target 1010
  ]
  edge [
    source 14
    target 1011
  ]
  edge [
    source 14
    target 457
  ]
  edge [
    source 14
    target 1012
  ]
  edge [
    source 14
    target 682
  ]
  edge [
    source 14
    target 1013
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 1014
  ]
  edge [
    source 14
    target 687
  ]
  edge [
    source 14
    target 1015
  ]
  edge [
    source 14
    target 1016
  ]
  edge [
    source 14
    target 1017
  ]
  edge [
    source 14
    target 94
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 1018
  ]
  edge [
    source 14
    target 1019
  ]
  edge [
    source 14
    target 1020
  ]
  edge [
    source 14
    target 1021
  ]
  edge [
    source 14
    target 436
  ]
  edge [
    source 14
    target 1022
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 1023
  ]
  edge [
    source 14
    target 1024
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 1025
  ]
  edge [
    source 14
    target 1026
  ]
  edge [
    source 14
    target 1027
  ]
  edge [
    source 14
    target 1028
  ]
  edge [
    source 14
    target 1029
  ]
  edge [
    source 14
    target 1030
  ]
  edge [
    source 14
    target 413
  ]
  edge [
    source 14
    target 1031
  ]
  edge [
    source 14
    target 1032
  ]
  edge [
    source 14
    target 1033
  ]
  edge [
    source 14
    target 1034
  ]
  edge [
    source 14
    target 1035
  ]
  edge [
    source 14
    target 1036
  ]
  edge [
    source 14
    target 1037
  ]
  edge [
    source 14
    target 444
  ]
  edge [
    source 14
    target 1038
  ]
  edge [
    source 14
    target 1039
  ]
  edge [
    source 14
    target 1040
  ]
  edge [
    source 14
    target 1041
  ]
  edge [
    source 14
    target 1042
  ]
  edge [
    source 14
    target 1043
  ]
  edge [
    source 14
    target 1044
  ]
  edge [
    source 14
    target 1045
  ]
  edge [
    source 14
    target 1046
  ]
  edge [
    source 14
    target 1047
  ]
  edge [
    source 14
    target 1048
  ]
  edge [
    source 14
    target 421
  ]
  edge [
    source 14
    target 812
  ]
  edge [
    source 14
    target 423
  ]
  edge [
    source 14
    target 1049
  ]
  edge [
    source 14
    target 1050
  ]
  edge [
    source 14
    target 1051
  ]
  edge [
    source 14
    target 1052
  ]
  edge [
    source 14
    target 1053
  ]
  edge [
    source 14
    target 1054
  ]
  edge [
    source 14
    target 1055
  ]
  edge [
    source 14
    target 1056
  ]
  edge [
    source 14
    target 1057
  ]
  edge [
    source 14
    target 426
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 832
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 224
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 352
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 209
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 99
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 898
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 232
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 53
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 438
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 325
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 440
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 1134
  ]
  edge [
    source 14
    target 366
  ]
  edge [
    source 14
    target 1135
  ]
  edge [
    source 14
    target 321
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 1139
  ]
  edge [
    source 14
    target 1140
  ]
  edge [
    source 14
    target 1141
  ]
  edge [
    source 14
    target 1142
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1144
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 14
    target 1146
  ]
  edge [
    source 14
    target 1147
  ]
  edge [
    source 14
    target 1148
  ]
  edge [
    source 14
    target 1149
  ]
  edge [
    source 14
    target 1150
  ]
  edge [
    source 14
    target 1151
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 1152
  ]
  edge [
    source 14
    target 1153
  ]
  edge [
    source 14
    target 1154
  ]
  edge [
    source 14
    target 1155
  ]
  edge [
    source 14
    target 1156
  ]
  edge [
    source 14
    target 1157
  ]
  edge [
    source 14
    target 462
  ]
  edge [
    source 14
    target 1158
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 1159
  ]
  edge [
    source 14
    target 1160
  ]
  edge [
    source 14
    target 1161
  ]
  edge [
    source 14
    target 1162
  ]
  edge [
    source 14
    target 1163
  ]
  edge [
    source 14
    target 613
  ]
  edge [
    source 14
    target 1164
  ]
  edge [
    source 14
    target 1165
  ]
  edge [
    source 14
    target 479
  ]
  edge [
    source 14
    target 1166
  ]
  edge [
    source 14
    target 1167
  ]
  edge [
    source 14
    target 1168
  ]
  edge [
    source 14
    target 1169
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 1170
  ]
  edge [
    source 14
    target 72
  ]
  edge [
    source 14
    target 1171
  ]
  edge [
    source 14
    target 897
  ]
  edge [
    source 14
    target 1172
  ]
  edge [
    source 14
    target 1173
  ]
  edge [
    source 14
    target 1174
  ]
  edge [
    source 14
    target 1175
  ]
  edge [
    source 14
    target 1176
  ]
  edge [
    source 14
    target 1177
  ]
  edge [
    source 14
    target 1178
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 14
    target 1179
  ]
  edge [
    source 14
    target 1180
  ]
  edge [
    source 14
    target 1181
  ]
  edge [
    source 14
    target 1182
  ]
  edge [
    source 14
    target 1183
  ]
  edge [
    source 14
    target 1184
  ]
  edge [
    source 14
    target 1185
  ]
  edge [
    source 14
    target 1186
  ]
  edge [
    source 14
    target 1187
  ]
  edge [
    source 14
    target 1188
  ]
  edge [
    source 14
    target 1189
  ]
  edge [
    source 14
    target 1190
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 1191
  ]
  edge [
    source 14
    target 1192
  ]
  edge [
    source 14
    target 1193
  ]
  edge [
    source 14
    target 1194
  ]
  edge [
    source 14
    target 1195
  ]
  edge [
    source 14
    target 1196
  ]
  edge [
    source 14
    target 1197
  ]
  edge [
    source 14
    target 1198
  ]
  edge [
    source 14
    target 1199
  ]
  edge [
    source 14
    target 1200
  ]
  edge [
    source 14
    target 1201
  ]
  edge [
    source 14
    target 1202
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1203
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 1204
  ]
  edge [
    source 15
    target 1205
  ]
  edge [
    source 15
    target 1206
  ]
  edge [
    source 15
    target 1207
  ]
  edge [
    source 15
    target 1208
  ]
  edge [
    source 15
    target 1209
  ]
  edge [
    source 15
    target 1210
  ]
  edge [
    source 15
    target 1211
  ]
  edge [
    source 15
    target 1212
  ]
  edge [
    source 15
    target 819
  ]
  edge [
    source 15
    target 1213
  ]
  edge [
    source 15
    target 224
  ]
  edge [
    source 15
    target 506
  ]
  edge [
    source 15
    target 1214
  ]
  edge [
    source 15
    target 1215
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 1002
  ]
  edge [
    source 15
    target 895
  ]
  edge [
    source 15
    target 1216
  ]
  edge [
    source 15
    target 1217
  ]
  edge [
    source 15
    target 1218
  ]
  edge [
    source 15
    target 1219
  ]
  edge [
    source 15
    target 236
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 1220
  ]
  edge [
    source 15
    target 1221
  ]
  edge [
    source 15
    target 65
  ]
  edge [
    source 15
    target 1222
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 1223
  ]
  edge [
    source 15
    target 1009
  ]
  edge [
    source 15
    target 1224
  ]
  edge [
    source 15
    target 1225
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 1226
  ]
  edge [
    source 15
    target 1227
  ]
  edge [
    source 15
    target 1228
  ]
  edge [
    source 15
    target 1229
  ]
  edge [
    source 15
    target 1013
  ]
  edge [
    source 15
    target 1230
  ]
  edge [
    source 15
    target 1231
  ]
  edge [
    source 15
    target 1232
  ]
  edge [
    source 15
    target 908
  ]
  edge [
    source 15
    target 1233
  ]
  edge [
    source 15
    target 1234
  ]
  edge [
    source 15
    target 1235
  ]
  edge [
    source 15
    target 1236
  ]
  edge [
    source 15
    target 1237
  ]
  edge [
    source 15
    target 1238
  ]
  edge [
    source 15
    target 1239
  ]
  edge [
    source 15
    target 1240
  ]
  edge [
    source 15
    target 1241
  ]
  edge [
    source 15
    target 1242
  ]
  edge [
    source 15
    target 1243
  ]
  edge [
    source 15
    target 1244
  ]
  edge [
    source 15
    target 1245
  ]
  edge [
    source 15
    target 1246
  ]
  edge [
    source 15
    target 41
  ]
  edge [
    source 15
    target 1247
  ]
  edge [
    source 15
    target 1248
  ]
  edge [
    source 15
    target 1249
  ]
  edge [
    source 15
    target 1250
  ]
  edge [
    source 15
    target 1251
  ]
  edge [
    source 15
    target 1252
  ]
  edge [
    source 15
    target 1253
  ]
  edge [
    source 15
    target 1254
  ]
  edge [
    source 15
    target 1255
  ]
  edge [
    source 15
    target 1256
  ]
  edge [
    source 15
    target 850
  ]
  edge [
    source 15
    target 1257
  ]
  edge [
    source 15
    target 1258
  ]
  edge [
    source 15
    target 1259
  ]
  edge [
    source 15
    target 1260
  ]
  edge [
    source 15
    target 1261
  ]
  edge [
    source 15
    target 216
  ]
  edge [
    source 15
    target 1262
  ]
  edge [
    source 15
    target 1263
  ]
  edge [
    source 15
    target 1264
  ]
  edge [
    source 15
    target 1265
  ]
  edge [
    source 15
    target 1266
  ]
  edge [
    source 15
    target 1267
  ]
  edge [
    source 15
    target 1268
  ]
  edge [
    source 15
    target 1269
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1270
  ]
  edge [
    source 16
    target 1271
  ]
  edge [
    source 16
    target 1272
  ]
  edge [
    source 16
    target 1273
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1274
  ]
  edge [
    source 17
    target 1275
  ]
  edge [
    source 17
    target 1276
  ]
  edge [
    source 17
    target 1277
  ]
  edge [
    source 17
    target 1278
  ]
  edge [
    source 17
    target 1279
  ]
  edge [
    source 17
    target 1280
  ]
  edge [
    source 17
    target 1281
  ]
  edge [
    source 17
    target 1282
  ]
  edge [
    source 17
    target 486
  ]
  edge [
    source 17
    target 1283
  ]
  edge [
    source 17
    target 1284
  ]
  edge [
    source 17
    target 1285
  ]
  edge [
    source 17
    target 1286
  ]
  edge [
    source 17
    target 1287
  ]
  edge [
    source 17
    target 35
  ]
  edge [
    source 17
    target 1288
  ]
  edge [
    source 17
    target 73
  ]
  edge [
    source 17
    target 1289
  ]
  edge [
    source 17
    target 1290
  ]
  edge [
    source 17
    target 1291
  ]
  edge [
    source 17
    target 1292
  ]
  edge [
    source 17
    target 1293
  ]
  edge [
    source 17
    target 1294
  ]
  edge [
    source 17
    target 1295
  ]
  edge [
    source 17
    target 1296
  ]
  edge [
    source 17
    target 1297
  ]
  edge [
    source 17
    target 1298
  ]
  edge [
    source 17
    target 1299
  ]
  edge [
    source 17
    target 1300
  ]
  edge [
    source 17
    target 1301
  ]
  edge [
    source 17
    target 1302
  ]
  edge [
    source 17
    target 1303
  ]
  edge [
    source 17
    target 1304
  ]
  edge [
    source 17
    target 1305
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1306
  ]
  edge [
    source 18
    target 1307
  ]
  edge [
    source 18
    target 1308
  ]
  edge [
    source 18
    target 1309
  ]
  edge [
    source 18
    target 1252
  ]
  edge [
    source 18
    target 1310
  ]
]
