graph [
  node [
    id 0
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 1
    label "mama"
    origin "text"
  ]
  node [
    id 2
    label "forum"
    origin "text"
  ]
  node [
    id 3
    label "pismak"
    origin "text"
  ]
  node [
    id 4
    label "coating"
  ]
  node [
    id 5
    label "conclusion"
  ]
  node [
    id 6
    label "koniec"
  ]
  node [
    id 7
    label "runda"
  ]
  node [
    id 8
    label "seria"
  ]
  node [
    id 9
    label "turniej"
  ]
  node [
    id 10
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 11
    label "okr&#261;&#380;enie"
  ]
  node [
    id 12
    label "rhythm"
  ]
  node [
    id 13
    label "rozgrywka"
  ]
  node [
    id 14
    label "czas"
  ]
  node [
    id 15
    label "faza"
  ]
  node [
    id 16
    label "punkt"
  ]
  node [
    id 17
    label "kres_&#380;ycia"
  ]
  node [
    id 18
    label "ostatnie_podrygi"
  ]
  node [
    id 19
    label "&#380;a&#322;oba"
  ]
  node [
    id 20
    label "kres"
  ]
  node [
    id 21
    label "zabicie"
  ]
  node [
    id 22
    label "pogrzebanie"
  ]
  node [
    id 23
    label "wydarzenie"
  ]
  node [
    id 24
    label "visitation"
  ]
  node [
    id 25
    label "miejsce"
  ]
  node [
    id 26
    label "agonia"
  ]
  node [
    id 27
    label "szeol"
  ]
  node [
    id 28
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 29
    label "szereg"
  ]
  node [
    id 30
    label "mogi&#322;a"
  ]
  node [
    id 31
    label "chwila"
  ]
  node [
    id 32
    label "dzia&#322;anie"
  ]
  node [
    id 33
    label "defenestracja"
  ]
  node [
    id 34
    label "rodzic"
  ]
  node [
    id 35
    label "matka_zast&#281;pcza"
  ]
  node [
    id 36
    label "stara"
  ]
  node [
    id 37
    label "Matka_Boska"
  ]
  node [
    id 38
    label "matczysko"
  ]
  node [
    id 39
    label "przodkini"
  ]
  node [
    id 40
    label "rodzice"
  ]
  node [
    id 41
    label "macierz"
  ]
  node [
    id 42
    label "macocha"
  ]
  node [
    id 43
    label "wapniaki"
  ]
  node [
    id 44
    label "starzy"
  ]
  node [
    id 45
    label "pokolenie"
  ]
  node [
    id 46
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 47
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 48
    label "krewna"
  ]
  node [
    id 49
    label "opiekun"
  ]
  node [
    id 50
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 51
    label "wapniak"
  ]
  node [
    id 52
    label "rodzic_chrzestny"
  ]
  node [
    id 53
    label "matka"
  ]
  node [
    id 54
    label "partnerka"
  ]
  node [
    id 55
    label "&#380;ona"
  ]
  node [
    id 56
    label "kobieta"
  ]
  node [
    id 57
    label "poj&#281;cie"
  ]
  node [
    id 58
    label "pa&#324;stwo"
  ]
  node [
    id 59
    label "mod"
  ]
  node [
    id 60
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 61
    label "patriota"
  ]
  node [
    id 62
    label "parametryzacja"
  ]
  node [
    id 63
    label "matuszka"
  ]
  node [
    id 64
    label "m&#281;&#380;atka"
  ]
  node [
    id 65
    label "przestrze&#324;"
  ]
  node [
    id 66
    label "grupa_dyskusyjna"
  ]
  node [
    id 67
    label "bazylika"
  ]
  node [
    id 68
    label "portal"
  ]
  node [
    id 69
    label "plac"
  ]
  node [
    id 70
    label "agora"
  ]
  node [
    id 71
    label "konferencja"
  ]
  node [
    id 72
    label "s&#261;d"
  ]
  node [
    id 73
    label "strona"
  ]
  node [
    id 74
    label "grupa"
  ]
  node [
    id 75
    label "asymilowa&#263;"
  ]
  node [
    id 76
    label "kompozycja"
  ]
  node [
    id 77
    label "pakiet_klimatyczny"
  ]
  node [
    id 78
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 79
    label "type"
  ]
  node [
    id 80
    label "cz&#261;steczka"
  ]
  node [
    id 81
    label "gromada"
  ]
  node [
    id 82
    label "specgrupa"
  ]
  node [
    id 83
    label "egzemplarz"
  ]
  node [
    id 84
    label "stage_set"
  ]
  node [
    id 85
    label "asymilowanie"
  ]
  node [
    id 86
    label "zbi&#243;r"
  ]
  node [
    id 87
    label "odm&#322;odzenie"
  ]
  node [
    id 88
    label "odm&#322;adza&#263;"
  ]
  node [
    id 89
    label "harcerze_starsi"
  ]
  node [
    id 90
    label "jednostka_systematyczna"
  ]
  node [
    id 91
    label "oddzia&#322;"
  ]
  node [
    id 92
    label "category"
  ]
  node [
    id 93
    label "liga"
  ]
  node [
    id 94
    label "&#346;wietliki"
  ]
  node [
    id 95
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 96
    label "formacja_geologiczna"
  ]
  node [
    id 97
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 98
    label "Eurogrupa"
  ]
  node [
    id 99
    label "Terranie"
  ]
  node [
    id 100
    label "odm&#322;adzanie"
  ]
  node [
    id 101
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 102
    label "Entuzjastki"
  ]
  node [
    id 103
    label "rz&#261;d"
  ]
  node [
    id 104
    label "uwaga"
  ]
  node [
    id 105
    label "cecha"
  ]
  node [
    id 106
    label "praca"
  ]
  node [
    id 107
    label "location"
  ]
  node [
    id 108
    label "warunek_lokalowy"
  ]
  node [
    id 109
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 110
    label "cia&#322;o"
  ]
  node [
    id 111
    label "status"
  ]
  node [
    id 112
    label "miasto"
  ]
  node [
    id 113
    label "obiekt_handlowy"
  ]
  node [
    id 114
    label "area"
  ]
  node [
    id 115
    label "stoisko"
  ]
  node [
    id 116
    label "obszar"
  ]
  node [
    id 117
    label "pole_bitwy"
  ]
  node [
    id 118
    label "&#321;ubianka"
  ]
  node [
    id 119
    label "targowica"
  ]
  node [
    id 120
    label "kram"
  ]
  node [
    id 121
    label "zgromadzenie"
  ]
  node [
    id 122
    label "Majdan"
  ]
  node [
    id 123
    label "pierzeja"
  ]
  node [
    id 124
    label "oktant"
  ]
  node [
    id 125
    label "przedzielenie"
  ]
  node [
    id 126
    label "przedzieli&#263;"
  ]
  node [
    id 127
    label "przestw&#243;r"
  ]
  node [
    id 128
    label "rozdziela&#263;"
  ]
  node [
    id 129
    label "nielito&#347;ciwy"
  ]
  node [
    id 130
    label "czasoprzestrze&#324;"
  ]
  node [
    id 131
    label "niezmierzony"
  ]
  node [
    id 132
    label "bezbrze&#380;e"
  ]
  node [
    id 133
    label "rozdzielanie"
  ]
  node [
    id 134
    label "linia"
  ]
  node [
    id 135
    label "orientowa&#263;"
  ]
  node [
    id 136
    label "zorientowa&#263;"
  ]
  node [
    id 137
    label "fragment"
  ]
  node [
    id 138
    label "skr&#281;cenie"
  ]
  node [
    id 139
    label "skr&#281;ci&#263;"
  ]
  node [
    id 140
    label "internet"
  ]
  node [
    id 141
    label "obiekt"
  ]
  node [
    id 142
    label "g&#243;ra"
  ]
  node [
    id 143
    label "orientowanie"
  ]
  node [
    id 144
    label "zorientowanie"
  ]
  node [
    id 145
    label "forma"
  ]
  node [
    id 146
    label "podmiot"
  ]
  node [
    id 147
    label "ty&#322;"
  ]
  node [
    id 148
    label "logowanie"
  ]
  node [
    id 149
    label "voice"
  ]
  node [
    id 150
    label "kartka"
  ]
  node [
    id 151
    label "layout"
  ]
  node [
    id 152
    label "bok"
  ]
  node [
    id 153
    label "powierzchnia"
  ]
  node [
    id 154
    label "posta&#263;"
  ]
  node [
    id 155
    label "skr&#281;canie"
  ]
  node [
    id 156
    label "orientacja"
  ]
  node [
    id 157
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 158
    label "pagina"
  ]
  node [
    id 159
    label "uj&#281;cie"
  ]
  node [
    id 160
    label "serwis_internetowy"
  ]
  node [
    id 161
    label "adres_internetowy"
  ]
  node [
    id 162
    label "prz&#243;d"
  ]
  node [
    id 163
    label "skr&#281;ca&#263;"
  ]
  node [
    id 164
    label "plik"
  ]
  node [
    id 165
    label "spotkanie"
  ]
  node [
    id 166
    label "grusza_pospolita"
  ]
  node [
    id 167
    label "konferencyjka"
  ]
  node [
    id 168
    label "conference"
  ]
  node [
    id 169
    label "Poczdam"
  ]
  node [
    id 170
    label "Ja&#322;ta"
  ]
  node [
    id 171
    label "archiwolta"
  ]
  node [
    id 172
    label "wej&#347;cie"
  ]
  node [
    id 173
    label "obramienie"
  ]
  node [
    id 174
    label "Onet"
  ]
  node [
    id 175
    label "westwerk"
  ]
  node [
    id 176
    label "hala"
  ]
  node [
    id 177
    label "ko&#347;ci&#243;&#322;"
  ]
  node [
    id 178
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 179
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 180
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 181
    label "s&#261;downictwo"
  ]
  node [
    id 182
    label "podejrzany"
  ]
  node [
    id 183
    label "&#347;wiadek"
  ]
  node [
    id 184
    label "instytucja"
  ]
  node [
    id 185
    label "biuro"
  ]
  node [
    id 186
    label "post&#281;powanie"
  ]
  node [
    id 187
    label "court"
  ]
  node [
    id 188
    label "my&#347;l"
  ]
  node [
    id 189
    label "obrona"
  ]
  node [
    id 190
    label "system"
  ]
  node [
    id 191
    label "broni&#263;"
  ]
  node [
    id 192
    label "antylogizm"
  ]
  node [
    id 193
    label "oskar&#380;yciel"
  ]
  node [
    id 194
    label "urz&#261;d"
  ]
  node [
    id 195
    label "skazany"
  ]
  node [
    id 196
    label "konektyw"
  ]
  node [
    id 197
    label "wypowied&#378;"
  ]
  node [
    id 198
    label "bronienie"
  ]
  node [
    id 199
    label "wytw&#243;r"
  ]
  node [
    id 200
    label "pods&#261;dny"
  ]
  node [
    id 201
    label "zesp&#243;&#322;"
  ]
  node [
    id 202
    label "procesowicz"
  ]
  node [
    id 203
    label "gryzipi&#243;rek"
  ]
  node [
    id 204
    label "dziennikarz_prasowy"
  ]
  node [
    id 205
    label "grafoman"
  ]
  node [
    id 206
    label "trybik"
  ]
  node [
    id 207
    label "urz&#281;dnik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
]
