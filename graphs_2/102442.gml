graph [
  node [
    id 0
    label "igor"
    origin "text"
  ]
  node [
    id 1
    label "stokfiszewski"
    origin "text"
  ]
  node [
    id 2
    label "wolny"
    origin "text"
  ]
  node [
    id 3
    label "lektura"
    origin "text"
  ]
  node [
    id 4
    label "rzedni&#281;cie"
  ]
  node [
    id 5
    label "niespieszny"
  ]
  node [
    id 6
    label "zwalnianie_si&#281;"
  ]
  node [
    id 7
    label "wakowa&#263;"
  ]
  node [
    id 8
    label "rozwadnianie"
  ]
  node [
    id 9
    label "niezale&#380;ny"
  ]
  node [
    id 10
    label "rozwodnienie"
  ]
  node [
    id 11
    label "zrzedni&#281;cie"
  ]
  node [
    id 12
    label "swobodnie"
  ]
  node [
    id 13
    label "rozrzedzanie"
  ]
  node [
    id 14
    label "rozrzedzenie"
  ]
  node [
    id 15
    label "strza&#322;"
  ]
  node [
    id 16
    label "wolnie"
  ]
  node [
    id 17
    label "zwolnienie_si&#281;"
  ]
  node [
    id 18
    label "wolno"
  ]
  node [
    id 19
    label "lu&#378;no"
  ]
  node [
    id 20
    label "niespiesznie"
  ]
  node [
    id 21
    label "spokojny"
  ]
  node [
    id 22
    label "trafny"
  ]
  node [
    id 23
    label "shot"
  ]
  node [
    id 24
    label "przykro&#347;&#263;"
  ]
  node [
    id 25
    label "huk"
  ]
  node [
    id 26
    label "bum-bum"
  ]
  node [
    id 27
    label "pi&#322;ka"
  ]
  node [
    id 28
    label "uderzenie"
  ]
  node [
    id 29
    label "eksplozja"
  ]
  node [
    id 30
    label "wyrzut"
  ]
  node [
    id 31
    label "usi&#322;owanie"
  ]
  node [
    id 32
    label "przypadek"
  ]
  node [
    id 33
    label "shooting"
  ]
  node [
    id 34
    label "odgadywanie"
  ]
  node [
    id 35
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 36
    label "usamodzielnienie"
  ]
  node [
    id 37
    label "usamodzielnianie"
  ]
  node [
    id 38
    label "niezale&#380;nie"
  ]
  node [
    id 39
    label "thinly"
  ]
  node [
    id 40
    label "wolniej"
  ]
  node [
    id 41
    label "swobodny"
  ]
  node [
    id 42
    label "free"
  ]
  node [
    id 43
    label "lu&#378;ny"
  ]
  node [
    id 44
    label "dowolnie"
  ]
  node [
    id 45
    label "naturalnie"
  ]
  node [
    id 46
    label "rzadki"
  ]
  node [
    id 47
    label "stawanie_si&#281;"
  ]
  node [
    id 48
    label "lekko"
  ]
  node [
    id 49
    label "&#322;atwo"
  ]
  node [
    id 50
    label "odlegle"
  ]
  node [
    id 51
    label "przyjemnie"
  ]
  node [
    id 52
    label "nieformalnie"
  ]
  node [
    id 53
    label "rarefaction"
  ]
  node [
    id 54
    label "czynno&#347;&#263;"
  ]
  node [
    id 55
    label "spowodowanie"
  ]
  node [
    id 56
    label "dilution"
  ]
  node [
    id 57
    label "powodowanie"
  ]
  node [
    id 58
    label "rozcie&#324;czanie"
  ]
  node [
    id 59
    label "chrzczenie"
  ]
  node [
    id 60
    label "stanie_si&#281;"
  ]
  node [
    id 61
    label "ochrzczenie"
  ]
  node [
    id 62
    label "rozcie&#324;czenie"
  ]
  node [
    id 63
    label "stanowisko"
  ]
  node [
    id 64
    label "by&#263;"
  ]
  node [
    id 65
    label "recitation"
  ]
  node [
    id 66
    label "wczytywanie_si&#281;"
  ]
  node [
    id 67
    label "poczytanie"
  ]
  node [
    id 68
    label "doczytywanie"
  ]
  node [
    id 69
    label "zaczytanie_si&#281;"
  ]
  node [
    id 70
    label "czytywanie"
  ]
  node [
    id 71
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 72
    label "zaczytywanie_si&#281;"
  ]
  node [
    id 73
    label "tekst"
  ]
  node [
    id 74
    label "poznawanie"
  ]
  node [
    id 75
    label "wyczytywanie"
  ]
  node [
    id 76
    label "oczytywanie_si&#281;"
  ]
  node [
    id 77
    label "egzemplarz"
  ]
  node [
    id 78
    label "rozdzia&#322;"
  ]
  node [
    id 79
    label "wk&#322;ad"
  ]
  node [
    id 80
    label "tytu&#322;"
  ]
  node [
    id 81
    label "zak&#322;adka"
  ]
  node [
    id 82
    label "nomina&#322;"
  ]
  node [
    id 83
    label "ok&#322;adka"
  ]
  node [
    id 84
    label "grzbiet_ksi&#261;&#380;ki"
  ]
  node [
    id 85
    label "wydawnictwo"
  ]
  node [
    id 86
    label "ekslibris"
  ]
  node [
    id 87
    label "przek&#322;adacz"
  ]
  node [
    id 88
    label "bibliofilstwo"
  ]
  node [
    id 89
    label "falc"
  ]
  node [
    id 90
    label "pagina"
  ]
  node [
    id 91
    label "zw&#243;j"
  ]
  node [
    id 92
    label "uczenie_si&#281;"
  ]
  node [
    id 93
    label "cognition"
  ]
  node [
    id 94
    label "proces"
  ]
  node [
    id 95
    label "znajomy"
  ]
  node [
    id 96
    label "umo&#380;liwianie"
  ]
  node [
    id 97
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 98
    label "rozr&#243;&#380;nianie"
  ]
  node [
    id 99
    label "zapoznawanie"
  ]
  node [
    id 100
    label "robienie"
  ]
  node [
    id 101
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 102
    label "designation"
  ]
  node [
    id 103
    label "inclusion"
  ]
  node [
    id 104
    label "zjawisko"
  ]
  node [
    id 105
    label "czucie"
  ]
  node [
    id 106
    label "recognition"
  ]
  node [
    id 107
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 108
    label "spotykanie"
  ]
  node [
    id 109
    label "merging"
  ]
  node [
    id 110
    label "przep&#322;ywanie"
  ]
  node [
    id 111
    label "zawieranie"
  ]
  node [
    id 112
    label "ekscerpcja"
  ]
  node [
    id 113
    label "j&#281;zykowo"
  ]
  node [
    id 114
    label "wypowied&#378;"
  ]
  node [
    id 115
    label "redakcja"
  ]
  node [
    id 116
    label "wytw&#243;r"
  ]
  node [
    id 117
    label "pomini&#281;cie"
  ]
  node [
    id 118
    label "dzie&#322;o"
  ]
  node [
    id 119
    label "preparacja"
  ]
  node [
    id 120
    label "odmianka"
  ]
  node [
    id 121
    label "opu&#347;ci&#263;"
  ]
  node [
    id 122
    label "koniektura"
  ]
  node [
    id 123
    label "pisa&#263;"
  ]
  node [
    id 124
    label "obelga"
  ]
  node [
    id 125
    label "podawanie"
  ]
  node [
    id 126
    label "czytanie"
  ]
  node [
    id 127
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 128
    label "uznanie"
  ]
  node [
    id 129
    label "ko&#324;czenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
]
