graph [
  node [
    id 0
    label "czym"
    origin "text"
  ]
  node [
    id 1
    label "jeszcze"
    origin "text"
  ]
  node [
    id 2
    label "warto"
    origin "text"
  ]
  node [
    id 3
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ci&#261;gle"
  ]
  node [
    id 5
    label "nieprzerwanie"
  ]
  node [
    id 6
    label "ci&#261;g&#322;y"
  ]
  node [
    id 7
    label "stale"
  ]
  node [
    id 8
    label "give"
  ]
  node [
    id 9
    label "bonanza"
  ]
  node [
    id 10
    label "przysparza&#263;"
  ]
  node [
    id 11
    label "kali&#263;_si&#281;"
  ]
  node [
    id 12
    label "enlarge"
  ]
  node [
    id 13
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 14
    label "dodawa&#263;"
  ]
  node [
    id 15
    label "&#378;r&#243;d&#322;o_dochodu"
  ]
  node [
    id 16
    label "bieganina"
  ]
  node [
    id 17
    label "&#380;y&#322;a_z&#322;ota"
  ]
  node [
    id 18
    label "wagon"
  ]
  node [
    id 19
    label "interes"
  ]
  node [
    id 20
    label "heca"
  ]
  node [
    id 21
    label "jazda"
  ]
  node [
    id 22
    label "cognizance"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 22
  ]
]
