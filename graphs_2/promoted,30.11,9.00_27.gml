graph [
  node [
    id 0
    label "ponad"
    origin "text"
  ]
  node [
    id 1
    label "po&#322;owa"
    origin "text"
  ]
  node [
    id 2
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 3
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "cenzura"
    origin "text"
  ]
  node [
    id 5
    label "n&#281;kanie"
    origin "text"
  ]
  node [
    id 6
    label "przemoc"
    origin "text"
  ]
  node [
    id 7
    label "t&#322;umi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "debata"
    origin "text"
  ]
  node [
    id 9
    label "publiczny"
    origin "text"
  ]
  node [
    id 10
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 11
    label "medium"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "poprzedzanie"
  ]
  node [
    id 14
    label "czasoprzestrze&#324;"
  ]
  node [
    id 15
    label "laba"
  ]
  node [
    id 16
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 17
    label "chronometria"
  ]
  node [
    id 18
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 19
    label "rachuba_czasu"
  ]
  node [
    id 20
    label "przep&#322;ywanie"
  ]
  node [
    id 21
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 22
    label "czasokres"
  ]
  node [
    id 23
    label "odczyt"
  ]
  node [
    id 24
    label "chwila"
  ]
  node [
    id 25
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 26
    label "dzieje"
  ]
  node [
    id 27
    label "kategoria_gramatyczna"
  ]
  node [
    id 28
    label "poprzedzenie"
  ]
  node [
    id 29
    label "trawienie"
  ]
  node [
    id 30
    label "pochodzi&#263;"
  ]
  node [
    id 31
    label "period"
  ]
  node [
    id 32
    label "okres_czasu"
  ]
  node [
    id 33
    label "poprzedza&#263;"
  ]
  node [
    id 34
    label "schy&#322;ek"
  ]
  node [
    id 35
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 36
    label "odwlekanie_si&#281;"
  ]
  node [
    id 37
    label "zegar"
  ]
  node [
    id 38
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 39
    label "czwarty_wymiar"
  ]
  node [
    id 40
    label "pochodzenie"
  ]
  node [
    id 41
    label "koniugacja"
  ]
  node [
    id 42
    label "Zeitgeist"
  ]
  node [
    id 43
    label "trawi&#263;"
  ]
  node [
    id 44
    label "pogoda"
  ]
  node [
    id 45
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 46
    label "poprzedzi&#263;"
  ]
  node [
    id 47
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 48
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 49
    label "time_period"
  ]
  node [
    id 50
    label "Rzym_Zachodni"
  ]
  node [
    id 51
    label "whole"
  ]
  node [
    id 52
    label "ilo&#347;&#263;"
  ]
  node [
    id 53
    label "element"
  ]
  node [
    id 54
    label "Rzym_Wschodni"
  ]
  node [
    id 55
    label "urz&#261;dzenie"
  ]
  node [
    id 56
    label "&#347;rodek"
  ]
  node [
    id 57
    label "jasnowidz"
  ]
  node [
    id 58
    label "hipnoza"
  ]
  node [
    id 59
    label "cz&#322;owiek"
  ]
  node [
    id 60
    label "spirytysta"
  ]
  node [
    id 61
    label "otoczenie"
  ]
  node [
    id 62
    label "publikator"
  ]
  node [
    id 63
    label "przekazior"
  ]
  node [
    id 64
    label "warunki"
  ]
  node [
    id 65
    label "strona"
  ]
  node [
    id 66
    label "Katar"
  ]
  node [
    id 67
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 68
    label "Libia"
  ]
  node [
    id 69
    label "Gwatemala"
  ]
  node [
    id 70
    label "Afganistan"
  ]
  node [
    id 71
    label "Ekwador"
  ]
  node [
    id 72
    label "Tad&#380;ykistan"
  ]
  node [
    id 73
    label "Bhutan"
  ]
  node [
    id 74
    label "Argentyna"
  ]
  node [
    id 75
    label "D&#380;ibuti"
  ]
  node [
    id 76
    label "Wenezuela"
  ]
  node [
    id 77
    label "Ukraina"
  ]
  node [
    id 78
    label "Gabon"
  ]
  node [
    id 79
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 80
    label "Rwanda"
  ]
  node [
    id 81
    label "Liechtenstein"
  ]
  node [
    id 82
    label "organizacja"
  ]
  node [
    id 83
    label "Sri_Lanka"
  ]
  node [
    id 84
    label "Madagaskar"
  ]
  node [
    id 85
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 86
    label "Tonga"
  ]
  node [
    id 87
    label "Kongo"
  ]
  node [
    id 88
    label "Bangladesz"
  ]
  node [
    id 89
    label "Kanada"
  ]
  node [
    id 90
    label "Wehrlen"
  ]
  node [
    id 91
    label "Algieria"
  ]
  node [
    id 92
    label "Surinam"
  ]
  node [
    id 93
    label "Chile"
  ]
  node [
    id 94
    label "Sahara_Zachodnia"
  ]
  node [
    id 95
    label "Uganda"
  ]
  node [
    id 96
    label "W&#281;gry"
  ]
  node [
    id 97
    label "Birma"
  ]
  node [
    id 98
    label "Kazachstan"
  ]
  node [
    id 99
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 100
    label "Armenia"
  ]
  node [
    id 101
    label "Tuwalu"
  ]
  node [
    id 102
    label "Timor_Wschodni"
  ]
  node [
    id 103
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 104
    label "Izrael"
  ]
  node [
    id 105
    label "Estonia"
  ]
  node [
    id 106
    label "Komory"
  ]
  node [
    id 107
    label "Kamerun"
  ]
  node [
    id 108
    label "Haiti"
  ]
  node [
    id 109
    label "Belize"
  ]
  node [
    id 110
    label "Sierra_Leone"
  ]
  node [
    id 111
    label "Luksemburg"
  ]
  node [
    id 112
    label "USA"
  ]
  node [
    id 113
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 114
    label "Barbados"
  ]
  node [
    id 115
    label "San_Marino"
  ]
  node [
    id 116
    label "Bu&#322;garia"
  ]
  node [
    id 117
    label "Wietnam"
  ]
  node [
    id 118
    label "Indonezja"
  ]
  node [
    id 119
    label "Malawi"
  ]
  node [
    id 120
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 121
    label "Francja"
  ]
  node [
    id 122
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 123
    label "partia"
  ]
  node [
    id 124
    label "Zambia"
  ]
  node [
    id 125
    label "Angola"
  ]
  node [
    id 126
    label "Grenada"
  ]
  node [
    id 127
    label "Nepal"
  ]
  node [
    id 128
    label "Panama"
  ]
  node [
    id 129
    label "Rumunia"
  ]
  node [
    id 130
    label "Czarnog&#243;ra"
  ]
  node [
    id 131
    label "Malediwy"
  ]
  node [
    id 132
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 133
    label "S&#322;owacja"
  ]
  node [
    id 134
    label "para"
  ]
  node [
    id 135
    label "Egipt"
  ]
  node [
    id 136
    label "zwrot"
  ]
  node [
    id 137
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 138
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 139
    label "Kolumbia"
  ]
  node [
    id 140
    label "Mozambik"
  ]
  node [
    id 141
    label "Laos"
  ]
  node [
    id 142
    label "Burundi"
  ]
  node [
    id 143
    label "Suazi"
  ]
  node [
    id 144
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 145
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 146
    label "Czechy"
  ]
  node [
    id 147
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 148
    label "Wyspy_Marshalla"
  ]
  node [
    id 149
    label "Trynidad_i_Tobago"
  ]
  node [
    id 150
    label "Dominika"
  ]
  node [
    id 151
    label "Palau"
  ]
  node [
    id 152
    label "Syria"
  ]
  node [
    id 153
    label "Gwinea_Bissau"
  ]
  node [
    id 154
    label "Liberia"
  ]
  node [
    id 155
    label "Zimbabwe"
  ]
  node [
    id 156
    label "Polska"
  ]
  node [
    id 157
    label "Jamajka"
  ]
  node [
    id 158
    label "Dominikana"
  ]
  node [
    id 159
    label "Senegal"
  ]
  node [
    id 160
    label "Gruzja"
  ]
  node [
    id 161
    label "Togo"
  ]
  node [
    id 162
    label "Chorwacja"
  ]
  node [
    id 163
    label "Meksyk"
  ]
  node [
    id 164
    label "Macedonia"
  ]
  node [
    id 165
    label "Gujana"
  ]
  node [
    id 166
    label "Zair"
  ]
  node [
    id 167
    label "Albania"
  ]
  node [
    id 168
    label "Kambod&#380;a"
  ]
  node [
    id 169
    label "Mauritius"
  ]
  node [
    id 170
    label "Monako"
  ]
  node [
    id 171
    label "Gwinea"
  ]
  node [
    id 172
    label "Mali"
  ]
  node [
    id 173
    label "Nigeria"
  ]
  node [
    id 174
    label "Kostaryka"
  ]
  node [
    id 175
    label "Hanower"
  ]
  node [
    id 176
    label "Paragwaj"
  ]
  node [
    id 177
    label "W&#322;ochy"
  ]
  node [
    id 178
    label "Wyspy_Salomona"
  ]
  node [
    id 179
    label "Seszele"
  ]
  node [
    id 180
    label "Hiszpania"
  ]
  node [
    id 181
    label "Boliwia"
  ]
  node [
    id 182
    label "Kirgistan"
  ]
  node [
    id 183
    label "Irlandia"
  ]
  node [
    id 184
    label "Czad"
  ]
  node [
    id 185
    label "Irak"
  ]
  node [
    id 186
    label "Lesoto"
  ]
  node [
    id 187
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 188
    label "Malta"
  ]
  node [
    id 189
    label "Andora"
  ]
  node [
    id 190
    label "Chiny"
  ]
  node [
    id 191
    label "Filipiny"
  ]
  node [
    id 192
    label "Antarktis"
  ]
  node [
    id 193
    label "Niemcy"
  ]
  node [
    id 194
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 195
    label "Brazylia"
  ]
  node [
    id 196
    label "terytorium"
  ]
  node [
    id 197
    label "Nikaragua"
  ]
  node [
    id 198
    label "Pakistan"
  ]
  node [
    id 199
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 200
    label "Kenia"
  ]
  node [
    id 201
    label "Niger"
  ]
  node [
    id 202
    label "Tunezja"
  ]
  node [
    id 203
    label "Portugalia"
  ]
  node [
    id 204
    label "Fid&#380;i"
  ]
  node [
    id 205
    label "Maroko"
  ]
  node [
    id 206
    label "Botswana"
  ]
  node [
    id 207
    label "Tajlandia"
  ]
  node [
    id 208
    label "Australia"
  ]
  node [
    id 209
    label "Burkina_Faso"
  ]
  node [
    id 210
    label "interior"
  ]
  node [
    id 211
    label "Benin"
  ]
  node [
    id 212
    label "Tanzania"
  ]
  node [
    id 213
    label "Indie"
  ]
  node [
    id 214
    label "&#321;otwa"
  ]
  node [
    id 215
    label "Kiribati"
  ]
  node [
    id 216
    label "Antigua_i_Barbuda"
  ]
  node [
    id 217
    label "Rodezja"
  ]
  node [
    id 218
    label "Cypr"
  ]
  node [
    id 219
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 220
    label "Peru"
  ]
  node [
    id 221
    label "Austria"
  ]
  node [
    id 222
    label "Urugwaj"
  ]
  node [
    id 223
    label "Jordania"
  ]
  node [
    id 224
    label "Grecja"
  ]
  node [
    id 225
    label "Azerbejd&#380;an"
  ]
  node [
    id 226
    label "Turcja"
  ]
  node [
    id 227
    label "Samoa"
  ]
  node [
    id 228
    label "Sudan"
  ]
  node [
    id 229
    label "Oman"
  ]
  node [
    id 230
    label "ziemia"
  ]
  node [
    id 231
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 232
    label "Uzbekistan"
  ]
  node [
    id 233
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 234
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 235
    label "Honduras"
  ]
  node [
    id 236
    label "Mongolia"
  ]
  node [
    id 237
    label "Portoryko"
  ]
  node [
    id 238
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 239
    label "Serbia"
  ]
  node [
    id 240
    label "Tajwan"
  ]
  node [
    id 241
    label "Wielka_Brytania"
  ]
  node [
    id 242
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 243
    label "Liban"
  ]
  node [
    id 244
    label "Japonia"
  ]
  node [
    id 245
    label "Ghana"
  ]
  node [
    id 246
    label "Bahrajn"
  ]
  node [
    id 247
    label "Belgia"
  ]
  node [
    id 248
    label "Etiopia"
  ]
  node [
    id 249
    label "Mikronezja"
  ]
  node [
    id 250
    label "Kuwejt"
  ]
  node [
    id 251
    label "grupa"
  ]
  node [
    id 252
    label "Bahamy"
  ]
  node [
    id 253
    label "Rosja"
  ]
  node [
    id 254
    label "Mo&#322;dawia"
  ]
  node [
    id 255
    label "Litwa"
  ]
  node [
    id 256
    label "S&#322;owenia"
  ]
  node [
    id 257
    label "Szwajcaria"
  ]
  node [
    id 258
    label "Erytrea"
  ]
  node [
    id 259
    label "Kuba"
  ]
  node [
    id 260
    label "Arabia_Saudyjska"
  ]
  node [
    id 261
    label "granica_pa&#324;stwa"
  ]
  node [
    id 262
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 263
    label "Malezja"
  ]
  node [
    id 264
    label "Korea"
  ]
  node [
    id 265
    label "Jemen"
  ]
  node [
    id 266
    label "Nowa_Zelandia"
  ]
  node [
    id 267
    label "Namibia"
  ]
  node [
    id 268
    label "Nauru"
  ]
  node [
    id 269
    label "holoarktyka"
  ]
  node [
    id 270
    label "Brunei"
  ]
  node [
    id 271
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 272
    label "Khitai"
  ]
  node [
    id 273
    label "Mauretania"
  ]
  node [
    id 274
    label "Iran"
  ]
  node [
    id 275
    label "Gambia"
  ]
  node [
    id 276
    label "Somalia"
  ]
  node [
    id 277
    label "Holandia"
  ]
  node [
    id 278
    label "Turkmenistan"
  ]
  node [
    id 279
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 280
    label "Salwador"
  ]
  node [
    id 281
    label "pair"
  ]
  node [
    id 282
    label "zesp&#243;&#322;"
  ]
  node [
    id 283
    label "odparowywanie"
  ]
  node [
    id 284
    label "gaz_cieplarniany"
  ]
  node [
    id 285
    label "chodzi&#263;"
  ]
  node [
    id 286
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 287
    label "poker"
  ]
  node [
    id 288
    label "moneta"
  ]
  node [
    id 289
    label "parowanie"
  ]
  node [
    id 290
    label "zbi&#243;r"
  ]
  node [
    id 291
    label "damp"
  ]
  node [
    id 292
    label "nale&#380;e&#263;"
  ]
  node [
    id 293
    label "sztuka"
  ]
  node [
    id 294
    label "odparowanie"
  ]
  node [
    id 295
    label "odparowa&#263;"
  ]
  node [
    id 296
    label "dodatek"
  ]
  node [
    id 297
    label "jednostka_monetarna"
  ]
  node [
    id 298
    label "smoke"
  ]
  node [
    id 299
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 300
    label "odparowywa&#263;"
  ]
  node [
    id 301
    label "uk&#322;ad"
  ]
  node [
    id 302
    label "gaz"
  ]
  node [
    id 303
    label "wyparowanie"
  ]
  node [
    id 304
    label "obszar"
  ]
  node [
    id 305
    label "Wile&#324;szczyzna"
  ]
  node [
    id 306
    label "jednostka_administracyjna"
  ]
  node [
    id 307
    label "Jukon"
  ]
  node [
    id 308
    label "podmiot"
  ]
  node [
    id 309
    label "jednostka_organizacyjna"
  ]
  node [
    id 310
    label "struktura"
  ]
  node [
    id 311
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 312
    label "TOPR"
  ]
  node [
    id 313
    label "endecki"
  ]
  node [
    id 314
    label "przedstawicielstwo"
  ]
  node [
    id 315
    label "od&#322;am"
  ]
  node [
    id 316
    label "Cepelia"
  ]
  node [
    id 317
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 318
    label "ZBoWiD"
  ]
  node [
    id 319
    label "organization"
  ]
  node [
    id 320
    label "centrala"
  ]
  node [
    id 321
    label "GOPR"
  ]
  node [
    id 322
    label "ZOMO"
  ]
  node [
    id 323
    label "ZMP"
  ]
  node [
    id 324
    label "komitet_koordynacyjny"
  ]
  node [
    id 325
    label "przybud&#243;wka"
  ]
  node [
    id 326
    label "boj&#243;wka"
  ]
  node [
    id 327
    label "punkt"
  ]
  node [
    id 328
    label "turn"
  ]
  node [
    id 329
    label "turning"
  ]
  node [
    id 330
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 331
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 332
    label "skr&#281;t"
  ]
  node [
    id 333
    label "obr&#243;t"
  ]
  node [
    id 334
    label "fraza_czasownikowa"
  ]
  node [
    id 335
    label "jednostka_leksykalna"
  ]
  node [
    id 336
    label "zmiana"
  ]
  node [
    id 337
    label "wyra&#380;enie"
  ]
  node [
    id 338
    label "odm&#322;adzanie"
  ]
  node [
    id 339
    label "liga"
  ]
  node [
    id 340
    label "jednostka_systematyczna"
  ]
  node [
    id 341
    label "asymilowanie"
  ]
  node [
    id 342
    label "gromada"
  ]
  node [
    id 343
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 344
    label "asymilowa&#263;"
  ]
  node [
    id 345
    label "egzemplarz"
  ]
  node [
    id 346
    label "Entuzjastki"
  ]
  node [
    id 347
    label "kompozycja"
  ]
  node [
    id 348
    label "Terranie"
  ]
  node [
    id 349
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 350
    label "category"
  ]
  node [
    id 351
    label "pakiet_klimatyczny"
  ]
  node [
    id 352
    label "oddzia&#322;"
  ]
  node [
    id 353
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 354
    label "cz&#261;steczka"
  ]
  node [
    id 355
    label "stage_set"
  ]
  node [
    id 356
    label "type"
  ]
  node [
    id 357
    label "specgrupa"
  ]
  node [
    id 358
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 359
    label "&#346;wietliki"
  ]
  node [
    id 360
    label "odm&#322;odzenie"
  ]
  node [
    id 361
    label "Eurogrupa"
  ]
  node [
    id 362
    label "odm&#322;adza&#263;"
  ]
  node [
    id 363
    label "formacja_geologiczna"
  ]
  node [
    id 364
    label "harcerze_starsi"
  ]
  node [
    id 365
    label "Bund"
  ]
  node [
    id 366
    label "PPR"
  ]
  node [
    id 367
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 368
    label "wybranek"
  ]
  node [
    id 369
    label "Jakobici"
  ]
  node [
    id 370
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 371
    label "SLD"
  ]
  node [
    id 372
    label "Razem"
  ]
  node [
    id 373
    label "PiS"
  ]
  node [
    id 374
    label "package"
  ]
  node [
    id 375
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 376
    label "Kuomintang"
  ]
  node [
    id 377
    label "ZSL"
  ]
  node [
    id 378
    label "AWS"
  ]
  node [
    id 379
    label "gra"
  ]
  node [
    id 380
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 381
    label "game"
  ]
  node [
    id 382
    label "blok"
  ]
  node [
    id 383
    label "materia&#322;"
  ]
  node [
    id 384
    label "PO"
  ]
  node [
    id 385
    label "si&#322;a"
  ]
  node [
    id 386
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 387
    label "niedoczas"
  ]
  node [
    id 388
    label "Federali&#347;ci"
  ]
  node [
    id 389
    label "PSL"
  ]
  node [
    id 390
    label "Wigowie"
  ]
  node [
    id 391
    label "ZChN"
  ]
  node [
    id 392
    label "egzekutywa"
  ]
  node [
    id 393
    label "aktyw"
  ]
  node [
    id 394
    label "wybranka"
  ]
  node [
    id 395
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 396
    label "unit"
  ]
  node [
    id 397
    label "biom"
  ]
  node [
    id 398
    label "szata_ro&#347;linna"
  ]
  node [
    id 399
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 400
    label "formacja_ro&#347;linna"
  ]
  node [
    id 401
    label "przyroda"
  ]
  node [
    id 402
    label "zielono&#347;&#263;"
  ]
  node [
    id 403
    label "pi&#281;tro"
  ]
  node [
    id 404
    label "plant"
  ]
  node [
    id 405
    label "ro&#347;lina"
  ]
  node [
    id 406
    label "geosystem"
  ]
  node [
    id 407
    label "teren"
  ]
  node [
    id 408
    label "inti"
  ]
  node [
    id 409
    label "Ameryka_Po&#322;udniowa"
  ]
  node [
    id 410
    label "sol"
  ]
  node [
    id 411
    label "Afryka_Zachodnia"
  ]
  node [
    id 412
    label "baht"
  ]
  node [
    id 413
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 414
    label "boliviano"
  ]
  node [
    id 415
    label "dong"
  ]
  node [
    id 416
    label "Annam"
  ]
  node [
    id 417
    label "Tonkin"
  ]
  node [
    id 418
    label "colon"
  ]
  node [
    id 419
    label "Ameryka_Centralna"
  ]
  node [
    id 420
    label "Piemont"
  ]
  node [
    id 421
    label "P&#243;&#322;wysep_Apeni&#324;ski"
  ]
  node [
    id 422
    label "NATO"
  ]
  node [
    id 423
    label "Italia"
  ]
  node [
    id 424
    label "Kalabria"
  ]
  node [
    id 425
    label "Sardynia"
  ]
  node [
    id 426
    label "Apulia"
  ]
  node [
    id 427
    label "strefa_euro"
  ]
  node [
    id 428
    label "Ok&#281;cie"
  ]
  node [
    id 429
    label "Karyntia"
  ]
  node [
    id 430
    label "Umbria"
  ]
  node [
    id 431
    label "Romania"
  ]
  node [
    id 432
    label "Sycylia"
  ]
  node [
    id 433
    label "Warszawa"
  ]
  node [
    id 434
    label "lir"
  ]
  node [
    id 435
    label "Toskania"
  ]
  node [
    id 436
    label "Lombardia"
  ]
  node [
    id 437
    label "Liguria"
  ]
  node [
    id 438
    label "Kampania"
  ]
  node [
    id 439
    label "P&#243;&#322;wysep_Arabski"
  ]
  node [
    id 440
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 441
    label "Ad&#380;aria"
  ]
  node [
    id 442
    label "lari"
  ]
  node [
    id 443
    label "Jukatan"
  ]
  node [
    id 444
    label "dolar_Belize"
  ]
  node [
    id 445
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 446
    label "dolar"
  ]
  node [
    id 447
    label "Ohio"
  ]
  node [
    id 448
    label "P&#243;&#322;noc"
  ]
  node [
    id 449
    label "Nowy_York"
  ]
  node [
    id 450
    label "Illinois"
  ]
  node [
    id 451
    label "Po&#322;udnie"
  ]
  node [
    id 452
    label "Kalifornia"
  ]
  node [
    id 453
    label "Wirginia"
  ]
  node [
    id 454
    label "Teksas"
  ]
  node [
    id 455
    label "Waszyngton"
  ]
  node [
    id 456
    label "zielona_karta"
  ]
  node [
    id 457
    label "Massachusetts"
  ]
  node [
    id 458
    label "Alaska"
  ]
  node [
    id 459
    label "Hawaje"
  ]
  node [
    id 460
    label "Maryland"
  ]
  node [
    id 461
    label "Michigan"
  ]
  node [
    id 462
    label "Arizona"
  ]
  node [
    id 463
    label "Georgia"
  ]
  node [
    id 464
    label "stan_wolny"
  ]
  node [
    id 465
    label "Dziki_Zach&#243;d"
  ]
  node [
    id 466
    label "Pensylwania"
  ]
  node [
    id 467
    label "Luizjana"
  ]
  node [
    id 468
    label "Nowy_Meksyk"
  ]
  node [
    id 469
    label "Wuj_Sam"
  ]
  node [
    id 470
    label "Alabama"
  ]
  node [
    id 471
    label "Kansas"
  ]
  node [
    id 472
    label "Oregon"
  ]
  node [
    id 473
    label "Zach&#243;d"
  ]
  node [
    id 474
    label "Floryda"
  ]
  node [
    id 475
    label "Oklahoma"
  ]
  node [
    id 476
    label "Hudson"
  ]
  node [
    id 477
    label "Ameryka_P&#243;&#322;nocna"
  ]
  node [
    id 478
    label "somoni"
  ]
  node [
    id 479
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 480
    label "perper"
  ]
  node [
    id 481
    label "Sand&#380;ak"
  ]
  node [
    id 482
    label "P&#243;&#322;wysep_Ba&#322;ka&#324;ski"
  ]
  node [
    id 483
    label "euro"
  ]
  node [
    id 484
    label "Bengal"
  ]
  node [
    id 485
    label "taka"
  ]
  node [
    id 486
    label "Karelia"
  ]
  node [
    id 487
    label "Mari_El"
  ]
  node [
    id 488
    label "Inguszetia"
  ]
  node [
    id 489
    label "Podkamienna_Tunguzka"
  ]
  node [
    id 490
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 491
    label "Udmurcja"
  ]
  node [
    id 492
    label "Newa"
  ]
  node [
    id 493
    label "&#321;adoga"
  ]
  node [
    id 494
    label "Czeczenia"
  ]
  node [
    id 495
    label "Anadyr"
  ]
  node [
    id 496
    label "Syberia"
  ]
  node [
    id 497
    label "Tatarstan"
  ]
  node [
    id 498
    label "Wszechrosja"
  ]
  node [
    id 499
    label "Azja"
  ]
  node [
    id 500
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 501
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 502
    label "Wyspa_So&#322;owiecka"
  ]
  node [
    id 503
    label "P&#322;askowy&#380;_Witimski"
  ]
  node [
    id 504
    label "Europa_Wschodnia"
  ]
  node [
    id 505
    label "Witim"
  ]
  node [
    id 506
    label "Kamczatka"
  ]
  node [
    id 507
    label "Jama&#322;"
  ]
  node [
    id 508
    label "Dagestan"
  ]
  node [
    id 509
    label "Baszkiria"
  ]
  node [
    id 510
    label "Tuwa"
  ]
  node [
    id 511
    label "car"
  ]
  node [
    id 512
    label "Komi"
  ]
  node [
    id 513
    label "Czuwaszja"
  ]
  node [
    id 514
    label "Chakasja"
  ]
  node [
    id 515
    label "Perm"
  ]
  node [
    id 516
    label "obw&#243;d_kaliningradzki"
  ]
  node [
    id 517
    label "Ajon"
  ]
  node [
    id 518
    label "Adygeja"
  ]
  node [
    id 519
    label "Dniepr"
  ]
  node [
    id 520
    label "rubel_rosyjski"
  ]
  node [
    id 521
    label "Don"
  ]
  node [
    id 522
    label "Mordowia"
  ]
  node [
    id 523
    label "s&#322;owianofilstwo"
  ]
  node [
    id 524
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 525
    label "gourde"
  ]
  node [
    id 526
    label "Karaiby"
  ]
  node [
    id 527
    label "escudo_angolskie"
  ]
  node [
    id 528
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 529
    label "kwanza"
  ]
  node [
    id 530
    label "ariary"
  ]
  node [
    id 531
    label "Ocean_Indyjski"
  ]
  node [
    id 532
    label "Afryka_Wschodnia"
  ]
  node [
    id 533
    label "frank_malgaski"
  ]
  node [
    id 534
    label "Unia_Europejska"
  ]
  node [
    id 535
    label "Windawa"
  ]
  node [
    id 536
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 537
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 538
    label "&#379;mud&#378;"
  ]
  node [
    id 539
    label "lit"
  ]
  node [
    id 540
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 541
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 542
    label "Synaj"
  ]
  node [
    id 543
    label "paraszyt"
  ]
  node [
    id 544
    label "funt_egipski"
  ]
  node [
    id 545
    label "Amhara"
  ]
  node [
    id 546
    label "birr"
  ]
  node [
    id 547
    label "Syjon"
  ]
  node [
    id 548
    label "negus"
  ]
  node [
    id 549
    label "peso_kolumbijskie"
  ]
  node [
    id 550
    label "Orinoko"
  ]
  node [
    id 551
    label "rial_katarski"
  ]
  node [
    id 552
    label "dram"
  ]
  node [
    id 553
    label "Limburgia"
  ]
  node [
    id 554
    label "gulden"
  ]
  node [
    id 555
    label "Zelandia"
  ]
  node [
    id 556
    label "Niderlandy"
  ]
  node [
    id 557
    label "Brabancja"
  ]
  node [
    id 558
    label "cedi"
  ]
  node [
    id 559
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 560
    label "milrejs"
  ]
  node [
    id 561
    label "cruzado"
  ]
  node [
    id 562
    label "real"
  ]
  node [
    id 563
    label "frank_monakijski"
  ]
  node [
    id 564
    label "Fryburg"
  ]
  node [
    id 565
    label "Bazylea"
  ]
  node [
    id 566
    label "Alpy"
  ]
  node [
    id 567
    label "frank_szwajcarski"
  ]
  node [
    id 568
    label "Helwecja"
  ]
  node [
    id 569
    label "Europa_Zachodnia"
  ]
  node [
    id 570
    label "Berno"
  ]
  node [
    id 571
    label "Ba&#322;kany"
  ]
  node [
    id 572
    label "lej_mo&#322;dawski"
  ]
  node [
    id 573
    label "Naddniestrze"
  ]
  node [
    id 574
    label "Dniestr"
  ]
  node [
    id 575
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 576
    label "Gagauzja"
  ]
  node [
    id 577
    label "Indie_Zachodnie"
  ]
  node [
    id 578
    label "Sikkim"
  ]
  node [
    id 579
    label "Asam"
  ]
  node [
    id 580
    label "Kaszmir"
  ]
  node [
    id 581
    label "rupia_indyjska"
  ]
  node [
    id 582
    label "Indie_Portugalskie"
  ]
  node [
    id 583
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 584
    label "Indie_Wschodnie"
  ]
  node [
    id 585
    label "Kerala"
  ]
  node [
    id 586
    label "Bollywood"
  ]
  node [
    id 587
    label "Pend&#380;ab"
  ]
  node [
    id 588
    label "boliwar"
  ]
  node [
    id 589
    label "naira"
  ]
  node [
    id 590
    label "frank_gwinejski"
  ]
  node [
    id 591
    label "sum"
  ]
  node [
    id 592
    label "Karaka&#322;pacja"
  ]
  node [
    id 593
    label "dolar_liberyjski"
  ]
  node [
    id 594
    label "Dacja"
  ]
  node [
    id 595
    label "lej_rumu&#324;ski"
  ]
  node [
    id 596
    label "Siedmiogr&#243;d"
  ]
  node [
    id 597
    label "Dobrud&#380;a"
  ]
  node [
    id 598
    label "Wo&#322;oszczyzna"
  ]
  node [
    id 599
    label "dolar_namibijski"
  ]
  node [
    id 600
    label "kuna"
  ]
  node [
    id 601
    label "Rugia"
  ]
  node [
    id 602
    label "Saksonia"
  ]
  node [
    id 603
    label "Dolna_Saksonia"
  ]
  node [
    id 604
    label "Anglosas"
  ]
  node [
    id 605
    label "Hesja"
  ]
  node [
    id 606
    label "Szlezwik"
  ]
  node [
    id 607
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 608
    label "Wirtembergia"
  ]
  node [
    id 609
    label "Po&#322;abie"
  ]
  node [
    id 610
    label "Germania"
  ]
  node [
    id 611
    label "Frankonia"
  ]
  node [
    id 612
    label "Badenia"
  ]
  node [
    id 613
    label "Holsztyn"
  ]
  node [
    id 614
    label "Bawaria"
  ]
  node [
    id 615
    label "marka"
  ]
  node [
    id 616
    label "Brandenburgia"
  ]
  node [
    id 617
    label "Szwabia"
  ]
  node [
    id 618
    label "Niemcy_Zachodnie"
  ]
  node [
    id 619
    label "Nadrenia"
  ]
  node [
    id 620
    label "Westfalia"
  ]
  node [
    id 621
    label "Turyngia"
  ]
  node [
    id 622
    label "Helgoland"
  ]
  node [
    id 623
    label "Karlsbad"
  ]
  node [
    id 624
    label "Niemcy_Wschodnie"
  ]
  node [
    id 625
    label "korona_w&#281;gierska"
  ]
  node [
    id 626
    label "forint"
  ]
  node [
    id 627
    label "Lipt&#243;w"
  ]
  node [
    id 628
    label "tenge"
  ]
  node [
    id 629
    label "szach"
  ]
  node [
    id 630
    label "Baktria"
  ]
  node [
    id 631
    label "afgani"
  ]
  node [
    id 632
    label "kip"
  ]
  node [
    id 633
    label "P&#243;&#322;wysep_Indochi&#324;ski"
  ]
  node [
    id 634
    label "Salzburg"
  ]
  node [
    id 635
    label "Rakuzy"
  ]
  node [
    id 636
    label "Dyja"
  ]
  node [
    id 637
    label "Tyrol"
  ]
  node [
    id 638
    label "konsulent"
  ]
  node [
    id 639
    label "szyling_austryjacki"
  ]
  node [
    id 640
    label "peso_urugwajskie"
  ]
  node [
    id 641
    label "rial_jeme&#324;ski"
  ]
  node [
    id 642
    label "korona_esto&#324;ska"
  ]
  node [
    id 643
    label "Skandynawia"
  ]
  node [
    id 644
    label "Inflanty"
  ]
  node [
    id 645
    label "marka_esto&#324;ska"
  ]
  node [
    id 646
    label "Polinezja"
  ]
  node [
    id 647
    label "tala"
  ]
  node [
    id 648
    label "Oceania"
  ]
  node [
    id 649
    label "Podole"
  ]
  node [
    id 650
    label "Ukraina_Zachodnia"
  ]
  node [
    id 651
    label "Wsch&#243;d"
  ]
  node [
    id 652
    label "Zakarpacie"
  ]
  node [
    id 653
    label "Naddnieprze"
  ]
  node [
    id 654
    label "Ma&#322;orosja"
  ]
  node [
    id 655
    label "Wo&#322;y&#324;"
  ]
  node [
    id 656
    label "Nadbu&#380;e"
  ]
  node [
    id 657
    label "hrywna"
  ]
  node [
    id 658
    label "Zaporo&#380;e"
  ]
  node [
    id 659
    label "Krym"
  ]
  node [
    id 660
    label "Przykarpacie"
  ]
  node [
    id 661
    label "Kozaczyzna"
  ]
  node [
    id 662
    label "karbowaniec"
  ]
  node [
    id 663
    label "riel"
  ]
  node [
    id 664
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 665
    label "peso_meksyka&#324;skie"
  ]
  node [
    id 666
    label "kyat"
  ]
  node [
    id 667
    label "Arakan"
  ]
  node [
    id 668
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 669
    label "funt_liba&#324;ski"
  ]
  node [
    id 670
    label "Mariany"
  ]
  node [
    id 671
    label "j&#281;zyk_kreolski_seszelski"
  ]
  node [
    id 672
    label "Maghreb"
  ]
  node [
    id 673
    label "Arabska_Unia_Maghrebu"
  ]
  node [
    id 674
    label "dinar_algierski"
  ]
  node [
    id 675
    label "Kabylia"
  ]
  node [
    id 676
    label "ringgit"
  ]
  node [
    id 677
    label "P&#243;&#322;wysep_Malajski"
  ]
  node [
    id 678
    label "Borneo"
  ]
  node [
    id 679
    label "peso_dominika&#324;skie"
  ]
  node [
    id 680
    label "peso_kuba&#324;skie_wymienialne"
  ]
  node [
    id 681
    label "peso_kuba&#324;skie"
  ]
  node [
    id 682
    label "lira_izraelska"
  ]
  node [
    id 683
    label "szekel"
  ]
  node [
    id 684
    label "Galilea"
  ]
  node [
    id 685
    label "Judea"
  ]
  node [
    id 686
    label "tolar"
  ]
  node [
    id 687
    label "frank_luksemburski"
  ]
  node [
    id 688
    label "lempira"
  ]
  node [
    id 689
    label "Pozna&#324;"
  ]
  node [
    id 690
    label "lira_malta&#324;ska"
  ]
  node [
    id 691
    label "Gozo"
  ]
  node [
    id 692
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 693
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 694
    label "Paros"
  ]
  node [
    id 695
    label "Epir"
  ]
  node [
    id 696
    label "panhellenizm"
  ]
  node [
    id 697
    label "Eubea"
  ]
  node [
    id 698
    label "Rodos"
  ]
  node [
    id 699
    label "Achaja"
  ]
  node [
    id 700
    label "Termopile"
  ]
  node [
    id 701
    label "Attyka"
  ]
  node [
    id 702
    label "Hellada"
  ]
  node [
    id 703
    label "Etolia"
  ]
  node [
    id 704
    label "palestra"
  ]
  node [
    id 705
    label "Kreta"
  ]
  node [
    id 706
    label "drachma"
  ]
  node [
    id 707
    label "Olimp"
  ]
  node [
    id 708
    label "Tesalia"
  ]
  node [
    id 709
    label "Peloponez"
  ]
  node [
    id 710
    label "Eolia"
  ]
  node [
    id 711
    label "Beocja"
  ]
  node [
    id 712
    label "Parnas"
  ]
  node [
    id 713
    label "Lesbos"
  ]
  node [
    id 714
    label "Atlantyk"
  ]
  node [
    id 715
    label "Irlandia_P&#243;&#322;nocna"
  ]
  node [
    id 716
    label "Ulster"
  ]
  node [
    id 717
    label "funt_irlandzki"
  ]
  node [
    id 718
    label "tugrik"
  ]
  node [
    id 719
    label "Azja_Wschodnia"
  ]
  node [
    id 720
    label "Buriaci"
  ]
  node [
    id 721
    label "ajmak"
  ]
  node [
    id 722
    label "denar_macedo&#324;ski"
  ]
  node [
    id 723
    label "Lotaryngia"
  ]
  node [
    id 724
    label "Bordeaux"
  ]
  node [
    id 725
    label "Pikardia"
  ]
  node [
    id 726
    label "Alzacja"
  ]
  node [
    id 727
    label "Masyw_Centralny"
  ]
  node [
    id 728
    label "Akwitania"
  ]
  node [
    id 729
    label "Sekwana"
  ]
  node [
    id 730
    label "Langwedocja"
  ]
  node [
    id 731
    label "Armagnac"
  ]
  node [
    id 732
    label "Martynika"
  ]
  node [
    id 733
    label "Bretania"
  ]
  node [
    id 734
    label "Sabaudia"
  ]
  node [
    id 735
    label "Korsyka"
  ]
  node [
    id 736
    label "Normandia"
  ]
  node [
    id 737
    label "Gaskonia"
  ]
  node [
    id 738
    label "Burgundia"
  ]
  node [
    id 739
    label "frank_francuski"
  ]
  node [
    id 740
    label "Wandea"
  ]
  node [
    id 741
    label "Prowansja"
  ]
  node [
    id 742
    label "Gwadelupa"
  ]
  node [
    id 743
    label "lew"
  ]
  node [
    id 744
    label "c&#243;rdoba"
  ]
  node [
    id 745
    label "dolar_Zimbabwe"
  ]
  node [
    id 746
    label "frank_rwandyjski"
  ]
  node [
    id 747
    label "kwacha_zambijska"
  ]
  node [
    id 748
    label "Kurlandia"
  ]
  node [
    id 749
    label "&#322;at"
  ]
  node [
    id 750
    label "Liwonia"
  ]
  node [
    id 751
    label "rubel_&#322;otewski"
  ]
  node [
    id 752
    label "Himalaje"
  ]
  node [
    id 753
    label "rupia_nepalska"
  ]
  node [
    id 754
    label "funt_suda&#324;ski"
  ]
  node [
    id 755
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 756
    label "dolar_bahamski"
  ]
  node [
    id 757
    label "Wielka_Bahama"
  ]
  node [
    id 758
    label "Mazowsze"
  ]
  node [
    id 759
    label "Pa&#322;uki"
  ]
  node [
    id 760
    label "Pomorze_Zachodnie"
  ]
  node [
    id 761
    label "Powi&#347;le"
  ]
  node [
    id 762
    label "Wolin"
  ]
  node [
    id 763
    label "z&#322;oty"
  ]
  node [
    id 764
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 765
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 766
    label "So&#322;a"
  ]
  node [
    id 767
    label "Krajna"
  ]
  node [
    id 768
    label "Opolskie"
  ]
  node [
    id 769
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 770
    label "Suwalszczyzna"
  ]
  node [
    id 771
    label "barwy_polskie"
  ]
  node [
    id 772
    label "Podlasie"
  ]
  node [
    id 773
    label "Izera"
  ]
  node [
    id 774
    label "Ma&#322;opolska"
  ]
  node [
    id 775
    label "Warmia"
  ]
  node [
    id 776
    label "Mazury"
  ]
  node [
    id 777
    label "Ziemia_Che&#322;mi&#324;ska"
  ]
  node [
    id 778
    label "Kaczawa"
  ]
  node [
    id 779
    label "Lubelszczyzna"
  ]
  node [
    id 780
    label "Ziemia_Dobrzy&#324;ska"
  ]
  node [
    id 781
    label "Kielecczyzna"
  ]
  node [
    id 782
    label "Lubuskie"
  ]
  node [
    id 783
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 784
    label "&#321;&#243;dzkie"
  ]
  node [
    id 785
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 786
    label "Kujawy"
  ]
  node [
    id 787
    label "Podkarpacie"
  ]
  node [
    id 788
    label "Wielkopolska"
  ]
  node [
    id 789
    label "Wis&#322;a"
  ]
  node [
    id 790
    label "ziemia_k&#322;odzka"
  ]
  node [
    id 791
    label "Bory_Tucholskie"
  ]
  node [
    id 792
    label "Antyle"
  ]
  node [
    id 793
    label "dolar_Tuvalu"
  ]
  node [
    id 794
    label "dinar_iracki"
  ]
  node [
    id 795
    label "korona_s&#322;owacka"
  ]
  node [
    id 796
    label "Turiec"
  ]
  node [
    id 797
    label "jen"
  ]
  node [
    id 798
    label "jinja"
  ]
  node [
    id 799
    label "Okinawa"
  ]
  node [
    id 800
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 801
    label "Japonica"
  ]
  node [
    id 802
    label "manat_turkme&#324;ski"
  ]
  node [
    id 803
    label "szyling_kenijski"
  ]
  node [
    id 804
    label "peso_chilijskie"
  ]
  node [
    id 805
    label "Zanzibar"
  ]
  node [
    id 806
    label "szyling_tanza&#324;ski"
  ]
  node [
    id 807
    label "peso_filipi&#324;skie"
  ]
  node [
    id 808
    label "Cebu"
  ]
  node [
    id 809
    label "Sahara"
  ]
  node [
    id 810
    label "Tasmania"
  ]
  node [
    id 811
    label "Nowy_&#346;wiat"
  ]
  node [
    id 812
    label "dolar_australijski"
  ]
  node [
    id 813
    label "Quebec"
  ]
  node [
    id 814
    label "dolar_kanadyjski"
  ]
  node [
    id 815
    label "Nowa_Fundlandia"
  ]
  node [
    id 816
    label "quetzal"
  ]
  node [
    id 817
    label "Manica"
  ]
  node [
    id 818
    label "escudo_mozambickie"
  ]
  node [
    id 819
    label "Cabo_Delgado"
  ]
  node [
    id 820
    label "Inhambane"
  ]
  node [
    id 821
    label "Maputo"
  ]
  node [
    id 822
    label "Gaza"
  ]
  node [
    id 823
    label "Niasa"
  ]
  node [
    id 824
    label "Nampula"
  ]
  node [
    id 825
    label "metical"
  ]
  node [
    id 826
    label "frank_tunezyjski"
  ]
  node [
    id 827
    label "dinar_tunezyjski"
  ]
  node [
    id 828
    label "lud"
  ]
  node [
    id 829
    label "frank_kongijski"
  ]
  node [
    id 830
    label "peso_argenty&#324;skie"
  ]
  node [
    id 831
    label "dinar_Bahrajnu"
  ]
  node [
    id 832
    label "P&#243;&#322;wysep_Iberyjski"
  ]
  node [
    id 833
    label "escudo_portugalskie"
  ]
  node [
    id 834
    label "Melanezja"
  ]
  node [
    id 835
    label "dolar_Fid&#380;i"
  ]
  node [
    id 836
    label "d&#380;amahirijja"
  ]
  node [
    id 837
    label "dinar_libijski"
  ]
  node [
    id 838
    label "balboa"
  ]
  node [
    id 839
    label "dolar_surinamski"
  ]
  node [
    id 840
    label "dolar_Brunei"
  ]
  node [
    id 841
    label "Estremadura"
  ]
  node [
    id 842
    label "Andaluzja"
  ]
  node [
    id 843
    label "Kastylia"
  ]
  node [
    id 844
    label "Galicja"
  ]
  node [
    id 845
    label "Aragonia"
  ]
  node [
    id 846
    label "hacjender"
  ]
  node [
    id 847
    label "Asturia"
  ]
  node [
    id 848
    label "Baskonia"
  ]
  node [
    id 849
    label "Majorka"
  ]
  node [
    id 850
    label "Walencja"
  ]
  node [
    id 851
    label "peseta"
  ]
  node [
    id 852
    label "Katalonia"
  ]
  node [
    id 853
    label "Luksemburgia"
  ]
  node [
    id 854
    label "frank_belgijski"
  ]
  node [
    id 855
    label "Walonia"
  ]
  node [
    id 856
    label "Flandria"
  ]
  node [
    id 857
    label "dolar_guja&#324;ski"
  ]
  node [
    id 858
    label "dolar_Barbadosu"
  ]
  node [
    id 859
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 860
    label "korona_czeska"
  ]
  node [
    id 861
    label "Lasko"
  ]
  node [
    id 862
    label "dinar_jorda&#324;ski"
  ]
  node [
    id 863
    label "Wojwodina"
  ]
  node [
    id 864
    label "dinar_serbski"
  ]
  node [
    id 865
    label "funt_syryjski"
  ]
  node [
    id 866
    label "alawizm"
  ]
  node [
    id 867
    label "Szantung"
  ]
  node [
    id 868
    label "Chiny_Zachodnie"
  ]
  node [
    id 869
    label "Kuantung"
  ]
  node [
    id 870
    label "D&#380;ungaria"
  ]
  node [
    id 871
    label "yuan"
  ]
  node [
    id 872
    label "Hongkong"
  ]
  node [
    id 873
    label "Chiny_Wschodnie"
  ]
  node [
    id 874
    label "Guangdong"
  ]
  node [
    id 875
    label "Junnan"
  ]
  node [
    id 876
    label "Mand&#380;uria"
  ]
  node [
    id 877
    label "Syczuan"
  ]
  node [
    id 878
    label "zair"
  ]
  node [
    id 879
    label "Katanga"
  ]
  node [
    id 880
    label "ugija"
  ]
  node [
    id 881
    label "dalasi"
  ]
  node [
    id 882
    label "funt_cypryjski"
  ]
  node [
    id 883
    label "Afrodyzje"
  ]
  node [
    id 884
    label "frank_alba&#324;ski"
  ]
  node [
    id 885
    label "lek"
  ]
  node [
    id 886
    label "rupia_pakista&#324;ska"
  ]
  node [
    id 887
    label "kafar"
  ]
  node [
    id 888
    label "dolar_jamajski"
  ]
  node [
    id 889
    label "Ocean_Spokojny"
  ]
  node [
    id 890
    label "dolar_tajwa&#324;ski"
  ]
  node [
    id 891
    label "som"
  ]
  node [
    id 892
    label "guarani"
  ]
  node [
    id 893
    label "rial_ira&#324;ski"
  ]
  node [
    id 894
    label "mu&#322;&#322;a"
  ]
  node [
    id 895
    label "Persja"
  ]
  node [
    id 896
    label "Jawa"
  ]
  node [
    id 897
    label "Sumatra"
  ]
  node [
    id 898
    label "rupia_indonezyjska"
  ]
  node [
    id 899
    label "Nowa_Gwinea"
  ]
  node [
    id 900
    label "Moluki"
  ]
  node [
    id 901
    label "szyling_somalijski"
  ]
  node [
    id 902
    label "szyling_ugandyjski"
  ]
  node [
    id 903
    label "dirham_maroka&#324;ski"
  ]
  node [
    id 904
    label "lira_turecka"
  ]
  node [
    id 905
    label "Azja_Mniejsza"
  ]
  node [
    id 906
    label "Ujgur"
  ]
  node [
    id 907
    label "Pireneje"
  ]
  node [
    id 908
    label "nakfa"
  ]
  node [
    id 909
    label "won"
  ]
  node [
    id 910
    label "rubel_bia&#322;oruski"
  ]
  node [
    id 911
    label "&#346;wite&#378;"
  ]
  node [
    id 912
    label "dinar_kuwejcki"
  ]
  node [
    id 913
    label "Nachiczewan"
  ]
  node [
    id 914
    label "manat_azerski"
  ]
  node [
    id 915
    label "Karabach"
  ]
  node [
    id 916
    label "dolar_Kiribati"
  ]
  node [
    id 917
    label "Anglia"
  ]
  node [
    id 918
    label "Amazonia"
  ]
  node [
    id 919
    label "plantowa&#263;"
  ]
  node [
    id 920
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 921
    label "zapadnia"
  ]
  node [
    id 922
    label "Zamojszczyzna"
  ]
  node [
    id 923
    label "budynek"
  ]
  node [
    id 924
    label "skorupa_ziemska"
  ]
  node [
    id 925
    label "Turkiestan"
  ]
  node [
    id 926
    label "Noworosja"
  ]
  node [
    id 927
    label "Mezoameryka"
  ]
  node [
    id 928
    label "glinowanie"
  ]
  node [
    id 929
    label "Kurdystan"
  ]
  node [
    id 930
    label "martwica"
  ]
  node [
    id 931
    label "Szkocja"
  ]
  node [
    id 932
    label "litosfera"
  ]
  node [
    id 933
    label "penetrator"
  ]
  node [
    id 934
    label "glinowa&#263;"
  ]
  node [
    id 935
    label "Zabajkale"
  ]
  node [
    id 936
    label "domain"
  ]
  node [
    id 937
    label "Bojkowszczyzna"
  ]
  node [
    id 938
    label "podglebie"
  ]
  node [
    id 939
    label "kompleks_sorpcyjny"
  ]
  node [
    id 940
    label "Pamir"
  ]
  node [
    id 941
    label "Indochiny"
  ]
  node [
    id 942
    label "miejsce"
  ]
  node [
    id 943
    label "Kurpie"
  ]
  node [
    id 944
    label "S&#261;decczyzna"
  ]
  node [
    id 945
    label "kort"
  ]
  node [
    id 946
    label "czynnik_produkcji"
  ]
  node [
    id 947
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 948
    label "Huculszczyzna"
  ]
  node [
    id 949
    label "pojazd"
  ]
  node [
    id 950
    label "powierzchnia"
  ]
  node [
    id 951
    label "Podhale"
  ]
  node [
    id 952
    label "pr&#243;chnica"
  ]
  node [
    id 953
    label "Hercegowina"
  ]
  node [
    id 954
    label "Walia"
  ]
  node [
    id 955
    label "pomieszczenie"
  ]
  node [
    id 956
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 957
    label "ryzosfera"
  ]
  node [
    id 958
    label "Kaukaz"
  ]
  node [
    id 959
    label "Biskupizna"
  ]
  node [
    id 960
    label "Bo&#347;nia"
  ]
  node [
    id 961
    label "p&#322;aszczyzna"
  ]
  node [
    id 962
    label "dotleni&#263;"
  ]
  node [
    id 963
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 964
    label "Podbeskidzie"
  ]
  node [
    id 965
    label "&#321;emkowszczyzna"
  ]
  node [
    id 966
    label "Opolszczyzna"
  ]
  node [
    id 967
    label "Kaszuby"
  ]
  node [
    id 968
    label "Ko&#322;yma"
  ]
  node [
    id 969
    label "glej"
  ]
  node [
    id 970
    label "posadzka"
  ]
  node [
    id 971
    label "Polesie"
  ]
  node [
    id 972
    label "Palestyna"
  ]
  node [
    id 973
    label "Lauda"
  ]
  node [
    id 974
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 975
    label "Laponia"
  ]
  node [
    id 976
    label "Yorkshire"
  ]
  node [
    id 977
    label "Zag&#243;rze"
  ]
  node [
    id 978
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 979
    label "&#379;ywiecczyzna"
  ]
  node [
    id 980
    label "Oksytania"
  ]
  node [
    id 981
    label "przestrze&#324;"
  ]
  node [
    id 982
    label "Kociewie"
  ]
  node [
    id 983
    label "korzysta&#263;"
  ]
  node [
    id 984
    label "liga&#263;"
  ]
  node [
    id 985
    label "give"
  ]
  node [
    id 986
    label "distribute"
  ]
  node [
    id 987
    label "u&#380;ywa&#263;"
  ]
  node [
    id 988
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 989
    label "use"
  ]
  node [
    id 990
    label "krzywdzi&#263;"
  ]
  node [
    id 991
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 992
    label "uzyskiwa&#263;"
  ]
  node [
    id 993
    label "bash"
  ]
  node [
    id 994
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 995
    label "doznawa&#263;"
  ]
  node [
    id 996
    label "robi&#263;"
  ]
  node [
    id 997
    label "ukrzywdza&#263;"
  ]
  node [
    id 998
    label "niesprawiedliwy"
  ]
  node [
    id 999
    label "szkodzi&#263;"
  ]
  node [
    id 1000
    label "powodowa&#263;"
  ]
  node [
    id 1001
    label "wrong"
  ]
  node [
    id 1002
    label "copulate"
  ]
  node [
    id 1003
    label "&#380;y&#263;"
  ]
  node [
    id 1004
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 1005
    label "wierzga&#263;"
  ]
  node [
    id 1006
    label "urz&#261;d"
  ]
  node [
    id 1007
    label "&#347;wiadectwo"
  ]
  node [
    id 1008
    label "proces"
  ]
  node [
    id 1009
    label "drugi_obieg"
  ]
  node [
    id 1010
    label "zdejmowanie"
  ]
  node [
    id 1011
    label "bell_ringer"
  ]
  node [
    id 1012
    label "krytyka"
  ]
  node [
    id 1013
    label "crisscross"
  ]
  node [
    id 1014
    label "p&#243;&#322;kownik"
  ]
  node [
    id 1015
    label "ekskomunikowa&#263;"
  ]
  node [
    id 1016
    label "kontrola"
  ]
  node [
    id 1017
    label "mark"
  ]
  node [
    id 1018
    label "zdejmowa&#263;"
  ]
  node [
    id 1019
    label "zjawisko"
  ]
  node [
    id 1020
    label "zdj&#281;cie"
  ]
  node [
    id 1021
    label "zdj&#261;&#263;"
  ]
  node [
    id 1022
    label "kara"
  ]
  node [
    id 1023
    label "ekskomunikowanie"
  ]
  node [
    id 1024
    label "dow&#243;d"
  ]
  node [
    id 1025
    label "o&#347;wiadczenie"
  ]
  node [
    id 1026
    label "za&#347;wiadczenie"
  ]
  node [
    id 1027
    label "certificate"
  ]
  node [
    id 1028
    label "promocja"
  ]
  node [
    id 1029
    label "dokument"
  ]
  node [
    id 1030
    label "kognicja"
  ]
  node [
    id 1031
    label "przebieg"
  ]
  node [
    id 1032
    label "rozprawa"
  ]
  node [
    id 1033
    label "wydarzenie"
  ]
  node [
    id 1034
    label "legislacyjnie"
  ]
  node [
    id 1035
    label "przes&#322;anka"
  ]
  node [
    id 1036
    label "nast&#281;pstwo"
  ]
  node [
    id 1037
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 1038
    label "boski"
  ]
  node [
    id 1039
    label "krajobraz"
  ]
  node [
    id 1040
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 1041
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 1042
    label "przywidzenie"
  ]
  node [
    id 1043
    label "presence"
  ]
  node [
    id 1044
    label "charakter"
  ]
  node [
    id 1045
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 1046
    label "kwota"
  ]
  node [
    id 1047
    label "nemezis"
  ]
  node [
    id 1048
    label "konsekwencja"
  ]
  node [
    id 1049
    label "punishment"
  ]
  node [
    id 1050
    label "klacz"
  ]
  node [
    id 1051
    label "forfeit"
  ]
  node [
    id 1052
    label "roboty_przymusowe"
  ]
  node [
    id 1053
    label "legalizacja_ponowna"
  ]
  node [
    id 1054
    label "instytucja"
  ]
  node [
    id 1055
    label "w&#322;adza"
  ]
  node [
    id 1056
    label "perlustracja"
  ]
  node [
    id 1057
    label "czynno&#347;&#263;"
  ]
  node [
    id 1058
    label "legalizacja_pierwotna"
  ]
  node [
    id 1059
    label "examination"
  ]
  node [
    id 1060
    label "stanowisko"
  ]
  node [
    id 1061
    label "position"
  ]
  node [
    id 1062
    label "siedziba"
  ]
  node [
    id 1063
    label "organ"
  ]
  node [
    id 1064
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1065
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1066
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1067
    label "mianowaniec"
  ]
  node [
    id 1068
    label "dzia&#322;"
  ]
  node [
    id 1069
    label "okienko"
  ]
  node [
    id 1070
    label "wykl&#261;&#263;"
  ]
  node [
    id 1071
    label "wyklina&#263;"
  ]
  node [
    id 1072
    label "wyklinanie"
  ]
  node [
    id 1073
    label "wykl&#281;cie"
  ]
  node [
    id 1074
    label "film"
  ]
  node [
    id 1075
    label "bra&#263;"
  ]
  node [
    id 1076
    label "uwalnia&#263;"
  ]
  node [
    id 1077
    label "seclude"
  ]
  node [
    id 1078
    label "take"
  ]
  node [
    id 1079
    label "snap"
  ]
  node [
    id 1080
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 1081
    label "abstract"
  ]
  node [
    id 1082
    label "zabrania&#263;"
  ]
  node [
    id 1083
    label "odsuwa&#263;"
  ]
  node [
    id 1084
    label "fotogaleria"
  ]
  node [
    id 1085
    label "retuszowanie"
  ]
  node [
    id 1086
    label "uwolnienie"
  ]
  node [
    id 1087
    label "cinch"
  ]
  node [
    id 1088
    label "obraz"
  ]
  node [
    id 1089
    label "monid&#322;o"
  ]
  node [
    id 1090
    label "fota"
  ]
  node [
    id 1091
    label "zabronienie"
  ]
  node [
    id 1092
    label "odsuni&#281;cie"
  ]
  node [
    id 1093
    label "fototeka"
  ]
  node [
    id 1094
    label "przepa&#322;"
  ]
  node [
    id 1095
    label "podlew"
  ]
  node [
    id 1096
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 1097
    label "relief"
  ]
  node [
    id 1098
    label "wyretuszowa&#263;"
  ]
  node [
    id 1099
    label "rozpakowanie"
  ]
  node [
    id 1100
    label "legitymacja"
  ]
  node [
    id 1101
    label "wyretuszowanie"
  ]
  node [
    id 1102
    label "talbotypia"
  ]
  node [
    id 1103
    label "abolicjonista"
  ]
  node [
    id 1104
    label "retuszowa&#263;"
  ]
  node [
    id 1105
    label "ziarno"
  ]
  node [
    id 1106
    label "picture"
  ]
  node [
    id 1107
    label "withdrawal"
  ]
  node [
    id 1108
    label "uniewa&#380;nienie"
  ]
  node [
    id 1109
    label "photograph"
  ]
  node [
    id 1110
    label "zrobienie"
  ]
  node [
    id 1111
    label "zabroni&#263;"
  ]
  node [
    id 1112
    label "pull"
  ]
  node [
    id 1113
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 1114
    label "draw"
  ]
  node [
    id 1115
    label "wyuzda&#263;"
  ]
  node [
    id 1116
    label "wzi&#261;&#263;"
  ]
  node [
    id 1117
    label "zrobi&#263;"
  ]
  node [
    id 1118
    label "odsun&#261;&#263;"
  ]
  node [
    id 1119
    label "uwolni&#263;"
  ]
  node [
    id 1120
    label "lift"
  ]
  node [
    id 1121
    label "zabranianie"
  ]
  node [
    id 1122
    label "branie"
  ]
  node [
    id 1123
    label "uniewa&#380;nianie"
  ]
  node [
    id 1124
    label "robienie"
  ]
  node [
    id 1125
    label "rozpakowywanie"
  ]
  node [
    id 1126
    label "uwalnianie"
  ]
  node [
    id 1127
    label "lokacja_atelierowa"
  ]
  node [
    id 1128
    label "strip"
  ]
  node [
    id 1129
    label "removal"
  ]
  node [
    id 1130
    label "odsuwanie"
  ]
  node [
    id 1131
    label "streszczenie"
  ]
  node [
    id 1132
    label "publicystyka"
  ]
  node [
    id 1133
    label "criticism"
  ]
  node [
    id 1134
    label "tekst"
  ]
  node [
    id 1135
    label "publiczno&#347;&#263;"
  ]
  node [
    id 1136
    label "diatryba"
  ]
  node [
    id 1137
    label "review"
  ]
  node [
    id 1138
    label "ocena"
  ]
  node [
    id 1139
    label "krytyka_literacka"
  ]
  node [
    id 1140
    label "dr&#281;czenie"
  ]
  node [
    id 1141
    label "dokuczanie"
  ]
  node [
    id 1142
    label "niepokojenie"
  ]
  node [
    id 1143
    label "m&#281;czenie"
  ]
  node [
    id 1144
    label "tease"
  ]
  node [
    id 1145
    label "zam&#281;czanie"
  ]
  node [
    id 1146
    label "wywo&#322;ywanie"
  ]
  node [
    id 1147
    label "fatigue_duty"
  ]
  node [
    id 1148
    label "wywodzenie"
  ]
  node [
    id 1149
    label "zastraszanie"
  ]
  node [
    id 1150
    label "effort"
  ]
  node [
    id 1151
    label "plague"
  ]
  node [
    id 1152
    label "krzywdzenie"
  ]
  node [
    id 1153
    label "wyko&#324;czanie"
  ]
  node [
    id 1154
    label "nudzenie"
  ]
  node [
    id 1155
    label "przem&#281;czenie"
  ]
  node [
    id 1156
    label "gn&#281;bienie_si&#281;"
  ]
  node [
    id 1157
    label "noise"
  ]
  node [
    id 1158
    label "wzbudzanie"
  ]
  node [
    id 1159
    label "turbowanie"
  ]
  node [
    id 1160
    label "irritation"
  ]
  node [
    id 1161
    label "dojmowanie"
  ]
  node [
    id 1162
    label "denerwowanie"
  ]
  node [
    id 1163
    label "bolenie"
  ]
  node [
    id 1164
    label "patologia"
  ]
  node [
    id 1165
    label "agresja"
  ]
  node [
    id 1166
    label "przewaga"
  ]
  node [
    id 1167
    label "drastyczny"
  ]
  node [
    id 1168
    label "zachowanie"
  ]
  node [
    id 1169
    label "rapt"
  ]
  node [
    id 1170
    label "okres_godowy"
  ]
  node [
    id 1171
    label "zwierz&#281;"
  ]
  node [
    id 1172
    label "aggression"
  ]
  node [
    id 1173
    label "potop_szwedzki"
  ]
  node [
    id 1174
    label "napad"
  ]
  node [
    id 1175
    label "ognisko"
  ]
  node [
    id 1176
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 1177
    label "powalenie"
  ]
  node [
    id 1178
    label "odezwanie_si&#281;"
  ]
  node [
    id 1179
    label "atakowanie"
  ]
  node [
    id 1180
    label "patomorfologia"
  ]
  node [
    id 1181
    label "grupa_ryzyka"
  ]
  node [
    id 1182
    label "przypadek"
  ]
  node [
    id 1183
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 1184
    label "patolnia"
  ]
  node [
    id 1185
    label "nabawienie_si&#281;"
  ]
  node [
    id 1186
    label "&#347;rodowisko"
  ]
  node [
    id 1187
    label "inkubacja"
  ]
  node [
    id 1188
    label "medycyna"
  ]
  node [
    id 1189
    label "szambo"
  ]
  node [
    id 1190
    label "gangsterski"
  ]
  node [
    id 1191
    label "fizjologia_patologiczna"
  ]
  node [
    id 1192
    label "kryzys"
  ]
  node [
    id 1193
    label "powali&#263;"
  ]
  node [
    id 1194
    label "remisja"
  ]
  node [
    id 1195
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 1196
    label "zajmowa&#263;"
  ]
  node [
    id 1197
    label "zaburzenie"
  ]
  node [
    id 1198
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 1199
    label "neuropatologia"
  ]
  node [
    id 1200
    label "aspo&#322;eczny"
  ]
  node [
    id 1201
    label "badanie_histopatologiczne"
  ]
  node [
    id 1202
    label "abnormality"
  ]
  node [
    id 1203
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1204
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 1205
    label "patogeneza"
  ]
  node [
    id 1206
    label "psychopatologia"
  ]
  node [
    id 1207
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 1208
    label "paleopatologia"
  ]
  node [
    id 1209
    label "logopatologia"
  ]
  node [
    id 1210
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1211
    label "osteopatologia"
  ]
  node [
    id 1212
    label "immunopatologia"
  ]
  node [
    id 1213
    label "odzywanie_si&#281;"
  ]
  node [
    id 1214
    label "diagnoza"
  ]
  node [
    id 1215
    label "atakowa&#263;"
  ]
  node [
    id 1216
    label "histopatologia"
  ]
  node [
    id 1217
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1218
    label "nabawianie_si&#281;"
  ]
  node [
    id 1219
    label "underworld"
  ]
  node [
    id 1220
    label "meteoropatologia"
  ]
  node [
    id 1221
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 1222
    label "zajmowanie"
  ]
  node [
    id 1223
    label "r&#243;&#380;nica"
  ]
  node [
    id 1224
    label "advantage"
  ]
  node [
    id 1225
    label "znaczenie"
  ]
  node [
    id 1226
    label "laterality"
  ]
  node [
    id 1227
    label "prym"
  ]
  node [
    id 1228
    label "mocny"
  ]
  node [
    id 1229
    label "nieprzyzwoity"
  ]
  node [
    id 1230
    label "radykalny"
  ]
  node [
    id 1231
    label "dosadny"
  ]
  node [
    id 1232
    label "drastycznie"
  ]
  node [
    id 1233
    label "frown"
  ]
  node [
    id 1234
    label "suppress"
  ]
  node [
    id 1235
    label "suspend"
  ]
  node [
    id 1236
    label "miarkowa&#263;"
  ]
  node [
    id 1237
    label "os&#322;abia&#263;"
  ]
  node [
    id 1238
    label "opanowywa&#263;"
  ]
  node [
    id 1239
    label "zmniejsza&#263;"
  ]
  node [
    id 1240
    label "zwalcza&#263;"
  ]
  node [
    id 1241
    label "os&#322;abianie"
  ]
  node [
    id 1242
    label "os&#322;abienie"
  ]
  node [
    id 1243
    label "kondycja_fizyczna"
  ]
  node [
    id 1244
    label "os&#322;abi&#263;"
  ]
  node [
    id 1245
    label "zdrowie"
  ]
  node [
    id 1246
    label "bate"
  ]
  node [
    id 1247
    label "zmienia&#263;"
  ]
  node [
    id 1248
    label "control"
  ]
  node [
    id 1249
    label "manipulate"
  ]
  node [
    id 1250
    label "niewoli&#263;"
  ]
  node [
    id 1251
    label "capture"
  ]
  node [
    id 1252
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 1253
    label "powstrzymywa&#263;"
  ]
  node [
    id 1254
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 1255
    label "meet"
  ]
  node [
    id 1256
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 1257
    label "dostawa&#263;"
  ]
  node [
    id 1258
    label "rede"
  ]
  node [
    id 1259
    label "raise"
  ]
  node [
    id 1260
    label "pokonywa&#263;"
  ]
  node [
    id 1261
    label "fight"
  ]
  node [
    id 1262
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 1263
    label "hamowa&#263;"
  ]
  node [
    id 1264
    label "domy&#347;la&#263;_si&#281;"
  ]
  node [
    id 1265
    label "rozmowa"
  ]
  node [
    id 1266
    label "sympozjon"
  ]
  node [
    id 1267
    label "conference"
  ]
  node [
    id 1268
    label "cisza"
  ]
  node [
    id 1269
    label "odpowied&#378;"
  ]
  node [
    id 1270
    label "rozhowor"
  ]
  node [
    id 1271
    label "discussion"
  ]
  node [
    id 1272
    label "esej"
  ]
  node [
    id 1273
    label "sympozjarcha"
  ]
  node [
    id 1274
    label "faza"
  ]
  node [
    id 1275
    label "rozrywka"
  ]
  node [
    id 1276
    label "symposium"
  ]
  node [
    id 1277
    label "przyj&#281;cie"
  ]
  node [
    id 1278
    label "utw&#243;r"
  ]
  node [
    id 1279
    label "konferencja"
  ]
  node [
    id 1280
    label "dyskusja"
  ]
  node [
    id 1281
    label "upublicznianie"
  ]
  node [
    id 1282
    label "jawny"
  ]
  node [
    id 1283
    label "upublicznienie"
  ]
  node [
    id 1284
    label "publicznie"
  ]
  node [
    id 1285
    label "jawnie"
  ]
  node [
    id 1286
    label "udost&#281;pnianie"
  ]
  node [
    id 1287
    label "udost&#281;pnienie"
  ]
  node [
    id 1288
    label "ujawnienie_si&#281;"
  ]
  node [
    id 1289
    label "ujawnianie_si&#281;"
  ]
  node [
    id 1290
    label "zdecydowany"
  ]
  node [
    id 1291
    label "znajomy"
  ]
  node [
    id 1292
    label "ujawnienie"
  ]
  node [
    id 1293
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 1294
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 1295
    label "ujawnianie"
  ]
  node [
    id 1296
    label "ewidentny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 2
    target 557
  ]
  edge [
    source 2
    target 558
  ]
  edge [
    source 2
    target 559
  ]
  edge [
    source 2
    target 560
  ]
  edge [
    source 2
    target 561
  ]
  edge [
    source 2
    target 562
  ]
  edge [
    source 2
    target 563
  ]
  edge [
    source 2
    target 564
  ]
  edge [
    source 2
    target 565
  ]
  edge [
    source 2
    target 566
  ]
  edge [
    source 2
    target 567
  ]
  edge [
    source 2
    target 568
  ]
  edge [
    source 2
    target 569
  ]
  edge [
    source 2
    target 570
  ]
  edge [
    source 2
    target 571
  ]
  edge [
    source 2
    target 572
  ]
  edge [
    source 2
    target 573
  ]
  edge [
    source 2
    target 574
  ]
  edge [
    source 2
    target 575
  ]
  edge [
    source 2
    target 576
  ]
  edge [
    source 2
    target 577
  ]
  edge [
    source 2
    target 578
  ]
  edge [
    source 2
    target 579
  ]
  edge [
    source 2
    target 580
  ]
  edge [
    source 2
    target 581
  ]
  edge [
    source 2
    target 582
  ]
  edge [
    source 2
    target 583
  ]
  edge [
    source 2
    target 584
  ]
  edge [
    source 2
    target 585
  ]
  edge [
    source 2
    target 586
  ]
  edge [
    source 2
    target 587
  ]
  edge [
    source 2
    target 588
  ]
  edge [
    source 2
    target 589
  ]
  edge [
    source 2
    target 590
  ]
  edge [
    source 2
    target 591
  ]
  edge [
    source 2
    target 592
  ]
  edge [
    source 2
    target 593
  ]
  edge [
    source 2
    target 594
  ]
  edge [
    source 2
    target 595
  ]
  edge [
    source 2
    target 596
  ]
  edge [
    source 2
    target 597
  ]
  edge [
    source 2
    target 598
  ]
  edge [
    source 2
    target 599
  ]
  edge [
    source 2
    target 600
  ]
  edge [
    source 2
    target 601
  ]
  edge [
    source 2
    target 602
  ]
  edge [
    source 2
    target 603
  ]
  edge [
    source 2
    target 604
  ]
  edge [
    source 2
    target 605
  ]
  edge [
    source 2
    target 606
  ]
  edge [
    source 2
    target 607
  ]
  edge [
    source 2
    target 608
  ]
  edge [
    source 2
    target 609
  ]
  edge [
    source 2
    target 610
  ]
  edge [
    source 2
    target 611
  ]
  edge [
    source 2
    target 612
  ]
  edge [
    source 2
    target 613
  ]
  edge [
    source 2
    target 614
  ]
  edge [
    source 2
    target 615
  ]
  edge [
    source 2
    target 616
  ]
  edge [
    source 2
    target 617
  ]
  edge [
    source 2
    target 618
  ]
  edge [
    source 2
    target 619
  ]
  edge [
    source 2
    target 620
  ]
  edge [
    source 2
    target 621
  ]
  edge [
    source 2
    target 622
  ]
  edge [
    source 2
    target 623
  ]
  edge [
    source 2
    target 624
  ]
  edge [
    source 2
    target 625
  ]
  edge [
    source 2
    target 626
  ]
  edge [
    source 2
    target 627
  ]
  edge [
    source 2
    target 628
  ]
  edge [
    source 2
    target 629
  ]
  edge [
    source 2
    target 630
  ]
  edge [
    source 2
    target 631
  ]
  edge [
    source 2
    target 632
  ]
  edge [
    source 2
    target 633
  ]
  edge [
    source 2
    target 634
  ]
  edge [
    source 2
    target 635
  ]
  edge [
    source 2
    target 636
  ]
  edge [
    source 2
    target 637
  ]
  edge [
    source 2
    target 638
  ]
  edge [
    source 2
    target 639
  ]
  edge [
    source 2
    target 640
  ]
  edge [
    source 2
    target 641
  ]
  edge [
    source 2
    target 642
  ]
  edge [
    source 2
    target 643
  ]
  edge [
    source 2
    target 644
  ]
  edge [
    source 2
    target 645
  ]
  edge [
    source 2
    target 646
  ]
  edge [
    source 2
    target 647
  ]
  edge [
    source 2
    target 648
  ]
  edge [
    source 2
    target 649
  ]
  edge [
    source 2
    target 650
  ]
  edge [
    source 2
    target 651
  ]
  edge [
    source 2
    target 652
  ]
  edge [
    source 2
    target 653
  ]
  edge [
    source 2
    target 654
  ]
  edge [
    source 2
    target 655
  ]
  edge [
    source 2
    target 656
  ]
  edge [
    source 2
    target 657
  ]
  edge [
    source 2
    target 658
  ]
  edge [
    source 2
    target 659
  ]
  edge [
    source 2
    target 660
  ]
  edge [
    source 2
    target 661
  ]
  edge [
    source 2
    target 662
  ]
  edge [
    source 2
    target 663
  ]
  edge [
    source 2
    target 664
  ]
  edge [
    source 2
    target 665
  ]
  edge [
    source 2
    target 666
  ]
  edge [
    source 2
    target 667
  ]
  edge [
    source 2
    target 668
  ]
  edge [
    source 2
    target 669
  ]
  edge [
    source 2
    target 670
  ]
  edge [
    source 2
    target 671
  ]
  edge [
    source 2
    target 672
  ]
  edge [
    source 2
    target 673
  ]
  edge [
    source 2
    target 674
  ]
  edge [
    source 2
    target 675
  ]
  edge [
    source 2
    target 676
  ]
  edge [
    source 2
    target 677
  ]
  edge [
    source 2
    target 678
  ]
  edge [
    source 2
    target 679
  ]
  edge [
    source 2
    target 680
  ]
  edge [
    source 2
    target 681
  ]
  edge [
    source 2
    target 682
  ]
  edge [
    source 2
    target 683
  ]
  edge [
    source 2
    target 684
  ]
  edge [
    source 2
    target 685
  ]
  edge [
    source 2
    target 686
  ]
  edge [
    source 2
    target 687
  ]
  edge [
    source 2
    target 688
  ]
  edge [
    source 2
    target 689
  ]
  edge [
    source 2
    target 690
  ]
  edge [
    source 2
    target 691
  ]
  edge [
    source 2
    target 692
  ]
  edge [
    source 2
    target 693
  ]
  edge [
    source 2
    target 694
  ]
  edge [
    source 2
    target 695
  ]
  edge [
    source 2
    target 696
  ]
  edge [
    source 2
    target 697
  ]
  edge [
    source 2
    target 698
  ]
  edge [
    source 2
    target 699
  ]
  edge [
    source 2
    target 700
  ]
  edge [
    source 2
    target 701
  ]
  edge [
    source 2
    target 702
  ]
  edge [
    source 2
    target 703
  ]
  edge [
    source 2
    target 704
  ]
  edge [
    source 2
    target 705
  ]
  edge [
    source 2
    target 706
  ]
  edge [
    source 2
    target 707
  ]
  edge [
    source 2
    target 708
  ]
  edge [
    source 2
    target 709
  ]
  edge [
    source 2
    target 710
  ]
  edge [
    source 2
    target 711
  ]
  edge [
    source 2
    target 712
  ]
  edge [
    source 2
    target 713
  ]
  edge [
    source 2
    target 714
  ]
  edge [
    source 2
    target 715
  ]
  edge [
    source 2
    target 716
  ]
  edge [
    source 2
    target 717
  ]
  edge [
    source 2
    target 718
  ]
  edge [
    source 2
    target 719
  ]
  edge [
    source 2
    target 720
  ]
  edge [
    source 2
    target 721
  ]
  edge [
    source 2
    target 722
  ]
  edge [
    source 2
    target 723
  ]
  edge [
    source 2
    target 724
  ]
  edge [
    source 2
    target 725
  ]
  edge [
    source 2
    target 726
  ]
  edge [
    source 2
    target 727
  ]
  edge [
    source 2
    target 728
  ]
  edge [
    source 2
    target 729
  ]
  edge [
    source 2
    target 730
  ]
  edge [
    source 2
    target 731
  ]
  edge [
    source 2
    target 732
  ]
  edge [
    source 2
    target 733
  ]
  edge [
    source 2
    target 734
  ]
  edge [
    source 2
    target 735
  ]
  edge [
    source 2
    target 736
  ]
  edge [
    source 2
    target 737
  ]
  edge [
    source 2
    target 738
  ]
  edge [
    source 2
    target 739
  ]
  edge [
    source 2
    target 740
  ]
  edge [
    source 2
    target 741
  ]
  edge [
    source 2
    target 742
  ]
  edge [
    source 2
    target 743
  ]
  edge [
    source 2
    target 744
  ]
  edge [
    source 2
    target 745
  ]
  edge [
    source 2
    target 746
  ]
  edge [
    source 2
    target 747
  ]
  edge [
    source 2
    target 748
  ]
  edge [
    source 2
    target 749
  ]
  edge [
    source 2
    target 750
  ]
  edge [
    source 2
    target 751
  ]
  edge [
    source 2
    target 752
  ]
  edge [
    source 2
    target 753
  ]
  edge [
    source 2
    target 754
  ]
  edge [
    source 2
    target 755
  ]
  edge [
    source 2
    target 756
  ]
  edge [
    source 2
    target 757
  ]
  edge [
    source 2
    target 758
  ]
  edge [
    source 2
    target 759
  ]
  edge [
    source 2
    target 760
  ]
  edge [
    source 2
    target 761
  ]
  edge [
    source 2
    target 762
  ]
  edge [
    source 2
    target 763
  ]
  edge [
    source 2
    target 764
  ]
  edge [
    source 2
    target 765
  ]
  edge [
    source 2
    target 766
  ]
  edge [
    source 2
    target 767
  ]
  edge [
    source 2
    target 768
  ]
  edge [
    source 2
    target 769
  ]
  edge [
    source 2
    target 770
  ]
  edge [
    source 2
    target 771
  ]
  edge [
    source 2
    target 772
  ]
  edge [
    source 2
    target 773
  ]
  edge [
    source 2
    target 774
  ]
  edge [
    source 2
    target 775
  ]
  edge [
    source 2
    target 776
  ]
  edge [
    source 2
    target 777
  ]
  edge [
    source 2
    target 778
  ]
  edge [
    source 2
    target 779
  ]
  edge [
    source 2
    target 780
  ]
  edge [
    source 2
    target 781
  ]
  edge [
    source 2
    target 782
  ]
  edge [
    source 2
    target 783
  ]
  edge [
    source 2
    target 784
  ]
  edge [
    source 2
    target 785
  ]
  edge [
    source 2
    target 786
  ]
  edge [
    source 2
    target 787
  ]
  edge [
    source 2
    target 788
  ]
  edge [
    source 2
    target 789
  ]
  edge [
    source 2
    target 790
  ]
  edge [
    source 2
    target 791
  ]
  edge [
    source 2
    target 792
  ]
  edge [
    source 2
    target 793
  ]
  edge [
    source 2
    target 794
  ]
  edge [
    source 2
    target 795
  ]
  edge [
    source 2
    target 796
  ]
  edge [
    source 2
    target 797
  ]
  edge [
    source 2
    target 798
  ]
  edge [
    source 2
    target 799
  ]
  edge [
    source 2
    target 800
  ]
  edge [
    source 2
    target 801
  ]
  edge [
    source 2
    target 802
  ]
  edge [
    source 2
    target 803
  ]
  edge [
    source 2
    target 804
  ]
  edge [
    source 2
    target 805
  ]
  edge [
    source 2
    target 806
  ]
  edge [
    source 2
    target 807
  ]
  edge [
    source 2
    target 808
  ]
  edge [
    source 2
    target 809
  ]
  edge [
    source 2
    target 810
  ]
  edge [
    source 2
    target 811
  ]
  edge [
    source 2
    target 812
  ]
  edge [
    source 2
    target 813
  ]
  edge [
    source 2
    target 814
  ]
  edge [
    source 2
    target 815
  ]
  edge [
    source 2
    target 816
  ]
  edge [
    source 2
    target 817
  ]
  edge [
    source 2
    target 818
  ]
  edge [
    source 2
    target 819
  ]
  edge [
    source 2
    target 820
  ]
  edge [
    source 2
    target 821
  ]
  edge [
    source 2
    target 822
  ]
  edge [
    source 2
    target 823
  ]
  edge [
    source 2
    target 824
  ]
  edge [
    source 2
    target 825
  ]
  edge [
    source 2
    target 826
  ]
  edge [
    source 2
    target 827
  ]
  edge [
    source 2
    target 828
  ]
  edge [
    source 2
    target 829
  ]
  edge [
    source 2
    target 830
  ]
  edge [
    source 2
    target 831
  ]
  edge [
    source 2
    target 832
  ]
  edge [
    source 2
    target 833
  ]
  edge [
    source 2
    target 834
  ]
  edge [
    source 2
    target 835
  ]
  edge [
    source 2
    target 836
  ]
  edge [
    source 2
    target 837
  ]
  edge [
    source 2
    target 838
  ]
  edge [
    source 2
    target 839
  ]
  edge [
    source 2
    target 840
  ]
  edge [
    source 2
    target 841
  ]
  edge [
    source 2
    target 842
  ]
  edge [
    source 2
    target 843
  ]
  edge [
    source 2
    target 844
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 845
  ]
  edge [
    source 2
    target 846
  ]
  edge [
    source 2
    target 847
  ]
  edge [
    source 2
    target 848
  ]
  edge [
    source 2
    target 849
  ]
  edge [
    source 2
    target 850
  ]
  edge [
    source 2
    target 851
  ]
  edge [
    source 2
    target 852
  ]
  edge [
    source 2
    target 853
  ]
  edge [
    source 2
    target 854
  ]
  edge [
    source 2
    target 855
  ]
  edge [
    source 2
    target 856
  ]
  edge [
    source 2
    target 857
  ]
  edge [
    source 2
    target 858
  ]
  edge [
    source 2
    target 859
  ]
  edge [
    source 2
    target 860
  ]
  edge [
    source 2
    target 861
  ]
  edge [
    source 2
    target 862
  ]
  edge [
    source 2
    target 863
  ]
  edge [
    source 2
    target 864
  ]
  edge [
    source 2
    target 865
  ]
  edge [
    source 2
    target 866
  ]
  edge [
    source 2
    target 867
  ]
  edge [
    source 2
    target 868
  ]
  edge [
    source 2
    target 869
  ]
  edge [
    source 2
    target 870
  ]
  edge [
    source 2
    target 871
  ]
  edge [
    source 2
    target 872
  ]
  edge [
    source 2
    target 873
  ]
  edge [
    source 2
    target 874
  ]
  edge [
    source 2
    target 875
  ]
  edge [
    source 2
    target 876
  ]
  edge [
    source 2
    target 877
  ]
  edge [
    source 2
    target 878
  ]
  edge [
    source 2
    target 879
  ]
  edge [
    source 2
    target 880
  ]
  edge [
    source 2
    target 881
  ]
  edge [
    source 2
    target 882
  ]
  edge [
    source 2
    target 883
  ]
  edge [
    source 2
    target 884
  ]
  edge [
    source 2
    target 885
  ]
  edge [
    source 2
    target 886
  ]
  edge [
    source 2
    target 887
  ]
  edge [
    source 2
    target 888
  ]
  edge [
    source 2
    target 889
  ]
  edge [
    source 2
    target 890
  ]
  edge [
    source 2
    target 891
  ]
  edge [
    source 2
    target 892
  ]
  edge [
    source 2
    target 893
  ]
  edge [
    source 2
    target 894
  ]
  edge [
    source 2
    target 895
  ]
  edge [
    source 2
    target 896
  ]
  edge [
    source 2
    target 897
  ]
  edge [
    source 2
    target 898
  ]
  edge [
    source 2
    target 899
  ]
  edge [
    source 2
    target 900
  ]
  edge [
    source 2
    target 901
  ]
  edge [
    source 2
    target 902
  ]
  edge [
    source 2
    target 903
  ]
  edge [
    source 2
    target 904
  ]
  edge [
    source 2
    target 905
  ]
  edge [
    source 2
    target 906
  ]
  edge [
    source 2
    target 907
  ]
  edge [
    source 2
    target 908
  ]
  edge [
    source 2
    target 909
  ]
  edge [
    source 2
    target 910
  ]
  edge [
    source 2
    target 911
  ]
  edge [
    source 2
    target 912
  ]
  edge [
    source 2
    target 913
  ]
  edge [
    source 2
    target 914
  ]
  edge [
    source 2
    target 915
  ]
  edge [
    source 2
    target 916
  ]
  edge [
    source 2
    target 917
  ]
  edge [
    source 2
    target 918
  ]
  edge [
    source 2
    target 919
  ]
  edge [
    source 2
    target 920
  ]
  edge [
    source 2
    target 921
  ]
  edge [
    source 2
    target 922
  ]
  edge [
    source 2
    target 923
  ]
  edge [
    source 2
    target 924
  ]
  edge [
    source 2
    target 925
  ]
  edge [
    source 2
    target 926
  ]
  edge [
    source 2
    target 927
  ]
  edge [
    source 2
    target 928
  ]
  edge [
    source 2
    target 929
  ]
  edge [
    source 2
    target 930
  ]
  edge [
    source 2
    target 931
  ]
  edge [
    source 2
    target 932
  ]
  edge [
    source 2
    target 933
  ]
  edge [
    source 2
    target 934
  ]
  edge [
    source 2
    target 935
  ]
  edge [
    source 2
    target 936
  ]
  edge [
    source 2
    target 937
  ]
  edge [
    source 2
    target 938
  ]
  edge [
    source 2
    target 939
  ]
  edge [
    source 2
    target 940
  ]
  edge [
    source 2
    target 941
  ]
  edge [
    source 2
    target 942
  ]
  edge [
    source 2
    target 943
  ]
  edge [
    source 2
    target 944
  ]
  edge [
    source 2
    target 945
  ]
  edge [
    source 2
    target 946
  ]
  edge [
    source 2
    target 947
  ]
  edge [
    source 2
    target 948
  ]
  edge [
    source 2
    target 949
  ]
  edge [
    source 2
    target 950
  ]
  edge [
    source 2
    target 951
  ]
  edge [
    source 2
    target 952
  ]
  edge [
    source 2
    target 953
  ]
  edge [
    source 2
    target 954
  ]
  edge [
    source 2
    target 955
  ]
  edge [
    source 2
    target 956
  ]
  edge [
    source 2
    target 957
  ]
  edge [
    source 2
    target 958
  ]
  edge [
    source 2
    target 959
  ]
  edge [
    source 2
    target 960
  ]
  edge [
    source 2
    target 961
  ]
  edge [
    source 2
    target 962
  ]
  edge [
    source 2
    target 963
  ]
  edge [
    source 2
    target 964
  ]
  edge [
    source 2
    target 965
  ]
  edge [
    source 2
    target 966
  ]
  edge [
    source 2
    target 967
  ]
  edge [
    source 2
    target 968
  ]
  edge [
    source 2
    target 969
  ]
  edge [
    source 2
    target 970
  ]
  edge [
    source 2
    target 971
  ]
  edge [
    source 2
    target 972
  ]
  edge [
    source 2
    target 973
  ]
  edge [
    source 2
    target 974
  ]
  edge [
    source 2
    target 975
  ]
  edge [
    source 2
    target 976
  ]
  edge [
    source 2
    target 977
  ]
  edge [
    source 2
    target 978
  ]
  edge [
    source 2
    target 979
  ]
  edge [
    source 2
    target 980
  ]
  edge [
    source 2
    target 981
  ]
  edge [
    source 2
    target 982
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 983
  ]
  edge [
    source 3
    target 984
  ]
  edge [
    source 3
    target 985
  ]
  edge [
    source 3
    target 986
  ]
  edge [
    source 3
    target 987
  ]
  edge [
    source 3
    target 988
  ]
  edge [
    source 3
    target 989
  ]
  edge [
    source 3
    target 990
  ]
  edge [
    source 3
    target 991
  ]
  edge [
    source 3
    target 992
  ]
  edge [
    source 3
    target 993
  ]
  edge [
    source 3
    target 994
  ]
  edge [
    source 3
    target 995
  ]
  edge [
    source 3
    target 996
  ]
  edge [
    source 3
    target 997
  ]
  edge [
    source 3
    target 998
  ]
  edge [
    source 3
    target 999
  ]
  edge [
    source 3
    target 1000
  ]
  edge [
    source 3
    target 1001
  ]
  edge [
    source 3
    target 1002
  ]
  edge [
    source 3
    target 1003
  ]
  edge [
    source 3
    target 1004
  ]
  edge [
    source 3
    target 1005
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 1006
  ]
  edge [
    source 4
    target 1007
  ]
  edge [
    source 4
    target 1008
  ]
  edge [
    source 4
    target 1009
  ]
  edge [
    source 4
    target 1010
  ]
  edge [
    source 4
    target 1011
  ]
  edge [
    source 4
    target 1012
  ]
  edge [
    source 4
    target 1013
  ]
  edge [
    source 4
    target 1014
  ]
  edge [
    source 4
    target 1015
  ]
  edge [
    source 4
    target 1016
  ]
  edge [
    source 4
    target 1017
  ]
  edge [
    source 4
    target 1018
  ]
  edge [
    source 4
    target 1019
  ]
  edge [
    source 4
    target 1020
  ]
  edge [
    source 4
    target 1021
  ]
  edge [
    source 4
    target 1022
  ]
  edge [
    source 4
    target 1023
  ]
  edge [
    source 4
    target 1024
  ]
  edge [
    source 4
    target 1025
  ]
  edge [
    source 4
    target 1026
  ]
  edge [
    source 4
    target 1027
  ]
  edge [
    source 4
    target 1028
  ]
  edge [
    source 4
    target 1029
  ]
  edge [
    source 4
    target 1030
  ]
  edge [
    source 4
    target 1031
  ]
  edge [
    source 4
    target 1032
  ]
  edge [
    source 4
    target 1033
  ]
  edge [
    source 4
    target 1034
  ]
  edge [
    source 4
    target 1035
  ]
  edge [
    source 4
    target 1036
  ]
  edge [
    source 4
    target 1037
  ]
  edge [
    source 4
    target 1038
  ]
  edge [
    source 4
    target 1039
  ]
  edge [
    source 4
    target 1040
  ]
  edge [
    source 4
    target 1041
  ]
  edge [
    source 4
    target 1042
  ]
  edge [
    source 4
    target 1043
  ]
  edge [
    source 4
    target 1044
  ]
  edge [
    source 4
    target 1045
  ]
  edge [
    source 4
    target 1046
  ]
  edge [
    source 4
    target 1047
  ]
  edge [
    source 4
    target 1048
  ]
  edge [
    source 4
    target 1049
  ]
  edge [
    source 4
    target 1050
  ]
  edge [
    source 4
    target 1051
  ]
  edge [
    source 4
    target 1052
  ]
  edge [
    source 4
    target 1053
  ]
  edge [
    source 4
    target 1054
  ]
  edge [
    source 4
    target 1055
  ]
  edge [
    source 4
    target 1056
  ]
  edge [
    source 4
    target 1057
  ]
  edge [
    source 4
    target 1058
  ]
  edge [
    source 4
    target 1059
  ]
  edge [
    source 4
    target 1060
  ]
  edge [
    source 4
    target 1061
  ]
  edge [
    source 4
    target 1062
  ]
  edge [
    source 4
    target 1063
  ]
  edge [
    source 4
    target 1064
  ]
  edge [
    source 4
    target 1065
  ]
  edge [
    source 4
    target 1066
  ]
  edge [
    source 4
    target 1067
  ]
  edge [
    source 4
    target 1068
  ]
  edge [
    source 4
    target 1069
  ]
  edge [
    source 4
    target 1070
  ]
  edge [
    source 4
    target 1071
  ]
  edge [
    source 4
    target 1072
  ]
  edge [
    source 4
    target 1073
  ]
  edge [
    source 4
    target 1074
  ]
  edge [
    source 4
    target 996
  ]
  edge [
    source 4
    target 1075
  ]
  edge [
    source 4
    target 1076
  ]
  edge [
    source 4
    target 1077
  ]
  edge [
    source 4
    target 1078
  ]
  edge [
    source 4
    target 1079
  ]
  edge [
    source 4
    target 1080
  ]
  edge [
    source 4
    target 1081
  ]
  edge [
    source 4
    target 1082
  ]
  edge [
    source 4
    target 1083
  ]
  edge [
    source 4
    target 1084
  ]
  edge [
    source 4
    target 1085
  ]
  edge [
    source 4
    target 1086
  ]
  edge [
    source 4
    target 1087
  ]
  edge [
    source 4
    target 1088
  ]
  edge [
    source 4
    target 1089
  ]
  edge [
    source 4
    target 1090
  ]
  edge [
    source 4
    target 1091
  ]
  edge [
    source 4
    target 1092
  ]
  edge [
    source 4
    target 1093
  ]
  edge [
    source 4
    target 1094
  ]
  edge [
    source 4
    target 1095
  ]
  edge [
    source 4
    target 1096
  ]
  edge [
    source 4
    target 1097
  ]
  edge [
    source 4
    target 1098
  ]
  edge [
    source 4
    target 1099
  ]
  edge [
    source 4
    target 1100
  ]
  edge [
    source 4
    target 1101
  ]
  edge [
    source 4
    target 1102
  ]
  edge [
    source 4
    target 1103
  ]
  edge [
    source 4
    target 1104
  ]
  edge [
    source 4
    target 1105
  ]
  edge [
    source 4
    target 1106
  ]
  edge [
    source 4
    target 1107
  ]
  edge [
    source 4
    target 1108
  ]
  edge [
    source 4
    target 1109
  ]
  edge [
    source 4
    target 1110
  ]
  edge [
    source 4
    target 1111
  ]
  edge [
    source 4
    target 1112
  ]
  edge [
    source 4
    target 1113
  ]
  edge [
    source 4
    target 1114
  ]
  edge [
    source 4
    target 1115
  ]
  edge [
    source 4
    target 1116
  ]
  edge [
    source 4
    target 1117
  ]
  edge [
    source 4
    target 1118
  ]
  edge [
    source 4
    target 1119
  ]
  edge [
    source 4
    target 1120
  ]
  edge [
    source 4
    target 1121
  ]
  edge [
    source 4
    target 1122
  ]
  edge [
    source 4
    target 1123
  ]
  edge [
    source 4
    target 1124
  ]
  edge [
    source 4
    target 1125
  ]
  edge [
    source 4
    target 1126
  ]
  edge [
    source 4
    target 1127
  ]
  edge [
    source 4
    target 1128
  ]
  edge [
    source 4
    target 1129
  ]
  edge [
    source 4
    target 1130
  ]
  edge [
    source 4
    target 1131
  ]
  edge [
    source 4
    target 1132
  ]
  edge [
    source 4
    target 1133
  ]
  edge [
    source 4
    target 1134
  ]
  edge [
    source 4
    target 1135
  ]
  edge [
    source 4
    target 1136
  ]
  edge [
    source 4
    target 1137
  ]
  edge [
    source 4
    target 1138
  ]
  edge [
    source 4
    target 1139
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 1140
  ]
  edge [
    source 5
    target 1141
  ]
  edge [
    source 5
    target 1142
  ]
  edge [
    source 5
    target 1143
  ]
  edge [
    source 5
    target 1144
  ]
  edge [
    source 5
    target 1145
  ]
  edge [
    source 5
    target 1146
  ]
  edge [
    source 5
    target 1147
  ]
  edge [
    source 5
    target 1148
  ]
  edge [
    source 5
    target 1124
  ]
  edge [
    source 5
    target 1149
  ]
  edge [
    source 5
    target 1150
  ]
  edge [
    source 5
    target 1151
  ]
  edge [
    source 5
    target 1152
  ]
  edge [
    source 5
    target 1153
  ]
  edge [
    source 5
    target 1154
  ]
  edge [
    source 5
    target 1155
  ]
  edge [
    source 5
    target 1057
  ]
  edge [
    source 5
    target 1156
  ]
  edge [
    source 5
    target 1157
  ]
  edge [
    source 5
    target 1158
  ]
  edge [
    source 5
    target 1159
  ]
  edge [
    source 5
    target 1160
  ]
  edge [
    source 5
    target 1161
  ]
  edge [
    source 5
    target 1162
  ]
  edge [
    source 5
    target 1163
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 1164
  ]
  edge [
    source 6
    target 1165
  ]
  edge [
    source 6
    target 1166
  ]
  edge [
    source 6
    target 1167
  ]
  edge [
    source 6
    target 1168
  ]
  edge [
    source 6
    target 1169
  ]
  edge [
    source 6
    target 1170
  ]
  edge [
    source 6
    target 1171
  ]
  edge [
    source 6
    target 1172
  ]
  edge [
    source 6
    target 1173
  ]
  edge [
    source 6
    target 1174
  ]
  edge [
    source 6
    target 1175
  ]
  edge [
    source 6
    target 1176
  ]
  edge [
    source 6
    target 1177
  ]
  edge [
    source 6
    target 1178
  ]
  edge [
    source 6
    target 1179
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 1180
  ]
  edge [
    source 6
    target 1181
  ]
  edge [
    source 6
    target 1182
  ]
  edge [
    source 6
    target 1183
  ]
  edge [
    source 6
    target 1184
  ]
  edge [
    source 6
    target 1185
  ]
  edge [
    source 6
    target 1186
  ]
  edge [
    source 6
    target 1187
  ]
  edge [
    source 6
    target 1188
  ]
  edge [
    source 6
    target 1189
  ]
  edge [
    source 6
    target 1190
  ]
  edge [
    source 6
    target 1191
  ]
  edge [
    source 6
    target 1192
  ]
  edge [
    source 6
    target 1193
  ]
  edge [
    source 6
    target 1194
  ]
  edge [
    source 6
    target 1195
  ]
  edge [
    source 6
    target 1196
  ]
  edge [
    source 6
    target 1197
  ]
  edge [
    source 6
    target 1198
  ]
  edge [
    source 6
    target 1199
  ]
  edge [
    source 6
    target 1200
  ]
  edge [
    source 6
    target 1201
  ]
  edge [
    source 6
    target 1202
  ]
  edge [
    source 6
    target 1203
  ]
  edge [
    source 6
    target 1204
  ]
  edge [
    source 6
    target 1205
  ]
  edge [
    source 6
    target 1206
  ]
  edge [
    source 6
    target 1207
  ]
  edge [
    source 6
    target 1208
  ]
  edge [
    source 6
    target 1209
  ]
  edge [
    source 6
    target 1210
  ]
  edge [
    source 6
    target 1211
  ]
  edge [
    source 6
    target 1212
  ]
  edge [
    source 6
    target 1213
  ]
  edge [
    source 6
    target 1214
  ]
  edge [
    source 6
    target 1215
  ]
  edge [
    source 6
    target 1216
  ]
  edge [
    source 6
    target 1217
  ]
  edge [
    source 6
    target 1218
  ]
  edge [
    source 6
    target 1219
  ]
  edge [
    source 6
    target 1220
  ]
  edge [
    source 6
    target 1221
  ]
  edge [
    source 6
    target 1222
  ]
  edge [
    source 6
    target 1223
  ]
  edge [
    source 6
    target 1224
  ]
  edge [
    source 6
    target 1225
  ]
  edge [
    source 6
    target 1226
  ]
  edge [
    source 6
    target 1227
  ]
  edge [
    source 6
    target 1228
  ]
  edge [
    source 6
    target 1229
  ]
  edge [
    source 6
    target 1230
  ]
  edge [
    source 6
    target 1231
  ]
  edge [
    source 6
    target 1232
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 1233
  ]
  edge [
    source 7
    target 1234
  ]
  edge [
    source 7
    target 1235
  ]
  edge [
    source 7
    target 1236
  ]
  edge [
    source 7
    target 1237
  ]
  edge [
    source 7
    target 1238
  ]
  edge [
    source 7
    target 1239
  ]
  edge [
    source 7
    target 1240
  ]
  edge [
    source 7
    target 1241
  ]
  edge [
    source 7
    target 996
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 1242
  ]
  edge [
    source 7
    target 1243
  ]
  edge [
    source 7
    target 1244
  ]
  edge [
    source 7
    target 1245
  ]
  edge [
    source 7
    target 1000
  ]
  edge [
    source 7
    target 1246
  ]
  edge [
    source 7
    target 1247
  ]
  edge [
    source 7
    target 1248
  ]
  edge [
    source 7
    target 1249
  ]
  edge [
    source 7
    target 1250
  ]
  edge [
    source 7
    target 1251
  ]
  edge [
    source 7
    target 1252
  ]
  edge [
    source 7
    target 1253
  ]
  edge [
    source 7
    target 1254
  ]
  edge [
    source 7
    target 1255
  ]
  edge [
    source 7
    target 1256
  ]
  edge [
    source 7
    target 1257
  ]
  edge [
    source 7
    target 1258
  ]
  edge [
    source 7
    target 1259
  ]
  edge [
    source 7
    target 1260
  ]
  edge [
    source 7
    target 1261
  ]
  edge [
    source 7
    target 1262
  ]
  edge [
    source 7
    target 1263
  ]
  edge [
    source 7
    target 1264
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 1265
  ]
  edge [
    source 8
    target 1266
  ]
  edge [
    source 8
    target 1267
  ]
  edge [
    source 8
    target 1268
  ]
  edge [
    source 8
    target 1269
  ]
  edge [
    source 8
    target 1270
  ]
  edge [
    source 8
    target 1271
  ]
  edge [
    source 8
    target 1057
  ]
  edge [
    source 8
    target 1272
  ]
  edge [
    source 8
    target 1273
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 1274
  ]
  edge [
    source 8
    target 1275
  ]
  edge [
    source 8
    target 1276
  ]
  edge [
    source 8
    target 1277
  ]
  edge [
    source 8
    target 1278
  ]
  edge [
    source 8
    target 1279
  ]
  edge [
    source 8
    target 1280
  ]
  edge [
    source 9
    target 1281
  ]
  edge [
    source 9
    target 1282
  ]
  edge [
    source 9
    target 1283
  ]
  edge [
    source 9
    target 1284
  ]
  edge [
    source 9
    target 1285
  ]
  edge [
    source 9
    target 1286
  ]
  edge [
    source 9
    target 1287
  ]
  edge [
    source 9
    target 1288
  ]
  edge [
    source 9
    target 1289
  ]
  edge [
    source 9
    target 1290
  ]
  edge [
    source 9
    target 1291
  ]
  edge [
    source 9
    target 1292
  ]
  edge [
    source 9
    target 1293
  ]
  edge [
    source 9
    target 1294
  ]
  edge [
    source 9
    target 1295
  ]
  edge [
    source 9
    target 1296
  ]
]
