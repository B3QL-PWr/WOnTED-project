graph [
  node [
    id 0
    label "nast&#281;pny"
    origin "text"
  ]
  node [
    id 1
    label "dla"
    origin "text"
  ]
  node [
    id 2
    label "linia"
    origin "text"
  ]
  node [
    id 3
    label "sto"
    origin "text"
  ]
  node [
    id 4
    label "trzydzie&#347;ci"
    origin "text"
  ]
  node [
    id 5
    label "prosi&#263;"
    origin "text"
  ]
  node [
    id 6
    label "pan"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 8
    label "przystanek"
    origin "text"
  ]
  node [
    id 9
    label "kostrzewski"
    origin "text"
  ]
  node [
    id 10
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "che&#322;mski"
    origin "text"
  ]
  node [
    id 13
    label "dopiero"
    origin "text"
  ]
  node [
    id 14
    label "nast&#281;pnie"
  ]
  node [
    id 15
    label "kolejno"
  ]
  node [
    id 16
    label "nastopny"
  ]
  node [
    id 17
    label "kolejny"
  ]
  node [
    id 18
    label "curve"
  ]
  node [
    id 19
    label "granica"
  ]
  node [
    id 20
    label "roz&#322;&#261;cza&#263;"
  ]
  node [
    id 21
    label "fragment"
  ]
  node [
    id 22
    label "szczep"
  ]
  node [
    id 23
    label "roz&#322;&#261;czy&#263;"
  ]
  node [
    id 24
    label "przeorientowanie"
  ]
  node [
    id 25
    label "point"
  ]
  node [
    id 26
    label "grupa_organizm&#243;w"
  ]
  node [
    id 27
    label "roz&#322;&#261;czenie"
  ]
  node [
    id 28
    label "przew&#243;d"
  ]
  node [
    id 29
    label "jard"
  ]
  node [
    id 30
    label "poprowadzi&#263;"
  ]
  node [
    id 31
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "zbi&#243;r"
  ]
  node [
    id 33
    label "tekst"
  ]
  node [
    id 34
    label "line"
  ]
  node [
    id 35
    label "koniec"
  ]
  node [
    id 36
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 37
    label "prowadzi&#263;"
  ]
  node [
    id 38
    label "Ural"
  ]
  node [
    id 39
    label "prowadzenie"
  ]
  node [
    id 40
    label "po&#322;&#261;czenie"
  ]
  node [
    id 41
    label "przewo&#378;nik"
  ]
  node [
    id 42
    label "przeorientowywanie"
  ]
  node [
    id 43
    label "coalescence"
  ]
  node [
    id 44
    label "prostoliniowo&#347;&#263;"
  ]
  node [
    id 45
    label "billing"
  ]
  node [
    id 46
    label "transporter"
  ]
  node [
    id 47
    label "cz&#322;owiek"
  ]
  node [
    id 48
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 49
    label "materia&#322;_zecerski"
  ]
  node [
    id 50
    label "sztrych"
  ]
  node [
    id 51
    label "drzewo_genealogiczne"
  ]
  node [
    id 52
    label "cecha"
  ]
  node [
    id 53
    label "linijka"
  ]
  node [
    id 54
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 55
    label "granice"
  ]
  node [
    id 56
    label "budowa"
  ]
  node [
    id 57
    label "phreaker"
  ]
  node [
    id 58
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 59
    label "figura_geometryczna"
  ]
  node [
    id 60
    label "trasa"
  ]
  node [
    id 61
    label "przeorientowa&#263;"
  ]
  node [
    id 62
    label "bearing"
  ]
  node [
    id 63
    label "spos&#243;b"
  ]
  node [
    id 64
    label "szpaler"
  ]
  node [
    id 65
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 66
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 67
    label "roz&#322;&#261;czanie"
  ]
  node [
    id 68
    label "uporz&#261;dkowanie"
  ]
  node [
    id 69
    label "tract"
  ]
  node [
    id 70
    label "przeorientowywa&#263;"
  ]
  node [
    id 71
    label "wygl&#261;d"
  ]
  node [
    id 72
    label "armia"
  ]
  node [
    id 73
    label "kompleksja"
  ]
  node [
    id 74
    label "access"
  ]
  node [
    id 75
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 76
    label "rz&#261;d"
  ]
  node [
    id 77
    label "kszta&#322;t"
  ]
  node [
    id 78
    label "cord"
  ]
  node [
    id 79
    label "kontakt"
  ]
  node [
    id 80
    label "transportowiec"
  ]
  node [
    id 81
    label "firma"
  ]
  node [
    id 82
    label "struktura"
  ]
  node [
    id 83
    label "spowodowanie"
  ]
  node [
    id 84
    label "structure"
  ]
  node [
    id 85
    label "succession"
  ]
  node [
    id 86
    label "sequence"
  ]
  node [
    id 87
    label "ustalenie"
  ]
  node [
    id 88
    label "czynno&#347;&#263;"
  ]
  node [
    id 89
    label "number"
  ]
  node [
    id 90
    label "Londyn"
  ]
  node [
    id 91
    label "przybli&#380;enie"
  ]
  node [
    id 92
    label "premier"
  ]
  node [
    id 93
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 94
    label "egzekutywa"
  ]
  node [
    id 95
    label "klasa"
  ]
  node [
    id 96
    label "w&#322;adza"
  ]
  node [
    id 97
    label "Konsulat"
  ]
  node [
    id 98
    label "gabinet_cieni"
  ]
  node [
    id 99
    label "lon&#380;a"
  ]
  node [
    id 100
    label "gromada"
  ]
  node [
    id 101
    label "jednostka_systematyczna"
  ]
  node [
    id 102
    label "kategoria"
  ]
  node [
    id 103
    label "instytucja"
  ]
  node [
    id 104
    label "umo&#380;liwienie"
  ]
  node [
    id 105
    label "zestawienie"
  ]
  node [
    id 106
    label "stworzenie"
  ]
  node [
    id 107
    label "port"
  ]
  node [
    id 108
    label "mention"
  ]
  node [
    id 109
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 110
    label "zgrzeina"
  ]
  node [
    id 111
    label "element"
  ]
  node [
    id 112
    label "zjednoczenie"
  ]
  node [
    id 113
    label "zespolenie"
  ]
  node [
    id 114
    label "komunikacja"
  ]
  node [
    id 115
    label "pomy&#347;lenie"
  ]
  node [
    id 116
    label "akt_p&#322;ciowy"
  ]
  node [
    id 117
    label "joining"
  ]
  node [
    id 118
    label "rzucenie"
  ]
  node [
    id 119
    label "dressing"
  ]
  node [
    id 120
    label "zwi&#261;zany"
  ]
  node [
    id 121
    label "alliance"
  ]
  node [
    id 122
    label "poj&#281;cie"
  ]
  node [
    id 123
    label "miara"
  ]
  node [
    id 124
    label "zakres"
  ]
  node [
    id 125
    label "kres"
  ]
  node [
    id 126
    label "frontier"
  ]
  node [
    id 127
    label "pu&#322;ap"
  ]
  node [
    id 128
    label "end"
  ]
  node [
    id 129
    label "granica_pa&#324;stwa"
  ]
  node [
    id 130
    label "przej&#347;cie"
  ]
  node [
    id 131
    label "Rzym_Zachodni"
  ]
  node [
    id 132
    label "Rzym_Wschodni"
  ]
  node [
    id 133
    label "ilo&#347;&#263;"
  ]
  node [
    id 134
    label "whole"
  ]
  node [
    id 135
    label "urz&#261;dzenie"
  ]
  node [
    id 136
    label "styk"
  ]
  node [
    id 137
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 138
    label "&#322;&#261;cznik"
  ]
  node [
    id 139
    label "soczewka"
  ]
  node [
    id 140
    label "association"
  ]
  node [
    id 141
    label "wydarzenie"
  ]
  node [
    id 142
    label "zwi&#261;zek"
  ]
  node [
    id 143
    label "z&#322;&#261;czenie"
  ]
  node [
    id 144
    label "socket"
  ]
  node [
    id 145
    label "regulator"
  ]
  node [
    id 146
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 147
    label "communication"
  ]
  node [
    id 148
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 149
    label "instalacja_elektryczna"
  ]
  node [
    id 150
    label "linkage"
  ]
  node [
    id 151
    label "formacja_geologiczna"
  ]
  node [
    id 152
    label "contact"
  ]
  node [
    id 153
    label "katalizator"
  ]
  node [
    id 154
    label "punkt"
  ]
  node [
    id 155
    label "kres_&#380;ycia"
  ]
  node [
    id 156
    label "ostatnie_podrygi"
  ]
  node [
    id 157
    label "&#380;a&#322;oba"
  ]
  node [
    id 158
    label "zabicie"
  ]
  node [
    id 159
    label "pogrzebanie"
  ]
  node [
    id 160
    label "visitation"
  ]
  node [
    id 161
    label "miejsce"
  ]
  node [
    id 162
    label "agonia"
  ]
  node [
    id 163
    label "szeol"
  ]
  node [
    id 164
    label "szereg"
  ]
  node [
    id 165
    label "mogi&#322;a"
  ]
  node [
    id 166
    label "chwila"
  ]
  node [
    id 167
    label "dzia&#322;anie"
  ]
  node [
    id 168
    label "defenestracja"
  ]
  node [
    id 169
    label "ubarwienie"
  ]
  node [
    id 170
    label "widok"
  ]
  node [
    id 171
    label "postarzenie"
  ]
  node [
    id 172
    label "postarzy&#263;"
  ]
  node [
    id 173
    label "postarza&#263;"
  ]
  node [
    id 174
    label "brzydota"
  ]
  node [
    id 175
    label "shape"
  ]
  node [
    id 176
    label "postarzanie"
  ]
  node [
    id 177
    label "prostota"
  ]
  node [
    id 178
    label "nadawanie"
  ]
  node [
    id 179
    label "portrecista"
  ]
  node [
    id 180
    label "charakterystyka"
  ]
  node [
    id 181
    label "m&#322;ot"
  ]
  node [
    id 182
    label "marka"
  ]
  node [
    id 183
    label "pr&#243;ba"
  ]
  node [
    id 184
    label "attribute"
  ]
  node [
    id 185
    label "drzewo"
  ]
  node [
    id 186
    label "znak"
  ]
  node [
    id 187
    label "intencja"
  ]
  node [
    id 188
    label "leaning"
  ]
  node [
    id 189
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 190
    label "model"
  ]
  node [
    id 191
    label "narz&#281;dzie"
  ]
  node [
    id 192
    label "nature"
  ]
  node [
    id 193
    label "tryb"
  ]
  node [
    id 194
    label "spirala"
  ]
  node [
    id 195
    label "charakter"
  ]
  node [
    id 196
    label "miniatura"
  ]
  node [
    id 197
    label "blaszka"
  ]
  node [
    id 198
    label "g&#322;owa"
  ]
  node [
    id 199
    label "kielich"
  ]
  node [
    id 200
    label "p&#322;at"
  ]
  node [
    id 201
    label "pasmo"
  ]
  node [
    id 202
    label "obiekt"
  ]
  node [
    id 203
    label "comeliness"
  ]
  node [
    id 204
    label "face"
  ]
  node [
    id 205
    label "formacja"
  ]
  node [
    id 206
    label "gwiazda"
  ]
  node [
    id 207
    label "punkt_widzenia"
  ]
  node [
    id 208
    label "p&#281;tla"
  ]
  node [
    id 209
    label "linearno&#347;&#263;"
  ]
  node [
    id 210
    label "utw&#243;r"
  ]
  node [
    id 211
    label "pakiet_klimatyczny"
  ]
  node [
    id 212
    label "uprawianie"
  ]
  node [
    id 213
    label "collection"
  ]
  node [
    id 214
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 215
    label "gathering"
  ]
  node [
    id 216
    label "album"
  ]
  node [
    id 217
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 218
    label "praca_rolnicza"
  ]
  node [
    id 219
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 220
    label "sum"
  ]
  node [
    id 221
    label "egzemplarz"
  ]
  node [
    id 222
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 223
    label "series"
  ]
  node [
    id 224
    label "dane"
  ]
  node [
    id 225
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 226
    label "przebieg"
  ]
  node [
    id 227
    label "infrastruktura"
  ]
  node [
    id 228
    label "w&#281;ze&#322;"
  ]
  node [
    id 229
    label "podbieg"
  ]
  node [
    id 230
    label "marszrutyzacja"
  ]
  node [
    id 231
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 232
    label "droga"
  ]
  node [
    id 233
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 234
    label "espalier"
  ]
  node [
    id 235
    label "szyk"
  ]
  node [
    id 236
    label "aleja"
  ]
  node [
    id 237
    label "oddzia&#322;_liniowy"
  ]
  node [
    id 238
    label "pozycja"
  ]
  node [
    id 239
    label "rezerwa"
  ]
  node [
    id 240
    label "werbowanie_si&#281;"
  ]
  node [
    id 241
    label "Eurokorpus"
  ]
  node [
    id 242
    label "Armia_Czerwona"
  ]
  node [
    id 243
    label "potencja"
  ]
  node [
    id 244
    label "soldateska"
  ]
  node [
    id 245
    label "pospolite_ruszenie"
  ]
  node [
    id 246
    label "kawaleria_powietrzna"
  ]
  node [
    id 247
    label "mobilizowa&#263;"
  ]
  node [
    id 248
    label "mobilizowanie"
  ]
  node [
    id 249
    label "military"
  ]
  node [
    id 250
    label "tabor"
  ]
  node [
    id 251
    label "zrejterowanie"
  ]
  node [
    id 252
    label "korpus"
  ]
  node [
    id 253
    label "zdemobilizowanie"
  ]
  node [
    id 254
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 255
    label "zmobilizowanie"
  ]
  node [
    id 256
    label "oddzia&#322;"
  ]
  node [
    id 257
    label "Bia&#322;a_Gwardia"
  ]
  node [
    id 258
    label "oddzia&#322;_karny"
  ]
  node [
    id 259
    label "or&#281;&#380;"
  ]
  node [
    id 260
    label "artyleria"
  ]
  node [
    id 261
    label "si&#322;a"
  ]
  node [
    id 262
    label "obrona"
  ]
  node [
    id 263
    label "bateria"
  ]
  node [
    id 264
    label "dywizjon_kawaleryjski"
  ]
  node [
    id 265
    label "Armia_Krajowa"
  ]
  node [
    id 266
    label "rzut"
  ]
  node [
    id 267
    label "brygada"
  ]
  node [
    id 268
    label "wermacht"
  ]
  node [
    id 269
    label "szlak_bojowy"
  ]
  node [
    id 270
    label "milicja"
  ]
  node [
    id 271
    label "Czerwona_Gwardia"
  ]
  node [
    id 272
    label "wojska_pancerne"
  ]
  node [
    id 273
    label "zmobilizowa&#263;"
  ]
  node [
    id 274
    label "legia"
  ]
  node [
    id 275
    label "zwi&#261;zek_operacyjny"
  ]
  node [
    id 276
    label "Legia_Cudzoziemska"
  ]
  node [
    id 277
    label "cofni&#281;cie"
  ]
  node [
    id 278
    label "dywizjon_artylerii"
  ]
  node [
    id 279
    label "zrejterowa&#263;"
  ]
  node [
    id 280
    label "t&#322;um"
  ]
  node [
    id 281
    label "Ukrai&#324;ska_Powsta&#324;cza_Armia"
  ]
  node [
    id 282
    label "zdemobilizowa&#263;"
  ]
  node [
    id 283
    label "wojsko"
  ]
  node [
    id 284
    label "rejterowa&#263;"
  ]
  node [
    id 285
    label "piechota"
  ]
  node [
    id 286
    label "rejterowanie"
  ]
  node [
    id 287
    label "relate"
  ]
  node [
    id 288
    label "zrobi&#263;"
  ]
  node [
    id 289
    label "zjednoczy&#263;"
  ]
  node [
    id 290
    label "incorporate"
  ]
  node [
    id 291
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 292
    label "connect"
  ]
  node [
    id 293
    label "spowodowa&#263;"
  ]
  node [
    id 294
    label "stworzy&#263;"
  ]
  node [
    id 295
    label "przerywanie"
  ]
  node [
    id 296
    label "prze&#322;&#261;czanie"
  ]
  node [
    id 297
    label "severance"
  ]
  node [
    id 298
    label "separation"
  ]
  node [
    id 299
    label "oddzielanie"
  ]
  node [
    id 300
    label "rozsuwanie"
  ]
  node [
    id 301
    label "od&#322;&#261;czanie"
  ]
  node [
    id 302
    label "rozdzielanie"
  ]
  node [
    id 303
    label "biling"
  ]
  node [
    id 304
    label "spis"
  ]
  node [
    id 305
    label "dissociation"
  ]
  node [
    id 306
    label "od&#322;&#261;czenie"
  ]
  node [
    id 307
    label "przerwanie"
  ]
  node [
    id 308
    label "rozdzielenie"
  ]
  node [
    id 309
    label "oddzielenie"
  ]
  node [
    id 310
    label "prze&#322;&#261;czenie"
  ]
  node [
    id 311
    label "rozdzieli&#263;"
  ]
  node [
    id 312
    label "amputate"
  ]
  node [
    id 313
    label "abstract"
  ]
  node [
    id 314
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 315
    label "przerwa&#263;"
  ]
  node [
    id 316
    label "detach"
  ]
  node [
    id 317
    label "oddzieli&#263;"
  ]
  node [
    id 318
    label "z&#322;odziej"
  ]
  node [
    id 319
    label "paj&#281;czarz"
  ]
  node [
    id 320
    label "przerywa&#263;"
  ]
  node [
    id 321
    label "part"
  ]
  node [
    id 322
    label "cover"
  ]
  node [
    id 323
    label "rozdziela&#263;"
  ]
  node [
    id 324
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 325
    label "oddziela&#263;"
  ]
  node [
    id 326
    label "gulf"
  ]
  node [
    id 327
    label "figura"
  ]
  node [
    id 328
    label "constitution"
  ]
  node [
    id 329
    label "miejsce_pracy"
  ]
  node [
    id 330
    label "wjazd"
  ]
  node [
    id 331
    label "zwierz&#281;"
  ]
  node [
    id 332
    label "praca"
  ]
  node [
    id 333
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 334
    label "konstrukcja"
  ]
  node [
    id 335
    label "r&#243;w"
  ]
  node [
    id 336
    label "mechanika"
  ]
  node [
    id 337
    label "kreacja"
  ]
  node [
    id 338
    label "posesja"
  ]
  node [
    id 339
    label "organ"
  ]
  node [
    id 340
    label "asymilowa&#263;"
  ]
  node [
    id 341
    label "nasada"
  ]
  node [
    id 342
    label "profanum"
  ]
  node [
    id 343
    label "wz&#243;r"
  ]
  node [
    id 344
    label "senior"
  ]
  node [
    id 345
    label "asymilowanie"
  ]
  node [
    id 346
    label "os&#322;abia&#263;"
  ]
  node [
    id 347
    label "homo_sapiens"
  ]
  node [
    id 348
    label "osoba"
  ]
  node [
    id 349
    label "ludzko&#347;&#263;"
  ]
  node [
    id 350
    label "Adam"
  ]
  node [
    id 351
    label "hominid"
  ]
  node [
    id 352
    label "posta&#263;"
  ]
  node [
    id 353
    label "polifag"
  ]
  node [
    id 354
    label "podw&#322;adny"
  ]
  node [
    id 355
    label "dwun&#243;g"
  ]
  node [
    id 356
    label "wapniak"
  ]
  node [
    id 357
    label "duch"
  ]
  node [
    id 358
    label "os&#322;abianie"
  ]
  node [
    id 359
    label "antropochoria"
  ]
  node [
    id 360
    label "mikrokosmos"
  ]
  node [
    id 361
    label "oddzia&#322;ywanie"
  ]
  node [
    id 362
    label "kierunek"
  ]
  node [
    id 363
    label "zmienienie"
  ]
  node [
    id 364
    label "zmieni&#263;"
  ]
  node [
    id 365
    label "zmienia&#263;"
  ]
  node [
    id 366
    label "zmienianie"
  ]
  node [
    id 367
    label "wprowadzanie"
  ]
  node [
    id 368
    label "g&#243;rowanie"
  ]
  node [
    id 369
    label "ukierunkowywanie"
  ]
  node [
    id 370
    label "poprowadzenie"
  ]
  node [
    id 371
    label "eksponowanie"
  ]
  node [
    id 372
    label "doprowadzenie"
  ]
  node [
    id 373
    label "przeci&#261;ganie"
  ]
  node [
    id 374
    label "sterowanie"
  ]
  node [
    id 375
    label "dysponowanie"
  ]
  node [
    id 376
    label "kszta&#322;towanie"
  ]
  node [
    id 377
    label "management"
  ]
  node [
    id 378
    label "powodowanie"
  ]
  node [
    id 379
    label "trzymanie"
  ]
  node [
    id 380
    label "dawanie"
  ]
  node [
    id 381
    label "linia_melodyczna"
  ]
  node [
    id 382
    label "drive"
  ]
  node [
    id 383
    label "wprowadzenie"
  ]
  node [
    id 384
    label "przywodzenie"
  ]
  node [
    id 385
    label "aim"
  ]
  node [
    id 386
    label "lead"
  ]
  node [
    id 387
    label "oprowadzenie"
  ]
  node [
    id 388
    label "prowadzanie"
  ]
  node [
    id 389
    label "oprowadzanie"
  ]
  node [
    id 390
    label "przewy&#380;szanie"
  ]
  node [
    id 391
    label "kierowanie"
  ]
  node [
    id 392
    label "zwracanie"
  ]
  node [
    id 393
    label "przeci&#281;cie"
  ]
  node [
    id 394
    label "granie"
  ]
  node [
    id 395
    label "ta&#324;czenie"
  ]
  node [
    id 396
    label "kre&#347;lenie"
  ]
  node [
    id 397
    label "ci&#261;gni&#281;cie_si&#281;"
  ]
  node [
    id 398
    label "zaprowadzanie"
  ]
  node [
    id 399
    label "pozarz&#261;dzanie"
  ]
  node [
    id 400
    label "robienie"
  ]
  node [
    id 401
    label "krzywa"
  ]
  node [
    id 402
    label "przecinanie"
  ]
  node [
    id 403
    label "doprowadzanie"
  ]
  node [
    id 404
    label "eksponowa&#263;"
  ]
  node [
    id 405
    label "g&#243;rowa&#263;"
  ]
  node [
    id 406
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 407
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 408
    label "sterowa&#263;"
  ]
  node [
    id 409
    label "kierowa&#263;"
  ]
  node [
    id 410
    label "string"
  ]
  node [
    id 411
    label "control"
  ]
  node [
    id 412
    label "kre&#347;li&#263;"
  ]
  node [
    id 413
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 414
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 415
    label "&#380;y&#263;"
  ]
  node [
    id 416
    label "partner"
  ]
  node [
    id 417
    label "ukierunkowywa&#263;"
  ]
  node [
    id 418
    label "przesuwa&#263;"
  ]
  node [
    id 419
    label "tworzy&#263;"
  ]
  node [
    id 420
    label "powodowa&#263;"
  ]
  node [
    id 421
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 422
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 423
    label "message"
  ]
  node [
    id 424
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 425
    label "robi&#263;"
  ]
  node [
    id 426
    label "navigate"
  ]
  node [
    id 427
    label "manipulate"
  ]
  node [
    id 428
    label "nakre&#347;li&#263;"
  ]
  node [
    id 429
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 430
    label "doprowadzi&#263;"
  ]
  node [
    id 431
    label "leave"
  ]
  node [
    id 432
    label "zbudowa&#263;"
  ]
  node [
    id 433
    label "guidebook"
  ]
  node [
    id 434
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 435
    label "moderate"
  ]
  node [
    id 436
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 437
    label "teriologia"
  ]
  node [
    id 438
    label "Wizygoci"
  ]
  node [
    id 439
    label "Maroni"
  ]
  node [
    id 440
    label "Wenedowie"
  ]
  node [
    id 441
    label "Tocharowie"
  ]
  node [
    id 442
    label "dru&#380;yna_harcerska"
  ]
  node [
    id 443
    label "Nawahowie"
  ]
  node [
    id 444
    label "ornitologia"
  ]
  node [
    id 445
    label "Ladynowie"
  ]
  node [
    id 446
    label "odmiana"
  ]
  node [
    id 447
    label "podk&#322;ad"
  ]
  node [
    id 448
    label "ro&#347;lina"
  ]
  node [
    id 449
    label "Po&#322;owcy"
  ]
  node [
    id 450
    label "Kozacy"
  ]
  node [
    id 451
    label "podgromada"
  ]
  node [
    id 452
    label "gatunek"
  ]
  node [
    id 453
    label "paleontologia"
  ]
  node [
    id 454
    label "grupa_etniczna"
  ]
  node [
    id 455
    label "Ugrowie"
  ]
  node [
    id 456
    label "strain"
  ]
  node [
    id 457
    label "Do&#322;ganie"
  ]
  node [
    id 458
    label "Dogonowie"
  ]
  node [
    id 459
    label "Negryci"
  ]
  node [
    id 460
    label "plemi&#281;"
  ]
  node [
    id 461
    label "linia_filogenetyczna"
  ]
  node [
    id 462
    label "Nogajowie"
  ]
  node [
    id 463
    label "Paleoazjaci"
  ]
  node [
    id 464
    label "Majowie"
  ]
  node [
    id 465
    label "Tagalowie"
  ]
  node [
    id 466
    label "Indoariowie"
  ]
  node [
    id 467
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 468
    label "Frygijczycy"
  ]
  node [
    id 469
    label "Kumbrowie"
  ]
  node [
    id 470
    label "podrodzina"
  ]
  node [
    id 471
    label "Indoira&#324;czycy"
  ]
  node [
    id 472
    label "zoosocjologia"
  ]
  node [
    id 473
    label "Kipczacy"
  ]
  node [
    id 474
    label "hufiec"
  ]
  node [
    id 475
    label "Retowie"
  ]
  node [
    id 476
    label "mikrobiologia"
  ]
  node [
    id 477
    label "obszar"
  ]
  node [
    id 478
    label "pisa&#263;"
  ]
  node [
    id 479
    label "redakcja"
  ]
  node [
    id 480
    label "j&#281;zykowo"
  ]
  node [
    id 481
    label "preparacja"
  ]
  node [
    id 482
    label "dzie&#322;o"
  ]
  node [
    id 483
    label "wypowied&#378;"
  ]
  node [
    id 484
    label "obelga"
  ]
  node [
    id 485
    label "wytw&#243;r"
  ]
  node [
    id 486
    label "odmianka"
  ]
  node [
    id 487
    label "opu&#347;ci&#263;"
  ]
  node [
    id 488
    label "pomini&#281;cie"
  ]
  node [
    id 489
    label "koniektura"
  ]
  node [
    id 490
    label "ekscerpcja"
  ]
  node [
    id 491
    label "stopa"
  ]
  node [
    id 492
    label "cal"
  ]
  node [
    id 493
    label "przyrz&#261;d"
  ]
  node [
    id 494
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 495
    label "artyku&#322;"
  ]
  node [
    id 496
    label "przyk&#322;adnica"
  ]
  node [
    id 497
    label "rule"
  ]
  node [
    id 498
    label "pojazd_niemechaniczny"
  ]
  node [
    id 499
    label "Eurazja"
  ]
  node [
    id 500
    label "stanowisko"
  ]
  node [
    id 501
    label "dostawczak"
  ]
  node [
    id 502
    label "kube&#322;"
  ]
  node [
    id 503
    label "Transporter"
  ]
  node [
    id 504
    label "bia&#322;ko"
  ]
  node [
    id 505
    label "samoch&#243;d"
  ]
  node [
    id 506
    label "bro&#324;_pancerna"
  ]
  node [
    id 507
    label "zabierak"
  ]
  node [
    id 508
    label "volkswagen"
  ]
  node [
    id 509
    label "zgrzeb&#322;o"
  ]
  node [
    id 510
    label "bro&#324;"
  ]
  node [
    id 511
    label "ta&#347;ma"
  ]
  node [
    id 512
    label "van"
  ]
  node [
    id 513
    label "pojemnik"
  ]
  node [
    id 514
    label "opancerzony_pojazd_bojowy"
  ]
  node [
    id 515
    label "nosiwo"
  ]
  node [
    id 516
    label "divider"
  ]
  node [
    id 517
    label "ta&#347;moci&#261;g"
  ]
  node [
    id 518
    label "ejektor"
  ]
  node [
    id 519
    label "rozprawa"
  ]
  node [
    id 520
    label "wtyczka"
  ]
  node [
    id 521
    label "kognicja"
  ]
  node [
    id 522
    label "duct"
  ]
  node [
    id 523
    label "tr&#243;jnik"
  ]
  node [
    id 524
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 525
    label "przewodnictwo"
  ]
  node [
    id 526
    label "przes&#322;anka"
  ]
  node [
    id 527
    label "przy&#322;&#261;cze"
  ]
  node [
    id 528
    label "&#380;y&#322;a"
  ]
  node [
    id 529
    label "post&#281;powanie"
  ]
  node [
    id 530
    label "pies"
  ]
  node [
    id 531
    label "suffice"
  ]
  node [
    id 532
    label "invite"
  ]
  node [
    id 533
    label "trwa&#263;"
  ]
  node [
    id 534
    label "zach&#281;ca&#263;"
  ]
  node [
    id 535
    label "dziwi&#263;_si&#281;"
  ]
  node [
    id 536
    label "ask"
  ]
  node [
    id 537
    label "preach"
  ]
  node [
    id 538
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 539
    label "zaprasza&#263;"
  ]
  node [
    id 540
    label "poleca&#263;"
  ]
  node [
    id 541
    label "zezwala&#263;"
  ]
  node [
    id 542
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 543
    label "oferowa&#263;"
  ]
  node [
    id 544
    label "adhere"
  ]
  node [
    id 545
    label "pozostawa&#263;"
  ]
  node [
    id 546
    label "stand"
  ]
  node [
    id 547
    label "zostawa&#263;"
  ]
  node [
    id 548
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 549
    label "istnie&#263;"
  ]
  node [
    id 550
    label "placard"
  ]
  node [
    id 551
    label "wydawa&#263;"
  ]
  node [
    id 552
    label "doradza&#263;"
  ]
  node [
    id 553
    label "m&#243;wi&#263;"
  ]
  node [
    id 554
    label "zadawa&#263;"
  ]
  node [
    id 555
    label "ordynowa&#263;"
  ]
  node [
    id 556
    label "powierza&#263;"
  ]
  node [
    id 557
    label "charge"
  ]
  node [
    id 558
    label "pozyskiwa&#263;"
  ]
  node [
    id 559
    label "act"
  ]
  node [
    id 560
    label "uznawa&#263;"
  ]
  node [
    id 561
    label "authorize"
  ]
  node [
    id 562
    label "dogoterapia"
  ]
  node [
    id 563
    label "wycie"
  ]
  node [
    id 564
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 565
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 566
    label "sobaka"
  ]
  node [
    id 567
    label "Cerber"
  ]
  node [
    id 568
    label "trufla"
  ]
  node [
    id 569
    label "istota_&#380;ywa"
  ]
  node [
    id 570
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 571
    label "rakarz"
  ]
  node [
    id 572
    label "wyzwisko"
  ]
  node [
    id 573
    label "spragniony"
  ]
  node [
    id 574
    label "szczucie"
  ]
  node [
    id 575
    label "policjant"
  ]
  node [
    id 576
    label "piese&#322;"
  ]
  node [
    id 577
    label "samiec"
  ]
  node [
    id 578
    label "s&#322;u&#380;enie"
  ]
  node [
    id 579
    label "czworon&#243;g"
  ]
  node [
    id 580
    label "kabanos"
  ]
  node [
    id 581
    label "psowate"
  ]
  node [
    id 582
    label "szczeka&#263;"
  ]
  node [
    id 583
    label "wy&#263;"
  ]
  node [
    id 584
    label "szczu&#263;"
  ]
  node [
    id 585
    label "&#322;ajdak"
  ]
  node [
    id 586
    label "zawy&#263;"
  ]
  node [
    id 587
    label "murza"
  ]
  node [
    id 588
    label "belfer"
  ]
  node [
    id 589
    label "szkolnik"
  ]
  node [
    id 590
    label "pupil"
  ]
  node [
    id 591
    label "ojciec"
  ]
  node [
    id 592
    label "kszta&#322;ciciel"
  ]
  node [
    id 593
    label "Midas"
  ]
  node [
    id 594
    label "przyw&#243;dca"
  ]
  node [
    id 595
    label "opiekun"
  ]
  node [
    id 596
    label "Mieszko_I"
  ]
  node [
    id 597
    label "doros&#322;y"
  ]
  node [
    id 598
    label "pracodawca"
  ]
  node [
    id 599
    label "profesor"
  ]
  node [
    id 600
    label "m&#261;&#380;"
  ]
  node [
    id 601
    label "rz&#261;dzenie"
  ]
  node [
    id 602
    label "bogaty"
  ]
  node [
    id 603
    label "pa&#324;stwo"
  ]
  node [
    id 604
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 605
    label "w&#322;odarz"
  ]
  node [
    id 606
    label "nabab"
  ]
  node [
    id 607
    label "preceptor"
  ]
  node [
    id 608
    label "pedagog"
  ]
  node [
    id 609
    label "efendi"
  ]
  node [
    id 610
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 611
    label "popularyzator"
  ]
  node [
    id 612
    label "gra_w_karty"
  ]
  node [
    id 613
    label "zwrot"
  ]
  node [
    id 614
    label "jegomo&#347;&#263;"
  ]
  node [
    id 615
    label "androlog"
  ]
  node [
    id 616
    label "bratek"
  ]
  node [
    id 617
    label "andropauza"
  ]
  node [
    id 618
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 619
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 620
    label "ch&#322;opina"
  ]
  node [
    id 621
    label "Sabataj_Cwi"
  ]
  node [
    id 622
    label "lider"
  ]
  node [
    id 623
    label "Mao"
  ]
  node [
    id 624
    label "Anders"
  ]
  node [
    id 625
    label "Fidel_Castro"
  ]
  node [
    id 626
    label "Miko&#322;ajczyk"
  ]
  node [
    id 627
    label "Tito"
  ]
  node [
    id 628
    label "Ko&#347;ciuszko"
  ]
  node [
    id 629
    label "zwierzchnik"
  ]
  node [
    id 630
    label "p&#322;atnik"
  ]
  node [
    id 631
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 632
    label "nadzorca"
  ]
  node [
    id 633
    label "funkcjonariusz"
  ]
  node [
    id 634
    label "podmiot"
  ]
  node [
    id 635
    label "wykupywanie"
  ]
  node [
    id 636
    label "wykupienie"
  ]
  node [
    id 637
    label "bycie_w_posiadaniu"
  ]
  node [
    id 638
    label "rozszerzyciel"
  ]
  node [
    id 639
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 640
    label "du&#380;y"
  ]
  node [
    id 641
    label "dojrza&#322;y"
  ]
  node [
    id 642
    label "dojrzale"
  ]
  node [
    id 643
    label "wydoro&#347;lenie"
  ]
  node [
    id 644
    label "doro&#347;lenie"
  ]
  node [
    id 645
    label "m&#261;dry"
  ]
  node [
    id 646
    label "&#378;ra&#322;y"
  ]
  node [
    id 647
    label "doletni"
  ]
  node [
    id 648
    label "doro&#347;le"
  ]
  node [
    id 649
    label "zmiana"
  ]
  node [
    id 650
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 651
    label "turn"
  ]
  node [
    id 652
    label "wyra&#380;enie"
  ]
  node [
    id 653
    label "fraza_czasownikowa"
  ]
  node [
    id 654
    label "turning"
  ]
  node [
    id 655
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 656
    label "skr&#281;t"
  ]
  node [
    id 657
    label "jednostka_leksykalna"
  ]
  node [
    id 658
    label "obr&#243;t"
  ]
  node [
    id 659
    label "starosta"
  ]
  node [
    id 660
    label "w&#322;adca"
  ]
  node [
    id 661
    label "zarz&#261;dca"
  ]
  node [
    id 662
    label "nauczyciel"
  ]
  node [
    id 663
    label "autor"
  ]
  node [
    id 664
    label "szko&#322;a"
  ]
  node [
    id 665
    label "tarcza"
  ]
  node [
    id 666
    label "elew"
  ]
  node [
    id 667
    label "wyprawka"
  ]
  node [
    id 668
    label "mundurek"
  ]
  node [
    id 669
    label "absolwent"
  ]
  node [
    id 670
    label "nauczyciel_akademicki"
  ]
  node [
    id 671
    label "tytu&#322;"
  ]
  node [
    id 672
    label "stopie&#324;_naukowy"
  ]
  node [
    id 673
    label "konsulent"
  ]
  node [
    id 674
    label "profesura"
  ]
  node [
    id 675
    label "wirtuoz"
  ]
  node [
    id 676
    label "ochotnik"
  ]
  node [
    id 677
    label "nauczyciel_muzyki"
  ]
  node [
    id 678
    label "pomocnik"
  ]
  node [
    id 679
    label "zakonnik"
  ]
  node [
    id 680
    label "student"
  ]
  node [
    id 681
    label "ekspert"
  ]
  node [
    id 682
    label "bogacz"
  ]
  node [
    id 683
    label "dostojnik"
  ]
  node [
    id 684
    label "urz&#281;dnik"
  ]
  node [
    id 685
    label "mo&#347;&#263;"
  ]
  node [
    id 686
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 687
    label "&#347;w"
  ]
  node [
    id 688
    label "rodzic"
  ]
  node [
    id 689
    label "pomys&#322;odawca"
  ]
  node [
    id 690
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 691
    label "rodzice"
  ]
  node [
    id 692
    label "wykonawca"
  ]
  node [
    id 693
    label "stary"
  ]
  node [
    id 694
    label "kuwada"
  ]
  node [
    id 695
    label "ojczym"
  ]
  node [
    id 696
    label "papa"
  ]
  node [
    id 697
    label "przodek"
  ]
  node [
    id 698
    label "tworzyciel"
  ]
  node [
    id 699
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 700
    label "facet"
  ]
  node [
    id 701
    label "kochanek"
  ]
  node [
    id 702
    label "fio&#322;ek"
  ]
  node [
    id 703
    label "brat"
  ]
  node [
    id 704
    label "pan_i_w&#322;adca"
  ]
  node [
    id 705
    label "pan_m&#322;ody"
  ]
  node [
    id 706
    label "ch&#322;op"
  ]
  node [
    id 707
    label "&#347;lubny"
  ]
  node [
    id 708
    label "m&#243;j"
  ]
  node [
    id 709
    label "pan_domu"
  ]
  node [
    id 710
    label "ma&#322;&#380;onek"
  ]
  node [
    id 711
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 712
    label "Frygia"
  ]
  node [
    id 713
    label "dominowanie"
  ]
  node [
    id 714
    label "reign"
  ]
  node [
    id 715
    label "sprawowanie"
  ]
  node [
    id 716
    label "dominion"
  ]
  node [
    id 717
    label "zwierz&#281;_domowe"
  ]
  node [
    id 718
    label "John_Dewey"
  ]
  node [
    id 719
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 720
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 721
    label "J&#281;drzejewicz"
  ]
  node [
    id 722
    label "specjalista"
  ]
  node [
    id 723
    label "&#380;ycie"
  ]
  node [
    id 724
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 725
    label "Turek"
  ]
  node [
    id 726
    label "effendi"
  ]
  node [
    id 727
    label "och&#281;do&#380;ny"
  ]
  node [
    id 728
    label "zapa&#347;ny"
  ]
  node [
    id 729
    label "sytuowany"
  ]
  node [
    id 730
    label "obfituj&#261;cy"
  ]
  node [
    id 731
    label "forsiasty"
  ]
  node [
    id 732
    label "spania&#322;y"
  ]
  node [
    id 733
    label "obficie"
  ]
  node [
    id 734
    label "r&#243;&#380;norodny"
  ]
  node [
    id 735
    label "bogato"
  ]
  node [
    id 736
    label "Japonia"
  ]
  node [
    id 737
    label "Zair"
  ]
  node [
    id 738
    label "Belize"
  ]
  node [
    id 739
    label "San_Marino"
  ]
  node [
    id 740
    label "Tanzania"
  ]
  node [
    id 741
    label "Antigua_i_Barbuda"
  ]
  node [
    id 742
    label "Senegal"
  ]
  node [
    id 743
    label "Seszele"
  ]
  node [
    id 744
    label "Mauretania"
  ]
  node [
    id 745
    label "Indie"
  ]
  node [
    id 746
    label "Filipiny"
  ]
  node [
    id 747
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 748
    label "Zimbabwe"
  ]
  node [
    id 749
    label "Malezja"
  ]
  node [
    id 750
    label "Rumunia"
  ]
  node [
    id 751
    label "Surinam"
  ]
  node [
    id 752
    label "Ukraina"
  ]
  node [
    id 753
    label "Syria"
  ]
  node [
    id 754
    label "Wyspy_Marshalla"
  ]
  node [
    id 755
    label "Burkina_Faso"
  ]
  node [
    id 756
    label "Grecja"
  ]
  node [
    id 757
    label "Polska"
  ]
  node [
    id 758
    label "Wenezuela"
  ]
  node [
    id 759
    label "Suazi"
  ]
  node [
    id 760
    label "Nepal"
  ]
  node [
    id 761
    label "S&#322;owacja"
  ]
  node [
    id 762
    label "Algieria"
  ]
  node [
    id 763
    label "Chiny"
  ]
  node [
    id 764
    label "Grenada"
  ]
  node [
    id 765
    label "Barbados"
  ]
  node [
    id 766
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 767
    label "Pakistan"
  ]
  node [
    id 768
    label "Niemcy"
  ]
  node [
    id 769
    label "Bahrajn"
  ]
  node [
    id 770
    label "Komory"
  ]
  node [
    id 771
    label "Australia"
  ]
  node [
    id 772
    label "Rodezja"
  ]
  node [
    id 773
    label "Malawi"
  ]
  node [
    id 774
    label "Gwinea"
  ]
  node [
    id 775
    label "Wehrlen"
  ]
  node [
    id 776
    label "Meksyk"
  ]
  node [
    id 777
    label "Liechtenstein"
  ]
  node [
    id 778
    label "Czarnog&#243;ra"
  ]
  node [
    id 779
    label "Wielka_Brytania"
  ]
  node [
    id 780
    label "Kuwejt"
  ]
  node [
    id 781
    label "Monako"
  ]
  node [
    id 782
    label "Angola"
  ]
  node [
    id 783
    label "Jemen"
  ]
  node [
    id 784
    label "Etiopia"
  ]
  node [
    id 785
    label "Madagaskar"
  ]
  node [
    id 786
    label "terytorium"
  ]
  node [
    id 787
    label "Kolumbia"
  ]
  node [
    id 788
    label "Portoryko"
  ]
  node [
    id 789
    label "Mauritius"
  ]
  node [
    id 790
    label "Kostaryka"
  ]
  node [
    id 791
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 792
    label "Tajlandia"
  ]
  node [
    id 793
    label "Argentyna"
  ]
  node [
    id 794
    label "Zambia"
  ]
  node [
    id 795
    label "Sri_Lanka"
  ]
  node [
    id 796
    label "Gwatemala"
  ]
  node [
    id 797
    label "Kirgistan"
  ]
  node [
    id 798
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 799
    label "Hiszpania"
  ]
  node [
    id 800
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 801
    label "Salwador"
  ]
  node [
    id 802
    label "Korea"
  ]
  node [
    id 803
    label "Macedonia"
  ]
  node [
    id 804
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 805
    label "Brunei"
  ]
  node [
    id 806
    label "Mozambik"
  ]
  node [
    id 807
    label "Turcja"
  ]
  node [
    id 808
    label "Kambod&#380;a"
  ]
  node [
    id 809
    label "Benin"
  ]
  node [
    id 810
    label "Bhutan"
  ]
  node [
    id 811
    label "Tunezja"
  ]
  node [
    id 812
    label "Austria"
  ]
  node [
    id 813
    label "Izrael"
  ]
  node [
    id 814
    label "Sierra_Leone"
  ]
  node [
    id 815
    label "Jamajka"
  ]
  node [
    id 816
    label "Rosja"
  ]
  node [
    id 817
    label "Rwanda"
  ]
  node [
    id 818
    label "holoarktyka"
  ]
  node [
    id 819
    label "Nigeria"
  ]
  node [
    id 820
    label "USA"
  ]
  node [
    id 821
    label "Oman"
  ]
  node [
    id 822
    label "Luksemburg"
  ]
  node [
    id 823
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 824
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 825
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 826
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 827
    label "Dominikana"
  ]
  node [
    id 828
    label "Irlandia"
  ]
  node [
    id 829
    label "Liban"
  ]
  node [
    id 830
    label "Hanower"
  ]
  node [
    id 831
    label "Estonia"
  ]
  node [
    id 832
    label "Iran"
  ]
  node [
    id 833
    label "Nowa_Zelandia"
  ]
  node [
    id 834
    label "Gabon"
  ]
  node [
    id 835
    label "Samoa"
  ]
  node [
    id 836
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 837
    label "S&#322;owenia"
  ]
  node [
    id 838
    label "Kiribati"
  ]
  node [
    id 839
    label "Egipt"
  ]
  node [
    id 840
    label "Togo"
  ]
  node [
    id 841
    label "Mongolia"
  ]
  node [
    id 842
    label "Sudan"
  ]
  node [
    id 843
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 844
    label "Bahamy"
  ]
  node [
    id 845
    label "Bangladesz"
  ]
  node [
    id 846
    label "partia"
  ]
  node [
    id 847
    label "Serbia"
  ]
  node [
    id 848
    label "Czechy"
  ]
  node [
    id 849
    label "Holandia"
  ]
  node [
    id 850
    label "Birma"
  ]
  node [
    id 851
    label "Albania"
  ]
  node [
    id 852
    label "Mikronezja"
  ]
  node [
    id 853
    label "Gambia"
  ]
  node [
    id 854
    label "Kazachstan"
  ]
  node [
    id 855
    label "interior"
  ]
  node [
    id 856
    label "Uzbekistan"
  ]
  node [
    id 857
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 858
    label "Malta"
  ]
  node [
    id 859
    label "Lesoto"
  ]
  node [
    id 860
    label "para"
  ]
  node [
    id 861
    label "Antarktis"
  ]
  node [
    id 862
    label "Andora"
  ]
  node [
    id 863
    label "Nauru"
  ]
  node [
    id 864
    label "Kuba"
  ]
  node [
    id 865
    label "Wietnam"
  ]
  node [
    id 866
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 867
    label "ziemia"
  ]
  node [
    id 868
    label "Kamerun"
  ]
  node [
    id 869
    label "Chorwacja"
  ]
  node [
    id 870
    label "Urugwaj"
  ]
  node [
    id 871
    label "Niger"
  ]
  node [
    id 872
    label "Turkmenistan"
  ]
  node [
    id 873
    label "Szwajcaria"
  ]
  node [
    id 874
    label "organizacja"
  ]
  node [
    id 875
    label "grupa"
  ]
  node [
    id 876
    label "Palau"
  ]
  node [
    id 877
    label "Litwa"
  ]
  node [
    id 878
    label "Gruzja"
  ]
  node [
    id 879
    label "Tajwan"
  ]
  node [
    id 880
    label "Kongo"
  ]
  node [
    id 881
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 882
    label "Honduras"
  ]
  node [
    id 883
    label "Boliwia"
  ]
  node [
    id 884
    label "Uganda"
  ]
  node [
    id 885
    label "Namibia"
  ]
  node [
    id 886
    label "Azerbejd&#380;an"
  ]
  node [
    id 887
    label "Erytrea"
  ]
  node [
    id 888
    label "Gujana"
  ]
  node [
    id 889
    label "Panama"
  ]
  node [
    id 890
    label "Somalia"
  ]
  node [
    id 891
    label "Burundi"
  ]
  node [
    id 892
    label "Tuwalu"
  ]
  node [
    id 893
    label "Libia"
  ]
  node [
    id 894
    label "Katar"
  ]
  node [
    id 895
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 896
    label "Sahara_Zachodnia"
  ]
  node [
    id 897
    label "Trynidad_i_Tobago"
  ]
  node [
    id 898
    label "Gwinea_Bissau"
  ]
  node [
    id 899
    label "Bu&#322;garia"
  ]
  node [
    id 900
    label "Fid&#380;i"
  ]
  node [
    id 901
    label "Nikaragua"
  ]
  node [
    id 902
    label "Tonga"
  ]
  node [
    id 903
    label "Timor_Wschodni"
  ]
  node [
    id 904
    label "Laos"
  ]
  node [
    id 905
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 906
    label "Ghana"
  ]
  node [
    id 907
    label "Brazylia"
  ]
  node [
    id 908
    label "Belgia"
  ]
  node [
    id 909
    label "Irak"
  ]
  node [
    id 910
    label "Peru"
  ]
  node [
    id 911
    label "Arabia_Saudyjska"
  ]
  node [
    id 912
    label "Indonezja"
  ]
  node [
    id 913
    label "Malediwy"
  ]
  node [
    id 914
    label "Afganistan"
  ]
  node [
    id 915
    label "Jordania"
  ]
  node [
    id 916
    label "Kenia"
  ]
  node [
    id 917
    label "Czad"
  ]
  node [
    id 918
    label "Liberia"
  ]
  node [
    id 919
    label "W&#281;gry"
  ]
  node [
    id 920
    label "Chile"
  ]
  node [
    id 921
    label "Mali"
  ]
  node [
    id 922
    label "Armenia"
  ]
  node [
    id 923
    label "Kanada"
  ]
  node [
    id 924
    label "Cypr"
  ]
  node [
    id 925
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 926
    label "Ekwador"
  ]
  node [
    id 927
    label "Mo&#322;dawia"
  ]
  node [
    id 928
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 929
    label "W&#322;ochy"
  ]
  node [
    id 930
    label "Wyspy_Salomona"
  ]
  node [
    id 931
    label "&#321;otwa"
  ]
  node [
    id 932
    label "D&#380;ibuti"
  ]
  node [
    id 933
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 934
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 935
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 936
    label "Portugalia"
  ]
  node [
    id 937
    label "Botswana"
  ]
  node [
    id 938
    label "Maroko"
  ]
  node [
    id 939
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 940
    label "Francja"
  ]
  node [
    id 941
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 942
    label "Dominika"
  ]
  node [
    id 943
    label "Paragwaj"
  ]
  node [
    id 944
    label "Tad&#380;ykistan"
  ]
  node [
    id 945
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 946
    label "Haiti"
  ]
  node [
    id 947
    label "Khitai"
  ]
  node [
    id 948
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 949
    label "postawi&#263;"
  ]
  node [
    id 950
    label "awansowa&#263;"
  ]
  node [
    id 951
    label "wakowa&#263;"
  ]
  node [
    id 952
    label "powierzanie"
  ]
  node [
    id 953
    label "po&#322;o&#380;enie"
  ]
  node [
    id 954
    label "pogl&#261;d"
  ]
  node [
    id 955
    label "awansowanie"
  ]
  node [
    id 956
    label "stawia&#263;"
  ]
  node [
    id 957
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 958
    label "stan"
  ]
  node [
    id 959
    label "equal"
  ]
  node [
    id 960
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 961
    label "chodzi&#263;"
  ]
  node [
    id 962
    label "uczestniczy&#263;"
  ]
  node [
    id 963
    label "obecno&#347;&#263;"
  ]
  node [
    id 964
    label "si&#281;ga&#263;"
  ]
  node [
    id 965
    label "mie&#263;_miejsce"
  ]
  node [
    id 966
    label "participate"
  ]
  node [
    id 967
    label "compass"
  ]
  node [
    id 968
    label "exsert"
  ]
  node [
    id 969
    label "get"
  ]
  node [
    id 970
    label "u&#380;ywa&#263;"
  ]
  node [
    id 971
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 972
    label "osi&#261;ga&#263;"
  ]
  node [
    id 973
    label "korzysta&#263;"
  ]
  node [
    id 974
    label "appreciation"
  ]
  node [
    id 975
    label "dociera&#263;"
  ]
  node [
    id 976
    label "mierzy&#263;"
  ]
  node [
    id 977
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 978
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 979
    label "being"
  ]
  node [
    id 980
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 981
    label "proceed"
  ]
  node [
    id 982
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 983
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 984
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 985
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 986
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 987
    label "str&#243;j"
  ]
  node [
    id 988
    label "krok"
  ]
  node [
    id 989
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 990
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 991
    label "przebiega&#263;"
  ]
  node [
    id 992
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 993
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 994
    label "continue"
  ]
  node [
    id 995
    label "carry"
  ]
  node [
    id 996
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 997
    label "wk&#322;ada&#263;"
  ]
  node [
    id 998
    label "p&#322;ywa&#263;"
  ]
  node [
    id 999
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1000
    label "bangla&#263;"
  ]
  node [
    id 1001
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1002
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1003
    label "bywa&#263;"
  ]
  node [
    id 1004
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1005
    label "dziama&#263;"
  ]
  node [
    id 1006
    label "run"
  ]
  node [
    id 1007
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1008
    label "Arakan"
  ]
  node [
    id 1009
    label "Teksas"
  ]
  node [
    id 1010
    label "Georgia"
  ]
  node [
    id 1011
    label "Maryland"
  ]
  node [
    id 1012
    label "warstwa"
  ]
  node [
    id 1013
    label "Luizjana"
  ]
  node [
    id 1014
    label "Massachusetts"
  ]
  node [
    id 1015
    label "Michigan"
  ]
  node [
    id 1016
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1017
    label "samopoczucie"
  ]
  node [
    id 1018
    label "Floryda"
  ]
  node [
    id 1019
    label "Ohio"
  ]
  node [
    id 1020
    label "Alaska"
  ]
  node [
    id 1021
    label "Nowy_Meksyk"
  ]
  node [
    id 1022
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1023
    label "wci&#281;cie"
  ]
  node [
    id 1024
    label "Kansas"
  ]
  node [
    id 1025
    label "Alabama"
  ]
  node [
    id 1026
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 1027
    label "Kalifornia"
  ]
  node [
    id 1028
    label "Wirginia"
  ]
  node [
    id 1029
    label "Nowy_York"
  ]
  node [
    id 1030
    label "Waszyngton"
  ]
  node [
    id 1031
    label "Pensylwania"
  ]
  node [
    id 1032
    label "wektor"
  ]
  node [
    id 1033
    label "Hawaje"
  ]
  node [
    id 1034
    label "state"
  ]
  node [
    id 1035
    label "poziom"
  ]
  node [
    id 1036
    label "jednostka_administracyjna"
  ]
  node [
    id 1037
    label "Illinois"
  ]
  node [
    id 1038
    label "Oklahoma"
  ]
  node [
    id 1039
    label "Jukatan"
  ]
  node [
    id 1040
    label "Arizona"
  ]
  node [
    id 1041
    label "Oregon"
  ]
  node [
    id 1042
    label "Goa"
  ]
  node [
    id 1043
    label "lubelski"
  ]
  node [
    id 1044
    label "po_che&#322;msku"
  ]
  node [
    id 1045
    label "polski"
  ]
  node [
    id 1046
    label "regionalny"
  ]
  node [
    id 1047
    label "po_lubelsku"
  ]
  node [
    id 1048
    label "par&#243;wka"
  ]
  node [
    id 1049
    label "cebularz"
  ]
  node [
    id 1050
    label "lubelak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 547
  ]
  edge [
    source 5
    target 548
  ]
  edge [
    source 5
    target 549
  ]
  edge [
    source 5
    target 550
  ]
  edge [
    source 5
    target 551
  ]
  edge [
    source 5
    target 552
  ]
  edge [
    source 5
    target 553
  ]
  edge [
    source 5
    target 554
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 555
  ]
  edge [
    source 5
    target 556
  ]
  edge [
    source 5
    target 557
  ]
  edge [
    source 5
    target 558
  ]
  edge [
    source 5
    target 559
  ]
  edge [
    source 5
    target 560
  ]
  edge [
    source 5
    target 561
  ]
  edge [
    source 5
    target 562
  ]
  edge [
    source 5
    target 563
  ]
  edge [
    source 5
    target 564
  ]
  edge [
    source 5
    target 565
  ]
  edge [
    source 5
    target 566
  ]
  edge [
    source 5
    target 567
  ]
  edge [
    source 5
    target 568
  ]
  edge [
    source 5
    target 569
  ]
  edge [
    source 5
    target 570
  ]
  edge [
    source 5
    target 571
  ]
  edge [
    source 5
    target 572
  ]
  edge [
    source 5
    target 573
  ]
  edge [
    source 5
    target 574
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 575
  ]
  edge [
    source 5
    target 576
  ]
  edge [
    source 5
    target 577
  ]
  edge [
    source 5
    target 578
  ]
  edge [
    source 5
    target 579
  ]
  edge [
    source 5
    target 580
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 582
  ]
  edge [
    source 5
    target 583
  ]
  edge [
    source 5
    target 584
  ]
  edge [
    source 5
    target 585
  ]
  edge [
    source 5
    target 586
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 769
  ]
  edge [
    source 6
    target 770
  ]
  edge [
    source 6
    target 771
  ]
  edge [
    source 6
    target 772
  ]
  edge [
    source 6
    target 773
  ]
  edge [
    source 6
    target 774
  ]
  edge [
    source 6
    target 775
  ]
  edge [
    source 6
    target 776
  ]
  edge [
    source 6
    target 777
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 6
    target 784
  ]
  edge [
    source 6
    target 785
  ]
  edge [
    source 6
    target 786
  ]
  edge [
    source 6
    target 787
  ]
  edge [
    source 6
    target 788
  ]
  edge [
    source 6
    target 789
  ]
  edge [
    source 6
    target 790
  ]
  edge [
    source 6
    target 791
  ]
  edge [
    source 6
    target 792
  ]
  edge [
    source 6
    target 793
  ]
  edge [
    source 6
    target 794
  ]
  edge [
    source 6
    target 795
  ]
  edge [
    source 6
    target 796
  ]
  edge [
    source 6
    target 797
  ]
  edge [
    source 6
    target 798
  ]
  edge [
    source 6
    target 799
  ]
  edge [
    source 6
    target 800
  ]
  edge [
    source 6
    target 801
  ]
  edge [
    source 6
    target 802
  ]
  edge [
    source 6
    target 803
  ]
  edge [
    source 6
    target 804
  ]
  edge [
    source 6
    target 805
  ]
  edge [
    source 6
    target 806
  ]
  edge [
    source 6
    target 807
  ]
  edge [
    source 6
    target 808
  ]
  edge [
    source 6
    target 809
  ]
  edge [
    source 6
    target 810
  ]
  edge [
    source 6
    target 811
  ]
  edge [
    source 6
    target 812
  ]
  edge [
    source 6
    target 813
  ]
  edge [
    source 6
    target 814
  ]
  edge [
    source 6
    target 815
  ]
  edge [
    source 6
    target 816
  ]
  edge [
    source 6
    target 817
  ]
  edge [
    source 6
    target 818
  ]
  edge [
    source 6
    target 819
  ]
  edge [
    source 6
    target 820
  ]
  edge [
    source 6
    target 821
  ]
  edge [
    source 6
    target 822
  ]
  edge [
    source 6
    target 823
  ]
  edge [
    source 6
    target 824
  ]
  edge [
    source 6
    target 825
  ]
  edge [
    source 6
    target 826
  ]
  edge [
    source 6
    target 827
  ]
  edge [
    source 6
    target 828
  ]
  edge [
    source 6
    target 829
  ]
  edge [
    source 6
    target 830
  ]
  edge [
    source 6
    target 831
  ]
  edge [
    source 6
    target 832
  ]
  edge [
    source 6
    target 833
  ]
  edge [
    source 6
    target 834
  ]
  edge [
    source 6
    target 835
  ]
  edge [
    source 6
    target 836
  ]
  edge [
    source 6
    target 837
  ]
  edge [
    source 6
    target 838
  ]
  edge [
    source 6
    target 839
  ]
  edge [
    source 6
    target 840
  ]
  edge [
    source 6
    target 841
  ]
  edge [
    source 6
    target 842
  ]
  edge [
    source 6
    target 843
  ]
  edge [
    source 6
    target 844
  ]
  edge [
    source 6
    target 845
  ]
  edge [
    source 6
    target 846
  ]
  edge [
    source 6
    target 847
  ]
  edge [
    source 6
    target 848
  ]
  edge [
    source 6
    target 849
  ]
  edge [
    source 6
    target 850
  ]
  edge [
    source 6
    target 851
  ]
  edge [
    source 6
    target 852
  ]
  edge [
    source 6
    target 853
  ]
  edge [
    source 6
    target 854
  ]
  edge [
    source 6
    target 855
  ]
  edge [
    source 6
    target 856
  ]
  edge [
    source 6
    target 857
  ]
  edge [
    source 6
    target 858
  ]
  edge [
    source 6
    target 859
  ]
  edge [
    source 6
    target 860
  ]
  edge [
    source 6
    target 861
  ]
  edge [
    source 6
    target 862
  ]
  edge [
    source 6
    target 863
  ]
  edge [
    source 6
    target 864
  ]
  edge [
    source 6
    target 865
  ]
  edge [
    source 6
    target 866
  ]
  edge [
    source 6
    target 867
  ]
  edge [
    source 6
    target 868
  ]
  edge [
    source 6
    target 869
  ]
  edge [
    source 6
    target 870
  ]
  edge [
    source 6
    target 871
  ]
  edge [
    source 6
    target 872
  ]
  edge [
    source 6
    target 873
  ]
  edge [
    source 6
    target 874
  ]
  edge [
    source 6
    target 875
  ]
  edge [
    source 6
    target 876
  ]
  edge [
    source 6
    target 877
  ]
  edge [
    source 6
    target 878
  ]
  edge [
    source 6
    target 879
  ]
  edge [
    source 6
    target 880
  ]
  edge [
    source 6
    target 881
  ]
  edge [
    source 6
    target 882
  ]
  edge [
    source 6
    target 883
  ]
  edge [
    source 6
    target 884
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 6
    target 892
  ]
  edge [
    source 6
    target 893
  ]
  edge [
    source 6
    target 894
  ]
  edge [
    source 6
    target 895
  ]
  edge [
    source 6
    target 896
  ]
  edge [
    source 6
    target 897
  ]
  edge [
    source 6
    target 898
  ]
  edge [
    source 6
    target 899
  ]
  edge [
    source 6
    target 900
  ]
  edge [
    source 6
    target 901
  ]
  edge [
    source 6
    target 902
  ]
  edge [
    source 6
    target 903
  ]
  edge [
    source 6
    target 904
  ]
  edge [
    source 6
    target 905
  ]
  edge [
    source 6
    target 906
  ]
  edge [
    source 6
    target 907
  ]
  edge [
    source 6
    target 908
  ]
  edge [
    source 6
    target 909
  ]
  edge [
    source 6
    target 910
  ]
  edge [
    source 6
    target 911
  ]
  edge [
    source 6
    target 912
  ]
  edge [
    source 6
    target 913
  ]
  edge [
    source 6
    target 914
  ]
  edge [
    source 6
    target 915
  ]
  edge [
    source 6
    target 916
  ]
  edge [
    source 6
    target 917
  ]
  edge [
    source 6
    target 918
  ]
  edge [
    source 6
    target 919
  ]
  edge [
    source 6
    target 920
  ]
  edge [
    source 6
    target 921
  ]
  edge [
    source 6
    target 922
  ]
  edge [
    source 6
    target 923
  ]
  edge [
    source 6
    target 924
  ]
  edge [
    source 6
    target 925
  ]
  edge [
    source 6
    target 926
  ]
  edge [
    source 6
    target 927
  ]
  edge [
    source 6
    target 928
  ]
  edge [
    source 6
    target 929
  ]
  edge [
    source 6
    target 930
  ]
  edge [
    source 6
    target 931
  ]
  edge [
    source 6
    target 932
  ]
  edge [
    source 6
    target 933
  ]
  edge [
    source 6
    target 934
  ]
  edge [
    source 6
    target 935
  ]
  edge [
    source 6
    target 936
  ]
  edge [
    source 6
    target 937
  ]
  edge [
    source 6
    target 938
  ]
  edge [
    source 6
    target 939
  ]
  edge [
    source 6
    target 940
  ]
  edge [
    source 6
    target 941
  ]
  edge [
    source 6
    target 942
  ]
  edge [
    source 6
    target 943
  ]
  edge [
    source 6
    target 944
  ]
  edge [
    source 6
    target 945
  ]
  edge [
    source 6
    target 946
  ]
  edge [
    source 6
    target 947
  ]
  edge [
    source 6
    target 948
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 154
  ]
  edge [
    source 8
    target 949
  ]
  edge [
    source 8
    target 950
  ]
  edge [
    source 8
    target 951
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 952
  ]
  edge [
    source 8
    target 953
  ]
  edge [
    source 8
    target 954
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 955
  ]
  edge [
    source 8
    target 956
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 957
  ]
  edge [
    source 11
    target 958
  ]
  edge [
    source 11
    target 546
  ]
  edge [
    source 11
    target 533
  ]
  edge [
    source 11
    target 959
  ]
  edge [
    source 11
    target 960
  ]
  edge [
    source 11
    target 961
  ]
  edge [
    source 11
    target 962
  ]
  edge [
    source 11
    target 963
  ]
  edge [
    source 11
    target 964
  ]
  edge [
    source 11
    target 965
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 966
  ]
  edge [
    source 11
    target 544
  ]
  edge [
    source 11
    target 545
  ]
  edge [
    source 11
    target 547
  ]
  edge [
    source 11
    target 548
  ]
  edge [
    source 11
    target 549
  ]
  edge [
    source 11
    target 967
  ]
  edge [
    source 11
    target 968
  ]
  edge [
    source 11
    target 969
  ]
  edge [
    source 11
    target 970
  ]
  edge [
    source 11
    target 971
  ]
  edge [
    source 11
    target 972
  ]
  edge [
    source 11
    target 973
  ]
  edge [
    source 11
    target 974
  ]
  edge [
    source 11
    target 975
  ]
  edge [
    source 11
    target 976
  ]
  edge [
    source 11
    target 977
  ]
  edge [
    source 11
    target 978
  ]
  edge [
    source 11
    target 979
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 980
  ]
  edge [
    source 11
    target 981
  ]
  edge [
    source 11
    target 982
  ]
  edge [
    source 11
    target 983
  ]
  edge [
    source 11
    target 984
  ]
  edge [
    source 11
    target 985
  ]
  edge [
    source 11
    target 986
  ]
  edge [
    source 11
    target 987
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 988
  ]
  edge [
    source 11
    target 989
  ]
  edge [
    source 11
    target 990
  ]
  edge [
    source 11
    target 991
  ]
  edge [
    source 11
    target 992
  ]
  edge [
    source 11
    target 993
  ]
  edge [
    source 11
    target 994
  ]
  edge [
    source 11
    target 995
  ]
  edge [
    source 11
    target 996
  ]
  edge [
    source 11
    target 997
  ]
  edge [
    source 11
    target 998
  ]
  edge [
    source 11
    target 999
  ]
  edge [
    source 11
    target 1000
  ]
  edge [
    source 11
    target 1001
  ]
  edge [
    source 11
    target 1002
  ]
  edge [
    source 11
    target 1003
  ]
  edge [
    source 11
    target 193
  ]
  edge [
    source 11
    target 1004
  ]
  edge [
    source 11
    target 1005
  ]
  edge [
    source 11
    target 1006
  ]
  edge [
    source 11
    target 1007
  ]
  edge [
    source 11
    target 1008
  ]
  edge [
    source 11
    target 1009
  ]
  edge [
    source 11
    target 1010
  ]
  edge [
    source 11
    target 1011
  ]
  edge [
    source 11
    target 1012
  ]
  edge [
    source 11
    target 1013
  ]
  edge [
    source 11
    target 1014
  ]
  edge [
    source 11
    target 1015
  ]
  edge [
    source 11
    target 1016
  ]
  edge [
    source 11
    target 1017
  ]
  edge [
    source 11
    target 1018
  ]
  edge [
    source 11
    target 1019
  ]
  edge [
    source 11
    target 1020
  ]
  edge [
    source 11
    target 1021
  ]
  edge [
    source 11
    target 1022
  ]
  edge [
    source 11
    target 1023
  ]
  edge [
    source 11
    target 1024
  ]
  edge [
    source 11
    target 1025
  ]
  edge [
    source 11
    target 161
  ]
  edge [
    source 11
    target 1026
  ]
  edge [
    source 11
    target 1027
  ]
  edge [
    source 11
    target 1028
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 1029
  ]
  edge [
    source 11
    target 1030
  ]
  edge [
    source 11
    target 1031
  ]
  edge [
    source 11
    target 1032
  ]
  edge [
    source 11
    target 1033
  ]
  edge [
    source 11
    target 1034
  ]
  edge [
    source 11
    target 1035
  ]
  edge [
    source 11
    target 1036
  ]
  edge [
    source 11
    target 1037
  ]
  edge [
    source 11
    target 1038
  ]
  edge [
    source 11
    target 1039
  ]
  edge [
    source 11
    target 1040
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 1041
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 1042
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
]
