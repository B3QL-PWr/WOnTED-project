graph [
  node [
    id 0
    label "gwiazda"
    origin "text"
  ]
  node [
    id 1
    label "pozosta&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 3
    label "inny"
    origin "text"
  ]
  node [
    id 4
    label "Arktur"
  ]
  node [
    id 5
    label "kszta&#322;t"
  ]
  node [
    id 6
    label "Gwiazda_Polarna"
  ]
  node [
    id 7
    label "agregatka"
  ]
  node [
    id 8
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 9
    label "gromada"
  ]
  node [
    id 10
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 11
    label "S&#322;o&#324;ce"
  ]
  node [
    id 12
    label "Nibiru"
  ]
  node [
    id 13
    label "konstelacja"
  ]
  node [
    id 14
    label "ornament"
  ]
  node [
    id 15
    label "delta_Scuti"
  ]
  node [
    id 16
    label "&#347;wiat&#322;o"
  ]
  node [
    id 17
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 18
    label "obiekt"
  ]
  node [
    id 19
    label "s&#322;awa"
  ]
  node [
    id 20
    label "promie&#324;"
  ]
  node [
    id 21
    label "star"
  ]
  node [
    id 22
    label "gwiazdosz"
  ]
  node [
    id 23
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 24
    label "asocjacja_gwiazd"
  ]
  node [
    id 25
    label "supergrupa"
  ]
  node [
    id 26
    label "kobierzec"
  ]
  node [
    id 27
    label "wz&#243;r"
  ]
  node [
    id 28
    label "ozdobnik"
  ]
  node [
    id 29
    label "brokatela"
  ]
  node [
    id 30
    label "zdobienie"
  ]
  node [
    id 31
    label "ilustracja"
  ]
  node [
    id 32
    label "ornamentacja"
  ]
  node [
    id 33
    label "kto&#347;"
  ]
  node [
    id 34
    label "renoma"
  ]
  node [
    id 35
    label "rozg&#322;os"
  ]
  node [
    id 36
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 37
    label "energia"
  ]
  node [
    id 38
    label "&#347;wieci&#263;"
  ]
  node [
    id 39
    label "odst&#281;p"
  ]
  node [
    id 40
    label "wpadni&#281;cie"
  ]
  node [
    id 41
    label "interpretacja"
  ]
  node [
    id 42
    label "zjawisko"
  ]
  node [
    id 43
    label "cecha"
  ]
  node [
    id 44
    label "fotokataliza"
  ]
  node [
    id 45
    label "promieniowanie_elektromagnetyczne"
  ]
  node [
    id 46
    label "wpa&#347;&#263;"
  ]
  node [
    id 47
    label "rzuca&#263;"
  ]
  node [
    id 48
    label "obsadnik"
  ]
  node [
    id 49
    label "promieniowanie_optyczne"
  ]
  node [
    id 50
    label "lampa"
  ]
  node [
    id 51
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 52
    label "ja&#347;nia"
  ]
  node [
    id 53
    label "light"
  ]
  node [
    id 54
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 55
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 56
    label "wpada&#263;"
  ]
  node [
    id 57
    label "rzuci&#263;"
  ]
  node [
    id 58
    label "o&#347;wietlenie"
  ]
  node [
    id 59
    label "punkt_widzenia"
  ]
  node [
    id 60
    label "&#347;wiat&#322;ocie&#324;"
  ]
  node [
    id 61
    label "przy&#263;mienie"
  ]
  node [
    id 62
    label "instalacja"
  ]
  node [
    id 63
    label "&#347;wiecenie"
  ]
  node [
    id 64
    label "radiance"
  ]
  node [
    id 65
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 66
    label "przy&#263;mi&#263;"
  ]
  node [
    id 67
    label "b&#322;ysk"
  ]
  node [
    id 68
    label "&#347;wiat&#322;y"
  ]
  node [
    id 69
    label "m&#261;drze"
  ]
  node [
    id 70
    label "rozja&#347;nia&#263;"
  ]
  node [
    id 71
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 72
    label "lighting"
  ]
  node [
    id 73
    label "lighter"
  ]
  node [
    id 74
    label "rzucenie"
  ]
  node [
    id 75
    label "plama"
  ]
  node [
    id 76
    label "&#347;rednica"
  ]
  node [
    id 77
    label "wpadanie"
  ]
  node [
    id 78
    label "przy&#263;miewanie"
  ]
  node [
    id 79
    label "rzucanie"
  ]
  node [
    id 80
    label "co&#347;"
  ]
  node [
    id 81
    label "budynek"
  ]
  node [
    id 82
    label "thing"
  ]
  node [
    id 83
    label "poj&#281;cie"
  ]
  node [
    id 84
    label "program"
  ]
  node [
    id 85
    label "rzecz"
  ]
  node [
    id 86
    label "strona"
  ]
  node [
    id 87
    label "wyrostek"
  ]
  node [
    id 88
    label "pi&#243;rko"
  ]
  node [
    id 89
    label "strumie&#324;"
  ]
  node [
    id 90
    label "odcinek"
  ]
  node [
    id 91
    label "zapowied&#378;"
  ]
  node [
    id 92
    label "odrobina"
  ]
  node [
    id 93
    label "rozeta"
  ]
  node [
    id 94
    label "grzyb"
  ]
  node [
    id 95
    label "pieczarniak"
  ]
  node [
    id 96
    label "grzyb_niejadalny"
  ]
  node [
    id 97
    label "gwiazdoszowce"
  ]
  node [
    id 98
    label "ro&#347;lina"
  ]
  node [
    id 99
    label "astrowate"
  ]
  node [
    id 100
    label "saprotrof"
  ]
  node [
    id 101
    label "zesp&#243;&#322;"
  ]
  node [
    id 102
    label "typ"
  ]
  node [
    id 103
    label "jednostka_administracyjna"
  ]
  node [
    id 104
    label "zoologia"
  ]
  node [
    id 105
    label "skupienie"
  ]
  node [
    id 106
    label "jednostka_systematyczna"
  ]
  node [
    id 107
    label "kr&#243;lestwo"
  ]
  node [
    id 108
    label "stage_set"
  ]
  node [
    id 109
    label "tribe"
  ]
  node [
    id 110
    label "hurma"
  ]
  node [
    id 111
    label "grupa"
  ]
  node [
    id 112
    label "botanika"
  ]
  node [
    id 113
    label "constellation"
  ]
  node [
    id 114
    label "struktura"
  ]
  node [
    id 115
    label "zbi&#243;r"
  ]
  node [
    id 116
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 117
    label "Ptak_Rajski"
  ]
  node [
    id 118
    label "W&#281;&#380;ownik"
  ]
  node [
    id 119
    label "Panna"
  ]
  node [
    id 120
    label "W&#261;&#380;"
  ]
  node [
    id 121
    label "Jowisz"
  ]
  node [
    id 122
    label "pochylanie_si&#281;"
  ]
  node [
    id 123
    label "pochyli&#263;_si&#281;"
  ]
  node [
    id 124
    label "pochyla&#263;_si&#281;"
  ]
  node [
    id 125
    label "apeks"
  ]
  node [
    id 126
    label "heliosfera"
  ]
  node [
    id 127
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 128
    label "s&#322;o&#324;ce"
  ]
  node [
    id 129
    label "pochylenie_si&#281;"
  ]
  node [
    id 130
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 131
    label "aspekt"
  ]
  node [
    id 132
    label "czas_s&#322;oneczny"
  ]
  node [
    id 133
    label "formacja"
  ]
  node [
    id 134
    label "wygl&#261;d"
  ]
  node [
    id 135
    label "g&#322;owa"
  ]
  node [
    id 136
    label "spirala"
  ]
  node [
    id 137
    label "p&#322;at"
  ]
  node [
    id 138
    label "comeliness"
  ]
  node [
    id 139
    label "kielich"
  ]
  node [
    id 140
    label "face"
  ]
  node [
    id 141
    label "blaszka"
  ]
  node [
    id 142
    label "charakter"
  ]
  node [
    id 143
    label "p&#281;tla"
  ]
  node [
    id 144
    label "pasmo"
  ]
  node [
    id 145
    label "linearno&#347;&#263;"
  ]
  node [
    id 146
    label "miniatura"
  ]
  node [
    id 147
    label "&#380;ywy"
  ]
  node [
    id 148
    label "kolejny"
  ]
  node [
    id 149
    label "osobno"
  ]
  node [
    id 150
    label "r&#243;&#380;ny"
  ]
  node [
    id 151
    label "inszy"
  ]
  node [
    id 152
    label "inaczej"
  ]
  node [
    id 153
    label "ciekawy"
  ]
  node [
    id 154
    label "szybki"
  ]
  node [
    id 155
    label "&#380;ywotny"
  ]
  node [
    id 156
    label "naturalny"
  ]
  node [
    id 157
    label "&#380;ywo"
  ]
  node [
    id 158
    label "cz&#322;owiek"
  ]
  node [
    id 159
    label "o&#380;ywianie"
  ]
  node [
    id 160
    label "&#380;ycie"
  ]
  node [
    id 161
    label "silny"
  ]
  node [
    id 162
    label "g&#322;&#281;boki"
  ]
  node [
    id 163
    label "wyra&#378;ny"
  ]
  node [
    id 164
    label "czynny"
  ]
  node [
    id 165
    label "aktualny"
  ]
  node [
    id 166
    label "zgrabny"
  ]
  node [
    id 167
    label "prawdziwy"
  ]
  node [
    id 168
    label "realistyczny"
  ]
  node [
    id 169
    label "energiczny"
  ]
  node [
    id 170
    label "defenestracja"
  ]
  node [
    id 171
    label "agonia"
  ]
  node [
    id 172
    label "kres"
  ]
  node [
    id 173
    label "mogi&#322;a"
  ]
  node [
    id 174
    label "kres_&#380;ycia"
  ]
  node [
    id 175
    label "upadek"
  ]
  node [
    id 176
    label "szeol"
  ]
  node [
    id 177
    label "pogrzebanie"
  ]
  node [
    id 178
    label "istota_nadprzyrodzona"
  ]
  node [
    id 179
    label "&#380;a&#322;oba"
  ]
  node [
    id 180
    label "pogrzeb"
  ]
  node [
    id 181
    label "zabicie"
  ]
  node [
    id 182
    label "ostatnie_podrygi"
  ]
  node [
    id 183
    label "punkt"
  ]
  node [
    id 184
    label "dzia&#322;anie"
  ]
  node [
    id 185
    label "chwila"
  ]
  node [
    id 186
    label "koniec"
  ]
  node [
    id 187
    label "gleba"
  ]
  node [
    id 188
    label "kondycja"
  ]
  node [
    id 189
    label "ruch"
  ]
  node [
    id 190
    label "pogorszenie"
  ]
  node [
    id 191
    label "inclination"
  ]
  node [
    id 192
    label "death"
  ]
  node [
    id 193
    label "zmierzch"
  ]
  node [
    id 194
    label "stan"
  ]
  node [
    id 195
    label "nieuleczalnie_chory"
  ]
  node [
    id 196
    label "spocz&#261;&#263;"
  ]
  node [
    id 197
    label "spocz&#281;cie"
  ]
  node [
    id 198
    label "pochowanie"
  ]
  node [
    id 199
    label "spoczywa&#263;"
  ]
  node [
    id 200
    label "chowanie"
  ]
  node [
    id 201
    label "park_sztywnych"
  ]
  node [
    id 202
    label "pomnik"
  ]
  node [
    id 203
    label "nagrobek"
  ]
  node [
    id 204
    label "prochowisko"
  ]
  node [
    id 205
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 206
    label "spoczywanie"
  ]
  node [
    id 207
    label "za&#347;wiaty"
  ]
  node [
    id 208
    label "piek&#322;o"
  ]
  node [
    id 209
    label "judaizm"
  ]
  node [
    id 210
    label "destruction"
  ]
  node [
    id 211
    label "zabrzmienie"
  ]
  node [
    id 212
    label "skrzywdzenie"
  ]
  node [
    id 213
    label "pozabijanie"
  ]
  node [
    id 214
    label "zniszczenie"
  ]
  node [
    id 215
    label "zaszkodzenie"
  ]
  node [
    id 216
    label "usuni&#281;cie"
  ]
  node [
    id 217
    label "spowodowanie"
  ]
  node [
    id 218
    label "killing"
  ]
  node [
    id 219
    label "zdarzenie_si&#281;"
  ]
  node [
    id 220
    label "czyn"
  ]
  node [
    id 221
    label "umarcie"
  ]
  node [
    id 222
    label "granie"
  ]
  node [
    id 223
    label "zamkni&#281;cie"
  ]
  node [
    id 224
    label "compaction"
  ]
  node [
    id 225
    label "&#380;al"
  ]
  node [
    id 226
    label "paznokie&#263;"
  ]
  node [
    id 227
    label "symbol"
  ]
  node [
    id 228
    label "czas"
  ]
  node [
    id 229
    label "kir"
  ]
  node [
    id 230
    label "brud"
  ]
  node [
    id 231
    label "wyrzucenie"
  ]
  node [
    id 232
    label "defenestration"
  ]
  node [
    id 233
    label "zaj&#347;cie"
  ]
  node [
    id 234
    label "burying"
  ]
  node [
    id 235
    label "zasypanie"
  ]
  node [
    id 236
    label "zw&#322;oki"
  ]
  node [
    id 237
    label "burial"
  ]
  node [
    id 238
    label "w&#322;o&#380;enie"
  ]
  node [
    id 239
    label "porobienie"
  ]
  node [
    id 240
    label "gr&#243;b"
  ]
  node [
    id 241
    label "uniemo&#380;liwienie"
  ]
  node [
    id 242
    label "niepowodzenie"
  ]
  node [
    id 243
    label "stypa"
  ]
  node [
    id 244
    label "pusta_noc"
  ]
  node [
    id 245
    label "grabarz"
  ]
  node [
    id 246
    label "ostatnia_pos&#322;uga"
  ]
  node [
    id 247
    label "obrz&#281;d"
  ]
  node [
    id 248
    label "raj_utracony"
  ]
  node [
    id 249
    label "umieranie"
  ]
  node [
    id 250
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 251
    label "prze&#380;ywanie"
  ]
  node [
    id 252
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 253
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 254
    label "po&#322;&#243;g"
  ]
  node [
    id 255
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 256
    label "subsistence"
  ]
  node [
    id 257
    label "power"
  ]
  node [
    id 258
    label "okres_noworodkowy"
  ]
  node [
    id 259
    label "prze&#380;ycie"
  ]
  node [
    id 260
    label "wiek_matuzalemowy"
  ]
  node [
    id 261
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 262
    label "entity"
  ]
  node [
    id 263
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 264
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 265
    label "do&#380;ywanie"
  ]
  node [
    id 266
    label "byt"
  ]
  node [
    id 267
    label "andropauza"
  ]
  node [
    id 268
    label "dzieci&#324;stwo"
  ]
  node [
    id 269
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 270
    label "rozw&#243;j"
  ]
  node [
    id 271
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 272
    label "menopauza"
  ]
  node [
    id 273
    label "koleje_losu"
  ]
  node [
    id 274
    label "bycie"
  ]
  node [
    id 275
    label "zegar_biologiczny"
  ]
  node [
    id 276
    label "szwung"
  ]
  node [
    id 277
    label "przebywanie"
  ]
  node [
    id 278
    label "warunki"
  ]
  node [
    id 279
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 280
    label "niemowl&#281;ctwo"
  ]
  node [
    id 281
    label "life"
  ]
  node [
    id 282
    label "staro&#347;&#263;"
  ]
  node [
    id 283
    label "energy"
  ]
  node [
    id 284
    label "odr&#281;bny"
  ]
  node [
    id 285
    label "nast&#281;pnie"
  ]
  node [
    id 286
    label "nastopny"
  ]
  node [
    id 287
    label "kolejno"
  ]
  node [
    id 288
    label "kt&#243;ry&#347;"
  ]
  node [
    id 289
    label "jaki&#347;"
  ]
  node [
    id 290
    label "r&#243;&#380;nie"
  ]
  node [
    id 291
    label "niestandardowo"
  ]
  node [
    id 292
    label "individually"
  ]
  node [
    id 293
    label "udzielnie"
  ]
  node [
    id 294
    label "osobnie"
  ]
  node [
    id 295
    label "odr&#281;bnie"
  ]
  node [
    id 296
    label "osobny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 2
    target 279
  ]
  edge [
    source 2
    target 280
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 281
  ]
  edge [
    source 2
    target 282
  ]
  edge [
    source 2
    target 283
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
]
