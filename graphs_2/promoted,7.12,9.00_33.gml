graph [
  node [
    id 0
    label "satysfakcjonuj&#261;cy"
    origin "text"
  ]
  node [
    id 1
    label "wideo"
    origin "text"
  ]
  node [
    id 2
    label "zadowalaj&#261;cy"
  ]
  node [
    id 3
    label "satysfakcjonuj&#261;co"
  ]
  node [
    id 4
    label "niez&#322;y"
  ]
  node [
    id 5
    label "odpowiedni"
  ]
  node [
    id 6
    label "zadowalaj&#261;co"
  ]
  node [
    id 7
    label "satisfactorily"
  ]
  node [
    id 8
    label "technika"
  ]
  node [
    id 9
    label "film"
  ]
  node [
    id 10
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 11
    label "wideokaseta"
  ]
  node [
    id 12
    label "odtwarzacz"
  ]
  node [
    id 13
    label "telekomunikacja"
  ]
  node [
    id 14
    label "cywilizacja"
  ]
  node [
    id 15
    label "przedmiot"
  ]
  node [
    id 16
    label "spos&#243;b"
  ]
  node [
    id 17
    label "wiedza"
  ]
  node [
    id 18
    label "sprawno&#347;&#263;"
  ]
  node [
    id 19
    label "engineering"
  ]
  node [
    id 20
    label "fotowoltaika"
  ]
  node [
    id 21
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 22
    label "teletechnika"
  ]
  node [
    id 23
    label "mechanika_precyzyjna"
  ]
  node [
    id 24
    label "technologia"
  ]
  node [
    id 25
    label "animatronika"
  ]
  node [
    id 26
    label "odczulenie"
  ]
  node [
    id 27
    label "odczula&#263;"
  ]
  node [
    id 28
    label "blik"
  ]
  node [
    id 29
    label "odczuli&#263;"
  ]
  node [
    id 30
    label "scena"
  ]
  node [
    id 31
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 32
    label "muza"
  ]
  node [
    id 33
    label "postprodukcja"
  ]
  node [
    id 34
    label "block"
  ]
  node [
    id 35
    label "trawiarnia"
  ]
  node [
    id 36
    label "sklejarka"
  ]
  node [
    id 37
    label "sztuka"
  ]
  node [
    id 38
    label "uj&#281;cie"
  ]
  node [
    id 39
    label "filmoteka"
  ]
  node [
    id 40
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 41
    label "klatka"
  ]
  node [
    id 42
    label "rozbieg&#243;wka"
  ]
  node [
    id 43
    label "napisy"
  ]
  node [
    id 44
    label "ta&#347;ma"
  ]
  node [
    id 45
    label "odczulanie"
  ]
  node [
    id 46
    label "anamorfoza"
  ]
  node [
    id 47
    label "dorobek"
  ]
  node [
    id 48
    label "ty&#322;&#243;wka"
  ]
  node [
    id 49
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 50
    label "b&#322;ona"
  ]
  node [
    id 51
    label "emulsja_fotograficzna"
  ]
  node [
    id 52
    label "photograph"
  ]
  node [
    id 53
    label "czo&#322;&#243;wka"
  ]
  node [
    id 54
    label "rola"
  ]
  node [
    id 55
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 56
    label "sprz&#281;t_audio"
  ]
  node [
    id 57
    label "magnetowid"
  ]
  node [
    id 58
    label "wideoteka"
  ]
  node [
    id 59
    label "wypo&#380;yczalnia_wideo"
  ]
  node [
    id 60
    label "kaseta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
]
