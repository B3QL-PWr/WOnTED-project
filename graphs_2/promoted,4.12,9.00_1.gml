graph [
  node [
    id 0
    label "dlaczego"
    origin "text"
  ]
  node [
    id 1
    label "nic"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ilo&#347;&#263;"
  ]
  node [
    id 5
    label "ciura"
  ]
  node [
    id 6
    label "miernota"
  ]
  node [
    id 7
    label "g&#243;wno"
  ]
  node [
    id 8
    label "love"
  ]
  node [
    id 9
    label "brak"
  ]
  node [
    id 10
    label "nieistnienie"
  ]
  node [
    id 11
    label "odej&#347;cie"
  ]
  node [
    id 12
    label "defect"
  ]
  node [
    id 13
    label "gap"
  ]
  node [
    id 14
    label "odej&#347;&#263;"
  ]
  node [
    id 15
    label "kr&#243;tki"
  ]
  node [
    id 16
    label "wada"
  ]
  node [
    id 17
    label "odchodzi&#263;"
  ]
  node [
    id 18
    label "wyr&#243;b"
  ]
  node [
    id 19
    label "odchodzenie"
  ]
  node [
    id 20
    label "prywatywny"
  ]
  node [
    id 21
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 22
    label "rozmiar"
  ]
  node [
    id 23
    label "part"
  ]
  node [
    id 24
    label "jako&#347;&#263;"
  ]
  node [
    id 25
    label "cz&#322;owiek"
  ]
  node [
    id 26
    label "podrz&#281;dno&#347;&#263;"
  ]
  node [
    id 27
    label "tandetno&#347;&#263;"
  ]
  node [
    id 28
    label "nieporz&#261;dno&#347;&#263;"
  ]
  node [
    id 29
    label "ka&#322;"
  ]
  node [
    id 30
    label "tandeta"
  ]
  node [
    id 31
    label "zero"
  ]
  node [
    id 32
    label "drobiazg"
  ]
  node [
    id 33
    label "chor&#261;&#380;y"
  ]
  node [
    id 34
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 35
    label "gaworzy&#263;"
  ]
  node [
    id 36
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 37
    label "remark"
  ]
  node [
    id 38
    label "rozmawia&#263;"
  ]
  node [
    id 39
    label "wyra&#380;a&#263;"
  ]
  node [
    id 40
    label "umie&#263;"
  ]
  node [
    id 41
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 42
    label "dziama&#263;"
  ]
  node [
    id 43
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 44
    label "formu&#322;owa&#263;"
  ]
  node [
    id 45
    label "dysfonia"
  ]
  node [
    id 46
    label "express"
  ]
  node [
    id 47
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 48
    label "talk"
  ]
  node [
    id 49
    label "u&#380;ywa&#263;"
  ]
  node [
    id 50
    label "prawi&#263;"
  ]
  node [
    id 51
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 52
    label "powiada&#263;"
  ]
  node [
    id 53
    label "tell"
  ]
  node [
    id 54
    label "chew_the_fat"
  ]
  node [
    id 55
    label "say"
  ]
  node [
    id 56
    label "j&#281;zyk"
  ]
  node [
    id 57
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 58
    label "informowa&#263;"
  ]
  node [
    id 59
    label "wydobywa&#263;"
  ]
  node [
    id 60
    label "okre&#347;la&#263;"
  ]
  node [
    id 61
    label "korzysta&#263;"
  ]
  node [
    id 62
    label "distribute"
  ]
  node [
    id 63
    label "give"
  ]
  node [
    id 64
    label "bash"
  ]
  node [
    id 65
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 66
    label "doznawa&#263;"
  ]
  node [
    id 67
    label "decydowa&#263;"
  ]
  node [
    id 68
    label "signify"
  ]
  node [
    id 69
    label "style"
  ]
  node [
    id 70
    label "powodowa&#263;"
  ]
  node [
    id 71
    label "komunikowa&#263;"
  ]
  node [
    id 72
    label "inform"
  ]
  node [
    id 73
    label "znaczy&#263;"
  ]
  node [
    id 74
    label "give_voice"
  ]
  node [
    id 75
    label "oznacza&#263;"
  ]
  node [
    id 76
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 77
    label "represent"
  ]
  node [
    id 78
    label "convey"
  ]
  node [
    id 79
    label "arouse"
  ]
  node [
    id 80
    label "robi&#263;"
  ]
  node [
    id 81
    label "determine"
  ]
  node [
    id 82
    label "work"
  ]
  node [
    id 83
    label "reakcja_chemiczna"
  ]
  node [
    id 84
    label "uwydatnia&#263;"
  ]
  node [
    id 85
    label "eksploatowa&#263;"
  ]
  node [
    id 86
    label "uzyskiwa&#263;"
  ]
  node [
    id 87
    label "wydostawa&#263;"
  ]
  node [
    id 88
    label "wyjmowa&#263;"
  ]
  node [
    id 89
    label "train"
  ]
  node [
    id 90
    label "u&#380;ytkowa&#263;"
  ]
  node [
    id 91
    label "wydawa&#263;"
  ]
  node [
    id 92
    label "dobywa&#263;"
  ]
  node [
    id 93
    label "ocala&#263;"
  ]
  node [
    id 94
    label "excavate"
  ]
  node [
    id 95
    label "g&#243;rnictwo"
  ]
  node [
    id 96
    label "raise"
  ]
  node [
    id 97
    label "wiedzie&#263;"
  ]
  node [
    id 98
    label "can"
  ]
  node [
    id 99
    label "m&#243;c"
  ]
  node [
    id 100
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 101
    label "rozumie&#263;"
  ]
  node [
    id 102
    label "szczeka&#263;"
  ]
  node [
    id 103
    label "funkcjonowa&#263;"
  ]
  node [
    id 104
    label "mawia&#263;"
  ]
  node [
    id 105
    label "opowiada&#263;"
  ]
  node [
    id 106
    label "chatter"
  ]
  node [
    id 107
    label "niemowl&#281;"
  ]
  node [
    id 108
    label "kosmetyk"
  ]
  node [
    id 109
    label "minera&#322;_akcesoryczny"
  ]
  node [
    id 110
    label "stanowisko_archeologiczne"
  ]
  node [
    id 111
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 112
    label "artykulator"
  ]
  node [
    id 113
    label "kod"
  ]
  node [
    id 114
    label "kawa&#322;ek"
  ]
  node [
    id 115
    label "przedmiot"
  ]
  node [
    id 116
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 117
    label "gramatyka"
  ]
  node [
    id 118
    label "stylik"
  ]
  node [
    id 119
    label "przet&#322;umaczenie"
  ]
  node [
    id 120
    label "formalizowanie"
  ]
  node [
    id 121
    label "ssa&#263;"
  ]
  node [
    id 122
    label "ssanie"
  ]
  node [
    id 123
    label "language"
  ]
  node [
    id 124
    label "liza&#263;"
  ]
  node [
    id 125
    label "napisa&#263;"
  ]
  node [
    id 126
    label "konsonantyzm"
  ]
  node [
    id 127
    label "wokalizm"
  ]
  node [
    id 128
    label "pisa&#263;"
  ]
  node [
    id 129
    label "fonetyka"
  ]
  node [
    id 130
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 131
    label "jeniec"
  ]
  node [
    id 132
    label "but"
  ]
  node [
    id 133
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 134
    label "po_koroniarsku"
  ]
  node [
    id 135
    label "kultura_duchowa"
  ]
  node [
    id 136
    label "t&#322;umaczenie"
  ]
  node [
    id 137
    label "m&#243;wienie"
  ]
  node [
    id 138
    label "pype&#263;"
  ]
  node [
    id 139
    label "lizanie"
  ]
  node [
    id 140
    label "pismo"
  ]
  node [
    id 141
    label "formalizowa&#263;"
  ]
  node [
    id 142
    label "organ"
  ]
  node [
    id 143
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 144
    label "rozumienie"
  ]
  node [
    id 145
    label "spos&#243;b"
  ]
  node [
    id 146
    label "makroglosja"
  ]
  node [
    id 147
    label "jama_ustna"
  ]
  node [
    id 148
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 149
    label "formacja_geologiczna"
  ]
  node [
    id 150
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 151
    label "natural_language"
  ]
  node [
    id 152
    label "s&#322;ownictwo"
  ]
  node [
    id 153
    label "urz&#261;dzenie"
  ]
  node [
    id 154
    label "dysphonia"
  ]
  node [
    id 155
    label "dysleksja"
  ]
  node [
    id 156
    label "instytut"
  ]
  node [
    id 157
    label "polski"
  ]
  node [
    id 158
    label "festiwal"
  ]
  node [
    id 159
    label "kultura"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 157
    target 159
  ]
  edge [
    source 158
    target 159
  ]
]
