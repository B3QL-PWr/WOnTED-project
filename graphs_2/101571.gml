graph [
  node [
    id 0
    label "pomieniony"
    origin "text"
  ]
  node [
    id 1
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 2
    label "g&#243;ra"
    origin "text"
  ]
  node [
    id 3
    label "swoje"
    origin "text"
  ]
  node [
    id 4
    label "godzina"
    origin "text"
  ]
  node [
    id 5
    label "wolne"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "dopomaga&#263;"
    origin "text"
  ]
  node [
    id 8
    label "rzemie&#347;lnik"
    origin "text"
  ]
  node [
    id 9
    label "nasi"
    origin "text"
  ]
  node [
    id 10
    label "d&#378;wiga&#263;"
    origin "text"
  ]
  node [
    id 11
    label "niekt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "wielki"
    origin "text"
  ]
  node [
    id 13
    label "kamie&#324;"
    origin "text"
  ]
  node [
    id 14
    label "dla"
    origin "text"
  ]
  node [
    id 15
    label "doko&#324;czenie"
    origin "text"
  ]
  node [
    id 16
    label "mur"
    origin "text"
  ]
  node [
    id 17
    label "zamek"
    origin "text"
  ]
  node [
    id 18
    label "inny"
    origin "text"
  ]
  node [
    id 19
    label "budowla"
    origin "text"
  ]
  node [
    id 20
    label "cesarski"
    origin "text"
  ]
  node [
    id 21
    label "ludzko&#347;&#263;"
  ]
  node [
    id 22
    label "asymilowanie"
  ]
  node [
    id 23
    label "wapniak"
  ]
  node [
    id 24
    label "asymilowa&#263;"
  ]
  node [
    id 25
    label "os&#322;abia&#263;"
  ]
  node [
    id 26
    label "posta&#263;"
  ]
  node [
    id 27
    label "hominid"
  ]
  node [
    id 28
    label "podw&#322;adny"
  ]
  node [
    id 29
    label "os&#322;abianie"
  ]
  node [
    id 30
    label "g&#322;owa"
  ]
  node [
    id 31
    label "figura"
  ]
  node [
    id 32
    label "portrecista"
  ]
  node [
    id 33
    label "dwun&#243;g"
  ]
  node [
    id 34
    label "profanum"
  ]
  node [
    id 35
    label "mikrokosmos"
  ]
  node [
    id 36
    label "nasada"
  ]
  node [
    id 37
    label "duch"
  ]
  node [
    id 38
    label "antropochoria"
  ]
  node [
    id 39
    label "osoba"
  ]
  node [
    id 40
    label "wz&#243;r"
  ]
  node [
    id 41
    label "senior"
  ]
  node [
    id 42
    label "oddzia&#322;ywanie"
  ]
  node [
    id 43
    label "Adam"
  ]
  node [
    id 44
    label "homo_sapiens"
  ]
  node [
    id 45
    label "polifag"
  ]
  node [
    id 46
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 47
    label "cz&#322;owiekowate"
  ]
  node [
    id 48
    label "konsument"
  ]
  node [
    id 49
    label "istota_&#380;ywa"
  ]
  node [
    id 50
    label "pracownik"
  ]
  node [
    id 51
    label "Chocho&#322;"
  ]
  node [
    id 52
    label "Herkules_Poirot"
  ]
  node [
    id 53
    label "Edyp"
  ]
  node [
    id 54
    label "parali&#380;owa&#263;"
  ]
  node [
    id 55
    label "Harry_Potter"
  ]
  node [
    id 56
    label "Casanova"
  ]
  node [
    id 57
    label "Zgredek"
  ]
  node [
    id 58
    label "Gargantua"
  ]
  node [
    id 59
    label "Winnetou"
  ]
  node [
    id 60
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 61
    label "Dulcynea"
  ]
  node [
    id 62
    label "kategoria_gramatyczna"
  ]
  node [
    id 63
    label "person"
  ]
  node [
    id 64
    label "Plastu&#347;"
  ]
  node [
    id 65
    label "Quasimodo"
  ]
  node [
    id 66
    label "Sherlock_Holmes"
  ]
  node [
    id 67
    label "Faust"
  ]
  node [
    id 68
    label "Wallenrod"
  ]
  node [
    id 69
    label "Dwukwiat"
  ]
  node [
    id 70
    label "Don_Juan"
  ]
  node [
    id 71
    label "koniugacja"
  ]
  node [
    id 72
    label "Don_Kiszot"
  ]
  node [
    id 73
    label "Hamlet"
  ]
  node [
    id 74
    label "Werter"
  ]
  node [
    id 75
    label "istota"
  ]
  node [
    id 76
    label "Szwejk"
  ]
  node [
    id 77
    label "doros&#322;y"
  ]
  node [
    id 78
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 79
    label "jajko"
  ]
  node [
    id 80
    label "rodzic"
  ]
  node [
    id 81
    label "wapniaki"
  ]
  node [
    id 82
    label "zwierzchnik"
  ]
  node [
    id 83
    label "feuda&#322;"
  ]
  node [
    id 84
    label "starzec"
  ]
  node [
    id 85
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 86
    label "zawodnik"
  ]
  node [
    id 87
    label "komendancja"
  ]
  node [
    id 88
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 89
    label "de-escalation"
  ]
  node [
    id 90
    label "powodowanie"
  ]
  node [
    id 91
    label "os&#322;abienie"
  ]
  node [
    id 92
    label "kondycja_fizyczna"
  ]
  node [
    id 93
    label "os&#322;abi&#263;"
  ]
  node [
    id 94
    label "debilitation"
  ]
  node [
    id 95
    label "zdrowie"
  ]
  node [
    id 96
    label "zmniejszanie"
  ]
  node [
    id 97
    label "s&#322;abszy"
  ]
  node [
    id 98
    label "pogarszanie"
  ]
  node [
    id 99
    label "suppress"
  ]
  node [
    id 100
    label "robi&#263;"
  ]
  node [
    id 101
    label "powodowa&#263;"
  ]
  node [
    id 102
    label "zmniejsza&#263;"
  ]
  node [
    id 103
    label "bate"
  ]
  node [
    id 104
    label "asymilowanie_si&#281;"
  ]
  node [
    id 105
    label "absorption"
  ]
  node [
    id 106
    label "pobieranie"
  ]
  node [
    id 107
    label "czerpanie"
  ]
  node [
    id 108
    label "acquisition"
  ]
  node [
    id 109
    label "zmienianie"
  ]
  node [
    id 110
    label "organizm"
  ]
  node [
    id 111
    label "assimilation"
  ]
  node [
    id 112
    label "upodabnianie"
  ]
  node [
    id 113
    label "g&#322;oska"
  ]
  node [
    id 114
    label "kultura"
  ]
  node [
    id 115
    label "podobny"
  ]
  node [
    id 116
    label "grupa"
  ]
  node [
    id 117
    label "fonetyka"
  ]
  node [
    id 118
    label "assimilate"
  ]
  node [
    id 119
    label "dostosowywa&#263;"
  ]
  node [
    id 120
    label "dostosowa&#263;"
  ]
  node [
    id 121
    label "przejmowa&#263;"
  ]
  node [
    id 122
    label "upodobni&#263;"
  ]
  node [
    id 123
    label "przej&#261;&#263;"
  ]
  node [
    id 124
    label "upodabnia&#263;"
  ]
  node [
    id 125
    label "pobiera&#263;"
  ]
  node [
    id 126
    label "pobra&#263;"
  ]
  node [
    id 127
    label "charakterystyka"
  ]
  node [
    id 128
    label "zaistnie&#263;"
  ]
  node [
    id 129
    label "Osjan"
  ]
  node [
    id 130
    label "cecha"
  ]
  node [
    id 131
    label "kto&#347;"
  ]
  node [
    id 132
    label "wygl&#261;d"
  ]
  node [
    id 133
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 134
    label "osobowo&#347;&#263;"
  ]
  node [
    id 135
    label "wytw&#243;r"
  ]
  node [
    id 136
    label "trim"
  ]
  node [
    id 137
    label "poby&#263;"
  ]
  node [
    id 138
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 139
    label "Aspazja"
  ]
  node [
    id 140
    label "punkt_widzenia"
  ]
  node [
    id 141
    label "kompleksja"
  ]
  node [
    id 142
    label "wytrzyma&#263;"
  ]
  node [
    id 143
    label "budowa"
  ]
  node [
    id 144
    label "formacja"
  ]
  node [
    id 145
    label "pozosta&#263;"
  ]
  node [
    id 146
    label "point"
  ]
  node [
    id 147
    label "przedstawienie"
  ]
  node [
    id 148
    label "go&#347;&#263;"
  ]
  node [
    id 149
    label "zapis"
  ]
  node [
    id 150
    label "figure"
  ]
  node [
    id 151
    label "typ"
  ]
  node [
    id 152
    label "spos&#243;b"
  ]
  node [
    id 153
    label "mildew"
  ]
  node [
    id 154
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 155
    label "ideal"
  ]
  node [
    id 156
    label "rule"
  ]
  node [
    id 157
    label "ruch"
  ]
  node [
    id 158
    label "dekal"
  ]
  node [
    id 159
    label "motyw"
  ]
  node [
    id 160
    label "projekt"
  ]
  node [
    id 161
    label "pryncypa&#322;"
  ]
  node [
    id 162
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 163
    label "kszta&#322;t"
  ]
  node [
    id 164
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 165
    label "wiedza"
  ]
  node [
    id 166
    label "kierowa&#263;"
  ]
  node [
    id 167
    label "alkohol"
  ]
  node [
    id 168
    label "zdolno&#347;&#263;"
  ]
  node [
    id 169
    label "&#380;ycie"
  ]
  node [
    id 170
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 171
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 172
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 173
    label "sztuka"
  ]
  node [
    id 174
    label "dekiel"
  ]
  node [
    id 175
    label "ro&#347;lina"
  ]
  node [
    id 176
    label "&#347;ci&#281;cie"
  ]
  node [
    id 177
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 178
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 179
    label "&#347;ci&#281;gno"
  ]
  node [
    id 180
    label "noosfera"
  ]
  node [
    id 181
    label "byd&#322;o"
  ]
  node [
    id 182
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 183
    label "makrocefalia"
  ]
  node [
    id 184
    label "obiekt"
  ]
  node [
    id 185
    label "ucho"
  ]
  node [
    id 186
    label "m&#243;zg"
  ]
  node [
    id 187
    label "kierownictwo"
  ]
  node [
    id 188
    label "fryzura"
  ]
  node [
    id 189
    label "umys&#322;"
  ]
  node [
    id 190
    label "cia&#322;o"
  ]
  node [
    id 191
    label "cz&#322;onek"
  ]
  node [
    id 192
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 193
    label "czaszka"
  ]
  node [
    id 194
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 195
    label "dziedzina"
  ]
  node [
    id 196
    label "hipnotyzowanie"
  ]
  node [
    id 197
    label "&#347;lad"
  ]
  node [
    id 198
    label "docieranie"
  ]
  node [
    id 199
    label "natural_process"
  ]
  node [
    id 200
    label "reakcja_chemiczna"
  ]
  node [
    id 201
    label "wdzieranie_si&#281;"
  ]
  node [
    id 202
    label "zjawisko"
  ]
  node [
    id 203
    label "act"
  ]
  node [
    id 204
    label "rezultat"
  ]
  node [
    id 205
    label "lobbysta"
  ]
  node [
    id 206
    label "allochoria"
  ]
  node [
    id 207
    label "fotograf"
  ]
  node [
    id 208
    label "malarz"
  ]
  node [
    id 209
    label "artysta"
  ]
  node [
    id 210
    label "p&#322;aszczyzna"
  ]
  node [
    id 211
    label "przedmiot"
  ]
  node [
    id 212
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 213
    label "bierka_szachowa"
  ]
  node [
    id 214
    label "obiekt_matematyczny"
  ]
  node [
    id 215
    label "gestaltyzm"
  ]
  node [
    id 216
    label "styl"
  ]
  node [
    id 217
    label "obraz"
  ]
  node [
    id 218
    label "rzecz"
  ]
  node [
    id 219
    label "d&#378;wi&#281;k"
  ]
  node [
    id 220
    label "character"
  ]
  node [
    id 221
    label "rze&#378;ba"
  ]
  node [
    id 222
    label "stylistyka"
  ]
  node [
    id 223
    label "miejsce"
  ]
  node [
    id 224
    label "antycypacja"
  ]
  node [
    id 225
    label "ornamentyka"
  ]
  node [
    id 226
    label "informacja"
  ]
  node [
    id 227
    label "facet"
  ]
  node [
    id 228
    label "popis"
  ]
  node [
    id 229
    label "wiersz"
  ]
  node [
    id 230
    label "symetria"
  ]
  node [
    id 231
    label "lingwistyka_kognitywna"
  ]
  node [
    id 232
    label "karta"
  ]
  node [
    id 233
    label "shape"
  ]
  node [
    id 234
    label "podzbi&#243;r"
  ]
  node [
    id 235
    label "perspektywa"
  ]
  node [
    id 236
    label "nak&#322;adka"
  ]
  node [
    id 237
    label "li&#347;&#263;"
  ]
  node [
    id 238
    label "jama_gard&#322;owa"
  ]
  node [
    id 239
    label "rezonator"
  ]
  node [
    id 240
    label "podstawa"
  ]
  node [
    id 241
    label "base"
  ]
  node [
    id 242
    label "piek&#322;o"
  ]
  node [
    id 243
    label "human_body"
  ]
  node [
    id 244
    label "ofiarowywanie"
  ]
  node [
    id 245
    label "sfera_afektywna"
  ]
  node [
    id 246
    label "nekromancja"
  ]
  node [
    id 247
    label "Po&#347;wist"
  ]
  node [
    id 248
    label "podekscytowanie"
  ]
  node [
    id 249
    label "deformowanie"
  ]
  node [
    id 250
    label "sumienie"
  ]
  node [
    id 251
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 252
    label "deformowa&#263;"
  ]
  node [
    id 253
    label "psychika"
  ]
  node [
    id 254
    label "zjawa"
  ]
  node [
    id 255
    label "zmar&#322;y"
  ]
  node [
    id 256
    label "istota_nadprzyrodzona"
  ]
  node [
    id 257
    label "power"
  ]
  node [
    id 258
    label "entity"
  ]
  node [
    id 259
    label "ofiarowywa&#263;"
  ]
  node [
    id 260
    label "oddech"
  ]
  node [
    id 261
    label "seksualno&#347;&#263;"
  ]
  node [
    id 262
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 263
    label "byt"
  ]
  node [
    id 264
    label "si&#322;a"
  ]
  node [
    id 265
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 266
    label "ego"
  ]
  node [
    id 267
    label "ofiarowanie"
  ]
  node [
    id 268
    label "charakter"
  ]
  node [
    id 269
    label "fizjonomia"
  ]
  node [
    id 270
    label "kompleks"
  ]
  node [
    id 271
    label "zapalno&#347;&#263;"
  ]
  node [
    id 272
    label "T&#281;sknica"
  ]
  node [
    id 273
    label "ofiarowa&#263;"
  ]
  node [
    id 274
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 275
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 276
    label "passion"
  ]
  node [
    id 277
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 278
    label "atom"
  ]
  node [
    id 279
    label "odbicie"
  ]
  node [
    id 280
    label "przyroda"
  ]
  node [
    id 281
    label "Ziemia"
  ]
  node [
    id 282
    label "kosmos"
  ]
  node [
    id 283
    label "miniatura"
  ]
  node [
    id 284
    label "przelezienie"
  ]
  node [
    id 285
    label "&#347;piew"
  ]
  node [
    id 286
    label "Synaj"
  ]
  node [
    id 287
    label "Kreml"
  ]
  node [
    id 288
    label "kierunek"
  ]
  node [
    id 289
    label "wysoki"
  ]
  node [
    id 290
    label "element"
  ]
  node [
    id 291
    label "wzniesienie"
  ]
  node [
    id 292
    label "pi&#281;tro"
  ]
  node [
    id 293
    label "Ropa"
  ]
  node [
    id 294
    label "kupa"
  ]
  node [
    id 295
    label "przele&#378;&#263;"
  ]
  node [
    id 296
    label "Ma&#322;a_Rawka"
  ]
  node [
    id 297
    label "karczek"
  ]
  node [
    id 298
    label "rami&#261;czko"
  ]
  node [
    id 299
    label "Jaworze"
  ]
  node [
    id 300
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 301
    label "odm&#322;adzanie"
  ]
  node [
    id 302
    label "liga"
  ]
  node [
    id 303
    label "jednostka_systematyczna"
  ]
  node [
    id 304
    label "gromada"
  ]
  node [
    id 305
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 306
    label "egzemplarz"
  ]
  node [
    id 307
    label "Entuzjastki"
  ]
  node [
    id 308
    label "zbi&#243;r"
  ]
  node [
    id 309
    label "kompozycja"
  ]
  node [
    id 310
    label "Terranie"
  ]
  node [
    id 311
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 312
    label "category"
  ]
  node [
    id 313
    label "pakiet_klimatyczny"
  ]
  node [
    id 314
    label "oddzia&#322;"
  ]
  node [
    id 315
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 316
    label "cz&#261;steczka"
  ]
  node [
    id 317
    label "stage_set"
  ]
  node [
    id 318
    label "type"
  ]
  node [
    id 319
    label "specgrupa"
  ]
  node [
    id 320
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 321
    label "&#346;wietliki"
  ]
  node [
    id 322
    label "odm&#322;odzenie"
  ]
  node [
    id 323
    label "Eurogrupa"
  ]
  node [
    id 324
    label "odm&#322;adza&#263;"
  ]
  node [
    id 325
    label "formacja_geologiczna"
  ]
  node [
    id 326
    label "harcerze_starsi"
  ]
  node [
    id 327
    label "Rzym_Zachodni"
  ]
  node [
    id 328
    label "whole"
  ]
  node [
    id 329
    label "ilo&#347;&#263;"
  ]
  node [
    id 330
    label "Rzym_Wschodni"
  ]
  node [
    id 331
    label "urz&#261;dzenie"
  ]
  node [
    id 332
    label "nabudowanie"
  ]
  node [
    id 333
    label "Skalnik"
  ]
  node [
    id 334
    label "raise"
  ]
  node [
    id 335
    label "wierzchowina"
  ]
  node [
    id 336
    label "wypi&#281;trzy&#263;_si&#281;"
  ]
  node [
    id 337
    label "Sikornik"
  ]
  node [
    id 338
    label "Bukowiec"
  ]
  node [
    id 339
    label "Izera"
  ]
  node [
    id 340
    label "wyro&#347;ni&#281;cie"
  ]
  node [
    id 341
    label "rise"
  ]
  node [
    id 342
    label "wypuk&#322;o&#347;&#263;"
  ]
  node [
    id 343
    label "wypi&#281;trza&#263;_si&#281;"
  ]
  node [
    id 344
    label "podniesienie"
  ]
  node [
    id 345
    label "Zwalisko"
  ]
  node [
    id 346
    label "Bielec"
  ]
  node [
    id 347
    label "construction"
  ]
  node [
    id 348
    label "zrobienie"
  ]
  node [
    id 349
    label "phone"
  ]
  node [
    id 350
    label "wpadni&#281;cie"
  ]
  node [
    id 351
    label "wydawa&#263;"
  ]
  node [
    id 352
    label "wyda&#263;"
  ]
  node [
    id 353
    label "intonacja"
  ]
  node [
    id 354
    label "wpa&#347;&#263;"
  ]
  node [
    id 355
    label "note"
  ]
  node [
    id 356
    label "onomatopeja"
  ]
  node [
    id 357
    label "modalizm"
  ]
  node [
    id 358
    label "nadlecenie"
  ]
  node [
    id 359
    label "sound"
  ]
  node [
    id 360
    label "chromatyczno&#347;&#263;"
  ]
  node [
    id 361
    label "wpada&#263;"
  ]
  node [
    id 362
    label "solmizacja"
  ]
  node [
    id 363
    label "seria"
  ]
  node [
    id 364
    label "dobiec"
  ]
  node [
    id 365
    label "transmiter"
  ]
  node [
    id 366
    label "heksachord"
  ]
  node [
    id 367
    label "akcent"
  ]
  node [
    id 368
    label "wydanie"
  ]
  node [
    id 369
    label "repetycja"
  ]
  node [
    id 370
    label "brzmienie"
  ]
  node [
    id 371
    label "wpadanie"
  ]
  node [
    id 372
    label "zboczenie"
  ]
  node [
    id 373
    label "om&#243;wienie"
  ]
  node [
    id 374
    label "sponiewieranie"
  ]
  node [
    id 375
    label "discipline"
  ]
  node [
    id 376
    label "omawia&#263;"
  ]
  node [
    id 377
    label "kr&#261;&#380;enie"
  ]
  node [
    id 378
    label "tre&#347;&#263;"
  ]
  node [
    id 379
    label "robienie"
  ]
  node [
    id 380
    label "sponiewiera&#263;"
  ]
  node [
    id 381
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 382
    label "tematyka"
  ]
  node [
    id 383
    label "w&#261;tek"
  ]
  node [
    id 384
    label "zbaczanie"
  ]
  node [
    id 385
    label "program_nauczania"
  ]
  node [
    id 386
    label "om&#243;wi&#263;"
  ]
  node [
    id 387
    label "omawianie"
  ]
  node [
    id 388
    label "thing"
  ]
  node [
    id 389
    label "zbacza&#263;"
  ]
  node [
    id 390
    label "zboczy&#263;"
  ]
  node [
    id 391
    label "r&#243;&#380;niczka"
  ]
  node [
    id 392
    label "&#347;rodowisko"
  ]
  node [
    id 393
    label "materia"
  ]
  node [
    id 394
    label "szambo"
  ]
  node [
    id 395
    label "aspo&#322;eczny"
  ]
  node [
    id 396
    label "component"
  ]
  node [
    id 397
    label "szkodnik"
  ]
  node [
    id 398
    label "gangsterski"
  ]
  node [
    id 399
    label "poj&#281;cie"
  ]
  node [
    id 400
    label "underworld"
  ]
  node [
    id 401
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 402
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 403
    label "chronozona"
  ]
  node [
    id 404
    label "kondygnacja"
  ]
  node [
    id 405
    label "budynek"
  ]
  node [
    id 406
    label "eta&#380;"
  ]
  node [
    id 407
    label "floor"
  ]
  node [
    id 408
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 409
    label "jednostka_geologiczna"
  ]
  node [
    id 410
    label "tragedia"
  ]
  node [
    id 411
    label "wydalina"
  ]
  node [
    id 412
    label "stool"
  ]
  node [
    id 413
    label "koprofilia"
  ]
  node [
    id 414
    label "odchody"
  ]
  node [
    id 415
    label "mn&#243;stwo"
  ]
  node [
    id 416
    label "knoll"
  ]
  node [
    id 417
    label "balas"
  ]
  node [
    id 418
    label "g&#243;wno"
  ]
  node [
    id 419
    label "fekalia"
  ]
  node [
    id 420
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 421
    label "Moj&#380;esz"
  ]
  node [
    id 422
    label "Egipt"
  ]
  node [
    id 423
    label "Beskid_Niski"
  ]
  node [
    id 424
    label "Tatry"
  ]
  node [
    id 425
    label "Ma&#322;opolska"
  ]
  node [
    id 426
    label "przebieg"
  ]
  node [
    id 427
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 428
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 429
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 430
    label "praktyka"
  ]
  node [
    id 431
    label "system"
  ]
  node [
    id 432
    label "przeorientowywanie"
  ]
  node [
    id 433
    label "studia"
  ]
  node [
    id 434
    label "linia"
  ]
  node [
    id 435
    label "bok"
  ]
  node [
    id 436
    label "skr&#281;canie"
  ]
  node [
    id 437
    label "skr&#281;ca&#263;"
  ]
  node [
    id 438
    label "przeorientowywa&#263;"
  ]
  node [
    id 439
    label "orientowanie"
  ]
  node [
    id 440
    label "skr&#281;ci&#263;"
  ]
  node [
    id 441
    label "przeorientowanie"
  ]
  node [
    id 442
    label "zorientowanie"
  ]
  node [
    id 443
    label "przeorientowa&#263;"
  ]
  node [
    id 444
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 445
    label "metoda"
  ]
  node [
    id 446
    label "ty&#322;"
  ]
  node [
    id 447
    label "zorientowa&#263;"
  ]
  node [
    id 448
    label "orientowa&#263;"
  ]
  node [
    id 449
    label "ideologia"
  ]
  node [
    id 450
    label "orientacja"
  ]
  node [
    id 451
    label "prz&#243;d"
  ]
  node [
    id 452
    label "bearing"
  ]
  node [
    id 453
    label "skr&#281;cenie"
  ]
  node [
    id 454
    label "ascent"
  ]
  node [
    id 455
    label "przeby&#263;"
  ]
  node [
    id 456
    label "beat"
  ]
  node [
    id 457
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 458
    label "min&#261;&#263;"
  ]
  node [
    id 459
    label "przekroczy&#263;"
  ]
  node [
    id 460
    label "pique"
  ]
  node [
    id 461
    label "mini&#281;cie"
  ]
  node [
    id 462
    label "przepuszczenie"
  ]
  node [
    id 463
    label "przebycie"
  ]
  node [
    id 464
    label "traversal"
  ]
  node [
    id 465
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 466
    label "prze&#322;a&#380;enie"
  ]
  node [
    id 467
    label "offense"
  ]
  node [
    id 468
    label "przekroczenie"
  ]
  node [
    id 469
    label "breeze"
  ]
  node [
    id 470
    label "wokal"
  ]
  node [
    id 471
    label "muzyka"
  ]
  node [
    id 472
    label "d&#243;&#322;"
  ]
  node [
    id 473
    label "impostacja"
  ]
  node [
    id 474
    label "g&#322;os"
  ]
  node [
    id 475
    label "odg&#322;os"
  ]
  node [
    id 476
    label "pienie"
  ]
  node [
    id 477
    label "czynno&#347;&#263;"
  ]
  node [
    id 478
    label "wyrafinowany"
  ]
  node [
    id 479
    label "niepo&#347;ledni"
  ]
  node [
    id 480
    label "du&#380;y"
  ]
  node [
    id 481
    label "chwalebny"
  ]
  node [
    id 482
    label "z_wysoka"
  ]
  node [
    id 483
    label "wznios&#322;y"
  ]
  node [
    id 484
    label "daleki"
  ]
  node [
    id 485
    label "wysoce"
  ]
  node [
    id 486
    label "szczytnie"
  ]
  node [
    id 487
    label "znaczny"
  ]
  node [
    id 488
    label "warto&#347;ciowy"
  ]
  node [
    id 489
    label "wysoko"
  ]
  node [
    id 490
    label "uprzywilejowany"
  ]
  node [
    id 491
    label "tusza"
  ]
  node [
    id 492
    label "mi&#281;so"
  ]
  node [
    id 493
    label "strap"
  ]
  node [
    id 494
    label "pasek"
  ]
  node [
    id 495
    label "time"
  ]
  node [
    id 496
    label "doba"
  ]
  node [
    id 497
    label "p&#243;&#322;godzina"
  ]
  node [
    id 498
    label "jednostka_czasu"
  ]
  node [
    id 499
    label "czas"
  ]
  node [
    id 500
    label "minuta"
  ]
  node [
    id 501
    label "kwadrans"
  ]
  node [
    id 502
    label "poprzedzanie"
  ]
  node [
    id 503
    label "czasoprzestrze&#324;"
  ]
  node [
    id 504
    label "laba"
  ]
  node [
    id 505
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 506
    label "chronometria"
  ]
  node [
    id 507
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 508
    label "rachuba_czasu"
  ]
  node [
    id 509
    label "przep&#322;ywanie"
  ]
  node [
    id 510
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 511
    label "czasokres"
  ]
  node [
    id 512
    label "odczyt"
  ]
  node [
    id 513
    label "chwila"
  ]
  node [
    id 514
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 515
    label "dzieje"
  ]
  node [
    id 516
    label "poprzedzenie"
  ]
  node [
    id 517
    label "trawienie"
  ]
  node [
    id 518
    label "pochodzi&#263;"
  ]
  node [
    id 519
    label "period"
  ]
  node [
    id 520
    label "okres_czasu"
  ]
  node [
    id 521
    label "poprzedza&#263;"
  ]
  node [
    id 522
    label "schy&#322;ek"
  ]
  node [
    id 523
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 524
    label "odwlekanie_si&#281;"
  ]
  node [
    id 525
    label "zegar"
  ]
  node [
    id 526
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 527
    label "czwarty_wymiar"
  ]
  node [
    id 528
    label "pochodzenie"
  ]
  node [
    id 529
    label "Zeitgeist"
  ]
  node [
    id 530
    label "trawi&#263;"
  ]
  node [
    id 531
    label "pogoda"
  ]
  node [
    id 532
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 533
    label "poprzedzi&#263;"
  ]
  node [
    id 534
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 535
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 536
    label "time_period"
  ]
  node [
    id 537
    label "sekunda"
  ]
  node [
    id 538
    label "jednostka"
  ]
  node [
    id 539
    label "stopie&#324;"
  ]
  node [
    id 540
    label "design"
  ]
  node [
    id 541
    label "tydzie&#324;"
  ]
  node [
    id 542
    label "noc"
  ]
  node [
    id 543
    label "dzie&#324;"
  ]
  node [
    id 544
    label "long_time"
  ]
  node [
    id 545
    label "czas_wolny"
  ]
  node [
    id 546
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 547
    label "mie&#263;_miejsce"
  ]
  node [
    id 548
    label "equal"
  ]
  node [
    id 549
    label "trwa&#263;"
  ]
  node [
    id 550
    label "chodzi&#263;"
  ]
  node [
    id 551
    label "si&#281;ga&#263;"
  ]
  node [
    id 552
    label "stan"
  ]
  node [
    id 553
    label "obecno&#347;&#263;"
  ]
  node [
    id 554
    label "stand"
  ]
  node [
    id 555
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 556
    label "uczestniczy&#263;"
  ]
  node [
    id 557
    label "participate"
  ]
  node [
    id 558
    label "istnie&#263;"
  ]
  node [
    id 559
    label "pozostawa&#263;"
  ]
  node [
    id 560
    label "zostawa&#263;"
  ]
  node [
    id 561
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 562
    label "adhere"
  ]
  node [
    id 563
    label "compass"
  ]
  node [
    id 564
    label "korzysta&#263;"
  ]
  node [
    id 565
    label "appreciation"
  ]
  node [
    id 566
    label "osi&#261;ga&#263;"
  ]
  node [
    id 567
    label "dociera&#263;"
  ]
  node [
    id 568
    label "get"
  ]
  node [
    id 569
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 570
    label "mierzy&#263;"
  ]
  node [
    id 571
    label "u&#380;ywa&#263;"
  ]
  node [
    id 572
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 573
    label "exsert"
  ]
  node [
    id 574
    label "being"
  ]
  node [
    id 575
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 576
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 577
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 578
    label "p&#322;ywa&#263;"
  ]
  node [
    id 579
    label "run"
  ]
  node [
    id 580
    label "bangla&#263;"
  ]
  node [
    id 581
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 582
    label "przebiega&#263;"
  ]
  node [
    id 583
    label "wk&#322;ada&#263;"
  ]
  node [
    id 584
    label "proceed"
  ]
  node [
    id 585
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 586
    label "carry"
  ]
  node [
    id 587
    label "bywa&#263;"
  ]
  node [
    id 588
    label "dziama&#263;"
  ]
  node [
    id 589
    label "stara&#263;_si&#281;"
  ]
  node [
    id 590
    label "para"
  ]
  node [
    id 591
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 592
    label "str&#243;j"
  ]
  node [
    id 593
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 594
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 595
    label "krok"
  ]
  node [
    id 596
    label "tryb"
  ]
  node [
    id 597
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 598
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 599
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 600
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 601
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 602
    label "continue"
  ]
  node [
    id 603
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 604
    label "Ohio"
  ]
  node [
    id 605
    label "wci&#281;cie"
  ]
  node [
    id 606
    label "Nowy_York"
  ]
  node [
    id 607
    label "warstwa"
  ]
  node [
    id 608
    label "samopoczucie"
  ]
  node [
    id 609
    label "Illinois"
  ]
  node [
    id 610
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 611
    label "state"
  ]
  node [
    id 612
    label "Jukatan"
  ]
  node [
    id 613
    label "Kalifornia"
  ]
  node [
    id 614
    label "Wirginia"
  ]
  node [
    id 615
    label "wektor"
  ]
  node [
    id 616
    label "Goa"
  ]
  node [
    id 617
    label "Teksas"
  ]
  node [
    id 618
    label "Waszyngton"
  ]
  node [
    id 619
    label "Massachusetts"
  ]
  node [
    id 620
    label "Alaska"
  ]
  node [
    id 621
    label "Arakan"
  ]
  node [
    id 622
    label "Hawaje"
  ]
  node [
    id 623
    label "Maryland"
  ]
  node [
    id 624
    label "punkt"
  ]
  node [
    id 625
    label "Michigan"
  ]
  node [
    id 626
    label "Arizona"
  ]
  node [
    id 627
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 628
    label "Georgia"
  ]
  node [
    id 629
    label "poziom"
  ]
  node [
    id 630
    label "Pensylwania"
  ]
  node [
    id 631
    label "Luizjana"
  ]
  node [
    id 632
    label "Nowy_Meksyk"
  ]
  node [
    id 633
    label "Alabama"
  ]
  node [
    id 634
    label "Kansas"
  ]
  node [
    id 635
    label "Oregon"
  ]
  node [
    id 636
    label "Oklahoma"
  ]
  node [
    id 637
    label "Floryda"
  ]
  node [
    id 638
    label "jednostka_administracyjna"
  ]
  node [
    id 639
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 640
    label "u&#322;atwia&#263;"
  ]
  node [
    id 641
    label "back"
  ]
  node [
    id 642
    label "Warszawa"
  ]
  node [
    id 643
    label "sprzyja&#263;"
  ]
  node [
    id 644
    label "&#322;atwi&#263;"
  ]
  node [
    id 645
    label "ease"
  ]
  node [
    id 646
    label "czu&#263;"
  ]
  node [
    id 647
    label "stanowi&#263;"
  ]
  node [
    id 648
    label "chowa&#263;"
  ]
  node [
    id 649
    label "cover"
  ]
  node [
    id 650
    label "warszawa"
  ]
  node [
    id 651
    label "Powi&#347;le"
  ]
  node [
    id 652
    label "Wawa"
  ]
  node [
    id 653
    label "syreni_gr&#243;d"
  ]
  node [
    id 654
    label "Wawer"
  ]
  node [
    id 655
    label "W&#322;ochy"
  ]
  node [
    id 656
    label "Ursyn&#243;w"
  ]
  node [
    id 657
    label "Bielany"
  ]
  node [
    id 658
    label "Weso&#322;a"
  ]
  node [
    id 659
    label "Bia&#322;o&#322;&#281;ka"
  ]
  node [
    id 660
    label "Targ&#243;wek"
  ]
  node [
    id 661
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 662
    label "Muran&#243;w"
  ]
  node [
    id 663
    label "Warsiawa"
  ]
  node [
    id 664
    label "Ursus"
  ]
  node [
    id 665
    label "Ochota"
  ]
  node [
    id 666
    label "Marymont"
  ]
  node [
    id 667
    label "Ujazd&#243;w"
  ]
  node [
    id 668
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 669
    label "Solec"
  ]
  node [
    id 670
    label "Bemowo"
  ]
  node [
    id 671
    label "Mokot&#243;w"
  ]
  node [
    id 672
    label "Wilan&#243;w"
  ]
  node [
    id 673
    label "warszawka"
  ]
  node [
    id 674
    label "varsaviana"
  ]
  node [
    id 675
    label "Wola"
  ]
  node [
    id 676
    label "Rembert&#243;w"
  ]
  node [
    id 677
    label "Praga"
  ]
  node [
    id 678
    label "&#379;oliborz"
  ]
  node [
    id 679
    label "remiecha"
  ]
  node [
    id 680
    label "podnosi&#263;"
  ]
  node [
    id 681
    label "zadawa&#263;"
  ]
  node [
    id 682
    label "odbudowywa&#263;"
  ]
  node [
    id 683
    label "znosi&#263;"
  ]
  node [
    id 684
    label "ulepsza&#263;"
  ]
  node [
    id 685
    label "enhance"
  ]
  node [
    id 686
    label "pryczy&#263;"
  ]
  node [
    id 687
    label "nie&#347;&#263;"
  ]
  node [
    id 688
    label "tire"
  ]
  node [
    id 689
    label "zmienia&#263;"
  ]
  node [
    id 690
    label "wstrzeliwa&#263;_si&#281;"
  ]
  node [
    id 691
    label "better"
  ]
  node [
    id 692
    label "sum_up"
  ]
  node [
    id 693
    label "odtwarza&#263;"
  ]
  node [
    id 694
    label "doprowadza&#263;"
  ]
  node [
    id 695
    label "powi&#281;ksza&#263;"
  ]
  node [
    id 696
    label "zaczyna&#263;"
  ]
  node [
    id 697
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 698
    label "escalate"
  ]
  node [
    id 699
    label "pia&#263;"
  ]
  node [
    id 700
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 701
    label "przybli&#380;a&#263;"
  ]
  node [
    id 702
    label "pomaga&#263;"
  ]
  node [
    id 703
    label "liczy&#263;"
  ]
  node [
    id 704
    label "express"
  ]
  node [
    id 705
    label "przemieszcza&#263;"
  ]
  node [
    id 706
    label "chwali&#263;"
  ]
  node [
    id 707
    label "rozg&#322;asza&#263;"
  ]
  node [
    id 708
    label "os&#322;awia&#263;"
  ]
  node [
    id 709
    label "drive"
  ]
  node [
    id 710
    label "za&#322;apywa&#263;"
  ]
  node [
    id 711
    label "lift"
  ]
  node [
    id 712
    label "gromadzi&#263;"
  ]
  node [
    id 713
    label "usuwa&#263;"
  ]
  node [
    id 714
    label "porywa&#263;"
  ]
  node [
    id 715
    label "sk&#322;ada&#263;"
  ]
  node [
    id 716
    label "ranny"
  ]
  node [
    id 717
    label "zbiera&#263;"
  ]
  node [
    id 718
    label "behave"
  ]
  node [
    id 719
    label "represent"
  ]
  node [
    id 720
    label "podrze&#263;"
  ]
  node [
    id 721
    label "przenosi&#263;"
  ]
  node [
    id 722
    label "wytrzymywa&#263;"
  ]
  node [
    id 723
    label "seclude"
  ]
  node [
    id 724
    label "wygrywa&#263;"
  ]
  node [
    id 725
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 726
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 727
    label "set"
  ]
  node [
    id 728
    label "zu&#380;y&#263;"
  ]
  node [
    id 729
    label "niszczy&#263;"
  ]
  node [
    id 730
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 731
    label "tolerowa&#263;"
  ]
  node [
    id 732
    label "i&#347;&#263;"
  ]
  node [
    id 733
    label "strzela&#263;"
  ]
  node [
    id 734
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 735
    label "go"
  ]
  node [
    id 736
    label "drift"
  ]
  node [
    id 737
    label "make"
  ]
  node [
    id 738
    label "tacha&#263;"
  ]
  node [
    id 739
    label "odci&#261;ga&#263;"
  ]
  node [
    id 740
    label "bacteriophage"
  ]
  node [
    id 741
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 742
    label "gotowa&#263;"
  ]
  node [
    id 743
    label "bra&#263;"
  ]
  node [
    id 744
    label "deal"
  ]
  node [
    id 745
    label "zajmowa&#263;"
  ]
  node [
    id 746
    label "karmi&#263;"
  ]
  node [
    id 747
    label "szkodzi&#263;"
  ]
  node [
    id 748
    label "zobowi&#261;zywa&#263;"
  ]
  node [
    id 749
    label "inflict"
  ]
  node [
    id 750
    label "share"
  ]
  node [
    id 751
    label "pose"
  ]
  node [
    id 752
    label "zak&#322;ada&#263;"
  ]
  node [
    id 753
    label "jaki&#347;"
  ]
  node [
    id 754
    label "przyzwoity"
  ]
  node [
    id 755
    label "ciekawy"
  ]
  node [
    id 756
    label "jako&#347;"
  ]
  node [
    id 757
    label "jako_tako"
  ]
  node [
    id 758
    label "niez&#322;y"
  ]
  node [
    id 759
    label "dziwny"
  ]
  node [
    id 760
    label "charakterystyczny"
  ]
  node [
    id 761
    label "wyj&#261;tkowy"
  ]
  node [
    id 762
    label "nieprzeci&#281;tny"
  ]
  node [
    id 763
    label "wa&#380;ny"
  ]
  node [
    id 764
    label "prawdziwy"
  ]
  node [
    id 765
    label "wybitny"
  ]
  node [
    id 766
    label "dupny"
  ]
  node [
    id 767
    label "intensywnie"
  ]
  node [
    id 768
    label "nieprzeci&#281;tnie"
  ]
  node [
    id 769
    label "niespotykany"
  ]
  node [
    id 770
    label "wydatny"
  ]
  node [
    id 771
    label "wspania&#322;y"
  ]
  node [
    id 772
    label "ponadprzeci&#281;tny"
  ]
  node [
    id 773
    label "&#347;wietny"
  ]
  node [
    id 774
    label "imponuj&#261;cy"
  ]
  node [
    id 775
    label "wybitnie"
  ]
  node [
    id 776
    label "celny"
  ]
  node [
    id 777
    label "&#380;ywny"
  ]
  node [
    id 778
    label "szczery"
  ]
  node [
    id 779
    label "naturalny"
  ]
  node [
    id 780
    label "naprawd&#281;"
  ]
  node [
    id 781
    label "realnie"
  ]
  node [
    id 782
    label "zgodny"
  ]
  node [
    id 783
    label "m&#261;dry"
  ]
  node [
    id 784
    label "prawdziwie"
  ]
  node [
    id 785
    label "wyj&#261;tkowo"
  ]
  node [
    id 786
    label "znacznie"
  ]
  node [
    id 787
    label "zauwa&#380;alny"
  ]
  node [
    id 788
    label "wynios&#322;y"
  ]
  node [
    id 789
    label "dono&#347;ny"
  ]
  node [
    id 790
    label "silny"
  ]
  node [
    id 791
    label "wa&#380;nie"
  ]
  node [
    id 792
    label "istotnie"
  ]
  node [
    id 793
    label "eksponowany"
  ]
  node [
    id 794
    label "dobry"
  ]
  node [
    id 795
    label "do_dupy"
  ]
  node [
    id 796
    label "z&#322;y"
  ]
  node [
    id 797
    label "Had&#380;ar"
  ]
  node [
    id 798
    label "kamienienie"
  ]
  node [
    id 799
    label "oczko"
  ]
  node [
    id 800
    label "ska&#322;a"
  ]
  node [
    id 801
    label "osad"
  ]
  node [
    id 802
    label "ci&#281;&#380;ar"
  ]
  node [
    id 803
    label "p&#322;ytka"
  ]
  node [
    id 804
    label "skamienienie"
  ]
  node [
    id 805
    label "cube"
  ]
  node [
    id 806
    label "funt"
  ]
  node [
    id 807
    label "mad&#380;ong"
  ]
  node [
    id 808
    label "tworzywo"
  ]
  node [
    id 809
    label "jednostka_avoirdupois"
  ]
  node [
    id 810
    label "domino"
  ]
  node [
    id 811
    label "rock"
  ]
  node [
    id 812
    label "z&#322;&#243;g"
  ]
  node [
    id 813
    label "lapidarium"
  ]
  node [
    id 814
    label "autografia"
  ]
  node [
    id 815
    label "rekwizyt_do_gry"
  ]
  node [
    id 816
    label "minera&#322;_barwny"
  ]
  node [
    id 817
    label "sedymentacja"
  ]
  node [
    id 818
    label "kompakcja"
  ]
  node [
    id 819
    label "terygeniczny"
  ]
  node [
    id 820
    label "wspomnienie"
  ]
  node [
    id 821
    label "kolmatacja"
  ]
  node [
    id 822
    label "deposit"
  ]
  node [
    id 823
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 824
    label "uskoczenie"
  ]
  node [
    id 825
    label "mieszanina"
  ]
  node [
    id 826
    label "zmetamorfizowanie"
  ]
  node [
    id 827
    label "soczewa"
  ]
  node [
    id 828
    label "opoka"
  ]
  node [
    id 829
    label "uskakiwa&#263;"
  ]
  node [
    id 830
    label "sklerometr"
  ]
  node [
    id 831
    label "&#322;upkowato&#347;&#263;"
  ]
  node [
    id 832
    label "uskakiwanie"
  ]
  node [
    id 833
    label "uskoczy&#263;"
  ]
  node [
    id 834
    label "porwak"
  ]
  node [
    id 835
    label "bloczno&#347;&#263;"
  ]
  node [
    id 836
    label "zmetamorfizowa&#263;"
  ]
  node [
    id 837
    label "lepiszcze_skalne"
  ]
  node [
    id 838
    label "rygiel"
  ]
  node [
    id 839
    label "lamina"
  ]
  node [
    id 840
    label "wyg&#322;ad_lodowcowy"
  ]
  node [
    id 841
    label "blaszka"
  ]
  node [
    id 842
    label "plate"
  ]
  node [
    id 843
    label "os&#322;ona_cia&#322;a"
  ]
  node [
    id 844
    label "p&#322;yta"
  ]
  node [
    id 845
    label "dysk_optyczny"
  ]
  node [
    id 846
    label "zmiana"
  ]
  node [
    id 847
    label "warto&#347;&#263;"
  ]
  node [
    id 848
    label "przeszkoda"
  ]
  node [
    id 849
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 850
    label "hantla"
  ]
  node [
    id 851
    label "hazard"
  ]
  node [
    id 852
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 853
    label "zawa&#380;y&#263;"
  ]
  node [
    id 854
    label "wym&#243;g"
  ]
  node [
    id 855
    label "obarczy&#263;"
  ]
  node [
    id 856
    label "zawa&#380;enie"
  ]
  node [
    id 857
    label "weight"
  ]
  node [
    id 858
    label "powinno&#347;&#263;"
  ]
  node [
    id 859
    label "load"
  ]
  node [
    id 860
    label "substancja"
  ]
  node [
    id 861
    label "jednostka_monetarna"
  ]
  node [
    id 862
    label "pens_brytyjski"
  ]
  node [
    id 863
    label "Falklandy"
  ]
  node [
    id 864
    label "Wielka_Brytania"
  ]
  node [
    id 865
    label "Wyspa_Man"
  ]
  node [
    id 866
    label "uncja"
  ]
  node [
    id 867
    label "cetnar"
  ]
  node [
    id 868
    label "Wyspa_&#346;wi&#281;tej_Heleny"
  ]
  node [
    id 869
    label "Guernsey"
  ]
  node [
    id 870
    label "marka"
  ]
  node [
    id 871
    label "haczyk"
  ]
  node [
    id 872
    label "&#347;cieg"
  ]
  node [
    id 873
    label "ekoton"
  ]
  node [
    id 874
    label "staw"
  ]
  node [
    id 875
    label "ozdoba"
  ]
  node [
    id 876
    label "ladder"
  ]
  node [
    id 877
    label "szczep"
  ]
  node [
    id 878
    label "stawon&#243;g"
  ]
  node [
    id 879
    label "p&#281;telka"
  ]
  node [
    id 880
    label "organ"
  ]
  node [
    id 881
    label "eye"
  ]
  node [
    id 882
    label "czcionka"
  ]
  node [
    id 883
    label "k&#243;&#322;ko"
  ]
  node [
    id 884
    label "pier&#347;cionek"
  ]
  node [
    id 885
    label "p&#261;k"
  ]
  node [
    id 886
    label "ros&#243;&#322;"
  ]
  node [
    id 887
    label "kostka"
  ]
  node [
    id 888
    label "wypowied&#378;"
  ]
  node [
    id 889
    label "uk&#322;ad"
  ]
  node [
    id 890
    label "oko"
  ]
  node [
    id 891
    label "ziemniak"
  ]
  node [
    id 892
    label "ogr&#243;d_wodny"
  ]
  node [
    id 893
    label "gra_w_karty"
  ]
  node [
    id 894
    label "muzeum"
  ]
  node [
    id 895
    label "gra_towarzyska"
  ]
  node [
    id 896
    label "gra"
  ]
  node [
    id 897
    label "kaptur"
  ]
  node [
    id 898
    label "p&#322;aszcz"
  ]
  node [
    id 899
    label "kostium"
  ]
  node [
    id 900
    label "technika_litograficzna"
  ]
  node [
    id 901
    label "napis"
  ]
  node [
    id 902
    label "reprodukcja"
  ]
  node [
    id 903
    label "przenoszenie"
  ]
  node [
    id 904
    label "oboj&#281;tnienie"
  ]
  node [
    id 905
    label "nieruchomienie"
  ]
  node [
    id 906
    label "twardnienie"
  ]
  node [
    id 907
    label "petrifaction"
  ]
  node [
    id 908
    label "stawanie_si&#281;"
  ]
  node [
    id 909
    label "zoboj&#281;tnienie"
  ]
  node [
    id 910
    label "stwardnienie"
  ]
  node [
    id 911
    label "stanie_si&#281;"
  ]
  node [
    id 912
    label "znieruchomienie"
  ]
  node [
    id 913
    label "riff"
  ]
  node [
    id 914
    label "muzyka_rozrywkowa"
  ]
  node [
    id 915
    label "dzia&#322;anie"
  ]
  node [
    id 916
    label "termination"
  ]
  node [
    id 917
    label "narobienie"
  ]
  node [
    id 918
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 919
    label "creation"
  ]
  node [
    id 920
    label "porobienie"
  ]
  node [
    id 921
    label "infimum"
  ]
  node [
    id 922
    label "liczenie"
  ]
  node [
    id 923
    label "skutek"
  ]
  node [
    id 924
    label "podzia&#322;anie"
  ]
  node [
    id 925
    label "supremum"
  ]
  node [
    id 926
    label "kampania"
  ]
  node [
    id 927
    label "uruchamianie"
  ]
  node [
    id 928
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 929
    label "operacja"
  ]
  node [
    id 930
    label "uruchomienie"
  ]
  node [
    id 931
    label "nakr&#281;canie"
  ]
  node [
    id 932
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 933
    label "matematyka"
  ]
  node [
    id 934
    label "tr&#243;jstronny"
  ]
  node [
    id 935
    label "nakr&#281;cenie"
  ]
  node [
    id 936
    label "zatrzymanie"
  ]
  node [
    id 937
    label "wp&#322;yw"
  ]
  node [
    id 938
    label "rzut"
  ]
  node [
    id 939
    label "podtrzymywanie"
  ]
  node [
    id 940
    label "w&#322;&#261;czanie"
  ]
  node [
    id 941
    label "operation"
  ]
  node [
    id 942
    label "dzianie_si&#281;"
  ]
  node [
    id 943
    label "zadzia&#322;anie"
  ]
  node [
    id 944
    label "priorytet"
  ]
  node [
    id 945
    label "bycie"
  ]
  node [
    id 946
    label "kres"
  ]
  node [
    id 947
    label "rozpocz&#281;cie"
  ]
  node [
    id 948
    label "funkcja"
  ]
  node [
    id 949
    label "czynny"
  ]
  node [
    id 950
    label "impact"
  ]
  node [
    id 951
    label "oferta"
  ]
  node [
    id 952
    label "zako&#324;czenie"
  ]
  node [
    id 953
    label "w&#322;&#261;czenie"
  ]
  node [
    id 954
    label "gzyms"
  ]
  node [
    id 955
    label "obstruction"
  ]
  node [
    id 956
    label "futbolista"
  ]
  node [
    id 957
    label "futr&#243;wka"
  ]
  node [
    id 958
    label "tynk"
  ]
  node [
    id 959
    label "ceg&#322;a"
  ]
  node [
    id 960
    label "fola"
  ]
  node [
    id 961
    label "trudno&#347;&#263;"
  ]
  node [
    id 962
    label "konstrukcja"
  ]
  node [
    id 963
    label "belkowanie"
  ]
  node [
    id 964
    label "&#347;ciana"
  ]
  node [
    id 965
    label "rz&#261;d"
  ]
  node [
    id 966
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 967
    label "napotka&#263;"
  ]
  node [
    id 968
    label "subiekcja"
  ]
  node [
    id 969
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 970
    label "k&#322;opotliwy"
  ]
  node [
    id 971
    label "napotkanie"
  ]
  node [
    id 972
    label "difficulty"
  ]
  node [
    id 973
    label "obstacle"
  ]
  node [
    id 974
    label "sytuacja"
  ]
  node [
    id 975
    label "przybli&#380;enie"
  ]
  node [
    id 976
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 977
    label "kategoria"
  ]
  node [
    id 978
    label "szpaler"
  ]
  node [
    id 979
    label "lon&#380;a"
  ]
  node [
    id 980
    label "uporz&#261;dkowanie"
  ]
  node [
    id 981
    label "instytucja"
  ]
  node [
    id 982
    label "egzekutywa"
  ]
  node [
    id 983
    label "premier"
  ]
  node [
    id 984
    label "Londyn"
  ]
  node [
    id 985
    label "gabinet_cieni"
  ]
  node [
    id 986
    label "number"
  ]
  node [
    id 987
    label "Konsulat"
  ]
  node [
    id 988
    label "tract"
  ]
  node [
    id 989
    label "klasa"
  ]
  node [
    id 990
    label "w&#322;adza"
  ]
  node [
    id 991
    label "profil"
  ]
  node [
    id 992
    label "zbocze"
  ]
  node [
    id 993
    label "przegroda"
  ]
  node [
    id 994
    label "bariera"
  ]
  node [
    id 995
    label "facebook"
  ]
  node [
    id 996
    label "wielo&#347;cian"
  ]
  node [
    id 997
    label "pow&#322;oka"
  ]
  node [
    id 998
    label "wyrobisko"
  ]
  node [
    id 999
    label "struktura"
  ]
  node [
    id 1000
    label "practice"
  ]
  node [
    id 1001
    label "wykre&#347;lanie"
  ]
  node [
    id 1002
    label "element_konstrukcyjny"
  ]
  node [
    id 1003
    label "obudowanie"
  ]
  node [
    id 1004
    label "obudowywa&#263;"
  ]
  node [
    id 1005
    label "zbudowa&#263;"
  ]
  node [
    id 1006
    label "obudowa&#263;"
  ]
  node [
    id 1007
    label "kolumnada"
  ]
  node [
    id 1008
    label "korpus"
  ]
  node [
    id 1009
    label "Sukiennice"
  ]
  node [
    id 1010
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 1011
    label "fundament"
  ]
  node [
    id 1012
    label "postanie"
  ]
  node [
    id 1013
    label "obudowywanie"
  ]
  node [
    id 1014
    label "zbudowanie"
  ]
  node [
    id 1015
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 1016
    label "stan_surowy"
  ]
  node [
    id 1017
    label "Messi"
  ]
  node [
    id 1018
    label "pi&#322;karstwo"
  ]
  node [
    id 1019
    label "pi&#322;karz"
  ]
  node [
    id 1020
    label "sportowiec"
  ]
  node [
    id 1021
    label "gracz"
  ]
  node [
    id 1022
    label "wyst&#281;p"
  ]
  node [
    id 1023
    label "karnisz"
  ]
  node [
    id 1024
    label "mebel"
  ]
  node [
    id 1025
    label "uk&#322;adanie"
  ]
  node [
    id 1026
    label "fryz"
  ]
  node [
    id 1027
    label "entablature"
  ]
  node [
    id 1028
    label "kokaina"
  ]
  node [
    id 1029
    label "narkotyk"
  ]
  node [
    id 1030
    label "makija&#380;"
  ]
  node [
    id 1031
    label "zaprawa"
  ]
  node [
    id 1032
    label "but"
  ]
  node [
    id 1033
    label "ok&#322;adzina"
  ]
  node [
    id 1034
    label "wy&#347;ci&#243;&#322;ka"
  ]
  node [
    id 1035
    label "statek"
  ]
  node [
    id 1036
    label "obicie"
  ]
  node [
    id 1037
    label "materia&#322;_budowlany"
  ]
  node [
    id 1038
    label "wi&#261;zanie"
  ]
  node [
    id 1039
    label "tile"
  ]
  node [
    id 1040
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 1041
    label "g&#322;&#243;wka"
  ]
  node [
    id 1042
    label "woz&#243;wka"
  ]
  node [
    id 1043
    label "skrzynia"
  ]
  node [
    id 1044
    label "sie&#263;_rybacka"
  ]
  node [
    id 1045
    label "wapno_gaszone"
  ]
  node [
    id 1046
    label "Zamek_Ogrodzieniec"
  ]
  node [
    id 1047
    label "drzwi"
  ]
  node [
    id 1048
    label "blokada"
  ]
  node [
    id 1049
    label "zamkni&#281;cie"
  ]
  node [
    id 1050
    label "wjazd"
  ]
  node [
    id 1051
    label "bro&#324;_palna"
  ]
  node [
    id 1052
    label "brama"
  ]
  node [
    id 1053
    label "blockage"
  ]
  node [
    id 1054
    label "zapi&#281;cie"
  ]
  node [
    id 1055
    label "tercja"
  ]
  node [
    id 1056
    label "ekspres"
  ]
  node [
    id 1057
    label "mechanizm"
  ]
  node [
    id 1058
    label "komora_zamkowa"
  ]
  node [
    id 1059
    label "Windsor"
  ]
  node [
    id 1060
    label "stra&#380;nica"
  ]
  node [
    id 1061
    label "fortyfikacja"
  ]
  node [
    id 1062
    label "rezydencja"
  ]
  node [
    id 1063
    label "bramka"
  ]
  node [
    id 1064
    label "iglica"
  ]
  node [
    id 1065
    label "informatyka"
  ]
  node [
    id 1066
    label "zagrywka"
  ]
  node [
    id 1067
    label "hokej"
  ]
  node [
    id 1068
    label "baszta"
  ]
  node [
    id 1069
    label "fastener"
  ]
  node [
    id 1070
    label "Wawel"
  ]
  node [
    id 1071
    label "bloking"
  ]
  node [
    id 1072
    label "znieczulenie"
  ]
  node [
    id 1073
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1074
    label "kolej"
  ]
  node [
    id 1075
    label "block"
  ]
  node [
    id 1076
    label "utrudnienie"
  ]
  node [
    id 1077
    label "arrest"
  ]
  node [
    id 1078
    label "anestezja"
  ]
  node [
    id 1079
    label "ochrona"
  ]
  node [
    id 1080
    label "sk&#322;ad_drukarski"
  ]
  node [
    id 1081
    label "izolacja"
  ]
  node [
    id 1082
    label "blok"
  ]
  node [
    id 1083
    label "zwrotnica"
  ]
  node [
    id 1084
    label "siatk&#243;wka"
  ]
  node [
    id 1085
    label "sankcja"
  ]
  node [
    id 1086
    label "semafor"
  ]
  node [
    id 1087
    label "obrona"
  ]
  node [
    id 1088
    label "deadlock"
  ]
  node [
    id 1089
    label "lock"
  ]
  node [
    id 1090
    label "niemo&#380;no&#347;&#263;"
  ]
  node [
    id 1091
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1092
    label "maszyneria"
  ]
  node [
    id 1093
    label "maszyna"
  ]
  node [
    id 1094
    label "gambit"
  ]
  node [
    id 1095
    label "rozgrywka"
  ]
  node [
    id 1096
    label "move"
  ]
  node [
    id 1097
    label "manewr"
  ]
  node [
    id 1098
    label "uderzenie"
  ]
  node [
    id 1099
    label "posuni&#281;cie"
  ]
  node [
    id 1100
    label "myk"
  ]
  node [
    id 1101
    label "mecz"
  ]
  node [
    id 1102
    label "travel"
  ]
  node [
    id 1103
    label "zapi&#281;cie_si&#281;"
  ]
  node [
    id 1104
    label "buckle"
  ]
  node [
    id 1105
    label "balkon"
  ]
  node [
    id 1106
    label "pod&#322;oga"
  ]
  node [
    id 1107
    label "skrzyd&#322;o"
  ]
  node [
    id 1108
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 1109
    label "dach"
  ]
  node [
    id 1110
    label "strop"
  ]
  node [
    id 1111
    label "klatka_schodowa"
  ]
  node [
    id 1112
    label "przedpro&#380;e"
  ]
  node [
    id 1113
    label "Pentagon"
  ]
  node [
    id 1114
    label "alkierz"
  ]
  node [
    id 1115
    label "front"
  ]
  node [
    id 1116
    label "siedziba"
  ]
  node [
    id 1117
    label "dom"
  ]
  node [
    id 1118
    label "wy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1119
    label "przyskrzynienie"
  ]
  node [
    id 1120
    label "przygaszenie"
  ]
  node [
    id 1121
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 1122
    label "profligacy"
  ]
  node [
    id 1123
    label "dissolution"
  ]
  node [
    id 1124
    label "spowodowanie"
  ]
  node [
    id 1125
    label "ukrycie"
  ]
  node [
    id 1126
    label "completion"
  ]
  node [
    id 1127
    label "end"
  ]
  node [
    id 1128
    label "uj&#281;cie"
  ]
  node [
    id 1129
    label "exit"
  ]
  node [
    id 1130
    label "rozwi&#261;zanie"
  ]
  node [
    id 1131
    label "zawarcie"
  ]
  node [
    id 1132
    label "closing"
  ]
  node [
    id 1133
    label "z&#322;o&#380;enie"
  ]
  node [
    id 1134
    label "release"
  ]
  node [
    id 1135
    label "umieszczenie"
  ]
  node [
    id 1136
    label "zablokowanie"
  ]
  node [
    id 1137
    label "pozamykanie"
  ]
  node [
    id 1138
    label "HP"
  ]
  node [
    id 1139
    label "dost&#281;p"
  ]
  node [
    id 1140
    label "infa"
  ]
  node [
    id 1141
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 1142
    label "kryptologia"
  ]
  node [
    id 1143
    label "baza_danych"
  ]
  node [
    id 1144
    label "przetwarzanie_informacji"
  ]
  node [
    id 1145
    label "sztuczna_inteligencja"
  ]
  node [
    id 1146
    label "gramatyka_formalna"
  ]
  node [
    id 1147
    label "program"
  ]
  node [
    id 1148
    label "dziedzina_informatyki"
  ]
  node [
    id 1149
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 1150
    label "artefakt"
  ]
  node [
    id 1151
    label "bezbramkowy"
  ]
  node [
    id 1152
    label "kij_hokejowy"
  ]
  node [
    id 1153
    label "bodiczek"
  ]
  node [
    id 1154
    label "kr&#261;&#380;ek"
  ]
  node [
    id 1155
    label "sport"
  ]
  node [
    id 1156
    label "sport_zespo&#322;owy"
  ]
  node [
    id 1157
    label "sport_zimowy"
  ]
  node [
    id 1158
    label "bramkarz"
  ]
  node [
    id 1159
    label "stopie&#324;_pisma"
  ]
  node [
    id 1160
    label "pole"
  ]
  node [
    id 1161
    label "godzina_kanoniczna"
  ]
  node [
    id 1162
    label "hokej_na_lodzie"
  ]
  node [
    id 1163
    label "interwa&#322;"
  ]
  node [
    id 1164
    label "mechanizm_uderzeniowy"
  ]
  node [
    id 1165
    label "narz&#281;dzie"
  ]
  node [
    id 1166
    label "zwie&#324;czenie"
  ]
  node [
    id 1167
    label "bodziszkowate"
  ]
  node [
    id 1168
    label "chwast"
  ]
  node [
    id 1169
    label "stylus"
  ]
  node [
    id 1170
    label "fosa"
  ]
  node [
    id 1171
    label "fort"
  ]
  node [
    id 1172
    label "szaniec"
  ]
  node [
    id 1173
    label "machiku&#322;"
  ]
  node [
    id 1174
    label "kazamata"
  ]
  node [
    id 1175
    label "bastion"
  ]
  node [
    id 1176
    label "kurtyna"
  ]
  node [
    id 1177
    label "barykada"
  ]
  node [
    id 1178
    label "przedbramie"
  ]
  node [
    id 1179
    label "transzeja"
  ]
  node [
    id 1180
    label "palisada"
  ]
  node [
    id 1181
    label "in&#380;ynieria"
  ]
  node [
    id 1182
    label "okop"
  ]
  node [
    id 1183
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 1184
    label "&#347;r&#243;dszaniec"
  ]
  node [
    id 1185
    label "basteja"
  ]
  node [
    id 1186
    label "wie&#380;a"
  ]
  node [
    id 1187
    label "skalica"
  ]
  node [
    id 1188
    label "Dipylon"
  ]
  node [
    id 1189
    label "ryba"
  ]
  node [
    id 1190
    label "antaba"
  ]
  node [
    id 1191
    label "samborze"
  ]
  node [
    id 1192
    label "brona"
  ]
  node [
    id 1193
    label "bramowate"
  ]
  node [
    id 1194
    label "wrzeci&#261;dz"
  ]
  node [
    id 1195
    label "Ostra_Brama"
  ]
  node [
    id 1196
    label "obstawianie"
  ]
  node [
    id 1197
    label "trafienie"
  ]
  node [
    id 1198
    label "obstawienie"
  ]
  node [
    id 1199
    label "zawiasy"
  ]
  node [
    id 1200
    label "s&#322;upek"
  ]
  node [
    id 1201
    label "boisko"
  ]
  node [
    id 1202
    label "siatka"
  ]
  node [
    id 1203
    label "obstawia&#263;"
  ]
  node [
    id 1204
    label "ogrodzenie"
  ]
  node [
    id 1205
    label "goal"
  ]
  node [
    id 1206
    label "poprzeczka"
  ]
  node [
    id 1207
    label "p&#322;ot"
  ]
  node [
    id 1208
    label "obstawi&#263;"
  ]
  node [
    id 1209
    label "wej&#347;cie"
  ]
  node [
    id 1210
    label "szafka"
  ]
  node [
    id 1211
    label "doj&#347;cie"
  ]
  node [
    id 1212
    label "klamka"
  ]
  node [
    id 1213
    label "wyj&#347;cie"
  ]
  node [
    id 1214
    label "szafa"
  ]
  node [
    id 1215
    label "samozamykacz"
  ]
  node [
    id 1216
    label "futryna"
  ]
  node [
    id 1217
    label "ko&#322;atka"
  ]
  node [
    id 1218
    label "droga"
  ]
  node [
    id 1219
    label "wydarzenie"
  ]
  node [
    id 1220
    label "poci&#261;g"
  ]
  node [
    id 1221
    label "urz&#261;dzenie_kuchenne"
  ]
  node [
    id 1222
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 1223
    label "przyrz&#261;d"
  ]
  node [
    id 1224
    label "pos&#322;aniec"
  ]
  node [
    id 1225
    label "us&#322;uga"
  ]
  node [
    id 1226
    label "kolba"
  ]
  node [
    id 1227
    label "zamek_b&#322;yskawiczny"
  ]
  node [
    id 1228
    label "sztucer"
  ]
  node [
    id 1229
    label "przesy&#322;ka"
  ]
  node [
    id 1230
    label "arras"
  ]
  node [
    id 1231
    label "kolejny"
  ]
  node [
    id 1232
    label "osobno"
  ]
  node [
    id 1233
    label "r&#243;&#380;ny"
  ]
  node [
    id 1234
    label "inszy"
  ]
  node [
    id 1235
    label "inaczej"
  ]
  node [
    id 1236
    label "odr&#281;bny"
  ]
  node [
    id 1237
    label "nast&#281;pnie"
  ]
  node [
    id 1238
    label "nastopny"
  ]
  node [
    id 1239
    label "kolejno"
  ]
  node [
    id 1240
    label "kt&#243;ry&#347;"
  ]
  node [
    id 1241
    label "r&#243;&#380;nie"
  ]
  node [
    id 1242
    label "niestandardowo"
  ]
  node [
    id 1243
    label "individually"
  ]
  node [
    id 1244
    label "udzielnie"
  ]
  node [
    id 1245
    label "osobnie"
  ]
  node [
    id 1246
    label "odr&#281;bnie"
  ]
  node [
    id 1247
    label "osobny"
  ]
  node [
    id 1248
    label "object"
  ]
  node [
    id 1249
    label "temat"
  ]
  node [
    id 1250
    label "mienie"
  ]
  node [
    id 1251
    label "documentation"
  ]
  node [
    id 1252
    label "kamie&#324;_w&#281;gielny"
  ]
  node [
    id 1253
    label "podwa&#322;"
  ]
  node [
    id 1254
    label "zasadzenie"
  ]
  node [
    id 1255
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1256
    label "punkt_odniesienia"
  ]
  node [
    id 1257
    label "zasadzi&#263;"
  ]
  node [
    id 1258
    label "podstawowy"
  ]
  node [
    id 1259
    label "pachwina"
  ]
  node [
    id 1260
    label "obudowa"
  ]
  node [
    id 1261
    label "corpus"
  ]
  node [
    id 1262
    label "brzuch"
  ]
  node [
    id 1263
    label "sto&#380;ek_wzrostu"
  ]
  node [
    id 1264
    label "dywizja"
  ]
  node [
    id 1265
    label "dekolt"
  ]
  node [
    id 1266
    label "zad"
  ]
  node [
    id 1267
    label "zwi&#261;zek_taktyczny"
  ]
  node [
    id 1268
    label "struktura_anatomiczna"
  ]
  node [
    id 1269
    label "pupa"
  ]
  node [
    id 1270
    label "krocze"
  ]
  node [
    id 1271
    label "pier&#347;"
  ]
  node [
    id 1272
    label "tuszka"
  ]
  node [
    id 1273
    label "konkordancja"
  ]
  node [
    id 1274
    label "plecy"
  ]
  node [
    id 1275
    label "klatka_piersiowa"
  ]
  node [
    id 1276
    label "dr&#243;b"
  ]
  node [
    id 1277
    label "wojsko"
  ]
  node [
    id 1278
    label "biodro"
  ]
  node [
    id 1279
    label "pacha"
  ]
  node [
    id 1280
    label "utworzenie"
  ]
  node [
    id 1281
    label "stworzenie"
  ]
  node [
    id 1282
    label "powstanie"
  ]
  node [
    id 1283
    label "potworzenie"
  ]
  node [
    id 1284
    label "erecting"
  ]
  node [
    id 1285
    label "zabezpieczanie"
  ]
  node [
    id 1286
    label "za&#322;&#261;czanie"
  ]
  node [
    id 1287
    label "otaczanie"
  ]
  node [
    id 1288
    label "wyposa&#380;anie"
  ]
  node [
    id 1289
    label "zakrywanie"
  ]
  node [
    id 1290
    label "pozostanie"
  ]
  node [
    id 1291
    label "pobycie"
  ]
  node [
    id 1292
    label "przetrwanie"
  ]
  node [
    id 1293
    label "colonnade"
  ]
  node [
    id 1294
    label "dipteros"
  ]
  node [
    id 1295
    label "perypter"
  ]
  node [
    id 1296
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 1297
    label "otacza&#263;"
  ]
  node [
    id 1298
    label "zabezpiecza&#263;"
  ]
  node [
    id 1299
    label "zakrywa&#263;"
  ]
  node [
    id 1300
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 1301
    label "stworzy&#263;"
  ]
  node [
    id 1302
    label "establish"
  ]
  node [
    id 1303
    label "evolve"
  ]
  node [
    id 1304
    label "zaplanowa&#263;"
  ]
  node [
    id 1305
    label "wytworzy&#263;"
  ]
  node [
    id 1306
    label "otoczenie"
  ]
  node [
    id 1307
    label "zabezpieczenie"
  ]
  node [
    id 1308
    label "wyposa&#380;enie"
  ]
  node [
    id 1309
    label "za&#322;&#261;czenie"
  ]
  node [
    id 1310
    label "zakrycie"
  ]
  node [
    id 1311
    label "za&#322;&#261;czy&#263;"
  ]
  node [
    id 1312
    label "otoczy&#263;"
  ]
  node [
    id 1313
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 1314
    label "case"
  ]
  node [
    id 1315
    label "zakry&#263;"
  ]
  node [
    id 1316
    label "smother"
  ]
  node [
    id 1317
    label "zabezpieczy&#263;"
  ]
  node [
    id 1318
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 1319
    label "po_cesarsku"
  ]
  node [
    id 1320
    label "po_kr&#243;lewsku"
  ]
  node [
    id 1321
    label "wspaniale"
  ]
  node [
    id 1322
    label "pomy&#347;lny"
  ]
  node [
    id 1323
    label "pozytywny"
  ]
  node [
    id 1324
    label "&#347;wietnie"
  ]
  node [
    id 1325
    label "spania&#322;y"
  ]
  node [
    id 1326
    label "och&#281;do&#380;ny"
  ]
  node [
    id 1327
    label "zajebisty"
  ]
  node [
    id 1328
    label "bogato"
  ]
  node [
    id 1329
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1330
    label "nale&#380;ny"
  ]
  node [
    id 1331
    label "nale&#380;yty"
  ]
  node [
    id 1332
    label "typowy"
  ]
  node [
    id 1333
    label "uprawniony"
  ]
  node [
    id 1334
    label "zasadniczy"
  ]
  node [
    id 1335
    label "stosownie"
  ]
  node [
    id 1336
    label "taki"
  ]
  node [
    id 1337
    label "ten"
  ]
  node [
    id 1338
    label "Skyresha"
  ]
  node [
    id 1339
    label "Bolgolama"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 209
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 211
  ]
  edge [
    source 1
    target 212
  ]
  edge [
    source 1
    target 213
  ]
  edge [
    source 1
    target 214
  ]
  edge [
    source 1
    target 215
  ]
  edge [
    source 1
    target 216
  ]
  edge [
    source 1
    target 217
  ]
  edge [
    source 1
    target 218
  ]
  edge [
    source 1
    target 219
  ]
  edge [
    source 1
    target 220
  ]
  edge [
    source 1
    target 221
  ]
  edge [
    source 1
    target 222
  ]
  edge [
    source 1
    target 223
  ]
  edge [
    source 1
    target 224
  ]
  edge [
    source 1
    target 225
  ]
  edge [
    source 1
    target 226
  ]
  edge [
    source 1
    target 227
  ]
  edge [
    source 1
    target 228
  ]
  edge [
    source 1
    target 229
  ]
  edge [
    source 1
    target 230
  ]
  edge [
    source 1
    target 231
  ]
  edge [
    source 1
    target 232
  ]
  edge [
    source 1
    target 233
  ]
  edge [
    source 1
    target 234
  ]
  edge [
    source 1
    target 235
  ]
  edge [
    source 1
    target 236
  ]
  edge [
    source 1
    target 237
  ]
  edge [
    source 1
    target 238
  ]
  edge [
    source 1
    target 239
  ]
  edge [
    source 1
    target 240
  ]
  edge [
    source 1
    target 241
  ]
  edge [
    source 1
    target 242
  ]
  edge [
    source 1
    target 243
  ]
  edge [
    source 1
    target 244
  ]
  edge [
    source 1
    target 245
  ]
  edge [
    source 1
    target 246
  ]
  edge [
    source 1
    target 247
  ]
  edge [
    source 1
    target 248
  ]
  edge [
    source 1
    target 249
  ]
  edge [
    source 1
    target 250
  ]
  edge [
    source 1
    target 251
  ]
  edge [
    source 1
    target 252
  ]
  edge [
    source 1
    target 253
  ]
  edge [
    source 1
    target 254
  ]
  edge [
    source 1
    target 255
  ]
  edge [
    source 1
    target 256
  ]
  edge [
    source 1
    target 257
  ]
  edge [
    source 1
    target 258
  ]
  edge [
    source 1
    target 259
  ]
  edge [
    source 1
    target 260
  ]
  edge [
    source 1
    target 261
  ]
  edge [
    source 1
    target 262
  ]
  edge [
    source 1
    target 263
  ]
  edge [
    source 1
    target 264
  ]
  edge [
    source 1
    target 265
  ]
  edge [
    source 1
    target 266
  ]
  edge [
    source 1
    target 267
  ]
  edge [
    source 1
    target 268
  ]
  edge [
    source 1
    target 269
  ]
  edge [
    source 1
    target 270
  ]
  edge [
    source 1
    target 271
  ]
  edge [
    source 1
    target 272
  ]
  edge [
    source 1
    target 273
  ]
  edge [
    source 1
    target 274
  ]
  edge [
    source 1
    target 275
  ]
  edge [
    source 1
    target 276
  ]
  edge [
    source 1
    target 277
  ]
  edge [
    source 1
    target 278
  ]
  edge [
    source 1
    target 279
  ]
  edge [
    source 1
    target 280
  ]
  edge [
    source 1
    target 281
  ]
  edge [
    source 1
    target 282
  ]
  edge [
    source 1
    target 283
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 284
  ]
  edge [
    source 2
    target 285
  ]
  edge [
    source 2
    target 286
  ]
  edge [
    source 2
    target 287
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 288
  ]
  edge [
    source 2
    target 289
  ]
  edge [
    source 2
    target 290
  ]
  edge [
    source 2
    target 291
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 292
  ]
  edge [
    source 2
    target 293
  ]
  edge [
    source 2
    target 294
  ]
  edge [
    source 2
    target 295
  ]
  edge [
    source 2
    target 296
  ]
  edge [
    source 2
    target 297
  ]
  edge [
    source 2
    target 298
  ]
  edge [
    source 2
    target 299
  ]
  edge [
    source 2
    target 300
  ]
  edge [
    source 2
    target 301
  ]
  edge [
    source 2
    target 302
  ]
  edge [
    source 2
    target 303
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 304
  ]
  edge [
    source 2
    target 305
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 306
  ]
  edge [
    source 2
    target 307
  ]
  edge [
    source 2
    target 308
  ]
  edge [
    source 2
    target 309
  ]
  edge [
    source 2
    target 310
  ]
  edge [
    source 2
    target 311
  ]
  edge [
    source 2
    target 312
  ]
  edge [
    source 2
    target 313
  ]
  edge [
    source 2
    target 314
  ]
  edge [
    source 2
    target 315
  ]
  edge [
    source 2
    target 316
  ]
  edge [
    source 2
    target 317
  ]
  edge [
    source 2
    target 318
  ]
  edge [
    source 2
    target 319
  ]
  edge [
    source 2
    target 320
  ]
  edge [
    source 2
    target 321
  ]
  edge [
    source 2
    target 322
  ]
  edge [
    source 2
    target 323
  ]
  edge [
    source 2
    target 324
  ]
  edge [
    source 2
    target 325
  ]
  edge [
    source 2
    target 326
  ]
  edge [
    source 2
    target 327
  ]
  edge [
    source 2
    target 328
  ]
  edge [
    source 2
    target 329
  ]
  edge [
    source 2
    target 330
  ]
  edge [
    source 2
    target 331
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 332
  ]
  edge [
    source 2
    target 333
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 334
  ]
  edge [
    source 2
    target 335
  ]
  edge [
    source 2
    target 336
  ]
  edge [
    source 2
    target 337
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 338
  ]
  edge [
    source 2
    target 339
  ]
  edge [
    source 2
    target 340
  ]
  edge [
    source 2
    target 341
  ]
  edge [
    source 2
    target 342
  ]
  edge [
    source 2
    target 343
  ]
  edge [
    source 2
    target 344
  ]
  edge [
    source 2
    target 345
  ]
  edge [
    source 2
    target 346
  ]
  edge [
    source 2
    target 347
  ]
  edge [
    source 2
    target 348
  ]
  edge [
    source 2
    target 349
  ]
  edge [
    source 2
    target 350
  ]
  edge [
    source 2
    target 351
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 352
  ]
  edge [
    source 2
    target 353
  ]
  edge [
    source 2
    target 354
  ]
  edge [
    source 2
    target 355
  ]
  edge [
    source 2
    target 356
  ]
  edge [
    source 2
    target 357
  ]
  edge [
    source 2
    target 358
  ]
  edge [
    source 2
    target 359
  ]
  edge [
    source 2
    target 360
  ]
  edge [
    source 2
    target 361
  ]
  edge [
    source 2
    target 362
  ]
  edge [
    source 2
    target 363
  ]
  edge [
    source 2
    target 364
  ]
  edge [
    source 2
    target 365
  ]
  edge [
    source 2
    target 366
  ]
  edge [
    source 2
    target 367
  ]
  edge [
    source 2
    target 368
  ]
  edge [
    source 2
    target 369
  ]
  edge [
    source 2
    target 370
  ]
  edge [
    source 2
    target 371
  ]
  edge [
    source 2
    target 372
  ]
  edge [
    source 2
    target 373
  ]
  edge [
    source 2
    target 374
  ]
  edge [
    source 2
    target 375
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 376
  ]
  edge [
    source 2
    target 377
  ]
  edge [
    source 2
    target 378
  ]
  edge [
    source 2
    target 379
  ]
  edge [
    source 2
    target 380
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 381
  ]
  edge [
    source 2
    target 382
  ]
  edge [
    source 2
    target 383
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 384
  ]
  edge [
    source 2
    target 385
  ]
  edge [
    source 2
    target 386
  ]
  edge [
    source 2
    target 387
  ]
  edge [
    source 2
    target 388
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 389
  ]
  edge [
    source 2
    target 390
  ]
  edge [
    source 2
    target 391
  ]
  edge [
    source 2
    target 392
  ]
  edge [
    source 2
    target 393
  ]
  edge [
    source 2
    target 394
  ]
  edge [
    source 2
    target 395
  ]
  edge [
    source 2
    target 396
  ]
  edge [
    source 2
    target 397
  ]
  edge [
    source 2
    target 398
  ]
  edge [
    source 2
    target 399
  ]
  edge [
    source 2
    target 400
  ]
  edge [
    source 2
    target 401
  ]
  edge [
    source 2
    target 402
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 403
  ]
  edge [
    source 2
    target 404
  ]
  edge [
    source 2
    target 405
  ]
  edge [
    source 2
    target 406
  ]
  edge [
    source 2
    target 407
  ]
  edge [
    source 2
    target 408
  ]
  edge [
    source 2
    target 409
  ]
  edge [
    source 2
    target 410
  ]
  edge [
    source 2
    target 411
  ]
  edge [
    source 2
    target 412
  ]
  edge [
    source 2
    target 413
  ]
  edge [
    source 2
    target 414
  ]
  edge [
    source 2
    target 415
  ]
  edge [
    source 2
    target 416
  ]
  edge [
    source 2
    target 417
  ]
  edge [
    source 2
    target 418
  ]
  edge [
    source 2
    target 419
  ]
  edge [
    source 2
    target 420
  ]
  edge [
    source 2
    target 421
  ]
  edge [
    source 2
    target 422
  ]
  edge [
    source 2
    target 423
  ]
  edge [
    source 2
    target 424
  ]
  edge [
    source 2
    target 425
  ]
  edge [
    source 2
    target 426
  ]
  edge [
    source 2
    target 427
  ]
  edge [
    source 2
    target 428
  ]
  edge [
    source 2
    target 429
  ]
  edge [
    source 2
    target 430
  ]
  edge [
    source 2
    target 431
  ]
  edge [
    source 2
    target 432
  ]
  edge [
    source 2
    target 433
  ]
  edge [
    source 2
    target 434
  ]
  edge [
    source 2
    target 435
  ]
  edge [
    source 2
    target 436
  ]
  edge [
    source 2
    target 437
  ]
  edge [
    source 2
    target 438
  ]
  edge [
    source 2
    target 439
  ]
  edge [
    source 2
    target 440
  ]
  edge [
    source 2
    target 441
  ]
  edge [
    source 2
    target 442
  ]
  edge [
    source 2
    target 443
  ]
  edge [
    source 2
    target 444
  ]
  edge [
    source 2
    target 445
  ]
  edge [
    source 2
    target 446
  ]
  edge [
    source 2
    target 447
  ]
  edge [
    source 2
    target 448
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 449
  ]
  edge [
    source 2
    target 450
  ]
  edge [
    source 2
    target 451
  ]
  edge [
    source 2
    target 452
  ]
  edge [
    source 2
    target 453
  ]
  edge [
    source 2
    target 454
  ]
  edge [
    source 2
    target 455
  ]
  edge [
    source 2
    target 456
  ]
  edge [
    source 2
    target 457
  ]
  edge [
    source 2
    target 458
  ]
  edge [
    source 2
    target 459
  ]
  edge [
    source 2
    target 460
  ]
  edge [
    source 2
    target 461
  ]
  edge [
    source 2
    target 462
  ]
  edge [
    source 2
    target 463
  ]
  edge [
    source 2
    target 464
  ]
  edge [
    source 2
    target 465
  ]
  edge [
    source 2
    target 466
  ]
  edge [
    source 2
    target 467
  ]
  edge [
    source 2
    target 468
  ]
  edge [
    source 2
    target 469
  ]
  edge [
    source 2
    target 470
  ]
  edge [
    source 2
    target 471
  ]
  edge [
    source 2
    target 472
  ]
  edge [
    source 2
    target 473
  ]
  edge [
    source 2
    target 474
  ]
  edge [
    source 2
    target 475
  ]
  edge [
    source 2
    target 476
  ]
  edge [
    source 2
    target 477
  ]
  edge [
    source 2
    target 478
  ]
  edge [
    source 2
    target 479
  ]
  edge [
    source 2
    target 480
  ]
  edge [
    source 2
    target 481
  ]
  edge [
    source 2
    target 482
  ]
  edge [
    source 2
    target 483
  ]
  edge [
    source 2
    target 484
  ]
  edge [
    source 2
    target 485
  ]
  edge [
    source 2
    target 486
  ]
  edge [
    source 2
    target 487
  ]
  edge [
    source 2
    target 488
  ]
  edge [
    source 2
    target 489
  ]
  edge [
    source 2
    target 490
  ]
  edge [
    source 2
    target 491
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 640
  ]
  edge [
    source 7
    target 641
  ]
  edge [
    source 7
    target 642
  ]
  edge [
    source 7
    target 643
  ]
  edge [
    source 7
    target 644
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 645
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 646
  ]
  edge [
    source 7
    target 647
  ]
  edge [
    source 7
    target 648
  ]
  edge [
    source 7
    target 649
  ]
  edge [
    source 7
    target 650
  ]
  edge [
    source 7
    target 651
  ]
  edge [
    source 7
    target 652
  ]
  edge [
    source 7
    target 653
  ]
  edge [
    source 7
    target 654
  ]
  edge [
    source 7
    target 655
  ]
  edge [
    source 7
    target 656
  ]
  edge [
    source 7
    target 657
  ]
  edge [
    source 7
    target 658
  ]
  edge [
    source 7
    target 659
  ]
  edge [
    source 7
    target 660
  ]
  edge [
    source 7
    target 661
  ]
  edge [
    source 7
    target 662
  ]
  edge [
    source 7
    target 663
  ]
  edge [
    source 7
    target 664
  ]
  edge [
    source 7
    target 665
  ]
  edge [
    source 7
    target 666
  ]
  edge [
    source 7
    target 667
  ]
  edge [
    source 7
    target 668
  ]
  edge [
    source 7
    target 669
  ]
  edge [
    source 7
    target 670
  ]
  edge [
    source 7
    target 671
  ]
  edge [
    source 7
    target 672
  ]
  edge [
    source 7
    target 673
  ]
  edge [
    source 7
    target 674
  ]
  edge [
    source 7
    target 675
  ]
  edge [
    source 7
    target 676
  ]
  edge [
    source 7
    target 677
  ]
  edge [
    source 7
    target 678
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 679
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 8
    target 22
  ]
  edge [
    source 8
    target 23
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 29
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 34
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 8
    target 40
  ]
  edge [
    source 8
    target 41
  ]
  edge [
    source 8
    target 42
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 18
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 586
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 592
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 731
  ]
  edge [
    source 10
    target 732
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 733
  ]
  edge [
    source 10
    target 734
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 735
  ]
  edge [
    source 10
    target 736
  ]
  edge [
    source 10
    target 737
  ]
  edge [
    source 10
    target 738
  ]
  edge [
    source 10
    target 739
  ]
  edge [
    source 10
    target 740
  ]
  edge [
    source 10
    target 741
  ]
  edge [
    source 10
    target 742
  ]
  edge [
    source 10
    target 743
  ]
  edge [
    source 10
    target 744
  ]
  edge [
    source 10
    target 745
  ]
  edge [
    source 10
    target 746
  ]
  edge [
    source 10
    target 747
  ]
  edge [
    source 10
    target 748
  ]
  edge [
    source 10
    target 749
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 751
  ]
  edge [
    source 10
    target 752
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 487
  ]
  edge [
    source 12
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 485
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 797
  ]
  edge [
    source 13
    target 798
  ]
  edge [
    source 13
    target 799
  ]
  edge [
    source 13
    target 800
  ]
  edge [
    source 13
    target 801
  ]
  edge [
    source 13
    target 802
  ]
  edge [
    source 13
    target 803
  ]
  edge [
    source 13
    target 804
  ]
  edge [
    source 13
    target 805
  ]
  edge [
    source 13
    target 806
  ]
  edge [
    source 13
    target 807
  ]
  edge [
    source 13
    target 808
  ]
  edge [
    source 13
    target 809
  ]
  edge [
    source 13
    target 810
  ]
  edge [
    source 13
    target 811
  ]
  edge [
    source 13
    target 812
  ]
  edge [
    source 13
    target 813
  ]
  edge [
    source 13
    target 814
  ]
  edge [
    source 13
    target 815
  ]
  edge [
    source 13
    target 816
  ]
  edge [
    source 13
    target 817
  ]
  edge [
    source 13
    target 818
  ]
  edge [
    source 13
    target 819
  ]
  edge [
    source 13
    target 820
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 822
  ]
  edge [
    source 13
    target 823
  ]
  edge [
    source 13
    target 824
  ]
  edge [
    source 13
    target 825
  ]
  edge [
    source 13
    target 826
  ]
  edge [
    source 13
    target 827
  ]
  edge [
    source 13
    target 828
  ]
  edge [
    source 13
    target 829
  ]
  edge [
    source 13
    target 830
  ]
  edge [
    source 13
    target 831
  ]
  edge [
    source 13
    target 832
  ]
  edge [
    source 13
    target 833
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 834
  ]
  edge [
    source 13
    target 835
  ]
  edge [
    source 13
    target 836
  ]
  edge [
    source 13
    target 837
  ]
  edge [
    source 13
    target 838
  ]
  edge [
    source 13
    target 839
  ]
  edge [
    source 13
    target 840
  ]
  edge [
    source 13
    target 841
  ]
  edge [
    source 13
    target 842
  ]
  edge [
    source 13
    target 843
  ]
  edge [
    source 13
    target 844
  ]
  edge [
    source 13
    target 845
  ]
  edge [
    source 13
    target 846
  ]
  edge [
    source 13
    target 847
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 848
  ]
  edge [
    source 13
    target 849
  ]
  edge [
    source 13
    target 850
  ]
  edge [
    source 13
    target 851
  ]
  edge [
    source 13
    target 852
  ]
  edge [
    source 13
    target 853
  ]
  edge [
    source 13
    target 854
  ]
  edge [
    source 13
    target 855
  ]
  edge [
    source 13
    target 856
  ]
  edge [
    source 13
    target 857
  ]
  edge [
    source 13
    target 858
  ]
  edge [
    source 13
    target 859
  ]
  edge [
    source 13
    target 860
  ]
  edge [
    source 13
    target 861
  ]
  edge [
    source 13
    target 862
  ]
  edge [
    source 13
    target 863
  ]
  edge [
    source 13
    target 864
  ]
  edge [
    source 13
    target 865
  ]
  edge [
    source 13
    target 866
  ]
  edge [
    source 13
    target 867
  ]
  edge [
    source 13
    target 868
  ]
  edge [
    source 13
    target 869
  ]
  edge [
    source 13
    target 870
  ]
  edge [
    source 13
    target 871
  ]
  edge [
    source 13
    target 872
  ]
  edge [
    source 13
    target 873
  ]
  edge [
    source 13
    target 874
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 875
  ]
  edge [
    source 13
    target 876
  ]
  edge [
    source 13
    target 877
  ]
  edge [
    source 13
    target 878
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 879
  ]
  edge [
    source 13
    target 880
  ]
  edge [
    source 13
    target 881
  ]
  edge [
    source 13
    target 882
  ]
  edge [
    source 13
    target 883
  ]
  edge [
    source 13
    target 884
  ]
  edge [
    source 13
    target 885
  ]
  edge [
    source 13
    target 886
  ]
  edge [
    source 13
    target 887
  ]
  edge [
    source 13
    target 888
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 915
  ]
  edge [
    source 15
    target 916
  ]
  edge [
    source 15
    target 348
  ]
  edge [
    source 15
    target 917
  ]
  edge [
    source 15
    target 918
  ]
  edge [
    source 15
    target 919
  ]
  edge [
    source 15
    target 920
  ]
  edge [
    source 15
    target 477
  ]
  edge [
    source 15
    target 921
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 922
  ]
  edge [
    source 15
    target 923
  ]
  edge [
    source 15
    target 924
  ]
  edge [
    source 15
    target 925
  ]
  edge [
    source 15
    target 926
  ]
  edge [
    source 15
    target 927
  ]
  edge [
    source 15
    target 928
  ]
  edge [
    source 15
    target 929
  ]
  edge [
    source 15
    target 538
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 379
  ]
  edge [
    source 15
    target 930
  ]
  edge [
    source 15
    target 931
  ]
  edge [
    source 15
    target 932
  ]
  edge [
    source 15
    target 933
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 934
  ]
  edge [
    source 15
    target 199
  ]
  edge [
    source 15
    target 935
  ]
  edge [
    source 15
    target 936
  ]
  edge [
    source 15
    target 937
  ]
  edge [
    source 15
    target 938
  ]
  edge [
    source 15
    target 939
  ]
  edge [
    source 15
    target 940
  ]
  edge [
    source 15
    target 703
  ]
  edge [
    source 15
    target 941
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 942
  ]
  edge [
    source 15
    target 943
  ]
  edge [
    source 15
    target 944
  ]
  edge [
    source 15
    target 945
  ]
  edge [
    source 15
    target 946
  ]
  edge [
    source 15
    target 947
  ]
  edge [
    source 15
    target 198
  ]
  edge [
    source 15
    target 948
  ]
  edge [
    source 15
    target 949
  ]
  edge [
    source 15
    target 950
  ]
  edge [
    source 15
    target 951
  ]
  edge [
    source 15
    target 952
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 953
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 954
  ]
  edge [
    source 16
    target 955
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 956
  ]
  edge [
    source 16
    target 957
  ]
  edge [
    source 16
    target 958
  ]
  edge [
    source 16
    target 959
  ]
  edge [
    source 16
    target 960
  ]
  edge [
    source 16
    target 961
  ]
  edge [
    source 16
    target 962
  ]
  edge [
    source 16
    target 963
  ]
  edge [
    source 16
    target 964
  ]
  edge [
    source 16
    target 965
  ]
  edge [
    source 16
    target 966
  ]
  edge [
    source 16
    target 967
  ]
  edge [
    source 16
    target 968
  ]
  edge [
    source 16
    target 969
  ]
  edge [
    source 16
    target 970
  ]
  edge [
    source 16
    target 971
  ]
  edge [
    source 16
    target 629
  ]
  edge [
    source 16
    target 972
  ]
  edge [
    source 16
    target 973
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 974
  ]
  edge [
    source 16
    target 975
  ]
  edge [
    source 16
    target 976
  ]
  edge [
    source 16
    target 977
  ]
  edge [
    source 16
    target 978
  ]
  edge [
    source 16
    target 979
  ]
  edge [
    source 16
    target 980
  ]
  edge [
    source 16
    target 981
  ]
  edge [
    source 16
    target 303
  ]
  edge [
    source 16
    target 982
  ]
  edge [
    source 16
    target 983
  ]
  edge [
    source 16
    target 984
  ]
  edge [
    source 16
    target 985
  ]
  edge [
    source 16
    target 304
  ]
  edge [
    source 16
    target 986
  ]
  edge [
    source 16
    target 987
  ]
  edge [
    source 16
    target 988
  ]
  edge [
    source 16
    target 989
  ]
  edge [
    source 16
    target 990
  ]
  edge [
    source 16
    target 991
  ]
  edge [
    source 16
    target 992
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 993
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 994
  ]
  edge [
    source 16
    target 946
  ]
  edge [
    source 16
    target 995
  ]
  edge [
    source 16
    target 996
  ]
  edge [
    source 16
    target 997
  ]
  edge [
    source 16
    target 998
  ]
  edge [
    source 16
    target 223
  ]
  edge [
    source 16
    target 300
  ]
  edge [
    source 16
    target 999
  ]
  edge [
    source 16
    target 1000
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 1001
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 218
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 16
    target 1006
  ]
  edge [
    source 16
    target 1007
  ]
  edge [
    source 16
    target 1008
  ]
  edge [
    source 16
    target 1009
  ]
  edge [
    source 16
    target 1010
  ]
  edge [
    source 16
    target 1011
  ]
  edge [
    source 16
    target 1012
  ]
  edge [
    source 16
    target 1013
  ]
  edge [
    source 16
    target 1014
  ]
  edge [
    source 16
    target 1015
  ]
  edge [
    source 16
    target 1016
  ]
  edge [
    source 16
    target 1017
  ]
  edge [
    source 16
    target 1018
  ]
  edge [
    source 16
    target 1019
  ]
  edge [
    source 16
    target 1020
  ]
  edge [
    source 16
    target 1021
  ]
  edge [
    source 16
    target 1022
  ]
  edge [
    source 16
    target 1023
  ]
  edge [
    source 16
    target 290
  ]
  edge [
    source 16
    target 1024
  ]
  edge [
    source 16
    target 1025
  ]
  edge [
    source 16
    target 1026
  ]
  edge [
    source 16
    target 1027
  ]
  edge [
    source 16
    target 1028
  ]
  edge [
    source 16
    target 1029
  ]
  edge [
    source 16
    target 607
  ]
  edge [
    source 16
    target 1030
  ]
  edge [
    source 16
    target 1031
  ]
  edge [
    source 16
    target 1032
  ]
  edge [
    source 16
    target 1033
  ]
  edge [
    source 16
    target 1034
  ]
  edge [
    source 16
    target 1035
  ]
  edge [
    source 16
    target 1036
  ]
  edge [
    source 16
    target 1037
  ]
  edge [
    source 16
    target 1038
  ]
  edge [
    source 16
    target 1039
  ]
  edge [
    source 16
    target 1040
  ]
  edge [
    source 16
    target 1041
  ]
  edge [
    source 16
    target 1042
  ]
  edge [
    source 16
    target 1043
  ]
  edge [
    source 16
    target 1044
  ]
  edge [
    source 16
    target 1045
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1046
  ]
  edge [
    source 17
    target 1047
  ]
  edge [
    source 17
    target 1048
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 1049
  ]
  edge [
    source 17
    target 1050
  ]
  edge [
    source 17
    target 1051
  ]
  edge [
    source 17
    target 1052
  ]
  edge [
    source 17
    target 1053
  ]
  edge [
    source 17
    target 1054
  ]
  edge [
    source 17
    target 1055
  ]
  edge [
    source 17
    target 405
  ]
  edge [
    source 17
    target 1056
  ]
  edge [
    source 17
    target 1057
  ]
  edge [
    source 17
    target 1058
  ]
  edge [
    source 17
    target 1059
  ]
  edge [
    source 17
    target 1060
  ]
  edge [
    source 17
    target 1061
  ]
  edge [
    source 17
    target 1062
  ]
  edge [
    source 17
    target 1063
  ]
  edge [
    source 17
    target 1064
  ]
  edge [
    source 17
    target 1065
  ]
  edge [
    source 17
    target 1066
  ]
  edge [
    source 17
    target 1067
  ]
  edge [
    source 17
    target 1068
  ]
  edge [
    source 17
    target 1069
  ]
  edge [
    source 17
    target 1070
  ]
  edge [
    source 17
    target 1071
  ]
  edge [
    source 17
    target 1072
  ]
  edge [
    source 17
    target 1073
  ]
  edge [
    source 17
    target 1074
  ]
  edge [
    source 17
    target 1075
  ]
  edge [
    source 17
    target 1076
  ]
  edge [
    source 17
    target 1077
  ]
  edge [
    source 17
    target 1078
  ]
  edge [
    source 17
    target 1079
  ]
  edge [
    source 17
    target 1080
  ]
  edge [
    source 17
    target 1081
  ]
  edge [
    source 17
    target 1082
  ]
  edge [
    source 17
    target 1083
  ]
  edge [
    source 17
    target 1084
  ]
  edge [
    source 17
    target 1085
  ]
  edge [
    source 17
    target 1086
  ]
  edge [
    source 17
    target 1087
  ]
  edge [
    source 17
    target 1088
  ]
  edge [
    source 17
    target 1089
  ]
  edge [
    source 17
    target 1090
  ]
  edge [
    source 17
    target 331
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 1091
  ]
  edge [
    source 17
    target 1092
  ]
  edge [
    source 17
    target 1093
  ]
  edge [
    source 17
    target 240
  ]
  edge [
    source 17
    target 1094
  ]
  edge [
    source 17
    target 1095
  ]
  edge [
    source 17
    target 1096
  ]
  edge [
    source 17
    target 1097
  ]
  edge [
    source 17
    target 1098
  ]
  edge [
    source 17
    target 896
  ]
  edge [
    source 17
    target 1099
  ]
  edge [
    source 17
    target 1100
  ]
  edge [
    source 17
    target 893
  ]
  edge [
    source 17
    target 1101
  ]
  edge [
    source 17
    target 1102
  ]
  edge [
    source 17
    target 1103
  ]
  edge [
    source 17
    target 1104
  ]
  edge [
    source 17
    target 1105
  ]
  edge [
    source 17
    target 1106
  ]
  edge [
    source 17
    target 404
  ]
  edge [
    source 17
    target 1107
  ]
  edge [
    source 17
    target 1108
  ]
  edge [
    source 17
    target 1109
  ]
  edge [
    source 17
    target 1110
  ]
  edge [
    source 17
    target 1111
  ]
  edge [
    source 17
    target 1112
  ]
  edge [
    source 17
    target 1113
  ]
  edge [
    source 17
    target 1114
  ]
  edge [
    source 17
    target 1115
  ]
  edge [
    source 17
    target 1116
  ]
  edge [
    source 17
    target 1117
  ]
  edge [
    source 17
    target 1003
  ]
  edge [
    source 17
    target 1004
  ]
  edge [
    source 17
    target 1005
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 962
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 211
  ]
  edge [
    source 17
    target 1118
  ]
  edge [
    source 17
    target 1119
  ]
  edge [
    source 17
    target 1120
  ]
  edge [
    source 17
    target 1121
  ]
  edge [
    source 17
    target 1122
  ]
  edge [
    source 17
    target 1123
  ]
  edge [
    source 17
    target 1124
  ]
  edge [
    source 17
    target 1125
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 1126
  ]
  edge [
    source 17
    target 1127
  ]
  edge [
    source 17
    target 1128
  ]
  edge [
    source 17
    target 1129
  ]
  edge [
    source 17
    target 936
  ]
  edge [
    source 17
    target 1130
  ]
  edge [
    source 17
    target 1131
  ]
  edge [
    source 17
    target 1132
  ]
  edge [
    source 17
    target 1133
  ]
  edge [
    source 17
    target 1134
  ]
  edge [
    source 17
    target 1135
  ]
  edge [
    source 17
    target 1136
  ]
  edge [
    source 17
    target 952
  ]
  edge [
    source 17
    target 1137
  ]
  edge [
    source 17
    target 348
  ]
  edge [
    source 17
    target 1138
  ]
  edge [
    source 17
    target 1139
  ]
  edge [
    source 17
    target 1140
  ]
  edge [
    source 17
    target 288
  ]
  edge [
    source 17
    target 1141
  ]
  edge [
    source 17
    target 1142
  ]
  edge [
    source 17
    target 1143
  ]
  edge [
    source 17
    target 1144
  ]
  edge [
    source 17
    target 1145
  ]
  edge [
    source 17
    target 1146
  ]
  edge [
    source 17
    target 1147
  ]
  edge [
    source 17
    target 1148
  ]
  edge [
    source 17
    target 1149
  ]
  edge [
    source 17
    target 1150
  ]
  edge [
    source 17
    target 1151
  ]
  edge [
    source 17
    target 1152
  ]
  edge [
    source 17
    target 1153
  ]
  edge [
    source 17
    target 1154
  ]
  edge [
    source 17
    target 1155
  ]
  edge [
    source 17
    target 1156
  ]
  edge [
    source 17
    target 1157
  ]
  edge [
    source 17
    target 1158
  ]
  edge [
    source 17
    target 1159
  ]
  edge [
    source 17
    target 1160
  ]
  edge [
    source 17
    target 1161
  ]
  edge [
    source 17
    target 538
  ]
  edge [
    source 17
    target 537
  ]
  edge [
    source 17
    target 1162
  ]
  edge [
    source 17
    target 499
  ]
  edge [
    source 17
    target 1163
  ]
  edge [
    source 17
    target 1164
  ]
  edge [
    source 17
    target 1165
  ]
  edge [
    source 17
    target 1166
  ]
  edge [
    source 17
    target 800
  ]
  edge [
    source 17
    target 1167
  ]
  edge [
    source 17
    target 1044
  ]
  edge [
    source 17
    target 1168
  ]
  edge [
    source 17
    target 1169
  ]
  edge [
    source 17
    target 1170
  ]
  edge [
    source 17
    target 1171
  ]
  edge [
    source 17
    target 1172
  ]
  edge [
    source 17
    target 1173
  ]
  edge [
    source 17
    target 1174
  ]
  edge [
    source 17
    target 1175
  ]
  edge [
    source 17
    target 1176
  ]
  edge [
    source 17
    target 1177
  ]
  edge [
    source 17
    target 1178
  ]
  edge [
    source 17
    target 1179
  ]
  edge [
    source 17
    target 1180
  ]
  edge [
    source 17
    target 1181
  ]
  edge [
    source 17
    target 1182
  ]
  edge [
    source 17
    target 1183
  ]
  edge [
    source 17
    target 1184
  ]
  edge [
    source 17
    target 1185
  ]
  edge [
    source 17
    target 1186
  ]
  edge [
    source 17
    target 1187
  ]
  edge [
    source 17
    target 1188
  ]
  edge [
    source 17
    target 1189
  ]
  edge [
    source 17
    target 1190
  ]
  edge [
    source 17
    target 1191
  ]
  edge [
    source 17
    target 1192
  ]
  edge [
    source 17
    target 1193
  ]
  edge [
    source 17
    target 1194
  ]
  edge [
    source 17
    target 1195
  ]
  edge [
    source 17
    target 1196
  ]
  edge [
    source 17
    target 1197
  ]
  edge [
    source 17
    target 1198
  ]
  edge [
    source 17
    target 848
  ]
  edge [
    source 17
    target 1199
  ]
  edge [
    source 17
    target 849
  ]
  edge [
    source 17
    target 1200
  ]
  edge [
    source 17
    target 1201
  ]
  edge [
    source 17
    target 1202
  ]
  edge [
    source 17
    target 1203
  ]
  edge [
    source 17
    target 1204
  ]
  edge [
    source 17
    target 1205
  ]
  edge [
    source 17
    target 1206
  ]
  edge [
    source 17
    target 1207
  ]
  edge [
    source 17
    target 1208
  ]
  edge [
    source 17
    target 1209
  ]
  edge [
    source 17
    target 1210
  ]
  edge [
    source 17
    target 1211
  ]
  edge [
    source 17
    target 1212
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 1213
  ]
  edge [
    source 17
    target 1214
  ]
  edge [
    source 17
    target 1215
  ]
  edge [
    source 17
    target 1216
  ]
  edge [
    source 17
    target 1217
  ]
  edge [
    source 17
    target 1218
  ]
  edge [
    source 17
    target 1219
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 1220
  ]
  edge [
    source 17
    target 1221
  ]
  edge [
    source 17
    target 1222
  ]
  edge [
    source 17
    target 1223
  ]
  edge [
    source 17
    target 1224
  ]
  edge [
    source 17
    target 1225
  ]
  edge [
    source 17
    target 1226
  ]
  edge [
    source 17
    target 1227
  ]
  edge [
    source 17
    target 704
  ]
  edge [
    source 17
    target 1228
  ]
  edge [
    source 17
    target 1229
  ]
  edge [
    source 17
    target 1230
  ]
  edge [
    source 18
    target 1231
  ]
  edge [
    source 18
    target 1232
  ]
  edge [
    source 18
    target 1233
  ]
  edge [
    source 18
    target 1234
  ]
  edge [
    source 18
    target 1235
  ]
  edge [
    source 18
    target 1236
  ]
  edge [
    source 18
    target 1237
  ]
  edge [
    source 18
    target 1238
  ]
  edge [
    source 18
    target 1239
  ]
  edge [
    source 18
    target 1240
  ]
  edge [
    source 18
    target 753
  ]
  edge [
    source 18
    target 1241
  ]
  edge [
    source 18
    target 1242
  ]
  edge [
    source 18
    target 1243
  ]
  edge [
    source 18
    target 1244
  ]
  edge [
    source 18
    target 1245
  ]
  edge [
    source 18
    target 1246
  ]
  edge [
    source 18
    target 1247
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 1003
  ]
  edge [
    source 19
    target 1004
  ]
  edge [
    source 19
    target 1005
  ]
  edge [
    source 19
    target 1006
  ]
  edge [
    source 19
    target 1007
  ]
  edge [
    source 19
    target 1008
  ]
  edge [
    source 19
    target 1009
  ]
  edge [
    source 19
    target 1010
  ]
  edge [
    source 19
    target 1011
  ]
  edge [
    source 19
    target 1013
  ]
  edge [
    source 19
    target 1012
  ]
  edge [
    source 19
    target 1014
  ]
  edge [
    source 19
    target 1015
  ]
  edge [
    source 19
    target 1016
  ]
  edge [
    source 19
    target 962
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 999
  ]
  edge [
    source 19
    target 1000
  ]
  edge [
    source 19
    target 135
  ]
  edge [
    source 19
    target 1001
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 1002
  ]
  edge [
    source 19
    target 1248
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 1249
  ]
  edge [
    source 19
    target 350
  ]
  edge [
    source 19
    target 1250
  ]
  edge [
    source 19
    target 280
  ]
  edge [
    source 19
    target 75
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 354
  ]
  edge [
    source 19
    target 371
  ]
  edge [
    source 19
    target 361
  ]
  edge [
    source 19
    target 1251
  ]
  edge [
    source 19
    target 1252
  ]
  edge [
    source 19
    target 1253
  ]
  edge [
    source 19
    target 1254
  ]
  edge [
    source 19
    target 1255
  ]
  edge [
    source 19
    target 1256
  ]
  edge [
    source 19
    target 1257
  ]
  edge [
    source 19
    target 1258
  ]
  edge [
    source 19
    target 240
  ]
  edge [
    source 19
    target 1259
  ]
  edge [
    source 19
    target 1260
  ]
  edge [
    source 19
    target 1261
  ]
  edge [
    source 19
    target 1262
  ]
  edge [
    source 19
    target 1263
  ]
  edge [
    source 19
    target 1264
  ]
  edge [
    source 19
    target 492
  ]
  edge [
    source 19
    target 1265
  ]
  edge [
    source 19
    target 1266
  ]
  edge [
    source 19
    target 308
  ]
  edge [
    source 19
    target 1267
  ]
  edge [
    source 19
    target 1268
  ]
  edge [
    source 19
    target 435
  ]
  edge [
    source 19
    target 1269
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 1270
  ]
  edge [
    source 19
    target 1271
  ]
  edge [
    source 19
    target 1272
  ]
  edge [
    source 19
    target 1273
  ]
  edge [
    source 19
    target 1274
  ]
  edge [
    source 19
    target 1275
  ]
  edge [
    source 19
    target 1276
  ]
  edge [
    source 19
    target 1277
  ]
  edge [
    source 19
    target 1278
  ]
  edge [
    source 19
    target 1279
  ]
  edge [
    source 19
    target 300
  ]
  edge [
    source 19
    target 1280
  ]
  edge [
    source 19
    target 1281
  ]
  edge [
    source 19
    target 332
  ]
  edge [
    source 19
    target 1282
  ]
  edge [
    source 19
    target 1283
  ]
  edge [
    source 19
    target 340
  ]
  edge [
    source 19
    target 1284
  ]
  edge [
    source 19
    target 347
  ]
  edge [
    source 19
    target 348
  ]
  edge [
    source 19
    target 1285
  ]
  edge [
    source 19
    target 1286
  ]
  edge [
    source 19
    target 1287
  ]
  edge [
    source 19
    target 1288
  ]
  edge [
    source 19
    target 1024
  ]
  edge [
    source 19
    target 1289
  ]
  edge [
    source 19
    target 1290
  ]
  edge [
    source 19
    target 1291
  ]
  edge [
    source 19
    target 1292
  ]
  edge [
    source 19
    target 1293
  ]
  edge [
    source 19
    target 1294
  ]
  edge [
    source 19
    target 1295
  ]
  edge [
    source 19
    target 965
  ]
  edge [
    source 19
    target 1296
  ]
  edge [
    source 19
    target 1297
  ]
  edge [
    source 19
    target 1298
  ]
  edge [
    source 19
    target 1299
  ]
  edge [
    source 19
    target 1300
  ]
  edge [
    source 19
    target 1301
  ]
  edge [
    source 19
    target 1302
  ]
  edge [
    source 19
    target 1303
  ]
  edge [
    source 19
    target 1304
  ]
  edge [
    source 19
    target 1305
  ]
  edge [
    source 19
    target 1306
  ]
  edge [
    source 19
    target 1307
  ]
  edge [
    source 19
    target 1308
  ]
  edge [
    source 19
    target 1309
  ]
  edge [
    source 19
    target 1310
  ]
  edge [
    source 19
    target 1311
  ]
  edge [
    source 19
    target 1312
  ]
  edge [
    source 19
    target 1313
  ]
  edge [
    source 19
    target 1314
  ]
  edge [
    source 19
    target 1315
  ]
  edge [
    source 19
    target 1316
  ]
  edge [
    source 19
    target 1317
  ]
  edge [
    source 20
    target 1318
  ]
  edge [
    source 20
    target 1319
  ]
  edge [
    source 20
    target 771
  ]
  edge [
    source 20
    target 1320
  ]
  edge [
    source 20
    target 1321
  ]
  edge [
    source 20
    target 1322
  ]
  edge [
    source 20
    target 1323
  ]
  edge [
    source 20
    target 1324
  ]
  edge [
    source 20
    target 1325
  ]
  edge [
    source 20
    target 1326
  ]
  edge [
    source 20
    target 488
  ]
  edge [
    source 20
    target 1327
  ]
  edge [
    source 20
    target 794
  ]
  edge [
    source 20
    target 1328
  ]
  edge [
    source 20
    target 1329
  ]
  edge [
    source 20
    target 1330
  ]
  edge [
    source 20
    target 1331
  ]
  edge [
    source 20
    target 1332
  ]
  edge [
    source 20
    target 1333
  ]
  edge [
    source 20
    target 1334
  ]
  edge [
    source 20
    target 1335
  ]
  edge [
    source 20
    target 1336
  ]
  edge [
    source 20
    target 760
  ]
  edge [
    source 20
    target 764
  ]
  edge [
    source 20
    target 1337
  ]
  edge [
    source 1338
    target 1339
  ]
]
