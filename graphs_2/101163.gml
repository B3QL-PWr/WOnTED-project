graph [
  node [
    id 0
    label "kania"
    origin "text"
  ]
  node [
    id 1
    label "stacja"
    origin "text"
  ]
  node [
    id 2
    label "ptak_drapie&#380;ny"
  ]
  node [
    id 3
    label "kite"
  ]
  node [
    id 4
    label "czubajka"
  ]
  node [
    id 5
    label "myszo&#322;owy"
  ]
  node [
    id 6
    label "saprotrof"
  ]
  node [
    id 7
    label "stroszka_strzelista"
  ]
  node [
    id 8
    label "saprofit"
  ]
  node [
    id 9
    label "pieczarkowiec"
  ]
  node [
    id 10
    label "czernid&#322;ak_ko&#322;pakowaty"
  ]
  node [
    id 11
    label "pieczarkowate"
  ]
  node [
    id 12
    label "grzyb_jadalny"
  ]
  node [
    id 13
    label "stroszka"
  ]
  node [
    id 14
    label "jastrz&#281;biowate"
  ]
  node [
    id 15
    label "punkt"
  ]
  node [
    id 16
    label "siedziba"
  ]
  node [
    id 17
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 18
    label "miejsce"
  ]
  node [
    id 19
    label "instytucja"
  ]
  node [
    id 20
    label "urz&#261;dzenie"
  ]
  node [
    id 21
    label "droga_krzy&#380;owa"
  ]
  node [
    id 22
    label "komora"
  ]
  node [
    id 23
    label "wyrz&#261;dzenie"
  ]
  node [
    id 24
    label "kom&#243;rka"
  ]
  node [
    id 25
    label "impulsator"
  ]
  node [
    id 26
    label "przygotowanie"
  ]
  node [
    id 27
    label "furnishing"
  ]
  node [
    id 28
    label "zabezpieczenie"
  ]
  node [
    id 29
    label "sprz&#281;t"
  ]
  node [
    id 30
    label "aparatura"
  ]
  node [
    id 31
    label "ig&#322;a"
  ]
  node [
    id 32
    label "wirnik"
  ]
  node [
    id 33
    label "przedmiot"
  ]
  node [
    id 34
    label "zablokowanie"
  ]
  node [
    id 35
    label "blokowanie"
  ]
  node [
    id 36
    label "j&#281;zyk"
  ]
  node [
    id 37
    label "czynno&#347;&#263;"
  ]
  node [
    id 38
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 39
    label "system_energetyczny"
  ]
  node [
    id 40
    label "narz&#281;dzie"
  ]
  node [
    id 41
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 42
    label "set"
  ]
  node [
    id 43
    label "zrobienie"
  ]
  node [
    id 44
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 45
    label "zagospodarowanie"
  ]
  node [
    id 46
    label "mechanizm"
  ]
  node [
    id 47
    label "miejsce_pracy"
  ]
  node [
    id 48
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 49
    label "budynek"
  ]
  node [
    id 50
    label "&#321;ubianka"
  ]
  node [
    id 51
    label "Bia&#322;y_Dom"
  ]
  node [
    id 52
    label "dzia&#322;_personalny"
  ]
  node [
    id 53
    label "Kreml"
  ]
  node [
    id 54
    label "sadowisko"
  ]
  node [
    id 55
    label "obiekt_matematyczny"
  ]
  node [
    id 56
    label "stopie&#324;_pisma"
  ]
  node [
    id 57
    label "pozycja"
  ]
  node [
    id 58
    label "problemat"
  ]
  node [
    id 59
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 60
    label "obiekt"
  ]
  node [
    id 61
    label "point"
  ]
  node [
    id 62
    label "plamka"
  ]
  node [
    id 63
    label "przestrze&#324;"
  ]
  node [
    id 64
    label "mark"
  ]
  node [
    id 65
    label "ust&#281;p"
  ]
  node [
    id 66
    label "po&#322;o&#380;enie"
  ]
  node [
    id 67
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 68
    label "kres"
  ]
  node [
    id 69
    label "plan"
  ]
  node [
    id 70
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 71
    label "chwila"
  ]
  node [
    id 72
    label "podpunkt"
  ]
  node [
    id 73
    label "jednostka"
  ]
  node [
    id 74
    label "sprawa"
  ]
  node [
    id 75
    label "problematyka"
  ]
  node [
    id 76
    label "prosta"
  ]
  node [
    id 77
    label "wojsko"
  ]
  node [
    id 78
    label "zapunktowa&#263;"
  ]
  node [
    id 79
    label "rz&#261;d"
  ]
  node [
    id 80
    label "uwaga"
  ]
  node [
    id 81
    label "cecha"
  ]
  node [
    id 82
    label "praca"
  ]
  node [
    id 83
    label "plac"
  ]
  node [
    id 84
    label "location"
  ]
  node [
    id 85
    label "warunek_lokalowy"
  ]
  node [
    id 86
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 87
    label "cia&#322;o"
  ]
  node [
    id 88
    label "status"
  ]
  node [
    id 89
    label "poj&#281;cie"
  ]
  node [
    id 90
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 91
    label "afiliowa&#263;"
  ]
  node [
    id 92
    label "establishment"
  ]
  node [
    id 93
    label "zamyka&#263;"
  ]
  node [
    id 94
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 95
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 96
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 97
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 98
    label "standard"
  ]
  node [
    id 99
    label "Fundusze_Unijne"
  ]
  node [
    id 100
    label "biuro"
  ]
  node [
    id 101
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 102
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 103
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 104
    label "zamykanie"
  ]
  node [
    id 105
    label "organizacja"
  ]
  node [
    id 106
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 107
    label "osoba_prawna"
  ]
  node [
    id 108
    label "urz&#261;d"
  ]
  node [
    id 109
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 110
    label "rejowiec"
  ]
  node [
    id 111
    label "fabryczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 110
    target 111
  ]
]
