graph [
  node [
    id 0
    label "tata"
    origin "text"
  ]
  node [
    id 1
    label "szepta&#263;"
    origin "text"
  ]
  node [
    id 2
    label "prosto"
    origin "text"
  ]
  node [
    id 3
    label "ucha"
    origin "text"
  ]
  node [
    id 4
    label "wstawa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "sp&#243;&#378;ni&#263;"
    origin "text"
  ]
  node [
    id 6
    label "przodek"
  ]
  node [
    id 7
    label "rodzic"
  ]
  node [
    id 8
    label "rodzice"
  ]
  node [
    id 9
    label "ojciec"
  ]
  node [
    id 10
    label "kuwada"
  ]
  node [
    id 11
    label "ojczym"
  ]
  node [
    id 12
    label "papa"
  ]
  node [
    id 13
    label "stary"
  ]
  node [
    id 14
    label "wapniaki"
  ]
  node [
    id 15
    label "starzy"
  ]
  node [
    id 16
    label "pokolenie"
  ]
  node [
    id 17
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 18
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 19
    label "&#347;w"
  ]
  node [
    id 20
    label "pomys&#322;odawca"
  ]
  node [
    id 21
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 22
    label "zakonnik"
  ]
  node [
    id 23
    label "samiec"
  ]
  node [
    id 24
    label "wykonawca"
  ]
  node [
    id 25
    label "kszta&#322;ciciel"
  ]
  node [
    id 26
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 27
    label "tworzyciel"
  ]
  node [
    id 28
    label "wyrobisko"
  ]
  node [
    id 29
    label "krewny"
  ]
  node [
    id 30
    label "ojcowie"
  ]
  node [
    id 31
    label "post&#281;p"
  ]
  node [
    id 32
    label "w&#243;z"
  ]
  node [
    id 33
    label "antecesor"
  ]
  node [
    id 34
    label "p&#322;ug"
  ]
  node [
    id 35
    label "dziad"
  ]
  node [
    id 36
    label "chodnik"
  ]
  node [
    id 37
    label "linea&#380;"
  ]
  node [
    id 38
    label "opiekun"
  ]
  node [
    id 39
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 40
    label "wapniak"
  ]
  node [
    id 41
    label "rodzic_chrzestny"
  ]
  node [
    id 42
    label "starzenie_si&#281;"
  ]
  node [
    id 43
    label "zwierzchnik"
  ]
  node [
    id 44
    label "charakterystyczny"
  ]
  node [
    id 45
    label "starczo"
  ]
  node [
    id 46
    label "p&#243;&#378;ny"
  ]
  node [
    id 47
    label "zestarzenie_si&#281;"
  ]
  node [
    id 48
    label "dawniej"
  ]
  node [
    id 49
    label "dojrza&#322;y"
  ]
  node [
    id 50
    label "brat"
  ]
  node [
    id 51
    label "m&#261;&#380;"
  ]
  node [
    id 52
    label "niegdysiejszy"
  ]
  node [
    id 53
    label "d&#322;ugoletni"
  ]
  node [
    id 54
    label "poprzedni"
  ]
  node [
    id 55
    label "gruba_ryba"
  ]
  node [
    id 56
    label "po_staro&#347;wiecku"
  ]
  node [
    id 57
    label "staro"
  ]
  node [
    id 58
    label "nienowoczesny"
  ]
  node [
    id 59
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 60
    label "odleg&#322;y"
  ]
  node [
    id 61
    label "dawno"
  ]
  node [
    id 62
    label "dotychczasowy"
  ]
  node [
    id 63
    label "znajomy"
  ]
  node [
    id 64
    label "izolacja"
  ]
  node [
    id 65
    label "gun_muzzle"
  ]
  node [
    id 66
    label "materia&#322;_budowlany"
  ]
  node [
    id 67
    label "twarz"
  ]
  node [
    id 68
    label "&#380;onaty"
  ]
  node [
    id 69
    label "syndrom_kuwady"
  ]
  node [
    id 70
    label "ci&#261;&#380;a"
  ]
  node [
    id 71
    label "zwyczaj"
  ]
  node [
    id 72
    label "na&#347;ladownictwo"
  ]
  node [
    id 73
    label "plotkowa&#263;"
  ]
  node [
    id 74
    label "m&#243;wi&#263;"
  ]
  node [
    id 75
    label "breathe"
  ]
  node [
    id 76
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 77
    label "dysfonia"
  ]
  node [
    id 78
    label "prawi&#263;"
  ]
  node [
    id 79
    label "remark"
  ]
  node [
    id 80
    label "express"
  ]
  node [
    id 81
    label "chew_the_fat"
  ]
  node [
    id 82
    label "talk"
  ]
  node [
    id 83
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 84
    label "say"
  ]
  node [
    id 85
    label "wyra&#380;a&#263;"
  ]
  node [
    id 86
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 87
    label "j&#281;zyk"
  ]
  node [
    id 88
    label "tell"
  ]
  node [
    id 89
    label "informowa&#263;"
  ]
  node [
    id 90
    label "rozmawia&#263;"
  ]
  node [
    id 91
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 92
    label "powiada&#263;"
  ]
  node [
    id 93
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 94
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 95
    label "okre&#347;la&#263;"
  ]
  node [
    id 96
    label "u&#380;ywa&#263;"
  ]
  node [
    id 97
    label "gaworzy&#263;"
  ]
  node [
    id 98
    label "formu&#322;owa&#263;"
  ]
  node [
    id 99
    label "dziama&#263;"
  ]
  node [
    id 100
    label "umie&#263;"
  ]
  node [
    id 101
    label "wydobywa&#263;"
  ]
  node [
    id 102
    label "prosty"
  ]
  node [
    id 103
    label "bezpo&#347;rednio"
  ]
  node [
    id 104
    label "niepozornie"
  ]
  node [
    id 105
    label "skromnie"
  ]
  node [
    id 106
    label "elementarily"
  ]
  node [
    id 107
    label "naturalnie"
  ]
  node [
    id 108
    label "&#322;atwo"
  ]
  node [
    id 109
    label "szczerze"
  ]
  node [
    id 110
    label "blisko"
  ]
  node [
    id 111
    label "bezpo&#347;redni"
  ]
  node [
    id 112
    label "skromno"
  ]
  node [
    id 113
    label "ma&#322;o"
  ]
  node [
    id 114
    label "skromny"
  ]
  node [
    id 115
    label "niewymy&#347;lnie"
  ]
  node [
    id 116
    label "grzecznie"
  ]
  node [
    id 117
    label "zwyczajnie"
  ]
  node [
    id 118
    label "niepozorny"
  ]
  node [
    id 119
    label "snadnie"
  ]
  node [
    id 120
    label "szybko"
  ]
  node [
    id 121
    label "&#322;atwy"
  ]
  node [
    id 122
    label "&#322;atwie"
  ]
  node [
    id 123
    label "przyjemnie"
  ]
  node [
    id 124
    label "&#322;acno"
  ]
  node [
    id 125
    label "immanentnie"
  ]
  node [
    id 126
    label "podobnie"
  ]
  node [
    id 127
    label "naturalny"
  ]
  node [
    id 128
    label "bezspornie"
  ]
  node [
    id 129
    label "po_prostu"
  ]
  node [
    id 130
    label "naiwny"
  ]
  node [
    id 131
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 132
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 133
    label "prostowanie"
  ]
  node [
    id 134
    label "zwyk&#322;y"
  ]
  node [
    id 135
    label "prostoduszny"
  ]
  node [
    id 136
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 137
    label "prostowanie_si&#281;"
  ]
  node [
    id 138
    label "rozprostowanie"
  ]
  node [
    id 139
    label "cios"
  ]
  node [
    id 140
    label "zupa_rybna"
  ]
  node [
    id 141
    label "rybny"
  ]
  node [
    id 142
    label "rybio"
  ]
  node [
    id 143
    label "obfituj&#261;cy"
  ]
  node [
    id 144
    label "sklep"
  ]
  node [
    id 145
    label "swoisty"
  ]
  node [
    id 146
    label "mi&#281;sny"
  ]
  node [
    id 147
    label "zdrowie&#263;"
  ]
  node [
    id 148
    label "heighten"
  ]
  node [
    id 149
    label "przestawa&#263;"
  ]
  node [
    id 150
    label "arise"
  ]
  node [
    id 151
    label "opuszcza&#263;"
  ]
  node [
    id 152
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 153
    label "rise"
  ]
  node [
    id 154
    label "stawa&#263;"
  ]
  node [
    id 155
    label "wschodzi&#263;"
  ]
  node [
    id 156
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 157
    label "&#380;y&#263;"
  ]
  node [
    id 158
    label "coating"
  ]
  node [
    id 159
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 160
    label "ko&#324;czy&#263;"
  ]
  node [
    id 161
    label "finish_up"
  ]
  node [
    id 162
    label "przebywa&#263;"
  ]
  node [
    id 163
    label "determine"
  ]
  node [
    id 164
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 165
    label "abort"
  ]
  node [
    id 166
    label "pozostawia&#263;"
  ]
  node [
    id 167
    label "traci&#263;"
  ]
  node [
    id 168
    label "obni&#380;a&#263;"
  ]
  node [
    id 169
    label "give"
  ]
  node [
    id 170
    label "omija&#263;"
  ]
  node [
    id 171
    label "forowa&#263;_si&#281;"
  ]
  node [
    id 172
    label "potania&#263;"
  ]
  node [
    id 173
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 174
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 175
    label "recuperate"
  ]
  node [
    id 176
    label "wylizywa&#263;_si&#281;"
  ]
  node [
    id 177
    label "wystarcza&#263;"
  ]
  node [
    id 178
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 179
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 180
    label "zostawa&#263;"
  ]
  node [
    id 181
    label "zatrzymywa&#263;_si&#281;"
  ]
  node [
    id 182
    label "by&#263;"
  ]
  node [
    id 183
    label "stop"
  ]
  node [
    id 184
    label "pull"
  ]
  node [
    id 185
    label "przybywa&#263;"
  ]
  node [
    id 186
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 187
    label "schodzi&#263;"
  ]
  node [
    id 188
    label "wzrasta&#263;"
  ]
  node [
    id 189
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 190
    label "wy&#322;ania&#263;_si&#281;"
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
]
