graph [
  node [
    id 0
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "bajka"
    origin "text"
  ]
  node [
    id 2
    label "edda"
    origin "text"
  ]
  node [
    id 3
    label "echo"
  ]
  node [
    id 4
    label "pilnowa&#263;"
  ]
  node [
    id 5
    label "robi&#263;"
  ]
  node [
    id 6
    label "recall"
  ]
  node [
    id 7
    label "si&#281;ga&#263;"
  ]
  node [
    id 8
    label "take_care"
  ]
  node [
    id 9
    label "troska&#263;_si&#281;"
  ]
  node [
    id 10
    label "chowa&#263;"
  ]
  node [
    id 11
    label "zachowywa&#263;"
  ]
  node [
    id 12
    label "zna&#263;"
  ]
  node [
    id 13
    label "think"
  ]
  node [
    id 14
    label "report"
  ]
  node [
    id 15
    label "hide"
  ]
  node [
    id 16
    label "znosi&#263;"
  ]
  node [
    id 17
    label "czu&#263;"
  ]
  node [
    id 18
    label "train"
  ]
  node [
    id 19
    label "przetrzymywa&#263;"
  ]
  node [
    id 20
    label "hodowa&#263;"
  ]
  node [
    id 21
    label "meliniarz"
  ]
  node [
    id 22
    label "umieszcza&#263;"
  ]
  node [
    id 23
    label "ukrywa&#263;"
  ]
  node [
    id 24
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 25
    label "continue"
  ]
  node [
    id 26
    label "wk&#322;ada&#263;"
  ]
  node [
    id 27
    label "tajemnica"
  ]
  node [
    id 28
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 29
    label "zdyscyplinowanie"
  ]
  node [
    id 30
    label "podtrzymywa&#263;"
  ]
  node [
    id 31
    label "post"
  ]
  node [
    id 32
    label "control"
  ]
  node [
    id 33
    label "przechowywa&#263;"
  ]
  node [
    id 34
    label "behave"
  ]
  node [
    id 35
    label "dieta"
  ]
  node [
    id 36
    label "hold"
  ]
  node [
    id 37
    label "post&#281;powa&#263;"
  ]
  node [
    id 38
    label "compass"
  ]
  node [
    id 39
    label "korzysta&#263;"
  ]
  node [
    id 40
    label "appreciation"
  ]
  node [
    id 41
    label "osi&#261;ga&#263;"
  ]
  node [
    id 42
    label "dociera&#263;"
  ]
  node [
    id 43
    label "get"
  ]
  node [
    id 44
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 45
    label "mierzy&#263;"
  ]
  node [
    id 46
    label "u&#380;ywa&#263;"
  ]
  node [
    id 47
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 48
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 49
    label "exsert"
  ]
  node [
    id 50
    label "organizowa&#263;"
  ]
  node [
    id 51
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 52
    label "czyni&#263;"
  ]
  node [
    id 53
    label "give"
  ]
  node [
    id 54
    label "stylizowa&#263;"
  ]
  node [
    id 55
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 56
    label "falowa&#263;"
  ]
  node [
    id 57
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 58
    label "peddle"
  ]
  node [
    id 59
    label "praca"
  ]
  node [
    id 60
    label "wydala&#263;"
  ]
  node [
    id 61
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 62
    label "tentegowa&#263;"
  ]
  node [
    id 63
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 64
    label "urz&#261;dza&#263;"
  ]
  node [
    id 65
    label "oszukiwa&#263;"
  ]
  node [
    id 66
    label "work"
  ]
  node [
    id 67
    label "ukazywa&#263;"
  ]
  node [
    id 68
    label "przerabia&#263;"
  ]
  node [
    id 69
    label "act"
  ]
  node [
    id 70
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 71
    label "cognizance"
  ]
  node [
    id 72
    label "wiedzie&#263;"
  ]
  node [
    id 73
    label "resonance"
  ]
  node [
    id 74
    label "zjawisko"
  ]
  node [
    id 75
    label "g&#322;upstwo"
  ]
  node [
    id 76
    label "narrative"
  ]
  node [
    id 77
    label "komfort"
  ]
  node [
    id 78
    label "film"
  ]
  node [
    id 79
    label "mora&#322;"
  ]
  node [
    id 80
    label "apolog"
  ]
  node [
    id 81
    label "utw&#243;r"
  ]
  node [
    id 82
    label "epika"
  ]
  node [
    id 83
    label "morfing"
  ]
  node [
    id 84
    label "Pok&#233;mon"
  ]
  node [
    id 85
    label "sytuacja"
  ]
  node [
    id 86
    label "opowie&#347;&#263;"
  ]
  node [
    id 87
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 88
    label "warunki"
  ]
  node [
    id 89
    label "szczeg&#243;&#322;"
  ]
  node [
    id 90
    label "state"
  ]
  node [
    id 91
    label "motyw"
  ]
  node [
    id 92
    label "realia"
  ]
  node [
    id 93
    label "animatronika"
  ]
  node [
    id 94
    label "odczulenie"
  ]
  node [
    id 95
    label "odczula&#263;"
  ]
  node [
    id 96
    label "blik"
  ]
  node [
    id 97
    label "odczuli&#263;"
  ]
  node [
    id 98
    label "scena"
  ]
  node [
    id 99
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 100
    label "muza"
  ]
  node [
    id 101
    label "postprodukcja"
  ]
  node [
    id 102
    label "block"
  ]
  node [
    id 103
    label "trawiarnia"
  ]
  node [
    id 104
    label "sklejarka"
  ]
  node [
    id 105
    label "sztuka"
  ]
  node [
    id 106
    label "uj&#281;cie"
  ]
  node [
    id 107
    label "filmoteka"
  ]
  node [
    id 108
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 109
    label "klatka"
  ]
  node [
    id 110
    label "rozbieg&#243;wka"
  ]
  node [
    id 111
    label "napisy"
  ]
  node [
    id 112
    label "ta&#347;ma"
  ]
  node [
    id 113
    label "odczulanie"
  ]
  node [
    id 114
    label "anamorfoza"
  ]
  node [
    id 115
    label "dorobek"
  ]
  node [
    id 116
    label "ty&#322;&#243;wka"
  ]
  node [
    id 117
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 118
    label "b&#322;ona"
  ]
  node [
    id 119
    label "emulsja_fotograficzna"
  ]
  node [
    id 120
    label "photograph"
  ]
  node [
    id 121
    label "czo&#322;&#243;wka"
  ]
  node [
    id 122
    label "rola"
  ]
  node [
    id 123
    label "obrazowanie"
  ]
  node [
    id 124
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 125
    label "organ"
  ]
  node [
    id 126
    label "tre&#347;&#263;"
  ]
  node [
    id 127
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 128
    label "part"
  ]
  node [
    id 129
    label "element_anatomiczny"
  ]
  node [
    id 130
    label "tekst"
  ]
  node [
    id 131
    label "komunikat"
  ]
  node [
    id 132
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 133
    label "wypowied&#378;"
  ]
  node [
    id 134
    label "opowiadanie"
  ]
  node [
    id 135
    label "fabu&#322;a"
  ]
  node [
    id 136
    label "farmazon"
  ]
  node [
    id 137
    label "banalny"
  ]
  node [
    id 138
    label "sofcik"
  ]
  node [
    id 139
    label "czyn"
  ]
  node [
    id 140
    label "baj&#281;da"
  ]
  node [
    id 141
    label "nonsense"
  ]
  node [
    id 142
    label "g&#322;upota"
  ]
  node [
    id 143
    label "g&#243;wno"
  ]
  node [
    id 144
    label "stupidity"
  ]
  node [
    id 145
    label "furda"
  ]
  node [
    id 146
    label "ease"
  ]
  node [
    id 147
    label "emocja"
  ]
  node [
    id 148
    label "zaleta"
  ]
  node [
    id 149
    label "moral"
  ]
  node [
    id 150
    label "nauka"
  ]
  node [
    id 151
    label "przypowie&#347;&#263;"
  ]
  node [
    id 152
    label "wniosek"
  ]
  node [
    id 153
    label "animacja"
  ]
  node [
    id 154
    label "przeobra&#380;anie"
  ]
  node [
    id 155
    label "rodzaj_literacki"
  ]
  node [
    id 156
    label "epos"
  ]
  node [
    id 157
    label "fantastyka"
  ]
  node [
    id 158
    label "romans"
  ]
  node [
    id 159
    label "nowelistyka"
  ]
  node [
    id 160
    label "literatura"
  ]
  node [
    id 161
    label "utw&#243;r_epicki"
  ]
  node [
    id 162
    label "Kambod&#380;a"
  ]
  node [
    id 163
    label "deska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 162
    target 163
  ]
]
