graph [
  node [
    id 0
    label "listopad"
    origin "text"
  ]
  node [
    id 1
    label "centrum"
    origin "text"
  ]
  node [
    id 2
    label "radom"
    origin "text"
  ]
  node [
    id 3
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 6
    label "rze&#378;ba"
    origin "text"
  ]
  node [
    id 7
    label "br&#261;z"
    origin "text"
  ]
  node [
    id 8
    label "miesi&#261;c"
  ]
  node [
    id 9
    label "tydzie&#324;"
  ]
  node [
    id 10
    label "miech"
  ]
  node [
    id 11
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "rok"
  ]
  node [
    id 14
    label "kalendy"
  ]
  node [
    id 15
    label "blok"
  ]
  node [
    id 16
    label "punkt"
  ]
  node [
    id 17
    label "Hollywood"
  ]
  node [
    id 18
    label "centrolew"
  ]
  node [
    id 19
    label "miejsce"
  ]
  node [
    id 20
    label "sejm"
  ]
  node [
    id 21
    label "o&#347;rodek"
  ]
  node [
    id 22
    label "centroprawica"
  ]
  node [
    id 23
    label "core"
  ]
  node [
    id 24
    label "&#347;rodek"
  ]
  node [
    id 25
    label "skupisko"
  ]
  node [
    id 26
    label "zal&#261;&#380;ek"
  ]
  node [
    id 27
    label "instytucja"
  ]
  node [
    id 28
    label "otoczenie"
  ]
  node [
    id 29
    label "warunki"
  ]
  node [
    id 30
    label "center"
  ]
  node [
    id 31
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 32
    label "bajt"
  ]
  node [
    id 33
    label "bloking"
  ]
  node [
    id 34
    label "j&#261;kanie"
  ]
  node [
    id 35
    label "przeszkoda"
  ]
  node [
    id 36
    label "zesp&#243;&#322;"
  ]
  node [
    id 37
    label "blokada"
  ]
  node [
    id 38
    label "bry&#322;a"
  ]
  node [
    id 39
    label "dzia&#322;"
  ]
  node [
    id 40
    label "kontynent"
  ]
  node [
    id 41
    label "nastawnia"
  ]
  node [
    id 42
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 43
    label "blockage"
  ]
  node [
    id 44
    label "zbi&#243;r"
  ]
  node [
    id 45
    label "block"
  ]
  node [
    id 46
    label "organizacja"
  ]
  node [
    id 47
    label "budynek"
  ]
  node [
    id 48
    label "start"
  ]
  node [
    id 49
    label "skorupa_ziemska"
  ]
  node [
    id 50
    label "program"
  ]
  node [
    id 51
    label "zeszyt"
  ]
  node [
    id 52
    label "grupa"
  ]
  node [
    id 53
    label "blokowisko"
  ]
  node [
    id 54
    label "artyku&#322;"
  ]
  node [
    id 55
    label "barak"
  ]
  node [
    id 56
    label "stok_kontynentalny"
  ]
  node [
    id 57
    label "whole"
  ]
  node [
    id 58
    label "square"
  ]
  node [
    id 59
    label "siatk&#243;wka"
  ]
  node [
    id 60
    label "kr&#261;g"
  ]
  node [
    id 61
    label "ram&#243;wka"
  ]
  node [
    id 62
    label "zamek"
  ]
  node [
    id 63
    label "obrona"
  ]
  node [
    id 64
    label "ok&#322;adka"
  ]
  node [
    id 65
    label "bie&#380;nia"
  ]
  node [
    id 66
    label "referat"
  ]
  node [
    id 67
    label "dom_wielorodzinny"
  ]
  node [
    id 68
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 69
    label "po&#322;o&#380;enie"
  ]
  node [
    id 70
    label "sprawa"
  ]
  node [
    id 71
    label "ust&#281;p"
  ]
  node [
    id 72
    label "plan"
  ]
  node [
    id 73
    label "obiekt_matematyczny"
  ]
  node [
    id 74
    label "problemat"
  ]
  node [
    id 75
    label "plamka"
  ]
  node [
    id 76
    label "stopie&#324;_pisma"
  ]
  node [
    id 77
    label "jednostka"
  ]
  node [
    id 78
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 79
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 80
    label "mark"
  ]
  node [
    id 81
    label "chwila"
  ]
  node [
    id 82
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 83
    label "prosta"
  ]
  node [
    id 84
    label "problematyka"
  ]
  node [
    id 85
    label "obiekt"
  ]
  node [
    id 86
    label "zapunktowa&#263;"
  ]
  node [
    id 87
    label "podpunkt"
  ]
  node [
    id 88
    label "wojsko"
  ]
  node [
    id 89
    label "kres"
  ]
  node [
    id 90
    label "przestrze&#324;"
  ]
  node [
    id 91
    label "point"
  ]
  node [
    id 92
    label "pozycja"
  ]
  node [
    id 93
    label "warunek_lokalowy"
  ]
  node [
    id 94
    label "plac"
  ]
  node [
    id 95
    label "location"
  ]
  node [
    id 96
    label "uwaga"
  ]
  node [
    id 97
    label "status"
  ]
  node [
    id 98
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 99
    label "cia&#322;o"
  ]
  node [
    id 100
    label "cecha"
  ]
  node [
    id 101
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 102
    label "praca"
  ]
  node [
    id 103
    label "rz&#261;d"
  ]
  node [
    id 104
    label "koalicja"
  ]
  node [
    id 105
    label "parlament"
  ]
  node [
    id 106
    label "izba_ni&#380;sza"
  ]
  node [
    id 107
    label "lewica"
  ]
  node [
    id 108
    label "siedziba"
  ]
  node [
    id 109
    label "parliament"
  ]
  node [
    id 110
    label "obrady"
  ]
  node [
    id 111
    label "prawica"
  ]
  node [
    id 112
    label "zgromadzenie"
  ]
  node [
    id 113
    label "Los_Angeles"
  ]
  node [
    id 114
    label "dziewczynka"
  ]
  node [
    id 115
    label "dziewczyna"
  ]
  node [
    id 116
    label "prostytutka"
  ]
  node [
    id 117
    label "dziecko"
  ]
  node [
    id 118
    label "cz&#322;owiek"
  ]
  node [
    id 119
    label "potomkini"
  ]
  node [
    id 120
    label "dziewka"
  ]
  node [
    id 121
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 122
    label "sikorka"
  ]
  node [
    id 123
    label "kora"
  ]
  node [
    id 124
    label "dziewcz&#281;"
  ]
  node [
    id 125
    label "dziecina"
  ]
  node [
    id 126
    label "m&#322;&#243;dka"
  ]
  node [
    id 127
    label "sympatia"
  ]
  node [
    id 128
    label "dziunia"
  ]
  node [
    id 129
    label "dziewczynina"
  ]
  node [
    id 130
    label "partnerka"
  ]
  node [
    id 131
    label "siksa"
  ]
  node [
    id 132
    label "dziewoja"
  ]
  node [
    id 133
    label "rze&#378;biarstwo"
  ]
  node [
    id 134
    label "planacja"
  ]
  node [
    id 135
    label "relief"
  ]
  node [
    id 136
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 137
    label "Ziemia"
  ]
  node [
    id 138
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 139
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 140
    label "bozzetto"
  ]
  node [
    id 141
    label "plastyka"
  ]
  node [
    id 142
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 143
    label "art"
  ]
  node [
    id 144
    label "plastic_surgery"
  ]
  node [
    id 145
    label "przedmiot"
  ]
  node [
    id 146
    label "operacja"
  ]
  node [
    id 147
    label "kszta&#322;towanie"
  ]
  node [
    id 148
    label "wyra&#378;no&#347;&#263;"
  ]
  node [
    id 149
    label "sugestywno&#347;&#263;"
  ]
  node [
    id 150
    label "color"
  ]
  node [
    id 151
    label "chirurgia"
  ]
  node [
    id 152
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 153
    label "sztuka"
  ]
  node [
    id 154
    label "Stary_&#346;wiat"
  ]
  node [
    id 155
    label "p&#243;&#322;noc"
  ]
  node [
    id 156
    label "geosfera"
  ]
  node [
    id 157
    label "przyroda"
  ]
  node [
    id 158
    label "po&#322;udnie"
  ]
  node [
    id 159
    label "morze"
  ]
  node [
    id 160
    label "hydrosfera"
  ]
  node [
    id 161
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 162
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 163
    label "geotermia"
  ]
  node [
    id 164
    label "ozonosfera"
  ]
  node [
    id 165
    label "biosfera"
  ]
  node [
    id 166
    label "magnetosfera"
  ]
  node [
    id 167
    label "Nowy_&#346;wiat"
  ]
  node [
    id 168
    label "biegun"
  ]
  node [
    id 169
    label "litosfera"
  ]
  node [
    id 170
    label "mikrokosmos"
  ]
  node [
    id 171
    label "p&#243;&#322;kula"
  ]
  node [
    id 172
    label "barysfera"
  ]
  node [
    id 173
    label "atmosfera"
  ]
  node [
    id 174
    label "geoida"
  ]
  node [
    id 175
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 176
    label "projekt"
  ]
  node [
    id 177
    label "sedymentacja"
  ]
  node [
    id 178
    label "denudacja"
  ]
  node [
    id 179
    label "proces_geologiczny"
  ]
  node [
    id 180
    label "zmiana"
  ]
  node [
    id 181
    label "kimation"
  ]
  node [
    id 182
    label "kolor"
  ]
  node [
    id 183
    label "stop"
  ]
  node [
    id 184
    label "medal"
  ]
  node [
    id 185
    label "metal_kolorowy"
  ]
  node [
    id 186
    label "tangent"
  ]
  node [
    id 187
    label "liczba_kwantowa"
  ]
  node [
    id 188
    label "&#347;wieci&#263;"
  ]
  node [
    id 189
    label "poker"
  ]
  node [
    id 190
    label "ubarwienie"
  ]
  node [
    id 191
    label "blakn&#261;&#263;"
  ]
  node [
    id 192
    label "struktura"
  ]
  node [
    id 193
    label "prze&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 194
    label "zblakni&#281;cie"
  ]
  node [
    id 195
    label "prze&#322;amywa&#263;"
  ]
  node [
    id 196
    label "prze&#322;amywanie_si&#281;"
  ]
  node [
    id 197
    label "prze&#322;ama&#263;_si&#281;"
  ]
  node [
    id 198
    label "prze&#322;amanie"
  ]
  node [
    id 199
    label "prze&#322;amywanie"
  ]
  node [
    id 200
    label "&#347;wiecenie"
  ]
  node [
    id 201
    label "prze&#322;ama&#263;"
  ]
  node [
    id 202
    label "zblakn&#261;&#263;"
  ]
  node [
    id 203
    label "symbol"
  ]
  node [
    id 204
    label "blakni&#281;cie"
  ]
  node [
    id 205
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 206
    label "przesyca&#263;"
  ]
  node [
    id 207
    label "przesycenie"
  ]
  node [
    id 208
    label "przesycanie"
  ]
  node [
    id 209
    label "struktura_metalu"
  ]
  node [
    id 210
    label "mieszanina"
  ]
  node [
    id 211
    label "znak_nakazu"
  ]
  node [
    id 212
    label "przeci&#261;gni&#281;cie"
  ]
  node [
    id 213
    label "reflektor"
  ]
  node [
    id 214
    label "alia&#380;"
  ]
  node [
    id 215
    label "przesyci&#263;"
  ]
  node [
    id 216
    label "przeci&#261;gn&#261;&#263;"
  ]
  node [
    id 217
    label "numizmatyka"
  ]
  node [
    id 218
    label "awers"
  ]
  node [
    id 219
    label "legenda"
  ]
  node [
    id 220
    label "rewers"
  ]
  node [
    id 221
    label "numizmat"
  ]
  node [
    id 222
    label "decoration"
  ]
  node [
    id 223
    label "odznaka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
]
