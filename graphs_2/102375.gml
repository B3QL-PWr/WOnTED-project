graph [
  node [
    id 0
    label "bardzo"
    origin "text"
  ]
  node [
    id 1
    label "interesowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "kolekcjoner"
    origin "text"
  ]
  node [
    id 3
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "zapewne"
    origin "text"
  ]
  node [
    id 6
    label "magazynowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ilo&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "wielokrotnie"
    origin "text"
  ]
  node [
    id 10
    label "przekracza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "indywidualny"
    origin "text"
  ]
  node [
    id 12
    label "mo&#380;liwo&#347;ci"
    origin "text"
  ]
  node [
    id 13
    label "przetrawi&#263;"
    origin "text"
  ]
  node [
    id 14
    label "ale"
    origin "text"
  ]
  node [
    id 15
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 16
    label "wiara"
    origin "text"
  ]
  node [
    id 17
    label "naiwny"
    origin "text"
  ]
  node [
    id 18
    label "&#347;ci&#261;ga&#263;"
    origin "text"
  ]
  node [
    id 19
    label "s&#322;u&#380;y&#263;"
    origin "text"
  ]
  node [
    id 20
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 21
    label "w_chuj"
  ]
  node [
    id 22
    label "sake"
  ]
  node [
    id 23
    label "rozciekawia&#263;"
  ]
  node [
    id 24
    label "alkohol"
  ]
  node [
    id 25
    label "zbieracz"
  ]
  node [
    id 26
    label "hobbysta"
  ]
  node [
    id 27
    label "zapaleniec"
  ]
  node [
    id 28
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 29
    label "nieprofesjonalista"
  ]
  node [
    id 30
    label "ludzko&#347;&#263;"
  ]
  node [
    id 31
    label "asymilowanie"
  ]
  node [
    id 32
    label "wapniak"
  ]
  node [
    id 33
    label "asymilowa&#263;"
  ]
  node [
    id 34
    label "os&#322;abia&#263;"
  ]
  node [
    id 35
    label "posta&#263;"
  ]
  node [
    id 36
    label "hominid"
  ]
  node [
    id 37
    label "podw&#322;adny"
  ]
  node [
    id 38
    label "os&#322;abianie"
  ]
  node [
    id 39
    label "g&#322;owa"
  ]
  node [
    id 40
    label "figura"
  ]
  node [
    id 41
    label "portrecista"
  ]
  node [
    id 42
    label "dwun&#243;g"
  ]
  node [
    id 43
    label "profanum"
  ]
  node [
    id 44
    label "mikrokosmos"
  ]
  node [
    id 45
    label "nasada"
  ]
  node [
    id 46
    label "duch"
  ]
  node [
    id 47
    label "antropochoria"
  ]
  node [
    id 48
    label "osoba"
  ]
  node [
    id 49
    label "wz&#243;r"
  ]
  node [
    id 50
    label "senior"
  ]
  node [
    id 51
    label "oddzia&#322;ywanie"
  ]
  node [
    id 52
    label "Adam"
  ]
  node [
    id 53
    label "homo_sapiens"
  ]
  node [
    id 54
    label "polifag"
  ]
  node [
    id 55
    label "ma&#322;pa_cz&#322;ekokszta&#322;tna"
  ]
  node [
    id 56
    label "cz&#322;owiekowate"
  ]
  node [
    id 57
    label "konsument"
  ]
  node [
    id 58
    label "istota_&#380;ywa"
  ]
  node [
    id 59
    label "pracownik"
  ]
  node [
    id 60
    label "Chocho&#322;"
  ]
  node [
    id 61
    label "Herkules_Poirot"
  ]
  node [
    id 62
    label "Edyp"
  ]
  node [
    id 63
    label "parali&#380;owa&#263;"
  ]
  node [
    id 64
    label "Harry_Potter"
  ]
  node [
    id 65
    label "Casanova"
  ]
  node [
    id 66
    label "Gargantua"
  ]
  node [
    id 67
    label "Zgredek"
  ]
  node [
    id 68
    label "Winnetou"
  ]
  node [
    id 69
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 70
    label "Dulcynea"
  ]
  node [
    id 71
    label "kategoria_gramatyczna"
  ]
  node [
    id 72
    label "person"
  ]
  node [
    id 73
    label "Sherlock_Holmes"
  ]
  node [
    id 74
    label "Quasimodo"
  ]
  node [
    id 75
    label "Plastu&#347;"
  ]
  node [
    id 76
    label "Faust"
  ]
  node [
    id 77
    label "Wallenrod"
  ]
  node [
    id 78
    label "Dwukwiat"
  ]
  node [
    id 79
    label "koniugacja"
  ]
  node [
    id 80
    label "Don_Juan"
  ]
  node [
    id 81
    label "Don_Kiszot"
  ]
  node [
    id 82
    label "Hamlet"
  ]
  node [
    id 83
    label "Werter"
  ]
  node [
    id 84
    label "istota"
  ]
  node [
    id 85
    label "Szwejk"
  ]
  node [
    id 86
    label "doros&#322;y"
  ]
  node [
    id 87
    label "naw&#243;z_sztuczny"
  ]
  node [
    id 88
    label "jajko"
  ]
  node [
    id 89
    label "rodzic"
  ]
  node [
    id 90
    label "wapniaki"
  ]
  node [
    id 91
    label "zwierzchnik"
  ]
  node [
    id 92
    label "feuda&#322;"
  ]
  node [
    id 93
    label "starzec"
  ]
  node [
    id 94
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 95
    label "zawodnik"
  ]
  node [
    id 96
    label "komendancja"
  ]
  node [
    id 97
    label "ksi&#261;&#380;&#281;"
  ]
  node [
    id 98
    label "de-escalation"
  ]
  node [
    id 99
    label "powodowanie"
  ]
  node [
    id 100
    label "os&#322;abienie"
  ]
  node [
    id 101
    label "kondycja_fizyczna"
  ]
  node [
    id 102
    label "os&#322;abi&#263;"
  ]
  node [
    id 103
    label "debilitation"
  ]
  node [
    id 104
    label "zdrowie"
  ]
  node [
    id 105
    label "zmniejszanie"
  ]
  node [
    id 106
    label "s&#322;abszy"
  ]
  node [
    id 107
    label "pogarszanie"
  ]
  node [
    id 108
    label "suppress"
  ]
  node [
    id 109
    label "powodowa&#263;"
  ]
  node [
    id 110
    label "zmniejsza&#263;"
  ]
  node [
    id 111
    label "bate"
  ]
  node [
    id 112
    label "asymilowanie_si&#281;"
  ]
  node [
    id 113
    label "absorption"
  ]
  node [
    id 114
    label "pobieranie"
  ]
  node [
    id 115
    label "czerpanie"
  ]
  node [
    id 116
    label "acquisition"
  ]
  node [
    id 117
    label "zmienianie"
  ]
  node [
    id 118
    label "organizm"
  ]
  node [
    id 119
    label "assimilation"
  ]
  node [
    id 120
    label "upodabnianie"
  ]
  node [
    id 121
    label "g&#322;oska"
  ]
  node [
    id 122
    label "kultura"
  ]
  node [
    id 123
    label "podobny"
  ]
  node [
    id 124
    label "grupa"
  ]
  node [
    id 125
    label "fonetyka"
  ]
  node [
    id 126
    label "assimilate"
  ]
  node [
    id 127
    label "dostosowywa&#263;"
  ]
  node [
    id 128
    label "dostosowa&#263;"
  ]
  node [
    id 129
    label "przejmowa&#263;"
  ]
  node [
    id 130
    label "upodobni&#263;"
  ]
  node [
    id 131
    label "przej&#261;&#263;"
  ]
  node [
    id 132
    label "upodabnia&#263;"
  ]
  node [
    id 133
    label "pobiera&#263;"
  ]
  node [
    id 134
    label "pobra&#263;"
  ]
  node [
    id 135
    label "charakterystyka"
  ]
  node [
    id 136
    label "zaistnie&#263;"
  ]
  node [
    id 137
    label "Osjan"
  ]
  node [
    id 138
    label "cecha"
  ]
  node [
    id 139
    label "kto&#347;"
  ]
  node [
    id 140
    label "wygl&#261;d"
  ]
  node [
    id 141
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 142
    label "osobowo&#347;&#263;"
  ]
  node [
    id 143
    label "wytw&#243;r"
  ]
  node [
    id 144
    label "trim"
  ]
  node [
    id 145
    label "poby&#263;"
  ]
  node [
    id 146
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 147
    label "Aspazja"
  ]
  node [
    id 148
    label "punkt_widzenia"
  ]
  node [
    id 149
    label "kompleksja"
  ]
  node [
    id 150
    label "wytrzyma&#263;"
  ]
  node [
    id 151
    label "budowa"
  ]
  node [
    id 152
    label "formacja"
  ]
  node [
    id 153
    label "pozosta&#263;"
  ]
  node [
    id 154
    label "point"
  ]
  node [
    id 155
    label "przedstawienie"
  ]
  node [
    id 156
    label "go&#347;&#263;"
  ]
  node [
    id 157
    label "zapis"
  ]
  node [
    id 158
    label "figure"
  ]
  node [
    id 159
    label "typ"
  ]
  node [
    id 160
    label "spos&#243;b"
  ]
  node [
    id 161
    label "mildew"
  ]
  node [
    id 162
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 163
    label "ideal"
  ]
  node [
    id 164
    label "rule"
  ]
  node [
    id 165
    label "ruch"
  ]
  node [
    id 166
    label "dekal"
  ]
  node [
    id 167
    label "motyw"
  ]
  node [
    id 168
    label "projekt"
  ]
  node [
    id 169
    label "pryncypa&#322;"
  ]
  node [
    id 170
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 171
    label "kszta&#322;t"
  ]
  node [
    id 172
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 173
    label "wiedza"
  ]
  node [
    id 174
    label "kierowa&#263;"
  ]
  node [
    id 175
    label "zdolno&#347;&#263;"
  ]
  node [
    id 176
    label "&#380;ycie"
  ]
  node [
    id 177
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 178
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 179
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 180
    label "sztuka"
  ]
  node [
    id 181
    label "dekiel"
  ]
  node [
    id 182
    label "ro&#347;lina"
  ]
  node [
    id 183
    label "&#347;ci&#281;cie"
  ]
  node [
    id 184
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 185
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 186
    label "&#347;ci&#281;gno"
  ]
  node [
    id 187
    label "noosfera"
  ]
  node [
    id 188
    label "byd&#322;o"
  ]
  node [
    id 189
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 190
    label "makrocefalia"
  ]
  node [
    id 191
    label "obiekt"
  ]
  node [
    id 192
    label "ucho"
  ]
  node [
    id 193
    label "g&#243;ra"
  ]
  node [
    id 194
    label "m&#243;zg"
  ]
  node [
    id 195
    label "kierownictwo"
  ]
  node [
    id 196
    label "fryzura"
  ]
  node [
    id 197
    label "umys&#322;"
  ]
  node [
    id 198
    label "cia&#322;o"
  ]
  node [
    id 199
    label "cz&#322;onek"
  ]
  node [
    id 200
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 201
    label "czaszka"
  ]
  node [
    id 202
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 203
    label "dziedzina"
  ]
  node [
    id 204
    label "hipnotyzowanie"
  ]
  node [
    id 205
    label "&#347;lad"
  ]
  node [
    id 206
    label "docieranie"
  ]
  node [
    id 207
    label "natural_process"
  ]
  node [
    id 208
    label "reakcja_chemiczna"
  ]
  node [
    id 209
    label "wdzieranie_si&#281;"
  ]
  node [
    id 210
    label "zjawisko"
  ]
  node [
    id 211
    label "act"
  ]
  node [
    id 212
    label "rezultat"
  ]
  node [
    id 213
    label "lobbysta"
  ]
  node [
    id 214
    label "allochoria"
  ]
  node [
    id 215
    label "fotograf"
  ]
  node [
    id 216
    label "malarz"
  ]
  node [
    id 217
    label "artysta"
  ]
  node [
    id 218
    label "p&#322;aszczyzna"
  ]
  node [
    id 219
    label "przedmiot"
  ]
  node [
    id 220
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 221
    label "bierka_szachowa"
  ]
  node [
    id 222
    label "obiekt_matematyczny"
  ]
  node [
    id 223
    label "gestaltyzm"
  ]
  node [
    id 224
    label "styl"
  ]
  node [
    id 225
    label "obraz"
  ]
  node [
    id 226
    label "rzecz"
  ]
  node [
    id 227
    label "d&#378;wi&#281;k"
  ]
  node [
    id 228
    label "character"
  ]
  node [
    id 229
    label "rze&#378;ba"
  ]
  node [
    id 230
    label "stylistyka"
  ]
  node [
    id 231
    label "miejsce"
  ]
  node [
    id 232
    label "antycypacja"
  ]
  node [
    id 233
    label "ornamentyka"
  ]
  node [
    id 234
    label "informacja"
  ]
  node [
    id 235
    label "facet"
  ]
  node [
    id 236
    label "popis"
  ]
  node [
    id 237
    label "wiersz"
  ]
  node [
    id 238
    label "symetria"
  ]
  node [
    id 239
    label "lingwistyka_kognitywna"
  ]
  node [
    id 240
    label "karta"
  ]
  node [
    id 241
    label "shape"
  ]
  node [
    id 242
    label "podzbi&#243;r"
  ]
  node [
    id 243
    label "perspektywa"
  ]
  node [
    id 244
    label "nak&#322;adka"
  ]
  node [
    id 245
    label "li&#347;&#263;"
  ]
  node [
    id 246
    label "jama_gard&#322;owa"
  ]
  node [
    id 247
    label "rezonator"
  ]
  node [
    id 248
    label "podstawa"
  ]
  node [
    id 249
    label "base"
  ]
  node [
    id 250
    label "piek&#322;o"
  ]
  node [
    id 251
    label "human_body"
  ]
  node [
    id 252
    label "ofiarowywanie"
  ]
  node [
    id 253
    label "sfera_afektywna"
  ]
  node [
    id 254
    label "nekromancja"
  ]
  node [
    id 255
    label "Po&#347;wist"
  ]
  node [
    id 256
    label "podekscytowanie"
  ]
  node [
    id 257
    label "deformowanie"
  ]
  node [
    id 258
    label "sumienie"
  ]
  node [
    id 259
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 260
    label "deformowa&#263;"
  ]
  node [
    id 261
    label "psychika"
  ]
  node [
    id 262
    label "zjawa"
  ]
  node [
    id 263
    label "zmar&#322;y"
  ]
  node [
    id 264
    label "istota_nadprzyrodzona"
  ]
  node [
    id 265
    label "power"
  ]
  node [
    id 266
    label "entity"
  ]
  node [
    id 267
    label "ofiarowywa&#263;"
  ]
  node [
    id 268
    label "oddech"
  ]
  node [
    id 269
    label "seksualno&#347;&#263;"
  ]
  node [
    id 270
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 271
    label "byt"
  ]
  node [
    id 272
    label "si&#322;a"
  ]
  node [
    id 273
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 274
    label "ego"
  ]
  node [
    id 275
    label "ofiarowanie"
  ]
  node [
    id 276
    label "charakter"
  ]
  node [
    id 277
    label "fizjonomia"
  ]
  node [
    id 278
    label "kompleks"
  ]
  node [
    id 279
    label "zapalno&#347;&#263;"
  ]
  node [
    id 280
    label "T&#281;sknica"
  ]
  node [
    id 281
    label "ofiarowa&#263;"
  ]
  node [
    id 282
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 283
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 284
    label "passion"
  ]
  node [
    id 285
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 286
    label "atom"
  ]
  node [
    id 287
    label "odbicie"
  ]
  node [
    id 288
    label "przyroda"
  ]
  node [
    id 289
    label "Ziemia"
  ]
  node [
    id 290
    label "kosmos"
  ]
  node [
    id 291
    label "miniatura"
  ]
  node [
    id 292
    label "throng"
  ]
  node [
    id 293
    label "gromadzi&#263;"
  ]
  node [
    id 294
    label "przechowywa&#263;"
  ]
  node [
    id 295
    label "posiada&#263;"
  ]
  node [
    id 296
    label "poci&#261;ga&#263;"
  ]
  node [
    id 297
    label "zbiera&#263;"
  ]
  node [
    id 298
    label "congregate"
  ]
  node [
    id 299
    label "podtrzymywa&#263;"
  ]
  node [
    id 300
    label "chroni&#263;"
  ]
  node [
    id 301
    label "przetrzymywa&#263;"
  ]
  node [
    id 302
    label "hold"
  ]
  node [
    id 303
    label "continue"
  ]
  node [
    id 304
    label "zachowywa&#263;"
  ]
  node [
    id 305
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 306
    label "rozmiar"
  ]
  node [
    id 307
    label "part"
  ]
  node [
    id 308
    label "warunek_lokalowy"
  ]
  node [
    id 309
    label "liczba"
  ]
  node [
    id 310
    label "circumference"
  ]
  node [
    id 311
    label "odzie&#380;"
  ]
  node [
    id 312
    label "znaczenie"
  ]
  node [
    id 313
    label "dymensja"
  ]
  node [
    id 314
    label "integer"
  ]
  node [
    id 315
    label "zlewanie_si&#281;"
  ]
  node [
    id 316
    label "uk&#322;ad"
  ]
  node [
    id 317
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 318
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 319
    label "pe&#322;ny"
  ]
  node [
    id 320
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 321
    label "ta&#347;ma"
  ]
  node [
    id 322
    label "plecionka"
  ]
  node [
    id 323
    label "parciak"
  ]
  node [
    id 324
    label "p&#322;&#243;tno"
  ]
  node [
    id 325
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 326
    label "temat"
  ]
  node [
    id 327
    label "zawarto&#347;&#263;"
  ]
  node [
    id 328
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 329
    label "wn&#281;trze"
  ]
  node [
    id 330
    label "punkt"
  ]
  node [
    id 331
    label "publikacja"
  ]
  node [
    id 332
    label "obiega&#263;"
  ]
  node [
    id 333
    label "powzi&#281;cie"
  ]
  node [
    id 334
    label "dane"
  ]
  node [
    id 335
    label "obiegni&#281;cie"
  ]
  node [
    id 336
    label "sygna&#322;"
  ]
  node [
    id 337
    label "obieganie"
  ]
  node [
    id 338
    label "powzi&#261;&#263;"
  ]
  node [
    id 339
    label "obiec"
  ]
  node [
    id 340
    label "doj&#347;cie"
  ]
  node [
    id 341
    label "doj&#347;&#263;"
  ]
  node [
    id 342
    label "mentalno&#347;&#263;"
  ]
  node [
    id 343
    label "superego"
  ]
  node [
    id 344
    label "sprawa"
  ]
  node [
    id 345
    label "wyraz_pochodny"
  ]
  node [
    id 346
    label "zboczenie"
  ]
  node [
    id 347
    label "om&#243;wienie"
  ]
  node [
    id 348
    label "omawia&#263;"
  ]
  node [
    id 349
    label "fraza"
  ]
  node [
    id 350
    label "forum"
  ]
  node [
    id 351
    label "topik"
  ]
  node [
    id 352
    label "tematyka"
  ]
  node [
    id 353
    label "w&#261;tek"
  ]
  node [
    id 354
    label "zbaczanie"
  ]
  node [
    id 355
    label "forma"
  ]
  node [
    id 356
    label "om&#243;wi&#263;"
  ]
  node [
    id 357
    label "omawianie"
  ]
  node [
    id 358
    label "melodia"
  ]
  node [
    id 359
    label "otoczka"
  ]
  node [
    id 360
    label "zbacza&#263;"
  ]
  node [
    id 361
    label "zboczy&#263;"
  ]
  node [
    id 362
    label "wielokrotny"
  ]
  node [
    id 363
    label "cz&#281;stokro&#263;"
  ]
  node [
    id 364
    label "tylekro&#263;"
  ]
  node [
    id 365
    label "wielekro&#263;"
  ]
  node [
    id 366
    label "du&#380;o"
  ]
  node [
    id 367
    label "cz&#281;sto"
  ]
  node [
    id 368
    label "z&#322;o&#380;ony"
  ]
  node [
    id 369
    label "du&#380;y"
  ]
  node [
    id 370
    label "mocno"
  ]
  node [
    id 371
    label "wiela"
  ]
  node [
    id 372
    label "ograniczenie"
  ]
  node [
    id 373
    label "przebywa&#263;"
  ]
  node [
    id 374
    label "conflict"
  ]
  node [
    id 375
    label "transgress"
  ]
  node [
    id 376
    label "appear"
  ]
  node [
    id 377
    label "osi&#261;ga&#263;"
  ]
  node [
    id 378
    label "mija&#263;"
  ]
  node [
    id 379
    label "tkwi&#263;"
  ]
  node [
    id 380
    label "istnie&#263;"
  ]
  node [
    id 381
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 382
    label "pause"
  ]
  node [
    id 383
    label "przestawa&#263;"
  ]
  node [
    id 384
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 385
    label "hesitate"
  ]
  node [
    id 386
    label "trwa&#263;"
  ]
  node [
    id 387
    label "base_on_balls"
  ]
  node [
    id 388
    label "go"
  ]
  node [
    id 389
    label "omija&#263;"
  ]
  node [
    id 390
    label "przechodzi&#263;"
  ]
  node [
    id 391
    label "proceed"
  ]
  node [
    id 392
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 393
    label "uzyskiwa&#263;"
  ]
  node [
    id 394
    label "dociera&#263;"
  ]
  node [
    id 395
    label "mark"
  ]
  node [
    id 396
    label "get"
  ]
  node [
    id 397
    label "organizowa&#263;"
  ]
  node [
    id 398
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 399
    label "czyni&#263;"
  ]
  node [
    id 400
    label "give"
  ]
  node [
    id 401
    label "stylizowa&#263;"
  ]
  node [
    id 402
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 403
    label "falowa&#263;"
  ]
  node [
    id 404
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 405
    label "peddle"
  ]
  node [
    id 406
    label "praca"
  ]
  node [
    id 407
    label "wydala&#263;"
  ]
  node [
    id 408
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 409
    label "tentegowa&#263;"
  ]
  node [
    id 410
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 411
    label "urz&#261;dza&#263;"
  ]
  node [
    id 412
    label "oszukiwa&#263;"
  ]
  node [
    id 413
    label "work"
  ]
  node [
    id 414
    label "ukazywa&#263;"
  ]
  node [
    id 415
    label "przerabia&#263;"
  ]
  node [
    id 416
    label "post&#281;powa&#263;"
  ]
  node [
    id 417
    label "g&#322;upstwo"
  ]
  node [
    id 418
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 419
    label "prevention"
  ]
  node [
    id 420
    label "pomiarkowanie"
  ]
  node [
    id 421
    label "przeszkoda"
  ]
  node [
    id 422
    label "intelekt"
  ]
  node [
    id 423
    label "zmniejszenie"
  ]
  node [
    id 424
    label "reservation"
  ]
  node [
    id 425
    label "przekroczenie"
  ]
  node [
    id 426
    label "finlandyzacja"
  ]
  node [
    id 427
    label "otoczenie"
  ]
  node [
    id 428
    label "osielstwo"
  ]
  node [
    id 429
    label "zdyskryminowanie"
  ]
  node [
    id 430
    label "warunek"
  ]
  node [
    id 431
    label "limitation"
  ]
  node [
    id 432
    label "przekroczy&#263;"
  ]
  node [
    id 433
    label "przekraczanie"
  ]
  node [
    id 434
    label "barrier"
  ]
  node [
    id 435
    label "swoisty"
  ]
  node [
    id 436
    label "indywidualnie"
  ]
  node [
    id 437
    label "osobny"
  ]
  node [
    id 438
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 439
    label "odr&#281;bny"
  ]
  node [
    id 440
    label "swoi&#347;cie"
  ]
  node [
    id 441
    label "wyodr&#281;bnienie_si&#281;"
  ]
  node [
    id 442
    label "wydzielenie"
  ]
  node [
    id 443
    label "osobno"
  ]
  node [
    id 444
    label "kolejny"
  ]
  node [
    id 445
    label "inszy"
  ]
  node [
    id 446
    label "wyodr&#281;bnianie"
  ]
  node [
    id 447
    label "wyodr&#281;bnianie_si&#281;"
  ]
  node [
    id 448
    label "individually"
  ]
  node [
    id 449
    label "singly"
  ]
  node [
    id 450
    label "capability"
  ]
  node [
    id 451
    label "potencja&#322;"
  ]
  node [
    id 452
    label "wielko&#347;&#263;"
  ]
  node [
    id 453
    label "zapomina&#263;"
  ]
  node [
    id 454
    label "zapomnienie"
  ]
  node [
    id 455
    label "zapominanie"
  ]
  node [
    id 456
    label "ability"
  ]
  node [
    id 457
    label "obliczeniowo"
  ]
  node [
    id 458
    label "zapomnie&#263;"
  ]
  node [
    id 459
    label "przemy&#347;le&#263;"
  ]
  node [
    id 460
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 461
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 462
    label "przerobi&#263;"
  ]
  node [
    id 463
    label "digest"
  ]
  node [
    id 464
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 465
    label "zniszczy&#263;"
  ]
  node [
    id 466
    label "chemia"
  ]
  node [
    id 467
    label "spowodowa&#263;"
  ]
  node [
    id 468
    label "zaszkodzi&#263;"
  ]
  node [
    id 469
    label "zu&#380;y&#263;"
  ]
  node [
    id 470
    label "spoil"
  ]
  node [
    id 471
    label "consume"
  ]
  node [
    id 472
    label "pamper"
  ]
  node [
    id 473
    label "wygra&#263;"
  ]
  node [
    id 474
    label "wear"
  ]
  node [
    id 475
    label "rozmie&#347;ci&#263;"
  ]
  node [
    id 476
    label "zepsu&#263;"
  ]
  node [
    id 477
    label "zmieni&#263;"
  ]
  node [
    id 478
    label "podzieli&#263;"
  ]
  node [
    id 479
    label "range"
  ]
  node [
    id 480
    label "oddali&#263;"
  ]
  node [
    id 481
    label "stagger"
  ]
  node [
    id 482
    label "note"
  ]
  node [
    id 483
    label "raise"
  ]
  node [
    id 484
    label "zaliczy&#263;"
  ]
  node [
    id 485
    label "overwork"
  ]
  node [
    id 486
    label "zamieni&#263;"
  ]
  node [
    id 487
    label "zmodyfikowa&#263;"
  ]
  node [
    id 488
    label "change"
  ]
  node [
    id 489
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 490
    label "zrobi&#263;"
  ]
  node [
    id 491
    label "przej&#347;&#263;"
  ]
  node [
    id 492
    label "convert"
  ]
  node [
    id 493
    label "wytworzy&#263;"
  ]
  node [
    id 494
    label "prze&#380;y&#263;"
  ]
  node [
    id 495
    label "przetworzy&#263;"
  ]
  node [
    id 496
    label "upora&#263;_si&#281;"
  ]
  node [
    id 497
    label "reconsideration"
  ]
  node [
    id 498
    label "pomy&#347;le&#263;"
  ]
  node [
    id 499
    label "zainteresowa&#263;"
  ]
  node [
    id 500
    label "spo&#380;y&#263;"
  ]
  node [
    id 501
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 502
    label "pozna&#263;"
  ]
  node [
    id 503
    label "zabra&#263;"
  ]
  node [
    id 504
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 505
    label "absorb"
  ]
  node [
    id 506
    label "swallow"
  ]
  node [
    id 507
    label "piwo"
  ]
  node [
    id 508
    label "uwarzenie"
  ]
  node [
    id 509
    label "warzenie"
  ]
  node [
    id 510
    label "nap&#243;j"
  ]
  node [
    id 511
    label "bacik"
  ]
  node [
    id 512
    label "wyj&#347;cie"
  ]
  node [
    id 513
    label "uwarzy&#263;"
  ]
  node [
    id 514
    label "birofilia"
  ]
  node [
    id 515
    label "warzy&#263;"
  ]
  node [
    id 516
    label "nawarzy&#263;"
  ]
  node [
    id 517
    label "browarnia"
  ]
  node [
    id 518
    label "nawarzenie"
  ]
  node [
    id 519
    label "anta&#322;"
  ]
  node [
    id 520
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 521
    label "billow"
  ]
  node [
    id 522
    label "clutter"
  ]
  node [
    id 523
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 524
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 525
    label "beckon"
  ]
  node [
    id 526
    label "powiewa&#263;"
  ]
  node [
    id 527
    label "planowa&#263;"
  ]
  node [
    id 528
    label "treat"
  ]
  node [
    id 529
    label "pozyskiwa&#263;"
  ]
  node [
    id 530
    label "ensnare"
  ]
  node [
    id 531
    label "skupia&#263;"
  ]
  node [
    id 532
    label "create"
  ]
  node [
    id 533
    label "przygotowywa&#263;"
  ]
  node [
    id 534
    label "tworzy&#263;"
  ]
  node [
    id 535
    label "standard"
  ]
  node [
    id 536
    label "wprowadza&#263;"
  ]
  node [
    id 537
    label "kopiowa&#263;"
  ]
  node [
    id 538
    label "czerpa&#263;"
  ]
  node [
    id 539
    label "dally"
  ]
  node [
    id 540
    label "mock"
  ]
  node [
    id 541
    label "sprawia&#263;"
  ]
  node [
    id 542
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 543
    label "decydowa&#263;"
  ]
  node [
    id 544
    label "cast"
  ]
  node [
    id 545
    label "podbija&#263;"
  ]
  node [
    id 546
    label "wytwarza&#263;"
  ]
  node [
    id 547
    label "amend"
  ]
  node [
    id 548
    label "zalicza&#263;"
  ]
  node [
    id 549
    label "prze&#380;ywa&#263;"
  ]
  node [
    id 550
    label "zamienia&#263;"
  ]
  node [
    id 551
    label "zmienia&#263;"
  ]
  node [
    id 552
    label "modyfikowa&#263;"
  ]
  node [
    id 553
    label "radzi&#263;_sobie"
  ]
  node [
    id 554
    label "pracowa&#263;"
  ]
  node [
    id 555
    label "przetwarza&#263;"
  ]
  node [
    id 556
    label "sp&#281;dza&#263;"
  ]
  node [
    id 557
    label "stylize"
  ]
  node [
    id 558
    label "nadawa&#263;"
  ]
  node [
    id 559
    label "toczy&#263;_si&#281;"
  ]
  node [
    id 560
    label "przybiera&#263;"
  ]
  node [
    id 561
    label "i&#347;&#263;"
  ]
  node [
    id 562
    label "use"
  ]
  node [
    id 563
    label "blurt_out"
  ]
  node [
    id 564
    label "za&#322;atwia&#263;_si&#281;"
  ]
  node [
    id 565
    label "usuwa&#263;"
  ]
  node [
    id 566
    label "unwrap"
  ]
  node [
    id 567
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 568
    label "pokazywa&#263;"
  ]
  node [
    id 569
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 570
    label "orzyna&#263;"
  ]
  node [
    id 571
    label "oszwabia&#263;"
  ]
  node [
    id 572
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 573
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 574
    label "cheat"
  ]
  node [
    id 575
    label "dispose"
  ]
  node [
    id 576
    label "aran&#380;owa&#263;"
  ]
  node [
    id 577
    label "satysfakcjonowa&#263;"
  ]
  node [
    id 578
    label "odpowiada&#263;"
  ]
  node [
    id 579
    label "zabezpiecza&#263;"
  ]
  node [
    id 580
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 581
    label "doprowadza&#263;"
  ]
  node [
    id 582
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 583
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 584
    label "najem"
  ]
  node [
    id 585
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 586
    label "zak&#322;ad"
  ]
  node [
    id 587
    label "stosunek_pracy"
  ]
  node [
    id 588
    label "benedykty&#324;ski"
  ]
  node [
    id 589
    label "poda&#380;_pracy"
  ]
  node [
    id 590
    label "pracowanie"
  ]
  node [
    id 591
    label "tyrka"
  ]
  node [
    id 592
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 593
    label "zaw&#243;d"
  ]
  node [
    id 594
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 595
    label "tynkarski"
  ]
  node [
    id 596
    label "czynno&#347;&#263;"
  ]
  node [
    id 597
    label "zmiana"
  ]
  node [
    id 598
    label "czynnik_produkcji"
  ]
  node [
    id 599
    label "zobowi&#261;zanie"
  ]
  node [
    id 600
    label "siedziba"
  ]
  node [
    id 601
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 602
    label "pogl&#261;d"
  ]
  node [
    id 603
    label "przes&#261;dny"
  ]
  node [
    id 604
    label "belief"
  ]
  node [
    id 605
    label "faith"
  ]
  node [
    id 606
    label "konwikcja"
  ]
  node [
    id 607
    label "postawa"
  ]
  node [
    id 608
    label "teologicznie"
  ]
  node [
    id 609
    label "s&#261;d"
  ]
  node [
    id 610
    label "zderzenie_si&#281;"
  ]
  node [
    id 611
    label "zderzy&#263;_si&#281;"
  ]
  node [
    id 612
    label "teoria_Arrheniusa"
  ]
  node [
    id 613
    label "stan"
  ]
  node [
    id 614
    label "nastawienie"
  ]
  node [
    id 615
    label "pozycja"
  ]
  node [
    id 616
    label "attitude"
  ]
  node [
    id 617
    label "pewno&#347;&#263;"
  ]
  node [
    id 618
    label "nieracjonalny"
  ]
  node [
    id 619
    label "przes&#261;dnie"
  ]
  node [
    id 620
    label "naiwnie"
  ]
  node [
    id 621
    label "poczciwy"
  ]
  node [
    id 622
    label "g&#322;upi"
  ]
  node [
    id 623
    label "prostoduszny"
  ]
  node [
    id 624
    label "szczery"
  ]
  node [
    id 625
    label "prostodusznie"
  ]
  node [
    id 626
    label "&#347;mieszny"
  ]
  node [
    id 627
    label "bezwolny"
  ]
  node [
    id 628
    label "g&#322;upienie"
  ]
  node [
    id 629
    label "mondzio&#322;"
  ]
  node [
    id 630
    label "niewa&#380;ny"
  ]
  node [
    id 631
    label "bezmy&#347;lny"
  ]
  node [
    id 632
    label "bezsensowny"
  ]
  node [
    id 633
    label "nadaremny"
  ]
  node [
    id 634
    label "niem&#261;dry"
  ]
  node [
    id 635
    label "nierozwa&#380;ny"
  ]
  node [
    id 636
    label "niezr&#281;czny"
  ]
  node [
    id 637
    label "g&#322;uptas"
  ]
  node [
    id 638
    label "zg&#322;upienie"
  ]
  node [
    id 639
    label "g&#322;upiec"
  ]
  node [
    id 640
    label "uprzykrzony"
  ]
  node [
    id 641
    label "bezcelowy"
  ]
  node [
    id 642
    label "ma&#322;y"
  ]
  node [
    id 643
    label "g&#322;upio"
  ]
  node [
    id 644
    label "prosty"
  ]
  node [
    id 645
    label "poczciwie"
  ]
  node [
    id 646
    label "sympatyczny"
  ]
  node [
    id 647
    label "dobry"
  ]
  node [
    id 648
    label "odprowadza&#263;"
  ]
  node [
    id 649
    label "przewi&#261;zywa&#263;"
  ]
  node [
    id 650
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 651
    label "stiffen"
  ]
  node [
    id 652
    label "ciecz"
  ]
  node [
    id 653
    label "znosi&#263;"
  ]
  node [
    id 654
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 655
    label "zmusza&#263;"
  ]
  node [
    id 656
    label "bind"
  ]
  node [
    id 657
    label "sprawdzian"
  ]
  node [
    id 658
    label "zdejmowa&#263;"
  ]
  node [
    id 659
    label "kra&#347;&#263;"
  ]
  node [
    id 660
    label "przepisywa&#263;"
  ]
  node [
    id 661
    label "kurczy&#263;"
  ]
  node [
    id 662
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 663
    label "clamp"
  ]
  node [
    id 664
    label "tease"
  ]
  node [
    id 665
    label "take"
  ]
  node [
    id 666
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 667
    label "cenzura"
  ]
  node [
    id 668
    label "bra&#263;"
  ]
  node [
    id 669
    label "uwalnia&#263;"
  ]
  node [
    id 670
    label "seclude"
  ]
  node [
    id 671
    label "snap"
  ]
  node [
    id 672
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 673
    label "abstract"
  ]
  node [
    id 674
    label "zabrania&#263;"
  ]
  node [
    id 675
    label "odsuwa&#263;"
  ]
  node [
    id 676
    label "przemieszcza&#263;"
  ]
  node [
    id 677
    label "dostarcza&#263;"
  ]
  node [
    id 678
    label "company"
  ]
  node [
    id 679
    label "mie&#263;_miejsce"
  ]
  node [
    id 680
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 681
    label "motywowa&#263;"
  ]
  node [
    id 682
    label "okr&#281;ca&#263;"
  ]
  node [
    id 683
    label "podpierdala&#263;"
  ]
  node [
    id 684
    label "mie&#263;_lepkie_r&#281;ce"
  ]
  node [
    id 685
    label "r&#261;ba&#263;"
  ]
  node [
    id 686
    label "podsuwa&#263;"
  ]
  node [
    id 687
    label "overcharge"
  ]
  node [
    id 688
    label "zar&#261;bywa&#263;"
  ]
  node [
    id 689
    label "przyw&#322;aszcza&#263;"
  ]
  node [
    id 690
    label "porywa&#263;"
  ]
  node [
    id 691
    label "sk&#322;ada&#263;"
  ]
  node [
    id 692
    label "ranny"
  ]
  node [
    id 693
    label "behave"
  ]
  node [
    id 694
    label "carry"
  ]
  node [
    id 695
    label "represent"
  ]
  node [
    id 696
    label "podrze&#263;"
  ]
  node [
    id 697
    label "przenosi&#263;"
  ]
  node [
    id 698
    label "str&#243;j"
  ]
  node [
    id 699
    label "wytrzymywa&#263;"
  ]
  node [
    id 700
    label "wygrywa&#263;"
  ]
  node [
    id 701
    label "poddawa&#263;_si&#281;"
  ]
  node [
    id 702
    label "set"
  ]
  node [
    id 703
    label "niszczy&#263;"
  ]
  node [
    id 704
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 705
    label "tolerowa&#263;"
  ]
  node [
    id 706
    label "zadawa&#263;_gwa&#322;t"
  ]
  node [
    id 707
    label "sandbag"
  ]
  node [
    id 708
    label "przekazywa&#263;"
  ]
  node [
    id 709
    label "transcribe"
  ]
  node [
    id 710
    label "answer"
  ]
  node [
    id 711
    label "save"
  ]
  node [
    id 712
    label "zaleca&#263;"
  ]
  node [
    id 713
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 714
    label "pirat"
  ]
  node [
    id 715
    label "condense"
  ]
  node [
    id 716
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 717
    label "wpadni&#281;cie"
  ]
  node [
    id 718
    label "p&#322;ywa&#263;"
  ]
  node [
    id 719
    label "ciek&#322;y"
  ]
  node [
    id 720
    label "chlupa&#263;"
  ]
  node [
    id 721
    label "wytoczenie"
  ]
  node [
    id 722
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 723
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 724
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 725
    label "stan_skupienia"
  ]
  node [
    id 726
    label "nieprzejrzysty"
  ]
  node [
    id 727
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 728
    label "podbiega&#263;"
  ]
  node [
    id 729
    label "baniak"
  ]
  node [
    id 730
    label "zachlupa&#263;"
  ]
  node [
    id 731
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 732
    label "odp&#322;ywanie"
  ]
  node [
    id 733
    label "podbiec"
  ]
  node [
    id 734
    label "wpadanie"
  ]
  node [
    id 735
    label "substancja"
  ]
  node [
    id 736
    label "fee"
  ]
  node [
    id 737
    label "kwota"
  ]
  node [
    id 738
    label "uregulowa&#263;"
  ]
  node [
    id 739
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 740
    label "faza"
  ]
  node [
    id 741
    label "podchodzi&#263;"
  ]
  node [
    id 742
    label "&#263;wiczenie"
  ]
  node [
    id 743
    label "pytanie"
  ]
  node [
    id 744
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 745
    label "praca_pisemna"
  ]
  node [
    id 746
    label "kontrola"
  ]
  node [
    id 747
    label "dydaktyka"
  ]
  node [
    id 748
    label "pr&#243;ba"
  ]
  node [
    id 749
    label "examination"
  ]
  node [
    id 750
    label "&#380;o&#322;nierz"
  ]
  node [
    id 751
    label "by&#263;"
  ]
  node [
    id 752
    label "suffice"
  ]
  node [
    id 753
    label "cel"
  ]
  node [
    id 754
    label "match"
  ]
  node [
    id 755
    label "przydawa&#263;_si&#281;"
  ]
  node [
    id 756
    label "pies"
  ]
  node [
    id 757
    label "s&#322;ugiwa&#263;"
  ]
  node [
    id 758
    label "wait"
  ]
  node [
    id 759
    label "pomaga&#263;"
  ]
  node [
    id 760
    label "aid"
  ]
  node [
    id 761
    label "u&#322;atwia&#263;"
  ]
  node [
    id 762
    label "concur"
  ]
  node [
    id 763
    label "sprzyja&#263;"
  ]
  node [
    id 764
    label "skutkowa&#263;"
  ]
  node [
    id 765
    label "Warszawa"
  ]
  node [
    id 766
    label "back"
  ]
  node [
    id 767
    label "pozostawa&#263;"
  ]
  node [
    id 768
    label "zostawa&#263;"
  ]
  node [
    id 769
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 770
    label "stand"
  ]
  node [
    id 771
    label "adhere"
  ]
  node [
    id 772
    label "determine"
  ]
  node [
    id 773
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 774
    label "equal"
  ]
  node [
    id 775
    label "chodzi&#263;"
  ]
  node [
    id 776
    label "si&#281;ga&#263;"
  ]
  node [
    id 777
    label "obecno&#347;&#263;"
  ]
  node [
    id 778
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 779
    label "uczestniczy&#263;"
  ]
  node [
    id 780
    label "endeavor"
  ]
  node [
    id 781
    label "podejmowa&#263;"
  ]
  node [
    id 782
    label "dziama&#263;"
  ]
  node [
    id 783
    label "do"
  ]
  node [
    id 784
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 785
    label "bangla&#263;"
  ]
  node [
    id 786
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 787
    label "maszyna"
  ]
  node [
    id 788
    label "dzia&#322;a&#263;"
  ]
  node [
    id 789
    label "tryb"
  ]
  node [
    id 790
    label "funkcjonowa&#263;"
  ]
  node [
    id 791
    label "piese&#322;"
  ]
  node [
    id 792
    label "Cerber"
  ]
  node [
    id 793
    label "szczeka&#263;"
  ]
  node [
    id 794
    label "&#322;ajdak"
  ]
  node [
    id 795
    label "kabanos"
  ]
  node [
    id 796
    label "wyzwisko"
  ]
  node [
    id 797
    label "samiec"
  ]
  node [
    id 798
    label "spragniony"
  ]
  node [
    id 799
    label "policjant"
  ]
  node [
    id 800
    label "rakarz"
  ]
  node [
    id 801
    label "szczu&#263;"
  ]
  node [
    id 802
    label "wycie"
  ]
  node [
    id 803
    label "trufla"
  ]
  node [
    id 804
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 805
    label "zawy&#263;"
  ]
  node [
    id 806
    label "sobaka"
  ]
  node [
    id 807
    label "dogoterapia"
  ]
  node [
    id 808
    label "s&#322;u&#380;enie"
  ]
  node [
    id 809
    label "psowate"
  ]
  node [
    id 810
    label "wy&#263;"
  ]
  node [
    id 811
    label "szczucie"
  ]
  node [
    id 812
    label "czworon&#243;g"
  ]
  node [
    id 813
    label "&#380;o&#322;nierstwo"
  ]
  node [
    id 814
    label "harcap"
  ]
  node [
    id 815
    label "wojsko"
  ]
  node [
    id 816
    label "elew"
  ]
  node [
    id 817
    label "s&#322;u&#380;y&#263;_w_wojsku"
  ]
  node [
    id 818
    label "demobilizowanie"
  ]
  node [
    id 819
    label "Gurkha"
  ]
  node [
    id 820
    label "zdemobilizowanie"
  ]
  node [
    id 821
    label "s&#322;u&#380;enie_w_wojsku"
  ]
  node [
    id 822
    label "so&#322;dat"
  ]
  node [
    id 823
    label "demobilizowa&#263;"
  ]
  node [
    id 824
    label "mundurowy"
  ]
  node [
    id 825
    label "rota"
  ]
  node [
    id 826
    label "zdemobilizowa&#263;"
  ]
  node [
    id 827
    label "walcz&#261;cy"
  ]
  node [
    id 828
    label "&#380;o&#322;dowy"
  ]
  node [
    id 829
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 830
    label "thing"
  ]
  node [
    id 831
    label "us&#322;ugiwa&#263;"
  ]
  node [
    id 832
    label "notice"
  ]
  node [
    id 833
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 834
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 835
    label "styka&#263;_si&#281;"
  ]
  node [
    id 836
    label "zaczyna&#263;"
  ]
  node [
    id 837
    label "nastawia&#263;"
  ]
  node [
    id 838
    label "get_in_touch"
  ]
  node [
    id 839
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 840
    label "dokoptowywa&#263;"
  ]
  node [
    id 841
    label "uruchamia&#263;"
  ]
  node [
    id 842
    label "involve"
  ]
  node [
    id 843
    label "umieszcza&#263;"
  ]
  node [
    id 844
    label "connect"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 362
  ]
  edge [
    source 9
    target 363
  ]
  edge [
    source 9
    target 364
  ]
  edge [
    source 9
    target 365
  ]
  edge [
    source 9
    target 366
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 368
  ]
  edge [
    source 9
    target 369
  ]
  edge [
    source 9
    target 370
  ]
  edge [
    source 9
    target 371
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 295
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 13
    target 483
  ]
  edge [
    source 13
    target 484
  ]
  edge [
    source 13
    target 485
  ]
  edge [
    source 13
    target 486
  ]
  edge [
    source 13
    target 487
  ]
  edge [
    source 13
    target 488
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 490
  ]
  edge [
    source 13
    target 491
  ]
  edge [
    source 13
    target 492
  ]
  edge [
    source 13
    target 493
  ]
  edge [
    source 13
    target 494
  ]
  edge [
    source 13
    target 495
  ]
  edge [
    source 13
    target 496
  ]
  edge [
    source 13
    target 497
  ]
  edge [
    source 13
    target 498
  ]
  edge [
    source 13
    target 499
  ]
  edge [
    source 13
    target 500
  ]
  edge [
    source 13
    target 501
  ]
  edge [
    source 13
    target 502
  ]
  edge [
    source 13
    target 503
  ]
  edge [
    source 13
    target 504
  ]
  edge [
    source 13
    target 505
  ]
  edge [
    source 13
    target 506
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 397
  ]
  edge [
    source 15
    target 398
  ]
  edge [
    source 15
    target 399
  ]
  edge [
    source 15
    target 400
  ]
  edge [
    source 15
    target 401
  ]
  edge [
    source 15
    target 402
  ]
  edge [
    source 15
    target 403
  ]
  edge [
    source 15
    target 404
  ]
  edge [
    source 15
    target 405
  ]
  edge [
    source 15
    target 406
  ]
  edge [
    source 15
    target 407
  ]
  edge [
    source 15
    target 408
  ]
  edge [
    source 15
    target 409
  ]
  edge [
    source 15
    target 410
  ]
  edge [
    source 15
    target 411
  ]
  edge [
    source 15
    target 412
  ]
  edge [
    source 15
    target 413
  ]
  edge [
    source 15
    target 414
  ]
  edge [
    source 15
    target 415
  ]
  edge [
    source 15
    target 211
  ]
  edge [
    source 15
    target 416
  ]
  edge [
    source 15
    target 520
  ]
  edge [
    source 15
    target 521
  ]
  edge [
    source 15
    target 522
  ]
  edge [
    source 15
    target 523
  ]
  edge [
    source 15
    target 524
  ]
  edge [
    source 15
    target 525
  ]
  edge [
    source 15
    target 526
  ]
  edge [
    source 15
    target 527
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 528
  ]
  edge [
    source 15
    target 529
  ]
  edge [
    source 15
    target 530
  ]
  edge [
    source 15
    target 531
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 534
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 536
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 15
    target 538
  ]
  edge [
    source 15
    target 539
  ]
  edge [
    source 15
    target 540
  ]
  edge [
    source 15
    target 541
  ]
  edge [
    source 15
    target 542
  ]
  edge [
    source 15
    target 543
  ]
  edge [
    source 15
    target 544
  ]
  edge [
    source 15
    target 545
  ]
  edge [
    source 15
    target 390
  ]
  edge [
    source 15
    target 546
  ]
  edge [
    source 15
    target 547
  ]
  edge [
    source 15
    target 548
  ]
  edge [
    source 15
    target 485
  ]
  edge [
    source 15
    target 492
  ]
  edge [
    source 15
    target 549
  ]
  edge [
    source 15
    target 550
  ]
  edge [
    source 15
    target 551
  ]
  edge [
    source 15
    target 552
  ]
  edge [
    source 15
    target 553
  ]
  edge [
    source 15
    target 554
  ]
  edge [
    source 15
    target 555
  ]
  edge [
    source 15
    target 556
  ]
  edge [
    source 15
    target 557
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 558
  ]
  edge [
    source 15
    target 559
  ]
  edge [
    source 15
    target 388
  ]
  edge [
    source 15
    target 560
  ]
  edge [
    source 15
    target 561
  ]
  edge [
    source 15
    target 562
  ]
  edge [
    source 15
    target 563
  ]
  edge [
    source 15
    target 564
  ]
  edge [
    source 15
    target 565
  ]
  edge [
    source 15
    target 566
  ]
  edge [
    source 15
    target 567
  ]
  edge [
    source 15
    target 568
  ]
  edge [
    source 15
    target 569
  ]
  edge [
    source 15
    target 570
  ]
  edge [
    source 15
    target 571
  ]
  edge [
    source 15
    target 572
  ]
  edge [
    source 15
    target 573
  ]
  edge [
    source 15
    target 574
  ]
  edge [
    source 15
    target 575
  ]
  edge [
    source 15
    target 576
  ]
  edge [
    source 15
    target 577
  ]
  edge [
    source 15
    target 578
  ]
  edge [
    source 15
    target 579
  ]
  edge [
    source 15
    target 580
  ]
  edge [
    source 15
    target 581
  ]
  edge [
    source 15
    target 582
  ]
  edge [
    source 15
    target 583
  ]
  edge [
    source 15
    target 584
  ]
  edge [
    source 15
    target 585
  ]
  edge [
    source 15
    target 586
  ]
  edge [
    source 15
    target 587
  ]
  edge [
    source 15
    target 588
  ]
  edge [
    source 15
    target 589
  ]
  edge [
    source 15
    target 590
  ]
  edge [
    source 15
    target 591
  ]
  edge [
    source 15
    target 592
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 231
  ]
  edge [
    source 15
    target 593
  ]
  edge [
    source 15
    target 594
  ]
  edge [
    source 15
    target 595
  ]
  edge [
    source 15
    target 596
  ]
  edge [
    source 15
    target 597
  ]
  edge [
    source 15
    target 598
  ]
  edge [
    source 15
    target 599
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 600
  ]
  edge [
    source 15
    target 601
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 602
  ]
  edge [
    source 16
    target 603
  ]
  edge [
    source 16
    target 604
  ]
  edge [
    source 16
    target 605
  ]
  edge [
    source 16
    target 606
  ]
  edge [
    source 16
    target 607
  ]
  edge [
    source 16
    target 608
  ]
  edge [
    source 16
    target 609
  ]
  edge [
    source 16
    target 610
  ]
  edge [
    source 16
    target 611
  ]
  edge [
    source 16
    target 612
  ]
  edge [
    source 16
    target 613
  ]
  edge [
    source 16
    target 614
  ]
  edge [
    source 16
    target 615
  ]
  edge [
    source 16
    target 616
  ]
  edge [
    source 16
    target 617
  ]
  edge [
    source 16
    target 618
  ]
  edge [
    source 16
    target 619
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 620
  ]
  edge [
    source 17
    target 621
  ]
  edge [
    source 17
    target 622
  ]
  edge [
    source 17
    target 623
  ]
  edge [
    source 17
    target 624
  ]
  edge [
    source 17
    target 625
  ]
  edge [
    source 17
    target 626
  ]
  edge [
    source 17
    target 627
  ]
  edge [
    source 17
    target 628
  ]
  edge [
    source 17
    target 629
  ]
  edge [
    source 17
    target 630
  ]
  edge [
    source 17
    target 631
  ]
  edge [
    source 17
    target 632
  ]
  edge [
    source 17
    target 633
  ]
  edge [
    source 17
    target 634
  ]
  edge [
    source 17
    target 635
  ]
  edge [
    source 17
    target 636
  ]
  edge [
    source 17
    target 637
  ]
  edge [
    source 17
    target 638
  ]
  edge [
    source 17
    target 58
  ]
  edge [
    source 17
    target 639
  ]
  edge [
    source 17
    target 640
  ]
  edge [
    source 17
    target 641
  ]
  edge [
    source 17
    target 642
  ]
  edge [
    source 17
    target 643
  ]
  edge [
    source 17
    target 644
  ]
  edge [
    source 17
    target 645
  ]
  edge [
    source 17
    target 646
  ]
  edge [
    source 17
    target 647
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 648
  ]
  edge [
    source 18
    target 649
  ]
  edge [
    source 18
    target 537
  ]
  edge [
    source 18
    target 650
  ]
  edge [
    source 18
    target 651
  ]
  edge [
    source 18
    target 529
  ]
  edge [
    source 18
    target 652
  ]
  edge [
    source 18
    target 653
  ]
  edge [
    source 18
    target 654
  ]
  edge [
    source 18
    target 655
  ]
  edge [
    source 18
    target 656
  ]
  edge [
    source 18
    target 657
  ]
  edge [
    source 18
    target 658
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 659
  ]
  edge [
    source 18
    target 660
  ]
  edge [
    source 18
    target 661
  ]
  edge [
    source 18
    target 662
  ]
  edge [
    source 18
    target 663
  ]
  edge [
    source 18
    target 393
  ]
  edge [
    source 18
    target 546
  ]
  edge [
    source 18
    target 664
  ]
  edge [
    source 18
    target 665
  ]
  edge [
    source 18
    target 666
  ]
  edge [
    source 18
    target 667
  ]
  edge [
    source 18
    target 668
  ]
  edge [
    source 18
    target 669
  ]
  edge [
    source 18
    target 670
  ]
  edge [
    source 18
    target 671
  ]
  edge [
    source 18
    target 672
  ]
  edge [
    source 18
    target 673
  ]
  edge [
    source 18
    target 674
  ]
  edge [
    source 18
    target 675
  ]
  edge [
    source 18
    target 676
  ]
  edge [
    source 18
    target 677
  ]
  edge [
    source 18
    target 678
  ]
  edge [
    source 18
    target 679
  ]
  edge [
    source 18
    target 680
  ]
  edge [
    source 18
    target 681
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 397
  ]
  edge [
    source 18
    target 398
  ]
  edge [
    source 18
    target 399
  ]
  edge [
    source 18
    target 400
  ]
  edge [
    source 18
    target 401
  ]
  edge [
    source 18
    target 402
  ]
  edge [
    source 18
    target 403
  ]
  edge [
    source 18
    target 404
  ]
  edge [
    source 18
    target 405
  ]
  edge [
    source 18
    target 406
  ]
  edge [
    source 18
    target 407
  ]
  edge [
    source 18
    target 408
  ]
  edge [
    source 18
    target 409
  ]
  edge [
    source 18
    target 410
  ]
  edge [
    source 18
    target 411
  ]
  edge [
    source 18
    target 412
  ]
  edge [
    source 18
    target 413
  ]
  edge [
    source 18
    target 414
  ]
  edge [
    source 18
    target 415
  ]
  edge [
    source 18
    target 416
  ]
  edge [
    source 18
    target 682
  ]
  edge [
    source 18
    target 683
  ]
  edge [
    source 18
    target 684
  ]
  edge [
    source 18
    target 685
  ]
  edge [
    source 18
    target 686
  ]
  edge [
    source 18
    target 687
  ]
  edge [
    source 18
    target 688
  ]
  edge [
    source 18
    target 689
  ]
  edge [
    source 18
    target 293
  ]
  edge [
    source 18
    target 565
  ]
  edge [
    source 18
    target 690
  ]
  edge [
    source 18
    target 691
  ]
  edge [
    source 18
    target 692
  ]
  edge [
    source 18
    target 297
  ]
  edge [
    source 18
    target 693
  ]
  edge [
    source 18
    target 694
  ]
  edge [
    source 18
    target 695
  ]
  edge [
    source 18
    target 696
  ]
  edge [
    source 18
    target 697
  ]
  edge [
    source 18
    target 698
  ]
  edge [
    source 18
    target 699
  ]
  edge [
    source 18
    target 700
  ]
  edge [
    source 18
    target 701
  ]
  edge [
    source 18
    target 702
  ]
  edge [
    source 18
    target 469
  ]
  edge [
    source 18
    target 703
  ]
  edge [
    source 18
    target 704
  ]
  edge [
    source 18
    target 705
  ]
  edge [
    source 18
    target 706
  ]
  edge [
    source 18
    target 707
  ]
  edge [
    source 18
    target 708
  ]
  edge [
    source 18
    target 709
  ]
  edge [
    source 18
    target 710
  ]
  edge [
    source 18
    target 711
  ]
  edge [
    source 18
    target 712
  ]
  edge [
    source 18
    target 713
  ]
  edge [
    source 18
    target 520
  ]
  edge [
    source 18
    target 714
  ]
  edge [
    source 18
    target 540
  ]
  edge [
    source 18
    target 715
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 716
  ]
  edge [
    source 18
    target 717
  ]
  edge [
    source 18
    target 718
  ]
  edge [
    source 18
    target 719
  ]
  edge [
    source 18
    target 720
  ]
  edge [
    source 18
    target 721
  ]
  edge [
    source 18
    target 722
  ]
  edge [
    source 18
    target 723
  ]
  edge [
    source 18
    target 724
  ]
  edge [
    source 18
    target 725
  ]
  edge [
    source 18
    target 726
  ]
  edge [
    source 18
    target 727
  ]
  edge [
    source 18
    target 728
  ]
  edge [
    source 18
    target 729
  ]
  edge [
    source 18
    target 730
  ]
  edge [
    source 18
    target 731
  ]
  edge [
    source 18
    target 732
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 733
  ]
  edge [
    source 18
    target 734
  ]
  edge [
    source 18
    target 735
  ]
  edge [
    source 18
    target 736
  ]
  edge [
    source 18
    target 737
  ]
  edge [
    source 18
    target 738
  ]
  edge [
    source 18
    target 739
  ]
  edge [
    source 18
    target 740
  ]
  edge [
    source 18
    target 741
  ]
  edge [
    source 18
    target 742
  ]
  edge [
    source 18
    target 743
  ]
  edge [
    source 18
    target 744
  ]
  edge [
    source 18
    target 745
  ]
  edge [
    source 18
    target 746
  ]
  edge [
    source 18
    target 747
  ]
  edge [
    source 18
    target 748
  ]
  edge [
    source 18
    target 749
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 750
  ]
  edge [
    source 19
    target 751
  ]
  edge [
    source 19
    target 386
  ]
  edge [
    source 19
    target 562
  ]
  edge [
    source 19
    target 752
  ]
  edge [
    source 19
    target 753
  ]
  edge [
    source 19
    target 554
  ]
  edge [
    source 19
    target 754
  ]
  edge [
    source 19
    target 755
  ]
  edge [
    source 19
    target 756
  ]
  edge [
    source 19
    target 757
  ]
  edge [
    source 19
    target 666
  ]
  edge [
    source 19
    target 758
  ]
  edge [
    source 19
    target 759
  ]
  edge [
    source 19
    target 679
  ]
  edge [
    source 19
    target 760
  ]
  edge [
    source 19
    target 761
  ]
  edge [
    source 19
    target 762
  ]
  edge [
    source 19
    target 763
  ]
  edge [
    source 19
    target 764
  ]
  edge [
    source 19
    target 463
  ]
  edge [
    source 19
    target 765
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 766
  ]
  edge [
    source 19
    target 380
  ]
  edge [
    source 19
    target 767
  ]
  edge [
    source 19
    target 768
  ]
  edge [
    source 19
    target 769
  ]
  edge [
    source 19
    target 770
  ]
  edge [
    source 19
    target 771
  ]
  edge [
    source 19
    target 772
  ]
  edge [
    source 19
    target 413
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 773
  ]
  edge [
    source 19
    target 774
  ]
  edge [
    source 19
    target 775
  ]
  edge [
    source 19
    target 776
  ]
  edge [
    source 19
    target 613
  ]
  edge [
    source 19
    target 777
  ]
  edge [
    source 19
    target 778
  ]
  edge [
    source 19
    target 779
  ]
  edge [
    source 19
    target 780
  ]
  edge [
    source 19
    target 408
  ]
  edge [
    source 19
    target 781
  ]
  edge [
    source 19
    target 782
  ]
  edge [
    source 19
    target 783
  ]
  edge [
    source 19
    target 784
  ]
  edge [
    source 19
    target 785
  ]
  edge [
    source 19
    target 786
  ]
  edge [
    source 19
    target 787
  ]
  edge [
    source 19
    target 788
  ]
  edge [
    source 19
    target 524
  ]
  edge [
    source 19
    target 789
  ]
  edge [
    source 19
    target 790
  ]
  edge [
    source 19
    target 406
  ]
  edge [
    source 19
    target 397
  ]
  edge [
    source 19
    target 398
  ]
  edge [
    source 19
    target 399
  ]
  edge [
    source 19
    target 400
  ]
  edge [
    source 19
    target 401
  ]
  edge [
    source 19
    target 402
  ]
  edge [
    source 19
    target 403
  ]
  edge [
    source 19
    target 404
  ]
  edge [
    source 19
    target 405
  ]
  edge [
    source 19
    target 407
  ]
  edge [
    source 19
    target 409
  ]
  edge [
    source 19
    target 410
  ]
  edge [
    source 19
    target 411
  ]
  edge [
    source 19
    target 412
  ]
  edge [
    source 19
    target 414
  ]
  edge [
    source 19
    target 415
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 416
  ]
  edge [
    source 19
    target 791
  ]
  edge [
    source 19
    target 792
  ]
  edge [
    source 19
    target 793
  ]
  edge [
    source 19
    target 794
  ]
  edge [
    source 19
    target 795
  ]
  edge [
    source 19
    target 796
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 19
    target 797
  ]
  edge [
    source 19
    target 798
  ]
  edge [
    source 19
    target 799
  ]
  edge [
    source 19
    target 800
  ]
  edge [
    source 19
    target 801
  ]
  edge [
    source 19
    target 802
  ]
  edge [
    source 19
    target 58
  ]
  edge [
    source 19
    target 803
  ]
  edge [
    source 19
    target 804
  ]
  edge [
    source 19
    target 805
  ]
  edge [
    source 19
    target 806
  ]
  edge [
    source 19
    target 807
  ]
  edge [
    source 19
    target 808
  ]
  edge [
    source 19
    target 809
  ]
  edge [
    source 19
    target 810
  ]
  edge [
    source 19
    target 811
  ]
  edge [
    source 19
    target 812
  ]
  edge [
    source 19
    target 813
  ]
  edge [
    source 19
    target 814
  ]
  edge [
    source 19
    target 815
  ]
  edge [
    source 19
    target 816
  ]
  edge [
    source 19
    target 817
  ]
  edge [
    source 19
    target 818
  ]
  edge [
    source 19
    target 819
  ]
  edge [
    source 19
    target 820
  ]
  edge [
    source 19
    target 821
  ]
  edge [
    source 19
    target 822
  ]
  edge [
    source 19
    target 823
  ]
  edge [
    source 19
    target 824
  ]
  edge [
    source 19
    target 825
  ]
  edge [
    source 19
    target 826
  ]
  edge [
    source 19
    target 827
  ]
  edge [
    source 19
    target 828
  ]
  edge [
    source 19
    target 330
  ]
  edge [
    source 19
    target 829
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 830
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 831
  ]
  edge [
    source 20
    target 832
  ]
  edge [
    source 20
    target 833
  ]
  edge [
    source 20
    target 834
  ]
  edge [
    source 20
    target 835
  ]
  edge [
    source 20
    target 836
  ]
  edge [
    source 20
    target 837
  ]
  edge [
    source 20
    target 838
  ]
  edge [
    source 20
    target 839
  ]
  edge [
    source 20
    target 840
  ]
  edge [
    source 20
    target 841
  ]
  edge [
    source 20
    target 842
  ]
  edge [
    source 20
    target 843
  ]
  edge [
    source 20
    target 844
  ]
]
