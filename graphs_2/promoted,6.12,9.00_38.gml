graph [
  node [
    id 0
    label "electronic"
    origin "text"
  ]
  node [
    id 1
    label "frontier"
    origin "text"
  ]
  node [
    id 2
    label "foundation"
    origin "text"
  ]
  node [
    id 3
    label "kr&#243;tko"
    origin "text"
  ]
  node [
    id 4
    label "temat"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "dzieje"
    origin "text"
  ]
  node [
    id 7
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 8
    label "dyrektywa"
    origin "text"
  ]
  node [
    id 9
    label "prawo"
    origin "text"
  ]
  node [
    id 10
    label "autorski"
    origin "text"
  ]
  node [
    id 11
    label "polska"
    origin "text"
  ]
  node [
    id 12
    label "kr&#243;tki"
  ]
  node [
    id 13
    label "szybki"
  ]
  node [
    id 14
    label "jednowyrazowy"
  ]
  node [
    id 15
    label "bliski"
  ]
  node [
    id 16
    label "s&#322;aby"
  ]
  node [
    id 17
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 18
    label "drobny"
  ]
  node [
    id 19
    label "ruch"
  ]
  node [
    id 20
    label "brak"
  ]
  node [
    id 21
    label "z&#322;y"
  ]
  node [
    id 22
    label "sprawa"
  ]
  node [
    id 23
    label "wyraz_pochodny"
  ]
  node [
    id 24
    label "zboczenie"
  ]
  node [
    id 25
    label "om&#243;wienie"
  ]
  node [
    id 26
    label "cecha"
  ]
  node [
    id 27
    label "rzecz"
  ]
  node [
    id 28
    label "omawia&#263;"
  ]
  node [
    id 29
    label "fraza"
  ]
  node [
    id 30
    label "tre&#347;&#263;"
  ]
  node [
    id 31
    label "entity"
  ]
  node [
    id 32
    label "forum"
  ]
  node [
    id 33
    label "topik"
  ]
  node [
    id 34
    label "tematyka"
  ]
  node [
    id 35
    label "w&#261;tek"
  ]
  node [
    id 36
    label "zbaczanie"
  ]
  node [
    id 37
    label "forma"
  ]
  node [
    id 38
    label "om&#243;wi&#263;"
  ]
  node [
    id 39
    label "omawianie"
  ]
  node [
    id 40
    label "melodia"
  ]
  node [
    id 41
    label "otoczka"
  ]
  node [
    id 42
    label "istota"
  ]
  node [
    id 43
    label "zbacza&#263;"
  ]
  node [
    id 44
    label "zboczy&#263;"
  ]
  node [
    id 45
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 46
    label "zbi&#243;r"
  ]
  node [
    id 47
    label "wypowiedzenie"
  ]
  node [
    id 48
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 49
    label "zdanie"
  ]
  node [
    id 50
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 51
    label "motyw"
  ]
  node [
    id 52
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 53
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 54
    label "informacja"
  ]
  node [
    id 55
    label "zawarto&#347;&#263;"
  ]
  node [
    id 56
    label "kszta&#322;t"
  ]
  node [
    id 57
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 58
    label "jednostka_systematyczna"
  ]
  node [
    id 59
    label "poznanie"
  ]
  node [
    id 60
    label "leksem"
  ]
  node [
    id 61
    label "dzie&#322;o"
  ]
  node [
    id 62
    label "stan"
  ]
  node [
    id 63
    label "blaszka"
  ]
  node [
    id 64
    label "poj&#281;cie"
  ]
  node [
    id 65
    label "kantyzm"
  ]
  node [
    id 66
    label "zdolno&#347;&#263;"
  ]
  node [
    id 67
    label "do&#322;ek"
  ]
  node [
    id 68
    label "gwiazda"
  ]
  node [
    id 69
    label "formality"
  ]
  node [
    id 70
    label "struktura"
  ]
  node [
    id 71
    label "wygl&#261;d"
  ]
  node [
    id 72
    label "mode"
  ]
  node [
    id 73
    label "morfem"
  ]
  node [
    id 74
    label "rdze&#324;"
  ]
  node [
    id 75
    label "posta&#263;"
  ]
  node [
    id 76
    label "kielich"
  ]
  node [
    id 77
    label "ornamentyka"
  ]
  node [
    id 78
    label "pasmo"
  ]
  node [
    id 79
    label "zwyczaj"
  ]
  node [
    id 80
    label "punkt_widzenia"
  ]
  node [
    id 81
    label "g&#322;owa"
  ]
  node [
    id 82
    label "naczynie"
  ]
  node [
    id 83
    label "p&#322;at"
  ]
  node [
    id 84
    label "maszyna_drukarska"
  ]
  node [
    id 85
    label "obiekt"
  ]
  node [
    id 86
    label "style"
  ]
  node [
    id 87
    label "linearno&#347;&#263;"
  ]
  node [
    id 88
    label "wyra&#380;enie"
  ]
  node [
    id 89
    label "formacja"
  ]
  node [
    id 90
    label "spirala"
  ]
  node [
    id 91
    label "dyspozycja"
  ]
  node [
    id 92
    label "odmiana"
  ]
  node [
    id 93
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 94
    label "wz&#243;r"
  ]
  node [
    id 95
    label "October"
  ]
  node [
    id 96
    label "creation"
  ]
  node [
    id 97
    label "p&#281;tla"
  ]
  node [
    id 98
    label "arystotelizm"
  ]
  node [
    id 99
    label "szablon"
  ]
  node [
    id 100
    label "miniatura"
  ]
  node [
    id 101
    label "zanucenie"
  ]
  node [
    id 102
    label "nuta"
  ]
  node [
    id 103
    label "zakosztowa&#263;"
  ]
  node [
    id 104
    label "zajawka"
  ]
  node [
    id 105
    label "zanuci&#263;"
  ]
  node [
    id 106
    label "emocja"
  ]
  node [
    id 107
    label "oskoma"
  ]
  node [
    id 108
    label "melika"
  ]
  node [
    id 109
    label "nucenie"
  ]
  node [
    id 110
    label "nuci&#263;"
  ]
  node [
    id 111
    label "brzmienie"
  ]
  node [
    id 112
    label "zjawisko"
  ]
  node [
    id 113
    label "taste"
  ]
  node [
    id 114
    label "muzyka"
  ]
  node [
    id 115
    label "inclination"
  ]
  node [
    id 116
    label "charakterystyka"
  ]
  node [
    id 117
    label "m&#322;ot"
  ]
  node [
    id 118
    label "znak"
  ]
  node [
    id 119
    label "drzewo"
  ]
  node [
    id 120
    label "pr&#243;ba"
  ]
  node [
    id 121
    label "attribute"
  ]
  node [
    id 122
    label "marka"
  ]
  node [
    id 123
    label "matter"
  ]
  node [
    id 124
    label "splot"
  ]
  node [
    id 125
    label "wytw&#243;r"
  ]
  node [
    id 126
    label "ceg&#322;a"
  ]
  node [
    id 127
    label "socket"
  ]
  node [
    id 128
    label "rozmieszczenie"
  ]
  node [
    id 129
    label "fabu&#322;a"
  ]
  node [
    id 130
    label "mentalno&#347;&#263;"
  ]
  node [
    id 131
    label "superego"
  ]
  node [
    id 132
    label "psychika"
  ]
  node [
    id 133
    label "znaczenie"
  ]
  node [
    id 134
    label "wn&#281;trze"
  ]
  node [
    id 135
    label "charakter"
  ]
  node [
    id 136
    label "okrywa"
  ]
  node [
    id 137
    label "kontekst"
  ]
  node [
    id 138
    label "object"
  ]
  node [
    id 139
    label "przedmiot"
  ]
  node [
    id 140
    label "wpadni&#281;cie"
  ]
  node [
    id 141
    label "mienie"
  ]
  node [
    id 142
    label "przyroda"
  ]
  node [
    id 143
    label "kultura"
  ]
  node [
    id 144
    label "wpa&#347;&#263;"
  ]
  node [
    id 145
    label "wpadanie"
  ]
  node [
    id 146
    label "wpada&#263;"
  ]
  node [
    id 147
    label "discussion"
  ]
  node [
    id 148
    label "rozpatrywanie"
  ]
  node [
    id 149
    label "dyskutowanie"
  ]
  node [
    id 150
    label "swerve"
  ]
  node [
    id 151
    label "kierunek"
  ]
  node [
    id 152
    label "digress"
  ]
  node [
    id 153
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 154
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 155
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 156
    label "odchodzi&#263;"
  ]
  node [
    id 157
    label "twist"
  ]
  node [
    id 158
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 159
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 160
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 161
    label "omowny"
  ]
  node [
    id 162
    label "figura_stylistyczna"
  ]
  node [
    id 163
    label "sformu&#322;owanie"
  ]
  node [
    id 164
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 165
    label "odchodzenie"
  ]
  node [
    id 166
    label "aberrance"
  ]
  node [
    id 167
    label "dyskutowa&#263;"
  ]
  node [
    id 168
    label "formu&#322;owa&#263;"
  ]
  node [
    id 169
    label "discourse"
  ]
  node [
    id 170
    label "perversion"
  ]
  node [
    id 171
    label "death"
  ]
  node [
    id 172
    label "odej&#347;cie"
  ]
  node [
    id 173
    label "turn"
  ]
  node [
    id 174
    label "k&#261;t"
  ]
  node [
    id 175
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 176
    label "odchylenie_si&#281;"
  ]
  node [
    id 177
    label "deviation"
  ]
  node [
    id 178
    label "patologia"
  ]
  node [
    id 179
    label "przedyskutowa&#263;"
  ]
  node [
    id 180
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 181
    label "publicize"
  ]
  node [
    id 182
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 183
    label "distract"
  ]
  node [
    id 184
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 185
    label "odej&#347;&#263;"
  ]
  node [
    id 186
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 187
    label "zmieni&#263;"
  ]
  node [
    id 188
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 189
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 190
    label "kognicja"
  ]
  node [
    id 191
    label "rozprawa"
  ]
  node [
    id 192
    label "wydarzenie"
  ]
  node [
    id 193
    label "szczeg&#243;&#322;"
  ]
  node [
    id 194
    label "proposition"
  ]
  node [
    id 195
    label "przes&#322;anka"
  ]
  node [
    id 196
    label "idea"
  ]
  node [
    id 197
    label "paj&#261;k"
  ]
  node [
    id 198
    label "przewodnik"
  ]
  node [
    id 199
    label "odcinek"
  ]
  node [
    id 200
    label "topikowate"
  ]
  node [
    id 201
    label "grupa_dyskusyjna"
  ]
  node [
    id 202
    label "s&#261;d"
  ]
  node [
    id 203
    label "plac"
  ]
  node [
    id 204
    label "bazylika"
  ]
  node [
    id 205
    label "przestrze&#324;"
  ]
  node [
    id 206
    label "miejsce"
  ]
  node [
    id 207
    label "portal"
  ]
  node [
    id 208
    label "konferencja"
  ]
  node [
    id 209
    label "agora"
  ]
  node [
    id 210
    label "grupa"
  ]
  node [
    id 211
    label "strona"
  ]
  node [
    id 212
    label "epoka"
  ]
  node [
    id 213
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 214
    label "koleje_losu"
  ]
  node [
    id 215
    label "&#380;ycie"
  ]
  node [
    id 216
    label "czas"
  ]
  node [
    id 217
    label "aalen"
  ]
  node [
    id 218
    label "jura_wczesna"
  ]
  node [
    id 219
    label "holocen"
  ]
  node [
    id 220
    label "pliocen"
  ]
  node [
    id 221
    label "plejstocen"
  ]
  node [
    id 222
    label "paleocen"
  ]
  node [
    id 223
    label "bajos"
  ]
  node [
    id 224
    label "kelowej"
  ]
  node [
    id 225
    label "eocen"
  ]
  node [
    id 226
    label "jednostka_geologiczna"
  ]
  node [
    id 227
    label "okres"
  ]
  node [
    id 228
    label "schy&#322;ek"
  ]
  node [
    id 229
    label "miocen"
  ]
  node [
    id 230
    label "&#347;rodkowy_trias"
  ]
  node [
    id 231
    label "term"
  ]
  node [
    id 232
    label "Zeitgeist"
  ]
  node [
    id 233
    label "jura_p&#243;&#378;na"
  ]
  node [
    id 234
    label "wczesny_trias"
  ]
  node [
    id 235
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 236
    label "jura_&#347;rodkowa"
  ]
  node [
    id 237
    label "oligocen"
  ]
  node [
    id 238
    label "odwadnia&#263;"
  ]
  node [
    id 239
    label "wi&#261;zanie"
  ]
  node [
    id 240
    label "odwodni&#263;"
  ]
  node [
    id 241
    label "bratnia_dusza"
  ]
  node [
    id 242
    label "powi&#261;zanie"
  ]
  node [
    id 243
    label "zwi&#261;zanie"
  ]
  node [
    id 244
    label "konstytucja"
  ]
  node [
    id 245
    label "organizacja"
  ]
  node [
    id 246
    label "marriage"
  ]
  node [
    id 247
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 248
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 249
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 250
    label "zwi&#261;za&#263;"
  ]
  node [
    id 251
    label "odwadnianie"
  ]
  node [
    id 252
    label "odwodnienie"
  ]
  node [
    id 253
    label "marketing_afiliacyjny"
  ]
  node [
    id 254
    label "substancja_chemiczna"
  ]
  node [
    id 255
    label "koligacja"
  ]
  node [
    id 256
    label "bearing"
  ]
  node [
    id 257
    label "lokant"
  ]
  node [
    id 258
    label "azeotrop"
  ]
  node [
    id 259
    label "odprowadza&#263;"
  ]
  node [
    id 260
    label "drain"
  ]
  node [
    id 261
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 262
    label "cia&#322;o"
  ]
  node [
    id 263
    label "powodowa&#263;"
  ]
  node [
    id 264
    label "osusza&#263;"
  ]
  node [
    id 265
    label "odci&#261;ga&#263;"
  ]
  node [
    id 266
    label "odsuwa&#263;"
  ]
  node [
    id 267
    label "akt"
  ]
  node [
    id 268
    label "cezar"
  ]
  node [
    id 269
    label "dokument"
  ]
  node [
    id 270
    label "budowa"
  ]
  node [
    id 271
    label "uchwa&#322;a"
  ]
  node [
    id 272
    label "numeracja"
  ]
  node [
    id 273
    label "odprowadzanie"
  ]
  node [
    id 274
    label "powodowanie"
  ]
  node [
    id 275
    label "odci&#261;ganie"
  ]
  node [
    id 276
    label "dehydratacja"
  ]
  node [
    id 277
    label "osuszanie"
  ]
  node [
    id 278
    label "proces_chemiczny"
  ]
  node [
    id 279
    label "odsuwanie"
  ]
  node [
    id 280
    label "odsun&#261;&#263;"
  ]
  node [
    id 281
    label "spowodowa&#263;"
  ]
  node [
    id 282
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 283
    label "odprowadzi&#263;"
  ]
  node [
    id 284
    label "osuszy&#263;"
  ]
  node [
    id 285
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 286
    label "dehydration"
  ]
  node [
    id 287
    label "oznaka"
  ]
  node [
    id 288
    label "osuszenie"
  ]
  node [
    id 289
    label "spowodowanie"
  ]
  node [
    id 290
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 291
    label "odprowadzenie"
  ]
  node [
    id 292
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 293
    label "odsuni&#281;cie"
  ]
  node [
    id 294
    label "narta"
  ]
  node [
    id 295
    label "podwi&#261;zywanie"
  ]
  node [
    id 296
    label "dressing"
  ]
  node [
    id 297
    label "szermierka"
  ]
  node [
    id 298
    label "przywi&#261;zywanie"
  ]
  node [
    id 299
    label "pakowanie"
  ]
  node [
    id 300
    label "my&#347;lenie"
  ]
  node [
    id 301
    label "do&#322;&#261;czanie"
  ]
  node [
    id 302
    label "communication"
  ]
  node [
    id 303
    label "wytwarzanie"
  ]
  node [
    id 304
    label "cement"
  ]
  node [
    id 305
    label "combination"
  ]
  node [
    id 306
    label "zobowi&#261;zywanie"
  ]
  node [
    id 307
    label "szcz&#281;ka"
  ]
  node [
    id 308
    label "anga&#380;owanie"
  ]
  node [
    id 309
    label "wi&#261;za&#263;"
  ]
  node [
    id 310
    label "twardnienie"
  ]
  node [
    id 311
    label "tobo&#322;ek"
  ]
  node [
    id 312
    label "podwi&#261;zanie"
  ]
  node [
    id 313
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 314
    label "przywi&#261;zanie"
  ]
  node [
    id 315
    label "przymocowywanie"
  ]
  node [
    id 316
    label "scalanie"
  ]
  node [
    id 317
    label "mezomeria"
  ]
  node [
    id 318
    label "wi&#281;&#378;"
  ]
  node [
    id 319
    label "fusion"
  ]
  node [
    id 320
    label "kojarzenie_si&#281;"
  ]
  node [
    id 321
    label "&#322;&#261;czenie"
  ]
  node [
    id 322
    label "uchwyt"
  ]
  node [
    id 323
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 324
    label "zmiana"
  ]
  node [
    id 325
    label "element_konstrukcyjny"
  ]
  node [
    id 326
    label "obezw&#322;adnianie"
  ]
  node [
    id 327
    label "manewr"
  ]
  node [
    id 328
    label "miecz"
  ]
  node [
    id 329
    label "oddzia&#322;ywanie"
  ]
  node [
    id 330
    label "obwi&#261;zanie"
  ]
  node [
    id 331
    label "zawi&#261;zek"
  ]
  node [
    id 332
    label "obwi&#261;zywanie"
  ]
  node [
    id 333
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 334
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 335
    label "w&#281;ze&#322;"
  ]
  node [
    id 336
    label "consort"
  ]
  node [
    id 337
    label "opakowa&#263;"
  ]
  node [
    id 338
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 339
    label "relate"
  ]
  node [
    id 340
    label "form"
  ]
  node [
    id 341
    label "unify"
  ]
  node [
    id 342
    label "incorporate"
  ]
  node [
    id 343
    label "bind"
  ]
  node [
    id 344
    label "zawi&#261;za&#263;"
  ]
  node [
    id 345
    label "zaprawa"
  ]
  node [
    id 346
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 347
    label "powi&#261;za&#263;"
  ]
  node [
    id 348
    label "scali&#263;"
  ]
  node [
    id 349
    label "zatrzyma&#263;"
  ]
  node [
    id 350
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 351
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 352
    label "ograniczenie"
  ]
  node [
    id 353
    label "po&#322;&#261;czenie"
  ]
  node [
    id 354
    label "do&#322;&#261;czenie"
  ]
  node [
    id 355
    label "opakowanie"
  ]
  node [
    id 356
    label "attachment"
  ]
  node [
    id 357
    label "obezw&#322;adnienie"
  ]
  node [
    id 358
    label "zawi&#261;zanie"
  ]
  node [
    id 359
    label "tying"
  ]
  node [
    id 360
    label "st&#281;&#380;enie"
  ]
  node [
    id 361
    label "affiliation"
  ]
  node [
    id 362
    label "fastening"
  ]
  node [
    id 363
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 364
    label "z&#322;&#261;czenie"
  ]
  node [
    id 365
    label "zobowi&#261;zanie"
  ]
  node [
    id 366
    label "roztw&#243;r"
  ]
  node [
    id 367
    label "podmiot"
  ]
  node [
    id 368
    label "jednostka_organizacyjna"
  ]
  node [
    id 369
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 370
    label "TOPR"
  ]
  node [
    id 371
    label "endecki"
  ]
  node [
    id 372
    label "zesp&#243;&#322;"
  ]
  node [
    id 373
    label "przedstawicielstwo"
  ]
  node [
    id 374
    label "od&#322;am"
  ]
  node [
    id 375
    label "Cepelia"
  ]
  node [
    id 376
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 377
    label "ZBoWiD"
  ]
  node [
    id 378
    label "organization"
  ]
  node [
    id 379
    label "centrala"
  ]
  node [
    id 380
    label "GOPR"
  ]
  node [
    id 381
    label "ZOMO"
  ]
  node [
    id 382
    label "ZMP"
  ]
  node [
    id 383
    label "komitet_koordynacyjny"
  ]
  node [
    id 384
    label "przybud&#243;wka"
  ]
  node [
    id 385
    label "boj&#243;wka"
  ]
  node [
    id 386
    label "zrelatywizowa&#263;"
  ]
  node [
    id 387
    label "zrelatywizowanie"
  ]
  node [
    id 388
    label "mention"
  ]
  node [
    id 389
    label "pomy&#347;lenie"
  ]
  node [
    id 390
    label "relatywizowa&#263;"
  ]
  node [
    id 391
    label "relatywizowanie"
  ]
  node [
    id 392
    label "kontakt"
  ]
  node [
    id 393
    label "guidance"
  ]
  node [
    id 394
    label "polecenie"
  ]
  node [
    id 395
    label "ukaz"
  ]
  node [
    id 396
    label "pognanie"
  ]
  node [
    id 397
    label "rekomendacja"
  ]
  node [
    id 398
    label "wypowied&#378;"
  ]
  node [
    id 399
    label "pobiegni&#281;cie"
  ]
  node [
    id 400
    label "education"
  ]
  node [
    id 401
    label "doradzenie"
  ]
  node [
    id 402
    label "statement"
  ]
  node [
    id 403
    label "recommendation"
  ]
  node [
    id 404
    label "zadanie"
  ]
  node [
    id 405
    label "zaordynowanie"
  ]
  node [
    id 406
    label "powierzenie"
  ]
  node [
    id 407
    label "przesadzenie"
  ]
  node [
    id 408
    label "consign"
  ]
  node [
    id 409
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 410
    label "umocowa&#263;"
  ]
  node [
    id 411
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 412
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 413
    label "procesualistyka"
  ]
  node [
    id 414
    label "regu&#322;a_Allena"
  ]
  node [
    id 415
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 416
    label "kryminalistyka"
  ]
  node [
    id 417
    label "szko&#322;a"
  ]
  node [
    id 418
    label "zasada_d'Alemberta"
  ]
  node [
    id 419
    label "obserwacja"
  ]
  node [
    id 420
    label "normatywizm"
  ]
  node [
    id 421
    label "jurisprudence"
  ]
  node [
    id 422
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 423
    label "kultura_duchowa"
  ]
  node [
    id 424
    label "przepis"
  ]
  node [
    id 425
    label "prawo_karne_procesowe"
  ]
  node [
    id 426
    label "criterion"
  ]
  node [
    id 427
    label "kazuistyka"
  ]
  node [
    id 428
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 429
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 430
    label "kryminologia"
  ]
  node [
    id 431
    label "opis"
  ]
  node [
    id 432
    label "regu&#322;a_Glogera"
  ]
  node [
    id 433
    label "prawo_Mendla"
  ]
  node [
    id 434
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 435
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 436
    label "prawo_karne"
  ]
  node [
    id 437
    label "legislacyjnie"
  ]
  node [
    id 438
    label "twierdzenie"
  ]
  node [
    id 439
    label "cywilistyka"
  ]
  node [
    id 440
    label "judykatura"
  ]
  node [
    id 441
    label "kanonistyka"
  ]
  node [
    id 442
    label "standard"
  ]
  node [
    id 443
    label "nauka_prawa"
  ]
  node [
    id 444
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 445
    label "law"
  ]
  node [
    id 446
    label "qualification"
  ]
  node [
    id 447
    label "dominion"
  ]
  node [
    id 448
    label "wykonawczy"
  ]
  node [
    id 449
    label "zasada"
  ]
  node [
    id 450
    label "normalizacja"
  ]
  node [
    id 451
    label "exposition"
  ]
  node [
    id 452
    label "czynno&#347;&#263;"
  ]
  node [
    id 453
    label "obja&#347;nienie"
  ]
  node [
    id 454
    label "model"
  ]
  node [
    id 455
    label "organizowa&#263;"
  ]
  node [
    id 456
    label "ordinariness"
  ]
  node [
    id 457
    label "instytucja"
  ]
  node [
    id 458
    label "zorganizowa&#263;"
  ]
  node [
    id 459
    label "taniec_towarzyski"
  ]
  node [
    id 460
    label "organizowanie"
  ]
  node [
    id 461
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 462
    label "zorganizowanie"
  ]
  node [
    id 463
    label "mechanika"
  ]
  node [
    id 464
    label "o&#347;"
  ]
  node [
    id 465
    label "usenet"
  ]
  node [
    id 466
    label "rozprz&#261;c"
  ]
  node [
    id 467
    label "zachowanie"
  ]
  node [
    id 468
    label "cybernetyk"
  ]
  node [
    id 469
    label "podsystem"
  ]
  node [
    id 470
    label "system"
  ]
  node [
    id 471
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 472
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 473
    label "sk&#322;ad"
  ]
  node [
    id 474
    label "systemat"
  ]
  node [
    id 475
    label "konstrukcja"
  ]
  node [
    id 476
    label "konstelacja"
  ]
  node [
    id 477
    label "przebieg"
  ]
  node [
    id 478
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 479
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 480
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 481
    label "praktyka"
  ]
  node [
    id 482
    label "przeorientowywanie"
  ]
  node [
    id 483
    label "studia"
  ]
  node [
    id 484
    label "linia"
  ]
  node [
    id 485
    label "bok"
  ]
  node [
    id 486
    label "skr&#281;canie"
  ]
  node [
    id 487
    label "skr&#281;ca&#263;"
  ]
  node [
    id 488
    label "przeorientowywa&#263;"
  ]
  node [
    id 489
    label "orientowanie"
  ]
  node [
    id 490
    label "skr&#281;ci&#263;"
  ]
  node [
    id 491
    label "przeorientowanie"
  ]
  node [
    id 492
    label "zorientowanie"
  ]
  node [
    id 493
    label "przeorientowa&#263;"
  ]
  node [
    id 494
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 495
    label "metoda"
  ]
  node [
    id 496
    label "ty&#322;"
  ]
  node [
    id 497
    label "zorientowa&#263;"
  ]
  node [
    id 498
    label "g&#243;ra"
  ]
  node [
    id 499
    label "orientowa&#263;"
  ]
  node [
    id 500
    label "spos&#243;b"
  ]
  node [
    id 501
    label "ideologia"
  ]
  node [
    id 502
    label "orientacja"
  ]
  node [
    id 503
    label "prz&#243;d"
  ]
  node [
    id 504
    label "skr&#281;cenie"
  ]
  node [
    id 505
    label "do&#347;wiadczenie"
  ]
  node [
    id 506
    label "teren_szko&#322;y"
  ]
  node [
    id 507
    label "wiedza"
  ]
  node [
    id 508
    label "Mickiewicz"
  ]
  node [
    id 509
    label "kwalifikacje"
  ]
  node [
    id 510
    label "podr&#281;cznik"
  ]
  node [
    id 511
    label "absolwent"
  ]
  node [
    id 512
    label "school"
  ]
  node [
    id 513
    label "zda&#263;"
  ]
  node [
    id 514
    label "gabinet"
  ]
  node [
    id 515
    label "urszulanki"
  ]
  node [
    id 516
    label "sztuba"
  ]
  node [
    id 517
    label "&#322;awa_szkolna"
  ]
  node [
    id 518
    label "nauka"
  ]
  node [
    id 519
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 520
    label "przepisa&#263;"
  ]
  node [
    id 521
    label "klasa"
  ]
  node [
    id 522
    label "lekcja"
  ]
  node [
    id 523
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 524
    label "przepisanie"
  ]
  node [
    id 525
    label "skolaryzacja"
  ]
  node [
    id 526
    label "stopek"
  ]
  node [
    id 527
    label "sekretariat"
  ]
  node [
    id 528
    label "lesson"
  ]
  node [
    id 529
    label "niepokalanki"
  ]
  node [
    id 530
    label "siedziba"
  ]
  node [
    id 531
    label "szkolenie"
  ]
  node [
    id 532
    label "kara"
  ]
  node [
    id 533
    label "tablica"
  ]
  node [
    id 534
    label "posiada&#263;"
  ]
  node [
    id 535
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 536
    label "egzekutywa"
  ]
  node [
    id 537
    label "potencja&#322;"
  ]
  node [
    id 538
    label "wyb&#243;r"
  ]
  node [
    id 539
    label "prospect"
  ]
  node [
    id 540
    label "ability"
  ]
  node [
    id 541
    label "obliczeniowo"
  ]
  node [
    id 542
    label "alternatywa"
  ]
  node [
    id 543
    label "operator_modalny"
  ]
  node [
    id 544
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 545
    label "alternatywa_Fredholma"
  ]
  node [
    id 546
    label "oznajmianie"
  ]
  node [
    id 547
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 548
    label "teoria"
  ]
  node [
    id 549
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 550
    label "paradoks_Leontiefa"
  ]
  node [
    id 551
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 552
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 553
    label "teza"
  ]
  node [
    id 554
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 555
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 556
    label "twierdzenie_Pettisa"
  ]
  node [
    id 557
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 558
    label "twierdzenie_Maya"
  ]
  node [
    id 559
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 560
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 561
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 562
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 563
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 564
    label "zapewnianie"
  ]
  node [
    id 565
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 566
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 567
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 568
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 569
    label "twierdzenie_Stokesa"
  ]
  node [
    id 570
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 571
    label "twierdzenie_Cevy"
  ]
  node [
    id 572
    label "twierdzenie_Pascala"
  ]
  node [
    id 573
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 574
    label "komunikowanie"
  ]
  node [
    id 575
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 576
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 577
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 578
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 579
    label "relacja"
  ]
  node [
    id 580
    label "badanie"
  ]
  node [
    id 581
    label "proces_my&#347;lowy"
  ]
  node [
    id 582
    label "remark"
  ]
  node [
    id 583
    label "stwierdzenie"
  ]
  node [
    id 584
    label "observation"
  ]
  node [
    id 585
    label "calibration"
  ]
  node [
    id 586
    label "operacja"
  ]
  node [
    id 587
    label "proces"
  ]
  node [
    id 588
    label "porz&#261;dek"
  ]
  node [
    id 589
    label "dominance"
  ]
  node [
    id 590
    label "zabieg"
  ]
  node [
    id 591
    label "standardization"
  ]
  node [
    id 592
    label "orzecznictwo"
  ]
  node [
    id 593
    label "wykonawczo"
  ]
  node [
    id 594
    label "byt"
  ]
  node [
    id 595
    label "cz&#322;owiek"
  ]
  node [
    id 596
    label "osobowo&#347;&#263;"
  ]
  node [
    id 597
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 598
    label "status"
  ]
  node [
    id 599
    label "procedura"
  ]
  node [
    id 600
    label "set"
  ]
  node [
    id 601
    label "nada&#263;"
  ]
  node [
    id 602
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 603
    label "cook"
  ]
  node [
    id 604
    label "base"
  ]
  node [
    id 605
    label "umowa"
  ]
  node [
    id 606
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 607
    label "moralno&#347;&#263;"
  ]
  node [
    id 608
    label "occupation"
  ]
  node [
    id 609
    label "podstawa"
  ]
  node [
    id 610
    label "substancja"
  ]
  node [
    id 611
    label "prawid&#322;o"
  ]
  node [
    id 612
    label "norma_prawna"
  ]
  node [
    id 613
    label "przedawnienie_si&#281;"
  ]
  node [
    id 614
    label "przedawnianie_si&#281;"
  ]
  node [
    id 615
    label "porada"
  ]
  node [
    id 616
    label "przedawni&#263;_si&#281;"
  ]
  node [
    id 617
    label "regulation"
  ]
  node [
    id 618
    label "recepta"
  ]
  node [
    id 619
    label "przedawnia&#263;_si&#281;"
  ]
  node [
    id 620
    label "kodeks"
  ]
  node [
    id 621
    label "casuistry"
  ]
  node [
    id 622
    label "manipulacja"
  ]
  node [
    id 623
    label "probabilizm"
  ]
  node [
    id 624
    label "dermatoglifika"
  ]
  node [
    id 625
    label "mikro&#347;lad"
  ]
  node [
    id 626
    label "technika_&#347;ledcza"
  ]
  node [
    id 627
    label "dzia&#322;"
  ]
  node [
    id 628
    label "w&#322;asny"
  ]
  node [
    id 629
    label "oryginalny"
  ]
  node [
    id 630
    label "autorsko"
  ]
  node [
    id 631
    label "samodzielny"
  ]
  node [
    id 632
    label "zwi&#261;zany"
  ]
  node [
    id 633
    label "czyj&#347;"
  ]
  node [
    id 634
    label "swoisty"
  ]
  node [
    id 635
    label "osobny"
  ]
  node [
    id 636
    label "niespotykany"
  ]
  node [
    id 637
    label "o&#380;ywczy"
  ]
  node [
    id 638
    label "ekscentryczny"
  ]
  node [
    id 639
    label "nowy"
  ]
  node [
    id 640
    label "oryginalnie"
  ]
  node [
    id 641
    label "inny"
  ]
  node [
    id 642
    label "pierwotny"
  ]
  node [
    id 643
    label "prawdziwy"
  ]
  node [
    id 644
    label "warto&#347;ciowy"
  ]
  node [
    id 645
    label "prawnie"
  ]
  node [
    id 646
    label "indywidualnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 9
    target 422
  ]
  edge [
    source 9
    target 423
  ]
  edge [
    source 9
    target 424
  ]
  edge [
    source 9
    target 425
  ]
  edge [
    source 9
    target 426
  ]
  edge [
    source 9
    target 427
  ]
  edge [
    source 9
    target 428
  ]
  edge [
    source 9
    target 429
  ]
  edge [
    source 9
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 210
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 192
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 202
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 194
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 324
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
]
