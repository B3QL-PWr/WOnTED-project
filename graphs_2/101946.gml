graph [
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "luty"
    origin "text"
  ]
  node [
    id 3
    label "poz"
    origin "text"
  ]
  node [
    id 4
    label "program_informacyjny"
  ]
  node [
    id 5
    label "journal"
  ]
  node [
    id 6
    label "diariusz"
  ]
  node [
    id 7
    label "spis"
  ]
  node [
    id 8
    label "ksi&#281;ga"
  ]
  node [
    id 9
    label "sheet"
  ]
  node [
    id 10
    label "pami&#281;tnik"
  ]
  node [
    id 11
    label "gazeta"
  ]
  node [
    id 12
    label "tytu&#322;"
  ]
  node [
    id 13
    label "redakcja"
  ]
  node [
    id 14
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 15
    label "czasopismo"
  ]
  node [
    id 16
    label "prasa"
  ]
  node [
    id 17
    label "rozdzia&#322;"
  ]
  node [
    id 18
    label "pismo"
  ]
  node [
    id 19
    label "Ewangelia"
  ]
  node [
    id 20
    label "book"
  ]
  node [
    id 21
    label "dokument"
  ]
  node [
    id 22
    label "tome"
  ]
  node [
    id 23
    label "pami&#281;tnikarstwo"
  ]
  node [
    id 24
    label "pami&#261;tka"
  ]
  node [
    id 25
    label "notes"
  ]
  node [
    id 26
    label "zapiski"
  ]
  node [
    id 27
    label "raptularz"
  ]
  node [
    id 28
    label "album"
  ]
  node [
    id 29
    label "utw&#243;r_epicki"
  ]
  node [
    id 30
    label "zbi&#243;r"
  ]
  node [
    id 31
    label "catalog"
  ]
  node [
    id 32
    label "pozycja"
  ]
  node [
    id 33
    label "akt"
  ]
  node [
    id 34
    label "tekst"
  ]
  node [
    id 35
    label "sumariusz"
  ]
  node [
    id 36
    label "stock"
  ]
  node [
    id 37
    label "figurowa&#263;"
  ]
  node [
    id 38
    label "czynno&#347;&#263;"
  ]
  node [
    id 39
    label "wyliczanka"
  ]
  node [
    id 40
    label "Karta_Nauczyciela"
  ]
  node [
    id 41
    label "przej&#347;cie"
  ]
  node [
    id 42
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 43
    label "przej&#347;&#263;"
  ]
  node [
    id 44
    label "charter"
  ]
  node [
    id 45
    label "marc&#243;wka"
  ]
  node [
    id 46
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 47
    label "podnieci&#263;"
  ]
  node [
    id 48
    label "scena"
  ]
  node [
    id 49
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 50
    label "numer"
  ]
  node [
    id 51
    label "po&#380;ycie"
  ]
  node [
    id 52
    label "poj&#281;cie"
  ]
  node [
    id 53
    label "podniecenie"
  ]
  node [
    id 54
    label "nago&#347;&#263;"
  ]
  node [
    id 55
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 56
    label "fascyku&#322;"
  ]
  node [
    id 57
    label "seks"
  ]
  node [
    id 58
    label "podniecanie"
  ]
  node [
    id 59
    label "imisja"
  ]
  node [
    id 60
    label "zwyczaj"
  ]
  node [
    id 61
    label "rozmna&#380;anie"
  ]
  node [
    id 62
    label "ruch_frykcyjny"
  ]
  node [
    id 63
    label "ontologia"
  ]
  node [
    id 64
    label "wydarzenie"
  ]
  node [
    id 65
    label "na_pieska"
  ]
  node [
    id 66
    label "pozycja_misjonarska"
  ]
  node [
    id 67
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 68
    label "fragment"
  ]
  node [
    id 69
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 70
    label "z&#322;&#261;czenie"
  ]
  node [
    id 71
    label "gra_wst&#281;pna"
  ]
  node [
    id 72
    label "erotyka"
  ]
  node [
    id 73
    label "urzeczywistnienie"
  ]
  node [
    id 74
    label "baraszki"
  ]
  node [
    id 75
    label "certificate"
  ]
  node [
    id 76
    label "po&#380;&#261;danie"
  ]
  node [
    id 77
    label "wzw&#243;d"
  ]
  node [
    id 78
    label "funkcja"
  ]
  node [
    id 79
    label "act"
  ]
  node [
    id 80
    label "arystotelizm"
  ]
  node [
    id 81
    label "podnieca&#263;"
  ]
  node [
    id 82
    label "zabory"
  ]
  node [
    id 83
    label "ci&#281;&#380;arna"
  ]
  node [
    id 84
    label "rozwi&#261;zanie"
  ]
  node [
    id 85
    label "podlec"
  ]
  node [
    id 86
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 87
    label "min&#261;&#263;"
  ]
  node [
    id 88
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 89
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 90
    label "zaliczy&#263;"
  ]
  node [
    id 91
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 92
    label "zmieni&#263;"
  ]
  node [
    id 93
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 94
    label "przeby&#263;"
  ]
  node [
    id 95
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 96
    label "die"
  ]
  node [
    id 97
    label "dozna&#263;"
  ]
  node [
    id 98
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 99
    label "zacz&#261;&#263;"
  ]
  node [
    id 100
    label "happen"
  ]
  node [
    id 101
    label "pass"
  ]
  node [
    id 102
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 103
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 104
    label "przewy&#380;szy&#263;"
  ]
  node [
    id 105
    label "beat"
  ]
  node [
    id 106
    label "mienie"
  ]
  node [
    id 107
    label "absorb"
  ]
  node [
    id 108
    label "przerobi&#263;"
  ]
  node [
    id 109
    label "pique"
  ]
  node [
    id 110
    label "przesta&#263;"
  ]
  node [
    id 111
    label "mini&#281;cie"
  ]
  node [
    id 112
    label "wymienienie"
  ]
  node [
    id 113
    label "zaliczenie"
  ]
  node [
    id 114
    label "traversal"
  ]
  node [
    id 115
    label "zdarzenie_si&#281;"
  ]
  node [
    id 116
    label "przewy&#380;szenie"
  ]
  node [
    id 117
    label "experience"
  ]
  node [
    id 118
    label "przepuszczenie"
  ]
  node [
    id 119
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 120
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 121
    label "strain"
  ]
  node [
    id 122
    label "faza"
  ]
  node [
    id 123
    label "przerobienie"
  ]
  node [
    id 124
    label "wydeptywanie"
  ]
  node [
    id 125
    label "miejsce"
  ]
  node [
    id 126
    label "crack"
  ]
  node [
    id 127
    label "wydeptanie"
  ]
  node [
    id 128
    label "wstawka"
  ]
  node [
    id 129
    label "prze&#380;ycie"
  ]
  node [
    id 130
    label "uznanie"
  ]
  node [
    id 131
    label "doznanie"
  ]
  node [
    id 132
    label "dostanie_si&#281;"
  ]
  node [
    id 133
    label "trwanie"
  ]
  node [
    id 134
    label "przebycie"
  ]
  node [
    id 135
    label "wytyczenie"
  ]
  node [
    id 136
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 137
    label "przepojenie"
  ]
  node [
    id 138
    label "nas&#261;czenie"
  ]
  node [
    id 139
    label "nale&#380;enie"
  ]
  node [
    id 140
    label "odmienienie"
  ]
  node [
    id 141
    label "przedostanie_si&#281;"
  ]
  node [
    id 142
    label "przemokni&#281;cie"
  ]
  node [
    id 143
    label "nasycenie_si&#281;"
  ]
  node [
    id 144
    label "zacz&#281;cie"
  ]
  node [
    id 145
    label "stanie_si&#281;"
  ]
  node [
    id 146
    label "offense"
  ]
  node [
    id 147
    label "przestanie"
  ]
  node [
    id 148
    label "odnaj&#281;cie"
  ]
  node [
    id 149
    label "naj&#281;cie"
  ]
  node [
    id 150
    label "walentynki"
  ]
  node [
    id 151
    label "Matka_Boska_Gromniczna"
  ]
  node [
    id 152
    label "miesi&#261;c"
  ]
  node [
    id 153
    label "tydzie&#324;"
  ]
  node [
    id 154
    label "miech"
  ]
  node [
    id 155
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 156
    label "czas"
  ]
  node [
    id 157
    label "rok"
  ]
  node [
    id 158
    label "kalendy"
  ]
  node [
    id 159
    label "ustawi&#263;"
  ]
  node [
    id 160
    label "ojciec"
  ]
  node [
    id 161
    label "zmiana"
  ]
  node [
    id 162
    label "ujawni&#263;"
  ]
  node [
    id 163
    label "praca"
  ]
  node [
    id 164
    label "lubi&#263;"
  ]
  node [
    id 165
    label "s&#322;u&#380;ba"
  ]
  node [
    id 166
    label "wyspa"
  ]
  node [
    id 167
    label "organy"
  ]
  node [
    id 168
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 169
    label "pa&#324;stwo"
  ]
  node [
    id 170
    label "wsp&#243;&#322;praca"
  ]
  node [
    id 171
    label "zeszyt"
  ]
  node [
    id 172
    label "on"
  ]
  node [
    id 173
    label "1944"
  ]
  node [
    id 174
    label "&#8211;"
  ]
  node [
    id 175
    label "1990"
  ]
  node [
    id 176
    label "osoba"
  ]
  node [
    id 177
    label "pe&#322;ni&#263;"
  ]
  node [
    id 178
    label "publiczny"
  ]
  node [
    id 179
    label "ordynacja"
  ]
  node [
    id 180
    label "wyborczy"
  ]
  node [
    id 181
    label "do"
  ]
  node [
    id 182
    label "sejm"
  ]
  node [
    id 183
    label "rzeczpospolita"
  ]
  node [
    id 184
    label "polski"
  ]
  node [
    id 185
    label "i"
  ]
  node [
    id 186
    label "senat"
  ]
  node [
    id 187
    label "dzie&#324;"
  ]
  node [
    id 188
    label "11"
  ]
  node [
    id 189
    label "kwietni"
  ]
  node [
    id 190
    label "1997"
  ]
  node [
    id 191
    label "u"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 78
    target 160
  ]
  edge [
    source 78
    target 161
  ]
  edge [
    source 78
    target 162
  ]
  edge [
    source 78
    target 163
  ]
  edge [
    source 78
    target 164
  ]
  edge [
    source 78
    target 165
  ]
  edge [
    source 78
    target 166
  ]
  edge [
    source 78
    target 167
  ]
  edge [
    source 78
    target 168
  ]
  edge [
    source 78
    target 169
  ]
  edge [
    source 78
    target 170
  ]
  edge [
    source 78
    target 171
  ]
  edge [
    source 78
    target 172
  ]
  edge [
    source 78
    target 157
  ]
  edge [
    source 78
    target 173
  ]
  edge [
    source 78
    target 174
  ]
  edge [
    source 78
    target 175
  ]
  edge [
    source 78
    target 176
  ]
  edge [
    source 78
    target 177
  ]
  edge [
    source 78
    target 178
  ]
  edge [
    source 78
    target 187
  ]
  edge [
    source 78
    target 188
  ]
  edge [
    source 78
    target 189
  ]
  edge [
    source 78
    target 190
  ]
  edge [
    source 157
    target 160
  ]
  edge [
    source 157
    target 161
  ]
  edge [
    source 157
    target 162
  ]
  edge [
    source 157
    target 163
  ]
  edge [
    source 157
    target 164
  ]
  edge [
    source 157
    target 165
  ]
  edge [
    source 157
    target 166
  ]
  edge [
    source 157
    target 167
  ]
  edge [
    source 157
    target 168
  ]
  edge [
    source 157
    target 169
  ]
  edge [
    source 157
    target 170
  ]
  edge [
    source 157
    target 171
  ]
  edge [
    source 157
    target 172
  ]
  edge [
    source 157
    target 173
  ]
  edge [
    source 157
    target 174
  ]
  edge [
    source 157
    target 175
  ]
  edge [
    source 157
    target 176
  ]
  edge [
    source 157
    target 177
  ]
  edge [
    source 157
    target 178
  ]
  edge [
    source 157
    target 187
  ]
  edge [
    source 157
    target 188
  ]
  edge [
    source 157
    target 189
  ]
  edge [
    source 157
    target 190
  ]
  edge [
    source 157
    target 157
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 160
  ]
  edge [
    source 160
    target 162
  ]
  edge [
    source 160
    target 163
  ]
  edge [
    source 160
    target 164
  ]
  edge [
    source 160
    target 165
  ]
  edge [
    source 160
    target 166
  ]
  edge [
    source 160
    target 167
  ]
  edge [
    source 160
    target 168
  ]
  edge [
    source 160
    target 169
  ]
  edge [
    source 160
    target 170
  ]
  edge [
    source 160
    target 171
  ]
  edge [
    source 160
    target 172
  ]
  edge [
    source 160
    target 173
  ]
  edge [
    source 160
    target 174
  ]
  edge [
    source 160
    target 175
  ]
  edge [
    source 160
    target 176
  ]
  edge [
    source 160
    target 177
  ]
  edge [
    source 160
    target 178
  ]
  edge [
    source 160
    target 187
  ]
  edge [
    source 160
    target 188
  ]
  edge [
    source 160
    target 189
  ]
  edge [
    source 160
    target 190
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 161
    target 163
  ]
  edge [
    source 161
    target 164
  ]
  edge [
    source 161
    target 165
  ]
  edge [
    source 161
    target 166
  ]
  edge [
    source 161
    target 167
  ]
  edge [
    source 161
    target 168
  ]
  edge [
    source 161
    target 169
  ]
  edge [
    source 161
    target 170
  ]
  edge [
    source 161
    target 171
  ]
  edge [
    source 161
    target 172
  ]
  edge [
    source 161
    target 173
  ]
  edge [
    source 161
    target 174
  ]
  edge [
    source 161
    target 175
  ]
  edge [
    source 161
    target 176
  ]
  edge [
    source 161
    target 177
  ]
  edge [
    source 161
    target 178
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 164
  ]
  edge [
    source 162
    target 165
  ]
  edge [
    source 162
    target 166
  ]
  edge [
    source 162
    target 167
  ]
  edge [
    source 162
    target 168
  ]
  edge [
    source 162
    target 169
  ]
  edge [
    source 162
    target 170
  ]
  edge [
    source 162
    target 171
  ]
  edge [
    source 162
    target 172
  ]
  edge [
    source 162
    target 173
  ]
  edge [
    source 162
    target 174
  ]
  edge [
    source 162
    target 175
  ]
  edge [
    source 162
    target 176
  ]
  edge [
    source 162
    target 177
  ]
  edge [
    source 162
    target 178
  ]
  edge [
    source 162
    target 187
  ]
  edge [
    source 162
    target 188
  ]
  edge [
    source 162
    target 189
  ]
  edge [
    source 162
    target 190
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 163
    target 165
  ]
  edge [
    source 163
    target 166
  ]
  edge [
    source 163
    target 167
  ]
  edge [
    source 163
    target 168
  ]
  edge [
    source 163
    target 169
  ]
  edge [
    source 163
    target 170
  ]
  edge [
    source 163
    target 171
  ]
  edge [
    source 163
    target 172
  ]
  edge [
    source 163
    target 173
  ]
  edge [
    source 163
    target 174
  ]
  edge [
    source 163
    target 175
  ]
  edge [
    source 163
    target 176
  ]
  edge [
    source 163
    target 177
  ]
  edge [
    source 163
    target 178
  ]
  edge [
    source 163
    target 187
  ]
  edge [
    source 163
    target 188
  ]
  edge [
    source 163
    target 189
  ]
  edge [
    source 163
    target 190
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 164
    target 166
  ]
  edge [
    source 164
    target 167
  ]
  edge [
    source 164
    target 168
  ]
  edge [
    source 164
    target 169
  ]
  edge [
    source 164
    target 164
  ]
  edge [
    source 164
    target 170
  ]
  edge [
    source 164
    target 171
  ]
  edge [
    source 164
    target 172
  ]
  edge [
    source 164
    target 173
  ]
  edge [
    source 164
    target 174
  ]
  edge [
    source 164
    target 175
  ]
  edge [
    source 164
    target 176
  ]
  edge [
    source 164
    target 177
  ]
  edge [
    source 164
    target 178
  ]
  edge [
    source 164
    target 187
  ]
  edge [
    source 164
    target 188
  ]
  edge [
    source 164
    target 189
  ]
  edge [
    source 164
    target 190
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 165
    target 167
  ]
  edge [
    source 165
    target 168
  ]
  edge [
    source 165
    target 169
  ]
  edge [
    source 165
    target 170
  ]
  edge [
    source 165
    target 171
  ]
  edge [
    source 165
    target 172
  ]
  edge [
    source 165
    target 173
  ]
  edge [
    source 165
    target 174
  ]
  edge [
    source 165
    target 175
  ]
  edge [
    source 165
    target 176
  ]
  edge [
    source 165
    target 177
  ]
  edge [
    source 165
    target 178
  ]
  edge [
    source 165
    target 187
  ]
  edge [
    source 165
    target 188
  ]
  edge [
    source 165
    target 189
  ]
  edge [
    source 165
    target 190
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 166
    target 168
  ]
  edge [
    source 166
    target 169
  ]
  edge [
    source 166
    target 170
  ]
  edge [
    source 166
    target 171
  ]
  edge [
    source 166
    target 172
  ]
  edge [
    source 166
    target 166
  ]
  edge [
    source 166
    target 173
  ]
  edge [
    source 166
    target 174
  ]
  edge [
    source 166
    target 175
  ]
  edge [
    source 166
    target 176
  ]
  edge [
    source 166
    target 177
  ]
  edge [
    source 166
    target 178
  ]
  edge [
    source 166
    target 187
  ]
  edge [
    source 166
    target 188
  ]
  edge [
    source 166
    target 189
  ]
  edge [
    source 166
    target 190
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 167
    target 169
  ]
  edge [
    source 167
    target 170
  ]
  edge [
    source 167
    target 171
  ]
  edge [
    source 167
    target 172
  ]
  edge [
    source 167
    target 173
  ]
  edge [
    source 167
    target 174
  ]
  edge [
    source 167
    target 175
  ]
  edge [
    source 167
    target 176
  ]
  edge [
    source 167
    target 177
  ]
  edge [
    source 167
    target 178
  ]
  edge [
    source 167
    target 187
  ]
  edge [
    source 167
    target 188
  ]
  edge [
    source 167
    target 189
  ]
  edge [
    source 167
    target 190
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 168
    target 170
  ]
  edge [
    source 168
    target 171
  ]
  edge [
    source 168
    target 172
  ]
  edge [
    source 168
    target 173
  ]
  edge [
    source 168
    target 174
  ]
  edge [
    source 168
    target 175
  ]
  edge [
    source 168
    target 176
  ]
  edge [
    source 168
    target 177
  ]
  edge [
    source 168
    target 178
  ]
  edge [
    source 168
    target 187
  ]
  edge [
    source 168
    target 188
  ]
  edge [
    source 168
    target 189
  ]
  edge [
    source 168
    target 190
  ]
  edge [
    source 169
    target 170
  ]
  edge [
    source 169
    target 171
  ]
  edge [
    source 169
    target 172
  ]
  edge [
    source 169
    target 173
  ]
  edge [
    source 169
    target 174
  ]
  edge [
    source 169
    target 175
  ]
  edge [
    source 169
    target 176
  ]
  edge [
    source 169
    target 177
  ]
  edge [
    source 169
    target 178
  ]
  edge [
    source 169
    target 187
  ]
  edge [
    source 169
    target 188
  ]
  edge [
    source 169
    target 189
  ]
  edge [
    source 169
    target 190
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 170
    target 172
  ]
  edge [
    source 170
    target 173
  ]
  edge [
    source 170
    target 174
  ]
  edge [
    source 170
    target 175
  ]
  edge [
    source 170
    target 176
  ]
  edge [
    source 170
    target 177
  ]
  edge [
    source 170
    target 178
  ]
  edge [
    source 170
    target 187
  ]
  edge [
    source 170
    target 188
  ]
  edge [
    source 170
    target 189
  ]
  edge [
    source 170
    target 190
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 171
    target 173
  ]
  edge [
    source 171
    target 174
  ]
  edge [
    source 171
    target 175
  ]
  edge [
    source 171
    target 176
  ]
  edge [
    source 171
    target 177
  ]
  edge [
    source 171
    target 178
  ]
  edge [
    source 171
    target 187
  ]
  edge [
    source 171
    target 188
  ]
  edge [
    source 171
    target 189
  ]
  edge [
    source 171
    target 190
  ]
  edge [
    source 171
    target 171
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 172
    target 174
  ]
  edge [
    source 172
    target 175
  ]
  edge [
    source 172
    target 176
  ]
  edge [
    source 172
    target 177
  ]
  edge [
    source 172
    target 178
  ]
  edge [
    source 172
    target 187
  ]
  edge [
    source 172
    target 188
  ]
  edge [
    source 172
    target 189
  ]
  edge [
    source 172
    target 190
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 173
    target 175
  ]
  edge [
    source 173
    target 176
  ]
  edge [
    source 173
    target 177
  ]
  edge [
    source 173
    target 178
  ]
  edge [
    source 173
    target 187
  ]
  edge [
    source 173
    target 188
  ]
  edge [
    source 173
    target 189
  ]
  edge [
    source 173
    target 190
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 174
    target 176
  ]
  edge [
    source 174
    target 177
  ]
  edge [
    source 174
    target 178
  ]
  edge [
    source 175
    target 176
  ]
  edge [
    source 175
    target 177
  ]
  edge [
    source 175
    target 178
  ]
  edge [
    source 175
    target 187
  ]
  edge [
    source 175
    target 188
  ]
  edge [
    source 175
    target 189
  ]
  edge [
    source 175
    target 190
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 176
    target 178
  ]
  edge [
    source 176
    target 187
  ]
  edge [
    source 176
    target 188
  ]
  edge [
    source 176
    target 189
  ]
  edge [
    source 176
    target 190
  ]
  edge [
    source 177
    target 178
  ]
  edge [
    source 177
    target 187
  ]
  edge [
    source 177
    target 188
  ]
  edge [
    source 177
    target 189
  ]
  edge [
    source 177
    target 190
  ]
  edge [
    source 178
    target 187
  ]
  edge [
    source 178
    target 188
  ]
  edge [
    source 178
    target 189
  ]
  edge [
    source 178
    target 190
  ]
  edge [
    source 179
    target 180
  ]
  edge [
    source 179
    target 181
  ]
  edge [
    source 179
    target 182
  ]
  edge [
    source 179
    target 183
  ]
  edge [
    source 179
    target 184
  ]
  edge [
    source 179
    target 185
  ]
  edge [
    source 179
    target 186
  ]
  edge [
    source 180
    target 181
  ]
  edge [
    source 180
    target 182
  ]
  edge [
    source 180
    target 183
  ]
  edge [
    source 180
    target 184
  ]
  edge [
    source 180
    target 185
  ]
  edge [
    source 180
    target 186
  ]
  edge [
    source 181
    target 182
  ]
  edge [
    source 181
    target 183
  ]
  edge [
    source 181
    target 184
  ]
  edge [
    source 181
    target 185
  ]
  edge [
    source 181
    target 181
  ]
  edge [
    source 181
    target 186
  ]
  edge [
    source 182
    target 183
  ]
  edge [
    source 182
    target 184
  ]
  edge [
    source 182
    target 185
  ]
  edge [
    source 182
    target 186
  ]
  edge [
    source 183
    target 184
  ]
  edge [
    source 183
    target 185
  ]
  edge [
    source 183
    target 186
  ]
  edge [
    source 183
    target 183
  ]
  edge [
    source 184
    target 185
  ]
  edge [
    source 184
    target 186
  ]
  edge [
    source 184
    target 184
  ]
  edge [
    source 185
    target 186
  ]
  edge [
    source 187
    target 188
  ]
  edge [
    source 187
    target 189
  ]
  edge [
    source 187
    target 190
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 188
    target 190
  ]
  edge [
    source 189
    target 190
  ]
]
