graph [
  node [
    id 0
    label "zn&#243;w"
    origin "text"
  ]
  node [
    id 1
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 2
    label "obejrze&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 5
    label "cz&#322;owiek"
  ]
  node [
    id 6
    label "posta&#263;"
  ]
  node [
    id 7
    label "osoba"
  ]
  node [
    id 8
    label "znaczenie"
  ]
  node [
    id 9
    label "go&#347;&#263;"
  ]
  node [
    id 10
    label "ludzko&#347;&#263;"
  ]
  node [
    id 11
    label "asymilowanie"
  ]
  node [
    id 12
    label "wapniak"
  ]
  node [
    id 13
    label "asymilowa&#263;"
  ]
  node [
    id 14
    label "os&#322;abia&#263;"
  ]
  node [
    id 15
    label "hominid"
  ]
  node [
    id 16
    label "podw&#322;adny"
  ]
  node [
    id 17
    label "os&#322;abianie"
  ]
  node [
    id 18
    label "g&#322;owa"
  ]
  node [
    id 19
    label "figura"
  ]
  node [
    id 20
    label "portrecista"
  ]
  node [
    id 21
    label "dwun&#243;g"
  ]
  node [
    id 22
    label "profanum"
  ]
  node [
    id 23
    label "mikrokosmos"
  ]
  node [
    id 24
    label "nasada"
  ]
  node [
    id 25
    label "duch"
  ]
  node [
    id 26
    label "antropochoria"
  ]
  node [
    id 27
    label "wz&#243;r"
  ]
  node [
    id 28
    label "senior"
  ]
  node [
    id 29
    label "oddzia&#322;ywanie"
  ]
  node [
    id 30
    label "Adam"
  ]
  node [
    id 31
    label "homo_sapiens"
  ]
  node [
    id 32
    label "polifag"
  ]
  node [
    id 33
    label "odwiedziny"
  ]
  node [
    id 34
    label "klient"
  ]
  node [
    id 35
    label "restauracja"
  ]
  node [
    id 36
    label "przybysz"
  ]
  node [
    id 37
    label "uczestnik"
  ]
  node [
    id 38
    label "hotel"
  ]
  node [
    id 39
    label "bratek"
  ]
  node [
    id 40
    label "sztuka"
  ]
  node [
    id 41
    label "facet"
  ]
  node [
    id 42
    label "Chocho&#322;"
  ]
  node [
    id 43
    label "Herkules_Poirot"
  ]
  node [
    id 44
    label "Edyp"
  ]
  node [
    id 45
    label "parali&#380;owa&#263;"
  ]
  node [
    id 46
    label "Harry_Potter"
  ]
  node [
    id 47
    label "Casanova"
  ]
  node [
    id 48
    label "Gargantua"
  ]
  node [
    id 49
    label "Zgredek"
  ]
  node [
    id 50
    label "Winnetou"
  ]
  node [
    id 51
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 52
    label "Dulcynea"
  ]
  node [
    id 53
    label "kategoria_gramatyczna"
  ]
  node [
    id 54
    label "person"
  ]
  node [
    id 55
    label "Sherlock_Holmes"
  ]
  node [
    id 56
    label "Quasimodo"
  ]
  node [
    id 57
    label "Plastu&#347;"
  ]
  node [
    id 58
    label "Faust"
  ]
  node [
    id 59
    label "Wallenrod"
  ]
  node [
    id 60
    label "Dwukwiat"
  ]
  node [
    id 61
    label "koniugacja"
  ]
  node [
    id 62
    label "Don_Juan"
  ]
  node [
    id 63
    label "Don_Kiszot"
  ]
  node [
    id 64
    label "Hamlet"
  ]
  node [
    id 65
    label "Werter"
  ]
  node [
    id 66
    label "istota"
  ]
  node [
    id 67
    label "Szwejk"
  ]
  node [
    id 68
    label "charakterystyka"
  ]
  node [
    id 69
    label "zaistnie&#263;"
  ]
  node [
    id 70
    label "Osjan"
  ]
  node [
    id 71
    label "cecha"
  ]
  node [
    id 72
    label "wygl&#261;d"
  ]
  node [
    id 73
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 74
    label "osobowo&#347;&#263;"
  ]
  node [
    id 75
    label "wytw&#243;r"
  ]
  node [
    id 76
    label "trim"
  ]
  node [
    id 77
    label "poby&#263;"
  ]
  node [
    id 78
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 79
    label "Aspazja"
  ]
  node [
    id 80
    label "punkt_widzenia"
  ]
  node [
    id 81
    label "kompleksja"
  ]
  node [
    id 82
    label "wytrzyma&#263;"
  ]
  node [
    id 83
    label "budowa"
  ]
  node [
    id 84
    label "formacja"
  ]
  node [
    id 85
    label "pozosta&#263;"
  ]
  node [
    id 86
    label "point"
  ]
  node [
    id 87
    label "przedstawienie"
  ]
  node [
    id 88
    label "odk&#322;adanie"
  ]
  node [
    id 89
    label "condition"
  ]
  node [
    id 90
    label "liczenie"
  ]
  node [
    id 91
    label "stawianie"
  ]
  node [
    id 92
    label "bycie"
  ]
  node [
    id 93
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 94
    label "assay"
  ]
  node [
    id 95
    label "wskazywanie"
  ]
  node [
    id 96
    label "wyraz"
  ]
  node [
    id 97
    label "gravity"
  ]
  node [
    id 98
    label "weight"
  ]
  node [
    id 99
    label "command"
  ]
  node [
    id 100
    label "odgrywanie_roli"
  ]
  node [
    id 101
    label "informacja"
  ]
  node [
    id 102
    label "okre&#347;lanie"
  ]
  node [
    id 103
    label "wyra&#380;enie"
  ]
  node [
    id 104
    label "visualize"
  ]
  node [
    id 105
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 106
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 107
    label "nastawi&#263;"
  ]
  node [
    id 108
    label "draw"
  ]
  node [
    id 109
    label "odkr&#281;ci&#263;_kurek"
  ]
  node [
    id 110
    label "incorporate"
  ]
  node [
    id 111
    label "impersonate"
  ]
  node [
    id 112
    label "dokoptowa&#263;"
  ]
  node [
    id 113
    label "prosecute"
  ]
  node [
    id 114
    label "uruchomi&#263;"
  ]
  node [
    id 115
    label "umie&#347;ci&#263;"
  ]
  node [
    id 116
    label "zacz&#261;&#263;"
  ]
  node [
    id 117
    label "para"
  ]
  node [
    id 118
    label "necessity"
  ]
  node [
    id 119
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 120
    label "trza"
  ]
  node [
    id 121
    label "uczestniczy&#263;"
  ]
  node [
    id 122
    label "participate"
  ]
  node [
    id 123
    label "robi&#263;"
  ]
  node [
    id 124
    label "by&#263;"
  ]
  node [
    id 125
    label "trzeba"
  ]
  node [
    id 126
    label "pair"
  ]
  node [
    id 127
    label "zesp&#243;&#322;"
  ]
  node [
    id 128
    label "odparowywanie"
  ]
  node [
    id 129
    label "gaz_cieplarniany"
  ]
  node [
    id 130
    label "chodzi&#263;"
  ]
  node [
    id 131
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 132
    label "poker"
  ]
  node [
    id 133
    label "moneta"
  ]
  node [
    id 134
    label "parowanie"
  ]
  node [
    id 135
    label "zbi&#243;r"
  ]
  node [
    id 136
    label "damp"
  ]
  node [
    id 137
    label "odparowanie"
  ]
  node [
    id 138
    label "grupa"
  ]
  node [
    id 139
    label "odparowa&#263;"
  ]
  node [
    id 140
    label "dodatek"
  ]
  node [
    id 141
    label "jednostka_monetarna"
  ]
  node [
    id 142
    label "smoke"
  ]
  node [
    id 143
    label "Imperium_Osma&#324;skie"
  ]
  node [
    id 144
    label "odparowywa&#263;"
  ]
  node [
    id 145
    label "uk&#322;ad"
  ]
  node [
    id 146
    label "Albania"
  ]
  node [
    id 147
    label "gaz"
  ]
  node [
    id 148
    label "wyparowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
]
