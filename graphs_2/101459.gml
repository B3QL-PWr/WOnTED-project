graph [
  node [
    id 0
    label "copernicus"
    origin "text"
  ]
  node [
    id 1
    label "nagroda"
    origin "text"
  ]
  node [
    id 2
    label "oskar"
  ]
  node [
    id 3
    label "wyr&#243;&#380;nienie"
  ]
  node [
    id 4
    label "return"
  ]
  node [
    id 5
    label "konsekwencja"
  ]
  node [
    id 6
    label "prize"
  ]
  node [
    id 7
    label "trophy"
  ]
  node [
    id 8
    label "oznaczenie"
  ]
  node [
    id 9
    label "potraktowanie"
  ]
  node [
    id 10
    label "nagrodzenie"
  ]
  node [
    id 11
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 12
    label "zrobienie"
  ]
  node [
    id 13
    label "odczuwa&#263;"
  ]
  node [
    id 14
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 15
    label "skrupienie_si&#281;"
  ]
  node [
    id 16
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 17
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 18
    label "odczucie"
  ]
  node [
    id 19
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 20
    label "koszula_Dejaniry"
  ]
  node [
    id 21
    label "odczuwanie"
  ]
  node [
    id 22
    label "event"
  ]
  node [
    id 23
    label "rezultat"
  ]
  node [
    id 24
    label "skrupianie_si&#281;"
  ]
  node [
    id 25
    label "odczu&#263;"
  ]
  node [
    id 26
    label "polsko"
  ]
  node [
    id 27
    label "niemiecki"
  ]
  node [
    id 28
    label "naukowy"
  ]
  node [
    id 29
    label "Copernicus"
  ]
  node [
    id 30
    label "Award"
  ]
  node [
    id 31
    label "fundacja"
  ]
  node [
    id 32
    label "na"
  ]
  node [
    id 33
    label "rzecz"
  ]
  node [
    id 34
    label "nauka"
  ]
  node [
    id 35
    label "polski"
  ]
  node [
    id 36
    label "deutsche"
  ]
  node [
    id 37
    label "Forschungsgemeinschaft"
  ]
  node [
    id 38
    label "Barbara"
  ]
  node [
    id 39
    label "Malinowska"
  ]
  node [
    id 40
    label "Eberhard"
  ]
  node [
    id 41
    label "Schlicker"
  ]
  node [
    id 42
    label "Andrzej"
  ]
  node [
    id 43
    label "sobolewski"
  ]
  node [
    id 44
    label "Wolfgang"
  ]
  node [
    id 45
    label "Domcke"
  ]
  node [
    id 46
    label "Alfreda"
  ]
  node [
    id 47
    label "Forchel"
  ]
  node [
    id 48
    label "Jan"
  ]
  node [
    id 49
    label "Misiewicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
]
