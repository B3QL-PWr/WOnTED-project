graph [
  node [
    id 0
    label "nazywa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "cezary"
    origin "text"
  ]
  node [
    id 3
    label "baryka"
    origin "text"
  ]
  node [
    id 4
    label "dwadzie&#347;cia"
    origin "text"
  ]
  node [
    id 5
    label "minuta"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 8
    label "oto"
    origin "text"
  ]
  node [
    id 9
    label "szklany"
    origin "text"
  ]
  node [
    id 10
    label "dom"
    origin "text"
  ]
  node [
    id 11
    label "mieni&#263;"
  ]
  node [
    id 12
    label "give"
  ]
  node [
    id 13
    label "nadawa&#263;"
  ]
  node [
    id 14
    label "okre&#347;la&#263;"
  ]
  node [
    id 15
    label "dawa&#263;"
  ]
  node [
    id 16
    label "assign"
  ]
  node [
    id 17
    label "gada&#263;"
  ]
  node [
    id 18
    label "donosi&#263;"
  ]
  node [
    id 19
    label "rekomendowa&#263;"
  ]
  node [
    id 20
    label "za&#322;atwia&#263;"
  ]
  node [
    id 21
    label "obgadywa&#263;"
  ]
  node [
    id 22
    label "sprawia&#263;"
  ]
  node [
    id 23
    label "przesy&#322;a&#263;"
  ]
  node [
    id 24
    label "decydowa&#263;"
  ]
  node [
    id 25
    label "signify"
  ]
  node [
    id 26
    label "style"
  ]
  node [
    id 27
    label "powodowa&#263;"
  ]
  node [
    id 28
    label "time"
  ]
  node [
    id 29
    label "zapis"
  ]
  node [
    id 30
    label "sekunda"
  ]
  node [
    id 31
    label "jednostka"
  ]
  node [
    id 32
    label "stopie&#324;"
  ]
  node [
    id 33
    label "godzina"
  ]
  node [
    id 34
    label "design"
  ]
  node [
    id 35
    label "kwadrans"
  ]
  node [
    id 36
    label "przyswoi&#263;"
  ]
  node [
    id 37
    label "ludzko&#347;&#263;"
  ]
  node [
    id 38
    label "one"
  ]
  node [
    id 39
    label "poj&#281;cie"
  ]
  node [
    id 40
    label "ewoluowanie"
  ]
  node [
    id 41
    label "supremum"
  ]
  node [
    id 42
    label "skala"
  ]
  node [
    id 43
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 44
    label "przyswajanie"
  ]
  node [
    id 45
    label "wyewoluowanie"
  ]
  node [
    id 46
    label "reakcja"
  ]
  node [
    id 47
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 48
    label "przeliczy&#263;"
  ]
  node [
    id 49
    label "wyewoluowa&#263;"
  ]
  node [
    id 50
    label "ewoluowa&#263;"
  ]
  node [
    id 51
    label "matematyka"
  ]
  node [
    id 52
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 53
    label "rzut"
  ]
  node [
    id 54
    label "liczba_naturalna"
  ]
  node [
    id 55
    label "czynnik_biotyczny"
  ]
  node [
    id 56
    label "g&#322;owa"
  ]
  node [
    id 57
    label "figura"
  ]
  node [
    id 58
    label "individual"
  ]
  node [
    id 59
    label "portrecista"
  ]
  node [
    id 60
    label "obiekt"
  ]
  node [
    id 61
    label "przyswaja&#263;"
  ]
  node [
    id 62
    label "przyswojenie"
  ]
  node [
    id 63
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 64
    label "profanum"
  ]
  node [
    id 65
    label "mikrokosmos"
  ]
  node [
    id 66
    label "starzenie_si&#281;"
  ]
  node [
    id 67
    label "duch"
  ]
  node [
    id 68
    label "przeliczanie"
  ]
  node [
    id 69
    label "osoba"
  ]
  node [
    id 70
    label "oddzia&#322;ywanie"
  ]
  node [
    id 71
    label "antropochoria"
  ]
  node [
    id 72
    label "funkcja"
  ]
  node [
    id 73
    label "homo_sapiens"
  ]
  node [
    id 74
    label "przelicza&#263;"
  ]
  node [
    id 75
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 76
    label "infimum"
  ]
  node [
    id 77
    label "przeliczenie"
  ]
  node [
    id 78
    label "spos&#243;b"
  ]
  node [
    id 79
    label "wytw&#243;r"
  ]
  node [
    id 80
    label "entrance"
  ]
  node [
    id 81
    label "czynno&#347;&#263;"
  ]
  node [
    id 82
    label "wpis"
  ]
  node [
    id 83
    label "normalizacja"
  ]
  node [
    id 84
    label "mikrosekunda"
  ]
  node [
    id 85
    label "milisekunda"
  ]
  node [
    id 86
    label "tercja"
  ]
  node [
    id 87
    label "nanosekunda"
  ]
  node [
    id 88
    label "uk&#322;ad_SI"
  ]
  node [
    id 89
    label "jednostka_czasu"
  ]
  node [
    id 90
    label "doba"
  ]
  node [
    id 91
    label "p&#243;&#322;godzina"
  ]
  node [
    id 92
    label "czas"
  ]
  node [
    id 93
    label "kszta&#322;t"
  ]
  node [
    id 94
    label "podstopie&#324;"
  ]
  node [
    id 95
    label "wielko&#347;&#263;"
  ]
  node [
    id 96
    label "rank"
  ]
  node [
    id 97
    label "d&#378;wi&#281;k"
  ]
  node [
    id 98
    label "wschodek"
  ]
  node [
    id 99
    label "przymiotnik"
  ]
  node [
    id 100
    label "gama"
  ]
  node [
    id 101
    label "podzia&#322;"
  ]
  node [
    id 102
    label "miejsce"
  ]
  node [
    id 103
    label "element"
  ]
  node [
    id 104
    label "schody"
  ]
  node [
    id 105
    label "kategoria_gramatyczna"
  ]
  node [
    id 106
    label "poziom"
  ]
  node [
    id 107
    label "przys&#322;&#243;wek"
  ]
  node [
    id 108
    label "ocena"
  ]
  node [
    id 109
    label "degree"
  ]
  node [
    id 110
    label "szczebel"
  ]
  node [
    id 111
    label "znaczenie"
  ]
  node [
    id 112
    label "podn&#243;&#380;ek"
  ]
  node [
    id 113
    label "forma"
  ]
  node [
    id 114
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 115
    label "wygl&#261;d"
  ]
  node [
    id 116
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 117
    label "mie&#263;_miejsce"
  ]
  node [
    id 118
    label "equal"
  ]
  node [
    id 119
    label "trwa&#263;"
  ]
  node [
    id 120
    label "chodzi&#263;"
  ]
  node [
    id 121
    label "si&#281;ga&#263;"
  ]
  node [
    id 122
    label "stan"
  ]
  node [
    id 123
    label "obecno&#347;&#263;"
  ]
  node [
    id 124
    label "stand"
  ]
  node [
    id 125
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 126
    label "uczestniczy&#263;"
  ]
  node [
    id 127
    label "participate"
  ]
  node [
    id 128
    label "robi&#263;"
  ]
  node [
    id 129
    label "istnie&#263;"
  ]
  node [
    id 130
    label "pozostawa&#263;"
  ]
  node [
    id 131
    label "zostawa&#263;"
  ]
  node [
    id 132
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 133
    label "adhere"
  ]
  node [
    id 134
    label "compass"
  ]
  node [
    id 135
    label "korzysta&#263;"
  ]
  node [
    id 136
    label "appreciation"
  ]
  node [
    id 137
    label "osi&#261;ga&#263;"
  ]
  node [
    id 138
    label "dociera&#263;"
  ]
  node [
    id 139
    label "get"
  ]
  node [
    id 140
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 141
    label "mierzy&#263;"
  ]
  node [
    id 142
    label "u&#380;ywa&#263;"
  ]
  node [
    id 143
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 144
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 145
    label "exsert"
  ]
  node [
    id 146
    label "being"
  ]
  node [
    id 147
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 148
    label "cecha"
  ]
  node [
    id 149
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 150
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 151
    label "p&#322;ywa&#263;"
  ]
  node [
    id 152
    label "run"
  ]
  node [
    id 153
    label "bangla&#263;"
  ]
  node [
    id 154
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 155
    label "przebiega&#263;"
  ]
  node [
    id 156
    label "wk&#322;ada&#263;"
  ]
  node [
    id 157
    label "proceed"
  ]
  node [
    id 158
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 159
    label "carry"
  ]
  node [
    id 160
    label "bywa&#263;"
  ]
  node [
    id 161
    label "dziama&#263;"
  ]
  node [
    id 162
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 163
    label "stara&#263;_si&#281;"
  ]
  node [
    id 164
    label "para"
  ]
  node [
    id 165
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 166
    label "str&#243;j"
  ]
  node [
    id 167
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 168
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 169
    label "krok"
  ]
  node [
    id 170
    label "tryb"
  ]
  node [
    id 171
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 172
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 173
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 174
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 175
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 176
    label "continue"
  ]
  node [
    id 177
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 178
    label "Ohio"
  ]
  node [
    id 179
    label "wci&#281;cie"
  ]
  node [
    id 180
    label "Nowy_York"
  ]
  node [
    id 181
    label "warstwa"
  ]
  node [
    id 182
    label "samopoczucie"
  ]
  node [
    id 183
    label "Illinois"
  ]
  node [
    id 184
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 185
    label "state"
  ]
  node [
    id 186
    label "Jukatan"
  ]
  node [
    id 187
    label "Kalifornia"
  ]
  node [
    id 188
    label "Wirginia"
  ]
  node [
    id 189
    label "wektor"
  ]
  node [
    id 190
    label "Goa"
  ]
  node [
    id 191
    label "Teksas"
  ]
  node [
    id 192
    label "Waszyngton"
  ]
  node [
    id 193
    label "Massachusetts"
  ]
  node [
    id 194
    label "Alaska"
  ]
  node [
    id 195
    label "Arakan"
  ]
  node [
    id 196
    label "Hawaje"
  ]
  node [
    id 197
    label "Maryland"
  ]
  node [
    id 198
    label "punkt"
  ]
  node [
    id 199
    label "Michigan"
  ]
  node [
    id 200
    label "Arizona"
  ]
  node [
    id 201
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 202
    label "Georgia"
  ]
  node [
    id 203
    label "Pensylwania"
  ]
  node [
    id 204
    label "shape"
  ]
  node [
    id 205
    label "Luizjana"
  ]
  node [
    id 206
    label "Nowy_Meksyk"
  ]
  node [
    id 207
    label "Alabama"
  ]
  node [
    id 208
    label "ilo&#347;&#263;"
  ]
  node [
    id 209
    label "Kansas"
  ]
  node [
    id 210
    label "Oregon"
  ]
  node [
    id 211
    label "Oklahoma"
  ]
  node [
    id 212
    label "Floryda"
  ]
  node [
    id 213
    label "jednostka_administracyjna"
  ]
  node [
    id 214
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 215
    label "podmiot"
  ]
  node [
    id 216
    label "wykupienie"
  ]
  node [
    id 217
    label "bycie_w_posiadaniu"
  ]
  node [
    id 218
    label "wykupywanie"
  ]
  node [
    id 219
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 220
    label "byt"
  ]
  node [
    id 221
    label "cz&#322;owiek"
  ]
  node [
    id 222
    label "osobowo&#347;&#263;"
  ]
  node [
    id 223
    label "organizacja"
  ]
  node [
    id 224
    label "prawo"
  ]
  node [
    id 225
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 226
    label "nauka_prawa"
  ]
  node [
    id 227
    label "kupowanie"
  ]
  node [
    id 228
    label "odkupywanie"
  ]
  node [
    id 229
    label "wyczerpywanie"
  ]
  node [
    id 230
    label "wyswobadzanie"
  ]
  node [
    id 231
    label "wyczerpanie"
  ]
  node [
    id 232
    label "odkupienie"
  ]
  node [
    id 233
    label "wyswobodzenie"
  ]
  node [
    id 234
    label "kupienie"
  ]
  node [
    id 235
    label "ransom"
  ]
  node [
    id 236
    label "pusty"
  ]
  node [
    id 237
    label "przezroczysty"
  ]
  node [
    id 238
    label "chorobliwy"
  ]
  node [
    id 239
    label "szkli&#347;cie"
  ]
  node [
    id 240
    label "oboj&#281;tny"
  ]
  node [
    id 241
    label "d&#378;wi&#281;czny"
  ]
  node [
    id 242
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 243
    label "prze&#378;roczy"
  ]
  node [
    id 244
    label "przezroczy&#347;cie"
  ]
  node [
    id 245
    label "blady"
  ]
  node [
    id 246
    label "klarowanie"
  ]
  node [
    id 247
    label "klarowanie_si&#281;"
  ]
  node [
    id 248
    label "sklarowanie"
  ]
  node [
    id 249
    label "czysty"
  ]
  node [
    id 250
    label "przezroczysto"
  ]
  node [
    id 251
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 252
    label "skrawy"
  ]
  node [
    id 253
    label "b&#322;yszcz&#261;co"
  ]
  node [
    id 254
    label "przepe&#322;niony"
  ]
  node [
    id 255
    label "zoboj&#281;tnienie"
  ]
  node [
    id 256
    label "nieszkodliwy"
  ]
  node [
    id 257
    label "&#347;ni&#281;ty"
  ]
  node [
    id 258
    label "oboj&#281;tnie"
  ]
  node [
    id 259
    label "neutralizowanie_si&#281;"
  ]
  node [
    id 260
    label "zneutralizowanie_si&#281;"
  ]
  node [
    id 261
    label "niewa&#380;ny"
  ]
  node [
    id 262
    label "neutralizowanie"
  ]
  node [
    id 263
    label "bierny"
  ]
  node [
    id 264
    label "zneutralizowanie"
  ]
  node [
    id 265
    label "neutralny"
  ]
  node [
    id 266
    label "wysychanie"
  ]
  node [
    id 267
    label "ja&#322;owy"
  ]
  node [
    id 268
    label "zoboj&#281;tnia&#322;y"
  ]
  node [
    id 269
    label "wyschni&#281;cie"
  ]
  node [
    id 270
    label "opr&#243;&#380;nianie_si&#281;"
  ]
  node [
    id 271
    label "bezmy&#347;lny"
  ]
  node [
    id 272
    label "do_czysta"
  ]
  node [
    id 273
    label "pusto"
  ]
  node [
    id 274
    label "opr&#243;&#380;nienie_si&#281;"
  ]
  node [
    id 275
    label "g&#322;upi"
  ]
  node [
    id 276
    label "nijaki"
  ]
  node [
    id 277
    label "straszny"
  ]
  node [
    id 278
    label "nadzwyczajny"
  ]
  node [
    id 279
    label "niepokoj&#261;cy"
  ]
  node [
    id 280
    label "chorobliwie"
  ]
  node [
    id 281
    label "nienormalny"
  ]
  node [
    id 282
    label "nieprawid&#322;owy"
  ]
  node [
    id 283
    label "d&#378;wi&#281;cznie"
  ]
  node [
    id 284
    label "efektowny"
  ]
  node [
    id 285
    label "pi&#281;kny"
  ]
  node [
    id 286
    label "klarowny"
  ]
  node [
    id 287
    label "nieprzytomnie"
  ]
  node [
    id 288
    label "nieruchomo"
  ]
  node [
    id 289
    label "szklisty"
  ]
  node [
    id 290
    label "transparently"
  ]
  node [
    id 291
    label "m&#281;tnie"
  ]
  node [
    id 292
    label "metalicznie"
  ]
  node [
    id 293
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 294
    label "rodzina"
  ]
  node [
    id 295
    label "substancja_mieszkaniowa"
  ]
  node [
    id 296
    label "instytucja"
  ]
  node [
    id 297
    label "siedziba"
  ]
  node [
    id 298
    label "dom_rodzinny"
  ]
  node [
    id 299
    label "budynek"
  ]
  node [
    id 300
    label "grupa"
  ]
  node [
    id 301
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 302
    label "stead"
  ]
  node [
    id 303
    label "garderoba"
  ]
  node [
    id 304
    label "wiecha"
  ]
  node [
    id 305
    label "fratria"
  ]
  node [
    id 306
    label "plemi&#281;"
  ]
  node [
    id 307
    label "family"
  ]
  node [
    id 308
    label "moiety"
  ]
  node [
    id 309
    label "odzie&#380;"
  ]
  node [
    id 310
    label "szatnia"
  ]
  node [
    id 311
    label "szafa_ubraniowa"
  ]
  node [
    id 312
    label "pomieszczenie"
  ]
  node [
    id 313
    label "powinowaci"
  ]
  node [
    id 314
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 315
    label "Radziwi&#322;&#322;owie"
  ]
  node [
    id 316
    label "rodze&#324;stwo"
  ]
  node [
    id 317
    label "jednostka_systematyczna"
  ]
  node [
    id 318
    label "krewni"
  ]
  node [
    id 319
    label "Ossoli&#324;scy"
  ]
  node [
    id 320
    label "potomstwo"
  ]
  node [
    id 321
    label "&#346;wi&#281;ta_Rodzina"
  ]
  node [
    id 322
    label "theater"
  ]
  node [
    id 323
    label "zbi&#243;r"
  ]
  node [
    id 324
    label "Soplicowie"
  ]
  node [
    id 325
    label "kin"
  ]
  node [
    id 326
    label "rodzice"
  ]
  node [
    id 327
    label "ordynacja"
  ]
  node [
    id 328
    label "Ostrogscy"
  ]
  node [
    id 329
    label "bliscy"
  ]
  node [
    id 330
    label "przyjaciel_domu"
  ]
  node [
    id 331
    label "rz&#261;d"
  ]
  node [
    id 332
    label "Firlejowie"
  ]
  node [
    id 333
    label "Kossakowie"
  ]
  node [
    id 334
    label "Czartoryscy"
  ]
  node [
    id 335
    label "Sapiehowie"
  ]
  node [
    id 336
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 337
    label "mienie"
  ]
  node [
    id 338
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 339
    label "rzecz"
  ]
  node [
    id 340
    label "immoblizacja"
  ]
  node [
    id 341
    label "balkon"
  ]
  node [
    id 342
    label "budowla"
  ]
  node [
    id 343
    label "pod&#322;oga"
  ]
  node [
    id 344
    label "kondygnacja"
  ]
  node [
    id 345
    label "skrzyd&#322;o"
  ]
  node [
    id 346
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 347
    label "dach"
  ]
  node [
    id 348
    label "strop"
  ]
  node [
    id 349
    label "klatka_schodowa"
  ]
  node [
    id 350
    label "przedpro&#380;e"
  ]
  node [
    id 351
    label "Pentagon"
  ]
  node [
    id 352
    label "alkierz"
  ]
  node [
    id 353
    label "front"
  ]
  node [
    id 354
    label "&#321;ubianka"
  ]
  node [
    id 355
    label "miejsce_pracy"
  ]
  node [
    id 356
    label "dzia&#322;_personalny"
  ]
  node [
    id 357
    label "Kreml"
  ]
  node [
    id 358
    label "Bia&#322;y_Dom"
  ]
  node [
    id 359
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 360
    label "sadowisko"
  ]
  node [
    id 361
    label "odm&#322;adzanie"
  ]
  node [
    id 362
    label "liga"
  ]
  node [
    id 363
    label "asymilowanie"
  ]
  node [
    id 364
    label "gromada"
  ]
  node [
    id 365
    label "asymilowa&#263;"
  ]
  node [
    id 366
    label "egzemplarz"
  ]
  node [
    id 367
    label "Entuzjastki"
  ]
  node [
    id 368
    label "kompozycja"
  ]
  node [
    id 369
    label "Terranie"
  ]
  node [
    id 370
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 371
    label "category"
  ]
  node [
    id 372
    label "pakiet_klimatyczny"
  ]
  node [
    id 373
    label "oddzia&#322;"
  ]
  node [
    id 374
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 375
    label "cz&#261;steczka"
  ]
  node [
    id 376
    label "stage_set"
  ]
  node [
    id 377
    label "type"
  ]
  node [
    id 378
    label "specgrupa"
  ]
  node [
    id 379
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 380
    label "&#346;wietliki"
  ]
  node [
    id 381
    label "odm&#322;odzenie"
  ]
  node [
    id 382
    label "Eurogrupa"
  ]
  node [
    id 383
    label "odm&#322;adza&#263;"
  ]
  node [
    id 384
    label "formacja_geologiczna"
  ]
  node [
    id 385
    label "harcerze_starsi"
  ]
  node [
    id 386
    label "osoba_prawna"
  ]
  node [
    id 387
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 388
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 389
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 390
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 391
    label "biuro"
  ]
  node [
    id 392
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 393
    label "Fundusze_Unijne"
  ]
  node [
    id 394
    label "zamyka&#263;"
  ]
  node [
    id 395
    label "establishment"
  ]
  node [
    id 396
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 397
    label "urz&#261;d"
  ]
  node [
    id 398
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 399
    label "afiliowa&#263;"
  ]
  node [
    id 400
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 401
    label "standard"
  ]
  node [
    id 402
    label "zamykanie"
  ]
  node [
    id 403
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 404
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 405
    label "pos&#322;uchanie"
  ]
  node [
    id 406
    label "skumanie"
  ]
  node [
    id 407
    label "orientacja"
  ]
  node [
    id 408
    label "zorientowanie"
  ]
  node [
    id 409
    label "teoria"
  ]
  node [
    id 410
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 411
    label "clasp"
  ]
  node [
    id 412
    label "przem&#243;wienie"
  ]
  node [
    id 413
    label "perch"
  ]
  node [
    id 414
    label "kita"
  ]
  node [
    id 415
    label "wieniec"
  ]
  node [
    id 416
    label "wilk"
  ]
  node [
    id 417
    label "kwiatostan"
  ]
  node [
    id 418
    label "p&#281;k"
  ]
  node [
    id 419
    label "ogon"
  ]
  node [
    id 420
    label "wi&#261;zka"
  ]
  node [
    id 421
    label "Cezary"
  ]
  node [
    id 422
    label "Baryka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 236
  ]
  edge [
    source 9
    target 237
  ]
  edge [
    source 9
    target 238
  ]
  edge [
    source 9
    target 239
  ]
  edge [
    source 9
    target 240
  ]
  edge [
    source 9
    target 241
  ]
  edge [
    source 9
    target 242
  ]
  edge [
    source 9
    target 243
  ]
  edge [
    source 9
    target 244
  ]
  edge [
    source 9
    target 245
  ]
  edge [
    source 9
    target 246
  ]
  edge [
    source 9
    target 247
  ]
  edge [
    source 9
    target 248
  ]
  edge [
    source 9
    target 249
  ]
  edge [
    source 9
    target 250
  ]
  edge [
    source 9
    target 251
  ]
  edge [
    source 9
    target 252
  ]
  edge [
    source 9
    target 253
  ]
  edge [
    source 9
    target 254
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 267
  ]
  edge [
    source 9
    target 268
  ]
  edge [
    source 9
    target 269
  ]
  edge [
    source 9
    target 270
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 273
  ]
  edge [
    source 9
    target 274
  ]
  edge [
    source 9
    target 275
  ]
  edge [
    source 9
    target 276
  ]
  edge [
    source 9
    target 277
  ]
  edge [
    source 9
    target 278
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 10
    target 293
  ]
  edge [
    source 10
    target 294
  ]
  edge [
    source 10
    target 295
  ]
  edge [
    source 10
    target 296
  ]
  edge [
    source 10
    target 297
  ]
  edge [
    source 10
    target 298
  ]
  edge [
    source 10
    target 299
  ]
  edge [
    source 10
    target 300
  ]
  edge [
    source 10
    target 301
  ]
  edge [
    source 10
    target 39
  ]
  edge [
    source 10
    target 302
  ]
  edge [
    source 10
    target 303
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 10
    target 329
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 331
  ]
  edge [
    source 10
    target 332
  ]
  edge [
    source 10
    target 333
  ]
  edge [
    source 10
    target 334
  ]
  edge [
    source 10
    target 335
  ]
  edge [
    source 10
    target 336
  ]
  edge [
    source 10
    target 337
  ]
  edge [
    source 10
    target 338
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 339
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 341
  ]
  edge [
    source 10
    target 342
  ]
  edge [
    source 10
    target 343
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 43
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 421
    target 422
  ]
]
