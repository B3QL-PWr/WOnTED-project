graph [
  node [
    id 0
    label "link"
    origin "text"
  ]
  node [
    id 1
    label "piotrzyla"
    origin "text"
  ]
  node [
    id 2
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 3
    label "skok"
    origin "text"
  ]
  node [
    id 4
    label "pewno"
    origin "text"
  ]
  node [
    id 5
    label "logikarozowychpaskow"
    origin "text"
  ]
  node [
    id 6
    label "odsy&#322;acz"
  ]
  node [
    id 7
    label "buton"
  ]
  node [
    id 8
    label "asterisk"
  ]
  node [
    id 9
    label "gloss"
  ]
  node [
    id 10
    label "znak_pisarski"
  ]
  node [
    id 11
    label "aparat_krytyczny"
  ]
  node [
    id 12
    label "znak_graficzny"
  ]
  node [
    id 13
    label "has&#322;o"
  ]
  node [
    id 14
    label "obja&#347;nienie"
  ]
  node [
    id 15
    label "dopisek"
  ]
  node [
    id 16
    label "klikanie"
  ]
  node [
    id 17
    label "przycisk"
  ]
  node [
    id 18
    label "guzik"
  ]
  node [
    id 19
    label "klika&#263;"
  ]
  node [
    id 20
    label "kolczyk"
  ]
  node [
    id 21
    label "derail"
  ]
  node [
    id 22
    label "noga"
  ]
  node [
    id 23
    label "ptak"
  ]
  node [
    id 24
    label "naskok"
  ]
  node [
    id 25
    label "struktura_anatomiczna"
  ]
  node [
    id 26
    label "wybicie"
  ]
  node [
    id 27
    label "l&#261;dowanie"
  ]
  node [
    id 28
    label "konkurencja"
  ]
  node [
    id 29
    label "caper"
  ]
  node [
    id 30
    label "stroke"
  ]
  node [
    id 31
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 32
    label "zaj&#261;c"
  ]
  node [
    id 33
    label "ruch"
  ]
  node [
    id 34
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 35
    label "&#322;apa"
  ]
  node [
    id 36
    label "zmiana"
  ]
  node [
    id 37
    label "napad"
  ]
  node [
    id 38
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 39
    label "mechanika"
  ]
  node [
    id 40
    label "utrzymywanie"
  ]
  node [
    id 41
    label "move"
  ]
  node [
    id 42
    label "poruszenie"
  ]
  node [
    id 43
    label "movement"
  ]
  node [
    id 44
    label "myk"
  ]
  node [
    id 45
    label "utrzyma&#263;"
  ]
  node [
    id 46
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 47
    label "zjawisko"
  ]
  node [
    id 48
    label "utrzymanie"
  ]
  node [
    id 49
    label "travel"
  ]
  node [
    id 50
    label "kanciasty"
  ]
  node [
    id 51
    label "commercial_enterprise"
  ]
  node [
    id 52
    label "model"
  ]
  node [
    id 53
    label "strumie&#324;"
  ]
  node [
    id 54
    label "proces"
  ]
  node [
    id 55
    label "aktywno&#347;&#263;"
  ]
  node [
    id 56
    label "kr&#243;tki"
  ]
  node [
    id 57
    label "taktyka"
  ]
  node [
    id 58
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 59
    label "apraksja"
  ]
  node [
    id 60
    label "natural_process"
  ]
  node [
    id 61
    label "utrzymywa&#263;"
  ]
  node [
    id 62
    label "d&#322;ugi"
  ]
  node [
    id 63
    label "wydarzenie"
  ]
  node [
    id 64
    label "dyssypacja_energii"
  ]
  node [
    id 65
    label "tumult"
  ]
  node [
    id 66
    label "stopek"
  ]
  node [
    id 67
    label "czynno&#347;&#263;"
  ]
  node [
    id 68
    label "manewr"
  ]
  node [
    id 69
    label "lokomocja"
  ]
  node [
    id 70
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 71
    label "komunikacja"
  ]
  node [
    id 72
    label "drift"
  ]
  node [
    id 73
    label "rzuci&#263;"
  ]
  node [
    id 74
    label "przest&#281;pstwo"
  ]
  node [
    id 75
    label "spasm"
  ]
  node [
    id 76
    label "oznaka"
  ]
  node [
    id 77
    label "rzucenie"
  ]
  node [
    id 78
    label "atak"
  ]
  node [
    id 79
    label "fit"
  ]
  node [
    id 80
    label "fire"
  ]
  node [
    id 81
    label "kaszel"
  ]
  node [
    id 82
    label "reakcja"
  ]
  node [
    id 83
    label "interakcja"
  ]
  node [
    id 84
    label "firma"
  ]
  node [
    id 85
    label "uczestnik"
  ]
  node [
    id 86
    label "contest"
  ]
  node [
    id 87
    label "dyscyplina_sportowa"
  ]
  node [
    id 88
    label "rywalizacja"
  ]
  node [
    id 89
    label "dob&#243;r_naturalny"
  ]
  node [
    id 90
    label "d&#322;o&#324;"
  ]
  node [
    id 91
    label "poduszka"
  ]
  node [
    id 92
    label "Rzym_Zachodni"
  ]
  node [
    id 93
    label "whole"
  ]
  node [
    id 94
    label "ilo&#347;&#263;"
  ]
  node [
    id 95
    label "element"
  ]
  node [
    id 96
    label "Rzym_Wschodni"
  ]
  node [
    id 97
    label "urz&#261;dzenie"
  ]
  node [
    id 98
    label "rewizja"
  ]
  node [
    id 99
    label "passage"
  ]
  node [
    id 100
    label "change"
  ]
  node [
    id 101
    label "ferment"
  ]
  node [
    id 102
    label "komplet"
  ]
  node [
    id 103
    label "anatomopatolog"
  ]
  node [
    id 104
    label "zmianka"
  ]
  node [
    id 105
    label "czas"
  ]
  node [
    id 106
    label "amendment"
  ]
  node [
    id 107
    label "praca"
  ]
  node [
    id 108
    label "odmienianie"
  ]
  node [
    id 109
    label "tura"
  ]
  node [
    id 110
    label "przej&#347;cie"
  ]
  node [
    id 111
    label "krytyka"
  ]
  node [
    id 112
    label "wypowied&#378;"
  ]
  node [
    id 113
    label "ofensywa"
  ]
  node [
    id 114
    label "knock"
  ]
  node [
    id 115
    label "lot"
  ]
  node [
    id 116
    label "descent"
  ]
  node [
    id 117
    label "trafienie"
  ]
  node [
    id 118
    label "trafianie"
  ]
  node [
    id 119
    label "przybycie"
  ]
  node [
    id 120
    label "radzenie_sobie"
  ]
  node [
    id 121
    label "poradzenie_sobie"
  ]
  node [
    id 122
    label "dobijanie"
  ]
  node [
    id 123
    label "podr&#243;&#380;owanie"
  ]
  node [
    id 124
    label "lecenie"
  ]
  node [
    id 125
    label "przybywanie"
  ]
  node [
    id 126
    label "dobicie"
  ]
  node [
    id 127
    label "destruction"
  ]
  node [
    id 128
    label "strike"
  ]
  node [
    id 129
    label "pokrycie"
  ]
  node [
    id 130
    label "wyt&#322;oczenie"
  ]
  node [
    id 131
    label "spowodowanie"
  ]
  node [
    id 132
    label "przebicie"
  ]
  node [
    id 133
    label "respite"
  ]
  node [
    id 134
    label "powybijanie"
  ]
  node [
    id 135
    label "wskazanie"
  ]
  node [
    id 136
    label "otw&#243;r"
  ]
  node [
    id 137
    label "nadanie"
  ]
  node [
    id 138
    label "pozabijanie"
  ]
  node [
    id 139
    label "zniszczenie"
  ]
  node [
    id 140
    label "try&#347;ni&#281;cie"
  ]
  node [
    id 141
    label "interruption"
  ]
  node [
    id 142
    label "wypadni&#281;cie"
  ]
  node [
    id 143
    label "zabrzmienie"
  ]
  node [
    id 144
    label "pobicie"
  ]
  node [
    id 145
    label "rytm"
  ]
  node [
    id 146
    label "zrobienie"
  ]
  node [
    id 147
    label "skrom"
  ]
  node [
    id 148
    label "&#322;o&#380;yskowiec"
  ]
  node [
    id 149
    label "trzeszcze"
  ]
  node [
    id 150
    label "zaj&#261;cowate"
  ]
  node [
    id 151
    label "kicaj"
  ]
  node [
    id 152
    label "omyk"
  ]
  node [
    id 153
    label "kopyra"
  ]
  node [
    id 154
    label "dziczyzna"
  ]
  node [
    id 155
    label "turzyca"
  ]
  node [
    id 156
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 157
    label "parkot"
  ]
  node [
    id 158
    label "dogrywa&#263;"
  ]
  node [
    id 159
    label "s&#322;abeusz"
  ]
  node [
    id 160
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 161
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 162
    label "czpas"
  ]
  node [
    id 163
    label "nerw_udowy"
  ]
  node [
    id 164
    label "bezbramkowy"
  ]
  node [
    id 165
    label "podpora"
  ]
  node [
    id 166
    label "faulowa&#263;"
  ]
  node [
    id 167
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 168
    label "zamurowanie"
  ]
  node [
    id 169
    label "depta&#263;"
  ]
  node [
    id 170
    label "mi&#281;czak"
  ]
  node [
    id 171
    label "stopa"
  ]
  node [
    id 172
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 173
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 174
    label "mato&#322;"
  ]
  node [
    id 175
    label "ekstraklasa"
  ]
  node [
    id 176
    label "sfaulowa&#263;"
  ]
  node [
    id 177
    label "&#322;&#261;czyna"
  ]
  node [
    id 178
    label "lobowanie"
  ]
  node [
    id 179
    label "dogrywanie"
  ]
  node [
    id 180
    label "napinacz"
  ]
  node [
    id 181
    label "dublet"
  ]
  node [
    id 182
    label "sfaulowanie"
  ]
  node [
    id 183
    label "lobowa&#263;"
  ]
  node [
    id 184
    label "gira"
  ]
  node [
    id 185
    label "bramkarz"
  ]
  node [
    id 186
    label "faulowanie"
  ]
  node [
    id 187
    label "zamurowywanie"
  ]
  node [
    id 188
    label "kopni&#281;cie"
  ]
  node [
    id 189
    label "&#322;amaga"
  ]
  node [
    id 190
    label "kopn&#261;&#263;"
  ]
  node [
    id 191
    label "dogranie"
  ]
  node [
    id 192
    label "kopanie"
  ]
  node [
    id 193
    label "pi&#322;ka"
  ]
  node [
    id 194
    label "przelobowa&#263;"
  ]
  node [
    id 195
    label "mundial"
  ]
  node [
    id 196
    label "kopa&#263;"
  ]
  node [
    id 197
    label "r&#281;ka"
  ]
  node [
    id 198
    label "catenaccio"
  ]
  node [
    id 199
    label "dogra&#263;"
  ]
  node [
    id 200
    label "ko&#324;czyna"
  ]
  node [
    id 201
    label "tackle"
  ]
  node [
    id 202
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 203
    label "narz&#261;d_ruchu"
  ]
  node [
    id 204
    label "zamurowywa&#263;"
  ]
  node [
    id 205
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 206
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 207
    label "interliga"
  ]
  node [
    id 208
    label "przelobowanie"
  ]
  node [
    id 209
    label "czerwona_kartka"
  ]
  node [
    id 210
    label "Wis&#322;a"
  ]
  node [
    id 211
    label "zamurowa&#263;"
  ]
  node [
    id 212
    label "jedenastka"
  ]
  node [
    id 213
    label "dziobni&#281;cie"
  ]
  node [
    id 214
    label "wysiadywa&#263;"
  ]
  node [
    id 215
    label "ko&#347;&#263;_potyliczna"
  ]
  node [
    id 216
    label "dzioba&#263;"
  ]
  node [
    id 217
    label "grzebie&#324;"
  ]
  node [
    id 218
    label "pi&#243;ro"
  ]
  node [
    id 219
    label "ptaki"
  ]
  node [
    id 220
    label "kuper"
  ]
  node [
    id 221
    label "hukni&#281;cie"
  ]
  node [
    id 222
    label "dziobn&#261;&#263;"
  ]
  node [
    id 223
    label "ptasz&#281;"
  ]
  node [
    id 224
    label "skrzyd&#322;o"
  ]
  node [
    id 225
    label "kloaka"
  ]
  node [
    id 226
    label "dzi&#243;bn&#261;&#263;"
  ]
  node [
    id 227
    label "wysiedzie&#263;"
  ]
  node [
    id 228
    label "upierzenie"
  ]
  node [
    id 229
    label "bird"
  ]
  node [
    id 230
    label "dziobanie"
  ]
  node [
    id 231
    label "pogwizdywanie"
  ]
  node [
    id 232
    label "dzi&#243;b"
  ]
  node [
    id 233
    label "ptactwo"
  ]
  node [
    id 234
    label "roz&#263;wierkanie_si&#281;"
  ]
  node [
    id 235
    label "zaklekotanie"
  ]
  node [
    id 236
    label "owodniowiec"
  ]
  node [
    id 237
    label "dzi&#243;bni&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 4
    target 5
  ]
]
