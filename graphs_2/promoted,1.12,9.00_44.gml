graph [
  node [
    id 0
    label "jarmark"
    origin "text"
  ]
  node [
    id 1
    label "bo&#380;onarodzeniowy"
    origin "text"
  ]
  node [
    id 2
    label "dortmund"
    origin "text"
  ]
  node [
    id 3
    label "jeden"
    origin "text"
  ]
  node [
    id 4
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 5
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 6
    label "plac"
  ]
  node [
    id 7
    label "market"
  ]
  node [
    id 8
    label "targ"
  ]
  node [
    id 9
    label "stoisko"
  ]
  node [
    id 10
    label "obiekt_handlowy"
  ]
  node [
    id 11
    label "targowica"
  ]
  node [
    id 12
    label "kram"
  ]
  node [
    id 13
    label "&#321;ubianka"
  ]
  node [
    id 14
    label "area"
  ]
  node [
    id 15
    label "Majdan"
  ]
  node [
    id 16
    label "pole_bitwy"
  ]
  node [
    id 17
    label "przestrze&#324;"
  ]
  node [
    id 18
    label "obszar"
  ]
  node [
    id 19
    label "pierzeja"
  ]
  node [
    id 20
    label "miejsce"
  ]
  node [
    id 21
    label "zgromadzenie"
  ]
  node [
    id 22
    label "miasto"
  ]
  node [
    id 23
    label "sprzeda&#380;"
  ]
  node [
    id 24
    label "szmartuz"
  ]
  node [
    id 25
    label "kramnica"
  ]
  node [
    id 26
    label "zdrada"
  ]
  node [
    id 27
    label "sklep"
  ]
  node [
    id 28
    label "shot"
  ]
  node [
    id 29
    label "jednakowy"
  ]
  node [
    id 30
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 31
    label "ujednolicenie"
  ]
  node [
    id 32
    label "jaki&#347;"
  ]
  node [
    id 33
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 34
    label "jednolicie"
  ]
  node [
    id 35
    label "kieliszek"
  ]
  node [
    id 36
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 37
    label "w&#243;dka"
  ]
  node [
    id 38
    label "ten"
  ]
  node [
    id 39
    label "szk&#322;o"
  ]
  node [
    id 40
    label "zawarto&#347;&#263;"
  ]
  node [
    id 41
    label "naczynie"
  ]
  node [
    id 42
    label "alkohol"
  ]
  node [
    id 43
    label "sznaps"
  ]
  node [
    id 44
    label "nap&#243;j"
  ]
  node [
    id 45
    label "gorza&#322;ka"
  ]
  node [
    id 46
    label "mohorycz"
  ]
  node [
    id 47
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 48
    label "zr&#243;wnanie"
  ]
  node [
    id 49
    label "mundurowanie"
  ]
  node [
    id 50
    label "taki&#380;"
  ]
  node [
    id 51
    label "jednakowo"
  ]
  node [
    id 52
    label "mundurowa&#263;"
  ]
  node [
    id 53
    label "zr&#243;wnywanie"
  ]
  node [
    id 54
    label "identyczny"
  ]
  node [
    id 55
    label "okre&#347;lony"
  ]
  node [
    id 56
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 57
    label "z&#322;o&#380;ony"
  ]
  node [
    id 58
    label "przyzwoity"
  ]
  node [
    id 59
    label "ciekawy"
  ]
  node [
    id 60
    label "jako&#347;"
  ]
  node [
    id 61
    label "jako_tako"
  ]
  node [
    id 62
    label "niez&#322;y"
  ]
  node [
    id 63
    label "dziwny"
  ]
  node [
    id 64
    label "charakterystyczny"
  ]
  node [
    id 65
    label "g&#322;&#281;bszy"
  ]
  node [
    id 66
    label "drink"
  ]
  node [
    id 67
    label "jednolity"
  ]
  node [
    id 68
    label "upodobnienie"
  ]
  node [
    id 69
    label "calibration"
  ]
  node [
    id 70
    label "doros&#322;y"
  ]
  node [
    id 71
    label "znaczny"
  ]
  node [
    id 72
    label "niema&#322;o"
  ]
  node [
    id 73
    label "wiele"
  ]
  node [
    id 74
    label "rozwini&#281;ty"
  ]
  node [
    id 75
    label "dorodny"
  ]
  node [
    id 76
    label "wa&#380;ny"
  ]
  node [
    id 77
    label "prawdziwy"
  ]
  node [
    id 78
    label "du&#380;o"
  ]
  node [
    id 79
    label "&#380;ywny"
  ]
  node [
    id 80
    label "szczery"
  ]
  node [
    id 81
    label "naturalny"
  ]
  node [
    id 82
    label "naprawd&#281;"
  ]
  node [
    id 83
    label "realnie"
  ]
  node [
    id 84
    label "podobny"
  ]
  node [
    id 85
    label "zgodny"
  ]
  node [
    id 86
    label "m&#261;dry"
  ]
  node [
    id 87
    label "prawdziwie"
  ]
  node [
    id 88
    label "znacznie"
  ]
  node [
    id 89
    label "zauwa&#380;alny"
  ]
  node [
    id 90
    label "wynios&#322;y"
  ]
  node [
    id 91
    label "dono&#347;ny"
  ]
  node [
    id 92
    label "silny"
  ]
  node [
    id 93
    label "wa&#380;nie"
  ]
  node [
    id 94
    label "istotnie"
  ]
  node [
    id 95
    label "eksponowany"
  ]
  node [
    id 96
    label "dobry"
  ]
  node [
    id 97
    label "ukszta&#322;towany"
  ]
  node [
    id 98
    label "do&#347;cig&#322;y"
  ]
  node [
    id 99
    label "&#378;ra&#322;y"
  ]
  node [
    id 100
    label "zdr&#243;w"
  ]
  node [
    id 101
    label "dorodnie"
  ]
  node [
    id 102
    label "okaza&#322;y"
  ]
  node [
    id 103
    label "mocno"
  ]
  node [
    id 104
    label "wiela"
  ]
  node [
    id 105
    label "bardzo"
  ]
  node [
    id 106
    label "cz&#281;sto"
  ]
  node [
    id 107
    label "wydoro&#347;lenie"
  ]
  node [
    id 108
    label "cz&#322;owiek"
  ]
  node [
    id 109
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 110
    label "doro&#347;lenie"
  ]
  node [
    id 111
    label "doro&#347;le"
  ]
  node [
    id 112
    label "senior"
  ]
  node [
    id 113
    label "dojrzale"
  ]
  node [
    id 114
    label "wapniak"
  ]
  node [
    id 115
    label "dojrza&#322;y"
  ]
  node [
    id 116
    label "doletni"
  ]
  node [
    id 117
    label "Stary_&#346;wiat"
  ]
  node [
    id 118
    label "asymilowanie_si&#281;"
  ]
  node [
    id 119
    label "p&#243;&#322;noc"
  ]
  node [
    id 120
    label "przedmiot"
  ]
  node [
    id 121
    label "Wsch&#243;d"
  ]
  node [
    id 122
    label "class"
  ]
  node [
    id 123
    label "geosfera"
  ]
  node [
    id 124
    label "obiekt_naturalny"
  ]
  node [
    id 125
    label "przejmowanie"
  ]
  node [
    id 126
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 127
    label "przyroda"
  ]
  node [
    id 128
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 129
    label "po&#322;udnie"
  ]
  node [
    id 130
    label "zjawisko"
  ]
  node [
    id 131
    label "rzecz"
  ]
  node [
    id 132
    label "makrokosmos"
  ]
  node [
    id 133
    label "huczek"
  ]
  node [
    id 134
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 135
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 136
    label "environment"
  ]
  node [
    id 137
    label "morze"
  ]
  node [
    id 138
    label "rze&#378;ba"
  ]
  node [
    id 139
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 140
    label "przejmowa&#263;"
  ]
  node [
    id 141
    label "hydrosfera"
  ]
  node [
    id 142
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 143
    label "ciemna_materia"
  ]
  node [
    id 144
    label "ekosystem"
  ]
  node [
    id 145
    label "biota"
  ]
  node [
    id 146
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 147
    label "planeta"
  ]
  node [
    id 148
    label "geotermia"
  ]
  node [
    id 149
    label "ekosfera"
  ]
  node [
    id 150
    label "ozonosfera"
  ]
  node [
    id 151
    label "wszechstworzenie"
  ]
  node [
    id 152
    label "grupa"
  ]
  node [
    id 153
    label "woda"
  ]
  node [
    id 154
    label "kuchnia"
  ]
  node [
    id 155
    label "biosfera"
  ]
  node [
    id 156
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 157
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 158
    label "populace"
  ]
  node [
    id 159
    label "magnetosfera"
  ]
  node [
    id 160
    label "Nowy_&#346;wiat"
  ]
  node [
    id 161
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 162
    label "universe"
  ]
  node [
    id 163
    label "biegun"
  ]
  node [
    id 164
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 165
    label "litosfera"
  ]
  node [
    id 166
    label "teren"
  ]
  node [
    id 167
    label "mikrokosmos"
  ]
  node [
    id 168
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 169
    label "stw&#243;r"
  ]
  node [
    id 170
    label "p&#243;&#322;kula"
  ]
  node [
    id 171
    label "przej&#281;cie"
  ]
  node [
    id 172
    label "barysfera"
  ]
  node [
    id 173
    label "czarna_dziura"
  ]
  node [
    id 174
    label "atmosfera"
  ]
  node [
    id 175
    label "przej&#261;&#263;"
  ]
  node [
    id 176
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 177
    label "Ziemia"
  ]
  node [
    id 178
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 179
    label "geoida"
  ]
  node [
    id 180
    label "zagranica"
  ]
  node [
    id 181
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 182
    label "fauna"
  ]
  node [
    id 183
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 184
    label "odm&#322;adzanie"
  ]
  node [
    id 185
    label "liga"
  ]
  node [
    id 186
    label "jednostka_systematyczna"
  ]
  node [
    id 187
    label "asymilowanie"
  ]
  node [
    id 188
    label "gromada"
  ]
  node [
    id 189
    label "asymilowa&#263;"
  ]
  node [
    id 190
    label "egzemplarz"
  ]
  node [
    id 191
    label "Entuzjastki"
  ]
  node [
    id 192
    label "zbi&#243;r"
  ]
  node [
    id 193
    label "kompozycja"
  ]
  node [
    id 194
    label "Terranie"
  ]
  node [
    id 195
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 196
    label "category"
  ]
  node [
    id 197
    label "pakiet_klimatyczny"
  ]
  node [
    id 198
    label "oddzia&#322;"
  ]
  node [
    id 199
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 200
    label "cz&#261;steczka"
  ]
  node [
    id 201
    label "stage_set"
  ]
  node [
    id 202
    label "type"
  ]
  node [
    id 203
    label "specgrupa"
  ]
  node [
    id 204
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 205
    label "&#346;wietliki"
  ]
  node [
    id 206
    label "odm&#322;odzenie"
  ]
  node [
    id 207
    label "Eurogrupa"
  ]
  node [
    id 208
    label "odm&#322;adza&#263;"
  ]
  node [
    id 209
    label "formacja_geologiczna"
  ]
  node [
    id 210
    label "harcerze_starsi"
  ]
  node [
    id 211
    label "Kosowo"
  ]
  node [
    id 212
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 213
    label "Zab&#322;ocie"
  ]
  node [
    id 214
    label "zach&#243;d"
  ]
  node [
    id 215
    label "Pow&#261;zki"
  ]
  node [
    id 216
    label "Piotrowo"
  ]
  node [
    id 217
    label "Olszanica"
  ]
  node [
    id 218
    label "holarktyka"
  ]
  node [
    id 219
    label "Ruda_Pabianicka"
  ]
  node [
    id 220
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 221
    label "Ludwin&#243;w"
  ]
  node [
    id 222
    label "Arktyka"
  ]
  node [
    id 223
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 224
    label "Zabu&#380;e"
  ]
  node [
    id 225
    label "antroposfera"
  ]
  node [
    id 226
    label "terytorium"
  ]
  node [
    id 227
    label "Neogea"
  ]
  node [
    id 228
    label "Syberia_Zachodnia"
  ]
  node [
    id 229
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 230
    label "zakres"
  ]
  node [
    id 231
    label "pas_planetoid"
  ]
  node [
    id 232
    label "Syberia_Wschodnia"
  ]
  node [
    id 233
    label "Antarktyka"
  ]
  node [
    id 234
    label "Rakowice"
  ]
  node [
    id 235
    label "akrecja"
  ]
  node [
    id 236
    label "wymiar"
  ]
  node [
    id 237
    label "&#321;&#281;g"
  ]
  node [
    id 238
    label "Kresy_Zachodnie"
  ]
  node [
    id 239
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 240
    label "wsch&#243;d"
  ]
  node [
    id 241
    label "Notogea"
  ]
  node [
    id 242
    label "integer"
  ]
  node [
    id 243
    label "liczba"
  ]
  node [
    id 244
    label "zlewanie_si&#281;"
  ]
  node [
    id 245
    label "ilo&#347;&#263;"
  ]
  node [
    id 246
    label "uk&#322;ad"
  ]
  node [
    id 247
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 248
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 249
    label "pe&#322;ny"
  ]
  node [
    id 250
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 251
    label "proces"
  ]
  node [
    id 252
    label "boski"
  ]
  node [
    id 253
    label "krajobraz"
  ]
  node [
    id 254
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 255
    label "przywidzenie"
  ]
  node [
    id 256
    label "presence"
  ]
  node [
    id 257
    label "charakter"
  ]
  node [
    id 258
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 259
    label "rozdzielanie"
  ]
  node [
    id 260
    label "bezbrze&#380;e"
  ]
  node [
    id 261
    label "punkt"
  ]
  node [
    id 262
    label "czasoprzestrze&#324;"
  ]
  node [
    id 263
    label "niezmierzony"
  ]
  node [
    id 264
    label "przedzielenie"
  ]
  node [
    id 265
    label "nielito&#347;ciwy"
  ]
  node [
    id 266
    label "rozdziela&#263;"
  ]
  node [
    id 267
    label "oktant"
  ]
  node [
    id 268
    label "przedzieli&#263;"
  ]
  node [
    id 269
    label "przestw&#243;r"
  ]
  node [
    id 270
    label "&#347;rodowisko"
  ]
  node [
    id 271
    label "rura"
  ]
  node [
    id 272
    label "grzebiuszka"
  ]
  node [
    id 273
    label "atom"
  ]
  node [
    id 274
    label "odbicie"
  ]
  node [
    id 275
    label "kosmos"
  ]
  node [
    id 276
    label "miniatura"
  ]
  node [
    id 277
    label "smok_wawelski"
  ]
  node [
    id 278
    label "niecz&#322;owiek"
  ]
  node [
    id 279
    label "monster"
  ]
  node [
    id 280
    label "istota_&#380;ywa"
  ]
  node [
    id 281
    label "potw&#243;r"
  ]
  node [
    id 282
    label "istota_fantastyczna"
  ]
  node [
    id 283
    label "kultura"
  ]
  node [
    id 284
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 285
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 286
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 287
    label "aspekt"
  ]
  node [
    id 288
    label "troposfera"
  ]
  node [
    id 289
    label "klimat"
  ]
  node [
    id 290
    label "metasfera"
  ]
  node [
    id 291
    label "atmosferyki"
  ]
  node [
    id 292
    label "homosfera"
  ]
  node [
    id 293
    label "cecha"
  ]
  node [
    id 294
    label "powietrznia"
  ]
  node [
    id 295
    label "jonosfera"
  ]
  node [
    id 296
    label "termosfera"
  ]
  node [
    id 297
    label "egzosfera"
  ]
  node [
    id 298
    label "heterosfera"
  ]
  node [
    id 299
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 300
    label "tropopauza"
  ]
  node [
    id 301
    label "kwas"
  ]
  node [
    id 302
    label "powietrze"
  ]
  node [
    id 303
    label "stratosfera"
  ]
  node [
    id 304
    label "pow&#322;oka"
  ]
  node [
    id 305
    label "mezosfera"
  ]
  node [
    id 306
    label "mezopauza"
  ]
  node [
    id 307
    label "atmosphere"
  ]
  node [
    id 308
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 309
    label "ciep&#322;o"
  ]
  node [
    id 310
    label "energia_termiczna"
  ]
  node [
    id 311
    label "bry&#322;a_obrotowa"
  ]
  node [
    id 312
    label "sferoida"
  ]
  node [
    id 313
    label "object"
  ]
  node [
    id 314
    label "temat"
  ]
  node [
    id 315
    label "wpadni&#281;cie"
  ]
  node [
    id 316
    label "mienie"
  ]
  node [
    id 317
    label "istota"
  ]
  node [
    id 318
    label "obiekt"
  ]
  node [
    id 319
    label "wpa&#347;&#263;"
  ]
  node [
    id 320
    label "wpadanie"
  ]
  node [
    id 321
    label "wpada&#263;"
  ]
  node [
    id 322
    label "wra&#380;enie"
  ]
  node [
    id 323
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 324
    label "interception"
  ]
  node [
    id 325
    label "wzbudzenie"
  ]
  node [
    id 326
    label "emotion"
  ]
  node [
    id 327
    label "movement"
  ]
  node [
    id 328
    label "zaczerpni&#281;cie"
  ]
  node [
    id 329
    label "wzi&#281;cie"
  ]
  node [
    id 330
    label "bang"
  ]
  node [
    id 331
    label "wzi&#261;&#263;"
  ]
  node [
    id 332
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 333
    label "stimulate"
  ]
  node [
    id 334
    label "ogarn&#261;&#263;"
  ]
  node [
    id 335
    label "wzbudzi&#263;"
  ]
  node [
    id 336
    label "thrill"
  ]
  node [
    id 337
    label "treat"
  ]
  node [
    id 338
    label "czerpa&#263;"
  ]
  node [
    id 339
    label "bra&#263;"
  ]
  node [
    id 340
    label "go"
  ]
  node [
    id 341
    label "handle"
  ]
  node [
    id 342
    label "wzbudza&#263;"
  ]
  node [
    id 343
    label "ogarnia&#263;"
  ]
  node [
    id 344
    label "czerpanie"
  ]
  node [
    id 345
    label "acquisition"
  ]
  node [
    id 346
    label "branie"
  ]
  node [
    id 347
    label "caparison"
  ]
  node [
    id 348
    label "wzbudzanie"
  ]
  node [
    id 349
    label "czynno&#347;&#263;"
  ]
  node [
    id 350
    label "ogarnianie"
  ]
  node [
    id 351
    label "zboczenie"
  ]
  node [
    id 352
    label "om&#243;wienie"
  ]
  node [
    id 353
    label "sponiewieranie"
  ]
  node [
    id 354
    label "discipline"
  ]
  node [
    id 355
    label "omawia&#263;"
  ]
  node [
    id 356
    label "kr&#261;&#380;enie"
  ]
  node [
    id 357
    label "tre&#347;&#263;"
  ]
  node [
    id 358
    label "robienie"
  ]
  node [
    id 359
    label "sponiewiera&#263;"
  ]
  node [
    id 360
    label "element"
  ]
  node [
    id 361
    label "entity"
  ]
  node [
    id 362
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 363
    label "tematyka"
  ]
  node [
    id 364
    label "w&#261;tek"
  ]
  node [
    id 365
    label "zbaczanie"
  ]
  node [
    id 366
    label "program_nauczania"
  ]
  node [
    id 367
    label "om&#243;wi&#263;"
  ]
  node [
    id 368
    label "omawianie"
  ]
  node [
    id 369
    label "thing"
  ]
  node [
    id 370
    label "zbacza&#263;"
  ]
  node [
    id 371
    label "zboczy&#263;"
  ]
  node [
    id 372
    label "performance"
  ]
  node [
    id 373
    label "sztuka"
  ]
  node [
    id 374
    label "granica_pa&#324;stwa"
  ]
  node [
    id 375
    label "Boreasz"
  ]
  node [
    id 376
    label "noc"
  ]
  node [
    id 377
    label "p&#243;&#322;nocek"
  ]
  node [
    id 378
    label "strona_&#347;wiata"
  ]
  node [
    id 379
    label "godzina"
  ]
  node [
    id 380
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 381
    label "&#347;rodek"
  ]
  node [
    id 382
    label "dzie&#324;"
  ]
  node [
    id 383
    label "dwunasta"
  ]
  node [
    id 384
    label "pora"
  ]
  node [
    id 385
    label "brzeg"
  ]
  node [
    id 386
    label "&#378;r&#243;d&#322;o_pola"
  ]
  node [
    id 387
    label "p&#322;oza"
  ]
  node [
    id 388
    label "zawiasy"
  ]
  node [
    id 389
    label "biegun_p&#243;&#322;nocny"
  ]
  node [
    id 390
    label "organ"
  ]
  node [
    id 391
    label "element_anatomiczny"
  ]
  node [
    id 392
    label "biegun_po&#322;udniowy"
  ]
  node [
    id 393
    label "reda"
  ]
  node [
    id 394
    label "zbiornik_wodny"
  ]
  node [
    id 395
    label "przymorze"
  ]
  node [
    id 396
    label "Morze_Ba&#322;tyckie"
  ]
  node [
    id 397
    label "bezmiar"
  ]
  node [
    id 398
    label "pe&#322;ne_morze"
  ]
  node [
    id 399
    label "latarnia_morska"
  ]
  node [
    id 400
    label "Morze_&#346;r&#243;dziemne"
  ]
  node [
    id 401
    label "nereida"
  ]
  node [
    id 402
    label "okeanida"
  ]
  node [
    id 403
    label "marina"
  ]
  node [
    id 404
    label "sk&#322;on_kontynentalny"
  ]
  node [
    id 405
    label "Morze_Czerwone"
  ]
  node [
    id 406
    label "talasoterapia"
  ]
  node [
    id 407
    label "Morze_Bia&#322;e"
  ]
  node [
    id 408
    label "paliszcze"
  ]
  node [
    id 409
    label "Neptun"
  ]
  node [
    id 410
    label "Morze_Czarne"
  ]
  node [
    id 411
    label "laguna"
  ]
  node [
    id 412
    label "Morze_Egejskie"
  ]
  node [
    id 413
    label "Morze_Tyrre&#324;skie"
  ]
  node [
    id 414
    label "Morze_Adriatyckie"
  ]
  node [
    id 415
    label "rze&#378;biarstwo"
  ]
  node [
    id 416
    label "planacja"
  ]
  node [
    id 417
    label "relief"
  ]
  node [
    id 418
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 419
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 420
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 421
    label "bozzetto"
  ]
  node [
    id 422
    label "plastyka"
  ]
  node [
    id 423
    label "sfera"
  ]
  node [
    id 424
    label "gleba"
  ]
  node [
    id 425
    label "p&#322;yta_tektoniczna"
  ]
  node [
    id 426
    label "warstwa"
  ]
  node [
    id 427
    label "sialma"
  ]
  node [
    id 428
    label "skorupa_ziemska"
  ]
  node [
    id 429
    label "warstwa_perydotytowa"
  ]
  node [
    id 430
    label "warstwa_granitowa"
  ]
  node [
    id 431
    label "kriosfera"
  ]
  node [
    id 432
    label "j&#261;dro"
  ]
  node [
    id 433
    label "lej_polarny"
  ]
  node [
    id 434
    label "kula"
  ]
  node [
    id 435
    label "kresom&#243;zgowie"
  ]
  node [
    id 436
    label "ozon"
  ]
  node [
    id 437
    label "przyra"
  ]
  node [
    id 438
    label "kontekst"
  ]
  node [
    id 439
    label "miejsce_pracy"
  ]
  node [
    id 440
    label "nation"
  ]
  node [
    id 441
    label "b&#322;otnisto&#347;&#263;"
  ]
  node [
    id 442
    label "wy&#380;ynno&#347;&#263;"
  ]
  node [
    id 443
    label "w&#322;adza"
  ]
  node [
    id 444
    label "iglak"
  ]
  node [
    id 445
    label "cyprysowate"
  ]
  node [
    id 446
    label "biom"
  ]
  node [
    id 447
    label "szata_ro&#347;linna"
  ]
  node [
    id 448
    label "pa&#324;stwo_ro&#347;linne"
  ]
  node [
    id 449
    label "formacja_ro&#347;linna"
  ]
  node [
    id 450
    label "zielono&#347;&#263;"
  ]
  node [
    id 451
    label "pi&#281;tro"
  ]
  node [
    id 452
    label "plant"
  ]
  node [
    id 453
    label "ro&#347;lina"
  ]
  node [
    id 454
    label "geosystem"
  ]
  node [
    id 455
    label "dotleni&#263;"
  ]
  node [
    id 456
    label "spi&#281;trza&#263;"
  ]
  node [
    id 457
    label "spi&#281;trzenie"
  ]
  node [
    id 458
    label "utylizator"
  ]
  node [
    id 459
    label "p&#322;ycizna"
  ]
  node [
    id 460
    label "nabranie"
  ]
  node [
    id 461
    label "Waruna"
  ]
  node [
    id 462
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 463
    label "przybieranie"
  ]
  node [
    id 464
    label "uci&#261;g"
  ]
  node [
    id 465
    label "bombast"
  ]
  node [
    id 466
    label "fala"
  ]
  node [
    id 467
    label "kryptodepresja"
  ]
  node [
    id 468
    label "water"
  ]
  node [
    id 469
    label "wysi&#281;k"
  ]
  node [
    id 470
    label "pustka"
  ]
  node [
    id 471
    label "ciecz"
  ]
  node [
    id 472
    label "przybrze&#380;e"
  ]
  node [
    id 473
    label "spi&#281;trzanie"
  ]
  node [
    id 474
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 475
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 476
    label "bicie"
  ]
  node [
    id 477
    label "klarownik"
  ]
  node [
    id 478
    label "chlastanie"
  ]
  node [
    id 479
    label "woda_s&#322;odka"
  ]
  node [
    id 480
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 481
    label "nabra&#263;"
  ]
  node [
    id 482
    label "chlasta&#263;"
  ]
  node [
    id 483
    label "uj&#281;cie_wody"
  ]
  node [
    id 484
    label "zrzut"
  ]
  node [
    id 485
    label "wypowied&#378;"
  ]
  node [
    id 486
    label "wodnik"
  ]
  node [
    id 487
    label "pojazd"
  ]
  node [
    id 488
    label "l&#243;d"
  ]
  node [
    id 489
    label "wybrze&#380;e"
  ]
  node [
    id 490
    label "deklamacja"
  ]
  node [
    id 491
    label "tlenek"
  ]
  node [
    id 492
    label "uk&#322;ad_ekologiczny"
  ]
  node [
    id 493
    label "biotop"
  ]
  node [
    id 494
    label "biocenoza"
  ]
  node [
    id 495
    label "awifauna"
  ]
  node [
    id 496
    label "ichtiofauna"
  ]
  node [
    id 497
    label "zaj&#281;cie"
  ]
  node [
    id 498
    label "instytucja"
  ]
  node [
    id 499
    label "tajniki"
  ]
  node [
    id 500
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 501
    label "jedzenie"
  ]
  node [
    id 502
    label "zaplecze"
  ]
  node [
    id 503
    label "pomieszczenie"
  ]
  node [
    id 504
    label "zlewozmywak"
  ]
  node [
    id 505
    label "gotowa&#263;"
  ]
  node [
    id 506
    label "Jowisz"
  ]
  node [
    id 507
    label "syzygia"
  ]
  node [
    id 508
    label "Saturn"
  ]
  node [
    id 509
    label "Uran"
  ]
  node [
    id 510
    label "strefa"
  ]
  node [
    id 511
    label "message"
  ]
  node [
    id 512
    label "dar"
  ]
  node [
    id 513
    label "real"
  ]
  node [
    id 514
    label "Ukraina"
  ]
  node [
    id 515
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 516
    label "blok_wschodni"
  ]
  node [
    id 517
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 518
    label "Europa_Wschodnia"
  ]
  node [
    id 519
    label "Czecho-S&#322;owacja"
  ]
  node [
    id 520
    label "Daleki_Wsch&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
]
