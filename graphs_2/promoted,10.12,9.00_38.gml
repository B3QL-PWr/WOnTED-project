graph [
  node [
    id 0
    label "znale&#378;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 2
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "poczucie"
    origin "text"
  ]
  node [
    id 4
    label "humor"
    origin "text"
  ]
  node [
    id 5
    label "wykop"
    origin "text"
  ]
  node [
    id 6
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 7
    label "pozyska&#263;"
  ]
  node [
    id 8
    label "oceni&#263;"
  ]
  node [
    id 9
    label "devise"
  ]
  node [
    id 10
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 11
    label "dozna&#263;"
  ]
  node [
    id 12
    label "wykry&#263;"
  ]
  node [
    id 13
    label "odzyska&#263;"
  ]
  node [
    id 14
    label "znaj&#347;&#263;"
  ]
  node [
    id 15
    label "invent"
  ]
  node [
    id 16
    label "wymy&#347;li&#263;"
  ]
  node [
    id 17
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 18
    label "stage"
  ]
  node [
    id 19
    label "uzyska&#263;"
  ]
  node [
    id 20
    label "wytworzy&#263;"
  ]
  node [
    id 21
    label "give_birth"
  ]
  node [
    id 22
    label "feel"
  ]
  node [
    id 23
    label "discover"
  ]
  node [
    id 24
    label "okre&#347;li&#263;"
  ]
  node [
    id 25
    label "dostrzec"
  ]
  node [
    id 26
    label "odkry&#263;"
  ]
  node [
    id 27
    label "concoct"
  ]
  node [
    id 28
    label "sta&#263;_si&#281;"
  ]
  node [
    id 29
    label "zrobi&#263;"
  ]
  node [
    id 30
    label "recapture"
  ]
  node [
    id 31
    label "odnowi&#263;_si&#281;"
  ]
  node [
    id 32
    label "visualize"
  ]
  node [
    id 33
    label "wystawi&#263;"
  ]
  node [
    id 34
    label "evaluate"
  ]
  node [
    id 35
    label "pomy&#347;le&#263;"
  ]
  node [
    id 36
    label "hide"
  ]
  node [
    id 37
    label "czu&#263;"
  ]
  node [
    id 38
    label "support"
  ]
  node [
    id 39
    label "need"
  ]
  node [
    id 40
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "wykonawca"
  ]
  node [
    id 42
    label "interpretator"
  ]
  node [
    id 43
    label "cover"
  ]
  node [
    id 44
    label "postrzega&#263;"
  ]
  node [
    id 45
    label "przewidywa&#263;"
  ]
  node [
    id 46
    label "by&#263;"
  ]
  node [
    id 47
    label "smell"
  ]
  node [
    id 48
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 49
    label "uczuwa&#263;"
  ]
  node [
    id 50
    label "spirit"
  ]
  node [
    id 51
    label "doznawa&#263;"
  ]
  node [
    id 52
    label "anticipate"
  ]
  node [
    id 53
    label "ekstraspekcja"
  ]
  node [
    id 54
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 55
    label "feeling"
  ]
  node [
    id 56
    label "doznanie"
  ]
  node [
    id 57
    label "wiedza"
  ]
  node [
    id 58
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 59
    label "zdarzenie_si&#281;"
  ]
  node [
    id 60
    label "opanowanie"
  ]
  node [
    id 61
    label "os&#322;upienie"
  ]
  node [
    id 62
    label "zareagowanie"
  ]
  node [
    id 63
    label "intuition"
  ]
  node [
    id 64
    label "cognition"
  ]
  node [
    id 65
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 66
    label "intelekt"
  ]
  node [
    id 67
    label "pozwolenie"
  ]
  node [
    id 68
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 69
    label "zaawansowanie"
  ]
  node [
    id 70
    label "wykszta&#322;cenie"
  ]
  node [
    id 71
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 72
    label "obserwacja"
  ]
  node [
    id 73
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 74
    label "wy&#347;wiadczenie"
  ]
  node [
    id 75
    label "zmys&#322;"
  ]
  node [
    id 76
    label "spotkanie"
  ]
  node [
    id 77
    label "czucie"
  ]
  node [
    id 78
    label "przeczulica"
  ]
  node [
    id 79
    label "wyniesienie"
  ]
  node [
    id 80
    label "podporz&#261;dkowanie"
  ]
  node [
    id 81
    label "pow&#347;ci&#261;gliwo&#347;&#263;"
  ]
  node [
    id 82
    label "dostanie"
  ]
  node [
    id 83
    label "zr&#243;wnowa&#380;enie"
  ]
  node [
    id 84
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 85
    label "spowodowanie"
  ]
  node [
    id 86
    label "nauczenie_si&#281;"
  ]
  node [
    id 87
    label "control"
  ]
  node [
    id 88
    label "nasilenie_si&#281;"
  ]
  node [
    id 89
    label "powstrzymanie"
  ]
  node [
    id 90
    label "rozprzestrzenienie_si&#281;"
  ]
  node [
    id 91
    label "convention"
  ]
  node [
    id 92
    label "po&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 93
    label "zrobienie"
  ]
  node [
    id 94
    label "bezruch"
  ]
  node [
    id 95
    label "oznaka"
  ]
  node [
    id 96
    label "znieruchomienie"
  ]
  node [
    id 97
    label "zdziwienie"
  ]
  node [
    id 98
    label "discouragement"
  ]
  node [
    id 99
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 100
    label "temper"
  ]
  node [
    id 101
    label "&#347;mieszno&#347;&#263;"
  ]
  node [
    id 102
    label "stan"
  ]
  node [
    id 103
    label "samopoczucie"
  ]
  node [
    id 104
    label "mechanizm_obronny"
  ]
  node [
    id 105
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 106
    label "fondness"
  ]
  node [
    id 107
    label "nastr&#243;j"
  ]
  node [
    id 108
    label "state"
  ]
  node [
    id 109
    label "wstyd"
  ]
  node [
    id 110
    label "upokorzenie"
  ]
  node [
    id 111
    label "cecha"
  ]
  node [
    id 112
    label "klimat"
  ]
  node [
    id 113
    label "charakter"
  ]
  node [
    id 114
    label "kwas"
  ]
  node [
    id 115
    label "Ohio"
  ]
  node [
    id 116
    label "wci&#281;cie"
  ]
  node [
    id 117
    label "Nowy_York"
  ]
  node [
    id 118
    label "warstwa"
  ]
  node [
    id 119
    label "Illinois"
  ]
  node [
    id 120
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 121
    label "Jukatan"
  ]
  node [
    id 122
    label "Kalifornia"
  ]
  node [
    id 123
    label "Wirginia"
  ]
  node [
    id 124
    label "wektor"
  ]
  node [
    id 125
    label "Teksas"
  ]
  node [
    id 126
    label "Goa"
  ]
  node [
    id 127
    label "Waszyngton"
  ]
  node [
    id 128
    label "miejsce"
  ]
  node [
    id 129
    label "Massachusetts"
  ]
  node [
    id 130
    label "Alaska"
  ]
  node [
    id 131
    label "Arakan"
  ]
  node [
    id 132
    label "Hawaje"
  ]
  node [
    id 133
    label "Maryland"
  ]
  node [
    id 134
    label "punkt"
  ]
  node [
    id 135
    label "Michigan"
  ]
  node [
    id 136
    label "Arizona"
  ]
  node [
    id 137
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 138
    label "Georgia"
  ]
  node [
    id 139
    label "poziom"
  ]
  node [
    id 140
    label "Pensylwania"
  ]
  node [
    id 141
    label "shape"
  ]
  node [
    id 142
    label "Luizjana"
  ]
  node [
    id 143
    label "Nowy_Meksyk"
  ]
  node [
    id 144
    label "Alabama"
  ]
  node [
    id 145
    label "ilo&#347;&#263;"
  ]
  node [
    id 146
    label "Kansas"
  ]
  node [
    id 147
    label "Oregon"
  ]
  node [
    id 148
    label "Floryda"
  ]
  node [
    id 149
    label "Oklahoma"
  ]
  node [
    id 150
    label "jednostka_administracyjna"
  ]
  node [
    id 151
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 152
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 153
    label "dyspozycja"
  ]
  node [
    id 154
    label "forma"
  ]
  node [
    id 155
    label "budowa"
  ]
  node [
    id 156
    label "zrzutowy"
  ]
  node [
    id 157
    label "odk&#322;ad"
  ]
  node [
    id 158
    label "chody"
  ]
  node [
    id 159
    label "szaniec"
  ]
  node [
    id 160
    label "wyrobisko"
  ]
  node [
    id 161
    label "kopniak"
  ]
  node [
    id 162
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 163
    label "odwa&#322;"
  ]
  node [
    id 164
    label "grodzisko"
  ]
  node [
    id 165
    label "cios"
  ]
  node [
    id 166
    label "kick"
  ]
  node [
    id 167
    label "kopni&#281;cie"
  ]
  node [
    id 168
    label "&#347;rodkowiec"
  ]
  node [
    id 169
    label "podsadzka"
  ]
  node [
    id 170
    label "obudowa"
  ]
  node [
    id 171
    label "sp&#261;g"
  ]
  node [
    id 172
    label "strop"
  ]
  node [
    id 173
    label "rabowarka"
  ]
  node [
    id 174
    label "opinka"
  ]
  node [
    id 175
    label "stojak_cierny"
  ]
  node [
    id 176
    label "kopalnia"
  ]
  node [
    id 177
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 178
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 179
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 180
    label "immersion"
  ]
  node [
    id 181
    label "umieszczenie"
  ]
  node [
    id 182
    label "las"
  ]
  node [
    id 183
    label "nora"
  ]
  node [
    id 184
    label "pies_my&#347;liwski"
  ]
  node [
    id 185
    label "trasa"
  ]
  node [
    id 186
    label "doj&#347;cie"
  ]
  node [
    id 187
    label "zesp&#243;&#322;"
  ]
  node [
    id 188
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 189
    label "horodyszcze"
  ]
  node [
    id 190
    label "Wyszogr&#243;d"
  ]
  node [
    id 191
    label "usypisko"
  ]
  node [
    id 192
    label "r&#243;w"
  ]
  node [
    id 193
    label "wa&#322;"
  ]
  node [
    id 194
    label "redoubt"
  ]
  node [
    id 195
    label "fortyfikacja"
  ]
  node [
    id 196
    label "mechanika"
  ]
  node [
    id 197
    label "struktura"
  ]
  node [
    id 198
    label "figura"
  ]
  node [
    id 199
    label "miejsce_pracy"
  ]
  node [
    id 200
    label "organ"
  ]
  node [
    id 201
    label "kreacja"
  ]
  node [
    id 202
    label "zwierz&#281;"
  ]
  node [
    id 203
    label "posesja"
  ]
  node [
    id 204
    label "konstrukcja"
  ]
  node [
    id 205
    label "wjazd"
  ]
  node [
    id 206
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 207
    label "praca"
  ]
  node [
    id 208
    label "constitution"
  ]
  node [
    id 209
    label "gleba"
  ]
  node [
    id 210
    label "p&#281;d"
  ]
  node [
    id 211
    label "zbi&#243;r"
  ]
  node [
    id 212
    label "ablegier"
  ]
  node [
    id 213
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 214
    label "layer"
  ]
  node [
    id 215
    label "r&#243;j"
  ]
  node [
    id 216
    label "mrowisko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
]
