graph [
  node [
    id 0
    label "dalmierz"
    origin "text"
  ]
  node [
    id 1
    label "tachymetria"
  ]
  node [
    id 2
    label "miernik"
  ]
  node [
    id 3
    label "aparat_dalmierzowy"
  ]
  node [
    id 4
    label "wska&#378;nik"
  ]
  node [
    id 5
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 6
    label "meter"
  ]
  node [
    id 7
    label "kryterium"
  ]
  node [
    id 8
    label "metoda"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
]
