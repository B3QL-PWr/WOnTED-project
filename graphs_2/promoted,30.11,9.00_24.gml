graph [
  node [
    id 0
    label "rzecznik"
    origin "text"
  ]
  node [
    id 1
    label "prawy"
    origin "text"
  ]
  node [
    id 2
    label "obywatelski"
    origin "text"
  ]
  node [
    id 3
    label "zorganizowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "spotkanie"
    origin "text"
  ]
  node [
    id 5
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 6
    label "problematyka"
    origin "text"
  ]
  node [
    id 7
    label "patologia"
    origin "text"
  ]
  node [
    id 8
    label "internet"
    origin "text"
  ]
  node [
    id 9
    label "przyjaciel"
  ]
  node [
    id 10
    label "przedstawiciel"
  ]
  node [
    id 11
    label "doradca"
  ]
  node [
    id 12
    label "czynnik"
  ]
  node [
    id 13
    label "radziciel"
  ]
  node [
    id 14
    label "pomocnik"
  ]
  node [
    id 15
    label "cz&#322;owiek"
  ]
  node [
    id 16
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 17
    label "cz&#322;onek"
  ]
  node [
    id 18
    label "przyk&#322;ad"
  ]
  node [
    id 19
    label "substytuowa&#263;"
  ]
  node [
    id 20
    label "substytuowanie"
  ]
  node [
    id 21
    label "zast&#281;pca"
  ]
  node [
    id 22
    label "kochanek"
  ]
  node [
    id 23
    label "kum"
  ]
  node [
    id 24
    label "amikus"
  ]
  node [
    id 25
    label "zaprzyja&#378;nianie_si&#281;"
  ]
  node [
    id 26
    label "pobratymiec"
  ]
  node [
    id 27
    label "zaprzyja&#378;nienie_si&#281;"
  ]
  node [
    id 28
    label "drogi"
  ]
  node [
    id 29
    label "sympatyk"
  ]
  node [
    id 30
    label "bratnia_dusza"
  ]
  node [
    id 31
    label "przyja&#378;nienie_si&#281;"
  ]
  node [
    id 32
    label "w_prawo"
  ]
  node [
    id 33
    label "s&#322;uszny"
  ]
  node [
    id 34
    label "naturalny"
  ]
  node [
    id 35
    label "chwalebny"
  ]
  node [
    id 36
    label "w_spos&#243;b_prawy"
  ]
  node [
    id 37
    label "zgodnie_z_prawem"
  ]
  node [
    id 38
    label "zacny"
  ]
  node [
    id 39
    label "moralny"
  ]
  node [
    id 40
    label "prawicowy"
  ]
  node [
    id 41
    label "na_prawo"
  ]
  node [
    id 42
    label "cnotliwy"
  ]
  node [
    id 43
    label "legalny"
  ]
  node [
    id 44
    label "z_prawa"
  ]
  node [
    id 45
    label "gajny"
  ]
  node [
    id 46
    label "legalnie"
  ]
  node [
    id 47
    label "pochwalny"
  ]
  node [
    id 48
    label "wspania&#322;y"
  ]
  node [
    id 49
    label "szlachetny"
  ]
  node [
    id 50
    label "powa&#380;ny"
  ]
  node [
    id 51
    label "chwalebnie"
  ]
  node [
    id 52
    label "moralnie"
  ]
  node [
    id 53
    label "warto&#347;ciowy"
  ]
  node [
    id 54
    label "etycznie"
  ]
  node [
    id 55
    label "dobry"
  ]
  node [
    id 56
    label "s&#322;usznie"
  ]
  node [
    id 57
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 58
    label "zasadny"
  ]
  node [
    id 59
    label "nale&#380;yty"
  ]
  node [
    id 60
    label "prawdziwy"
  ]
  node [
    id 61
    label "solidny"
  ]
  node [
    id 62
    label "skromny"
  ]
  node [
    id 63
    label "niewinny"
  ]
  node [
    id 64
    label "cny"
  ]
  node [
    id 65
    label "cnotliwie"
  ]
  node [
    id 66
    label "prostolinijny"
  ]
  node [
    id 67
    label "zacnie"
  ]
  node [
    id 68
    label "szczery"
  ]
  node [
    id 69
    label "zrozumia&#322;y"
  ]
  node [
    id 70
    label "immanentny"
  ]
  node [
    id 71
    label "zwyczajny"
  ]
  node [
    id 72
    label "bezsporny"
  ]
  node [
    id 73
    label "organicznie"
  ]
  node [
    id 74
    label "pierwotny"
  ]
  node [
    id 75
    label "neutralny"
  ]
  node [
    id 76
    label "normalny"
  ]
  node [
    id 77
    label "rzeczywisty"
  ]
  node [
    id 78
    label "naturalnie"
  ]
  node [
    id 79
    label "prawicowo"
  ]
  node [
    id 80
    label "prawoskr&#281;tny"
  ]
  node [
    id 81
    label "konserwatywny"
  ]
  node [
    id 82
    label "oddolny"
  ]
  node [
    id 83
    label "odpowiedzialny"
  ]
  node [
    id 84
    label "obywatelsko"
  ]
  node [
    id 85
    label "odpowiedzialnie"
  ]
  node [
    id 86
    label "sprawca"
  ]
  node [
    id 87
    label "&#347;wiadomy"
  ]
  node [
    id 88
    label "przewinienie"
  ]
  node [
    id 89
    label "odpowiadanie"
  ]
  node [
    id 90
    label "dobrowolny"
  ]
  node [
    id 91
    label "spo&#322;eczny"
  ]
  node [
    id 92
    label "oddolnie"
  ]
  node [
    id 93
    label "spontaniczny"
  ]
  node [
    id 94
    label "dostosowa&#263;"
  ]
  node [
    id 95
    label "pozyska&#263;"
  ]
  node [
    id 96
    label "stworzy&#263;"
  ]
  node [
    id 97
    label "plan"
  ]
  node [
    id 98
    label "stage"
  ]
  node [
    id 99
    label "urobi&#263;"
  ]
  node [
    id 100
    label "ensnare"
  ]
  node [
    id 101
    label "wprowadzi&#263;"
  ]
  node [
    id 102
    label "zaplanowa&#263;"
  ]
  node [
    id 103
    label "przygotowa&#263;"
  ]
  node [
    id 104
    label "standard"
  ]
  node [
    id 105
    label "skupi&#263;"
  ]
  node [
    id 106
    label "create"
  ]
  node [
    id 107
    label "specjalista_od_public_relations"
  ]
  node [
    id 108
    label "zrobi&#263;"
  ]
  node [
    id 109
    label "wizerunek"
  ]
  node [
    id 110
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 111
    label "uzyska&#263;"
  ]
  node [
    id 112
    label "wytworzy&#263;"
  ]
  node [
    id 113
    label "give_birth"
  ]
  node [
    id 114
    label "compress"
  ]
  node [
    id 115
    label "ognisko"
  ]
  node [
    id 116
    label "po&#347;wi&#281;ci&#263;"
  ]
  node [
    id 117
    label "concentrate"
  ]
  node [
    id 118
    label "zebra&#263;"
  ]
  node [
    id 119
    label "spowodowa&#263;"
  ]
  node [
    id 120
    label "kupi&#263;"
  ]
  node [
    id 121
    label "przemy&#347;le&#263;"
  ]
  node [
    id 122
    label "line_up"
  ]
  node [
    id 123
    label "opracowa&#263;"
  ]
  node [
    id 124
    label "map"
  ]
  node [
    id 125
    label "pomy&#347;le&#263;"
  ]
  node [
    id 126
    label "adjust"
  ]
  node [
    id 127
    label "zmieni&#263;"
  ]
  node [
    id 128
    label "rynek"
  ]
  node [
    id 129
    label "doprowadzi&#263;"
  ]
  node [
    id 130
    label "testify"
  ]
  node [
    id 131
    label "insert"
  ]
  node [
    id 132
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 133
    label "wpisa&#263;"
  ]
  node [
    id 134
    label "picture"
  ]
  node [
    id 135
    label "zapozna&#263;"
  ]
  node [
    id 136
    label "wej&#347;&#263;"
  ]
  node [
    id 137
    label "zej&#347;&#263;"
  ]
  node [
    id 138
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 139
    label "umie&#347;ci&#263;"
  ]
  node [
    id 140
    label "zacz&#261;&#263;"
  ]
  node [
    id 141
    label "indicate"
  ]
  node [
    id 142
    label "set"
  ]
  node [
    id 143
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 144
    label "wykona&#263;"
  ]
  node [
    id 145
    label "cook"
  ]
  node [
    id 146
    label "wyszkoli&#263;"
  ]
  node [
    id 147
    label "train"
  ]
  node [
    id 148
    label "arrange"
  ]
  node [
    id 149
    label "dress"
  ]
  node [
    id 150
    label "ukierunkowa&#263;"
  ]
  node [
    id 151
    label "model"
  ]
  node [
    id 152
    label "intencja"
  ]
  node [
    id 153
    label "punkt"
  ]
  node [
    id 154
    label "rysunek"
  ]
  node [
    id 155
    label "miejsce_pracy"
  ]
  node [
    id 156
    label "przestrze&#324;"
  ]
  node [
    id 157
    label "wytw&#243;r"
  ]
  node [
    id 158
    label "device"
  ]
  node [
    id 159
    label "pomys&#322;"
  ]
  node [
    id 160
    label "obraz"
  ]
  node [
    id 161
    label "reprezentacja"
  ]
  node [
    id 162
    label "agreement"
  ]
  node [
    id 163
    label "dekoracja"
  ]
  node [
    id 164
    label "perspektywa"
  ]
  node [
    id 165
    label "organizowa&#263;"
  ]
  node [
    id 166
    label "ordinariness"
  ]
  node [
    id 167
    label "instytucja"
  ]
  node [
    id 168
    label "taniec_towarzyski"
  ]
  node [
    id 169
    label "organizowanie"
  ]
  node [
    id 170
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 171
    label "criterion"
  ]
  node [
    id 172
    label "zorganizowanie"
  ]
  node [
    id 173
    label "uczyni&#263;"
  ]
  node [
    id 174
    label "cast"
  ]
  node [
    id 175
    label "od&#322;upa&#263;"
  ]
  node [
    id 176
    label "um&#281;czy&#263;"
  ]
  node [
    id 177
    label "get"
  ]
  node [
    id 178
    label "wypracowa&#263;"
  ]
  node [
    id 179
    label "przerobi&#263;"
  ]
  node [
    id 180
    label "ugnie&#347;&#263;"
  ]
  node [
    id 181
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 182
    label "doznanie"
  ]
  node [
    id 183
    label "gathering"
  ]
  node [
    id 184
    label "zawarcie"
  ]
  node [
    id 185
    label "wydarzenie"
  ]
  node [
    id 186
    label "znajomy"
  ]
  node [
    id 187
    label "powitanie"
  ]
  node [
    id 188
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 189
    label "spowodowanie"
  ]
  node [
    id 190
    label "zdarzenie_si&#281;"
  ]
  node [
    id 191
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 192
    label "znalezienie"
  ]
  node [
    id 193
    label "match"
  ]
  node [
    id 194
    label "employment"
  ]
  node [
    id 195
    label "po&#380;egnanie"
  ]
  node [
    id 196
    label "gather"
  ]
  node [
    id 197
    label "spotykanie"
  ]
  node [
    id 198
    label "spotkanie_si&#281;"
  ]
  node [
    id 199
    label "dzianie_si&#281;"
  ]
  node [
    id 200
    label "zaznawanie"
  ]
  node [
    id 201
    label "znajdowanie"
  ]
  node [
    id 202
    label "zdarzanie_si&#281;"
  ]
  node [
    id 203
    label "merging"
  ]
  node [
    id 204
    label "meeting"
  ]
  node [
    id 205
    label "zawieranie"
  ]
  node [
    id 206
    label "czynno&#347;&#263;"
  ]
  node [
    id 207
    label "campaign"
  ]
  node [
    id 208
    label "causing"
  ]
  node [
    id 209
    label "przebiec"
  ]
  node [
    id 210
    label "charakter"
  ]
  node [
    id 211
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 212
    label "motyw"
  ]
  node [
    id 213
    label "przebiegni&#281;cie"
  ]
  node [
    id 214
    label "fabu&#322;a"
  ]
  node [
    id 215
    label "postaranie_si&#281;"
  ]
  node [
    id 216
    label "discovery"
  ]
  node [
    id 217
    label "wymy&#347;lenie"
  ]
  node [
    id 218
    label "determination"
  ]
  node [
    id 219
    label "dorwanie"
  ]
  node [
    id 220
    label "znalezienie_si&#281;"
  ]
  node [
    id 221
    label "wykrycie"
  ]
  node [
    id 222
    label "poszukanie"
  ]
  node [
    id 223
    label "invention"
  ]
  node [
    id 224
    label "pozyskanie"
  ]
  node [
    id 225
    label "zmieszczenie"
  ]
  node [
    id 226
    label "umawianie_si&#281;"
  ]
  node [
    id 227
    label "zapoznanie"
  ]
  node [
    id 228
    label "um&#243;wienie_si&#281;"
  ]
  node [
    id 229
    label "zapoznanie_si&#281;"
  ]
  node [
    id 230
    label "ustalenie"
  ]
  node [
    id 231
    label "dissolution"
  ]
  node [
    id 232
    label "przyskrzynienie"
  ]
  node [
    id 233
    label "uk&#322;ad"
  ]
  node [
    id 234
    label "pozamykanie"
  ]
  node [
    id 235
    label "inclusion"
  ]
  node [
    id 236
    label "zamkni&#281;cie_si&#281;"
  ]
  node [
    id 237
    label "uchwalenie"
  ]
  node [
    id 238
    label "umowa"
  ]
  node [
    id 239
    label "zrobienie"
  ]
  node [
    id 240
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 241
    label "wy&#347;wiadczenie"
  ]
  node [
    id 242
    label "zmys&#322;"
  ]
  node [
    id 243
    label "czucie"
  ]
  node [
    id 244
    label "przeczulica"
  ]
  node [
    id 245
    label "poczucie"
  ]
  node [
    id 246
    label "znany"
  ]
  node [
    id 247
    label "sw&#243;j"
  ]
  node [
    id 248
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 249
    label "znajomek"
  ]
  node [
    id 250
    label "zapoznawanie"
  ]
  node [
    id 251
    label "przyj&#281;ty"
  ]
  node [
    id 252
    label "pewien"
  ]
  node [
    id 253
    label "zawieranie_znajomo&#347;ci"
  ]
  node [
    id 254
    label "znajomo"
  ]
  node [
    id 255
    label "za_pan_brat"
  ]
  node [
    id 256
    label "rozstanie_si&#281;"
  ]
  node [
    id 257
    label "adieu"
  ]
  node [
    id 258
    label "pozdrowienie"
  ]
  node [
    id 259
    label "zwyczaj"
  ]
  node [
    id 260
    label "farewell"
  ]
  node [
    id 261
    label "welcome"
  ]
  node [
    id 262
    label "greeting"
  ]
  node [
    id 263
    label "wyra&#380;enie"
  ]
  node [
    id 264
    label "oddany"
  ]
  node [
    id 265
    label "wierny"
  ]
  node [
    id 266
    label "ofiarny"
  ]
  node [
    id 267
    label "problem"
  ]
  node [
    id 268
    label "zbi&#243;r"
  ]
  node [
    id 269
    label "egzemplarz"
  ]
  node [
    id 270
    label "series"
  ]
  node [
    id 271
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 272
    label "uprawianie"
  ]
  node [
    id 273
    label "praca_rolnicza"
  ]
  node [
    id 274
    label "collection"
  ]
  node [
    id 275
    label "dane"
  ]
  node [
    id 276
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 277
    label "pakiet_klimatyczny"
  ]
  node [
    id 278
    label "poj&#281;cie"
  ]
  node [
    id 279
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 280
    label "sum"
  ]
  node [
    id 281
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 282
    label "album"
  ]
  node [
    id 283
    label "sprawa"
  ]
  node [
    id 284
    label "subiekcja"
  ]
  node [
    id 285
    label "problemat"
  ]
  node [
    id 286
    label "jajko_Kolumba"
  ]
  node [
    id 287
    label "obstruction"
  ]
  node [
    id 288
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 289
    label "trudno&#347;&#263;"
  ]
  node [
    id 290
    label "pierepa&#322;ka"
  ]
  node [
    id 291
    label "ambaras"
  ]
  node [
    id 292
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 293
    label "powalenie"
  ]
  node [
    id 294
    label "odezwanie_si&#281;"
  ]
  node [
    id 295
    label "atakowanie"
  ]
  node [
    id 296
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 297
    label "patomorfologia"
  ]
  node [
    id 298
    label "grupa_ryzyka"
  ]
  node [
    id 299
    label "przypadek"
  ]
  node [
    id 300
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 301
    label "patolnia"
  ]
  node [
    id 302
    label "nabawienie_si&#281;"
  ]
  node [
    id 303
    label "przemoc"
  ]
  node [
    id 304
    label "&#347;rodowisko"
  ]
  node [
    id 305
    label "inkubacja"
  ]
  node [
    id 306
    label "medycyna"
  ]
  node [
    id 307
    label "szambo"
  ]
  node [
    id 308
    label "gangsterski"
  ]
  node [
    id 309
    label "fizjologia_patologiczna"
  ]
  node [
    id 310
    label "kryzys"
  ]
  node [
    id 311
    label "powali&#263;"
  ]
  node [
    id 312
    label "remisja"
  ]
  node [
    id 313
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 314
    label "zajmowa&#263;"
  ]
  node [
    id 315
    label "zaburzenie"
  ]
  node [
    id 316
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 317
    label "neuropatologia"
  ]
  node [
    id 318
    label "aspo&#322;eczny"
  ]
  node [
    id 319
    label "badanie_histopatologiczne"
  ]
  node [
    id 320
    label "abnormality"
  ]
  node [
    id 321
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 322
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 323
    label "patogeneza"
  ]
  node [
    id 324
    label "psychopatologia"
  ]
  node [
    id 325
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 326
    label "paleopatologia"
  ]
  node [
    id 327
    label "logopatologia"
  ]
  node [
    id 328
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 329
    label "osteopatologia"
  ]
  node [
    id 330
    label "immunopatologia"
  ]
  node [
    id 331
    label "odzywanie_si&#281;"
  ]
  node [
    id 332
    label "diagnoza"
  ]
  node [
    id 333
    label "atakowa&#263;"
  ]
  node [
    id 334
    label "histopatologia"
  ]
  node [
    id 335
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 336
    label "nabawianie_si&#281;"
  ]
  node [
    id 337
    label "underworld"
  ]
  node [
    id 338
    label "meteoropatologia"
  ]
  node [
    id 339
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 340
    label "zajmowanie"
  ]
  node [
    id 341
    label "nies&#322;uszno&#347;&#263;"
  ]
  node [
    id 342
    label "sytuacja"
  ]
  node [
    id 343
    label "niedopasowanie"
  ]
  node [
    id 344
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 345
    label "discourtesy"
  ]
  node [
    id 346
    label "disorder"
  ]
  node [
    id 347
    label "transgresja"
  ]
  node [
    id 348
    label "zjawisko"
  ]
  node [
    id 349
    label "class"
  ]
  node [
    id 350
    label "zesp&#243;&#322;"
  ]
  node [
    id 351
    label "obiekt_naturalny"
  ]
  node [
    id 352
    label "otoczenie"
  ]
  node [
    id 353
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 354
    label "environment"
  ]
  node [
    id 355
    label "rzecz"
  ]
  node [
    id 356
    label "huczek"
  ]
  node [
    id 357
    label "ekosystem"
  ]
  node [
    id 358
    label "wszechstworzenie"
  ]
  node [
    id 359
    label "grupa"
  ]
  node [
    id 360
    label "woda"
  ]
  node [
    id 361
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 362
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 363
    label "teren"
  ]
  node [
    id 364
    label "mikrokosmos"
  ]
  node [
    id 365
    label "stw&#243;r"
  ]
  node [
    id 366
    label "warunki"
  ]
  node [
    id 367
    label "Ziemia"
  ]
  node [
    id 368
    label "fauna"
  ]
  node [
    id 369
    label "biota"
  ]
  node [
    id 370
    label "smr&#243;d"
  ]
  node [
    id 371
    label "gips"
  ]
  node [
    id 372
    label "koszmar"
  ]
  node [
    id 373
    label "pasztet"
  ]
  node [
    id 374
    label "kanalizacja"
  ]
  node [
    id 375
    label "mire"
  ]
  node [
    id 376
    label "budowla"
  ]
  node [
    id 377
    label "zbiornik"
  ]
  node [
    id 378
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 379
    label "kloaka"
  ]
  node [
    id 380
    label "agresja"
  ]
  node [
    id 381
    label "przewaga"
  ]
  node [
    id 382
    label "drastyczny"
  ]
  node [
    id 383
    label "skupisko"
  ]
  node [
    id 384
    label "&#347;wiat&#322;o"
  ]
  node [
    id 385
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 386
    label "impreza"
  ]
  node [
    id 387
    label "Hollywood"
  ]
  node [
    id 388
    label "miejsce"
  ]
  node [
    id 389
    label "schorzenie"
  ]
  node [
    id 390
    label "center"
  ]
  node [
    id 391
    label "palenisko"
  ]
  node [
    id 392
    label "skupia&#263;"
  ]
  node [
    id 393
    label "o&#347;rodek"
  ]
  node [
    id 394
    label "watra"
  ]
  node [
    id 395
    label "hotbed"
  ]
  node [
    id 396
    label "powodowanie"
  ]
  node [
    id 397
    label "lokowanie_si&#281;"
  ]
  node [
    id 398
    label "zajmowanie_si&#281;"
  ]
  node [
    id 399
    label "rozprzestrzenianie_si&#281;"
  ]
  node [
    id 400
    label "stosowanie"
  ]
  node [
    id 401
    label "anektowanie"
  ]
  node [
    id 402
    label "ciekawy"
  ]
  node [
    id 403
    label "zabieranie"
  ]
  node [
    id 404
    label "robienie"
  ]
  node [
    id 405
    label "sytuowanie_si&#281;"
  ]
  node [
    id 406
    label "wype&#322;nianie"
  ]
  node [
    id 407
    label "obejmowanie"
  ]
  node [
    id 408
    label "klasyfikacja"
  ]
  node [
    id 409
    label "bycie"
  ]
  node [
    id 410
    label "branie"
  ]
  node [
    id 411
    label "rz&#261;dzenie"
  ]
  node [
    id 412
    label "occupation"
  ]
  node [
    id 413
    label "zadawanie"
  ]
  node [
    id 414
    label "zaj&#281;ty"
  ]
  node [
    id 415
    label "grasowanie"
  ]
  node [
    id 416
    label "napadanie"
  ]
  node [
    id 417
    label "rozgrywanie"
  ]
  node [
    id 418
    label "k&#322;&#243;cenie_si&#281;"
  ]
  node [
    id 419
    label "polowanie"
  ]
  node [
    id 420
    label "walczenie"
  ]
  node [
    id 421
    label "usi&#322;owanie"
  ]
  node [
    id 422
    label "epidemia"
  ]
  node [
    id 423
    label "sport"
  ]
  node [
    id 424
    label "m&#243;wienie"
  ]
  node [
    id 425
    label "wyskakiwanie_z_g&#281;b&#261;"
  ]
  node [
    id 426
    label "pojawianie_si&#281;"
  ]
  node [
    id 427
    label "krytykowanie"
  ]
  node [
    id 428
    label "torpedowanie"
  ]
  node [
    id 429
    label "szczucie"
  ]
  node [
    id 430
    label "przebywanie"
  ]
  node [
    id 431
    label "oddzia&#322;ywanie"
  ]
  node [
    id 432
    label "friction"
  ]
  node [
    id 433
    label "nast&#281;powanie"
  ]
  node [
    id 434
    label "granie"
  ]
  node [
    id 435
    label "waln&#261;&#263;"
  ]
  node [
    id 436
    label "zamordowa&#263;"
  ]
  node [
    id 437
    label "drop"
  ]
  node [
    id 438
    label "os&#322;abi&#263;"
  ]
  node [
    id 439
    label "give"
  ]
  node [
    id 440
    label "spot"
  ]
  node [
    id 441
    label "os&#322;abienie"
  ]
  node [
    id 442
    label "walni&#281;cie"
  ]
  node [
    id 443
    label "collapse"
  ]
  node [
    id 444
    label "zamordowanie"
  ]
  node [
    id 445
    label "prostration"
  ]
  node [
    id 446
    label "strike"
  ]
  node [
    id 447
    label "robi&#263;"
  ]
  node [
    id 448
    label "dzia&#322;a&#263;"
  ]
  node [
    id 449
    label "ofensywny"
  ]
  node [
    id 450
    label "attack"
  ]
  node [
    id 451
    label "rozgrywa&#263;"
  ]
  node [
    id 452
    label "krytykowa&#263;"
  ]
  node [
    id 453
    label "walczy&#263;"
  ]
  node [
    id 454
    label "aim"
  ]
  node [
    id 455
    label "trouble_oneself"
  ]
  node [
    id 456
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 457
    label "napada&#263;"
  ]
  node [
    id 458
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 459
    label "m&#243;wi&#263;"
  ]
  node [
    id 460
    label "nast&#281;powa&#263;"
  ]
  node [
    id 461
    label "usi&#322;owa&#263;"
  ]
  node [
    id 462
    label "dostarcza&#263;"
  ]
  node [
    id 463
    label "korzysta&#263;"
  ]
  node [
    id 464
    label "komornik"
  ]
  node [
    id 465
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 466
    label "return"
  ]
  node [
    id 467
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 468
    label "trwa&#263;"
  ]
  node [
    id 469
    label "bra&#263;"
  ]
  node [
    id 470
    label "rozciekawia&#263;"
  ]
  node [
    id 471
    label "zadawa&#263;"
  ]
  node [
    id 472
    label "fill"
  ]
  node [
    id 473
    label "zabiera&#263;"
  ]
  node [
    id 474
    label "topographic_point"
  ]
  node [
    id 475
    label "obejmowa&#263;"
  ]
  node [
    id 476
    label "pali&#263;_si&#281;"
  ]
  node [
    id 477
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 478
    label "anektowa&#263;"
  ]
  node [
    id 479
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 480
    label "prosecute"
  ]
  node [
    id 481
    label "powodowa&#263;"
  ]
  node [
    id 482
    label "sake"
  ]
  node [
    id 483
    label "do"
  ]
  node [
    id 484
    label "diagnosis"
  ]
  node [
    id 485
    label "sprawdzian"
  ]
  node [
    id 486
    label "rozpoznanie"
  ]
  node [
    id 487
    label "dokument"
  ]
  node [
    id 488
    label "ocena"
  ]
  node [
    id 489
    label "pacjent"
  ]
  node [
    id 490
    label "happening"
  ]
  node [
    id 491
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 492
    label "kategoria_gramatyczna"
  ]
  node [
    id 493
    label "przeznaczenie"
  ]
  node [
    id 494
    label "po_gangstersku"
  ]
  node [
    id 495
    label "przest&#281;pczy"
  ]
  node [
    id 496
    label "wyl&#281;g"
  ]
  node [
    id 497
    label "proces"
  ]
  node [
    id 498
    label "July"
  ]
  node [
    id 499
    label "k&#322;opot"
  ]
  node [
    id 500
    label "cykl_koniunkturalny"
  ]
  node [
    id 501
    label "zwrot"
  ]
  node [
    id 502
    label "Marzec_'68"
  ]
  node [
    id 503
    label "pogorszenie"
  ]
  node [
    id 504
    label "head"
  ]
  node [
    id 505
    label "histologia"
  ]
  node [
    id 506
    label "geneza"
  ]
  node [
    id 507
    label "immunologia"
  ]
  node [
    id 508
    label "nauka"
  ]
  node [
    id 509
    label "archeologia"
  ]
  node [
    id 510
    label "zmiana_patologiczna"
  ]
  node [
    id 511
    label "zmiana_morfologiczna"
  ]
  node [
    id 512
    label "psychologia_kliniczna"
  ]
  node [
    id 513
    label "balneologia"
  ]
  node [
    id 514
    label "serologia"
  ]
  node [
    id 515
    label "proktologia"
  ]
  node [
    id 516
    label "symptomatologia"
  ]
  node [
    id 517
    label "neurologia"
  ]
  node [
    id 518
    label "radiologia"
  ]
  node [
    id 519
    label "chirurgia"
  ]
  node [
    id 520
    label "medycyna_ratunkowa"
  ]
  node [
    id 521
    label "okulistyka"
  ]
  node [
    id 522
    label "transfuzjologia"
  ]
  node [
    id 523
    label "hemodynamika"
  ]
  node [
    id 524
    label "psychosomatyka"
  ]
  node [
    id 525
    label "medycyna_weterynaryjna"
  ]
  node [
    id 526
    label "medycyna_zapobiegawcza"
  ]
  node [
    id 527
    label "medycyna_kosmiczna"
  ]
  node [
    id 528
    label "reumatologia"
  ]
  node [
    id 529
    label "gerontologia"
  ]
  node [
    id 530
    label "geriatria"
  ]
  node [
    id 531
    label "stomatologia"
  ]
  node [
    id 532
    label "biomedycyna"
  ]
  node [
    id 533
    label "ginekologia"
  ]
  node [
    id 534
    label "epidemiologia"
  ]
  node [
    id 535
    label "andrologia"
  ]
  node [
    id 536
    label "toksykologia"
  ]
  node [
    id 537
    label "hipertensjologia"
  ]
  node [
    id 538
    label "kierunek"
  ]
  node [
    id 539
    label "medycyna_s&#261;dowa"
  ]
  node [
    id 540
    label "endokrynologia"
  ]
  node [
    id 541
    label "medycyna_tropikalna"
  ]
  node [
    id 542
    label "p&#322;yn_fizjologiczny"
  ]
  node [
    id 543
    label "psychiatria"
  ]
  node [
    id 544
    label "psychoonkologia"
  ]
  node [
    id 545
    label "neuropsychiatria"
  ]
  node [
    id 546
    label "podologia"
  ]
  node [
    id 547
    label "polisomnografia"
  ]
  node [
    id 548
    label "nefrologia"
  ]
  node [
    id 549
    label "medycyna_lotnicza"
  ]
  node [
    id 550
    label "elektromedycyna"
  ]
  node [
    id 551
    label "higiena"
  ]
  node [
    id 552
    label "zio&#322;olecznictwo"
  ]
  node [
    id 553
    label "antropotomia"
  ]
  node [
    id 554
    label "osmologia"
  ]
  node [
    id 555
    label "ortopedia"
  ]
  node [
    id 556
    label "diabetologia"
  ]
  node [
    id 557
    label "protetyka"
  ]
  node [
    id 558
    label "medycyna_wewn&#281;trzna"
  ]
  node [
    id 559
    label "hepatologia"
  ]
  node [
    id 560
    label "transplantologia"
  ]
  node [
    id 561
    label "diagnostyka"
  ]
  node [
    id 562
    label "psychofizjologia"
  ]
  node [
    id 563
    label "medycyna_nuklearna"
  ]
  node [
    id 564
    label "torakotomia"
  ]
  node [
    id 565
    label "anestezjologia"
  ]
  node [
    id 566
    label "otorynolaryngologia"
  ]
  node [
    id 567
    label "hematologia"
  ]
  node [
    id 568
    label "laryngologia"
  ]
  node [
    id 569
    label "audiologia"
  ]
  node [
    id 570
    label "pediatria"
  ]
  node [
    id 571
    label "gastroenterologia"
  ]
  node [
    id 572
    label "dermatologia"
  ]
  node [
    id 573
    label "nozologia"
  ]
  node [
    id 574
    label "bariatria"
  ]
  node [
    id 575
    label "onkologia"
  ]
  node [
    id 576
    label "kardiologia"
  ]
  node [
    id 577
    label "urologia"
  ]
  node [
    id 578
    label "wenerologia"
  ]
  node [
    id 579
    label "etiologia"
  ]
  node [
    id 580
    label "seksuologia"
  ]
  node [
    id 581
    label "medycyna_sportowa"
  ]
  node [
    id 582
    label "pulmonologia"
  ]
  node [
    id 583
    label "sztuka_leczenia"
  ]
  node [
    id 584
    label "cytologia"
  ]
  node [
    id 585
    label "elektromiografia"
  ]
  node [
    id 586
    label "alergologia"
  ]
  node [
    id 587
    label "ftyzjologia"
  ]
  node [
    id 588
    label "po&#322;o&#380;nictwo"
  ]
  node [
    id 589
    label "neurofizjologia"
  ]
  node [
    id 590
    label "niekorzystny"
  ]
  node [
    id 591
    label "aspo&#322;ecznie"
  ]
  node [
    id 592
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 593
    label "typowy"
  ]
  node [
    id 594
    label "niech&#281;tny"
  ]
  node [
    id 595
    label "provider"
  ]
  node [
    id 596
    label "hipertekst"
  ]
  node [
    id 597
    label "cyberprzestrze&#324;"
  ]
  node [
    id 598
    label "mem"
  ]
  node [
    id 599
    label "gra_sieciowa"
  ]
  node [
    id 600
    label "grooming"
  ]
  node [
    id 601
    label "media"
  ]
  node [
    id 602
    label "biznes_elektroniczny"
  ]
  node [
    id 603
    label "sie&#263;_komputerowa"
  ]
  node [
    id 604
    label "punkt_dost&#281;pu"
  ]
  node [
    id 605
    label "us&#322;uga_internetowa"
  ]
  node [
    id 606
    label "netbook"
  ]
  node [
    id 607
    label "e-hazard"
  ]
  node [
    id 608
    label "podcast"
  ]
  node [
    id 609
    label "strona"
  ]
  node [
    id 610
    label "mass-media"
  ]
  node [
    id 611
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 612
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 613
    label "przekazior"
  ]
  node [
    id 614
    label "uzbrajanie"
  ]
  node [
    id 615
    label "medium"
  ]
  node [
    id 616
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 617
    label "tekst"
  ]
  node [
    id 618
    label "cyberprzest&#281;pczo&#347;&#263;"
  ]
  node [
    id 619
    label "kartka"
  ]
  node [
    id 620
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 621
    label "logowanie"
  ]
  node [
    id 622
    label "plik"
  ]
  node [
    id 623
    label "s&#261;d"
  ]
  node [
    id 624
    label "adres_internetowy"
  ]
  node [
    id 625
    label "linia"
  ]
  node [
    id 626
    label "serwis_internetowy"
  ]
  node [
    id 627
    label "posta&#263;"
  ]
  node [
    id 628
    label "bok"
  ]
  node [
    id 629
    label "skr&#281;canie"
  ]
  node [
    id 630
    label "skr&#281;ca&#263;"
  ]
  node [
    id 631
    label "orientowanie"
  ]
  node [
    id 632
    label "skr&#281;ci&#263;"
  ]
  node [
    id 633
    label "uj&#281;cie"
  ]
  node [
    id 634
    label "zorientowanie"
  ]
  node [
    id 635
    label "ty&#322;"
  ]
  node [
    id 636
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 637
    label "fragment"
  ]
  node [
    id 638
    label "layout"
  ]
  node [
    id 639
    label "obiekt"
  ]
  node [
    id 640
    label "zorientowa&#263;"
  ]
  node [
    id 641
    label "pagina"
  ]
  node [
    id 642
    label "podmiot"
  ]
  node [
    id 643
    label "g&#243;ra"
  ]
  node [
    id 644
    label "orientowa&#263;"
  ]
  node [
    id 645
    label "voice"
  ]
  node [
    id 646
    label "orientacja"
  ]
  node [
    id 647
    label "prz&#243;d"
  ]
  node [
    id 648
    label "powierzchnia"
  ]
  node [
    id 649
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 650
    label "forma"
  ]
  node [
    id 651
    label "skr&#281;cenie"
  ]
  node [
    id 652
    label "komputer_przeno&#347;ny"
  ]
  node [
    id 653
    label "ma&#322;y"
  ]
  node [
    id 654
    label "dostawca"
  ]
  node [
    id 655
    label "telefonia"
  ]
  node [
    id 656
    label "wydawnictwo"
  ]
  node [
    id 657
    label "meme"
  ]
  node [
    id 658
    label "hazard"
  ]
  node [
    id 659
    label "molestowanie_seksualne"
  ]
  node [
    id 660
    label "piel&#281;gnacja"
  ]
  node [
    id 661
    label "zwierz&#281;_domowe"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 397
  ]
  edge [
    source 7
    target 398
  ]
  edge [
    source 7
    target 399
  ]
  edge [
    source 7
    target 400
  ]
  edge [
    source 7
    target 401
  ]
  edge [
    source 7
    target 402
  ]
  edge [
    source 7
    target 403
  ]
  edge [
    source 7
    target 404
  ]
  edge [
    source 7
    target 405
  ]
  edge [
    source 7
    target 406
  ]
  edge [
    source 7
    target 407
  ]
  edge [
    source 7
    target 408
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 409
  ]
  edge [
    source 7
    target 410
  ]
  edge [
    source 7
    target 411
  ]
  edge [
    source 7
    target 412
  ]
  edge [
    source 7
    target 413
  ]
  edge [
    source 7
    target 414
  ]
  edge [
    source 7
    target 415
  ]
  edge [
    source 7
    target 416
  ]
  edge [
    source 7
    target 417
  ]
  edge [
    source 7
    target 418
  ]
  edge [
    source 7
    target 419
  ]
  edge [
    source 7
    target 420
  ]
  edge [
    source 7
    target 421
  ]
  edge [
    source 7
    target 422
  ]
  edge [
    source 7
    target 423
  ]
  edge [
    source 7
    target 424
  ]
  edge [
    source 7
    target 425
  ]
  edge [
    source 7
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 7
    target 428
  ]
  edge [
    source 7
    target 429
  ]
  edge [
    source 7
    target 430
  ]
  edge [
    source 7
    target 431
  ]
  edge [
    source 7
    target 432
  ]
  edge [
    source 7
    target 433
  ]
  edge [
    source 7
    target 434
  ]
  edge [
    source 7
    target 435
  ]
  edge [
    source 7
    target 436
  ]
  edge [
    source 7
    target 437
  ]
  edge [
    source 7
    target 438
  ]
  edge [
    source 7
    target 439
  ]
  edge [
    source 7
    target 440
  ]
  edge [
    source 7
    target 441
  ]
  edge [
    source 7
    target 442
  ]
  edge [
    source 7
    target 443
  ]
  edge [
    source 7
    target 444
  ]
  edge [
    source 7
    target 445
  ]
  edge [
    source 7
    target 446
  ]
  edge [
    source 7
    target 447
  ]
  edge [
    source 7
    target 448
  ]
  edge [
    source 7
    target 449
  ]
  edge [
    source 7
    target 450
  ]
  edge [
    source 7
    target 451
  ]
  edge [
    source 7
    target 452
  ]
  edge [
    source 7
    target 453
  ]
  edge [
    source 7
    target 454
  ]
  edge [
    source 7
    target 455
  ]
  edge [
    source 7
    target 456
  ]
  edge [
    source 7
    target 457
  ]
  edge [
    source 7
    target 458
  ]
  edge [
    source 7
    target 459
  ]
  edge [
    source 7
    target 460
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 489
  ]
  edge [
    source 7
    target 490
  ]
  edge [
    source 7
    target 491
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 492
  ]
  edge [
    source 7
    target 493
  ]
  edge [
    source 7
    target 494
  ]
  edge [
    source 7
    target 495
  ]
  edge [
    source 7
    target 496
  ]
  edge [
    source 7
    target 497
  ]
  edge [
    source 7
    target 498
  ]
  edge [
    source 7
    target 499
  ]
  edge [
    source 7
    target 500
  ]
  edge [
    source 7
    target 501
  ]
  edge [
    source 7
    target 502
  ]
  edge [
    source 7
    target 503
  ]
  edge [
    source 7
    target 504
  ]
  edge [
    source 7
    target 505
  ]
  edge [
    source 7
    target 506
  ]
  edge [
    source 7
    target 507
  ]
  edge [
    source 7
    target 508
  ]
  edge [
    source 7
    target 509
  ]
  edge [
    source 7
    target 510
  ]
  edge [
    source 7
    target 511
  ]
  edge [
    source 7
    target 512
  ]
  edge [
    source 7
    target 513
  ]
  edge [
    source 7
    target 514
  ]
  edge [
    source 7
    target 515
  ]
  edge [
    source 7
    target 516
  ]
  edge [
    source 7
    target 517
  ]
  edge [
    source 7
    target 518
  ]
  edge [
    source 7
    target 519
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 521
  ]
  edge [
    source 7
    target 522
  ]
  edge [
    source 7
    target 523
  ]
  edge [
    source 7
    target 524
  ]
  edge [
    source 7
    target 525
  ]
  edge [
    source 7
    target 526
  ]
  edge [
    source 7
    target 527
  ]
  edge [
    source 7
    target 528
  ]
  edge [
    source 7
    target 529
  ]
  edge [
    source 7
    target 530
  ]
  edge [
    source 7
    target 531
  ]
  edge [
    source 7
    target 532
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 534
  ]
  edge [
    source 7
    target 535
  ]
  edge [
    source 7
    target 536
  ]
  edge [
    source 7
    target 537
  ]
  edge [
    source 7
    target 538
  ]
  edge [
    source 7
    target 539
  ]
  edge [
    source 7
    target 540
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 542
  ]
  edge [
    source 7
    target 543
  ]
  edge [
    source 7
    target 544
  ]
  edge [
    source 7
    target 545
  ]
  edge [
    source 7
    target 546
  ]
  edge [
    source 7
    target 547
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 549
  ]
  edge [
    source 7
    target 550
  ]
  edge [
    source 7
    target 551
  ]
  edge [
    source 7
    target 552
  ]
  edge [
    source 7
    target 553
  ]
  edge [
    source 7
    target 554
  ]
  edge [
    source 7
    target 555
  ]
  edge [
    source 7
    target 556
  ]
  edge [
    source 7
    target 557
  ]
  edge [
    source 7
    target 558
  ]
  edge [
    source 7
    target 559
  ]
  edge [
    source 7
    target 560
  ]
  edge [
    source 7
    target 561
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 563
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 565
  ]
  edge [
    source 7
    target 566
  ]
  edge [
    source 7
    target 567
  ]
  edge [
    source 7
    target 568
  ]
  edge [
    source 7
    target 569
  ]
  edge [
    source 7
    target 570
  ]
  edge [
    source 7
    target 571
  ]
  edge [
    source 7
    target 572
  ]
  edge [
    source 7
    target 573
  ]
  edge [
    source 7
    target 574
  ]
  edge [
    source 7
    target 575
  ]
  edge [
    source 7
    target 576
  ]
  edge [
    source 7
    target 577
  ]
  edge [
    source 7
    target 578
  ]
  edge [
    source 7
    target 579
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 592
  ]
  edge [
    source 7
    target 593
  ]
  edge [
    source 7
    target 594
  ]
  edge [
    source 8
    target 595
  ]
  edge [
    source 8
    target 596
  ]
  edge [
    source 8
    target 597
  ]
  edge [
    source 8
    target 598
  ]
  edge [
    source 8
    target 599
  ]
  edge [
    source 8
    target 600
  ]
  edge [
    source 8
    target 601
  ]
  edge [
    source 8
    target 602
  ]
  edge [
    source 8
    target 603
  ]
  edge [
    source 8
    target 604
  ]
  edge [
    source 8
    target 605
  ]
  edge [
    source 8
    target 606
  ]
  edge [
    source 8
    target 607
  ]
  edge [
    source 8
    target 608
  ]
  edge [
    source 8
    target 609
  ]
  edge [
    source 8
    target 610
  ]
  edge [
    source 8
    target 611
  ]
  edge [
    source 8
    target 612
  ]
  edge [
    source 8
    target 613
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 615
  ]
  edge [
    source 8
    target 616
  ]
  edge [
    source 8
    target 617
  ]
  edge [
    source 8
    target 618
  ]
  edge [
    source 8
    target 619
  ]
  edge [
    source 8
    target 620
  ]
  edge [
    source 8
    target 621
  ]
  edge [
    source 8
    target 622
  ]
  edge [
    source 8
    target 623
  ]
  edge [
    source 8
    target 624
  ]
  edge [
    source 8
    target 625
  ]
  edge [
    source 8
    target 626
  ]
  edge [
    source 8
    target 627
  ]
  edge [
    source 8
    target 628
  ]
  edge [
    source 8
    target 629
  ]
  edge [
    source 8
    target 630
  ]
  edge [
    source 8
    target 631
  ]
  edge [
    source 8
    target 632
  ]
  edge [
    source 8
    target 633
  ]
  edge [
    source 8
    target 634
  ]
  edge [
    source 8
    target 635
  ]
  edge [
    source 8
    target 636
  ]
  edge [
    source 8
    target 637
  ]
  edge [
    source 8
    target 638
  ]
  edge [
    source 8
    target 639
  ]
  edge [
    source 8
    target 640
  ]
  edge [
    source 8
    target 641
  ]
  edge [
    source 8
    target 642
  ]
  edge [
    source 8
    target 643
  ]
  edge [
    source 8
    target 644
  ]
  edge [
    source 8
    target 645
  ]
  edge [
    source 8
    target 646
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 648
  ]
  edge [
    source 8
    target 649
  ]
  edge [
    source 8
    target 650
  ]
  edge [
    source 8
    target 651
  ]
  edge [
    source 8
    target 652
  ]
  edge [
    source 8
    target 653
  ]
  edge [
    source 8
    target 654
  ]
  edge [
    source 8
    target 655
  ]
  edge [
    source 8
    target 656
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 657
  ]
  edge [
    source 8
    target 658
  ]
  edge [
    source 8
    target 659
  ]
  edge [
    source 8
    target 660
  ]
  edge [
    source 8
    target 661
  ]
]
