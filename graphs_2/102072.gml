graph [
  node [
    id 0
    label "konferencja"
    origin "text"
  ]
  node [
    id 1
    label "sejmowy"
    origin "text"
  ]
  node [
    id 2
    label "zasoby"
    origin "text"
  ]
  node [
    id 3
    label "edukacyjny"
    origin "text"
  ]
  node [
    id 4
    label "polska"
    origin "text"
  ]
  node [
    id 5
    label "Ja&#322;ta"
  ]
  node [
    id 6
    label "spotkanie"
  ]
  node [
    id 7
    label "konferencyjka"
  ]
  node [
    id 8
    label "conference"
  ]
  node [
    id 9
    label "grusza_pospolita"
  ]
  node [
    id 10
    label "Poczdam"
  ]
  node [
    id 11
    label "doznanie"
  ]
  node [
    id 12
    label "gathering"
  ]
  node [
    id 13
    label "zawarcie"
  ]
  node [
    id 14
    label "wydarzenie"
  ]
  node [
    id 15
    label "znajomy"
  ]
  node [
    id 16
    label "powitanie"
  ]
  node [
    id 17
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 18
    label "spowodowanie"
  ]
  node [
    id 19
    label "zdarzenie_si&#281;"
  ]
  node [
    id 20
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 21
    label "znalezienie"
  ]
  node [
    id 22
    label "match"
  ]
  node [
    id 23
    label "employment"
  ]
  node [
    id 24
    label "po&#380;egnanie"
  ]
  node [
    id 25
    label "gather"
  ]
  node [
    id 26
    label "spotykanie"
  ]
  node [
    id 27
    label "spotkanie_si&#281;"
  ]
  node [
    id 28
    label "zasoby_kopalin"
  ]
  node [
    id 29
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 30
    label "podmiot_gospodarczy"
  ]
  node [
    id 31
    label "z&#322;o&#380;e"
  ]
  node [
    id 32
    label "integer"
  ]
  node [
    id 33
    label "liczba"
  ]
  node [
    id 34
    label "zlewanie_si&#281;"
  ]
  node [
    id 35
    label "ilo&#347;&#263;"
  ]
  node [
    id 36
    label "uk&#322;ad"
  ]
  node [
    id 37
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 38
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 39
    label "pe&#322;ny"
  ]
  node [
    id 40
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 41
    label "zaleganie"
  ]
  node [
    id 42
    label "bilansowo&#347;&#263;"
  ]
  node [
    id 43
    label "skupienie"
  ]
  node [
    id 44
    label "zalega&#263;"
  ]
  node [
    id 45
    label "wychodnia"
  ]
  node [
    id 46
    label "edukacyjnie"
  ]
  node [
    id 47
    label "naukowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
]
