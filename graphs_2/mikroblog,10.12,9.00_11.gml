graph [
  node [
    id 0
    label "wczorajszy"
    origin "text"
  ]
  node [
    id 1
    label "tinder"
    origin "text"
  ]
  node [
    id 2
    label "randka"
    origin "text"
  ]
  node [
    id 3
    label "uwa&#380;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "udany"
    origin "text"
  ]
  node [
    id 5
    label "dawny"
  ]
  node [
    id 6
    label "zestarzenie_si&#281;"
  ]
  node [
    id 7
    label "starzenie_si&#281;"
  ]
  node [
    id 8
    label "archaicznie"
  ]
  node [
    id 9
    label "zgrzybienie"
  ]
  node [
    id 10
    label "niedzisiejszy"
  ]
  node [
    id 11
    label "przestarzale"
  ]
  node [
    id 12
    label "stary"
  ]
  node [
    id 13
    label "nie&#347;wie&#380;y"
  ]
  node [
    id 14
    label "ojciec"
  ]
  node [
    id 15
    label "nienowoczesny"
  ]
  node [
    id 16
    label "gruba_ryba"
  ]
  node [
    id 17
    label "poprzedni"
  ]
  node [
    id 18
    label "dawno"
  ]
  node [
    id 19
    label "staro"
  ]
  node [
    id 20
    label "m&#261;&#380;"
  ]
  node [
    id 21
    label "starzy"
  ]
  node [
    id 22
    label "dotychczasowy"
  ]
  node [
    id 23
    label "p&#243;&#378;ny"
  ]
  node [
    id 24
    label "d&#322;ugoletni"
  ]
  node [
    id 25
    label "charakterystyczny"
  ]
  node [
    id 26
    label "brat"
  ]
  node [
    id 27
    label "po_staro&#347;wiecku"
  ]
  node [
    id 28
    label "zwierzchnik"
  ]
  node [
    id 29
    label "znajomy"
  ]
  node [
    id 30
    label "odleg&#322;y"
  ]
  node [
    id 31
    label "starczo"
  ]
  node [
    id 32
    label "dawniej"
  ]
  node [
    id 33
    label "niegdysiejszy"
  ]
  node [
    id 34
    label "dojrza&#322;y"
  ]
  node [
    id 35
    label "niewsp&#243;&#322;czesny"
  ]
  node [
    id 36
    label "przestarza&#322;y"
  ]
  node [
    id 37
    label "przesz&#322;y"
  ]
  node [
    id 38
    label "od_dawna"
  ]
  node [
    id 39
    label "anachroniczny"
  ]
  node [
    id 40
    label "wcze&#347;niejszy"
  ]
  node [
    id 41
    label "kombatant"
  ]
  node [
    id 42
    label "niemodnie"
  ]
  node [
    id 43
    label "zdezaktualizowanie_si&#281;"
  ]
  node [
    id 44
    label "zniedo&#322;&#281;&#380;nienie"
  ]
  node [
    id 45
    label "zramolenie"
  ]
  node [
    id 46
    label "postarzenie_si&#281;"
  ]
  node [
    id 47
    label "archaiczny"
  ]
  node [
    id 48
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 49
    label "amory"
  ]
  node [
    id 50
    label "appointment"
  ]
  node [
    id 51
    label "spotkanie"
  ]
  node [
    id 52
    label "sytuacja"
  ]
  node [
    id 53
    label "doznanie"
  ]
  node [
    id 54
    label "gathering"
  ]
  node [
    id 55
    label "zawarcie"
  ]
  node [
    id 56
    label "wydarzenie"
  ]
  node [
    id 57
    label "powitanie"
  ]
  node [
    id 58
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 59
    label "spowodowanie"
  ]
  node [
    id 60
    label "zdarzenie_si&#281;"
  ]
  node [
    id 61
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 62
    label "znalezienie"
  ]
  node [
    id 63
    label "match"
  ]
  node [
    id 64
    label "employment"
  ]
  node [
    id 65
    label "po&#380;egnanie"
  ]
  node [
    id 66
    label "gather"
  ]
  node [
    id 67
    label "spotykanie"
  ]
  node [
    id 68
    label "spotkanie_si&#281;"
  ]
  node [
    id 69
    label "mie&#263;_na_baczenie"
  ]
  node [
    id 70
    label "pilnowa&#263;"
  ]
  node [
    id 71
    label "robi&#263;"
  ]
  node [
    id 72
    label "my&#347;le&#263;"
  ]
  node [
    id 73
    label "continue"
  ]
  node [
    id 74
    label "consider"
  ]
  node [
    id 75
    label "deliver"
  ]
  node [
    id 76
    label "obserwowa&#263;"
  ]
  node [
    id 77
    label "skupia&#263;_si&#281;"
  ]
  node [
    id 78
    label "uznawa&#263;"
  ]
  node [
    id 79
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 80
    label "organizowa&#263;"
  ]
  node [
    id 81
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 82
    label "czyni&#263;"
  ]
  node [
    id 83
    label "give"
  ]
  node [
    id 84
    label "stylizowa&#263;"
  ]
  node [
    id 85
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 86
    label "falowa&#263;"
  ]
  node [
    id 87
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 88
    label "peddle"
  ]
  node [
    id 89
    label "praca"
  ]
  node [
    id 90
    label "wydala&#263;"
  ]
  node [
    id 91
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 92
    label "tentegowa&#263;"
  ]
  node [
    id 93
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 94
    label "urz&#261;dza&#263;"
  ]
  node [
    id 95
    label "oszukiwa&#263;"
  ]
  node [
    id 96
    label "work"
  ]
  node [
    id 97
    label "ukazywa&#263;"
  ]
  node [
    id 98
    label "przerabia&#263;"
  ]
  node [
    id 99
    label "act"
  ]
  node [
    id 100
    label "post&#281;powa&#263;"
  ]
  node [
    id 101
    label "take_care"
  ]
  node [
    id 102
    label "troska&#263;_si&#281;"
  ]
  node [
    id 103
    label "rozpatrywa&#263;"
  ]
  node [
    id 104
    label "zamierza&#263;"
  ]
  node [
    id 105
    label "argue"
  ]
  node [
    id 106
    label "os&#261;dza&#263;"
  ]
  node [
    id 107
    label "notice"
  ]
  node [
    id 108
    label "stwierdza&#263;"
  ]
  node [
    id 109
    label "przyznawa&#263;"
  ]
  node [
    id 110
    label "zachowywa&#263;"
  ]
  node [
    id 111
    label "dostrzega&#263;"
  ]
  node [
    id 112
    label "patrze&#263;"
  ]
  node [
    id 113
    label "look"
  ]
  node [
    id 114
    label "cover"
  ]
  node [
    id 115
    label "udanie"
  ]
  node [
    id 116
    label "przyjemny"
  ]
  node [
    id 117
    label "fajny"
  ]
  node [
    id 118
    label "dobry"
  ]
  node [
    id 119
    label "dobroczynny"
  ]
  node [
    id 120
    label "czw&#243;rka"
  ]
  node [
    id 121
    label "spokojny"
  ]
  node [
    id 122
    label "skuteczny"
  ]
  node [
    id 123
    label "&#347;mieszny"
  ]
  node [
    id 124
    label "mi&#322;y"
  ]
  node [
    id 125
    label "grzeczny"
  ]
  node [
    id 126
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 127
    label "dobrze"
  ]
  node [
    id 128
    label "ca&#322;y"
  ]
  node [
    id 129
    label "zwrot"
  ]
  node [
    id 130
    label "pomy&#347;lny"
  ]
  node [
    id 131
    label "moralny"
  ]
  node [
    id 132
    label "drogi"
  ]
  node [
    id 133
    label "pozytywny"
  ]
  node [
    id 134
    label "odpowiedni"
  ]
  node [
    id 135
    label "korzystny"
  ]
  node [
    id 136
    label "pos&#322;uszny"
  ]
  node [
    id 137
    label "byczy"
  ]
  node [
    id 138
    label "fajnie"
  ]
  node [
    id 139
    label "klawy"
  ]
  node [
    id 140
    label "przyjemnie"
  ]
  node [
    id 141
    label "zachowanie_si&#281;"
  ]
  node [
    id 142
    label "maneuver"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
]
