graph [
  node [
    id 0
    label "siemanko"
    origin "text"
  ]
  node [
    id 1
    label "wykopki"
    origin "text"
  ]
  node [
    id 2
    label "sadzeniak"
  ]
  node [
    id 3
    label "zbi&#243;r"
  ]
  node [
    id 4
    label "egzemplarz"
  ]
  node [
    id 5
    label "series"
  ]
  node [
    id 6
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 7
    label "uprawianie"
  ]
  node [
    id 8
    label "praca_rolnicza"
  ]
  node [
    id 9
    label "collection"
  ]
  node [
    id 10
    label "dane"
  ]
  node [
    id 11
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 12
    label "pakiet_klimatyczny"
  ]
  node [
    id 13
    label "poj&#281;cie"
  ]
  node [
    id 14
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 15
    label "sum"
  ]
  node [
    id 16
    label "gathering"
  ]
  node [
    id 17
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 18
    label "album"
  ]
  node [
    id 19
    label "ziemniak"
  ]
  node [
    id 20
    label "szko&#322;a"
  ]
  node [
    id 21
    label "policja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 20
    target 21
  ]
]
