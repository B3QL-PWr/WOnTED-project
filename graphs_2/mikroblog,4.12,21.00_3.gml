graph [
  node [
    id 0
    label "szczepienie"
    origin "text"
  ]
  node [
    id 1
    label "antyszczepionkowcy"
    origin "text"
  ]
  node [
    id 2
    label "niedojebaniemozgowe"
    origin "text"
  ]
  node [
    id 3
    label "heheszki"
    origin "text"
  ]
  node [
    id 4
    label "uprawianie"
  ]
  node [
    id 5
    label "uodpornianie"
  ]
  node [
    id 6
    label "metoda"
  ]
  node [
    id 7
    label "zabieg"
  ]
  node [
    id 8
    label "wariolacja"
  ]
  node [
    id 9
    label "immunizacja"
  ]
  node [
    id 10
    label "uszlachetnianie"
  ]
  node [
    id 11
    label "inoculation"
  ]
  node [
    id 12
    label "immunizacja_czynna"
  ]
  node [
    id 13
    label "czyn"
  ]
  node [
    id 14
    label "leczenie"
  ]
  node [
    id 15
    label "operation"
  ]
  node [
    id 16
    label "uodparnianie_si&#281;"
  ]
  node [
    id 17
    label "powodowanie"
  ]
  node [
    id 18
    label "wzmacnianie"
  ]
  node [
    id 19
    label "odporny"
  ]
  node [
    id 20
    label "wytrzyma&#322;y"
  ]
  node [
    id 21
    label "immunization"
  ]
  node [
    id 22
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 23
    label "method"
  ]
  node [
    id 24
    label "spos&#243;b"
  ]
  node [
    id 25
    label "doktryna"
  ]
  node [
    id 26
    label "uwznio&#347;lanie"
  ]
  node [
    id 27
    label "ulepszanie"
  ]
  node [
    id 28
    label "polish"
  ]
  node [
    id 29
    label "surowica"
  ]
  node [
    id 30
    label "odporno&#347;&#263;"
  ]
  node [
    id 31
    label "pielenie"
  ]
  node [
    id 32
    label "culture"
  ]
  node [
    id 33
    label "sianie"
  ]
  node [
    id 34
    label "zbi&#243;r"
  ]
  node [
    id 35
    label "stanowisko"
  ]
  node [
    id 36
    label "sadzenie"
  ]
  node [
    id 37
    label "oprysk"
  ]
  node [
    id 38
    label "orka"
  ]
  node [
    id 39
    label "rolnictwo"
  ]
  node [
    id 40
    label "siew"
  ]
  node [
    id 41
    label "exercise"
  ]
  node [
    id 42
    label "koszenie"
  ]
  node [
    id 43
    label "obrabianie"
  ]
  node [
    id 44
    label "czynno&#347;&#263;"
  ]
  node [
    id 45
    label "zajmowanie_si&#281;"
  ]
  node [
    id 46
    label "use"
  ]
  node [
    id 47
    label "biotechnika"
  ]
  node [
    id 48
    label "hodowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
