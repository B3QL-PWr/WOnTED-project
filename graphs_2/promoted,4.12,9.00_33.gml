graph [
  node [
    id 0
    label "pedofil"
    origin "text"
  ]
  node [
    id 1
    label "polski"
    origin "text"
  ]
  node [
    id 2
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "katolicki"
    origin "text"
  ]
  node [
    id 4
    label "wydarzenie"
    origin "text"
  ]
  node [
    id 5
    label "kilka"
    origin "text"
  ]
  node [
    id 6
    label "ostatni"
    origin "text"
  ]
  node [
    id 7
    label "lato"
    origin "text"
  ]
  node [
    id 8
    label "organizm"
  ]
  node [
    id 9
    label "dewiant"
  ]
  node [
    id 10
    label "dziecko"
  ]
  node [
    id 11
    label "p&#322;aszczyzna"
  ]
  node [
    id 12
    label "odwadnia&#263;"
  ]
  node [
    id 13
    label "przyswoi&#263;"
  ]
  node [
    id 14
    label "sk&#243;ra"
  ]
  node [
    id 15
    label "odwodni&#263;"
  ]
  node [
    id 16
    label "ewoluowanie"
  ]
  node [
    id 17
    label "staw"
  ]
  node [
    id 18
    label "ow&#322;osienie"
  ]
  node [
    id 19
    label "unerwienie"
  ]
  node [
    id 20
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 21
    label "reakcja"
  ]
  node [
    id 22
    label "wyewoluowanie"
  ]
  node [
    id 23
    label "przyswajanie"
  ]
  node [
    id 24
    label "kultura_kom&#243;rkowa"
  ]
  node [
    id 25
    label "wyewoluowa&#263;"
  ]
  node [
    id 26
    label "miejsce"
  ]
  node [
    id 27
    label "biorytm"
  ]
  node [
    id 28
    label "ewoluowa&#263;"
  ]
  node [
    id 29
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 30
    label "istota_&#380;ywa"
  ]
  node [
    id 31
    label "otworzy&#263;"
  ]
  node [
    id 32
    label "otwiera&#263;"
  ]
  node [
    id 33
    label "czynnik_biotyczny"
  ]
  node [
    id 34
    label "wn&#281;trzno&#347;ci"
  ]
  node [
    id 35
    label "otworzenie"
  ]
  node [
    id 36
    label "otwieranie"
  ]
  node [
    id 37
    label "individual"
  ]
  node [
    id 38
    label "ty&#322;"
  ]
  node [
    id 39
    label "szkielet"
  ]
  node [
    id 40
    label "obiekt"
  ]
  node [
    id 41
    label "przyswaja&#263;"
  ]
  node [
    id 42
    label "przyswojenie"
  ]
  node [
    id 43
    label "odwadnianie"
  ]
  node [
    id 44
    label "odwodnienie"
  ]
  node [
    id 45
    label "umi&#281;&#347;nienie"
  ]
  node [
    id 46
    label "starzenie_si&#281;"
  ]
  node [
    id 47
    label "uk&#322;ad"
  ]
  node [
    id 48
    label "prz&#243;d"
  ]
  node [
    id 49
    label "temperatura"
  ]
  node [
    id 50
    label "l&#281;d&#378;wie"
  ]
  node [
    id 51
    label "cia&#322;o"
  ]
  node [
    id 52
    label "cz&#322;onek"
  ]
  node [
    id 53
    label "cz&#322;owiek"
  ]
  node [
    id 54
    label "&#347;wintuch"
  ]
  node [
    id 55
    label "nienormalny"
  ]
  node [
    id 56
    label "parafilia"
  ]
  node [
    id 57
    label "bodziec"
  ]
  node [
    id 58
    label "zbok"
  ]
  node [
    id 59
    label "utulenie"
  ]
  node [
    id 60
    label "pediatra"
  ]
  node [
    id 61
    label "dzieciak"
  ]
  node [
    id 62
    label "utulanie"
  ]
  node [
    id 63
    label "dzieciarnia"
  ]
  node [
    id 64
    label "niepe&#322;noletni"
  ]
  node [
    id 65
    label "utula&#263;"
  ]
  node [
    id 66
    label "cz&#322;owieczek"
  ]
  node [
    id 67
    label "fledgling"
  ]
  node [
    id 68
    label "zwierz&#281;"
  ]
  node [
    id 69
    label "utuli&#263;"
  ]
  node [
    id 70
    label "m&#322;odzik"
  ]
  node [
    id 71
    label "m&#322;odziak"
  ]
  node [
    id 72
    label "potomek"
  ]
  node [
    id 73
    label "entliczek-pentliczek"
  ]
  node [
    id 74
    label "potomstwo"
  ]
  node [
    id 75
    label "sraluch"
  ]
  node [
    id 76
    label "przedmiot"
  ]
  node [
    id 77
    label "Polish"
  ]
  node [
    id 78
    label "goniony"
  ]
  node [
    id 79
    label "oberek"
  ]
  node [
    id 80
    label "ryba_po_grecku"
  ]
  node [
    id 81
    label "sztajer"
  ]
  node [
    id 82
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 83
    label "krakowiak"
  ]
  node [
    id 84
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 85
    label "pierogi_ruskie"
  ]
  node [
    id 86
    label "lacki"
  ]
  node [
    id 87
    label "polak"
  ]
  node [
    id 88
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 89
    label "chodzony"
  ]
  node [
    id 90
    label "po_polsku"
  ]
  node [
    id 91
    label "mazur"
  ]
  node [
    id 92
    label "polsko"
  ]
  node [
    id 93
    label "skoczny"
  ]
  node [
    id 94
    label "drabant"
  ]
  node [
    id 95
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 96
    label "j&#281;zyk"
  ]
  node [
    id 97
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 98
    label "artykulator"
  ]
  node [
    id 99
    label "kod"
  ]
  node [
    id 100
    label "kawa&#322;ek"
  ]
  node [
    id 101
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 102
    label "gramatyka"
  ]
  node [
    id 103
    label "stylik"
  ]
  node [
    id 104
    label "przet&#322;umaczenie"
  ]
  node [
    id 105
    label "formalizowanie"
  ]
  node [
    id 106
    label "ssanie"
  ]
  node [
    id 107
    label "ssa&#263;"
  ]
  node [
    id 108
    label "language"
  ]
  node [
    id 109
    label "liza&#263;"
  ]
  node [
    id 110
    label "napisa&#263;"
  ]
  node [
    id 111
    label "konsonantyzm"
  ]
  node [
    id 112
    label "wokalizm"
  ]
  node [
    id 113
    label "pisa&#263;"
  ]
  node [
    id 114
    label "fonetyka"
  ]
  node [
    id 115
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 116
    label "jeniec"
  ]
  node [
    id 117
    label "but"
  ]
  node [
    id 118
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 119
    label "po_koroniarsku"
  ]
  node [
    id 120
    label "kultura_duchowa"
  ]
  node [
    id 121
    label "t&#322;umaczenie"
  ]
  node [
    id 122
    label "m&#243;wienie"
  ]
  node [
    id 123
    label "pype&#263;"
  ]
  node [
    id 124
    label "lizanie"
  ]
  node [
    id 125
    label "pismo"
  ]
  node [
    id 126
    label "formalizowa&#263;"
  ]
  node [
    id 127
    label "rozumie&#263;"
  ]
  node [
    id 128
    label "organ"
  ]
  node [
    id 129
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 130
    label "rozumienie"
  ]
  node [
    id 131
    label "spos&#243;b"
  ]
  node [
    id 132
    label "makroglosja"
  ]
  node [
    id 133
    label "m&#243;wi&#263;"
  ]
  node [
    id 134
    label "jama_ustna"
  ]
  node [
    id 135
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 136
    label "formacja_geologiczna"
  ]
  node [
    id 137
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 138
    label "natural_language"
  ]
  node [
    id 139
    label "s&#322;ownictwo"
  ]
  node [
    id 140
    label "urz&#261;dzenie"
  ]
  node [
    id 141
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 142
    label "wschodnioeuropejski"
  ]
  node [
    id 143
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 144
    label "poga&#324;ski"
  ]
  node [
    id 145
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 146
    label "topielec"
  ]
  node [
    id 147
    label "europejski"
  ]
  node [
    id 148
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 149
    label "langosz"
  ]
  node [
    id 150
    label "zboczenie"
  ]
  node [
    id 151
    label "om&#243;wienie"
  ]
  node [
    id 152
    label "sponiewieranie"
  ]
  node [
    id 153
    label "discipline"
  ]
  node [
    id 154
    label "rzecz"
  ]
  node [
    id 155
    label "omawia&#263;"
  ]
  node [
    id 156
    label "kr&#261;&#380;enie"
  ]
  node [
    id 157
    label "tre&#347;&#263;"
  ]
  node [
    id 158
    label "robienie"
  ]
  node [
    id 159
    label "sponiewiera&#263;"
  ]
  node [
    id 160
    label "element"
  ]
  node [
    id 161
    label "entity"
  ]
  node [
    id 162
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 163
    label "tematyka"
  ]
  node [
    id 164
    label "w&#261;tek"
  ]
  node [
    id 165
    label "charakter"
  ]
  node [
    id 166
    label "zbaczanie"
  ]
  node [
    id 167
    label "program_nauczania"
  ]
  node [
    id 168
    label "om&#243;wi&#263;"
  ]
  node [
    id 169
    label "omawianie"
  ]
  node [
    id 170
    label "thing"
  ]
  node [
    id 171
    label "kultura"
  ]
  node [
    id 172
    label "istota"
  ]
  node [
    id 173
    label "zbacza&#263;"
  ]
  node [
    id 174
    label "zboczy&#263;"
  ]
  node [
    id 175
    label "gwardzista"
  ]
  node [
    id 176
    label "melodia"
  ]
  node [
    id 177
    label "taniec"
  ]
  node [
    id 178
    label "taniec_ludowy"
  ]
  node [
    id 179
    label "&#347;redniowieczny"
  ]
  node [
    id 180
    label "specjalny"
  ]
  node [
    id 181
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 182
    label "weso&#322;y"
  ]
  node [
    id 183
    label "sprawny"
  ]
  node [
    id 184
    label "rytmiczny"
  ]
  node [
    id 185
    label "skocznie"
  ]
  node [
    id 186
    label "energiczny"
  ]
  node [
    id 187
    label "lendler"
  ]
  node [
    id 188
    label "austriacki"
  ]
  node [
    id 189
    label "polka"
  ]
  node [
    id 190
    label "europejsko"
  ]
  node [
    id 191
    label "przytup"
  ]
  node [
    id 192
    label "ho&#322;ubiec"
  ]
  node [
    id 193
    label "wodzi&#263;"
  ]
  node [
    id 194
    label "ludowy"
  ]
  node [
    id 195
    label "pie&#347;&#324;"
  ]
  node [
    id 196
    label "mieszkaniec"
  ]
  node [
    id 197
    label "centu&#347;"
  ]
  node [
    id 198
    label "lalka"
  ]
  node [
    id 199
    label "Ma&#322;opolanin"
  ]
  node [
    id 200
    label "krakauer"
  ]
  node [
    id 201
    label "kult"
  ]
  node [
    id 202
    label "ub&#322;agalnia"
  ]
  node [
    id 203
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 204
    label "nawa"
  ]
  node [
    id 205
    label "wsp&#243;lnota"
  ]
  node [
    id 206
    label "Ska&#322;ka"
  ]
  node [
    id 207
    label "zakrystia"
  ]
  node [
    id 208
    label "prezbiterium"
  ]
  node [
    id 209
    label "kropielnica"
  ]
  node [
    id 210
    label "organizacja_religijna"
  ]
  node [
    id 211
    label "nerwica_eklezjogenna"
  ]
  node [
    id 212
    label "church"
  ]
  node [
    id 213
    label "kruchta"
  ]
  node [
    id 214
    label "dom"
  ]
  node [
    id 215
    label "zwi&#261;zanie"
  ]
  node [
    id 216
    label "Wsp&#243;lnota_Narod&#243;w"
  ]
  node [
    id 217
    label "podobie&#324;stwo"
  ]
  node [
    id 218
    label "Skandynawia"
  ]
  node [
    id 219
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 220
    label "partnership"
  ]
  node [
    id 221
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 222
    label "wi&#261;zanie"
  ]
  node [
    id 223
    label "Ba&#322;kany"
  ]
  node [
    id 224
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 225
    label "society"
  ]
  node [
    id 226
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 227
    label "Walencja"
  ]
  node [
    id 228
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 229
    label "bratnia_dusza"
  ]
  node [
    id 230
    label "zwi&#261;zek"
  ]
  node [
    id 231
    label "zwi&#261;za&#263;"
  ]
  node [
    id 232
    label "marriage"
  ]
  node [
    id 233
    label "przybytek"
  ]
  node [
    id 234
    label "siedlisko"
  ]
  node [
    id 235
    label "budynek"
  ]
  node [
    id 236
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 237
    label "rodzina"
  ]
  node [
    id 238
    label "substancja_mieszkaniowa"
  ]
  node [
    id 239
    label "instytucja"
  ]
  node [
    id 240
    label "siedziba"
  ]
  node [
    id 241
    label "dom_rodzinny"
  ]
  node [
    id 242
    label "grupa"
  ]
  node [
    id 243
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 244
    label "poj&#281;cie"
  ]
  node [
    id 245
    label "stead"
  ]
  node [
    id 246
    label "garderoba"
  ]
  node [
    id 247
    label "wiecha"
  ]
  node [
    id 248
    label "fratria"
  ]
  node [
    id 249
    label "uwielbienie"
  ]
  node [
    id 250
    label "religia"
  ]
  node [
    id 251
    label "translacja"
  ]
  node [
    id 252
    label "postawa"
  ]
  node [
    id 253
    label "egzegeta"
  ]
  node [
    id 254
    label "worship"
  ]
  node [
    id 255
    label "obrz&#281;d"
  ]
  node [
    id 256
    label "babiniec"
  ]
  node [
    id 257
    label "przedsionek"
  ]
  node [
    id 258
    label "pomieszczenie"
  ]
  node [
    id 259
    label "zesp&#243;&#322;"
  ]
  node [
    id 260
    label "korpus"
  ]
  node [
    id 261
    label "&#347;rodowisko"
  ]
  node [
    id 262
    label "o&#322;tarz"
  ]
  node [
    id 263
    label "stalle"
  ]
  node [
    id 264
    label "lampka_wieczysta"
  ]
  node [
    id 265
    label "tabernakulum"
  ]
  node [
    id 266
    label "duchowie&#324;stwo"
  ]
  node [
    id 267
    label "naczynie_na_wod&#281;_&#347;wi&#281;con&#261;"
  ]
  node [
    id 268
    label "paramenty"
  ]
  node [
    id 269
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 270
    label "powszechny"
  ]
  node [
    id 271
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 272
    label "typowy"
  ]
  node [
    id 273
    label "po_katolicku"
  ]
  node [
    id 274
    label "zgodny"
  ]
  node [
    id 275
    label "wyznaniowy"
  ]
  node [
    id 276
    label "zgodnie"
  ]
  node [
    id 277
    label "zbie&#380;ny"
  ]
  node [
    id 278
    label "spokojny"
  ]
  node [
    id 279
    label "dobry"
  ]
  node [
    id 280
    label "znany"
  ]
  node [
    id 281
    label "zbiorowy"
  ]
  node [
    id 282
    label "cz&#281;sty"
  ]
  node [
    id 283
    label "powszechnie"
  ]
  node [
    id 284
    label "generalny"
  ]
  node [
    id 285
    label "zwyczajny"
  ]
  node [
    id 286
    label "typowo"
  ]
  node [
    id 287
    label "zwyk&#322;y"
  ]
  node [
    id 288
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 289
    label "nale&#380;ny"
  ]
  node [
    id 290
    label "nale&#380;yty"
  ]
  node [
    id 291
    label "uprawniony"
  ]
  node [
    id 292
    label "zasadniczy"
  ]
  node [
    id 293
    label "stosownie"
  ]
  node [
    id 294
    label "taki"
  ]
  node [
    id 295
    label "charakterystyczny"
  ]
  node [
    id 296
    label "prawdziwy"
  ]
  node [
    id 297
    label "ten"
  ]
  node [
    id 298
    label "religijny"
  ]
  node [
    id 299
    label "chrze&#347;cija&#324;sko"
  ]
  node [
    id 300
    label "krze&#347;cija&#324;ski"
  ]
  node [
    id 301
    label "przebiec"
  ]
  node [
    id 302
    label "czynno&#347;&#263;"
  ]
  node [
    id 303
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 304
    label "motyw"
  ]
  node [
    id 305
    label "przebiegni&#281;cie"
  ]
  node [
    id 306
    label "fabu&#322;a"
  ]
  node [
    id 307
    label "w&#281;ze&#322;"
  ]
  node [
    id 308
    label "perypetia"
  ]
  node [
    id 309
    label "opowiadanie"
  ]
  node [
    id 310
    label "fraza"
  ]
  node [
    id 311
    label "temat"
  ]
  node [
    id 312
    label "cecha"
  ]
  node [
    id 313
    label "przyczyna"
  ]
  node [
    id 314
    label "sytuacja"
  ]
  node [
    id 315
    label "ozdoba"
  ]
  node [
    id 316
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 317
    label "zbi&#243;r"
  ]
  node [
    id 318
    label "osobowo&#347;&#263;"
  ]
  node [
    id 319
    label "psychika"
  ]
  node [
    id 320
    label "posta&#263;"
  ]
  node [
    id 321
    label "kompleksja"
  ]
  node [
    id 322
    label "fizjonomia"
  ]
  node [
    id 323
    label "zjawisko"
  ]
  node [
    id 324
    label "activity"
  ]
  node [
    id 325
    label "bezproblemowy"
  ]
  node [
    id 326
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 327
    label "przeby&#263;"
  ]
  node [
    id 328
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 329
    label "run"
  ]
  node [
    id 330
    label "proceed"
  ]
  node [
    id 331
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 332
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 333
    label "przemierzy&#263;"
  ]
  node [
    id 334
    label "fly"
  ]
  node [
    id 335
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 336
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 337
    label "przesun&#261;&#263;"
  ]
  node [
    id 338
    label "przemkni&#281;cie"
  ]
  node [
    id 339
    label "zabrzmienie"
  ]
  node [
    id 340
    label "przebycie"
  ]
  node [
    id 341
    label "zdarzenie_si&#281;"
  ]
  node [
    id 342
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 343
    label "ryba"
  ]
  node [
    id 344
    label "&#347;ledziowate"
  ]
  node [
    id 345
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 346
    label "kr&#281;gowiec"
  ]
  node [
    id 347
    label "systemik"
  ]
  node [
    id 348
    label "doniczkowiec"
  ]
  node [
    id 349
    label "mi&#281;so"
  ]
  node [
    id 350
    label "system"
  ]
  node [
    id 351
    label "patroszy&#263;"
  ]
  node [
    id 352
    label "rakowato&#347;&#263;"
  ]
  node [
    id 353
    label "w&#281;dkarstwo"
  ]
  node [
    id 354
    label "ryby"
  ]
  node [
    id 355
    label "fish"
  ]
  node [
    id 356
    label "linia_boczna"
  ]
  node [
    id 357
    label "tar&#322;o"
  ]
  node [
    id 358
    label "wyrostek_filtracyjny"
  ]
  node [
    id 359
    label "m&#281;tnooki"
  ]
  node [
    id 360
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 361
    label "pokrywa_skrzelowa"
  ]
  node [
    id 362
    label "ikra"
  ]
  node [
    id 363
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 364
    label "szczelina_skrzelowa"
  ]
  node [
    id 365
    label "&#347;ledziokszta&#322;tne"
  ]
  node [
    id 366
    label "kolejny"
  ]
  node [
    id 367
    label "niedawno"
  ]
  node [
    id 368
    label "poprzedni"
  ]
  node [
    id 369
    label "pozosta&#322;y"
  ]
  node [
    id 370
    label "ostatnio"
  ]
  node [
    id 371
    label "sko&#324;czony"
  ]
  node [
    id 372
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 373
    label "aktualny"
  ]
  node [
    id 374
    label "najgorszy"
  ]
  node [
    id 375
    label "w&#261;tpliwy"
  ]
  node [
    id 376
    label "nast&#281;pnie"
  ]
  node [
    id 377
    label "inny"
  ]
  node [
    id 378
    label "nastopny"
  ]
  node [
    id 379
    label "kolejno"
  ]
  node [
    id 380
    label "kt&#243;ry&#347;"
  ]
  node [
    id 381
    label "przesz&#322;y"
  ]
  node [
    id 382
    label "wcze&#347;niejszy"
  ]
  node [
    id 383
    label "poprzednio"
  ]
  node [
    id 384
    label "w&#261;tpliwie"
  ]
  node [
    id 385
    label "pozorny"
  ]
  node [
    id 386
    label "&#380;ywy"
  ]
  node [
    id 387
    label "ostateczny"
  ]
  node [
    id 388
    label "wa&#380;ny"
  ]
  node [
    id 389
    label "ludzko&#347;&#263;"
  ]
  node [
    id 390
    label "asymilowanie"
  ]
  node [
    id 391
    label "wapniak"
  ]
  node [
    id 392
    label "asymilowa&#263;"
  ]
  node [
    id 393
    label "os&#322;abia&#263;"
  ]
  node [
    id 394
    label "hominid"
  ]
  node [
    id 395
    label "podw&#322;adny"
  ]
  node [
    id 396
    label "os&#322;abianie"
  ]
  node [
    id 397
    label "g&#322;owa"
  ]
  node [
    id 398
    label "figura"
  ]
  node [
    id 399
    label "portrecista"
  ]
  node [
    id 400
    label "dwun&#243;g"
  ]
  node [
    id 401
    label "profanum"
  ]
  node [
    id 402
    label "mikrokosmos"
  ]
  node [
    id 403
    label "nasada"
  ]
  node [
    id 404
    label "duch"
  ]
  node [
    id 405
    label "antropochoria"
  ]
  node [
    id 406
    label "osoba"
  ]
  node [
    id 407
    label "wz&#243;r"
  ]
  node [
    id 408
    label "senior"
  ]
  node [
    id 409
    label "oddzia&#322;ywanie"
  ]
  node [
    id 410
    label "Adam"
  ]
  node [
    id 411
    label "homo_sapiens"
  ]
  node [
    id 412
    label "polifag"
  ]
  node [
    id 413
    label "wykszta&#322;cony"
  ]
  node [
    id 414
    label "dyplomowany"
  ]
  node [
    id 415
    label "wykwalifikowany"
  ]
  node [
    id 416
    label "pe&#322;n&#261;_g&#281;b&#261;"
  ]
  node [
    id 417
    label "kompletny"
  ]
  node [
    id 418
    label "sko&#324;czenie"
  ]
  node [
    id 419
    label "okre&#347;lony"
  ]
  node [
    id 420
    label "wielki"
  ]
  node [
    id 421
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 422
    label "aktualnie"
  ]
  node [
    id 423
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 424
    label "aktualizowanie"
  ]
  node [
    id 425
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 426
    label "uaktualnienie"
  ]
  node [
    id 427
    label "pora_roku"
  ]
  node [
    id 428
    label "Henryk"
  ]
  node [
    id 429
    label "Jankowski"
  ]
  node [
    id 430
    label "&#347;wi&#281;ty"
  ]
  node [
    id 431
    label "Brygida"
  ]
  node [
    id 432
    label "parafia"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 7
    target 427
  ]
  edge [
    source 428
    target 429
  ]
  edge [
    source 430
    target 431
  ]
  edge [
    source 430
    target 432
  ]
  edge [
    source 431
    target 432
  ]
]
