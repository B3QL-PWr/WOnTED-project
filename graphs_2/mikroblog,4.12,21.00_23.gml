graph [
  node [
    id 0
    label "praca"
    origin "text"
  ]
  node [
    id 1
    label "pracbaza"
    origin "text"
  ]
  node [
    id 2
    label "wpolscejakwlesie"
    origin "text"
  ]
  node [
    id 3
    label "januszebiznesu"
    origin "text"
  ]
  node [
    id 4
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 5
    label "najem"
  ]
  node [
    id 6
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 7
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 8
    label "zak&#322;ad"
  ]
  node [
    id 9
    label "stosunek_pracy"
  ]
  node [
    id 10
    label "benedykty&#324;ski"
  ]
  node [
    id 11
    label "poda&#380;_pracy"
  ]
  node [
    id 12
    label "pracowanie"
  ]
  node [
    id 13
    label "tyrka"
  ]
  node [
    id 14
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 15
    label "wytw&#243;r"
  ]
  node [
    id 16
    label "miejsce"
  ]
  node [
    id 17
    label "zaw&#243;d"
  ]
  node [
    id 18
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 19
    label "tynkarski"
  ]
  node [
    id 20
    label "pracowa&#263;"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "zmiana"
  ]
  node [
    id 23
    label "czynnik_produkcji"
  ]
  node [
    id 24
    label "zobowi&#261;zanie"
  ]
  node [
    id 25
    label "kierownictwo"
  ]
  node [
    id 26
    label "siedziba"
  ]
  node [
    id 27
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 28
    label "przedmiot"
  ]
  node [
    id 29
    label "p&#322;&#243;d"
  ]
  node [
    id 30
    label "work"
  ]
  node [
    id 31
    label "rezultat"
  ]
  node [
    id 32
    label "activity"
  ]
  node [
    id 33
    label "bezproblemowy"
  ]
  node [
    id 34
    label "wydarzenie"
  ]
  node [
    id 35
    label "warunek_lokalowy"
  ]
  node [
    id 36
    label "plac"
  ]
  node [
    id 37
    label "location"
  ]
  node [
    id 38
    label "uwaga"
  ]
  node [
    id 39
    label "przestrze&#324;"
  ]
  node [
    id 40
    label "status"
  ]
  node [
    id 41
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 42
    label "chwila"
  ]
  node [
    id 43
    label "cia&#322;o"
  ]
  node [
    id 44
    label "cecha"
  ]
  node [
    id 45
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 46
    label "rz&#261;d"
  ]
  node [
    id 47
    label "stosunek_prawny"
  ]
  node [
    id 48
    label "oblig"
  ]
  node [
    id 49
    label "uregulowa&#263;"
  ]
  node [
    id 50
    label "oddzia&#322;anie"
  ]
  node [
    id 51
    label "occupation"
  ]
  node [
    id 52
    label "duty"
  ]
  node [
    id 53
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 54
    label "zapowied&#378;"
  ]
  node [
    id 55
    label "obowi&#261;zek"
  ]
  node [
    id 56
    label "statement"
  ]
  node [
    id 57
    label "zapewnienie"
  ]
  node [
    id 58
    label "miejsce_pracy"
  ]
  node [
    id 59
    label "zak&#322;adka"
  ]
  node [
    id 60
    label "jednostka_organizacyjna"
  ]
  node [
    id 61
    label "instytucja"
  ]
  node [
    id 62
    label "wyko&#324;czenie"
  ]
  node [
    id 63
    label "firma"
  ]
  node [
    id 64
    label "czyn"
  ]
  node [
    id 65
    label "company"
  ]
  node [
    id 66
    label "instytut"
  ]
  node [
    id 67
    label "umowa"
  ]
  node [
    id 68
    label "&#321;ubianka"
  ]
  node [
    id 69
    label "dzia&#322;_personalny"
  ]
  node [
    id 70
    label "Kreml"
  ]
  node [
    id 71
    label "Bia&#322;y_Dom"
  ]
  node [
    id 72
    label "budynek"
  ]
  node [
    id 73
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 74
    label "sadowisko"
  ]
  node [
    id 75
    label "rewizja"
  ]
  node [
    id 76
    label "passage"
  ]
  node [
    id 77
    label "oznaka"
  ]
  node [
    id 78
    label "change"
  ]
  node [
    id 79
    label "ferment"
  ]
  node [
    id 80
    label "komplet"
  ]
  node [
    id 81
    label "anatomopatolog"
  ]
  node [
    id 82
    label "zmianka"
  ]
  node [
    id 83
    label "czas"
  ]
  node [
    id 84
    label "zjawisko"
  ]
  node [
    id 85
    label "amendment"
  ]
  node [
    id 86
    label "odmienianie"
  ]
  node [
    id 87
    label "tura"
  ]
  node [
    id 88
    label "cierpliwy"
  ]
  node [
    id 89
    label "mozolny"
  ]
  node [
    id 90
    label "wytrwa&#322;y"
  ]
  node [
    id 91
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 92
    label "benedykty&#324;sko"
  ]
  node [
    id 93
    label "typowy"
  ]
  node [
    id 94
    label "po_benedykty&#324;sku"
  ]
  node [
    id 95
    label "endeavor"
  ]
  node [
    id 96
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 97
    label "mie&#263;_miejsce"
  ]
  node [
    id 98
    label "podejmowa&#263;"
  ]
  node [
    id 99
    label "dziama&#263;"
  ]
  node [
    id 100
    label "do"
  ]
  node [
    id 101
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 102
    label "bangla&#263;"
  ]
  node [
    id 103
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 104
    label "maszyna"
  ]
  node [
    id 105
    label "dzia&#322;a&#263;"
  ]
  node [
    id 106
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 107
    label "tryb"
  ]
  node [
    id 108
    label "funkcjonowa&#263;"
  ]
  node [
    id 109
    label "zawodoznawstwo"
  ]
  node [
    id 110
    label "emocja"
  ]
  node [
    id 111
    label "office"
  ]
  node [
    id 112
    label "kwalifikacje"
  ]
  node [
    id 113
    label "craft"
  ]
  node [
    id 114
    label "przepracowanie_si&#281;"
  ]
  node [
    id 115
    label "zarz&#261;dzanie"
  ]
  node [
    id 116
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 117
    label "podlizanie_si&#281;"
  ]
  node [
    id 118
    label "dopracowanie"
  ]
  node [
    id 119
    label "podlizywanie_si&#281;"
  ]
  node [
    id 120
    label "uruchamianie"
  ]
  node [
    id 121
    label "dzia&#322;anie"
  ]
  node [
    id 122
    label "d&#261;&#380;enie"
  ]
  node [
    id 123
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 124
    label "uruchomienie"
  ]
  node [
    id 125
    label "nakr&#281;canie"
  ]
  node [
    id 126
    label "funkcjonowanie"
  ]
  node [
    id 127
    label "tr&#243;jstronny"
  ]
  node [
    id 128
    label "postaranie_si&#281;"
  ]
  node [
    id 129
    label "odpocz&#281;cie"
  ]
  node [
    id 130
    label "nakr&#281;cenie"
  ]
  node [
    id 131
    label "zatrzymanie"
  ]
  node [
    id 132
    label "spracowanie_si&#281;"
  ]
  node [
    id 133
    label "skakanie"
  ]
  node [
    id 134
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 135
    label "podtrzymywanie"
  ]
  node [
    id 136
    label "w&#322;&#261;czanie"
  ]
  node [
    id 137
    label "zaprz&#281;ganie"
  ]
  node [
    id 138
    label "podejmowanie"
  ]
  node [
    id 139
    label "wyrabianie"
  ]
  node [
    id 140
    label "dzianie_si&#281;"
  ]
  node [
    id 141
    label "use"
  ]
  node [
    id 142
    label "przepracowanie"
  ]
  node [
    id 143
    label "poruszanie_si&#281;"
  ]
  node [
    id 144
    label "funkcja"
  ]
  node [
    id 145
    label "impact"
  ]
  node [
    id 146
    label "przepracowywanie"
  ]
  node [
    id 147
    label "awansowanie"
  ]
  node [
    id 148
    label "courtship"
  ]
  node [
    id 149
    label "zapracowanie"
  ]
  node [
    id 150
    label "wyrobienie"
  ]
  node [
    id 151
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 152
    label "w&#322;&#261;czenie"
  ]
  node [
    id 153
    label "transakcja"
  ]
  node [
    id 154
    label "biuro"
  ]
  node [
    id 155
    label "lead"
  ]
  node [
    id 156
    label "zesp&#243;&#322;"
  ]
  node [
    id 157
    label "w&#322;adza"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
