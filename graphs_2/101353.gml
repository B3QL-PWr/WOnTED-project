graph [
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "cmentarz"
    origin "text"
  ]
  node [
    id 2
    label "&#380;ydowski"
    origin "text"
  ]
  node [
    id 3
    label "cieszyn"
    origin "text"
  ]
  node [
    id 4
    label "kolejny"
  ]
  node [
    id 5
    label "nowo"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "bie&#380;&#261;cy"
  ]
  node [
    id 8
    label "drugi"
  ]
  node [
    id 9
    label "narybek"
  ]
  node [
    id 10
    label "obcy"
  ]
  node [
    id 11
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 12
    label "nowotny"
  ]
  node [
    id 13
    label "nadprzyrodzony"
  ]
  node [
    id 14
    label "nieznany"
  ]
  node [
    id 15
    label "pozaludzki"
  ]
  node [
    id 16
    label "obco"
  ]
  node [
    id 17
    label "tameczny"
  ]
  node [
    id 18
    label "osoba"
  ]
  node [
    id 19
    label "nieznajomo"
  ]
  node [
    id 20
    label "inny"
  ]
  node [
    id 21
    label "cudzy"
  ]
  node [
    id 22
    label "istota_&#380;ywa"
  ]
  node [
    id 23
    label "zaziemsko"
  ]
  node [
    id 24
    label "jednoczesny"
  ]
  node [
    id 25
    label "unowocze&#347;nianie"
  ]
  node [
    id 26
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 27
    label "tera&#378;niejszy"
  ]
  node [
    id 28
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 29
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 30
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 31
    label "nast&#281;pnie"
  ]
  node [
    id 32
    label "nastopny"
  ]
  node [
    id 33
    label "kolejno"
  ]
  node [
    id 34
    label "kt&#243;ry&#347;"
  ]
  node [
    id 35
    label "sw&#243;j"
  ]
  node [
    id 36
    label "przeciwny"
  ]
  node [
    id 37
    label "wt&#243;ry"
  ]
  node [
    id 38
    label "dzie&#324;"
  ]
  node [
    id 39
    label "odwrotnie"
  ]
  node [
    id 40
    label "podobny"
  ]
  node [
    id 41
    label "bie&#380;&#261;co"
  ]
  node [
    id 42
    label "ci&#261;g&#322;y"
  ]
  node [
    id 43
    label "aktualny"
  ]
  node [
    id 44
    label "ludzko&#347;&#263;"
  ]
  node [
    id 45
    label "asymilowanie"
  ]
  node [
    id 46
    label "wapniak"
  ]
  node [
    id 47
    label "asymilowa&#263;"
  ]
  node [
    id 48
    label "os&#322;abia&#263;"
  ]
  node [
    id 49
    label "posta&#263;"
  ]
  node [
    id 50
    label "hominid"
  ]
  node [
    id 51
    label "podw&#322;adny"
  ]
  node [
    id 52
    label "os&#322;abianie"
  ]
  node [
    id 53
    label "g&#322;owa"
  ]
  node [
    id 54
    label "figura"
  ]
  node [
    id 55
    label "portrecista"
  ]
  node [
    id 56
    label "dwun&#243;g"
  ]
  node [
    id 57
    label "profanum"
  ]
  node [
    id 58
    label "mikrokosmos"
  ]
  node [
    id 59
    label "nasada"
  ]
  node [
    id 60
    label "duch"
  ]
  node [
    id 61
    label "antropochoria"
  ]
  node [
    id 62
    label "wz&#243;r"
  ]
  node [
    id 63
    label "senior"
  ]
  node [
    id 64
    label "oddzia&#322;ywanie"
  ]
  node [
    id 65
    label "Adam"
  ]
  node [
    id 66
    label "homo_sapiens"
  ]
  node [
    id 67
    label "polifag"
  ]
  node [
    id 68
    label "dopiero_co"
  ]
  node [
    id 69
    label "formacja"
  ]
  node [
    id 70
    label "potomstwo"
  ]
  node [
    id 71
    label "cinerarium"
  ]
  node [
    id 72
    label "&#380;alnik"
  ]
  node [
    id 73
    label "park_sztywnych"
  ]
  node [
    id 74
    label "sm&#281;tarz"
  ]
  node [
    id 75
    label "pogrobowisko"
  ]
  node [
    id 76
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 77
    label "smentarz"
  ]
  node [
    id 78
    label "miejsce"
  ]
  node [
    id 79
    label "pejsaty"
  ]
  node [
    id 80
    label "&#380;ydowsko"
  ]
  node [
    id 81
    label "semicki"
  ]
  node [
    id 82
    label "charakterystyczny"
  ]
  node [
    id 83
    label "parchaty"
  ]
  node [
    id 84
    label "moszaw"
  ]
  node [
    id 85
    label "charakterystycznie"
  ]
  node [
    id 86
    label "szczeg&#243;lny"
  ]
  node [
    id 87
    label "wyj&#261;tkowy"
  ]
  node [
    id 88
    label "typowy"
  ]
  node [
    id 89
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 90
    label "egzotyczny"
  ]
  node [
    id 91
    label "semicko"
  ]
  node [
    id 92
    label "po_&#380;ydowsku"
  ]
  node [
    id 93
    label "Palestyna"
  ]
  node [
    id 94
    label "izraelski"
  ]
  node [
    id 95
    label "sp&#243;&#322;dzielnia"
  ]
  node [
    id 96
    label "chory"
  ]
  node [
    id 97
    label "paskudny"
  ]
  node [
    id 98
    label "ow&#322;osiony"
  ]
  node [
    id 99
    label "stary"
  ]
  node [
    id 100
    label "Andrzej"
  ]
  node [
    id 101
    label "folwarczny"
  ]
  node [
    id 102
    label "ministerstwo"
  ]
  node [
    id 103
    label "sprawi&#263;"
  ]
  node [
    id 104
    label "wewn&#281;trzny"
  ]
  node [
    id 105
    label "naczelny"
  ]
  node [
    id 106
    label "s&#261;d"
  ]
  node [
    id 107
    label "administracyjny"
  ]
  node [
    id 108
    label "Jan"
  ]
  node [
    id 109
    label "Glesingera"
  ]
  node [
    id 110
    label "Alojzy"
  ]
  node [
    id 111
    label "Jedek"
  ]
  node [
    id 112
    label "Jakoba"
  ]
  node [
    id 113
    label "Gartnera"
  ]
  node [
    id 114
    label "starzy"
  ]
  node [
    id 115
    label "ii"
  ]
  node [
    id 116
    label "wojna"
  ]
  node [
    id 117
    label "&#347;wiatowy"
  ]
  node [
    id 118
    label "iii"
  ]
  node [
    id 119
    label "rzesza"
  ]
  node [
    id 120
    label "kongregacja"
  ]
  node [
    id 121
    label "wyzna&#263;"
  ]
  node [
    id 122
    label "moj&#380;eszowy"
  ]
  node [
    id 123
    label "bielski"
  ]
  node [
    id 124
    label "bia&#322;y"
  ]
  node [
    id 125
    label "Edwarda"
  ]
  node [
    id 126
    label "weber"
  ]
  node [
    id 127
    label "gmina"
  ]
  node [
    id 128
    label "wyznaniowy"
  ]
  node [
    id 129
    label "J"
  ]
  node [
    id 130
    label "Phillips"
  ]
  node [
    id 131
    label "Tadeusz"
  ]
  node [
    id 132
    label "Dordy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 100
    target 101
  ]
  edge [
    source 102
    target 103
  ]
  edge [
    source 102
    target 104
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 107
  ]
  edge [
    source 106
    target 107
  ]
  edge [
    source 108
    target 109
  ]
  edge [
    source 110
    target 111
  ]
  edge [
    source 112
    target 113
  ]
  edge [
    source 115
    target 116
  ]
  edge [
    source 115
    target 117
  ]
  edge [
    source 116
    target 117
  ]
  edge [
    source 118
    target 119
  ]
  edge [
    source 120
    target 121
  ]
  edge [
    source 120
    target 122
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 123
    target 124
  ]
  edge [
    source 125
    target 126
  ]
  edge [
    source 125
    target 129
  ]
  edge [
    source 125
    target 130
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 129
    target 130
  ]
  edge [
    source 129
    target 131
  ]
  edge [
    source 129
    target 132
  ]
  edge [
    source 131
    target 132
  ]
]
