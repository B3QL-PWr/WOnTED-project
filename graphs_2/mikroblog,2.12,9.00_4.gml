graph [
  node [
    id 0
    label "ponownie"
    origin "text"
  ]
  node [
    id 1
    label "weekend"
    origin "text"
  ]
  node [
    id 2
    label "wszystko"
    origin "text"
  ]
  node [
    id 3
    label "dobre"
    origin "text"
  ]
  node [
    id 4
    label "ponowny"
  ]
  node [
    id 5
    label "znowu"
  ]
  node [
    id 6
    label "wznawianie_si&#281;"
  ]
  node [
    id 7
    label "wznawianie"
  ]
  node [
    id 8
    label "drugi"
  ]
  node [
    id 9
    label "nawrotny"
  ]
  node [
    id 10
    label "dalszy"
  ]
  node [
    id 11
    label "wznowienie_si&#281;"
  ]
  node [
    id 12
    label "wznowienie"
  ]
  node [
    id 13
    label "sobota"
  ]
  node [
    id 14
    label "tydzie&#324;"
  ]
  node [
    id 15
    label "niedziela"
  ]
  node [
    id 16
    label "dzie&#324;_powszedni"
  ]
  node [
    id 17
    label "Wielka_Sobota"
  ]
  node [
    id 18
    label "bia&#322;a_niedziela"
  ]
  node [
    id 19
    label "Dzie&#324;_Papieski"
  ]
  node [
    id 20
    label "dzie&#324;_&#347;wi&#261;teczny"
  ]
  node [
    id 21
    label "Niedziela_Palmowa"
  ]
  node [
    id 22
    label "Wielkanoc"
  ]
  node [
    id 23
    label "niedziela_przewodnia"
  ]
  node [
    id 24
    label "dzie&#324;_&#347;wi&#281;ty"
  ]
  node [
    id 25
    label "Zes&#322;anie_Ducha_&#346;wi&#281;tego"
  ]
  node [
    id 26
    label "doba"
  ]
  node [
    id 27
    label "miesi&#261;c"
  ]
  node [
    id 28
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 29
    label "czas"
  ]
  node [
    id 30
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 31
    label "absolut"
  ]
  node [
    id 32
    label "lock"
  ]
  node [
    id 33
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 34
    label "liczba"
  ]
  node [
    id 35
    label "uk&#322;ad"
  ]
  node [
    id 36
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 37
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 38
    label "integer"
  ]
  node [
    id 39
    label "zlewanie_si&#281;"
  ]
  node [
    id 40
    label "ilo&#347;&#263;"
  ]
  node [
    id 41
    label "pe&#322;ny"
  ]
  node [
    id 42
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 43
    label "olejek_eteryczny"
  ]
  node [
    id 44
    label "byt"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
]
