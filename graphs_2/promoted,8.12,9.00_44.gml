graph [
  node [
    id 0
    label "starcie"
    origin "text"
  ]
  node [
    id 1
    label "gks"
    origin "text"
  ]
  node [
    id 2
    label "katowice"
    origin "text"
  ]
  node [
    id 3
    label "ruch"
    origin "text"
  ]
  node [
    id 4
    label "chorz&#243;w"
    origin "text"
  ]
  node [
    id 5
    label "obok"
    origin "text"
  ]
  node [
    id 6
    label "szpital"
    origin "text"
  ]
  node [
    id 7
    label "dzieci&#281;cy"
    origin "text"
  ]
  node [
    id 8
    label "ligocie"
    origin "text"
  ]
  node [
    id 9
    label "trafienie"
  ]
  node [
    id 10
    label "oczyszczenie"
  ]
  node [
    id 11
    label "otarcie"
  ]
  node [
    id 12
    label "rewan&#380;owy"
  ]
  node [
    id 13
    label "faza"
  ]
  node [
    id 14
    label "zaatakowanie"
  ]
  node [
    id 15
    label "euroliga"
  ]
  node [
    id 16
    label "sambo"
  ]
  node [
    id 17
    label "expunction"
  ]
  node [
    id 18
    label "rub"
  ]
  node [
    id 19
    label "wydarzenie"
  ]
  node [
    id 20
    label "rozdrobnienie"
  ]
  node [
    id 21
    label "contest"
  ]
  node [
    id 22
    label "k&#322;&#243;tnia"
  ]
  node [
    id 23
    label "zranienie"
  ]
  node [
    id 24
    label "military_action"
  ]
  node [
    id 25
    label "zagrywka"
  ]
  node [
    id 26
    label "obrona"
  ]
  node [
    id 27
    label "usuni&#281;cie"
  ]
  node [
    id 28
    label "konfrontacyjny"
  ]
  node [
    id 29
    label "scramble"
  ]
  node [
    id 30
    label "discord"
  ]
  node [
    id 31
    label "interliga"
  ]
  node [
    id 32
    label "runda"
  ]
  node [
    id 33
    label "przebiec"
  ]
  node [
    id 34
    label "charakter"
  ]
  node [
    id 35
    label "czynno&#347;&#263;"
  ]
  node [
    id 36
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 37
    label "motyw"
  ]
  node [
    id 38
    label "przebiegni&#281;cie"
  ]
  node [
    id 39
    label "fabu&#322;a"
  ]
  node [
    id 40
    label "rozognia&#263;_si&#281;"
  ]
  node [
    id 41
    label "rozognianie_si&#281;"
  ]
  node [
    id 42
    label "zadra"
  ]
  node [
    id 43
    label "spina"
  ]
  node [
    id 44
    label "rozognienie_si&#281;"
  ]
  node [
    id 45
    label "gniew"
  ]
  node [
    id 46
    label "sp&#243;r"
  ]
  node [
    id 47
    label "rozogni&#263;_si&#281;"
  ]
  node [
    id 48
    label "swar"
  ]
  node [
    id 49
    label "row"
  ]
  node [
    id 50
    label "cykl_astronomiczny"
  ]
  node [
    id 51
    label "coil"
  ]
  node [
    id 52
    label "zjawisko"
  ]
  node [
    id 53
    label "fotoelement"
  ]
  node [
    id 54
    label "komutowanie"
  ]
  node [
    id 55
    label "stan_skupienia"
  ]
  node [
    id 56
    label "nastr&#243;j"
  ]
  node [
    id 57
    label "przerywacz"
  ]
  node [
    id 58
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 59
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 60
    label "kraw&#281;d&#378;"
  ]
  node [
    id 61
    label "obsesja"
  ]
  node [
    id 62
    label "dw&#243;jnik"
  ]
  node [
    id 63
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 64
    label "okres"
  ]
  node [
    id 65
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 66
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 67
    label "przew&#243;d"
  ]
  node [
    id 68
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 69
    label "czas"
  ]
  node [
    id 70
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 71
    label "obw&#243;d"
  ]
  node [
    id 72
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 73
    label "degree"
  ]
  node [
    id 74
    label "komutowa&#263;"
  ]
  node [
    id 75
    label "wyniesienie"
  ]
  node [
    id 76
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 77
    label "odej&#347;cie"
  ]
  node [
    id 78
    label "pozabieranie"
  ]
  node [
    id 79
    label "pozbycie_si&#281;"
  ]
  node [
    id 80
    label "pousuwanie"
  ]
  node [
    id 81
    label "przesuni&#281;cie"
  ]
  node [
    id 82
    label "przeniesienie"
  ]
  node [
    id 83
    label "znikni&#281;cie"
  ]
  node [
    id 84
    label "spowodowanie"
  ]
  node [
    id 85
    label "coitus_interruptus"
  ]
  node [
    id 86
    label "abstraction"
  ]
  node [
    id 87
    label "removal"
  ]
  node [
    id 88
    label "wyrugowanie"
  ]
  node [
    id 89
    label "zszy&#263;"
  ]
  node [
    id 90
    label "wzbudzenie"
  ]
  node [
    id 91
    label "j&#261;trzy&#263;"
  ]
  node [
    id 92
    label "zaklejanie"
  ]
  node [
    id 93
    label "uszkodzenie"
  ]
  node [
    id 94
    label "rozwalenie_si&#281;"
  ]
  node [
    id 95
    label "poranienie"
  ]
  node [
    id 96
    label "uraz"
  ]
  node [
    id 97
    label "ropie&#263;"
  ]
  node [
    id 98
    label "wound"
  ]
  node [
    id 99
    label "zszywanie"
  ]
  node [
    id 100
    label "zszycie"
  ]
  node [
    id 101
    label "damage"
  ]
  node [
    id 102
    label "ropienie"
  ]
  node [
    id 103
    label "zaklejenie"
  ]
  node [
    id 104
    label "offense"
  ]
  node [
    id 105
    label "rych&#322;ozrost"
  ]
  node [
    id 106
    label "zszywa&#263;"
  ]
  node [
    id 107
    label "chla&#347;ni&#281;cie"
  ]
  node [
    id 108
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 109
    label "expurgation"
  ]
  node [
    id 110
    label "uporz&#261;dkowanie"
  ]
  node [
    id 111
    label "uwolnienie"
  ]
  node [
    id 112
    label "purification"
  ]
  node [
    id 113
    label "fragmentation"
  ]
  node [
    id 114
    label "podzielenie"
  ]
  node [
    id 115
    label "cecha"
  ]
  node [
    id 116
    label "skrytykowanie"
  ]
  node [
    id 117
    label "time"
  ]
  node [
    id 118
    label "walka"
  ]
  node [
    id 119
    label "manewr"
  ]
  node [
    id 120
    label "nast&#261;pienie"
  ]
  node [
    id 121
    label "oddzia&#322;anie"
  ]
  node [
    id 122
    label "przebycie"
  ]
  node [
    id 123
    label "upolowanie"
  ]
  node [
    id 124
    label "wdarcie_si&#281;"
  ]
  node [
    id 125
    label "wyskoczenie_z_g&#281;b&#261;"
  ]
  node [
    id 126
    label "sport"
  ]
  node [
    id 127
    label "progress"
  ]
  node [
    id 128
    label "spr&#243;bowanie"
  ]
  node [
    id 129
    label "powiedzenie"
  ]
  node [
    id 130
    label "rozegranie"
  ]
  node [
    id 131
    label "zrobienie"
  ]
  node [
    id 132
    label "egzamin"
  ]
  node [
    id 133
    label "liga"
  ]
  node [
    id 134
    label "gracz"
  ]
  node [
    id 135
    label "poj&#281;cie"
  ]
  node [
    id 136
    label "protection"
  ]
  node [
    id 137
    label "poparcie"
  ]
  node [
    id 138
    label "mecz"
  ]
  node [
    id 139
    label "reakcja"
  ]
  node [
    id 140
    label "defense"
  ]
  node [
    id 141
    label "s&#261;d"
  ]
  node [
    id 142
    label "auspices"
  ]
  node [
    id 143
    label "gra"
  ]
  node [
    id 144
    label "ochrona"
  ]
  node [
    id 145
    label "post&#281;powanie"
  ]
  node [
    id 146
    label "wojsko"
  ]
  node [
    id 147
    label "defensive_structure"
  ]
  node [
    id 148
    label "guard_duty"
  ]
  node [
    id 149
    label "strona"
  ]
  node [
    id 150
    label "rozgrywka"
  ]
  node [
    id 151
    label "seria"
  ]
  node [
    id 152
    label "rhythm"
  ]
  node [
    id 153
    label "turniej"
  ]
  node [
    id 154
    label "okr&#261;&#380;enie"
  ]
  node [
    id 155
    label "gambit"
  ]
  node [
    id 156
    label "move"
  ]
  node [
    id 157
    label "uderzenie"
  ]
  node [
    id 158
    label "posuni&#281;cie"
  ]
  node [
    id 159
    label "myk"
  ]
  node [
    id 160
    label "gra_w_karty"
  ]
  node [
    id 161
    label "travel"
  ]
  node [
    id 162
    label "zjawienie_si&#281;"
  ]
  node [
    id 163
    label "dolecenie"
  ]
  node [
    id 164
    label "punkt"
  ]
  node [
    id 165
    label "strike"
  ]
  node [
    id 166
    label "dostanie_si&#281;"
  ]
  node [
    id 167
    label "wpadni&#281;cie"
  ]
  node [
    id 168
    label "pocisk"
  ]
  node [
    id 169
    label "hit"
  ]
  node [
    id 170
    label "zdarzenie_si&#281;"
  ]
  node [
    id 171
    label "sukces"
  ]
  node [
    id 172
    label "znalezienie_si&#281;"
  ]
  node [
    id 173
    label "znalezienie"
  ]
  node [
    id 174
    label "dopasowanie_si&#281;"
  ]
  node [
    id 175
    label "dotarcie"
  ]
  node [
    id 176
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 177
    label "gather"
  ]
  node [
    id 178
    label "dostanie"
  ]
  node [
    id 179
    label "agresywny"
  ]
  node [
    id 180
    label "konfrontacyjnie"
  ]
  node [
    id 181
    label "zapasy"
  ]
  node [
    id 182
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 183
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 184
    label "koszyk&#243;wka"
  ]
  node [
    id 185
    label "attrition"
  ]
  node [
    id 186
    label "przetarcie"
  ]
  node [
    id 187
    label "mechanika"
  ]
  node [
    id 188
    label "utrzymywanie"
  ]
  node [
    id 189
    label "poruszenie"
  ]
  node [
    id 190
    label "movement"
  ]
  node [
    id 191
    label "utrzyma&#263;"
  ]
  node [
    id 192
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 193
    label "utrzymanie"
  ]
  node [
    id 194
    label "kanciasty"
  ]
  node [
    id 195
    label "commercial_enterprise"
  ]
  node [
    id 196
    label "model"
  ]
  node [
    id 197
    label "strumie&#324;"
  ]
  node [
    id 198
    label "proces"
  ]
  node [
    id 199
    label "aktywno&#347;&#263;"
  ]
  node [
    id 200
    label "kr&#243;tki"
  ]
  node [
    id 201
    label "taktyka"
  ]
  node [
    id 202
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 203
    label "apraksja"
  ]
  node [
    id 204
    label "natural_process"
  ]
  node [
    id 205
    label "utrzymywa&#263;"
  ]
  node [
    id 206
    label "d&#322;ugi"
  ]
  node [
    id 207
    label "dyssypacja_energii"
  ]
  node [
    id 208
    label "tumult"
  ]
  node [
    id 209
    label "stopek"
  ]
  node [
    id 210
    label "zmiana"
  ]
  node [
    id 211
    label "lokomocja"
  ]
  node [
    id 212
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 213
    label "komunikacja"
  ]
  node [
    id 214
    label "drift"
  ]
  node [
    id 215
    label "kognicja"
  ]
  node [
    id 216
    label "przebieg"
  ]
  node [
    id 217
    label "rozprawa"
  ]
  node [
    id 218
    label "legislacyjnie"
  ]
  node [
    id 219
    label "przes&#322;anka"
  ]
  node [
    id 220
    label "nast&#281;pstwo"
  ]
  node [
    id 221
    label "action"
  ]
  node [
    id 222
    label "stan"
  ]
  node [
    id 223
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 224
    label "postawa"
  ]
  node [
    id 225
    label "maneuver"
  ]
  node [
    id 226
    label "absolutorium"
  ]
  node [
    id 227
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 228
    label "dzia&#322;anie"
  ]
  node [
    id 229
    label "activity"
  ]
  node [
    id 230
    label "boski"
  ]
  node [
    id 231
    label "krajobraz"
  ]
  node [
    id 232
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 233
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 234
    label "przywidzenie"
  ]
  node [
    id 235
    label "presence"
  ]
  node [
    id 236
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 237
    label "transportation_system"
  ]
  node [
    id 238
    label "explicite"
  ]
  node [
    id 239
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 240
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 241
    label "wydeptywanie"
  ]
  node [
    id 242
    label "miejsce"
  ]
  node [
    id 243
    label "wydeptanie"
  ]
  node [
    id 244
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 245
    label "implicite"
  ]
  node [
    id 246
    label "ekspedytor"
  ]
  node [
    id 247
    label "bezproblemowy"
  ]
  node [
    id 248
    label "rewizja"
  ]
  node [
    id 249
    label "passage"
  ]
  node [
    id 250
    label "oznaka"
  ]
  node [
    id 251
    label "change"
  ]
  node [
    id 252
    label "ferment"
  ]
  node [
    id 253
    label "komplet"
  ]
  node [
    id 254
    label "anatomopatolog"
  ]
  node [
    id 255
    label "zmianka"
  ]
  node [
    id 256
    label "amendment"
  ]
  node [
    id 257
    label "praca"
  ]
  node [
    id 258
    label "odmienianie"
  ]
  node [
    id 259
    label "tura"
  ]
  node [
    id 260
    label "daleki"
  ]
  node [
    id 261
    label "d&#322;ugo"
  ]
  node [
    id 262
    label "struktura"
  ]
  node [
    id 263
    label "mechanika_teoretyczna"
  ]
  node [
    id 264
    label "mechanika_gruntu"
  ]
  node [
    id 265
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 266
    label "mechanika_klasyczna"
  ]
  node [
    id 267
    label "elektromechanika"
  ]
  node [
    id 268
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 269
    label "nauka"
  ]
  node [
    id 270
    label "fizyka"
  ]
  node [
    id 271
    label "aeromechanika"
  ]
  node [
    id 272
    label "telemechanika"
  ]
  node [
    id 273
    label "hydromechanika"
  ]
  node [
    id 274
    label "disquiet"
  ]
  node [
    id 275
    label "ha&#322;as"
  ]
  node [
    id 276
    label "woda_powierzchniowa"
  ]
  node [
    id 277
    label "ciek_wodny"
  ]
  node [
    id 278
    label "mn&#243;stwo"
  ]
  node [
    id 279
    label "Ajgospotamoj"
  ]
  node [
    id 280
    label "fala"
  ]
  node [
    id 281
    label "szybki"
  ]
  node [
    id 282
    label "jednowyrazowy"
  ]
  node [
    id 283
    label "bliski"
  ]
  node [
    id 284
    label "s&#322;aby"
  ]
  node [
    id 285
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 286
    label "kr&#243;tko"
  ]
  node [
    id 287
    label "drobny"
  ]
  node [
    id 288
    label "brak"
  ]
  node [
    id 289
    label "z&#322;y"
  ]
  node [
    id 290
    label "obronienie"
  ]
  node [
    id 291
    label "zap&#322;acenie"
  ]
  node [
    id 292
    label "zachowanie"
  ]
  node [
    id 293
    label "potrzymanie"
  ]
  node [
    id 294
    label "przetrzymanie"
  ]
  node [
    id 295
    label "preservation"
  ]
  node [
    id 296
    label "byt"
  ]
  node [
    id 297
    label "bearing"
  ]
  node [
    id 298
    label "zdo&#322;anie"
  ]
  node [
    id 299
    label "subsystencja"
  ]
  node [
    id 300
    label "uniesienie"
  ]
  node [
    id 301
    label "wy&#380;ywienie"
  ]
  node [
    id 302
    label "zapewnienie"
  ]
  node [
    id 303
    label "podtrzymanie"
  ]
  node [
    id 304
    label "wychowanie"
  ]
  node [
    id 305
    label "obroni&#263;"
  ]
  node [
    id 306
    label "potrzyma&#263;"
  ]
  node [
    id 307
    label "op&#322;aci&#263;"
  ]
  node [
    id 308
    label "zdo&#322;a&#263;"
  ]
  node [
    id 309
    label "podtrzyma&#263;"
  ]
  node [
    id 310
    label "feed"
  ]
  node [
    id 311
    label "zrobi&#263;"
  ]
  node [
    id 312
    label "przetrzyma&#263;"
  ]
  node [
    id 313
    label "foster"
  ]
  node [
    id 314
    label "preserve"
  ]
  node [
    id 315
    label "zapewni&#263;"
  ]
  node [
    id 316
    label "zachowa&#263;"
  ]
  node [
    id 317
    label "unie&#347;&#263;"
  ]
  node [
    id 318
    label "argue"
  ]
  node [
    id 319
    label "podtrzymywa&#263;"
  ]
  node [
    id 320
    label "s&#261;dzi&#263;"
  ]
  node [
    id 321
    label "twierdzi&#263;"
  ]
  node [
    id 322
    label "zapewnia&#263;"
  ]
  node [
    id 323
    label "corroborate"
  ]
  node [
    id 324
    label "trzyma&#263;"
  ]
  node [
    id 325
    label "panowa&#263;"
  ]
  node [
    id 326
    label "defy"
  ]
  node [
    id 327
    label "cope"
  ]
  node [
    id 328
    label "broni&#263;"
  ]
  node [
    id 329
    label "sprawowa&#263;"
  ]
  node [
    id 330
    label "&#322;o&#380;y&#263;"
  ]
  node [
    id 331
    label "zachowywa&#263;"
  ]
  node [
    id 332
    label "bronienie"
  ]
  node [
    id 333
    label "trzymanie"
  ]
  node [
    id 334
    label "podtrzymywanie"
  ]
  node [
    id 335
    label "bycie"
  ]
  node [
    id 336
    label "wychowywanie"
  ]
  node [
    id 337
    label "panowanie"
  ]
  node [
    id 338
    label "zachowywanie"
  ]
  node [
    id 339
    label "twierdzenie"
  ]
  node [
    id 340
    label "chowanie"
  ]
  node [
    id 341
    label "retention"
  ]
  node [
    id 342
    label "op&#322;acanie"
  ]
  node [
    id 343
    label "s&#261;dzenie"
  ]
  node [
    id 344
    label "zapewnianie"
  ]
  node [
    id 345
    label "gesture"
  ]
  node [
    id 346
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 347
    label "poruszanie_si&#281;"
  ]
  node [
    id 348
    label "nietaktowny"
  ]
  node [
    id 349
    label "kanciasto"
  ]
  node [
    id 350
    label "niezgrabny"
  ]
  node [
    id 351
    label "kanciaty"
  ]
  node [
    id 352
    label "szorstki"
  ]
  node [
    id 353
    label "niesk&#322;adny"
  ]
  node [
    id 354
    label "stra&#380;nik"
  ]
  node [
    id 355
    label "szko&#322;a"
  ]
  node [
    id 356
    label "przedszkole"
  ]
  node [
    id 357
    label "opiekun"
  ]
  node [
    id 358
    label "spos&#243;b"
  ]
  node [
    id 359
    label "cz&#322;owiek"
  ]
  node [
    id 360
    label "prezenter"
  ]
  node [
    id 361
    label "typ"
  ]
  node [
    id 362
    label "mildew"
  ]
  node [
    id 363
    label "zi&#243;&#322;ko"
  ]
  node [
    id 364
    label "motif"
  ]
  node [
    id 365
    label "pozowanie"
  ]
  node [
    id 366
    label "ideal"
  ]
  node [
    id 367
    label "wz&#243;r"
  ]
  node [
    id 368
    label "matryca"
  ]
  node [
    id 369
    label "adaptation"
  ]
  node [
    id 370
    label "pozowa&#263;"
  ]
  node [
    id 371
    label "imitacja"
  ]
  node [
    id 372
    label "orygina&#322;"
  ]
  node [
    id 373
    label "facet"
  ]
  node [
    id 374
    label "miniatura"
  ]
  node [
    id 375
    label "apraxia"
  ]
  node [
    id 376
    label "zaburzenie"
  ]
  node [
    id 377
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 378
    label "sport_motorowy"
  ]
  node [
    id 379
    label "jazda"
  ]
  node [
    id 380
    label "zwiad"
  ]
  node [
    id 381
    label "metoda"
  ]
  node [
    id 382
    label "pocz&#261;tki"
  ]
  node [
    id 383
    label "wrinkle"
  ]
  node [
    id 384
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 385
    label "Sierpie&#324;"
  ]
  node [
    id 386
    label "Michnik"
  ]
  node [
    id 387
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 388
    label "solidarno&#347;ciowiec"
  ]
  node [
    id 389
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 390
    label "blisko"
  ]
  node [
    id 391
    label "znajomy"
  ]
  node [
    id 392
    label "zwi&#261;zany"
  ]
  node [
    id 393
    label "przesz&#322;y"
  ]
  node [
    id 394
    label "silny"
  ]
  node [
    id 395
    label "zbli&#380;enie"
  ]
  node [
    id 396
    label "oddalony"
  ]
  node [
    id 397
    label "dok&#322;adny"
  ]
  node [
    id 398
    label "nieodleg&#322;y"
  ]
  node [
    id 399
    label "przysz&#322;y"
  ]
  node [
    id 400
    label "gotowy"
  ]
  node [
    id 401
    label "ma&#322;y"
  ]
  node [
    id 402
    label "centrum_urazowe"
  ]
  node [
    id 403
    label "kostnica"
  ]
  node [
    id 404
    label "izba_chorych"
  ]
  node [
    id 405
    label "instytucja"
  ]
  node [
    id 406
    label "oddzia&#322;_septyczny"
  ]
  node [
    id 407
    label "klinicysta"
  ]
  node [
    id 408
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 409
    label "oddzia&#322;"
  ]
  node [
    id 410
    label "blok_operacyjny"
  ]
  node [
    id 411
    label "zabieg&#243;wka"
  ]
  node [
    id 412
    label "sala_chorych"
  ]
  node [
    id 413
    label "&#322;&#243;&#380;eczko_nadziei"
  ]
  node [
    id 414
    label "szpitalnictwo"
  ]
  node [
    id 415
    label "osoba_prawna"
  ]
  node [
    id 416
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 417
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 418
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 419
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 420
    label "biuro"
  ]
  node [
    id 421
    label "organizacja"
  ]
  node [
    id 422
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 423
    label "Fundusze_Unijne"
  ]
  node [
    id 424
    label "zamyka&#263;"
  ]
  node [
    id 425
    label "establishment"
  ]
  node [
    id 426
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 427
    label "urz&#261;d"
  ]
  node [
    id 428
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 429
    label "afiliowa&#263;"
  ]
  node [
    id 430
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 431
    label "standard"
  ]
  node [
    id 432
    label "zamykanie"
  ]
  node [
    id 433
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 434
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 435
    label "gabinet"
  ]
  node [
    id 436
    label "zesp&#243;&#322;"
  ]
  node [
    id 437
    label "dzia&#322;"
  ]
  node [
    id 438
    label "system"
  ]
  node [
    id 439
    label "lias"
  ]
  node [
    id 440
    label "jednostka"
  ]
  node [
    id 441
    label "pi&#281;tro"
  ]
  node [
    id 442
    label "klasa"
  ]
  node [
    id 443
    label "jednostka_geologiczna"
  ]
  node [
    id 444
    label "filia"
  ]
  node [
    id 445
    label "malm"
  ]
  node [
    id 446
    label "whole"
  ]
  node [
    id 447
    label "dogger"
  ]
  node [
    id 448
    label "poziom"
  ]
  node [
    id 449
    label "promocja"
  ]
  node [
    id 450
    label "kurs"
  ]
  node [
    id 451
    label "bank"
  ]
  node [
    id 452
    label "formacja"
  ]
  node [
    id 453
    label "ajencja"
  ]
  node [
    id 454
    label "siedziba"
  ]
  node [
    id 455
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 456
    label "agencja"
  ]
  node [
    id 457
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 458
    label "trupiarnia"
  ]
  node [
    id 459
    label "opieka_medyczna"
  ]
  node [
    id 460
    label "lekarz"
  ]
  node [
    id 461
    label "dzieci&#281;co"
  ]
  node [
    id 462
    label "dzieci&#324;ski"
  ]
  node [
    id 463
    label "pocz&#261;tkowy"
  ]
  node [
    id 464
    label "typowy"
  ]
  node [
    id 465
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 466
    label "zwyczajny"
  ]
  node [
    id 467
    label "typowo"
  ]
  node [
    id 468
    label "cz&#281;sty"
  ]
  node [
    id 469
    label "zwyk&#322;y"
  ]
  node [
    id 470
    label "podstawowy"
  ]
  node [
    id 471
    label "pierwszy"
  ]
  node [
    id 472
    label "elementarny"
  ]
  node [
    id 473
    label "pocz&#261;tkowo"
  ]
  node [
    id 474
    label "GKS"
  ]
  node [
    id 475
    label "Katowice"
  ]
  node [
    id 476
    label "Chorz&#243;w"
  ]
  node [
    id 477
    label "kibol"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 474
  ]
  edge [
    source 0
    target 475
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
  edge [
    source 3
    target 341
  ]
  edge [
    source 3
    target 342
  ]
  edge [
    source 3
    target 343
  ]
  edge [
    source 3
    target 344
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 345
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 346
  ]
  edge [
    source 3
    target 347
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 348
  ]
  edge [
    source 3
    target 349
  ]
  edge [
    source 3
    target 350
  ]
  edge [
    source 3
    target 351
  ]
  edge [
    source 3
    target 352
  ]
  edge [
    source 3
    target 353
  ]
  edge [
    source 3
    target 354
  ]
  edge [
    source 3
    target 355
  ]
  edge [
    source 3
    target 356
  ]
  edge [
    source 3
    target 357
  ]
  edge [
    source 3
    target 358
  ]
  edge [
    source 3
    target 359
  ]
  edge [
    source 3
    target 360
  ]
  edge [
    source 3
    target 361
  ]
  edge [
    source 3
    target 362
  ]
  edge [
    source 3
    target 363
  ]
  edge [
    source 3
    target 364
  ]
  edge [
    source 3
    target 365
  ]
  edge [
    source 3
    target 366
  ]
  edge [
    source 3
    target 367
  ]
  edge [
    source 3
    target 368
  ]
  edge [
    source 3
    target 369
  ]
  edge [
    source 3
    target 370
  ]
  edge [
    source 3
    target 371
  ]
  edge [
    source 3
    target 372
  ]
  edge [
    source 3
    target 373
  ]
  edge [
    source 3
    target 374
  ]
  edge [
    source 3
    target 375
  ]
  edge [
    source 3
    target 376
  ]
  edge [
    source 3
    target 377
  ]
  edge [
    source 3
    target 378
  ]
  edge [
    source 3
    target 379
  ]
  edge [
    source 3
    target 380
  ]
  edge [
    source 3
    target 381
  ]
  edge [
    source 3
    target 382
  ]
  edge [
    source 3
    target 383
  ]
  edge [
    source 3
    target 384
  ]
  edge [
    source 3
    target 385
  ]
  edge [
    source 3
    target 386
  ]
  edge [
    source 3
    target 387
  ]
  edge [
    source 3
    target 388
  ]
  edge [
    source 3
    target 476
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 462
  ]
  edge [
    source 7
    target 463
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 474
    target 475
  ]
  edge [
    source 474
    target 477
  ]
  edge [
    source 475
    target 477
  ]
]
