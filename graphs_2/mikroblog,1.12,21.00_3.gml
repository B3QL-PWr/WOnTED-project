graph [
  node [
    id 0
    label "podrywajzwykopem"
    origin "text"
  ]
  node [
    id 1
    label "logikaniebieskichpaskow"
    origin "text"
  ]
  node [
    id 2
    label "zwiazki"
    origin "text"
  ]
  node [
    id 3
    label "dopiero"
    origin "text"
  ]
  node [
    id 4
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 5
    label "dotrze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "odjeba&#263;by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "chyba"
    origin "text"
  ]
  node [
    id 8
    label "nie"
    origin "text"
  ]
  node [
    id 9
    label "dobrze"
    origin "text"
  ]
  node [
    id 10
    label "doba"
  ]
  node [
    id 11
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 12
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 13
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 14
    label "dzi&#347;"
  ]
  node [
    id 15
    label "teraz"
  ]
  node [
    id 16
    label "czas"
  ]
  node [
    id 17
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 18
    label "jednocze&#347;nie"
  ]
  node [
    id 19
    label "tydzie&#324;"
  ]
  node [
    id 20
    label "noc"
  ]
  node [
    id 21
    label "dzie&#324;"
  ]
  node [
    id 22
    label "godzina"
  ]
  node [
    id 23
    label "long_time"
  ]
  node [
    id 24
    label "jednostka_geologiczna"
  ]
  node [
    id 25
    label "utrze&#263;"
  ]
  node [
    id 26
    label "znale&#378;&#263;"
  ]
  node [
    id 27
    label "skontaktowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "silnik"
  ]
  node [
    id 29
    label "catch"
  ]
  node [
    id 30
    label "dopasowa&#263;"
  ]
  node [
    id 31
    label "advance"
  ]
  node [
    id 32
    label "get"
  ]
  node [
    id 33
    label "spowodowa&#263;"
  ]
  node [
    id 34
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 35
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 36
    label "wyg&#322;adzi&#263;"
  ]
  node [
    id 37
    label "dorobi&#263;"
  ]
  node [
    id 38
    label "become"
  ]
  node [
    id 39
    label "zarobi&#263;"
  ]
  node [
    id 40
    label "doda&#263;"
  ]
  node [
    id 41
    label "do&#322;o&#380;y&#263;"
  ]
  node [
    id 42
    label "zrobi&#263;"
  ]
  node [
    id 43
    label "wytworzy&#263;"
  ]
  node [
    id 44
    label "wymy&#347;li&#263;"
  ]
  node [
    id 45
    label "grate"
  ]
  node [
    id 46
    label "rozdrobni&#263;"
  ]
  node [
    id 47
    label "dostosowa&#263;"
  ]
  node [
    id 48
    label "przeszy&#263;"
  ]
  node [
    id 49
    label "scali&#263;"
  ]
  node [
    id 50
    label "zw&#281;zi&#263;"
  ]
  node [
    id 51
    label "accommodate"
  ]
  node [
    id 52
    label "match"
  ]
  node [
    id 53
    label "adjust"
  ]
  node [
    id 54
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 55
    label "act"
  ]
  node [
    id 56
    label "pozyska&#263;"
  ]
  node [
    id 57
    label "oceni&#263;"
  ]
  node [
    id 58
    label "devise"
  ]
  node [
    id 59
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 60
    label "dozna&#263;"
  ]
  node [
    id 61
    label "wykry&#263;"
  ]
  node [
    id 62
    label "odzyska&#263;"
  ]
  node [
    id 63
    label "znaj&#347;&#263;"
  ]
  node [
    id 64
    label "invent"
  ]
  node [
    id 65
    label "wyr&#243;wna&#263;"
  ]
  node [
    id 66
    label "udoskonali&#263;"
  ]
  node [
    id 67
    label "evening"
  ]
  node [
    id 68
    label "biblioteka"
  ]
  node [
    id 69
    label "wyci&#261;garka"
  ]
  node [
    id 70
    label "gondola_silnikowa"
  ]
  node [
    id 71
    label "aerosanie"
  ]
  node [
    id 72
    label "rz&#281;&#380;enie"
  ]
  node [
    id 73
    label "podgrzewacz"
  ]
  node [
    id 74
    label "motogodzina"
  ]
  node [
    id 75
    label "motoszybowiec"
  ]
  node [
    id 76
    label "program"
  ]
  node [
    id 77
    label "gniazdo_zaworowe"
  ]
  node [
    id 78
    label "mechanizm"
  ]
  node [
    id 79
    label "uk&#322;ad_zasilania"
  ]
  node [
    id 80
    label "dociera&#263;"
  ]
  node [
    id 81
    label "samoch&#243;d"
  ]
  node [
    id 82
    label "dotarcie"
  ]
  node [
    id 83
    label "nap&#281;d"
  ]
  node [
    id 84
    label "motor&#243;wka"
  ]
  node [
    id 85
    label "rz&#281;zi&#263;"
  ]
  node [
    id 86
    label "perpetuum_mobile"
  ]
  node [
    id 87
    label "docieranie"
  ]
  node [
    id 88
    label "bombowiec"
  ]
  node [
    id 89
    label "radiator"
  ]
  node [
    id 90
    label "sprzeciw"
  ]
  node [
    id 91
    label "czerwona_kartka"
  ]
  node [
    id 92
    label "protestacja"
  ]
  node [
    id 93
    label "reakcja"
  ]
  node [
    id 94
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 95
    label "odpowiednio"
  ]
  node [
    id 96
    label "dobroczynnie"
  ]
  node [
    id 97
    label "moralnie"
  ]
  node [
    id 98
    label "korzystnie"
  ]
  node [
    id 99
    label "pozytywnie"
  ]
  node [
    id 100
    label "lepiej"
  ]
  node [
    id 101
    label "wiele"
  ]
  node [
    id 102
    label "skutecznie"
  ]
  node [
    id 103
    label "pomy&#347;lnie"
  ]
  node [
    id 104
    label "dobry"
  ]
  node [
    id 105
    label "charakterystycznie"
  ]
  node [
    id 106
    label "nale&#380;nie"
  ]
  node [
    id 107
    label "stosowny"
  ]
  node [
    id 108
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 109
    label "nale&#380;ycie"
  ]
  node [
    id 110
    label "prawdziwie"
  ]
  node [
    id 111
    label "auspiciously"
  ]
  node [
    id 112
    label "pomy&#347;lny"
  ]
  node [
    id 113
    label "moralny"
  ]
  node [
    id 114
    label "etyczny"
  ]
  node [
    id 115
    label "skuteczny"
  ]
  node [
    id 116
    label "wiela"
  ]
  node [
    id 117
    label "du&#380;y"
  ]
  node [
    id 118
    label "utylitarnie"
  ]
  node [
    id 119
    label "korzystny"
  ]
  node [
    id 120
    label "beneficially"
  ]
  node [
    id 121
    label "przyjemnie"
  ]
  node [
    id 122
    label "pozytywny"
  ]
  node [
    id 123
    label "ontologicznie"
  ]
  node [
    id 124
    label "dodatni"
  ]
  node [
    id 125
    label "odpowiedni"
  ]
  node [
    id 126
    label "wiersz"
  ]
  node [
    id 127
    label "dobroczynny"
  ]
  node [
    id 128
    label "czw&#243;rka"
  ]
  node [
    id 129
    label "spokojny"
  ]
  node [
    id 130
    label "&#347;mieszny"
  ]
  node [
    id 131
    label "mi&#322;y"
  ]
  node [
    id 132
    label "grzeczny"
  ]
  node [
    id 133
    label "powitanie"
  ]
  node [
    id 134
    label "ca&#322;y"
  ]
  node [
    id 135
    label "zwrot"
  ]
  node [
    id 136
    label "drogi"
  ]
  node [
    id 137
    label "pos&#322;uszny"
  ]
  node [
    id 138
    label "philanthropically"
  ]
  node [
    id 139
    label "spo&#322;ecznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
]
