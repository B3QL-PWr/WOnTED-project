graph [
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "fansuberzy"
    origin "text"
  ]
  node [
    id 2
    label "anime"
    origin "text"
  ]
  node [
    id 3
    label "anna"
    origin "text"
  ]
  node [
    id 4
    label "koralewsk&#261;"
    origin "text"
  ]
  node [
    id 5
    label "temat"
    origin "text"
  ]
  node [
    id 6
    label "fan"
    origin "text"
  ]
  node [
    id 7
    label "t&#322;umacz"
    origin "text"
  ]
  node [
    id 8
    label "przedmiot"
  ]
  node [
    id 9
    label "Polish"
  ]
  node [
    id 10
    label "goniony"
  ]
  node [
    id 11
    label "oberek"
  ]
  node [
    id 12
    label "ryba_po_grecku"
  ]
  node [
    id 13
    label "sztajer"
  ]
  node [
    id 14
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 15
    label "krakowiak"
  ]
  node [
    id 16
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 17
    label "pierogi_ruskie"
  ]
  node [
    id 18
    label "lacki"
  ]
  node [
    id 19
    label "polak"
  ]
  node [
    id 20
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 21
    label "chodzony"
  ]
  node [
    id 22
    label "po_polsku"
  ]
  node [
    id 23
    label "mazur"
  ]
  node [
    id 24
    label "polsko"
  ]
  node [
    id 25
    label "skoczny"
  ]
  node [
    id 26
    label "drabant"
  ]
  node [
    id 27
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 28
    label "j&#281;zyk"
  ]
  node [
    id 29
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 30
    label "artykulator"
  ]
  node [
    id 31
    label "kod"
  ]
  node [
    id 32
    label "kawa&#322;ek"
  ]
  node [
    id 33
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 34
    label "gramatyka"
  ]
  node [
    id 35
    label "stylik"
  ]
  node [
    id 36
    label "przet&#322;umaczenie"
  ]
  node [
    id 37
    label "formalizowanie"
  ]
  node [
    id 38
    label "ssa&#263;"
  ]
  node [
    id 39
    label "ssanie"
  ]
  node [
    id 40
    label "language"
  ]
  node [
    id 41
    label "liza&#263;"
  ]
  node [
    id 42
    label "napisa&#263;"
  ]
  node [
    id 43
    label "konsonantyzm"
  ]
  node [
    id 44
    label "wokalizm"
  ]
  node [
    id 45
    label "pisa&#263;"
  ]
  node [
    id 46
    label "fonetyka"
  ]
  node [
    id 47
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 48
    label "jeniec"
  ]
  node [
    id 49
    label "but"
  ]
  node [
    id 50
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 51
    label "po_koroniarsku"
  ]
  node [
    id 52
    label "kultura_duchowa"
  ]
  node [
    id 53
    label "t&#322;umaczenie"
  ]
  node [
    id 54
    label "m&#243;wienie"
  ]
  node [
    id 55
    label "pype&#263;"
  ]
  node [
    id 56
    label "lizanie"
  ]
  node [
    id 57
    label "pismo"
  ]
  node [
    id 58
    label "formalizowa&#263;"
  ]
  node [
    id 59
    label "rozumie&#263;"
  ]
  node [
    id 60
    label "organ"
  ]
  node [
    id 61
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 62
    label "rozumienie"
  ]
  node [
    id 63
    label "spos&#243;b"
  ]
  node [
    id 64
    label "makroglosja"
  ]
  node [
    id 65
    label "m&#243;wi&#263;"
  ]
  node [
    id 66
    label "jama_ustna"
  ]
  node [
    id 67
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 68
    label "formacja_geologiczna"
  ]
  node [
    id 69
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 70
    label "natural_language"
  ]
  node [
    id 71
    label "s&#322;ownictwo"
  ]
  node [
    id 72
    label "urz&#261;dzenie"
  ]
  node [
    id 73
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 74
    label "wschodnioeuropejski"
  ]
  node [
    id 75
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 76
    label "poga&#324;ski"
  ]
  node [
    id 77
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 78
    label "topielec"
  ]
  node [
    id 79
    label "europejski"
  ]
  node [
    id 80
    label "po_&#347;rodkowoeuropejsku"
  ]
  node [
    id 81
    label "langosz"
  ]
  node [
    id 82
    label "zboczenie"
  ]
  node [
    id 83
    label "om&#243;wienie"
  ]
  node [
    id 84
    label "sponiewieranie"
  ]
  node [
    id 85
    label "discipline"
  ]
  node [
    id 86
    label "rzecz"
  ]
  node [
    id 87
    label "omawia&#263;"
  ]
  node [
    id 88
    label "kr&#261;&#380;enie"
  ]
  node [
    id 89
    label "tre&#347;&#263;"
  ]
  node [
    id 90
    label "robienie"
  ]
  node [
    id 91
    label "sponiewiera&#263;"
  ]
  node [
    id 92
    label "element"
  ]
  node [
    id 93
    label "entity"
  ]
  node [
    id 94
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 95
    label "tematyka"
  ]
  node [
    id 96
    label "w&#261;tek"
  ]
  node [
    id 97
    label "charakter"
  ]
  node [
    id 98
    label "zbaczanie"
  ]
  node [
    id 99
    label "program_nauczania"
  ]
  node [
    id 100
    label "om&#243;wi&#263;"
  ]
  node [
    id 101
    label "omawianie"
  ]
  node [
    id 102
    label "thing"
  ]
  node [
    id 103
    label "kultura"
  ]
  node [
    id 104
    label "istota"
  ]
  node [
    id 105
    label "zbacza&#263;"
  ]
  node [
    id 106
    label "zboczy&#263;"
  ]
  node [
    id 107
    label "gwardzista"
  ]
  node [
    id 108
    label "melodia"
  ]
  node [
    id 109
    label "taniec"
  ]
  node [
    id 110
    label "taniec_ludowy"
  ]
  node [
    id 111
    label "&#347;redniowieczny"
  ]
  node [
    id 112
    label "specjalny"
  ]
  node [
    id 113
    label "&#380;ywio&#322;owy"
  ]
  node [
    id 114
    label "weso&#322;y"
  ]
  node [
    id 115
    label "sprawny"
  ]
  node [
    id 116
    label "rytmiczny"
  ]
  node [
    id 117
    label "skocznie"
  ]
  node [
    id 118
    label "energiczny"
  ]
  node [
    id 119
    label "lendler"
  ]
  node [
    id 120
    label "austriacki"
  ]
  node [
    id 121
    label "polka"
  ]
  node [
    id 122
    label "europejsko"
  ]
  node [
    id 123
    label "przytup"
  ]
  node [
    id 124
    label "ho&#322;ubiec"
  ]
  node [
    id 125
    label "wodzi&#263;"
  ]
  node [
    id 126
    label "ludowy"
  ]
  node [
    id 127
    label "pie&#347;&#324;"
  ]
  node [
    id 128
    label "mieszkaniec"
  ]
  node [
    id 129
    label "centu&#347;"
  ]
  node [
    id 130
    label "lalka"
  ]
  node [
    id 131
    label "Ma&#322;opolanin"
  ]
  node [
    id 132
    label "krakauer"
  ]
  node [
    id 133
    label "otaku"
  ]
  node [
    id 134
    label "film"
  ]
  node [
    id 135
    label "Cowboy_Bebop"
  ]
  node [
    id 136
    label "animatronika"
  ]
  node [
    id 137
    label "odczulenie"
  ]
  node [
    id 138
    label "odczula&#263;"
  ]
  node [
    id 139
    label "blik"
  ]
  node [
    id 140
    label "odczuli&#263;"
  ]
  node [
    id 141
    label "scena"
  ]
  node [
    id 142
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 143
    label "muza"
  ]
  node [
    id 144
    label "postprodukcja"
  ]
  node [
    id 145
    label "block"
  ]
  node [
    id 146
    label "trawiarnia"
  ]
  node [
    id 147
    label "sklejarka"
  ]
  node [
    id 148
    label "sztuka"
  ]
  node [
    id 149
    label "uj&#281;cie"
  ]
  node [
    id 150
    label "filmoteka"
  ]
  node [
    id 151
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 152
    label "klatka"
  ]
  node [
    id 153
    label "rozbieg&#243;wka"
  ]
  node [
    id 154
    label "napisy"
  ]
  node [
    id 155
    label "ta&#347;ma"
  ]
  node [
    id 156
    label "odczulanie"
  ]
  node [
    id 157
    label "anamorfoza"
  ]
  node [
    id 158
    label "dorobek"
  ]
  node [
    id 159
    label "ty&#322;&#243;wka"
  ]
  node [
    id 160
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 161
    label "b&#322;ona"
  ]
  node [
    id 162
    label "emulsja_fotograficzna"
  ]
  node [
    id 163
    label "photograph"
  ]
  node [
    id 164
    label "czo&#322;&#243;wka"
  ]
  node [
    id 165
    label "rola"
  ]
  node [
    id 166
    label "manga"
  ]
  node [
    id 167
    label "sprawa"
  ]
  node [
    id 168
    label "wyraz_pochodny"
  ]
  node [
    id 169
    label "cecha"
  ]
  node [
    id 170
    label "fraza"
  ]
  node [
    id 171
    label "forum"
  ]
  node [
    id 172
    label "topik"
  ]
  node [
    id 173
    label "forma"
  ]
  node [
    id 174
    label "otoczka"
  ]
  node [
    id 175
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 176
    label "zbi&#243;r"
  ]
  node [
    id 177
    label "wypowiedzenie"
  ]
  node [
    id 178
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 179
    label "zdanie"
  ]
  node [
    id 180
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 181
    label "motyw"
  ]
  node [
    id 182
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 183
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 184
    label "informacja"
  ]
  node [
    id 185
    label "zawarto&#347;&#263;"
  ]
  node [
    id 186
    label "kszta&#322;t"
  ]
  node [
    id 187
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 188
    label "jednostka_systematyczna"
  ]
  node [
    id 189
    label "poznanie"
  ]
  node [
    id 190
    label "leksem"
  ]
  node [
    id 191
    label "dzie&#322;o"
  ]
  node [
    id 192
    label "stan"
  ]
  node [
    id 193
    label "blaszka"
  ]
  node [
    id 194
    label "poj&#281;cie"
  ]
  node [
    id 195
    label "kantyzm"
  ]
  node [
    id 196
    label "zdolno&#347;&#263;"
  ]
  node [
    id 197
    label "do&#322;ek"
  ]
  node [
    id 198
    label "gwiazda"
  ]
  node [
    id 199
    label "formality"
  ]
  node [
    id 200
    label "struktura"
  ]
  node [
    id 201
    label "wygl&#261;d"
  ]
  node [
    id 202
    label "mode"
  ]
  node [
    id 203
    label "morfem"
  ]
  node [
    id 204
    label "rdze&#324;"
  ]
  node [
    id 205
    label "posta&#263;"
  ]
  node [
    id 206
    label "kielich"
  ]
  node [
    id 207
    label "ornamentyka"
  ]
  node [
    id 208
    label "pasmo"
  ]
  node [
    id 209
    label "zwyczaj"
  ]
  node [
    id 210
    label "punkt_widzenia"
  ]
  node [
    id 211
    label "g&#322;owa"
  ]
  node [
    id 212
    label "naczynie"
  ]
  node [
    id 213
    label "p&#322;at"
  ]
  node [
    id 214
    label "maszyna_drukarska"
  ]
  node [
    id 215
    label "obiekt"
  ]
  node [
    id 216
    label "style"
  ]
  node [
    id 217
    label "linearno&#347;&#263;"
  ]
  node [
    id 218
    label "wyra&#380;enie"
  ]
  node [
    id 219
    label "formacja"
  ]
  node [
    id 220
    label "spirala"
  ]
  node [
    id 221
    label "dyspozycja"
  ]
  node [
    id 222
    label "odmiana"
  ]
  node [
    id 223
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 224
    label "wz&#243;r"
  ]
  node [
    id 225
    label "October"
  ]
  node [
    id 226
    label "creation"
  ]
  node [
    id 227
    label "p&#281;tla"
  ]
  node [
    id 228
    label "arystotelizm"
  ]
  node [
    id 229
    label "szablon"
  ]
  node [
    id 230
    label "miniatura"
  ]
  node [
    id 231
    label "zanucenie"
  ]
  node [
    id 232
    label "nuta"
  ]
  node [
    id 233
    label "zakosztowa&#263;"
  ]
  node [
    id 234
    label "zajawka"
  ]
  node [
    id 235
    label "zanuci&#263;"
  ]
  node [
    id 236
    label "emocja"
  ]
  node [
    id 237
    label "oskoma"
  ]
  node [
    id 238
    label "melika"
  ]
  node [
    id 239
    label "nucenie"
  ]
  node [
    id 240
    label "nuci&#263;"
  ]
  node [
    id 241
    label "brzmienie"
  ]
  node [
    id 242
    label "zjawisko"
  ]
  node [
    id 243
    label "taste"
  ]
  node [
    id 244
    label "muzyka"
  ]
  node [
    id 245
    label "inclination"
  ]
  node [
    id 246
    label "charakterystyka"
  ]
  node [
    id 247
    label "m&#322;ot"
  ]
  node [
    id 248
    label "znak"
  ]
  node [
    id 249
    label "drzewo"
  ]
  node [
    id 250
    label "pr&#243;ba"
  ]
  node [
    id 251
    label "attribute"
  ]
  node [
    id 252
    label "marka"
  ]
  node [
    id 253
    label "matter"
  ]
  node [
    id 254
    label "splot"
  ]
  node [
    id 255
    label "wytw&#243;r"
  ]
  node [
    id 256
    label "ceg&#322;a"
  ]
  node [
    id 257
    label "socket"
  ]
  node [
    id 258
    label "rozmieszczenie"
  ]
  node [
    id 259
    label "fabu&#322;a"
  ]
  node [
    id 260
    label "mentalno&#347;&#263;"
  ]
  node [
    id 261
    label "superego"
  ]
  node [
    id 262
    label "psychika"
  ]
  node [
    id 263
    label "znaczenie"
  ]
  node [
    id 264
    label "wn&#281;trze"
  ]
  node [
    id 265
    label "okrywa"
  ]
  node [
    id 266
    label "kontekst"
  ]
  node [
    id 267
    label "object"
  ]
  node [
    id 268
    label "wpadni&#281;cie"
  ]
  node [
    id 269
    label "mienie"
  ]
  node [
    id 270
    label "przyroda"
  ]
  node [
    id 271
    label "wpa&#347;&#263;"
  ]
  node [
    id 272
    label "wpadanie"
  ]
  node [
    id 273
    label "wpada&#263;"
  ]
  node [
    id 274
    label "discussion"
  ]
  node [
    id 275
    label "rozpatrywanie"
  ]
  node [
    id 276
    label "dyskutowanie"
  ]
  node [
    id 277
    label "swerve"
  ]
  node [
    id 278
    label "kierunek"
  ]
  node [
    id 279
    label "digress"
  ]
  node [
    id 280
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 281
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 282
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 283
    label "odchodzi&#263;"
  ]
  node [
    id 284
    label "twist"
  ]
  node [
    id 285
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 286
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 287
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 288
    label "omowny"
  ]
  node [
    id 289
    label "figura_stylistyczna"
  ]
  node [
    id 290
    label "sformu&#322;owanie"
  ]
  node [
    id 291
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 292
    label "odchodzenie"
  ]
  node [
    id 293
    label "aberrance"
  ]
  node [
    id 294
    label "dyskutowa&#263;"
  ]
  node [
    id 295
    label "formu&#322;owa&#263;"
  ]
  node [
    id 296
    label "discourse"
  ]
  node [
    id 297
    label "perversion"
  ]
  node [
    id 298
    label "death"
  ]
  node [
    id 299
    label "odej&#347;cie"
  ]
  node [
    id 300
    label "turn"
  ]
  node [
    id 301
    label "k&#261;t"
  ]
  node [
    id 302
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 303
    label "odchylenie_si&#281;"
  ]
  node [
    id 304
    label "deviation"
  ]
  node [
    id 305
    label "patologia"
  ]
  node [
    id 306
    label "przedyskutowa&#263;"
  ]
  node [
    id 307
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 308
    label "publicize"
  ]
  node [
    id 309
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 310
    label "distract"
  ]
  node [
    id 311
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 312
    label "odej&#347;&#263;"
  ]
  node [
    id 313
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 314
    label "zmieni&#263;"
  ]
  node [
    id 315
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 316
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 317
    label "kognicja"
  ]
  node [
    id 318
    label "rozprawa"
  ]
  node [
    id 319
    label "wydarzenie"
  ]
  node [
    id 320
    label "szczeg&#243;&#322;"
  ]
  node [
    id 321
    label "proposition"
  ]
  node [
    id 322
    label "przes&#322;anka"
  ]
  node [
    id 323
    label "idea"
  ]
  node [
    id 324
    label "paj&#261;k"
  ]
  node [
    id 325
    label "przewodnik"
  ]
  node [
    id 326
    label "odcinek"
  ]
  node [
    id 327
    label "topikowate"
  ]
  node [
    id 328
    label "grupa_dyskusyjna"
  ]
  node [
    id 329
    label "s&#261;d"
  ]
  node [
    id 330
    label "plac"
  ]
  node [
    id 331
    label "bazylika"
  ]
  node [
    id 332
    label "przestrze&#324;"
  ]
  node [
    id 333
    label "miejsce"
  ]
  node [
    id 334
    label "portal"
  ]
  node [
    id 335
    label "konferencja"
  ]
  node [
    id 336
    label "agora"
  ]
  node [
    id 337
    label "grupa"
  ]
  node [
    id 338
    label "strona"
  ]
  node [
    id 339
    label "fan_club"
  ]
  node [
    id 340
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 341
    label "fandom"
  ]
  node [
    id 342
    label "sympatyk"
  ]
  node [
    id 343
    label "entuzjasta"
  ]
  node [
    id 344
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 345
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 346
    label "Jakub_Wujek"
  ]
  node [
    id 347
    label "Jan_Czeczot"
  ]
  node [
    id 348
    label "przek&#322;adacz"
  ]
  node [
    id 349
    label "przek&#322;adowca"
  ]
  node [
    id 350
    label "aplikacja"
  ]
  node [
    id 351
    label "interpretator"
  ]
  node [
    id 352
    label "interpreter"
  ]
  node [
    id 353
    label "uczony"
  ]
  node [
    id 354
    label "program"
  ]
  node [
    id 355
    label "autor"
  ]
  node [
    id 356
    label "aktor"
  ]
  node [
    id 357
    label "artysta"
  ]
  node [
    id 358
    label "application"
  ]
  node [
    id 359
    label "naszycie"
  ]
  node [
    id 360
    label "applique"
  ]
  node [
    id 361
    label "praktyka"
  ]
  node [
    id 362
    label "ozdoba"
  ]
  node [
    id 363
    label "cz&#322;owiek"
  ]
  node [
    id 364
    label "ksi&#261;&#380;ka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
]
