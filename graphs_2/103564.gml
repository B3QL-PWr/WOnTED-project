graph [
  node [
    id 0
    label "aktor"
    origin "text"
  ]
  node [
    id 1
    label "uczestnik"
  ]
  node [
    id 2
    label "Eastwood"
  ]
  node [
    id 3
    label "fanfaron"
  ]
  node [
    id 4
    label "odtw&#243;rca"
  ]
  node [
    id 5
    label "obsada"
  ]
  node [
    id 6
    label "Allen"
  ]
  node [
    id 7
    label "wykonawca"
  ]
  node [
    id 8
    label "Roland_Topor"
  ]
  node [
    id 9
    label "Stuhr"
  ]
  node [
    id 10
    label "pracownik"
  ]
  node [
    id 11
    label "bajerant"
  ]
  node [
    id 12
    label "podmiot"
  ]
  node [
    id 13
    label "teatr"
  ]
  node [
    id 14
    label "interpretator"
  ]
  node [
    id 15
    label "Daniel_Olbrychski"
  ]
  node [
    id 16
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 17
    label "antyteatr"
  ]
  node [
    id 18
    label "przedstawienie"
  ]
  node [
    id 19
    label "literatura"
  ]
  node [
    id 20
    label "sala"
  ]
  node [
    id 21
    label "gra"
  ]
  node [
    id 22
    label "budynek"
  ]
  node [
    id 23
    label "deski"
  ]
  node [
    id 24
    label "teren"
  ]
  node [
    id 25
    label "sztuka"
  ]
  node [
    id 26
    label "przedstawia&#263;"
  ]
  node [
    id 27
    label "play"
  ]
  node [
    id 28
    label "widzownia"
  ]
  node [
    id 29
    label "modelatornia"
  ]
  node [
    id 30
    label "dekoratornia"
  ]
  node [
    id 31
    label "instytucja"
  ]
  node [
    id 32
    label "przedstawianie"
  ]
  node [
    id 33
    label "persona&#322;"
  ]
  node [
    id 34
    label "zbi&#243;r"
  ]
  node [
    id 35
    label "appointment"
  ]
  node [
    id 36
    label "fitting"
  ]
  node [
    id 37
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 38
    label "force"
  ]
  node [
    id 39
    label "zesp&#243;&#322;"
  ]
  node [
    id 40
    label "czynno&#347;&#263;"
  ]
  node [
    id 41
    label "cz&#322;owiek"
  ]
  node [
    id 42
    label "artysta"
  ]
  node [
    id 43
    label "podmiot_gospodarczy"
  ]
  node [
    id 44
    label "muzyk"
  ]
  node [
    id 45
    label "uczony"
  ]
  node [
    id 46
    label "autor"
  ]
  node [
    id 47
    label "program"
  ]
  node [
    id 48
    label "interpreter"
  ]
  node [
    id 49
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 50
    label "delegowa&#263;"
  ]
  node [
    id 51
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 52
    label "salariat"
  ]
  node [
    id 53
    label "pracu&#347;"
  ]
  node [
    id 54
    label "r&#281;ka"
  ]
  node [
    id 55
    label "delegowanie"
  ]
  node [
    id 56
    label "kontynuator"
  ]
  node [
    id 57
    label "udawacz"
  ]
  node [
    id 58
    label "pisarz"
  ]
  node [
    id 59
    label "obserwator"
  ]
  node [
    id 60
    label "byt"
  ]
  node [
    id 61
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 62
    label "nauka_prawa"
  ]
  node [
    id 63
    label "prawo"
  ]
  node [
    id 64
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 65
    label "organizacja"
  ]
  node [
    id 66
    label "osobowo&#347;&#263;"
  ]
  node [
    id 67
    label "nad&#281;ty"
  ]
  node [
    id 68
    label "pysza&#322;ek"
  ]
  node [
    id 69
    label "oszust"
  ]
  node [
    id 70
    label "farmazon"
  ]
  node [
    id 71
    label "kombinator"
  ]
  node [
    id 72
    label "kinematografia"
  ]
  node [
    id 73
    label "komedia"
  ]
  node [
    id 74
    label "western"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
]
