graph [
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "wikipedia"
    origin "text"
  ]
  node [
    id 2
    label "dodanie"
    origin "text"
  ]
  node [
    id 3
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 4
    label "miejscowo&#347;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "konrad"
    origin "text"
  ]
  node [
    id 6
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 8
    label "tysi&#261;c"
    origin "text"
  ]
  node [
    id 9
    label "has&#322;o"
    origin "text"
  ]
  node [
    id 10
    label "addition"
  ]
  node [
    id 11
    label "do&#322;&#261;czenie"
  ]
  node [
    id 12
    label "summation"
  ]
  node [
    id 13
    label "wym&#243;wienie"
  ]
  node [
    id 14
    label "rozdzielno&#347;&#263;_mno&#380;enia_wzgl&#281;dem_dodawania"
  ]
  node [
    id 15
    label "dokupienie"
  ]
  node [
    id 16
    label "dop&#322;acenie"
  ]
  node [
    id 17
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 18
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 19
    label "policzenie"
  ]
  node [
    id 20
    label "do&#347;wietlenie"
  ]
  node [
    id 21
    label "zap&#322;acenie"
  ]
  node [
    id 22
    label "o&#347;wietlenie"
  ]
  node [
    id 23
    label "kupienie"
  ]
  node [
    id 24
    label "czynno&#347;&#263;"
  ]
  node [
    id 25
    label "affiliation"
  ]
  node [
    id 26
    label "zrobienie"
  ]
  node [
    id 27
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 28
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 29
    label "rozwi&#261;zanie"
  ]
  node [
    id 30
    label "wydanie"
  ]
  node [
    id 31
    label "zastrze&#380;enie"
  ]
  node [
    id 32
    label "wydobycie"
  ]
  node [
    id 33
    label "zwerbalizowanie"
  ]
  node [
    id 34
    label "notification"
  ]
  node [
    id 35
    label "notice"
  ]
  node [
    id 36
    label "powiedzenie"
  ]
  node [
    id 37
    label "denunciation"
  ]
  node [
    id 38
    label "evaluation"
  ]
  node [
    id 39
    label "wyrachowanie"
  ]
  node [
    id 40
    label "ustalenie"
  ]
  node [
    id 41
    label "zakwalifikowanie"
  ]
  node [
    id 42
    label "wynagrodzenie"
  ]
  node [
    id 43
    label "wyznaczenie"
  ]
  node [
    id 44
    label "wycenienie"
  ]
  node [
    id 45
    label "wyj&#347;cie"
  ]
  node [
    id 46
    label "zbadanie"
  ]
  node [
    id 47
    label "sprowadzenie"
  ]
  node [
    id 48
    label "przeliczenie_si&#281;"
  ]
  node [
    id 49
    label "formacja"
  ]
  node [
    id 50
    label "armia"
  ]
  node [
    id 51
    label "coalescence"
  ]
  node [
    id 52
    label "bearing"
  ]
  node [
    id 53
    label "komunikacja"
  ]
  node [
    id 54
    label "cecha"
  ]
  node [
    id 55
    label "dodawanie"
  ]
  node [
    id 56
    label "mno&#380;enie"
  ]
  node [
    id 57
    label "kontakt"
  ]
  node [
    id 58
    label "blok"
  ]
  node [
    id 59
    label "prawda"
  ]
  node [
    id 60
    label "znak_j&#281;zykowy"
  ]
  node [
    id 61
    label "nag&#322;&#243;wek"
  ]
  node [
    id 62
    label "szkic"
  ]
  node [
    id 63
    label "line"
  ]
  node [
    id 64
    label "fragment"
  ]
  node [
    id 65
    label "tekst"
  ]
  node [
    id 66
    label "wyr&#243;b"
  ]
  node [
    id 67
    label "rodzajnik"
  ]
  node [
    id 68
    label "dokument"
  ]
  node [
    id 69
    label "towar"
  ]
  node [
    id 70
    label "paragraf"
  ]
  node [
    id 71
    label "ekscerpcja"
  ]
  node [
    id 72
    label "j&#281;zykowo"
  ]
  node [
    id 73
    label "wypowied&#378;"
  ]
  node [
    id 74
    label "redakcja"
  ]
  node [
    id 75
    label "wytw&#243;r"
  ]
  node [
    id 76
    label "pomini&#281;cie"
  ]
  node [
    id 77
    label "dzie&#322;o"
  ]
  node [
    id 78
    label "preparacja"
  ]
  node [
    id 79
    label "odmianka"
  ]
  node [
    id 80
    label "opu&#347;ci&#263;"
  ]
  node [
    id 81
    label "koniektura"
  ]
  node [
    id 82
    label "pisa&#263;"
  ]
  node [
    id 83
    label "obelga"
  ]
  node [
    id 84
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 85
    label "utw&#243;r"
  ]
  node [
    id 86
    label "s&#261;d"
  ]
  node [
    id 87
    label "za&#322;o&#380;enie"
  ]
  node [
    id 88
    label "nieprawdziwy"
  ]
  node [
    id 89
    label "prawdziwy"
  ]
  node [
    id 90
    label "truth"
  ]
  node [
    id 91
    label "realia"
  ]
  node [
    id 92
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 93
    label "sprzedawanie_si&#281;"
  ]
  node [
    id 94
    label "produkt"
  ]
  node [
    id 95
    label "creation"
  ]
  node [
    id 96
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 97
    label "p&#322;uczkarnia"
  ]
  node [
    id 98
    label "znakowarka"
  ]
  node [
    id 99
    label "produkcja"
  ]
  node [
    id 100
    label "tytu&#322;"
  ]
  node [
    id 101
    label "head"
  ]
  node [
    id 102
    label "znak_pisarski"
  ]
  node [
    id 103
    label "przepis"
  ]
  node [
    id 104
    label "bajt"
  ]
  node [
    id 105
    label "bloking"
  ]
  node [
    id 106
    label "j&#261;kanie"
  ]
  node [
    id 107
    label "przeszkoda"
  ]
  node [
    id 108
    label "zesp&#243;&#322;"
  ]
  node [
    id 109
    label "blokada"
  ]
  node [
    id 110
    label "bry&#322;a"
  ]
  node [
    id 111
    label "dzia&#322;"
  ]
  node [
    id 112
    label "kontynent"
  ]
  node [
    id 113
    label "nastawnia"
  ]
  node [
    id 114
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 115
    label "blockage"
  ]
  node [
    id 116
    label "zbi&#243;r"
  ]
  node [
    id 117
    label "block"
  ]
  node [
    id 118
    label "organizacja"
  ]
  node [
    id 119
    label "budynek"
  ]
  node [
    id 120
    label "start"
  ]
  node [
    id 121
    label "skorupa_ziemska"
  ]
  node [
    id 122
    label "program"
  ]
  node [
    id 123
    label "zeszyt"
  ]
  node [
    id 124
    label "grupa"
  ]
  node [
    id 125
    label "blokowisko"
  ]
  node [
    id 126
    label "barak"
  ]
  node [
    id 127
    label "stok_kontynentalny"
  ]
  node [
    id 128
    label "whole"
  ]
  node [
    id 129
    label "square"
  ]
  node [
    id 130
    label "siatk&#243;wka"
  ]
  node [
    id 131
    label "kr&#261;g"
  ]
  node [
    id 132
    label "ram&#243;wka"
  ]
  node [
    id 133
    label "zamek"
  ]
  node [
    id 134
    label "obrona"
  ]
  node [
    id 135
    label "ok&#322;adka"
  ]
  node [
    id 136
    label "bie&#380;nia"
  ]
  node [
    id 137
    label "referat"
  ]
  node [
    id 138
    label "dom_wielorodzinny"
  ]
  node [
    id 139
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 140
    label "zapis"
  ]
  node [
    id 141
    label "&#347;wiadectwo"
  ]
  node [
    id 142
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 143
    label "parafa"
  ]
  node [
    id 144
    label "plik"
  ]
  node [
    id 145
    label "raport&#243;wka"
  ]
  node [
    id 146
    label "record"
  ]
  node [
    id 147
    label "registratura"
  ]
  node [
    id 148
    label "dokumentacja"
  ]
  node [
    id 149
    label "fascyku&#322;"
  ]
  node [
    id 150
    label "writing"
  ]
  node [
    id 151
    label "sygnatariusz"
  ]
  node [
    id 152
    label "rysunek"
  ]
  node [
    id 153
    label "szkicownik"
  ]
  node [
    id 154
    label "opracowanie"
  ]
  node [
    id 155
    label "sketch"
  ]
  node [
    id 156
    label "plot"
  ]
  node [
    id 157
    label "pomys&#322;"
  ]
  node [
    id 158
    label "opowiadanie"
  ]
  node [
    id 159
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 160
    label "metka"
  ]
  node [
    id 161
    label "poci&#261;gaj&#261;cy"
  ]
  node [
    id 162
    label "cz&#322;owiek"
  ]
  node [
    id 163
    label "szprycowa&#263;"
  ]
  node [
    id 164
    label "naszprycowa&#263;"
  ]
  node [
    id 165
    label "rzuca&#263;"
  ]
  node [
    id 166
    label "tandeta"
  ]
  node [
    id 167
    label "obr&#243;t_handlowy"
  ]
  node [
    id 168
    label "&#347;rodek_psychoaktywny"
  ]
  node [
    id 169
    label "rzuci&#263;"
  ]
  node [
    id 170
    label "naszprycowanie"
  ]
  node [
    id 171
    label "tkanina"
  ]
  node [
    id 172
    label "szprycowanie"
  ]
  node [
    id 173
    label "za&#322;adownia"
  ]
  node [
    id 174
    label "asortyment"
  ]
  node [
    id 175
    label "&#322;&#243;dzki"
  ]
  node [
    id 176
    label "narkobiznes"
  ]
  node [
    id 177
    label "rzucenie"
  ]
  node [
    id 178
    label "rzucanie"
  ]
  node [
    id 179
    label "Aurignac"
  ]
  node [
    id 180
    label "Sabaudia"
  ]
  node [
    id 181
    label "Cecora"
  ]
  node [
    id 182
    label "Saint-Acheul"
  ]
  node [
    id 183
    label "Boulogne"
  ]
  node [
    id 184
    label "Opat&#243;wek"
  ]
  node [
    id 185
    label "osiedle"
  ]
  node [
    id 186
    label "Levallois-Perret"
  ]
  node [
    id 187
    label "Szcz&#281;&#347;liwice"
  ]
  node [
    id 188
    label "Jelcz"
  ]
  node [
    id 189
    label "Kaw&#281;czyn"
  ]
  node [
    id 190
    label "Br&#243;dno"
  ]
  node [
    id 191
    label "Marysin"
  ]
  node [
    id 192
    label "Ochock"
  ]
  node [
    id 193
    label "Kabaty"
  ]
  node [
    id 194
    label "Paw&#322;owice"
  ]
  node [
    id 195
    label "Falenica"
  ]
  node [
    id 196
    label "Osobowice"
  ]
  node [
    id 197
    label "Wielopole"
  ]
  node [
    id 198
    label "Boryszew"
  ]
  node [
    id 199
    label "Chojny"
  ]
  node [
    id 200
    label "Szack"
  ]
  node [
    id 201
    label "Powsin"
  ]
  node [
    id 202
    label "Bielice"
  ]
  node [
    id 203
    label "Wi&#347;niowiec"
  ]
  node [
    id 204
    label "Branice"
  ]
  node [
    id 205
    label "Rej&#243;w"
  ]
  node [
    id 206
    label "Zerze&#324;"
  ]
  node [
    id 207
    label "Rakowiec"
  ]
  node [
    id 208
    label "osadnictwo"
  ]
  node [
    id 209
    label "Jelonki"
  ]
  node [
    id 210
    label "Gronik"
  ]
  node [
    id 211
    label "Horodyszcze"
  ]
  node [
    id 212
    label "S&#281;polno"
  ]
  node [
    id 213
    label "Salwator"
  ]
  node [
    id 214
    label "Mariensztat"
  ]
  node [
    id 215
    label "Lubiesz&#243;w"
  ]
  node [
    id 216
    label "Izborsk"
  ]
  node [
    id 217
    label "Orunia"
  ]
  node [
    id 218
    label "Opor&#243;w"
  ]
  node [
    id 219
    label "Miedzeszyn"
  ]
  node [
    id 220
    label "Nadodrze"
  ]
  node [
    id 221
    label "Natolin"
  ]
  node [
    id 222
    label "Wi&#347;niewo"
  ]
  node [
    id 223
    label "Wojn&#243;w"
  ]
  node [
    id 224
    label "Ujazd&#243;w"
  ]
  node [
    id 225
    label "Solec"
  ]
  node [
    id 226
    label "Biskupin"
  ]
  node [
    id 227
    label "G&#243;rce"
  ]
  node [
    id 228
    label "Siersza"
  ]
  node [
    id 229
    label "Wawrzyszew"
  ]
  node [
    id 230
    label "&#321;agiewniki"
  ]
  node [
    id 231
    label "Azory"
  ]
  node [
    id 232
    label "&#379;erniki"
  ]
  node [
    id 233
    label "jednostka_administracyjna"
  ]
  node [
    id 234
    label "Goc&#322;aw"
  ]
  node [
    id 235
    label "Latycz&#243;w"
  ]
  node [
    id 236
    label "Micha&#322;owo"
  ]
  node [
    id 237
    label "Broch&#243;w"
  ]
  node [
    id 238
    label "jednostka_osadnicza"
  ]
  node [
    id 239
    label "M&#322;ociny"
  ]
  node [
    id 240
    label "Groch&#243;w"
  ]
  node [
    id 241
    label "dzielnica"
  ]
  node [
    id 242
    label "Laskowice_O&#322;awskie"
  ]
  node [
    id 243
    label "Marysin_Wawerski"
  ]
  node [
    id 244
    label "Le&#347;nica"
  ]
  node [
    id 245
    label "Kortowo"
  ]
  node [
    id 246
    label "G&#322;uszyna"
  ]
  node [
    id 247
    label "Kar&#322;owice"
  ]
  node [
    id 248
    label "Kujbyszewe"
  ]
  node [
    id 249
    label "Tarchomin"
  ]
  node [
    id 250
    label "&#379;era&#324;"
  ]
  node [
    id 251
    label "Jasienica"
  ]
  node [
    id 252
    label "Ok&#281;cie"
  ]
  node [
    id 253
    label "Zakrz&#243;w"
  ]
  node [
    id 254
    label "G&#243;rczyn"
  ]
  node [
    id 255
    label "Powi&#347;le"
  ]
  node [
    id 256
    label "Lewin&#243;w"
  ]
  node [
    id 257
    label "Gutkowo"
  ]
  node [
    id 258
    label "Wad&#243;w"
  ]
  node [
    id 259
    label "Gr&#281;ba&#322;&#243;w"
  ]
  node [
    id 260
    label "Dojlidy"
  ]
  node [
    id 261
    label "Marymont"
  ]
  node [
    id 262
    label "Rataje"
  ]
  node [
    id 263
    label "Grabiszyn"
  ]
  node [
    id 264
    label "Szczytniki"
  ]
  node [
    id 265
    label "Anin"
  ]
  node [
    id 266
    label "Imielin"
  ]
  node [
    id 267
    label "siedziba"
  ]
  node [
    id 268
    label "Zalesie"
  ]
  node [
    id 269
    label "Arsk"
  ]
  node [
    id 270
    label "Bogucice"
  ]
  node [
    id 271
    label "kultura_aszelska"
  ]
  node [
    id 272
    label "Francja"
  ]
  node [
    id 273
    label "report"
  ]
  node [
    id 274
    label "dyskalkulia"
  ]
  node [
    id 275
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 276
    label "osi&#261;ga&#263;"
  ]
  node [
    id 277
    label "wymienia&#263;"
  ]
  node [
    id 278
    label "posiada&#263;"
  ]
  node [
    id 279
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 280
    label "wycenia&#263;"
  ]
  node [
    id 281
    label "bra&#263;"
  ]
  node [
    id 282
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 283
    label "mierzy&#263;"
  ]
  node [
    id 284
    label "rachowa&#263;"
  ]
  node [
    id 285
    label "count"
  ]
  node [
    id 286
    label "tell"
  ]
  node [
    id 287
    label "odlicza&#263;"
  ]
  node [
    id 288
    label "dodawa&#263;"
  ]
  node [
    id 289
    label "wyznacza&#263;"
  ]
  node [
    id 290
    label "admit"
  ]
  node [
    id 291
    label "policza&#263;"
  ]
  node [
    id 292
    label "okre&#347;la&#263;"
  ]
  node [
    id 293
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 294
    label "take"
  ]
  node [
    id 295
    label "odejmowa&#263;"
  ]
  node [
    id 296
    label "odmierza&#263;"
  ]
  node [
    id 297
    label "my&#347;le&#263;"
  ]
  node [
    id 298
    label "involve"
  ]
  node [
    id 299
    label "dochrapywa&#263;_si&#281;"
  ]
  node [
    id 300
    label "uzyskiwa&#263;"
  ]
  node [
    id 301
    label "dociera&#263;"
  ]
  node [
    id 302
    label "mark"
  ]
  node [
    id 303
    label "get"
  ]
  node [
    id 304
    label "wiedzie&#263;"
  ]
  node [
    id 305
    label "zawiera&#263;"
  ]
  node [
    id 306
    label "mie&#263;"
  ]
  node [
    id 307
    label "support"
  ]
  node [
    id 308
    label "zdolno&#347;&#263;"
  ]
  node [
    id 309
    label "keep_open"
  ]
  node [
    id 310
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 311
    label "podawa&#263;"
  ]
  node [
    id 312
    label "mienia&#263;"
  ]
  node [
    id 313
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 314
    label "zmienia&#263;"
  ]
  node [
    id 315
    label "zakomunikowa&#263;"
  ]
  node [
    id 316
    label "quote"
  ]
  node [
    id 317
    label "mention"
  ]
  node [
    id 318
    label "dawa&#263;"
  ]
  node [
    id 319
    label "bind"
  ]
  node [
    id 320
    label "suma"
  ]
  node [
    id 321
    label "uzupe&#322;nia&#263;"
  ]
  node [
    id 322
    label "nadawa&#263;"
  ]
  node [
    id 323
    label "set"
  ]
  node [
    id 324
    label "zaznacza&#263;"
  ]
  node [
    id 325
    label "wybiera&#263;"
  ]
  node [
    id 326
    label "inflict"
  ]
  node [
    id 327
    label "ustala&#263;"
  ]
  node [
    id 328
    label "robi&#263;"
  ]
  node [
    id 329
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 330
    label "porywa&#263;"
  ]
  node [
    id 331
    label "korzysta&#263;"
  ]
  node [
    id 332
    label "wchodzi&#263;"
  ]
  node [
    id 333
    label "poczytywa&#263;"
  ]
  node [
    id 334
    label "levy"
  ]
  node [
    id 335
    label "wk&#322;ada&#263;"
  ]
  node [
    id 336
    label "raise"
  ]
  node [
    id 337
    label "pokonywa&#263;"
  ]
  node [
    id 338
    label "by&#263;"
  ]
  node [
    id 339
    label "przyjmowa&#263;"
  ]
  node [
    id 340
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 341
    label "rucha&#263;"
  ]
  node [
    id 342
    label "prowadzi&#263;"
  ]
  node [
    id 343
    label "za&#380;ywa&#263;"
  ]
  node [
    id 344
    label "otrzymywa&#263;"
  ]
  node [
    id 345
    label "&#263;pa&#263;"
  ]
  node [
    id 346
    label "interpretowa&#263;"
  ]
  node [
    id 347
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 348
    label "dostawa&#263;"
  ]
  node [
    id 349
    label "rusza&#263;"
  ]
  node [
    id 350
    label "chwyta&#263;"
  ]
  node [
    id 351
    label "grza&#263;"
  ]
  node [
    id 352
    label "wch&#322;ania&#263;"
  ]
  node [
    id 353
    label "wygrywa&#263;"
  ]
  node [
    id 354
    label "u&#380;ywa&#263;"
  ]
  node [
    id 355
    label "ucieka&#263;"
  ]
  node [
    id 356
    label "arise"
  ]
  node [
    id 357
    label "uprawia&#263;_seks"
  ]
  node [
    id 358
    label "abstract"
  ]
  node [
    id 359
    label "towarzystwo"
  ]
  node [
    id 360
    label "atakowa&#263;"
  ]
  node [
    id 361
    label "branie"
  ]
  node [
    id 362
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 363
    label "zalicza&#263;"
  ]
  node [
    id 364
    label "open"
  ]
  node [
    id 365
    label "wzi&#261;&#263;"
  ]
  node [
    id 366
    label "&#322;apa&#263;"
  ]
  node [
    id 367
    label "przewa&#380;a&#263;"
  ]
  node [
    id 368
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 369
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 370
    label "decydowa&#263;"
  ]
  node [
    id 371
    label "signify"
  ]
  node [
    id 372
    label "style"
  ]
  node [
    id 373
    label "powodowa&#263;"
  ]
  node [
    id 374
    label "umowa"
  ]
  node [
    id 375
    label "cover"
  ]
  node [
    id 376
    label "stanowisko_archeologiczne"
  ]
  node [
    id 377
    label "wlicza&#263;"
  ]
  node [
    id 378
    label "appreciate"
  ]
  node [
    id 379
    label "danie"
  ]
  node [
    id 380
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 381
    label "return"
  ]
  node [
    id 382
    label "refund"
  ]
  node [
    id 383
    label "liczenie"
  ]
  node [
    id 384
    label "doch&#243;d"
  ]
  node [
    id 385
    label "wynagrodzenie_brutto"
  ]
  node [
    id 386
    label "koszt_rodzajowy"
  ]
  node [
    id 387
    label "policzy&#263;"
  ]
  node [
    id 388
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 389
    label "ordynaria"
  ]
  node [
    id 390
    label "bud&#380;et_domowy"
  ]
  node [
    id 391
    label "pay"
  ]
  node [
    id 392
    label "zap&#322;ata"
  ]
  node [
    id 393
    label "dysleksja"
  ]
  node [
    id 394
    label "dyscalculia"
  ]
  node [
    id 395
    label "analfabetyzm_matematyczny"
  ]
  node [
    id 396
    label "licytacja"
  ]
  node [
    id 397
    label "kwota"
  ]
  node [
    id 398
    label "liczba"
  ]
  node [
    id 399
    label "molarity"
  ]
  node [
    id 400
    label "tauzen"
  ]
  node [
    id 401
    label "gra_w_karty"
  ]
  node [
    id 402
    label "patyk"
  ]
  node [
    id 403
    label "musik"
  ]
  node [
    id 404
    label "wynie&#347;&#263;"
  ]
  node [
    id 405
    label "pieni&#261;dze"
  ]
  node [
    id 406
    label "ilo&#347;&#263;"
  ]
  node [
    id 407
    label "limit"
  ]
  node [
    id 408
    label "wynosi&#263;"
  ]
  node [
    id 409
    label "kategoria"
  ]
  node [
    id 410
    label "pierwiastek"
  ]
  node [
    id 411
    label "rozmiar"
  ]
  node [
    id 412
    label "wyra&#380;enie"
  ]
  node [
    id 413
    label "poj&#281;cie"
  ]
  node [
    id 414
    label "number"
  ]
  node [
    id 415
    label "kategoria_gramatyczna"
  ]
  node [
    id 416
    label "kwadrat_magiczny"
  ]
  node [
    id 417
    label "koniugacja"
  ]
  node [
    id 418
    label "przymus"
  ]
  node [
    id 419
    label "przetarg"
  ]
  node [
    id 420
    label "rozdanie"
  ]
  node [
    id 421
    label "faza"
  ]
  node [
    id 422
    label "pas"
  ]
  node [
    id 423
    label "sprzeda&#380;"
  ]
  node [
    id 424
    label "bryd&#380;"
  ]
  node [
    id 425
    label "skat"
  ]
  node [
    id 426
    label "kij"
  ]
  node [
    id 427
    label "obiekt_naturalny"
  ]
  node [
    id 428
    label "pr&#281;t"
  ]
  node [
    id 429
    label "chudzielec"
  ]
  node [
    id 430
    label "rod"
  ]
  node [
    id 431
    label "dost&#281;p"
  ]
  node [
    id 432
    label "definicja"
  ]
  node [
    id 433
    label "sztuka_dla_sztuki"
  ]
  node [
    id 434
    label "kod"
  ]
  node [
    id 435
    label "solicitation"
  ]
  node [
    id 436
    label "leksem"
  ]
  node [
    id 437
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 438
    label "pozycja"
  ]
  node [
    id 439
    label "sygna&#322;"
  ]
  node [
    id 440
    label "przes&#322;anie"
  ]
  node [
    id 441
    label "artyku&#322;_has&#322;owy"
  ]
  node [
    id 442
    label "kwalifikator"
  ]
  node [
    id 443
    label "ochrona"
  ]
  node [
    id 444
    label "idea"
  ]
  node [
    id 445
    label "guide_word"
  ]
  node [
    id 446
    label "sformu&#322;owanie"
  ]
  node [
    id 447
    label "zdarzenie_si&#281;"
  ]
  node [
    id 448
    label "poinformowanie"
  ]
  node [
    id 449
    label "wording"
  ]
  node [
    id 450
    label "kompozycja"
  ]
  node [
    id 451
    label "oznaczenie"
  ]
  node [
    id 452
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 453
    label "ozdobnik"
  ]
  node [
    id 454
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 455
    label "grupa_imienna"
  ]
  node [
    id 456
    label "jednostka_leksykalna"
  ]
  node [
    id 457
    label "term"
  ]
  node [
    id 458
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 459
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 460
    label "ujawnienie"
  ]
  node [
    id 461
    label "affirmation"
  ]
  node [
    id 462
    label "zapisanie"
  ]
  node [
    id 463
    label "Stra&#380;_Ochrony_Kolei"
  ]
  node [
    id 464
    label "obstawianie"
  ]
  node [
    id 465
    label "obstawienie"
  ]
  node [
    id 466
    label "tarcza"
  ]
  node [
    id 467
    label "ubezpieczenie"
  ]
  node [
    id 468
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 469
    label "transportacja"
  ]
  node [
    id 470
    label "obstawia&#263;"
  ]
  node [
    id 471
    label "obiekt"
  ]
  node [
    id 472
    label "borowiec"
  ]
  node [
    id 473
    label "chemical_bond"
  ]
  node [
    id 474
    label "struktura"
  ]
  node [
    id 475
    label "language"
  ]
  node [
    id 476
    label "code"
  ]
  node [
    id 477
    label "szyfrowanie"
  ]
  node [
    id 478
    label "ci&#261;g"
  ]
  node [
    id 479
    label "szablon"
  ]
  node [
    id 480
    label "po&#322;o&#380;enie"
  ]
  node [
    id 481
    label "debit"
  ]
  node [
    id 482
    label "druk"
  ]
  node [
    id 483
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 484
    label "szata_graficzna"
  ]
  node [
    id 485
    label "wydawa&#263;"
  ]
  node [
    id 486
    label "szermierka"
  ]
  node [
    id 487
    label "spis"
  ]
  node [
    id 488
    label "wyda&#263;"
  ]
  node [
    id 489
    label "ustawienie"
  ]
  node [
    id 490
    label "publikacja"
  ]
  node [
    id 491
    label "status"
  ]
  node [
    id 492
    label "miejsce"
  ]
  node [
    id 493
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 494
    label "adres"
  ]
  node [
    id 495
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 496
    label "rozmieszczenie"
  ]
  node [
    id 497
    label "sytuacja"
  ]
  node [
    id 498
    label "rz&#261;d"
  ]
  node [
    id 499
    label "redaktor"
  ]
  node [
    id 500
    label "awansowa&#263;"
  ]
  node [
    id 501
    label "wojsko"
  ]
  node [
    id 502
    label "znaczenie"
  ]
  node [
    id 503
    label "awans"
  ]
  node [
    id 504
    label "awansowanie"
  ]
  node [
    id 505
    label "poster"
  ]
  node [
    id 506
    label "le&#380;e&#263;"
  ]
  node [
    id 507
    label "wordnet"
  ]
  node [
    id 508
    label "wypowiedzenie"
  ]
  node [
    id 509
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 510
    label "morfem"
  ]
  node [
    id 511
    label "s&#322;ownictwo"
  ]
  node [
    id 512
    label "wykrzyknik"
  ]
  node [
    id 513
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 514
    label "pole_semantyczne"
  ]
  node [
    id 515
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 516
    label "pisanie_si&#281;"
  ]
  node [
    id 517
    label "nag&#322;os"
  ]
  node [
    id 518
    label "wyg&#322;os"
  ]
  node [
    id 519
    label "rozwleczenie"
  ]
  node [
    id 520
    label "wyznanie"
  ]
  node [
    id 521
    label "przepowiedzenie"
  ]
  node [
    id 522
    label "podanie"
  ]
  node [
    id 523
    label "zapeszenie"
  ]
  node [
    id 524
    label "proverb"
  ]
  node [
    id 525
    label "ozwanie_si&#281;"
  ]
  node [
    id 526
    label "nazwanie"
  ]
  node [
    id 527
    label "statement"
  ]
  node [
    id 528
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 529
    label "doprowadzenie"
  ]
  node [
    id 530
    label "ideologia"
  ]
  node [
    id 531
    label "byt"
  ]
  node [
    id 532
    label "intelekt"
  ]
  node [
    id 533
    label "Kant"
  ]
  node [
    id 534
    label "p&#322;&#243;d"
  ]
  node [
    id 535
    label "cel"
  ]
  node [
    id 536
    label "istota"
  ]
  node [
    id 537
    label "ideacja"
  ]
  node [
    id 538
    label "przekazywa&#263;"
  ]
  node [
    id 539
    label "s&#322;uchawka_telefoniczna"
  ]
  node [
    id 540
    label "pulsation"
  ]
  node [
    id 541
    label "przekazywanie"
  ]
  node [
    id 542
    label "przewodzenie"
  ]
  node [
    id 543
    label "d&#378;wi&#281;k"
  ]
  node [
    id 544
    label "po&#322;&#261;czenie"
  ]
  node [
    id 545
    label "fala"
  ]
  node [
    id 546
    label "doj&#347;cie"
  ]
  node [
    id 547
    label "przekazanie"
  ]
  node [
    id 548
    label "przewodzi&#263;"
  ]
  node [
    id 549
    label "znak"
  ]
  node [
    id 550
    label "zapowied&#378;"
  ]
  node [
    id 551
    label "medium_transmisyjne"
  ]
  node [
    id 552
    label "demodulacja"
  ]
  node [
    id 553
    label "doj&#347;&#263;"
  ]
  node [
    id 554
    label "przekaza&#263;"
  ]
  node [
    id 555
    label "czynnik"
  ]
  node [
    id 556
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 557
    label "aliasing"
  ]
  node [
    id 558
    label "wizja"
  ]
  node [
    id 559
    label "modulacja"
  ]
  node [
    id 560
    label "point"
  ]
  node [
    id 561
    label "drift"
  ]
  node [
    id 562
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 563
    label "po&#322;&#243;g"
  ]
  node [
    id 564
    label "spe&#322;nienie"
  ]
  node [
    id 565
    label "dula"
  ]
  node [
    id 566
    label "spos&#243;b"
  ]
  node [
    id 567
    label "usuni&#281;cie"
  ]
  node [
    id 568
    label "wymy&#347;lenie"
  ]
  node [
    id 569
    label "po&#322;o&#380;na"
  ]
  node [
    id 570
    label "uniewa&#380;nienie"
  ]
  node [
    id 571
    label "proces_fizjologiczny"
  ]
  node [
    id 572
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 573
    label "szok_poporodowy"
  ]
  node [
    id 574
    label "event"
  ]
  node [
    id 575
    label "marc&#243;wka"
  ]
  node [
    id 576
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 577
    label "birth"
  ]
  node [
    id 578
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 579
    label "wynik"
  ]
  node [
    id 580
    label "przestanie"
  ]
  node [
    id 581
    label "informatyka"
  ]
  node [
    id 582
    label "operacja"
  ]
  node [
    id 583
    label "konto"
  ]
  node [
    id 584
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 585
    label "definiendum"
  ]
  node [
    id 586
    label "definiens"
  ]
  node [
    id 587
    label "obja&#347;nienie"
  ]
  node [
    id 588
    label "definition"
  ]
  node [
    id 589
    label "modifier"
  ]
  node [
    id 590
    label "skr&#243;t"
  ]
  node [
    id 591
    label "informacja"
  ]
  node [
    id 592
    label "selekcjoner"
  ]
  node [
    id 593
    label "pismo"
  ]
  node [
    id 594
    label "forward"
  ]
  node [
    id 595
    label "message"
  ]
  node [
    id 596
    label "p&#243;j&#347;cie"
  ]
  node [
    id 597
    label "bed"
  ]
  node [
    id 598
    label "kaczki_w&#322;a&#347;ciwe"
  ]
  node [
    id 599
    label "szarada"
  ]
  node [
    id 600
    label "p&#243;&#322;tusza"
  ]
  node [
    id 601
    label "hybrid"
  ]
  node [
    id 602
    label "skrzy&#380;owanie"
  ]
  node [
    id 603
    label "synteza"
  ]
  node [
    id 604
    label "kaczka"
  ]
  node [
    id 605
    label "metyzacja"
  ]
  node [
    id 606
    label "przeci&#281;cie"
  ]
  node [
    id 607
    label "&#347;wiat&#322;a"
  ]
  node [
    id 608
    label "istota_&#380;ywa"
  ]
  node [
    id 609
    label "mi&#281;so"
  ]
  node [
    id 610
    label "kontaminacja"
  ]
  node [
    id 611
    label "ptak_&#322;owny"
  ]
  node [
    id 612
    label "polski"
  ]
  node [
    id 613
    label "Wikipedia"
  ]
  node [
    id 614
    label "Heather"
  ]
  node [
    id 615
    label "Morrison"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 4
    target 242
  ]
  edge [
    source 4
    target 243
  ]
  edge [
    source 4
    target 244
  ]
  edge [
    source 4
    target 245
  ]
  edge [
    source 4
    target 246
  ]
  edge [
    source 4
    target 247
  ]
  edge [
    source 4
    target 248
  ]
  edge [
    source 4
    target 249
  ]
  edge [
    source 4
    target 250
  ]
  edge [
    source 4
    target 251
  ]
  edge [
    source 4
    target 252
  ]
  edge [
    source 4
    target 253
  ]
  edge [
    source 4
    target 254
  ]
  edge [
    source 4
    target 255
  ]
  edge [
    source 4
    target 256
  ]
  edge [
    source 4
    target 257
  ]
  edge [
    source 4
    target 258
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 259
  ]
  edge [
    source 4
    target 260
  ]
  edge [
    source 4
    target 261
  ]
  edge [
    source 4
    target 262
  ]
  edge [
    source 4
    target 263
  ]
  edge [
    source 4
    target 264
  ]
  edge [
    source 4
    target 265
  ]
  edge [
    source 4
    target 266
  ]
  edge [
    source 4
    target 267
  ]
  edge [
    source 4
    target 268
  ]
  edge [
    source 4
    target 269
  ]
  edge [
    source 4
    target 270
  ]
  edge [
    source 4
    target 271
  ]
  edge [
    source 4
    target 272
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 9
    target 431
  ]
  edge [
    source 9
    target 432
  ]
  edge [
    source 9
    target 433
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 434
  ]
  edge [
    source 9
    target 435
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 436
  ]
  edge [
    source 9
    target 437
  ]
  edge [
    source 9
    target 438
  ]
  edge [
    source 9
    target 439
  ]
  edge [
    source 9
    target 440
  ]
  edge [
    source 9
    target 441
  ]
  edge [
    source 9
    target 442
  ]
  edge [
    source 9
    target 443
  ]
  edge [
    source 9
    target 444
  ]
  edge [
    source 9
    target 445
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 446
  ]
  edge [
    source 9
    target 447
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 448
  ]
  edge [
    source 9
    target 449
  ]
  edge [
    source 9
    target 450
  ]
  edge [
    source 9
    target 451
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 452
  ]
  edge [
    source 9
    target 453
  ]
  edge [
    source 9
    target 454
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 456
  ]
  edge [
    source 9
    target 457
  ]
  edge [
    source 9
    target 458
  ]
  edge [
    source 9
    target 459
  ]
  edge [
    source 9
    target 460
  ]
  edge [
    source 9
    target 461
  ]
  edge [
    source 9
    target 462
  ]
  edge [
    source 9
    target 177
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 464
  ]
  edge [
    source 9
    target 465
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 467
  ]
  edge [
    source 9
    target 468
  ]
  edge [
    source 9
    target 469
  ]
  edge [
    source 9
    target 470
  ]
  edge [
    source 9
    target 471
  ]
  edge [
    source 9
    target 472
  ]
  edge [
    source 9
    target 473
  ]
  edge [
    source 9
    target 474
  ]
  edge [
    source 9
    target 475
  ]
  edge [
    source 9
    target 476
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 612
    target 613
  ]
  edge [
    source 614
    target 615
  ]
]
