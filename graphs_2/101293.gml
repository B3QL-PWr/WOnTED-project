graph [
  node [
    id 0
    label "telatycki"
    origin "text"
  ]
  node [
    id 1
    label "herb"
    origin "text"
  ]
  node [
    id 2
    label "szlachecki"
    origin "text"
  ]
  node [
    id 3
    label "znak"
  ]
  node [
    id 4
    label "tarcza_herbowa"
  ]
  node [
    id 5
    label "klejnot_herbowy"
  ]
  node [
    id 6
    label "trzymacz"
  ]
  node [
    id 7
    label "barwy"
  ]
  node [
    id 8
    label "heraldyka"
  ]
  node [
    id 9
    label "korona_rangowa"
  ]
  node [
    id 10
    label "blazonowanie"
  ]
  node [
    id 11
    label "blazonowa&#263;"
  ]
  node [
    id 12
    label "symbol"
  ]
  node [
    id 13
    label "znak_pisarski"
  ]
  node [
    id 14
    label "notacja"
  ]
  node [
    id 15
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 16
    label "character"
  ]
  node [
    id 17
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 18
    label "wcielenie"
  ]
  node [
    id 19
    label "symbolizowanie"
  ]
  node [
    id 20
    label "baretka"
  ]
  node [
    id 21
    label "postawi&#263;"
  ]
  node [
    id 22
    label "mark"
  ]
  node [
    id 23
    label "kodzik"
  ]
  node [
    id 24
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 25
    label "oznakowanie"
  ]
  node [
    id 26
    label "implikowa&#263;"
  ]
  node [
    id 27
    label "attribute"
  ]
  node [
    id 28
    label "wytw&#243;r"
  ]
  node [
    id 29
    label "point"
  ]
  node [
    id 30
    label "fakt"
  ]
  node [
    id 31
    label "dow&#243;d"
  ]
  node [
    id 32
    label "stawia&#263;"
  ]
  node [
    id 33
    label "coat_of_arms"
  ]
  node [
    id 34
    label "opisywanie"
  ]
  node [
    id 35
    label "opisywa&#263;"
  ]
  node [
    id 36
    label "or&#281;&#380;"
  ]
  node [
    id 37
    label "s&#322;up"
  ]
  node [
    id 38
    label "pas"
  ]
  node [
    id 39
    label "historia"
  ]
  node [
    id 40
    label "barwa_heraldyczna"
  ]
  node [
    id 41
    label "oksza"
  ]
  node [
    id 42
    label "&#347;lachecki"
  ]
  node [
    id 43
    label "szlachecko"
  ]
  node [
    id 44
    label "po_szlachecku"
  ]
  node [
    id 45
    label "aristocratically"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
]
