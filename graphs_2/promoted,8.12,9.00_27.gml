graph [
  node [
    id 0
    label "niezwykle"
    origin "text"
  ]
  node [
    id 1
    label "skuteczny"
    origin "text"
  ]
  node [
    id 2
    label "spalanie"
    origin "text"
  ]
  node [
    id 3
    label "kaloria"
    origin "text"
  ]
  node [
    id 4
    label "niezwyk&#322;y"
  ]
  node [
    id 5
    label "inny"
  ]
  node [
    id 6
    label "poskutkowanie"
  ]
  node [
    id 7
    label "sprawny"
  ]
  node [
    id 8
    label "skutecznie"
  ]
  node [
    id 9
    label "skutkowanie"
  ]
  node [
    id 10
    label "dobry"
  ]
  node [
    id 11
    label "szybki"
  ]
  node [
    id 12
    label "letki"
  ]
  node [
    id 13
    label "umiej&#281;tny"
  ]
  node [
    id 14
    label "zdrowy"
  ]
  node [
    id 15
    label "dzia&#322;alny"
  ]
  node [
    id 16
    label "sprawnie"
  ]
  node [
    id 17
    label "dobroczynny"
  ]
  node [
    id 18
    label "czw&#243;rka"
  ]
  node [
    id 19
    label "spokojny"
  ]
  node [
    id 20
    label "&#347;mieszny"
  ]
  node [
    id 21
    label "mi&#322;y"
  ]
  node [
    id 22
    label "grzeczny"
  ]
  node [
    id 23
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 24
    label "powitanie"
  ]
  node [
    id 25
    label "dobrze"
  ]
  node [
    id 26
    label "ca&#322;y"
  ]
  node [
    id 27
    label "zwrot"
  ]
  node [
    id 28
    label "pomy&#347;lny"
  ]
  node [
    id 29
    label "moralny"
  ]
  node [
    id 30
    label "drogi"
  ]
  node [
    id 31
    label "pozytywny"
  ]
  node [
    id 32
    label "odpowiedni"
  ]
  node [
    id 33
    label "korzystny"
  ]
  node [
    id 34
    label "pos&#322;uszny"
  ]
  node [
    id 35
    label "zdarzenie_si&#281;"
  ]
  node [
    id 36
    label "dzianie_si&#281;"
  ]
  node [
    id 37
    label "skutek_prawny"
  ]
  node [
    id 38
    label "utlenianie"
  ]
  node [
    id 39
    label "burning"
  ]
  node [
    id 40
    label "zabijanie"
  ]
  node [
    id 41
    label "przygrzewanie"
  ]
  node [
    id 42
    label "niszczenie"
  ]
  node [
    id 43
    label "spiekanie_si&#281;"
  ]
  node [
    id 44
    label "paliwo"
  ]
  node [
    id 45
    label "combustion"
  ]
  node [
    id 46
    label "podpalanie"
  ]
  node [
    id 47
    label "palenie_si&#281;"
  ]
  node [
    id 48
    label "incineration"
  ]
  node [
    id 49
    label "zu&#380;ywanie"
  ]
  node [
    id 50
    label "chemikalia"
  ]
  node [
    id 51
    label "metabolizowanie"
  ]
  node [
    id 52
    label "proces_chemiczny"
  ]
  node [
    id 53
    label "destruction"
  ]
  node [
    id 54
    label "powodowanie"
  ]
  node [
    id 55
    label "os&#322;abianie"
  ]
  node [
    id 56
    label "pl&#261;drowanie"
  ]
  node [
    id 57
    label "ravaging"
  ]
  node [
    id 58
    label "gnojenie"
  ]
  node [
    id 59
    label "szkodzenie"
  ]
  node [
    id 60
    label "pustoszenie"
  ]
  node [
    id 61
    label "decay"
  ]
  node [
    id 62
    label "poniewieranie_si&#281;"
  ]
  node [
    id 63
    label "zdrowie"
  ]
  node [
    id 64
    label "devastation"
  ]
  node [
    id 65
    label "stawanie_si&#281;"
  ]
  node [
    id 66
    label "utlenianie_si&#281;"
  ]
  node [
    id 67
    label "reakcja_chemiczna"
  ]
  node [
    id 68
    label "rozk&#322;adanie"
  ]
  node [
    id 69
    label "zamykanie"
  ]
  node [
    id 70
    label "skonany"
  ]
  node [
    id 71
    label "umieranie"
  ]
  node [
    id 72
    label "ruszanie_si&#281;"
  ]
  node [
    id 73
    label "killing"
  ]
  node [
    id 74
    label "usuwanie"
  ]
  node [
    id 75
    label "handling"
  ]
  node [
    id 76
    label "krzywdzenie"
  ]
  node [
    id 77
    label "kill"
  ]
  node [
    id 78
    label "nu&#380;enie"
  ]
  node [
    id 79
    label "hit"
  ]
  node [
    id 80
    label "granie"
  ]
  node [
    id 81
    label "fire"
  ]
  node [
    id 82
    label "zestrzeliwanie"
  ]
  node [
    id 83
    label "odstrzeliwanie"
  ]
  node [
    id 84
    label "zestrzelenie"
  ]
  node [
    id 85
    label "wystrzelanie"
  ]
  node [
    id 86
    label "wylatywanie"
  ]
  node [
    id 87
    label "chybianie"
  ]
  node [
    id 88
    label "przypieprzanie"
  ]
  node [
    id 89
    label "przestrzeliwanie"
  ]
  node [
    id 90
    label "walczenie"
  ]
  node [
    id 91
    label "ostrzelanie"
  ]
  node [
    id 92
    label "ostrzeliwanie"
  ]
  node [
    id 93
    label "trafianie"
  ]
  node [
    id 94
    label "odpalanie"
  ]
  node [
    id 95
    label "odstrzelenie"
  ]
  node [
    id 96
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 97
    label "postrzelanie"
  ]
  node [
    id 98
    label "uderzanie"
  ]
  node [
    id 99
    label "chybienie"
  ]
  node [
    id 100
    label "grzanie"
  ]
  node [
    id 101
    label "film_editing"
  ]
  node [
    id 102
    label "podgrzewanie"
  ]
  node [
    id 103
    label "palenie"
  ]
  node [
    id 104
    label "kropni&#281;cie"
  ]
  node [
    id 105
    label "prze&#322;adowywanie"
  ]
  node [
    id 106
    label "plucie"
  ]
  node [
    id 107
    label "wydawanie"
  ]
  node [
    id 108
    label "depreciation"
  ]
  node [
    id 109
    label "robienie"
  ]
  node [
    id 110
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 111
    label "zapalanie"
  ]
  node [
    id 112
    label "rozpalanie"
  ]
  node [
    id 113
    label "przypalanie"
  ]
  node [
    id 114
    label "spalenie"
  ]
  node [
    id 115
    label "zbi&#243;r"
  ]
  node [
    id 116
    label "tankowanie"
  ]
  node [
    id 117
    label "spali&#263;"
  ]
  node [
    id 118
    label "fuel"
  ]
  node [
    id 119
    label "zgazowa&#263;"
  ]
  node [
    id 120
    label "spala&#263;"
  ]
  node [
    id 121
    label "pompa_wtryskowa"
  ]
  node [
    id 122
    label "antydetonator"
  ]
  node [
    id 123
    label "Orlen"
  ]
  node [
    id 124
    label "substancja"
  ]
  node [
    id 125
    label "tankowa&#263;"
  ]
  node [
    id 126
    label "kilokaloria"
  ]
  node [
    id 127
    label "jednostka_energii"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
]
