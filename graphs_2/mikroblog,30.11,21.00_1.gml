graph [
  node [
    id 0
    label "wykop"
    origin "text"
  ]
  node [
    id 1
    label "bojowkaas"
    origin "text"
  ]
  node [
    id 2
    label "aswykopu"
    origin "text"
  ]
  node [
    id 3
    label "itaksiezyjenatymwykopie"
    origin "text"
  ]
  node [
    id 4
    label "heheszki"
    origin "text"
  ]
  node [
    id 5
    label "budowa"
  ]
  node [
    id 6
    label "zrzutowy"
  ]
  node [
    id 7
    label "odk&#322;ad"
  ]
  node [
    id 8
    label "chody"
  ]
  node [
    id 9
    label "szaniec"
  ]
  node [
    id 10
    label "wyrobisko"
  ]
  node [
    id 11
    label "kopniak"
  ]
  node [
    id 12
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 13
    label "odwa&#322;"
  ]
  node [
    id 14
    label "grodzisko"
  ]
  node [
    id 15
    label "cios"
  ]
  node [
    id 16
    label "kick"
  ]
  node [
    id 17
    label "kopni&#281;cie"
  ]
  node [
    id 18
    label "&#347;rodkowiec"
  ]
  node [
    id 19
    label "podsadzka"
  ]
  node [
    id 20
    label "obudowa"
  ]
  node [
    id 21
    label "sp&#261;g"
  ]
  node [
    id 22
    label "strop"
  ]
  node [
    id 23
    label "rabowarka"
  ]
  node [
    id 24
    label "opinka"
  ]
  node [
    id 25
    label "stojak_cierny"
  ]
  node [
    id 26
    label "kopalnia"
  ]
  node [
    id 27
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 28
    label "zag&#322;&#281;bi&#263;"
  ]
  node [
    id 29
    label "zag&#322;&#281;bienie_si&#281;"
  ]
  node [
    id 30
    label "immersion"
  ]
  node [
    id 31
    label "umieszczenie"
  ]
  node [
    id 32
    label "las"
  ]
  node [
    id 33
    label "nora"
  ]
  node [
    id 34
    label "pies_my&#347;liwski"
  ]
  node [
    id 35
    label "miejsce"
  ]
  node [
    id 36
    label "trasa"
  ]
  node [
    id 37
    label "doj&#347;cie"
  ]
  node [
    id 38
    label "zesp&#243;&#322;"
  ]
  node [
    id 39
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 40
    label "horodyszcze"
  ]
  node [
    id 41
    label "Wyszogr&#243;d"
  ]
  node [
    id 42
    label "usypisko"
  ]
  node [
    id 43
    label "r&#243;w"
  ]
  node [
    id 44
    label "wa&#322;"
  ]
  node [
    id 45
    label "redoubt"
  ]
  node [
    id 46
    label "fortyfikacja"
  ]
  node [
    id 47
    label "mechanika"
  ]
  node [
    id 48
    label "struktura"
  ]
  node [
    id 49
    label "figura"
  ]
  node [
    id 50
    label "miejsce_pracy"
  ]
  node [
    id 51
    label "cecha"
  ]
  node [
    id 52
    label "organ"
  ]
  node [
    id 53
    label "kreacja"
  ]
  node [
    id 54
    label "zwierz&#281;"
  ]
  node [
    id 55
    label "posesja"
  ]
  node [
    id 56
    label "konstrukcja"
  ]
  node [
    id 57
    label "wjazd"
  ]
  node [
    id 58
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 59
    label "praca"
  ]
  node [
    id 60
    label "constitution"
  ]
  node [
    id 61
    label "gleba"
  ]
  node [
    id 62
    label "p&#281;d"
  ]
  node [
    id 63
    label "zbi&#243;r"
  ]
  node [
    id 64
    label "ablegier"
  ]
  node [
    id 65
    label "rozmna&#380;anie_bezp&#322;ciowe"
  ]
  node [
    id 66
    label "layer"
  ]
  node [
    id 67
    label "r&#243;j"
  ]
  node [
    id 68
    label "mrowisko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
]
