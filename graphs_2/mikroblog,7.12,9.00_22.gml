graph [
  node [
    id 0
    label "niestety"
    origin "text"
  ]
  node [
    id 1
    label "jeszcze"
    origin "text"
  ]
  node [
    id 2
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 3
    label "xdd"
    origin "text"
  ]
  node [
    id 4
    label "pokazmorde"
    origin "text"
  ]
  node [
    id 5
    label "ci&#261;gle"
  ]
  node [
    id 6
    label "stale"
  ]
  node [
    id 7
    label "ci&#261;g&#322;y"
  ]
  node [
    id 8
    label "nieprzerwanie"
  ]
  node [
    id 9
    label "istnie&#263;"
  ]
  node [
    id 10
    label "pause"
  ]
  node [
    id 11
    label "stay"
  ]
  node [
    id 12
    label "consist"
  ]
  node [
    id 13
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 14
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 15
    label "mie&#263;_miejsce"
  ]
  node [
    id 16
    label "odst&#281;powa&#263;"
  ]
  node [
    id 17
    label "perform"
  ]
  node [
    id 18
    label "wychodzi&#263;"
  ]
  node [
    id 19
    label "seclude"
  ]
  node [
    id 20
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 21
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 22
    label "nak&#322;ania&#263;"
  ]
  node [
    id 23
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 24
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 25
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 26
    label "dzia&#322;a&#263;"
  ]
  node [
    id 27
    label "act"
  ]
  node [
    id 28
    label "appear"
  ]
  node [
    id 29
    label "unwrap"
  ]
  node [
    id 30
    label "rezygnowa&#263;"
  ]
  node [
    id 31
    label "overture"
  ]
  node [
    id 32
    label "uczestniczy&#263;"
  ]
  node [
    id 33
    label "stand"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
]
