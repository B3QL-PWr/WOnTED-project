graph [
  node [
    id 0
    label "justyn"
    origin "text"
  ]
  node [
    id 1
    label "&#380;y&#322;a"
    origin "text"
  ]
  node [
    id 2
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 3
    label "&#380;ona"
    origin "text"
  ]
  node [
    id 4
    label "skoczek"
    origin "text"
  ]
  node [
    id 5
    label "narciarski"
    origin "text"
  ]
  node [
    id 6
    label "piotr"
    origin "text"
  ]
  node [
    id 7
    label "swoje"
    origin "text"
  ]
  node [
    id 8
    label "program"
    origin "text"
  ]
  node [
    id 9
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 10
    label "rozwi&#261;zywa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "konflikt"
    origin "text"
  ]
  node [
    id 12
    label "uczestnik"
    origin "text"
  ]
  node [
    id 13
    label "projekt"
    origin "text"
  ]
  node [
    id 14
    label "atleta"
  ]
  node [
    id 15
    label "sk&#261;py"
  ]
  node [
    id 16
    label "naczynie"
  ]
  node [
    id 17
    label "cz&#322;owiek"
  ]
  node [
    id 18
    label "vein"
  ]
  node [
    id 19
    label "lina"
  ]
  node [
    id 20
    label "dost&#281;p_do&#380;ylny"
  ]
  node [
    id 21
    label "sk&#261;piarz"
  ]
  node [
    id 22
    label "okrutnik"
  ]
  node [
    id 23
    label "materialista"
  ]
  node [
    id 24
    label "przew&#243;d"
  ]
  node [
    id 25
    label "formacja_geologiczna"
  ]
  node [
    id 26
    label "chciwiec"
  ]
  node [
    id 27
    label "wymagaj&#261;cy"
  ]
  node [
    id 28
    label "krzepa"
  ]
  node [
    id 29
    label "typ_atletyczny"
  ]
  node [
    id 30
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 31
    label "vessel"
  ]
  node [
    id 32
    label "sprz&#281;t"
  ]
  node [
    id 33
    label "statki"
  ]
  node [
    id 34
    label "rewaskularyzacja"
  ]
  node [
    id 35
    label "ceramika"
  ]
  node [
    id 36
    label "drewno"
  ]
  node [
    id 37
    label "unaczyni&#263;"
  ]
  node [
    id 38
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 39
    label "receptacle"
  ]
  node [
    id 40
    label "Lamarck"
  ]
  node [
    id 41
    label "monista"
  ]
  node [
    id 42
    label "egoista"
  ]
  node [
    id 43
    label "istota_&#380;ywa"
  ]
  node [
    id 44
    label "ludzko&#347;&#263;"
  ]
  node [
    id 45
    label "asymilowanie"
  ]
  node [
    id 46
    label "wapniak"
  ]
  node [
    id 47
    label "asymilowa&#263;"
  ]
  node [
    id 48
    label "os&#322;abia&#263;"
  ]
  node [
    id 49
    label "posta&#263;"
  ]
  node [
    id 50
    label "hominid"
  ]
  node [
    id 51
    label "podw&#322;adny"
  ]
  node [
    id 52
    label "os&#322;abianie"
  ]
  node [
    id 53
    label "g&#322;owa"
  ]
  node [
    id 54
    label "figura"
  ]
  node [
    id 55
    label "portrecista"
  ]
  node [
    id 56
    label "dwun&#243;g"
  ]
  node [
    id 57
    label "profanum"
  ]
  node [
    id 58
    label "mikrokosmos"
  ]
  node [
    id 59
    label "nasada"
  ]
  node [
    id 60
    label "duch"
  ]
  node [
    id 61
    label "antropochoria"
  ]
  node [
    id 62
    label "osoba"
  ]
  node [
    id 63
    label "wz&#243;r"
  ]
  node [
    id 64
    label "senior"
  ]
  node [
    id 65
    label "oddzia&#322;ywanie"
  ]
  node [
    id 66
    label "Adam"
  ]
  node [
    id 67
    label "homo_sapiens"
  ]
  node [
    id 68
    label "polifag"
  ]
  node [
    id 69
    label "kognicja"
  ]
  node [
    id 70
    label "linia"
  ]
  node [
    id 71
    label "przy&#322;&#261;cze"
  ]
  node [
    id 72
    label "rozprawa"
  ]
  node [
    id 73
    label "wydarzenie"
  ]
  node [
    id 74
    label "organ"
  ]
  node [
    id 75
    label "przes&#322;anka"
  ]
  node [
    id 76
    label "post&#281;powanie"
  ]
  node [
    id 77
    label "przewodnictwo"
  ]
  node [
    id 78
    label "tr&#243;jnik"
  ]
  node [
    id 79
    label "wtyczka"
  ]
  node [
    id 80
    label "obw&#243;d_elektryczny"
  ]
  node [
    id 81
    label "duct"
  ]
  node [
    id 82
    label "urz&#261;dzenie"
  ]
  node [
    id 83
    label "przedmiot"
  ]
  node [
    id 84
    label "wyluzowanie"
  ]
  node [
    id 85
    label "skr&#281;tka"
  ]
  node [
    id 86
    label "pika-pina"
  ]
  node [
    id 87
    label "bom"
  ]
  node [
    id 88
    label "abaka"
  ]
  node [
    id 89
    label "wyluzowa&#263;"
  ]
  node [
    id 90
    label "z&#322;y_cz&#322;owiek"
  ]
  node [
    id 91
    label "okrutny"
  ]
  node [
    id 92
    label "sk&#261;piec"
  ]
  node [
    id 93
    label "nieobfity"
  ]
  node [
    id 94
    label "mizerny"
  ]
  node [
    id 95
    label "sk&#261;po"
  ]
  node [
    id 96
    label "nienale&#380;yty"
  ]
  node [
    id 97
    label "niewystarczaj&#261;cy"
  ]
  node [
    id 98
    label "oszcz&#281;dny"
  ]
  node [
    id 99
    label "z&#322;y"
  ]
  node [
    id 100
    label "wymagaj&#261;co"
  ]
  node [
    id 101
    label "partnerka"
  ]
  node [
    id 102
    label "aktorka"
  ]
  node [
    id 103
    label "kobieta"
  ]
  node [
    id 104
    label "partner"
  ]
  node [
    id 105
    label "kobita"
  ]
  node [
    id 106
    label "ma&#322;&#380;onek"
  ]
  node [
    id 107
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 108
    label "&#347;lubna"
  ]
  node [
    id 109
    label "panna_m&#322;oda"
  ]
  node [
    id 110
    label "stan_cywilny"
  ]
  node [
    id 111
    label "para"
  ]
  node [
    id 112
    label "matrymonialny"
  ]
  node [
    id 113
    label "lewirat"
  ]
  node [
    id 114
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 115
    label "sakrament"
  ]
  node [
    id 116
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 117
    label "zwi&#261;zek"
  ]
  node [
    id 118
    label "partia"
  ]
  node [
    id 119
    label "ch&#322;op"
  ]
  node [
    id 120
    label "pan_m&#322;ody"
  ]
  node [
    id 121
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 122
    label "&#347;lubny"
  ]
  node [
    id 123
    label "pan_domu"
  ]
  node [
    id 124
    label "pan_i_w&#322;adca"
  ]
  node [
    id 125
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 126
    label "stary"
  ]
  node [
    id 127
    label "szara&#324;czowate"
  ]
  node [
    id 128
    label "szara&#324;czak"
  ]
  node [
    id 129
    label "sportowiec"
  ]
  node [
    id 130
    label "charakterystyka"
  ]
  node [
    id 131
    label "p&#322;aszczyzna"
  ]
  node [
    id 132
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 133
    label "bierka_szachowa"
  ]
  node [
    id 134
    label "obiekt_matematyczny"
  ]
  node [
    id 135
    label "gestaltyzm"
  ]
  node [
    id 136
    label "styl"
  ]
  node [
    id 137
    label "obraz"
  ]
  node [
    id 138
    label "cecha"
  ]
  node [
    id 139
    label "Osjan"
  ]
  node [
    id 140
    label "rzecz"
  ]
  node [
    id 141
    label "d&#378;wi&#281;k"
  ]
  node [
    id 142
    label "character"
  ]
  node [
    id 143
    label "kto&#347;"
  ]
  node [
    id 144
    label "rze&#378;ba"
  ]
  node [
    id 145
    label "stylistyka"
  ]
  node [
    id 146
    label "figure"
  ]
  node [
    id 147
    label "wygl&#261;d"
  ]
  node [
    id 148
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 149
    label "wytw&#243;r"
  ]
  node [
    id 150
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 151
    label "miejsce"
  ]
  node [
    id 152
    label "antycypacja"
  ]
  node [
    id 153
    label "ornamentyka"
  ]
  node [
    id 154
    label "sztuka"
  ]
  node [
    id 155
    label "informacja"
  ]
  node [
    id 156
    label "Aspazja"
  ]
  node [
    id 157
    label "facet"
  ]
  node [
    id 158
    label "popis"
  ]
  node [
    id 159
    label "wiersz"
  ]
  node [
    id 160
    label "kompleksja"
  ]
  node [
    id 161
    label "budowa"
  ]
  node [
    id 162
    label "symetria"
  ]
  node [
    id 163
    label "lingwistyka_kognitywna"
  ]
  node [
    id 164
    label "karta"
  ]
  node [
    id 165
    label "shape"
  ]
  node [
    id 166
    label "podzbi&#243;r"
  ]
  node [
    id 167
    label "przedstawienie"
  ]
  node [
    id 168
    label "point"
  ]
  node [
    id 169
    label "perspektywa"
  ]
  node [
    id 170
    label "zgrupowanie"
  ]
  node [
    id 171
    label "ro&#347;lino&#380;erca"
  ]
  node [
    id 172
    label "kr&#243;tkoczu&#322;kie"
  ]
  node [
    id 173
    label "owad"
  ]
  node [
    id 174
    label "zimowy"
  ]
  node [
    id 175
    label "specjalny"
  ]
  node [
    id 176
    label "intencjonalny"
  ]
  node [
    id 177
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 178
    label "niedorozw&#243;j"
  ]
  node [
    id 179
    label "szczeg&#243;lny"
  ]
  node [
    id 180
    label "specjalnie"
  ]
  node [
    id 181
    label "nieetatowy"
  ]
  node [
    id 182
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 183
    label "nienormalny"
  ]
  node [
    id 184
    label "umy&#347;lnie"
  ]
  node [
    id 185
    label "odpowiedni"
  ]
  node [
    id 186
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 187
    label "typowy"
  ]
  node [
    id 188
    label "sezonowy"
  ]
  node [
    id 189
    label "zimowo"
  ]
  node [
    id 190
    label "ch&#322;odny"
  ]
  node [
    id 191
    label "hibernowy"
  ]
  node [
    id 192
    label "instalowa&#263;"
  ]
  node [
    id 193
    label "oprogramowanie"
  ]
  node [
    id 194
    label "odinstalowywa&#263;"
  ]
  node [
    id 195
    label "spis"
  ]
  node [
    id 196
    label "zaprezentowanie"
  ]
  node [
    id 197
    label "podprogram"
  ]
  node [
    id 198
    label "ogranicznik_referencyjny"
  ]
  node [
    id 199
    label "course_of_study"
  ]
  node [
    id 200
    label "booklet"
  ]
  node [
    id 201
    label "dzia&#322;"
  ]
  node [
    id 202
    label "odinstalowanie"
  ]
  node [
    id 203
    label "broszura"
  ]
  node [
    id 204
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 205
    label "kana&#322;"
  ]
  node [
    id 206
    label "teleferie"
  ]
  node [
    id 207
    label "zainstalowanie"
  ]
  node [
    id 208
    label "struktura_organizacyjna"
  ]
  node [
    id 209
    label "pirat"
  ]
  node [
    id 210
    label "zaprezentowa&#263;"
  ]
  node [
    id 211
    label "prezentowanie"
  ]
  node [
    id 212
    label "prezentowa&#263;"
  ]
  node [
    id 213
    label "interfejs"
  ]
  node [
    id 214
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 215
    label "okno"
  ]
  node [
    id 216
    label "blok"
  ]
  node [
    id 217
    label "punkt"
  ]
  node [
    id 218
    label "folder"
  ]
  node [
    id 219
    label "zainstalowa&#263;"
  ]
  node [
    id 220
    label "za&#322;o&#380;enie"
  ]
  node [
    id 221
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 222
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 223
    label "ram&#243;wka"
  ]
  node [
    id 224
    label "tryb"
  ]
  node [
    id 225
    label "emitowa&#263;"
  ]
  node [
    id 226
    label "emitowanie"
  ]
  node [
    id 227
    label "odinstalowywanie"
  ]
  node [
    id 228
    label "instrukcja"
  ]
  node [
    id 229
    label "informatyka"
  ]
  node [
    id 230
    label "deklaracja"
  ]
  node [
    id 231
    label "menu"
  ]
  node [
    id 232
    label "sekcja_krytyczna"
  ]
  node [
    id 233
    label "furkacja"
  ]
  node [
    id 234
    label "podstawa"
  ]
  node [
    id 235
    label "instalowanie"
  ]
  node [
    id 236
    label "oferta"
  ]
  node [
    id 237
    label "odinstalowa&#263;"
  ]
  node [
    id 238
    label "druk_ulotny"
  ]
  node [
    id 239
    label "wydawnictwo"
  ]
  node [
    id 240
    label "rozmiar"
  ]
  node [
    id 241
    label "zakres"
  ]
  node [
    id 242
    label "izochronizm"
  ]
  node [
    id 243
    label "zasi&#261;g"
  ]
  node [
    id 244
    label "bridge"
  ]
  node [
    id 245
    label "distribution"
  ]
  node [
    id 246
    label "pot&#281;ga"
  ]
  node [
    id 247
    label "documentation"
  ]
  node [
    id 248
    label "column"
  ]
  node [
    id 249
    label "zasadzenie"
  ]
  node [
    id 250
    label "punkt_odniesienia"
  ]
  node [
    id 251
    label "zasadzi&#263;"
  ]
  node [
    id 252
    label "bok"
  ]
  node [
    id 253
    label "d&#243;&#322;"
  ]
  node [
    id 254
    label "dzieci&#281;ctwo"
  ]
  node [
    id 255
    label "background"
  ]
  node [
    id 256
    label "podstawowy"
  ]
  node [
    id 257
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 258
    label "strategia"
  ]
  node [
    id 259
    label "pomys&#322;"
  ]
  node [
    id 260
    label "&#347;ciana"
  ]
  node [
    id 261
    label "p&#322;&#243;d"
  ]
  node [
    id 262
    label "work"
  ]
  node [
    id 263
    label "rezultat"
  ]
  node [
    id 264
    label "ko&#322;o"
  ]
  node [
    id 265
    label "spos&#243;b"
  ]
  node [
    id 266
    label "modalno&#347;&#263;"
  ]
  node [
    id 267
    label "z&#261;b"
  ]
  node [
    id 268
    label "kategoria_gramatyczna"
  ]
  node [
    id 269
    label "skala"
  ]
  node [
    id 270
    label "funkcjonowa&#263;"
  ]
  node [
    id 271
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 272
    label "koniugacja"
  ]
  node [
    id 273
    label "offer"
  ]
  node [
    id 274
    label "propozycja"
  ]
  node [
    id 275
    label "o&#347;wiadczenie"
  ]
  node [
    id 276
    label "obietnica"
  ]
  node [
    id 277
    label "formularz"
  ]
  node [
    id 278
    label "statement"
  ]
  node [
    id 279
    label "announcement"
  ]
  node [
    id 280
    label "akt"
  ]
  node [
    id 281
    label "digest"
  ]
  node [
    id 282
    label "konstrukcja"
  ]
  node [
    id 283
    label "dokument"
  ]
  node [
    id 284
    label "o&#347;wiadczyny"
  ]
  node [
    id 285
    label "szaniec"
  ]
  node [
    id 286
    label "topologia_magistrali"
  ]
  node [
    id 287
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 288
    label "grodzisko"
  ]
  node [
    id 289
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 290
    label "tarapaty"
  ]
  node [
    id 291
    label "piaskownik"
  ]
  node [
    id 292
    label "struktura_anatomiczna"
  ]
  node [
    id 293
    label "bystrza"
  ]
  node [
    id 294
    label "pit"
  ]
  node [
    id 295
    label "odk&#322;ad"
  ]
  node [
    id 296
    label "chody"
  ]
  node [
    id 297
    label "klarownia"
  ]
  node [
    id 298
    label "kanalizacja"
  ]
  node [
    id 299
    label "ciek"
  ]
  node [
    id 300
    label "teatr"
  ]
  node [
    id 301
    label "gara&#380;"
  ]
  node [
    id 302
    label "zrzutowy"
  ]
  node [
    id 303
    label "warsztat"
  ]
  node [
    id 304
    label "syfon"
  ]
  node [
    id 305
    label "odwa&#322;"
  ]
  node [
    id 306
    label "zbi&#243;r"
  ]
  node [
    id 307
    label "catalog"
  ]
  node [
    id 308
    label "pozycja"
  ]
  node [
    id 309
    label "tekst"
  ]
  node [
    id 310
    label "sumariusz"
  ]
  node [
    id 311
    label "book"
  ]
  node [
    id 312
    label "stock"
  ]
  node [
    id 313
    label "figurowa&#263;"
  ]
  node [
    id 314
    label "czynno&#347;&#263;"
  ]
  node [
    id 315
    label "wyliczanka"
  ]
  node [
    id 316
    label "zr&#243;&#380;nicowanie"
  ]
  node [
    id 317
    label "usuwanie"
  ]
  node [
    id 318
    label "usuni&#281;cie"
  ]
  node [
    id 319
    label "HP"
  ]
  node [
    id 320
    label "dost&#281;p"
  ]
  node [
    id 321
    label "infa"
  ]
  node [
    id 322
    label "kierunek"
  ]
  node [
    id 323
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 324
    label "kryptologia"
  ]
  node [
    id 325
    label "baza_danych"
  ]
  node [
    id 326
    label "przetwarzanie_informacji"
  ]
  node [
    id 327
    label "sztuczna_inteligencja"
  ]
  node [
    id 328
    label "gramatyka_formalna"
  ]
  node [
    id 329
    label "zamek"
  ]
  node [
    id 330
    label "dziedzina_informatyki"
  ]
  node [
    id 331
    label "przetwarzanie_j&#281;zyka_naturalnego"
  ]
  node [
    id 332
    label "artefakt"
  ]
  node [
    id 333
    label "dostosowa&#263;"
  ]
  node [
    id 334
    label "zrobi&#263;"
  ]
  node [
    id 335
    label "komputer"
  ]
  node [
    id 336
    label "install"
  ]
  node [
    id 337
    label "umie&#347;ci&#263;"
  ]
  node [
    id 338
    label "dostosowywa&#263;"
  ]
  node [
    id 339
    label "supply"
  ]
  node [
    id 340
    label "robi&#263;"
  ]
  node [
    id 341
    label "accommodate"
  ]
  node [
    id 342
    label "umieszcza&#263;"
  ]
  node [
    id 343
    label "fit"
  ]
  node [
    id 344
    label "usuwa&#263;"
  ]
  node [
    id 345
    label "zdolno&#347;&#263;"
  ]
  node [
    id 346
    label "usun&#261;&#263;"
  ]
  node [
    id 347
    label "dostosowanie"
  ]
  node [
    id 348
    label "installation"
  ]
  node [
    id 349
    label "pozak&#322;adanie"
  ]
  node [
    id 350
    label "proposition"
  ]
  node [
    id 351
    label "layout"
  ]
  node [
    id 352
    label "umieszczenie"
  ]
  node [
    id 353
    label "zrobienie"
  ]
  node [
    id 354
    label "parapet"
  ]
  node [
    id 355
    label "szyba"
  ]
  node [
    id 356
    label "okiennica"
  ]
  node [
    id 357
    label "prze&#347;wit"
  ]
  node [
    id 358
    label "pulpit"
  ]
  node [
    id 359
    label "transenna"
  ]
  node [
    id 360
    label "kwatera_okienna"
  ]
  node [
    id 361
    label "inspekt"
  ]
  node [
    id 362
    label "nora"
  ]
  node [
    id 363
    label "futryna"
  ]
  node [
    id 364
    label "nadokiennik"
  ]
  node [
    id 365
    label "skrzyd&#322;o"
  ]
  node [
    id 366
    label "lufcik"
  ]
  node [
    id 367
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 368
    label "casement"
  ]
  node [
    id 369
    label "menad&#380;er_okien"
  ]
  node [
    id 370
    label "otw&#243;r"
  ]
  node [
    id 371
    label "umieszczanie"
  ]
  node [
    id 372
    label "collection"
  ]
  node [
    id 373
    label "robienie"
  ]
  node [
    id 374
    label "wmontowanie"
  ]
  node [
    id 375
    label "wmontowywanie"
  ]
  node [
    id 376
    label "fitting"
  ]
  node [
    id 377
    label "dostosowywanie"
  ]
  node [
    id 378
    label "testify"
  ]
  node [
    id 379
    label "przedstawi&#263;"
  ]
  node [
    id 380
    label "zapozna&#263;"
  ]
  node [
    id 381
    label "pokaza&#263;"
  ]
  node [
    id 382
    label "represent"
  ]
  node [
    id 383
    label "typify"
  ]
  node [
    id 384
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 385
    label "uprzedzi&#263;"
  ]
  node [
    id 386
    label "attest"
  ]
  node [
    id 387
    label "wyra&#380;anie"
  ]
  node [
    id 388
    label "uprzedzanie"
  ]
  node [
    id 389
    label "representation"
  ]
  node [
    id 390
    label "zapoznawanie"
  ]
  node [
    id 391
    label "present"
  ]
  node [
    id 392
    label "przedstawianie"
  ]
  node [
    id 393
    label "display"
  ]
  node [
    id 394
    label "demonstrowanie"
  ]
  node [
    id 395
    label "presentation"
  ]
  node [
    id 396
    label "granie"
  ]
  node [
    id 397
    label "przest&#281;pca"
  ]
  node [
    id 398
    label "kopiowa&#263;"
  ]
  node [
    id 399
    label "podr&#243;bka"
  ]
  node [
    id 400
    label "kieruj&#261;cy"
  ]
  node [
    id 401
    label "&#380;agl&#243;wka"
  ]
  node [
    id 402
    label "rum"
  ]
  node [
    id 403
    label "rozb&#243;jnik"
  ]
  node [
    id 404
    label "postrzeleniec"
  ]
  node [
    id 405
    label "zapoznanie"
  ]
  node [
    id 406
    label "zapoznanie_si&#281;"
  ]
  node [
    id 407
    label "exhibit"
  ]
  node [
    id 408
    label "pokazanie"
  ]
  node [
    id 409
    label "wyst&#261;pienie"
  ]
  node [
    id 410
    label "uprzedzenie"
  ]
  node [
    id 411
    label "gra&#263;"
  ]
  node [
    id 412
    label "zapoznawa&#263;"
  ]
  node [
    id 413
    label "uprzedza&#263;"
  ]
  node [
    id 414
    label "wyra&#380;a&#263;"
  ]
  node [
    id 415
    label "przedstawia&#263;"
  ]
  node [
    id 416
    label "rynek"
  ]
  node [
    id 417
    label "energia"
  ]
  node [
    id 418
    label "wysy&#322;anie"
  ]
  node [
    id 419
    label "wys&#322;anie"
  ]
  node [
    id 420
    label "wydzielenie"
  ]
  node [
    id 421
    label "tembr"
  ]
  node [
    id 422
    label "wprowadzenie"
  ]
  node [
    id 423
    label "wydobycie"
  ]
  node [
    id 424
    label "wydzielanie"
  ]
  node [
    id 425
    label "wydobywanie"
  ]
  node [
    id 426
    label "nadawanie"
  ]
  node [
    id 427
    label "emission"
  ]
  node [
    id 428
    label "wprowadzanie"
  ]
  node [
    id 429
    label "nadanie"
  ]
  node [
    id 430
    label "issue"
  ]
  node [
    id 431
    label "nadawa&#263;"
  ]
  node [
    id 432
    label "wysy&#322;a&#263;"
  ]
  node [
    id 433
    label "nada&#263;"
  ]
  node [
    id 434
    label "air"
  ]
  node [
    id 435
    label "wydoby&#263;"
  ]
  node [
    id 436
    label "emit"
  ]
  node [
    id 437
    label "wys&#322;a&#263;"
  ]
  node [
    id 438
    label "wydzieli&#263;"
  ]
  node [
    id 439
    label "wydziela&#263;"
  ]
  node [
    id 440
    label "wprowadzi&#263;"
  ]
  node [
    id 441
    label "wydobywa&#263;"
  ]
  node [
    id 442
    label "wprowadza&#263;"
  ]
  node [
    id 443
    label "ulotka"
  ]
  node [
    id 444
    label "wskaz&#243;wka"
  ]
  node [
    id 445
    label "instruktarz"
  ]
  node [
    id 446
    label "routine"
  ]
  node [
    id 447
    label "proceduralnie"
  ]
  node [
    id 448
    label "danie"
  ]
  node [
    id 449
    label "od&#380;ywianie_si&#281;"
  ]
  node [
    id 450
    label "restauracja"
  ]
  node [
    id 451
    label "cennik"
  ]
  node [
    id 452
    label "chart"
  ]
  node [
    id 453
    label "zestaw"
  ]
  node [
    id 454
    label "bajt"
  ]
  node [
    id 455
    label "bloking"
  ]
  node [
    id 456
    label "j&#261;kanie"
  ]
  node [
    id 457
    label "przeszkoda"
  ]
  node [
    id 458
    label "zesp&#243;&#322;"
  ]
  node [
    id 459
    label "blokada"
  ]
  node [
    id 460
    label "bry&#322;a"
  ]
  node [
    id 461
    label "kontynent"
  ]
  node [
    id 462
    label "nastawnia"
  ]
  node [
    id 463
    label "blockage"
  ]
  node [
    id 464
    label "block"
  ]
  node [
    id 465
    label "organizacja"
  ]
  node [
    id 466
    label "budynek"
  ]
  node [
    id 467
    label "start"
  ]
  node [
    id 468
    label "skorupa_ziemska"
  ]
  node [
    id 469
    label "zeszyt"
  ]
  node [
    id 470
    label "grupa"
  ]
  node [
    id 471
    label "blokowisko"
  ]
  node [
    id 472
    label "artyku&#322;"
  ]
  node [
    id 473
    label "barak"
  ]
  node [
    id 474
    label "stok_kontynentalny"
  ]
  node [
    id 475
    label "whole"
  ]
  node [
    id 476
    label "square"
  ]
  node [
    id 477
    label "siatk&#243;wka"
  ]
  node [
    id 478
    label "kr&#261;g"
  ]
  node [
    id 479
    label "obrona"
  ]
  node [
    id 480
    label "ok&#322;adka"
  ]
  node [
    id 481
    label "bie&#380;nia"
  ]
  node [
    id 482
    label "referat"
  ]
  node [
    id 483
    label "dom_wielorodzinny"
  ]
  node [
    id 484
    label "jednostka_pami&#281;ci_komputera"
  ]
  node [
    id 485
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 486
    label "jednostka_organizacyjna"
  ]
  node [
    id 487
    label "urz&#261;d"
  ]
  node [
    id 488
    label "sfera"
  ]
  node [
    id 489
    label "miejsce_pracy"
  ]
  node [
    id 490
    label "insourcing"
  ]
  node [
    id 491
    label "stopie&#324;"
  ]
  node [
    id 492
    label "competence"
  ]
  node [
    id 493
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 494
    label "bezdro&#380;e"
  ]
  node [
    id 495
    label "poddzia&#322;"
  ]
  node [
    id 496
    label "podwini&#281;cie"
  ]
  node [
    id 497
    label "zap&#322;acenie"
  ]
  node [
    id 498
    label "przyodzianie"
  ]
  node [
    id 499
    label "budowla"
  ]
  node [
    id 500
    label "pokrycie"
  ]
  node [
    id 501
    label "rozebranie"
  ]
  node [
    id 502
    label "zak&#322;adka"
  ]
  node [
    id 503
    label "struktura"
  ]
  node [
    id 504
    label "poubieranie"
  ]
  node [
    id 505
    label "infliction"
  ]
  node [
    id 506
    label "spowodowanie"
  ]
  node [
    id 507
    label "przebranie"
  ]
  node [
    id 508
    label "przywdzianie"
  ]
  node [
    id 509
    label "obleczenie_si&#281;"
  ]
  node [
    id 510
    label "utworzenie"
  ]
  node [
    id 511
    label "str&#243;j"
  ]
  node [
    id 512
    label "twierdzenie"
  ]
  node [
    id 513
    label "obleczenie"
  ]
  node [
    id 514
    label "przygotowywanie"
  ]
  node [
    id 515
    label "przymierzenie"
  ]
  node [
    id 516
    label "wyko&#324;czenie"
  ]
  node [
    id 517
    label "przygotowanie"
  ]
  node [
    id 518
    label "przewidzenie"
  ]
  node [
    id 519
    label "po&#322;o&#380;enie"
  ]
  node [
    id 520
    label "sprawa"
  ]
  node [
    id 521
    label "ust&#281;p"
  ]
  node [
    id 522
    label "plan"
  ]
  node [
    id 523
    label "problemat"
  ]
  node [
    id 524
    label "plamka"
  ]
  node [
    id 525
    label "stopie&#324;_pisma"
  ]
  node [
    id 526
    label "jednostka"
  ]
  node [
    id 527
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 528
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 529
    label "mark"
  ]
  node [
    id 530
    label "chwila"
  ]
  node [
    id 531
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 532
    label "prosta"
  ]
  node [
    id 533
    label "problematyka"
  ]
  node [
    id 534
    label "obiekt"
  ]
  node [
    id 535
    label "zapunktowa&#263;"
  ]
  node [
    id 536
    label "podpunkt"
  ]
  node [
    id 537
    label "wojsko"
  ]
  node [
    id 538
    label "kres"
  ]
  node [
    id 539
    label "przestrze&#324;"
  ]
  node [
    id 540
    label "reengineering"
  ]
  node [
    id 541
    label "scheduling"
  ]
  node [
    id 542
    label "pi&#322;a_r&#281;czna"
  ]
  node [
    id 543
    label "okienko"
  ]
  node [
    id 544
    label "czyj&#347;"
  ]
  node [
    id 545
    label "m&#261;&#380;"
  ]
  node [
    id 546
    label "prywatny"
  ]
  node [
    id 547
    label "odkrywa&#263;"
  ]
  node [
    id 548
    label "urzeczywistnia&#263;"
  ]
  node [
    id 549
    label "undo"
  ]
  node [
    id 550
    label "przestawa&#263;"
  ]
  node [
    id 551
    label "uniewa&#380;nia&#263;"
  ]
  node [
    id 552
    label "cope"
  ]
  node [
    id 553
    label "&#380;y&#263;"
  ]
  node [
    id 554
    label "coating"
  ]
  node [
    id 555
    label "przebywa&#263;"
  ]
  node [
    id 556
    label "determine"
  ]
  node [
    id 557
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 558
    label "ko&#324;czy&#263;"
  ]
  node [
    id 559
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 560
    label "finish_up"
  ]
  node [
    id 561
    label "zrywa&#263;"
  ]
  node [
    id 562
    label "retract"
  ]
  node [
    id 563
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 564
    label "poznawa&#263;"
  ]
  node [
    id 565
    label "podnosi&#263;"
  ]
  node [
    id 566
    label "&#322;owca_talent&#243;w"
  ]
  node [
    id 567
    label "demaskator"
  ]
  node [
    id 568
    label "discovery"
  ]
  node [
    id 569
    label "zsuwa&#263;"
  ]
  node [
    id 570
    label "ukazywa&#263;"
  ]
  node [
    id 571
    label "objawia&#263;"
  ]
  node [
    id 572
    label "znajdowa&#263;"
  ]
  node [
    id 573
    label "unwrap"
  ]
  node [
    id 574
    label "informowa&#263;"
  ]
  node [
    id 575
    label "indicate"
  ]
  node [
    id 576
    label "przeprowadza&#263;"
  ]
  node [
    id 577
    label "prosecute"
  ]
  node [
    id 578
    label "prawdzi&#263;"
  ]
  node [
    id 579
    label "pozbywa&#263;_si&#281;"
  ]
  node [
    id 580
    label "zabija&#263;"
  ]
  node [
    id 581
    label "przesuwa&#263;"
  ]
  node [
    id 582
    label "rugowa&#263;"
  ]
  node [
    id 583
    label "powodowa&#263;"
  ]
  node [
    id 584
    label "blurt_out"
  ]
  node [
    id 585
    label "przenosi&#263;"
  ]
  node [
    id 586
    label "clash"
  ]
  node [
    id 587
    label "przeciwstawno&#347;&#263;"
  ]
  node [
    id 588
    label "przebiec"
  ]
  node [
    id 589
    label "charakter"
  ]
  node [
    id 590
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 591
    label "motyw"
  ]
  node [
    id 592
    label "przebiegni&#281;cie"
  ]
  node [
    id 593
    label "fabu&#322;a"
  ]
  node [
    id 594
    label "sprzecznia"
  ]
  node [
    id 595
    label "przeciwie&#324;stwo"
  ]
  node [
    id 596
    label "relacja"
  ]
  node [
    id 597
    label "sprzeczno&#347;&#263;"
  ]
  node [
    id 598
    label "zjawisko"
  ]
  node [
    id 599
    label "intencja"
  ]
  node [
    id 600
    label "device"
  ]
  node [
    id 601
    label "program_u&#380;ytkowy"
  ]
  node [
    id 602
    label "dokumentacja"
  ]
  node [
    id 603
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 604
    label "agreement"
  ]
  node [
    id 605
    label "thinking"
  ]
  node [
    id 606
    label "model"
  ]
  node [
    id 607
    label "rysunek"
  ]
  node [
    id 608
    label "reprezentacja"
  ]
  node [
    id 609
    label "dekoracja"
  ]
  node [
    id 610
    label "ekscerpcja"
  ]
  node [
    id 611
    label "materia&#322;"
  ]
  node [
    id 612
    label "operat"
  ]
  node [
    id 613
    label "kosztorys"
  ]
  node [
    id 614
    label "zapis"
  ]
  node [
    id 615
    label "&#347;wiadectwo"
  ]
  node [
    id 616
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 617
    label "parafa"
  ]
  node [
    id 618
    label "plik"
  ]
  node [
    id 619
    label "raport&#243;wka"
  ]
  node [
    id 620
    label "utw&#243;r"
  ]
  node [
    id 621
    label "record"
  ]
  node [
    id 622
    label "fascyku&#322;"
  ]
  node [
    id 623
    label "registratura"
  ]
  node [
    id 624
    label "writing"
  ]
  node [
    id 625
    label "sygnatariusz"
  ]
  node [
    id 626
    label "idea"
  ]
  node [
    id 627
    label "pocz&#261;tki"
  ]
  node [
    id 628
    label "ukradzenie"
  ]
  node [
    id 629
    label "ukra&#347;&#263;"
  ]
  node [
    id 630
    label "system"
  ]
  node [
    id 631
    label "Justyn"
  ]
  node [
    id 632
    label "Piotr"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 631
  ]
  edge [
    source 1
    target 632
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 192
  ]
  edge [
    source 8
    target 193
  ]
  edge [
    source 8
    target 194
  ]
  edge [
    source 8
    target 195
  ]
  edge [
    source 8
    target 196
  ]
  edge [
    source 8
    target 197
  ]
  edge [
    source 8
    target 198
  ]
  edge [
    source 8
    target 199
  ]
  edge [
    source 8
    target 200
  ]
  edge [
    source 8
    target 201
  ]
  edge [
    source 8
    target 202
  ]
  edge [
    source 8
    target 203
  ]
  edge [
    source 8
    target 149
  ]
  edge [
    source 8
    target 204
  ]
  edge [
    source 8
    target 205
  ]
  edge [
    source 8
    target 206
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 208
  ]
  edge [
    source 8
    target 209
  ]
  edge [
    source 8
    target 210
  ]
  edge [
    source 8
    target 211
  ]
  edge [
    source 8
    target 212
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 214
  ]
  edge [
    source 8
    target 215
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 217
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 219
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 221
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 224
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 226
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 151
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 8
    target 161
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 303
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 8
    target 334
  ]
  edge [
    source 8
    target 335
  ]
  edge [
    source 8
    target 336
  ]
  edge [
    source 8
    target 337
  ]
  edge [
    source 8
    target 338
  ]
  edge [
    source 8
    target 339
  ]
  edge [
    source 8
    target 340
  ]
  edge [
    source 8
    target 341
  ]
  edge [
    source 8
    target 342
  ]
  edge [
    source 8
    target 343
  ]
  edge [
    source 8
    target 344
  ]
  edge [
    source 8
    target 345
  ]
  edge [
    source 8
    target 346
  ]
  edge [
    source 8
    target 347
  ]
  edge [
    source 8
    target 348
  ]
  edge [
    source 8
    target 349
  ]
  edge [
    source 8
    target 350
  ]
  edge [
    source 8
    target 351
  ]
  edge [
    source 8
    target 352
  ]
  edge [
    source 8
    target 353
  ]
  edge [
    source 8
    target 354
  ]
  edge [
    source 8
    target 355
  ]
  edge [
    source 8
    target 356
  ]
  edge [
    source 8
    target 357
  ]
  edge [
    source 8
    target 358
  ]
  edge [
    source 8
    target 359
  ]
  edge [
    source 8
    target 360
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 362
  ]
  edge [
    source 8
    target 363
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 365
  ]
  edge [
    source 8
    target 366
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 368
  ]
  edge [
    source 8
    target 369
  ]
  edge [
    source 8
    target 370
  ]
  edge [
    source 8
    target 371
  ]
  edge [
    source 8
    target 372
  ]
  edge [
    source 8
    target 373
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 167
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 164
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 8
    target 477
  ]
  edge [
    source 8
    target 478
  ]
  edge [
    source 8
    target 479
  ]
  edge [
    source 8
    target 480
  ]
  edge [
    source 8
    target 481
  ]
  edge [
    source 8
    target 482
  ]
  edge [
    source 8
    target 483
  ]
  edge [
    source 8
    target 484
  ]
  edge [
    source 8
    target 485
  ]
  edge [
    source 8
    target 486
  ]
  edge [
    source 8
    target 487
  ]
  edge [
    source 8
    target 488
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 168
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 344
  ]
  edge [
    source 10
    target 547
  ]
  edge [
    source 10
    target 548
  ]
  edge [
    source 10
    target 549
  ]
  edge [
    source 10
    target 550
  ]
  edge [
    source 10
    target 551
  ]
  edge [
    source 10
    target 552
  ]
  edge [
    source 10
    target 553
  ]
  edge [
    source 10
    target 554
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 556
  ]
  edge [
    source 10
    target 557
  ]
  edge [
    source 10
    target 558
  ]
  edge [
    source 10
    target 559
  ]
  edge [
    source 10
    target 560
  ]
  edge [
    source 10
    target 561
  ]
  edge [
    source 10
    target 562
  ]
  edge [
    source 10
    target 563
  ]
  edge [
    source 10
    target 564
  ]
  edge [
    source 10
    target 565
  ]
  edge [
    source 10
    target 566
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 567
  ]
  edge [
    source 10
    target 568
  ]
  edge [
    source 10
    target 569
  ]
  edge [
    source 10
    target 570
  ]
  edge [
    source 10
    target 571
  ]
  edge [
    source 10
    target 572
  ]
  edge [
    source 10
    target 573
  ]
  edge [
    source 10
    target 574
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 576
  ]
  edge [
    source 10
    target 577
  ]
  edge [
    source 10
    target 578
  ]
  edge [
    source 10
    target 579
  ]
  edge [
    source 10
    target 580
  ]
  edge [
    source 10
    target 340
  ]
  edge [
    source 10
    target 581
  ]
  edge [
    source 10
    target 582
  ]
  edge [
    source 10
    target 583
  ]
  edge [
    source 10
    target 584
  ]
  edge [
    source 10
    target 585
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 586
  ]
  edge [
    source 11
    target 587
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 588
  ]
  edge [
    source 11
    target 589
  ]
  edge [
    source 11
    target 314
  ]
  edge [
    source 11
    target 590
  ]
  edge [
    source 11
    target 591
  ]
  edge [
    source 11
    target 592
  ]
  edge [
    source 11
    target 593
  ]
  edge [
    source 11
    target 594
  ]
  edge [
    source 11
    target 595
  ]
  edge [
    source 11
    target 596
  ]
  edge [
    source 11
    target 597
  ]
  edge [
    source 11
    target 598
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 44
  ]
  edge [
    source 12
    target 45
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 47
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 49
  ]
  edge [
    source 12
    target 50
  ]
  edge [
    source 12
    target 51
  ]
  edge [
    source 12
    target 52
  ]
  edge [
    source 12
    target 53
  ]
  edge [
    source 12
    target 54
  ]
  edge [
    source 12
    target 55
  ]
  edge [
    source 12
    target 56
  ]
  edge [
    source 12
    target 57
  ]
  edge [
    source 12
    target 58
  ]
  edge [
    source 12
    target 59
  ]
  edge [
    source 12
    target 60
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 13
    target 599
  ]
  edge [
    source 13
    target 522
  ]
  edge [
    source 13
    target 600
  ]
  edge [
    source 13
    target 601
  ]
  edge [
    source 13
    target 259
  ]
  edge [
    source 13
    target 602
  ]
  edge [
    source 13
    target 603
  ]
  edge [
    source 13
    target 604
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 605
  ]
  edge [
    source 13
    target 606
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 607
  ]
  edge [
    source 13
    target 489
  ]
  edge [
    source 13
    target 539
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 609
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 610
  ]
  edge [
    source 13
    target 611
  ]
  edge [
    source 13
    target 612
  ]
  edge [
    source 13
    target 613
  ]
  edge [
    source 13
    target 614
  ]
  edge [
    source 13
    target 615
  ]
  edge [
    source 13
    target 616
  ]
  edge [
    source 13
    target 617
  ]
  edge [
    source 13
    target 618
  ]
  edge [
    source 13
    target 619
  ]
  edge [
    source 13
    target 620
  ]
  edge [
    source 13
    target 621
  ]
  edge [
    source 13
    target 622
  ]
  edge [
    source 13
    target 623
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 624
  ]
  edge [
    source 13
    target 625
  ]
  edge [
    source 13
    target 626
  ]
  edge [
    source 13
    target 627
  ]
  edge [
    source 13
    target 628
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 630
  ]
]
