graph [
  node [
    id 0
    label "pkn"
    origin "text"
  ]
  node [
    id 1
    label "orlen"
    origin "text"
  ]
  node [
    id 2
    label "potwierdzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "oficjalny"
    origin "text"
  ]
  node [
    id 5
    label "partner"
    origin "text"
  ]
  node [
    id 6
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 7
    label "formu&#322;a"
    origin "text"
  ]
  node [
    id 8
    label "williams"
    origin "text"
  ]
  node [
    id 9
    label "martini"
    origin "text"
  ]
  node [
    id 10
    label "racing"
    origin "text"
  ]
  node [
    id 11
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 12
    label "przysz&#322;y"
    origin "text"
  ]
  node [
    id 13
    label "rok"
    origin "text"
  ]
  node [
    id 14
    label "by&#263;"
    origin "text"
  ]
  node [
    id 15
    label "si&#281;"
    origin "text"
  ]
  node [
    id 16
    label "&#347;ciga&#263;"
    origin "text"
  ]
  node [
    id 17
    label "robert"
    origin "text"
  ]
  node [
    id 18
    label "kubica"
    origin "text"
  ]
  node [
    id 19
    label "acknowledge"
  ]
  node [
    id 20
    label "zgodzi&#263;_si&#281;"
  ]
  node [
    id 21
    label "stwierdzi&#263;"
  ]
  node [
    id 22
    label "przy&#347;wiadczy&#263;"
  ]
  node [
    id 23
    label "attest"
  ]
  node [
    id 24
    label "testify"
  ]
  node [
    id 25
    label "powiedzie&#263;"
  ]
  node [
    id 26
    label "uzna&#263;"
  ]
  node [
    id 27
    label "oznajmi&#263;"
  ]
  node [
    id 28
    label "declare"
  ]
  node [
    id 29
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 30
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 31
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 32
    label "osta&#263;_si&#281;"
  ]
  node [
    id 33
    label "change"
  ]
  node [
    id 34
    label "pozosta&#263;"
  ]
  node [
    id 35
    label "catch"
  ]
  node [
    id 36
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 37
    label "proceed"
  ]
  node [
    id 38
    label "support"
  ]
  node [
    id 39
    label "prze&#380;y&#263;"
  ]
  node [
    id 40
    label "utrzyma&#263;_si&#281;"
  ]
  node [
    id 41
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 42
    label "formalizowanie"
  ]
  node [
    id 43
    label "formalnie"
  ]
  node [
    id 44
    label "oficjalnie"
  ]
  node [
    id 45
    label "jawny"
  ]
  node [
    id 46
    label "legalny"
  ]
  node [
    id 47
    label "sformalizowanie"
  ]
  node [
    id 48
    label "pow&#347;ci&#261;gliwie"
  ]
  node [
    id 49
    label "ujawnienie_si&#281;"
  ]
  node [
    id 50
    label "ujawnianie_si&#281;"
  ]
  node [
    id 51
    label "zdecydowany"
  ]
  node [
    id 52
    label "znajomy"
  ]
  node [
    id 53
    label "ujawnienie"
  ]
  node [
    id 54
    label "jawnie"
  ]
  node [
    id 55
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 56
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 57
    label "ujawnianie"
  ]
  node [
    id 58
    label "ewidentny"
  ]
  node [
    id 59
    label "gajny"
  ]
  node [
    id 60
    label "legalnie"
  ]
  node [
    id 61
    label "formalny"
  ]
  node [
    id 62
    label "regularly"
  ]
  node [
    id 63
    label "pozornie"
  ]
  node [
    id 64
    label "kompletnie"
  ]
  node [
    id 65
    label "nadanie"
  ]
  node [
    id 66
    label "sprecyzowanie"
  ]
  node [
    id 67
    label "j&#281;zyk"
  ]
  node [
    id 68
    label "nadawanie"
  ]
  node [
    id 69
    label "precyzowanie"
  ]
  node [
    id 70
    label "pracownik"
  ]
  node [
    id 71
    label "przedsi&#281;biorca"
  ]
  node [
    id 72
    label "cz&#322;owiek"
  ]
  node [
    id 73
    label "wsp&#243;&#322;partner"
  ]
  node [
    id 74
    label "wsp&#243;&#322;uczestnik"
  ]
  node [
    id 75
    label "kolaborator"
  ]
  node [
    id 76
    label "prowadzi&#263;"
  ]
  node [
    id 77
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 78
    label "sp&#243;lnik"
  ]
  node [
    id 79
    label "aktor"
  ]
  node [
    id 80
    label "uczestniczenie"
  ]
  node [
    id 81
    label "ludzko&#347;&#263;"
  ]
  node [
    id 82
    label "asymilowanie"
  ]
  node [
    id 83
    label "wapniak"
  ]
  node [
    id 84
    label "asymilowa&#263;"
  ]
  node [
    id 85
    label "os&#322;abia&#263;"
  ]
  node [
    id 86
    label "posta&#263;"
  ]
  node [
    id 87
    label "hominid"
  ]
  node [
    id 88
    label "podw&#322;adny"
  ]
  node [
    id 89
    label "os&#322;abianie"
  ]
  node [
    id 90
    label "g&#322;owa"
  ]
  node [
    id 91
    label "figura"
  ]
  node [
    id 92
    label "portrecista"
  ]
  node [
    id 93
    label "dwun&#243;g"
  ]
  node [
    id 94
    label "profanum"
  ]
  node [
    id 95
    label "mikrokosmos"
  ]
  node [
    id 96
    label "nasada"
  ]
  node [
    id 97
    label "duch"
  ]
  node [
    id 98
    label "antropochoria"
  ]
  node [
    id 99
    label "osoba"
  ]
  node [
    id 100
    label "wz&#243;r"
  ]
  node [
    id 101
    label "senior"
  ]
  node [
    id 102
    label "oddzia&#322;ywanie"
  ]
  node [
    id 103
    label "Adam"
  ]
  node [
    id 104
    label "homo_sapiens"
  ]
  node [
    id 105
    label "polifag"
  ]
  node [
    id 106
    label "podmiot"
  ]
  node [
    id 107
    label "wykupienie"
  ]
  node [
    id 108
    label "bycie_w_posiadaniu"
  ]
  node [
    id 109
    label "wykupywanie"
  ]
  node [
    id 110
    label "teatr"
  ]
  node [
    id 111
    label "Roland_Topor"
  ]
  node [
    id 112
    label "odtw&#243;rca"
  ]
  node [
    id 113
    label "Daniel_Olbrychski"
  ]
  node [
    id 114
    label "uczestnik"
  ]
  node [
    id 115
    label "fanfaron"
  ]
  node [
    id 116
    label "bajerant"
  ]
  node [
    id 117
    label "Eastwood"
  ]
  node [
    id 118
    label "wykonawca"
  ]
  node [
    id 119
    label "Allen"
  ]
  node [
    id 120
    label "Stuhr"
  ]
  node [
    id 121
    label "interpretator"
  ]
  node [
    id 122
    label "obsada"
  ]
  node [
    id 123
    label "salariat"
  ]
  node [
    id 124
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 125
    label "delegowanie"
  ]
  node [
    id 126
    label "pracu&#347;"
  ]
  node [
    id 127
    label "r&#281;ka"
  ]
  node [
    id 128
    label "delegowa&#263;"
  ]
  node [
    id 129
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 130
    label "wsp&#243;lnik"
  ]
  node [
    id 131
    label "wsp&#243;&#322;pracownik"
  ]
  node [
    id 132
    label "w&#322;&#261;czanie"
  ]
  node [
    id 133
    label "bycie"
  ]
  node [
    id 134
    label "robienie"
  ]
  node [
    id 135
    label "participation"
  ]
  node [
    id 136
    label "czynno&#347;&#263;"
  ]
  node [
    id 137
    label "wydawca"
  ]
  node [
    id 138
    label "kapitalista"
  ]
  node [
    id 139
    label "klasa_&#347;rednia"
  ]
  node [
    id 140
    label "osoba_fizyczna"
  ]
  node [
    id 141
    label "&#380;y&#263;"
  ]
  node [
    id 142
    label "robi&#263;"
  ]
  node [
    id 143
    label "kierowa&#263;"
  ]
  node [
    id 144
    label "g&#243;rowa&#263;"
  ]
  node [
    id 145
    label "tworzy&#263;"
  ]
  node [
    id 146
    label "krzywa"
  ]
  node [
    id 147
    label "linia_melodyczna"
  ]
  node [
    id 148
    label "control"
  ]
  node [
    id 149
    label "string"
  ]
  node [
    id 150
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 151
    label "ukierunkowywa&#263;"
  ]
  node [
    id 152
    label "sterowa&#263;"
  ]
  node [
    id 153
    label "kre&#347;li&#263;"
  ]
  node [
    id 154
    label "przewy&#380;sza&#263;"
  ]
  node [
    id 155
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 156
    label "message"
  ]
  node [
    id 157
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 158
    label "obs&#322;ugiwa&#263;"
  ]
  node [
    id 159
    label "eksponowa&#263;"
  ]
  node [
    id 160
    label "navigate"
  ]
  node [
    id 161
    label "manipulate"
  ]
  node [
    id 162
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 163
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 164
    label "przesuwa&#263;"
  ]
  node [
    id 165
    label "prowadzenie"
  ]
  node [
    id 166
    label "powodowa&#263;"
  ]
  node [
    id 167
    label "Mazowsze"
  ]
  node [
    id 168
    label "odm&#322;adzanie"
  ]
  node [
    id 169
    label "&#346;wietliki"
  ]
  node [
    id 170
    label "zbi&#243;r"
  ]
  node [
    id 171
    label "whole"
  ]
  node [
    id 172
    label "skupienie"
  ]
  node [
    id 173
    label "The_Beatles"
  ]
  node [
    id 174
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 175
    label "odm&#322;adza&#263;"
  ]
  node [
    id 176
    label "zabudowania"
  ]
  node [
    id 177
    label "group"
  ]
  node [
    id 178
    label "zespolik"
  ]
  node [
    id 179
    label "schorzenie"
  ]
  node [
    id 180
    label "ro&#347;lina"
  ]
  node [
    id 181
    label "grupa"
  ]
  node [
    id 182
    label "Depeche_Mode"
  ]
  node [
    id 183
    label "batch"
  ]
  node [
    id 184
    label "odm&#322;odzenie"
  ]
  node [
    id 185
    label "liga"
  ]
  node [
    id 186
    label "jednostka_systematyczna"
  ]
  node [
    id 187
    label "gromada"
  ]
  node [
    id 188
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 189
    label "egzemplarz"
  ]
  node [
    id 190
    label "Entuzjastki"
  ]
  node [
    id 191
    label "kompozycja"
  ]
  node [
    id 192
    label "Terranie"
  ]
  node [
    id 193
    label "category"
  ]
  node [
    id 194
    label "pakiet_klimatyczny"
  ]
  node [
    id 195
    label "oddzia&#322;"
  ]
  node [
    id 196
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 197
    label "cz&#261;steczka"
  ]
  node [
    id 198
    label "stage_set"
  ]
  node [
    id 199
    label "type"
  ]
  node [
    id 200
    label "specgrupa"
  ]
  node [
    id 201
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 202
    label "Eurogrupa"
  ]
  node [
    id 203
    label "formacja_geologiczna"
  ]
  node [
    id 204
    label "harcerze_starsi"
  ]
  node [
    id 205
    label "ognisko"
  ]
  node [
    id 206
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 207
    label "powalenie"
  ]
  node [
    id 208
    label "odezwanie_si&#281;"
  ]
  node [
    id 209
    label "atakowanie"
  ]
  node [
    id 210
    label "grupa_ryzyka"
  ]
  node [
    id 211
    label "przypadek"
  ]
  node [
    id 212
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 213
    label "nabawienie_si&#281;"
  ]
  node [
    id 214
    label "inkubacja"
  ]
  node [
    id 215
    label "kryzys"
  ]
  node [
    id 216
    label "powali&#263;"
  ]
  node [
    id 217
    label "remisja"
  ]
  node [
    id 218
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 219
    label "zajmowa&#263;"
  ]
  node [
    id 220
    label "zaburzenie"
  ]
  node [
    id 221
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 222
    label "badanie_histopatologiczne"
  ]
  node [
    id 223
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 224
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 225
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 226
    label "odzywanie_si&#281;"
  ]
  node [
    id 227
    label "diagnoza"
  ]
  node [
    id 228
    label "atakowa&#263;"
  ]
  node [
    id 229
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 230
    label "nabawianie_si&#281;"
  ]
  node [
    id 231
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 232
    label "zajmowanie"
  ]
  node [
    id 233
    label "series"
  ]
  node [
    id 234
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 235
    label "uprawianie"
  ]
  node [
    id 236
    label "praca_rolnicza"
  ]
  node [
    id 237
    label "collection"
  ]
  node [
    id 238
    label "dane"
  ]
  node [
    id 239
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 240
    label "poj&#281;cie"
  ]
  node [
    id 241
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 242
    label "sum"
  ]
  node [
    id 243
    label "gathering"
  ]
  node [
    id 244
    label "album"
  ]
  node [
    id 245
    label "agglomeration"
  ]
  node [
    id 246
    label "uwaga"
  ]
  node [
    id 247
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 248
    label "przegrupowanie"
  ]
  node [
    id 249
    label "spowodowanie"
  ]
  node [
    id 250
    label "congestion"
  ]
  node [
    id 251
    label "zgromadzenie"
  ]
  node [
    id 252
    label "kupienie"
  ]
  node [
    id 253
    label "z&#322;&#261;czenie"
  ]
  node [
    id 254
    label "po&#322;&#261;czenie"
  ]
  node [
    id 255
    label "concentration"
  ]
  node [
    id 256
    label "kompleks"
  ]
  node [
    id 257
    label "obszar"
  ]
  node [
    id 258
    label "Polska"
  ]
  node [
    id 259
    label "Kurpie"
  ]
  node [
    id 260
    label "Mogielnica"
  ]
  node [
    id 261
    label "odtwarzanie"
  ]
  node [
    id 262
    label "uatrakcyjnianie"
  ]
  node [
    id 263
    label "zast&#281;powanie"
  ]
  node [
    id 264
    label "odbudowywanie"
  ]
  node [
    id 265
    label "rejuvenation"
  ]
  node [
    id 266
    label "m&#322;odszy"
  ]
  node [
    id 267
    label "odbudowywa&#263;"
  ]
  node [
    id 268
    label "m&#322;odzi&#263;"
  ]
  node [
    id 269
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 270
    label "przewietrza&#263;"
  ]
  node [
    id 271
    label "wymienia&#263;"
  ]
  node [
    id 272
    label "odtwarza&#263;"
  ]
  node [
    id 273
    label "uatrakcyjni&#263;"
  ]
  node [
    id 274
    label "przewietrzy&#263;"
  ]
  node [
    id 275
    label "regenerate"
  ]
  node [
    id 276
    label "odtworzy&#263;"
  ]
  node [
    id 277
    label "wymieni&#263;"
  ]
  node [
    id 278
    label "odbudowa&#263;"
  ]
  node [
    id 279
    label "wymienienie"
  ]
  node [
    id 280
    label "uatrakcyjnienie"
  ]
  node [
    id 281
    label "odbudowanie"
  ]
  node [
    id 282
    label "odtworzenie"
  ]
  node [
    id 283
    label "zbiorowisko"
  ]
  node [
    id 284
    label "ro&#347;liny"
  ]
  node [
    id 285
    label "p&#281;d"
  ]
  node [
    id 286
    label "wegetowanie"
  ]
  node [
    id 287
    label "zadziorek"
  ]
  node [
    id 288
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 289
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 290
    label "do&#322;owa&#263;"
  ]
  node [
    id 291
    label "wegetacja"
  ]
  node [
    id 292
    label "owoc"
  ]
  node [
    id 293
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 294
    label "strzyc"
  ]
  node [
    id 295
    label "w&#322;&#243;kno"
  ]
  node [
    id 296
    label "g&#322;uszenie"
  ]
  node [
    id 297
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 298
    label "fitotron"
  ]
  node [
    id 299
    label "bulwka"
  ]
  node [
    id 300
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 301
    label "odn&#243;&#380;ka"
  ]
  node [
    id 302
    label "epiderma"
  ]
  node [
    id 303
    label "gumoza"
  ]
  node [
    id 304
    label "strzy&#380;enie"
  ]
  node [
    id 305
    label "wypotnik"
  ]
  node [
    id 306
    label "flawonoid"
  ]
  node [
    id 307
    label "wyro&#347;le"
  ]
  node [
    id 308
    label "do&#322;owanie"
  ]
  node [
    id 309
    label "g&#322;uszy&#263;"
  ]
  node [
    id 310
    label "pora&#380;a&#263;"
  ]
  node [
    id 311
    label "fitocenoza"
  ]
  node [
    id 312
    label "hodowla"
  ]
  node [
    id 313
    label "fotoautotrof"
  ]
  node [
    id 314
    label "nieuleczalnie_chory"
  ]
  node [
    id 315
    label "wegetowa&#263;"
  ]
  node [
    id 316
    label "pochewka"
  ]
  node [
    id 317
    label "sok"
  ]
  node [
    id 318
    label "system_korzeniowy"
  ]
  node [
    id 319
    label "zawi&#261;zek"
  ]
  node [
    id 320
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 321
    label "zapis"
  ]
  node [
    id 322
    label "formularz"
  ]
  node [
    id 323
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 324
    label "sformu&#322;owanie"
  ]
  node [
    id 325
    label "kultura_duchowa"
  ]
  node [
    id 326
    label "rule"
  ]
  node [
    id 327
    label "kultura"
  ]
  node [
    id 328
    label "ceremony"
  ]
  node [
    id 329
    label "tradycja"
  ]
  node [
    id 330
    label "asymilowanie_si&#281;"
  ]
  node [
    id 331
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 332
    label "Wsch&#243;d"
  ]
  node [
    id 333
    label "przedmiot"
  ]
  node [
    id 334
    label "przejmowanie"
  ]
  node [
    id 335
    label "zjawisko"
  ]
  node [
    id 336
    label "cecha"
  ]
  node [
    id 337
    label "makrokosmos"
  ]
  node [
    id 338
    label "rzecz"
  ]
  node [
    id 339
    label "konwencja"
  ]
  node [
    id 340
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 341
    label "propriety"
  ]
  node [
    id 342
    label "przejmowa&#263;"
  ]
  node [
    id 343
    label "brzoskwiniarnia"
  ]
  node [
    id 344
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 345
    label "sztuka"
  ]
  node [
    id 346
    label "zwyczaj"
  ]
  node [
    id 347
    label "jako&#347;&#263;"
  ]
  node [
    id 348
    label "kuchnia"
  ]
  node [
    id 349
    label "populace"
  ]
  node [
    id 350
    label "religia"
  ]
  node [
    id 351
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 352
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 353
    label "przej&#281;cie"
  ]
  node [
    id 354
    label "przej&#261;&#263;"
  ]
  node [
    id 355
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 356
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 357
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 358
    label "wording"
  ]
  node [
    id 359
    label "wypowied&#378;"
  ]
  node [
    id 360
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 361
    label "statement"
  ]
  node [
    id 362
    label "zapisanie"
  ]
  node [
    id 363
    label "rzucenie"
  ]
  node [
    id 364
    label "poinformowanie"
  ]
  node [
    id 365
    label "spos&#243;b"
  ]
  node [
    id 366
    label "wytw&#243;r"
  ]
  node [
    id 367
    label "entrance"
  ]
  node [
    id 368
    label "wpis"
  ]
  node [
    id 369
    label "normalizacja"
  ]
  node [
    id 370
    label "dokument"
  ]
  node [
    id 371
    label "zestaw"
  ]
  node [
    id 372
    label "wermut"
  ]
  node [
    id 373
    label "short_drink"
  ]
  node [
    id 374
    label "wino_zio&#322;owe"
  ]
  node [
    id 375
    label "wino_wzmacniane"
  ]
  node [
    id 376
    label "kolejny"
  ]
  node [
    id 377
    label "nast&#281;pnie"
  ]
  node [
    id 378
    label "inny"
  ]
  node [
    id 379
    label "nastopny"
  ]
  node [
    id 380
    label "kolejno"
  ]
  node [
    id 381
    label "kt&#243;ry&#347;"
  ]
  node [
    id 382
    label "p&#243;&#322;rocze"
  ]
  node [
    id 383
    label "martwy_sezon"
  ]
  node [
    id 384
    label "kalendarz"
  ]
  node [
    id 385
    label "cykl_astronomiczny"
  ]
  node [
    id 386
    label "lata"
  ]
  node [
    id 387
    label "pora_roku"
  ]
  node [
    id 388
    label "stulecie"
  ]
  node [
    id 389
    label "kurs"
  ]
  node [
    id 390
    label "czas"
  ]
  node [
    id 391
    label "jubileusz"
  ]
  node [
    id 392
    label "kwarta&#322;"
  ]
  node [
    id 393
    label "miesi&#261;c"
  ]
  node [
    id 394
    label "summer"
  ]
  node [
    id 395
    label "poprzedzanie"
  ]
  node [
    id 396
    label "czasoprzestrze&#324;"
  ]
  node [
    id 397
    label "laba"
  ]
  node [
    id 398
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 399
    label "chronometria"
  ]
  node [
    id 400
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 401
    label "rachuba_czasu"
  ]
  node [
    id 402
    label "przep&#322;ywanie"
  ]
  node [
    id 403
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 404
    label "czasokres"
  ]
  node [
    id 405
    label "odczyt"
  ]
  node [
    id 406
    label "chwila"
  ]
  node [
    id 407
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 408
    label "dzieje"
  ]
  node [
    id 409
    label "kategoria_gramatyczna"
  ]
  node [
    id 410
    label "poprzedzenie"
  ]
  node [
    id 411
    label "trawienie"
  ]
  node [
    id 412
    label "pochodzi&#263;"
  ]
  node [
    id 413
    label "period"
  ]
  node [
    id 414
    label "okres_czasu"
  ]
  node [
    id 415
    label "poprzedza&#263;"
  ]
  node [
    id 416
    label "schy&#322;ek"
  ]
  node [
    id 417
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 418
    label "odwlekanie_si&#281;"
  ]
  node [
    id 419
    label "zegar"
  ]
  node [
    id 420
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 421
    label "czwarty_wymiar"
  ]
  node [
    id 422
    label "pochodzenie"
  ]
  node [
    id 423
    label "koniugacja"
  ]
  node [
    id 424
    label "Zeitgeist"
  ]
  node [
    id 425
    label "trawi&#263;"
  ]
  node [
    id 426
    label "pogoda"
  ]
  node [
    id 427
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 428
    label "poprzedzi&#263;"
  ]
  node [
    id 429
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 430
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 431
    label "time_period"
  ]
  node [
    id 432
    label "term"
  ]
  node [
    id 433
    label "rok_akademicki"
  ]
  node [
    id 434
    label "rok_szkolny"
  ]
  node [
    id 435
    label "semester"
  ]
  node [
    id 436
    label "anniwersarz"
  ]
  node [
    id 437
    label "rocznica"
  ]
  node [
    id 438
    label "tydzie&#324;"
  ]
  node [
    id 439
    label "miech"
  ]
  node [
    id 440
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 441
    label "kalendy"
  ]
  node [
    id 442
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 443
    label "long_time"
  ]
  node [
    id 444
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 445
    label "almanac"
  ]
  node [
    id 446
    label "rozk&#322;ad"
  ]
  node [
    id 447
    label "wydawnictwo"
  ]
  node [
    id 448
    label "Juliusz_Cezar"
  ]
  node [
    id 449
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 450
    label "zwy&#380;kowanie"
  ]
  node [
    id 451
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 452
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 453
    label "zaj&#281;cia"
  ]
  node [
    id 454
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 455
    label "trasa"
  ]
  node [
    id 456
    label "przeorientowywanie"
  ]
  node [
    id 457
    label "przejazd"
  ]
  node [
    id 458
    label "kierunek"
  ]
  node [
    id 459
    label "przeorientowywa&#263;"
  ]
  node [
    id 460
    label "nauka"
  ]
  node [
    id 461
    label "przeorientowanie"
  ]
  node [
    id 462
    label "klasa"
  ]
  node [
    id 463
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 464
    label "przeorientowa&#263;"
  ]
  node [
    id 465
    label "manner"
  ]
  node [
    id 466
    label "course"
  ]
  node [
    id 467
    label "passage"
  ]
  node [
    id 468
    label "zni&#380;kowanie"
  ]
  node [
    id 469
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 470
    label "seria"
  ]
  node [
    id 471
    label "stawka"
  ]
  node [
    id 472
    label "way"
  ]
  node [
    id 473
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 474
    label "deprecjacja"
  ]
  node [
    id 475
    label "cedu&#322;a"
  ]
  node [
    id 476
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 477
    label "drive"
  ]
  node [
    id 478
    label "bearing"
  ]
  node [
    id 479
    label "Lira"
  ]
  node [
    id 480
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 481
    label "mie&#263;_miejsce"
  ]
  node [
    id 482
    label "equal"
  ]
  node [
    id 483
    label "trwa&#263;"
  ]
  node [
    id 484
    label "chodzi&#263;"
  ]
  node [
    id 485
    label "si&#281;ga&#263;"
  ]
  node [
    id 486
    label "stan"
  ]
  node [
    id 487
    label "obecno&#347;&#263;"
  ]
  node [
    id 488
    label "stand"
  ]
  node [
    id 489
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 490
    label "uczestniczy&#263;"
  ]
  node [
    id 491
    label "participate"
  ]
  node [
    id 492
    label "istnie&#263;"
  ]
  node [
    id 493
    label "pozostawa&#263;"
  ]
  node [
    id 494
    label "zostawa&#263;"
  ]
  node [
    id 495
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 496
    label "adhere"
  ]
  node [
    id 497
    label "compass"
  ]
  node [
    id 498
    label "korzysta&#263;"
  ]
  node [
    id 499
    label "appreciation"
  ]
  node [
    id 500
    label "osi&#261;ga&#263;"
  ]
  node [
    id 501
    label "dociera&#263;"
  ]
  node [
    id 502
    label "get"
  ]
  node [
    id 503
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 504
    label "mierzy&#263;"
  ]
  node [
    id 505
    label "u&#380;ywa&#263;"
  ]
  node [
    id 506
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 507
    label "exsert"
  ]
  node [
    id 508
    label "being"
  ]
  node [
    id 509
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 510
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 511
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 512
    label "p&#322;ywa&#263;"
  ]
  node [
    id 513
    label "run"
  ]
  node [
    id 514
    label "bangla&#263;"
  ]
  node [
    id 515
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 516
    label "przebiega&#263;"
  ]
  node [
    id 517
    label "wk&#322;ada&#263;"
  ]
  node [
    id 518
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 519
    label "carry"
  ]
  node [
    id 520
    label "bywa&#263;"
  ]
  node [
    id 521
    label "dziama&#263;"
  ]
  node [
    id 522
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 523
    label "stara&#263;_si&#281;"
  ]
  node [
    id 524
    label "para"
  ]
  node [
    id 525
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 526
    label "str&#243;j"
  ]
  node [
    id 527
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 528
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 529
    label "krok"
  ]
  node [
    id 530
    label "tryb"
  ]
  node [
    id 531
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 532
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 533
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 534
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 535
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 536
    label "continue"
  ]
  node [
    id 537
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 538
    label "Ohio"
  ]
  node [
    id 539
    label "wci&#281;cie"
  ]
  node [
    id 540
    label "Nowy_York"
  ]
  node [
    id 541
    label "warstwa"
  ]
  node [
    id 542
    label "samopoczucie"
  ]
  node [
    id 543
    label "Illinois"
  ]
  node [
    id 544
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 545
    label "state"
  ]
  node [
    id 546
    label "Jukatan"
  ]
  node [
    id 547
    label "Kalifornia"
  ]
  node [
    id 548
    label "Wirginia"
  ]
  node [
    id 549
    label "wektor"
  ]
  node [
    id 550
    label "Teksas"
  ]
  node [
    id 551
    label "Goa"
  ]
  node [
    id 552
    label "Waszyngton"
  ]
  node [
    id 553
    label "miejsce"
  ]
  node [
    id 554
    label "Massachusetts"
  ]
  node [
    id 555
    label "Alaska"
  ]
  node [
    id 556
    label "Arakan"
  ]
  node [
    id 557
    label "Hawaje"
  ]
  node [
    id 558
    label "Maryland"
  ]
  node [
    id 559
    label "punkt"
  ]
  node [
    id 560
    label "Michigan"
  ]
  node [
    id 561
    label "Arizona"
  ]
  node [
    id 562
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 563
    label "Georgia"
  ]
  node [
    id 564
    label "poziom"
  ]
  node [
    id 565
    label "Pensylwania"
  ]
  node [
    id 566
    label "shape"
  ]
  node [
    id 567
    label "Luizjana"
  ]
  node [
    id 568
    label "Nowy_Meksyk"
  ]
  node [
    id 569
    label "Alabama"
  ]
  node [
    id 570
    label "ilo&#347;&#263;"
  ]
  node [
    id 571
    label "Kansas"
  ]
  node [
    id 572
    label "Oregon"
  ]
  node [
    id 573
    label "Floryda"
  ]
  node [
    id 574
    label "Oklahoma"
  ]
  node [
    id 575
    label "jednostka_administracyjna"
  ]
  node [
    id 576
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 577
    label "kara&#263;"
  ]
  node [
    id 578
    label "up&#281;dza&#263;_si&#281;"
  ]
  node [
    id 579
    label "straszy&#263;"
  ]
  node [
    id 580
    label "prosecute"
  ]
  node [
    id 581
    label "poszukiwa&#263;"
  ]
  node [
    id 582
    label "usi&#322;owa&#263;"
  ]
  node [
    id 583
    label "szuka&#263;"
  ]
  node [
    id 584
    label "ask"
  ]
  node [
    id 585
    label "look"
  ]
  node [
    id 586
    label "discipline"
  ]
  node [
    id 587
    label "try"
  ]
  node [
    id 588
    label "threaten"
  ]
  node [
    id 589
    label "zapowiada&#263;"
  ]
  node [
    id 590
    label "boast"
  ]
  node [
    id 591
    label "wzbudza&#263;"
  ]
  node [
    id 592
    label "PKN"
  ]
  node [
    id 593
    label "Orlen"
  ]
  node [
    id 594
    label "Williams"
  ]
  node [
    id 595
    label "Racing"
  ]
  node [
    id 596
    label "Robert"
  ]
  node [
    id 597
    label "Kubica"
  ]
  node [
    id 598
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 598
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 372
  ]
  edge [
    source 9
    target 373
  ]
  edge [
    source 9
    target 374
  ]
  edge [
    source 9
    target 375
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 376
  ]
  edge [
    source 12
    target 377
  ]
  edge [
    source 12
    target 378
  ]
  edge [
    source 12
    target 379
  ]
  edge [
    source 12
    target 380
  ]
  edge [
    source 12
    target 381
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 382
  ]
  edge [
    source 13
    target 383
  ]
  edge [
    source 13
    target 384
  ]
  edge [
    source 13
    target 385
  ]
  edge [
    source 13
    target 386
  ]
  edge [
    source 13
    target 387
  ]
  edge [
    source 13
    target 388
  ]
  edge [
    source 13
    target 389
  ]
  edge [
    source 13
    target 390
  ]
  edge [
    source 13
    target 391
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 13
    target 392
  ]
  edge [
    source 13
    target 393
  ]
  edge [
    source 13
    target 394
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 395
  ]
  edge [
    source 13
    target 396
  ]
  edge [
    source 13
    target 397
  ]
  edge [
    source 13
    target 398
  ]
  edge [
    source 13
    target 399
  ]
  edge [
    source 13
    target 400
  ]
  edge [
    source 13
    target 401
  ]
  edge [
    source 13
    target 402
  ]
  edge [
    source 13
    target 403
  ]
  edge [
    source 13
    target 404
  ]
  edge [
    source 13
    target 405
  ]
  edge [
    source 13
    target 406
  ]
  edge [
    source 13
    target 407
  ]
  edge [
    source 13
    target 408
  ]
  edge [
    source 13
    target 409
  ]
  edge [
    source 13
    target 410
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 13
    target 415
  ]
  edge [
    source 13
    target 416
  ]
  edge [
    source 13
    target 417
  ]
  edge [
    source 13
    target 418
  ]
  edge [
    source 13
    target 419
  ]
  edge [
    source 13
    target 420
  ]
  edge [
    source 13
    target 421
  ]
  edge [
    source 13
    target 422
  ]
  edge [
    source 13
    target 423
  ]
  edge [
    source 13
    target 424
  ]
  edge [
    source 13
    target 425
  ]
  edge [
    source 13
    target 426
  ]
  edge [
    source 13
    target 427
  ]
  edge [
    source 13
    target 428
  ]
  edge [
    source 13
    target 429
  ]
  edge [
    source 13
    target 430
  ]
  edge [
    source 13
    target 431
  ]
  edge [
    source 13
    target 432
  ]
  edge [
    source 13
    target 433
  ]
  edge [
    source 13
    target 434
  ]
  edge [
    source 13
    target 435
  ]
  edge [
    source 13
    target 436
  ]
  edge [
    source 13
    target 437
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 438
  ]
  edge [
    source 13
    target 439
  ]
  edge [
    source 13
    target 440
  ]
  edge [
    source 13
    target 441
  ]
  edge [
    source 13
    target 442
  ]
  edge [
    source 13
    target 443
  ]
  edge [
    source 13
    target 444
  ]
  edge [
    source 13
    target 445
  ]
  edge [
    source 13
    target 446
  ]
  edge [
    source 13
    target 447
  ]
  edge [
    source 13
    target 448
  ]
  edge [
    source 13
    target 449
  ]
  edge [
    source 13
    target 450
  ]
  edge [
    source 13
    target 451
  ]
  edge [
    source 13
    target 452
  ]
  edge [
    source 13
    target 453
  ]
  edge [
    source 13
    target 454
  ]
  edge [
    source 13
    target 455
  ]
  edge [
    source 13
    target 456
  ]
  edge [
    source 13
    target 457
  ]
  edge [
    source 13
    target 458
  ]
  edge [
    source 13
    target 459
  ]
  edge [
    source 13
    target 460
  ]
  edge [
    source 13
    target 461
  ]
  edge [
    source 13
    target 462
  ]
  edge [
    source 13
    target 463
  ]
  edge [
    source 13
    target 464
  ]
  edge [
    source 13
    target 465
  ]
  edge [
    source 13
    target 466
  ]
  edge [
    source 13
    target 467
  ]
  edge [
    source 13
    target 468
  ]
  edge [
    source 13
    target 469
  ]
  edge [
    source 13
    target 470
  ]
  edge [
    source 13
    target 471
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 365
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 480
  ]
  edge [
    source 14
    target 481
  ]
  edge [
    source 14
    target 482
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 14
    target 400
  ]
  edge [
    source 14
    target 506
  ]
  edge [
    source 14
    target 507
  ]
  edge [
    source 14
    target 508
  ]
  edge [
    source 14
    target 509
  ]
  edge [
    source 14
    target 336
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 511
  ]
  edge [
    source 14
    target 512
  ]
  edge [
    source 14
    target 513
  ]
  edge [
    source 14
    target 514
  ]
  edge [
    source 14
    target 515
  ]
  edge [
    source 14
    target 516
  ]
  edge [
    source 14
    target 517
  ]
  edge [
    source 14
    target 37
  ]
  edge [
    source 14
    target 518
  ]
  edge [
    source 14
    target 519
  ]
  edge [
    source 14
    target 520
  ]
  edge [
    source 14
    target 521
  ]
  edge [
    source 14
    target 522
  ]
  edge [
    source 14
    target 523
  ]
  edge [
    source 14
    target 524
  ]
  edge [
    source 14
    target 525
  ]
  edge [
    source 14
    target 526
  ]
  edge [
    source 14
    target 527
  ]
  edge [
    source 14
    target 528
  ]
  edge [
    source 14
    target 529
  ]
  edge [
    source 14
    target 530
  ]
  edge [
    source 14
    target 531
  ]
  edge [
    source 14
    target 532
  ]
  edge [
    source 14
    target 533
  ]
  edge [
    source 14
    target 534
  ]
  edge [
    source 14
    target 535
  ]
  edge [
    source 14
    target 536
  ]
  edge [
    source 14
    target 537
  ]
  edge [
    source 14
    target 538
  ]
  edge [
    source 14
    target 539
  ]
  edge [
    source 14
    target 540
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 542
  ]
  edge [
    source 14
    target 543
  ]
  edge [
    source 14
    target 544
  ]
  edge [
    source 14
    target 545
  ]
  edge [
    source 14
    target 546
  ]
  edge [
    source 14
    target 547
  ]
  edge [
    source 14
    target 548
  ]
  edge [
    source 14
    target 549
  ]
  edge [
    source 14
    target 550
  ]
  edge [
    source 14
    target 551
  ]
  edge [
    source 14
    target 552
  ]
  edge [
    source 14
    target 553
  ]
  edge [
    source 14
    target 554
  ]
  edge [
    source 14
    target 555
  ]
  edge [
    source 14
    target 556
  ]
  edge [
    source 14
    target 557
  ]
  edge [
    source 14
    target 558
  ]
  edge [
    source 14
    target 559
  ]
  edge [
    source 14
    target 560
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 562
  ]
  edge [
    source 14
    target 563
  ]
  edge [
    source 14
    target 564
  ]
  edge [
    source 14
    target 565
  ]
  edge [
    source 14
    target 566
  ]
  edge [
    source 14
    target 567
  ]
  edge [
    source 14
    target 568
  ]
  edge [
    source 14
    target 569
  ]
  edge [
    source 14
    target 570
  ]
  edge [
    source 14
    target 571
  ]
  edge [
    source 14
    target 572
  ]
  edge [
    source 14
    target 573
  ]
  edge [
    source 14
    target 574
  ]
  edge [
    source 14
    target 575
  ]
  edge [
    source 14
    target 576
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 577
  ]
  edge [
    source 16
    target 578
  ]
  edge [
    source 16
    target 579
  ]
  edge [
    source 16
    target 580
  ]
  edge [
    source 16
    target 581
  ]
  edge [
    source 16
    target 582
  ]
  edge [
    source 16
    target 523
  ]
  edge [
    source 16
    target 583
  ]
  edge [
    source 16
    target 584
  ]
  edge [
    source 16
    target 585
  ]
  edge [
    source 16
    target 586
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 587
  ]
  edge [
    source 16
    target 588
  ]
  edge [
    source 16
    target 589
  ]
  edge [
    source 16
    target 590
  ]
  edge [
    source 16
    target 591
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 592
    target 593
  ]
  edge [
    source 594
    target 595
  ]
  edge [
    source 596
    target 597
  ]
]
