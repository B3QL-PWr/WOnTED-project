graph [
  node [
    id 0
    label "kto"
    origin "text"
  ]
  node [
    id 1
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "plus"
    origin "text"
  ]
  node [
    id 4
    label "inny"
    origin "text"
  ]
  node [
    id 5
    label "przewija&#263;"
    origin "text"
  ]
  node [
    id 6
    label "daleko"
    origin "text"
  ]
  node [
    id 7
    label "echo"
  ]
  node [
    id 8
    label "pilnowa&#263;"
  ]
  node [
    id 9
    label "robi&#263;"
  ]
  node [
    id 10
    label "recall"
  ]
  node [
    id 11
    label "si&#281;ga&#263;"
  ]
  node [
    id 12
    label "take_care"
  ]
  node [
    id 13
    label "troska&#263;_si&#281;"
  ]
  node [
    id 14
    label "chowa&#263;"
  ]
  node [
    id 15
    label "zachowywa&#263;"
  ]
  node [
    id 16
    label "zna&#263;"
  ]
  node [
    id 17
    label "think"
  ]
  node [
    id 18
    label "report"
  ]
  node [
    id 19
    label "hide"
  ]
  node [
    id 20
    label "znosi&#263;"
  ]
  node [
    id 21
    label "czu&#263;"
  ]
  node [
    id 22
    label "train"
  ]
  node [
    id 23
    label "przetrzymywa&#263;"
  ]
  node [
    id 24
    label "hodowa&#263;"
  ]
  node [
    id 25
    label "meliniarz"
  ]
  node [
    id 26
    label "umieszcza&#263;"
  ]
  node [
    id 27
    label "ukrywa&#263;"
  ]
  node [
    id 28
    label "opiekowa&#263;_si&#281;"
  ]
  node [
    id 29
    label "continue"
  ]
  node [
    id 30
    label "wk&#322;ada&#263;"
  ]
  node [
    id 31
    label "tajemnica"
  ]
  node [
    id 32
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 33
    label "zdyscyplinowanie"
  ]
  node [
    id 34
    label "podtrzymywa&#263;"
  ]
  node [
    id 35
    label "post"
  ]
  node [
    id 36
    label "control"
  ]
  node [
    id 37
    label "przechowywa&#263;"
  ]
  node [
    id 38
    label "behave"
  ]
  node [
    id 39
    label "dieta"
  ]
  node [
    id 40
    label "hold"
  ]
  node [
    id 41
    label "post&#281;powa&#263;"
  ]
  node [
    id 42
    label "compass"
  ]
  node [
    id 43
    label "korzysta&#263;"
  ]
  node [
    id 44
    label "appreciation"
  ]
  node [
    id 45
    label "osi&#261;ga&#263;"
  ]
  node [
    id 46
    label "dociera&#263;"
  ]
  node [
    id 47
    label "get"
  ]
  node [
    id 48
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 49
    label "mierzy&#263;"
  ]
  node [
    id 50
    label "u&#380;ywa&#263;"
  ]
  node [
    id 51
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 52
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 53
    label "exsert"
  ]
  node [
    id 54
    label "organizowa&#263;"
  ]
  node [
    id 55
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 56
    label "czyni&#263;"
  ]
  node [
    id 57
    label "give"
  ]
  node [
    id 58
    label "stylizowa&#263;"
  ]
  node [
    id 59
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 60
    label "falowa&#263;"
  ]
  node [
    id 61
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 62
    label "peddle"
  ]
  node [
    id 63
    label "praca"
  ]
  node [
    id 64
    label "wydala&#263;"
  ]
  node [
    id 65
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 66
    label "tentegowa&#263;"
  ]
  node [
    id 67
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 68
    label "urz&#261;dza&#263;"
  ]
  node [
    id 69
    label "oszukiwa&#263;"
  ]
  node [
    id 70
    label "work"
  ]
  node [
    id 71
    label "ukazywa&#263;"
  ]
  node [
    id 72
    label "przerabia&#263;"
  ]
  node [
    id 73
    label "act"
  ]
  node [
    id 74
    label "str&#243;&#380;y&#263;"
  ]
  node [
    id 75
    label "cognizance"
  ]
  node [
    id 76
    label "wiedzie&#263;"
  ]
  node [
    id 77
    label "resonance"
  ]
  node [
    id 78
    label "zjawisko"
  ]
  node [
    id 79
    label "przekazywa&#263;"
  ]
  node [
    id 80
    label "dostarcza&#263;"
  ]
  node [
    id 81
    label "mie&#263;_miejsce"
  ]
  node [
    id 82
    label "&#322;adowa&#263;"
  ]
  node [
    id 83
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 84
    label "przeznacza&#263;"
  ]
  node [
    id 85
    label "surrender"
  ]
  node [
    id 86
    label "traktowa&#263;"
  ]
  node [
    id 87
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 88
    label "obiecywa&#263;"
  ]
  node [
    id 89
    label "odst&#281;powa&#263;"
  ]
  node [
    id 90
    label "tender"
  ]
  node [
    id 91
    label "rap"
  ]
  node [
    id 92
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 93
    label "t&#322;uc"
  ]
  node [
    id 94
    label "powierza&#263;"
  ]
  node [
    id 95
    label "render"
  ]
  node [
    id 96
    label "wpiernicza&#263;"
  ]
  node [
    id 97
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 98
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 99
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 100
    label "p&#322;aci&#263;"
  ]
  node [
    id 101
    label "hold_out"
  ]
  node [
    id 102
    label "nalewa&#263;"
  ]
  node [
    id 103
    label "zezwala&#263;"
  ]
  node [
    id 104
    label "harbinger"
  ]
  node [
    id 105
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 106
    label "pledge"
  ]
  node [
    id 107
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 108
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 109
    label "poddawa&#263;"
  ]
  node [
    id 110
    label "dotyczy&#263;"
  ]
  node [
    id 111
    label "use"
  ]
  node [
    id 112
    label "perform"
  ]
  node [
    id 113
    label "wychodzi&#263;"
  ]
  node [
    id 114
    label "seclude"
  ]
  node [
    id 115
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 116
    label "nak&#322;ania&#263;"
  ]
  node [
    id 117
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 118
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 119
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 120
    label "dzia&#322;a&#263;"
  ]
  node [
    id 121
    label "appear"
  ]
  node [
    id 122
    label "unwrap"
  ]
  node [
    id 123
    label "rezygnowa&#263;"
  ]
  node [
    id 124
    label "overture"
  ]
  node [
    id 125
    label "uczestniczy&#263;"
  ]
  node [
    id 126
    label "plasowa&#263;"
  ]
  node [
    id 127
    label "umie&#347;ci&#263;"
  ]
  node [
    id 128
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 129
    label "pomieszcza&#263;"
  ]
  node [
    id 130
    label "accommodate"
  ]
  node [
    id 131
    label "zmienia&#263;"
  ]
  node [
    id 132
    label "powodowa&#263;"
  ]
  node [
    id 133
    label "venture"
  ]
  node [
    id 134
    label "okre&#347;la&#263;"
  ]
  node [
    id 135
    label "wyznawa&#263;"
  ]
  node [
    id 136
    label "oddawa&#263;"
  ]
  node [
    id 137
    label "confide"
  ]
  node [
    id 138
    label "zleca&#263;"
  ]
  node [
    id 139
    label "ufa&#263;"
  ]
  node [
    id 140
    label "command"
  ]
  node [
    id 141
    label "grant"
  ]
  node [
    id 142
    label "wydawa&#263;"
  ]
  node [
    id 143
    label "pay"
  ]
  node [
    id 144
    label "buli&#263;"
  ]
  node [
    id 145
    label "wytwarza&#263;"
  ]
  node [
    id 146
    label "odsuwa&#263;_si&#281;"
  ]
  node [
    id 147
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 148
    label "odwr&#243;t"
  ]
  node [
    id 149
    label "zrzeka&#263;_si&#281;"
  ]
  node [
    id 150
    label "impart"
  ]
  node [
    id 151
    label "uznawa&#263;"
  ]
  node [
    id 152
    label "authorize"
  ]
  node [
    id 153
    label "ustala&#263;"
  ]
  node [
    id 154
    label "indicate"
  ]
  node [
    id 155
    label "wysy&#322;a&#263;"
  ]
  node [
    id 156
    label "podawa&#263;"
  ]
  node [
    id 157
    label "wp&#322;aca&#263;"
  ]
  node [
    id 158
    label "sygna&#322;"
  ]
  node [
    id 159
    label "muzyka_rozrywkowa"
  ]
  node [
    id 160
    label "karpiowate"
  ]
  node [
    id 161
    label "ryba"
  ]
  node [
    id 162
    label "czarna_muzyka"
  ]
  node [
    id 163
    label "drapie&#380;nik"
  ]
  node [
    id 164
    label "asp"
  ]
  node [
    id 165
    label "wagon"
  ]
  node [
    id 166
    label "pojazd_kolejowy"
  ]
  node [
    id 167
    label "poci&#261;g"
  ]
  node [
    id 168
    label "statek"
  ]
  node [
    id 169
    label "parow&#243;z_tendrowy"
  ]
  node [
    id 170
    label "okr&#281;t"
  ]
  node [
    id 171
    label "piure"
  ]
  node [
    id 172
    label "butcher"
  ]
  node [
    id 173
    label "murder"
  ]
  node [
    id 174
    label "produkowa&#263;"
  ]
  node [
    id 175
    label "napierdziela&#263;"
  ]
  node [
    id 176
    label "fight"
  ]
  node [
    id 177
    label "mia&#380;d&#380;y&#263;"
  ]
  node [
    id 178
    label "bi&#263;"
  ]
  node [
    id 179
    label "rozdrabnia&#263;"
  ]
  node [
    id 180
    label "wystukiwa&#263;"
  ]
  node [
    id 181
    label "rzn&#261;&#263;"
  ]
  node [
    id 182
    label "plu&#263;"
  ]
  node [
    id 183
    label "walczy&#263;"
  ]
  node [
    id 184
    label "uderza&#263;"
  ]
  node [
    id 185
    label "gra&#263;"
  ]
  node [
    id 186
    label "odpala&#263;"
  ]
  node [
    id 187
    label "r&#380;n&#261;&#263;"
  ]
  node [
    id 188
    label "zabija&#263;"
  ]
  node [
    id 189
    label "powtarza&#263;"
  ]
  node [
    id 190
    label "stuka&#263;"
  ]
  node [
    id 191
    label "niszczy&#263;"
  ]
  node [
    id 192
    label "write_out"
  ]
  node [
    id 193
    label "zalewa&#263;"
  ]
  node [
    id 194
    label "inculcate"
  ]
  node [
    id 195
    label "pour"
  ]
  node [
    id 196
    label "la&#263;"
  ]
  node [
    id 197
    label "wype&#322;nia&#263;"
  ]
  node [
    id 198
    label "applaud"
  ]
  node [
    id 199
    label "zasila&#263;"
  ]
  node [
    id 200
    label "charge"
  ]
  node [
    id 201
    label "nabija&#263;"
  ]
  node [
    id 202
    label "bro&#324;_palna"
  ]
  node [
    id 203
    label "wpl&#261;tywa&#263;"
  ]
  node [
    id 204
    label "je&#347;&#263;"
  ]
  node [
    id 205
    label "z&#322;o&#347;ci&#263;"
  ]
  node [
    id 206
    label "wpycha&#263;"
  ]
  node [
    id 207
    label "warto&#347;&#263;"
  ]
  node [
    id 208
    label "liczba"
  ]
  node [
    id 209
    label "rewaluowa&#263;"
  ]
  node [
    id 210
    label "zrewaluowa&#263;"
  ]
  node [
    id 211
    label "rewaluowanie"
  ]
  node [
    id 212
    label "znak_matematyczny"
  ]
  node [
    id 213
    label "korzy&#347;&#263;"
  ]
  node [
    id 214
    label "stopie&#324;"
  ]
  node [
    id 215
    label "zrewaluowanie"
  ]
  node [
    id 216
    label "dodawanie"
  ]
  node [
    id 217
    label "ocena"
  ]
  node [
    id 218
    label "wabik"
  ]
  node [
    id 219
    label "strona"
  ]
  node [
    id 220
    label "pogl&#261;d"
  ]
  node [
    id 221
    label "decyzja"
  ]
  node [
    id 222
    label "sofcik"
  ]
  node [
    id 223
    label "kryterium"
  ]
  node [
    id 224
    label "informacja"
  ]
  node [
    id 225
    label "appraisal"
  ]
  node [
    id 226
    label "kategoria"
  ]
  node [
    id 227
    label "pierwiastek"
  ]
  node [
    id 228
    label "rozmiar"
  ]
  node [
    id 229
    label "wyra&#380;enie"
  ]
  node [
    id 230
    label "poj&#281;cie"
  ]
  node [
    id 231
    label "number"
  ]
  node [
    id 232
    label "cecha"
  ]
  node [
    id 233
    label "kategoria_gramatyczna"
  ]
  node [
    id 234
    label "grupa"
  ]
  node [
    id 235
    label "kwadrat_magiczny"
  ]
  node [
    id 236
    label "koniugacja"
  ]
  node [
    id 237
    label "kszta&#322;t"
  ]
  node [
    id 238
    label "podstopie&#324;"
  ]
  node [
    id 239
    label "wielko&#347;&#263;"
  ]
  node [
    id 240
    label "rank"
  ]
  node [
    id 241
    label "minuta"
  ]
  node [
    id 242
    label "d&#378;wi&#281;k"
  ]
  node [
    id 243
    label "wschodek"
  ]
  node [
    id 244
    label "przymiotnik"
  ]
  node [
    id 245
    label "gama"
  ]
  node [
    id 246
    label "jednostka"
  ]
  node [
    id 247
    label "podzia&#322;"
  ]
  node [
    id 248
    label "miejsce"
  ]
  node [
    id 249
    label "element"
  ]
  node [
    id 250
    label "schody"
  ]
  node [
    id 251
    label "poziom"
  ]
  node [
    id 252
    label "przys&#322;&#243;wek"
  ]
  node [
    id 253
    label "degree"
  ]
  node [
    id 254
    label "szczebel"
  ]
  node [
    id 255
    label "znaczenie"
  ]
  node [
    id 256
    label "podn&#243;&#380;ek"
  ]
  node [
    id 257
    label "forma"
  ]
  node [
    id 258
    label "zaleta"
  ]
  node [
    id 259
    label "dobro"
  ]
  node [
    id 260
    label "kartka"
  ]
  node [
    id 261
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 262
    label "logowanie"
  ]
  node [
    id 263
    label "plik"
  ]
  node [
    id 264
    label "s&#261;d"
  ]
  node [
    id 265
    label "adres_internetowy"
  ]
  node [
    id 266
    label "linia"
  ]
  node [
    id 267
    label "serwis_internetowy"
  ]
  node [
    id 268
    label "posta&#263;"
  ]
  node [
    id 269
    label "bok"
  ]
  node [
    id 270
    label "skr&#281;canie"
  ]
  node [
    id 271
    label "skr&#281;ca&#263;"
  ]
  node [
    id 272
    label "orientowanie"
  ]
  node [
    id 273
    label "skr&#281;ci&#263;"
  ]
  node [
    id 274
    label "uj&#281;cie"
  ]
  node [
    id 275
    label "zorientowanie"
  ]
  node [
    id 276
    label "ty&#322;"
  ]
  node [
    id 277
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 278
    label "fragment"
  ]
  node [
    id 279
    label "layout"
  ]
  node [
    id 280
    label "obiekt"
  ]
  node [
    id 281
    label "zorientowa&#263;"
  ]
  node [
    id 282
    label "pagina"
  ]
  node [
    id 283
    label "podmiot"
  ]
  node [
    id 284
    label "g&#243;ra"
  ]
  node [
    id 285
    label "orientowa&#263;"
  ]
  node [
    id 286
    label "voice"
  ]
  node [
    id 287
    label "orientacja"
  ]
  node [
    id 288
    label "prz&#243;d"
  ]
  node [
    id 289
    label "internet"
  ]
  node [
    id 290
    label "powierzchnia"
  ]
  node [
    id 291
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 292
    label "skr&#281;cenie"
  ]
  node [
    id 293
    label "zmienna"
  ]
  node [
    id 294
    label "wskazywanie"
  ]
  node [
    id 295
    label "cel"
  ]
  node [
    id 296
    label "wskazywa&#263;"
  ]
  node [
    id 297
    label "worth"
  ]
  node [
    id 298
    label "do&#322;&#261;czanie"
  ]
  node [
    id 299
    label "addition"
  ]
  node [
    id 300
    label "liczenie"
  ]
  node [
    id 301
    label "dop&#322;acanie"
  ]
  node [
    id 302
    label "summation"
  ]
  node [
    id 303
    label "rozdzielno&#347;&#263;_mno&#380;enia_wzgl&#281;dem_dodawania"
  ]
  node [
    id 304
    label "uzupe&#322;nianie"
  ]
  node [
    id 305
    label "dokupowanie"
  ]
  node [
    id 306
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 307
    label "&#322;&#261;czno&#347;&#263;"
  ]
  node [
    id 308
    label "do&#347;wietlanie"
  ]
  node [
    id 309
    label "suma"
  ]
  node [
    id 310
    label "wspominanie"
  ]
  node [
    id 311
    label "podniesienie"
  ]
  node [
    id 312
    label "warto&#347;ciowy"
  ]
  node [
    id 313
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 314
    label "czynnik"
  ]
  node [
    id 315
    label "przedmiot"
  ]
  node [
    id 316
    label "magnes"
  ]
  node [
    id 317
    label "appreciate"
  ]
  node [
    id 318
    label "podnosi&#263;"
  ]
  node [
    id 319
    label "podnie&#347;&#263;"
  ]
  node [
    id 320
    label "przewarto&#347;ciowywanie"
  ]
  node [
    id 321
    label "podnoszenie"
  ]
  node [
    id 322
    label "kolejny"
  ]
  node [
    id 323
    label "osobno"
  ]
  node [
    id 324
    label "r&#243;&#380;ny"
  ]
  node [
    id 325
    label "inszy"
  ]
  node [
    id 326
    label "inaczej"
  ]
  node [
    id 327
    label "odr&#281;bny"
  ]
  node [
    id 328
    label "nast&#281;pnie"
  ]
  node [
    id 329
    label "nastopny"
  ]
  node [
    id 330
    label "kolejno"
  ]
  node [
    id 331
    label "kt&#243;ry&#347;"
  ]
  node [
    id 332
    label "jaki&#347;"
  ]
  node [
    id 333
    label "r&#243;&#380;nie"
  ]
  node [
    id 334
    label "niestandardowo"
  ]
  node [
    id 335
    label "individually"
  ]
  node [
    id 336
    label "udzielnie"
  ]
  node [
    id 337
    label "osobnie"
  ]
  node [
    id 338
    label "odr&#281;bnie"
  ]
  node [
    id 339
    label "osobny"
  ]
  node [
    id 340
    label "owija&#263;"
  ]
  node [
    id 341
    label "pielucha"
  ]
  node [
    id 342
    label "kaseta"
  ]
  node [
    id 343
    label "przebiera&#263;"
  ]
  node [
    id 344
    label "swathe"
  ]
  node [
    id 345
    label "otacza&#263;"
  ]
  node [
    id 346
    label "extort"
  ]
  node [
    id 347
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 348
    label "motywowa&#263;"
  ]
  node [
    id 349
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 350
    label "dress"
  ]
  node [
    id 351
    label "przemienia&#263;"
  ]
  node [
    id 352
    label "trim"
  ]
  node [
    id 353
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 354
    label "ceregieli&#263;_si&#281;"
  ]
  node [
    id 355
    label "upodabnia&#263;"
  ]
  node [
    id 356
    label "cull"
  ]
  node [
    id 357
    label "zastanawia&#263;_si&#281;"
  ]
  node [
    id 358
    label "rusza&#263;"
  ]
  node [
    id 359
    label "przerzuca&#263;"
  ]
  node [
    id 360
    label "ta&#347;ma_magnetyczna"
  ]
  node [
    id 361
    label "skrzynka"
  ]
  node [
    id 362
    label "no&#347;nik_danych"
  ]
  node [
    id 363
    label "pude&#322;ko"
  ]
  node [
    id 364
    label "przewijanie"
  ]
  node [
    id 365
    label "coffin"
  ]
  node [
    id 366
    label "opakowanie"
  ]
  node [
    id 367
    label "wyprawka"
  ]
  node [
    id 368
    label "wytw&#243;r"
  ]
  node [
    id 369
    label "diaper"
  ]
  node [
    id 370
    label "powijak"
  ]
  node [
    id 371
    label "nisko"
  ]
  node [
    id 372
    label "znacznie"
  ]
  node [
    id 373
    label "het"
  ]
  node [
    id 374
    label "dawno"
  ]
  node [
    id 375
    label "daleki"
  ]
  node [
    id 376
    label "g&#322;&#281;boko"
  ]
  node [
    id 377
    label "nieobecnie"
  ]
  node [
    id 378
    label "wysoko"
  ]
  node [
    id 379
    label "du&#380;o"
  ]
  node [
    id 380
    label "dawny"
  ]
  node [
    id 381
    label "ogl&#281;dny"
  ]
  node [
    id 382
    label "d&#322;ugi"
  ]
  node [
    id 383
    label "du&#380;y"
  ]
  node [
    id 384
    label "odleg&#322;y"
  ]
  node [
    id 385
    label "zwi&#261;zany"
  ]
  node [
    id 386
    label "s&#322;aby"
  ]
  node [
    id 387
    label "odlegle"
  ]
  node [
    id 388
    label "oddalony"
  ]
  node [
    id 389
    label "g&#322;&#281;boki"
  ]
  node [
    id 390
    label "obcy"
  ]
  node [
    id 391
    label "nieobecny"
  ]
  node [
    id 392
    label "przysz&#322;y"
  ]
  node [
    id 393
    label "niepo&#347;lednio"
  ]
  node [
    id 394
    label "wysoki"
  ]
  node [
    id 395
    label "g&#243;rno"
  ]
  node [
    id 396
    label "chwalebnie"
  ]
  node [
    id 397
    label "wznio&#347;le"
  ]
  node [
    id 398
    label "szczytny"
  ]
  node [
    id 399
    label "d&#322;ugotrwale"
  ]
  node [
    id 400
    label "wcze&#347;niej"
  ]
  node [
    id 401
    label "ongi&#347;"
  ]
  node [
    id 402
    label "dawnie"
  ]
  node [
    id 403
    label "zamy&#347;lony"
  ]
  node [
    id 404
    label "uni&#380;enie"
  ]
  node [
    id 405
    label "pospolicie"
  ]
  node [
    id 406
    label "blisko"
  ]
  node [
    id 407
    label "wstydliwie"
  ]
  node [
    id 408
    label "ma&#322;o"
  ]
  node [
    id 409
    label "vilely"
  ]
  node [
    id 410
    label "despicably"
  ]
  node [
    id 411
    label "niski"
  ]
  node [
    id 412
    label "po&#347;lednio"
  ]
  node [
    id 413
    label "ma&#322;y"
  ]
  node [
    id 414
    label "mocno"
  ]
  node [
    id 415
    label "gruntownie"
  ]
  node [
    id 416
    label "szczerze"
  ]
  node [
    id 417
    label "silnie"
  ]
  node [
    id 418
    label "intensywnie"
  ]
  node [
    id 419
    label "zauwa&#380;alnie"
  ]
  node [
    id 420
    label "znaczny"
  ]
  node [
    id 421
    label "wiela"
  ]
  node [
    id 422
    label "bardzo"
  ]
  node [
    id 423
    label "cz&#281;sto"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
]
