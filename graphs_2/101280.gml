graph [
  node [
    id 0
    label "naw&#322;o&#263;"
    origin "text"
  ]
  node [
    id 1
    label "kanadyjski"
    origin "text"
  ]
  node [
    id 2
    label "astrowate"
  ]
  node [
    id 3
    label "bylina"
  ]
  node [
    id 4
    label "goldenrod"
  ]
  node [
    id 5
    label "ludowy"
  ]
  node [
    id 6
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 7
    label "utw&#243;r_epicki"
  ]
  node [
    id 8
    label "pie&#347;&#324;"
  ]
  node [
    id 9
    label "astrowce"
  ]
  node [
    id 10
    label "plewinka"
  ]
  node [
    id 11
    label "ameryka&#324;ski"
  ]
  node [
    id 12
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 13
    label "po_kanadyjsku"
  ]
  node [
    id 14
    label "anglosaski"
  ]
  node [
    id 15
    label "j&#281;zyk_angielski"
  ]
  node [
    id 16
    label "typowy"
  ]
  node [
    id 17
    label "charakterystyczny"
  ]
  node [
    id 18
    label "po_anglosasku"
  ]
  node [
    id 19
    label "anglosasko"
  ]
  node [
    id 20
    label "fajny"
  ]
  node [
    id 21
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 22
    label "po_ameryka&#324;sku"
  ]
  node [
    id 23
    label "boston"
  ]
  node [
    id 24
    label "pepperoni"
  ]
  node [
    id 25
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 26
    label "nowoczesny"
  ]
  node [
    id 27
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 28
    label "zachodni"
  ]
  node [
    id 29
    label "Princeton"
  ]
  node [
    id 30
    label "cake-walk"
  ]
  node [
    id 31
    label "matka"
  ]
  node [
    id 32
    label "boski"
  ]
  node [
    id 33
    label "ameryk"
  ]
  node [
    id 34
    label "p&#243;&#322;nocny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
]
