graph [
  node [
    id 0
    label "bania"
    origin "text"
  ]
  node [
    id 1
    label "ali"
    origin "text"
  ]
  node [
    id 2
    label "przedmiot"
  ]
  node [
    id 3
    label "dynia"
  ]
  node [
    id 4
    label "popijawa"
  ]
  node [
    id 5
    label "p&#281;katy"
  ]
  node [
    id 6
    label "warzywo"
  ]
  node [
    id 7
    label "czaszka"
  ]
  node [
    id 8
    label "niedostateczny"
  ]
  node [
    id 9
    label "&#322;eb"
  ]
  node [
    id 10
    label "nibyjagoda"
  ]
  node [
    id 11
    label "owoc"
  ]
  node [
    id 12
    label "mak&#243;wka"
  ]
  node [
    id 13
    label "pa&#322;a"
  ]
  node [
    id 14
    label "dw&#243;jka"
  ]
  node [
    id 15
    label "jedynka"
  ]
  node [
    id 16
    label "niezadowalaj&#261;cy"
  ]
  node [
    id 17
    label "mak"
  ]
  node [
    id 18
    label "puszka"
  ]
  node [
    id 19
    label "gourd"
  ]
  node [
    id 20
    label "dyniowate"
  ]
  node [
    id 21
    label "ro&#347;lina"
  ]
  node [
    id 22
    label "zdolno&#347;&#263;"
  ]
  node [
    id 23
    label "g&#322;owa"
  ]
  node [
    id 24
    label "zwierz&#281;"
  ]
  node [
    id 25
    label "cecha"
  ]
  node [
    id 26
    label "umys&#322;"
  ]
  node [
    id 27
    label "wiedza"
  ]
  node [
    id 28
    label "morda"
  ]
  node [
    id 29
    label "alkohol"
  ]
  node [
    id 30
    label "noosfera"
  ]
  node [
    id 31
    label "rozszczep_czaszki"
  ]
  node [
    id 32
    label "ko&#347;&#263;_czo&#322;owa"
  ]
  node [
    id 33
    label "diafanoskopia"
  ]
  node [
    id 34
    label "&#380;uchwa"
  ]
  node [
    id 35
    label "ma&#322;og&#322;owie"
  ]
  node [
    id 36
    label "siode&#322;ko_tureckie"
  ]
  node [
    id 37
    label "&#322;uk_jarzmowy"
  ]
  node [
    id 38
    label "zatoka"
  ]
  node [
    id 39
    label "szew_kostny"
  ]
  node [
    id 40
    label "ko&#347;&#263;_podniebienna"
  ]
  node [
    id 41
    label "lemiesz"
  ]
  node [
    id 42
    label "ko&#347;&#263;_klinowa"
  ]
  node [
    id 43
    label "oczod&#243;&#322;"
  ]
  node [
    id 44
    label "ciemi&#281;"
  ]
  node [
    id 45
    label "m&#243;zgoczaszka"
  ]
  node [
    id 46
    label "po&#347;redniog&#322;owo&#347;&#263;"
  ]
  node [
    id 47
    label "wa&#322;_nadoczodo&#322;owy"
  ]
  node [
    id 48
    label "szkielet"
  ]
  node [
    id 49
    label "szew_strza&#322;kowy"
  ]
  node [
    id 50
    label "potylica"
  ]
  node [
    id 51
    label "puszka_m&#243;zgowa"
  ]
  node [
    id 52
    label "szczelina_oczodo&#322;owa"
  ]
  node [
    id 53
    label "trzewioczaszka"
  ]
  node [
    id 54
    label "drink"
  ]
  node [
    id 55
    label "impreza"
  ]
  node [
    id 56
    label "fruktoza"
  ]
  node [
    id 57
    label "produkt"
  ]
  node [
    id 58
    label "glukoza"
  ]
  node [
    id 59
    label "mi&#261;&#380;sz"
  ]
  node [
    id 60
    label "rezultat"
  ]
  node [
    id 61
    label "obiekt"
  ]
  node [
    id 62
    label "owocnia"
  ]
  node [
    id 63
    label "drylowanie"
  ]
  node [
    id 64
    label "gniazdo_nasienne"
  ]
  node [
    id 65
    label "frukt"
  ]
  node [
    id 66
    label "obieralnia"
  ]
  node [
    id 67
    label "blanszownik"
  ]
  node [
    id 68
    label "ogrodowizna"
  ]
  node [
    id 69
    label "zielenina"
  ]
  node [
    id 70
    label "nieuleczalnie_chory"
  ]
  node [
    id 71
    label "owoc_rzekomy"
  ]
  node [
    id 72
    label "jagoda"
  ]
  node [
    id 73
    label "discipline"
  ]
  node [
    id 74
    label "zboczy&#263;"
  ]
  node [
    id 75
    label "w&#261;tek"
  ]
  node [
    id 76
    label "kultura"
  ]
  node [
    id 77
    label "entity"
  ]
  node [
    id 78
    label "sponiewiera&#263;"
  ]
  node [
    id 79
    label "zboczenie"
  ]
  node [
    id 80
    label "zbaczanie"
  ]
  node [
    id 81
    label "charakter"
  ]
  node [
    id 82
    label "thing"
  ]
  node [
    id 83
    label "om&#243;wi&#263;"
  ]
  node [
    id 84
    label "tre&#347;&#263;"
  ]
  node [
    id 85
    label "element"
  ]
  node [
    id 86
    label "kr&#261;&#380;enie"
  ]
  node [
    id 87
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 88
    label "istota"
  ]
  node [
    id 89
    label "zbacza&#263;"
  ]
  node [
    id 90
    label "om&#243;wienie"
  ]
  node [
    id 91
    label "rzecz"
  ]
  node [
    id 92
    label "tematyka"
  ]
  node [
    id 93
    label "omawianie"
  ]
  node [
    id 94
    label "omawia&#263;"
  ]
  node [
    id 95
    label "robienie"
  ]
  node [
    id 96
    label "program_nauczania"
  ]
  node [
    id 97
    label "sponiewieranie"
  ]
  node [
    id 98
    label "p&#281;kato"
  ]
  node [
    id 99
    label "grubiutki"
  ]
  node [
    id 100
    label "pe&#322;ny"
  ]
  node [
    id 101
    label "gruby"
  ]
  node [
    id 102
    label "bu"
  ]
  node [
    id 103
    label "Asz"
  ]
  node [
    id 104
    label "Szarkijja"
  ]
  node [
    id 105
    label "&#1576;&#1606;&#1609;"
  ]
  node [
    id 106
    label "&#1576;&#1608;"
  ]
  node [
    id 107
    label "&#1593;&#1604;&#1610;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 105
    target 106
  ]
  edge [
    source 105
    target 107
  ]
  edge [
    source 106
    target 107
  ]
]
