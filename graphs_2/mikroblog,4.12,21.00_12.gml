graph [
  node [
    id 0
    label "roba"
    origin "text"
  ]
  node [
    id 1
    label "czajepki"
    origin "text"
  ]
  node [
    id 2
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 3
    label "avatarach"
    origin "text"
  ]
  node [
    id 4
    label "pisacz"
    origin "text"
  ]
  node [
    id 5
    label "komentacz"
    origin "text"
  ]
  node [
    id 6
    label "suknia"
  ]
  node [
    id 7
    label "tren"
  ]
  node [
    id 8
    label "element"
  ]
  node [
    id 9
    label "przedmiot"
  ]
  node [
    id 10
    label "plisa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
