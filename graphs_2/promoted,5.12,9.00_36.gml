graph [
  node [
    id 0
    label "kole&#380;ka"
    origin "text"
  ]
  node [
    id 1
    label "zainstalowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "linuxa"
    origin "text"
  ]
  node [
    id 3
    label "tesla"
    origin "text"
  ]
  node [
    id 4
    label "kolega"
  ]
  node [
    id 5
    label "znajomy"
  ]
  node [
    id 6
    label "towarzysz"
  ]
  node [
    id 7
    label "kumplowanie_si&#281;"
  ]
  node [
    id 8
    label "ziom"
  ]
  node [
    id 9
    label "partner"
  ]
  node [
    id 10
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 11
    label "konfrater"
  ]
  node [
    id 12
    label "dostosowa&#263;"
  ]
  node [
    id 13
    label "zrobi&#263;"
  ]
  node [
    id 14
    label "komputer"
  ]
  node [
    id 15
    label "program"
  ]
  node [
    id 16
    label "install"
  ]
  node [
    id 17
    label "umie&#347;ci&#263;"
  ]
  node [
    id 18
    label "set"
  ]
  node [
    id 19
    label "put"
  ]
  node [
    id 20
    label "uplasowa&#263;"
  ]
  node [
    id 21
    label "wpierniczy&#263;"
  ]
  node [
    id 22
    label "okre&#347;li&#263;"
  ]
  node [
    id 23
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 24
    label "zmieni&#263;"
  ]
  node [
    id 25
    label "umieszcza&#263;"
  ]
  node [
    id 26
    label "post&#261;pi&#263;"
  ]
  node [
    id 27
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 28
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 29
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 30
    label "zorganizowa&#263;"
  ]
  node [
    id 31
    label "appoint"
  ]
  node [
    id 32
    label "wystylizowa&#263;"
  ]
  node [
    id 33
    label "cause"
  ]
  node [
    id 34
    label "przerobi&#263;"
  ]
  node [
    id 35
    label "nabra&#263;"
  ]
  node [
    id 36
    label "make"
  ]
  node [
    id 37
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 38
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 39
    label "wydali&#263;"
  ]
  node [
    id 40
    label "adjust"
  ]
  node [
    id 41
    label "stacja_dysk&#243;w"
  ]
  node [
    id 42
    label "instalowa&#263;"
  ]
  node [
    id 43
    label "moc_obliczeniowa"
  ]
  node [
    id 44
    label "elektroniczna_maszyna_licz&#261;ca"
  ]
  node [
    id 45
    label "pad"
  ]
  node [
    id 46
    label "modem"
  ]
  node [
    id 47
    label "pami&#281;&#263;"
  ]
  node [
    id 48
    label "monitor"
  ]
  node [
    id 49
    label "zainstalowanie"
  ]
  node [
    id 50
    label "emulacja"
  ]
  node [
    id 51
    label "procesor"
  ]
  node [
    id 52
    label "maszyna_Turinga"
  ]
  node [
    id 53
    label "karta"
  ]
  node [
    id 54
    label "twardy_dysk"
  ]
  node [
    id 55
    label "klawiatura"
  ]
  node [
    id 56
    label "botnet"
  ]
  node [
    id 57
    label "mysz"
  ]
  node [
    id 58
    label "instalowanie"
  ]
  node [
    id 59
    label "radiator"
  ]
  node [
    id 60
    label "urz&#261;dzenie"
  ]
  node [
    id 61
    label "p&#322;yta_g&#322;&#243;wna"
  ]
  node [
    id 62
    label "oprogramowanie"
  ]
  node [
    id 63
    label "odinstalowywa&#263;"
  ]
  node [
    id 64
    label "spis"
  ]
  node [
    id 65
    label "zaprezentowanie"
  ]
  node [
    id 66
    label "podprogram"
  ]
  node [
    id 67
    label "ogranicznik_referencyjny"
  ]
  node [
    id 68
    label "course_of_study"
  ]
  node [
    id 69
    label "booklet"
  ]
  node [
    id 70
    label "dzia&#322;"
  ]
  node [
    id 71
    label "odinstalowanie"
  ]
  node [
    id 72
    label "broszura"
  ]
  node [
    id 73
    label "wytw&#243;r"
  ]
  node [
    id 74
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 75
    label "kana&#322;"
  ]
  node [
    id 76
    label "teleferie"
  ]
  node [
    id 77
    label "struktura_organizacyjna"
  ]
  node [
    id 78
    label "pirat"
  ]
  node [
    id 79
    label "zaprezentowa&#263;"
  ]
  node [
    id 80
    label "prezentowanie"
  ]
  node [
    id 81
    label "prezentowa&#263;"
  ]
  node [
    id 82
    label "interfejs"
  ]
  node [
    id 83
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 84
    label "okno"
  ]
  node [
    id 85
    label "blok"
  ]
  node [
    id 86
    label "punkt"
  ]
  node [
    id 87
    label "folder"
  ]
  node [
    id 88
    label "za&#322;o&#380;enie"
  ]
  node [
    id 89
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 90
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 91
    label "ram&#243;wka"
  ]
  node [
    id 92
    label "tryb"
  ]
  node [
    id 93
    label "emitowa&#263;"
  ]
  node [
    id 94
    label "emitowanie"
  ]
  node [
    id 95
    label "odinstalowywanie"
  ]
  node [
    id 96
    label "instrukcja"
  ]
  node [
    id 97
    label "informatyka"
  ]
  node [
    id 98
    label "deklaracja"
  ]
  node [
    id 99
    label "sekcja_krytyczna"
  ]
  node [
    id 100
    label "menu"
  ]
  node [
    id 101
    label "furkacja"
  ]
  node [
    id 102
    label "podstawa"
  ]
  node [
    id 103
    label "oferta"
  ]
  node [
    id 104
    label "odinstalowa&#263;"
  ]
  node [
    id 105
    label "samoch&#243;d"
  ]
  node [
    id 106
    label "jednostka_indukcji_magnetycznej"
  ]
  node [
    id 107
    label "gaus"
  ]
  node [
    id 108
    label "pojazd_drogowy"
  ]
  node [
    id 109
    label "spryskiwacz"
  ]
  node [
    id 110
    label "most"
  ]
  node [
    id 111
    label "baga&#380;nik"
  ]
  node [
    id 112
    label "silnik"
  ]
  node [
    id 113
    label "dachowanie"
  ]
  node [
    id 114
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 115
    label "pompa_wodna"
  ]
  node [
    id 116
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 117
    label "poduszka_powietrzna"
  ]
  node [
    id 118
    label "tempomat"
  ]
  node [
    id 119
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 120
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 121
    label "deska_rozdzielcza"
  ]
  node [
    id 122
    label "immobilizer"
  ]
  node [
    id 123
    label "t&#322;umik"
  ]
  node [
    id 124
    label "ABS"
  ]
  node [
    id 125
    label "kierownica"
  ]
  node [
    id 126
    label "bak"
  ]
  node [
    id 127
    label "dwu&#347;lad"
  ]
  node [
    id 128
    label "poci&#261;g_drogowy"
  ]
  node [
    id 129
    label "wycieraczka"
  ]
  node [
    id 130
    label "mikrogaus"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
]
