graph [
  node [
    id 0
    label "zenon"
    origin "text"
  ]
  node [
    id 1
    label "smolarek"
    origin "text"
  ]
  node [
    id 2
    label "Zenona"
  ]
  node [
    id 3
    label "Smolarek"
  ]
  node [
    id 4
    label "milicja"
  ]
  node [
    id 5
    label "obywatelski"
  ]
  node [
    id 6
    label "wydzia&#322;"
  ]
  node [
    id 7
    label "prawy"
  ]
  node [
    id 8
    label "szko&#322;a"
  ]
  node [
    id 9
    label "oficerski"
  ]
  node [
    id 10
    label "uniwersytet"
  ]
  node [
    id 11
    label "Adam"
  ]
  node [
    id 12
    label "Mickiewicz"
  ]
  node [
    id 13
    label "Dochodzeniowo"
  ]
  node [
    id 14
    label "&#347;ledczy"
  ]
  node [
    id 15
    label "wojew&#243;dzki"
  ]
  node [
    id 16
    label "urz&#261;d"
  ]
  node [
    id 17
    label "sprawi&#263;"
  ]
  node [
    id 18
    label "wewn&#281;trzny"
  ]
  node [
    id 19
    label "ostr&#243;w"
  ]
  node [
    id 20
    label "wielkopolski"
  ]
  node [
    id 21
    label "pomnik"
  ]
  node [
    id 22
    label "pomordowa&#263;"
  ]
  node [
    id 23
    label "policjant"
  ]
  node [
    id 24
    label "ii"
  ]
  node [
    id 25
    label "RP"
  ]
  node [
    id 26
    label "wojew&#243;dztwo"
  ]
  node [
    id 27
    label "pozna&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 26
    target 27
  ]
]
