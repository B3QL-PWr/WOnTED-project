graph [
  node [
    id 0
    label "wk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 1
    label "parasol"
    origin "text"
  ]
  node [
    id 2
    label "gard&#322;o"
    origin "text"
  ]
  node [
    id 3
    label "przekazywa&#263;"
  ]
  node [
    id 4
    label "obleka&#263;"
  ]
  node [
    id 5
    label "odziewa&#263;"
  ]
  node [
    id 6
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 7
    label "ubiera&#263;"
  ]
  node [
    id 8
    label "inspirowa&#263;"
  ]
  node [
    id 9
    label "pour"
  ]
  node [
    id 10
    label "nosi&#263;"
  ]
  node [
    id 11
    label "introduce"
  ]
  node [
    id 12
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 13
    label "wzbudza&#263;"
  ]
  node [
    id 14
    label "umieszcza&#263;"
  ]
  node [
    id 15
    label "place"
  ]
  node [
    id 16
    label "wpaja&#263;"
  ]
  node [
    id 17
    label "wysy&#322;a&#263;"
  ]
  node [
    id 18
    label "podawa&#263;"
  ]
  node [
    id 19
    label "give"
  ]
  node [
    id 20
    label "wp&#322;aca&#263;"
  ]
  node [
    id 21
    label "sygna&#322;"
  ]
  node [
    id 22
    label "powodowa&#263;"
  ]
  node [
    id 23
    label "impart"
  ]
  node [
    id 24
    label "pozostawia&#263;"
  ]
  node [
    id 25
    label "zaczyna&#263;"
  ]
  node [
    id 26
    label "psu&#263;"
  ]
  node [
    id 27
    label "przygotowywa&#263;"
  ]
  node [
    id 28
    label "go"
  ]
  node [
    id 29
    label "znak"
  ]
  node [
    id 30
    label "seat"
  ]
  node [
    id 31
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 32
    label "wygrywa&#263;"
  ]
  node [
    id 33
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 34
    label "go&#347;ci&#263;"
  ]
  node [
    id 35
    label "uniemo&#380;liwia&#263;"
  ]
  node [
    id 36
    label "set"
  ]
  node [
    id 37
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 38
    label "elaborate"
  ]
  node [
    id 39
    label "train"
  ]
  node [
    id 40
    label "pokrywa&#263;"
  ]
  node [
    id 41
    label "zmienia&#263;"
  ]
  node [
    id 42
    label "robi&#263;"
  ]
  node [
    id 43
    label "za&#322;&#261;cza&#263;"
  ]
  node [
    id 44
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 45
    label "po&#347;redniczy&#263;"
  ]
  node [
    id 46
    label "relate"
  ]
  node [
    id 47
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 48
    label "plasowa&#263;"
  ]
  node [
    id 49
    label "umie&#347;ci&#263;"
  ]
  node [
    id 50
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 51
    label "pomieszcza&#263;"
  ]
  node [
    id 52
    label "accommodate"
  ]
  node [
    id 53
    label "venture"
  ]
  node [
    id 54
    label "wpiernicza&#263;"
  ]
  node [
    id 55
    label "okre&#347;la&#263;"
  ]
  node [
    id 56
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 57
    label "wra&#380;a&#263;"
  ]
  node [
    id 58
    label "przekonywa&#263;"
  ]
  node [
    id 59
    label "uczy&#263;"
  ]
  node [
    id 60
    label "report"
  ]
  node [
    id 61
    label "nak&#322;ada&#263;"
  ]
  node [
    id 62
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 63
    label "trim"
  ]
  node [
    id 64
    label "wystrycha&#263;"
  ]
  node [
    id 65
    label "przedstawia&#263;"
  ]
  node [
    id 66
    label "wear"
  ]
  node [
    id 67
    label "posiada&#263;"
  ]
  node [
    id 68
    label "str&#243;j"
  ]
  node [
    id 69
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 70
    label "carry"
  ]
  node [
    id 71
    label "mie&#263;"
  ]
  node [
    id 72
    label "przemieszcza&#263;"
  ]
  node [
    id 73
    label "tug"
  ]
  node [
    id 74
    label "kopiowa&#263;"
  ]
  node [
    id 75
    label "motywowa&#263;"
  ]
  node [
    id 76
    label "os&#322;ona"
  ]
  node [
    id 77
    label "deszczochron"
  ]
  node [
    id 78
    label "ciennik"
  ]
  node [
    id 79
    label "ochrona"
  ]
  node [
    id 80
    label "oddzia&#322;"
  ]
  node [
    id 81
    label "operacja"
  ]
  node [
    id 82
    label "farmaceutyk"
  ]
  node [
    id 83
    label "pergola"
  ]
  node [
    id 84
    label "altana"
  ]
  node [
    id 85
    label "drzewo"
  ]
  node [
    id 86
    label "cie&#324;"
  ]
  node [
    id 87
    label "przew&#243;d_pokarmowy"
  ]
  node [
    id 88
    label "pier&#347;cie&#324;_gard&#322;owy_Waldeyera"
  ]
  node [
    id 89
    label "zedrze&#263;"
  ]
  node [
    id 90
    label "miejsce"
  ]
  node [
    id 91
    label "jama_gard&#322;owa"
  ]
  node [
    id 92
    label "element_anatomiczny"
  ]
  node [
    id 93
    label "zdarcie"
  ]
  node [
    id 94
    label "zachy&#322;ek_gruszkowaty"
  ]
  node [
    id 95
    label "throat"
  ]
  node [
    id 96
    label "warunek_lokalowy"
  ]
  node [
    id 97
    label "plac"
  ]
  node [
    id 98
    label "location"
  ]
  node [
    id 99
    label "uwaga"
  ]
  node [
    id 100
    label "przestrze&#324;"
  ]
  node [
    id 101
    label "status"
  ]
  node [
    id 102
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 103
    label "chwila"
  ]
  node [
    id 104
    label "cia&#322;o"
  ]
  node [
    id 105
    label "cecha"
  ]
  node [
    id 106
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 107
    label "praca"
  ]
  node [
    id 108
    label "rz&#261;d"
  ]
  node [
    id 109
    label "zniszczenie"
  ]
  node [
    id 110
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 111
    label "nadwyr&#281;&#380;enie"
  ]
  node [
    id 112
    label "zdrowie"
  ]
  node [
    id 113
    label "attrition"
  ]
  node [
    id 114
    label "zranienie"
  ]
  node [
    id 115
    label "s&#322;ony"
  ]
  node [
    id 116
    label "wzi&#281;cie"
  ]
  node [
    id 117
    label "zarobi&#263;"
  ]
  node [
    id 118
    label "nadwyr&#281;&#380;y&#263;"
  ]
  node [
    id 119
    label "wzi&#261;&#263;"
  ]
  node [
    id 120
    label "zrani&#263;"
  ]
  node [
    id 121
    label "&#347;ci&#261;gn&#261;&#263;"
  ]
  node [
    id 122
    label "zniszczy&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
]
