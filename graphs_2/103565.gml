graph [
  node [
    id 0
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 1
    label "coating"
  ]
  node [
    id 2
    label "conclusion"
  ]
  node [
    id 3
    label "koniec"
  ]
  node [
    id 4
    label "runda"
  ]
  node [
    id 5
    label "seria"
  ]
  node [
    id 6
    label "turniej"
  ]
  node [
    id 7
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 8
    label "okr&#261;&#380;enie"
  ]
  node [
    id 9
    label "rhythm"
  ]
  node [
    id 10
    label "rozgrywka"
  ]
  node [
    id 11
    label "czas"
  ]
  node [
    id 12
    label "faza"
  ]
  node [
    id 13
    label "punkt"
  ]
  node [
    id 14
    label "kres_&#380;ycia"
  ]
  node [
    id 15
    label "ostatnie_podrygi"
  ]
  node [
    id 16
    label "&#380;a&#322;oba"
  ]
  node [
    id 17
    label "kres"
  ]
  node [
    id 18
    label "zabicie"
  ]
  node [
    id 19
    label "pogrzebanie"
  ]
  node [
    id 20
    label "wydarzenie"
  ]
  node [
    id 21
    label "visitation"
  ]
  node [
    id 22
    label "miejsce"
  ]
  node [
    id 23
    label "agonia"
  ]
  node [
    id 24
    label "szeol"
  ]
  node [
    id 25
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 26
    label "szereg"
  ]
  node [
    id 27
    label "mogi&#322;a"
  ]
  node [
    id 28
    label "chwila"
  ]
  node [
    id 29
    label "dzia&#322;anie"
  ]
  node [
    id 30
    label "defenestracja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
]
