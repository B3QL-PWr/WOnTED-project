graph [
  node [
    id 0
    label "pi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wczoraj"
    origin "text"
  ]
  node [
    id 2
    label "herbata"
    origin "text"
  ]
  node [
    id 3
    label "fus"
    origin "text"
  ]
  node [
    id 4
    label "u&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "znacz&#261;co"
    origin "text"
  ]
  node [
    id 7
    label "przeprowadzi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dok&#322;adny"
    origin "text"
  ]
  node [
    id 9
    label "analiza"
    origin "text"
  ]
  node [
    id 10
    label "globalny"
    origin "text"
  ]
  node [
    id 11
    label "sytuacja"
    origin "text"
  ]
  node [
    id 12
    label "gospodarczy"
    origin "text"
  ]
  node [
    id 13
    label "zainteresowana"
    origin "text"
  ]
  node [
    id 14
    label "metoda"
    origin "text"
  ]
  node [
    id 15
    label "badawczy"
    origin "text"
  ]
  node [
    id 16
    label "odsy&#322;a&#263;"
    origin "text"
  ]
  node [
    id 17
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 18
    label "niestety"
    origin "text"
  ]
  node [
    id 19
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 20
    label "ustali&#263;"
    origin "text"
  ]
  node [
    id 21
    label "kolejno&#347;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "chronologiczny"
    origin "text"
  ]
  node [
    id 23
    label "tychy"
    origin "text"
  ]
  node [
    id 24
    label "wszyscy"
    origin "text"
  ]
  node [
    id 25
    label "tragiczny"
    origin "text"
  ]
  node [
    id 26
    label "zdarzenie"
    origin "text"
  ]
  node [
    id 27
    label "trzeba"
    origin "text"
  ]
  node [
    id 28
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 29
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 30
    label "mocny"
    origin "text"
  ]
  node [
    id 31
    label "zalewa&#263;"
    origin "text"
  ]
  node [
    id 32
    label "kawa"
    origin "text"
  ]
  node [
    id 33
    label "jeszcze"
    origin "text"
  ]
  node [
    id 34
    label "by&#263;"
    origin "text"
  ]
  node [
    id 35
    label "gotowy"
    origin "text"
  ]
  node [
    id 36
    label "taki"
    origin "text"
  ]
  node [
    id 37
    label "po&#347;wi&#281;cenie"
    origin "text"
  ]
  node [
    id 38
    label "dla"
    origin "text"
  ]
  node [
    id 39
    label "ludzko&#347;&#263;"
    origin "text"
  ]
  node [
    id 40
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 41
    label "przypadkowy"
    origin "text"
  ]
  node [
    id 42
    label "dawno"
  ]
  node [
    id 43
    label "doba"
  ]
  node [
    id 44
    label "niedawno"
  ]
  node [
    id 45
    label "ostatni"
  ]
  node [
    id 46
    label "aktualnie"
  ]
  node [
    id 47
    label "ongi&#347;"
  ]
  node [
    id 48
    label "d&#322;ugotrwale"
  ]
  node [
    id 49
    label "dawnie"
  ]
  node [
    id 50
    label "wcze&#347;niej"
  ]
  node [
    id 51
    label "dawny"
  ]
  node [
    id 52
    label "long_time"
  ]
  node [
    id 53
    label "tydzie&#324;"
  ]
  node [
    id 54
    label "noc"
  ]
  node [
    id 55
    label "czas"
  ]
  node [
    id 56
    label "godzina"
  ]
  node [
    id 57
    label "dzie&#324;"
  ]
  node [
    id 58
    label "jednostka_geologiczna"
  ]
  node [
    id 59
    label "susz"
  ]
  node [
    id 60
    label "krzew"
  ]
  node [
    id 61
    label "ro&#347;lina"
  ]
  node [
    id 62
    label "napar"
  ]
  node [
    id 63
    label "parzy&#263;"
  ]
  node [
    id 64
    label "egzotyk"
  ]
  node [
    id 65
    label "teofilina"
  ]
  node [
    id 66
    label "nap&#243;j"
  ]
  node [
    id 67
    label "u&#380;ywka"
  ]
  node [
    id 68
    label "teina"
  ]
  node [
    id 69
    label "kamelia"
  ]
  node [
    id 70
    label "ciecz"
  ]
  node [
    id 71
    label "porcja"
  ]
  node [
    id 72
    label "substancja"
  ]
  node [
    id 73
    label "wypitek"
  ]
  node [
    id 74
    label "wyr&#243;b"
  ]
  node [
    id 75
    label "&#347;rodek_pobudzaj&#261;cy"
  ]
  node [
    id 76
    label "stimulation"
  ]
  node [
    id 77
    label "herbatowate"
  ]
  node [
    id 78
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 79
    label "skupina"
  ]
  node [
    id 80
    label "wykarczowanie"
  ]
  node [
    id 81
    label "&#322;yko"
  ]
  node [
    id 82
    label "fanerofit"
  ]
  node [
    id 83
    label "karczowa&#263;"
  ]
  node [
    id 84
    label "wykarczowa&#263;"
  ]
  node [
    id 85
    label "&#380;ywop&#322;ot"
  ]
  node [
    id 86
    label "ga&#322;&#261;&#378;"
  ]
  node [
    id 87
    label "karczowanie"
  ]
  node [
    id 88
    label "przedmiot"
  ]
  node [
    id 89
    label "organizm"
  ]
  node [
    id 90
    label "drewno"
  ]
  node [
    id 91
    label "ska&#322;a"
  ]
  node [
    id 92
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 93
    label "wypotnik"
  ]
  node [
    id 94
    label "pochewka"
  ]
  node [
    id 95
    label "strzyc"
  ]
  node [
    id 96
    label "wegetacja"
  ]
  node [
    id 97
    label "zadziorek"
  ]
  node [
    id 98
    label "flawonoid"
  ]
  node [
    id 99
    label "fitotron"
  ]
  node [
    id 100
    label "w&#322;&#243;kno"
  ]
  node [
    id 101
    label "zawi&#261;zek"
  ]
  node [
    id 102
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 103
    label "pora&#380;a&#263;"
  ]
  node [
    id 104
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 105
    label "zbiorowisko"
  ]
  node [
    id 106
    label "do&#322;owa&#263;"
  ]
  node [
    id 107
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 108
    label "hodowla"
  ]
  node [
    id 109
    label "wegetowa&#263;"
  ]
  node [
    id 110
    label "bulwka"
  ]
  node [
    id 111
    label "sok"
  ]
  node [
    id 112
    label "epiderma"
  ]
  node [
    id 113
    label "g&#322;uszy&#263;"
  ]
  node [
    id 114
    label "system_korzeniowy"
  ]
  node [
    id 115
    label "g&#322;uszenie"
  ]
  node [
    id 116
    label "owoc"
  ]
  node [
    id 117
    label "strzy&#380;enie"
  ]
  node [
    id 118
    label "p&#281;d"
  ]
  node [
    id 119
    label "wegetowanie"
  ]
  node [
    id 120
    label "fotoautotrof"
  ]
  node [
    id 121
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 122
    label "gumoza"
  ]
  node [
    id 123
    label "wyro&#347;le"
  ]
  node [
    id 124
    label "fitocenoza"
  ]
  node [
    id 125
    label "ro&#347;liny"
  ]
  node [
    id 126
    label "odn&#243;&#380;ka"
  ]
  node [
    id 127
    label "do&#322;owanie"
  ]
  node [
    id 128
    label "nieuleczalnie_chory"
  ]
  node [
    id 129
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 130
    label "kofeina"
  ]
  node [
    id 131
    label "alkaloid"
  ]
  node [
    id 132
    label "karbonyl"
  ]
  node [
    id 133
    label "theophylline"
  ]
  node [
    id 134
    label "metyl"
  ]
  node [
    id 135
    label "lekarstwo"
  ]
  node [
    id 136
    label "gotowa&#263;"
  ]
  node [
    id 137
    label "uszkadza&#263;"
  ]
  node [
    id 138
    label "inculcate"
  ]
  node [
    id 139
    label "dra&#380;ni&#263;"
  ]
  node [
    id 140
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 141
    label "odka&#380;a&#263;"
  ]
  node [
    id 142
    label "fusy"
  ]
  node [
    id 143
    label "osad"
  ]
  node [
    id 144
    label "terygeniczny"
  ]
  node [
    id 145
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 146
    label "kolmatacja"
  ]
  node [
    id 147
    label "sedymentacja"
  ]
  node [
    id 148
    label "warstwa"
  ]
  node [
    id 149
    label "deposit"
  ]
  node [
    id 150
    label "kompakcja"
  ]
  node [
    id 151
    label "wspomnienie"
  ]
  node [
    id 152
    label "odpad"
  ]
  node [
    id 153
    label "zawiesina"
  ]
  node [
    id 154
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 155
    label "marshal"
  ]
  node [
    id 156
    label "nauczy&#263;"
  ]
  node [
    id 157
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 158
    label "wykszta&#322;ci&#263;"
  ]
  node [
    id 159
    label "zrozumie&#263;"
  ]
  node [
    id 160
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 161
    label "meet"
  ]
  node [
    id 162
    label "przygotowa&#263;"
  ]
  node [
    id 163
    label "spowodowa&#263;"
  ]
  node [
    id 164
    label "stworzy&#263;"
  ]
  node [
    id 165
    label "evolve"
  ]
  node [
    id 166
    label "manipulate"
  ]
  node [
    id 167
    label "wykona&#263;"
  ]
  node [
    id 168
    label "wytworzy&#263;"
  ]
  node [
    id 169
    label "arrange"
  ]
  node [
    id 170
    label "set"
  ]
  node [
    id 171
    label "wyszkoli&#263;"
  ]
  node [
    id 172
    label "dress"
  ]
  node [
    id 173
    label "cook"
  ]
  node [
    id 174
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 175
    label "train"
  ]
  node [
    id 176
    label "ukierunkowa&#263;"
  ]
  node [
    id 177
    label "udoskonali&#263;"
  ]
  node [
    id 178
    label "wys&#322;a&#263;"
  ]
  node [
    id 179
    label "wychowa&#263;"
  ]
  node [
    id 180
    label "wyedukowa&#263;"
  ]
  node [
    id 181
    label "rozwin&#261;&#263;"
  ]
  node [
    id 182
    label "make"
  ]
  node [
    id 183
    label "think"
  ]
  node [
    id 184
    label "skuma&#263;"
  ]
  node [
    id 185
    label "zacz&#261;&#263;"
  ]
  node [
    id 186
    label "oceni&#263;"
  ]
  node [
    id 187
    label "do"
  ]
  node [
    id 188
    label "poczu&#263;"
  ]
  node [
    id 189
    label "pokry&#263;"
  ]
  node [
    id 190
    label "zmieni&#263;"
  ]
  node [
    id 191
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 192
    label "plant"
  ]
  node [
    id 193
    label "pozostawi&#263;"
  ]
  node [
    id 194
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 195
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 196
    label "stagger"
  ]
  node [
    id 197
    label "wear"
  ]
  node [
    id 198
    label "return"
  ]
  node [
    id 199
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 200
    label "wygra&#263;"
  ]
  node [
    id 201
    label "zepsu&#263;"
  ]
  node [
    id 202
    label "raise"
  ]
  node [
    id 203
    label "umie&#347;ci&#263;"
  ]
  node [
    id 204
    label "znak"
  ]
  node [
    id 205
    label "zorganizowa&#263;"
  ]
  node [
    id 206
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 207
    label "wydali&#263;"
  ]
  node [
    id 208
    label "wystylizowa&#263;"
  ]
  node [
    id 209
    label "appoint"
  ]
  node [
    id 210
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 211
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 212
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 213
    label "post&#261;pi&#263;"
  ]
  node [
    id 214
    label "przerobi&#263;"
  ]
  node [
    id 215
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 216
    label "cause"
  ]
  node [
    id 217
    label "nabra&#263;"
  ]
  node [
    id 218
    label "zebra&#263;"
  ]
  node [
    id 219
    label "order"
  ]
  node [
    id 220
    label "ustawi&#263;"
  ]
  node [
    id 221
    label "posprz&#261;ta&#263;"
  ]
  node [
    id 222
    label "zadba&#263;"
  ]
  node [
    id 223
    label "wizerunek"
  ]
  node [
    id 224
    label "specjalista_od_public_relations"
  ]
  node [
    id 225
    label "create"
  ]
  node [
    id 226
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 227
    label "act"
  ]
  node [
    id 228
    label "instruct"
  ]
  node [
    id 229
    label "model"
  ]
  node [
    id 230
    label "nada&#263;"
  ]
  node [
    id 231
    label "znacznie"
  ]
  node [
    id 232
    label "znacz&#261;cy"
  ]
  node [
    id 233
    label "znaczny"
  ]
  node [
    id 234
    label "zauwa&#380;alnie"
  ]
  node [
    id 235
    label "istotnie"
  ]
  node [
    id 236
    label "dono&#347;ny"
  ]
  node [
    id 237
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 238
    label "leave"
  ]
  node [
    id 239
    label "przewie&#347;&#263;"
  ]
  node [
    id 240
    label "zbudowa&#263;"
  ]
  node [
    id 241
    label "pom&#243;c"
  ]
  node [
    id 242
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 243
    label "draw"
  ]
  node [
    id 244
    label "carry"
  ]
  node [
    id 245
    label "dotrze&#263;"
  ]
  node [
    id 246
    label "score"
  ]
  node [
    id 247
    label "uzyska&#263;"
  ]
  node [
    id 248
    label "profit"
  ]
  node [
    id 249
    label "picture"
  ]
  node [
    id 250
    label "manufacture"
  ]
  node [
    id 251
    label "go"
  ]
  node [
    id 252
    label "travel"
  ]
  node [
    id 253
    label "establish"
  ]
  node [
    id 254
    label "zaplanowa&#263;"
  ]
  node [
    id 255
    label "budowla"
  ]
  node [
    id 256
    label "zaskutkowa&#263;"
  ]
  node [
    id 257
    label "help"
  ]
  node [
    id 258
    label "u&#322;atwi&#263;"
  ]
  node [
    id 259
    label "concur"
  ]
  node [
    id 260
    label "aid"
  ]
  node [
    id 261
    label "precyzowanie"
  ]
  node [
    id 262
    label "rzetelny"
  ]
  node [
    id 263
    label "sprecyzowanie"
  ]
  node [
    id 264
    label "miliamperomierz"
  ]
  node [
    id 265
    label "precyzyjny"
  ]
  node [
    id 266
    label "dok&#322;adnie"
  ]
  node [
    id 267
    label "meticulously"
  ]
  node [
    id 268
    label "punctiliously"
  ]
  node [
    id 269
    label "precyzyjnie"
  ]
  node [
    id 270
    label "rzetelnie"
  ]
  node [
    id 271
    label "ustalenie"
  ]
  node [
    id 272
    label "ustalanie"
  ]
  node [
    id 273
    label "amperomierz"
  ]
  node [
    id 274
    label "dobry"
  ]
  node [
    id 275
    label "porz&#261;dny"
  ]
  node [
    id 276
    label "przekonuj&#261;cy"
  ]
  node [
    id 277
    label "konkretny"
  ]
  node [
    id 278
    label "kwalifikowany"
  ]
  node [
    id 279
    label "badanie"
  ]
  node [
    id 280
    label "opis"
  ]
  node [
    id 281
    label "analysis"
  ]
  node [
    id 282
    label "reakcja_chemiczna"
  ]
  node [
    id 283
    label "dissection"
  ]
  node [
    id 284
    label "method"
  ]
  node [
    id 285
    label "spos&#243;b"
  ]
  node [
    id 286
    label "doktryna"
  ]
  node [
    id 287
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 288
    label "krytykowanie"
  ]
  node [
    id 289
    label "sprawdzanie"
  ]
  node [
    id 290
    label "praca"
  ]
  node [
    id 291
    label "udowadnianie"
  ]
  node [
    id 292
    label "examination"
  ]
  node [
    id 293
    label "macanie"
  ]
  node [
    id 294
    label "rezultat"
  ]
  node [
    id 295
    label "investigation"
  ]
  node [
    id 296
    label "usi&#322;owanie"
  ]
  node [
    id 297
    label "czynno&#347;&#263;"
  ]
  node [
    id 298
    label "dociekanie"
  ]
  node [
    id 299
    label "bia&#322;a_niedziela"
  ]
  node [
    id 300
    label "obserwowanie"
  ]
  node [
    id 301
    label "penetrowanie"
  ]
  node [
    id 302
    label "kontrola"
  ]
  node [
    id 303
    label "zrecenzowanie"
  ]
  node [
    id 304
    label "diagnostyka"
  ]
  node [
    id 305
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 306
    label "omawianie"
  ]
  node [
    id 307
    label "wziernikowanie"
  ]
  node [
    id 308
    label "rozpatrywanie"
  ]
  node [
    id 309
    label "rektalny"
  ]
  node [
    id 310
    label "wypowied&#378;"
  ]
  node [
    id 311
    label "obja&#347;nienie"
  ]
  node [
    id 312
    label "exposition"
  ]
  node [
    id 313
    label "globalnie"
  ]
  node [
    id 314
    label "&#347;wiatowo"
  ]
  node [
    id 315
    label "generalny"
  ]
  node [
    id 316
    label "ca&#322;o&#347;ciowy"
  ]
  node [
    id 317
    label "ca&#322;o&#347;ciowo"
  ]
  node [
    id 318
    label "pe&#322;ny"
  ]
  node [
    id 319
    label "og&#243;lny"
  ]
  node [
    id 320
    label "wielostronny"
  ]
  node [
    id 321
    label "nadrz&#281;dny"
  ]
  node [
    id 322
    label "podstawowy"
  ]
  node [
    id 323
    label "zwierzchni"
  ]
  node [
    id 324
    label "og&#243;lnie"
  ]
  node [
    id 325
    label "zasadniczy"
  ]
  node [
    id 326
    label "generalnie"
  ]
  node [
    id 327
    label "&#347;wiatowy"
  ]
  node [
    id 328
    label "internationally"
  ]
  node [
    id 329
    label "warunki"
  ]
  node [
    id 330
    label "szczeg&#243;&#322;"
  ]
  node [
    id 331
    label "realia"
  ]
  node [
    id 332
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 333
    label "state"
  ]
  node [
    id 334
    label "motyw"
  ]
  node [
    id 335
    label "sk&#322;adnik"
  ]
  node [
    id 336
    label "wydarzenie"
  ]
  node [
    id 337
    label "status"
  ]
  node [
    id 338
    label "zniuansowa&#263;"
  ]
  node [
    id 339
    label "niuansowa&#263;"
  ]
  node [
    id 340
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 341
    label "element"
  ]
  node [
    id 342
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 343
    label "cecha"
  ]
  node [
    id 344
    label "fraza"
  ]
  node [
    id 345
    label "ozdoba"
  ]
  node [
    id 346
    label "przyczyna"
  ]
  node [
    id 347
    label "temat"
  ]
  node [
    id 348
    label "melodia"
  ]
  node [
    id 349
    label "message"
  ]
  node [
    id 350
    label "kontekst"
  ]
  node [
    id 351
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 352
    label "gospodarski"
  ]
  node [
    id 353
    label "gospodarnie"
  ]
  node [
    id 354
    label "dziarski"
  ]
  node [
    id 355
    label "oszcz&#281;dny"
  ]
  node [
    id 356
    label "typowy"
  ]
  node [
    id 357
    label "stronniczy"
  ]
  node [
    id 358
    label "wiejski"
  ]
  node [
    id 359
    label "po_gospodarsku"
  ]
  node [
    id 360
    label "racjonalny"
  ]
  node [
    id 361
    label "zbi&#243;r"
  ]
  node [
    id 362
    label "narz&#281;dzie"
  ]
  node [
    id 363
    label "nature"
  ]
  node [
    id 364
    label "tryb"
  ]
  node [
    id 365
    label "strategia"
  ]
  node [
    id 366
    label "doctrine"
  ]
  node [
    id 367
    label "teoria"
  ]
  node [
    id 368
    label "badawczo"
  ]
  node [
    id 369
    label "uwa&#380;ny"
  ]
  node [
    id 370
    label "czujny"
  ]
  node [
    id 371
    label "bystry"
  ]
  node [
    id 372
    label "uwa&#380;nie"
  ]
  node [
    id 373
    label "podpowiada&#263;"
  ]
  node [
    id 374
    label "remit"
  ]
  node [
    id 375
    label "doradza&#263;"
  ]
  node [
    id 376
    label "pomaga&#263;"
  ]
  node [
    id 377
    label "m&#243;wi&#263;"
  ]
  node [
    id 378
    label "prompt"
  ]
  node [
    id 379
    label "czynnik"
  ]
  node [
    id 380
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 381
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 382
    label "&#347;wiadectwo"
  ]
  node [
    id 383
    label "kamena"
  ]
  node [
    id 384
    label "pocz&#261;tek"
  ]
  node [
    id 385
    label "poci&#261;ganie"
  ]
  node [
    id 386
    label "geneza"
  ]
  node [
    id 387
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 388
    label "bra&#263;_si&#281;"
  ]
  node [
    id 389
    label "ciek_wodny"
  ]
  node [
    id 390
    label "matuszka"
  ]
  node [
    id 391
    label "subject"
  ]
  node [
    id 392
    label "certificate"
  ]
  node [
    id 393
    label "o&#347;wiadczenie"
  ]
  node [
    id 394
    label "dokument"
  ]
  node [
    id 395
    label "za&#347;wiadczenie"
  ]
  node [
    id 396
    label "dow&#243;d"
  ]
  node [
    id 397
    label "promocja"
  ]
  node [
    id 398
    label "miejsce"
  ]
  node [
    id 399
    label "upgrade"
  ]
  node [
    id 400
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 401
    label "pierworodztwo"
  ]
  node [
    id 402
    label "faza"
  ]
  node [
    id 403
    label "nast&#281;pstwo"
  ]
  node [
    id 404
    label "faktor"
  ]
  node [
    id 405
    label "iloczyn"
  ]
  node [
    id 406
    label "divisor"
  ]
  node [
    id 407
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 408
    label "ekspozycja"
  ]
  node [
    id 409
    label "agent"
  ]
  node [
    id 410
    label "wieszczka"
  ]
  node [
    id 411
    label "nimfa"
  ]
  node [
    id 412
    label "Egeria"
  ]
  node [
    id 413
    label "rzymski"
  ]
  node [
    id 414
    label "move"
  ]
  node [
    id 415
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 416
    label "upicie"
  ]
  node [
    id 417
    label "myk"
  ]
  node [
    id 418
    label "wessanie"
  ]
  node [
    id 419
    label "ruszenie"
  ]
  node [
    id 420
    label "pull"
  ]
  node [
    id 421
    label "przechylenie"
  ]
  node [
    id 422
    label "zainstalowanie"
  ]
  node [
    id 423
    label "nos"
  ]
  node [
    id 424
    label "przesuni&#281;cie"
  ]
  node [
    id 425
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 426
    label "wyszarpanie"
  ]
  node [
    id 427
    label "powianie"
  ]
  node [
    id 428
    label "si&#261;kanie"
  ]
  node [
    id 429
    label "p&#243;j&#347;cie"
  ]
  node [
    id 430
    label "wywo&#322;anie"
  ]
  node [
    id 431
    label "posuni&#281;cie"
  ]
  node [
    id 432
    label "pokrycie"
  ]
  node [
    id 433
    label "zaci&#261;ganie"
  ]
  node [
    id 434
    label "typ"
  ]
  node [
    id 435
    label "dzia&#322;anie"
  ]
  node [
    id 436
    label "event"
  ]
  node [
    id 437
    label "temptation"
  ]
  node [
    id 438
    label "wsysanie"
  ]
  node [
    id 439
    label "zerwanie"
  ]
  node [
    id 440
    label "przechylanie"
  ]
  node [
    id 441
    label "oddarcie"
  ]
  node [
    id 442
    label "dzianie_si&#281;"
  ]
  node [
    id 443
    label "powiewanie"
  ]
  node [
    id 444
    label "manienie"
  ]
  node [
    id 445
    label "urwanie"
  ]
  node [
    id 446
    label "urywanie"
  ]
  node [
    id 447
    label "powodowanie"
  ]
  node [
    id 448
    label "interesowanie"
  ]
  node [
    id 449
    label "implikacja"
  ]
  node [
    id 450
    label "przesuwanie"
  ]
  node [
    id 451
    label "upijanie"
  ]
  node [
    id 452
    label "powlekanie"
  ]
  node [
    id 453
    label "powleczenie"
  ]
  node [
    id 454
    label "oddzieranie"
  ]
  node [
    id 455
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 456
    label "traction"
  ]
  node [
    id 457
    label "ruszanie"
  ]
  node [
    id 458
    label "pokrywanie"
  ]
  node [
    id 459
    label "ojczyzna"
  ]
  node [
    id 460
    label "popadia"
  ]
  node [
    id 461
    label "proces"
  ]
  node [
    id 462
    label "powstanie"
  ]
  node [
    id 463
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 464
    label "zaistnienie"
  ]
  node [
    id 465
    label "give"
  ]
  node [
    id 466
    label "rodny"
  ]
  node [
    id 467
    label "monogeneza"
  ]
  node [
    id 468
    label "zesp&#243;&#322;"
  ]
  node [
    id 469
    label "represent"
  ]
  node [
    id 470
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 471
    label "zdecydowa&#263;"
  ]
  node [
    id 472
    label "unwrap"
  ]
  node [
    id 473
    label "put"
  ]
  node [
    id 474
    label "bind"
  ]
  node [
    id 475
    label "umocni&#263;"
  ]
  node [
    id 476
    label "podnie&#347;&#263;"
  ]
  node [
    id 477
    label "umocnienie"
  ]
  node [
    id 478
    label "fixate"
  ]
  node [
    id 479
    label "wzmocni&#263;"
  ]
  node [
    id 480
    label "utrwali&#263;"
  ]
  node [
    id 481
    label "zabezpieczy&#263;"
  ]
  node [
    id 482
    label "ustabilizowa&#263;"
  ]
  node [
    id 483
    label "decide"
  ]
  node [
    id 484
    label "podj&#261;&#263;"
  ]
  node [
    id 485
    label "sta&#263;_si&#281;"
  ]
  node [
    id 486
    label "determine"
  ]
  node [
    id 487
    label "uk&#322;ad"
  ]
  node [
    id 488
    label "ONZ"
  ]
  node [
    id 489
    label "podsystem"
  ]
  node [
    id 490
    label "NATO"
  ]
  node [
    id 491
    label "systemat"
  ]
  node [
    id 492
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 493
    label "traktat_wersalski"
  ]
  node [
    id 494
    label "przestawi&#263;"
  ]
  node [
    id 495
    label "konstelacja"
  ]
  node [
    id 496
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 497
    label "organ"
  ]
  node [
    id 498
    label "zawarcie"
  ]
  node [
    id 499
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 500
    label "rozprz&#261;c"
  ]
  node [
    id 501
    label "usenet"
  ]
  node [
    id 502
    label "wi&#281;&#378;"
  ]
  node [
    id 503
    label "treaty"
  ]
  node [
    id 504
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 505
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 506
    label "struktura"
  ]
  node [
    id 507
    label "o&#347;"
  ]
  node [
    id 508
    label "zachowanie"
  ]
  node [
    id 509
    label "umowa"
  ]
  node [
    id 510
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 511
    label "cybernetyk"
  ]
  node [
    id 512
    label "system"
  ]
  node [
    id 513
    label "cia&#322;o"
  ]
  node [
    id 514
    label "zawrze&#263;"
  ]
  node [
    id 515
    label "alliance"
  ]
  node [
    id 516
    label "sk&#322;ad"
  ]
  node [
    id 517
    label "chronologicznie"
  ]
  node [
    id 518
    label "straszny"
  ]
  node [
    id 519
    label "koszmarny"
  ]
  node [
    id 520
    label "nacechowany"
  ]
  node [
    id 521
    label "wstrz&#261;saj&#261;cy"
  ]
  node [
    id 522
    label "dramatyczny"
  ]
  node [
    id 523
    label "feralny"
  ]
  node [
    id 524
    label "&#347;miertelny"
  ]
  node [
    id 525
    label "pechowy"
  ]
  node [
    id 526
    label "traiczny"
  ]
  node [
    id 527
    label "nieszcz&#281;sny"
  ]
  node [
    id 528
    label "tragicznie"
  ]
  node [
    id 529
    label "feralnie"
  ]
  node [
    id 530
    label "nieszcz&#281;&#347;liwy"
  ]
  node [
    id 531
    label "niepomy&#347;lny"
  ]
  node [
    id 532
    label "nieudany"
  ]
  node [
    id 533
    label "nieszcz&#281;&#347;liwie"
  ]
  node [
    id 534
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 535
    label "powa&#380;ny"
  ]
  node [
    id 536
    label "przej&#281;ty"
  ]
  node [
    id 537
    label "emocjonuj&#261;cy"
  ]
  node [
    id 538
    label "dramatycznie"
  ]
  node [
    id 539
    label "dynamiczny"
  ]
  node [
    id 540
    label "niegrzeczny"
  ]
  node [
    id 541
    label "kurewski"
  ]
  node [
    id 542
    label "niemoralny"
  ]
  node [
    id 543
    label "strasznie"
  ]
  node [
    id 544
    label "olbrzymi"
  ]
  node [
    id 545
    label "przejmuj&#261;cy"
  ]
  node [
    id 546
    label "pora&#380;aj&#261;co"
  ]
  node [
    id 547
    label "typowo"
  ]
  node [
    id 548
    label "zwyk&#322;y"
  ]
  node [
    id 549
    label "zwyczajny"
  ]
  node [
    id 550
    label "cz&#281;sty"
  ]
  node [
    id 551
    label "straszliwy"
  ]
  node [
    id 552
    label "okropny"
  ]
  node [
    id 553
    label "masakryczny"
  ]
  node [
    id 554
    label "koszmarnie"
  ]
  node [
    id 555
    label "senny"
  ]
  node [
    id 556
    label "makabrycznie"
  ]
  node [
    id 557
    label "&#347;miertelnie"
  ]
  node [
    id 558
    label "przed&#347;miertelny"
  ]
  node [
    id 559
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 560
    label "za&#380;arty"
  ]
  node [
    id 561
    label "zaciek&#322;y"
  ]
  node [
    id 562
    label "niebezpieczny"
  ]
  node [
    id 563
    label "wielki"
  ]
  node [
    id 564
    label "ko&#324;cowy"
  ]
  node [
    id 565
    label "charakter"
  ]
  node [
    id 566
    label "przebiegni&#281;cie"
  ]
  node [
    id 567
    label "przebiec"
  ]
  node [
    id 568
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 569
    label "fabu&#322;a"
  ]
  node [
    id 570
    label "perypetia"
  ]
  node [
    id 571
    label "w&#261;tek"
  ]
  node [
    id 572
    label "opowiadanie"
  ]
  node [
    id 573
    label "w&#281;ze&#322;"
  ]
  node [
    id 574
    label "cz&#322;owiek"
  ]
  node [
    id 575
    label "fizjonomia"
  ]
  node [
    id 576
    label "kompleksja"
  ]
  node [
    id 577
    label "zjawisko"
  ]
  node [
    id 578
    label "entity"
  ]
  node [
    id 579
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 580
    label "psychika"
  ]
  node [
    id 581
    label "posta&#263;"
  ]
  node [
    id 582
    label "osobowo&#347;&#263;"
  ]
  node [
    id 583
    label "bezproblemowy"
  ]
  node [
    id 584
    label "activity"
  ]
  node [
    id 585
    label "proceed"
  ]
  node [
    id 586
    label "fly"
  ]
  node [
    id 587
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 588
    label "przeby&#263;"
  ]
  node [
    id 589
    label "przesun&#261;&#263;_si&#281;"
  ]
  node [
    id 590
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 591
    label "przesun&#261;&#263;"
  ]
  node [
    id 592
    label "przemierzy&#263;"
  ]
  node [
    id 593
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 594
    label "p&#243;j&#347;&#263;"
  ]
  node [
    id 595
    label "run"
  ]
  node [
    id 596
    label "przebycie"
  ]
  node [
    id 597
    label "przemkni&#281;cie"
  ]
  node [
    id 598
    label "zdarzenie_si&#281;"
  ]
  node [
    id 599
    label "up&#322;yni&#281;cie"
  ]
  node [
    id 600
    label "zabrzmienie"
  ]
  node [
    id 601
    label "necessity"
  ]
  node [
    id 602
    label "trza"
  ]
  node [
    id 603
    label "see"
  ]
  node [
    id 604
    label "advance"
  ]
  node [
    id 605
    label "za&#322;atwi&#263;_si&#281;"
  ]
  node [
    id 606
    label "sack"
  ]
  node [
    id 607
    label "usun&#261;&#263;"
  ]
  node [
    id 608
    label "restore"
  ]
  node [
    id 609
    label "stage"
  ]
  node [
    id 610
    label "ensnare"
  ]
  node [
    id 611
    label "pozyska&#263;"
  ]
  node [
    id 612
    label "plan"
  ]
  node [
    id 613
    label "wprowadzi&#263;"
  ]
  node [
    id 614
    label "standard"
  ]
  node [
    id 615
    label "urobi&#263;"
  ]
  node [
    id 616
    label "skupi&#263;"
  ]
  node [
    id 617
    label "dostosowa&#263;"
  ]
  node [
    id 618
    label "podbi&#263;"
  ]
  node [
    id 619
    label "accommodate"
  ]
  node [
    id 620
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 621
    label "doprowadzi&#263;"
  ]
  node [
    id 622
    label "pomy&#347;le&#263;"
  ]
  node [
    id 623
    label "zadowoli&#263;"
  ]
  node [
    id 624
    label "zaaran&#380;owa&#263;"
  ]
  node [
    id 625
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 626
    label "wkr&#281;ci&#263;"
  ]
  node [
    id 627
    label "or&#380;n&#261;&#263;"
  ]
  node [
    id 628
    label "kupi&#263;"
  ]
  node [
    id 629
    label "deceive"
  ]
  node [
    id 630
    label "oszwabi&#263;"
  ]
  node [
    id 631
    label "przyw&#322;aszczy&#263;"
  ]
  node [
    id 632
    label "naby&#263;"
  ]
  node [
    id 633
    label "fraud"
  ]
  node [
    id 634
    label "woda"
  ]
  node [
    id 635
    label "wzi&#261;&#263;"
  ]
  node [
    id 636
    label "dopu&#347;ci&#263;_si&#281;"
  ]
  node [
    id 637
    label "gull"
  ]
  node [
    id 638
    label "hoax"
  ]
  node [
    id 639
    label "objecha&#263;"
  ]
  node [
    id 640
    label "stylize"
  ]
  node [
    id 641
    label "upodobni&#263;"
  ]
  node [
    id 642
    label "zmodyfikowa&#263;"
  ]
  node [
    id 643
    label "zaliczy&#263;"
  ]
  node [
    id 644
    label "upora&#263;_si&#281;"
  ]
  node [
    id 645
    label "convert"
  ]
  node [
    id 646
    label "przetworzy&#263;"
  ]
  node [
    id 647
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 648
    label "change"
  ]
  node [
    id 649
    label "prze&#380;y&#263;"
  ]
  node [
    id 650
    label "overwork"
  ]
  node [
    id 651
    label "przej&#347;&#263;"
  ]
  node [
    id 652
    label "zamieni&#263;"
  ]
  node [
    id 653
    label "sprawi&#263;"
  ]
  node [
    id 654
    label "wzmacnia&#263;"
  ]
  node [
    id 655
    label "silny"
  ]
  node [
    id 656
    label "wytrzyma&#322;y"
  ]
  node [
    id 657
    label "mocno"
  ]
  node [
    id 658
    label "krzepki"
  ]
  node [
    id 659
    label "du&#380;y"
  ]
  node [
    id 660
    label "niepodwa&#380;alny"
  ]
  node [
    id 661
    label "wyrazisty"
  ]
  node [
    id 662
    label "stabilny"
  ]
  node [
    id 663
    label "widoczny"
  ]
  node [
    id 664
    label "trudny"
  ]
  node [
    id 665
    label "szczery"
  ]
  node [
    id 666
    label "intensywnie"
  ]
  node [
    id 667
    label "meflochina"
  ]
  node [
    id 668
    label "zdecydowany"
  ]
  node [
    id 669
    label "silnie"
  ]
  node [
    id 670
    label "wyrazi&#347;cie"
  ]
  node [
    id 671
    label "ciekawy"
  ]
  node [
    id 672
    label "nieoboj&#281;tny"
  ]
  node [
    id 673
    label "wyra&#378;ny"
  ]
  node [
    id 674
    label "wyra&#378;nie"
  ]
  node [
    id 675
    label "ci&#281;&#380;ko"
  ]
  node [
    id 676
    label "skomplikowany"
  ]
  node [
    id 677
    label "k&#322;opotliwy"
  ]
  node [
    id 678
    label "wymagaj&#261;cy"
  ]
  node [
    id 679
    label "pewny"
  ]
  node [
    id 680
    label "stabilnie"
  ]
  node [
    id 681
    label "trwa&#322;y"
  ]
  node [
    id 682
    label "sta&#322;y"
  ]
  node [
    id 683
    label "zauwa&#380;alny"
  ]
  node [
    id 684
    label "zdecydowanie"
  ]
  node [
    id 685
    label "zajebisty"
  ]
  node [
    id 686
    label "&#380;ywotny"
  ]
  node [
    id 687
    label "zdrowy"
  ]
  node [
    id 688
    label "intensywny"
  ]
  node [
    id 689
    label "krzepienie"
  ]
  node [
    id 690
    label "pokrzepienie"
  ]
  node [
    id 691
    label "krzepko"
  ]
  node [
    id 692
    label "skuteczny"
  ]
  node [
    id 693
    label "ca&#322;y"
  ]
  node [
    id 694
    label "czw&#243;rka"
  ]
  node [
    id 695
    label "spokojny"
  ]
  node [
    id 696
    label "pos&#322;uszny"
  ]
  node [
    id 697
    label "korzystny"
  ]
  node [
    id 698
    label "drogi"
  ]
  node [
    id 699
    label "pozytywny"
  ]
  node [
    id 700
    label "moralny"
  ]
  node [
    id 701
    label "pomy&#347;lny"
  ]
  node [
    id 702
    label "powitanie"
  ]
  node [
    id 703
    label "grzeczny"
  ]
  node [
    id 704
    label "&#347;mieszny"
  ]
  node [
    id 705
    label "odpowiedni"
  ]
  node [
    id 706
    label "zwrot"
  ]
  node [
    id 707
    label "dobrze"
  ]
  node [
    id 708
    label "dobroczynny"
  ]
  node [
    id 709
    label "mi&#322;y"
  ]
  node [
    id 710
    label "du&#380;o"
  ]
  node [
    id 711
    label "wa&#380;ny"
  ]
  node [
    id 712
    label "niema&#322;o"
  ]
  node [
    id 713
    label "wiele"
  ]
  node [
    id 714
    label "prawdziwy"
  ]
  node [
    id 715
    label "rozwini&#281;ty"
  ]
  node [
    id 716
    label "doros&#322;y"
  ]
  node [
    id 717
    label "dorodny"
  ]
  node [
    id 718
    label "widny"
  ]
  node [
    id 719
    label "widomy"
  ]
  node [
    id 720
    label "dostrzegalny"
  ]
  node [
    id 721
    label "wystawianie_si&#281;"
  ]
  node [
    id 722
    label "widocznie"
  ]
  node [
    id 723
    label "fizyczny"
  ]
  node [
    id 724
    label "wygl&#261;danie"
  ]
  node [
    id 725
    label "ods&#322;oni&#281;cie"
  ]
  node [
    id 726
    label "wyjrzenie"
  ]
  node [
    id 727
    label "ods&#322;anianie"
  ]
  node [
    id 728
    label "pojawianie_si&#281;"
  ]
  node [
    id 729
    label "zarysowanie_si&#281;"
  ]
  node [
    id 730
    label "wystawienie_si&#281;"
  ]
  node [
    id 731
    label "widnienie"
  ]
  node [
    id 732
    label "widzialny"
  ]
  node [
    id 733
    label "czysty"
  ]
  node [
    id 734
    label "prostoduszny"
  ]
  node [
    id 735
    label "s&#322;uszny"
  ]
  node [
    id 736
    label "uczciwy"
  ]
  node [
    id 737
    label "szczodry"
  ]
  node [
    id 738
    label "szczerze"
  ]
  node [
    id 739
    label "szczyry"
  ]
  node [
    id 740
    label "niepodwa&#380;alnie"
  ]
  node [
    id 741
    label "przekonuj&#261;co"
  ]
  node [
    id 742
    label "po&#380;ywny"
  ]
  node [
    id 743
    label "ogarni&#281;ty"
  ]
  node [
    id 744
    label "posilny"
  ]
  node [
    id 745
    label "niez&#322;y"
  ]
  node [
    id 746
    label "tre&#347;ciwy"
  ]
  node [
    id 747
    label "konkretnie"
  ]
  node [
    id 748
    label "skupiony"
  ]
  node [
    id 749
    label "jasny"
  ]
  node [
    id 750
    label "&#322;adny"
  ]
  node [
    id 751
    label "jaki&#347;"
  ]
  node [
    id 752
    label "solidnie"
  ]
  node [
    id 753
    label "okre&#347;lony"
  ]
  node [
    id 754
    label "abstrakcyjny"
  ]
  node [
    id 755
    label "powerfully"
  ]
  node [
    id 756
    label "strongly"
  ]
  node [
    id 757
    label "robi&#263;"
  ]
  node [
    id 758
    label "zmienia&#263;"
  ]
  node [
    id 759
    label "podnosi&#263;"
  ]
  node [
    id 760
    label "confirm"
  ]
  node [
    id 761
    label "wzmaga&#263;"
  ]
  node [
    id 762
    label "utrwala&#263;"
  ]
  node [
    id 763
    label "powodowa&#263;"
  ]
  node [
    id 764
    label "uskutecznia&#263;"
  ]
  node [
    id 765
    label "regulowa&#263;"
  ]
  node [
    id 766
    label "zabezpiecza&#263;"
  ]
  node [
    id 767
    label "build_up"
  ]
  node [
    id 768
    label "reinforce"
  ]
  node [
    id 769
    label "poprawia&#263;"
  ]
  node [
    id 770
    label "zwi&#281;ksza&#263;"
  ]
  node [
    id 771
    label "uskuteczni&#263;"
  ]
  node [
    id 772
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 773
    label "wyregulowa&#263;"
  ]
  node [
    id 774
    label "wzm&#243;c"
  ]
  node [
    id 775
    label "consolidate"
  ]
  node [
    id 776
    label "g&#281;sto"
  ]
  node [
    id 777
    label "dynamicznie"
  ]
  node [
    id 778
    label "zajebi&#347;cie"
  ]
  node [
    id 779
    label "dusznie"
  ]
  node [
    id 780
    label "antymalaryczny"
  ]
  node [
    id 781
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 782
    label "antymalaryk"
  ]
  node [
    id 783
    label "doustny"
  ]
  node [
    id 784
    label "twardnienie"
  ]
  node [
    id 785
    label "wytrzymale"
  ]
  node [
    id 786
    label "uodparnianie"
  ]
  node [
    id 787
    label "uodparnianie_si&#281;"
  ]
  node [
    id 788
    label "uodpornienie"
  ]
  node [
    id 789
    label "zahartowanie"
  ]
  node [
    id 790
    label "uodpornienie_si&#281;"
  ]
  node [
    id 791
    label "odporny"
  ]
  node [
    id 792
    label "hartowny"
  ]
  node [
    id 793
    label "utwardzanie"
  ]
  node [
    id 794
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 795
    label "plami&#263;"
  ]
  node [
    id 796
    label "k&#322;ama&#263;"
  ]
  node [
    id 797
    label "ton&#261;&#263;"
  ]
  node [
    id 798
    label "pokrywa&#263;"
  ]
  node [
    id 799
    label "pour"
  ]
  node [
    id 800
    label "moczy&#263;"
  ]
  node [
    id 801
    label "dawa&#263;"
  ]
  node [
    id 802
    label "spuszcza&#263;_si&#281;"
  ]
  node [
    id 803
    label "flood"
  ]
  node [
    id 804
    label "oblewa&#263;"
  ]
  node [
    id 805
    label "wlewa&#263;"
  ]
  node [
    id 806
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 807
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 808
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 809
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 810
    label "stan"
  ]
  node [
    id 811
    label "stand"
  ]
  node [
    id 812
    label "trwa&#263;"
  ]
  node [
    id 813
    label "equal"
  ]
  node [
    id 814
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 815
    label "chodzi&#263;"
  ]
  node [
    id 816
    label "uczestniczy&#263;"
  ]
  node [
    id 817
    label "obecno&#347;&#263;"
  ]
  node [
    id 818
    label "si&#281;ga&#263;"
  ]
  node [
    id 819
    label "mie&#263;_miejsce"
  ]
  node [
    id 820
    label "wychodzi&#263;"
  ]
  node [
    id 821
    label "seclude"
  ]
  node [
    id 822
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 823
    label "overture"
  ]
  node [
    id 824
    label "perform"
  ]
  node [
    id 825
    label "dzia&#322;a&#263;"
  ]
  node [
    id 826
    label "odst&#281;powa&#263;"
  ]
  node [
    id 827
    label "appear"
  ]
  node [
    id 828
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 829
    label "rezygnowa&#263;"
  ]
  node [
    id 830
    label "nak&#322;ania&#263;"
  ]
  node [
    id 831
    label "dopiera&#263;_si&#281;"
  ]
  node [
    id 832
    label "exsert"
  ]
  node [
    id 833
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 834
    label "umieszcza&#263;"
  ]
  node [
    id 835
    label "powierza&#263;"
  ]
  node [
    id 836
    label "wpiernicza&#263;"
  ]
  node [
    id 837
    label "tender"
  ]
  node [
    id 838
    label "hold_out"
  ]
  node [
    id 839
    label "rap"
  ]
  node [
    id 840
    label "obiecywa&#263;"
  ]
  node [
    id 841
    label "&#322;adowa&#263;"
  ]
  node [
    id 842
    label "t&#322;uc"
  ]
  node [
    id 843
    label "nalewa&#263;"
  ]
  node [
    id 844
    label "hold"
  ]
  node [
    id 845
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 846
    label "przekazywa&#263;"
  ]
  node [
    id 847
    label "zezwala&#263;"
  ]
  node [
    id 848
    label "render"
  ]
  node [
    id 849
    label "dostarcza&#263;"
  ]
  node [
    id 850
    label "przeznacza&#263;"
  ]
  node [
    id 851
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 852
    label "p&#322;aci&#263;"
  ]
  node [
    id 853
    label "surrender"
  ]
  node [
    id 854
    label "traktowa&#263;"
  ]
  node [
    id 855
    label "glaze"
  ]
  node [
    id 856
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 857
    label "powleka&#263;"
  ]
  node [
    id 858
    label "przegrywa&#263;"
  ]
  node [
    id 859
    label "la&#263;"
  ]
  node [
    id 860
    label "egzamin"
  ]
  node [
    id 861
    label "barwi&#263;"
  ]
  node [
    id 862
    label "ocenia&#263;"
  ]
  node [
    id 863
    label "egzaminowa&#263;"
  ]
  node [
    id 864
    label "op&#322;ywa&#263;"
  ]
  node [
    id 865
    label "spill"
  ]
  node [
    id 866
    label "kali&#263;"
  ]
  node [
    id 867
    label "szkodzi&#263;"
  ]
  node [
    id 868
    label "zeszmaca&#263;"
  ]
  node [
    id 869
    label "szarpa&#263;"
  ]
  node [
    id 870
    label "mistreat"
  ]
  node [
    id 871
    label "defile"
  ]
  node [
    id 872
    label "brudzi&#263;"
  ]
  node [
    id 873
    label "szarga&#263;"
  ]
  node [
    id 874
    label "prostytuowa&#263;"
  ]
  node [
    id 875
    label "overcharge"
  ]
  node [
    id 876
    label "ogarnia&#263;"
  ]
  node [
    id 877
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 878
    label "shoot"
  ]
  node [
    id 879
    label "zasila&#263;"
  ]
  node [
    id 880
    label "wype&#322;nia&#263;"
  ]
  node [
    id 881
    label "wzbiera&#263;"
  ]
  node [
    id 882
    label "kapita&#322;"
  ]
  node [
    id 883
    label "dociera&#263;"
  ]
  node [
    id 884
    label "dane"
  ]
  node [
    id 885
    label "gromadzi&#263;_si&#281;"
  ]
  node [
    id 886
    label "zbiera&#263;_si&#281;"
  ]
  node [
    id 887
    label "smother"
  ]
  node [
    id 888
    label "cover"
  ]
  node [
    id 889
    label "przykrywa&#263;"
  ]
  node [
    id 890
    label "report"
  ]
  node [
    id 891
    label "defray"
  ]
  node [
    id 892
    label "r&#243;wna&#263;"
  ]
  node [
    id 893
    label "supernatural"
  ]
  node [
    id 894
    label "zaspokaja&#263;"
  ]
  node [
    id 895
    label "rozwija&#263;"
  ]
  node [
    id 896
    label "zap&#322;adnia&#263;"
  ]
  node [
    id 897
    label "maskowa&#263;"
  ]
  node [
    id 898
    label "oszukiwa&#263;"
  ]
  node [
    id 899
    label "mitomania"
  ]
  node [
    id 900
    label "pitoli&#263;"
  ]
  node [
    id 901
    label "lie"
  ]
  node [
    id 902
    label "zanurza&#263;_si&#281;"
  ]
  node [
    id 903
    label "opada&#263;"
  ]
  node [
    id 904
    label "p&#322;ywa&#263;"
  ]
  node [
    id 905
    label "sink"
  ]
  node [
    id 906
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 907
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 908
    label "przepada&#263;"
  ]
  node [
    id 909
    label "swimming"
  ]
  node [
    id 910
    label "zanika&#263;"
  ]
  node [
    id 911
    label "pogarsza&#263;_si&#281;"
  ]
  node [
    id 912
    label "pogr&#261;&#380;a&#263;_si&#281;"
  ]
  node [
    id 913
    label "przesadza&#263;"
  ]
  node [
    id 914
    label "inspirowa&#263;"
  ]
  node [
    id 915
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 916
    label "wmusza&#263;"
  ]
  node [
    id 917
    label "wzbudza&#263;"
  ]
  node [
    id 918
    label "wpaja&#263;"
  ]
  node [
    id 919
    label "pestkowiec"
  ]
  node [
    id 920
    label "jedzenie"
  ]
  node [
    id 921
    label "produkt"
  ]
  node [
    id 922
    label "chemex"
  ]
  node [
    id 923
    label "ziarno"
  ]
  node [
    id 924
    label "marzanowate"
  ]
  node [
    id 925
    label "dripper"
  ]
  node [
    id 926
    label "przejadanie"
  ]
  node [
    id 927
    label "jadanie"
  ]
  node [
    id 928
    label "podanie"
  ]
  node [
    id 929
    label "posilanie"
  ]
  node [
    id 930
    label "przejedzenie"
  ]
  node [
    id 931
    label "szama"
  ]
  node [
    id 932
    label "odpasienie_si&#281;"
  ]
  node [
    id 933
    label "papusianie"
  ]
  node [
    id 934
    label "ufetowanie_si&#281;"
  ]
  node [
    id 935
    label "wyjadanie"
  ]
  node [
    id 936
    label "wpieprzanie"
  ]
  node [
    id 937
    label "wmuszanie"
  ]
  node [
    id 938
    label "objadanie"
  ]
  node [
    id 939
    label "odpasanie_si&#281;"
  ]
  node [
    id 940
    label "mlaskanie"
  ]
  node [
    id 941
    label "posilenie"
  ]
  node [
    id 942
    label "polowanie"
  ]
  node [
    id 943
    label "&#380;arcie"
  ]
  node [
    id 944
    label "przejadanie_si&#281;"
  ]
  node [
    id 945
    label "podawanie"
  ]
  node [
    id 946
    label "koryto"
  ]
  node [
    id 947
    label "podawa&#263;"
  ]
  node [
    id 948
    label "jad&#322;o"
  ]
  node [
    id 949
    label "przejedzenie_si&#281;"
  ]
  node [
    id 950
    label "eating"
  ]
  node [
    id 951
    label "wiwenda"
  ]
  node [
    id 952
    label "rzecz"
  ]
  node [
    id 953
    label "wyjedzenie"
  ]
  node [
    id 954
    label "poda&#263;"
  ]
  node [
    id 955
    label "robienie"
  ]
  node [
    id 956
    label "smakowanie"
  ]
  node [
    id 957
    label "zatruwanie_si&#281;"
  ]
  node [
    id 958
    label "wytw&#243;r"
  ]
  node [
    id 959
    label "production"
  ]
  node [
    id 960
    label "odrobina"
  ]
  node [
    id 961
    label "zalewnia"
  ]
  node [
    id 962
    label "ziarko"
  ]
  node [
    id 963
    label "fotografia"
  ]
  node [
    id 964
    label "faktura"
  ]
  node [
    id 965
    label "nasiono"
  ]
  node [
    id 966
    label "obraz"
  ]
  node [
    id 967
    label "k&#322;os"
  ]
  node [
    id 968
    label "bry&#322;ka"
  ]
  node [
    id 969
    label "nie&#322;upka"
  ]
  node [
    id 970
    label "dekortykacja"
  ]
  node [
    id 971
    label "grain"
  ]
  node [
    id 972
    label "&#380;o&#322;d"
  ]
  node [
    id 973
    label "ilo&#347;&#263;"
  ]
  node [
    id 974
    label "zas&#243;b"
  ]
  node [
    id 975
    label "zaparzacz"
  ]
  node [
    id 976
    label "lejek"
  ]
  node [
    id 977
    label "dzbanek"
  ]
  node [
    id 978
    label "ekspres_do_kawy"
  ]
  node [
    id 979
    label "caffeine"
  ]
  node [
    id 980
    label "stymulant"
  ]
  node [
    id 981
    label "anksjogenik"
  ]
  node [
    id 982
    label "pestka"
  ]
  node [
    id 983
    label "goryczkowce"
  ]
  node [
    id 984
    label "Rubiaceae"
  ]
  node [
    id 985
    label "ci&#261;gle"
  ]
  node [
    id 986
    label "nieprzerwanie"
  ]
  node [
    id 987
    label "ci&#261;g&#322;y"
  ]
  node [
    id 988
    label "stale"
  ]
  node [
    id 989
    label "participate"
  ]
  node [
    id 990
    label "adhere"
  ]
  node [
    id 991
    label "pozostawa&#263;"
  ]
  node [
    id 992
    label "zostawa&#263;"
  ]
  node [
    id 993
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 994
    label "istnie&#263;"
  ]
  node [
    id 995
    label "compass"
  ]
  node [
    id 996
    label "get"
  ]
  node [
    id 997
    label "u&#380;ywa&#263;"
  ]
  node [
    id 998
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 999
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1000
    label "korzysta&#263;"
  ]
  node [
    id 1001
    label "appreciation"
  ]
  node [
    id 1002
    label "mierzy&#263;"
  ]
  node [
    id 1003
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1004
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1005
    label "being"
  ]
  node [
    id 1006
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1007
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1008
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1009
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1010
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1011
    label "str&#243;j"
  ]
  node [
    id 1012
    label "para"
  ]
  node [
    id 1013
    label "krok"
  ]
  node [
    id 1014
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1015
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1016
    label "przebiega&#263;"
  ]
  node [
    id 1017
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1018
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1019
    label "continue"
  ]
  node [
    id 1020
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1021
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1022
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1023
    label "bangla&#263;"
  ]
  node [
    id 1024
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1025
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1026
    label "bywa&#263;"
  ]
  node [
    id 1027
    label "dziama&#263;"
  ]
  node [
    id 1028
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1029
    label "Arakan"
  ]
  node [
    id 1030
    label "Teksas"
  ]
  node [
    id 1031
    label "Georgia"
  ]
  node [
    id 1032
    label "Maryland"
  ]
  node [
    id 1033
    label "Michigan"
  ]
  node [
    id 1034
    label "Massachusetts"
  ]
  node [
    id 1035
    label "Luizjana"
  ]
  node [
    id 1036
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1037
    label "samopoczucie"
  ]
  node [
    id 1038
    label "Floryda"
  ]
  node [
    id 1039
    label "Ohio"
  ]
  node [
    id 1040
    label "Alaska"
  ]
  node [
    id 1041
    label "Nowy_Meksyk"
  ]
  node [
    id 1042
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1043
    label "wci&#281;cie"
  ]
  node [
    id 1044
    label "Kansas"
  ]
  node [
    id 1045
    label "Alabama"
  ]
  node [
    id 1046
    label "Kalifornia"
  ]
  node [
    id 1047
    label "Wirginia"
  ]
  node [
    id 1048
    label "punkt"
  ]
  node [
    id 1049
    label "Nowy_York"
  ]
  node [
    id 1050
    label "Waszyngton"
  ]
  node [
    id 1051
    label "Pensylwania"
  ]
  node [
    id 1052
    label "wektor"
  ]
  node [
    id 1053
    label "Hawaje"
  ]
  node [
    id 1054
    label "poziom"
  ]
  node [
    id 1055
    label "jednostka_administracyjna"
  ]
  node [
    id 1056
    label "Illinois"
  ]
  node [
    id 1057
    label "Oklahoma"
  ]
  node [
    id 1058
    label "Oregon"
  ]
  node [
    id 1059
    label "Arizona"
  ]
  node [
    id 1060
    label "Jukatan"
  ]
  node [
    id 1061
    label "shape"
  ]
  node [
    id 1062
    label "Goa"
  ]
  node [
    id 1063
    label "nietrze&#378;wy"
  ]
  node [
    id 1064
    label "gotowo"
  ]
  node [
    id 1065
    label "przygotowywanie"
  ]
  node [
    id 1066
    label "dyspozycyjny"
  ]
  node [
    id 1067
    label "przygotowanie"
  ]
  node [
    id 1068
    label "bliski"
  ]
  node [
    id 1069
    label "martwy"
  ]
  node [
    id 1070
    label "zalany"
  ]
  node [
    id 1071
    label "doj&#347;cie"
  ]
  node [
    id 1072
    label "nieuchronny"
  ]
  node [
    id 1073
    label "m&#243;c"
  ]
  node [
    id 1074
    label "czekanie"
  ]
  node [
    id 1075
    label "umieranie"
  ]
  node [
    id 1076
    label "niesprawny"
  ]
  node [
    id 1077
    label "obumarcie"
  ]
  node [
    id 1078
    label "nieumar&#322;y"
  ]
  node [
    id 1079
    label "umarcie"
  ]
  node [
    id 1080
    label "zw&#322;oki"
  ]
  node [
    id 1081
    label "bezmy&#347;lny"
  ]
  node [
    id 1082
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 1083
    label "umarlak"
  ]
  node [
    id 1084
    label "wiszenie"
  ]
  node [
    id 1085
    label "nieaktualny"
  ]
  node [
    id 1086
    label "martwo"
  ]
  node [
    id 1087
    label "duch"
  ]
  node [
    id 1088
    label "obumieranie"
  ]
  node [
    id 1089
    label "chowanie"
  ]
  node [
    id 1090
    label "najebany"
  ]
  node [
    id 1091
    label "pijany"
  ]
  node [
    id 1092
    label "nieuniknienie"
  ]
  node [
    id 1093
    label "uprawi&#263;"
  ]
  node [
    id 1094
    label "might"
  ]
  node [
    id 1095
    label "spowodowanie"
  ]
  node [
    id 1096
    label "przesy&#322;ka"
  ]
  node [
    id 1097
    label "dolecenie"
  ]
  node [
    id 1098
    label "przy&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1099
    label "uro&#347;ni&#281;cie"
  ]
  node [
    id 1100
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 1101
    label "avenue"
  ]
  node [
    id 1102
    label "dochrapanie_si&#281;"
  ]
  node [
    id 1103
    label "dochodzenie"
  ]
  node [
    id 1104
    label "dor&#281;czenie"
  ]
  node [
    id 1105
    label "rozwini&#281;cie_si&#281;"
  ]
  node [
    id 1106
    label "dop&#322;ata"
  ]
  node [
    id 1107
    label "dosi&#281;gni&#281;cie"
  ]
  node [
    id 1108
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 1109
    label "dojrza&#322;y"
  ]
  node [
    id 1110
    label "dodatek"
  ]
  node [
    id 1111
    label "stanie_si&#281;"
  ]
  node [
    id 1112
    label "orgazm"
  ]
  node [
    id 1113
    label "dojechanie"
  ]
  node [
    id 1114
    label "skill"
  ]
  node [
    id 1115
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 1116
    label "odbycie_stosunku_p&#322;ciowego"
  ]
  node [
    id 1117
    label "znajomo&#347;ci"
  ]
  node [
    id 1118
    label "strzelenie"
  ]
  node [
    id 1119
    label "informacja"
  ]
  node [
    id 1120
    label "ingress"
  ]
  node [
    id 1121
    label "powi&#261;zanie"
  ]
  node [
    id 1122
    label "dop&#322;yni&#281;cie"
  ]
  node [
    id 1123
    label "uzyskanie"
  ]
  node [
    id 1124
    label "affiliation"
  ]
  node [
    id 1125
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 1126
    label "entrance"
  ]
  node [
    id 1127
    label "doznanie"
  ]
  node [
    id 1128
    label "bodziec"
  ]
  node [
    id 1129
    label "dost&#281;p"
  ]
  node [
    id 1130
    label "postrzeganie"
  ]
  node [
    id 1131
    label "rozpowszechnienie"
  ]
  node [
    id 1132
    label "zrobienie"
  ]
  node [
    id 1133
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1134
    label "orzekni&#281;cie"
  ]
  node [
    id 1135
    label "trwanie"
  ]
  node [
    id 1136
    label "odczekiwanie"
  ]
  node [
    id 1137
    label "bycie"
  ]
  node [
    id 1138
    label "kiblowanie"
  ]
  node [
    id 1139
    label "spodziewanie_si&#281;"
  ]
  node [
    id 1140
    label "delay"
  ]
  node [
    id 1141
    label "anticipation"
  ]
  node [
    id 1142
    label "przeczekiwanie"
  ]
  node [
    id 1143
    label "spotykanie"
  ]
  node [
    id 1144
    label "przeczekanie"
  ]
  node [
    id 1145
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 1146
    label "naczekanie_si&#281;"
  ]
  node [
    id 1147
    label "odczekanie"
  ]
  node [
    id 1148
    label "wait"
  ]
  node [
    id 1149
    label "poczekanie"
  ]
  node [
    id 1150
    label "zorganizowanie"
  ]
  node [
    id 1151
    label "przekwalifikowanie"
  ]
  node [
    id 1152
    label "zaplanowanie"
  ]
  node [
    id 1153
    label "preparation"
  ]
  node [
    id 1154
    label "wykonanie"
  ]
  node [
    id 1155
    label "nastawienie"
  ]
  node [
    id 1156
    label "nauczenie"
  ]
  node [
    id 1157
    label "wst&#281;p"
  ]
  node [
    id 1158
    label "kszta&#322;cenie"
  ]
  node [
    id 1159
    label "homework"
  ]
  node [
    id 1160
    label "cooking"
  ]
  node [
    id 1161
    label "sposobienie"
  ]
  node [
    id 1162
    label "usposabianie"
  ]
  node [
    id 1163
    label "organizowanie"
  ]
  node [
    id 1164
    label "wykonywanie"
  ]
  node [
    id 1165
    label "przysz&#322;y"
  ]
  node [
    id 1166
    label "ma&#322;y"
  ]
  node [
    id 1167
    label "zwi&#261;zany"
  ]
  node [
    id 1168
    label "przesz&#322;y"
  ]
  node [
    id 1169
    label "nieodleg&#322;y"
  ]
  node [
    id 1170
    label "oddalony"
  ]
  node [
    id 1171
    label "znajomy"
  ]
  node [
    id 1172
    label "blisko"
  ]
  node [
    id 1173
    label "zbli&#380;enie"
  ]
  node [
    id 1174
    label "kr&#243;tki"
  ]
  node [
    id 1175
    label "dyspozycyjnie"
  ]
  node [
    id 1176
    label "jako&#347;"
  ]
  node [
    id 1177
    label "charakterystyczny"
  ]
  node [
    id 1178
    label "jako_tako"
  ]
  node [
    id 1179
    label "dziwny"
  ]
  node [
    id 1180
    label "przyzwoity"
  ]
  node [
    id 1181
    label "wiadomy"
  ]
  node [
    id 1182
    label "wyrzekni&#281;cie_si&#281;"
  ]
  node [
    id 1183
    label "przeznaczenie"
  ]
  node [
    id 1184
    label "nadanie"
  ]
  node [
    id 1185
    label "Oblation"
  ]
  node [
    id 1186
    label "pit"
  ]
  node [
    id 1187
    label "bearing"
  ]
  node [
    id 1188
    label "powaga"
  ]
  node [
    id 1189
    label "gotowanie_si&#281;"
  ]
  node [
    id 1190
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1191
    label "ponastawianie"
  ]
  node [
    id 1192
    label "umieszczenie"
  ]
  node [
    id 1193
    label "oddzia&#322;anie"
  ]
  node [
    id 1194
    label "ustawienie"
  ]
  node [
    id 1195
    label "ukierunkowanie"
  ]
  node [
    id 1196
    label "podej&#347;cie"
  ]
  node [
    id 1197
    label "z&#322;amanie"
  ]
  node [
    id 1198
    label "z&#322;o&#380;enie"
  ]
  node [
    id 1199
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 1200
    label "narobienie"
  ]
  node [
    id 1201
    label "porobienie"
  ]
  node [
    id 1202
    label "creation"
  ]
  node [
    id 1203
    label "przyznanie"
  ]
  node [
    id 1204
    label "akt"
  ]
  node [
    id 1205
    label "broadcast"
  ]
  node [
    id 1206
    label "nazwanie"
  ]
  node [
    id 1207
    label "przes&#322;anie"
  ]
  node [
    id 1208
    label "denomination"
  ]
  node [
    id 1209
    label "destiny"
  ]
  node [
    id 1210
    label "przymus"
  ]
  node [
    id 1211
    label "wybranie"
  ]
  node [
    id 1212
    label "rzuci&#263;"
  ]
  node [
    id 1213
    label "si&#322;a"
  ]
  node [
    id 1214
    label "przydzielenie"
  ]
  node [
    id 1215
    label "oblat"
  ]
  node [
    id 1216
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 1217
    label "obowi&#261;zek"
  ]
  node [
    id 1218
    label "rzucenie"
  ]
  node [
    id 1219
    label "ameryka&#324;ski_pitbulterier"
  ]
  node [
    id 1220
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 1221
    label "facylitacja"
  ]
  node [
    id 1222
    label "asymilowa&#263;"
  ]
  node [
    id 1223
    label "nasada"
  ]
  node [
    id 1224
    label "profanum"
  ]
  node [
    id 1225
    label "wz&#243;r"
  ]
  node [
    id 1226
    label "senior"
  ]
  node [
    id 1227
    label "asymilowanie"
  ]
  node [
    id 1228
    label "os&#322;abia&#263;"
  ]
  node [
    id 1229
    label "homo_sapiens"
  ]
  node [
    id 1230
    label "osoba"
  ]
  node [
    id 1231
    label "Adam"
  ]
  node [
    id 1232
    label "hominid"
  ]
  node [
    id 1233
    label "portrecista"
  ]
  node [
    id 1234
    label "polifag"
  ]
  node [
    id 1235
    label "podw&#322;adny"
  ]
  node [
    id 1236
    label "dwun&#243;g"
  ]
  node [
    id 1237
    label "wapniak"
  ]
  node [
    id 1238
    label "os&#322;abianie"
  ]
  node [
    id 1239
    label "antropochoria"
  ]
  node [
    id 1240
    label "figura"
  ]
  node [
    id 1241
    label "g&#322;owa"
  ]
  node [
    id 1242
    label "mikrokosmos"
  ]
  node [
    id 1243
    label "oddzia&#322;ywanie"
  ]
  node [
    id 1244
    label "zg&#322;asza&#263;"
  ]
  node [
    id 1245
    label "ukazywa&#263;"
  ]
  node [
    id 1246
    label "przedstawienie"
  ]
  node [
    id 1247
    label "exhibit"
  ]
  node [
    id 1248
    label "stanowi&#263;"
  ]
  node [
    id 1249
    label "demonstrowa&#263;"
  ]
  node [
    id 1250
    label "display"
  ]
  node [
    id 1251
    label "typify"
  ]
  node [
    id 1252
    label "attest"
  ]
  node [
    id 1253
    label "pokazywa&#263;"
  ]
  node [
    id 1254
    label "teatr"
  ]
  node [
    id 1255
    label "opisywa&#263;"
  ]
  node [
    id 1256
    label "zapoznawa&#263;"
  ]
  node [
    id 1257
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 1258
    label "informowa&#263;"
  ]
  node [
    id 1259
    label "wyraz"
  ]
  node [
    id 1260
    label "bespeak"
  ]
  node [
    id 1261
    label "introduce"
  ]
  node [
    id 1262
    label "przeszkala&#263;"
  ]
  node [
    id 1263
    label "warto&#347;&#263;"
  ]
  node [
    id 1264
    label "wyra&#380;a&#263;"
  ]
  node [
    id 1265
    label "indicate"
  ]
  node [
    id 1266
    label "write"
  ]
  node [
    id 1267
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 1268
    label "pies_my&#347;liwski"
  ]
  node [
    id 1269
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1270
    label "decydowa&#263;"
  ]
  node [
    id 1271
    label "zatrzymywa&#263;"
  ]
  node [
    id 1272
    label "siatk&#243;wka"
  ]
  node [
    id 1273
    label "kelner"
  ]
  node [
    id 1274
    label "tenis"
  ]
  node [
    id 1275
    label "faszerowa&#263;"
  ]
  node [
    id 1276
    label "serwowa&#263;"
  ]
  node [
    id 1277
    label "stawia&#263;"
  ]
  node [
    id 1278
    label "rozgrywa&#263;"
  ]
  node [
    id 1279
    label "deal"
  ]
  node [
    id 1280
    label "obznajamia&#263;"
  ]
  node [
    id 1281
    label "go_steady"
  ]
  node [
    id 1282
    label "zawiera&#263;"
  ]
  node [
    id 1283
    label "poznawa&#263;"
  ]
  node [
    id 1284
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 1285
    label "antyteatr"
  ]
  node [
    id 1286
    label "literatura"
  ]
  node [
    id 1287
    label "sala"
  ]
  node [
    id 1288
    label "gra"
  ]
  node [
    id 1289
    label "budynek"
  ]
  node [
    id 1290
    label "deski"
  ]
  node [
    id 1291
    label "teren"
  ]
  node [
    id 1292
    label "sztuka"
  ]
  node [
    id 1293
    label "play"
  ]
  node [
    id 1294
    label "widzownia"
  ]
  node [
    id 1295
    label "modelatornia"
  ]
  node [
    id 1296
    label "dekoratornia"
  ]
  node [
    id 1297
    label "instytucja"
  ]
  node [
    id 1298
    label "przedstawianie"
  ]
  node [
    id 1299
    label "ods&#322;ona"
  ]
  node [
    id 1300
    label "opisanie"
  ]
  node [
    id 1301
    label "pokazanie"
  ]
  node [
    id 1302
    label "wyst&#261;pienie"
  ]
  node [
    id 1303
    label "ukazanie"
  ]
  node [
    id 1304
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 1305
    label "zapoznanie"
  ]
  node [
    id 1306
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 1307
    label "scena"
  ]
  node [
    id 1308
    label "malarstwo"
  ]
  node [
    id 1309
    label "theatrical_performance"
  ]
  node [
    id 1310
    label "pokaz"
  ]
  node [
    id 1311
    label "pr&#243;bowanie"
  ]
  node [
    id 1312
    label "obgadanie"
  ]
  node [
    id 1313
    label "narration"
  ]
  node [
    id 1314
    label "cyrk"
  ]
  node [
    id 1315
    label "scenografia"
  ]
  node [
    id 1316
    label "realizacja"
  ]
  node [
    id 1317
    label "rola"
  ]
  node [
    id 1318
    label "zademonstrowanie"
  ]
  node [
    id 1319
    label "przedstawi&#263;"
  ]
  node [
    id 1320
    label "przypadkowo"
  ]
  node [
    id 1321
    label "nieuzasadniony"
  ]
  node [
    id 1322
    label "rzutem_na_ta&#347;m&#281;"
  ]
  node [
    id 1323
    label "Saint"
  ]
  node [
    id 1324
    label "Johns"
  ]
  node [
    id 1325
    label "hala"
  ]
  node [
    id 1326
    label "stulecie"
  ]
  node [
    id 1327
    label "Second"
  ]
  node [
    id 1328
    label "Life"
  ]
  node [
    id 1329
    label "Attribution"
  ]
  node [
    id 1330
    label "ShareAlike"
  ]
  node [
    id 1331
    label "LowWatt"
  ]
  node [
    id 1332
    label "Creative"
  ]
  node [
    id 1333
    label "Commons"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 215
  ]
  edge [
    source 4
    target 216
  ]
  edge [
    source 4
    target 217
  ]
  edge [
    source 4
    target 218
  ]
  edge [
    source 4
    target 219
  ]
  edge [
    source 4
    target 220
  ]
  edge [
    source 4
    target 221
  ]
  edge [
    source 4
    target 222
  ]
  edge [
    source 4
    target 223
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 225
  ]
  edge [
    source 4
    target 226
  ]
  edge [
    source 4
    target 227
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 279
  ]
  edge [
    source 9
    target 280
  ]
  edge [
    source 9
    target 281
  ]
  edge [
    source 9
    target 282
  ]
  edge [
    source 9
    target 14
  ]
  edge [
    source 9
    target 283
  ]
  edge [
    source 9
    target 284
  ]
  edge [
    source 9
    target 285
  ]
  edge [
    source 9
    target 286
  ]
  edge [
    source 9
    target 287
  ]
  edge [
    source 9
    target 288
  ]
  edge [
    source 9
    target 289
  ]
  edge [
    source 9
    target 290
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 292
  ]
  edge [
    source 9
    target 271
  ]
  edge [
    source 9
    target 293
  ]
  edge [
    source 9
    target 294
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 296
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 298
  ]
  edge [
    source 9
    target 299
  ]
  edge [
    source 9
    target 300
  ]
  edge [
    source 9
    target 301
  ]
  edge [
    source 9
    target 302
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 9
    target 304
  ]
  edge [
    source 9
    target 305
  ]
  edge [
    source 9
    target 272
  ]
  edge [
    source 9
    target 306
  ]
  edge [
    source 9
    target 307
  ]
  edge [
    source 9
    target 308
  ]
  edge [
    source 9
    target 309
  ]
  edge [
    source 9
    target 310
  ]
  edge [
    source 9
    target 311
  ]
  edge [
    source 9
    target 312
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 10
    target 317
  ]
  edge [
    source 10
    target 318
  ]
  edge [
    source 10
    target 319
  ]
  edge [
    source 10
    target 320
  ]
  edge [
    source 10
    target 321
  ]
  edge [
    source 10
    target 275
  ]
  edge [
    source 10
    target 322
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 324
  ]
  edge [
    source 10
    target 325
  ]
  edge [
    source 10
    target 326
  ]
  edge [
    source 10
    target 327
  ]
  edge [
    source 10
    target 328
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 352
  ]
  edge [
    source 12
    target 353
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 354
  ]
  edge [
    source 12
    target 355
  ]
  edge [
    source 12
    target 356
  ]
  edge [
    source 12
    target 357
  ]
  edge [
    source 12
    target 358
  ]
  edge [
    source 12
    target 359
  ]
  edge [
    source 12
    target 360
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 284
  ]
  edge [
    source 14
    target 285
  ]
  edge [
    source 14
    target 286
  ]
  edge [
    source 14
    target 287
  ]
  edge [
    source 14
    target 361
  ]
  edge [
    source 14
    target 229
  ]
  edge [
    source 14
    target 362
  ]
  edge [
    source 14
    target 363
  ]
  edge [
    source 14
    target 364
  ]
  edge [
    source 14
    target 365
  ]
  edge [
    source 14
    target 366
  ]
  edge [
    source 14
    target 367
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 368
  ]
  edge [
    source 15
    target 369
  ]
  edge [
    source 15
    target 370
  ]
  edge [
    source 15
    target 371
  ]
  edge [
    source 15
    target 372
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 379
  ]
  edge [
    source 17
    target 380
  ]
  edge [
    source 17
    target 381
  ]
  edge [
    source 17
    target 382
  ]
  edge [
    source 17
    target 346
  ]
  edge [
    source 17
    target 383
  ]
  edge [
    source 17
    target 384
  ]
  edge [
    source 17
    target 294
  ]
  edge [
    source 17
    target 385
  ]
  edge [
    source 17
    target 386
  ]
  edge [
    source 17
    target 387
  ]
  edge [
    source 17
    target 388
  ]
  edge [
    source 17
    target 389
  ]
  edge [
    source 17
    target 390
  ]
  edge [
    source 17
    target 391
  ]
  edge [
    source 17
    target 392
  ]
  edge [
    source 17
    target 393
  ]
  edge [
    source 17
    target 394
  ]
  edge [
    source 17
    target 395
  ]
  edge [
    source 17
    target 396
  ]
  edge [
    source 17
    target 397
  ]
  edge [
    source 17
    target 398
  ]
  edge [
    source 17
    target 399
  ]
  edge [
    source 17
    target 400
  ]
  edge [
    source 17
    target 401
  ]
  edge [
    source 17
    target 402
  ]
  edge [
    source 17
    target 403
  ]
  edge [
    source 17
    target 404
  ]
  edge [
    source 17
    target 405
  ]
  edge [
    source 17
    target 406
  ]
  edge [
    source 17
    target 407
  ]
  edge [
    source 17
    target 408
  ]
  edge [
    source 17
    target 332
  ]
  edge [
    source 17
    target 409
  ]
  edge [
    source 17
    target 410
  ]
  edge [
    source 17
    target 411
  ]
  edge [
    source 17
    target 412
  ]
  edge [
    source 17
    target 413
  ]
  edge [
    source 17
    target 414
  ]
  edge [
    source 17
    target 415
  ]
  edge [
    source 17
    target 416
  ]
  edge [
    source 17
    target 417
  ]
  edge [
    source 17
    target 418
  ]
  edge [
    source 17
    target 419
  ]
  edge [
    source 17
    target 420
  ]
  edge [
    source 17
    target 421
  ]
  edge [
    source 17
    target 422
  ]
  edge [
    source 17
    target 423
  ]
  edge [
    source 17
    target 424
  ]
  edge [
    source 17
    target 425
  ]
  edge [
    source 17
    target 426
  ]
  edge [
    source 17
    target 427
  ]
  edge [
    source 17
    target 428
  ]
  edge [
    source 17
    target 429
  ]
  edge [
    source 17
    target 430
  ]
  edge [
    source 17
    target 431
  ]
  edge [
    source 17
    target 432
  ]
  edge [
    source 17
    target 433
  ]
  edge [
    source 17
    target 434
  ]
  edge [
    source 17
    target 435
  ]
  edge [
    source 17
    target 436
  ]
  edge [
    source 17
    target 437
  ]
  edge [
    source 17
    target 438
  ]
  edge [
    source 17
    target 439
  ]
  edge [
    source 17
    target 440
  ]
  edge [
    source 17
    target 441
  ]
  edge [
    source 17
    target 442
  ]
  edge [
    source 17
    target 443
  ]
  edge [
    source 17
    target 444
  ]
  edge [
    source 17
    target 445
  ]
  edge [
    source 17
    target 446
  ]
  edge [
    source 17
    target 447
  ]
  edge [
    source 17
    target 448
  ]
  edge [
    source 17
    target 449
  ]
  edge [
    source 17
    target 450
  ]
  edge [
    source 17
    target 451
  ]
  edge [
    source 17
    target 452
  ]
  edge [
    source 17
    target 453
  ]
  edge [
    source 17
    target 454
  ]
  edge [
    source 17
    target 455
  ]
  edge [
    source 17
    target 456
  ]
  edge [
    source 17
    target 457
  ]
  edge [
    source 17
    target 458
  ]
  edge [
    source 17
    target 459
  ]
  edge [
    source 17
    target 460
  ]
  edge [
    source 17
    target 461
  ]
  edge [
    source 17
    target 462
  ]
  edge [
    source 17
    target 463
  ]
  edge [
    source 17
    target 464
  ]
  edge [
    source 17
    target 465
  ]
  edge [
    source 17
    target 466
  ]
  edge [
    source 17
    target 467
  ]
  edge [
    source 17
    target 468
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 469
  ]
  edge [
    source 19
    target 470
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 29
  ]
  edge [
    source 20
    target 471
  ]
  edge [
    source 20
    target 472
  ]
  edge [
    source 20
    target 473
  ]
  edge [
    source 20
    target 474
  ]
  edge [
    source 20
    target 475
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 476
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 477
  ]
  edge [
    source 20
    target 478
  ]
  edge [
    source 20
    target 479
  ]
  edge [
    source 20
    target 480
  ]
  edge [
    source 20
    target 481
  ]
  edge [
    source 20
    target 482
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 483
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 484
  ]
  edge [
    source 20
    target 485
  ]
  edge [
    source 20
    target 486
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 20
    target 207
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 208
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 26
  ]
  edge [
    source 21
    target 41
  ]
  edge [
    source 21
    target 487
  ]
  edge [
    source 21
    target 488
  ]
  edge [
    source 21
    target 489
  ]
  edge [
    source 21
    target 490
  ]
  edge [
    source 21
    target 491
  ]
  edge [
    source 21
    target 492
  ]
  edge [
    source 21
    target 493
  ]
  edge [
    source 21
    target 494
  ]
  edge [
    source 21
    target 495
  ]
  edge [
    source 21
    target 496
  ]
  edge [
    source 21
    target 497
  ]
  edge [
    source 21
    target 361
  ]
  edge [
    source 21
    target 498
  ]
  edge [
    source 21
    target 499
  ]
  edge [
    source 21
    target 500
  ]
  edge [
    source 21
    target 501
  ]
  edge [
    source 21
    target 502
  ]
  edge [
    source 21
    target 503
  ]
  edge [
    source 21
    target 504
  ]
  edge [
    source 21
    target 505
  ]
  edge [
    source 21
    target 506
  ]
  edge [
    source 21
    target 507
  ]
  edge [
    source 21
    target 508
  ]
  edge [
    source 21
    target 509
  ]
  edge [
    source 21
    target 510
  ]
  edge [
    source 21
    target 511
  ]
  edge [
    source 21
    target 512
  ]
  edge [
    source 21
    target 513
  ]
  edge [
    source 21
    target 514
  ]
  edge [
    source 21
    target 515
  ]
  edge [
    source 21
    target 516
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 517
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 518
  ]
  edge [
    source 25
    target 519
  ]
  edge [
    source 25
    target 520
  ]
  edge [
    source 25
    target 521
  ]
  edge [
    source 25
    target 522
  ]
  edge [
    source 25
    target 523
  ]
  edge [
    source 25
    target 524
  ]
  edge [
    source 25
    target 525
  ]
  edge [
    source 25
    target 356
  ]
  edge [
    source 25
    target 526
  ]
  edge [
    source 25
    target 527
  ]
  edge [
    source 25
    target 528
  ]
  edge [
    source 25
    target 529
  ]
  edge [
    source 25
    target 530
  ]
  edge [
    source 25
    target 531
  ]
  edge [
    source 25
    target 532
  ]
  edge [
    source 25
    target 533
  ]
  edge [
    source 25
    target 534
  ]
  edge [
    source 25
    target 535
  ]
  edge [
    source 25
    target 536
  ]
  edge [
    source 25
    target 537
  ]
  edge [
    source 25
    target 538
  ]
  edge [
    source 25
    target 539
  ]
  edge [
    source 25
    target 540
  ]
  edge [
    source 25
    target 541
  ]
  edge [
    source 25
    target 542
  ]
  edge [
    source 25
    target 543
  ]
  edge [
    source 25
    target 544
  ]
  edge [
    source 25
    target 545
  ]
  edge [
    source 25
    target 546
  ]
  edge [
    source 25
    target 547
  ]
  edge [
    source 25
    target 548
  ]
  edge [
    source 25
    target 549
  ]
  edge [
    source 25
    target 550
  ]
  edge [
    source 25
    target 551
  ]
  edge [
    source 25
    target 552
  ]
  edge [
    source 25
    target 553
  ]
  edge [
    source 25
    target 554
  ]
  edge [
    source 25
    target 555
  ]
  edge [
    source 25
    target 556
  ]
  edge [
    source 25
    target 557
  ]
  edge [
    source 25
    target 558
  ]
  edge [
    source 25
    target 559
  ]
  edge [
    source 25
    target 560
  ]
  edge [
    source 25
    target 561
  ]
  edge [
    source 25
    target 562
  ]
  edge [
    source 25
    target 563
  ]
  edge [
    source 25
    target 564
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 565
  ]
  edge [
    source 26
    target 566
  ]
  edge [
    source 26
    target 567
  ]
  edge [
    source 26
    target 568
  ]
  edge [
    source 26
    target 297
  ]
  edge [
    source 26
    target 332
  ]
  edge [
    source 26
    target 334
  ]
  edge [
    source 26
    target 569
  ]
  edge [
    source 26
    target 335
  ]
  edge [
    source 26
    target 336
  ]
  edge [
    source 26
    target 329
  ]
  edge [
    source 26
    target 570
  ]
  edge [
    source 26
    target 571
  ]
  edge [
    source 26
    target 572
  ]
  edge [
    source 26
    target 573
  ]
  edge [
    source 26
    target 343
  ]
  edge [
    source 26
    target 344
  ]
  edge [
    source 26
    target 345
  ]
  edge [
    source 26
    target 346
  ]
  edge [
    source 26
    target 347
  ]
  edge [
    source 26
    target 348
  ]
  edge [
    source 26
    target 574
  ]
  edge [
    source 26
    target 88
  ]
  edge [
    source 26
    target 361
  ]
  edge [
    source 26
    target 575
  ]
  edge [
    source 26
    target 576
  ]
  edge [
    source 26
    target 577
  ]
  edge [
    source 26
    target 578
  ]
  edge [
    source 26
    target 579
  ]
  edge [
    source 26
    target 580
  ]
  edge [
    source 26
    target 581
  ]
  edge [
    source 26
    target 582
  ]
  edge [
    source 26
    target 583
  ]
  edge [
    source 26
    target 584
  ]
  edge [
    source 26
    target 585
  ]
  edge [
    source 26
    target 586
  ]
  edge [
    source 26
    target 587
  ]
  edge [
    source 26
    target 588
  ]
  edge [
    source 26
    target 589
  ]
  edge [
    source 26
    target 590
  ]
  edge [
    source 26
    target 591
  ]
  edge [
    source 26
    target 592
  ]
  edge [
    source 26
    target 593
  ]
  edge [
    source 26
    target 594
  ]
  edge [
    source 26
    target 226
  ]
  edge [
    source 26
    target 595
  ]
  edge [
    source 26
    target 596
  ]
  edge [
    source 26
    target 597
  ]
  edge [
    source 26
    target 598
  ]
  edge [
    source 26
    target 599
  ]
  edge [
    source 26
    target 600
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 601
  ]
  edge [
    source 27
    target 602
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 205
  ]
  edge [
    source 29
    target 206
  ]
  edge [
    source 29
    target 207
  ]
  edge [
    source 29
    target 182
  ]
  edge [
    source 29
    target 208
  ]
  edge [
    source 29
    target 209
  ]
  edge [
    source 29
    target 210
  ]
  edge [
    source 29
    target 211
  ]
  edge [
    source 29
    target 212
  ]
  edge [
    source 29
    target 213
  ]
  edge [
    source 29
    target 214
  ]
  edge [
    source 29
    target 215
  ]
  edge [
    source 29
    target 216
  ]
  edge [
    source 29
    target 217
  ]
  edge [
    source 29
    target 603
  ]
  edge [
    source 29
    target 227
  ]
  edge [
    source 29
    target 604
  ]
  edge [
    source 29
    target 594
  ]
  edge [
    source 29
    target 605
  ]
  edge [
    source 29
    target 606
  ]
  edge [
    source 29
    target 607
  ]
  edge [
    source 29
    target 608
  ]
  edge [
    source 29
    target 609
  ]
  edge [
    source 29
    target 610
  ]
  edge [
    source 29
    target 611
  ]
  edge [
    source 29
    target 612
  ]
  edge [
    source 29
    target 254
  ]
  edge [
    source 29
    target 613
  ]
  edge [
    source 29
    target 162
  ]
  edge [
    source 29
    target 164
  ]
  edge [
    source 29
    target 614
  ]
  edge [
    source 29
    target 615
  ]
  edge [
    source 29
    target 616
  ]
  edge [
    source 29
    target 617
  ]
  edge [
    source 29
    target 475
  ]
  edge [
    source 29
    target 618
  ]
  edge [
    source 29
    target 619
  ]
  edge [
    source 29
    target 168
  ]
  edge [
    source 29
    target 620
  ]
  edge [
    source 29
    target 621
  ]
  edge [
    source 29
    target 481
  ]
  edge [
    source 29
    target 622
  ]
  edge [
    source 29
    target 623
  ]
  edge [
    source 29
    target 624
  ]
  edge [
    source 29
    target 625
  ]
  edge [
    source 29
    target 626
  ]
  edge [
    source 29
    target 627
  ]
  edge [
    source 29
    target 628
  ]
  edge [
    source 29
    target 629
  ]
  edge [
    source 29
    target 630
  ]
  edge [
    source 29
    target 631
  ]
  edge [
    source 29
    target 632
  ]
  edge [
    source 29
    target 633
  ]
  edge [
    source 29
    target 634
  ]
  edge [
    source 29
    target 635
  ]
  edge [
    source 29
    target 636
  ]
  edge [
    source 29
    target 637
  ]
  edge [
    source 29
    target 638
  ]
  edge [
    source 29
    target 639
  ]
  edge [
    source 29
    target 640
  ]
  edge [
    source 29
    target 154
  ]
  edge [
    source 29
    target 641
  ]
  edge [
    source 29
    target 230
  ]
  edge [
    source 29
    target 642
  ]
  edge [
    source 29
    target 643
  ]
  edge [
    source 29
    target 190
  ]
  edge [
    source 29
    target 644
  ]
  edge [
    source 29
    target 645
  ]
  edge [
    source 29
    target 646
  ]
  edge [
    source 29
    target 647
  ]
  edge [
    source 29
    target 648
  ]
  edge [
    source 29
    target 649
  ]
  edge [
    source 29
    target 650
  ]
  edge [
    source 29
    target 651
  ]
  edge [
    source 29
    target 652
  ]
  edge [
    source 29
    target 653
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 654
  ]
  edge [
    source 30
    target 655
  ]
  edge [
    source 30
    target 479
  ]
  edge [
    source 30
    target 656
  ]
  edge [
    source 30
    target 657
  ]
  edge [
    source 30
    target 658
  ]
  edge [
    source 30
    target 659
  ]
  edge [
    source 30
    target 660
  ]
  edge [
    source 30
    target 277
  ]
  edge [
    source 30
    target 274
  ]
  edge [
    source 30
    target 661
  ]
  edge [
    source 30
    target 662
  ]
  edge [
    source 30
    target 663
  ]
  edge [
    source 30
    target 664
  ]
  edge [
    source 30
    target 665
  ]
  edge [
    source 30
    target 666
  ]
  edge [
    source 30
    target 667
  ]
  edge [
    source 30
    target 276
  ]
  edge [
    source 30
    target 668
  ]
  edge [
    source 30
    target 669
  ]
  edge [
    source 30
    target 670
  ]
  edge [
    source 30
    target 671
  ]
  edge [
    source 30
    target 672
  ]
  edge [
    source 30
    target 673
  ]
  edge [
    source 30
    target 674
  ]
  edge [
    source 30
    target 675
  ]
  edge [
    source 30
    target 676
  ]
  edge [
    source 30
    target 677
  ]
  edge [
    source 30
    target 678
  ]
  edge [
    source 30
    target 679
  ]
  edge [
    source 30
    target 275
  ]
  edge [
    source 30
    target 680
  ]
  edge [
    source 30
    target 681
  ]
  edge [
    source 30
    target 682
  ]
  edge [
    source 30
    target 683
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 684
  ]
  edge [
    source 30
    target 685
  ]
  edge [
    source 30
    target 686
  ]
  edge [
    source 30
    target 687
  ]
  edge [
    source 30
    target 688
  ]
  edge [
    source 30
    target 689
  ]
  edge [
    source 30
    target 690
  ]
  edge [
    source 30
    target 354
  ]
  edge [
    source 30
    target 691
  ]
  edge [
    source 30
    target 534
  ]
  edge [
    source 30
    target 692
  ]
  edge [
    source 30
    target 693
  ]
  edge [
    source 30
    target 694
  ]
  edge [
    source 30
    target 695
  ]
  edge [
    source 30
    target 696
  ]
  edge [
    source 30
    target 697
  ]
  edge [
    source 30
    target 698
  ]
  edge [
    source 30
    target 699
  ]
  edge [
    source 30
    target 700
  ]
  edge [
    source 30
    target 701
  ]
  edge [
    source 30
    target 702
  ]
  edge [
    source 30
    target 703
  ]
  edge [
    source 30
    target 704
  ]
  edge [
    source 30
    target 705
  ]
  edge [
    source 30
    target 706
  ]
  edge [
    source 30
    target 707
  ]
  edge [
    source 30
    target 708
  ]
  edge [
    source 30
    target 709
  ]
  edge [
    source 30
    target 233
  ]
  edge [
    source 30
    target 710
  ]
  edge [
    source 30
    target 711
  ]
  edge [
    source 30
    target 712
  ]
  edge [
    source 30
    target 713
  ]
  edge [
    source 30
    target 714
  ]
  edge [
    source 30
    target 715
  ]
  edge [
    source 30
    target 716
  ]
  edge [
    source 30
    target 717
  ]
  edge [
    source 30
    target 718
  ]
  edge [
    source 30
    target 719
  ]
  edge [
    source 30
    target 720
  ]
  edge [
    source 30
    target 721
  ]
  edge [
    source 30
    target 722
  ]
  edge [
    source 30
    target 723
  ]
  edge [
    source 30
    target 724
  ]
  edge [
    source 30
    target 725
  ]
  edge [
    source 30
    target 726
  ]
  edge [
    source 30
    target 727
  ]
  edge [
    source 30
    target 728
  ]
  edge [
    source 30
    target 729
  ]
  edge [
    source 30
    target 730
  ]
  edge [
    source 30
    target 731
  ]
  edge [
    source 30
    target 732
  ]
  edge [
    source 30
    target 733
  ]
  edge [
    source 30
    target 734
  ]
  edge [
    source 30
    target 735
  ]
  edge [
    source 30
    target 736
  ]
  edge [
    source 30
    target 737
  ]
  edge [
    source 30
    target 738
  ]
  edge [
    source 30
    target 739
  ]
  edge [
    source 30
    target 740
  ]
  edge [
    source 30
    target 741
  ]
  edge [
    source 30
    target 742
  ]
  edge [
    source 30
    target 743
  ]
  edge [
    source 30
    target 744
  ]
  edge [
    source 30
    target 745
  ]
  edge [
    source 30
    target 746
  ]
  edge [
    source 30
    target 747
  ]
  edge [
    source 30
    target 748
  ]
  edge [
    source 30
    target 749
  ]
  edge [
    source 30
    target 750
  ]
  edge [
    source 30
    target 751
  ]
  edge [
    source 30
    target 752
  ]
  edge [
    source 30
    target 753
  ]
  edge [
    source 30
    target 754
  ]
  edge [
    source 30
    target 755
  ]
  edge [
    source 30
    target 756
  ]
  edge [
    source 30
    target 757
  ]
  edge [
    source 30
    target 758
  ]
  edge [
    source 30
    target 759
  ]
  edge [
    source 30
    target 477
  ]
  edge [
    source 30
    target 760
  ]
  edge [
    source 30
    target 761
  ]
  edge [
    source 30
    target 762
  ]
  edge [
    source 30
    target 763
  ]
  edge [
    source 30
    target 764
  ]
  edge [
    source 30
    target 765
  ]
  edge [
    source 30
    target 766
  ]
  edge [
    source 30
    target 767
  ]
  edge [
    source 30
    target 768
  ]
  edge [
    source 30
    target 769
  ]
  edge [
    source 30
    target 770
  ]
  edge [
    source 30
    target 476
  ]
  edge [
    source 30
    target 190
  ]
  edge [
    source 30
    target 478
  ]
  edge [
    source 30
    target 771
  ]
  edge [
    source 30
    target 480
  ]
  edge [
    source 30
    target 481
  ]
  edge [
    source 30
    target 163
  ]
  edge [
    source 30
    target 772
  ]
  edge [
    source 30
    target 773
  ]
  edge [
    source 30
    target 774
  ]
  edge [
    source 30
    target 775
  ]
  edge [
    source 30
    target 776
  ]
  edge [
    source 30
    target 777
  ]
  edge [
    source 30
    target 778
  ]
  edge [
    source 30
    target 779
  ]
  edge [
    source 30
    target 780
  ]
  edge [
    source 30
    target 781
  ]
  edge [
    source 30
    target 782
  ]
  edge [
    source 30
    target 783
  ]
  edge [
    source 30
    target 784
  ]
  edge [
    source 30
    target 785
  ]
  edge [
    source 30
    target 786
  ]
  edge [
    source 30
    target 787
  ]
  edge [
    source 30
    target 788
  ]
  edge [
    source 30
    target 789
  ]
  edge [
    source 30
    target 790
  ]
  edge [
    source 30
    target 791
  ]
  edge [
    source 30
    target 792
  ]
  edge [
    source 30
    target 793
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 794
  ]
  edge [
    source 31
    target 795
  ]
  edge [
    source 31
    target 796
  ]
  edge [
    source 31
    target 797
  ]
  edge [
    source 31
    target 798
  ]
  edge [
    source 31
    target 799
  ]
  edge [
    source 31
    target 800
  ]
  edge [
    source 31
    target 801
  ]
  edge [
    source 31
    target 802
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 803
  ]
  edge [
    source 31
    target 804
  ]
  edge [
    source 31
    target 805
  ]
  edge [
    source 31
    target 806
  ]
  edge [
    source 31
    target 807
  ]
  edge [
    source 31
    target 808
  ]
  edge [
    source 31
    target 809
  ]
  edge [
    source 31
    target 810
  ]
  edge [
    source 31
    target 811
  ]
  edge [
    source 31
    target 812
  ]
  edge [
    source 31
    target 813
  ]
  edge [
    source 31
    target 814
  ]
  edge [
    source 31
    target 815
  ]
  edge [
    source 31
    target 816
  ]
  edge [
    source 31
    target 817
  ]
  edge [
    source 31
    target 818
  ]
  edge [
    source 31
    target 819
  ]
  edge [
    source 31
    target 820
  ]
  edge [
    source 31
    target 821
  ]
  edge [
    source 31
    target 822
  ]
  edge [
    source 31
    target 823
  ]
  edge [
    source 31
    target 824
  ]
  edge [
    source 31
    target 825
  ]
  edge [
    source 31
    target 826
  ]
  edge [
    source 31
    target 227
  ]
  edge [
    source 31
    target 827
  ]
  edge [
    source 31
    target 828
  ]
  edge [
    source 31
    target 472
  ]
  edge [
    source 31
    target 829
  ]
  edge [
    source 31
    target 830
  ]
  edge [
    source 31
    target 831
  ]
  edge [
    source 31
    target 832
  ]
  edge [
    source 31
    target 833
  ]
  edge [
    source 31
    target 834
  ]
  edge [
    source 31
    target 835
  ]
  edge [
    source 31
    target 836
  ]
  edge [
    source 31
    target 191
  ]
  edge [
    source 31
    target 837
  ]
  edge [
    source 31
    target 838
  ]
  edge [
    source 31
    target 839
  ]
  edge [
    source 31
    target 840
  ]
  edge [
    source 31
    target 841
  ]
  edge [
    source 31
    target 842
  ]
  edge [
    source 31
    target 843
  ]
  edge [
    source 31
    target 844
  ]
  edge [
    source 31
    target 845
  ]
  edge [
    source 31
    target 465
  ]
  edge [
    source 31
    target 846
  ]
  edge [
    source 31
    target 847
  ]
  edge [
    source 31
    target 175
  ]
  edge [
    source 31
    target 848
  ]
  edge [
    source 31
    target 757
  ]
  edge [
    source 31
    target 849
  ]
  edge [
    source 31
    target 850
  ]
  edge [
    source 31
    target 851
  ]
  edge [
    source 31
    target 852
  ]
  edge [
    source 31
    target 853
  ]
  edge [
    source 31
    target 854
  ]
  edge [
    source 31
    target 855
  ]
  edge [
    source 31
    target 856
  ]
  edge [
    source 31
    target 857
  ]
  edge [
    source 31
    target 858
  ]
  edge [
    source 31
    target 859
  ]
  edge [
    source 31
    target 860
  ]
  edge [
    source 31
    target 861
  ]
  edge [
    source 31
    target 862
  ]
  edge [
    source 31
    target 863
  ]
  edge [
    source 31
    target 864
  ]
  edge [
    source 31
    target 865
  ]
  edge [
    source 31
    target 866
  ]
  edge [
    source 31
    target 867
  ]
  edge [
    source 31
    target 868
  ]
  edge [
    source 31
    target 869
  ]
  edge [
    source 31
    target 870
  ]
  edge [
    source 31
    target 871
  ]
  edge [
    source 31
    target 872
  ]
  edge [
    source 31
    target 873
  ]
  edge [
    source 31
    target 874
  ]
  edge [
    source 31
    target 875
  ]
  edge [
    source 31
    target 876
  ]
  edge [
    source 31
    target 877
  ]
  edge [
    source 31
    target 161
  ]
  edge [
    source 31
    target 878
  ]
  edge [
    source 31
    target 879
  ]
  edge [
    source 31
    target 880
  ]
  edge [
    source 31
    target 881
  ]
  edge [
    source 31
    target 882
  ]
  edge [
    source 31
    target 883
  ]
  edge [
    source 31
    target 884
  ]
  edge [
    source 31
    target 885
  ]
  edge [
    source 31
    target 886
  ]
  edge [
    source 31
    target 887
  ]
  edge [
    source 31
    target 888
  ]
  edge [
    source 31
    target 889
  ]
  edge [
    source 31
    target 890
  ]
  edge [
    source 31
    target 891
  ]
  edge [
    source 31
    target 892
  ]
  edge [
    source 31
    target 893
  ]
  edge [
    source 31
    target 894
  ]
  edge [
    source 31
    target 895
  ]
  edge [
    source 31
    target 896
  ]
  edge [
    source 31
    target 897
  ]
  edge [
    source 31
    target 898
  ]
  edge [
    source 31
    target 899
  ]
  edge [
    source 31
    target 377
  ]
  edge [
    source 31
    target 900
  ]
  edge [
    source 31
    target 901
  ]
  edge [
    source 31
    target 902
  ]
  edge [
    source 31
    target 903
  ]
  edge [
    source 31
    target 904
  ]
  edge [
    source 31
    target 905
  ]
  edge [
    source 31
    target 906
  ]
  edge [
    source 31
    target 907
  ]
  edge [
    source 31
    target 908
  ]
  edge [
    source 31
    target 909
  ]
  edge [
    source 31
    target 910
  ]
  edge [
    source 31
    target 911
  ]
  edge [
    source 31
    target 912
  ]
  edge [
    source 31
    target 913
  ]
  edge [
    source 31
    target 914
  ]
  edge [
    source 31
    target 915
  ]
  edge [
    source 31
    target 916
  ]
  edge [
    source 31
    target 138
  ]
  edge [
    source 31
    target 917
  ]
  edge [
    source 31
    target 918
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 71
  ]
  edge [
    source 32
    target 130
  ]
  edge [
    source 32
    target 919
  ]
  edge [
    source 32
    target 920
  ]
  edge [
    source 32
    target 921
  ]
  edge [
    source 32
    target 61
  ]
  edge [
    source 32
    target 922
  ]
  edge [
    source 32
    target 78
  ]
  edge [
    source 32
    target 923
  ]
  edge [
    source 32
    target 924
  ]
  edge [
    source 32
    target 64
  ]
  edge [
    source 32
    target 66
  ]
  edge [
    source 32
    target 925
  ]
  edge [
    source 32
    target 67
  ]
  edge [
    source 32
    target 70
  ]
  edge [
    source 32
    target 72
  ]
  edge [
    source 32
    target 73
  ]
  edge [
    source 32
    target 926
  ]
  edge [
    source 32
    target 927
  ]
  edge [
    source 32
    target 928
  ]
  edge [
    source 32
    target 929
  ]
  edge [
    source 32
    target 930
  ]
  edge [
    source 32
    target 931
  ]
  edge [
    source 32
    target 932
  ]
  edge [
    source 32
    target 933
  ]
  edge [
    source 32
    target 934
  ]
  edge [
    source 32
    target 935
  ]
  edge [
    source 32
    target 936
  ]
  edge [
    source 32
    target 937
  ]
  edge [
    source 32
    target 938
  ]
  edge [
    source 32
    target 939
  ]
  edge [
    source 32
    target 940
  ]
  edge [
    source 32
    target 297
  ]
  edge [
    source 32
    target 941
  ]
  edge [
    source 32
    target 942
  ]
  edge [
    source 32
    target 943
  ]
  edge [
    source 32
    target 944
  ]
  edge [
    source 32
    target 945
  ]
  edge [
    source 32
    target 946
  ]
  edge [
    source 32
    target 947
  ]
  edge [
    source 32
    target 948
  ]
  edge [
    source 32
    target 949
  ]
  edge [
    source 32
    target 950
  ]
  edge [
    source 32
    target 951
  ]
  edge [
    source 32
    target 952
  ]
  edge [
    source 32
    target 953
  ]
  edge [
    source 32
    target 954
  ]
  edge [
    source 32
    target 955
  ]
  edge [
    source 32
    target 956
  ]
  edge [
    source 32
    target 957
  ]
  edge [
    source 32
    target 294
  ]
  edge [
    source 32
    target 958
  ]
  edge [
    source 32
    target 959
  ]
  edge [
    source 32
    target 75
  ]
  edge [
    source 32
    target 76
  ]
  edge [
    source 32
    target 960
  ]
  edge [
    source 32
    target 961
  ]
  edge [
    source 32
    target 962
  ]
  edge [
    source 32
    target 963
  ]
  edge [
    source 32
    target 964
  ]
  edge [
    source 32
    target 965
  ]
  edge [
    source 32
    target 966
  ]
  edge [
    source 32
    target 967
  ]
  edge [
    source 32
    target 968
  ]
  edge [
    source 32
    target 969
  ]
  edge [
    source 32
    target 970
  ]
  edge [
    source 32
    target 971
  ]
  edge [
    source 32
    target 972
  ]
  edge [
    source 32
    target 973
  ]
  edge [
    source 32
    target 974
  ]
  edge [
    source 32
    target 88
  ]
  edge [
    source 32
    target 89
  ]
  edge [
    source 32
    target 90
  ]
  edge [
    source 32
    target 91
  ]
  edge [
    source 32
    target 92
  ]
  edge [
    source 32
    target 93
  ]
  edge [
    source 32
    target 94
  ]
  edge [
    source 32
    target 95
  ]
  edge [
    source 32
    target 96
  ]
  edge [
    source 32
    target 97
  ]
  edge [
    source 32
    target 98
  ]
  edge [
    source 32
    target 99
  ]
  edge [
    source 32
    target 100
  ]
  edge [
    source 32
    target 101
  ]
  edge [
    source 32
    target 102
  ]
  edge [
    source 32
    target 103
  ]
  edge [
    source 32
    target 104
  ]
  edge [
    source 32
    target 105
  ]
  edge [
    source 32
    target 106
  ]
  edge [
    source 32
    target 107
  ]
  edge [
    source 32
    target 108
  ]
  edge [
    source 32
    target 109
  ]
  edge [
    source 32
    target 110
  ]
  edge [
    source 32
    target 111
  ]
  edge [
    source 32
    target 112
  ]
  edge [
    source 32
    target 113
  ]
  edge [
    source 32
    target 114
  ]
  edge [
    source 32
    target 115
  ]
  edge [
    source 32
    target 116
  ]
  edge [
    source 32
    target 117
  ]
  edge [
    source 32
    target 118
  ]
  edge [
    source 32
    target 119
  ]
  edge [
    source 32
    target 120
  ]
  edge [
    source 32
    target 121
  ]
  edge [
    source 32
    target 122
  ]
  edge [
    source 32
    target 123
  ]
  edge [
    source 32
    target 124
  ]
  edge [
    source 32
    target 125
  ]
  edge [
    source 32
    target 126
  ]
  edge [
    source 32
    target 127
  ]
  edge [
    source 32
    target 128
  ]
  edge [
    source 32
    target 129
  ]
  edge [
    source 32
    target 975
  ]
  edge [
    source 32
    target 976
  ]
  edge [
    source 32
    target 977
  ]
  edge [
    source 32
    target 978
  ]
  edge [
    source 32
    target 135
  ]
  edge [
    source 32
    target 131
  ]
  edge [
    source 32
    target 132
  ]
  edge [
    source 32
    target 979
  ]
  edge [
    source 32
    target 134
  ]
  edge [
    source 32
    target 980
  ]
  edge [
    source 32
    target 981
  ]
  edge [
    source 32
    target 982
  ]
  edge [
    source 32
    target 983
  ]
  edge [
    source 32
    target 984
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 985
  ]
  edge [
    source 33
    target 986
  ]
  edge [
    source 33
    target 987
  ]
  edge [
    source 33
    target 988
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 809
  ]
  edge [
    source 34
    target 810
  ]
  edge [
    source 34
    target 811
  ]
  edge [
    source 34
    target 812
  ]
  edge [
    source 34
    target 813
  ]
  edge [
    source 34
    target 814
  ]
  edge [
    source 34
    target 815
  ]
  edge [
    source 34
    target 816
  ]
  edge [
    source 34
    target 817
  ]
  edge [
    source 34
    target 818
  ]
  edge [
    source 34
    target 819
  ]
  edge [
    source 34
    target 757
  ]
  edge [
    source 34
    target 989
  ]
  edge [
    source 34
    target 990
  ]
  edge [
    source 34
    target 991
  ]
  edge [
    source 34
    target 992
  ]
  edge [
    source 34
    target 993
  ]
  edge [
    source 34
    target 994
  ]
  edge [
    source 34
    target 995
  ]
  edge [
    source 34
    target 832
  ]
  edge [
    source 34
    target 996
  ]
  edge [
    source 34
    target 997
  ]
  edge [
    source 34
    target 998
  ]
  edge [
    source 34
    target 999
  ]
  edge [
    source 34
    target 1000
  ]
  edge [
    source 34
    target 1001
  ]
  edge [
    source 34
    target 883
  ]
  edge [
    source 34
    target 1002
  ]
  edge [
    source 34
    target 1003
  ]
  edge [
    source 34
    target 1004
  ]
  edge [
    source 34
    target 1005
  ]
  edge [
    source 34
    target 343
  ]
  edge [
    source 34
    target 806
  ]
  edge [
    source 34
    target 585
  ]
  edge [
    source 34
    target 1006
  ]
  edge [
    source 34
    target 1007
  ]
  edge [
    source 34
    target 1008
  ]
  edge [
    source 34
    target 1009
  ]
  edge [
    source 34
    target 1010
  ]
  edge [
    source 34
    target 1011
  ]
  edge [
    source 34
    target 1012
  ]
  edge [
    source 34
    target 1013
  ]
  edge [
    source 34
    target 1014
  ]
  edge [
    source 34
    target 1015
  ]
  edge [
    source 34
    target 1016
  ]
  edge [
    source 34
    target 1017
  ]
  edge [
    source 34
    target 1018
  ]
  edge [
    source 34
    target 1019
  ]
  edge [
    source 34
    target 244
  ]
  edge [
    source 34
    target 1020
  ]
  edge [
    source 34
    target 1021
  ]
  edge [
    source 34
    target 904
  ]
  edge [
    source 34
    target 1022
  ]
  edge [
    source 34
    target 1023
  ]
  edge [
    source 34
    target 1024
  ]
  edge [
    source 34
    target 1025
  ]
  edge [
    source 34
    target 1026
  ]
  edge [
    source 34
    target 364
  ]
  edge [
    source 34
    target 808
  ]
  edge [
    source 34
    target 1027
  ]
  edge [
    source 34
    target 595
  ]
  edge [
    source 34
    target 1028
  ]
  edge [
    source 34
    target 1029
  ]
  edge [
    source 34
    target 1030
  ]
  edge [
    source 34
    target 1031
  ]
  edge [
    source 34
    target 1032
  ]
  edge [
    source 34
    target 148
  ]
  edge [
    source 34
    target 1033
  ]
  edge [
    source 34
    target 1034
  ]
  edge [
    source 34
    target 1035
  ]
  edge [
    source 34
    target 1036
  ]
  edge [
    source 34
    target 1037
  ]
  edge [
    source 34
    target 1038
  ]
  edge [
    source 34
    target 1039
  ]
  edge [
    source 34
    target 1040
  ]
  edge [
    source 34
    target 1041
  ]
  edge [
    source 34
    target 1042
  ]
  edge [
    source 34
    target 1043
  ]
  edge [
    source 34
    target 1044
  ]
  edge [
    source 34
    target 1045
  ]
  edge [
    source 34
    target 398
  ]
  edge [
    source 34
    target 332
  ]
  edge [
    source 34
    target 1046
  ]
  edge [
    source 34
    target 1047
  ]
  edge [
    source 34
    target 1048
  ]
  edge [
    source 34
    target 1049
  ]
  edge [
    source 34
    target 1050
  ]
  edge [
    source 34
    target 1051
  ]
  edge [
    source 34
    target 1052
  ]
  edge [
    source 34
    target 1053
  ]
  edge [
    source 34
    target 333
  ]
  edge [
    source 34
    target 1054
  ]
  edge [
    source 34
    target 1055
  ]
  edge [
    source 34
    target 1056
  ]
  edge [
    source 34
    target 1057
  ]
  edge [
    source 34
    target 1058
  ]
  edge [
    source 34
    target 1059
  ]
  edge [
    source 34
    target 973
  ]
  edge [
    source 34
    target 1060
  ]
  edge [
    source 34
    target 1061
  ]
  edge [
    source 34
    target 1062
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1063
  ]
  edge [
    source 35
    target 1064
  ]
  edge [
    source 35
    target 1065
  ]
  edge [
    source 35
    target 1066
  ]
  edge [
    source 35
    target 1067
  ]
  edge [
    source 35
    target 1068
  ]
  edge [
    source 35
    target 1069
  ]
  edge [
    source 35
    target 1070
  ]
  edge [
    source 35
    target 1071
  ]
  edge [
    source 35
    target 1072
  ]
  edge [
    source 35
    target 1073
  ]
  edge [
    source 35
    target 1074
  ]
  edge [
    source 35
    target 1075
  ]
  edge [
    source 35
    target 574
  ]
  edge [
    source 35
    target 1076
  ]
  edge [
    source 35
    target 1077
  ]
  edge [
    source 35
    target 1078
  ]
  edge [
    source 35
    target 1079
  ]
  edge [
    source 35
    target 681
  ]
  edge [
    source 35
    target 1080
  ]
  edge [
    source 35
    target 1081
  ]
  edge [
    source 35
    target 1082
  ]
  edge [
    source 35
    target 1083
  ]
  edge [
    source 35
    target 1084
  ]
  edge [
    source 35
    target 1085
  ]
  edge [
    source 35
    target 1086
  ]
  edge [
    source 35
    target 1087
  ]
  edge [
    source 35
    target 1088
  ]
  edge [
    source 35
    target 1089
  ]
  edge [
    source 35
    target 1090
  ]
  edge [
    source 35
    target 1091
  ]
  edge [
    source 35
    target 679
  ]
  edge [
    source 35
    target 1092
  ]
  edge [
    source 35
    target 1093
  ]
  edge [
    source 35
    target 1094
  ]
  edge [
    source 35
    target 1095
  ]
  edge [
    source 35
    target 1096
  ]
  edge [
    source 35
    target 1097
  ]
  edge [
    source 35
    target 1098
  ]
  edge [
    source 35
    target 1099
  ]
  edge [
    source 35
    target 1100
  ]
  edge [
    source 35
    target 1101
  ]
  edge [
    source 35
    target 1102
  ]
  edge [
    source 35
    target 1103
  ]
  edge [
    source 35
    target 1104
  ]
  edge [
    source 35
    target 1105
  ]
  edge [
    source 35
    target 1106
  ]
  edge [
    source 35
    target 1107
  ]
  edge [
    source 35
    target 1108
  ]
  edge [
    source 35
    target 1109
  ]
  edge [
    source 35
    target 1110
  ]
  edge [
    source 35
    target 1111
  ]
  edge [
    source 35
    target 1112
  ]
  edge [
    source 35
    target 1113
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 1114
  ]
  edge [
    source 35
    target 1115
  ]
  edge [
    source 35
    target 1116
  ]
  edge [
    source 35
    target 1117
  ]
  edge [
    source 35
    target 1118
  ]
  edge [
    source 35
    target 1119
  ]
  edge [
    source 35
    target 1120
  ]
  edge [
    source 35
    target 1121
  ]
  edge [
    source 35
    target 1122
  ]
  edge [
    source 35
    target 1123
  ]
  edge [
    source 35
    target 1124
  ]
  edge [
    source 35
    target 1125
  ]
  edge [
    source 35
    target 1126
  ]
  edge [
    source 35
    target 1127
  ]
  edge [
    source 35
    target 1128
  ]
  edge [
    source 35
    target 1129
  ]
  edge [
    source 35
    target 1130
  ]
  edge [
    source 35
    target 1131
  ]
  edge [
    source 35
    target 1132
  ]
  edge [
    source 35
    target 1133
  ]
  edge [
    source 35
    target 1134
  ]
  edge [
    source 35
    target 1135
  ]
  edge [
    source 35
    target 1136
  ]
  edge [
    source 35
    target 1137
  ]
  edge [
    source 35
    target 1138
  ]
  edge [
    source 35
    target 1139
  ]
  edge [
    source 35
    target 1140
  ]
  edge [
    source 35
    target 1141
  ]
  edge [
    source 35
    target 1142
  ]
  edge [
    source 35
    target 1143
  ]
  edge [
    source 35
    target 1144
  ]
  edge [
    source 35
    target 1145
  ]
  edge [
    source 35
    target 1146
  ]
  edge [
    source 35
    target 955
  ]
  edge [
    source 35
    target 1147
  ]
  edge [
    source 35
    target 1148
  ]
  edge [
    source 35
    target 1149
  ]
  edge [
    source 35
    target 1150
  ]
  edge [
    source 35
    target 1151
  ]
  edge [
    source 35
    target 1152
  ]
  edge [
    source 35
    target 1153
  ]
  edge [
    source 35
    target 1154
  ]
  edge [
    source 35
    target 1155
  ]
  edge [
    source 35
    target 1156
  ]
  edge [
    source 35
    target 1157
  ]
  edge [
    source 35
    target 1158
  ]
  edge [
    source 35
    target 1159
  ]
  edge [
    source 35
    target 1160
  ]
  edge [
    source 35
    target 1161
  ]
  edge [
    source 35
    target 1162
  ]
  edge [
    source 35
    target 1163
  ]
  edge [
    source 35
    target 1164
  ]
  edge [
    source 35
    target 1165
  ]
  edge [
    source 35
    target 655
  ]
  edge [
    source 35
    target 1166
  ]
  edge [
    source 35
    target 1167
  ]
  edge [
    source 35
    target 1168
  ]
  edge [
    source 35
    target 1169
  ]
  edge [
    source 35
    target 1170
  ]
  edge [
    source 35
    target 1171
  ]
  edge [
    source 35
    target 1172
  ]
  edge [
    source 35
    target 1173
  ]
  edge [
    source 35
    target 1174
  ]
  edge [
    source 35
    target 1175
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 751
  ]
  edge [
    source 36
    target 753
  ]
  edge [
    source 36
    target 1176
  ]
  edge [
    source 36
    target 745
  ]
  edge [
    source 36
    target 1177
  ]
  edge [
    source 36
    target 1178
  ]
  edge [
    source 36
    target 671
  ]
  edge [
    source 36
    target 1179
  ]
  edge [
    source 36
    target 1180
  ]
  edge [
    source 36
    target 1181
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1182
  ]
  edge [
    source 37
    target 1183
  ]
  edge [
    source 37
    target 1155
  ]
  edge [
    source 37
    target 1184
  ]
  edge [
    source 37
    target 1132
  ]
  edge [
    source 37
    target 1185
  ]
  edge [
    source 37
    target 1186
  ]
  edge [
    source 37
    target 1187
  ]
  edge [
    source 37
    target 1188
  ]
  edge [
    source 37
    target 343
  ]
  edge [
    source 37
    target 170
  ]
  edge [
    source 37
    target 1189
  ]
  edge [
    source 37
    target 1190
  ]
  edge [
    source 37
    target 1191
  ]
  edge [
    source 37
    target 1192
  ]
  edge [
    source 37
    target 1193
  ]
  edge [
    source 37
    target 1194
  ]
  edge [
    source 37
    target 1195
  ]
  edge [
    source 37
    target 1196
  ]
  edge [
    source 37
    target 1197
  ]
  edge [
    source 37
    target 1198
  ]
  edge [
    source 37
    target 1199
  ]
  edge [
    source 37
    target 1200
  ]
  edge [
    source 37
    target 1201
  ]
  edge [
    source 37
    target 1202
  ]
  edge [
    source 37
    target 297
  ]
  edge [
    source 37
    target 1203
  ]
  edge [
    source 37
    target 1095
  ]
  edge [
    source 37
    target 598
  ]
  edge [
    source 37
    target 1204
  ]
  edge [
    source 37
    target 1205
  ]
  edge [
    source 37
    target 1206
  ]
  edge [
    source 37
    target 1207
  ]
  edge [
    source 37
    target 1208
  ]
  edge [
    source 37
    target 1209
  ]
  edge [
    source 37
    target 1210
  ]
  edge [
    source 37
    target 1211
  ]
  edge [
    source 37
    target 1212
  ]
  edge [
    source 37
    target 1213
  ]
  edge [
    source 37
    target 429
  ]
  edge [
    source 37
    target 1214
  ]
  edge [
    source 37
    target 1215
  ]
  edge [
    source 37
    target 1216
  ]
  edge [
    source 37
    target 1217
  ]
  edge [
    source 37
    target 1218
  ]
  edge [
    source 37
    target 271
  ]
  edge [
    source 37
    target 1219
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 574
  ]
  edge [
    source 39
    target 1220
  ]
  edge [
    source 39
    target 1221
  ]
  edge [
    source 39
    target 361
  ]
  edge [
    source 39
    target 1222
  ]
  edge [
    source 39
    target 1223
  ]
  edge [
    source 39
    target 1224
  ]
  edge [
    source 39
    target 1225
  ]
  edge [
    source 39
    target 1226
  ]
  edge [
    source 39
    target 1227
  ]
  edge [
    source 39
    target 1228
  ]
  edge [
    source 39
    target 1229
  ]
  edge [
    source 39
    target 1230
  ]
  edge [
    source 39
    target 1231
  ]
  edge [
    source 39
    target 1232
  ]
  edge [
    source 39
    target 581
  ]
  edge [
    source 39
    target 1233
  ]
  edge [
    source 39
    target 1234
  ]
  edge [
    source 39
    target 1235
  ]
  edge [
    source 39
    target 1236
  ]
  edge [
    source 39
    target 1237
  ]
  edge [
    source 39
    target 1087
  ]
  edge [
    source 39
    target 1238
  ]
  edge [
    source 39
    target 1239
  ]
  edge [
    source 39
    target 1240
  ]
  edge [
    source 39
    target 1241
  ]
  edge [
    source 39
    target 1242
  ]
  edge [
    source 39
    target 1243
  ]
  edge [
    source 40
    target 794
  ]
  edge [
    source 40
    target 947
  ]
  edge [
    source 40
    target 1244
  ]
  edge [
    source 40
    target 1245
  ]
  edge [
    source 40
    target 1246
  ]
  edge [
    source 40
    target 1247
  ]
  edge [
    source 40
    target 1248
  ]
  edge [
    source 40
    target 1249
  ]
  edge [
    source 40
    target 1250
  ]
  edge [
    source 40
    target 1251
  ]
  edge [
    source 40
    target 1252
  ]
  edge [
    source 40
    target 469
  ]
  edge [
    source 40
    target 1253
  ]
  edge [
    source 40
    target 1254
  ]
  edge [
    source 40
    target 1255
  ]
  edge [
    source 40
    target 1256
  ]
  edge [
    source 40
    target 832
  ]
  edge [
    source 40
    target 1257
  ]
  edge [
    source 40
    target 1258
  ]
  edge [
    source 40
    target 1259
  ]
  edge [
    source 40
    target 763
  ]
  edge [
    source 40
    target 1260
  ]
  edge [
    source 40
    target 1261
  ]
  edge [
    source 40
    target 1262
  ]
  edge [
    source 40
    target 1263
  ]
  edge [
    source 40
    target 1264
  ]
  edge [
    source 40
    target 1265
  ]
  edge [
    source 40
    target 820
  ]
  edge [
    source 40
    target 821
  ]
  edge [
    source 40
    target 822
  ]
  edge [
    source 40
    target 823
  ]
  edge [
    source 40
    target 824
  ]
  edge [
    source 40
    target 825
  ]
  edge [
    source 40
    target 826
  ]
  edge [
    source 40
    target 227
  ]
  edge [
    source 40
    target 827
  ]
  edge [
    source 40
    target 828
  ]
  edge [
    source 40
    target 472
  ]
  edge [
    source 40
    target 806
  ]
  edge [
    source 40
    target 829
  ]
  edge [
    source 40
    target 816
  ]
  edge [
    source 40
    target 830
  ]
  edge [
    source 40
    target 819
  ]
  edge [
    source 40
    target 808
  ]
  edge [
    source 40
    target 831
  ]
  edge [
    source 40
    target 1266
  ]
  edge [
    source 40
    target 890
  ]
  edge [
    source 40
    target 1267
  ]
  edge [
    source 40
    target 191
  ]
  edge [
    source 40
    target 1268
  ]
  edge [
    source 40
    target 1269
  ]
  edge [
    source 40
    target 1270
  ]
  edge [
    source 40
    target 483
  ]
  edge [
    source 40
    target 140
  ]
  edge [
    source 40
    target 1271
  ]
  edge [
    source 40
    target 1272
  ]
  edge [
    source 40
    target 1273
  ]
  edge [
    source 40
    target 888
  ]
  edge [
    source 40
    target 1274
  ]
  edge [
    source 40
    target 920
  ]
  edge [
    source 40
    target 837
  ]
  edge [
    source 40
    target 801
  ]
  edge [
    source 40
    target 1275
  ]
  edge [
    source 40
    target 1276
  ]
  edge [
    source 40
    target 1277
  ]
  edge [
    source 40
    target 1278
  ]
  edge [
    source 40
    target 1279
  ]
  edge [
    source 40
    target 1280
  ]
  edge [
    source 40
    target 1281
  ]
  edge [
    source 40
    target 1282
  ]
  edge [
    source 40
    target 1283
  ]
  edge [
    source 40
    target 1284
  ]
  edge [
    source 40
    target 1285
  ]
  edge [
    source 40
    target 1286
  ]
  edge [
    source 40
    target 1287
  ]
  edge [
    source 40
    target 1288
  ]
  edge [
    source 40
    target 1289
  ]
  edge [
    source 40
    target 1290
  ]
  edge [
    source 40
    target 1291
  ]
  edge [
    source 40
    target 1292
  ]
  edge [
    source 40
    target 1293
  ]
  edge [
    source 40
    target 1294
  ]
  edge [
    source 40
    target 1295
  ]
  edge [
    source 40
    target 1296
  ]
  edge [
    source 40
    target 1297
  ]
  edge [
    source 40
    target 1298
  ]
  edge [
    source 40
    target 928
  ]
  edge [
    source 40
    target 1299
  ]
  edge [
    source 40
    target 1300
  ]
  edge [
    source 40
    target 1301
  ]
  edge [
    source 40
    target 1302
  ]
  edge [
    source 40
    target 1303
  ]
  edge [
    source 40
    target 1304
  ]
  edge [
    source 40
    target 1305
  ]
  edge [
    source 40
    target 1306
  ]
  edge [
    source 40
    target 581
  ]
  edge [
    source 40
    target 1307
  ]
  edge [
    source 40
    target 1308
  ]
  edge [
    source 40
    target 1309
  ]
  edge [
    source 40
    target 1310
  ]
  edge [
    source 40
    target 1311
  ]
  edge [
    source 40
    target 1312
  ]
  edge [
    source 40
    target 285
  ]
  edge [
    source 40
    target 1313
  ]
  edge [
    source 40
    target 1314
  ]
  edge [
    source 40
    target 1315
  ]
  edge [
    source 40
    target 958
  ]
  edge [
    source 40
    target 1316
  ]
  edge [
    source 40
    target 1317
  ]
  edge [
    source 40
    target 1318
  ]
  edge [
    source 40
    target 1319
  ]
  edge [
    source 41
    target 1320
  ]
  edge [
    source 41
    target 1321
  ]
  edge [
    source 41
    target 1322
  ]
  edge [
    source 1323
    target 1324
  ]
  edge [
    source 1325
    target 1326
  ]
  edge [
    source 1327
    target 1328
  ]
  edge [
    source 1329
    target 1330
  ]
  edge [
    source 1329
    target 1331
  ]
  edge [
    source 1330
    target 1331
  ]
  edge [
    source 1332
    target 1333
  ]
]
