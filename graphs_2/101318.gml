graph [
  node [
    id 0
    label "luigi"
    origin "text"
  ]
  node [
    id 1
    label "cadorna"
    origin "text"
  ]
  node [
    id 2
    label "Luigi"
  ]
  node [
    id 3
    label "Cadorna"
  ]
  node [
    id 4
    label "sztab"
  ]
  node [
    id 5
    label "generalny"
  ]
  node [
    id 6
    label "armia"
  ]
  node [
    id 7
    label "w&#322;oski"
  ]
  node [
    id 8
    label "i"
  ]
  node [
    id 9
    label "wojna"
  ]
  node [
    id 10
    label "&#347;wiatowy"
  ]
  node [
    id 11
    label "Austro"
  ]
  node [
    id 12
    label "w&#281;gier"
  ]
  node [
    id 13
    label "bitwa"
  ]
  node [
    id 14
    label "pod"
  ]
  node [
    id 15
    label "Caporetto"
  ]
  node [
    id 16
    label "Armando"
  ]
  node [
    id 17
    label "Diaza"
  ]
  node [
    id 18
    label "Benita"
  ]
  node [
    id 19
    label "Mussolini"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
]
