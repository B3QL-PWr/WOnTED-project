graph [
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "trybuna&#322;"
    origin "text"
  ]
  node [
    id 5
    label "konstytucyjny"
    origin "text"
  ]
  node [
    id 6
    label "wyrok"
    origin "text"
  ]
  node [
    id 7
    label "wrzesie&#324;"
    origin "text"
  ]
  node [
    id 8
    label "rocznik"
    origin "text"
  ]
  node [
    id 9
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 10
    label "metr"
    origin "text"
  ]
  node [
    id 11
    label "jeden"
    origin "text"
  ]
  node [
    id 12
    label "podstawowy"
    origin "text"
  ]
  node [
    id 13
    label "przes&#322;anka"
    origin "text"
  ]
  node [
    id 14
    label "wype&#322;nienie"
    origin "text"
  ]
  node [
    id 15
    label "ustawowo"
    origin "text"
  ]
  node [
    id 16
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 17
    label "misja"
    origin "text"
  ]
  node [
    id 18
    label "przez"
    origin "text"
  ]
  node [
    id 19
    label "media"
    origin "text"
  ]
  node [
    id 20
    label "publiczny"
    origin "text"
  ]
  node [
    id 21
    label "by&#263;"
    origin "text"
  ]
  node [
    id 22
    label "w&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 23
    label "zasada"
    origin "text"
  ]
  node [
    id 24
    label "funkcjonowanie"
    origin "text"
  ]
  node [
    id 25
    label "tymczasem"
    origin "text"
  ]
  node [
    id 26
    label "sp&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 27
    label "radiofonia"
    origin "text"
  ]
  node [
    id 28
    label "telewizja"
    origin "text"
  ]
  node [
    id 29
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "coraz"
    origin "text"
  ]
  node [
    id 31
    label "mniejszy"
    origin "text"
  ]
  node [
    id 32
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 33
    label "finansowy"
    origin "text"
  ]
  node [
    id 34
    label "zdecydowanie"
    origin "text"
  ]
  node [
    id 35
    label "pogarsza&#263;"
    origin "text"
  ]
  node [
    id 36
    label "sytuacja"
    origin "text"
  ]
  node [
    id 37
    label "finansowo"
    origin "text"
  ]
  node [
    id 38
    label "ekonomiczny"
    origin "text"
  ]
  node [
    id 39
    label "nadawca"
    origin "text"
  ]
  node [
    id 40
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 41
    label "tychy"
    origin "text"
  ]
  node [
    id 42
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 43
    label "obecnie"
    origin "text"
  ]
  node [
    id 44
    label "drastyczny"
    origin "text"
  ]
  node [
    id 45
    label "ci&#281;cie"
    origin "text"
  ]
  node [
    id 46
    label "bud&#380;etowy"
    origin "text"
  ]
  node [
    id 47
    label "zwolnienie"
    origin "text"
  ]
  node [
    id 48
    label "pracownik"
    origin "text"
  ]
  node [
    id 49
    label "w&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 50
    label "za&#322;amanie"
    origin "text"
  ]
  node [
    id 51
    label "poziom"
    origin "text"
  ]
  node [
    id 52
    label "inkasowa&#263;"
    origin "text"
  ]
  node [
    id 53
    label "wp&#322;yw"
    origin "text"
  ]
  node [
    id 54
    label "abonamentowy"
    origin "text"
  ]
  node [
    id 55
    label "nast&#261;pi&#263;"
    origin "text"
  ]
  node [
    id 56
    label "zapowied&#378;"
    origin "text"
  ]
  node [
    id 57
    label "rz&#261;d"
    origin "text"
  ]
  node [
    id 58
    label "ustawowy"
    origin "text"
  ]
  node [
    id 59
    label "op&#322;ata"
    origin "text"
  ]
  node [
    id 60
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 61
    label "grupa"
    origin "text"
  ]
  node [
    id 62
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 63
    label "ca&#322;kowity"
    origin "text"
  ]
  node [
    id 64
    label "zniesienie"
    origin "text"
  ]
  node [
    id 65
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 66
    label "tym"
    origin "text"
  ]
  node [
    id 67
    label "koalicyjny"
    origin "text"
  ]
  node [
    id 68
    label "czuj"
    origin "text"
  ]
  node [
    id 69
    label "si&#281;"
    origin "text"
  ]
  node [
    id 70
    label "odpowiedzialny"
    origin "text"
  ]
  node [
    id 71
    label "pogr&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 72
    label "obecna"
    origin "text"
  ]
  node [
    id 73
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 74
    label "mo&#380;liwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 75
    label "naprawienie"
    origin "text"
  ]
  node [
    id 76
    label "swoje"
    origin "text"
  ]
  node [
    id 77
    label "nieodpowiedzialny"
    origin "text"
  ]
  node [
    id 78
    label "dzia&#322;ania"
    origin "text"
  ]
  node [
    id 79
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 80
    label "doprowadzi&#263;"
    origin "text"
  ]
  node [
    id 81
    label "zapa&#347;&#263;"
    origin "text"
  ]
  node [
    id 82
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 83
    label "bardzo"
    origin "text"
  ]
  node [
    id 84
    label "oklask"
    origin "text"
  ]
  node [
    id 85
    label "belfer"
  ]
  node [
    id 86
    label "murza"
  ]
  node [
    id 87
    label "cz&#322;owiek"
  ]
  node [
    id 88
    label "ojciec"
  ]
  node [
    id 89
    label "samiec"
  ]
  node [
    id 90
    label "androlog"
  ]
  node [
    id 91
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 92
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 93
    label "efendi"
  ]
  node [
    id 94
    label "opiekun"
  ]
  node [
    id 95
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 96
    label "pa&#324;stwo"
  ]
  node [
    id 97
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 98
    label "bratek"
  ]
  node [
    id 99
    label "Mieszko_I"
  ]
  node [
    id 100
    label "Midas"
  ]
  node [
    id 101
    label "m&#261;&#380;"
  ]
  node [
    id 102
    label "bogaty"
  ]
  node [
    id 103
    label "popularyzator"
  ]
  node [
    id 104
    label "pracodawca"
  ]
  node [
    id 105
    label "kszta&#322;ciciel"
  ]
  node [
    id 106
    label "preceptor"
  ]
  node [
    id 107
    label "nabab"
  ]
  node [
    id 108
    label "pupil"
  ]
  node [
    id 109
    label "andropauza"
  ]
  node [
    id 110
    label "zwrot"
  ]
  node [
    id 111
    label "przyw&#243;dca"
  ]
  node [
    id 112
    label "doros&#322;y"
  ]
  node [
    id 113
    label "pedagog"
  ]
  node [
    id 114
    label "rz&#261;dzenie"
  ]
  node [
    id 115
    label "jegomo&#347;&#263;"
  ]
  node [
    id 116
    label "szkolnik"
  ]
  node [
    id 117
    label "ch&#322;opina"
  ]
  node [
    id 118
    label "w&#322;odarz"
  ]
  node [
    id 119
    label "profesor"
  ]
  node [
    id 120
    label "gra_w_karty"
  ]
  node [
    id 121
    label "w&#322;adza"
  ]
  node [
    id 122
    label "Fidel_Castro"
  ]
  node [
    id 123
    label "Anders"
  ]
  node [
    id 124
    label "Ko&#347;ciuszko"
  ]
  node [
    id 125
    label "Tito"
  ]
  node [
    id 126
    label "Miko&#322;ajczyk"
  ]
  node [
    id 127
    label "Sabataj_Cwi"
  ]
  node [
    id 128
    label "lider"
  ]
  node [
    id 129
    label "Mao"
  ]
  node [
    id 130
    label "p&#322;atnik"
  ]
  node [
    id 131
    label "zwierzchnik"
  ]
  node [
    id 132
    label "ponadzak&#322;adowy_uk&#322;ad_zbiorowy_pracy"
  ]
  node [
    id 133
    label "nadzorca"
  ]
  node [
    id 134
    label "funkcjonariusz"
  ]
  node [
    id 135
    label "podmiot"
  ]
  node [
    id 136
    label "wykupienie"
  ]
  node [
    id 137
    label "bycie_w_posiadaniu"
  ]
  node [
    id 138
    label "wykupywanie"
  ]
  node [
    id 139
    label "rozszerzyciel"
  ]
  node [
    id 140
    label "ludzko&#347;&#263;"
  ]
  node [
    id 141
    label "asymilowanie"
  ]
  node [
    id 142
    label "wapniak"
  ]
  node [
    id 143
    label "asymilowa&#263;"
  ]
  node [
    id 144
    label "os&#322;abia&#263;"
  ]
  node [
    id 145
    label "posta&#263;"
  ]
  node [
    id 146
    label "hominid"
  ]
  node [
    id 147
    label "podw&#322;adny"
  ]
  node [
    id 148
    label "os&#322;abianie"
  ]
  node [
    id 149
    label "g&#322;owa"
  ]
  node [
    id 150
    label "figura"
  ]
  node [
    id 151
    label "portrecista"
  ]
  node [
    id 152
    label "dwun&#243;g"
  ]
  node [
    id 153
    label "profanum"
  ]
  node [
    id 154
    label "mikrokosmos"
  ]
  node [
    id 155
    label "nasada"
  ]
  node [
    id 156
    label "duch"
  ]
  node [
    id 157
    label "antropochoria"
  ]
  node [
    id 158
    label "osoba"
  ]
  node [
    id 159
    label "wz&#243;r"
  ]
  node [
    id 160
    label "senior"
  ]
  node [
    id 161
    label "oddzia&#322;ywanie"
  ]
  node [
    id 162
    label "Adam"
  ]
  node [
    id 163
    label "homo_sapiens"
  ]
  node [
    id 164
    label "polifag"
  ]
  node [
    id 165
    label "wydoro&#347;lenie"
  ]
  node [
    id 166
    label "doro&#347;lenie"
  ]
  node [
    id 167
    label "&#378;ra&#322;y"
  ]
  node [
    id 168
    label "doro&#347;le"
  ]
  node [
    id 169
    label "dojrzale"
  ]
  node [
    id 170
    label "dojrza&#322;y"
  ]
  node [
    id 171
    label "m&#261;dry"
  ]
  node [
    id 172
    label "doletni"
  ]
  node [
    id 173
    label "punkt"
  ]
  node [
    id 174
    label "turn"
  ]
  node [
    id 175
    label "turning"
  ]
  node [
    id 176
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 177
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 178
    label "skr&#281;t"
  ]
  node [
    id 179
    label "obr&#243;t"
  ]
  node [
    id 180
    label "fraza_czasownikowa"
  ]
  node [
    id 181
    label "jednostka_leksykalna"
  ]
  node [
    id 182
    label "zmiana"
  ]
  node [
    id 183
    label "wyra&#380;enie"
  ]
  node [
    id 184
    label "starosta"
  ]
  node [
    id 185
    label "zarz&#261;dca"
  ]
  node [
    id 186
    label "w&#322;adca"
  ]
  node [
    id 187
    label "nauczyciel"
  ]
  node [
    id 188
    label "autor"
  ]
  node [
    id 189
    label "wyprawka"
  ]
  node [
    id 190
    label "mundurek"
  ]
  node [
    id 191
    label "szko&#322;a"
  ]
  node [
    id 192
    label "tarcza"
  ]
  node [
    id 193
    label "elew"
  ]
  node [
    id 194
    label "absolwent"
  ]
  node [
    id 195
    label "klasa"
  ]
  node [
    id 196
    label "stopie&#324;_naukowy"
  ]
  node [
    id 197
    label "nauczyciel_akademicki"
  ]
  node [
    id 198
    label "tytu&#322;"
  ]
  node [
    id 199
    label "profesura"
  ]
  node [
    id 200
    label "konsulent"
  ]
  node [
    id 201
    label "wirtuoz"
  ]
  node [
    id 202
    label "ekspert"
  ]
  node [
    id 203
    label "ochotnik"
  ]
  node [
    id 204
    label "pomocnik"
  ]
  node [
    id 205
    label "student"
  ]
  node [
    id 206
    label "nauczyciel_muzyki"
  ]
  node [
    id 207
    label "zakonnik"
  ]
  node [
    id 208
    label "urz&#281;dnik"
  ]
  node [
    id 209
    label "bogacz"
  ]
  node [
    id 210
    label "dostojnik"
  ]
  node [
    id 211
    label "mo&#347;&#263;"
  ]
  node [
    id 212
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 213
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 214
    label "kuwada"
  ]
  node [
    id 215
    label "tworzyciel"
  ]
  node [
    id 216
    label "rodzice"
  ]
  node [
    id 217
    label "&#347;w"
  ]
  node [
    id 218
    label "pomys&#322;odawca"
  ]
  node [
    id 219
    label "rodzic"
  ]
  node [
    id 220
    label "wykonawca"
  ]
  node [
    id 221
    label "ojczym"
  ]
  node [
    id 222
    label "przodek"
  ]
  node [
    id 223
    label "papa"
  ]
  node [
    id 224
    label "stary"
  ]
  node [
    id 225
    label "zwierz&#281;"
  ]
  node [
    id 226
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 227
    label "kochanek"
  ]
  node [
    id 228
    label "fio&#322;ek"
  ]
  node [
    id 229
    label "facet"
  ]
  node [
    id 230
    label "brat"
  ]
  node [
    id 231
    label "ma&#322;&#380;onek"
  ]
  node [
    id 232
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 233
    label "m&#243;j"
  ]
  node [
    id 234
    label "ch&#322;op"
  ]
  node [
    id 235
    label "pan_m&#322;ody"
  ]
  node [
    id 236
    label "&#347;lubny"
  ]
  node [
    id 237
    label "pan_domu"
  ]
  node [
    id 238
    label "pan_i_w&#322;adca"
  ]
  node [
    id 239
    label "Frygia"
  ]
  node [
    id 240
    label "sprawowanie"
  ]
  node [
    id 241
    label "dominion"
  ]
  node [
    id 242
    label "dominowanie"
  ]
  node [
    id 243
    label "reign"
  ]
  node [
    id 244
    label "rule"
  ]
  node [
    id 245
    label "zwierz&#281;_domowe"
  ]
  node [
    id 246
    label "J&#281;drzejewicz"
  ]
  node [
    id 247
    label "Friedrich_Fr&#246;bel"
  ]
  node [
    id 248
    label "specjalista_od_nauk_spo&#322;ecznych"
  ]
  node [
    id 249
    label "John_Dewey"
  ]
  node [
    id 250
    label "specjalista"
  ]
  node [
    id 251
    label "&#380;ycie"
  ]
  node [
    id 252
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 253
    label "Turek"
  ]
  node [
    id 254
    label "effendi"
  ]
  node [
    id 255
    label "obfituj&#261;cy"
  ]
  node [
    id 256
    label "r&#243;&#380;norodny"
  ]
  node [
    id 257
    label "spania&#322;y"
  ]
  node [
    id 258
    label "obficie"
  ]
  node [
    id 259
    label "sytuowany"
  ]
  node [
    id 260
    label "och&#281;do&#380;ny"
  ]
  node [
    id 261
    label "forsiasty"
  ]
  node [
    id 262
    label "zapa&#347;ny"
  ]
  node [
    id 263
    label "bogato"
  ]
  node [
    id 264
    label "Katar"
  ]
  node [
    id 265
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 266
    label "Libia"
  ]
  node [
    id 267
    label "Gwatemala"
  ]
  node [
    id 268
    label "Afganistan"
  ]
  node [
    id 269
    label "Ekwador"
  ]
  node [
    id 270
    label "Tad&#380;ykistan"
  ]
  node [
    id 271
    label "Bhutan"
  ]
  node [
    id 272
    label "Argentyna"
  ]
  node [
    id 273
    label "D&#380;ibuti"
  ]
  node [
    id 274
    label "Wenezuela"
  ]
  node [
    id 275
    label "Ukraina"
  ]
  node [
    id 276
    label "Gabon"
  ]
  node [
    id 277
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 278
    label "Rwanda"
  ]
  node [
    id 279
    label "Liechtenstein"
  ]
  node [
    id 280
    label "organizacja"
  ]
  node [
    id 281
    label "Sri_Lanka"
  ]
  node [
    id 282
    label "Madagaskar"
  ]
  node [
    id 283
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 284
    label "Tonga"
  ]
  node [
    id 285
    label "Kongo"
  ]
  node [
    id 286
    label "Bangladesz"
  ]
  node [
    id 287
    label "Kanada"
  ]
  node [
    id 288
    label "Wehrlen"
  ]
  node [
    id 289
    label "Algieria"
  ]
  node [
    id 290
    label "Surinam"
  ]
  node [
    id 291
    label "Chile"
  ]
  node [
    id 292
    label "Sahara_Zachodnia"
  ]
  node [
    id 293
    label "Uganda"
  ]
  node [
    id 294
    label "W&#281;gry"
  ]
  node [
    id 295
    label "Birma"
  ]
  node [
    id 296
    label "Kazachstan"
  ]
  node [
    id 297
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 298
    label "Armenia"
  ]
  node [
    id 299
    label "Tuwalu"
  ]
  node [
    id 300
    label "Timor_Wschodni"
  ]
  node [
    id 301
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 302
    label "Izrael"
  ]
  node [
    id 303
    label "Estonia"
  ]
  node [
    id 304
    label "Komory"
  ]
  node [
    id 305
    label "Kamerun"
  ]
  node [
    id 306
    label "Haiti"
  ]
  node [
    id 307
    label "Belize"
  ]
  node [
    id 308
    label "Sierra_Leone"
  ]
  node [
    id 309
    label "Luksemburg"
  ]
  node [
    id 310
    label "USA"
  ]
  node [
    id 311
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 312
    label "Barbados"
  ]
  node [
    id 313
    label "San_Marino"
  ]
  node [
    id 314
    label "Bu&#322;garia"
  ]
  node [
    id 315
    label "Wietnam"
  ]
  node [
    id 316
    label "Indonezja"
  ]
  node [
    id 317
    label "Malawi"
  ]
  node [
    id 318
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 319
    label "Francja"
  ]
  node [
    id 320
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 321
    label "partia"
  ]
  node [
    id 322
    label "Zambia"
  ]
  node [
    id 323
    label "Angola"
  ]
  node [
    id 324
    label "Grenada"
  ]
  node [
    id 325
    label "Nepal"
  ]
  node [
    id 326
    label "Panama"
  ]
  node [
    id 327
    label "Rumunia"
  ]
  node [
    id 328
    label "Czarnog&#243;ra"
  ]
  node [
    id 329
    label "Malediwy"
  ]
  node [
    id 330
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 331
    label "S&#322;owacja"
  ]
  node [
    id 332
    label "para"
  ]
  node [
    id 333
    label "Egipt"
  ]
  node [
    id 334
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 335
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 336
    label "Kolumbia"
  ]
  node [
    id 337
    label "Mozambik"
  ]
  node [
    id 338
    label "Laos"
  ]
  node [
    id 339
    label "Burundi"
  ]
  node [
    id 340
    label "Suazi"
  ]
  node [
    id 341
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 342
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 343
    label "Czechy"
  ]
  node [
    id 344
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 345
    label "Wyspy_Marshalla"
  ]
  node [
    id 346
    label "Trynidad_i_Tobago"
  ]
  node [
    id 347
    label "Dominika"
  ]
  node [
    id 348
    label "Palau"
  ]
  node [
    id 349
    label "Syria"
  ]
  node [
    id 350
    label "Gwinea_Bissau"
  ]
  node [
    id 351
    label "Liberia"
  ]
  node [
    id 352
    label "Zimbabwe"
  ]
  node [
    id 353
    label "Polska"
  ]
  node [
    id 354
    label "Jamajka"
  ]
  node [
    id 355
    label "Dominikana"
  ]
  node [
    id 356
    label "Senegal"
  ]
  node [
    id 357
    label "Gruzja"
  ]
  node [
    id 358
    label "Togo"
  ]
  node [
    id 359
    label "Chorwacja"
  ]
  node [
    id 360
    label "Meksyk"
  ]
  node [
    id 361
    label "Macedonia"
  ]
  node [
    id 362
    label "Gujana"
  ]
  node [
    id 363
    label "Zair"
  ]
  node [
    id 364
    label "Albania"
  ]
  node [
    id 365
    label "Kambod&#380;a"
  ]
  node [
    id 366
    label "Mauritius"
  ]
  node [
    id 367
    label "Monako"
  ]
  node [
    id 368
    label "Gwinea"
  ]
  node [
    id 369
    label "Mali"
  ]
  node [
    id 370
    label "Nigeria"
  ]
  node [
    id 371
    label "Kostaryka"
  ]
  node [
    id 372
    label "Hanower"
  ]
  node [
    id 373
    label "Paragwaj"
  ]
  node [
    id 374
    label "W&#322;ochy"
  ]
  node [
    id 375
    label "Wyspy_Salomona"
  ]
  node [
    id 376
    label "Seszele"
  ]
  node [
    id 377
    label "Hiszpania"
  ]
  node [
    id 378
    label "Boliwia"
  ]
  node [
    id 379
    label "Kirgistan"
  ]
  node [
    id 380
    label "Irlandia"
  ]
  node [
    id 381
    label "Czad"
  ]
  node [
    id 382
    label "Irak"
  ]
  node [
    id 383
    label "Lesoto"
  ]
  node [
    id 384
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 385
    label "Malta"
  ]
  node [
    id 386
    label "Andora"
  ]
  node [
    id 387
    label "Chiny"
  ]
  node [
    id 388
    label "Filipiny"
  ]
  node [
    id 389
    label "Antarktis"
  ]
  node [
    id 390
    label "Niemcy"
  ]
  node [
    id 391
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 392
    label "Brazylia"
  ]
  node [
    id 393
    label "terytorium"
  ]
  node [
    id 394
    label "Nikaragua"
  ]
  node [
    id 395
    label "Pakistan"
  ]
  node [
    id 396
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 397
    label "Kenia"
  ]
  node [
    id 398
    label "Niger"
  ]
  node [
    id 399
    label "Tunezja"
  ]
  node [
    id 400
    label "Portugalia"
  ]
  node [
    id 401
    label "Fid&#380;i"
  ]
  node [
    id 402
    label "Maroko"
  ]
  node [
    id 403
    label "Botswana"
  ]
  node [
    id 404
    label "Tajlandia"
  ]
  node [
    id 405
    label "Australia"
  ]
  node [
    id 406
    label "Burkina_Faso"
  ]
  node [
    id 407
    label "interior"
  ]
  node [
    id 408
    label "Benin"
  ]
  node [
    id 409
    label "Tanzania"
  ]
  node [
    id 410
    label "Indie"
  ]
  node [
    id 411
    label "&#321;otwa"
  ]
  node [
    id 412
    label "Kiribati"
  ]
  node [
    id 413
    label "Antigua_i_Barbuda"
  ]
  node [
    id 414
    label "Rodezja"
  ]
  node [
    id 415
    label "Cypr"
  ]
  node [
    id 416
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 417
    label "Peru"
  ]
  node [
    id 418
    label "Austria"
  ]
  node [
    id 419
    label "Urugwaj"
  ]
  node [
    id 420
    label "Jordania"
  ]
  node [
    id 421
    label "Grecja"
  ]
  node [
    id 422
    label "Azerbejd&#380;an"
  ]
  node [
    id 423
    label "Turcja"
  ]
  node [
    id 424
    label "Samoa"
  ]
  node [
    id 425
    label "Sudan"
  ]
  node [
    id 426
    label "Oman"
  ]
  node [
    id 427
    label "ziemia"
  ]
  node [
    id 428
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 429
    label "Uzbekistan"
  ]
  node [
    id 430
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 431
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 432
    label "Honduras"
  ]
  node [
    id 433
    label "Mongolia"
  ]
  node [
    id 434
    label "Portoryko"
  ]
  node [
    id 435
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 436
    label "Serbia"
  ]
  node [
    id 437
    label "Tajwan"
  ]
  node [
    id 438
    label "Wielka_Brytania"
  ]
  node [
    id 439
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 440
    label "Liban"
  ]
  node [
    id 441
    label "Japonia"
  ]
  node [
    id 442
    label "Ghana"
  ]
  node [
    id 443
    label "Bahrajn"
  ]
  node [
    id 444
    label "Belgia"
  ]
  node [
    id 445
    label "Etiopia"
  ]
  node [
    id 446
    label "Mikronezja"
  ]
  node [
    id 447
    label "Kuwejt"
  ]
  node [
    id 448
    label "Bahamy"
  ]
  node [
    id 449
    label "Rosja"
  ]
  node [
    id 450
    label "Mo&#322;dawia"
  ]
  node [
    id 451
    label "Litwa"
  ]
  node [
    id 452
    label "S&#322;owenia"
  ]
  node [
    id 453
    label "Szwajcaria"
  ]
  node [
    id 454
    label "Erytrea"
  ]
  node [
    id 455
    label "Kuba"
  ]
  node [
    id 456
    label "Arabia_Saudyjska"
  ]
  node [
    id 457
    label "granica_pa&#324;stwa"
  ]
  node [
    id 458
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 459
    label "Malezja"
  ]
  node [
    id 460
    label "Korea"
  ]
  node [
    id 461
    label "Jemen"
  ]
  node [
    id 462
    label "Nowa_Zelandia"
  ]
  node [
    id 463
    label "Namibia"
  ]
  node [
    id 464
    label "Nauru"
  ]
  node [
    id 465
    label "holoarktyka"
  ]
  node [
    id 466
    label "Brunei"
  ]
  node [
    id 467
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 468
    label "Khitai"
  ]
  node [
    id 469
    label "Mauretania"
  ]
  node [
    id 470
    label "Iran"
  ]
  node [
    id 471
    label "Gambia"
  ]
  node [
    id 472
    label "Somalia"
  ]
  node [
    id 473
    label "Holandia"
  ]
  node [
    id 474
    label "Turkmenistan"
  ]
  node [
    id 475
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 476
    label "Salwador"
  ]
  node [
    id 477
    label "Pi&#322;sudski"
  ]
  node [
    id 478
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 479
    label "parlamentarzysta"
  ]
  node [
    id 480
    label "oficer"
  ]
  node [
    id 481
    label "podchor&#261;&#380;y"
  ]
  node [
    id 482
    label "podoficer"
  ]
  node [
    id 483
    label "mundurowy"
  ]
  node [
    id 484
    label "mandatariusz"
  ]
  node [
    id 485
    label "grupa_bilateralna"
  ]
  node [
    id 486
    label "polityk"
  ]
  node [
    id 487
    label "parlament"
  ]
  node [
    id 488
    label "notabl"
  ]
  node [
    id 489
    label "oficja&#322;"
  ]
  node [
    id 490
    label "Komendant"
  ]
  node [
    id 491
    label "kasztanka"
  ]
  node [
    id 492
    label "wyrafinowany"
  ]
  node [
    id 493
    label "niepo&#347;ledni"
  ]
  node [
    id 494
    label "chwalebny"
  ]
  node [
    id 495
    label "z_wysoka"
  ]
  node [
    id 496
    label "wznios&#322;y"
  ]
  node [
    id 497
    label "daleki"
  ]
  node [
    id 498
    label "wysoce"
  ]
  node [
    id 499
    label "szczytnie"
  ]
  node [
    id 500
    label "znaczny"
  ]
  node [
    id 501
    label "warto&#347;ciowy"
  ]
  node [
    id 502
    label "wysoko"
  ]
  node [
    id 503
    label "uprzywilejowany"
  ]
  node [
    id 504
    label "niema&#322;o"
  ]
  node [
    id 505
    label "wiele"
  ]
  node [
    id 506
    label "rozwini&#281;ty"
  ]
  node [
    id 507
    label "dorodny"
  ]
  node [
    id 508
    label "wa&#380;ny"
  ]
  node [
    id 509
    label "prawdziwy"
  ]
  node [
    id 510
    label "du&#380;o"
  ]
  node [
    id 511
    label "znacznie"
  ]
  node [
    id 512
    label "zauwa&#380;alny"
  ]
  node [
    id 513
    label "szczeg&#243;lny"
  ]
  node [
    id 514
    label "lekki"
  ]
  node [
    id 515
    label "wyr&#243;&#380;niony"
  ]
  node [
    id 516
    label "niez&#322;y"
  ]
  node [
    id 517
    label "niepo&#347;lednio"
  ]
  node [
    id 518
    label "wyj&#261;tkowy"
  ]
  node [
    id 519
    label "szlachetny"
  ]
  node [
    id 520
    label "powa&#380;ny"
  ]
  node [
    id 521
    label "podnios&#322;y"
  ]
  node [
    id 522
    label "wznio&#347;le"
  ]
  node [
    id 523
    label "oderwany"
  ]
  node [
    id 524
    label "pi&#281;kny"
  ]
  node [
    id 525
    label "pochwalny"
  ]
  node [
    id 526
    label "wspania&#322;y"
  ]
  node [
    id 527
    label "chwalebnie"
  ]
  node [
    id 528
    label "obyty"
  ]
  node [
    id 529
    label "wykwintny"
  ]
  node [
    id 530
    label "wyrafinowanie"
  ]
  node [
    id 531
    label "wymy&#347;lny"
  ]
  node [
    id 532
    label "rewaluowanie"
  ]
  node [
    id 533
    label "warto&#347;ciowo"
  ]
  node [
    id 534
    label "drogi"
  ]
  node [
    id 535
    label "u&#380;yteczny"
  ]
  node [
    id 536
    label "zrewaluowanie"
  ]
  node [
    id 537
    label "dobry"
  ]
  node [
    id 538
    label "dawny"
  ]
  node [
    id 539
    label "ogl&#281;dny"
  ]
  node [
    id 540
    label "d&#322;ugi"
  ]
  node [
    id 541
    label "daleko"
  ]
  node [
    id 542
    label "odleg&#322;y"
  ]
  node [
    id 543
    label "zwi&#261;zany"
  ]
  node [
    id 544
    label "r&#243;&#380;ny"
  ]
  node [
    id 545
    label "s&#322;aby"
  ]
  node [
    id 546
    label "odlegle"
  ]
  node [
    id 547
    label "oddalony"
  ]
  node [
    id 548
    label "g&#322;&#281;boki"
  ]
  node [
    id 549
    label "obcy"
  ]
  node [
    id 550
    label "nieobecny"
  ]
  node [
    id 551
    label "przysz&#322;y"
  ]
  node [
    id 552
    label "g&#243;rno"
  ]
  node [
    id 553
    label "szczytny"
  ]
  node [
    id 554
    label "intensywnie"
  ]
  node [
    id 555
    label "wielki"
  ]
  node [
    id 556
    label "niezmiernie"
  ]
  node [
    id 557
    label "NIK"
  ]
  node [
    id 558
    label "urz&#261;d"
  ]
  node [
    id 559
    label "organ"
  ]
  node [
    id 560
    label "pok&#243;j"
  ]
  node [
    id 561
    label "pomieszczenie"
  ]
  node [
    id 562
    label "mir"
  ]
  node [
    id 563
    label "uk&#322;ad"
  ]
  node [
    id 564
    label "pacyfista"
  ]
  node [
    id 565
    label "preliminarium_pokojowe"
  ]
  node [
    id 566
    label "spok&#243;j"
  ]
  node [
    id 567
    label "tkanka"
  ]
  node [
    id 568
    label "jednostka_organizacyjna"
  ]
  node [
    id 569
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 570
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 571
    label "tw&#243;r"
  ]
  node [
    id 572
    label "organogeneza"
  ]
  node [
    id 573
    label "zesp&#243;&#322;"
  ]
  node [
    id 574
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 575
    label "struktura_anatomiczna"
  ]
  node [
    id 576
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 577
    label "dekortykacja"
  ]
  node [
    id 578
    label "Izba_Konsyliarska"
  ]
  node [
    id 579
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 580
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 581
    label "stomia"
  ]
  node [
    id 582
    label "budowa"
  ]
  node [
    id 583
    label "okolica"
  ]
  node [
    id 584
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 585
    label "Komitet_Region&#243;w"
  ]
  node [
    id 586
    label "odwadnia&#263;"
  ]
  node [
    id 587
    label "wi&#261;zanie"
  ]
  node [
    id 588
    label "odwodni&#263;"
  ]
  node [
    id 589
    label "bratnia_dusza"
  ]
  node [
    id 590
    label "powi&#261;zanie"
  ]
  node [
    id 591
    label "zwi&#261;zanie"
  ]
  node [
    id 592
    label "konstytucja"
  ]
  node [
    id 593
    label "marriage"
  ]
  node [
    id 594
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 595
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 596
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 597
    label "zwi&#261;za&#263;"
  ]
  node [
    id 598
    label "odwadnianie"
  ]
  node [
    id 599
    label "odwodnienie"
  ]
  node [
    id 600
    label "marketing_afiliacyjny"
  ]
  node [
    id 601
    label "substancja_chemiczna"
  ]
  node [
    id 602
    label "koligacja"
  ]
  node [
    id 603
    label "bearing"
  ]
  node [
    id 604
    label "lokant"
  ]
  node [
    id 605
    label "azeotrop"
  ]
  node [
    id 606
    label "stanowisko"
  ]
  node [
    id 607
    label "position"
  ]
  node [
    id 608
    label "instytucja"
  ]
  node [
    id 609
    label "siedziba"
  ]
  node [
    id 610
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 611
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 612
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 613
    label "mianowaniec"
  ]
  node [
    id 614
    label "dzia&#322;"
  ]
  node [
    id 615
    label "okienko"
  ]
  node [
    id 616
    label "amfilada"
  ]
  node [
    id 617
    label "front"
  ]
  node [
    id 618
    label "apartment"
  ]
  node [
    id 619
    label "pod&#322;oga"
  ]
  node [
    id 620
    label "udost&#281;pnienie"
  ]
  node [
    id 621
    label "miejsce"
  ]
  node [
    id 622
    label "sklepienie"
  ]
  node [
    id 623
    label "sufit"
  ]
  node [
    id 624
    label "umieszczenie"
  ]
  node [
    id 625
    label "zakamarek"
  ]
  node [
    id 626
    label "europarlament"
  ]
  node [
    id 627
    label "plankton_polityczny"
  ]
  node [
    id 628
    label "ustawodawca"
  ]
  node [
    id 629
    label "s&#261;d"
  ]
  node [
    id 630
    label "Europejski_Trybuna&#322;_Praw_Cz&#322;owieka"
  ]
  node [
    id 631
    label "podejrzany"
  ]
  node [
    id 632
    label "s&#261;downictwo"
  ]
  node [
    id 633
    label "system"
  ]
  node [
    id 634
    label "biuro"
  ]
  node [
    id 635
    label "wytw&#243;r"
  ]
  node [
    id 636
    label "court"
  ]
  node [
    id 637
    label "forum"
  ]
  node [
    id 638
    label "bronienie"
  ]
  node [
    id 639
    label "wydarzenie"
  ]
  node [
    id 640
    label "oskar&#380;yciel"
  ]
  node [
    id 641
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 642
    label "skazany"
  ]
  node [
    id 643
    label "post&#281;powanie"
  ]
  node [
    id 644
    label "broni&#263;"
  ]
  node [
    id 645
    label "my&#347;l"
  ]
  node [
    id 646
    label "pods&#261;dny"
  ]
  node [
    id 647
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 648
    label "obrona"
  ]
  node [
    id 649
    label "wypowied&#378;"
  ]
  node [
    id 650
    label "antylogizm"
  ]
  node [
    id 651
    label "konektyw"
  ]
  node [
    id 652
    label "&#347;wiadek"
  ]
  node [
    id 653
    label "procesowicz"
  ]
  node [
    id 654
    label "strona"
  ]
  node [
    id 655
    label "konstytucyjnie"
  ]
  node [
    id 656
    label "regulaminowy"
  ]
  node [
    id 657
    label "constitutionally"
  ]
  node [
    id 658
    label "zgodnie"
  ]
  node [
    id 659
    label "sentencja"
  ]
  node [
    id 660
    label "kara"
  ]
  node [
    id 661
    label "orzeczenie"
  ]
  node [
    id 662
    label "judgment"
  ]
  node [
    id 663
    label "order"
  ]
  node [
    id 664
    label "przebiec"
  ]
  node [
    id 665
    label "charakter"
  ]
  node [
    id 666
    label "czynno&#347;&#263;"
  ]
  node [
    id 667
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 668
    label "motyw"
  ]
  node [
    id 669
    label "przebiegni&#281;cie"
  ]
  node [
    id 670
    label "fabu&#322;a"
  ]
  node [
    id 671
    label "kwota"
  ]
  node [
    id 672
    label "nemezis"
  ]
  node [
    id 673
    label "konsekwencja"
  ]
  node [
    id 674
    label "punishment"
  ]
  node [
    id 675
    label "klacz"
  ]
  node [
    id 676
    label "forfeit"
  ]
  node [
    id 677
    label "roboty_przymusowe"
  ]
  node [
    id 678
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 679
    label "predykatywno&#347;&#263;"
  ]
  node [
    id 680
    label "decyzja"
  ]
  node [
    id 681
    label "odznaka"
  ]
  node [
    id 682
    label "kawaler"
  ]
  node [
    id 683
    label "powiedzenie"
  ]
  node [
    id 684
    label "rubrum"
  ]
  node [
    id 685
    label "miesi&#261;c"
  ]
  node [
    id 686
    label "tydzie&#324;"
  ]
  node [
    id 687
    label "miech"
  ]
  node [
    id 688
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 689
    label "czas"
  ]
  node [
    id 690
    label "rok"
  ]
  node [
    id 691
    label "kalendy"
  ]
  node [
    id 692
    label "formacja"
  ]
  node [
    id 693
    label "yearbook"
  ]
  node [
    id 694
    label "czasopismo"
  ]
  node [
    id 695
    label "kronika"
  ]
  node [
    id 696
    label "Bund"
  ]
  node [
    id 697
    label "Mazowsze"
  ]
  node [
    id 698
    label "PPR"
  ]
  node [
    id 699
    label "Jakobici"
  ]
  node [
    id 700
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 701
    label "leksem"
  ]
  node [
    id 702
    label "SLD"
  ]
  node [
    id 703
    label "zespolik"
  ]
  node [
    id 704
    label "Razem"
  ]
  node [
    id 705
    label "PiS"
  ]
  node [
    id 706
    label "zjawisko"
  ]
  node [
    id 707
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 708
    label "Kuomintang"
  ]
  node [
    id 709
    label "ZSL"
  ]
  node [
    id 710
    label "jednostka"
  ]
  node [
    id 711
    label "proces"
  ]
  node [
    id 712
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 713
    label "rugby"
  ]
  node [
    id 714
    label "AWS"
  ]
  node [
    id 715
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 716
    label "blok"
  ]
  node [
    id 717
    label "PO"
  ]
  node [
    id 718
    label "si&#322;a"
  ]
  node [
    id 719
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 720
    label "Federali&#347;ci"
  ]
  node [
    id 721
    label "PSL"
  ]
  node [
    id 722
    label "wojsko"
  ]
  node [
    id 723
    label "Wigowie"
  ]
  node [
    id 724
    label "ZChN"
  ]
  node [
    id 725
    label "egzekutywa"
  ]
  node [
    id 726
    label "The_Beatles"
  ]
  node [
    id 727
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 728
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 729
    label "unit"
  ]
  node [
    id 730
    label "Depeche_Mode"
  ]
  node [
    id 731
    label "forma"
  ]
  node [
    id 732
    label "zapis"
  ]
  node [
    id 733
    label "chronograf"
  ]
  node [
    id 734
    label "latopis"
  ]
  node [
    id 735
    label "ksi&#281;ga"
  ]
  node [
    id 736
    label "egzemplarz"
  ]
  node [
    id 737
    label "psychotest"
  ]
  node [
    id 738
    label "pismo"
  ]
  node [
    id 739
    label "communication"
  ]
  node [
    id 740
    label "wk&#322;ad"
  ]
  node [
    id 741
    label "zajawka"
  ]
  node [
    id 742
    label "ok&#322;adka"
  ]
  node [
    id 743
    label "Zwrotnica"
  ]
  node [
    id 744
    label "prasa"
  ]
  node [
    id 745
    label "testify"
  ]
  node [
    id 746
    label "powiedzie&#263;"
  ]
  node [
    id 747
    label "uzna&#263;"
  ]
  node [
    id 748
    label "oznajmi&#263;"
  ]
  node [
    id 749
    label "declare"
  ]
  node [
    id 750
    label "oceni&#263;"
  ]
  node [
    id 751
    label "przyzna&#263;"
  ]
  node [
    id 752
    label "assent"
  ]
  node [
    id 753
    label "rede"
  ]
  node [
    id 754
    label "see"
  ]
  node [
    id 755
    label "discover"
  ]
  node [
    id 756
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 757
    label "wydoby&#263;"
  ]
  node [
    id 758
    label "okre&#347;li&#263;"
  ]
  node [
    id 759
    label "poda&#263;"
  ]
  node [
    id 760
    label "express"
  ]
  node [
    id 761
    label "wyrazi&#263;"
  ]
  node [
    id 762
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 763
    label "ozwa&#263;_si&#281;"
  ]
  node [
    id 764
    label "rzekn&#261;&#263;"
  ]
  node [
    id 765
    label "unwrap"
  ]
  node [
    id 766
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 767
    label "convey"
  ]
  node [
    id 768
    label "poinformowa&#263;"
  ]
  node [
    id 769
    label "advise"
  ]
  node [
    id 770
    label "kilometr_kwadratowy"
  ]
  node [
    id 771
    label "centymetr_kwadratowy"
  ]
  node [
    id 772
    label "dekametr"
  ]
  node [
    id 773
    label "gigametr"
  ]
  node [
    id 774
    label "plon"
  ]
  node [
    id 775
    label "meter"
  ]
  node [
    id 776
    label "miara"
  ]
  node [
    id 777
    label "uk&#322;ad_SI"
  ]
  node [
    id 778
    label "wiersz"
  ]
  node [
    id 779
    label "jednostka_metryczna"
  ]
  node [
    id 780
    label "metrum"
  ]
  node [
    id 781
    label "decymetr"
  ]
  node [
    id 782
    label "megabyte"
  ]
  node [
    id 783
    label "literaturoznawstwo"
  ]
  node [
    id 784
    label "jednostka_powierzchni"
  ]
  node [
    id 785
    label "jednostka_masy"
  ]
  node [
    id 786
    label "proportion"
  ]
  node [
    id 787
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 788
    label "wielko&#347;&#263;"
  ]
  node [
    id 789
    label "poj&#281;cie"
  ]
  node [
    id 790
    label "continence"
  ]
  node [
    id 791
    label "supremum"
  ]
  node [
    id 792
    label "cecha"
  ]
  node [
    id 793
    label "skala"
  ]
  node [
    id 794
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 795
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 796
    label "przeliczy&#263;"
  ]
  node [
    id 797
    label "matematyka"
  ]
  node [
    id 798
    label "rzut"
  ]
  node [
    id 799
    label "odwiedziny"
  ]
  node [
    id 800
    label "liczba"
  ]
  node [
    id 801
    label "granica"
  ]
  node [
    id 802
    label "zakres"
  ]
  node [
    id 803
    label "warunek_lokalowy"
  ]
  node [
    id 804
    label "ilo&#347;&#263;"
  ]
  node [
    id 805
    label "przeliczanie"
  ]
  node [
    id 806
    label "dymensja"
  ]
  node [
    id 807
    label "funkcja"
  ]
  node [
    id 808
    label "przelicza&#263;"
  ]
  node [
    id 809
    label "infimum"
  ]
  node [
    id 810
    label "przeliczenie"
  ]
  node [
    id 811
    label "struktura"
  ]
  node [
    id 812
    label "standard"
  ]
  node [
    id 813
    label "rytm"
  ]
  node [
    id 814
    label "rytmika"
  ]
  node [
    id 815
    label "centymetr"
  ]
  node [
    id 816
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 817
    label "hektometr"
  ]
  node [
    id 818
    label "return"
  ]
  node [
    id 819
    label "wydawa&#263;"
  ]
  node [
    id 820
    label "wyda&#263;"
  ]
  node [
    id 821
    label "rezultat"
  ]
  node [
    id 822
    label "produkcja"
  ]
  node [
    id 823
    label "naturalia"
  ]
  node [
    id 824
    label "strofoida"
  ]
  node [
    id 825
    label "figura_stylistyczna"
  ]
  node [
    id 826
    label "podmiot_liryczny"
  ]
  node [
    id 827
    label "cezura"
  ]
  node [
    id 828
    label "zwrotka"
  ]
  node [
    id 829
    label "fragment"
  ]
  node [
    id 830
    label "refren"
  ]
  node [
    id 831
    label "tekst"
  ]
  node [
    id 832
    label "dzie&#322;o_poetyckie"
  ]
  node [
    id 833
    label "nauka_humanistyczna"
  ]
  node [
    id 834
    label "teoria_literatury"
  ]
  node [
    id 835
    label "historia_literatury"
  ]
  node [
    id 836
    label "s&#322;owo_kluczowe"
  ]
  node [
    id 837
    label "komparatystyka"
  ]
  node [
    id 838
    label "literature"
  ]
  node [
    id 839
    label "stylistyka"
  ]
  node [
    id 840
    label "krytyka_literacka"
  ]
  node [
    id 841
    label "shot"
  ]
  node [
    id 842
    label "jednakowy"
  ]
  node [
    id 843
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 844
    label "ujednolicenie"
  ]
  node [
    id 845
    label "jaki&#347;"
  ]
  node [
    id 846
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 847
    label "jednolicie"
  ]
  node [
    id 848
    label "kieliszek"
  ]
  node [
    id 849
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 850
    label "w&#243;dka"
  ]
  node [
    id 851
    label "ten"
  ]
  node [
    id 852
    label "szk&#322;o"
  ]
  node [
    id 853
    label "zawarto&#347;&#263;"
  ]
  node [
    id 854
    label "naczynie"
  ]
  node [
    id 855
    label "alkohol"
  ]
  node [
    id 856
    label "sznaps"
  ]
  node [
    id 857
    label "nap&#243;j"
  ]
  node [
    id 858
    label "gorza&#322;ka"
  ]
  node [
    id 859
    label "mohorycz"
  ]
  node [
    id 860
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 861
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 862
    label "zr&#243;wnanie"
  ]
  node [
    id 863
    label "mundurowanie"
  ]
  node [
    id 864
    label "taki&#380;"
  ]
  node [
    id 865
    label "jednakowo"
  ]
  node [
    id 866
    label "mundurowa&#263;"
  ]
  node [
    id 867
    label "zr&#243;wnywanie"
  ]
  node [
    id 868
    label "identyczny"
  ]
  node [
    id 869
    label "z&#322;o&#380;ony"
  ]
  node [
    id 870
    label "przyzwoity"
  ]
  node [
    id 871
    label "ciekawy"
  ]
  node [
    id 872
    label "jako&#347;"
  ]
  node [
    id 873
    label "jako_tako"
  ]
  node [
    id 874
    label "dziwny"
  ]
  node [
    id 875
    label "charakterystyczny"
  ]
  node [
    id 876
    label "g&#322;&#281;bszy"
  ]
  node [
    id 877
    label "drink"
  ]
  node [
    id 878
    label "upodobnienie"
  ]
  node [
    id 879
    label "jednolity"
  ]
  node [
    id 880
    label "calibration"
  ]
  node [
    id 881
    label "niezaawansowany"
  ]
  node [
    id 882
    label "najwa&#380;niejszy"
  ]
  node [
    id 883
    label "pocz&#261;tkowy"
  ]
  node [
    id 884
    label "podstawowo"
  ]
  node [
    id 885
    label "dzieci&#281;cy"
  ]
  node [
    id 886
    label "pierwszy"
  ]
  node [
    id 887
    label "elementarny"
  ]
  node [
    id 888
    label "pocz&#261;tkowo"
  ]
  node [
    id 889
    label "fakt"
  ]
  node [
    id 890
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 891
    label "przyczyna"
  ]
  node [
    id 892
    label "wnioskowanie"
  ]
  node [
    id 893
    label "sk&#322;adnik"
  ]
  node [
    id 894
    label "warunki"
  ]
  node [
    id 895
    label "bia&#322;e_plamy"
  ]
  node [
    id 896
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 897
    label "subject"
  ]
  node [
    id 898
    label "czynnik"
  ]
  node [
    id 899
    label "matuszka"
  ]
  node [
    id 900
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 901
    label "geneza"
  ]
  node [
    id 902
    label "poci&#261;ganie"
  ]
  node [
    id 903
    label "proszenie"
  ]
  node [
    id 904
    label "dochodzenie"
  ]
  node [
    id 905
    label "proces_my&#347;lowy"
  ]
  node [
    id 906
    label "lead"
  ]
  node [
    id 907
    label "konkluzja"
  ]
  node [
    id 908
    label "sk&#322;adanie"
  ]
  node [
    id 909
    label "wniosek"
  ]
  node [
    id 910
    label "kognicja"
  ]
  node [
    id 911
    label "przebieg"
  ]
  node [
    id 912
    label "rozprawa"
  ]
  node [
    id 913
    label "legislacyjnie"
  ]
  node [
    id 914
    label "nast&#281;pstwo"
  ]
  node [
    id 915
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 916
    label "uzupe&#322;nienie"
  ]
  node [
    id 917
    label "woof"
  ]
  node [
    id 918
    label "activity"
  ]
  node [
    id 919
    label "spowodowanie"
  ]
  node [
    id 920
    label "control"
  ]
  node [
    id 921
    label "pe&#322;ny"
  ]
  node [
    id 922
    label "zdarzenie_si&#281;"
  ]
  node [
    id 923
    label "znalezienie_si&#281;"
  ]
  node [
    id 924
    label "element"
  ]
  node [
    id 925
    label "completion"
  ]
  node [
    id 926
    label "bash"
  ]
  node [
    id 927
    label "rubryka"
  ]
  node [
    id 928
    label "ziszczenie_si&#281;"
  ]
  node [
    id 929
    label "nasilenie_si&#281;"
  ]
  node [
    id 930
    label "poczucie"
  ]
  node [
    id 931
    label "performance"
  ]
  node [
    id 932
    label "zrobienie"
  ]
  node [
    id 933
    label "summation"
  ]
  node [
    id 934
    label "doj&#347;cie"
  ]
  node [
    id 935
    label "attachment"
  ]
  node [
    id 936
    label "dodanie"
  ]
  node [
    id 937
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 938
    label "rzecz"
  ]
  node [
    id 939
    label "doj&#347;&#263;"
  ]
  node [
    id 940
    label "narobienie"
  ]
  node [
    id 941
    label "zm&#281;czenie_si&#281;"
  ]
  node [
    id 942
    label "creation"
  ]
  node [
    id 943
    label "porobienie"
  ]
  node [
    id 944
    label "campaign"
  ]
  node [
    id 945
    label "causing"
  ]
  node [
    id 946
    label "poumieszczanie"
  ]
  node [
    id 947
    label "ustalenie"
  ]
  node [
    id 948
    label "uplasowanie"
  ]
  node [
    id 949
    label "ulokowanie_si&#281;"
  ]
  node [
    id 950
    label "prze&#322;adowanie"
  ]
  node [
    id 951
    label "layout"
  ]
  node [
    id 952
    label "siedzenie"
  ]
  node [
    id 953
    label "zakrycie"
  ]
  node [
    id 954
    label "r&#243;&#380;niczka"
  ]
  node [
    id 955
    label "&#347;rodowisko"
  ]
  node [
    id 956
    label "przedmiot"
  ]
  node [
    id 957
    label "materia"
  ]
  node [
    id 958
    label "szambo"
  ]
  node [
    id 959
    label "aspo&#322;eczny"
  ]
  node [
    id 960
    label "component"
  ]
  node [
    id 961
    label "szkodnik"
  ]
  node [
    id 962
    label "gangsterski"
  ]
  node [
    id 963
    label "underworld"
  ]
  node [
    id 964
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 965
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 966
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 967
    label "sztuka_wsp&#243;&#322;czesna"
  ]
  node [
    id 968
    label "przedstawienie"
  ]
  node [
    id 969
    label "zachowanie"
  ]
  node [
    id 970
    label "klawisz"
  ]
  node [
    id 971
    label "tabela"
  ]
  node [
    id 972
    label "pozycja"
  ]
  node [
    id 973
    label "wype&#322;nianie"
  ]
  node [
    id 974
    label "heading"
  ]
  node [
    id 975
    label "artyku&#322;"
  ]
  node [
    id 976
    label "ekstraspekcja"
  ]
  node [
    id 977
    label "domy&#347;lenie_si&#281;"
  ]
  node [
    id 978
    label "feeling"
  ]
  node [
    id 979
    label "doznanie"
  ]
  node [
    id 980
    label "wiedza"
  ]
  node [
    id 981
    label "smell"
  ]
  node [
    id 982
    label "dowiedzenie_si&#281;"
  ]
  node [
    id 983
    label "opanowanie"
  ]
  node [
    id 984
    label "os&#322;upienie"
  ]
  node [
    id 985
    label "zareagowanie"
  ]
  node [
    id 986
    label "intuition"
  ]
  node [
    id 987
    label "nieograniczony"
  ]
  node [
    id 988
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 989
    label "satysfakcja"
  ]
  node [
    id 990
    label "bezwzgl&#281;dny"
  ]
  node [
    id 991
    label "ca&#322;y"
  ]
  node [
    id 992
    label "otwarty"
  ]
  node [
    id 993
    label "kompletny"
  ]
  node [
    id 994
    label "pe&#322;no"
  ]
  node [
    id 995
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 996
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 997
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 998
    label "zupe&#322;ny"
  ]
  node [
    id 999
    label "r&#243;wny"
  ]
  node [
    id 1000
    label "regulaminowo"
  ]
  node [
    id 1001
    label "legalnie"
  ]
  node [
    id 1002
    label "wiadomy"
  ]
  node [
    id 1003
    label "konkretny"
  ]
  node [
    id 1004
    label "znany"
  ]
  node [
    id 1005
    label "wiadomie"
  ]
  node [
    id 1006
    label "obowi&#261;zek"
  ]
  node [
    id 1007
    label "plac&#243;wka"
  ]
  node [
    id 1008
    label "zadanie"
  ]
  node [
    id 1009
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 1010
    label "reprezentacja"
  ]
  node [
    id 1011
    label "misje"
  ]
  node [
    id 1012
    label "absolutorium"
  ]
  node [
    id 1013
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 1014
    label "dzia&#322;anie"
  ]
  node [
    id 1015
    label "zaj&#281;cie"
  ]
  node [
    id 1016
    label "yield"
  ]
  node [
    id 1017
    label "zbi&#243;r"
  ]
  node [
    id 1018
    label "zaszkodzenie"
  ]
  node [
    id 1019
    label "za&#322;o&#380;enie"
  ]
  node [
    id 1020
    label "duty"
  ]
  node [
    id 1021
    label "powierzanie"
  ]
  node [
    id 1022
    label "work"
  ]
  node [
    id 1023
    label "problem"
  ]
  node [
    id 1024
    label "przepisanie"
  ]
  node [
    id 1025
    label "nakarmienie"
  ]
  node [
    id 1026
    label "przepisa&#263;"
  ]
  node [
    id 1027
    label "d&#378;wigni&#281;cie"
  ]
  node [
    id 1028
    label "zobowi&#261;zanie"
  ]
  node [
    id 1029
    label "wym&#243;g"
  ]
  node [
    id 1030
    label "obarczy&#263;"
  ]
  node [
    id 1031
    label "powinno&#347;&#263;"
  ]
  node [
    id 1032
    label "dru&#380;yna"
  ]
  node [
    id 1033
    label "emblemat"
  ]
  node [
    id 1034
    label "deputation"
  ]
  node [
    id 1035
    label "agencja"
  ]
  node [
    id 1036
    label "sie&#263;"
  ]
  node [
    id 1037
    label "rekolekcje"
  ]
  node [
    id 1038
    label "mass-media"
  ]
  node [
    id 1039
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 1040
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 1041
    label "przekazior"
  ]
  node [
    id 1042
    label "uzbrajanie"
  ]
  node [
    id 1043
    label "medium"
  ]
  node [
    id 1044
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 1045
    label "jasnowidz"
  ]
  node [
    id 1046
    label "hipnoza"
  ]
  node [
    id 1047
    label "spirytysta"
  ]
  node [
    id 1048
    label "otoczenie"
  ]
  node [
    id 1049
    label "publikator"
  ]
  node [
    id 1050
    label "przeka&#378;nik"
  ]
  node [
    id 1051
    label "&#347;rodek_przekazu"
  ]
  node [
    id 1052
    label "armament"
  ]
  node [
    id 1053
    label "arming"
  ]
  node [
    id 1054
    label "instalacja"
  ]
  node [
    id 1055
    label "wyposa&#380;anie"
  ]
  node [
    id 1056
    label "dozbrajanie"
  ]
  node [
    id 1057
    label "dozbrojenie"
  ]
  node [
    id 1058
    label "montowanie"
  ]
  node [
    id 1059
    label "upublicznianie"
  ]
  node [
    id 1060
    label "jawny"
  ]
  node [
    id 1061
    label "upublicznienie"
  ]
  node [
    id 1062
    label "publicznie"
  ]
  node [
    id 1063
    label "jawnie"
  ]
  node [
    id 1064
    label "udost&#281;pnianie"
  ]
  node [
    id 1065
    label "ujawnienie_si&#281;"
  ]
  node [
    id 1066
    label "ujawnianie_si&#281;"
  ]
  node [
    id 1067
    label "zdecydowany"
  ]
  node [
    id 1068
    label "znajomy"
  ]
  node [
    id 1069
    label "ods&#322;oni&#281;cie_si&#281;"
  ]
  node [
    id 1070
    label "ujawnienie"
  ]
  node [
    id 1071
    label "ods&#322;anianie_si&#281;"
  ]
  node [
    id 1072
    label "ujawnianie"
  ]
  node [
    id 1073
    label "ewidentny"
  ]
  node [
    id 1074
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 1075
    label "mie&#263;_miejsce"
  ]
  node [
    id 1076
    label "equal"
  ]
  node [
    id 1077
    label "trwa&#263;"
  ]
  node [
    id 1078
    label "chodzi&#263;"
  ]
  node [
    id 1079
    label "si&#281;ga&#263;"
  ]
  node [
    id 1080
    label "stan"
  ]
  node [
    id 1081
    label "obecno&#347;&#263;"
  ]
  node [
    id 1082
    label "stand"
  ]
  node [
    id 1083
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 1084
    label "uczestniczy&#263;"
  ]
  node [
    id 1085
    label "participate"
  ]
  node [
    id 1086
    label "robi&#263;"
  ]
  node [
    id 1087
    label "istnie&#263;"
  ]
  node [
    id 1088
    label "pozostawa&#263;"
  ]
  node [
    id 1089
    label "zostawa&#263;"
  ]
  node [
    id 1090
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 1091
    label "adhere"
  ]
  node [
    id 1092
    label "compass"
  ]
  node [
    id 1093
    label "korzysta&#263;"
  ]
  node [
    id 1094
    label "appreciation"
  ]
  node [
    id 1095
    label "osi&#261;ga&#263;"
  ]
  node [
    id 1096
    label "dociera&#263;"
  ]
  node [
    id 1097
    label "get"
  ]
  node [
    id 1098
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 1099
    label "mierzy&#263;"
  ]
  node [
    id 1100
    label "u&#380;ywa&#263;"
  ]
  node [
    id 1101
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 1102
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 1103
    label "exsert"
  ]
  node [
    id 1104
    label "being"
  ]
  node [
    id 1105
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 1106
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 1107
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 1108
    label "p&#322;ywa&#263;"
  ]
  node [
    id 1109
    label "run"
  ]
  node [
    id 1110
    label "bangla&#263;"
  ]
  node [
    id 1111
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 1112
    label "przebiega&#263;"
  ]
  node [
    id 1113
    label "wk&#322;ada&#263;"
  ]
  node [
    id 1114
    label "proceed"
  ]
  node [
    id 1115
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 1116
    label "carry"
  ]
  node [
    id 1117
    label "bywa&#263;"
  ]
  node [
    id 1118
    label "dziama&#263;"
  ]
  node [
    id 1119
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 1120
    label "stara&#263;_si&#281;"
  ]
  node [
    id 1121
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 1122
    label "str&#243;j"
  ]
  node [
    id 1123
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 1124
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 1125
    label "krok"
  ]
  node [
    id 1126
    label "tryb"
  ]
  node [
    id 1127
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 1128
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 1129
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 1130
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 1131
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 1132
    label "continue"
  ]
  node [
    id 1133
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 1134
    label "Ohio"
  ]
  node [
    id 1135
    label "wci&#281;cie"
  ]
  node [
    id 1136
    label "Nowy_York"
  ]
  node [
    id 1137
    label "warstwa"
  ]
  node [
    id 1138
    label "samopoczucie"
  ]
  node [
    id 1139
    label "Illinois"
  ]
  node [
    id 1140
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 1141
    label "state"
  ]
  node [
    id 1142
    label "Jukatan"
  ]
  node [
    id 1143
    label "Kalifornia"
  ]
  node [
    id 1144
    label "Wirginia"
  ]
  node [
    id 1145
    label "wektor"
  ]
  node [
    id 1146
    label "Goa"
  ]
  node [
    id 1147
    label "Teksas"
  ]
  node [
    id 1148
    label "Waszyngton"
  ]
  node [
    id 1149
    label "Massachusetts"
  ]
  node [
    id 1150
    label "Alaska"
  ]
  node [
    id 1151
    label "Arakan"
  ]
  node [
    id 1152
    label "Hawaje"
  ]
  node [
    id 1153
    label "Maryland"
  ]
  node [
    id 1154
    label "Michigan"
  ]
  node [
    id 1155
    label "Arizona"
  ]
  node [
    id 1156
    label "Georgia"
  ]
  node [
    id 1157
    label "Pensylwania"
  ]
  node [
    id 1158
    label "shape"
  ]
  node [
    id 1159
    label "Luizjana"
  ]
  node [
    id 1160
    label "Nowy_Meksyk"
  ]
  node [
    id 1161
    label "Alabama"
  ]
  node [
    id 1162
    label "Kansas"
  ]
  node [
    id 1163
    label "Oregon"
  ]
  node [
    id 1164
    label "Oklahoma"
  ]
  node [
    id 1165
    label "Floryda"
  ]
  node [
    id 1166
    label "jednostka_administracyjna"
  ]
  node [
    id 1167
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 1168
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 1169
    label "nale&#380;ny"
  ]
  node [
    id 1170
    label "nale&#380;yty"
  ]
  node [
    id 1171
    label "typowy"
  ]
  node [
    id 1172
    label "uprawniony"
  ]
  node [
    id 1173
    label "zasadniczy"
  ]
  node [
    id 1174
    label "stosownie"
  ]
  node [
    id 1175
    label "taki"
  ]
  node [
    id 1176
    label "powinny"
  ]
  node [
    id 1177
    label "nale&#380;nie"
  ]
  node [
    id 1178
    label "godny"
  ]
  node [
    id 1179
    label "przynale&#380;ny"
  ]
  node [
    id 1180
    label "zwyczajny"
  ]
  node [
    id 1181
    label "typowo"
  ]
  node [
    id 1182
    label "cz&#281;sty"
  ]
  node [
    id 1183
    label "zwyk&#322;y"
  ]
  node [
    id 1184
    label "charakterystycznie"
  ]
  node [
    id 1185
    label "wyr&#243;&#380;niaj&#261;cy_si&#281;"
  ]
  node [
    id 1186
    label "podobny"
  ]
  node [
    id 1187
    label "&#380;ywny"
  ]
  node [
    id 1188
    label "szczery"
  ]
  node [
    id 1189
    label "naturalny"
  ]
  node [
    id 1190
    label "naprawd&#281;"
  ]
  node [
    id 1191
    label "realnie"
  ]
  node [
    id 1192
    label "zgodny"
  ]
  node [
    id 1193
    label "prawdziwie"
  ]
  node [
    id 1194
    label "w&#322;ady"
  ]
  node [
    id 1195
    label "g&#322;&#243;wny"
  ]
  node [
    id 1196
    label "og&#243;lny"
  ]
  node [
    id 1197
    label "zasadniczo"
  ]
  node [
    id 1198
    label "surowy"
  ]
  node [
    id 1199
    label "zadowalaj&#261;cy"
  ]
  node [
    id 1200
    label "nale&#380;ycie"
  ]
  node [
    id 1201
    label "przystojny"
  ]
  node [
    id 1202
    label "dobroczynny"
  ]
  node [
    id 1203
    label "czw&#243;rka"
  ]
  node [
    id 1204
    label "spokojny"
  ]
  node [
    id 1205
    label "skuteczny"
  ]
  node [
    id 1206
    label "&#347;mieszny"
  ]
  node [
    id 1207
    label "mi&#322;y"
  ]
  node [
    id 1208
    label "grzeczny"
  ]
  node [
    id 1209
    label "powitanie"
  ]
  node [
    id 1210
    label "dobrze"
  ]
  node [
    id 1211
    label "pomy&#347;lny"
  ]
  node [
    id 1212
    label "moralny"
  ]
  node [
    id 1213
    label "pozytywny"
  ]
  node [
    id 1214
    label "odpowiedni"
  ]
  node [
    id 1215
    label "korzystny"
  ]
  node [
    id 1216
    label "pos&#322;uszny"
  ]
  node [
    id 1217
    label "stosowny"
  ]
  node [
    id 1218
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 1219
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 1220
    label "regu&#322;a_Allena"
  ]
  node [
    id 1221
    label "base"
  ]
  node [
    id 1222
    label "umowa"
  ]
  node [
    id 1223
    label "obserwacja"
  ]
  node [
    id 1224
    label "zasada_d'Alemberta"
  ]
  node [
    id 1225
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1226
    label "normalizacja"
  ]
  node [
    id 1227
    label "moralno&#347;&#263;"
  ]
  node [
    id 1228
    label "criterion"
  ]
  node [
    id 1229
    label "opis"
  ]
  node [
    id 1230
    label "regu&#322;a_Glogera"
  ]
  node [
    id 1231
    label "prawo_Mendla"
  ]
  node [
    id 1232
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 1233
    label "twierdzenie"
  ]
  node [
    id 1234
    label "prawo"
  ]
  node [
    id 1235
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 1236
    label "spos&#243;b"
  ]
  node [
    id 1237
    label "qualification"
  ]
  node [
    id 1238
    label "occupation"
  ]
  node [
    id 1239
    label "podstawa"
  ]
  node [
    id 1240
    label "substancja"
  ]
  node [
    id 1241
    label "prawid&#322;o"
  ]
  node [
    id 1242
    label "dobro&#263;"
  ]
  node [
    id 1243
    label "aretologia"
  ]
  node [
    id 1244
    label "morality"
  ]
  node [
    id 1245
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 1246
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 1247
    label "honesty"
  ]
  node [
    id 1248
    label "model"
  ]
  node [
    id 1249
    label "organizowa&#263;"
  ]
  node [
    id 1250
    label "ordinariness"
  ]
  node [
    id 1251
    label "zorganizowa&#263;"
  ]
  node [
    id 1252
    label "taniec_towarzyski"
  ]
  node [
    id 1253
    label "organizowanie"
  ]
  node [
    id 1254
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 1255
    label "zorganizowanie"
  ]
  node [
    id 1256
    label "exposition"
  ]
  node [
    id 1257
    label "obja&#347;nienie"
  ]
  node [
    id 1258
    label "zawarcie"
  ]
  node [
    id 1259
    label "zawrze&#263;"
  ]
  node [
    id 1260
    label "czyn"
  ]
  node [
    id 1261
    label "warunek"
  ]
  node [
    id 1262
    label "gestia_transportowa"
  ]
  node [
    id 1263
    label "contract"
  ]
  node [
    id 1264
    label "porozumienie"
  ]
  node [
    id 1265
    label "klauzula"
  ]
  node [
    id 1266
    label "przenikanie"
  ]
  node [
    id 1267
    label "byt"
  ]
  node [
    id 1268
    label "cz&#261;steczka"
  ]
  node [
    id 1269
    label "temperatura_krytyczna"
  ]
  node [
    id 1270
    label "przenika&#263;"
  ]
  node [
    id 1271
    label "smolisty"
  ]
  node [
    id 1272
    label "pot&#281;ga"
  ]
  node [
    id 1273
    label "documentation"
  ]
  node [
    id 1274
    label "column"
  ]
  node [
    id 1275
    label "zasadzenie"
  ]
  node [
    id 1276
    label "punkt_odniesienia"
  ]
  node [
    id 1277
    label "zasadzi&#263;"
  ]
  node [
    id 1278
    label "bok"
  ]
  node [
    id 1279
    label "d&#243;&#322;"
  ]
  node [
    id 1280
    label "dzieci&#281;ctwo"
  ]
  node [
    id 1281
    label "background"
  ]
  node [
    id 1282
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 1283
    label "strategia"
  ]
  node [
    id 1284
    label "pomys&#322;"
  ]
  node [
    id 1285
    label "&#347;ciana"
  ]
  node [
    id 1286
    label "narz&#281;dzie"
  ]
  node [
    id 1287
    label "nature"
  ]
  node [
    id 1288
    label "shoetree"
  ]
  node [
    id 1289
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 1290
    label "alternatywa_Fredholma"
  ]
  node [
    id 1291
    label "oznajmianie"
  ]
  node [
    id 1292
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 1293
    label "teoria"
  ]
  node [
    id 1294
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 1295
    label "paradoks_Leontiefa"
  ]
  node [
    id 1296
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 1297
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 1298
    label "teza"
  ]
  node [
    id 1299
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 1300
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 1301
    label "twierdzenie_Pettisa"
  ]
  node [
    id 1302
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 1303
    label "twierdzenie_Maya"
  ]
  node [
    id 1304
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 1305
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 1306
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 1307
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 1308
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 1309
    label "zapewnianie"
  ]
  node [
    id 1310
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 1311
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 1312
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 1313
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 1314
    label "twierdzenie_Stokesa"
  ]
  node [
    id 1315
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 1316
    label "twierdzenie_Cevy"
  ]
  node [
    id 1317
    label "twierdzenie_Pascala"
  ]
  node [
    id 1318
    label "proposition"
  ]
  node [
    id 1319
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 1320
    label "komunikowanie"
  ]
  node [
    id 1321
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 1322
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 1323
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 1324
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 1325
    label "relacja"
  ]
  node [
    id 1326
    label "badanie"
  ]
  node [
    id 1327
    label "remark"
  ]
  node [
    id 1328
    label "metoda"
  ]
  node [
    id 1329
    label "stwierdzenie"
  ]
  node [
    id 1330
    label "observation"
  ]
  node [
    id 1331
    label "operacja"
  ]
  node [
    id 1332
    label "porz&#261;dek"
  ]
  node [
    id 1333
    label "dominance"
  ]
  node [
    id 1334
    label "zabieg"
  ]
  node [
    id 1335
    label "standardization"
  ]
  node [
    id 1336
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 1337
    label "umocowa&#263;"
  ]
  node [
    id 1338
    label "procesualistyka"
  ]
  node [
    id 1339
    label "kryminalistyka"
  ]
  node [
    id 1340
    label "kierunek"
  ]
  node [
    id 1341
    label "normatywizm"
  ]
  node [
    id 1342
    label "jurisprudence"
  ]
  node [
    id 1343
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 1344
    label "kultura_duchowa"
  ]
  node [
    id 1345
    label "przepis"
  ]
  node [
    id 1346
    label "prawo_karne_procesowe"
  ]
  node [
    id 1347
    label "kazuistyka"
  ]
  node [
    id 1348
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1349
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 1350
    label "kryminologia"
  ]
  node [
    id 1351
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 1352
    label "prawo_karne"
  ]
  node [
    id 1353
    label "cywilistyka"
  ]
  node [
    id 1354
    label "judykatura"
  ]
  node [
    id 1355
    label "kanonistyka"
  ]
  node [
    id 1356
    label "nauka_prawa"
  ]
  node [
    id 1357
    label "law"
  ]
  node [
    id 1358
    label "wykonawczy"
  ]
  node [
    id 1359
    label "podtrzymywanie"
  ]
  node [
    id 1360
    label "w&#322;&#261;czanie"
  ]
  node [
    id 1361
    label "uruchamianie"
  ]
  node [
    id 1362
    label "nakr&#281;cenie"
  ]
  node [
    id 1363
    label "uruchomienie"
  ]
  node [
    id 1364
    label "nakr&#281;canie"
  ]
  node [
    id 1365
    label "impact"
  ]
  node [
    id 1366
    label "tr&#243;jstronny"
  ]
  node [
    id 1367
    label "dzianie_si&#281;"
  ]
  node [
    id 1368
    label "w&#322;&#261;czenie"
  ]
  node [
    id 1369
    label "zatrzymanie"
  ]
  node [
    id 1370
    label "addytywno&#347;&#263;"
  ]
  node [
    id 1371
    label "function"
  ]
  node [
    id 1372
    label "zastosowanie"
  ]
  node [
    id 1373
    label "praca"
  ]
  node [
    id 1374
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 1375
    label "cel"
  ]
  node [
    id 1376
    label "dziedzina"
  ]
  node [
    id 1377
    label "przeciwdziedzina"
  ]
  node [
    id 1378
    label "awansowa&#263;"
  ]
  node [
    id 1379
    label "stawia&#263;"
  ]
  node [
    id 1380
    label "wakowa&#263;"
  ]
  node [
    id 1381
    label "znaczenie"
  ]
  node [
    id 1382
    label "postawi&#263;"
  ]
  node [
    id 1383
    label "awansowanie"
  ]
  node [
    id 1384
    label "utrzymywanie"
  ]
  node [
    id 1385
    label "obstawanie"
  ]
  node [
    id 1386
    label "bycie"
  ]
  node [
    id 1387
    label "preservation"
  ]
  node [
    id 1388
    label "robienie"
  ]
  node [
    id 1389
    label "boost"
  ]
  node [
    id 1390
    label "continuance"
  ]
  node [
    id 1391
    label "pocieszanie"
  ]
  node [
    id 1392
    label "powodowanie"
  ]
  node [
    id 1393
    label "przefiltrowanie"
  ]
  node [
    id 1394
    label "zamkni&#281;cie"
  ]
  node [
    id 1395
    label "career"
  ]
  node [
    id 1396
    label "zaaresztowanie"
  ]
  node [
    id 1397
    label "przechowanie"
  ]
  node [
    id 1398
    label "closure"
  ]
  node [
    id 1399
    label "uniemo&#380;liwienie"
  ]
  node [
    id 1400
    label "pochowanie"
  ]
  node [
    id 1401
    label "discontinuance"
  ]
  node [
    id 1402
    label "przerwanie"
  ]
  node [
    id 1403
    label "zaczepienie"
  ]
  node [
    id 1404
    label "pozajmowanie"
  ]
  node [
    id 1405
    label "hipostaza"
  ]
  node [
    id 1406
    label "capture"
  ]
  node [
    id 1407
    label "przetrzymanie"
  ]
  node [
    id 1408
    label "oddzia&#322;anie"
  ]
  node [
    id 1409
    label "&#322;apanie"
  ]
  node [
    id 1410
    label "z&#322;apanie"
  ]
  node [
    id 1411
    label "check"
  ]
  node [
    id 1412
    label "unieruchomienie"
  ]
  node [
    id 1413
    label "zabranie"
  ]
  node [
    id 1414
    label "przestanie"
  ]
  node [
    id 1415
    label "ukr&#281;cenie"
  ]
  node [
    id 1416
    label "gyration"
  ]
  node [
    id 1417
    label "ruszanie"
  ]
  node [
    id 1418
    label "dokr&#281;cenie"
  ]
  node [
    id 1419
    label "kr&#281;cenie"
  ]
  node [
    id 1420
    label "zakr&#281;canie"
  ]
  node [
    id 1421
    label "tworzenie"
  ]
  node [
    id 1422
    label "nagrywanie"
  ]
  node [
    id 1423
    label "wind"
  ]
  node [
    id 1424
    label "nak&#322;adanie"
  ]
  node [
    id 1425
    label "okr&#281;canie"
  ]
  node [
    id 1426
    label "wzmaganie"
  ]
  node [
    id 1427
    label "wzbudzanie"
  ]
  node [
    id 1428
    label "dokr&#281;canie"
  ]
  node [
    id 1429
    label "kapita&#322;"
  ]
  node [
    id 1430
    label "zacz&#281;cie"
  ]
  node [
    id 1431
    label "propulsion"
  ]
  node [
    id 1432
    label "odkr&#281;cenie_kurka"
  ]
  node [
    id 1433
    label "pos&#322;uchanie"
  ]
  node [
    id 1434
    label "obejrzenie"
  ]
  node [
    id 1435
    label "involvement"
  ]
  node [
    id 1436
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 1437
    label "za&#347;wiecenie"
  ]
  node [
    id 1438
    label "nastawienie"
  ]
  node [
    id 1439
    label "wy&#322;&#261;czenie"
  ]
  node [
    id 1440
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1441
    label "widzenie"
  ]
  node [
    id 1442
    label "wy&#322;&#261;czanie"
  ]
  node [
    id 1443
    label "incorporation"
  ]
  node [
    id 1444
    label "zaczynanie"
  ]
  node [
    id 1445
    label "nastawianie"
  ]
  node [
    id 1446
    label "przy&#322;&#261;czanie"
  ]
  node [
    id 1447
    label "zapalanie"
  ]
  node [
    id 1448
    label "inclusion"
  ]
  node [
    id 1449
    label "przes&#322;uchiwanie"
  ]
  node [
    id 1450
    label "odkr&#281;canie_kurka"
  ]
  node [
    id 1451
    label "uczestniczenie"
  ]
  node [
    id 1452
    label "pozawijanie"
  ]
  node [
    id 1453
    label "stworzenie"
  ]
  node [
    id 1454
    label "nagranie"
  ]
  node [
    id 1455
    label "wzbudzenie"
  ]
  node [
    id 1456
    label "ruszenie"
  ]
  node [
    id 1457
    label "zakr&#281;cenie"
  ]
  node [
    id 1458
    label "naniesienie"
  ]
  node [
    id 1459
    label "suppression"
  ]
  node [
    id 1460
    label "wzmo&#380;enie"
  ]
  node [
    id 1461
    label "okr&#281;cenie"
  ]
  node [
    id 1462
    label "nak&#322;amanie"
  ]
  node [
    id 1463
    label "czasowo"
  ]
  node [
    id 1464
    label "wtedy"
  ]
  node [
    id 1465
    label "czasowy"
  ]
  node [
    id 1466
    label "temporarily"
  ]
  node [
    id 1467
    label "kiedy&#347;"
  ]
  node [
    id 1468
    label "podmiot_gospodarczy"
  ]
  node [
    id 1469
    label "wsp&#243;lnictwo"
  ]
  node [
    id 1470
    label "Liga_Obrony_Kraju"
  ]
  node [
    id 1471
    label "TOPR"
  ]
  node [
    id 1472
    label "endecki"
  ]
  node [
    id 1473
    label "przedstawicielstwo"
  ]
  node [
    id 1474
    label "od&#322;am"
  ]
  node [
    id 1475
    label "Cepelia"
  ]
  node [
    id 1476
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1477
    label "ZBoWiD"
  ]
  node [
    id 1478
    label "organization"
  ]
  node [
    id 1479
    label "centrala"
  ]
  node [
    id 1480
    label "GOPR"
  ]
  node [
    id 1481
    label "ZOMO"
  ]
  node [
    id 1482
    label "ZMP"
  ]
  node [
    id 1483
    label "komitet_koordynacyjny"
  ]
  node [
    id 1484
    label "przybud&#243;wka"
  ]
  node [
    id 1485
    label "boj&#243;wka"
  ]
  node [
    id 1486
    label "odm&#322;adzanie"
  ]
  node [
    id 1487
    label "&#346;wietliki"
  ]
  node [
    id 1488
    label "whole"
  ]
  node [
    id 1489
    label "skupienie"
  ]
  node [
    id 1490
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 1491
    label "odm&#322;adza&#263;"
  ]
  node [
    id 1492
    label "zabudowania"
  ]
  node [
    id 1493
    label "group"
  ]
  node [
    id 1494
    label "schorzenie"
  ]
  node [
    id 1495
    label "ro&#347;lina"
  ]
  node [
    id 1496
    label "batch"
  ]
  node [
    id 1497
    label "odm&#322;odzenie"
  ]
  node [
    id 1498
    label "uczestnictwo"
  ]
  node [
    id 1499
    label "radio"
  ]
  node [
    id 1500
    label "radiokomunikacja"
  ]
  node [
    id 1501
    label "infrastruktura"
  ]
  node [
    id 1502
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 1503
    label "radiofonizacja"
  ]
  node [
    id 1504
    label "zaplecze"
  ]
  node [
    id 1505
    label "trasa"
  ]
  node [
    id 1506
    label "telefonia"
  ]
  node [
    id 1507
    label "telekomunikacja"
  ]
  node [
    id 1508
    label "kr&#243;tkofalarstwo"
  ]
  node [
    id 1509
    label "cz&#281;stotliwo&#347;&#263;"
  ]
  node [
    id 1510
    label "modernizacja"
  ]
  node [
    id 1511
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 1512
    label "paj&#281;czarz"
  ]
  node [
    id 1513
    label "radiola"
  ]
  node [
    id 1514
    label "programowiec"
  ]
  node [
    id 1515
    label "redakcja"
  ]
  node [
    id 1516
    label "spot"
  ]
  node [
    id 1517
    label "stacja"
  ]
  node [
    id 1518
    label "odbiornik"
  ]
  node [
    id 1519
    label "eliminator"
  ]
  node [
    id 1520
    label "radiolinia"
  ]
  node [
    id 1521
    label "fala_radiowa"
  ]
  node [
    id 1522
    label "odbieranie"
  ]
  node [
    id 1523
    label "studio"
  ]
  node [
    id 1524
    label "dyskryminator"
  ]
  node [
    id 1525
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 1526
    label "odbiera&#263;"
  ]
  node [
    id 1527
    label "ekran"
  ]
  node [
    id 1528
    label "BBC"
  ]
  node [
    id 1529
    label "Interwizja"
  ]
  node [
    id 1530
    label "Polsat"
  ]
  node [
    id 1531
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 1532
    label "muza"
  ]
  node [
    id 1533
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 1534
    label "technologia"
  ]
  node [
    id 1535
    label "technika"
  ]
  node [
    id 1536
    label "teletransmisja"
  ]
  node [
    id 1537
    label "telemetria"
  ]
  node [
    id 1538
    label "telegrafia"
  ]
  node [
    id 1539
    label "transmisja_danych"
  ]
  node [
    id 1540
    label "kontrola_parzysto&#347;ci"
  ]
  node [
    id 1541
    label "trunking"
  ]
  node [
    id 1542
    label "komunikacja"
  ]
  node [
    id 1543
    label "telekomutacja"
  ]
  node [
    id 1544
    label "teletechnika"
  ]
  node [
    id 1545
    label "teleks"
  ]
  node [
    id 1546
    label "telematyka"
  ]
  node [
    id 1547
    label "teleinformatyka"
  ]
  node [
    id 1548
    label "mikrotechnologia"
  ]
  node [
    id 1549
    label "technologia_nieorganiczna"
  ]
  node [
    id 1550
    label "engineering"
  ]
  node [
    id 1551
    label "biotechnologia"
  ]
  node [
    id 1552
    label "osoba_prawna"
  ]
  node [
    id 1553
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1554
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1555
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1556
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1557
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1558
    label "Fundusze_Unijne"
  ]
  node [
    id 1559
    label "zamyka&#263;"
  ]
  node [
    id 1560
    label "establishment"
  ]
  node [
    id 1561
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1562
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1563
    label "afiliowa&#263;"
  ]
  node [
    id 1564
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1565
    label "zamykanie"
  ]
  node [
    id 1566
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1567
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1568
    label "antena"
  ]
  node [
    id 1569
    label "urz&#261;dzenie"
  ]
  node [
    id 1570
    label "amplituner"
  ]
  node [
    id 1571
    label "tuner"
  ]
  node [
    id 1572
    label "inspiratorka"
  ]
  node [
    id 1573
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 1574
    label "banan"
  ]
  node [
    id 1575
    label "talent"
  ]
  node [
    id 1576
    label "kobieta"
  ]
  node [
    id 1577
    label "Melpomena"
  ]
  node [
    id 1578
    label "natchnienie"
  ]
  node [
    id 1579
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 1580
    label "bogini"
  ]
  node [
    id 1581
    label "muzyka"
  ]
  node [
    id 1582
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 1583
    label "palma"
  ]
  node [
    id 1584
    label "redaktor"
  ]
  node [
    id 1585
    label "composition"
  ]
  node [
    id 1586
    label "wydawnictwo"
  ]
  node [
    id 1587
    label "redaction"
  ]
  node [
    id 1588
    label "obr&#243;bka"
  ]
  node [
    id 1589
    label "p&#322;aszczyzna"
  ]
  node [
    id 1590
    label "naszywka"
  ]
  node [
    id 1591
    label "kominek"
  ]
  node [
    id 1592
    label "zas&#322;ona"
  ]
  node [
    id 1593
    label "os&#322;ona"
  ]
  node [
    id 1594
    label "u&#380;ytkownik"
  ]
  node [
    id 1595
    label "oszust"
  ]
  node [
    id 1596
    label "telewizor"
  ]
  node [
    id 1597
    label "pirat"
  ]
  node [
    id 1598
    label "rozsi&#261;dni&#281;cie_si&#281;"
  ]
  node [
    id 1599
    label "wpadni&#281;cie"
  ]
  node [
    id 1600
    label "collection"
  ]
  node [
    id 1601
    label "konfiskowanie"
  ]
  node [
    id 1602
    label "rozsiadanie_si&#281;"
  ]
  node [
    id 1603
    label "zabieranie"
  ]
  node [
    id 1604
    label "zlecenie"
  ]
  node [
    id 1605
    label "przyjmowanie"
  ]
  node [
    id 1606
    label "solicitation"
  ]
  node [
    id 1607
    label "przyp&#322;yni&#281;cie"
  ]
  node [
    id 1608
    label "zniewalanie"
  ]
  node [
    id 1609
    label "przyp&#322;ywanie"
  ]
  node [
    id 1610
    label "odzyskiwanie"
  ]
  node [
    id 1611
    label "branie"
  ]
  node [
    id 1612
    label "perception"
  ]
  node [
    id 1613
    label "odp&#322;ywanie"
  ]
  node [
    id 1614
    label "wpadanie"
  ]
  node [
    id 1615
    label "zabiera&#263;"
  ]
  node [
    id 1616
    label "odzyskiwa&#263;"
  ]
  node [
    id 1617
    label "przyjmowa&#263;"
  ]
  node [
    id 1618
    label "bra&#263;"
  ]
  node [
    id 1619
    label "fall"
  ]
  node [
    id 1620
    label "liszy&#263;"
  ]
  node [
    id 1621
    label "pozbawia&#263;"
  ]
  node [
    id 1622
    label "konfiskowa&#263;"
  ]
  node [
    id 1623
    label "deprive"
  ]
  node [
    id 1624
    label "accept"
  ]
  node [
    id 1625
    label "doznawa&#263;"
  ]
  node [
    id 1626
    label "dostawa&#263;"
  ]
  node [
    id 1627
    label "take"
  ]
  node [
    id 1628
    label "wytwarza&#263;"
  ]
  node [
    id 1629
    label "nabywa&#263;"
  ]
  node [
    id 1630
    label "uzyskiwa&#263;"
  ]
  node [
    id 1631
    label "winnings"
  ]
  node [
    id 1632
    label "opanowywa&#263;"
  ]
  node [
    id 1633
    label "range"
  ]
  node [
    id 1634
    label "wystarcza&#263;"
  ]
  node [
    id 1635
    label "kupowa&#263;"
  ]
  node [
    id 1636
    label "obskakiwa&#263;"
  ]
  node [
    id 1637
    label "create"
  ]
  node [
    id 1638
    label "give"
  ]
  node [
    id 1639
    label "zmniejszenie_si&#281;"
  ]
  node [
    id 1640
    label "zmniejszanie"
  ]
  node [
    id 1641
    label "inny"
  ]
  node [
    id 1642
    label "zmniejszanie_si&#281;"
  ]
  node [
    id 1643
    label "chudni&#281;cie"
  ]
  node [
    id 1644
    label "zmienianie"
  ]
  node [
    id 1645
    label "decrease"
  ]
  node [
    id 1646
    label "spr&#281;&#380;anie"
  ]
  node [
    id 1647
    label "chudszy"
  ]
  node [
    id 1648
    label "stawanie_si&#281;"
  ]
  node [
    id 1649
    label "kolejny"
  ]
  node [
    id 1650
    label "osobno"
  ]
  node [
    id 1651
    label "inszy"
  ]
  node [
    id 1652
    label "inaczej"
  ]
  node [
    id 1653
    label "abstrakcja"
  ]
  node [
    id 1654
    label "chemikalia"
  ]
  node [
    id 1655
    label "poprzedzanie"
  ]
  node [
    id 1656
    label "czasoprzestrze&#324;"
  ]
  node [
    id 1657
    label "laba"
  ]
  node [
    id 1658
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 1659
    label "chronometria"
  ]
  node [
    id 1660
    label "rachuba_czasu"
  ]
  node [
    id 1661
    label "przep&#322;ywanie"
  ]
  node [
    id 1662
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 1663
    label "czasokres"
  ]
  node [
    id 1664
    label "odczyt"
  ]
  node [
    id 1665
    label "chwila"
  ]
  node [
    id 1666
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 1667
    label "dzieje"
  ]
  node [
    id 1668
    label "kategoria_gramatyczna"
  ]
  node [
    id 1669
    label "poprzedzenie"
  ]
  node [
    id 1670
    label "trawienie"
  ]
  node [
    id 1671
    label "pochodzi&#263;"
  ]
  node [
    id 1672
    label "period"
  ]
  node [
    id 1673
    label "okres_czasu"
  ]
  node [
    id 1674
    label "poprzedza&#263;"
  ]
  node [
    id 1675
    label "schy&#322;ek"
  ]
  node [
    id 1676
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 1677
    label "odwlekanie_si&#281;"
  ]
  node [
    id 1678
    label "zegar"
  ]
  node [
    id 1679
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 1680
    label "czwarty_wymiar"
  ]
  node [
    id 1681
    label "pochodzenie"
  ]
  node [
    id 1682
    label "koniugacja"
  ]
  node [
    id 1683
    label "Zeitgeist"
  ]
  node [
    id 1684
    label "trawi&#263;"
  ]
  node [
    id 1685
    label "pogoda"
  ]
  node [
    id 1686
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 1687
    label "poprzedzi&#263;"
  ]
  node [
    id 1688
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 1689
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 1690
    label "time_period"
  ]
  node [
    id 1691
    label "po&#322;o&#380;enie"
  ]
  node [
    id 1692
    label "sprawa"
  ]
  node [
    id 1693
    label "ust&#281;p"
  ]
  node [
    id 1694
    label "plan"
  ]
  node [
    id 1695
    label "obiekt_matematyczny"
  ]
  node [
    id 1696
    label "problemat"
  ]
  node [
    id 1697
    label "plamka"
  ]
  node [
    id 1698
    label "stopie&#324;_pisma"
  ]
  node [
    id 1699
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 1700
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 1701
    label "mark"
  ]
  node [
    id 1702
    label "prosta"
  ]
  node [
    id 1703
    label "problematyka"
  ]
  node [
    id 1704
    label "obiekt"
  ]
  node [
    id 1705
    label "zapunktowa&#263;"
  ]
  node [
    id 1706
    label "podpunkt"
  ]
  node [
    id 1707
    label "kres"
  ]
  node [
    id 1708
    label "przestrze&#324;"
  ]
  node [
    id 1709
    label "point"
  ]
  node [
    id 1710
    label "plac"
  ]
  node [
    id 1711
    label "location"
  ]
  node [
    id 1712
    label "uwaga"
  ]
  node [
    id 1713
    label "status"
  ]
  node [
    id 1714
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 1715
    label "cia&#322;o"
  ]
  node [
    id 1716
    label "abstractedness"
  ]
  node [
    id 1717
    label "abstraction"
  ]
  node [
    id 1718
    label "obraz"
  ]
  node [
    id 1719
    label "spalenie"
  ]
  node [
    id 1720
    label "spalanie"
  ]
  node [
    id 1721
    label "mi&#281;dzybankowy"
  ]
  node [
    id 1722
    label "pozamaterialny"
  ]
  node [
    id 1723
    label "materjalny"
  ]
  node [
    id 1724
    label "fizyczny"
  ]
  node [
    id 1725
    label "materialny"
  ]
  node [
    id 1726
    label "niematerialnie"
  ]
  node [
    id 1727
    label "financially"
  ]
  node [
    id 1728
    label "fiscally"
  ]
  node [
    id 1729
    label "bytowo"
  ]
  node [
    id 1730
    label "ekonomicznie"
  ]
  node [
    id 1731
    label "fizykalnie"
  ]
  node [
    id 1732
    label "materializowanie"
  ]
  node [
    id 1733
    label "fizycznie"
  ]
  node [
    id 1734
    label "namacalny"
  ]
  node [
    id 1735
    label "widoczny"
  ]
  node [
    id 1736
    label "zmaterializowanie"
  ]
  node [
    id 1737
    label "organiczny"
  ]
  node [
    id 1738
    label "gimnastyczny"
  ]
  node [
    id 1739
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 1740
    label "pewnie"
  ]
  node [
    id 1741
    label "zauwa&#380;alnie"
  ]
  node [
    id 1742
    label "podj&#281;cie"
  ]
  node [
    id 1743
    label "resoluteness"
  ]
  node [
    id 1744
    label "najpewniej"
  ]
  node [
    id 1745
    label "pewny"
  ]
  node [
    id 1746
    label "wiarygodnie"
  ]
  node [
    id 1747
    label "mocno"
  ]
  node [
    id 1748
    label "pewniej"
  ]
  node [
    id 1749
    label "bezpiecznie"
  ]
  node [
    id 1750
    label "zwinnie"
  ]
  node [
    id 1751
    label "postrzegalnie"
  ]
  node [
    id 1752
    label "entertainment"
  ]
  node [
    id 1753
    label "consumption"
  ]
  node [
    id 1754
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 1755
    label "erecting"
  ]
  node [
    id 1756
    label "movement"
  ]
  node [
    id 1757
    label "reply"
  ]
  node [
    id 1758
    label "zahipnotyzowanie"
  ]
  node [
    id 1759
    label "chemia"
  ]
  node [
    id 1760
    label "wdarcie_si&#281;"
  ]
  node [
    id 1761
    label "dotarcie"
  ]
  node [
    id 1762
    label "reakcja_chemiczna"
  ]
  node [
    id 1763
    label "charakterystyka"
  ]
  node [
    id 1764
    label "m&#322;ot"
  ]
  node [
    id 1765
    label "znak"
  ]
  node [
    id 1766
    label "drzewo"
  ]
  node [
    id 1767
    label "pr&#243;ba"
  ]
  node [
    id 1768
    label "attribute"
  ]
  node [
    id 1769
    label "marka"
  ]
  node [
    id 1770
    label "gotowy"
  ]
  node [
    id 1771
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 1772
    label "management"
  ]
  node [
    id 1773
    label "resolution"
  ]
  node [
    id 1774
    label "dokument"
  ]
  node [
    id 1775
    label "zmienia&#263;"
  ]
  node [
    id 1776
    label "worsen"
  ]
  node [
    id 1777
    label "traci&#263;"
  ]
  node [
    id 1778
    label "alternate"
  ]
  node [
    id 1779
    label "change"
  ]
  node [
    id 1780
    label "reengineering"
  ]
  node [
    id 1781
    label "zast&#281;powa&#263;"
  ]
  node [
    id 1782
    label "sprawia&#263;"
  ]
  node [
    id 1783
    label "zyskiwa&#263;"
  ]
  node [
    id 1784
    label "przechodzi&#263;"
  ]
  node [
    id 1785
    label "szczeg&#243;&#322;"
  ]
  node [
    id 1786
    label "realia"
  ]
  node [
    id 1787
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 1788
    label "niuansowa&#263;"
  ]
  node [
    id 1789
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 1790
    label "zniuansowa&#263;"
  ]
  node [
    id 1791
    label "fraza"
  ]
  node [
    id 1792
    label "temat"
  ]
  node [
    id 1793
    label "melodia"
  ]
  node [
    id 1794
    label "ozdoba"
  ]
  node [
    id 1795
    label "message"
  ]
  node [
    id 1796
    label "kontekst"
  ]
  node [
    id 1797
    label "korzystnie"
  ]
  node [
    id 1798
    label "oszcz&#281;dnie"
  ]
  node [
    id 1799
    label "ontologicznie"
  ]
  node [
    id 1800
    label "oszcz&#281;dny"
  ]
  node [
    id 1801
    label "prosty"
  ]
  node [
    id 1802
    label "pow&#347;ci&#261;gliwy"
  ]
  node [
    id 1803
    label "rozwa&#380;ny"
  ]
  node [
    id 1804
    label "klient"
  ]
  node [
    id 1805
    label "przesy&#322;ka"
  ]
  node [
    id 1806
    label "agent_rozliczeniowy"
  ]
  node [
    id 1807
    label "komputer_cyfrowy"
  ]
  node [
    id 1808
    label "us&#322;ugobiorca"
  ]
  node [
    id 1809
    label "Rzymianin"
  ]
  node [
    id 1810
    label "szlachcic"
  ]
  node [
    id 1811
    label "obywatel"
  ]
  node [
    id 1812
    label "klientela"
  ]
  node [
    id 1813
    label "program"
  ]
  node [
    id 1814
    label "osobowo&#347;&#263;"
  ]
  node [
    id 1815
    label "posy&#322;ka"
  ]
  node [
    id 1816
    label "adres"
  ]
  node [
    id 1817
    label "dochodzi&#263;"
  ]
  node [
    id 1818
    label "wyj&#261;tkowo"
  ]
  node [
    id 1819
    label "specially"
  ]
  node [
    id 1820
    label "osobnie"
  ]
  node [
    id 1821
    label "niestandardowo"
  ]
  node [
    id 1822
    label "niezwykle"
  ]
  node [
    id 1823
    label "r&#243;&#380;nie"
  ]
  node [
    id 1824
    label "naciska&#263;"
  ]
  node [
    id 1825
    label "atakowa&#263;"
  ]
  node [
    id 1826
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 1827
    label "chance"
  ]
  node [
    id 1828
    label "force"
  ]
  node [
    id 1829
    label "rush"
  ]
  node [
    id 1830
    label "crowd"
  ]
  node [
    id 1831
    label "napierdziela&#263;"
  ]
  node [
    id 1832
    label "przekonywa&#263;"
  ]
  node [
    id 1833
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 1834
    label "strike"
  ]
  node [
    id 1835
    label "dzia&#322;a&#263;"
  ]
  node [
    id 1836
    label "ofensywny"
  ]
  node [
    id 1837
    label "przewaga"
  ]
  node [
    id 1838
    label "sport"
  ]
  node [
    id 1839
    label "epidemia"
  ]
  node [
    id 1840
    label "attack"
  ]
  node [
    id 1841
    label "rozgrywa&#263;"
  ]
  node [
    id 1842
    label "krytykowa&#263;"
  ]
  node [
    id 1843
    label "walczy&#263;"
  ]
  node [
    id 1844
    label "aim"
  ]
  node [
    id 1845
    label "trouble_oneself"
  ]
  node [
    id 1846
    label "napada&#263;"
  ]
  node [
    id 1847
    label "m&#243;wi&#263;"
  ]
  node [
    id 1848
    label "usi&#322;owa&#263;"
  ]
  node [
    id 1849
    label "ninie"
  ]
  node [
    id 1850
    label "aktualny"
  ]
  node [
    id 1851
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 1852
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1853
    label "jednocze&#347;nie"
  ]
  node [
    id 1854
    label "aktualnie"
  ]
  node [
    id 1855
    label "zaktualizowanie_si&#281;"
  ]
  node [
    id 1856
    label "aktualizowanie"
  ]
  node [
    id 1857
    label "uaktualnianie_si&#281;"
  ]
  node [
    id 1858
    label "uaktualnienie"
  ]
  node [
    id 1859
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 1860
    label "mocny"
  ]
  node [
    id 1861
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 1862
    label "nieprzyzwoity"
  ]
  node [
    id 1863
    label "radykalny"
  ]
  node [
    id 1864
    label "dosadny"
  ]
  node [
    id 1865
    label "przemoc"
  ]
  node [
    id 1866
    label "drastycznie"
  ]
  node [
    id 1867
    label "konsekwentny"
  ]
  node [
    id 1868
    label "gruntowny"
  ]
  node [
    id 1869
    label "radykalnie"
  ]
  node [
    id 1870
    label "bezkompromisowy"
  ]
  node [
    id 1871
    label "niepodwa&#380;alny"
  ]
  node [
    id 1872
    label "stabilny"
  ]
  node [
    id 1873
    label "trudny"
  ]
  node [
    id 1874
    label "krzepki"
  ]
  node [
    id 1875
    label "silny"
  ]
  node [
    id 1876
    label "wyrazisty"
  ]
  node [
    id 1877
    label "przekonuj&#261;cy"
  ]
  node [
    id 1878
    label "wzmocni&#263;"
  ]
  node [
    id 1879
    label "wzmacnia&#263;"
  ]
  node [
    id 1880
    label "wytrzyma&#322;y"
  ]
  node [
    id 1881
    label "silnie"
  ]
  node [
    id 1882
    label "meflochina"
  ]
  node [
    id 1883
    label "choro"
  ]
  node [
    id 1884
    label "nieprzyzwoicie"
  ]
  node [
    id 1885
    label "nieuczciwie"
  ]
  node [
    id 1886
    label "nierzetelny"
  ]
  node [
    id 1887
    label "nieobyczajny"
  ]
  node [
    id 1888
    label "niezrozumia&#322;y"
  ]
  node [
    id 1889
    label "naganny"
  ]
  node [
    id 1890
    label "dosadnie"
  ]
  node [
    id 1891
    label "rabelaisowski"
  ]
  node [
    id 1892
    label "bezpo&#347;redni"
  ]
  node [
    id 1893
    label "patologia"
  ]
  node [
    id 1894
    label "agresja"
  ]
  node [
    id 1895
    label "dowalanie"
  ]
  node [
    id 1896
    label "wygarnianie"
  ]
  node [
    id 1897
    label "uci&#281;cie"
  ]
  node [
    id 1898
    label "mikrotom"
  ]
  node [
    id 1899
    label "snub"
  ]
  node [
    id 1900
    label "retrenchment"
  ]
  node [
    id 1901
    label "rozci&#281;cie_si&#281;"
  ]
  node [
    id 1902
    label "naci&#281;cie_si&#281;"
  ]
  node [
    id 1903
    label "uderzenie"
  ]
  node [
    id 1904
    label "redukcja"
  ]
  node [
    id 1905
    label "cut"
  ]
  node [
    id 1906
    label "okrojenie"
  ]
  node [
    id 1907
    label "m&#243;wienie"
  ]
  node [
    id 1908
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 1909
    label "time"
  ]
  node [
    id 1910
    label "wcinanie"
  ]
  node [
    id 1911
    label "okrawanie"
  ]
  node [
    id 1912
    label "przecinanie"
  ]
  node [
    id 1913
    label "poci&#281;cie"
  ]
  node [
    id 1914
    label "dzielenie"
  ]
  node [
    id 1915
    label "uderzanie"
  ]
  node [
    id 1916
    label "film_editing"
  ]
  node [
    id 1917
    label "przeci&#281;cie"
  ]
  node [
    id 1918
    label "k&#261;sanie"
  ]
  node [
    id 1919
    label "reduction"
  ]
  node [
    id 1920
    label "uproszczenie"
  ]
  node [
    id 1921
    label "dzielnik"
  ]
  node [
    id 1922
    label "stosowanie"
  ]
  node [
    id 1923
    label "liczenie"
  ]
  node [
    id 1924
    label "dzielna"
  ]
  node [
    id 1925
    label "przeszkoda"
  ]
  node [
    id 1926
    label "rozprowadzanie"
  ]
  node [
    id 1927
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 1928
    label "wyodr&#281;bnianie"
  ]
  node [
    id 1929
    label "rozdawanie"
  ]
  node [
    id 1930
    label "contribution"
  ]
  node [
    id 1931
    label "division"
  ]
  node [
    id 1932
    label "iloraz"
  ]
  node [
    id 1933
    label "sk&#322;&#243;canie"
  ]
  node [
    id 1934
    label "separation"
  ]
  node [
    id 1935
    label "dzielenie_si&#281;"
  ]
  node [
    id 1936
    label "zagryzanie"
  ]
  node [
    id 1937
    label "kaleczenie"
  ]
  node [
    id 1938
    label "mr&#243;z"
  ]
  node [
    id 1939
    label "bolenie"
  ]
  node [
    id 1940
    label "kaganiec"
  ]
  node [
    id 1941
    label "bite"
  ]
  node [
    id 1942
    label "zagryzienie"
  ]
  node [
    id 1943
    label "public_speaking"
  ]
  node [
    id 1944
    label "powiadanie"
  ]
  node [
    id 1945
    label "przepowiadanie"
  ]
  node [
    id 1946
    label "wyznawanie"
  ]
  node [
    id 1947
    label "wypowiadanie"
  ]
  node [
    id 1948
    label "wydobywanie"
  ]
  node [
    id 1949
    label "gaworzenie"
  ]
  node [
    id 1950
    label "j&#281;zyk"
  ]
  node [
    id 1951
    label "wyra&#380;anie"
  ]
  node [
    id 1952
    label "formu&#322;owanie"
  ]
  node [
    id 1953
    label "dowalenie"
  ]
  node [
    id 1954
    label "przerywanie"
  ]
  node [
    id 1955
    label "wydawanie"
  ]
  node [
    id 1956
    label "dogadywanie_si&#281;"
  ]
  node [
    id 1957
    label "dodawanie"
  ]
  node [
    id 1958
    label "prawienie"
  ]
  node [
    id 1959
    label "opowiadanie"
  ]
  node [
    id 1960
    label "ozywanie_si&#281;"
  ]
  node [
    id 1961
    label "zapeszanie"
  ]
  node [
    id 1962
    label "zwracanie_si&#281;"
  ]
  node [
    id 1963
    label "dysfonia"
  ]
  node [
    id 1964
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 1965
    label "speaking"
  ]
  node [
    id 1966
    label "zauwa&#380;enie"
  ]
  node [
    id 1967
    label "mawianie"
  ]
  node [
    id 1968
    label "opowiedzenie"
  ]
  node [
    id 1969
    label "wej&#347;cie_w_s&#322;owo"
  ]
  node [
    id 1970
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1971
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 1972
    label "informowanie"
  ]
  node [
    id 1973
    label "dogadanie_si&#281;"
  ]
  node [
    id 1974
    label "wygadanie"
  ]
  node [
    id 1975
    label "zwracanie_uwagi"
  ]
  node [
    id 1976
    label "&#347;cinanie"
  ]
  node [
    id 1977
    label "stukanie"
  ]
  node [
    id 1978
    label "grasowanie"
  ]
  node [
    id 1979
    label "napadanie"
  ]
  node [
    id 1980
    label "taranowanie"
  ]
  node [
    id 1981
    label "pouderzanie"
  ]
  node [
    id 1982
    label "t&#322;uczenie"
  ]
  node [
    id 1983
    label "skontrowanie"
  ]
  node [
    id 1984
    label "dostawanie"
  ]
  node [
    id 1985
    label "walczenie"
  ]
  node [
    id 1986
    label "haratanie"
  ]
  node [
    id 1987
    label "bicie"
  ]
  node [
    id 1988
    label "kontrowanie"
  ]
  node [
    id 1989
    label "dotykanie"
  ]
  node [
    id 1990
    label "pobijanie"
  ]
  node [
    id 1991
    label "staranowanie"
  ]
  node [
    id 1992
    label "wytrzepanie"
  ]
  node [
    id 1993
    label "trzepanie"
  ]
  node [
    id 1994
    label "zamachiwanie_si&#281;"
  ]
  node [
    id 1995
    label "krytykowanie"
  ]
  node [
    id 1996
    label "torpedowanie"
  ]
  node [
    id 1997
    label "friction"
  ]
  node [
    id 1998
    label "nast&#281;powanie"
  ]
  node [
    id 1999
    label "odbijanie"
  ]
  node [
    id 2000
    label "knock"
  ]
  node [
    id 2001
    label "zadawanie"
  ]
  node [
    id 2002
    label "rozkwaszenie"
  ]
  node [
    id 2003
    label "instrumentalizacja"
  ]
  node [
    id 2004
    label "trafienie"
  ]
  node [
    id 2005
    label "walka"
  ]
  node [
    id 2006
    label "cios"
  ]
  node [
    id 2007
    label "pogorszenie"
  ]
  node [
    id 2008
    label "d&#378;wi&#281;k"
  ]
  node [
    id 2009
    label "coup"
  ]
  node [
    id 2010
    label "reakcja"
  ]
  node [
    id 2011
    label "contact"
  ]
  node [
    id 2012
    label "stukni&#281;cie"
  ]
  node [
    id 2013
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 2014
    label "bat"
  ]
  node [
    id 2015
    label "odbicie"
  ]
  node [
    id 2016
    label "dawka"
  ]
  node [
    id 2017
    label "&#347;ci&#281;cie"
  ]
  node [
    id 2018
    label "st&#322;uczenie"
  ]
  node [
    id 2019
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 2020
    label "odbicie_si&#281;"
  ]
  node [
    id 2021
    label "dotkni&#281;cie"
  ]
  node [
    id 2022
    label "charge"
  ]
  node [
    id 2023
    label "dostanie"
  ]
  node [
    id 2024
    label "skrytykowanie"
  ]
  node [
    id 2025
    label "zagrywka"
  ]
  node [
    id 2026
    label "manewr"
  ]
  node [
    id 2027
    label "nast&#261;pienie"
  ]
  node [
    id 2028
    label "stroke"
  ]
  node [
    id 2029
    label "pobicie"
  ]
  node [
    id 2030
    label "ruch"
  ]
  node [
    id 2031
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 2032
    label "flap"
  ]
  node [
    id 2033
    label "dotyk"
  ]
  node [
    id 2034
    label "fabrication"
  ]
  node [
    id 2035
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 2036
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 2037
    label "act"
  ]
  node [
    id 2038
    label "tentegowanie"
  ]
  node [
    id 2039
    label "bezproblemowy"
  ]
  node [
    id 2040
    label "abscission"
  ]
  node [
    id 2041
    label "ugryzienie"
  ]
  node [
    id 2042
    label "amputowanie"
  ]
  node [
    id 2043
    label "oddzielenie"
  ]
  node [
    id 2044
    label "pok&#261;sanie"
  ]
  node [
    id 2045
    label "usuwanie"
  ]
  node [
    id 2046
    label "skrajanie"
  ]
  node [
    id 2047
    label "redukowanie"
  ]
  node [
    id 2048
    label "usuni&#281;cie"
  ]
  node [
    id 2049
    label "condensation"
  ]
  node [
    id 2050
    label "skrojenie"
  ]
  node [
    id 2051
    label "zredukowanie"
  ]
  node [
    id 2052
    label "poprzecinanie"
  ]
  node [
    id 2053
    label "carving"
  ]
  node [
    id 2054
    label "w&#281;ze&#322;"
  ]
  node [
    id 2055
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 2056
    label "zranienie"
  ]
  node [
    id 2057
    label "podzielenie"
  ]
  node [
    id 2058
    label "z&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 2059
    label "&#322;&#261;czenie_si&#281;"
  ]
  node [
    id 2060
    label "intersection"
  ]
  node [
    id 2061
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 2062
    label "zag&#322;&#281;bienie"
  ]
  node [
    id 2063
    label "indentation"
  ]
  node [
    id 2064
    label "zjedzenie"
  ]
  node [
    id 2065
    label "zag&#322;&#281;bianie"
  ]
  node [
    id 2066
    label "jedzenie"
  ]
  node [
    id 2067
    label "do&#322;adowywanie"
  ]
  node [
    id 2068
    label "dokuczanie"
  ]
  node [
    id 2069
    label "dopieprzanie"
  ]
  node [
    id 2070
    label "przykrochmalanie"
  ]
  node [
    id 2071
    label "fire"
  ]
  node [
    id 2072
    label "zestrzeliwanie"
  ]
  node [
    id 2073
    label "odstrzeliwanie"
  ]
  node [
    id 2074
    label "zestrzelenie"
  ]
  node [
    id 2075
    label "wystrzelanie"
  ]
  node [
    id 2076
    label "wylatywanie"
  ]
  node [
    id 2077
    label "chybianie"
  ]
  node [
    id 2078
    label "przestrzeliwanie"
  ]
  node [
    id 2079
    label "ostrzelanie"
  ]
  node [
    id 2080
    label "ostrzeliwanie"
  ]
  node [
    id 2081
    label "trafianie"
  ]
  node [
    id 2082
    label "odpalanie"
  ]
  node [
    id 2083
    label "odstrzelenie"
  ]
  node [
    id 2084
    label "wstrzeliwanie_si&#281;"
  ]
  node [
    id 2085
    label "postrzelanie"
  ]
  node [
    id 2086
    label "wypominanie"
  ]
  node [
    id 2087
    label "chybienie"
  ]
  node [
    id 2088
    label "grzanie"
  ]
  node [
    id 2089
    label "palenie"
  ]
  node [
    id 2090
    label "kropni&#281;cie"
  ]
  node [
    id 2091
    label "prze&#322;adowywanie"
  ]
  node [
    id 2092
    label "plucie"
  ]
  node [
    id 2093
    label "przyrz&#261;d_laboratoryjny"
  ]
  node [
    id 2094
    label "przyrz&#261;d"
  ]
  node [
    id 2095
    label "preparat_mikroskopowy"
  ]
  node [
    id 2096
    label "ci&#261;&#263;"
  ]
  node [
    id 2097
    label "budgetary"
  ]
  node [
    id 2098
    label "bud&#380;etowo"
  ]
  node [
    id 2099
    label "etatowy"
  ]
  node [
    id 2100
    label "etatowo"
  ]
  node [
    id 2101
    label "sta&#322;y"
  ]
  node [
    id 2102
    label "oficjalny"
  ]
  node [
    id 2103
    label "zatrudniony"
  ]
  node [
    id 2104
    label "oddalenie"
  ]
  node [
    id 2105
    label "wylanie"
  ]
  node [
    id 2106
    label "ulga"
  ]
  node [
    id 2107
    label "za&#347;wiadczenie"
  ]
  node [
    id 2108
    label "wypowiedzenie"
  ]
  node [
    id 2109
    label "liberation"
  ]
  node [
    id 2110
    label "zmniejszenie"
  ]
  node [
    id 2111
    label "dowolny"
  ]
  node [
    id 2112
    label "spowolnienie"
  ]
  node [
    id 2113
    label "relief"
  ]
  node [
    id 2114
    label "nieobecno&#347;&#263;"
  ]
  node [
    id 2115
    label "uwolnienie"
  ]
  node [
    id 2116
    label "release"
  ]
  node [
    id 2117
    label "wolniejszy"
  ]
  node [
    id 2118
    label "relaxation"
  ]
  node [
    id 2119
    label "zmienienie"
  ]
  node [
    id 2120
    label "nieistnienie"
  ]
  node [
    id 2121
    label "failure"
  ]
  node [
    id 2122
    label "refleksyjno&#347;&#263;"
  ]
  node [
    id 2123
    label "potwierdzenie"
  ]
  node [
    id 2124
    label "certificate"
  ]
  node [
    id 2125
    label "konwersja"
  ]
  node [
    id 2126
    label "notice"
  ]
  node [
    id 2127
    label "&#347;cie&#347;nienie"
  ]
  node [
    id 2128
    label "przepowiedzenie"
  ]
  node [
    id 2129
    label "rozwi&#261;zanie"
  ]
  node [
    id 2130
    label "generowa&#263;"
  ]
  node [
    id 2131
    label "wydanie"
  ]
  node [
    id 2132
    label "generowanie"
  ]
  node [
    id 2133
    label "wydobycie"
  ]
  node [
    id 2134
    label "zwerbalizowanie"
  ]
  node [
    id 2135
    label "szyk"
  ]
  node [
    id 2136
    label "notification"
  ]
  node [
    id 2137
    label "denunciation"
  ]
  node [
    id 2138
    label "uspokojenie"
  ]
  node [
    id 2139
    label "zni&#380;ka"
  ]
  node [
    id 2140
    label "pomo&#380;enie"
  ]
  node [
    id 2141
    label "redemption"
  ]
  node [
    id 2142
    label "niepodleg&#322;y"
  ]
  node [
    id 2143
    label "wyswobodzenie_si&#281;"
  ]
  node [
    id 2144
    label "kimation"
  ]
  node [
    id 2145
    label "rze&#378;ba"
  ]
  node [
    id 2146
    label "distance"
  ]
  node [
    id 2147
    label "nakazanie"
  ]
  node [
    id 2148
    label "pokazanie"
  ]
  node [
    id 2149
    label "od&#322;o&#380;enie"
  ]
  node [
    id 2150
    label "oddalenie_si&#281;"
  ]
  node [
    id 2151
    label "przemieszczenie"
  ]
  node [
    id 2152
    label "coitus_interruptus"
  ]
  node [
    id 2153
    label "oddalanie"
  ]
  node [
    id 2154
    label "oddala&#263;"
  ]
  node [
    id 2155
    label "dismissal"
  ]
  node [
    id 2156
    label "odrzucenie"
  ]
  node [
    id 2157
    label "odprawienie"
  ]
  node [
    id 2158
    label "oddali&#263;"
  ]
  node [
    id 2159
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 2160
    label "wyrzucenie"
  ]
  node [
    id 2161
    label "zabetonowanie"
  ]
  node [
    id 2162
    label "effusion"
  ]
  node [
    id 2163
    label "opr&#243;&#380;nienie"
  ]
  node [
    id 2164
    label "wydostanie_si&#281;"
  ]
  node [
    id 2165
    label "pokrycie"
  ]
  node [
    id 2166
    label "slowdown"
  ]
  node [
    id 2167
    label "uwalnianie"
  ]
  node [
    id 2168
    label "dowolnie"
  ]
  node [
    id 2169
    label "salariat"
  ]
  node [
    id 2170
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 2171
    label "delegowanie"
  ]
  node [
    id 2172
    label "pracu&#347;"
  ]
  node [
    id 2173
    label "r&#281;ka"
  ]
  node [
    id 2174
    label "delegowa&#263;"
  ]
  node [
    id 2175
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 2176
    label "p&#322;aca"
  ]
  node [
    id 2177
    label "krzy&#380;"
  ]
  node [
    id 2178
    label "&#347;r&#243;dr&#281;cze"
  ]
  node [
    id 2179
    label "handwriting"
  ]
  node [
    id 2180
    label "d&#322;o&#324;"
  ]
  node [
    id 2181
    label "gestykulowa&#263;"
  ]
  node [
    id 2182
    label "ko&#324;czyna_g&#243;rna"
  ]
  node [
    id 2183
    label "palec"
  ]
  node [
    id 2184
    label "przedrami&#281;"
  ]
  node [
    id 2185
    label "hand"
  ]
  node [
    id 2186
    label "&#322;okie&#263;"
  ]
  node [
    id 2187
    label "hazena"
  ]
  node [
    id 2188
    label "pi&#322;ka_no&#380;na"
  ]
  node [
    id 2189
    label "bramkarz"
  ]
  node [
    id 2190
    label "nadgarstek"
  ]
  node [
    id 2191
    label "graba"
  ]
  node [
    id 2192
    label "r&#261;czyna"
  ]
  node [
    id 2193
    label "k&#322;&#261;b"
  ]
  node [
    id 2194
    label "pi&#322;ka"
  ]
  node [
    id 2195
    label "chwyta&#263;"
  ]
  node [
    id 2196
    label "cmoknonsens"
  ]
  node [
    id 2197
    label "gestykulowanie"
  ]
  node [
    id 2198
    label "chwytanie"
  ]
  node [
    id 2199
    label "obietnica"
  ]
  node [
    id 2200
    label "kroki"
  ]
  node [
    id 2201
    label "hasta"
  ]
  node [
    id 2202
    label "wykroczenie"
  ]
  node [
    id 2203
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 2204
    label "czerwona_kartka"
  ]
  node [
    id 2205
    label "paw"
  ]
  node [
    id 2206
    label "rami&#281;"
  ]
  node [
    id 2207
    label "zapaleniec"
  ]
  node [
    id 2208
    label "wysy&#322;a&#263;"
  ]
  node [
    id 2209
    label "air"
  ]
  node [
    id 2210
    label "wys&#322;a&#263;"
  ]
  node [
    id 2211
    label "oddelegowa&#263;"
  ]
  node [
    id 2212
    label "oddelegowywa&#263;"
  ]
  node [
    id 2213
    label "wysy&#322;anie"
  ]
  node [
    id 2214
    label "wys&#322;anie"
  ]
  node [
    id 2215
    label "delegacy"
  ]
  node [
    id 2216
    label "oddelegowywanie"
  ]
  node [
    id 2217
    label "oddelegowanie"
  ]
  node [
    id 2218
    label "za&#322;amanie_si&#281;"
  ]
  node [
    id 2219
    label "choroba_ducha"
  ]
  node [
    id 2220
    label "nier&#243;wno&#347;&#263;"
  ]
  node [
    id 2221
    label "zniszczenie"
  ]
  node [
    id 2222
    label "zagi&#281;cie"
  ]
  node [
    id 2223
    label "dislocation"
  ]
  node [
    id 2224
    label "choroba_psychiczna"
  ]
  node [
    id 2225
    label "przygn&#281;bienie"
  ]
  node [
    id 2226
    label "deprecha"
  ]
  node [
    id 2227
    label "zesp&#243;&#322;_napi&#281;cia_przedmiesi&#261;czkowego"
  ]
  node [
    id 2228
    label "p&#281;kni&#281;cie"
  ]
  node [
    id 2229
    label "melancholy"
  ]
  node [
    id 2230
    label "smutek"
  ]
  node [
    id 2231
    label "deprymuj&#261;co"
  ]
  node [
    id 2232
    label "devastation"
  ]
  node [
    id 2233
    label "r&#243;&#380;nica"
  ]
  node [
    id 2234
    label "imbalance"
  ]
  node [
    id 2235
    label "brzydota"
  ]
  node [
    id 2236
    label "krzywda"
  ]
  node [
    id 2237
    label "wear"
  ]
  node [
    id 2238
    label "destruction"
  ]
  node [
    id 2239
    label "zu&#380;ycie"
  ]
  node [
    id 2240
    label "attrition"
  ]
  node [
    id 2241
    label "os&#322;abienie"
  ]
  node [
    id 2242
    label "podpalenie"
  ]
  node [
    id 2243
    label "strata"
  ]
  node [
    id 2244
    label "kondycja_fizyczna"
  ]
  node [
    id 2245
    label "spl&#261;drowanie"
  ]
  node [
    id 2246
    label "zdrowie"
  ]
  node [
    id 2247
    label "poniszczenie"
  ]
  node [
    id 2248
    label "ruin"
  ]
  node [
    id 2249
    label "stanie_si&#281;"
  ]
  node [
    id 2250
    label "poniszczenie_si&#281;"
  ]
  node [
    id 2251
    label "uchylenie"
  ]
  node [
    id 2252
    label "zaskoczenie"
  ]
  node [
    id 2253
    label "zawstydzenie"
  ]
  node [
    id 2254
    label "zajechanie"
  ]
  node [
    id 2255
    label "bending"
  ]
  node [
    id 2256
    label "zniszczenie_si&#281;"
  ]
  node [
    id 2257
    label "sp&#281;kanie_ciosowe"
  ]
  node [
    id 2258
    label "pop&#281;kanie"
  ]
  node [
    id 2259
    label "fracture"
  ]
  node [
    id 2260
    label "interstice"
  ]
  node [
    id 2261
    label "wyko&#324;czenie"
  ]
  node [
    id 2262
    label "uszkodzenie"
  ]
  node [
    id 2263
    label "slit"
  ]
  node [
    id 2264
    label "rozerwanie_si&#281;"
  ]
  node [
    id 2265
    label "explosion"
  ]
  node [
    id 2266
    label "przestraszenie_si&#281;"
  ]
  node [
    id 2267
    label "jako&#347;&#263;"
  ]
  node [
    id 2268
    label "punkt_widzenia"
  ]
  node [
    id 2269
    label "wyk&#322;adnik"
  ]
  node [
    id 2270
    label "faza"
  ]
  node [
    id 2271
    label "szczebel"
  ]
  node [
    id 2272
    label "budynek"
  ]
  node [
    id 2273
    label "wysoko&#347;&#263;"
  ]
  node [
    id 2274
    label "ranga"
  ]
  node [
    id 2275
    label "wymiar"
  ]
  node [
    id 2276
    label "surface"
  ]
  node [
    id 2277
    label "kwadrant"
  ]
  node [
    id 2278
    label "degree"
  ]
  node [
    id 2279
    label "p&#243;&#322;p&#322;aszczyzna"
  ]
  node [
    id 2280
    label "powierzchnia"
  ]
  node [
    id 2281
    label "ukszta&#322;towanie"
  ]
  node [
    id 2282
    label "p&#322;aszczak"
  ]
  node [
    id 2283
    label "tallness"
  ]
  node [
    id 2284
    label "altitude"
  ]
  node [
    id 2285
    label "rozmiar"
  ]
  node [
    id 2286
    label "odcinek"
  ]
  node [
    id 2287
    label "k&#261;t"
  ]
  node [
    id 2288
    label "brzmienie"
  ]
  node [
    id 2289
    label "sum"
  ]
  node [
    id 2290
    label "przenocowanie"
  ]
  node [
    id 2291
    label "pora&#380;ka"
  ]
  node [
    id 2292
    label "nak&#322;adzenie"
  ]
  node [
    id 2293
    label "pouk&#322;adanie"
  ]
  node [
    id 2294
    label "zepsucie"
  ]
  node [
    id 2295
    label "ustawienie"
  ]
  node [
    id 2296
    label "trim"
  ]
  node [
    id 2297
    label "decyzja_lokalizacyjna"
  ]
  node [
    id 2298
    label "ugoszczenie"
  ]
  node [
    id 2299
    label "le&#380;enie"
  ]
  node [
    id 2300
    label "zbudowanie"
  ]
  node [
    id 2301
    label "reading"
  ]
  node [
    id 2302
    label "zabicie"
  ]
  node [
    id 2303
    label "wygranie"
  ]
  node [
    id 2304
    label "presentation"
  ]
  node [
    id 2305
    label "le&#380;e&#263;"
  ]
  node [
    id 2306
    label "wska&#378;nik"
  ]
  node [
    id 2307
    label "exponent"
  ]
  node [
    id 2308
    label "warto&#347;&#263;"
  ]
  node [
    id 2309
    label "quality"
  ]
  node [
    id 2310
    label "co&#347;"
  ]
  node [
    id 2311
    label "syf"
  ]
  node [
    id 2312
    label "stopie&#324;"
  ]
  node [
    id 2313
    label "drabina"
  ]
  node [
    id 2314
    label "gradation"
  ]
  node [
    id 2315
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 2316
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 2317
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 2318
    label "praktyka"
  ]
  node [
    id 2319
    label "przeorientowywanie"
  ]
  node [
    id 2320
    label "studia"
  ]
  node [
    id 2321
    label "linia"
  ]
  node [
    id 2322
    label "skr&#281;canie"
  ]
  node [
    id 2323
    label "skr&#281;ca&#263;"
  ]
  node [
    id 2324
    label "przeorientowywa&#263;"
  ]
  node [
    id 2325
    label "orientowanie"
  ]
  node [
    id 2326
    label "skr&#281;ci&#263;"
  ]
  node [
    id 2327
    label "przeorientowanie"
  ]
  node [
    id 2328
    label "zorientowanie"
  ]
  node [
    id 2329
    label "przeorientowa&#263;"
  ]
  node [
    id 2330
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 2331
    label "ty&#322;"
  ]
  node [
    id 2332
    label "zorientowa&#263;"
  ]
  node [
    id 2333
    label "g&#243;ra"
  ]
  node [
    id 2334
    label "orientowa&#263;"
  ]
  node [
    id 2335
    label "ideologia"
  ]
  node [
    id 2336
    label "orientacja"
  ]
  node [
    id 2337
    label "prz&#243;d"
  ]
  node [
    id 2338
    label "skr&#281;cenie"
  ]
  node [
    id 2339
    label "cykl_astronomiczny"
  ]
  node [
    id 2340
    label "coil"
  ]
  node [
    id 2341
    label "fotoelement"
  ]
  node [
    id 2342
    label "komutowanie"
  ]
  node [
    id 2343
    label "stan_skupienia"
  ]
  node [
    id 2344
    label "nastr&#243;j"
  ]
  node [
    id 2345
    label "przerywacz"
  ]
  node [
    id 2346
    label "uk&#322;ad_fizyczny"
  ]
  node [
    id 2347
    label "po&#322;&#261;czenie_elektryczne"
  ]
  node [
    id 2348
    label "kraw&#281;d&#378;"
  ]
  node [
    id 2349
    label "obsesja"
  ]
  node [
    id 2350
    label "dw&#243;jnik"
  ]
  node [
    id 2351
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 2352
    label "okres"
  ]
  node [
    id 2353
    label "przyrz&#261;d_elektryczny"
  ]
  node [
    id 2354
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 2355
    label "przew&#243;d"
  ]
  node [
    id 2356
    label "przew&#243;d_elektryczny"
  ]
  node [
    id 2357
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 2358
    label "obw&#243;d"
  ]
  node [
    id 2359
    label "komutowa&#263;"
  ]
  node [
    id 2360
    label "Rzym_Zachodni"
  ]
  node [
    id 2361
    label "Rzym_Wschodni"
  ]
  node [
    id 2362
    label "numer"
  ]
  node [
    id 2363
    label "stopie&#324;_s&#322;u&#380;bowy"
  ]
  node [
    id 2364
    label "balkon"
  ]
  node [
    id 2365
    label "budowla"
  ]
  node [
    id 2366
    label "kondygnacja"
  ]
  node [
    id 2367
    label "skrzyd&#322;o"
  ]
  node [
    id 2368
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 2369
    label "dach"
  ]
  node [
    id 2370
    label "strop"
  ]
  node [
    id 2371
    label "klatka_schodowa"
  ]
  node [
    id 2372
    label "przedpro&#380;e"
  ]
  node [
    id 2373
    label "Pentagon"
  ]
  node [
    id 2374
    label "alkierz"
  ]
  node [
    id 2375
    label "arise"
  ]
  node [
    id 2376
    label "zaopatrywa&#263;_si&#281;"
  ]
  node [
    id 2377
    label "porywa&#263;"
  ]
  node [
    id 2378
    label "wchodzi&#263;"
  ]
  node [
    id 2379
    label "poczytywa&#263;"
  ]
  node [
    id 2380
    label "levy"
  ]
  node [
    id 2381
    label "raise"
  ]
  node [
    id 2382
    label "pokonywa&#263;"
  ]
  node [
    id 2383
    label "buzowa&#263;_si&#281;"
  ]
  node [
    id 2384
    label "rucha&#263;"
  ]
  node [
    id 2385
    label "prowadzi&#263;"
  ]
  node [
    id 2386
    label "za&#380;ywa&#263;"
  ]
  node [
    id 2387
    label "&#263;pa&#263;"
  ]
  node [
    id 2388
    label "interpretowa&#263;"
  ]
  node [
    id 2389
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 2390
    label "rusza&#263;"
  ]
  node [
    id 2391
    label "grza&#263;"
  ]
  node [
    id 2392
    label "wch&#322;ania&#263;"
  ]
  node [
    id 2393
    label "wygrywa&#263;"
  ]
  node [
    id 2394
    label "ucieka&#263;"
  ]
  node [
    id 2395
    label "uprawia&#263;_seks"
  ]
  node [
    id 2396
    label "abstract"
  ]
  node [
    id 2397
    label "towarzystwo"
  ]
  node [
    id 2398
    label "zalicza&#263;"
  ]
  node [
    id 2399
    label "open"
  ]
  node [
    id 2400
    label "wzi&#261;&#263;"
  ]
  node [
    id 2401
    label "&#322;apa&#263;"
  ]
  node [
    id 2402
    label "przewa&#380;a&#263;"
  ]
  node [
    id 2403
    label "wzmaga&#263;_si&#281;"
  ]
  node [
    id 2404
    label "odurza&#263;_si&#281;"
  ]
  node [
    id 2405
    label "&#347;lad"
  ]
  node [
    id 2406
    label "lobbysta"
  ]
  node [
    id 2407
    label "doch&#243;d_narodowy"
  ]
  node [
    id 2408
    label "typ"
  ]
  node [
    id 2409
    label "event"
  ]
  node [
    id 2410
    label "boski"
  ]
  node [
    id 2411
    label "krajobraz"
  ]
  node [
    id 2412
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 2413
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 2414
    label "przywidzenie"
  ]
  node [
    id 2415
    label "presence"
  ]
  node [
    id 2416
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 2417
    label "wynie&#347;&#263;"
  ]
  node [
    id 2418
    label "pieni&#261;dze"
  ]
  node [
    id 2419
    label "limit"
  ]
  node [
    id 2420
    label "wynosi&#263;"
  ]
  node [
    id 2421
    label "sznurowanie"
  ]
  node [
    id 2422
    label "odrobina"
  ]
  node [
    id 2423
    label "skutek"
  ]
  node [
    id 2424
    label "sznurowa&#263;"
  ]
  node [
    id 2425
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 2426
    label "odcisk"
  ]
  node [
    id 2427
    label "przedstawiciel"
  ]
  node [
    id 2428
    label "grupa_nacisku"
  ]
  node [
    id 2429
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 2430
    label "zaatakowa&#263;"
  ]
  node [
    id 2431
    label "supervene"
  ]
  node [
    id 2432
    label "nacisn&#261;&#263;"
  ]
  node [
    id 2433
    label "gamble"
  ]
  node [
    id 2434
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 2435
    label "zach&#281;ci&#263;"
  ]
  node [
    id 2436
    label "nadusi&#263;"
  ]
  node [
    id 2437
    label "spowodowa&#263;"
  ]
  node [
    id 2438
    label "nak&#322;oni&#263;"
  ]
  node [
    id 2439
    label "tug"
  ]
  node [
    id 2440
    label "cram"
  ]
  node [
    id 2441
    label "przeby&#263;"
  ]
  node [
    id 2442
    label "spell"
  ]
  node [
    id 2443
    label "postara&#263;_si&#281;"
  ]
  node [
    id 2444
    label "rozegra&#263;"
  ]
  node [
    id 2445
    label "zrobi&#263;"
  ]
  node [
    id 2446
    label "anoint"
  ]
  node [
    id 2447
    label "spr&#243;bowa&#263;"
  ]
  node [
    id 2448
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 2449
    label "skrytykowa&#263;"
  ]
  node [
    id 2450
    label "signal"
  ]
  node [
    id 2451
    label "przewidywanie"
  ]
  node [
    id 2452
    label "oznaka"
  ]
  node [
    id 2453
    label "zawiadomienie"
  ]
  node [
    id 2454
    label "declaration"
  ]
  node [
    id 2455
    label "implikowa&#263;"
  ]
  node [
    id 2456
    label "symbol"
  ]
  node [
    id 2457
    label "announcement"
  ]
  node [
    id 2458
    label "poinformowanie"
  ]
  node [
    id 2459
    label "komunikat"
  ]
  node [
    id 2460
    label "providence"
  ]
  node [
    id 2461
    label "spodziewanie_si&#281;"
  ]
  node [
    id 2462
    label "zamierzanie"
  ]
  node [
    id 2463
    label "przybli&#380;enie"
  ]
  node [
    id 2464
    label "administracja_rz&#261;dowa"
  ]
  node [
    id 2465
    label "kategoria"
  ]
  node [
    id 2466
    label "szpaler"
  ]
  node [
    id 2467
    label "lon&#380;a"
  ]
  node [
    id 2468
    label "uporz&#261;dkowanie"
  ]
  node [
    id 2469
    label "jednostka_systematyczna"
  ]
  node [
    id 2470
    label "premier"
  ]
  node [
    id 2471
    label "Londyn"
  ]
  node [
    id 2472
    label "gabinet_cieni"
  ]
  node [
    id 2473
    label "gromada"
  ]
  node [
    id 2474
    label "number"
  ]
  node [
    id 2475
    label "Konsulat"
  ]
  node [
    id 2476
    label "tract"
  ]
  node [
    id 2477
    label "structure"
  ]
  node [
    id 2478
    label "sequence"
  ]
  node [
    id 2479
    label "succession"
  ]
  node [
    id 2480
    label "zapoznanie"
  ]
  node [
    id 2481
    label "podanie"
  ]
  node [
    id 2482
    label "bliski"
  ]
  node [
    id 2483
    label "wyja&#347;nienie"
  ]
  node [
    id 2484
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 2485
    label "approach"
  ]
  node [
    id 2486
    label "pickup"
  ]
  node [
    id 2487
    label "estimate"
  ]
  node [
    id 2488
    label "po&#322;&#261;czenie"
  ]
  node [
    id 2489
    label "ocena"
  ]
  node [
    id 2490
    label "type"
  ]
  node [
    id 2491
    label "obrady"
  ]
  node [
    id 2492
    label "executive"
  ]
  node [
    id 2493
    label "federacja"
  ]
  node [
    id 2494
    label "przej&#347;cie"
  ]
  node [
    id 2495
    label "espalier"
  ]
  node [
    id 2496
    label "aleja"
  ]
  node [
    id 2497
    label "zoologia"
  ]
  node [
    id 2498
    label "kr&#243;lestwo"
  ]
  node [
    id 2499
    label "stage_set"
  ]
  node [
    id 2500
    label "tribe"
  ]
  node [
    id 2501
    label "hurma"
  ]
  node [
    id 2502
    label "botanika"
  ]
  node [
    id 2503
    label "wagon"
  ]
  node [
    id 2504
    label "mecz_mistrzowski"
  ]
  node [
    id 2505
    label "arrangement"
  ]
  node [
    id 2506
    label "class"
  ]
  node [
    id 2507
    label "&#322;awka"
  ]
  node [
    id 2508
    label "wykrzyknik"
  ]
  node [
    id 2509
    label "zaleta"
  ]
  node [
    id 2510
    label "programowanie_obiektowe"
  ]
  node [
    id 2511
    label "tablica"
  ]
  node [
    id 2512
    label "rezerwa"
  ]
  node [
    id 2513
    label "Ekwici"
  ]
  node [
    id 2514
    label "sala"
  ]
  node [
    id 2515
    label "pomoc"
  ]
  node [
    id 2516
    label "form"
  ]
  node [
    id 2517
    label "znak_jako&#347;ci"
  ]
  node [
    id 2518
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 2519
    label "promocja"
  ]
  node [
    id 2520
    label "kurs"
  ]
  node [
    id 2521
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 2522
    label "dziennik_lekcyjny"
  ]
  node [
    id 2523
    label "fakcja"
  ]
  node [
    id 2524
    label "atak"
  ]
  node [
    id 2525
    label "lina"
  ]
  node [
    id 2526
    label "sprz&#281;t_wspinaczkowy"
  ]
  node [
    id 2527
    label "Bismarck"
  ]
  node [
    id 2528
    label "Sto&#322;ypin"
  ]
  node [
    id 2529
    label "Chruszczow"
  ]
  node [
    id 2530
    label "Jelcyn"
  ]
  node [
    id 2531
    label "panowanie"
  ]
  node [
    id 2532
    label "Kreml"
  ]
  node [
    id 2533
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 2534
    label "wydolno&#347;&#263;"
  ]
  node [
    id 2535
    label "Wimbledon"
  ]
  node [
    id 2536
    label "Westminster"
  ]
  node [
    id 2537
    label "Londek"
  ]
  node [
    id 2538
    label "obowi&#261;zuj&#261;cy"
  ]
  node [
    id 2539
    label "transakcja"
  ]
  node [
    id 2540
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 2541
    label "rozliczenie"
  ]
  node [
    id 2542
    label "wynios&#322;y"
  ]
  node [
    id 2543
    label "dono&#347;ny"
  ]
  node [
    id 2544
    label "wa&#380;nie"
  ]
  node [
    id 2545
    label "istotnie"
  ]
  node [
    id 2546
    label "eksponowany"
  ]
  node [
    id 2547
    label "ukszta&#322;towany"
  ]
  node [
    id 2548
    label "do&#347;cig&#322;y"
  ]
  node [
    id 2549
    label "zdr&#243;w"
  ]
  node [
    id 2550
    label "dorodnie"
  ]
  node [
    id 2551
    label "okaza&#322;y"
  ]
  node [
    id 2552
    label "wiela"
  ]
  node [
    id 2553
    label "cz&#281;sto"
  ]
  node [
    id 2554
    label "liga"
  ]
  node [
    id 2555
    label "Entuzjastki"
  ]
  node [
    id 2556
    label "kompozycja"
  ]
  node [
    id 2557
    label "Terranie"
  ]
  node [
    id 2558
    label "category"
  ]
  node [
    id 2559
    label "pakiet_klimatyczny"
  ]
  node [
    id 2560
    label "oddzia&#322;"
  ]
  node [
    id 2561
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 2562
    label "specgrupa"
  ]
  node [
    id 2563
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 2564
    label "Eurogrupa"
  ]
  node [
    id 2565
    label "formacja_geologiczna"
  ]
  node [
    id 2566
    label "harcerze_starsi"
  ]
  node [
    id 2567
    label "konfiguracja"
  ]
  node [
    id 2568
    label "cz&#261;stka"
  ]
  node [
    id 2569
    label "masa_cz&#261;steczkowa"
  ]
  node [
    id 2570
    label "diadochia"
  ]
  node [
    id 2571
    label "grupa_funkcyjna"
  ]
  node [
    id 2572
    label "integer"
  ]
  node [
    id 2573
    label "zlewanie_si&#281;"
  ]
  node [
    id 2574
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 2575
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 2576
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 2577
    label "series"
  ]
  node [
    id 2578
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 2579
    label "uprawianie"
  ]
  node [
    id 2580
    label "praca_rolnicza"
  ]
  node [
    id 2581
    label "dane"
  ]
  node [
    id 2582
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 2583
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 2584
    label "gathering"
  ]
  node [
    id 2585
    label "album"
  ]
  node [
    id 2586
    label "lias"
  ]
  node [
    id 2587
    label "pi&#281;tro"
  ]
  node [
    id 2588
    label "jednostka_geologiczna"
  ]
  node [
    id 2589
    label "filia"
  ]
  node [
    id 2590
    label "malm"
  ]
  node [
    id 2591
    label "dogger"
  ]
  node [
    id 2592
    label "bank"
  ]
  node [
    id 2593
    label "ajencja"
  ]
  node [
    id 2594
    label "przegrupowywa&#263;_si&#281;"
  ]
  node [
    id 2595
    label "przegrupowywanie_si&#281;"
  ]
  node [
    id 2596
    label "szpital"
  ]
  node [
    id 2597
    label "blend"
  ]
  node [
    id 2598
    label "dzie&#322;o"
  ]
  node [
    id 2599
    label "figuracja"
  ]
  node [
    id 2600
    label "chwyt"
  ]
  node [
    id 2601
    label "okup"
  ]
  node [
    id 2602
    label "muzykologia"
  ]
  node [
    id 2603
    label "&#347;redniowiecze"
  ]
  node [
    id 2604
    label "czynnik_biotyczny"
  ]
  node [
    id 2605
    label "wyewoluowanie"
  ]
  node [
    id 2606
    label "individual"
  ]
  node [
    id 2607
    label "przyswoi&#263;"
  ]
  node [
    id 2608
    label "starzenie_si&#281;"
  ]
  node [
    id 2609
    label "wyewoluowa&#263;"
  ]
  node [
    id 2610
    label "okaz"
  ]
  node [
    id 2611
    label "part"
  ]
  node [
    id 2612
    label "przyswojenie"
  ]
  node [
    id 2613
    label "ewoluowanie"
  ]
  node [
    id 2614
    label "ewoluowa&#263;"
  ]
  node [
    id 2615
    label "sztuka"
  ]
  node [
    id 2616
    label "agent"
  ]
  node [
    id 2617
    label "przyswaja&#263;"
  ]
  node [
    id 2618
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 2619
    label "nicpo&#324;"
  ]
  node [
    id 2620
    label "przyswajanie"
  ]
  node [
    id 2621
    label "feminizm"
  ]
  node [
    id 2622
    label "Unia_Europejska"
  ]
  node [
    id 2623
    label "uatrakcyjni&#263;"
  ]
  node [
    id 2624
    label "przewietrzy&#263;"
  ]
  node [
    id 2625
    label "regenerate"
  ]
  node [
    id 2626
    label "odtworzy&#263;"
  ]
  node [
    id 2627
    label "wymieni&#263;"
  ]
  node [
    id 2628
    label "odbudowa&#263;"
  ]
  node [
    id 2629
    label "odbudowywa&#263;"
  ]
  node [
    id 2630
    label "m&#322;odzi&#263;"
  ]
  node [
    id 2631
    label "uatrakcyjnia&#263;"
  ]
  node [
    id 2632
    label "przewietrza&#263;"
  ]
  node [
    id 2633
    label "wymienia&#263;"
  ]
  node [
    id 2634
    label "odtwarza&#263;"
  ]
  node [
    id 2635
    label "odtwarzanie"
  ]
  node [
    id 2636
    label "uatrakcyjnianie"
  ]
  node [
    id 2637
    label "zast&#281;powanie"
  ]
  node [
    id 2638
    label "odbudowywanie"
  ]
  node [
    id 2639
    label "rejuvenation"
  ]
  node [
    id 2640
    label "m&#322;odszy"
  ]
  node [
    id 2641
    label "wymienienie"
  ]
  node [
    id 2642
    label "uatrakcyjnienie"
  ]
  node [
    id 2643
    label "odbudowanie"
  ]
  node [
    id 2644
    label "odtworzenie"
  ]
  node [
    id 2645
    label "asymilowanie_si&#281;"
  ]
  node [
    id 2646
    label "absorption"
  ]
  node [
    id 2647
    label "pobieranie"
  ]
  node [
    id 2648
    label "czerpanie"
  ]
  node [
    id 2649
    label "acquisition"
  ]
  node [
    id 2650
    label "organizm"
  ]
  node [
    id 2651
    label "assimilation"
  ]
  node [
    id 2652
    label "upodabnianie"
  ]
  node [
    id 2653
    label "g&#322;oska"
  ]
  node [
    id 2654
    label "kultura"
  ]
  node [
    id 2655
    label "fonetyka"
  ]
  node [
    id 2656
    label "moneta"
  ]
  node [
    id 2657
    label "union"
  ]
  node [
    id 2658
    label "assimilate"
  ]
  node [
    id 2659
    label "dostosowywa&#263;"
  ]
  node [
    id 2660
    label "dostosowa&#263;"
  ]
  node [
    id 2661
    label "przejmowa&#263;"
  ]
  node [
    id 2662
    label "upodobni&#263;"
  ]
  node [
    id 2663
    label "przej&#261;&#263;"
  ]
  node [
    id 2664
    label "upodabnia&#263;"
  ]
  node [
    id 2665
    label "pobiera&#263;"
  ]
  node [
    id 2666
    label "pobra&#263;"
  ]
  node [
    id 2667
    label "spo&#322;ecznie"
  ]
  node [
    id 2668
    label "niepubliczny"
  ]
  node [
    id 2669
    label "og&#243;lnie"
  ]
  node [
    id 2670
    label "w_pizdu"
  ]
  node [
    id 2671
    label "kompletnie"
  ]
  node [
    id 2672
    label "&#322;&#261;czny"
  ]
  node [
    id 2673
    label "zupe&#322;nie"
  ]
  node [
    id 2674
    label "jedyny"
  ]
  node [
    id 2675
    label "calu&#347;ko"
  ]
  node [
    id 2676
    label "&#380;ywy"
  ]
  node [
    id 2677
    label "ca&#322;o"
  ]
  node [
    id 2678
    label "&#322;&#261;cznie"
  ]
  node [
    id 2679
    label "zbiorczy"
  ]
  node [
    id 2680
    label "nadrz&#281;dnie"
  ]
  node [
    id 2681
    label "posp&#243;lnie"
  ]
  node [
    id 2682
    label "zbiorowo"
  ]
  node [
    id 2683
    label "generalny"
  ]
  node [
    id 2684
    label "wniwecz"
  ]
  node [
    id 2685
    label "ranny"
  ]
  node [
    id 2686
    label "jajko"
  ]
  node [
    id 2687
    label "zgromadzenie"
  ]
  node [
    id 2688
    label "urodzenie"
  ]
  node [
    id 2689
    label "suspension"
  ]
  node [
    id 2690
    label "poddanie_si&#281;"
  ]
  node [
    id 2691
    label "extinction"
  ]
  node [
    id 2692
    label "przetrwanie"
  ]
  node [
    id 2693
    label "&#347;cierpienie"
  ]
  node [
    id 2694
    label "abolicjonista"
  ]
  node [
    id 2695
    label "posk&#322;adanie"
  ]
  node [
    id 2696
    label "zebranie"
  ]
  node [
    id 2697
    label "przeniesienie"
  ]
  node [
    id 2698
    label "removal"
  ]
  node [
    id 2699
    label "withdrawal"
  ]
  node [
    id 2700
    label "revocation"
  ]
  node [
    id 2701
    label "porwanie"
  ]
  node [
    id 2702
    label "uniewa&#380;nienie"
  ]
  node [
    id 2703
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 2704
    label "beat"
  ]
  node [
    id 2705
    label "zwojowanie"
  ]
  node [
    id 2706
    label "wr&#243;cenie_z_tarcz&#261;"
  ]
  node [
    id 2707
    label "zapanowanie"
  ]
  node [
    id 2708
    label "wygrywanie"
  ]
  node [
    id 2709
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 2710
    label "concourse"
  ]
  node [
    id 2711
    label "templum"
  ]
  node [
    id 2712
    label "spotkanie"
  ]
  node [
    id 2713
    label "konwentykiel"
  ]
  node [
    id 2714
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 2715
    label "pozyskanie"
  ]
  node [
    id 2716
    label "wzi&#281;cie"
  ]
  node [
    id 2717
    label "sprz&#261;tni&#281;cie"
  ]
  node [
    id 2718
    label "&#347;ci&#261;gni&#281;cie"
  ]
  node [
    id 2719
    label "caucus"
  ]
  node [
    id 2720
    label "merging"
  ]
  node [
    id 2721
    label "party"
  ]
  node [
    id 2722
    label "ocenienie"
  ]
  node [
    id 2723
    label "gather"
  ]
  node [
    id 2724
    label "porodzenie"
  ]
  node [
    id 2725
    label "narodzenie"
  ]
  node [
    id 2726
    label "pocz&#261;tek"
  ]
  node [
    id 2727
    label "urodzenie_si&#281;"
  ]
  node [
    id 2728
    label "powicie"
  ]
  node [
    id 2729
    label "donoszenie"
  ]
  node [
    id 2730
    label "zlegni&#281;cie"
  ]
  node [
    id 2731
    label "beginning"
  ]
  node [
    id 2732
    label "wsp&#243;lnota"
  ]
  node [
    id 2733
    label "gromadzenie"
  ]
  node [
    id 2734
    label "klasztor"
  ]
  node [
    id 2735
    label "kongregacja"
  ]
  node [
    id 2736
    label "survival"
  ]
  node [
    id 2737
    label "pozostanie"
  ]
  node [
    id 2738
    label "experience"
  ]
  node [
    id 2739
    label "wyniesienie"
  ]
  node [
    id 2740
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 2741
    label "odej&#347;cie"
  ]
  node [
    id 2742
    label "pozabieranie"
  ]
  node [
    id 2743
    label "pozbycie_si&#281;"
  ]
  node [
    id 2744
    label "pousuwanie"
  ]
  node [
    id 2745
    label "przesuni&#281;cie"
  ]
  node [
    id 2746
    label "znikni&#281;cie"
  ]
  node [
    id 2747
    label "wyrugowanie"
  ]
  node [
    id 2748
    label "nak&#322;onienie"
  ]
  node [
    id 2749
    label "przest&#281;pstwo"
  ]
  node [
    id 2750
    label "catch"
  ]
  node [
    id 2751
    label "poruszenie"
  ]
  node [
    id 2752
    label "rozerwanie"
  ]
  node [
    id 2753
    label "hijack"
  ]
  node [
    id 2754
    label "powyrywanie"
  ]
  node [
    id 2755
    label "bust"
  ]
  node [
    id 2756
    label "kidnapping"
  ]
  node [
    id 2757
    label "dostosowanie"
  ]
  node [
    id 2758
    label "przesuni&#281;cie_si&#281;"
  ]
  node [
    id 2759
    label "rozpowszechnienie"
  ]
  node [
    id 2760
    label "skopiowanie"
  ]
  node [
    id 2761
    label "transfer"
  ]
  node [
    id 2762
    label "move"
  ]
  node [
    id 2763
    label "pocisk"
  ]
  node [
    id 2764
    label "assignment"
  ]
  node [
    id 2765
    label "przelecenie"
  ]
  node [
    id 2766
    label "mechanizm_obronny"
  ]
  node [
    id 2767
    label "strzelenie"
  ]
  node [
    id 2768
    label "przesadzenie"
  ]
  node [
    id 2769
    label "poprzesuwanie"
  ]
  node [
    id 2770
    label "retraction"
  ]
  node [
    id 2771
    label "uniewa&#380;nienie_si&#281;"
  ]
  node [
    id 2772
    label "zerwanie"
  ]
  node [
    id 2773
    label "znoszenie"
  ]
  node [
    id 2774
    label "nap&#322;ywanie"
  ]
  node [
    id 2775
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 2776
    label "znosi&#263;"
  ]
  node [
    id 2777
    label "znie&#347;&#263;"
  ]
  node [
    id 2778
    label "zarys"
  ]
  node [
    id 2779
    label "informacja"
  ]
  node [
    id 2780
    label "depesza_emska"
  ]
  node [
    id 2781
    label "ball"
  ]
  node [
    id 2782
    label "kszta&#322;t"
  ]
  node [
    id 2783
    label "nabia&#322;"
  ]
  node [
    id 2784
    label "pisanka"
  ]
  node [
    id 2785
    label "jajo"
  ]
  node [
    id 2786
    label "bia&#322;ko"
  ]
  node [
    id 2787
    label "rozbijarka"
  ]
  node [
    id 2788
    label "wyt&#322;aczanka"
  ]
  node [
    id 2789
    label "owoskop"
  ]
  node [
    id 2790
    label "ryboflawina"
  ]
  node [
    id 2791
    label "produkt"
  ]
  node [
    id 2792
    label "skorupka"
  ]
  node [
    id 2793
    label "&#380;&#243;&#322;tko"
  ]
  node [
    id 2794
    label "porannie"
  ]
  node [
    id 2795
    label "zaranny"
  ]
  node [
    id 2796
    label "poszkodowany"
  ]
  node [
    id 2797
    label "specjalny"
  ]
  node [
    id 2798
    label "przeciwnik"
  ]
  node [
    id 2799
    label "zwolennik"
  ]
  node [
    id 2800
    label "czarnosk&#243;ry"
  ]
  node [
    id 2801
    label "z&#322;&#261;czenie"
  ]
  node [
    id 2802
    label "wp&#322;acenie"
  ]
  node [
    id 2803
    label "u&#322;o&#380;enie"
  ]
  node [
    id 2804
    label "odprowadza&#263;"
  ]
  node [
    id 2805
    label "drain"
  ]
  node [
    id 2806
    label "zwi&#261;zek_chemiczny"
  ]
  node [
    id 2807
    label "powodowa&#263;"
  ]
  node [
    id 2808
    label "osusza&#263;"
  ]
  node [
    id 2809
    label "odci&#261;ga&#263;"
  ]
  node [
    id 2810
    label "odsuwa&#263;"
  ]
  node [
    id 2811
    label "akt"
  ]
  node [
    id 2812
    label "cezar"
  ]
  node [
    id 2813
    label "uchwa&#322;a"
  ]
  node [
    id 2814
    label "numeracja"
  ]
  node [
    id 2815
    label "odprowadzanie"
  ]
  node [
    id 2816
    label "odci&#261;ganie"
  ]
  node [
    id 2817
    label "dehydratacja"
  ]
  node [
    id 2818
    label "osuszanie"
  ]
  node [
    id 2819
    label "proces_chemiczny"
  ]
  node [
    id 2820
    label "odsuwanie"
  ]
  node [
    id 2821
    label "odsun&#261;&#263;"
  ]
  node [
    id 2822
    label "odprowadzi&#263;"
  ]
  node [
    id 2823
    label "osuszy&#263;"
  ]
  node [
    id 2824
    label "odci&#261;gn&#261;&#263;"
  ]
  node [
    id 2825
    label "choroba_lasu_Kyasanur"
  ]
  node [
    id 2826
    label "dehydration"
  ]
  node [
    id 2827
    label "osuszenie"
  ]
  node [
    id 2828
    label "omska_gor&#261;czka_krwotoczna"
  ]
  node [
    id 2829
    label "odprowadzenie"
  ]
  node [
    id 2830
    label "odci&#261;gni&#281;cie"
  ]
  node [
    id 2831
    label "odsuni&#281;cie"
  ]
  node [
    id 2832
    label "narta"
  ]
  node [
    id 2833
    label "podwi&#261;zywanie"
  ]
  node [
    id 2834
    label "dressing"
  ]
  node [
    id 2835
    label "socket"
  ]
  node [
    id 2836
    label "szermierka"
  ]
  node [
    id 2837
    label "przywi&#261;zywanie"
  ]
  node [
    id 2838
    label "pakowanie"
  ]
  node [
    id 2839
    label "my&#347;lenie"
  ]
  node [
    id 2840
    label "do&#322;&#261;czanie"
  ]
  node [
    id 2841
    label "wytwarzanie"
  ]
  node [
    id 2842
    label "cement"
  ]
  node [
    id 2843
    label "ceg&#322;a"
  ]
  node [
    id 2844
    label "combination"
  ]
  node [
    id 2845
    label "zobowi&#261;zywanie"
  ]
  node [
    id 2846
    label "szcz&#281;ka"
  ]
  node [
    id 2847
    label "anga&#380;owanie"
  ]
  node [
    id 2848
    label "wi&#261;za&#263;"
  ]
  node [
    id 2849
    label "twardnienie"
  ]
  node [
    id 2850
    label "tobo&#322;ek"
  ]
  node [
    id 2851
    label "podwi&#261;zanie"
  ]
  node [
    id 2852
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 2853
    label "przywi&#261;zanie"
  ]
  node [
    id 2854
    label "przymocowywanie"
  ]
  node [
    id 2855
    label "scalanie"
  ]
  node [
    id 2856
    label "mezomeria"
  ]
  node [
    id 2857
    label "wi&#281;&#378;"
  ]
  node [
    id 2858
    label "fusion"
  ]
  node [
    id 2859
    label "kojarzenie_si&#281;"
  ]
  node [
    id 2860
    label "&#322;&#261;czenie"
  ]
  node [
    id 2861
    label "uchwyt"
  ]
  node [
    id 2862
    label "rozmieszczenie"
  ]
  node [
    id 2863
    label "warto&#347;ciowo&#347;&#263;"
  ]
  node [
    id 2864
    label "element_konstrukcyjny"
  ]
  node [
    id 2865
    label "obezw&#322;adnianie"
  ]
  node [
    id 2866
    label "miecz"
  ]
  node [
    id 2867
    label "obwi&#261;zanie"
  ]
  node [
    id 2868
    label "zawi&#261;zek"
  ]
  node [
    id 2869
    label "obwi&#261;zywanie"
  ]
  node [
    id 2870
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 2871
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 2872
    label "consort"
  ]
  node [
    id 2873
    label "opakowa&#263;"
  ]
  node [
    id 2874
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 2875
    label "relate"
  ]
  node [
    id 2876
    label "unify"
  ]
  node [
    id 2877
    label "incorporate"
  ]
  node [
    id 2878
    label "bind"
  ]
  node [
    id 2879
    label "zawi&#261;za&#263;"
  ]
  node [
    id 2880
    label "zaprawa"
  ]
  node [
    id 2881
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 2882
    label "powi&#261;za&#263;"
  ]
  node [
    id 2883
    label "scali&#263;"
  ]
  node [
    id 2884
    label "zatrzyma&#263;"
  ]
  node [
    id 2885
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 2886
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 2887
    label "ograniczenie"
  ]
  node [
    id 2888
    label "do&#322;&#261;czenie"
  ]
  node [
    id 2889
    label "opakowanie"
  ]
  node [
    id 2890
    label "obezw&#322;adnienie"
  ]
  node [
    id 2891
    label "zawi&#261;zanie"
  ]
  node [
    id 2892
    label "tying"
  ]
  node [
    id 2893
    label "st&#281;&#380;enie"
  ]
  node [
    id 2894
    label "affiliation"
  ]
  node [
    id 2895
    label "fastening"
  ]
  node [
    id 2896
    label "nawi&#261;zanie_si&#281;"
  ]
  node [
    id 2897
    label "roztw&#243;r"
  ]
  node [
    id 2898
    label "zrelatywizowa&#263;"
  ]
  node [
    id 2899
    label "zrelatywizowanie"
  ]
  node [
    id 2900
    label "mention"
  ]
  node [
    id 2901
    label "pomy&#347;lenie"
  ]
  node [
    id 2902
    label "relatywizowa&#263;"
  ]
  node [
    id 2903
    label "relatywizowanie"
  ]
  node [
    id 2904
    label "kontakt"
  ]
  node [
    id 2905
    label "wsp&#243;lny"
  ]
  node [
    id 2906
    label "koalicyjnie"
  ]
  node [
    id 2907
    label "spolny"
  ]
  node [
    id 2908
    label "wsp&#243;lnie"
  ]
  node [
    id 2909
    label "sp&#243;lny"
  ]
  node [
    id 2910
    label "uwsp&#243;lnienie"
  ]
  node [
    id 2911
    label "uwsp&#243;lnianie"
  ]
  node [
    id 2912
    label "flare"
  ]
  node [
    id 2913
    label "zdolno&#347;&#263;"
  ]
  node [
    id 2914
    label "posiada&#263;"
  ]
  node [
    id 2915
    label "potencja&#322;"
  ]
  node [
    id 2916
    label "zapomina&#263;"
  ]
  node [
    id 2917
    label "zapomnienie"
  ]
  node [
    id 2918
    label "zapominanie"
  ]
  node [
    id 2919
    label "ability"
  ]
  node [
    id 2920
    label "obliczeniowo"
  ]
  node [
    id 2921
    label "zapomnie&#263;"
  ]
  node [
    id 2922
    label "odpowiedzialnie"
  ]
  node [
    id 2923
    label "sprawca"
  ]
  node [
    id 2924
    label "&#347;wiadomy"
  ]
  node [
    id 2925
    label "przewinienie"
  ]
  node [
    id 2926
    label "odpowiadanie"
  ]
  node [
    id 2927
    label "ci&#281;&#380;ki"
  ]
  node [
    id 2928
    label "spowa&#380;nienie"
  ]
  node [
    id 2929
    label "ci&#281;&#380;ko"
  ]
  node [
    id 2930
    label "gro&#378;ny"
  ]
  node [
    id 2931
    label "powa&#380;nie"
  ]
  node [
    id 2932
    label "powa&#380;nienie"
  ]
  node [
    id 2933
    label "przemy&#347;lany"
  ]
  node [
    id 2934
    label "przytomnie"
  ]
  node [
    id 2935
    label "bycie_&#347;wiadomym"
  ]
  node [
    id 2936
    label "rozs&#261;dny"
  ]
  node [
    id 2937
    label "&#347;wiadomie"
  ]
  node [
    id 2938
    label "sprawiciel"
  ]
  node [
    id 2939
    label "reagowanie"
  ]
  node [
    id 2940
    label "dawanie"
  ]
  node [
    id 2941
    label "pokutowanie"
  ]
  node [
    id 2942
    label "pytanie"
  ]
  node [
    id 2943
    label "winny"
  ]
  node [
    id 2944
    label "wypowiadanie_si&#281;"
  ]
  node [
    id 2945
    label "picie_piwa"
  ]
  node [
    id 2946
    label "parry"
  ]
  node [
    id 2947
    label "fit"
  ]
  node [
    id 2948
    label "rendition"
  ]
  node [
    id 2949
    label "ponoszenie"
  ]
  node [
    id 2950
    label "rozmawianie"
  ]
  node [
    id 2951
    label "responsibly"
  ]
  node [
    id 2952
    label "pope&#322;nienie"
  ]
  node [
    id 2953
    label "discourtesy"
  ]
  node [
    id 2954
    label "zanurzy&#263;"
  ]
  node [
    id 2955
    label "poch&#322;on&#261;&#263;"
  ]
  node [
    id 2956
    label "dip"
  ]
  node [
    id 2957
    label "zag&#322;ada"
  ]
  node [
    id 2958
    label "wprowadzi&#263;"
  ]
  node [
    id 2959
    label "set"
  ]
  node [
    id 2960
    label "wykona&#263;"
  ]
  node [
    id 2961
    label "pos&#322;a&#263;"
  ]
  node [
    id 2962
    label "have_a_bun_in_the_oven"
  ]
  node [
    id 2963
    label "poprowadzi&#263;"
  ]
  node [
    id 2964
    label "wzbudzi&#263;"
  ]
  node [
    id 2965
    label "rynek"
  ]
  node [
    id 2966
    label "insert"
  ]
  node [
    id 2967
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 2968
    label "wpisa&#263;"
  ]
  node [
    id 2969
    label "picture"
  ]
  node [
    id 2970
    label "zapozna&#263;"
  ]
  node [
    id 2971
    label "wej&#347;&#263;"
  ]
  node [
    id 2972
    label "zej&#347;&#263;"
  ]
  node [
    id 2973
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 2974
    label "umie&#347;ci&#263;"
  ]
  node [
    id 2975
    label "zacz&#261;&#263;"
  ]
  node [
    id 2976
    label "indicate"
  ]
  node [
    id 2977
    label "zainteresowa&#263;"
  ]
  node [
    id 2978
    label "spo&#380;y&#263;"
  ]
  node [
    id 2979
    label "wci&#261;gn&#261;&#263;"
  ]
  node [
    id 2980
    label "pozna&#263;"
  ]
  node [
    id 2981
    label "zabra&#263;"
  ]
  node [
    id 2982
    label "sparali&#380;owa&#263;"
  ]
  node [
    id 2983
    label "absorb"
  ]
  node [
    id 2984
    label "swallow"
  ]
  node [
    id 2985
    label "consume"
  ]
  node [
    id 2986
    label "souse"
  ]
  node [
    id 2987
    label "crash"
  ]
  node [
    id 2988
    label "pogr&#261;&#380;enie"
  ]
  node [
    id 2989
    label "ludob&#243;jstwo"
  ]
  node [
    id 2990
    label "kl&#281;ska_&#380;ywio&#322;owa"
  ]
  node [
    id 2991
    label "szmalcownik"
  ]
  node [
    id 2992
    label "holocaust"
  ]
  node [
    id 2993
    label "apokalipsa"
  ]
  node [
    id 2994
    label "negacjonizm"
  ]
  node [
    id 2995
    label "sos"
  ]
  node [
    id 2996
    label "postrzega&#263;"
  ]
  node [
    id 2997
    label "perceive"
  ]
  node [
    id 2998
    label "aprobowa&#263;"
  ]
  node [
    id 2999
    label "wzrok"
  ]
  node [
    id 3000
    label "zmale&#263;"
  ]
  node [
    id 3001
    label "male&#263;"
  ]
  node [
    id 3002
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 3003
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 3004
    label "spotka&#263;"
  ]
  node [
    id 3005
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 3006
    label "ogl&#261;da&#263;"
  ]
  node [
    id 3007
    label "dostrzega&#263;"
  ]
  node [
    id 3008
    label "go_steady"
  ]
  node [
    id 3009
    label "reagowa&#263;"
  ]
  node [
    id 3010
    label "os&#261;dza&#263;"
  ]
  node [
    id 3011
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 3012
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 3013
    label "react"
  ]
  node [
    id 3014
    label "answer"
  ]
  node [
    id 3015
    label "odpowiada&#263;"
  ]
  node [
    id 3016
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 3017
    label "obacza&#263;"
  ]
  node [
    id 3018
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 3019
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 3020
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 3021
    label "styka&#263;_si&#281;"
  ]
  node [
    id 3022
    label "visualize"
  ]
  node [
    id 3023
    label "befall"
  ]
  node [
    id 3024
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 3025
    label "znale&#378;&#263;"
  ]
  node [
    id 3026
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 3027
    label "approbate"
  ]
  node [
    id 3028
    label "uznawa&#263;"
  ]
  node [
    id 3029
    label "s&#261;dzi&#263;"
  ]
  node [
    id 3030
    label "znajdowa&#263;"
  ]
  node [
    id 3031
    label "hold"
  ]
  node [
    id 3032
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 3033
    label "nagradza&#263;"
  ]
  node [
    id 3034
    label "forytowa&#263;"
  ]
  node [
    id 3035
    label "traktowa&#263;"
  ]
  node [
    id 3036
    label "sign"
  ]
  node [
    id 3037
    label "m&#281;tnienie"
  ]
  node [
    id 3038
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 3039
    label "okulista"
  ]
  node [
    id 3040
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 3041
    label "zmys&#322;"
  ]
  node [
    id 3042
    label "expression"
  ]
  node [
    id 3043
    label "oko"
  ]
  node [
    id 3044
    label "m&#281;tnie&#263;"
  ]
  node [
    id 3045
    label "slack"
  ]
  node [
    id 3046
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 3047
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 3048
    label "relax"
  ]
  node [
    id 3049
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 3050
    label "sta&#263;_si&#281;"
  ]
  node [
    id 3051
    label "reduce"
  ]
  node [
    id 3052
    label "zmniejszy&#263;_si&#281;"
  ]
  node [
    id 3053
    label "wyb&#243;r"
  ]
  node [
    id 3054
    label "prospect"
  ]
  node [
    id 3055
    label "alternatywa"
  ]
  node [
    id 3056
    label "operator_modalny"
  ]
  node [
    id 3057
    label "moc_obliczeniowa"
  ]
  node [
    id 3058
    label "wiedzie&#263;"
  ]
  node [
    id 3059
    label "zawiera&#263;"
  ]
  node [
    id 3060
    label "mie&#263;"
  ]
  node [
    id 3061
    label "support"
  ]
  node [
    id 3062
    label "keep_open"
  ]
  node [
    id 3063
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 3064
    label "pick"
  ]
  node [
    id 3065
    label "zdecydowanie_si&#281;"
  ]
  node [
    id 3066
    label "zdecydowa&#263;_si&#281;"
  ]
  node [
    id 3067
    label "reform"
  ]
  node [
    id 3068
    label "przywr&#243;cenie"
  ]
  node [
    id 3069
    label "ponaprawianie"
  ]
  node [
    id 3070
    label "restytuowanie"
  ]
  node [
    id 3071
    label "restoration"
  ]
  node [
    id 3072
    label "niebezpieczny"
  ]
  node [
    id 3073
    label "zawodny"
  ]
  node [
    id 3074
    label "niem&#261;dry"
  ]
  node [
    id 3075
    label "nieodpowiedzialnie"
  ]
  node [
    id 3076
    label "niedojrza&#322;y"
  ]
  node [
    id 3077
    label "beztroski"
  ]
  node [
    id 3078
    label "niebezpiecznie"
  ]
  node [
    id 3079
    label "k&#322;opotliwy"
  ]
  node [
    id 3080
    label "z&#322;y"
  ]
  node [
    id 3081
    label "niem&#261;drze"
  ]
  node [
    id 3082
    label "zg&#322;upienie"
  ]
  node [
    id 3083
    label "g&#322;upienie"
  ]
  node [
    id 3084
    label "zawodnie"
  ]
  node [
    id 3085
    label "niepewny"
  ]
  node [
    id 3086
    label "beztroskliwy"
  ]
  node [
    id 3087
    label "lekko"
  ]
  node [
    id 3088
    label "letki"
  ]
  node [
    id 3089
    label "wolny"
  ]
  node [
    id 3090
    label "beztrosko"
  ]
  node [
    id 3091
    label "nierozwa&#380;ny"
  ]
  node [
    id 3092
    label "pogodny"
  ]
  node [
    id 3093
    label "niedobry"
  ]
  node [
    id 3094
    label "niegotowy"
  ]
  node [
    id 3095
    label "m&#322;ody"
  ]
  node [
    id 3096
    label "niedojrzale"
  ]
  node [
    id 3097
    label "g&#322;upi"
  ]
  node [
    id 3098
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 3099
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 3100
    label "gra"
  ]
  node [
    id 3101
    label "pocz&#261;tki"
  ]
  node [
    id 3102
    label "wzorzec_projektowy"
  ]
  node [
    id 3103
    label "doktryna"
  ]
  node [
    id 3104
    label "wrinkle"
  ]
  node [
    id 3105
    label "wojskowo&#347;&#263;"
  ]
  node [
    id 3106
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 3107
    label "zbudowa&#263;"
  ]
  node [
    id 3108
    label "krzywa"
  ]
  node [
    id 3109
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 3110
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 3111
    label "leave"
  ]
  node [
    id 3112
    label "nakre&#347;li&#263;"
  ]
  node [
    id 3113
    label "moderate"
  ]
  node [
    id 3114
    label "guidebook"
  ]
  node [
    id 3115
    label "wytworzy&#263;"
  ]
  node [
    id 3116
    label "manufacture"
  ]
  node [
    id 3117
    label "nakaza&#263;"
  ]
  node [
    id 3118
    label "pod&#322;o&#380;y&#263;"
  ]
  node [
    id 3119
    label "przekaza&#263;"
  ]
  node [
    id 3120
    label "dispatch"
  ]
  node [
    id 3121
    label "report"
  ]
  node [
    id 3122
    label "ship"
  ]
  node [
    id 3123
    label "sprz&#261;tn&#261;&#263;"
  ]
  node [
    id 3124
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 3125
    label "&#322;&#243;&#380;ko"
  ]
  node [
    id 3126
    label "post"
  ]
  node [
    id 3127
    label "wywo&#322;a&#263;"
  ]
  node [
    id 3128
    label "arouse"
  ]
  node [
    id 3129
    label "gem"
  ]
  node [
    id 3130
    label "runda"
  ]
  node [
    id 3131
    label "zestaw"
  ]
  node [
    id 3132
    label "subside"
  ]
  node [
    id 3133
    label "July"
  ]
  node [
    id 3134
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 3135
    label "drop"
  ]
  node [
    id 3136
    label "opa&#347;&#263;"
  ]
  node [
    id 3137
    label "Marzec_'68"
  ]
  node [
    id 3138
    label "ukry&#263;_si&#281;"
  ]
  node [
    id 3139
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 3140
    label "pogr&#261;&#380;y&#263;_si&#281;"
  ]
  node [
    id 3141
    label "utrwali&#263;_si&#281;"
  ]
  node [
    id 3142
    label "popa&#347;&#263;"
  ]
  node [
    id 3143
    label "sink"
  ]
  node [
    id 3144
    label "znale&#378;&#263;_si&#281;"
  ]
  node [
    id 3145
    label "obni&#380;y&#263;_si&#281;"
  ]
  node [
    id 3146
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 3147
    label "spa&#347;&#263;"
  ]
  node [
    id 3148
    label "utuczy&#263;"
  ]
  node [
    id 3149
    label "otoczy&#263;"
  ]
  node [
    id 3150
    label "odpa&#347;&#263;"
  ]
  node [
    id 3151
    label "zaj&#261;&#263;"
  ]
  node [
    id 3152
    label "os&#322;abn&#261;&#263;"
  ]
  node [
    id 3153
    label "zmieni&#263;"
  ]
  node [
    id 3154
    label "przyj&#261;&#263;"
  ]
  node [
    id 3155
    label "ogarn&#261;&#263;"
  ]
  node [
    id 3156
    label "spend"
  ]
  node [
    id 3157
    label "aggravation"
  ]
  node [
    id 3158
    label "worsening"
  ]
  node [
    id 3159
    label "gorszy"
  ]
  node [
    id 3160
    label "dropiowate"
  ]
  node [
    id 3161
    label "kania"
  ]
  node [
    id 3162
    label "bustard"
  ]
  node [
    id 3163
    label "ptak"
  ]
  node [
    id 3164
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 3165
    label "sk&#322;ada&#263;"
  ]
  node [
    id 3166
    label "odmawia&#263;"
  ]
  node [
    id 3167
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 3168
    label "wyra&#380;a&#263;"
  ]
  node [
    id 3169
    label "thank"
  ]
  node [
    id 3170
    label "etykieta"
  ]
  node [
    id 3171
    label "przekazywa&#263;"
  ]
  node [
    id 3172
    label "zbiera&#263;"
  ]
  node [
    id 3173
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 3174
    label "przywraca&#263;"
  ]
  node [
    id 3175
    label "dawa&#263;"
  ]
  node [
    id 3176
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 3177
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 3178
    label "publicize"
  ]
  node [
    id 3179
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 3180
    label "render"
  ]
  node [
    id 3181
    label "uk&#322;ada&#263;"
  ]
  node [
    id 3182
    label "opracowywa&#263;"
  ]
  node [
    id 3183
    label "oddawa&#263;"
  ]
  node [
    id 3184
    label "train"
  ]
  node [
    id 3185
    label "dzieli&#263;"
  ]
  node [
    id 3186
    label "scala&#263;"
  ]
  node [
    id 3187
    label "znaczy&#263;"
  ]
  node [
    id 3188
    label "give_voice"
  ]
  node [
    id 3189
    label "oznacza&#263;"
  ]
  node [
    id 3190
    label "zdradza&#263;_si&#281;"
  ]
  node [
    id 3191
    label "represent"
  ]
  node [
    id 3192
    label "komunikowa&#263;"
  ]
  node [
    id 3193
    label "contest"
  ]
  node [
    id 3194
    label "wypowiada&#263;"
  ]
  node [
    id 3195
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 3196
    label "odrzuca&#263;"
  ]
  node [
    id 3197
    label "frame"
  ]
  node [
    id 3198
    label "tab"
  ]
  node [
    id 3199
    label "naklejka"
  ]
  node [
    id 3200
    label "zwyczaj"
  ]
  node [
    id 3201
    label "tabliczka"
  ]
  node [
    id 3202
    label "formality"
  ]
  node [
    id 3203
    label "w_chuj"
  ]
  node [
    id 3204
    label "kla&#347;ni&#281;cie"
  ]
  node [
    id 3205
    label "oklaski"
  ]
  node [
    id 3206
    label "bang"
  ]
  node [
    id 3207
    label "zabrzmienie"
  ]
  node [
    id 3208
    label "gestod&#378;wi&#281;k"
  ]
  node [
    id 3209
    label "aprobata"
  ]
  node [
    id 3210
    label "ovation"
  ]
  node [
    id 3211
    label "Stefan"
  ]
  node [
    id 3212
    label "Niesio&#322;owski"
  ]
  node [
    id 3213
    label "gabriela"
  ]
  node [
    id 3214
    label "mas&#322;owski"
  ]
  node [
    id 3215
    label "i"
  ]
  node [
    id 3216
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 3217
    label "Rafa&#322;"
  ]
  node [
    id 3218
    label "Kmity"
  ]
  node [
    id 3219
    label "teleturniej"
  ]
  node [
    id 3220
    label "zawsze"
  ]
  node [
    id 3221
    label "dziewica"
  ]
  node [
    id 3222
    label "Kabareton"
  ]
  node [
    id 3223
    label "Sopot"
  ]
  node [
    id 3224
    label "topi&#263;"
  ]
  node [
    id 3225
    label "trend"
  ]
  node [
    id 3226
    label "krajowy"
  ]
  node [
    id 3227
    label "rada"
  ]
  node [
    id 3228
    label "Maryja"
  ]
  node [
    id 3229
    label "Tadeusz"
  ]
  node [
    id 3230
    label "rydzyk"
  ]
  node [
    id 3231
    label "Leszek"
  ]
  node [
    id 3232
    label "Deptu&#322;a"
  ]
  node [
    id 3233
    label "Jaros&#322;awa"
  ]
  node [
    id 3234
    label "rusiecki"
  ]
  node [
    id 3235
    label "Jerzy"
  ]
  node [
    id 3236
    label "Feliksa"
  ]
  node [
    id 3237
    label "Fedorowicz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 227
  ]
  edge [
    source 0
    target 228
  ]
  edge [
    source 0
    target 229
  ]
  edge [
    source 0
    target 230
  ]
  edge [
    source 0
    target 231
  ]
  edge [
    source 0
    target 232
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 0
    target 236
  ]
  edge [
    source 0
    target 237
  ]
  edge [
    source 0
    target 238
  ]
  edge [
    source 0
    target 239
  ]
  edge [
    source 0
    target 240
  ]
  edge [
    source 0
    target 241
  ]
  edge [
    source 0
    target 242
  ]
  edge [
    source 0
    target 243
  ]
  edge [
    source 0
    target 244
  ]
  edge [
    source 0
    target 245
  ]
  edge [
    source 0
    target 246
  ]
  edge [
    source 0
    target 247
  ]
  edge [
    source 0
    target 248
  ]
  edge [
    source 0
    target 249
  ]
  edge [
    source 0
    target 250
  ]
  edge [
    source 0
    target 251
  ]
  edge [
    source 0
    target 252
  ]
  edge [
    source 0
    target 253
  ]
  edge [
    source 0
    target 254
  ]
  edge [
    source 0
    target 255
  ]
  edge [
    source 0
    target 256
  ]
  edge [
    source 0
    target 257
  ]
  edge [
    source 0
    target 258
  ]
  edge [
    source 0
    target 259
  ]
  edge [
    source 0
    target 260
  ]
  edge [
    source 0
    target 261
  ]
  edge [
    source 0
    target 262
  ]
  edge [
    source 0
    target 263
  ]
  edge [
    source 0
    target 264
  ]
  edge [
    source 0
    target 265
  ]
  edge [
    source 0
    target 266
  ]
  edge [
    source 0
    target 267
  ]
  edge [
    source 0
    target 268
  ]
  edge [
    source 0
    target 269
  ]
  edge [
    source 0
    target 270
  ]
  edge [
    source 0
    target 271
  ]
  edge [
    source 0
    target 272
  ]
  edge [
    source 0
    target 273
  ]
  edge [
    source 0
    target 274
  ]
  edge [
    source 0
    target 275
  ]
  edge [
    source 0
    target 276
  ]
  edge [
    source 0
    target 277
  ]
  edge [
    source 0
    target 278
  ]
  edge [
    source 0
    target 279
  ]
  edge [
    source 0
    target 280
  ]
  edge [
    source 0
    target 281
  ]
  edge [
    source 0
    target 282
  ]
  edge [
    source 0
    target 283
  ]
  edge [
    source 0
    target 284
  ]
  edge [
    source 0
    target 285
  ]
  edge [
    source 0
    target 286
  ]
  edge [
    source 0
    target 287
  ]
  edge [
    source 0
    target 288
  ]
  edge [
    source 0
    target 289
  ]
  edge [
    source 0
    target 290
  ]
  edge [
    source 0
    target 291
  ]
  edge [
    source 0
    target 292
  ]
  edge [
    source 0
    target 293
  ]
  edge [
    source 0
    target 294
  ]
  edge [
    source 0
    target 295
  ]
  edge [
    source 0
    target 296
  ]
  edge [
    source 0
    target 297
  ]
  edge [
    source 0
    target 298
  ]
  edge [
    source 0
    target 299
  ]
  edge [
    source 0
    target 300
  ]
  edge [
    source 0
    target 301
  ]
  edge [
    source 0
    target 302
  ]
  edge [
    source 0
    target 303
  ]
  edge [
    source 0
    target 304
  ]
  edge [
    source 0
    target 305
  ]
  edge [
    source 0
    target 306
  ]
  edge [
    source 0
    target 307
  ]
  edge [
    source 0
    target 308
  ]
  edge [
    source 0
    target 309
  ]
  edge [
    source 0
    target 310
  ]
  edge [
    source 0
    target 311
  ]
  edge [
    source 0
    target 312
  ]
  edge [
    source 0
    target 313
  ]
  edge [
    source 0
    target 314
  ]
  edge [
    source 0
    target 315
  ]
  edge [
    source 0
    target 316
  ]
  edge [
    source 0
    target 317
  ]
  edge [
    source 0
    target 318
  ]
  edge [
    source 0
    target 319
  ]
  edge [
    source 0
    target 320
  ]
  edge [
    source 0
    target 321
  ]
  edge [
    source 0
    target 322
  ]
  edge [
    source 0
    target 323
  ]
  edge [
    source 0
    target 324
  ]
  edge [
    source 0
    target 325
  ]
  edge [
    source 0
    target 326
  ]
  edge [
    source 0
    target 327
  ]
  edge [
    source 0
    target 328
  ]
  edge [
    source 0
    target 329
  ]
  edge [
    source 0
    target 330
  ]
  edge [
    source 0
    target 331
  ]
  edge [
    source 0
    target 332
  ]
  edge [
    source 0
    target 333
  ]
  edge [
    source 0
    target 334
  ]
  edge [
    source 0
    target 335
  ]
  edge [
    source 0
    target 336
  ]
  edge [
    source 0
    target 337
  ]
  edge [
    source 0
    target 338
  ]
  edge [
    source 0
    target 339
  ]
  edge [
    source 0
    target 340
  ]
  edge [
    source 0
    target 341
  ]
  edge [
    source 0
    target 342
  ]
  edge [
    source 0
    target 343
  ]
  edge [
    source 0
    target 344
  ]
  edge [
    source 0
    target 345
  ]
  edge [
    source 0
    target 346
  ]
  edge [
    source 0
    target 347
  ]
  edge [
    source 0
    target 348
  ]
  edge [
    source 0
    target 349
  ]
  edge [
    source 0
    target 350
  ]
  edge [
    source 0
    target 351
  ]
  edge [
    source 0
    target 352
  ]
  edge [
    source 0
    target 353
  ]
  edge [
    source 0
    target 354
  ]
  edge [
    source 0
    target 355
  ]
  edge [
    source 0
    target 356
  ]
  edge [
    source 0
    target 357
  ]
  edge [
    source 0
    target 358
  ]
  edge [
    source 0
    target 359
  ]
  edge [
    source 0
    target 360
  ]
  edge [
    source 0
    target 361
  ]
  edge [
    source 0
    target 362
  ]
  edge [
    source 0
    target 363
  ]
  edge [
    source 0
    target 364
  ]
  edge [
    source 0
    target 365
  ]
  edge [
    source 0
    target 366
  ]
  edge [
    source 0
    target 367
  ]
  edge [
    source 0
    target 368
  ]
  edge [
    source 0
    target 369
  ]
  edge [
    source 0
    target 370
  ]
  edge [
    source 0
    target 371
  ]
  edge [
    source 0
    target 372
  ]
  edge [
    source 0
    target 373
  ]
  edge [
    source 0
    target 374
  ]
  edge [
    source 0
    target 375
  ]
  edge [
    source 0
    target 376
  ]
  edge [
    source 0
    target 377
  ]
  edge [
    source 0
    target 378
  ]
  edge [
    source 0
    target 379
  ]
  edge [
    source 0
    target 380
  ]
  edge [
    source 0
    target 381
  ]
  edge [
    source 0
    target 382
  ]
  edge [
    source 0
    target 383
  ]
  edge [
    source 0
    target 384
  ]
  edge [
    source 0
    target 385
  ]
  edge [
    source 0
    target 386
  ]
  edge [
    source 0
    target 387
  ]
  edge [
    source 0
    target 388
  ]
  edge [
    source 0
    target 389
  ]
  edge [
    source 0
    target 390
  ]
  edge [
    source 0
    target 391
  ]
  edge [
    source 0
    target 392
  ]
  edge [
    source 0
    target 393
  ]
  edge [
    source 0
    target 394
  ]
  edge [
    source 0
    target 395
  ]
  edge [
    source 0
    target 396
  ]
  edge [
    source 0
    target 397
  ]
  edge [
    source 0
    target 398
  ]
  edge [
    source 0
    target 399
  ]
  edge [
    source 0
    target 400
  ]
  edge [
    source 0
    target 401
  ]
  edge [
    source 0
    target 402
  ]
  edge [
    source 0
    target 403
  ]
  edge [
    source 0
    target 404
  ]
  edge [
    source 0
    target 405
  ]
  edge [
    source 0
    target 406
  ]
  edge [
    source 0
    target 407
  ]
  edge [
    source 0
    target 408
  ]
  edge [
    source 0
    target 409
  ]
  edge [
    source 0
    target 410
  ]
  edge [
    source 0
    target 411
  ]
  edge [
    source 0
    target 412
  ]
  edge [
    source 0
    target 413
  ]
  edge [
    source 0
    target 414
  ]
  edge [
    source 0
    target 415
  ]
  edge [
    source 0
    target 416
  ]
  edge [
    source 0
    target 417
  ]
  edge [
    source 0
    target 418
  ]
  edge [
    source 0
    target 419
  ]
  edge [
    source 0
    target 420
  ]
  edge [
    source 0
    target 421
  ]
  edge [
    source 0
    target 422
  ]
  edge [
    source 0
    target 423
  ]
  edge [
    source 0
    target 424
  ]
  edge [
    source 0
    target 425
  ]
  edge [
    source 0
    target 426
  ]
  edge [
    source 0
    target 427
  ]
  edge [
    source 0
    target 428
  ]
  edge [
    source 0
    target 429
  ]
  edge [
    source 0
    target 430
  ]
  edge [
    source 0
    target 431
  ]
  edge [
    source 0
    target 432
  ]
  edge [
    source 0
    target 433
  ]
  edge [
    source 0
    target 434
  ]
  edge [
    source 0
    target 435
  ]
  edge [
    source 0
    target 436
  ]
  edge [
    source 0
    target 437
  ]
  edge [
    source 0
    target 438
  ]
  edge [
    source 0
    target 439
  ]
  edge [
    source 0
    target 440
  ]
  edge [
    source 0
    target 441
  ]
  edge [
    source 0
    target 442
  ]
  edge [
    source 0
    target 443
  ]
  edge [
    source 0
    target 444
  ]
  edge [
    source 0
    target 445
  ]
  edge [
    source 0
    target 446
  ]
  edge [
    source 0
    target 447
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 448
  ]
  edge [
    source 0
    target 449
  ]
  edge [
    source 0
    target 450
  ]
  edge [
    source 0
    target 451
  ]
  edge [
    source 0
    target 452
  ]
  edge [
    source 0
    target 453
  ]
  edge [
    source 0
    target 454
  ]
  edge [
    source 0
    target 455
  ]
  edge [
    source 0
    target 456
  ]
  edge [
    source 0
    target 457
  ]
  edge [
    source 0
    target 458
  ]
  edge [
    source 0
    target 459
  ]
  edge [
    source 0
    target 460
  ]
  edge [
    source 0
    target 461
  ]
  edge [
    source 0
    target 462
  ]
  edge [
    source 0
    target 463
  ]
  edge [
    source 0
    target 464
  ]
  edge [
    source 0
    target 465
  ]
  edge [
    source 0
    target 466
  ]
  edge [
    source 0
    target 467
  ]
  edge [
    source 0
    target 468
  ]
  edge [
    source 0
    target 469
  ]
  edge [
    source 0
    target 470
  ]
  edge [
    source 0
    target 471
  ]
  edge [
    source 0
    target 472
  ]
  edge [
    source 0
    target 473
  ]
  edge [
    source 0
    target 474
  ]
  edge [
    source 0
    target 475
  ]
  edge [
    source 0
    target 476
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 477
  ]
  edge [
    source 1
    target 478
  ]
  edge [
    source 1
    target 479
  ]
  edge [
    source 1
    target 480
  ]
  edge [
    source 1
    target 210
  ]
  edge [
    source 1
    target 481
  ]
  edge [
    source 1
    target 482
  ]
  edge [
    source 1
    target 483
  ]
  edge [
    source 1
    target 484
  ]
  edge [
    source 1
    target 485
  ]
  edge [
    source 1
    target 486
  ]
  edge [
    source 1
    target 487
  ]
  edge [
    source 1
    target 208
  ]
  edge [
    source 1
    target 488
  ]
  edge [
    source 1
    target 489
  ]
  edge [
    source 1
    target 490
  ]
  edge [
    source 1
    target 491
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 492
  ]
  edge [
    source 2
    target 493
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 494
  ]
  edge [
    source 2
    target 495
  ]
  edge [
    source 2
    target 496
  ]
  edge [
    source 2
    target 497
  ]
  edge [
    source 2
    target 498
  ]
  edge [
    source 2
    target 499
  ]
  edge [
    source 2
    target 500
  ]
  edge [
    source 2
    target 501
  ]
  edge [
    source 2
    target 502
  ]
  edge [
    source 2
    target 503
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 504
  ]
  edge [
    source 2
    target 505
  ]
  edge [
    source 2
    target 506
  ]
  edge [
    source 2
    target 507
  ]
  edge [
    source 2
    target 508
  ]
  edge [
    source 2
    target 509
  ]
  edge [
    source 2
    target 510
  ]
  edge [
    source 2
    target 511
  ]
  edge [
    source 2
    target 512
  ]
  edge [
    source 2
    target 513
  ]
  edge [
    source 2
    target 514
  ]
  edge [
    source 2
    target 515
  ]
  edge [
    source 2
    target 516
  ]
  edge [
    source 2
    target 517
  ]
  edge [
    source 2
    target 518
  ]
  edge [
    source 2
    target 519
  ]
  edge [
    source 2
    target 520
  ]
  edge [
    source 2
    target 521
  ]
  edge [
    source 2
    target 522
  ]
  edge [
    source 2
    target 523
  ]
  edge [
    source 2
    target 524
  ]
  edge [
    source 2
    target 525
  ]
  edge [
    source 2
    target 526
  ]
  edge [
    source 2
    target 527
  ]
  edge [
    source 2
    target 528
  ]
  edge [
    source 2
    target 529
  ]
  edge [
    source 2
    target 530
  ]
  edge [
    source 2
    target 531
  ]
  edge [
    source 2
    target 532
  ]
  edge [
    source 2
    target 533
  ]
  edge [
    source 2
    target 534
  ]
  edge [
    source 2
    target 535
  ]
  edge [
    source 2
    target 536
  ]
  edge [
    source 2
    target 537
  ]
  edge [
    source 2
    target 538
  ]
  edge [
    source 2
    target 539
  ]
  edge [
    source 2
    target 540
  ]
  edge [
    source 2
    target 541
  ]
  edge [
    source 2
    target 542
  ]
  edge [
    source 2
    target 543
  ]
  edge [
    source 2
    target 544
  ]
  edge [
    source 2
    target 545
  ]
  edge [
    source 2
    target 546
  ]
  edge [
    source 2
    target 547
  ]
  edge [
    source 2
    target 548
  ]
  edge [
    source 2
    target 549
  ]
  edge [
    source 2
    target 550
  ]
  edge [
    source 2
    target 551
  ]
  edge [
    source 2
    target 552
  ]
  edge [
    source 2
    target 553
  ]
  edge [
    source 2
    target 554
  ]
  edge [
    source 2
    target 555
  ]
  edge [
    source 2
    target 556
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 557
  ]
  edge [
    source 3
    target 487
  ]
  edge [
    source 3
    target 558
  ]
  edge [
    source 3
    target 559
  ]
  edge [
    source 3
    target 560
  ]
  edge [
    source 3
    target 561
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 562
  ]
  edge [
    source 3
    target 563
  ]
  edge [
    source 3
    target 564
  ]
  edge [
    source 3
    target 565
  ]
  edge [
    source 3
    target 566
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 567
  ]
  edge [
    source 3
    target 568
  ]
  edge [
    source 3
    target 569
  ]
  edge [
    source 3
    target 570
  ]
  edge [
    source 3
    target 571
  ]
  edge [
    source 3
    target 572
  ]
  edge [
    source 3
    target 573
  ]
  edge [
    source 3
    target 574
  ]
  edge [
    source 3
    target 575
  ]
  edge [
    source 3
    target 576
  ]
  edge [
    source 3
    target 577
  ]
  edge [
    source 3
    target 578
  ]
  edge [
    source 3
    target 579
  ]
  edge [
    source 3
    target 580
  ]
  edge [
    source 3
    target 581
  ]
  edge [
    source 3
    target 582
  ]
  edge [
    source 3
    target 583
  ]
  edge [
    source 3
    target 584
  ]
  edge [
    source 3
    target 585
  ]
  edge [
    source 3
    target 586
  ]
  edge [
    source 3
    target 587
  ]
  edge [
    source 3
    target 588
  ]
  edge [
    source 3
    target 589
  ]
  edge [
    source 3
    target 590
  ]
  edge [
    source 3
    target 591
  ]
  edge [
    source 3
    target 592
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 593
  ]
  edge [
    source 3
    target 594
  ]
  edge [
    source 3
    target 595
  ]
  edge [
    source 3
    target 596
  ]
  edge [
    source 3
    target 597
  ]
  edge [
    source 3
    target 598
  ]
  edge [
    source 3
    target 599
  ]
  edge [
    source 3
    target 600
  ]
  edge [
    source 3
    target 601
  ]
  edge [
    source 3
    target 602
  ]
  edge [
    source 3
    target 603
  ]
  edge [
    source 3
    target 604
  ]
  edge [
    source 3
    target 605
  ]
  edge [
    source 3
    target 606
  ]
  edge [
    source 3
    target 607
  ]
  edge [
    source 3
    target 608
  ]
  edge [
    source 3
    target 609
  ]
  edge [
    source 3
    target 610
  ]
  edge [
    source 3
    target 611
  ]
  edge [
    source 3
    target 612
  ]
  edge [
    source 3
    target 613
  ]
  edge [
    source 3
    target 614
  ]
  edge [
    source 3
    target 615
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 616
  ]
  edge [
    source 3
    target 617
  ]
  edge [
    source 3
    target 618
  ]
  edge [
    source 3
    target 619
  ]
  edge [
    source 3
    target 620
  ]
  edge [
    source 3
    target 621
  ]
  edge [
    source 3
    target 622
  ]
  edge [
    source 3
    target 623
  ]
  edge [
    source 3
    target 624
  ]
  edge [
    source 3
    target 625
  ]
  edge [
    source 3
    target 626
  ]
  edge [
    source 3
    target 627
  ]
  edge [
    source 3
    target 485
  ]
  edge [
    source 3
    target 628
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 655
  ]
  edge [
    source 5
    target 656
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 657
  ]
  edge [
    source 5
    target 658
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 685
  ]
  edge [
    source 7
    target 686
  ]
  edge [
    source 7
    target 687
  ]
  edge [
    source 7
    target 688
  ]
  edge [
    source 7
    target 689
  ]
  edge [
    source 7
    target 690
  ]
  edge [
    source 7
    target 691
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 692
  ]
  edge [
    source 8
    target 693
  ]
  edge [
    source 8
    target 694
  ]
  edge [
    source 8
    target 695
  ]
  edge [
    source 8
    target 696
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 698
  ]
  edge [
    source 8
    target 699
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 700
  ]
  edge [
    source 8
    target 701
  ]
  edge [
    source 8
    target 702
  ]
  edge [
    source 8
    target 703
  ]
  edge [
    source 8
    target 704
  ]
  edge [
    source 8
    target 705
  ]
  edge [
    source 8
    target 706
  ]
  edge [
    source 8
    target 707
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 708
  ]
  edge [
    source 8
    target 709
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 710
  ]
  edge [
    source 8
    target 711
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 712
  ]
  edge [
    source 8
    target 713
  ]
  edge [
    source 8
    target 714
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 715
  ]
  edge [
    source 8
    target 716
  ]
  edge [
    source 8
    target 717
  ]
  edge [
    source 8
    target 718
  ]
  edge [
    source 8
    target 719
  ]
  edge [
    source 8
    target 720
  ]
  edge [
    source 8
    target 721
  ]
  edge [
    source 8
    target 666
  ]
  edge [
    source 8
    target 722
  ]
  edge [
    source 8
    target 723
  ]
  edge [
    source 8
    target 724
  ]
  edge [
    source 8
    target 725
  ]
  edge [
    source 8
    target 726
  ]
  edge [
    source 8
    target 727
  ]
  edge [
    source 8
    target 728
  ]
  edge [
    source 8
    target 729
  ]
  edge [
    source 8
    target 730
  ]
  edge [
    source 8
    target 731
  ]
  edge [
    source 8
    target 732
  ]
  edge [
    source 8
    target 733
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 735
  ]
  edge [
    source 8
    target 736
  ]
  edge [
    source 8
    target 737
  ]
  edge [
    source 8
    target 738
  ]
  edge [
    source 8
    target 739
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 740
  ]
  edge [
    source 8
    target 741
  ]
  edge [
    source 8
    target 742
  ]
  edge [
    source 8
    target 743
  ]
  edge [
    source 8
    target 614
  ]
  edge [
    source 8
    target 744
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 745
  ]
  edge [
    source 9
    target 746
  ]
  edge [
    source 9
    target 747
  ]
  edge [
    source 9
    target 748
  ]
  edge [
    source 9
    target 749
  ]
  edge [
    source 9
    target 750
  ]
  edge [
    source 9
    target 751
  ]
  edge [
    source 9
    target 752
  ]
  edge [
    source 9
    target 753
  ]
  edge [
    source 9
    target 754
  ]
  edge [
    source 9
    target 755
  ]
  edge [
    source 9
    target 756
  ]
  edge [
    source 9
    target 757
  ]
  edge [
    source 9
    target 758
  ]
  edge [
    source 9
    target 759
  ]
  edge [
    source 9
    target 760
  ]
  edge [
    source 9
    target 761
  ]
  edge [
    source 9
    target 762
  ]
  edge [
    source 9
    target 763
  ]
  edge [
    source 9
    target 764
  ]
  edge [
    source 9
    target 765
  ]
  edge [
    source 9
    target 766
  ]
  edge [
    source 9
    target 767
  ]
  edge [
    source 9
    target 768
  ]
  edge [
    source 9
    target 769
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 770
  ]
  edge [
    source 10
    target 771
  ]
  edge [
    source 10
    target 772
  ]
  edge [
    source 10
    target 773
  ]
  edge [
    source 10
    target 774
  ]
  edge [
    source 10
    target 775
  ]
  edge [
    source 10
    target 776
  ]
  edge [
    source 10
    target 777
  ]
  edge [
    source 10
    target 778
  ]
  edge [
    source 10
    target 779
  ]
  edge [
    source 10
    target 780
  ]
  edge [
    source 10
    target 781
  ]
  edge [
    source 10
    target 782
  ]
  edge [
    source 10
    target 783
  ]
  edge [
    source 10
    target 784
  ]
  edge [
    source 10
    target 785
  ]
  edge [
    source 10
    target 786
  ]
  edge [
    source 10
    target 787
  ]
  edge [
    source 10
    target 788
  ]
  edge [
    source 10
    target 789
  ]
  edge [
    source 10
    target 790
  ]
  edge [
    source 10
    target 791
  ]
  edge [
    source 10
    target 792
  ]
  edge [
    source 10
    target 793
  ]
  edge [
    source 10
    target 794
  ]
  edge [
    source 10
    target 795
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 796
  ]
  edge [
    source 10
    target 797
  ]
  edge [
    source 10
    target 798
  ]
  edge [
    source 10
    target 799
  ]
  edge [
    source 10
    target 800
  ]
  edge [
    source 10
    target 801
  ]
  edge [
    source 10
    target 802
  ]
  edge [
    source 10
    target 803
  ]
  edge [
    source 10
    target 804
  ]
  edge [
    source 10
    target 805
  ]
  edge [
    source 10
    target 806
  ]
  edge [
    source 10
    target 807
  ]
  edge [
    source 10
    target 808
  ]
  edge [
    source 10
    target 809
  ]
  edge [
    source 10
    target 810
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 811
  ]
  edge [
    source 10
    target 812
  ]
  edge [
    source 10
    target 813
  ]
  edge [
    source 10
    target 814
  ]
  edge [
    source 10
    target 815
  ]
  edge [
    source 10
    target 816
  ]
  edge [
    source 10
    target 817
  ]
  edge [
    source 10
    target 818
  ]
  edge [
    source 10
    target 819
  ]
  edge [
    source 10
    target 820
  ]
  edge [
    source 10
    target 821
  ]
  edge [
    source 10
    target 822
  ]
  edge [
    source 10
    target 823
  ]
  edge [
    source 10
    target 824
  ]
  edge [
    source 10
    target 825
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 826
  ]
  edge [
    source 10
    target 827
  ]
  edge [
    source 10
    target 828
  ]
  edge [
    source 10
    target 829
  ]
  edge [
    source 10
    target 830
  ]
  edge [
    source 10
    target 831
  ]
  edge [
    source 10
    target 832
  ]
  edge [
    source 10
    target 833
  ]
  edge [
    source 10
    target 834
  ]
  edge [
    source 10
    target 835
  ]
  edge [
    source 10
    target 836
  ]
  edge [
    source 10
    target 837
  ]
  edge [
    source 10
    target 838
  ]
  edge [
    source 10
    target 839
  ]
  edge [
    source 10
    target 840
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 841
  ]
  edge [
    source 11
    target 842
  ]
  edge [
    source 11
    target 843
  ]
  edge [
    source 11
    target 844
  ]
  edge [
    source 11
    target 845
  ]
  edge [
    source 11
    target 846
  ]
  edge [
    source 11
    target 847
  ]
  edge [
    source 11
    target 848
  ]
  edge [
    source 11
    target 849
  ]
  edge [
    source 11
    target 850
  ]
  edge [
    source 11
    target 851
  ]
  edge [
    source 11
    target 852
  ]
  edge [
    source 11
    target 853
  ]
  edge [
    source 11
    target 854
  ]
  edge [
    source 11
    target 855
  ]
  edge [
    source 11
    target 856
  ]
  edge [
    source 11
    target 857
  ]
  edge [
    source 11
    target 858
  ]
  edge [
    source 11
    target 859
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 860
  ]
  edge [
    source 11
    target 861
  ]
  edge [
    source 11
    target 862
  ]
  edge [
    source 11
    target 863
  ]
  edge [
    source 11
    target 864
  ]
  edge [
    source 11
    target 865
  ]
  edge [
    source 11
    target 866
  ]
  edge [
    source 11
    target 867
  ]
  edge [
    source 11
    target 868
  ]
  edge [
    source 11
    target 869
  ]
  edge [
    source 11
    target 870
  ]
  edge [
    source 11
    target 871
  ]
  edge [
    source 11
    target 872
  ]
  edge [
    source 11
    target 873
  ]
  edge [
    source 11
    target 516
  ]
  edge [
    source 11
    target 874
  ]
  edge [
    source 11
    target 875
  ]
  edge [
    source 11
    target 876
  ]
  edge [
    source 11
    target 877
  ]
  edge [
    source 11
    target 878
  ]
  edge [
    source 11
    target 879
  ]
  edge [
    source 11
    target 880
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 881
  ]
  edge [
    source 12
    target 882
  ]
  edge [
    source 12
    target 883
  ]
  edge [
    source 12
    target 884
  ]
  edge [
    source 12
    target 885
  ]
  edge [
    source 12
    target 886
  ]
  edge [
    source 12
    target 887
  ]
  edge [
    source 12
    target 888
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 629
  ]
  edge [
    source 13
    target 889
  ]
  edge [
    source 13
    target 890
  ]
  edge [
    source 13
    target 711
  ]
  edge [
    source 13
    target 891
  ]
  edge [
    source 13
    target 892
  ]
  edge [
    source 13
    target 573
  ]
  edge [
    source 13
    target 631
  ]
  edge [
    source 13
    target 632
  ]
  edge [
    source 13
    target 633
  ]
  edge [
    source 13
    target 634
  ]
  edge [
    source 13
    target 635
  ]
  edge [
    source 13
    target 636
  ]
  edge [
    source 13
    target 637
  ]
  edge [
    source 13
    target 638
  ]
  edge [
    source 13
    target 558
  ]
  edge [
    source 13
    target 639
  ]
  edge [
    source 13
    target 640
  ]
  edge [
    source 13
    target 641
  ]
  edge [
    source 13
    target 642
  ]
  edge [
    source 13
    target 643
  ]
  edge [
    source 13
    target 644
  ]
  edge [
    source 13
    target 645
  ]
  edge [
    source 13
    target 646
  ]
  edge [
    source 13
    target 647
  ]
  edge [
    source 13
    target 648
  ]
  edge [
    source 13
    target 649
  ]
  edge [
    source 13
    target 608
  ]
  edge [
    source 13
    target 650
  ]
  edge [
    source 13
    target 651
  ]
  edge [
    source 13
    target 652
  ]
  edge [
    source 13
    target 653
  ]
  edge [
    source 13
    target 654
  ]
  edge [
    source 13
    target 893
  ]
  edge [
    source 13
    target 894
  ]
  edge [
    source 13
    target 36
  ]
  edge [
    source 13
    target 895
  ]
  edge [
    source 13
    target 896
  ]
  edge [
    source 13
    target 897
  ]
  edge [
    source 13
    target 898
  ]
  edge [
    source 13
    target 899
  ]
  edge [
    source 13
    target 821
  ]
  edge [
    source 13
    target 900
  ]
  edge [
    source 13
    target 901
  ]
  edge [
    source 13
    target 902
  ]
  edge [
    source 13
    target 903
  ]
  edge [
    source 13
    target 904
  ]
  edge [
    source 13
    target 905
  ]
  edge [
    source 13
    target 906
  ]
  edge [
    source 13
    target 907
  ]
  edge [
    source 13
    target 908
  ]
  edge [
    source 13
    target 909
  ]
  edge [
    source 13
    target 910
  ]
  edge [
    source 13
    target 911
  ]
  edge [
    source 13
    target 912
  ]
  edge [
    source 13
    target 913
  ]
  edge [
    source 13
    target 706
  ]
  edge [
    source 13
    target 914
  ]
  edge [
    source 13
    target 915
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 624
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 928
  ]
  edge [
    source 14
    target 929
  ]
  edge [
    source 14
    target 930
  ]
  edge [
    source 14
    target 931
  ]
  edge [
    source 14
    target 932
  ]
  edge [
    source 14
    target 904
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 933
  ]
  edge [
    source 14
    target 934
  ]
  edge [
    source 14
    target 935
  ]
  edge [
    source 14
    target 936
  ]
  edge [
    source 14
    target 937
  ]
  edge [
    source 14
    target 938
  ]
  edge [
    source 14
    target 939
  ]
  edge [
    source 14
    target 940
  ]
  edge [
    source 14
    target 941
  ]
  edge [
    source 14
    target 942
  ]
  edge [
    source 14
    target 943
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 944
  ]
  edge [
    source 14
    target 945
  ]
  edge [
    source 14
    target 946
  ]
  edge [
    source 14
    target 947
  ]
  edge [
    source 14
    target 948
  ]
  edge [
    source 14
    target 949
  ]
  edge [
    source 14
    target 950
  ]
  edge [
    source 14
    target 951
  ]
  edge [
    source 14
    target 561
  ]
  edge [
    source 14
    target 952
  ]
  edge [
    source 14
    target 953
  ]
  edge [
    source 14
    target 954
  ]
  edge [
    source 14
    target 955
  ]
  edge [
    source 14
    target 956
  ]
  edge [
    source 14
    target 957
  ]
  edge [
    source 14
    target 958
  ]
  edge [
    source 14
    target 959
  ]
  edge [
    source 14
    target 960
  ]
  edge [
    source 14
    target 961
  ]
  edge [
    source 14
    target 962
  ]
  edge [
    source 14
    target 789
  ]
  edge [
    source 14
    target 963
  ]
  edge [
    source 14
    target 964
  ]
  edge [
    source 14
    target 965
  ]
  edge [
    source 14
    target 584
  ]
  edge [
    source 14
    target 966
  ]
  edge [
    source 14
    target 967
  ]
  edge [
    source 14
    target 968
  ]
  edge [
    source 14
    target 969
  ]
  edge [
    source 14
    target 970
  ]
  edge [
    source 14
    target 971
  ]
  edge [
    source 14
    target 972
  ]
  edge [
    source 14
    target 973
  ]
  edge [
    source 14
    target 974
  ]
  edge [
    source 14
    target 975
  ]
  edge [
    source 14
    target 614
  ]
  edge [
    source 14
    target 976
  ]
  edge [
    source 14
    target 977
  ]
  edge [
    source 14
    target 978
  ]
  edge [
    source 14
    target 979
  ]
  edge [
    source 14
    target 980
  ]
  edge [
    source 14
    target 981
  ]
  edge [
    source 14
    target 982
  ]
  edge [
    source 14
    target 983
  ]
  edge [
    source 14
    target 984
  ]
  edge [
    source 14
    target 985
  ]
  edge [
    source 14
    target 986
  ]
  edge [
    source 14
    target 987
  ]
  edge [
    source 14
    target 988
  ]
  edge [
    source 14
    target 989
  ]
  edge [
    source 14
    target 990
  ]
  edge [
    source 14
    target 991
  ]
  edge [
    source 14
    target 992
  ]
  edge [
    source 14
    target 993
  ]
  edge [
    source 14
    target 994
  ]
  edge [
    source 14
    target 995
  ]
  edge [
    source 14
    target 996
  ]
  edge [
    source 14
    target 997
  ]
  edge [
    source 14
    target 998
  ]
  edge [
    source 14
    target 999
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 58
  ]
  edge [
    source 15
    target 1000
  ]
  edge [
    source 15
    target 1001
  ]
  edge [
    source 15
    target 658
  ]
  edge [
    source 15
    target 656
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1002
  ]
  edge [
    source 16
    target 1003
  ]
  edge [
    source 16
    target 1004
  ]
  edge [
    source 16
    target 851
  ]
  edge [
    source 16
    target 1005
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 1006
  ]
  edge [
    source 17
    target 1007
  ]
  edge [
    source 17
    target 1008
  ]
  edge [
    source 17
    target 1009
  ]
  edge [
    source 17
    target 1010
  ]
  edge [
    source 17
    target 1011
  ]
  edge [
    source 17
    target 1012
  ]
  edge [
    source 17
    target 1013
  ]
  edge [
    source 17
    target 1014
  ]
  edge [
    source 17
    target 918
  ]
  edge [
    source 17
    target 1015
  ]
  edge [
    source 17
    target 1016
  ]
  edge [
    source 17
    target 1017
  ]
  edge [
    source 17
    target 1018
  ]
  edge [
    source 17
    target 1019
  ]
  edge [
    source 17
    target 1020
  ]
  edge [
    source 17
    target 1021
  ]
  edge [
    source 17
    target 1022
  ]
  edge [
    source 17
    target 1023
  ]
  edge [
    source 17
    target 1024
  ]
  edge [
    source 17
    target 1025
  ]
  edge [
    source 17
    target 1026
  ]
  edge [
    source 17
    target 1027
  ]
  edge [
    source 17
    target 666
  ]
  edge [
    source 17
    target 1028
  ]
  edge [
    source 17
    target 1029
  ]
  edge [
    source 17
    target 1030
  ]
  edge [
    source 17
    target 1031
  ]
  edge [
    source 17
    target 573
  ]
  edge [
    source 17
    target 1032
  ]
  edge [
    source 17
    target 1033
  ]
  edge [
    source 17
    target 1034
  ]
  edge [
    source 17
    target 1035
  ]
  edge [
    source 17
    target 609
  ]
  edge [
    source 17
    target 1036
  ]
  edge [
    source 17
    target 1037
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 33
  ]
  edge [
    source 19
    target 1038
  ]
  edge [
    source 19
    target 1039
  ]
  edge [
    source 19
    target 1040
  ]
  edge [
    source 19
    target 1041
  ]
  edge [
    source 19
    target 1042
  ]
  edge [
    source 19
    target 1043
  ]
  edge [
    source 19
    target 1044
  ]
  edge [
    source 19
    target 32
  ]
  edge [
    source 19
    target 1045
  ]
  edge [
    source 19
    target 1046
  ]
  edge [
    source 19
    target 87
  ]
  edge [
    source 19
    target 1047
  ]
  edge [
    source 19
    target 1048
  ]
  edge [
    source 19
    target 1049
  ]
  edge [
    source 19
    target 894
  ]
  edge [
    source 19
    target 654
  ]
  edge [
    source 19
    target 1050
  ]
  edge [
    source 19
    target 1051
  ]
  edge [
    source 19
    target 1052
  ]
  edge [
    source 19
    target 1053
  ]
  edge [
    source 19
    target 1054
  ]
  edge [
    source 19
    target 1055
  ]
  edge [
    source 19
    target 1056
  ]
  edge [
    source 19
    target 1057
  ]
  edge [
    source 19
    target 1058
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 28
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 26
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 39
  ]
  edge [
    source 20
    target 40
  ]
  edge [
    source 20
    target 82
  ]
  edge [
    source 20
    target 1059
  ]
  edge [
    source 20
    target 1060
  ]
  edge [
    source 20
    target 1061
  ]
  edge [
    source 20
    target 1062
  ]
  edge [
    source 20
    target 1063
  ]
  edge [
    source 20
    target 1064
  ]
  edge [
    source 20
    target 620
  ]
  edge [
    source 20
    target 1065
  ]
  edge [
    source 20
    target 1066
  ]
  edge [
    source 20
    target 1067
  ]
  edge [
    source 20
    target 1068
  ]
  edge [
    source 20
    target 1069
  ]
  edge [
    source 20
    target 1070
  ]
  edge [
    source 20
    target 1071
  ]
  edge [
    source 20
    target 1072
  ]
  edge [
    source 20
    target 1073
  ]
  edge [
    source 20
    target 62
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 20
    target 43
  ]
  edge [
    source 20
    target 79
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 1074
  ]
  edge [
    source 21
    target 1075
  ]
  edge [
    source 21
    target 1076
  ]
  edge [
    source 21
    target 1077
  ]
  edge [
    source 21
    target 1078
  ]
  edge [
    source 21
    target 1079
  ]
  edge [
    source 21
    target 1080
  ]
  edge [
    source 21
    target 1081
  ]
  edge [
    source 21
    target 1082
  ]
  edge [
    source 21
    target 1083
  ]
  edge [
    source 21
    target 1084
  ]
  edge [
    source 21
    target 1085
  ]
  edge [
    source 21
    target 1086
  ]
  edge [
    source 21
    target 1087
  ]
  edge [
    source 21
    target 1088
  ]
  edge [
    source 21
    target 1089
  ]
  edge [
    source 21
    target 1090
  ]
  edge [
    source 21
    target 1091
  ]
  edge [
    source 21
    target 1092
  ]
  edge [
    source 21
    target 1093
  ]
  edge [
    source 21
    target 1094
  ]
  edge [
    source 21
    target 1095
  ]
  edge [
    source 21
    target 1096
  ]
  edge [
    source 21
    target 1097
  ]
  edge [
    source 21
    target 1098
  ]
  edge [
    source 21
    target 1099
  ]
  edge [
    source 21
    target 1100
  ]
  edge [
    source 21
    target 1101
  ]
  edge [
    source 21
    target 1102
  ]
  edge [
    source 21
    target 1103
  ]
  edge [
    source 21
    target 1104
  ]
  edge [
    source 21
    target 1105
  ]
  edge [
    source 21
    target 792
  ]
  edge [
    source 21
    target 1106
  ]
  edge [
    source 21
    target 1107
  ]
  edge [
    source 21
    target 1108
  ]
  edge [
    source 21
    target 1109
  ]
  edge [
    source 21
    target 1110
  ]
  edge [
    source 21
    target 1111
  ]
  edge [
    source 21
    target 1112
  ]
  edge [
    source 21
    target 1113
  ]
  edge [
    source 21
    target 1114
  ]
  edge [
    source 21
    target 1115
  ]
  edge [
    source 21
    target 1116
  ]
  edge [
    source 21
    target 1117
  ]
  edge [
    source 21
    target 1118
  ]
  edge [
    source 21
    target 1119
  ]
  edge [
    source 21
    target 1120
  ]
  edge [
    source 21
    target 332
  ]
  edge [
    source 21
    target 1121
  ]
  edge [
    source 21
    target 1122
  ]
  edge [
    source 21
    target 1123
  ]
  edge [
    source 21
    target 1124
  ]
  edge [
    source 21
    target 1125
  ]
  edge [
    source 21
    target 1126
  ]
  edge [
    source 21
    target 1127
  ]
  edge [
    source 21
    target 1128
  ]
  edge [
    source 21
    target 1129
  ]
  edge [
    source 21
    target 1130
  ]
  edge [
    source 21
    target 1131
  ]
  edge [
    source 21
    target 1132
  ]
  edge [
    source 21
    target 1133
  ]
  edge [
    source 21
    target 1134
  ]
  edge [
    source 21
    target 1135
  ]
  edge [
    source 21
    target 1136
  ]
  edge [
    source 21
    target 1137
  ]
  edge [
    source 21
    target 1138
  ]
  edge [
    source 21
    target 1139
  ]
  edge [
    source 21
    target 1140
  ]
  edge [
    source 21
    target 1141
  ]
  edge [
    source 21
    target 1142
  ]
  edge [
    source 21
    target 1143
  ]
  edge [
    source 21
    target 1144
  ]
  edge [
    source 21
    target 1145
  ]
  edge [
    source 21
    target 1146
  ]
  edge [
    source 21
    target 1147
  ]
  edge [
    source 21
    target 1148
  ]
  edge [
    source 21
    target 621
  ]
  edge [
    source 21
    target 1149
  ]
  edge [
    source 21
    target 1150
  ]
  edge [
    source 21
    target 1151
  ]
  edge [
    source 21
    target 1152
  ]
  edge [
    source 21
    target 1153
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 1154
  ]
  edge [
    source 21
    target 1155
  ]
  edge [
    source 21
    target 890
  ]
  edge [
    source 21
    target 1156
  ]
  edge [
    source 21
    target 51
  ]
  edge [
    source 21
    target 1157
  ]
  edge [
    source 21
    target 1158
  ]
  edge [
    source 21
    target 1159
  ]
  edge [
    source 21
    target 1160
  ]
  edge [
    source 21
    target 1161
  ]
  edge [
    source 21
    target 804
  ]
  edge [
    source 21
    target 1162
  ]
  edge [
    source 21
    target 1163
  ]
  edge [
    source 21
    target 1164
  ]
  edge [
    source 21
    target 1165
  ]
  edge [
    source 21
    target 1166
  ]
  edge [
    source 21
    target 1167
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 52
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 1168
  ]
  edge [
    source 22
    target 1169
  ]
  edge [
    source 22
    target 1170
  ]
  edge [
    source 22
    target 1171
  ]
  edge [
    source 22
    target 1172
  ]
  edge [
    source 22
    target 1173
  ]
  edge [
    source 22
    target 1174
  ]
  edge [
    source 22
    target 1175
  ]
  edge [
    source 22
    target 875
  ]
  edge [
    source 22
    target 509
  ]
  edge [
    source 22
    target 851
  ]
  edge [
    source 22
    target 537
  ]
  edge [
    source 22
    target 1176
  ]
  edge [
    source 22
    target 1177
  ]
  edge [
    source 22
    target 1178
  ]
  edge [
    source 22
    target 1179
  ]
  edge [
    source 22
    target 1180
  ]
  edge [
    source 22
    target 1181
  ]
  edge [
    source 22
    target 1182
  ]
  edge [
    source 22
    target 1183
  ]
  edge [
    source 22
    target 1184
  ]
  edge [
    source 22
    target 513
  ]
  edge [
    source 22
    target 518
  ]
  edge [
    source 22
    target 1185
  ]
  edge [
    source 22
    target 1186
  ]
  edge [
    source 22
    target 1187
  ]
  edge [
    source 22
    target 1188
  ]
  edge [
    source 22
    target 1189
  ]
  edge [
    source 22
    target 1190
  ]
  edge [
    source 22
    target 1191
  ]
  edge [
    source 22
    target 1192
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 1193
  ]
  edge [
    source 22
    target 1194
  ]
  edge [
    source 22
    target 1195
  ]
  edge [
    source 22
    target 1196
  ]
  edge [
    source 22
    target 1197
  ]
  edge [
    source 22
    target 1198
  ]
  edge [
    source 22
    target 1199
  ]
  edge [
    source 22
    target 1200
  ]
  edge [
    source 22
    target 1201
  ]
  edge [
    source 22
    target 860
  ]
  edge [
    source 22
    target 1202
  ]
  edge [
    source 22
    target 1203
  ]
  edge [
    source 22
    target 1204
  ]
  edge [
    source 22
    target 1205
  ]
  edge [
    source 22
    target 1206
  ]
  edge [
    source 22
    target 1207
  ]
  edge [
    source 22
    target 1208
  ]
  edge [
    source 22
    target 1209
  ]
  edge [
    source 22
    target 1210
  ]
  edge [
    source 22
    target 991
  ]
  edge [
    source 22
    target 110
  ]
  edge [
    source 22
    target 1211
  ]
  edge [
    source 22
    target 1212
  ]
  edge [
    source 22
    target 534
  ]
  edge [
    source 22
    target 1213
  ]
  edge [
    source 22
    target 1214
  ]
  edge [
    source 22
    target 1215
  ]
  edge [
    source 22
    target 1216
  ]
  edge [
    source 22
    target 845
  ]
  edge [
    source 22
    target 1217
  ]
  edge [
    source 22
    target 60
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 1218
  ]
  edge [
    source 23
    target 1219
  ]
  edge [
    source 23
    target 1220
  ]
  edge [
    source 23
    target 1221
  ]
  edge [
    source 23
    target 1222
  ]
  edge [
    source 23
    target 1223
  ]
  edge [
    source 23
    target 1224
  ]
  edge [
    source 23
    target 1225
  ]
  edge [
    source 23
    target 1226
  ]
  edge [
    source 23
    target 1227
  ]
  edge [
    source 23
    target 1228
  ]
  edge [
    source 23
    target 1229
  ]
  edge [
    source 23
    target 1230
  ]
  edge [
    source 23
    target 1231
  ]
  edge [
    source 23
    target 1232
  ]
  edge [
    source 23
    target 1233
  ]
  edge [
    source 23
    target 1234
  ]
  edge [
    source 23
    target 812
  ]
  edge [
    source 23
    target 1235
  ]
  edge [
    source 23
    target 1236
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 1237
  ]
  edge [
    source 23
    target 1238
  ]
  edge [
    source 23
    target 1239
  ]
  edge [
    source 23
    target 1240
  ]
  edge [
    source 23
    target 1241
  ]
  edge [
    source 23
    target 1242
  ]
  edge [
    source 23
    target 1243
  ]
  edge [
    source 23
    target 573
  ]
  edge [
    source 23
    target 1244
  ]
  edge [
    source 23
    target 1245
  ]
  edge [
    source 23
    target 1246
  ]
  edge [
    source 23
    target 1247
  ]
  edge [
    source 23
    target 792
  ]
  edge [
    source 23
    target 1248
  ]
  edge [
    source 23
    target 1249
  ]
  edge [
    source 23
    target 1250
  ]
  edge [
    source 23
    target 608
  ]
  edge [
    source 23
    target 1251
  ]
  edge [
    source 23
    target 1252
  ]
  edge [
    source 23
    target 1253
  ]
  edge [
    source 23
    target 1254
  ]
  edge [
    source 23
    target 1255
  ]
  edge [
    source 23
    target 649
  ]
  edge [
    source 23
    target 1256
  ]
  edge [
    source 23
    target 666
  ]
  edge [
    source 23
    target 1257
  ]
  edge [
    source 23
    target 1258
  ]
  edge [
    source 23
    target 1259
  ]
  edge [
    source 23
    target 1260
  ]
  edge [
    source 23
    target 1261
  ]
  edge [
    source 23
    target 1262
  ]
  edge [
    source 23
    target 1263
  ]
  edge [
    source 23
    target 1264
  ]
  edge [
    source 23
    target 1265
  ]
  edge [
    source 23
    target 1266
  ]
  edge [
    source 23
    target 1267
  ]
  edge [
    source 23
    target 957
  ]
  edge [
    source 23
    target 1268
  ]
  edge [
    source 23
    target 1269
  ]
  edge [
    source 23
    target 1270
  ]
  edge [
    source 23
    target 1271
  ]
  edge [
    source 23
    target 1272
  ]
  edge [
    source 23
    target 1273
  ]
  edge [
    source 23
    target 956
  ]
  edge [
    source 23
    target 1274
  ]
  edge [
    source 23
    target 1275
  ]
  edge [
    source 23
    target 1019
  ]
  edge [
    source 23
    target 1276
  ]
  edge [
    source 23
    target 1277
  ]
  edge [
    source 23
    target 1278
  ]
  edge [
    source 23
    target 1279
  ]
  edge [
    source 23
    target 1280
  ]
  edge [
    source 23
    target 1281
  ]
  edge [
    source 23
    target 1282
  ]
  edge [
    source 23
    target 1283
  ]
  edge [
    source 23
    target 1284
  ]
  edge [
    source 23
    target 1285
  ]
  edge [
    source 23
    target 1286
  ]
  edge [
    source 23
    target 1017
  ]
  edge [
    source 23
    target 1126
  ]
  edge [
    source 23
    target 1287
  ]
  edge [
    source 23
    target 1288
  ]
  edge [
    source 23
    target 1289
  ]
  edge [
    source 23
    target 1290
  ]
  edge [
    source 23
    target 1291
  ]
  edge [
    source 23
    target 1292
  ]
  edge [
    source 23
    target 1293
  ]
  edge [
    source 23
    target 1294
  ]
  edge [
    source 23
    target 1295
  ]
  edge [
    source 23
    target 629
  ]
  edge [
    source 23
    target 1296
  ]
  edge [
    source 23
    target 1297
  ]
  edge [
    source 23
    target 1298
  ]
  edge [
    source 23
    target 1299
  ]
  edge [
    source 23
    target 1300
  ]
  edge [
    source 23
    target 1301
  ]
  edge [
    source 23
    target 1302
  ]
  edge [
    source 23
    target 1303
  ]
  edge [
    source 23
    target 1304
  ]
  edge [
    source 23
    target 1305
  ]
  edge [
    source 23
    target 1306
  ]
  edge [
    source 23
    target 1307
  ]
  edge [
    source 23
    target 1308
  ]
  edge [
    source 23
    target 1309
  ]
  edge [
    source 23
    target 1310
  ]
  edge [
    source 23
    target 1311
  ]
  edge [
    source 23
    target 1312
  ]
  edge [
    source 23
    target 1313
  ]
  edge [
    source 23
    target 1314
  ]
  edge [
    source 23
    target 1315
  ]
  edge [
    source 23
    target 1316
  ]
  edge [
    source 23
    target 1317
  ]
  edge [
    source 23
    target 1318
  ]
  edge [
    source 23
    target 1319
  ]
  edge [
    source 23
    target 1320
  ]
  edge [
    source 23
    target 1321
  ]
  edge [
    source 23
    target 1322
  ]
  edge [
    source 23
    target 1323
  ]
  edge [
    source 23
    target 1324
  ]
  edge [
    source 23
    target 1325
  ]
  edge [
    source 23
    target 1326
  ]
  edge [
    source 23
    target 905
  ]
  edge [
    source 23
    target 1327
  ]
  edge [
    source 23
    target 1328
  ]
  edge [
    source 23
    target 1329
  ]
  edge [
    source 23
    target 1330
  ]
  edge [
    source 23
    target 880
  ]
  edge [
    source 23
    target 1331
  ]
  edge [
    source 23
    target 711
  ]
  edge [
    source 23
    target 1332
  ]
  edge [
    source 23
    target 1333
  ]
  edge [
    source 23
    target 1334
  ]
  edge [
    source 23
    target 1335
  ]
  edge [
    source 23
    target 182
  ]
  edge [
    source 23
    target 1336
  ]
  edge [
    source 23
    target 1337
  ]
  edge [
    source 23
    target 1338
  ]
  edge [
    source 23
    target 74
  ]
  edge [
    source 23
    target 1339
  ]
  edge [
    source 23
    target 811
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 1340
  ]
  edge [
    source 23
    target 1341
  ]
  edge [
    source 23
    target 1342
  ]
  edge [
    source 23
    target 1343
  ]
  edge [
    source 23
    target 1344
  ]
  edge [
    source 23
    target 1345
  ]
  edge [
    source 23
    target 1346
  ]
  edge [
    source 23
    target 1347
  ]
  edge [
    source 23
    target 1348
  ]
  edge [
    source 23
    target 1349
  ]
  edge [
    source 23
    target 1350
  ]
  edge [
    source 23
    target 1351
  ]
  edge [
    source 23
    target 1352
  ]
  edge [
    source 23
    target 913
  ]
  edge [
    source 23
    target 1353
  ]
  edge [
    source 23
    target 1354
  ]
  edge [
    source 23
    target 1355
  ]
  edge [
    source 23
    target 1356
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 23
    target 1357
  ]
  edge [
    source 23
    target 1358
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 1359
  ]
  edge [
    source 24
    target 1360
  ]
  edge [
    source 24
    target 1361
  ]
  edge [
    source 24
    target 1362
  ]
  edge [
    source 24
    target 1363
  ]
  edge [
    source 24
    target 807
  ]
  edge [
    source 24
    target 1364
  ]
  edge [
    source 24
    target 1365
  ]
  edge [
    source 24
    target 1366
  ]
  edge [
    source 24
    target 1367
  ]
  edge [
    source 24
    target 1368
  ]
  edge [
    source 24
    target 1369
  ]
  edge [
    source 24
    target 1260
  ]
  edge [
    source 24
    target 791
  ]
  edge [
    source 24
    target 1370
  ]
  edge [
    source 24
    target 794
  ]
  edge [
    source 24
    target 710
  ]
  edge [
    source 24
    target 1371
  ]
  edge [
    source 24
    target 1372
  ]
  edge [
    source 24
    target 797
  ]
  edge [
    source 24
    target 1373
  ]
  edge [
    source 24
    target 798
  ]
  edge [
    source 24
    target 1374
  ]
  edge [
    source 24
    target 1021
  ]
  edge [
    source 24
    target 1375
  ]
  edge [
    source 24
    target 1376
  ]
  edge [
    source 24
    target 1377
  ]
  edge [
    source 24
    target 1378
  ]
  edge [
    source 24
    target 1379
  ]
  edge [
    source 24
    target 1380
  ]
  edge [
    source 24
    target 1381
  ]
  edge [
    source 24
    target 1382
  ]
  edge [
    source 24
    target 1383
  ]
  edge [
    source 24
    target 809
  ]
  edge [
    source 24
    target 1384
  ]
  edge [
    source 24
    target 1385
  ]
  edge [
    source 24
    target 1386
  ]
  edge [
    source 24
    target 1387
  ]
  edge [
    source 24
    target 1388
  ]
  edge [
    source 24
    target 1389
  ]
  edge [
    source 24
    target 1390
  ]
  edge [
    source 24
    target 1391
  ]
  edge [
    source 24
    target 1392
  ]
  edge [
    source 24
    target 1393
  ]
  edge [
    source 24
    target 1394
  ]
  edge [
    source 24
    target 1395
  ]
  edge [
    source 24
    target 1396
  ]
  edge [
    source 24
    target 1397
  ]
  edge [
    source 24
    target 919
  ]
  edge [
    source 24
    target 1398
  ]
  edge [
    source 24
    target 1330
  ]
  edge [
    source 24
    target 1399
  ]
  edge [
    source 24
    target 1400
  ]
  edge [
    source 24
    target 1401
  ]
  edge [
    source 24
    target 1402
  ]
  edge [
    source 24
    target 1403
  ]
  edge [
    source 24
    target 666
  ]
  edge [
    source 24
    target 1404
  ]
  edge [
    source 24
    target 1405
  ]
  edge [
    source 24
    target 1406
  ]
  edge [
    source 24
    target 1407
  ]
  edge [
    source 24
    target 1408
  ]
  edge [
    source 24
    target 1409
  ]
  edge [
    source 24
    target 1410
  ]
  edge [
    source 24
    target 1411
  ]
  edge [
    source 24
    target 1412
  ]
  edge [
    source 24
    target 1413
  ]
  edge [
    source 24
    target 1414
  ]
  edge [
    source 24
    target 1415
  ]
  edge [
    source 24
    target 1416
  ]
  edge [
    source 24
    target 1417
  ]
  edge [
    source 24
    target 1418
  ]
  edge [
    source 24
    target 1419
  ]
  edge [
    source 24
    target 1420
  ]
  edge [
    source 24
    target 1421
  ]
  edge [
    source 24
    target 1422
  ]
  edge [
    source 24
    target 1423
  ]
  edge [
    source 24
    target 1424
  ]
  edge [
    source 24
    target 1425
  ]
  edge [
    source 24
    target 1426
  ]
  edge [
    source 24
    target 1427
  ]
  edge [
    source 24
    target 1428
  ]
  edge [
    source 24
    target 1429
  ]
  edge [
    source 24
    target 1430
  ]
  edge [
    source 24
    target 1431
  ]
  edge [
    source 24
    target 932
  ]
  edge [
    source 24
    target 1432
  ]
  edge [
    source 24
    target 1433
  ]
  edge [
    source 24
    target 1434
  ]
  edge [
    source 24
    target 1435
  ]
  edge [
    source 24
    target 1436
  ]
  edge [
    source 24
    target 1437
  ]
  edge [
    source 24
    target 1438
  ]
  edge [
    source 24
    target 1439
  ]
  edge [
    source 24
    target 1440
  ]
  edge [
    source 24
    target 1441
  ]
  edge [
    source 24
    target 1442
  ]
  edge [
    source 24
    target 1443
  ]
  edge [
    source 24
    target 935
  ]
  edge [
    source 24
    target 1444
  ]
  edge [
    source 24
    target 1445
  ]
  edge [
    source 24
    target 1446
  ]
  edge [
    source 24
    target 1447
  ]
  edge [
    source 24
    target 1448
  ]
  edge [
    source 24
    target 1449
  ]
  edge [
    source 24
    target 1450
  ]
  edge [
    source 24
    target 1451
  ]
  edge [
    source 24
    target 1452
  ]
  edge [
    source 24
    target 1453
  ]
  edge [
    source 24
    target 1454
  ]
  edge [
    source 24
    target 1455
  ]
  edge [
    source 24
    target 1456
  ]
  edge [
    source 24
    target 1457
  ]
  edge [
    source 24
    target 1458
  ]
  edge [
    source 24
    target 1459
  ]
  edge [
    source 24
    target 1460
  ]
  edge [
    source 24
    target 1461
  ]
  edge [
    source 24
    target 1462
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 43
  ]
  edge [
    source 24
    target 79
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 1463
  ]
  edge [
    source 25
    target 1464
  ]
  edge [
    source 25
    target 1465
  ]
  edge [
    source 25
    target 1466
  ]
  edge [
    source 25
    target 1467
  ]
  edge [
    source 26
    target 40
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 41
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 573
  ]
  edge [
    source 26
    target 280
  ]
  edge [
    source 26
    target 1468
  ]
  edge [
    source 26
    target 1469
  ]
  edge [
    source 26
    target 135
  ]
  edge [
    source 26
    target 568
  ]
  edge [
    source 26
    target 811
  ]
  edge [
    source 26
    target 1470
  ]
  edge [
    source 26
    target 1471
  ]
  edge [
    source 26
    target 1472
  ]
  edge [
    source 26
    target 1473
  ]
  edge [
    source 26
    target 1474
  ]
  edge [
    source 26
    target 1475
  ]
  edge [
    source 26
    target 1476
  ]
  edge [
    source 26
    target 1477
  ]
  edge [
    source 26
    target 1478
  ]
  edge [
    source 26
    target 1479
  ]
  edge [
    source 26
    target 1480
  ]
  edge [
    source 26
    target 1481
  ]
  edge [
    source 26
    target 1482
  ]
  edge [
    source 26
    target 1483
  ]
  edge [
    source 26
    target 1484
  ]
  edge [
    source 26
    target 1485
  ]
  edge [
    source 26
    target 697
  ]
  edge [
    source 26
    target 1486
  ]
  edge [
    source 26
    target 1487
  ]
  edge [
    source 26
    target 1017
  ]
  edge [
    source 26
    target 1488
  ]
  edge [
    source 26
    target 1489
  ]
  edge [
    source 26
    target 726
  ]
  edge [
    source 26
    target 1490
  ]
  edge [
    source 26
    target 1491
  ]
  edge [
    source 26
    target 1492
  ]
  edge [
    source 26
    target 1493
  ]
  edge [
    source 26
    target 703
  ]
  edge [
    source 26
    target 1494
  ]
  edge [
    source 26
    target 1495
  ]
  edge [
    source 26
    target 61
  ]
  edge [
    source 26
    target 730
  ]
  edge [
    source 26
    target 1496
  ]
  edge [
    source 26
    target 1497
  ]
  edge [
    source 26
    target 1498
  ]
  edge [
    source 26
    target 58
  ]
  edge [
    source 26
    target 59
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 57
  ]
  edge [
    source 27
    target 1499
  ]
  edge [
    source 27
    target 1500
  ]
  edge [
    source 27
    target 1501
  ]
  edge [
    source 27
    target 1502
  ]
  edge [
    source 27
    target 1503
  ]
  edge [
    source 27
    target 1044
  ]
  edge [
    source 27
    target 1504
  ]
  edge [
    source 27
    target 1505
  ]
  edge [
    source 27
    target 1506
  ]
  edge [
    source 27
    target 1507
  ]
  edge [
    source 27
    target 1508
  ]
  edge [
    source 27
    target 1509
  ]
  edge [
    source 27
    target 1510
  ]
  edge [
    source 27
    target 1511
  ]
  edge [
    source 27
    target 1512
  ]
  edge [
    source 27
    target 1513
  ]
  edge [
    source 27
    target 1514
  ]
  edge [
    source 27
    target 1515
  ]
  edge [
    source 27
    target 1516
  ]
  edge [
    source 27
    target 1517
  ]
  edge [
    source 27
    target 563
  ]
  edge [
    source 27
    target 1518
  ]
  edge [
    source 27
    target 1519
  ]
  edge [
    source 27
    target 1520
  ]
  edge [
    source 27
    target 1521
  ]
  edge [
    source 27
    target 1522
  ]
  edge [
    source 27
    target 1523
  ]
  edge [
    source 27
    target 1524
  ]
  edge [
    source 27
    target 1525
  ]
  edge [
    source 27
    target 1526
  ]
  edge [
    source 27
    target 3226
  ]
  edge [
    source 27
    target 3227
  ]
  edge [
    source 27
    target 3215
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 1507
  ]
  edge [
    source 28
    target 1527
  ]
  edge [
    source 28
    target 1528
  ]
  edge [
    source 28
    target 1529
  ]
  edge [
    source 28
    target 1512
  ]
  edge [
    source 28
    target 1514
  ]
  edge [
    source 28
    target 1515
  ]
  edge [
    source 28
    target 608
  ]
  edge [
    source 28
    target 1530
  ]
  edge [
    source 28
    target 1531
  ]
  edge [
    source 28
    target 1518
  ]
  edge [
    source 28
    target 1532
  ]
  edge [
    source 28
    target 1533
  ]
  edge [
    source 28
    target 1522
  ]
  edge [
    source 28
    target 1523
  ]
  edge [
    source 28
    target 1525
  ]
  edge [
    source 28
    target 1526
  ]
  edge [
    source 28
    target 1534
  ]
  edge [
    source 28
    target 1535
  ]
  edge [
    source 28
    target 1536
  ]
  edge [
    source 28
    target 1537
  ]
  edge [
    source 28
    target 1538
  ]
  edge [
    source 28
    target 1539
  ]
  edge [
    source 28
    target 1540
  ]
  edge [
    source 28
    target 1541
  ]
  edge [
    source 28
    target 1542
  ]
  edge [
    source 28
    target 1543
  ]
  edge [
    source 28
    target 1544
  ]
  edge [
    source 28
    target 1545
  ]
  edge [
    source 28
    target 1546
  ]
  edge [
    source 28
    target 1547
  ]
  edge [
    source 28
    target 1506
  ]
  edge [
    source 28
    target 1548
  ]
  edge [
    source 28
    target 1236
  ]
  edge [
    source 28
    target 1549
  ]
  edge [
    source 28
    target 1550
  ]
  edge [
    source 28
    target 1551
  ]
  edge [
    source 28
    target 1552
  ]
  edge [
    source 28
    target 1553
  ]
  edge [
    source 28
    target 1554
  ]
  edge [
    source 28
    target 789
  ]
  edge [
    source 28
    target 1555
  ]
  edge [
    source 28
    target 1556
  ]
  edge [
    source 28
    target 634
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 1557
  ]
  edge [
    source 28
    target 1558
  ]
  edge [
    source 28
    target 1559
  ]
  edge [
    source 28
    target 1560
  ]
  edge [
    source 28
    target 1561
  ]
  edge [
    source 28
    target 558
  ]
  edge [
    source 28
    target 1562
  ]
  edge [
    source 28
    target 1563
  ]
  edge [
    source 28
    target 1564
  ]
  edge [
    source 28
    target 812
  ]
  edge [
    source 28
    target 1565
  ]
  edge [
    source 28
    target 1566
  ]
  edge [
    source 28
    target 1567
  ]
  edge [
    source 28
    target 1568
  ]
  edge [
    source 28
    target 1569
  ]
  edge [
    source 28
    target 1570
  ]
  edge [
    source 28
    target 1571
  ]
  edge [
    source 28
    target 1038
  ]
  edge [
    source 28
    target 1039
  ]
  edge [
    source 28
    target 1040
  ]
  edge [
    source 28
    target 1041
  ]
  edge [
    source 28
    target 1042
  ]
  edge [
    source 28
    target 1043
  ]
  edge [
    source 28
    target 1044
  ]
  edge [
    source 28
    target 1572
  ]
  edge [
    source 28
    target 1573
  ]
  edge [
    source 28
    target 87
  ]
  edge [
    source 28
    target 1574
  ]
  edge [
    source 28
    target 1575
  ]
  edge [
    source 28
    target 1576
  ]
  edge [
    source 28
    target 1577
  ]
  edge [
    source 28
    target 1578
  ]
  edge [
    source 28
    target 1579
  ]
  edge [
    source 28
    target 1580
  ]
  edge [
    source 28
    target 1495
  ]
  edge [
    source 28
    target 1581
  ]
  edge [
    source 28
    target 1582
  ]
  edge [
    source 28
    target 1583
  ]
  edge [
    source 28
    target 1499
  ]
  edge [
    source 28
    target 561
  ]
  edge [
    source 28
    target 1584
  ]
  edge [
    source 28
    target 573
  ]
  edge [
    source 28
    target 609
  ]
  edge [
    source 28
    target 1585
  ]
  edge [
    source 28
    target 1586
  ]
  edge [
    source 28
    target 1587
  ]
  edge [
    source 28
    target 831
  ]
  edge [
    source 28
    target 1588
  ]
  edge [
    source 28
    target 1589
  ]
  edge [
    source 28
    target 1590
  ]
  edge [
    source 28
    target 1591
  ]
  edge [
    source 28
    target 1592
  ]
  edge [
    source 28
    target 1593
  ]
  edge [
    source 28
    target 1594
  ]
  edge [
    source 28
    target 1595
  ]
  edge [
    source 28
    target 1596
  ]
  edge [
    source 28
    target 1597
  ]
  edge [
    source 28
    target 904
  ]
  edge [
    source 28
    target 1598
  ]
  edge [
    source 28
    target 1392
  ]
  edge [
    source 28
    target 1599
  ]
  edge [
    source 28
    target 1600
  ]
  edge [
    source 28
    target 1601
  ]
  edge [
    source 28
    target 1602
  ]
  edge [
    source 28
    target 1603
  ]
  edge [
    source 28
    target 1604
  ]
  edge [
    source 28
    target 1605
  ]
  edge [
    source 28
    target 1606
  ]
  edge [
    source 28
    target 1607
  ]
  edge [
    source 28
    target 1388
  ]
  edge [
    source 28
    target 1608
  ]
  edge [
    source 28
    target 934
  ]
  edge [
    source 28
    target 1609
  ]
  edge [
    source 28
    target 1610
  ]
  edge [
    source 28
    target 666
  ]
  edge [
    source 28
    target 1611
  ]
  edge [
    source 28
    target 1612
  ]
  edge [
    source 28
    target 1613
  ]
  edge [
    source 28
    target 1614
  ]
  edge [
    source 28
    target 1615
  ]
  edge [
    source 28
    target 1616
  ]
  edge [
    source 28
    target 1617
  ]
  edge [
    source 28
    target 1618
  ]
  edge [
    source 28
    target 1619
  ]
  edge [
    source 28
    target 1620
  ]
  edge [
    source 28
    target 1621
  ]
  edge [
    source 28
    target 1622
  ]
  edge [
    source 28
    target 1623
  ]
  edge [
    source 28
    target 1624
  ]
  edge [
    source 28
    target 1625
  ]
  edge [
    source 28
    target 3226
  ]
  edge [
    source 28
    target 3227
  ]
  edge [
    source 28
    target 3215
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 818
  ]
  edge [
    source 29
    target 1626
  ]
  edge [
    source 29
    target 1627
  ]
  edge [
    source 29
    target 1628
  ]
  edge [
    source 29
    target 1075
  ]
  edge [
    source 29
    target 1629
  ]
  edge [
    source 29
    target 1630
  ]
  edge [
    source 29
    target 1618
  ]
  edge [
    source 29
    target 1631
  ]
  edge [
    source 29
    target 1632
  ]
  edge [
    source 29
    target 1079
  ]
  edge [
    source 29
    target 1633
  ]
  edge [
    source 29
    target 1634
  ]
  edge [
    source 29
    target 1635
  ]
  edge [
    source 29
    target 1636
  ]
  edge [
    source 29
    target 1637
  ]
  edge [
    source 29
    target 1638
  ]
  edge [
    source 29
    target 1086
  ]
  edge [
    source 29
    target 52
  ]
  edge [
    source 29
    target 41
  ]
  edge [
    source 29
    target 44
  ]
  edge [
    source 29
    target 78
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 43
  ]
  edge [
    source 30
    target 79
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 1639
  ]
  edge [
    source 31
    target 1640
  ]
  edge [
    source 31
    target 1641
  ]
  edge [
    source 31
    target 1642
  ]
  edge [
    source 31
    target 1643
  ]
  edge [
    source 31
    target 1644
  ]
  edge [
    source 31
    target 1645
  ]
  edge [
    source 31
    target 666
  ]
  edge [
    source 31
    target 1646
  ]
  edge [
    source 31
    target 598
  ]
  edge [
    source 31
    target 1647
  ]
  edge [
    source 31
    target 1648
  ]
  edge [
    source 31
    target 1649
  ]
  edge [
    source 31
    target 1650
  ]
  edge [
    source 31
    target 544
  ]
  edge [
    source 31
    target 1651
  ]
  edge [
    source 31
    target 1652
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 31
    target 42
  ]
  edge [
    source 31
    target 80
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 173
  ]
  edge [
    source 32
    target 1236
  ]
  edge [
    source 32
    target 621
  ]
  edge [
    source 32
    target 1653
  ]
  edge [
    source 32
    target 689
  ]
  edge [
    source 32
    target 1654
  ]
  edge [
    source 32
    target 1240
  ]
  edge [
    source 32
    target 1655
  ]
  edge [
    source 32
    target 1656
  ]
  edge [
    source 32
    target 1657
  ]
  edge [
    source 32
    target 1658
  ]
  edge [
    source 32
    target 1659
  ]
  edge [
    source 32
    target 1101
  ]
  edge [
    source 32
    target 1660
  ]
  edge [
    source 32
    target 1661
  ]
  edge [
    source 32
    target 1662
  ]
  edge [
    source 32
    target 1663
  ]
  edge [
    source 32
    target 1664
  ]
  edge [
    source 32
    target 1665
  ]
  edge [
    source 32
    target 1666
  ]
  edge [
    source 32
    target 1667
  ]
  edge [
    source 32
    target 1668
  ]
  edge [
    source 32
    target 1669
  ]
  edge [
    source 32
    target 1670
  ]
  edge [
    source 32
    target 1671
  ]
  edge [
    source 32
    target 1672
  ]
  edge [
    source 32
    target 1673
  ]
  edge [
    source 32
    target 1674
  ]
  edge [
    source 32
    target 1675
  ]
  edge [
    source 32
    target 1676
  ]
  edge [
    source 32
    target 1677
  ]
  edge [
    source 32
    target 1678
  ]
  edge [
    source 32
    target 1679
  ]
  edge [
    source 32
    target 1680
  ]
  edge [
    source 32
    target 1681
  ]
  edge [
    source 32
    target 1682
  ]
  edge [
    source 32
    target 1683
  ]
  edge [
    source 32
    target 1684
  ]
  edge [
    source 32
    target 1685
  ]
  edge [
    source 32
    target 1686
  ]
  edge [
    source 32
    target 1687
  ]
  edge [
    source 32
    target 1688
  ]
  edge [
    source 32
    target 1689
  ]
  edge [
    source 32
    target 1690
  ]
  edge [
    source 32
    target 1248
  ]
  edge [
    source 32
    target 1286
  ]
  edge [
    source 32
    target 1017
  ]
  edge [
    source 32
    target 1126
  ]
  edge [
    source 32
    target 1287
  ]
  edge [
    source 32
    target 1691
  ]
  edge [
    source 32
    target 1692
  ]
  edge [
    source 32
    target 1693
  ]
  edge [
    source 32
    target 1694
  ]
  edge [
    source 32
    target 1695
  ]
  edge [
    source 32
    target 1696
  ]
  edge [
    source 32
    target 1697
  ]
  edge [
    source 32
    target 1698
  ]
  edge [
    source 32
    target 710
  ]
  edge [
    source 32
    target 1699
  ]
  edge [
    source 32
    target 1700
  ]
  edge [
    source 32
    target 1701
  ]
  edge [
    source 32
    target 816
  ]
  edge [
    source 32
    target 1702
  ]
  edge [
    source 32
    target 1703
  ]
  edge [
    source 32
    target 1704
  ]
  edge [
    source 32
    target 1705
  ]
  edge [
    source 32
    target 1706
  ]
  edge [
    source 32
    target 722
  ]
  edge [
    source 32
    target 1707
  ]
  edge [
    source 32
    target 1708
  ]
  edge [
    source 32
    target 1709
  ]
  edge [
    source 32
    target 972
  ]
  edge [
    source 32
    target 803
  ]
  edge [
    source 32
    target 1710
  ]
  edge [
    source 32
    target 1711
  ]
  edge [
    source 32
    target 1712
  ]
  edge [
    source 32
    target 1713
  ]
  edge [
    source 32
    target 1714
  ]
  edge [
    source 32
    target 1715
  ]
  edge [
    source 32
    target 792
  ]
  edge [
    source 32
    target 584
  ]
  edge [
    source 32
    target 1373
  ]
  edge [
    source 32
    target 57
  ]
  edge [
    source 32
    target 1266
  ]
  edge [
    source 32
    target 1267
  ]
  edge [
    source 32
    target 957
  ]
  edge [
    source 32
    target 1268
  ]
  edge [
    source 32
    target 1269
  ]
  edge [
    source 32
    target 1270
  ]
  edge [
    source 32
    target 1271
  ]
  edge [
    source 32
    target 905
  ]
  edge [
    source 32
    target 1716
  ]
  edge [
    source 32
    target 1717
  ]
  edge [
    source 32
    target 789
  ]
  edge [
    source 32
    target 1718
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 1719
  ]
  edge [
    source 32
    target 1720
  ]
  edge [
    source 32
    target 42
  ]
  edge [
    source 32
    target 80
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 71
  ]
  edge [
    source 33
    target 81
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 1721
  ]
  edge [
    source 33
    target 1722
  ]
  edge [
    source 33
    target 1723
  ]
  edge [
    source 33
    target 1724
  ]
  edge [
    source 33
    target 1725
  ]
  edge [
    source 33
    target 1726
  ]
  edge [
    source 33
    target 1727
  ]
  edge [
    source 33
    target 1728
  ]
  edge [
    source 33
    target 1729
  ]
  edge [
    source 33
    target 1730
  ]
  edge [
    source 33
    target 48
  ]
  edge [
    source 33
    target 1731
  ]
  edge [
    source 33
    target 1732
  ]
  edge [
    source 33
    target 1733
  ]
  edge [
    source 33
    target 1734
  ]
  edge [
    source 33
    target 1735
  ]
  edge [
    source 33
    target 1736
  ]
  edge [
    source 33
    target 1737
  ]
  edge [
    source 33
    target 1738
  ]
  edge [
    source 33
    target 64
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 1739
  ]
  edge [
    source 34
    target 680
  ]
  edge [
    source 34
    target 1740
  ]
  edge [
    source 34
    target 1067
  ]
  edge [
    source 34
    target 1741
  ]
  edge [
    source 34
    target 1408
  ]
  edge [
    source 34
    target 1742
  ]
  edge [
    source 34
    target 792
  ]
  edge [
    source 34
    target 1743
  ]
  edge [
    source 34
    target 662
  ]
  edge [
    source 34
    target 932
  ]
  edge [
    source 34
    target 1744
  ]
  edge [
    source 34
    target 1745
  ]
  edge [
    source 34
    target 1746
  ]
  edge [
    source 34
    target 1747
  ]
  edge [
    source 34
    target 1748
  ]
  edge [
    source 34
    target 1749
  ]
  edge [
    source 34
    target 1750
  ]
  edge [
    source 34
    target 512
  ]
  edge [
    source 34
    target 1751
  ]
  edge [
    source 34
    target 940
  ]
  edge [
    source 34
    target 941
  ]
  edge [
    source 34
    target 942
  ]
  edge [
    source 34
    target 943
  ]
  edge [
    source 34
    target 666
  ]
  edge [
    source 34
    target 1752
  ]
  edge [
    source 34
    target 1753
  ]
  edge [
    source 34
    target 919
  ]
  edge [
    source 34
    target 1754
  ]
  edge [
    source 34
    target 1755
  ]
  edge [
    source 34
    target 1756
  ]
  edge [
    source 34
    target 1430
  ]
  edge [
    source 34
    target 985
  ]
  edge [
    source 34
    target 1757
  ]
  edge [
    source 34
    target 1758
  ]
  edge [
    source 34
    target 922
  ]
  edge [
    source 34
    target 1759
  ]
  edge [
    source 34
    target 1760
  ]
  edge [
    source 34
    target 1761
  ]
  edge [
    source 34
    target 1762
  ]
  edge [
    source 34
    target 1763
  ]
  edge [
    source 34
    target 1764
  ]
  edge [
    source 34
    target 1765
  ]
  edge [
    source 34
    target 1766
  ]
  edge [
    source 34
    target 1767
  ]
  edge [
    source 34
    target 1768
  ]
  edge [
    source 34
    target 1769
  ]
  edge [
    source 34
    target 1770
  ]
  edge [
    source 34
    target 1771
  ]
  edge [
    source 34
    target 1772
  ]
  edge [
    source 34
    target 1773
  ]
  edge [
    source 34
    target 635
  ]
  edge [
    source 34
    target 1774
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 1775
  ]
  edge [
    source 35
    target 1776
  ]
  edge [
    source 35
    target 1777
  ]
  edge [
    source 35
    target 1778
  ]
  edge [
    source 35
    target 1779
  ]
  edge [
    source 35
    target 1780
  ]
  edge [
    source 35
    target 1781
  ]
  edge [
    source 35
    target 1782
  ]
  edge [
    source 35
    target 1783
  ]
  edge [
    source 35
    target 1784
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 72
  ]
  edge [
    source 36
    target 73
  ]
  edge [
    source 36
    target 890
  ]
  edge [
    source 36
    target 894
  ]
  edge [
    source 36
    target 1785
  ]
  edge [
    source 36
    target 1141
  ]
  edge [
    source 36
    target 668
  ]
  edge [
    source 36
    target 1786
  ]
  edge [
    source 36
    target 1713
  ]
  edge [
    source 36
    target 1787
  ]
  edge [
    source 36
    target 1788
  ]
  edge [
    source 36
    target 924
  ]
  edge [
    source 36
    target 1789
  ]
  edge [
    source 36
    target 893
  ]
  edge [
    source 36
    target 1790
  ]
  edge [
    source 36
    target 639
  ]
  edge [
    source 36
    target 1791
  ]
  edge [
    source 36
    target 1792
  ]
  edge [
    source 36
    target 1793
  ]
  edge [
    source 36
    target 792
  ]
  edge [
    source 36
    target 891
  ]
  edge [
    source 36
    target 1794
  ]
  edge [
    source 36
    target 1013
  ]
  edge [
    source 36
    target 1795
  ]
  edge [
    source 36
    target 1796
  ]
  edge [
    source 36
    target 51
  ]
  edge [
    source 36
    target 74
  ]
  edge [
    source 36
    target 81
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 1727
  ]
  edge [
    source 37
    target 1728
  ]
  edge [
    source 37
    target 1729
  ]
  edge [
    source 37
    target 1730
  ]
  edge [
    source 37
    target 1797
  ]
  edge [
    source 37
    target 1798
  ]
  edge [
    source 37
    target 1721
  ]
  edge [
    source 37
    target 1722
  ]
  edge [
    source 37
    target 1723
  ]
  edge [
    source 37
    target 1724
  ]
  edge [
    source 37
    target 1799
  ]
  edge [
    source 37
    target 77
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 1730
  ]
  edge [
    source 38
    target 1800
  ]
  edge [
    source 38
    target 1215
  ]
  edge [
    source 38
    target 1797
  ]
  edge [
    source 38
    target 537
  ]
  edge [
    source 38
    target 1801
  ]
  edge [
    source 38
    target 1802
  ]
  edge [
    source 38
    target 1803
  ]
  edge [
    source 38
    target 1798
  ]
  edge [
    source 39
    target 135
  ]
  edge [
    source 39
    target 1804
  ]
  edge [
    source 39
    target 280
  ]
  edge [
    source 39
    target 188
  ]
  edge [
    source 39
    target 1805
  ]
  edge [
    source 39
    target 1806
  ]
  edge [
    source 39
    target 1807
  ]
  edge [
    source 39
    target 1808
  ]
  edge [
    source 39
    target 87
  ]
  edge [
    source 39
    target 1809
  ]
  edge [
    source 39
    target 1810
  ]
  edge [
    source 39
    target 1811
  ]
  edge [
    source 39
    target 1812
  ]
  edge [
    source 39
    target 98
  ]
  edge [
    source 39
    target 1813
  ]
  edge [
    source 39
    target 1336
  ]
  edge [
    source 39
    target 1267
  ]
  edge [
    source 39
    target 1814
  ]
  edge [
    source 39
    target 1234
  ]
  edge [
    source 39
    target 678
  ]
  edge [
    source 39
    target 1356
  ]
  edge [
    source 39
    target 105
  ]
  edge [
    source 39
    target 213
  ]
  edge [
    source 39
    target 215
  ]
  edge [
    source 39
    target 220
  ]
  edge [
    source 39
    target 218
  ]
  edge [
    source 39
    target 217
  ]
  edge [
    source 39
    target 568
  ]
  edge [
    source 39
    target 811
  ]
  edge [
    source 39
    target 1470
  ]
  edge [
    source 39
    target 1471
  ]
  edge [
    source 39
    target 1472
  ]
  edge [
    source 39
    target 573
  ]
  edge [
    source 39
    target 1473
  ]
  edge [
    source 39
    target 1474
  ]
  edge [
    source 39
    target 1475
  ]
  edge [
    source 39
    target 1476
  ]
  edge [
    source 39
    target 1477
  ]
  edge [
    source 39
    target 1478
  ]
  edge [
    source 39
    target 1479
  ]
  edge [
    source 39
    target 1480
  ]
  edge [
    source 39
    target 1481
  ]
  edge [
    source 39
    target 1482
  ]
  edge [
    source 39
    target 1483
  ]
  edge [
    source 39
    target 1484
  ]
  edge [
    source 39
    target 1485
  ]
  edge [
    source 39
    target 904
  ]
  edge [
    source 39
    target 956
  ]
  edge [
    source 39
    target 1815
  ]
  edge [
    source 39
    target 1816
  ]
  edge [
    source 39
    target 1817
  ]
  edge [
    source 39
    target 934
  ]
  edge [
    source 39
    target 939
  ]
  edge [
    source 40
    target 1818
  ]
  edge [
    source 40
    target 1819
  ]
  edge [
    source 40
    target 513
  ]
  edge [
    source 40
    target 1820
  ]
  edge [
    source 40
    target 1821
  ]
  edge [
    source 40
    target 518
  ]
  edge [
    source 40
    target 1822
  ]
  edge [
    source 40
    target 1650
  ]
  edge [
    source 40
    target 1823
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 78
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 1824
  ]
  edge [
    source 42
    target 1075
  ]
  edge [
    source 42
    target 1825
  ]
  edge [
    source 42
    target 1778
  ]
  edge [
    source 42
    target 1826
  ]
  edge [
    source 42
    target 1827
  ]
  edge [
    source 42
    target 1828
  ]
  edge [
    source 42
    target 1829
  ]
  edge [
    source 42
    target 1830
  ]
  edge [
    source 42
    target 1831
  ]
  edge [
    source 42
    target 1832
  ]
  edge [
    source 42
    target 1833
  ]
  edge [
    source 42
    target 1113
  ]
  edge [
    source 42
    target 1834
  ]
  edge [
    source 42
    target 1086
  ]
  edge [
    source 42
    target 1494
  ]
  edge [
    source 42
    target 1835
  ]
  edge [
    source 42
    target 1836
  ]
  edge [
    source 42
    target 1837
  ]
  edge [
    source 42
    target 1838
  ]
  edge [
    source 42
    target 1839
  ]
  edge [
    source 42
    target 1840
  ]
  edge [
    source 42
    target 1841
  ]
  edge [
    source 42
    target 1842
  ]
  edge [
    source 42
    target 1843
  ]
  edge [
    source 42
    target 1844
  ]
  edge [
    source 42
    target 1845
  ]
  edge [
    source 42
    target 1846
  ]
  edge [
    source 42
    target 1130
  ]
  edge [
    source 42
    target 1847
  ]
  edge [
    source 42
    target 1848
  ]
  edge [
    source 42
    target 80
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 1849
  ]
  edge [
    source 43
    target 1850
  ]
  edge [
    source 43
    target 1851
  ]
  edge [
    source 43
    target 1852
  ]
  edge [
    source 43
    target 1853
  ]
  edge [
    source 43
    target 1854
  ]
  edge [
    source 43
    target 1855
  ]
  edge [
    source 43
    target 508
  ]
  edge [
    source 43
    target 1856
  ]
  edge [
    source 43
    target 1857
  ]
  edge [
    source 43
    target 1858
  ]
  edge [
    source 43
    target 79
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 1859
  ]
  edge [
    source 44
    target 1860
  ]
  edge [
    source 44
    target 1861
  ]
  edge [
    source 44
    target 1862
  ]
  edge [
    source 44
    target 1863
  ]
  edge [
    source 44
    target 1864
  ]
  edge [
    source 44
    target 1865
  ]
  edge [
    source 44
    target 1866
  ]
  edge [
    source 44
    target 1867
  ]
  edge [
    source 44
    target 1868
  ]
  edge [
    source 44
    target 1869
  ]
  edge [
    source 44
    target 1870
  ]
  edge [
    source 44
    target 1188
  ]
  edge [
    source 44
    target 1871
  ]
  edge [
    source 44
    target 1067
  ]
  edge [
    source 44
    target 1872
  ]
  edge [
    source 44
    target 1873
  ]
  edge [
    source 44
    target 1874
  ]
  edge [
    source 44
    target 1875
  ]
  edge [
    source 44
    target 60
  ]
  edge [
    source 44
    target 1876
  ]
  edge [
    source 44
    target 1877
  ]
  edge [
    source 44
    target 1735
  ]
  edge [
    source 44
    target 1747
  ]
  edge [
    source 44
    target 1878
  ]
  edge [
    source 44
    target 1879
  ]
  edge [
    source 44
    target 1003
  ]
  edge [
    source 44
    target 1880
  ]
  edge [
    source 44
    target 1881
  ]
  edge [
    source 44
    target 554
  ]
  edge [
    source 44
    target 1882
  ]
  edge [
    source 44
    target 537
  ]
  edge [
    source 44
    target 1883
  ]
  edge [
    source 44
    target 1884
  ]
  edge [
    source 44
    target 1885
  ]
  edge [
    source 44
    target 1886
  ]
  edge [
    source 44
    target 1887
  ]
  edge [
    source 44
    target 1888
  ]
  edge [
    source 44
    target 1889
  ]
  edge [
    source 44
    target 1890
  ]
  edge [
    source 44
    target 1891
  ]
  edge [
    source 44
    target 1892
  ]
  edge [
    source 44
    target 1893
  ]
  edge [
    source 44
    target 1894
  ]
  edge [
    source 44
    target 1837
  ]
  edge [
    source 44
    target 78
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 1895
  ]
  edge [
    source 45
    target 1135
  ]
  edge [
    source 45
    target 1896
  ]
  edge [
    source 45
    target 1897
  ]
  edge [
    source 45
    target 1898
  ]
  edge [
    source 45
    target 1899
  ]
  edge [
    source 45
    target 1900
  ]
  edge [
    source 45
    target 1901
  ]
  edge [
    source 45
    target 1902
  ]
  edge [
    source 45
    target 1903
  ]
  edge [
    source 45
    target 1388
  ]
  edge [
    source 45
    target 1904
  ]
  edge [
    source 45
    target 1905
  ]
  edge [
    source 45
    target 1906
  ]
  edge [
    source 45
    target 1907
  ]
  edge [
    source 45
    target 1908
  ]
  edge [
    source 45
    target 1909
  ]
  edge [
    source 45
    target 1910
  ]
  edge [
    source 45
    target 1911
  ]
  edge [
    source 45
    target 1640
  ]
  edge [
    source 45
    target 1912
  ]
  edge [
    source 45
    target 1913
  ]
  edge [
    source 45
    target 666
  ]
  edge [
    source 45
    target 1914
  ]
  edge [
    source 45
    target 1915
  ]
  edge [
    source 45
    target 1916
  ]
  edge [
    source 45
    target 1917
  ]
  edge [
    source 45
    target 1918
  ]
  edge [
    source 45
    target 1644
  ]
  edge [
    source 45
    target 598
  ]
  edge [
    source 45
    target 1645
  ]
  edge [
    source 45
    target 1646
  ]
  edge [
    source 45
    target 1919
  ]
  edge [
    source 45
    target 1920
  ]
  edge [
    source 45
    target 1762
  ]
  edge [
    source 45
    target 182
  ]
  edge [
    source 45
    target 892
  ]
  edge [
    source 45
    target 1921
  ]
  edge [
    source 45
    target 1922
  ]
  edge [
    source 45
    target 1923
  ]
  edge [
    source 45
    target 1924
  ]
  edge [
    source 45
    target 1392
  ]
  edge [
    source 45
    target 1925
  ]
  edge [
    source 45
    target 1926
  ]
  edge [
    source 45
    target 1927
  ]
  edge [
    source 45
    target 1928
  ]
  edge [
    source 45
    target 1929
  ]
  edge [
    source 45
    target 1930
  ]
  edge [
    source 45
    target 1931
  ]
  edge [
    source 45
    target 1932
  ]
  edge [
    source 45
    target 1933
  ]
  edge [
    source 45
    target 1934
  ]
  edge [
    source 45
    target 1935
  ]
  edge [
    source 45
    target 1936
  ]
  edge [
    source 45
    target 1937
  ]
  edge [
    source 45
    target 1938
  ]
  edge [
    source 45
    target 1939
  ]
  edge [
    source 45
    target 1940
  ]
  edge [
    source 45
    target 1941
  ]
  edge [
    source 45
    target 1942
  ]
  edge [
    source 45
    target 1943
  ]
  edge [
    source 45
    target 1944
  ]
  edge [
    source 45
    target 1945
  ]
  edge [
    source 45
    target 1946
  ]
  edge [
    source 45
    target 1947
  ]
  edge [
    source 45
    target 1948
  ]
  edge [
    source 45
    target 1949
  ]
  edge [
    source 45
    target 1950
  ]
  edge [
    source 45
    target 1951
  ]
  edge [
    source 45
    target 1952
  ]
  edge [
    source 45
    target 1953
  ]
  edge [
    source 45
    target 1954
  ]
  edge [
    source 45
    target 1955
  ]
  edge [
    source 45
    target 1956
  ]
  edge [
    source 45
    target 1957
  ]
  edge [
    source 45
    target 1958
  ]
  edge [
    source 45
    target 1959
  ]
  edge [
    source 45
    target 1960
  ]
  edge [
    source 45
    target 1961
  ]
  edge [
    source 45
    target 1962
  ]
  edge [
    source 45
    target 1963
  ]
  edge [
    source 45
    target 1964
  ]
  edge [
    source 45
    target 1965
  ]
  edge [
    source 45
    target 1966
  ]
  edge [
    source 45
    target 1967
  ]
  edge [
    source 45
    target 1968
  ]
  edge [
    source 45
    target 1969
  ]
  edge [
    source 45
    target 1970
  ]
  edge [
    source 45
    target 1971
  ]
  edge [
    source 45
    target 1972
  ]
  edge [
    source 45
    target 1973
  ]
  edge [
    source 45
    target 1974
  ]
  edge [
    source 45
    target 1975
  ]
  edge [
    source 45
    target 1976
  ]
  edge [
    source 45
    target 1977
  ]
  edge [
    source 45
    target 1978
  ]
  edge [
    source 45
    target 1979
  ]
  edge [
    source 45
    target 662
  ]
  edge [
    source 45
    target 1980
  ]
  edge [
    source 45
    target 1981
  ]
  edge [
    source 45
    target 1982
  ]
  edge [
    source 45
    target 1983
  ]
  edge [
    source 45
    target 1984
  ]
  edge [
    source 45
    target 1985
  ]
  edge [
    source 45
    target 1986
  ]
  edge [
    source 45
    target 1987
  ]
  edge [
    source 45
    target 1988
  ]
  edge [
    source 45
    target 1989
  ]
  edge [
    source 45
    target 1990
  ]
  edge [
    source 45
    target 1991
  ]
  edge [
    source 45
    target 1367
  ]
  edge [
    source 45
    target 1992
  ]
  edge [
    source 45
    target 1993
  ]
  edge [
    source 45
    target 1994
  ]
  edge [
    source 45
    target 1995
  ]
  edge [
    source 45
    target 1996
  ]
  edge [
    source 45
    target 1997
  ]
  edge [
    source 45
    target 1998
  ]
  edge [
    source 45
    target 1999
  ]
  edge [
    source 45
    target 2000
  ]
  edge [
    source 45
    target 2001
  ]
  edge [
    source 45
    target 2002
  ]
  edge [
    source 45
    target 2003
  ]
  edge [
    source 45
    target 2004
  ]
  edge [
    source 45
    target 2005
  ]
  edge [
    source 45
    target 2006
  ]
  edge [
    source 45
    target 922
  ]
  edge [
    source 45
    target 1760
  ]
  edge [
    source 45
    target 2007
  ]
  edge [
    source 45
    target 2008
  ]
  edge [
    source 45
    target 930
  ]
  edge [
    source 45
    target 2009
  ]
  edge [
    source 45
    target 2010
  ]
  edge [
    source 45
    target 2011
  ]
  edge [
    source 45
    target 2012
  ]
  edge [
    source 45
    target 2013
  ]
  edge [
    source 45
    target 2014
  ]
  edge [
    source 45
    target 919
  ]
  edge [
    source 45
    target 1829
  ]
  edge [
    source 45
    target 2015
  ]
  edge [
    source 45
    target 2016
  ]
  edge [
    source 45
    target 1008
  ]
  edge [
    source 45
    target 2017
  ]
  edge [
    source 45
    target 2018
  ]
  edge [
    source 45
    target 2019
  ]
  edge [
    source 45
    target 2020
  ]
  edge [
    source 45
    target 2021
  ]
  edge [
    source 45
    target 2022
  ]
  edge [
    source 45
    target 2023
  ]
  edge [
    source 45
    target 2024
  ]
  edge [
    source 45
    target 2025
  ]
  edge [
    source 45
    target 2026
  ]
  edge [
    source 45
    target 2027
  ]
  edge [
    source 45
    target 1685
  ]
  edge [
    source 45
    target 2028
  ]
  edge [
    source 45
    target 2029
  ]
  edge [
    source 45
    target 2030
  ]
  edge [
    source 45
    target 2031
  ]
  edge [
    source 45
    target 2032
  ]
  edge [
    source 45
    target 2033
  ]
  edge [
    source 45
    target 932
  ]
  edge [
    source 45
    target 2034
  ]
  edge [
    source 45
    target 956
  ]
  edge [
    source 45
    target 1386
  ]
  edge [
    source 45
    target 942
  ]
  edge [
    source 45
    target 2035
  ]
  edge [
    source 45
    target 2036
  ]
  edge [
    source 45
    target 2037
  ]
  edge [
    source 45
    target 943
  ]
  edge [
    source 45
    target 2038
  ]
  edge [
    source 45
    target 918
  ]
  edge [
    source 45
    target 2039
  ]
  edge [
    source 45
    target 639
  ]
  edge [
    source 45
    target 2040
  ]
  edge [
    source 45
    target 2041
  ]
  edge [
    source 45
    target 1402
  ]
  edge [
    source 45
    target 2042
  ]
  edge [
    source 45
    target 2043
  ]
  edge [
    source 45
    target 2044
  ]
  edge [
    source 45
    target 2045
  ]
  edge [
    source 45
    target 2046
  ]
  edge [
    source 45
    target 2047
  ]
  edge [
    source 45
    target 2048
  ]
  edge [
    source 45
    target 2049
  ]
  edge [
    source 45
    target 2050
  ]
  edge [
    source 45
    target 2051
  ]
  edge [
    source 45
    target 2052
  ]
  edge [
    source 45
    target 621
  ]
  edge [
    source 45
    target 2053
  ]
  edge [
    source 45
    target 2054
  ]
  edge [
    source 45
    target 2055
  ]
  edge [
    source 45
    target 2056
  ]
  edge [
    source 45
    target 2057
  ]
  edge [
    source 45
    target 2058
  ]
  edge [
    source 45
    target 2059
  ]
  edge [
    source 45
    target 2060
  ]
  edge [
    source 45
    target 2061
  ]
  edge [
    source 45
    target 2062
  ]
  edge [
    source 45
    target 2063
  ]
  edge [
    source 45
    target 2064
  ]
  edge [
    source 45
    target 2065
  ]
  edge [
    source 45
    target 2066
  ]
  edge [
    source 45
    target 2067
  ]
  edge [
    source 45
    target 2068
  ]
  edge [
    source 45
    target 2069
  ]
  edge [
    source 45
    target 2070
  ]
  edge [
    source 45
    target 2071
  ]
  edge [
    source 45
    target 2072
  ]
  edge [
    source 45
    target 2073
  ]
  edge [
    source 45
    target 2074
  ]
  edge [
    source 45
    target 2075
  ]
  edge [
    source 45
    target 2076
  ]
  edge [
    source 45
    target 2077
  ]
  edge [
    source 45
    target 2078
  ]
  edge [
    source 45
    target 2079
  ]
  edge [
    source 45
    target 2080
  ]
  edge [
    source 45
    target 2081
  ]
  edge [
    source 45
    target 2082
  ]
  edge [
    source 45
    target 2083
  ]
  edge [
    source 45
    target 2084
  ]
  edge [
    source 45
    target 2085
  ]
  edge [
    source 45
    target 2086
  ]
  edge [
    source 45
    target 2087
  ]
  edge [
    source 45
    target 2088
  ]
  edge [
    source 45
    target 2089
  ]
  edge [
    source 45
    target 2090
  ]
  edge [
    source 45
    target 2091
  ]
  edge [
    source 45
    target 2092
  ]
  edge [
    source 45
    target 2093
  ]
  edge [
    source 45
    target 2094
  ]
  edge [
    source 45
    target 2095
  ]
  edge [
    source 45
    target 2096
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 2097
  ]
  edge [
    source 46
    target 2098
  ]
  edge [
    source 46
    target 2099
  ]
  edge [
    source 46
    target 2100
  ]
  edge [
    source 46
    target 2101
  ]
  edge [
    source 46
    target 2102
  ]
  edge [
    source 46
    target 2103
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 58
  ]
  edge [
    source 47
    target 59
  ]
  edge [
    source 47
    target 2104
  ]
  edge [
    source 47
    target 2105
  ]
  edge [
    source 47
    target 2106
  ]
  edge [
    source 47
    target 2107
  ]
  edge [
    source 47
    target 2108
  ]
  edge [
    source 47
    target 2109
  ]
  edge [
    source 47
    target 2110
  ]
  edge [
    source 47
    target 1408
  ]
  edge [
    source 47
    target 2111
  ]
  edge [
    source 47
    target 919
  ]
  edge [
    source 47
    target 2112
  ]
  edge [
    source 47
    target 2113
  ]
  edge [
    source 47
    target 2114
  ]
  edge [
    source 47
    target 2115
  ]
  edge [
    source 47
    target 661
  ]
  edge [
    source 47
    target 2116
  ]
  edge [
    source 47
    target 666
  ]
  edge [
    source 47
    target 931
  ]
  edge [
    source 47
    target 2117
  ]
  edge [
    source 47
    target 599
  ]
  edge [
    source 47
    target 2118
  ]
  edge [
    source 47
    target 182
  ]
  edge [
    source 47
    target 2119
  ]
  edge [
    source 47
    target 1080
  ]
  edge [
    source 47
    target 2120
  ]
  edge [
    source 47
    target 2121
  ]
  edge [
    source 47
    target 2122
  ]
  edge [
    source 47
    target 2123
  ]
  edge [
    source 47
    target 2124
  ]
  edge [
    source 47
    target 1774
  ]
  edge [
    source 47
    target 932
  ]
  edge [
    source 47
    target 2125
  ]
  edge [
    source 47
    target 2126
  ]
  edge [
    source 47
    target 2127
  ]
  edge [
    source 47
    target 2128
  ]
  edge [
    source 47
    target 2129
  ]
  edge [
    source 47
    target 2130
  ]
  edge [
    source 47
    target 2131
  ]
  edge [
    source 47
    target 1795
  ]
  edge [
    source 47
    target 2132
  ]
  edge [
    source 47
    target 2133
  ]
  edge [
    source 47
    target 2134
  ]
  edge [
    source 47
    target 2135
  ]
  edge [
    source 47
    target 2136
  ]
  edge [
    source 47
    target 683
  ]
  edge [
    source 47
    target 678
  ]
  edge [
    source 47
    target 2137
  ]
  edge [
    source 47
    target 183
  ]
  edge [
    source 47
    target 2138
  ]
  edge [
    source 47
    target 2139
  ]
  edge [
    source 47
    target 649
  ]
  edge [
    source 47
    target 679
  ]
  edge [
    source 47
    target 680
  ]
  edge [
    source 47
    target 2140
  ]
  edge [
    source 47
    target 1455
  ]
  edge [
    source 47
    target 2141
  ]
  edge [
    source 47
    target 2142
  ]
  edge [
    source 47
    target 2143
  ]
  edge [
    source 47
    target 1757
  ]
  edge [
    source 47
    target 1758
  ]
  edge [
    source 47
    target 922
  ]
  edge [
    source 47
    target 1759
  ]
  edge [
    source 47
    target 1760
  ]
  edge [
    source 47
    target 1761
  ]
  edge [
    source 47
    target 1762
  ]
  edge [
    source 47
    target 918
  ]
  edge [
    source 47
    target 2039
  ]
  edge [
    source 47
    target 639
  ]
  edge [
    source 47
    target 944
  ]
  edge [
    source 47
    target 945
  ]
  edge [
    source 47
    target 967
  ]
  edge [
    source 47
    target 968
  ]
  edge [
    source 47
    target 969
  ]
  edge [
    source 47
    target 2144
  ]
  edge [
    source 47
    target 2145
  ]
  edge [
    source 47
    target 2146
  ]
  edge [
    source 47
    target 2147
  ]
  edge [
    source 47
    target 2148
  ]
  edge [
    source 47
    target 2149
  ]
  edge [
    source 47
    target 2150
  ]
  edge [
    source 47
    target 2151
  ]
  edge [
    source 47
    target 2152
  ]
  edge [
    source 47
    target 2153
  ]
  edge [
    source 47
    target 2154
  ]
  edge [
    source 47
    target 2155
  ]
  edge [
    source 47
    target 2156
  ]
  edge [
    source 47
    target 2157
  ]
  edge [
    source 47
    target 2158
  ]
  edge [
    source 47
    target 2159
  ]
  edge [
    source 47
    target 2057
  ]
  edge [
    source 47
    target 2160
  ]
  edge [
    source 47
    target 2161
  ]
  edge [
    source 47
    target 2162
  ]
  edge [
    source 47
    target 2163
  ]
  edge [
    source 47
    target 2164
  ]
  edge [
    source 47
    target 2165
  ]
  edge [
    source 47
    target 2166
  ]
  edge [
    source 47
    target 2167
  ]
  edge [
    source 47
    target 2168
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 2169
  ]
  edge [
    source 48
    target 2170
  ]
  edge [
    source 48
    target 87
  ]
  edge [
    source 48
    target 2171
  ]
  edge [
    source 48
    target 2172
  ]
  edge [
    source 48
    target 2173
  ]
  edge [
    source 48
    target 2174
  ]
  edge [
    source 48
    target 2175
  ]
  edge [
    source 48
    target 1137
  ]
  edge [
    source 48
    target 2176
  ]
  edge [
    source 48
    target 140
  ]
  edge [
    source 48
    target 141
  ]
  edge [
    source 48
    target 142
  ]
  edge [
    source 48
    target 143
  ]
  edge [
    source 48
    target 144
  ]
  edge [
    source 48
    target 145
  ]
  edge [
    source 48
    target 146
  ]
  edge [
    source 48
    target 147
  ]
  edge [
    source 48
    target 148
  ]
  edge [
    source 48
    target 149
  ]
  edge [
    source 48
    target 150
  ]
  edge [
    source 48
    target 151
  ]
  edge [
    source 48
    target 152
  ]
  edge [
    source 48
    target 153
  ]
  edge [
    source 48
    target 154
  ]
  edge [
    source 48
    target 155
  ]
  edge [
    source 48
    target 156
  ]
  edge [
    source 48
    target 157
  ]
  edge [
    source 48
    target 158
  ]
  edge [
    source 48
    target 159
  ]
  edge [
    source 48
    target 160
  ]
  edge [
    source 48
    target 161
  ]
  edge [
    source 48
    target 162
  ]
  edge [
    source 48
    target 163
  ]
  edge [
    source 48
    target 164
  ]
  edge [
    source 48
    target 2177
  ]
  edge [
    source 48
    target 2178
  ]
  edge [
    source 48
    target 2179
  ]
  edge [
    source 48
    target 2180
  ]
  edge [
    source 48
    target 2181
  ]
  edge [
    source 48
    target 2182
  ]
  edge [
    source 48
    target 2183
  ]
  edge [
    source 48
    target 2184
  ]
  edge [
    source 48
    target 792
  ]
  edge [
    source 48
    target 2185
  ]
  edge [
    source 48
    target 2186
  ]
  edge [
    source 48
    target 2187
  ]
  edge [
    source 48
    target 2188
  ]
  edge [
    source 48
    target 2189
  ]
  edge [
    source 48
    target 2190
  ]
  edge [
    source 48
    target 2191
  ]
  edge [
    source 48
    target 2192
  ]
  edge [
    source 48
    target 2193
  ]
  edge [
    source 48
    target 2194
  ]
  edge [
    source 48
    target 2195
  ]
  edge [
    source 48
    target 2196
  ]
  edge [
    source 48
    target 204
  ]
  edge [
    source 48
    target 2197
  ]
  edge [
    source 48
    target 2198
  ]
  edge [
    source 48
    target 2199
  ]
  edge [
    source 48
    target 1236
  ]
  edge [
    source 48
    target 2025
  ]
  edge [
    source 48
    target 2200
  ]
  edge [
    source 48
    target 2201
  ]
  edge [
    source 48
    target 2202
  ]
  edge [
    source 48
    target 2203
  ]
  edge [
    source 48
    target 2204
  ]
  edge [
    source 48
    target 2205
  ]
  edge [
    source 48
    target 2206
  ]
  edge [
    source 48
    target 2207
  ]
  edge [
    source 48
    target 2208
  ]
  edge [
    source 48
    target 2209
  ]
  edge [
    source 48
    target 2210
  ]
  edge [
    source 48
    target 2211
  ]
  edge [
    source 48
    target 2212
  ]
  edge [
    source 48
    target 2213
  ]
  edge [
    source 48
    target 2214
  ]
  edge [
    source 48
    target 2215
  ]
  edge [
    source 48
    target 2216
  ]
  edge [
    source 48
    target 2217
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 2218
  ]
  edge [
    source 50
    target 2219
  ]
  edge [
    source 50
    target 2220
  ]
  edge [
    source 50
    target 2221
  ]
  edge [
    source 50
    target 2222
  ]
  edge [
    source 50
    target 2223
  ]
  edge [
    source 50
    target 2224
  ]
  edge [
    source 50
    target 2225
  ]
  edge [
    source 50
    target 2226
  ]
  edge [
    source 50
    target 2227
  ]
  edge [
    source 50
    target 2228
  ]
  edge [
    source 50
    target 2229
  ]
  edge [
    source 50
    target 1455
  ]
  edge [
    source 50
    target 2230
  ]
  edge [
    source 50
    target 2231
  ]
  edge [
    source 50
    target 2232
  ]
  edge [
    source 50
    target 2233
  ]
  edge [
    source 50
    target 2234
  ]
  edge [
    source 50
    target 2235
  ]
  edge [
    source 50
    target 621
  ]
  edge [
    source 50
    target 789
  ]
  edge [
    source 50
    target 2236
  ]
  edge [
    source 50
    target 584
  ]
  edge [
    source 50
    target 2237
  ]
  edge [
    source 50
    target 2238
  ]
  edge [
    source 50
    target 2239
  ]
  edge [
    source 50
    target 2240
  ]
  edge [
    source 50
    target 1018
  ]
  edge [
    source 50
    target 2241
  ]
  edge [
    source 50
    target 2242
  ]
  edge [
    source 50
    target 2243
  ]
  edge [
    source 50
    target 2244
  ]
  edge [
    source 50
    target 919
  ]
  edge [
    source 50
    target 2245
  ]
  edge [
    source 50
    target 2246
  ]
  edge [
    source 50
    target 2247
  ]
  edge [
    source 50
    target 2248
  ]
  edge [
    source 50
    target 2249
  ]
  edge [
    source 50
    target 821
  ]
  edge [
    source 50
    target 666
  ]
  edge [
    source 50
    target 2250
  ]
  edge [
    source 50
    target 174
  ]
  edge [
    source 50
    target 2251
  ]
  edge [
    source 50
    target 2252
  ]
  edge [
    source 50
    target 2253
  ]
  edge [
    source 50
    target 2254
  ]
  edge [
    source 50
    target 2255
  ]
  edge [
    source 50
    target 2256
  ]
  edge [
    source 50
    target 2257
  ]
  edge [
    source 50
    target 2258
  ]
  edge [
    source 50
    target 2259
  ]
  edge [
    source 50
    target 2260
  ]
  edge [
    source 50
    target 2261
  ]
  edge [
    source 50
    target 2262
  ]
  edge [
    source 50
    target 2263
  ]
  edge [
    source 50
    target 2264
  ]
  edge [
    source 50
    target 2265
  ]
  edge [
    source 50
    target 2266
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 1691
  ]
  edge [
    source 51
    target 2267
  ]
  edge [
    source 51
    target 1589
  ]
  edge [
    source 51
    target 2268
  ]
  edge [
    source 51
    target 1340
  ]
  edge [
    source 51
    target 2269
  ]
  edge [
    source 51
    target 2270
  ]
  edge [
    source 51
    target 2271
  ]
  edge [
    source 51
    target 2272
  ]
  edge [
    source 51
    target 2273
  ]
  edge [
    source 51
    target 2274
  ]
  edge [
    source 51
    target 584
  ]
  edge [
    source 51
    target 2275
  ]
  edge [
    source 51
    target 1285
  ]
  edge [
    source 51
    target 2276
  ]
  edge [
    source 51
    target 802
  ]
  edge [
    source 51
    target 2277
  ]
  edge [
    source 51
    target 2278
  ]
  edge [
    source 51
    target 2279
  ]
  edge [
    source 51
    target 2280
  ]
  edge [
    source 51
    target 2281
  ]
  edge [
    source 51
    target 1715
  ]
  edge [
    source 51
    target 966
  ]
  edge [
    source 51
    target 2282
  ]
  edge [
    source 51
    target 2283
  ]
  edge [
    source 51
    target 2284
  ]
  edge [
    source 51
    target 2285
  ]
  edge [
    source 51
    target 1509
  ]
  edge [
    source 51
    target 2286
  ]
  edge [
    source 51
    target 2287
  ]
  edge [
    source 51
    target 788
  ]
  edge [
    source 51
    target 2288
  ]
  edge [
    source 51
    target 2289
  ]
  edge [
    source 51
    target 2159
  ]
  edge [
    source 51
    target 2290
  ]
  edge [
    source 51
    target 2291
  ]
  edge [
    source 51
    target 2292
  ]
  edge [
    source 51
    target 2293
  ]
  edge [
    source 51
    target 2165
  ]
  edge [
    source 51
    target 2294
  ]
  edge [
    source 51
    target 2295
  ]
  edge [
    source 51
    target 919
  ]
  edge [
    source 51
    target 2296
  ]
  edge [
    source 51
    target 621
  ]
  edge [
    source 51
    target 2297
  ]
  edge [
    source 51
    target 2298
  ]
  edge [
    source 51
    target 2299
  ]
  edge [
    source 51
    target 1816
  ]
  edge [
    source 51
    target 2300
  ]
  edge [
    source 51
    target 624
  ]
  edge [
    source 51
    target 2301
  ]
  edge [
    source 51
    target 666
  ]
  edge [
    source 51
    target 2302
  ]
  edge [
    source 51
    target 2303
  ]
  edge [
    source 51
    target 2304
  ]
  edge [
    source 51
    target 2305
  ]
  edge [
    source 51
    target 1272
  ]
  edge [
    source 51
    target 800
  ]
  edge [
    source 51
    target 2306
  ]
  edge [
    source 51
    target 2307
  ]
  edge [
    source 51
    target 1282
  ]
  edge [
    source 51
    target 792
  ]
  edge [
    source 51
    target 2308
  ]
  edge [
    source 51
    target 2309
  ]
  edge [
    source 51
    target 2310
  ]
  edge [
    source 51
    target 1141
  ]
  edge [
    source 51
    target 2311
  ]
  edge [
    source 51
    target 2312
  ]
  edge [
    source 51
    target 2313
  ]
  edge [
    source 51
    target 2314
  ]
  edge [
    source 51
    target 911
  ]
  edge [
    source 51
    target 2315
  ]
  edge [
    source 51
    target 2316
  ]
  edge [
    source 51
    target 2317
  ]
  edge [
    source 51
    target 2318
  ]
  edge [
    source 51
    target 633
  ]
  edge [
    source 51
    target 2319
  ]
  edge [
    source 51
    target 2320
  ]
  edge [
    source 51
    target 2321
  ]
  edge [
    source 51
    target 1278
  ]
  edge [
    source 51
    target 2322
  ]
  edge [
    source 51
    target 2323
  ]
  edge [
    source 51
    target 2324
  ]
  edge [
    source 51
    target 2325
  ]
  edge [
    source 51
    target 2326
  ]
  edge [
    source 51
    target 2327
  ]
  edge [
    source 51
    target 2328
  ]
  edge [
    source 51
    target 2329
  ]
  edge [
    source 51
    target 2330
  ]
  edge [
    source 51
    target 1328
  ]
  edge [
    source 51
    target 2331
  ]
  edge [
    source 51
    target 2332
  ]
  edge [
    source 51
    target 2333
  ]
  edge [
    source 51
    target 2334
  ]
  edge [
    source 51
    target 1236
  ]
  edge [
    source 51
    target 2335
  ]
  edge [
    source 51
    target 2336
  ]
  edge [
    source 51
    target 2337
  ]
  edge [
    source 51
    target 603
  ]
  edge [
    source 51
    target 2338
  ]
  edge [
    source 51
    target 2339
  ]
  edge [
    source 51
    target 2340
  ]
  edge [
    source 51
    target 706
  ]
  edge [
    source 51
    target 2341
  ]
  edge [
    source 51
    target 2342
  ]
  edge [
    source 51
    target 2343
  ]
  edge [
    source 51
    target 2344
  ]
  edge [
    source 51
    target 2345
  ]
  edge [
    source 51
    target 2346
  ]
  edge [
    source 51
    target 2347
  ]
  edge [
    source 51
    target 2348
  ]
  edge [
    source 51
    target 2349
  ]
  edge [
    source 51
    target 2350
  ]
  edge [
    source 51
    target 2351
  ]
  edge [
    source 51
    target 2352
  ]
  edge [
    source 51
    target 2353
  ]
  edge [
    source 51
    target 2354
  ]
  edge [
    source 51
    target 2355
  ]
  edge [
    source 51
    target 2356
  ]
  edge [
    source 51
    target 689
  ]
  edge [
    source 51
    target 2357
  ]
  edge [
    source 51
    target 2358
  ]
  edge [
    source 51
    target 915
  ]
  edge [
    source 51
    target 2359
  ]
  edge [
    source 51
    target 2360
  ]
  edge [
    source 51
    target 1488
  ]
  edge [
    source 51
    target 804
  ]
  edge [
    source 51
    target 924
  ]
  edge [
    source 51
    target 2361
  ]
  edge [
    source 51
    target 1569
  ]
  edge [
    source 51
    target 1713
  ]
  edge [
    source 51
    target 2362
  ]
  edge [
    source 51
    target 478
  ]
  edge [
    source 51
    target 2363
  ]
  edge [
    source 51
    target 2364
  ]
  edge [
    source 51
    target 2365
  ]
  edge [
    source 51
    target 619
  ]
  edge [
    source 51
    target 2366
  ]
  edge [
    source 51
    target 2367
  ]
  edge [
    source 51
    target 2368
  ]
  edge [
    source 51
    target 2369
  ]
  edge [
    source 51
    target 2370
  ]
  edge [
    source 51
    target 2371
  ]
  edge [
    source 51
    target 2372
  ]
  edge [
    source 51
    target 2373
  ]
  edge [
    source 51
    target 2374
  ]
  edge [
    source 51
    target 617
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 51
    target 61
  ]
  edge [
    source 51
    target 63
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 1618
  ]
  edge [
    source 52
    target 2375
  ]
  edge [
    source 52
    target 1086
  ]
  edge [
    source 52
    target 2376
  ]
  edge [
    source 52
    target 2377
  ]
  edge [
    source 52
    target 1093
  ]
  edge [
    source 52
    target 1627
  ]
  edge [
    source 52
    target 2378
  ]
  edge [
    source 52
    target 2379
  ]
  edge [
    source 52
    target 2380
  ]
  edge [
    source 52
    target 1113
  ]
  edge [
    source 52
    target 2381
  ]
  edge [
    source 52
    target 2382
  ]
  edge [
    source 52
    target 1617
  ]
  edge [
    source 52
    target 2383
  ]
  edge [
    source 52
    target 2384
  ]
  edge [
    source 52
    target 2385
  ]
  edge [
    source 52
    target 2386
  ]
  edge [
    source 52
    target 1097
  ]
  edge [
    source 52
    target 2387
  ]
  edge [
    source 52
    target 2388
  ]
  edge [
    source 52
    target 2389
  ]
  edge [
    source 52
    target 1626
  ]
  edge [
    source 52
    target 2390
  ]
  edge [
    source 52
    target 2195
  ]
  edge [
    source 52
    target 2391
  ]
  edge [
    source 52
    target 2392
  ]
  edge [
    source 52
    target 2393
  ]
  edge [
    source 52
    target 1100
  ]
  edge [
    source 52
    target 2394
  ]
  edge [
    source 52
    target 2395
  ]
  edge [
    source 52
    target 2396
  ]
  edge [
    source 52
    target 2397
  ]
  edge [
    source 52
    target 1825
  ]
  edge [
    source 52
    target 1611
  ]
  edge [
    source 52
    target 1129
  ]
  edge [
    source 52
    target 2398
  ]
  edge [
    source 52
    target 2399
  ]
  edge [
    source 52
    target 2400
  ]
  edge [
    source 52
    target 2401
  ]
  edge [
    source 52
    target 2402
  ]
  edge [
    source 52
    target 2403
  ]
  edge [
    source 52
    target 2404
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 671
  ]
  edge [
    source 53
    target 2405
  ]
  edge [
    source 53
    target 706
  ]
  edge [
    source 53
    target 821
  ]
  edge [
    source 53
    target 2406
  ]
  edge [
    source 53
    target 2407
  ]
  edge [
    source 53
    target 1014
  ]
  edge [
    source 53
    target 2408
  ]
  edge [
    source 53
    target 2409
  ]
  edge [
    source 53
    target 891
  ]
  edge [
    source 53
    target 711
  ]
  edge [
    source 53
    target 2410
  ]
  edge [
    source 53
    target 2411
  ]
  edge [
    source 53
    target 2412
  ]
  edge [
    source 53
    target 2413
  ]
  edge [
    source 53
    target 2414
  ]
  edge [
    source 53
    target 2415
  ]
  edge [
    source 53
    target 665
  ]
  edge [
    source 53
    target 2416
  ]
  edge [
    source 53
    target 2417
  ]
  edge [
    source 53
    target 2418
  ]
  edge [
    source 53
    target 804
  ]
  edge [
    source 53
    target 2419
  ]
  edge [
    source 53
    target 2420
  ]
  edge [
    source 53
    target 2421
  ]
  edge [
    source 53
    target 2422
  ]
  edge [
    source 53
    target 2423
  ]
  edge [
    source 53
    target 2424
  ]
  edge [
    source 53
    target 2425
  ]
  edge [
    source 53
    target 1768
  ]
  edge [
    source 53
    target 2426
  ]
  edge [
    source 53
    target 2427
  ]
  edge [
    source 53
    target 87
  ]
  edge [
    source 53
    target 2428
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 59
  ]
  edge [
    source 54
    target 60
  ]
  edge [
    source 54
    target 65
  ]
  edge [
    source 55
    target 2429
  ]
  edge [
    source 55
    target 2430
  ]
  edge [
    source 55
    target 2431
  ]
  edge [
    source 55
    target 2432
  ]
  edge [
    source 55
    target 2433
  ]
  edge [
    source 55
    target 2434
  ]
  edge [
    source 55
    target 2435
  ]
  edge [
    source 55
    target 2436
  ]
  edge [
    source 55
    target 2437
  ]
  edge [
    source 55
    target 2438
  ]
  edge [
    source 55
    target 2439
  ]
  edge [
    source 55
    target 2440
  ]
  edge [
    source 55
    target 1840
  ]
  edge [
    source 55
    target 2441
  ]
  edge [
    source 55
    target 2442
  ]
  edge [
    source 55
    target 2443
  ]
  edge [
    source 55
    target 2444
  ]
  edge [
    source 55
    target 2445
  ]
  edge [
    source 55
    target 746
  ]
  edge [
    source 55
    target 2446
  ]
  edge [
    source 55
    target 1838
  ]
  edge [
    source 55
    target 2447
  ]
  edge [
    source 55
    target 2448
  ]
  edge [
    source 55
    target 2449
  ]
  edge [
    source 55
    target 81
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 2450
  ]
  edge [
    source 56
    target 2451
  ]
  edge [
    source 56
    target 2452
  ]
  edge [
    source 56
    target 2453
  ]
  edge [
    source 56
    target 2454
  ]
  edge [
    source 56
    target 2455
  ]
  edge [
    source 56
    target 889
  ]
  edge [
    source 56
    target 2456
  ]
  edge [
    source 56
    target 2457
  ]
  edge [
    source 56
    target 2458
  ]
  edge [
    source 56
    target 2459
  ]
  edge [
    source 56
    target 2460
  ]
  edge [
    source 56
    target 2461
  ]
  edge [
    source 56
    target 635
  ]
  edge [
    source 56
    target 1388
  ]
  edge [
    source 56
    target 2462
  ]
  edge [
    source 56
    target 666
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 66
  ]
  edge [
    source 57
    target 67
  ]
  edge [
    source 57
    target 72
  ]
  edge [
    source 57
    target 2463
  ]
  edge [
    source 57
    target 2464
  ]
  edge [
    source 57
    target 2465
  ]
  edge [
    source 57
    target 2466
  ]
  edge [
    source 57
    target 2467
  ]
  edge [
    source 57
    target 2468
  ]
  edge [
    source 57
    target 608
  ]
  edge [
    source 57
    target 2469
  ]
  edge [
    source 57
    target 725
  ]
  edge [
    source 57
    target 2470
  ]
  edge [
    source 57
    target 2471
  ]
  edge [
    source 57
    target 2472
  ]
  edge [
    source 57
    target 2473
  ]
  edge [
    source 57
    target 2474
  ]
  edge [
    source 57
    target 2475
  ]
  edge [
    source 57
    target 2476
  ]
  edge [
    source 57
    target 195
  ]
  edge [
    source 57
    target 121
  ]
  edge [
    source 57
    target 811
  ]
  edge [
    source 57
    target 947
  ]
  edge [
    source 57
    target 919
  ]
  edge [
    source 57
    target 2477
  ]
  edge [
    source 57
    target 666
  ]
  edge [
    source 57
    target 2478
  ]
  edge [
    source 57
    target 2479
  ]
  edge [
    source 57
    target 794
  ]
  edge [
    source 57
    target 2480
  ]
  edge [
    source 57
    target 2481
  ]
  edge [
    source 57
    target 2482
  ]
  edge [
    source 57
    target 2483
  ]
  edge [
    source 57
    target 2484
  ]
  edge [
    source 57
    target 2151
  ]
  edge [
    source 57
    target 2485
  ]
  edge [
    source 57
    target 2486
  ]
  edge [
    source 57
    target 2487
  ]
  edge [
    source 57
    target 2488
  ]
  edge [
    source 57
    target 2489
  ]
  edge [
    source 57
    target 1017
  ]
  edge [
    source 57
    target 635
  ]
  edge [
    source 57
    target 2490
  ]
  edge [
    source 57
    target 789
  ]
  edge [
    source 57
    target 1293
  ]
  edge [
    source 57
    target 731
  ]
  edge [
    source 57
    target 1552
  ]
  edge [
    source 57
    target 1553
  ]
  edge [
    source 57
    target 1554
  ]
  edge [
    source 57
    target 1555
  ]
  edge [
    source 57
    target 1556
  ]
  edge [
    source 57
    target 634
  ]
  edge [
    source 57
    target 280
  ]
  edge [
    source 57
    target 1557
  ]
  edge [
    source 57
    target 1558
  ]
  edge [
    source 57
    target 1559
  ]
  edge [
    source 57
    target 1560
  ]
  edge [
    source 57
    target 1561
  ]
  edge [
    source 57
    target 558
  ]
  edge [
    source 57
    target 1562
  ]
  edge [
    source 57
    target 1563
  ]
  edge [
    source 57
    target 1564
  ]
  edge [
    source 57
    target 812
  ]
  edge [
    source 57
    target 1565
  ]
  edge [
    source 57
    target 1566
  ]
  edge [
    source 57
    target 1567
  ]
  edge [
    source 57
    target 559
  ]
  edge [
    source 57
    target 2491
  ]
  edge [
    source 57
    target 2492
  ]
  edge [
    source 57
    target 74
  ]
  edge [
    source 57
    target 321
  ]
  edge [
    source 57
    target 2493
  ]
  edge [
    source 57
    target 2494
  ]
  edge [
    source 57
    target 2495
  ]
  edge [
    source 57
    target 2496
  ]
  edge [
    source 57
    target 2135
  ]
  edge [
    source 57
    target 2408
  ]
  edge [
    source 57
    target 1166
  ]
  edge [
    source 57
    target 2497
  ]
  edge [
    source 57
    target 1489
  ]
  edge [
    source 57
    target 2498
  ]
  edge [
    source 57
    target 2499
  ]
  edge [
    source 57
    target 2500
  ]
  edge [
    source 57
    target 2501
  ]
  edge [
    source 57
    target 61
  ]
  edge [
    source 57
    target 2502
  ]
  edge [
    source 57
    target 2503
  ]
  edge [
    source 57
    target 2504
  ]
  edge [
    source 57
    target 956
  ]
  edge [
    source 57
    target 2505
  ]
  edge [
    source 57
    target 2506
  ]
  edge [
    source 57
    target 2507
  ]
  edge [
    source 57
    target 2508
  ]
  edge [
    source 57
    target 2509
  ]
  edge [
    source 57
    target 2510
  ]
  edge [
    source 57
    target 2511
  ]
  edge [
    source 57
    target 1137
  ]
  edge [
    source 57
    target 2512
  ]
  edge [
    source 57
    target 2513
  ]
  edge [
    source 57
    target 955
  ]
  edge [
    source 57
    target 191
  ]
  edge [
    source 57
    target 2514
  ]
  edge [
    source 57
    target 2515
  ]
  edge [
    source 57
    target 2516
  ]
  edge [
    source 57
    target 1026
  ]
  edge [
    source 57
    target 2267
  ]
  edge [
    source 57
    target 2517
  ]
  edge [
    source 57
    target 2518
  ]
  edge [
    source 57
    target 2519
  ]
  edge [
    source 57
    target 1024
  ]
  edge [
    source 57
    target 2520
  ]
  edge [
    source 57
    target 1704
  ]
  edge [
    source 57
    target 2521
  ]
  edge [
    source 57
    target 2522
  ]
  edge [
    source 57
    target 2523
  ]
  edge [
    source 57
    target 648
  ]
  edge [
    source 57
    target 2524
  ]
  edge [
    source 57
    target 2525
  ]
  edge [
    source 57
    target 2526
  ]
  edge [
    source 57
    target 2527
  ]
  edge [
    source 57
    target 2528
  ]
  edge [
    source 57
    target 131
  ]
  edge [
    source 57
    target 126
  ]
  edge [
    source 57
    target 2529
  ]
  edge [
    source 57
    target 2530
  ]
  edge [
    source 57
    target 91
  ]
  edge [
    source 57
    target 210
  ]
  edge [
    source 57
    target 1234
  ]
  edge [
    source 57
    target 87
  ]
  edge [
    source 57
    target 114
  ]
  edge [
    source 57
    target 2531
  ]
  edge [
    source 57
    target 2532
  ]
  edge [
    source 57
    target 2533
  ]
  edge [
    source 57
    target 2534
  ]
  edge [
    source 57
    target 2535
  ]
  edge [
    source 57
    target 2536
  ]
  edge [
    source 57
    target 2537
  ]
  edge [
    source 58
    target 656
  ]
  edge [
    source 58
    target 2538
  ]
  edge [
    source 58
    target 1192
  ]
  edge [
    source 58
    target 1000
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 59
    target 64
  ]
  edge [
    source 59
    target 671
  ]
  edge [
    source 59
    target 177
  ]
  edge [
    source 59
    target 2539
  ]
  edge [
    source 59
    target 2540
  ]
  edge [
    source 59
    target 2541
  ]
  edge [
    source 59
    target 2417
  ]
  edge [
    source 59
    target 2418
  ]
  edge [
    source 59
    target 804
  ]
  edge [
    source 59
    target 2419
  ]
  edge [
    source 59
    target 2420
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 112
  ]
  edge [
    source 60
    target 500
  ]
  edge [
    source 60
    target 504
  ]
  edge [
    source 60
    target 505
  ]
  edge [
    source 60
    target 506
  ]
  edge [
    source 60
    target 507
  ]
  edge [
    source 60
    target 508
  ]
  edge [
    source 60
    target 509
  ]
  edge [
    source 60
    target 510
  ]
  edge [
    source 60
    target 1187
  ]
  edge [
    source 60
    target 1188
  ]
  edge [
    source 60
    target 1189
  ]
  edge [
    source 60
    target 1190
  ]
  edge [
    source 60
    target 1191
  ]
  edge [
    source 60
    target 1186
  ]
  edge [
    source 60
    target 1192
  ]
  edge [
    source 60
    target 171
  ]
  edge [
    source 60
    target 1193
  ]
  edge [
    source 60
    target 511
  ]
  edge [
    source 60
    target 512
  ]
  edge [
    source 60
    target 2542
  ]
  edge [
    source 60
    target 2543
  ]
  edge [
    source 60
    target 1875
  ]
  edge [
    source 60
    target 2544
  ]
  edge [
    source 60
    target 2545
  ]
  edge [
    source 60
    target 2546
  ]
  edge [
    source 60
    target 537
  ]
  edge [
    source 60
    target 2547
  ]
  edge [
    source 60
    target 2548
  ]
  edge [
    source 60
    target 167
  ]
  edge [
    source 60
    target 2549
  ]
  edge [
    source 60
    target 2550
  ]
  edge [
    source 60
    target 2551
  ]
  edge [
    source 60
    target 1747
  ]
  edge [
    source 60
    target 2552
  ]
  edge [
    source 60
    target 83
  ]
  edge [
    source 60
    target 2553
  ]
  edge [
    source 60
    target 165
  ]
  edge [
    source 60
    target 87
  ]
  edge [
    source 60
    target 166
  ]
  edge [
    source 60
    target 168
  ]
  edge [
    source 60
    target 160
  ]
  edge [
    source 60
    target 169
  ]
  edge [
    source 60
    target 142
  ]
  edge [
    source 60
    target 170
  ]
  edge [
    source 60
    target 172
  ]
  edge [
    source 60
    target 63
  ]
  edge [
    source 60
    target 70
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 1486
  ]
  edge [
    source 61
    target 2554
  ]
  edge [
    source 61
    target 2469
  ]
  edge [
    source 61
    target 141
  ]
  edge [
    source 61
    target 2473
  ]
  edge [
    source 61
    target 966
  ]
  edge [
    source 61
    target 143
  ]
  edge [
    source 61
    target 736
  ]
  edge [
    source 61
    target 2555
  ]
  edge [
    source 61
    target 1017
  ]
  edge [
    source 61
    target 2556
  ]
  edge [
    source 61
    target 2557
  ]
  edge [
    source 61
    target 1490
  ]
  edge [
    source 61
    target 2558
  ]
  edge [
    source 61
    target 2559
  ]
  edge [
    source 61
    target 2560
  ]
  edge [
    source 61
    target 2561
  ]
  edge [
    source 61
    target 1268
  ]
  edge [
    source 61
    target 2499
  ]
  edge [
    source 61
    target 2490
  ]
  edge [
    source 61
    target 2562
  ]
  edge [
    source 61
    target 2563
  ]
  edge [
    source 61
    target 1487
  ]
  edge [
    source 61
    target 1497
  ]
  edge [
    source 61
    target 2564
  ]
  edge [
    source 61
    target 1491
  ]
  edge [
    source 61
    target 2565
  ]
  edge [
    source 61
    target 2566
  ]
  edge [
    source 61
    target 2567
  ]
  edge [
    source 61
    target 2568
  ]
  edge [
    source 61
    target 2569
  ]
  edge [
    source 61
    target 2570
  ]
  edge [
    source 61
    target 1240
  ]
  edge [
    source 61
    target 2571
  ]
  edge [
    source 61
    target 2572
  ]
  edge [
    source 61
    target 800
  ]
  edge [
    source 61
    target 2573
  ]
  edge [
    source 61
    target 804
  ]
  edge [
    source 61
    target 563
  ]
  edge [
    source 61
    target 2574
  ]
  edge [
    source 61
    target 2575
  ]
  edge [
    source 61
    target 921
  ]
  edge [
    source 61
    target 2576
  ]
  edge [
    source 61
    target 2577
  ]
  edge [
    source 61
    target 2578
  ]
  edge [
    source 61
    target 2579
  ]
  edge [
    source 61
    target 2580
  ]
  edge [
    source 61
    target 1600
  ]
  edge [
    source 61
    target 2581
  ]
  edge [
    source 61
    target 2582
  ]
  edge [
    source 61
    target 789
  ]
  edge [
    source 61
    target 2583
  ]
  edge [
    source 61
    target 2289
  ]
  edge [
    source 61
    target 2584
  ]
  edge [
    source 61
    target 2585
  ]
  edge [
    source 61
    target 573
  ]
  edge [
    source 61
    target 614
  ]
  edge [
    source 61
    target 633
  ]
  edge [
    source 61
    target 2586
  ]
  edge [
    source 61
    target 710
  ]
  edge [
    source 61
    target 2587
  ]
  edge [
    source 61
    target 195
  ]
  edge [
    source 61
    target 2588
  ]
  edge [
    source 61
    target 2589
  ]
  edge [
    source 61
    target 2590
  ]
  edge [
    source 61
    target 1488
  ]
  edge [
    source 61
    target 2591
  ]
  edge [
    source 61
    target 2519
  ]
  edge [
    source 61
    target 2520
  ]
  edge [
    source 61
    target 2592
  ]
  edge [
    source 61
    target 692
  ]
  edge [
    source 61
    target 2593
  ]
  edge [
    source 61
    target 722
  ]
  edge [
    source 61
    target 609
  ]
  edge [
    source 61
    target 2594
  ]
  edge [
    source 61
    target 1035
  ]
  edge [
    source 61
    target 2595
  ]
  edge [
    source 61
    target 2596
  ]
  edge [
    source 61
    target 2597
  ]
  edge [
    source 61
    target 811
  ]
  edge [
    source 61
    target 1352
  ]
  edge [
    source 61
    target 701
  ]
  edge [
    source 61
    target 2598
  ]
  edge [
    source 61
    target 2599
  ]
  edge [
    source 61
    target 2600
  ]
  edge [
    source 61
    target 2601
  ]
  edge [
    source 61
    target 2602
  ]
  edge [
    source 61
    target 2603
  ]
  edge [
    source 61
    target 2604
  ]
  edge [
    source 61
    target 2605
  ]
  edge [
    source 61
    target 2010
  ]
  edge [
    source 61
    target 2606
  ]
  edge [
    source 61
    target 2607
  ]
  edge [
    source 61
    target 635
  ]
  edge [
    source 61
    target 2608
  ]
  edge [
    source 61
    target 2609
  ]
  edge [
    source 61
    target 2610
  ]
  edge [
    source 61
    target 2611
  ]
  edge [
    source 61
    target 2612
  ]
  edge [
    source 61
    target 2613
  ]
  edge [
    source 61
    target 2614
  ]
  edge [
    source 61
    target 1704
  ]
  edge [
    source 61
    target 2615
  ]
  edge [
    source 61
    target 2616
  ]
  edge [
    source 61
    target 2617
  ]
  edge [
    source 61
    target 2618
  ]
  edge [
    source 61
    target 2619
  ]
  edge [
    source 61
    target 2620
  ]
  edge [
    source 61
    target 2621
  ]
  edge [
    source 61
    target 2622
  ]
  edge [
    source 61
    target 2623
  ]
  edge [
    source 61
    target 2624
  ]
  edge [
    source 61
    target 2625
  ]
  edge [
    source 61
    target 2626
  ]
  edge [
    source 61
    target 2627
  ]
  edge [
    source 61
    target 2628
  ]
  edge [
    source 61
    target 2629
  ]
  edge [
    source 61
    target 2630
  ]
  edge [
    source 61
    target 2631
  ]
  edge [
    source 61
    target 2632
  ]
  edge [
    source 61
    target 2633
  ]
  edge [
    source 61
    target 2634
  ]
  edge [
    source 61
    target 2635
  ]
  edge [
    source 61
    target 2636
  ]
  edge [
    source 61
    target 2637
  ]
  edge [
    source 61
    target 2638
  ]
  edge [
    source 61
    target 2639
  ]
  edge [
    source 61
    target 2640
  ]
  edge [
    source 61
    target 2641
  ]
  edge [
    source 61
    target 2642
  ]
  edge [
    source 61
    target 2643
  ]
  edge [
    source 61
    target 2644
  ]
  edge [
    source 61
    target 2645
  ]
  edge [
    source 61
    target 2646
  ]
  edge [
    source 61
    target 2647
  ]
  edge [
    source 61
    target 2648
  ]
  edge [
    source 61
    target 2649
  ]
  edge [
    source 61
    target 1644
  ]
  edge [
    source 61
    target 87
  ]
  edge [
    source 61
    target 2650
  ]
  edge [
    source 61
    target 2651
  ]
  edge [
    source 61
    target 2652
  ]
  edge [
    source 61
    target 2653
  ]
  edge [
    source 61
    target 2654
  ]
  edge [
    source 61
    target 1186
  ]
  edge [
    source 61
    target 2655
  ]
  edge [
    source 61
    target 2504
  ]
  edge [
    source 61
    target 955
  ]
  edge [
    source 61
    target 2505
  ]
  edge [
    source 61
    target 648
  ]
  edge [
    source 61
    target 2515
  ]
  edge [
    source 61
    target 280
  ]
  edge [
    source 61
    target 2512
  ]
  edge [
    source 61
    target 2521
  ]
  edge [
    source 61
    target 1767
  ]
  edge [
    source 61
    target 2524
  ]
  edge [
    source 61
    target 2656
  ]
  edge [
    source 61
    target 2657
  ]
  edge [
    source 61
    target 2658
  ]
  edge [
    source 61
    target 2659
  ]
  edge [
    source 61
    target 2660
  ]
  edge [
    source 61
    target 2661
  ]
  edge [
    source 61
    target 2662
  ]
  edge [
    source 61
    target 2663
  ]
  edge [
    source 61
    target 2664
  ]
  edge [
    source 61
    target 2665
  ]
  edge [
    source 61
    target 2666
  ]
  edge [
    source 61
    target 2408
  ]
  edge [
    source 61
    target 1166
  ]
  edge [
    source 61
    target 2497
  ]
  edge [
    source 61
    target 1489
  ]
  edge [
    source 61
    target 2498
  ]
  edge [
    source 61
    target 2500
  ]
  edge [
    source 61
    target 2501
  ]
  edge [
    source 61
    target 2502
  ]
  edge [
    source 61
    target 64
  ]
  edge [
    source 61
    target 72
  ]
  edge [
    source 61
    target 81
  ]
  edge [
    source 61
    target 3217
  ]
  edge [
    source 61
    target 3218
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 2667
  ]
  edge [
    source 62
    target 2668
  ]
  edge [
    source 62
    target 1062
  ]
  edge [
    source 62
    target 1059
  ]
  edge [
    source 62
    target 1060
  ]
  edge [
    source 62
    target 1061
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 2669
  ]
  edge [
    source 63
    target 2670
  ]
  edge [
    source 63
    target 991
  ]
  edge [
    source 63
    target 2671
  ]
  edge [
    source 63
    target 2672
  ]
  edge [
    source 63
    target 2673
  ]
  edge [
    source 63
    target 921
  ]
  edge [
    source 63
    target 2674
  ]
  edge [
    source 63
    target 2549
  ]
  edge [
    source 63
    target 2675
  ]
  edge [
    source 63
    target 993
  ]
  edge [
    source 63
    target 2676
  ]
  edge [
    source 63
    target 1186
  ]
  edge [
    source 63
    target 2677
  ]
  edge [
    source 63
    target 2678
  ]
  edge [
    source 63
    target 2679
  ]
  edge [
    source 63
    target 2680
  ]
  edge [
    source 63
    target 1196
  ]
  edge [
    source 63
    target 2681
  ]
  edge [
    source 63
    target 2682
  ]
  edge [
    source 63
    target 2683
  ]
  edge [
    source 63
    target 998
  ]
  edge [
    source 63
    target 2684
  ]
  edge [
    source 63
    target 987
  ]
  edge [
    source 63
    target 988
  ]
  edge [
    source 63
    target 989
  ]
  edge [
    source 63
    target 990
  ]
  edge [
    source 63
    target 992
  ]
  edge [
    source 63
    target 966
  ]
  edge [
    source 63
    target 994
  ]
  edge [
    source 63
    target 995
  ]
  edge [
    source 63
    target 996
  ]
  edge [
    source 63
    target 997
  ]
  edge [
    source 63
    target 999
  ]
  edge [
    source 64
    target 2685
  ]
  edge [
    source 64
    target 2686
  ]
  edge [
    source 64
    target 2687
  ]
  edge [
    source 64
    target 2688
  ]
  edge [
    source 64
    target 2689
  ]
  edge [
    source 64
    target 2690
  ]
  edge [
    source 64
    target 2691
  ]
  edge [
    source 64
    target 2152
  ]
  edge [
    source 64
    target 2692
  ]
  edge [
    source 64
    target 2693
  ]
  edge [
    source 64
    target 2694
  ]
  edge [
    source 64
    target 2221
  ]
  edge [
    source 64
    target 2695
  ]
  edge [
    source 64
    target 2696
  ]
  edge [
    source 64
    target 2697
  ]
  edge [
    source 64
    target 2698
  ]
  edge [
    source 64
    target 2699
  ]
  edge [
    source 64
    target 2700
  ]
  edge [
    source 64
    target 2048
  ]
  edge [
    source 64
    target 2303
  ]
  edge [
    source 64
    target 2701
  ]
  edge [
    source 64
    target 2702
  ]
  edge [
    source 64
    target 2703
  ]
  edge [
    source 64
    target 2704
  ]
  edge [
    source 64
    target 2705
  ]
  edge [
    source 64
    target 2706
  ]
  edge [
    source 64
    target 922
  ]
  edge [
    source 64
    target 2707
  ]
  edge [
    source 64
    target 2708
  ]
  edge [
    source 64
    target 2709
  ]
  edge [
    source 64
    target 932
  ]
  edge [
    source 64
    target 2237
  ]
  edge [
    source 64
    target 2238
  ]
  edge [
    source 64
    target 2239
  ]
  edge [
    source 64
    target 2240
  ]
  edge [
    source 64
    target 1018
  ]
  edge [
    source 64
    target 2241
  ]
  edge [
    source 64
    target 2242
  ]
  edge [
    source 64
    target 2243
  ]
  edge [
    source 64
    target 2244
  ]
  edge [
    source 64
    target 919
  ]
  edge [
    source 64
    target 2245
  ]
  edge [
    source 64
    target 2246
  ]
  edge [
    source 64
    target 2247
  ]
  edge [
    source 64
    target 2248
  ]
  edge [
    source 64
    target 2249
  ]
  edge [
    source 64
    target 821
  ]
  edge [
    source 64
    target 666
  ]
  edge [
    source 64
    target 2250
  ]
  edge [
    source 64
    target 2004
  ]
  edge [
    source 64
    target 2710
  ]
  edge [
    source 64
    target 2711
  ]
  edge [
    source 64
    target 2712
  ]
  edge [
    source 64
    target 2713
  ]
  edge [
    source 64
    target 2714
  ]
  edge [
    source 64
    target 1984
  ]
  edge [
    source 64
    target 1903
  ]
  edge [
    source 64
    target 2715
  ]
  edge [
    source 64
    target 2716
  ]
  edge [
    source 64
    target 2717
  ]
  edge [
    source 64
    target 2718
  ]
  edge [
    source 64
    target 2719
  ]
  edge [
    source 64
    target 2584
  ]
  edge [
    source 64
    target 2720
  ]
  edge [
    source 64
    target 1489
  ]
  edge [
    source 64
    target 2721
  ]
  edge [
    source 64
    target 2722
  ]
  edge [
    source 64
    target 2723
  ]
  edge [
    source 64
    target 1713
  ]
  edge [
    source 64
    target 2724
  ]
  edge [
    source 64
    target 2725
  ]
  edge [
    source 64
    target 2726
  ]
  edge [
    source 64
    target 2727
  ]
  edge [
    source 64
    target 2728
  ]
  edge [
    source 64
    target 2729
  ]
  edge [
    source 64
    target 2730
  ]
  edge [
    source 64
    target 2731
  ]
  edge [
    source 64
    target 2732
  ]
  edge [
    source 64
    target 559
  ]
  edge [
    source 64
    target 2733
  ]
  edge [
    source 64
    target 2734
  ]
  edge [
    source 64
    target 2735
  ]
  edge [
    source 64
    target 1753
  ]
  edge [
    source 64
    target 251
  ]
  edge [
    source 64
    target 2736
  ]
  edge [
    source 64
    target 2737
  ]
  edge [
    source 64
    target 2738
  ]
  edge [
    source 64
    target 2739
  ]
  edge [
    source 64
    target 2740
  ]
  edge [
    source 64
    target 2741
  ]
  edge [
    source 64
    target 2742
  ]
  edge [
    source 64
    target 2743
  ]
  edge [
    source 64
    target 2744
  ]
  edge [
    source 64
    target 2745
  ]
  edge [
    source 64
    target 2746
  ]
  edge [
    source 64
    target 1717
  ]
  edge [
    source 64
    target 2747
  ]
  edge [
    source 64
    target 2748
  ]
  edge [
    source 64
    target 2749
  ]
  edge [
    source 64
    target 1456
  ]
  edge [
    source 64
    target 2750
  ]
  edge [
    source 64
    target 2751
  ]
  edge [
    source 64
    target 1410
  ]
  edge [
    source 64
    target 2752
  ]
  edge [
    source 64
    target 983
  ]
  edge [
    source 64
    target 2753
  ]
  edge [
    source 64
    target 2754
  ]
  edge [
    source 64
    target 1413
  ]
  edge [
    source 64
    target 2755
  ]
  edge [
    source 64
    target 2756
  ]
  edge [
    source 64
    target 2757
  ]
  edge [
    source 64
    target 2758
  ]
  edge [
    source 64
    target 2759
  ]
  edge [
    source 64
    target 2760
  ]
  edge [
    source 64
    target 2761
  ]
  edge [
    source 64
    target 2762
  ]
  edge [
    source 64
    target 2763
  ]
  edge [
    source 64
    target 2764
  ]
  edge [
    source 64
    target 2151
  ]
  edge [
    source 64
    target 2765
  ]
  edge [
    source 64
    target 2766
  ]
  edge [
    source 64
    target 2119
  ]
  edge [
    source 64
    target 624
  ]
  edge [
    source 64
    target 2767
  ]
  edge [
    source 64
    target 2768
  ]
  edge [
    source 64
    target 2769
  ]
  edge [
    source 64
    target 2770
  ]
  edge [
    source 64
    target 2771
  ]
  edge [
    source 64
    target 2772
  ]
  edge [
    source 64
    target 673
  ]
  edge [
    source 64
    target 2773
  ]
  edge [
    source 64
    target 2774
  ]
  edge [
    source 64
    target 739
  ]
  edge [
    source 64
    target 2450
  ]
  edge [
    source 64
    target 2775
  ]
  edge [
    source 64
    target 2776
  ]
  edge [
    source 64
    target 2777
  ]
  edge [
    source 64
    target 2778
  ]
  edge [
    source 64
    target 2779
  ]
  edge [
    source 64
    target 2459
  ]
  edge [
    source 64
    target 2780
  ]
  edge [
    source 64
    target 2781
  ]
  edge [
    source 64
    target 2782
  ]
  edge [
    source 64
    target 2783
  ]
  edge [
    source 64
    target 2784
  ]
  edge [
    source 64
    target 2785
  ]
  edge [
    source 64
    target 2786
  ]
  edge [
    source 64
    target 2787
  ]
  edge [
    source 64
    target 2788
  ]
  edge [
    source 64
    target 2789
  ]
  edge [
    source 64
    target 2790
  ]
  edge [
    source 64
    target 2791
  ]
  edge [
    source 64
    target 2792
  ]
  edge [
    source 64
    target 2793
  ]
  edge [
    source 64
    target 2794
  ]
  edge [
    source 64
    target 2795
  ]
  edge [
    source 64
    target 2796
  ]
  edge [
    source 64
    target 2797
  ]
  edge [
    source 64
    target 2798
  ]
  edge [
    source 64
    target 2799
  ]
  edge [
    source 64
    target 2800
  ]
  edge [
    source 64
    target 2801
  ]
  edge [
    source 64
    target 2802
  ]
  edge [
    source 64
    target 2803
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 586
  ]
  edge [
    source 65
    target 587
  ]
  edge [
    source 65
    target 588
  ]
  edge [
    source 65
    target 589
  ]
  edge [
    source 65
    target 590
  ]
  edge [
    source 65
    target 591
  ]
  edge [
    source 65
    target 592
  ]
  edge [
    source 65
    target 280
  ]
  edge [
    source 65
    target 593
  ]
  edge [
    source 65
    target 594
  ]
  edge [
    source 65
    target 595
  ]
  edge [
    source 65
    target 596
  ]
  edge [
    source 65
    target 597
  ]
  edge [
    source 65
    target 598
  ]
  edge [
    source 65
    target 599
  ]
  edge [
    source 65
    target 600
  ]
  edge [
    source 65
    target 601
  ]
  edge [
    source 65
    target 602
  ]
  edge [
    source 65
    target 603
  ]
  edge [
    source 65
    target 604
  ]
  edge [
    source 65
    target 605
  ]
  edge [
    source 65
    target 2804
  ]
  edge [
    source 65
    target 2805
  ]
  edge [
    source 65
    target 2806
  ]
  edge [
    source 65
    target 1715
  ]
  edge [
    source 65
    target 2807
  ]
  edge [
    source 65
    target 2808
  ]
  edge [
    source 65
    target 2809
  ]
  edge [
    source 65
    target 2810
  ]
  edge [
    source 65
    target 811
  ]
  edge [
    source 65
    target 1017
  ]
  edge [
    source 65
    target 2811
  ]
  edge [
    source 65
    target 2812
  ]
  edge [
    source 65
    target 1774
  ]
  edge [
    source 65
    target 582
  ]
  edge [
    source 65
    target 2813
  ]
  edge [
    source 65
    target 2814
  ]
  edge [
    source 65
    target 2815
  ]
  edge [
    source 65
    target 1392
  ]
  edge [
    source 65
    target 2816
  ]
  edge [
    source 65
    target 2817
  ]
  edge [
    source 65
    target 2818
  ]
  edge [
    source 65
    target 2819
  ]
  edge [
    source 65
    target 2820
  ]
  edge [
    source 65
    target 2437
  ]
  edge [
    source 65
    target 2821
  ]
  edge [
    source 65
    target 2822
  ]
  edge [
    source 65
    target 2823
  ]
  edge [
    source 65
    target 2824
  ]
  edge [
    source 65
    target 2825
  ]
  edge [
    source 65
    target 2826
  ]
  edge [
    source 65
    target 2452
  ]
  edge [
    source 65
    target 2827
  ]
  edge [
    source 65
    target 919
  ]
  edge [
    source 65
    target 2828
  ]
  edge [
    source 65
    target 2829
  ]
  edge [
    source 65
    target 2830
  ]
  edge [
    source 65
    target 2831
  ]
  edge [
    source 65
    target 2832
  ]
  edge [
    source 65
    target 956
  ]
  edge [
    source 65
    target 2833
  ]
  edge [
    source 65
    target 2834
  ]
  edge [
    source 65
    target 2835
  ]
  edge [
    source 65
    target 2836
  ]
  edge [
    source 65
    target 2837
  ]
  edge [
    source 65
    target 2838
  ]
  edge [
    source 65
    target 2839
  ]
  edge [
    source 65
    target 2840
  ]
  edge [
    source 65
    target 739
  ]
  edge [
    source 65
    target 2841
  ]
  edge [
    source 65
    target 2842
  ]
  edge [
    source 65
    target 2843
  ]
  edge [
    source 65
    target 2844
  ]
  edge [
    source 65
    target 2845
  ]
  edge [
    source 65
    target 2846
  ]
  edge [
    source 65
    target 2847
  ]
  edge [
    source 65
    target 2848
  ]
  edge [
    source 65
    target 2849
  ]
  edge [
    source 65
    target 2850
  ]
  edge [
    source 65
    target 2851
  ]
  edge [
    source 65
    target 2852
  ]
  edge [
    source 65
    target 2853
  ]
  edge [
    source 65
    target 2854
  ]
  edge [
    source 65
    target 2855
  ]
  edge [
    source 65
    target 2856
  ]
  edge [
    source 65
    target 2857
  ]
  edge [
    source 65
    target 2858
  ]
  edge [
    source 65
    target 2859
  ]
  edge [
    source 65
    target 2860
  ]
  edge [
    source 65
    target 2861
  ]
  edge [
    source 65
    target 2862
  ]
  edge [
    source 65
    target 2863
  ]
  edge [
    source 65
    target 182
  ]
  edge [
    source 65
    target 2864
  ]
  edge [
    source 65
    target 2865
  ]
  edge [
    source 65
    target 2026
  ]
  edge [
    source 65
    target 2866
  ]
  edge [
    source 65
    target 161
  ]
  edge [
    source 65
    target 2867
  ]
  edge [
    source 65
    target 2868
  ]
  edge [
    source 65
    target 2869
  ]
  edge [
    source 65
    target 2870
  ]
  edge [
    source 65
    target 2871
  ]
  edge [
    source 65
    target 2054
  ]
  edge [
    source 65
    target 2872
  ]
  edge [
    source 65
    target 2873
  ]
  edge [
    source 65
    target 2874
  ]
  edge [
    source 65
    target 2875
  ]
  edge [
    source 65
    target 2516
  ]
  edge [
    source 65
    target 2876
  ]
  edge [
    source 65
    target 2877
  ]
  edge [
    source 65
    target 2878
  ]
  edge [
    source 65
    target 2879
  ]
  edge [
    source 65
    target 2880
  ]
  edge [
    source 65
    target 2881
  ]
  edge [
    source 65
    target 2882
  ]
  edge [
    source 65
    target 2883
  ]
  edge [
    source 65
    target 2884
  ]
  edge [
    source 65
    target 2885
  ]
  edge [
    source 65
    target 2886
  ]
  edge [
    source 65
    target 2887
  ]
  edge [
    source 65
    target 2488
  ]
  edge [
    source 65
    target 2888
  ]
  edge [
    source 65
    target 2889
  ]
  edge [
    source 65
    target 935
  ]
  edge [
    source 65
    target 2890
  ]
  edge [
    source 65
    target 2891
  ]
  edge [
    source 65
    target 2892
  ]
  edge [
    source 65
    target 2893
  ]
  edge [
    source 65
    target 2894
  ]
  edge [
    source 65
    target 2895
  ]
  edge [
    source 65
    target 2896
  ]
  edge [
    source 65
    target 2801
  ]
  edge [
    source 65
    target 1028
  ]
  edge [
    source 65
    target 2897
  ]
  edge [
    source 65
    target 135
  ]
  edge [
    source 65
    target 568
  ]
  edge [
    source 65
    target 1470
  ]
  edge [
    source 65
    target 1471
  ]
  edge [
    source 65
    target 1472
  ]
  edge [
    source 65
    target 573
  ]
  edge [
    source 65
    target 1473
  ]
  edge [
    source 65
    target 1474
  ]
  edge [
    source 65
    target 1475
  ]
  edge [
    source 65
    target 1476
  ]
  edge [
    source 65
    target 1477
  ]
  edge [
    source 65
    target 1478
  ]
  edge [
    source 65
    target 1479
  ]
  edge [
    source 65
    target 1480
  ]
  edge [
    source 65
    target 1481
  ]
  edge [
    source 65
    target 1482
  ]
  edge [
    source 65
    target 1483
  ]
  edge [
    source 65
    target 1484
  ]
  edge [
    source 65
    target 1485
  ]
  edge [
    source 65
    target 2898
  ]
  edge [
    source 65
    target 2899
  ]
  edge [
    source 65
    target 2900
  ]
  edge [
    source 65
    target 2901
  ]
  edge [
    source 65
    target 2902
  ]
  edge [
    source 65
    target 2903
  ]
  edge [
    source 65
    target 2904
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 2905
  ]
  edge [
    source 67
    target 2906
  ]
  edge [
    source 67
    target 2907
  ]
  edge [
    source 67
    target 2908
  ]
  edge [
    source 67
    target 2909
  ]
  edge [
    source 67
    target 2910
  ]
  edge [
    source 67
    target 2911
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 68
    target 2912
  ]
  edge [
    source 68
    target 2913
  ]
  edge [
    source 68
    target 2914
  ]
  edge [
    source 68
    target 792
  ]
  edge [
    source 68
    target 2915
  ]
  edge [
    source 68
    target 2916
  ]
  edge [
    source 68
    target 2917
  ]
  edge [
    source 68
    target 2918
  ]
  edge [
    source 68
    target 2919
  ]
  edge [
    source 68
    target 2920
  ]
  edge [
    source 68
    target 2921
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 2922
  ]
  edge [
    source 70
    target 2923
  ]
  edge [
    source 70
    target 87
  ]
  edge [
    source 70
    target 2924
  ]
  edge [
    source 70
    target 520
  ]
  edge [
    source 70
    target 2925
  ]
  edge [
    source 70
    target 2926
  ]
  edge [
    source 70
    target 140
  ]
  edge [
    source 70
    target 141
  ]
  edge [
    source 70
    target 142
  ]
  edge [
    source 70
    target 143
  ]
  edge [
    source 70
    target 144
  ]
  edge [
    source 70
    target 145
  ]
  edge [
    source 70
    target 146
  ]
  edge [
    source 70
    target 147
  ]
  edge [
    source 70
    target 148
  ]
  edge [
    source 70
    target 149
  ]
  edge [
    source 70
    target 150
  ]
  edge [
    source 70
    target 151
  ]
  edge [
    source 70
    target 152
  ]
  edge [
    source 70
    target 153
  ]
  edge [
    source 70
    target 154
  ]
  edge [
    source 70
    target 155
  ]
  edge [
    source 70
    target 156
  ]
  edge [
    source 70
    target 157
  ]
  edge [
    source 70
    target 158
  ]
  edge [
    source 70
    target 159
  ]
  edge [
    source 70
    target 160
  ]
  edge [
    source 70
    target 161
  ]
  edge [
    source 70
    target 162
  ]
  edge [
    source 70
    target 163
  ]
  edge [
    source 70
    target 164
  ]
  edge [
    source 70
    target 1873
  ]
  edge [
    source 70
    target 2927
  ]
  edge [
    source 70
    target 509
  ]
  edge [
    source 70
    target 2928
  ]
  edge [
    source 70
    target 2929
  ]
  edge [
    source 70
    target 2930
  ]
  edge [
    source 70
    target 2931
  ]
  edge [
    source 70
    target 2932
  ]
  edge [
    source 70
    target 2933
  ]
  edge [
    source 70
    target 2934
  ]
  edge [
    source 70
    target 2935
  ]
  edge [
    source 70
    target 2936
  ]
  edge [
    source 70
    target 170
  ]
  edge [
    source 70
    target 2937
  ]
  edge [
    source 70
    target 2938
  ]
  edge [
    source 70
    target 2939
  ]
  edge [
    source 70
    target 2940
  ]
  edge [
    source 70
    target 1392
  ]
  edge [
    source 70
    target 1386
  ]
  edge [
    source 70
    target 2941
  ]
  edge [
    source 70
    target 2942
  ]
  edge [
    source 70
    target 2943
  ]
  edge [
    source 70
    target 2944
  ]
  edge [
    source 70
    target 2945
  ]
  edge [
    source 70
    target 1214
  ]
  edge [
    source 70
    target 2946
  ]
  edge [
    source 70
    target 2947
  ]
  edge [
    source 70
    target 1367
  ]
  edge [
    source 70
    target 2948
  ]
  edge [
    source 70
    target 2949
  ]
  edge [
    source 70
    target 2950
  ]
  edge [
    source 70
    target 2951
  ]
  edge [
    source 70
    target 1260
  ]
  edge [
    source 70
    target 2952
  ]
  edge [
    source 70
    target 2953
  ]
  edge [
    source 71
    target 80
  ]
  edge [
    source 71
    target 2954
  ]
  edge [
    source 71
    target 2955
  ]
  edge [
    source 71
    target 2956
  ]
  edge [
    source 71
    target 2957
  ]
  edge [
    source 71
    target 2958
  ]
  edge [
    source 71
    target 2959
  ]
  edge [
    source 71
    target 2960
  ]
  edge [
    source 71
    target 2961
  ]
  edge [
    source 71
    target 1116
  ]
  edge [
    source 71
    target 2962
  ]
  edge [
    source 71
    target 2963
  ]
  edge [
    source 71
    target 1627
  ]
  edge [
    source 71
    target 2437
  ]
  edge [
    source 71
    target 2964
  ]
  edge [
    source 71
    target 2965
  ]
  edge [
    source 71
    target 745
  ]
  edge [
    source 71
    target 2966
  ]
  edge [
    source 71
    target 2967
  ]
  edge [
    source 71
    target 2968
  ]
  edge [
    source 71
    target 2969
  ]
  edge [
    source 71
    target 2970
  ]
  edge [
    source 71
    target 2445
  ]
  edge [
    source 71
    target 2971
  ]
  edge [
    source 71
    target 2972
  ]
  edge [
    source 71
    target 2973
  ]
  edge [
    source 71
    target 2974
  ]
  edge [
    source 71
    target 2975
  ]
  edge [
    source 71
    target 2976
  ]
  edge [
    source 71
    target 2977
  ]
  edge [
    source 71
    target 2978
  ]
  edge [
    source 71
    target 2979
  ]
  edge [
    source 71
    target 2980
  ]
  edge [
    source 71
    target 2981
  ]
  edge [
    source 71
    target 2982
  ]
  edge [
    source 71
    target 2983
  ]
  edge [
    source 71
    target 2984
  ]
  edge [
    source 71
    target 2985
  ]
  edge [
    source 71
    target 2986
  ]
  edge [
    source 71
    target 2987
  ]
  edge [
    source 71
    target 2221
  ]
  edge [
    source 71
    target 2988
  ]
  edge [
    source 71
    target 2989
  ]
  edge [
    source 71
    target 2990
  ]
  edge [
    source 71
    target 2991
  ]
  edge [
    source 71
    target 2992
  ]
  edge [
    source 71
    target 2993
  ]
  edge [
    source 71
    target 2994
  ]
  edge [
    source 71
    target 2995
  ]
  edge [
    source 72
    target 81
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 73
    target 2996
  ]
  edge [
    source 73
    target 2997
  ]
  edge [
    source 73
    target 2998
  ]
  edge [
    source 73
    target 2999
  ]
  edge [
    source 73
    target 3000
  ]
  edge [
    source 73
    target 2268
  ]
  edge [
    source 73
    target 3001
  ]
  edge [
    source 73
    target 3002
  ]
  edge [
    source 73
    target 3003
  ]
  edge [
    source 73
    target 3004
  ]
  edge [
    source 73
    target 3005
  ]
  edge [
    source 73
    target 3006
  ]
  edge [
    source 73
    target 3007
  ]
  edge [
    source 73
    target 2437
  ]
  edge [
    source 73
    target 2126
  ]
  edge [
    source 73
    target 3008
  ]
  edge [
    source 73
    target 3009
  ]
  edge [
    source 73
    target 3010
  ]
  edge [
    source 73
    target 3011
  ]
  edge [
    source 73
    target 3012
  ]
  edge [
    source 73
    target 3013
  ]
  edge [
    source 73
    target 3014
  ]
  edge [
    source 73
    target 3015
  ]
  edge [
    source 73
    target 1084
  ]
  edge [
    source 73
    target 3016
  ]
  edge [
    source 73
    target 3017
  ]
  edge [
    source 73
    target 1817
  ]
  edge [
    source 73
    target 3018
  ]
  edge [
    source 73
    target 939
  ]
  edge [
    source 73
    target 3019
  ]
  edge [
    source 73
    target 3020
  ]
  edge [
    source 73
    target 3021
  ]
  edge [
    source 73
    target 2429
  ]
  edge [
    source 73
    target 2966
  ]
  edge [
    source 73
    target 3022
  ]
  edge [
    source 73
    target 2980
  ]
  edge [
    source 73
    target 3023
  ]
  edge [
    source 73
    target 3024
  ]
  edge [
    source 73
    target 3025
  ]
  edge [
    source 73
    target 3026
  ]
  edge [
    source 73
    target 3027
  ]
  edge [
    source 73
    target 3028
  ]
  edge [
    source 73
    target 2037
  ]
  edge [
    source 73
    target 1834
  ]
  edge [
    source 73
    target 1086
  ]
  edge [
    source 73
    target 3029
  ]
  edge [
    source 73
    target 2807
  ]
  edge [
    source 73
    target 3030
  ]
  edge [
    source 73
    target 3031
  ]
  edge [
    source 73
    target 3032
  ]
  edge [
    source 73
    target 3033
  ]
  edge [
    source 73
    target 3034
  ]
  edge [
    source 73
    target 3035
  ]
  edge [
    source 73
    target 3036
  ]
  edge [
    source 73
    target 3037
  ]
  edge [
    source 73
    target 3038
  ]
  edge [
    source 73
    target 1441
  ]
  edge [
    source 73
    target 3039
  ]
  edge [
    source 73
    target 3040
  ]
  edge [
    source 73
    target 3041
  ]
  edge [
    source 73
    target 3042
  ]
  edge [
    source 73
    target 3043
  ]
  edge [
    source 73
    target 3044
  ]
  edge [
    source 73
    target 2904
  ]
  edge [
    source 73
    target 3045
  ]
  edge [
    source 73
    target 3046
  ]
  edge [
    source 73
    target 3047
  ]
  edge [
    source 73
    target 3048
  ]
  edge [
    source 73
    target 3049
  ]
  edge [
    source 73
    target 3050
  ]
  edge [
    source 73
    target 3051
  ]
  edge [
    source 73
    target 3052
  ]
  edge [
    source 73
    target 1776
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 2914
  ]
  edge [
    source 74
    target 890
  ]
  edge [
    source 74
    target 639
  ]
  edge [
    source 74
    target 725
  ]
  edge [
    source 74
    target 2915
  ]
  edge [
    source 74
    target 3053
  ]
  edge [
    source 74
    target 3054
  ]
  edge [
    source 74
    target 2919
  ]
  edge [
    source 74
    target 2920
  ]
  edge [
    source 74
    target 3055
  ]
  edge [
    source 74
    target 792
  ]
  edge [
    source 74
    target 3056
  ]
  edge [
    source 74
    target 788
  ]
  edge [
    source 74
    target 1763
  ]
  edge [
    source 74
    target 1764
  ]
  edge [
    source 74
    target 1765
  ]
  edge [
    source 74
    target 1766
  ]
  edge [
    source 74
    target 1767
  ]
  edge [
    source 74
    target 1768
  ]
  edge [
    source 74
    target 1769
  ]
  edge [
    source 74
    target 893
  ]
  edge [
    source 74
    target 894
  ]
  edge [
    source 74
    target 664
  ]
  edge [
    source 74
    target 665
  ]
  edge [
    source 74
    target 666
  ]
  edge [
    source 74
    target 667
  ]
  edge [
    source 74
    target 668
  ]
  edge [
    source 74
    target 669
  ]
  edge [
    source 74
    target 670
  ]
  edge [
    source 74
    target 3057
  ]
  edge [
    source 74
    target 2913
  ]
  edge [
    source 74
    target 3058
  ]
  edge [
    source 74
    target 3059
  ]
  edge [
    source 74
    target 3060
  ]
  edge [
    source 74
    target 3061
  ]
  edge [
    source 74
    target 3062
  ]
  edge [
    source 74
    target 1083
  ]
  edge [
    source 74
    target 3063
  ]
  edge [
    source 74
    target 680
  ]
  edge [
    source 74
    target 966
  ]
  edge [
    source 74
    target 3064
  ]
  edge [
    source 74
    target 629
  ]
  edge [
    source 74
    target 2129
  ]
  edge [
    source 74
    target 3065
  ]
  edge [
    source 74
    target 1023
  ]
  edge [
    source 74
    target 2030
  ]
  edge [
    source 74
    target 3066
  ]
  edge [
    source 74
    target 559
  ]
  edge [
    source 74
    target 2491
  ]
  edge [
    source 74
    target 2492
  ]
  edge [
    source 74
    target 321
  ]
  edge [
    source 74
    target 2493
  ]
  edge [
    source 74
    target 121
  ]
  edge [
    source 75
    target 76
  ]
  edge [
    source 75
    target 3067
  ]
  edge [
    source 75
    target 3068
  ]
  edge [
    source 75
    target 3069
  ]
  edge [
    source 75
    target 919
  ]
  edge [
    source 75
    target 666
  ]
  edge [
    source 75
    target 3070
  ]
  edge [
    source 75
    target 3071
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 77
    target 78
  ]
  edge [
    source 77
    target 3072
  ]
  edge [
    source 77
    target 3073
  ]
  edge [
    source 77
    target 3074
  ]
  edge [
    source 77
    target 3075
  ]
  edge [
    source 77
    target 3076
  ]
  edge [
    source 77
    target 3077
  ]
  edge [
    source 77
    target 3078
  ]
  edge [
    source 77
    target 2930
  ]
  edge [
    source 77
    target 3079
  ]
  edge [
    source 77
    target 3080
  ]
  edge [
    source 77
    target 3081
  ]
  edge [
    source 77
    target 3082
  ]
  edge [
    source 77
    target 3083
  ]
  edge [
    source 77
    target 3084
  ]
  edge [
    source 77
    target 3085
  ]
  edge [
    source 77
    target 3086
  ]
  edge [
    source 77
    target 3087
  ]
  edge [
    source 77
    target 3088
  ]
  edge [
    source 77
    target 3089
  ]
  edge [
    source 77
    target 3090
  ]
  edge [
    source 77
    target 3091
  ]
  edge [
    source 77
    target 3092
  ]
  edge [
    source 77
    target 3093
  ]
  edge [
    source 77
    target 3094
  ]
  edge [
    source 77
    target 883
  ]
  edge [
    source 77
    target 3095
  ]
  edge [
    source 77
    target 3096
  ]
  edge [
    source 77
    target 874
  ]
  edge [
    source 77
    target 3097
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 3098
  ]
  edge [
    source 78
    target 1009
  ]
  edge [
    source 78
    target 1283
  ]
  edge [
    source 78
    target 1476
  ]
  edge [
    source 78
    target 3099
  ]
  edge [
    source 78
    target 1694
  ]
  edge [
    source 78
    target 1331
  ]
  edge [
    source 78
    target 1328
  ]
  edge [
    source 78
    target 3100
  ]
  edge [
    source 78
    target 3101
  ]
  edge [
    source 78
    target 3102
  ]
  edge [
    source 78
    target 1376
  ]
  edge [
    source 78
    target 1813
  ]
  edge [
    source 78
    target 3103
  ]
  edge [
    source 78
    target 3104
  ]
  edge [
    source 78
    target 1774
  ]
  edge [
    source 78
    target 3105
  ]
  edge [
    source 78
    target 1012
  ]
  edge [
    source 78
    target 1013
  ]
  edge [
    source 78
    target 1014
  ]
  edge [
    source 78
    target 918
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 2959
  ]
  edge [
    source 80
    target 2960
  ]
  edge [
    source 80
    target 2961
  ]
  edge [
    source 80
    target 1116
  ]
  edge [
    source 80
    target 2962
  ]
  edge [
    source 80
    target 2963
  ]
  edge [
    source 80
    target 1627
  ]
  edge [
    source 80
    target 2437
  ]
  edge [
    source 80
    target 2958
  ]
  edge [
    source 80
    target 2964
  ]
  edge [
    source 80
    target 2429
  ]
  edge [
    source 80
    target 2037
  ]
  edge [
    source 80
    target 3106
  ]
  edge [
    source 80
    target 3107
  ]
  edge [
    source 80
    target 3108
  ]
  edge [
    source 80
    target 3109
  ]
  edge [
    source 80
    target 920
  ]
  edge [
    source 80
    target 3110
  ]
  edge [
    source 80
    target 3111
  ]
  edge [
    source 80
    target 3112
  ]
  edge [
    source 80
    target 3113
  ]
  edge [
    source 80
    target 3114
  ]
  edge [
    source 80
    target 3115
  ]
  edge [
    source 80
    target 2969
  ]
  edge [
    source 80
    target 3116
  ]
  edge [
    source 80
    target 2445
  ]
  edge [
    source 80
    target 3117
  ]
  edge [
    source 80
    target 3118
  ]
  edge [
    source 80
    target 3119
  ]
  edge [
    source 80
    target 3120
  ]
  edge [
    source 80
    target 3121
  ]
  edge [
    source 80
    target 3122
  ]
  edge [
    source 80
    target 2210
  ]
  edge [
    source 80
    target 3123
  ]
  edge [
    source 80
    target 3124
  ]
  edge [
    source 80
    target 3125
  ]
  edge [
    source 80
    target 3126
  ]
  edge [
    source 80
    target 767
  ]
  edge [
    source 80
    target 3127
  ]
  edge [
    source 80
    target 3128
  ]
  edge [
    source 80
    target 3129
  ]
  edge [
    source 80
    target 2556
  ]
  edge [
    source 80
    target 3130
  ]
  edge [
    source 80
    target 1581
  ]
  edge [
    source 80
    target 3131
  ]
  edge [
    source 80
    target 2965
  ]
  edge [
    source 80
    target 745
  ]
  edge [
    source 80
    target 2966
  ]
  edge [
    source 80
    target 2967
  ]
  edge [
    source 80
    target 2968
  ]
  edge [
    source 80
    target 2970
  ]
  edge [
    source 80
    target 2971
  ]
  edge [
    source 80
    target 2972
  ]
  edge [
    source 80
    target 2973
  ]
  edge [
    source 80
    target 2974
  ]
  edge [
    source 80
    target 2975
  ]
  edge [
    source 80
    target 2976
  ]
  edge [
    source 81
    target 3132
  ]
  edge [
    source 81
    target 3133
  ]
  edge [
    source 81
    target 3134
  ]
  edge [
    source 81
    target 2452
  ]
  edge [
    source 81
    target 1619
  ]
  edge [
    source 81
    target 3135
  ]
  edge [
    source 81
    target 3136
  ]
  edge [
    source 81
    target 3137
  ]
  edge [
    source 81
    target 3138
  ]
  edge [
    source 81
    target 3139
  ]
  edge [
    source 81
    target 3140
  ]
  edge [
    source 81
    target 2007
  ]
  edge [
    source 81
    target 3141
  ]
  edge [
    source 81
    target 3142
  ]
  edge [
    source 81
    target 3143
  ]
  edge [
    source 81
    target 2429
  ]
  edge [
    source 81
    target 2430
  ]
  edge [
    source 81
    target 2431
  ]
  edge [
    source 81
    target 2432
  ]
  edge [
    source 81
    target 2433
  ]
  edge [
    source 81
    target 3144
  ]
  edge [
    source 81
    target 2455
  ]
  edge [
    source 81
    target 2450
  ]
  edge [
    source 81
    target 889
  ]
  edge [
    source 81
    target 2456
  ]
  edge [
    source 81
    target 3145
  ]
  edge [
    source 81
    target 3050
  ]
  edge [
    source 81
    target 3146
  ]
  edge [
    source 81
    target 3147
  ]
  edge [
    source 81
    target 3148
  ]
  edge [
    source 81
    target 3149
  ]
  edge [
    source 81
    target 3150
  ]
  edge [
    source 81
    target 3151
  ]
  edge [
    source 81
    target 3052
  ]
  edge [
    source 81
    target 3152
  ]
  edge [
    source 81
    target 3153
  ]
  edge [
    source 81
    target 3154
  ]
  edge [
    source 81
    target 3155
  ]
  edge [
    source 81
    target 3156
  ]
  edge [
    source 81
    target 890
  ]
  edge [
    source 81
    target 894
  ]
  edge [
    source 81
    target 1785
  ]
  edge [
    source 81
    target 1141
  ]
  edge [
    source 81
    target 668
  ]
  edge [
    source 81
    target 1786
  ]
  edge [
    source 81
    target 182
  ]
  edge [
    source 81
    target 3157
  ]
  edge [
    source 81
    target 3158
  ]
  edge [
    source 81
    target 2119
  ]
  edge [
    source 81
    target 3159
  ]
  edge [
    source 81
    target 3160
  ]
  edge [
    source 81
    target 3161
  ]
  edge [
    source 81
    target 3162
  ]
  edge [
    source 81
    target 3163
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 3164
  ]
  edge [
    source 82
    target 3165
  ]
  edge [
    source 82
    target 3166
  ]
  edge [
    source 82
    target 3167
  ]
  edge [
    source 82
    target 3168
  ]
  edge [
    source 82
    target 3169
  ]
  edge [
    source 82
    target 3170
  ]
  edge [
    source 82
    target 3171
  ]
  edge [
    source 82
    target 3172
  ]
  edge [
    source 82
    target 3173
  ]
  edge [
    source 82
    target 3174
  ]
  edge [
    source 82
    target 3175
  ]
  edge [
    source 82
    target 3176
  ]
  edge [
    source 82
    target 3177
  ]
  edge [
    source 82
    target 767
  ]
  edge [
    source 82
    target 3178
  ]
  edge [
    source 82
    target 3179
  ]
  edge [
    source 82
    target 3180
  ]
  edge [
    source 82
    target 3181
  ]
  edge [
    source 82
    target 3182
  ]
  edge [
    source 82
    target 2959
  ]
  edge [
    source 82
    target 3183
  ]
  edge [
    source 82
    target 3184
  ]
  edge [
    source 82
    target 1775
  ]
  edge [
    source 82
    target 3185
  ]
  edge [
    source 82
    target 3186
  ]
  edge [
    source 82
    target 3131
  ]
  edge [
    source 82
    target 3187
  ]
  edge [
    source 82
    target 3188
  ]
  edge [
    source 82
    target 3189
  ]
  edge [
    source 82
    target 3190
  ]
  edge [
    source 82
    target 3191
  ]
  edge [
    source 82
    target 3192
  ]
  edge [
    source 82
    target 3128
  ]
  edge [
    source 82
    target 3193
  ]
  edge [
    source 82
    target 3194
  ]
  edge [
    source 82
    target 3195
  ]
  edge [
    source 82
    target 3015
  ]
  edge [
    source 82
    target 3196
  ]
  edge [
    source 82
    target 3197
  ]
  edge [
    source 82
    target 3010
  ]
  edge [
    source 82
    target 3198
  ]
  edge [
    source 82
    target 3199
  ]
  edge [
    source 82
    target 3200
  ]
  edge [
    source 82
    target 3201
  ]
  edge [
    source 82
    target 3202
  ]
  edge [
    source 83
    target 84
  ]
  edge [
    source 83
    target 3203
  ]
  edge [
    source 84
    target 3204
  ]
  edge [
    source 84
    target 3205
  ]
  edge [
    source 84
    target 3206
  ]
  edge [
    source 84
    target 3207
  ]
  edge [
    source 84
    target 1903
  ]
  edge [
    source 84
    target 3208
  ]
  edge [
    source 84
    target 2008
  ]
  edge [
    source 84
    target 3209
  ]
  edge [
    source 84
    target 3210
  ]
  edge [
    source 1234
    target 3215
  ]
  edge [
    source 1234
    target 3216
  ]
  edge [
    source 1499
    target 3228
  ]
  edge [
    source 3211
    target 3212
  ]
  edge [
    source 3213
    target 3214
  ]
  edge [
    source 3215
    target 3216
  ]
  edge [
    source 3215
    target 3226
  ]
  edge [
    source 3215
    target 3227
  ]
  edge [
    source 3217
    target 3218
  ]
  edge [
    source 3219
    target 3220
  ]
  edge [
    source 3219
    target 3221
  ]
  edge [
    source 3220
    target 3221
  ]
  edge [
    source 3222
    target 3223
  ]
  edge [
    source 3222
    target 3224
  ]
  edge [
    source 3222
    target 3225
  ]
  edge [
    source 3223
    target 3224
  ]
  edge [
    source 3223
    target 3225
  ]
  edge [
    source 3224
    target 3225
  ]
  edge [
    source 3226
    target 3227
  ]
  edge [
    source 3229
    target 3230
  ]
  edge [
    source 3231
    target 3232
  ]
  edge [
    source 3233
    target 3234
  ]
  edge [
    source 3235
    target 3236
  ]
  edge [
    source 3235
    target 3237
  ]
  edge [
    source 3236
    target 3237
  ]
]
