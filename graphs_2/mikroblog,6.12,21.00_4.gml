graph [
  node [
    id 0
    label "si&#281;"
    origin "text"
  ]
  node [
    id 1
    label "wczoraj"
    origin "text"
  ]
  node [
    id 2
    label "odjeba&#263;"
    origin "text"
  ]
  node [
    id 3
    label "dawno"
  ]
  node [
    id 4
    label "doba"
  ]
  node [
    id 5
    label "niedawno"
  ]
  node [
    id 6
    label "aktualnie"
  ]
  node [
    id 7
    label "ostatni"
  ]
  node [
    id 8
    label "dawny"
  ]
  node [
    id 9
    label "d&#322;ugotrwale"
  ]
  node [
    id 10
    label "wcze&#347;niej"
  ]
  node [
    id 11
    label "ongi&#347;"
  ]
  node [
    id 12
    label "dawnie"
  ]
  node [
    id 13
    label "tydzie&#324;"
  ]
  node [
    id 14
    label "noc"
  ]
  node [
    id 15
    label "dzie&#324;"
  ]
  node [
    id 16
    label "czas"
  ]
  node [
    id 17
    label "godzina"
  ]
  node [
    id 18
    label "long_time"
  ]
  node [
    id 19
    label "jednostka_geologiczna"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
]
