graph [
  node [
    id 0
    label "czeladnik"
    origin "text"
  ]
  node [
    id 1
    label "kamionek"
    origin "text"
  ]
  node [
    id 2
    label "b&#322;ysn&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "bia&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "z&#261;b"
    origin "text"
  ]
  node [
    id 5
    label "rzemie&#347;lnik"
  ]
  node [
    id 6
    label "ucze&#324;"
  ]
  node [
    id 7
    label "rzemie&#347;lniczek"
  ]
  node [
    id 8
    label "remiecha"
  ]
  node [
    id 9
    label "cz&#322;owiek"
  ]
  node [
    id 10
    label "wyprawka"
  ]
  node [
    id 11
    label "mundurek"
  ]
  node [
    id 12
    label "szko&#322;a"
  ]
  node [
    id 13
    label "tarcza"
  ]
  node [
    id 14
    label "elew"
  ]
  node [
    id 15
    label "kontynuator"
  ]
  node [
    id 16
    label "absolwent"
  ]
  node [
    id 17
    label "zwolennik"
  ]
  node [
    id 18
    label "praktykant"
  ]
  node [
    id 19
    label "klasa"
  ]
  node [
    id 20
    label "fall"
  ]
  node [
    id 21
    label "zademonstrowa&#263;"
  ]
  node [
    id 22
    label "za&#347;wieci&#263;_si&#281;"
  ]
  node [
    id 23
    label "&#322;ysn&#261;&#263;"
  ]
  node [
    id 24
    label "przedstawi&#263;"
  ]
  node [
    id 25
    label "pokaza&#263;"
  ]
  node [
    id 26
    label "wyrazi&#263;"
  ]
  node [
    id 27
    label "attest"
  ]
  node [
    id 28
    label "indicate"
  ]
  node [
    id 29
    label "spojrze&#263;"
  ]
  node [
    id 30
    label "mign&#261;&#263;"
  ]
  node [
    id 31
    label "kamfenol"
  ]
  node [
    id 32
    label "artykulator"
  ]
  node [
    id 33
    label "borowa&#263;"
  ]
  node [
    id 34
    label "leczenie_kana&#322;owe"
  ]
  node [
    id 35
    label "uz&#281;bienie"
  ]
  node [
    id 36
    label "polakowa&#263;"
  ]
  node [
    id 37
    label "miazga_z&#281;ba"
  ]
  node [
    id 38
    label "obr&#281;cz"
  ]
  node [
    id 39
    label "ubytek"
  ]
  node [
    id 40
    label "tooth"
  ]
  node [
    id 41
    label "cement"
  ]
  node [
    id 42
    label "element_anatomiczny"
  ]
  node [
    id 43
    label "plombowa&#263;"
  ]
  node [
    id 44
    label "z&#281;batka"
  ]
  node [
    id 45
    label "emaliowanie"
  ]
  node [
    id 46
    label "wierzcho&#322;ek_korzenia"
  ]
  node [
    id 47
    label "ostrze"
  ]
  node [
    id 48
    label "polakowanie"
  ]
  node [
    id 49
    label "zaplombowa&#263;"
  ]
  node [
    id 50
    label "borowanie"
  ]
  node [
    id 51
    label "zaplombowanie"
  ]
  node [
    id 52
    label "emaliowa&#263;"
  ]
  node [
    id 53
    label "szkliwo"
  ]
  node [
    id 54
    label "&#380;uchwa"
  ]
  node [
    id 55
    label "z&#281;bina"
  ]
  node [
    id 56
    label "szczoteczka"
  ]
  node [
    id 57
    label "&#322;uk_z&#281;bowy"
  ]
  node [
    id 58
    label "mostek"
  ]
  node [
    id 59
    label "plombowanie"
  ]
  node [
    id 60
    label "korona"
  ]
  node [
    id 61
    label "kraw&#281;d&#378;"
  ]
  node [
    id 62
    label "aparat_artykulacyjny"
  ]
  node [
    id 63
    label "urz&#261;dzenie"
  ]
  node [
    id 64
    label "przek&#322;adnia"
  ]
  node [
    id 65
    label "podci&#261;gnik_z&#281;batkowy"
  ]
  node [
    id 66
    label "aparat_ortodontyczny"
  ]
  node [
    id 67
    label "klawiatura"
  ]
  node [
    id 68
    label "denture"
  ]
  node [
    id 69
    label "uk&#322;ad"
  ]
  node [
    id 70
    label "jama_ustna"
  ]
  node [
    id 71
    label "bezz&#281;bno&#347;&#263;"
  ]
  node [
    id 72
    label "zgryz"
  ]
  node [
    id 73
    label "szczena"
  ]
  node [
    id 74
    label "guzowato&#347;&#263;_br&#243;dkowa"
  ]
  node [
    id 75
    label "dzi&#261;s&#322;o"
  ]
  node [
    id 76
    label "ko&#347;&#263;_z&#281;bowa"
  ]
  node [
    id 77
    label "z&#281;bod&#243;&#322;"
  ]
  node [
    id 78
    label "artykulacja"
  ]
  node [
    id 79
    label "&#322;&#281;kotka"
  ]
  node [
    id 80
    label "czaszka"
  ]
  node [
    id 81
    label "materia&#322;_budowlany"
  ]
  node [
    id 82
    label "wi&#261;zanie"
  ]
  node [
    id 83
    label "spoiwo"
  ]
  node [
    id 84
    label "wertebroplastyka"
  ]
  node [
    id 85
    label "wype&#322;nienie"
  ]
  node [
    id 86
    label "tworzywo"
  ]
  node [
    id 87
    label "wi&#261;za&#263;"
  ]
  node [
    id 88
    label "tkanka_kostna"
  ]
  node [
    id 89
    label "zwi&#261;za&#263;"
  ]
  node [
    id 90
    label "ko&#322;o"
  ]
  node [
    id 91
    label "element"
  ]
  node [
    id 92
    label "okucie"
  ]
  node [
    id 93
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 94
    label "tkanka"
  ]
  node [
    id 95
    label "odontoblast"
  ]
  node [
    id 96
    label "dentine"
  ]
  node [
    id 97
    label "ekskawator"
  ]
  node [
    id 98
    label "corona"
  ]
  node [
    id 99
    label "zwie&#324;czenie"
  ]
  node [
    id 100
    label "zesp&#243;&#322;"
  ]
  node [
    id 101
    label "warkocz"
  ]
  node [
    id 102
    label "regalia"
  ]
  node [
    id 103
    label "drzewo"
  ]
  node [
    id 104
    label "czub"
  ]
  node [
    id 105
    label "Kr&#243;lestwo_Prus"
  ]
  node [
    id 106
    label "bryd&#380;"
  ]
  node [
    id 107
    label "moneta"
  ]
  node [
    id 108
    label "przepaska"
  ]
  node [
    id 109
    label "r&#243;g"
  ]
  node [
    id 110
    label "wieniec"
  ]
  node [
    id 111
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 112
    label "motyl"
  ]
  node [
    id 113
    label "geofit"
  ]
  node [
    id 114
    label "liliowate"
  ]
  node [
    id 115
    label "pa&#324;stwo"
  ]
  node [
    id 116
    label "kwiat"
  ]
  node [
    id 117
    label "jednostka_monetarna"
  ]
  node [
    id 118
    label "proteza_dentystyczna"
  ]
  node [
    id 119
    label "urz&#261;d"
  ]
  node [
    id 120
    label "kok"
  ]
  node [
    id 121
    label "diadem"
  ]
  node [
    id 122
    label "p&#322;atek"
  ]
  node [
    id 123
    label "genitalia"
  ]
  node [
    id 124
    label "maksimum"
  ]
  node [
    id 125
    label "Crown"
  ]
  node [
    id 126
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 127
    label "g&#243;ra"
  ]
  node [
    id 128
    label "kres"
  ]
  node [
    id 129
    label "znak_muzyczny"
  ]
  node [
    id 130
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 131
    label "pow&#322;oka"
  ]
  node [
    id 132
    label "w&#322;osowina"
  ]
  node [
    id 133
    label "zabezpieczanie"
  ]
  node [
    id 134
    label "painting"
  ]
  node [
    id 135
    label "malowanie"
  ]
  node [
    id 136
    label "powlekanie"
  ]
  node [
    id 137
    label "varnish"
  ]
  node [
    id 138
    label "wype&#322;ni&#263;"
  ]
  node [
    id 139
    label "sealing_wax"
  ]
  node [
    id 140
    label "pie&#324;"
  ]
  node [
    id 141
    label "zabezpieczy&#263;"
  ]
  node [
    id 142
    label "przymocowa&#263;"
  ]
  node [
    id 143
    label "r&#243;&#380;nica"
  ]
  node [
    id 144
    label "spadek"
  ]
  node [
    id 145
    label "pr&#243;chnica"
  ]
  node [
    id 146
    label "brak"
  ]
  node [
    id 147
    label "otw&#243;r"
  ]
  node [
    id 148
    label "martwica"
  ]
  node [
    id 149
    label "narz&#281;dzie"
  ]
  node [
    id 150
    label "miote&#322;ka"
  ]
  node [
    id 151
    label "rekwizyt_muzyczny"
  ]
  node [
    id 152
    label "ow&#322;osienie"
  ]
  node [
    id 153
    label "przybory_toaletowe"
  ]
  node [
    id 154
    label "pszczo&#322;a_robotnica"
  ]
  node [
    id 155
    label "malowa&#263;"
  ]
  node [
    id 156
    label "enamel"
  ]
  node [
    id 157
    label "powleka&#263;"
  ]
  node [
    id 158
    label "zabezpiecza&#263;"
  ]
  node [
    id 159
    label "wiertarka"
  ]
  node [
    id 160
    label "przewiercanie"
  ]
  node [
    id 161
    label "leczenie"
  ]
  node [
    id 162
    label "robienie"
  ]
  node [
    id 163
    label "przewiercenie"
  ]
  node [
    id 164
    label "czyszczenie"
  ]
  node [
    id 165
    label "wwiercanie_si&#281;"
  ]
  node [
    id 166
    label "exercise"
  ]
  node [
    id 167
    label "dentysta"
  ]
  node [
    id 168
    label "wwiercenie_si&#281;"
  ]
  node [
    id 169
    label "przymocowywa&#263;"
  ]
  node [
    id 170
    label "wype&#322;nia&#263;"
  ]
  node [
    id 171
    label "waterproofing"
  ]
  node [
    id 172
    label "przymocowywanie"
  ]
  node [
    id 173
    label "wype&#322;nianie"
  ]
  node [
    id 174
    label "klatka_piersiowa"
  ]
  node [
    id 175
    label "rower"
  ]
  node [
    id 176
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 177
    label "&#263;wiczenie"
  ]
  node [
    id 178
    label "sieciowanie"
  ]
  node [
    id 179
    label "r&#281;koje&#347;&#263;_mostka"
  ]
  node [
    id 180
    label "sternum"
  ]
  node [
    id 181
    label "trzon_mostka"
  ]
  node [
    id 182
    label "wyrostek_mieczykowaty"
  ]
  node [
    id 183
    label "okulary"
  ]
  node [
    id 184
    label "ko&#347;&#263;"
  ]
  node [
    id 185
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 186
    label "bridge"
  ]
  node [
    id 187
    label "instrument_strunowy"
  ]
  node [
    id 188
    label "obw&#243;d_elektroniczny"
  ]
  node [
    id 189
    label "leczy&#263;"
  ]
  node [
    id 190
    label "robi&#263;"
  ]
  node [
    id 191
    label "oczyszcza&#263;"
  ]
  node [
    id 192
    label "drill"
  ]
  node [
    id 193
    label "tug"
  ]
  node [
    id 194
    label "przymocowanie"
  ]
  node [
    id 195
    label "zabezpieczenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
]
