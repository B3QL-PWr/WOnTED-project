graph [
  node [
    id 0
    label "adam"
    origin "text"
  ]
  node [
    id 1
    label "widzie&#263;"
    origin "text"
  ]
  node [
    id 2
    label "jak"
    origin "text"
  ]
  node [
    id 3
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 4
    label "markotny"
    origin "text"
  ]
  node [
    id 5
    label "krzes&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "strzela&#263;"
    origin "text"
  ]
  node [
    id 7
    label "palce"
    origin "text"
  ]
  node [
    id 8
    label "podchodzi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "g&#322;ad&#378;"
    origin "text"
  ]
  node [
    id 10
    label "policzek"
    origin "text"
  ]
  node [
    id 11
    label "ogl&#261;da&#263;"
  ]
  node [
    id 12
    label "wyobra&#380;a&#263;_sobie"
  ]
  node [
    id 13
    label "notice"
  ]
  node [
    id 14
    label "perceive"
  ]
  node [
    id 15
    label "punkt_widzenia"
  ]
  node [
    id 16
    label "male&#263;"
  ]
  node [
    id 17
    label "zmale&#263;"
  ]
  node [
    id 18
    label "go_steady"
  ]
  node [
    id 19
    label "zwi&#281;ksza&#263;_si&#281;"
  ]
  node [
    id 20
    label "postrzega&#263;"
  ]
  node [
    id 21
    label "dostrzega&#263;"
  ]
  node [
    id 22
    label "aprobowa&#263;"
  ]
  node [
    id 23
    label "spowodowa&#263;"
  ]
  node [
    id 24
    label "os&#261;dza&#263;"
  ]
  node [
    id 25
    label "wyr&#243;&#380;nia&#263;"
  ]
  node [
    id 26
    label "spotka&#263;"
  ]
  node [
    id 27
    label "reagowa&#263;"
  ]
  node [
    id 28
    label "zdawa&#263;_sobie_spraw&#281;"
  ]
  node [
    id 29
    label "wzrok"
  ]
  node [
    id 30
    label "react"
  ]
  node [
    id 31
    label "answer"
  ]
  node [
    id 32
    label "odpowiada&#263;"
  ]
  node [
    id 33
    label "uczestniczy&#263;"
  ]
  node [
    id 34
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 35
    label "dochodzi&#263;"
  ]
  node [
    id 36
    label "odp&#322;ywa&#263;"
  ]
  node [
    id 37
    label "doj&#347;&#263;"
  ]
  node [
    id 38
    label "obacza&#263;"
  ]
  node [
    id 39
    label "zwraca&#263;_uwag&#281;"
  ]
  node [
    id 40
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 41
    label "styka&#263;_si&#281;"
  ]
  node [
    id 42
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 43
    label "visualize"
  ]
  node [
    id 44
    label "pozna&#263;"
  ]
  node [
    id 45
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 46
    label "insert"
  ]
  node [
    id 47
    label "znale&#378;&#263;"
  ]
  node [
    id 48
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 49
    label "befall"
  ]
  node [
    id 50
    label "orientowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "uznawa&#263;"
  ]
  node [
    id 52
    label "approbate"
  ]
  node [
    id 53
    label "act"
  ]
  node [
    id 54
    label "robi&#263;"
  ]
  node [
    id 55
    label "hold"
  ]
  node [
    id 56
    label "strike"
  ]
  node [
    id 57
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 58
    label "s&#261;dzi&#263;"
  ]
  node [
    id 59
    label "powodowa&#263;"
  ]
  node [
    id 60
    label "znajdowa&#263;"
  ]
  node [
    id 61
    label "traktowa&#263;"
  ]
  node [
    id 62
    label "nagradza&#263;"
  ]
  node [
    id 63
    label "sign"
  ]
  node [
    id 64
    label "forytowa&#263;"
  ]
  node [
    id 65
    label "zm&#261;cenie_si&#281;"
  ]
  node [
    id 66
    label "czynno&#347;&#263;_wzrokowa"
  ]
  node [
    id 67
    label "okulista"
  ]
  node [
    id 68
    label "zmys&#322;"
  ]
  node [
    id 69
    label "oko"
  ]
  node [
    id 70
    label "widzenie"
  ]
  node [
    id 71
    label "m&#281;tnienie"
  ]
  node [
    id 72
    label "m&#281;tnie&#263;"
  ]
  node [
    id 73
    label "expression"
  ]
  node [
    id 74
    label "kontakt"
  ]
  node [
    id 75
    label "worsen"
  ]
  node [
    id 76
    label "zmniejszy&#263;_si&#281;"
  ]
  node [
    id 77
    label "reduce"
  ]
  node [
    id 78
    label "sta&#263;_si&#281;"
  ]
  node [
    id 79
    label "relax"
  ]
  node [
    id 80
    label "zmniejsza&#263;_si&#281;"
  ]
  node [
    id 81
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 82
    label "slack"
  ]
  node [
    id 83
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 84
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 85
    label "zobo"
  ]
  node [
    id 86
    label "byd&#322;o"
  ]
  node [
    id 87
    label "dzo"
  ]
  node [
    id 88
    label "yakalo"
  ]
  node [
    id 89
    label "zbi&#243;r"
  ]
  node [
    id 90
    label "kr&#281;torogie"
  ]
  node [
    id 91
    label "g&#322;owa"
  ]
  node [
    id 92
    label "livestock"
  ]
  node [
    id 93
    label "posp&#243;lstwo"
  ]
  node [
    id 94
    label "kraal"
  ]
  node [
    id 95
    label "czochrad&#322;o"
  ]
  node [
    id 96
    label "prze&#380;uwacz"
  ]
  node [
    id 97
    label "zebu"
  ]
  node [
    id 98
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 99
    label "bizon"
  ]
  node [
    id 100
    label "byd&#322;o_domowe"
  ]
  node [
    id 101
    label "pause"
  ]
  node [
    id 102
    label "garowa&#263;"
  ]
  node [
    id 103
    label "brood"
  ]
  node [
    id 104
    label "zwierz&#281;"
  ]
  node [
    id 105
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 106
    label "trwa&#263;"
  ]
  node [
    id 107
    label "przebywa&#263;"
  ]
  node [
    id 108
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 109
    label "sit"
  ]
  node [
    id 110
    label "doprowadza&#263;"
  ]
  node [
    id 111
    label "mieszka&#263;"
  ]
  node [
    id 112
    label "ptak"
  ]
  node [
    id 113
    label "spoczywa&#263;"
  ]
  node [
    id 114
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 115
    label "tkwi&#263;"
  ]
  node [
    id 116
    label "hesitate"
  ]
  node [
    id 117
    label "przestawa&#263;"
  ]
  node [
    id 118
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 119
    label "istnie&#263;"
  ]
  node [
    id 120
    label "nadzieja"
  ]
  node [
    id 121
    label "lie"
  ]
  node [
    id 122
    label "by&#263;"
  ]
  node [
    id 123
    label "odpoczywa&#263;"
  ]
  node [
    id 124
    label "gr&#243;b"
  ]
  node [
    id 125
    label "adhere"
  ]
  node [
    id 126
    label "pozostawa&#263;"
  ]
  node [
    id 127
    label "stand"
  ]
  node [
    id 128
    label "zostawa&#263;"
  ]
  node [
    id 129
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 130
    label "panowa&#263;"
  ]
  node [
    id 131
    label "sta&#263;"
  ]
  node [
    id 132
    label "fall"
  ]
  node [
    id 133
    label "zajmowa&#263;"
  ]
  node [
    id 134
    label "room"
  ]
  node [
    id 135
    label "fix"
  ]
  node [
    id 136
    label "polega&#263;"
  ]
  node [
    id 137
    label "moderate"
  ]
  node [
    id 138
    label "wprowadza&#263;"
  ]
  node [
    id 139
    label "prowadzi&#263;"
  ]
  node [
    id 140
    label "wykonywa&#263;"
  ]
  node [
    id 141
    label "rig"
  ]
  node [
    id 142
    label "message"
  ]
  node [
    id 143
    label "wzbudza&#263;"
  ]
  node [
    id 144
    label "deliver"
  ]
  node [
    id 145
    label "&#347;l&#281;cze&#263;"
  ]
  node [
    id 146
    label "tankowa&#263;"
  ]
  node [
    id 147
    label "le&#380;akowa&#263;"
  ]
  node [
    id 148
    label "rosn&#261;&#263;"
  ]
  node [
    id 149
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 150
    label "harowa&#263;"
  ]
  node [
    id 151
    label "monogamia"
  ]
  node [
    id 152
    label "grzbiet"
  ]
  node [
    id 153
    label "bestia"
  ]
  node [
    id 154
    label "treser"
  ]
  node [
    id 155
    label "niecz&#322;owiek"
  ]
  node [
    id 156
    label "zar&#380;ni&#281;cie"
  ]
  node [
    id 157
    label "agresja"
  ]
  node [
    id 158
    label "skubni&#281;cie"
  ]
  node [
    id 159
    label "skuba&#263;"
  ]
  node [
    id 160
    label "tresowa&#263;"
  ]
  node [
    id 161
    label "oz&#243;r"
  ]
  node [
    id 162
    label "istota_&#380;ywa"
  ]
  node [
    id 163
    label "wylinka"
  ]
  node [
    id 164
    label "poskramia&#263;"
  ]
  node [
    id 165
    label "fukni&#281;cie"
  ]
  node [
    id 166
    label "siedzenie"
  ]
  node [
    id 167
    label "wios&#322;owa&#263;"
  ]
  node [
    id 168
    label "zwyrol"
  ]
  node [
    id 169
    label "pa&#347;&#263;_si&#281;"
  ]
  node [
    id 170
    label "budowa_cia&#322;a"
  ]
  node [
    id 171
    label "sodomita"
  ]
  node [
    id 172
    label "wiwarium"
  ]
  node [
    id 173
    label "oswaja&#263;"
  ]
  node [
    id 174
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 175
    label "degenerat"
  ]
  node [
    id 176
    label "le&#380;e&#263;"
  ]
  node [
    id 177
    label "przyssawka"
  ]
  node [
    id 178
    label "animalista"
  ]
  node [
    id 179
    label "fauna"
  ]
  node [
    id 180
    label "hodowla"
  ]
  node [
    id 181
    label "popapraniec"
  ]
  node [
    id 182
    label "napa&#347;&#263;_si&#281;"
  ]
  node [
    id 183
    label "cz&#322;owiek"
  ]
  node [
    id 184
    label "le&#380;enie"
  ]
  node [
    id 185
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 186
    label "poligamia"
  ]
  node [
    id 187
    label "budowa"
  ]
  node [
    id 188
    label "napasienie_si&#281;"
  ]
  node [
    id 189
    label "&#322;eb"
  ]
  node [
    id 190
    label "paszcza"
  ]
  node [
    id 191
    label "czerniak"
  ]
  node [
    id 192
    label "zwierz&#281;ta"
  ]
  node [
    id 193
    label "p&#322;yni&#281;cie"
  ]
  node [
    id 194
    label "zachowanie"
  ]
  node [
    id 195
    label "skubn&#261;&#263;"
  ]
  node [
    id 196
    label "wios&#322;owanie"
  ]
  node [
    id 197
    label "skubanie"
  ]
  node [
    id 198
    label "okrutnik"
  ]
  node [
    id 199
    label "pasienie_si&#281;"
  ]
  node [
    id 200
    label "farba"
  ]
  node [
    id 201
    label "weterynarz"
  ]
  node [
    id 202
    label "gad"
  ]
  node [
    id 203
    label "dor&#380;n&#261;&#263;"
  ]
  node [
    id 204
    label "fukanie"
  ]
  node [
    id 205
    label "bird"
  ]
  node [
    id 206
    label "roz&#263;wierkanie_si&#281;"
  ]
  node [
    id 207
    label "hukni&#281;cie"
  ]
  node [
    id 208
    label "upierzenie"
  ]
  node [
    id 209
    label "grzebie&#324;"
  ]
  node [
    id 210
    label "pi&#243;ro"
  ]
  node [
    id 211
    label "skok"
  ]
  node [
    id 212
    label "zaklekotanie"
  ]
  node [
    id 213
    label "ptaki"
  ]
  node [
    id 214
    label "owodniowiec"
  ]
  node [
    id 215
    label "kloaka"
  ]
  node [
    id 216
    label "dzi&#243;bni&#281;cie"
  ]
  node [
    id 217
    label "kuper"
  ]
  node [
    id 218
    label "ko&#347;&#263;_potyliczna"
  ]
  node [
    id 219
    label "wysiadywa&#263;"
  ]
  node [
    id 220
    label "dziobn&#261;&#263;"
  ]
  node [
    id 221
    label "ptactwo"
  ]
  node [
    id 222
    label "ptasz&#281;"
  ]
  node [
    id 223
    label "dziobni&#281;cie"
  ]
  node [
    id 224
    label "dzi&#243;b"
  ]
  node [
    id 225
    label "dziobanie"
  ]
  node [
    id 226
    label "skrzyd&#322;o"
  ]
  node [
    id 227
    label "dzioba&#263;"
  ]
  node [
    id 228
    label "pogwizdywanie"
  ]
  node [
    id 229
    label "dzi&#243;bn&#261;&#263;"
  ]
  node [
    id 230
    label "wysiedzie&#263;"
  ]
  node [
    id 231
    label "ro&#347;lina_zielna"
  ]
  node [
    id 232
    label "sitowate"
  ]
  node [
    id 233
    label "cover"
  ]
  node [
    id 234
    label "markotnie"
  ]
  node [
    id 235
    label "smutny"
  ]
  node [
    id 236
    label "przykry"
  ]
  node [
    id 237
    label "z&#322;y"
  ]
  node [
    id 238
    label "smutno"
  ]
  node [
    id 239
    label "negatywny"
  ]
  node [
    id 240
    label "dispiritedly"
  ]
  node [
    id 241
    label "gloomily"
  ]
  node [
    id 242
    label "krzes&#322;o_elektryczne"
  ]
  node [
    id 243
    label "mebel"
  ]
  node [
    id 244
    label "por&#281;cz"
  ]
  node [
    id 245
    label "noga"
  ]
  node [
    id 246
    label "&#322;awka"
  ]
  node [
    id 247
    label "oparcie"
  ]
  node [
    id 248
    label "siedzisko"
  ]
  node [
    id 249
    label "zaplecek"
  ]
  node [
    id 250
    label "umeblowanie"
  ]
  node [
    id 251
    label "przeszklenie"
  ]
  node [
    id 252
    label "gzyms"
  ]
  node [
    id 253
    label "nadstawa"
  ]
  node [
    id 254
    label "obudowywanie"
  ]
  node [
    id 255
    label "obudowywa&#263;"
  ]
  node [
    id 256
    label "element_wyposa&#380;enia"
  ]
  node [
    id 257
    label "sprz&#281;t"
  ]
  node [
    id 258
    label "ramiak"
  ]
  node [
    id 259
    label "obudowa&#263;"
  ]
  node [
    id 260
    label "obudowanie"
  ]
  node [
    id 261
    label "pupa"
  ]
  node [
    id 262
    label "przedzia&#322;"
  ]
  node [
    id 263
    label "position"
  ]
  node [
    id 264
    label "otarcie_si&#281;"
  ]
  node [
    id 265
    label "jadalnia"
  ]
  node [
    id 266
    label "spoczywanie"
  ]
  node [
    id 267
    label "otaczanie_si&#281;"
  ]
  node [
    id 268
    label "wysiedzenie"
  ]
  node [
    id 269
    label "otoczenie_si&#281;"
  ]
  node [
    id 270
    label "trwanie"
  ]
  node [
    id 271
    label "trybuna"
  ]
  node [
    id 272
    label "przedmiot"
  ]
  node [
    id 273
    label "autobus"
  ]
  node [
    id 274
    label "posadzenie"
  ]
  node [
    id 275
    label "ujmowanie"
  ]
  node [
    id 276
    label "sadzanie"
  ]
  node [
    id 277
    label "miejsce"
  ]
  node [
    id 278
    label "samolot"
  ]
  node [
    id 279
    label "bycie"
  ]
  node [
    id 280
    label "wyj&#347;cie"
  ]
  node [
    id 281
    label "wysiadywanie"
  ]
  node [
    id 282
    label "przebywanie"
  ]
  node [
    id 283
    label "odsiadywanie"
  ]
  node [
    id 284
    label "touch"
  ]
  node [
    id 285
    label "ocieranie_si&#281;"
  ]
  node [
    id 286
    label "wstanie"
  ]
  node [
    id 287
    label "tkwienie"
  ]
  node [
    id 288
    label "residency"
  ]
  node [
    id 289
    label "mieszkanie"
  ]
  node [
    id 290
    label "odsiedzenie"
  ]
  node [
    id 291
    label "posiedzenie"
  ]
  node [
    id 292
    label "zajmowanie_si&#281;"
  ]
  node [
    id 293
    label "umieszczenie"
  ]
  node [
    id 294
    label "wychodzenie"
  ]
  node [
    id 295
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 296
    label "powo&#322;anie_si&#281;"
  ]
  node [
    id 297
    label "podstawa"
  ]
  node [
    id 298
    label "zaczerpni&#281;cie"
  ]
  node [
    id 299
    label "back"
  ]
  node [
    id 300
    label "anchor"
  ]
  node [
    id 301
    label "ustawienie"
  ]
  node [
    id 302
    label "podpora"
  ]
  node [
    id 303
    label "siod&#322;o"
  ]
  node [
    id 304
    label "czpas"
  ]
  node [
    id 305
    label "ekstraklasa"
  ]
  node [
    id 306
    label "bezbramkowy"
  ]
  node [
    id 307
    label "pi&#322;ka"
  ]
  node [
    id 308
    label "kopn&#261;&#263;"
  ]
  node [
    id 309
    label "catenaccio"
  ]
  node [
    id 310
    label "kopanie"
  ]
  node [
    id 311
    label "&#380;&#243;&#322;ta_kartka"
  ]
  node [
    id 312
    label "bramkarz"
  ]
  node [
    id 313
    label "dogra&#263;"
  ]
  node [
    id 314
    label "dogranie"
  ]
  node [
    id 315
    label "lobowanie"
  ]
  node [
    id 316
    label "tackle"
  ]
  node [
    id 317
    label "zamurowywa&#263;"
  ]
  node [
    id 318
    label "&#322;&#261;czyna"
  ]
  node [
    id 319
    label "dublet"
  ]
  node [
    id 320
    label "mi&#281;czak"
  ]
  node [
    id 321
    label "zamurowanie"
  ]
  node [
    id 322
    label "Wis&#322;a"
  ]
  node [
    id 323
    label "sfaulowanie"
  ]
  node [
    id 324
    label "sfaulowa&#263;"
  ]
  node [
    id 325
    label "r&#281;ka"
  ]
  node [
    id 326
    label "mi&#281;sie&#324;_krawiecki"
  ]
  node [
    id 327
    label "stopa"
  ]
  node [
    id 328
    label "nerw_udowo-goleniowy"
  ]
  node [
    id 329
    label "lobowa&#263;"
  ]
  node [
    id 330
    label "dogrywanie"
  ]
  node [
    id 331
    label "czerwona_kartka"
  ]
  node [
    id 332
    label "faulowanie"
  ]
  node [
    id 333
    label "mato&#322;"
  ]
  node [
    id 334
    label "&#322;amaga"
  ]
  node [
    id 335
    label "narz&#261;d_ruchu"
  ]
  node [
    id 336
    label "przelobowa&#263;"
  ]
  node [
    id 337
    label "mi&#281;sie&#324;_po&#347;ladkowy_wielki"
  ]
  node [
    id 338
    label "d&#322;uga_pi&#322;ka"
  ]
  node [
    id 339
    label "mundial"
  ]
  node [
    id 340
    label "mi&#281;sie&#324;_po&#347;ladkowy_ma&#322;y"
  ]
  node [
    id 341
    label "faulowa&#263;"
  ]
  node [
    id 342
    label "s&#322;abeusz"
  ]
  node [
    id 343
    label "napinacz"
  ]
  node [
    id 344
    label "przelobowanie"
  ]
  node [
    id 345
    label "ko&#324;czyna_dolna"
  ]
  node [
    id 346
    label "zamurowa&#263;"
  ]
  node [
    id 347
    label "dogrywa&#263;"
  ]
  node [
    id 348
    label "ko&#324;czyna"
  ]
  node [
    id 349
    label "gira"
  ]
  node [
    id 350
    label "zamurowywanie"
  ]
  node [
    id 351
    label "kopni&#281;cie"
  ]
  node [
    id 352
    label "interliga"
  ]
  node [
    id 353
    label "mi&#281;sie&#324;_po&#347;ladkowy_&#347;redni"
  ]
  node [
    id 354
    label "nerw_udowy"
  ]
  node [
    id 355
    label "jedenastka"
  ]
  node [
    id 356
    label "depta&#263;"
  ]
  node [
    id 357
    label "kopa&#263;"
  ]
  node [
    id 358
    label "handhold"
  ]
  node [
    id 359
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 360
    label "uchwyt"
  ]
  node [
    id 361
    label "tron"
  ]
  node [
    id 362
    label "fotel"
  ]
  node [
    id 363
    label "blat"
  ]
  node [
    id 364
    label "klasa"
  ]
  node [
    id 365
    label "grupa"
  ]
  node [
    id 366
    label "uderza&#263;"
  ]
  node [
    id 367
    label "snap"
  ]
  node [
    id 368
    label "odpala&#263;"
  ]
  node [
    id 369
    label "kierowa&#263;"
  ]
  node [
    id 370
    label "fight"
  ]
  node [
    id 371
    label "napierdziela&#263;"
  ]
  node [
    id 372
    label "train"
  ]
  node [
    id 373
    label "plu&#263;"
  ]
  node [
    id 374
    label "walczy&#263;"
  ]
  node [
    id 375
    label "match"
  ]
  node [
    id 376
    label "przeznacza&#263;"
  ]
  node [
    id 377
    label "administrowa&#263;"
  ]
  node [
    id 378
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 379
    label "motywowa&#263;"
  ]
  node [
    id 380
    label "order"
  ]
  node [
    id 381
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 382
    label "sterowa&#263;"
  ]
  node [
    id 383
    label "ustawia&#263;"
  ]
  node [
    id 384
    label "zwierzchnik"
  ]
  node [
    id 385
    label "wysy&#322;a&#263;"
  ]
  node [
    id 386
    label "control"
  ]
  node [
    id 387
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 388
    label "give"
  ]
  node [
    id 389
    label "manipulate"
  ]
  node [
    id 390
    label "indicate"
  ]
  node [
    id 391
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 392
    label "uderza&#263;_do_panny"
  ]
  node [
    id 393
    label "zadawa&#263;"
  ]
  node [
    id 394
    label "break"
  ]
  node [
    id 395
    label "konkurowa&#263;"
  ]
  node [
    id 396
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 397
    label "mie&#263;_miejsce"
  ]
  node [
    id 398
    label "hopka&#263;"
  ]
  node [
    id 399
    label "blend"
  ]
  node [
    id 400
    label "startowa&#263;"
  ]
  node [
    id 401
    label "rani&#263;"
  ]
  node [
    id 402
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 403
    label "przypieprza&#263;"
  ]
  node [
    id 404
    label "go"
  ]
  node [
    id 405
    label "funkcjonowa&#263;"
  ]
  node [
    id 406
    label "chop"
  ]
  node [
    id 407
    label "ofensywny"
  ]
  node [
    id 408
    label "sztacha&#263;"
  ]
  node [
    id 409
    label "rwa&#263;"
  ]
  node [
    id 410
    label "napada&#263;"
  ]
  node [
    id 411
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 412
    label "woo"
  ]
  node [
    id 413
    label "wstrzeliwa&#263;_si&#281;"
  ]
  node [
    id 414
    label "nast&#281;powa&#263;"
  ]
  node [
    id 415
    label "take"
  ]
  node [
    id 416
    label "krytykowa&#263;"
  ]
  node [
    id 417
    label "dotyka&#263;"
  ]
  node [
    id 418
    label "stara&#263;_si&#281;"
  ]
  node [
    id 419
    label "oszukiwa&#263;"
  ]
  node [
    id 420
    label "tentegowa&#263;"
  ]
  node [
    id 421
    label "urz&#261;dza&#263;"
  ]
  node [
    id 422
    label "praca"
  ]
  node [
    id 423
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 424
    label "czyni&#263;"
  ]
  node [
    id 425
    label "work"
  ]
  node [
    id 426
    label "przerabia&#263;"
  ]
  node [
    id 427
    label "post&#281;powa&#263;"
  ]
  node [
    id 428
    label "peddle"
  ]
  node [
    id 429
    label "organizowa&#263;"
  ]
  node [
    id 430
    label "falowa&#263;"
  ]
  node [
    id 431
    label "stylizowa&#263;"
  ]
  node [
    id 432
    label "wydala&#263;"
  ]
  node [
    id 433
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 434
    label "ukazywa&#263;"
  ]
  node [
    id 435
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 436
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 437
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 438
    label "my&#347;lenie"
  ]
  node [
    id 439
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 440
    label "argue"
  ]
  node [
    id 441
    label "dzia&#322;a&#263;"
  ]
  node [
    id 442
    label "contend"
  ]
  node [
    id 443
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 444
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 445
    label "zawody"
  ]
  node [
    id 446
    label "cope"
  ]
  node [
    id 447
    label "wrestle"
  ]
  node [
    id 448
    label "fuel"
  ]
  node [
    id 449
    label "dawa&#263;"
  ]
  node [
    id 450
    label "reject"
  ]
  node [
    id 451
    label "za&#347;wieca&#263;"
  ]
  node [
    id 452
    label "zapala&#263;"
  ]
  node [
    id 453
    label "gardzi&#263;"
  ]
  node [
    id 454
    label "spew"
  ]
  node [
    id 455
    label "plwa&#263;"
  ]
  node [
    id 456
    label "wydziela&#263;"
  ]
  node [
    id 457
    label "naciska&#263;"
  ]
  node [
    id 458
    label "gra&#263;"
  ]
  node [
    id 459
    label "gada&#263;"
  ]
  node [
    id 460
    label "bi&#263;"
  ]
  node [
    id 461
    label "bole&#263;"
  ]
  node [
    id 462
    label "popyla&#263;"
  ]
  node [
    id 463
    label "psu&#263;_si&#281;"
  ]
  node [
    id 464
    label "zawarto&#347;&#263;"
  ]
  node [
    id 465
    label "struktura_anatomiczna"
  ]
  node [
    id 466
    label "podeszwa"
  ]
  node [
    id 467
    label "paluch"
  ]
  node [
    id 468
    label "metrical_foot"
  ]
  node [
    id 469
    label "sylaba"
  ]
  node [
    id 470
    label "centrala"
  ]
  node [
    id 471
    label "ko&#347;&#263;_klinowata"
  ]
  node [
    id 472
    label "ko&#324;sko&#347;&#263;"
  ]
  node [
    id 473
    label "odn&#243;&#380;e"
  ]
  node [
    id 474
    label "wska&#378;nik"
  ]
  node [
    id 475
    label "arsa"
  ]
  node [
    id 476
    label "podbicie"
  ]
  node [
    id 477
    label "footing"
  ]
  node [
    id 478
    label "st&#281;p"
  ]
  node [
    id 479
    label "iloczas"
  ]
  node [
    id 480
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 481
    label "pi&#281;ta"
  ]
  node [
    id 482
    label "t&#281;tnica_&#322;ukowata"
  ]
  node [
    id 483
    label "poziom"
  ]
  node [
    id 484
    label "trypodia"
  ]
  node [
    id 485
    label "hi-hat"
  ]
  node [
    id 486
    label "ko&#347;&#263;_sze&#347;cienna"
  ]
  node [
    id 487
    label "&#347;r&#243;dstopie"
  ]
  node [
    id 488
    label "mechanizm"
  ]
  node [
    id 489
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 490
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 491
    label "zmienia&#263;_pozycj&#281;"
  ]
  node [
    id 492
    label "ciecz"
  ]
  node [
    id 493
    label "wype&#322;nia&#263;"
  ]
  node [
    id 494
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 495
    label "sprawdzian"
  ]
  node [
    id 496
    label "set_about"
  ]
  node [
    id 497
    label "nasyca&#263;_si&#281;"
  ]
  node [
    id 498
    label "dociera&#263;"
  ]
  node [
    id 499
    label "wspina&#263;_si&#281;"
  ]
  node [
    id 500
    label "approach"
  ]
  node [
    id 501
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 502
    label "trze&#263;"
  ]
  node [
    id 503
    label "get"
  ]
  node [
    id 504
    label "boost"
  ]
  node [
    id 505
    label "g&#322;adzi&#263;"
  ]
  node [
    id 506
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 507
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 508
    label "silnik"
  ]
  node [
    id 509
    label "dopasowywa&#263;"
  ]
  node [
    id 510
    label "dorabia&#263;"
  ]
  node [
    id 511
    label "dopuszcza&#263;_si&#281;"
  ]
  node [
    id 512
    label "obje&#380;d&#380;a&#263;"
  ]
  node [
    id 513
    label "wkr&#281;ca&#263;"
  ]
  node [
    id 514
    label "oszwabia&#263;"
  ]
  node [
    id 515
    label "orzyna&#263;"
  ]
  node [
    id 516
    label "cheat"
  ]
  node [
    id 517
    label "zaczyna&#263;"
  ]
  node [
    id 518
    label "submit"
  ]
  node [
    id 519
    label "wchodzi&#263;"
  ]
  node [
    id 520
    label "pytanie"
  ]
  node [
    id 521
    label "equate"
  ]
  node [
    id 522
    label "tone"
  ]
  node [
    id 523
    label "report"
  ]
  node [
    id 524
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 525
    label "impart"
  ]
  node [
    id 526
    label "ponosi&#263;"
  ]
  node [
    id 527
    label "dolatywa&#263;"
  ]
  node [
    id 528
    label "przesy&#322;ka"
  ]
  node [
    id 529
    label "doznawa&#263;"
  ]
  node [
    id 530
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 531
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 532
    label "dop&#322;ywa&#263;"
  ]
  node [
    id 533
    label "zachodzi&#263;"
  ]
  node [
    id 534
    label "orgazm"
  ]
  node [
    id 535
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 536
    label "doczeka&#263;"
  ]
  node [
    id 537
    label "osi&#261;ga&#263;"
  ]
  node [
    id 538
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 539
    label "ripen"
  ]
  node [
    id 540
    label "uzyskiwa&#263;"
  ]
  node [
    id 541
    label "claim"
  ]
  node [
    id 542
    label "supervene"
  ]
  node [
    id 543
    label "dokoptowywa&#263;"
  ]
  node [
    id 544
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 545
    label "reach"
  ]
  node [
    id 546
    label "dotyczy&#263;"
  ]
  node [
    id 547
    label "use"
  ]
  node [
    id 548
    label "cz&#281;stowa&#263;"
  ]
  node [
    id 549
    label "poddawa&#263;"
  ]
  node [
    id 550
    label "perform"
  ]
  node [
    id 551
    label "meet"
  ]
  node [
    id 552
    label "do"
  ]
  node [
    id 553
    label "umieszcza&#263;"
  ]
  node [
    id 554
    label "close"
  ]
  node [
    id 555
    label "charge"
  ]
  node [
    id 556
    label "praca_pisemna"
  ]
  node [
    id 557
    label "&#347;ci&#261;ga&#263;"
  ]
  node [
    id 558
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 559
    label "examination"
  ]
  node [
    id 560
    label "pr&#243;ba"
  ]
  node [
    id 561
    label "&#263;wiczenie"
  ]
  node [
    id 562
    label "dydaktyka"
  ]
  node [
    id 563
    label "faza"
  ]
  node [
    id 564
    label "kontrola"
  ]
  node [
    id 565
    label "ciek&#322;y"
  ]
  node [
    id 566
    label "podbiec"
  ]
  node [
    id 567
    label "&#347;ciekni&#281;cie"
  ]
  node [
    id 568
    label "w&#322;oskowato&#347;&#263;"
  ]
  node [
    id 569
    label "&#347;ci&#261;ganie"
  ]
  node [
    id 570
    label "baniak"
  ]
  node [
    id 571
    label "podbiega&#263;"
  ]
  node [
    id 572
    label "nieprzejrzysty"
  ]
  node [
    id 573
    label "wpadanie"
  ]
  node [
    id 574
    label "chlupa&#263;"
  ]
  node [
    id 575
    label "p&#322;ywa&#263;"
  ]
  node [
    id 576
    label "odp&#322;yn&#261;&#263;"
  ]
  node [
    id 577
    label "stan_skupienia"
  ]
  node [
    id 578
    label "mieszalno&#347;&#263;"
  ]
  node [
    id 579
    label "odp&#322;ywanie"
  ]
  node [
    id 580
    label "zachlupa&#263;"
  ]
  node [
    id 581
    label "wpadni&#281;cie"
  ]
  node [
    id 582
    label "cia&#322;o"
  ]
  node [
    id 583
    label "wytoczenie"
  ]
  node [
    id 584
    label "substancja"
  ]
  node [
    id 585
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 586
    label "ukszta&#322;towanie"
  ]
  node [
    id 587
    label "warstwa"
  ]
  node [
    id 588
    label "degree"
  ]
  node [
    id 589
    label "kowad&#322;o"
  ]
  node [
    id 590
    label "rozwini&#281;cie"
  ]
  node [
    id 591
    label "training"
  ]
  node [
    id 592
    label "figuration"
  ]
  node [
    id 593
    label "shape"
  ]
  node [
    id 594
    label "zrobienie"
  ]
  node [
    id 595
    label "zakr&#281;cenie"
  ]
  node [
    id 596
    label "kszta&#322;t"
  ]
  node [
    id 597
    label "czynno&#347;&#263;"
  ]
  node [
    id 598
    label "podwarstwa"
  ]
  node [
    id 599
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 600
    label "p&#322;aszczyzna"
  ]
  node [
    id 601
    label "covering"
  ]
  node [
    id 602
    label "przek&#322;adaniec"
  ]
  node [
    id 603
    label "bitnia"
  ]
  node [
    id 604
    label "m&#322;ot"
  ]
  node [
    id 605
    label "anvil"
  ]
  node [
    id 606
    label "uderzenie"
  ]
  node [
    id 607
    label "tekst"
  ]
  node [
    id 608
    label "nieuprzejmo&#347;&#263;"
  ]
  node [
    id 609
    label "do&#322;eczek"
  ]
  node [
    id 610
    label "wyzwisko"
  ]
  node [
    id 611
    label "indignation"
  ]
  node [
    id 612
    label "krzywda"
  ]
  node [
    id 613
    label "element_anatomiczny"
  ]
  node [
    id 614
    label "niedorobek"
  ]
  node [
    id 615
    label "ubliga"
  ]
  node [
    id 616
    label "wrzuta"
  ]
  node [
    id 617
    label "lico"
  ]
  node [
    id 618
    label "twarz"
  ]
  node [
    id 619
    label "pogorszenie"
  ]
  node [
    id 620
    label "time"
  ]
  node [
    id 621
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 622
    label "spowodowanie"
  ]
  node [
    id 623
    label "d&#378;wi&#281;k"
  ]
  node [
    id 624
    label "odbicie"
  ]
  node [
    id 625
    label "odbicie_si&#281;"
  ]
  node [
    id 626
    label "dotkni&#281;cie"
  ]
  node [
    id 627
    label "zadanie"
  ]
  node [
    id 628
    label "pobicie"
  ]
  node [
    id 629
    label "skrytykowanie"
  ]
  node [
    id 630
    label "instrumentalizacja"
  ]
  node [
    id 631
    label "manewr"
  ]
  node [
    id 632
    label "st&#322;uczenie"
  ]
  node [
    id 633
    label "rush"
  ]
  node [
    id 634
    label "uderzanie"
  ]
  node [
    id 635
    label "walka"
  ]
  node [
    id 636
    label "pogoda"
  ]
  node [
    id 637
    label "stukni&#281;cie"
  ]
  node [
    id 638
    label "&#347;ci&#281;cie"
  ]
  node [
    id 639
    label "dotyk"
  ]
  node [
    id 640
    label "zwr&#243;cenie_uwagi"
  ]
  node [
    id 641
    label "trafienie"
  ]
  node [
    id 642
    label "zagrywka"
  ]
  node [
    id 643
    label "zdarzenie_si&#281;"
  ]
  node [
    id 644
    label "dostanie"
  ]
  node [
    id 645
    label "poczucie"
  ]
  node [
    id 646
    label "reakcja"
  ]
  node [
    id 647
    label "cios"
  ]
  node [
    id 648
    label "stroke"
  ]
  node [
    id 649
    label "contact"
  ]
  node [
    id 650
    label "nast&#261;pienie"
  ]
  node [
    id 651
    label "bat"
  ]
  node [
    id 652
    label "flap"
  ]
  node [
    id 653
    label "wdarcie_si&#281;"
  ]
  node [
    id 654
    label "ruch"
  ]
  node [
    id 655
    label "zwr&#243;cenie_si&#281;"
  ]
  node [
    id 656
    label "dawka"
  ]
  node [
    id 657
    label "coup"
  ]
  node [
    id 658
    label "czyn"
  ]
  node [
    id 659
    label "niegrzeczno&#347;&#263;"
  ]
  node [
    id 660
    label "wyraz_twarzy"
  ]
  node [
    id 661
    label "p&#243;&#322;profil"
  ]
  node [
    id 662
    label "rys"
  ]
  node [
    id 663
    label "brew"
  ]
  node [
    id 664
    label "micha"
  ]
  node [
    id 665
    label "ucho"
  ]
  node [
    id 666
    label "profil"
  ]
  node [
    id 667
    label "podbr&#243;dek"
  ]
  node [
    id 668
    label "sk&#243;ra"
  ]
  node [
    id 669
    label "powierzchnia"
  ]
  node [
    id 670
    label "cera"
  ]
  node [
    id 671
    label "&#347;ciana"
  ]
  node [
    id 672
    label "czo&#322;o"
  ]
  node [
    id 673
    label "maskowato&#347;&#263;"
  ]
  node [
    id 674
    label "powieka"
  ]
  node [
    id 675
    label "nos"
  ]
  node [
    id 676
    label "prz&#243;d"
  ]
  node [
    id 677
    label "zas&#322;ona"
  ]
  node [
    id 678
    label "twarzyczka"
  ]
  node [
    id 679
    label "pysk"
  ]
  node [
    id 680
    label "liczko"
  ]
  node [
    id 681
    label "usta"
  ]
  node [
    id 682
    label "p&#322;e&#263;"
  ]
  node [
    id 683
    label "obelga"
  ]
  node [
    id 684
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 685
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 686
    label "posta&#263;"
  ]
  node [
    id 687
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 688
    label "uj&#281;cie"
  ]
  node [
    id 689
    label "wielko&#347;&#263;"
  ]
  node [
    id 690
    label "reputacja"
  ]
  node [
    id 691
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 692
    label "przedstawiciel"
  ]
  node [
    id 693
    label "rezultat"
  ]
  node [
    id 694
    label "niepowodzenie"
  ]
  node [
    id 695
    label "strata"
  ]
  node [
    id 696
    label "nieszcz&#281;&#347;cie"
  ]
  node [
    id 697
    label "bias"
  ]
  node [
    id 698
    label "cholera"
  ]
  node [
    id 699
    label "bluzg"
  ]
  node [
    id 700
    label "pies"
  ]
  node [
    id 701
    label "wypowied&#378;"
  ]
  node [
    id 702
    label "chujowy"
  ]
  node [
    id 703
    label "chuj"
  ]
  node [
    id 704
    label "szmata"
  ]
  node [
    id 705
    label "pisa&#263;"
  ]
  node [
    id 706
    label "j&#281;zykowo"
  ]
  node [
    id 707
    label "redakcja"
  ]
  node [
    id 708
    label "preparacja"
  ]
  node [
    id 709
    label "dzie&#322;o"
  ]
  node [
    id 710
    label "wytw&#243;r"
  ]
  node [
    id 711
    label "odmianka"
  ]
  node [
    id 712
    label "opu&#347;ci&#263;"
  ]
  node [
    id 713
    label "pomini&#281;cie"
  ]
  node [
    id 714
    label "koniektura"
  ]
  node [
    id 715
    label "ekscerpcja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 3
    target 193
  ]
  edge [
    source 3
    target 194
  ]
  edge [
    source 3
    target 195
  ]
  edge [
    source 3
    target 196
  ]
  edge [
    source 3
    target 197
  ]
  edge [
    source 3
    target 198
  ]
  edge [
    source 3
    target 199
  ]
  edge [
    source 3
    target 200
  ]
  edge [
    source 3
    target 201
  ]
  edge [
    source 3
    target 202
  ]
  edge [
    source 3
    target 203
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 240
  ]
  edge [
    source 4
    target 241
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 464
  ]
  edge [
    source 7
    target 465
  ]
  edge [
    source 7
    target 466
  ]
  edge [
    source 7
    target 467
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 468
  ]
  edge [
    source 7
    target 469
  ]
  edge [
    source 7
    target 470
  ]
  edge [
    source 7
    target 471
  ]
  edge [
    source 7
    target 472
  ]
  edge [
    source 7
    target 473
  ]
  edge [
    source 7
    target 474
  ]
  edge [
    source 7
    target 475
  ]
  edge [
    source 7
    target 476
  ]
  edge [
    source 7
    target 477
  ]
  edge [
    source 7
    target 478
  ]
  edge [
    source 7
    target 479
  ]
  edge [
    source 7
    target 480
  ]
  edge [
    source 7
    target 481
  ]
  edge [
    source 7
    target 482
  ]
  edge [
    source 7
    target 483
  ]
  edge [
    source 7
    target 484
  ]
  edge [
    source 7
    target 485
  ]
  edge [
    source 7
    target 486
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 487
  ]
  edge [
    source 7
    target 488
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 35
  ]
  edge [
    source 8
    target 489
  ]
  edge [
    source 8
    target 490
  ]
  edge [
    source 8
    target 491
  ]
  edge [
    source 8
    target 32
  ]
  edge [
    source 8
    target 492
  ]
  edge [
    source 8
    target 493
  ]
  edge [
    source 8
    target 494
  ]
  edge [
    source 8
    target 495
  ]
  edge [
    source 8
    target 496
  ]
  edge [
    source 8
    target 497
  ]
  edge [
    source 8
    target 498
  ]
  edge [
    source 8
    target 499
  ]
  edge [
    source 8
    target 500
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 501
  ]
  edge [
    source 8
    target 502
  ]
  edge [
    source 8
    target 503
  ]
  edge [
    source 8
    target 504
  ]
  edge [
    source 8
    target 505
  ]
  edge [
    source 8
    target 506
  ]
  edge [
    source 8
    target 507
  ]
  edge [
    source 8
    target 508
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 509
  ]
  edge [
    source 8
    target 510
  ]
  edge [
    source 8
    target 511
  ]
  edge [
    source 8
    target 512
  ]
  edge [
    source 8
    target 513
  ]
  edge [
    source 8
    target 514
  ]
  edge [
    source 8
    target 515
  ]
  edge [
    source 8
    target 516
  ]
  edge [
    source 8
    target 517
  ]
  edge [
    source 8
    target 518
  ]
  edge [
    source 8
    target 519
  ]
  edge [
    source 8
    target 30
  ]
  edge [
    source 8
    target 520
  ]
  edge [
    source 8
    target 521
  ]
  edge [
    source 8
    target 522
  ]
  edge [
    source 8
    target 31
  ]
  edge [
    source 8
    target 523
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 524
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 525
  ]
  edge [
    source 8
    target 27
  ]
  edge [
    source 8
    target 526
  ]
  edge [
    source 8
    target 527
  ]
  edge [
    source 8
    target 528
  ]
  edge [
    source 8
    target 529
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 530
  ]
  edge [
    source 8
    target 531
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 532
  ]
  edge [
    source 8
    target 533
  ]
  edge [
    source 8
    target 534
  ]
  edge [
    source 8
    target 535
  ]
  edge [
    source 8
    target 536
  ]
  edge [
    source 8
    target 537
  ]
  edge [
    source 8
    target 538
  ]
  edge [
    source 8
    target 539
  ]
  edge [
    source 8
    target 540
  ]
  edge [
    source 8
    target 541
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 542
  ]
  edge [
    source 8
    target 543
  ]
  edge [
    source 8
    target 544
  ]
  edge [
    source 8
    target 545
  ]
  edge [
    source 8
    target 546
  ]
  edge [
    source 8
    target 547
  ]
  edge [
    source 8
    target 548
  ]
  edge [
    source 8
    target 549
  ]
  edge [
    source 8
    target 550
  ]
  edge [
    source 8
    target 551
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 552
  ]
  edge [
    source 8
    target 553
  ]
  edge [
    source 8
    target 554
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 555
  ]
  edge [
    source 8
    target 556
  ]
  edge [
    source 8
    target 557
  ]
  edge [
    source 8
    target 558
  ]
  edge [
    source 8
    target 559
  ]
  edge [
    source 8
    target 560
  ]
  edge [
    source 8
    target 561
  ]
  edge [
    source 8
    target 562
  ]
  edge [
    source 8
    target 563
  ]
  edge [
    source 8
    target 564
  ]
  edge [
    source 8
    target 565
  ]
  edge [
    source 8
    target 566
  ]
  edge [
    source 8
    target 567
  ]
  edge [
    source 8
    target 568
  ]
  edge [
    source 8
    target 569
  ]
  edge [
    source 8
    target 570
  ]
  edge [
    source 8
    target 571
  ]
  edge [
    source 8
    target 572
  ]
  edge [
    source 8
    target 573
  ]
  edge [
    source 8
    target 574
  ]
  edge [
    source 8
    target 575
  ]
  edge [
    source 8
    target 576
  ]
  edge [
    source 8
    target 577
  ]
  edge [
    source 8
    target 578
  ]
  edge [
    source 8
    target 579
  ]
  edge [
    source 8
    target 580
  ]
  edge [
    source 8
    target 581
  ]
  edge [
    source 8
    target 582
  ]
  edge [
    source 8
    target 583
  ]
  edge [
    source 8
    target 584
  ]
  edge [
    source 8
    target 585
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 295
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 10
    target 606
  ]
  edge [
    source 10
    target 607
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 609
  ]
  edge [
    source 10
    target 610
  ]
  edge [
    source 10
    target 611
  ]
  edge [
    source 10
    target 612
  ]
  edge [
    source 10
    target 613
  ]
  edge [
    source 10
    target 614
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 616
  ]
  edge [
    source 10
    target 617
  ]
  edge [
    source 10
    target 618
  ]
  edge [
    source 10
    target 619
  ]
  edge [
    source 10
    target 620
  ]
  edge [
    source 10
    target 621
  ]
  edge [
    source 10
    target 622
  ]
  edge [
    source 10
    target 623
  ]
  edge [
    source 10
    target 624
  ]
  edge [
    source 10
    target 625
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 555
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 594
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 10
    target 679
  ]
  edge [
    source 10
    target 680
  ]
  edge [
    source 10
    target 681
  ]
  edge [
    source 10
    target 682
  ]
  edge [
    source 10
    target 683
  ]
  edge [
    source 10
    target 684
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 685
  ]
  edge [
    source 10
    target 686
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 687
  ]
  edge [
    source 10
    target 688
  ]
  edge [
    source 10
    target 689
  ]
  edge [
    source 10
    target 690
  ]
  edge [
    source 10
    target 691
  ]
  edge [
    source 10
    target 692
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 693
  ]
  edge [
    source 10
    target 694
  ]
  edge [
    source 10
    target 695
  ]
  edge [
    source 10
    target 696
  ]
  edge [
    source 10
    target 697
  ]
  edge [
    source 10
    target 698
  ]
  edge [
    source 10
    target 699
  ]
  edge [
    source 10
    target 700
  ]
  edge [
    source 10
    target 701
  ]
  edge [
    source 10
    target 702
  ]
  edge [
    source 10
    target 703
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
]
