graph [
  node [
    id 0
    label "most"
    origin "text"
  ]
  node [
    id 1
    label "&#347;redzkie"
    origin "text"
  ]
  node [
    id 2
    label "rzuci&#263;"
  ]
  node [
    id 3
    label "prz&#281;s&#322;o"
  ]
  node [
    id 4
    label "m&#243;zg"
  ]
  node [
    id 5
    label "trasa"
  ]
  node [
    id 6
    label "jarzmo_mostowe"
  ]
  node [
    id 7
    label "pylon"
  ]
  node [
    id 8
    label "zam&#243;zgowie"
  ]
  node [
    id 9
    label "obiekt_mostowy"
  ]
  node [
    id 10
    label "samoch&#243;d"
  ]
  node [
    id 11
    label "szczelina_dylatacyjna"
  ]
  node [
    id 12
    label "rzucenie"
  ]
  node [
    id 13
    label "bridge"
  ]
  node [
    id 14
    label "rzuca&#263;"
  ]
  node [
    id 15
    label "suwnica"
  ]
  node [
    id 16
    label "porozumienie"
  ]
  node [
    id 17
    label "nap&#281;d"
  ]
  node [
    id 18
    label "urz&#261;dzenie"
  ]
  node [
    id 19
    label "rzucanie"
  ]
  node [
    id 20
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 21
    label "ty&#322;om&#243;zgowie"
  ]
  node [
    id 22
    label "m&#243;&#380;d&#380;ek"
  ]
  node [
    id 23
    label "oliwka"
  ]
  node [
    id 24
    label "substancja_szara"
  ]
  node [
    id 25
    label "wiedza"
  ]
  node [
    id 26
    label "encefalografia"
  ]
  node [
    id 27
    label "przedmurze"
  ]
  node [
    id 28
    label "bruzda"
  ]
  node [
    id 29
    label "cecha"
  ]
  node [
    id 30
    label "g&#322;adkom&#243;zgowie"
  ]
  node [
    id 31
    label "mi&#281;dzym&#243;zgowie"
  ]
  node [
    id 32
    label "przepuklina_m&#243;zgowa"
  ]
  node [
    id 33
    label "podwzg&#243;rze"
  ]
  node [
    id 34
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 35
    label "bezzakr&#281;towo&#347;&#263;"
  ]
  node [
    id 36
    label "wzg&#243;rze"
  ]
  node [
    id 37
    label "g&#322;owa"
  ]
  node [
    id 38
    label "&#347;r&#243;dm&#243;zgowie"
  ]
  node [
    id 39
    label "noosfera"
  ]
  node [
    id 40
    label "elektroencefalogram"
  ]
  node [
    id 41
    label "przodom&#243;zgowie"
  ]
  node [
    id 42
    label "organ"
  ]
  node [
    id 43
    label "cia&#322;o_migda&#322;owate"
  ]
  node [
    id 44
    label "projektodawca"
  ]
  node [
    id 45
    label "przysadka"
  ]
  node [
    id 46
    label "splot_naczyni&#243;wkowy"
  ]
  node [
    id 47
    label "zw&#243;j"
  ]
  node [
    id 48
    label "pie&#324;_m&#243;zgu"
  ]
  node [
    id 49
    label "uk&#322;ad_limbiczny"
  ]
  node [
    id 50
    label "kora_m&#243;zgowa"
  ]
  node [
    id 51
    label "umys&#322;"
  ]
  node [
    id 52
    label "kresom&#243;zgowie"
  ]
  node [
    id 53
    label "poduszka"
  ]
  node [
    id 54
    label "pojazd_drogowy"
  ]
  node [
    id 55
    label "spryskiwacz"
  ]
  node [
    id 56
    label "baga&#380;nik"
  ]
  node [
    id 57
    label "silnik"
  ]
  node [
    id 58
    label "dachowanie"
  ]
  node [
    id 59
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 60
    label "pompa_wodna"
  ]
  node [
    id 61
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 62
    label "poduszka_powietrzna"
  ]
  node [
    id 63
    label "tempomat"
  ]
  node [
    id 64
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 65
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 66
    label "deska_rozdzielcza"
  ]
  node [
    id 67
    label "immobilizer"
  ]
  node [
    id 68
    label "t&#322;umik"
  ]
  node [
    id 69
    label "kierownica"
  ]
  node [
    id 70
    label "ABS"
  ]
  node [
    id 71
    label "bak"
  ]
  node [
    id 72
    label "dwu&#347;lad"
  ]
  node [
    id 73
    label "poci&#261;g_drogowy"
  ]
  node [
    id 74
    label "wycieraczka"
  ]
  node [
    id 75
    label "zawiesie"
  ]
  node [
    id 76
    label "zblocze"
  ]
  node [
    id 77
    label "d&#378;wig"
  ]
  node [
    id 78
    label "communication"
  ]
  node [
    id 79
    label "uk&#322;ad_dayto&#324;ski"
  ]
  node [
    id 80
    label "zgoda"
  ]
  node [
    id 81
    label "z&#322;oty_blok"
  ]
  node [
    id 82
    label "rozstrzygni&#281;cie"
  ]
  node [
    id 83
    label "agent"
  ]
  node [
    id 84
    label "umowa"
  ]
  node [
    id 85
    label "przedmiot"
  ]
  node [
    id 86
    label "kom&#243;rka"
  ]
  node [
    id 87
    label "furnishing"
  ]
  node [
    id 88
    label "zabezpieczenie"
  ]
  node [
    id 89
    label "zrobienie"
  ]
  node [
    id 90
    label "wyrz&#261;dzenie"
  ]
  node [
    id 91
    label "zagospodarowanie"
  ]
  node [
    id 92
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 93
    label "ig&#322;a"
  ]
  node [
    id 94
    label "narz&#281;dzie"
  ]
  node [
    id 95
    label "wirnik"
  ]
  node [
    id 96
    label "aparatura"
  ]
  node [
    id 97
    label "system_energetyczny"
  ]
  node [
    id 98
    label "impulsator"
  ]
  node [
    id 99
    label "mechanizm"
  ]
  node [
    id 100
    label "sprz&#281;t"
  ]
  node [
    id 101
    label "czynno&#347;&#263;"
  ]
  node [
    id 102
    label "blokowanie"
  ]
  node [
    id 103
    label "set"
  ]
  node [
    id 104
    label "zablokowanie"
  ]
  node [
    id 105
    label "przygotowanie"
  ]
  node [
    id 106
    label "komora"
  ]
  node [
    id 107
    label "j&#281;zyk"
  ]
  node [
    id 108
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 109
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 110
    label "droga"
  ]
  node [
    id 111
    label "przebieg"
  ]
  node [
    id 112
    label "infrastruktura"
  ]
  node [
    id 113
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 114
    label "w&#281;ze&#322;"
  ]
  node [
    id 115
    label "marszrutyzacja"
  ]
  node [
    id 116
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 117
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 118
    label "podbieg"
  ]
  node [
    id 119
    label "energia"
  ]
  node [
    id 120
    label "propulsion"
  ]
  node [
    id 121
    label "powodowanie"
  ]
  node [
    id 122
    label "przestawanie"
  ]
  node [
    id 123
    label "poruszanie"
  ]
  node [
    id 124
    label "wrzucanie"
  ]
  node [
    id 125
    label "przerzucanie"
  ]
  node [
    id 126
    label "odchodzenie"
  ]
  node [
    id 127
    label "konstruowanie"
  ]
  node [
    id 128
    label "chow"
  ]
  node [
    id 129
    label "wyzwanie"
  ]
  node [
    id 130
    label "przewracanie"
  ]
  node [
    id 131
    label "odrzucenie"
  ]
  node [
    id 132
    label "przemieszczanie"
  ]
  node [
    id 133
    label "m&#243;wienie"
  ]
  node [
    id 134
    label "opuszczanie"
  ]
  node [
    id 135
    label "odrzucanie"
  ]
  node [
    id 136
    label "wywo&#322;ywanie"
  ]
  node [
    id 137
    label "trafianie"
  ]
  node [
    id 138
    label "&#347;wiat&#322;o"
  ]
  node [
    id 139
    label "rezygnowanie"
  ]
  node [
    id 140
    label "decydowanie"
  ]
  node [
    id 141
    label "podejrzenie"
  ]
  node [
    id 142
    label "zamachiwanie_si&#281;"
  ]
  node [
    id 143
    label "ruszanie"
  ]
  node [
    id 144
    label "czar"
  ]
  node [
    id 145
    label "grzmocenie"
  ]
  node [
    id 146
    label "wyposa&#380;anie"
  ]
  node [
    id 147
    label "oddzia&#322;ywanie"
  ]
  node [
    id 148
    label "narzucanie"
  ]
  node [
    id 149
    label "cie&#324;"
  ]
  node [
    id 150
    label "porzucanie"
  ]
  node [
    id 151
    label "towar"
  ]
  node [
    id 152
    label "opuszcza&#263;"
  ]
  node [
    id 153
    label "porusza&#263;"
  ]
  node [
    id 154
    label "grzmoci&#263;"
  ]
  node [
    id 155
    label "konstruowa&#263;"
  ]
  node [
    id 156
    label "spring"
  ]
  node [
    id 157
    label "rush"
  ]
  node [
    id 158
    label "odchodzi&#263;"
  ]
  node [
    id 159
    label "unwrap"
  ]
  node [
    id 160
    label "rusza&#263;"
  ]
  node [
    id 161
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 162
    label "przestawa&#263;"
  ]
  node [
    id 163
    label "przemieszcza&#263;"
  ]
  node [
    id 164
    label "flip"
  ]
  node [
    id 165
    label "bequeath"
  ]
  node [
    id 166
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 167
    label "przewraca&#263;"
  ]
  node [
    id 168
    label "m&#243;wi&#263;"
  ]
  node [
    id 169
    label "zmienia&#263;"
  ]
  node [
    id 170
    label "syga&#263;"
  ]
  node [
    id 171
    label "tug"
  ]
  node [
    id 172
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 173
    label "konwulsja"
  ]
  node [
    id 174
    label "ruszenie"
  ]
  node [
    id 175
    label "pierdolni&#281;cie"
  ]
  node [
    id 176
    label "poruszenie"
  ]
  node [
    id 177
    label "opuszczenie"
  ]
  node [
    id 178
    label "wywo&#322;anie"
  ]
  node [
    id 179
    label "odej&#347;cie"
  ]
  node [
    id 180
    label "przewr&#243;cenie"
  ]
  node [
    id 181
    label "zamachni&#281;cie_si&#281;"
  ]
  node [
    id 182
    label "skonstruowanie"
  ]
  node [
    id 183
    label "spowodowanie"
  ]
  node [
    id 184
    label "grzmotni&#281;cie"
  ]
  node [
    id 185
    label "zdecydowanie"
  ]
  node [
    id 186
    label "przeznaczenie"
  ]
  node [
    id 187
    label "przemieszczenie"
  ]
  node [
    id 188
    label "wyposa&#380;enie"
  ]
  node [
    id 189
    label "shy"
  ]
  node [
    id 190
    label "oddzia&#322;anie"
  ]
  node [
    id 191
    label "zrezygnowanie"
  ]
  node [
    id 192
    label "porzucenie"
  ]
  node [
    id 193
    label "atak"
  ]
  node [
    id 194
    label "powiedzenie"
  ]
  node [
    id 195
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 196
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 197
    label "przewr&#243;ci&#263;"
  ]
  node [
    id 198
    label "ruszy&#263;"
  ]
  node [
    id 199
    label "powiedzie&#263;"
  ]
  node [
    id 200
    label "majdn&#261;&#263;"
  ]
  node [
    id 201
    label "poruszy&#263;"
  ]
  node [
    id 202
    label "da&#263;"
  ]
  node [
    id 203
    label "peddle"
  ]
  node [
    id 204
    label "pierdoln&#261;&#263;"
  ]
  node [
    id 205
    label "zmieni&#263;"
  ]
  node [
    id 206
    label "bewilder"
  ]
  node [
    id 207
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 208
    label "skonstruowa&#263;"
  ]
  node [
    id 209
    label "sygn&#261;&#263;"
  ]
  node [
    id 210
    label "spowodowa&#263;"
  ]
  node [
    id 211
    label "wywo&#322;a&#263;"
  ]
  node [
    id 212
    label "frame"
  ]
  node [
    id 213
    label "project"
  ]
  node [
    id 214
    label "odej&#347;&#263;"
  ]
  node [
    id 215
    label "zdecydowa&#263;"
  ]
  node [
    id 216
    label "opu&#347;ci&#263;"
  ]
  node [
    id 217
    label "zmieni&#263;_w&#322;a&#347;ciwo&#347;ci_fizyczne_lub_chemiczne_czego&#347;"
  ]
  node [
    id 218
    label "podpora"
  ]
  node [
    id 219
    label "wie&#380;a"
  ]
  node [
    id 220
    label "przegroda"
  ]
  node [
    id 221
    label "odcinek"
  ]
  node [
    id 222
    label "element"
  ]
  node [
    id 223
    label "sklepienie"
  ]
  node [
    id 224
    label "strop"
  ]
  node [
    id 225
    label "element_konstrukcyjny"
  ]
  node [
    id 226
    label "&#346;redzkie"
  ]
  node [
    id 227
    label "drogi"
  ]
  node [
    id 228
    label "krajowy"
  ]
  node [
    id 229
    label "nr"
  ]
  node [
    id 230
    label "94"
  ]
  node [
    id 231
    label "m&#322;yn"
  ]
  node [
    id 232
    label "Le&#347;nica"
  ]
  node [
    id 233
    label "&#346;redzki"
  ]
  node [
    id 234
    label "wschodni"
  ]
  node [
    id 235
    label "zachodny"
  ]
  node [
    id 236
    label "Weistritz"
  ]
  node [
    id 237
    label "Br&#252;cke"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 0
    target 171
  ]
  edge [
    source 0
    target 172
  ]
  edge [
    source 0
    target 173
  ]
  edge [
    source 0
    target 174
  ]
  edge [
    source 0
    target 175
  ]
  edge [
    source 0
    target 176
  ]
  edge [
    source 0
    target 177
  ]
  edge [
    source 0
    target 178
  ]
  edge [
    source 0
    target 179
  ]
  edge [
    source 0
    target 180
  ]
  edge [
    source 0
    target 181
  ]
  edge [
    source 0
    target 182
  ]
  edge [
    source 0
    target 183
  ]
  edge [
    source 0
    target 184
  ]
  edge [
    source 0
    target 185
  ]
  edge [
    source 0
    target 186
  ]
  edge [
    source 0
    target 187
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 0
    target 189
  ]
  edge [
    source 0
    target 190
  ]
  edge [
    source 0
    target 191
  ]
  edge [
    source 0
    target 192
  ]
  edge [
    source 0
    target 193
  ]
  edge [
    source 0
    target 194
  ]
  edge [
    source 0
    target 195
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 197
  ]
  edge [
    source 0
    target 198
  ]
  edge [
    source 0
    target 199
  ]
  edge [
    source 0
    target 200
  ]
  edge [
    source 0
    target 201
  ]
  edge [
    source 0
    target 202
  ]
  edge [
    source 0
    target 203
  ]
  edge [
    source 0
    target 204
  ]
  edge [
    source 0
    target 205
  ]
  edge [
    source 0
    target 206
  ]
  edge [
    source 0
    target 207
  ]
  edge [
    source 0
    target 208
  ]
  edge [
    source 0
    target 209
  ]
  edge [
    source 0
    target 210
  ]
  edge [
    source 0
    target 211
  ]
  edge [
    source 0
    target 212
  ]
  edge [
    source 0
    target 213
  ]
  edge [
    source 0
    target 214
  ]
  edge [
    source 0
    target 215
  ]
  edge [
    source 0
    target 216
  ]
  edge [
    source 0
    target 217
  ]
  edge [
    source 0
    target 218
  ]
  edge [
    source 0
    target 219
  ]
  edge [
    source 0
    target 220
  ]
  edge [
    source 0
    target 221
  ]
  edge [
    source 0
    target 222
  ]
  edge [
    source 0
    target 223
  ]
  edge [
    source 0
    target 224
  ]
  edge [
    source 0
    target 225
  ]
  edge [
    source 0
    target 226
  ]
  edge [
    source 0
    target 233
  ]
  edge [
    source 0
    target 234
  ]
  edge [
    source 0
    target 235
  ]
  edge [
    source 227
    target 228
  ]
  edge [
    source 227
    target 229
  ]
  edge [
    source 227
    target 230
  ]
  edge [
    source 228
    target 229
  ]
  edge [
    source 228
    target 230
  ]
  edge [
    source 229
    target 230
  ]
  edge [
    source 231
    target 232
  ]
  edge [
    source 233
    target 234
  ]
  edge [
    source 233
    target 235
  ]
  edge [
    source 236
    target 237
  ]
]
