graph [
  node [
    id 0
    label "castel"
    origin "text"
  ]
  node [
    id 1
    label "san"
    origin "text"
  ]
  node [
    id 2
    label "pietro"
    origin "text"
  ]
  node [
    id 3
    label "terme"
    origin "text"
  ]
  node [
    id 4
    label "alfabet_grecki"
  ]
  node [
    id 5
    label "litera"
  ]
  node [
    id 6
    label "alfabet"
  ]
  node [
    id 7
    label "znak_pisarski"
  ]
  node [
    id 8
    label "character"
  ]
  node [
    id 9
    label "pismo"
  ]
  node [
    id 10
    label "Castel"
  ]
  node [
    id 11
    label "Pietro"
  ]
  node [
    id 12
    label "Terme"
  ]
  node [
    id 13
    label "Emilia"
  ]
  node [
    id 14
    label "Romania"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
]
