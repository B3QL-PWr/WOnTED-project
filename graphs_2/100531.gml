graph [
  node [
    id 0
    label "bon"
    origin "text"
  ]
  node [
    id 1
    label "komunalny"
    origin "text"
  ]
  node [
    id 2
    label "dokument"
  ]
  node [
    id 3
    label "voucher"
  ]
  node [
    id 4
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 5
    label "zapis"
  ]
  node [
    id 6
    label "&#347;wiadectwo"
  ]
  node [
    id 7
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 8
    label "wytw&#243;r"
  ]
  node [
    id 9
    label "parafa"
  ]
  node [
    id 10
    label "plik"
  ]
  node [
    id 11
    label "raport&#243;wka"
  ]
  node [
    id 12
    label "utw&#243;r"
  ]
  node [
    id 13
    label "record"
  ]
  node [
    id 14
    label "fascyku&#322;"
  ]
  node [
    id 15
    label "dokumentacja"
  ]
  node [
    id 16
    label "registratura"
  ]
  node [
    id 17
    label "artyku&#322;"
  ]
  node [
    id 18
    label "writing"
  ]
  node [
    id 19
    label "sygnatariusz"
  ]
  node [
    id 20
    label "pozwolenie"
  ]
  node [
    id 21
    label "czek"
  ]
  node [
    id 22
    label "za&#347;wiadczenie"
  ]
  node [
    id 23
    label "miejski"
  ]
  node [
    id 24
    label "skomunalizowanie"
  ]
  node [
    id 25
    label "komunalizowanie"
  ]
  node [
    id 26
    label "publiczny"
  ]
  node [
    id 27
    label "typowy"
  ]
  node [
    id 28
    label "miastowy"
  ]
  node [
    id 29
    label "miejsko"
  ]
  node [
    id 30
    label "przejmowanie"
  ]
  node [
    id 31
    label "powodowanie"
  ]
  node [
    id 32
    label "czynno&#347;&#263;"
  ]
  node [
    id 33
    label "spowodowanie"
  ]
  node [
    id 34
    label "przej&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
]
