graph [
  node [
    id 0
    label "zygmunt"
    origin "text"
  ]
  node [
    id 1
    label "hohenzollern"
    origin "text"
  ]
  node [
    id 2
    label "arcybiskup"
    origin "text"
  ]
  node [
    id 3
    label "magdeburski"
    origin "text"
  ]
  node [
    id 4
    label "ordynariusz"
  ]
  node [
    id 5
    label "biskup"
  ]
  node [
    id 6
    label "Zygmunt"
  ]
  node [
    id 7
    label "Hohenzollern"
  ]
  node [
    id 8
    label "Joachima"
  ]
  node [
    id 9
    label "ii"
  ]
  node [
    id 10
    label "i"
  ]
  node [
    id 11
    label "stary"
  ]
  node [
    id 12
    label "Jadwiga"
  ]
  node [
    id 13
    label "Jagiellonka"
  ]
  node [
    id 14
    label "Juliusz"
  ]
  node [
    id 15
    label "iii"
  ]
  node [
    id 16
    label "august"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
]
