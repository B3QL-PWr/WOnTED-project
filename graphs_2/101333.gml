graph [
  node [
    id 0
    label "plan"
    origin "text"
  ]
  node [
    id 1
    label "czteroletni"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "reprezentacja"
  ]
  node [
    id 4
    label "przestrze&#324;"
  ]
  node [
    id 5
    label "intencja"
  ]
  node [
    id 6
    label "punkt"
  ]
  node [
    id 7
    label "perspektywa"
  ]
  node [
    id 8
    label "model"
  ]
  node [
    id 9
    label "miejsce_pracy"
  ]
  node [
    id 10
    label "device"
  ]
  node [
    id 11
    label "obraz"
  ]
  node [
    id 12
    label "rysunek"
  ]
  node [
    id 13
    label "agreement"
  ]
  node [
    id 14
    label "dekoracja"
  ]
  node [
    id 15
    label "wytw&#243;r"
  ]
  node [
    id 16
    label "pomys&#322;"
  ]
  node [
    id 17
    label "grafika"
  ]
  node [
    id 18
    label "ilustracja"
  ]
  node [
    id 19
    label "plastyka"
  ]
  node [
    id 20
    label "kreska"
  ]
  node [
    id 21
    label "shape"
  ]
  node [
    id 22
    label "teka"
  ]
  node [
    id 23
    label "picture"
  ]
  node [
    id 24
    label "kszta&#322;t"
  ]
  node [
    id 25
    label "photograph"
  ]
  node [
    id 26
    label "dru&#380;yna"
  ]
  node [
    id 27
    label "zesp&#243;&#322;"
  ]
  node [
    id 28
    label "deputation"
  ]
  node [
    id 29
    label "emblemat"
  ]
  node [
    id 30
    label "cz&#322;owiek"
  ]
  node [
    id 31
    label "spos&#243;b"
  ]
  node [
    id 32
    label "matryca"
  ]
  node [
    id 33
    label "facet"
  ]
  node [
    id 34
    label "zi&#243;&#322;ko"
  ]
  node [
    id 35
    label "mildew"
  ]
  node [
    id 36
    label "miniatura"
  ]
  node [
    id 37
    label "ideal"
  ]
  node [
    id 38
    label "adaptation"
  ]
  node [
    id 39
    label "typ"
  ]
  node [
    id 40
    label "ruch"
  ]
  node [
    id 41
    label "imitacja"
  ]
  node [
    id 42
    label "pozowa&#263;"
  ]
  node [
    id 43
    label "orygina&#322;"
  ]
  node [
    id 44
    label "wz&#243;r"
  ]
  node [
    id 45
    label "motif"
  ]
  node [
    id 46
    label "prezenter"
  ]
  node [
    id 47
    label "pozowanie"
  ]
  node [
    id 48
    label "oktant"
  ]
  node [
    id 49
    label "przedzielenie"
  ]
  node [
    id 50
    label "przedzieli&#263;"
  ]
  node [
    id 51
    label "zbi&#243;r"
  ]
  node [
    id 52
    label "przestw&#243;r"
  ]
  node [
    id 53
    label "rozdziela&#263;"
  ]
  node [
    id 54
    label "nielito&#347;ciwy"
  ]
  node [
    id 55
    label "czasoprzestrze&#324;"
  ]
  node [
    id 56
    label "miejsce"
  ]
  node [
    id 57
    label "niezmierzony"
  ]
  node [
    id 58
    label "bezbrze&#380;e"
  ]
  node [
    id 59
    label "rozdzielanie"
  ]
  node [
    id 60
    label "projekcja"
  ]
  node [
    id 61
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 62
    label "wypunktowa&#263;"
  ]
  node [
    id 63
    label "opinion"
  ]
  node [
    id 64
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 65
    label "inning"
  ]
  node [
    id 66
    label "zaj&#347;cie"
  ]
  node [
    id 67
    label "representation"
  ]
  node [
    id 68
    label "filmoteka"
  ]
  node [
    id 69
    label "widok"
  ]
  node [
    id 70
    label "podobrazie"
  ]
  node [
    id 71
    label "przeplot"
  ]
  node [
    id 72
    label "napisy"
  ]
  node [
    id 73
    label "pogl&#261;d"
  ]
  node [
    id 74
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 75
    label "oprawia&#263;"
  ]
  node [
    id 76
    label "ziarno"
  ]
  node [
    id 77
    label "human_body"
  ]
  node [
    id 78
    label "ostro&#347;&#263;"
  ]
  node [
    id 79
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 80
    label "pulment"
  ]
  node [
    id 81
    label "sztafa&#380;"
  ]
  node [
    id 82
    label "punktowa&#263;"
  ]
  node [
    id 83
    label "scena"
  ]
  node [
    id 84
    label "przedstawienie"
  ]
  node [
    id 85
    label "persona"
  ]
  node [
    id 86
    label "zjawisko"
  ]
  node [
    id 87
    label "malarz"
  ]
  node [
    id 88
    label "oprawianie"
  ]
  node [
    id 89
    label "uj&#281;cie"
  ]
  node [
    id 90
    label "parkiet"
  ]
  node [
    id 91
    label "t&#322;o"
  ]
  node [
    id 92
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 93
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 94
    label "effigy"
  ]
  node [
    id 95
    label "anamorfoza"
  ]
  node [
    id 96
    label "czo&#322;&#243;wka"
  ]
  node [
    id 97
    label "plama_barwna"
  ]
  node [
    id 98
    label "postprodukcja"
  ]
  node [
    id 99
    label "rola"
  ]
  node [
    id 100
    label "ty&#322;&#243;wka"
  ]
  node [
    id 101
    label "thinking"
  ]
  node [
    id 102
    label "ukradzenie"
  ]
  node [
    id 103
    label "pocz&#261;tki"
  ]
  node [
    id 104
    label "idea"
  ]
  node [
    id 105
    label "ukra&#347;&#263;"
  ]
  node [
    id 106
    label "system"
  ]
  node [
    id 107
    label "przedmiot"
  ]
  node [
    id 108
    label "rezultat"
  ]
  node [
    id 109
    label "p&#322;&#243;d"
  ]
  node [
    id 110
    label "work"
  ]
  node [
    id 111
    label "patrze&#263;"
  ]
  node [
    id 112
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 113
    label "anticipation"
  ]
  node [
    id 114
    label "dystans"
  ]
  node [
    id 115
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 116
    label "posta&#263;"
  ]
  node [
    id 117
    label "patrzenie"
  ]
  node [
    id 118
    label "decentracja"
  ]
  node [
    id 119
    label "scene"
  ]
  node [
    id 120
    label "metoda"
  ]
  node [
    id 121
    label "prognoza"
  ]
  node [
    id 122
    label "figura_geometryczna"
  ]
  node [
    id 123
    label "pojmowanie"
  ]
  node [
    id 124
    label "widzie&#263;"
  ]
  node [
    id 125
    label "krajobraz"
  ]
  node [
    id 126
    label "tryb"
  ]
  node [
    id 127
    label "expectation"
  ]
  node [
    id 128
    label "sznurownia"
  ]
  node [
    id 129
    label "upi&#281;kszanie"
  ]
  node [
    id 130
    label "adornment"
  ]
  node [
    id 131
    label "ozdoba"
  ]
  node [
    id 132
    label "pi&#281;kniejszy"
  ]
  node [
    id 133
    label "ferm"
  ]
  node [
    id 134
    label "scenografia"
  ]
  node [
    id 135
    label "plan_zdj&#281;ciowy"
  ]
  node [
    id 136
    label "wystr&#243;j"
  ]
  node [
    id 137
    label "obiekt_matematyczny"
  ]
  node [
    id 138
    label "stopie&#324;_pisma"
  ]
  node [
    id 139
    label "pozycja"
  ]
  node [
    id 140
    label "problemat"
  ]
  node [
    id 141
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 142
    label "obiekt"
  ]
  node [
    id 143
    label "point"
  ]
  node [
    id 144
    label "plamka"
  ]
  node [
    id 145
    label "mark"
  ]
  node [
    id 146
    label "ust&#281;p"
  ]
  node [
    id 147
    label "po&#322;o&#380;enie"
  ]
  node [
    id 148
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 149
    label "kres"
  ]
  node [
    id 150
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 151
    label "chwila"
  ]
  node [
    id 152
    label "podpunkt"
  ]
  node [
    id 153
    label "jednostka"
  ]
  node [
    id 154
    label "sprawa"
  ]
  node [
    id 155
    label "problematyka"
  ]
  node [
    id 156
    label "prosta"
  ]
  node [
    id 157
    label "wojsko"
  ]
  node [
    id 158
    label "zapunktowa&#263;"
  ]
  node [
    id 159
    label "kilkuletni"
  ]
  node [
    id 160
    label "Eugeniusz"
  ]
  node [
    id 161
    label "Kwiatkowski"
  ]
  node [
    id 162
    label "ii"
  ]
  node [
    id 163
    label "RP"
  ]
  node [
    id 164
    label "warszawski"
  ]
  node [
    id 165
    label "okr&#281;g"
  ]
  node [
    id 166
    label "przemys&#322;owy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 164
    target 166
  ]
  edge [
    source 165
    target 166
  ]
]
