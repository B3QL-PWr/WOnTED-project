graph [
  node [
    id 0
    label "opis"
    origin "text"
  ]
  node [
    id 1
    label "muszy"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wypowied&#378;"
  ]
  node [
    id 4
    label "exposition"
  ]
  node [
    id 5
    label "czynno&#347;&#263;"
  ]
  node [
    id 6
    label "obja&#347;nienie"
  ]
  node [
    id 7
    label "activity"
  ]
  node [
    id 8
    label "bezproblemowy"
  ]
  node [
    id 9
    label "wydarzenie"
  ]
  node [
    id 10
    label "explanation"
  ]
  node [
    id 11
    label "remark"
  ]
  node [
    id 12
    label "report"
  ]
  node [
    id 13
    label "zrozumia&#322;y"
  ]
  node [
    id 14
    label "przedstawienie"
  ]
  node [
    id 15
    label "informacja"
  ]
  node [
    id 16
    label "poinformowanie"
  ]
  node [
    id 17
    label "pos&#322;uchanie"
  ]
  node [
    id 18
    label "s&#261;d"
  ]
  node [
    id 19
    label "sparafrazowanie"
  ]
  node [
    id 20
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 21
    label "strawestowa&#263;"
  ]
  node [
    id 22
    label "sparafrazowa&#263;"
  ]
  node [
    id 23
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 24
    label "trawestowa&#263;"
  ]
  node [
    id 25
    label "sformu&#322;owanie"
  ]
  node [
    id 26
    label "parafrazowanie"
  ]
  node [
    id 27
    label "ozdobnik"
  ]
  node [
    id 28
    label "delimitacja"
  ]
  node [
    id 29
    label "parafrazowa&#263;"
  ]
  node [
    id 30
    label "stylizacja"
  ]
  node [
    id 31
    label "komunikat"
  ]
  node [
    id 32
    label "trawestowanie"
  ]
  node [
    id 33
    label "strawestowanie"
  ]
  node [
    id 34
    label "rezultat"
  ]
  node [
    id 35
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 36
    label "mie&#263;_miejsce"
  ]
  node [
    id 37
    label "equal"
  ]
  node [
    id 38
    label "trwa&#263;"
  ]
  node [
    id 39
    label "chodzi&#263;"
  ]
  node [
    id 40
    label "si&#281;ga&#263;"
  ]
  node [
    id 41
    label "stan"
  ]
  node [
    id 42
    label "obecno&#347;&#263;"
  ]
  node [
    id 43
    label "stand"
  ]
  node [
    id 44
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "uczestniczy&#263;"
  ]
  node [
    id 46
    label "participate"
  ]
  node [
    id 47
    label "robi&#263;"
  ]
  node [
    id 48
    label "istnie&#263;"
  ]
  node [
    id 49
    label "pozostawa&#263;"
  ]
  node [
    id 50
    label "zostawa&#263;"
  ]
  node [
    id 51
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 52
    label "adhere"
  ]
  node [
    id 53
    label "compass"
  ]
  node [
    id 54
    label "korzysta&#263;"
  ]
  node [
    id 55
    label "appreciation"
  ]
  node [
    id 56
    label "osi&#261;ga&#263;"
  ]
  node [
    id 57
    label "dociera&#263;"
  ]
  node [
    id 58
    label "get"
  ]
  node [
    id 59
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 60
    label "mierzy&#263;"
  ]
  node [
    id 61
    label "u&#380;ywa&#263;"
  ]
  node [
    id 62
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 63
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 64
    label "exsert"
  ]
  node [
    id 65
    label "being"
  ]
  node [
    id 66
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 67
    label "cecha"
  ]
  node [
    id 68
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 69
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 70
    label "p&#322;ywa&#263;"
  ]
  node [
    id 71
    label "run"
  ]
  node [
    id 72
    label "bangla&#263;"
  ]
  node [
    id 73
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 74
    label "przebiega&#263;"
  ]
  node [
    id 75
    label "wk&#322;ada&#263;"
  ]
  node [
    id 76
    label "proceed"
  ]
  node [
    id 77
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 78
    label "carry"
  ]
  node [
    id 79
    label "bywa&#263;"
  ]
  node [
    id 80
    label "dziama&#263;"
  ]
  node [
    id 81
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 82
    label "stara&#263;_si&#281;"
  ]
  node [
    id 83
    label "para"
  ]
  node [
    id 84
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 85
    label "str&#243;j"
  ]
  node [
    id 86
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 87
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 88
    label "krok"
  ]
  node [
    id 89
    label "tryb"
  ]
  node [
    id 90
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 91
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 92
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 93
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 94
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 95
    label "continue"
  ]
  node [
    id 96
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 97
    label "Ohio"
  ]
  node [
    id 98
    label "wci&#281;cie"
  ]
  node [
    id 99
    label "Nowy_York"
  ]
  node [
    id 100
    label "warstwa"
  ]
  node [
    id 101
    label "samopoczucie"
  ]
  node [
    id 102
    label "Illinois"
  ]
  node [
    id 103
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 104
    label "state"
  ]
  node [
    id 105
    label "Jukatan"
  ]
  node [
    id 106
    label "Kalifornia"
  ]
  node [
    id 107
    label "Wirginia"
  ]
  node [
    id 108
    label "wektor"
  ]
  node [
    id 109
    label "Goa"
  ]
  node [
    id 110
    label "Teksas"
  ]
  node [
    id 111
    label "Waszyngton"
  ]
  node [
    id 112
    label "miejsce"
  ]
  node [
    id 113
    label "Massachusetts"
  ]
  node [
    id 114
    label "Alaska"
  ]
  node [
    id 115
    label "Arakan"
  ]
  node [
    id 116
    label "Hawaje"
  ]
  node [
    id 117
    label "Maryland"
  ]
  node [
    id 118
    label "punkt"
  ]
  node [
    id 119
    label "Michigan"
  ]
  node [
    id 120
    label "Arizona"
  ]
  node [
    id 121
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 122
    label "Georgia"
  ]
  node [
    id 123
    label "poziom"
  ]
  node [
    id 124
    label "Pensylwania"
  ]
  node [
    id 125
    label "shape"
  ]
  node [
    id 126
    label "Luizjana"
  ]
  node [
    id 127
    label "Nowy_Meksyk"
  ]
  node [
    id 128
    label "Alabama"
  ]
  node [
    id 129
    label "ilo&#347;&#263;"
  ]
  node [
    id 130
    label "Kansas"
  ]
  node [
    id 131
    label "Oregon"
  ]
  node [
    id 132
    label "Oklahoma"
  ]
  node [
    id 133
    label "Floryda"
  ]
  node [
    id 134
    label "jednostka_administracyjna"
  ]
  node [
    id 135
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
]
