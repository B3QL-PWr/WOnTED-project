graph [
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 2
    label "aby"
    origin "text"
  ]
  node [
    id 3
    label "&#322;amanie"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "op&#322;atek"
    origin "text"
  ]
  node [
    id 6
    label "w&#322;&#261;cza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "debata"
    origin "text"
  ]
  node [
    id 8
    label "sejmowy"
    origin "text"
  ]
  node [
    id 9
    label "przodkini"
  ]
  node [
    id 10
    label "matka_zast&#281;pcza"
  ]
  node [
    id 11
    label "matczysko"
  ]
  node [
    id 12
    label "rodzice"
  ]
  node [
    id 13
    label "stara"
  ]
  node [
    id 14
    label "macierz"
  ]
  node [
    id 15
    label "rodzic"
  ]
  node [
    id 16
    label "Matka_Boska"
  ]
  node [
    id 17
    label "macocha"
  ]
  node [
    id 18
    label "starzy"
  ]
  node [
    id 19
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 20
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 21
    label "pokolenie"
  ]
  node [
    id 22
    label "wapniaki"
  ]
  node [
    id 23
    label "krewna"
  ]
  node [
    id 24
    label "opiekun"
  ]
  node [
    id 25
    label "wapniak"
  ]
  node [
    id 26
    label "rodzic_chrzestny"
  ]
  node [
    id 27
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 28
    label "matka"
  ]
  node [
    id 29
    label "&#380;ona"
  ]
  node [
    id 30
    label "kobieta"
  ]
  node [
    id 31
    label "partnerka"
  ]
  node [
    id 32
    label "ma&#322;a_ojczyzna"
  ]
  node [
    id 33
    label "matuszka"
  ]
  node [
    id 34
    label "parametryzacja"
  ]
  node [
    id 35
    label "pa&#324;stwo"
  ]
  node [
    id 36
    label "poj&#281;cie"
  ]
  node [
    id 37
    label "mod"
  ]
  node [
    id 38
    label "patriota"
  ]
  node [
    id 39
    label "m&#281;&#380;atka"
  ]
  node [
    id 40
    label "wypowied&#378;"
  ]
  node [
    id 41
    label "solicitation"
  ]
  node [
    id 42
    label "pos&#322;uchanie"
  ]
  node [
    id 43
    label "s&#261;d"
  ]
  node [
    id 44
    label "sparafrazowanie"
  ]
  node [
    id 45
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 46
    label "strawestowa&#263;"
  ]
  node [
    id 47
    label "sparafrazowa&#263;"
  ]
  node [
    id 48
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 49
    label "trawestowa&#263;"
  ]
  node [
    id 50
    label "sformu&#322;owanie"
  ]
  node [
    id 51
    label "parafrazowanie"
  ]
  node [
    id 52
    label "ozdobnik"
  ]
  node [
    id 53
    label "delimitacja"
  ]
  node [
    id 54
    label "parafrazowa&#263;"
  ]
  node [
    id 55
    label "stylizacja"
  ]
  node [
    id 56
    label "komunikat"
  ]
  node [
    id 57
    label "trawestowanie"
  ]
  node [
    id 58
    label "strawestowanie"
  ]
  node [
    id 59
    label "rezultat"
  ]
  node [
    id 60
    label "troch&#281;"
  ]
  node [
    id 61
    label "wy&#322;amywanie"
  ]
  node [
    id 62
    label "ko&#322;o"
  ]
  node [
    id 63
    label "dzielenie"
  ]
  node [
    id 64
    label "powodowanie"
  ]
  node [
    id 65
    label "kszta&#322;towanie"
  ]
  node [
    id 66
    label "b&#243;l"
  ]
  node [
    id 67
    label "spread"
  ]
  node [
    id 68
    label "breakage"
  ]
  node [
    id 69
    label "pokonywanie"
  ]
  node [
    id 70
    label "za&#322;amywanie_si&#281;"
  ]
  node [
    id 71
    label "robienie"
  ]
  node [
    id 72
    label "sk&#322;adanie"
  ]
  node [
    id 73
    label "przygn&#281;bianie"
  ]
  node [
    id 74
    label "misdemeanor"
  ]
  node [
    id 75
    label "czynno&#347;&#263;"
  ]
  node [
    id 76
    label "fabrication"
  ]
  node [
    id 77
    label "porobienie"
  ]
  node [
    id 78
    label "przedmiot"
  ]
  node [
    id 79
    label "bycie"
  ]
  node [
    id 80
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 81
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 82
    label "creation"
  ]
  node [
    id 83
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 84
    label "act"
  ]
  node [
    id 85
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 86
    label "tentegowanie"
  ]
  node [
    id 87
    label "activity"
  ]
  node [
    id 88
    label "bezproblemowy"
  ]
  node [
    id 89
    label "wydarzenie"
  ]
  node [
    id 90
    label "cause"
  ]
  node [
    id 91
    label "causal_agent"
  ]
  node [
    id 92
    label "dzielnik"
  ]
  node [
    id 93
    label "stosowanie"
  ]
  node [
    id 94
    label "liczenie"
  ]
  node [
    id 95
    label "dzielna"
  ]
  node [
    id 96
    label "przeszkoda"
  ]
  node [
    id 97
    label "rozprowadzanie"
  ]
  node [
    id 98
    label "dzia&#322;anie_arytmetyczne"
  ]
  node [
    id 99
    label "wyodr&#281;bnianie"
  ]
  node [
    id 100
    label "rozdawanie"
  ]
  node [
    id 101
    label "contribution"
  ]
  node [
    id 102
    label "division"
  ]
  node [
    id 103
    label "iloraz"
  ]
  node [
    id 104
    label "sk&#322;&#243;canie"
  ]
  node [
    id 105
    label "separation"
  ]
  node [
    id 106
    label "dzielenie_si&#281;"
  ]
  node [
    id 107
    label "niepokojenie"
  ]
  node [
    id 108
    label "gn&#281;bienie_si&#281;"
  ]
  node [
    id 109
    label "znoszenie"
  ]
  node [
    id 110
    label "radzenie_sobie"
  ]
  node [
    id 111
    label "wygranie"
  ]
  node [
    id 112
    label "atakowanie"
  ]
  node [
    id 113
    label "zmierzanie"
  ]
  node [
    id 114
    label "sojourn"
  ]
  node [
    id 115
    label "podbijanie"
  ]
  node [
    id 116
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 117
    label "dawanie"
  ]
  node [
    id 118
    label "przyk&#322;adanie"
  ]
  node [
    id 119
    label "collection"
  ]
  node [
    id 120
    label "gromadzenie"
  ]
  node [
    id 121
    label "zestawianie"
  ]
  node [
    id 122
    label "opracowywanie"
  ]
  node [
    id 123
    label "m&#243;wienie"
  ]
  node [
    id 124
    label "gi&#281;cie"
  ]
  node [
    id 125
    label "formation"
  ]
  node [
    id 126
    label "rozwijanie"
  ]
  node [
    id 127
    label "training"
  ]
  node [
    id 128
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 129
    label "tkliwo&#347;&#263;"
  ]
  node [
    id 130
    label "doznanie"
  ]
  node [
    id 131
    label "irradiacja"
  ]
  node [
    id 132
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 133
    label "cier&#324;"
  ]
  node [
    id 134
    label "toleration"
  ]
  node [
    id 135
    label "m&#281;ka_Pa&#324;ska"
  ]
  node [
    id 136
    label "drzazga"
  ]
  node [
    id 137
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 138
    label "prze&#380;ycie"
  ]
  node [
    id 139
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 140
    label "gang"
  ]
  node [
    id 141
    label "&#322;ama&#263;"
  ]
  node [
    id 142
    label "zabawa"
  ]
  node [
    id 143
    label "obr&#281;cz"
  ]
  node [
    id 144
    label "piasta"
  ]
  node [
    id 145
    label "lap"
  ]
  node [
    id 146
    label "figura_geometryczna"
  ]
  node [
    id 147
    label "sphere"
  ]
  node [
    id 148
    label "grupa"
  ]
  node [
    id 149
    label "o&#347;"
  ]
  node [
    id 150
    label "kolokwium"
  ]
  node [
    id 151
    label "pi"
  ]
  node [
    id 152
    label "zwolnica"
  ]
  node [
    id 153
    label "p&#243;&#322;kole"
  ]
  node [
    id 154
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 155
    label "sejmik"
  ]
  node [
    id 156
    label "pojazd"
  ]
  node [
    id 157
    label "figura_ograniczona"
  ]
  node [
    id 158
    label "whip"
  ]
  node [
    id 159
    label "okr&#261;g"
  ]
  node [
    id 160
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 161
    label "odcinek_ko&#322;a"
  ]
  node [
    id 162
    label "stowarzyszenie"
  ]
  node [
    id 163
    label "podwozie"
  ]
  node [
    id 164
    label "wafelek"
  ]
  node [
    id 165
    label "wigilia"
  ]
  node [
    id 166
    label "spotkanie"
  ]
  node [
    id 167
    label "baton"
  ]
  node [
    id 168
    label "wypiek"
  ]
  node [
    id 169
    label "ciastko"
  ]
  node [
    id 170
    label "wafel"
  ]
  node [
    id 171
    label "doba"
  ]
  node [
    id 172
    label "impreza"
  ]
  node [
    id 173
    label "postnik"
  ]
  node [
    id 174
    label "pasterka"
  ]
  node [
    id 175
    label "kolacja"
  ]
  node [
    id 176
    label "wilia"
  ]
  node [
    id 177
    label "vigil"
  ]
  node [
    id 178
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 179
    label "gathering"
  ]
  node [
    id 180
    label "zawarcie"
  ]
  node [
    id 181
    label "znajomy"
  ]
  node [
    id 182
    label "powitanie"
  ]
  node [
    id 183
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 184
    label "spowodowanie"
  ]
  node [
    id 185
    label "zdarzenie_si&#281;"
  ]
  node [
    id 186
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 187
    label "znalezienie"
  ]
  node [
    id 188
    label "match"
  ]
  node [
    id 189
    label "employment"
  ]
  node [
    id 190
    label "po&#380;egnanie"
  ]
  node [
    id 191
    label "gather"
  ]
  node [
    id 192
    label "spotykanie"
  ]
  node [
    id 193
    label "spotkanie_si&#281;"
  ]
  node [
    id 194
    label "zaczyna&#263;"
  ]
  node [
    id 195
    label "nastawia&#263;"
  ]
  node [
    id 196
    label "get_in_touch"
  ]
  node [
    id 197
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 198
    label "dokoptowywa&#263;"
  ]
  node [
    id 199
    label "ogl&#261;da&#263;"
  ]
  node [
    id 200
    label "uruchamia&#263;"
  ]
  node [
    id 201
    label "involve"
  ]
  node [
    id 202
    label "umieszcza&#263;"
  ]
  node [
    id 203
    label "connect"
  ]
  node [
    id 204
    label "odejmowa&#263;"
  ]
  node [
    id 205
    label "mie&#263;_miejsce"
  ]
  node [
    id 206
    label "bankrupt"
  ]
  node [
    id 207
    label "open"
  ]
  node [
    id 208
    label "set_about"
  ]
  node [
    id 209
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 210
    label "begin"
  ]
  node [
    id 211
    label "post&#281;powa&#263;"
  ]
  node [
    id 212
    label "kapita&#322;"
  ]
  node [
    id 213
    label "plasowa&#263;"
  ]
  node [
    id 214
    label "umie&#347;ci&#263;"
  ]
  node [
    id 215
    label "robi&#263;"
  ]
  node [
    id 216
    label "prze&#322;adowywa&#263;"
  ]
  node [
    id 217
    label "pomieszcza&#263;"
  ]
  node [
    id 218
    label "accommodate"
  ]
  node [
    id 219
    label "zmienia&#263;"
  ]
  node [
    id 220
    label "powodowa&#263;"
  ]
  node [
    id 221
    label "venture"
  ]
  node [
    id 222
    label "wpiernicza&#263;"
  ]
  node [
    id 223
    label "okre&#347;la&#263;"
  ]
  node [
    id 224
    label "notice"
  ]
  node [
    id 225
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 226
    label "styka&#263;_si&#281;"
  ]
  node [
    id 227
    label "pobudowa&#263;"
  ]
  node [
    id 228
    label "z&#322;amanie"
  ]
  node [
    id 229
    label "set"
  ]
  node [
    id 230
    label "kierowa&#263;"
  ]
  node [
    id 231
    label "poumieszcza&#263;"
  ]
  node [
    id 232
    label "narobi&#263;"
  ]
  node [
    id 233
    label "ustawia&#263;"
  ]
  node [
    id 234
    label "sk&#322;ada&#263;"
  ]
  node [
    id 235
    label "poprawia&#263;"
  ]
  node [
    id 236
    label "przyrz&#261;dza&#263;"
  ]
  node [
    id 237
    label "marshal"
  ]
  node [
    id 238
    label "deliver"
  ]
  node [
    id 239
    label "predispose"
  ]
  node [
    id 240
    label "wyznacza&#263;"
  ]
  node [
    id 241
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 242
    label "powyznacza&#263;"
  ]
  node [
    id 243
    label "indicate"
  ]
  node [
    id 244
    label "dokooptowywa&#263;"
  ]
  node [
    id 245
    label "przy&#322;&#261;cza&#263;_si&#281;"
  ]
  node [
    id 246
    label "rozmowa"
  ]
  node [
    id 247
    label "sympozjon"
  ]
  node [
    id 248
    label "conference"
  ]
  node [
    id 249
    label "cisza"
  ]
  node [
    id 250
    label "odpowied&#378;"
  ]
  node [
    id 251
    label "rozhowor"
  ]
  node [
    id 252
    label "discussion"
  ]
  node [
    id 253
    label "esej"
  ]
  node [
    id 254
    label "sympozjarcha"
  ]
  node [
    id 255
    label "zbi&#243;r"
  ]
  node [
    id 256
    label "faza"
  ]
  node [
    id 257
    label "rozrywka"
  ]
  node [
    id 258
    label "symposium"
  ]
  node [
    id 259
    label "przyj&#281;cie"
  ]
  node [
    id 260
    label "utw&#243;r"
  ]
  node [
    id 261
    label "konferencja"
  ]
  node [
    id 262
    label "dyskusja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
]
