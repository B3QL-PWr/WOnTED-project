graph [
  node [
    id 0
    label "festiwal"
    origin "text"
  ]
  node [
    id 1
    label "machinimy"
    origin "text"
  ]
  node [
    id 2
    label "teraz"
    origin "text"
  ]
  node [
    id 3
    label "europa"
    origin "text"
  ]
  node [
    id 4
    label "Przystanek_Woodstock"
  ]
  node [
    id 5
    label "Woodstock"
  ]
  node [
    id 6
    label "Opole"
  ]
  node [
    id 7
    label "Eurowizja"
  ]
  node [
    id 8
    label "Open'er"
  ]
  node [
    id 9
    label "Metalmania"
  ]
  node [
    id 10
    label "impreza"
  ]
  node [
    id 11
    label "Brutal"
  ]
  node [
    id 12
    label "FAMA"
  ]
  node [
    id 13
    label "Interwizja"
  ]
  node [
    id 14
    label "Nowe_Horyzonty"
  ]
  node [
    id 15
    label "impra"
  ]
  node [
    id 16
    label "rozrywka"
  ]
  node [
    id 17
    label "przyj&#281;cie"
  ]
  node [
    id 18
    label "okazja"
  ]
  node [
    id 19
    label "party"
  ]
  node [
    id 20
    label "hipster"
  ]
  node [
    id 21
    label "&#346;wierkle"
  ]
  node [
    id 22
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 23
    label "chwila"
  ]
  node [
    id 24
    label "time"
  ]
  node [
    id 25
    label "czas"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
]
