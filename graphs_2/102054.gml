graph [
  node [
    id 0
    label "oto"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "w&#347;r&#243;d"
    origin "text"
  ]
  node [
    id 3
    label "qmamowc&#243;w"
    origin "text"
  ]
  node [
    id 4
    label "laureat"
    origin "text"
  ]
  node [
    id 5
    label "miesi&#281;czny"
    origin "text"
  ]
  node [
    id 6
    label "wyb&#243;r"
    origin "text"
  ]
  node [
    id 7
    label "konkurs"
    origin "text"
  ]
  node [
    id 8
    label "qmam"
    origin "text"
  ]
  node [
    id 9
    label "media"
    origin "text"
  ]
  node [
    id 10
    label "jooke"
    origin "text"
  ]
  node [
    id 11
    label "zszywka"
    origin "text"
  ]
  node [
    id 12
    label "szczepan"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "mix"
    origin "text"
  ]
  node [
    id 15
    label "szkolny"
    origin "text"
  ]
  node [
    id 16
    label "d&#380;ungla"
    origin "text"
  ]
  node [
    id 17
    label "dobroczynny"
  ]
  node [
    id 18
    label "czw&#243;rka"
  ]
  node [
    id 19
    label "spokojny"
  ]
  node [
    id 20
    label "skuteczny"
  ]
  node [
    id 21
    label "&#347;mieszny"
  ]
  node [
    id 22
    label "mi&#322;y"
  ]
  node [
    id 23
    label "grzeczny"
  ]
  node [
    id 24
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 25
    label "powitanie"
  ]
  node [
    id 26
    label "dobrze"
  ]
  node [
    id 27
    label "ca&#322;y"
  ]
  node [
    id 28
    label "zwrot"
  ]
  node [
    id 29
    label "pomy&#347;lny"
  ]
  node [
    id 30
    label "moralny"
  ]
  node [
    id 31
    label "drogi"
  ]
  node [
    id 32
    label "pozytywny"
  ]
  node [
    id 33
    label "odpowiedni"
  ]
  node [
    id 34
    label "korzystny"
  ]
  node [
    id 35
    label "pos&#322;uszny"
  ]
  node [
    id 36
    label "moralnie"
  ]
  node [
    id 37
    label "warto&#347;ciowy"
  ]
  node [
    id 38
    label "etycznie"
  ]
  node [
    id 39
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 40
    label "nale&#380;ny"
  ]
  node [
    id 41
    label "nale&#380;yty"
  ]
  node [
    id 42
    label "typowy"
  ]
  node [
    id 43
    label "uprawniony"
  ]
  node [
    id 44
    label "zasadniczy"
  ]
  node [
    id 45
    label "stosownie"
  ]
  node [
    id 46
    label "taki"
  ]
  node [
    id 47
    label "charakterystyczny"
  ]
  node [
    id 48
    label "prawdziwy"
  ]
  node [
    id 49
    label "ten"
  ]
  node [
    id 50
    label "pozytywnie"
  ]
  node [
    id 51
    label "fajny"
  ]
  node [
    id 52
    label "dodatnio"
  ]
  node [
    id 53
    label "przyjemny"
  ]
  node [
    id 54
    label "po&#380;&#261;dany"
  ]
  node [
    id 55
    label "niepowa&#380;ny"
  ]
  node [
    id 56
    label "o&#347;mieszanie"
  ]
  node [
    id 57
    label "&#347;miesznie"
  ]
  node [
    id 58
    label "bawny"
  ]
  node [
    id 59
    label "o&#347;mieszenie"
  ]
  node [
    id 60
    label "dziwny"
  ]
  node [
    id 61
    label "nieadekwatny"
  ]
  node [
    id 62
    label "wolny"
  ]
  node [
    id 63
    label "uspokajanie_si&#281;"
  ]
  node [
    id 64
    label "bezproblemowy"
  ]
  node [
    id 65
    label "spokojnie"
  ]
  node [
    id 66
    label "uspokojenie_si&#281;"
  ]
  node [
    id 67
    label "cicho"
  ]
  node [
    id 68
    label "uspokojenie"
  ]
  node [
    id 69
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 70
    label "nietrudny"
  ]
  node [
    id 71
    label "uspokajanie"
  ]
  node [
    id 72
    label "zale&#380;ny"
  ]
  node [
    id 73
    label "uleg&#322;y"
  ]
  node [
    id 74
    label "pos&#322;usznie"
  ]
  node [
    id 75
    label "grzecznie"
  ]
  node [
    id 76
    label "stosowny"
  ]
  node [
    id 77
    label "niewinny"
  ]
  node [
    id 78
    label "konserwatywny"
  ]
  node [
    id 79
    label "nijaki"
  ]
  node [
    id 80
    label "korzystnie"
  ]
  node [
    id 81
    label "drogo"
  ]
  node [
    id 82
    label "cz&#322;owiek"
  ]
  node [
    id 83
    label "bliski"
  ]
  node [
    id 84
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 85
    label "przyjaciel"
  ]
  node [
    id 86
    label "jedyny"
  ]
  node [
    id 87
    label "du&#380;y"
  ]
  node [
    id 88
    label "zdr&#243;w"
  ]
  node [
    id 89
    label "calu&#347;ko"
  ]
  node [
    id 90
    label "kompletny"
  ]
  node [
    id 91
    label "&#380;ywy"
  ]
  node [
    id 92
    label "pe&#322;ny"
  ]
  node [
    id 93
    label "podobny"
  ]
  node [
    id 94
    label "ca&#322;o"
  ]
  node [
    id 95
    label "poskutkowanie"
  ]
  node [
    id 96
    label "sprawny"
  ]
  node [
    id 97
    label "skutecznie"
  ]
  node [
    id 98
    label "skutkowanie"
  ]
  node [
    id 99
    label "pomy&#347;lnie"
  ]
  node [
    id 100
    label "toto-lotek"
  ]
  node [
    id 101
    label "trafienie"
  ]
  node [
    id 102
    label "zbi&#243;r"
  ]
  node [
    id 103
    label "arkusz_drukarski"
  ]
  node [
    id 104
    label "&#322;&#243;dka"
  ]
  node [
    id 105
    label "four"
  ]
  node [
    id 106
    label "&#263;wiartka"
  ]
  node [
    id 107
    label "hotel"
  ]
  node [
    id 108
    label "cyfra"
  ]
  node [
    id 109
    label "pok&#243;j"
  ]
  node [
    id 110
    label "stopie&#324;"
  ]
  node [
    id 111
    label "obiekt"
  ]
  node [
    id 112
    label "minialbum"
  ]
  node [
    id 113
    label "osada"
  ]
  node [
    id 114
    label "p&#322;yta_winylowa"
  ]
  node [
    id 115
    label "blotka"
  ]
  node [
    id 116
    label "zaprz&#281;g"
  ]
  node [
    id 117
    label "przedtrzonowiec"
  ]
  node [
    id 118
    label "punkt"
  ]
  node [
    id 119
    label "turn"
  ]
  node [
    id 120
    label "turning"
  ]
  node [
    id 121
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 122
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 123
    label "skr&#281;t"
  ]
  node [
    id 124
    label "obr&#243;t"
  ]
  node [
    id 125
    label "fraza_czasownikowa"
  ]
  node [
    id 126
    label "jednostka_leksykalna"
  ]
  node [
    id 127
    label "zmiana"
  ]
  node [
    id 128
    label "wyra&#380;enie"
  ]
  node [
    id 129
    label "welcome"
  ]
  node [
    id 130
    label "spotkanie"
  ]
  node [
    id 131
    label "pozdrowienie"
  ]
  node [
    id 132
    label "zwyczaj"
  ]
  node [
    id 133
    label "greeting"
  ]
  node [
    id 134
    label "zdarzony"
  ]
  node [
    id 135
    label "odpowiednio"
  ]
  node [
    id 136
    label "odpowiadanie"
  ]
  node [
    id 137
    label "specjalny"
  ]
  node [
    id 138
    label "kochanek"
  ]
  node [
    id 139
    label "sk&#322;onny"
  ]
  node [
    id 140
    label "wybranek"
  ]
  node [
    id 141
    label "umi&#322;owany"
  ]
  node [
    id 142
    label "przyjemnie"
  ]
  node [
    id 143
    label "mi&#322;o"
  ]
  node [
    id 144
    label "kochanie"
  ]
  node [
    id 145
    label "dyplomata"
  ]
  node [
    id 146
    label "dobroczynnie"
  ]
  node [
    id 147
    label "lepiej"
  ]
  node [
    id 148
    label "wiele"
  ]
  node [
    id 149
    label "spo&#322;eczny"
  ]
  node [
    id 150
    label "zdobywca"
  ]
  node [
    id 151
    label "w&#243;dz"
  ]
  node [
    id 152
    label "zwyci&#281;zca"
  ]
  node [
    id 153
    label "odkrywca"
  ]
  node [
    id 154
    label "podr&#243;&#380;nik"
  ]
  node [
    id 155
    label "kilkudziesi&#281;ciodniowy"
  ]
  node [
    id 156
    label "trzydziestodniowy"
  ]
  node [
    id 157
    label "miesi&#281;cznie"
  ]
  node [
    id 158
    label "jasny"
  ]
  node [
    id 159
    label "o&#347;wietlenie"
  ]
  node [
    id 160
    label "szczery"
  ]
  node [
    id 161
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 162
    label "jasno"
  ]
  node [
    id 163
    label "o&#347;wietlanie"
  ]
  node [
    id 164
    label "przytomny"
  ]
  node [
    id 165
    label "zrozumia&#322;y"
  ]
  node [
    id 166
    label "niezm&#261;cony"
  ]
  node [
    id 167
    label "bia&#322;y"
  ]
  node [
    id 168
    label "klarowny"
  ]
  node [
    id 169
    label "jednoznaczny"
  ]
  node [
    id 170
    label "pogodny"
  ]
  node [
    id 171
    label "ewentualno&#347;&#263;"
  ]
  node [
    id 172
    label "decyzja"
  ]
  node [
    id 173
    label "czynno&#347;&#263;"
  ]
  node [
    id 174
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 175
    label "pick"
  ]
  node [
    id 176
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 177
    label "management"
  ]
  node [
    id 178
    label "resolution"
  ]
  node [
    id 179
    label "wytw&#243;r"
  ]
  node [
    id 180
    label "zdecydowanie"
  ]
  node [
    id 181
    label "dokument"
  ]
  node [
    id 182
    label "integer"
  ]
  node [
    id 183
    label "liczba"
  ]
  node [
    id 184
    label "zlewanie_si&#281;"
  ]
  node [
    id 185
    label "ilo&#347;&#263;"
  ]
  node [
    id 186
    label "uk&#322;ad"
  ]
  node [
    id 187
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 188
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 189
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 190
    label "activity"
  ]
  node [
    id 191
    label "wydarzenie"
  ]
  node [
    id 192
    label "alternatywa"
  ]
  node [
    id 193
    label "casting"
  ]
  node [
    id 194
    label "nab&#243;r"
  ]
  node [
    id 195
    label "Eurowizja"
  ]
  node [
    id 196
    label "eliminacje"
  ]
  node [
    id 197
    label "impreza"
  ]
  node [
    id 198
    label "emulation"
  ]
  node [
    id 199
    label "Interwizja"
  ]
  node [
    id 200
    label "impra"
  ]
  node [
    id 201
    label "rozrywka"
  ]
  node [
    id 202
    label "przyj&#281;cie"
  ]
  node [
    id 203
    label "okazja"
  ]
  node [
    id 204
    label "party"
  ]
  node [
    id 205
    label "recruitment"
  ]
  node [
    id 206
    label "faza"
  ]
  node [
    id 207
    label "runda"
  ]
  node [
    id 208
    label "turniej"
  ]
  node [
    id 209
    label "retirement"
  ]
  node [
    id 210
    label "przes&#322;uchanie"
  ]
  node [
    id 211
    label "w&#281;dkarstwo"
  ]
  node [
    id 212
    label "mass-media"
  ]
  node [
    id 213
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 214
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 215
    label "przekazior"
  ]
  node [
    id 216
    label "uzbrajanie"
  ]
  node [
    id 217
    label "medium"
  ]
  node [
    id 218
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 219
    label "&#347;rodek"
  ]
  node [
    id 220
    label "jasnowidz"
  ]
  node [
    id 221
    label "hipnoza"
  ]
  node [
    id 222
    label "spirytysta"
  ]
  node [
    id 223
    label "otoczenie"
  ]
  node [
    id 224
    label "publikator"
  ]
  node [
    id 225
    label "warunki"
  ]
  node [
    id 226
    label "strona"
  ]
  node [
    id 227
    label "przeka&#378;nik"
  ]
  node [
    id 228
    label "&#347;rodek_przekazu"
  ]
  node [
    id 229
    label "armament"
  ]
  node [
    id 230
    label "arming"
  ]
  node [
    id 231
    label "instalacja"
  ]
  node [
    id 232
    label "wyposa&#380;anie"
  ]
  node [
    id 233
    label "dozbrajanie"
  ]
  node [
    id 234
    label "dozbrojenie"
  ]
  node [
    id 235
    label "montowanie"
  ]
  node [
    id 236
    label "tom"
  ]
  node [
    id 237
    label "zszywacz"
  ]
  node [
    id 238
    label "drucik"
  ]
  node [
    id 239
    label "wire"
  ]
  node [
    id 240
    label "pr&#281;cik"
  ]
  node [
    id 241
    label "b&#281;ben"
  ]
  node [
    id 242
    label "pi&#281;cioksi&#261;g"
  ]
  node [
    id 243
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 244
    label "przyrz&#261;d_biurowy"
  ]
  node [
    id 245
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 246
    label "mie&#263;_miejsce"
  ]
  node [
    id 247
    label "equal"
  ]
  node [
    id 248
    label "trwa&#263;"
  ]
  node [
    id 249
    label "chodzi&#263;"
  ]
  node [
    id 250
    label "si&#281;ga&#263;"
  ]
  node [
    id 251
    label "stan"
  ]
  node [
    id 252
    label "obecno&#347;&#263;"
  ]
  node [
    id 253
    label "stand"
  ]
  node [
    id 254
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 255
    label "uczestniczy&#263;"
  ]
  node [
    id 256
    label "participate"
  ]
  node [
    id 257
    label "robi&#263;"
  ]
  node [
    id 258
    label "istnie&#263;"
  ]
  node [
    id 259
    label "pozostawa&#263;"
  ]
  node [
    id 260
    label "zostawa&#263;"
  ]
  node [
    id 261
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 262
    label "adhere"
  ]
  node [
    id 263
    label "compass"
  ]
  node [
    id 264
    label "korzysta&#263;"
  ]
  node [
    id 265
    label "appreciation"
  ]
  node [
    id 266
    label "osi&#261;ga&#263;"
  ]
  node [
    id 267
    label "dociera&#263;"
  ]
  node [
    id 268
    label "get"
  ]
  node [
    id 269
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 270
    label "mierzy&#263;"
  ]
  node [
    id 271
    label "u&#380;ywa&#263;"
  ]
  node [
    id 272
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 273
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 274
    label "exsert"
  ]
  node [
    id 275
    label "being"
  ]
  node [
    id 276
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 277
    label "cecha"
  ]
  node [
    id 278
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 279
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 280
    label "p&#322;ywa&#263;"
  ]
  node [
    id 281
    label "run"
  ]
  node [
    id 282
    label "bangla&#263;"
  ]
  node [
    id 283
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 284
    label "przebiega&#263;"
  ]
  node [
    id 285
    label "wk&#322;ada&#263;"
  ]
  node [
    id 286
    label "proceed"
  ]
  node [
    id 287
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 288
    label "carry"
  ]
  node [
    id 289
    label "bywa&#263;"
  ]
  node [
    id 290
    label "dziama&#263;"
  ]
  node [
    id 291
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 292
    label "stara&#263;_si&#281;"
  ]
  node [
    id 293
    label "para"
  ]
  node [
    id 294
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 295
    label "str&#243;j"
  ]
  node [
    id 296
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 297
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 298
    label "krok"
  ]
  node [
    id 299
    label "tryb"
  ]
  node [
    id 300
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 301
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 302
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 303
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 304
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 305
    label "continue"
  ]
  node [
    id 306
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 307
    label "Ohio"
  ]
  node [
    id 308
    label "wci&#281;cie"
  ]
  node [
    id 309
    label "Nowy_York"
  ]
  node [
    id 310
    label "warstwa"
  ]
  node [
    id 311
    label "samopoczucie"
  ]
  node [
    id 312
    label "Illinois"
  ]
  node [
    id 313
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 314
    label "state"
  ]
  node [
    id 315
    label "Jukatan"
  ]
  node [
    id 316
    label "Kalifornia"
  ]
  node [
    id 317
    label "Wirginia"
  ]
  node [
    id 318
    label "wektor"
  ]
  node [
    id 319
    label "Teksas"
  ]
  node [
    id 320
    label "Goa"
  ]
  node [
    id 321
    label "Waszyngton"
  ]
  node [
    id 322
    label "miejsce"
  ]
  node [
    id 323
    label "Massachusetts"
  ]
  node [
    id 324
    label "Alaska"
  ]
  node [
    id 325
    label "Arakan"
  ]
  node [
    id 326
    label "Hawaje"
  ]
  node [
    id 327
    label "Maryland"
  ]
  node [
    id 328
    label "Michigan"
  ]
  node [
    id 329
    label "Arizona"
  ]
  node [
    id 330
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 331
    label "Georgia"
  ]
  node [
    id 332
    label "poziom"
  ]
  node [
    id 333
    label "Pensylwania"
  ]
  node [
    id 334
    label "shape"
  ]
  node [
    id 335
    label "Luizjana"
  ]
  node [
    id 336
    label "Nowy_Meksyk"
  ]
  node [
    id 337
    label "Alabama"
  ]
  node [
    id 338
    label "Kansas"
  ]
  node [
    id 339
    label "Oregon"
  ]
  node [
    id 340
    label "Floryda"
  ]
  node [
    id 341
    label "Oklahoma"
  ]
  node [
    id 342
    label "jednostka_administracyjna"
  ]
  node [
    id 343
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 344
    label "szkolnie"
  ]
  node [
    id 345
    label "podstawowy"
  ]
  node [
    id 346
    label "prosty"
  ]
  node [
    id 347
    label "szkoleniowy"
  ]
  node [
    id 348
    label "skromny"
  ]
  node [
    id 349
    label "po_prostu"
  ]
  node [
    id 350
    label "naturalny"
  ]
  node [
    id 351
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 352
    label "rozprostowanie"
  ]
  node [
    id 353
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 354
    label "prosto"
  ]
  node [
    id 355
    label "prostowanie_si&#281;"
  ]
  node [
    id 356
    label "niepozorny"
  ]
  node [
    id 357
    label "cios"
  ]
  node [
    id 358
    label "prostoduszny"
  ]
  node [
    id 359
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 360
    label "naiwny"
  ]
  node [
    id 361
    label "&#322;atwy"
  ]
  node [
    id 362
    label "prostowanie"
  ]
  node [
    id 363
    label "zwyk&#322;y"
  ]
  node [
    id 364
    label "naukowy"
  ]
  node [
    id 365
    label "niezaawansowany"
  ]
  node [
    id 366
    label "najwa&#380;niejszy"
  ]
  node [
    id 367
    label "pocz&#261;tkowy"
  ]
  node [
    id 368
    label "podstawowo"
  ]
  node [
    id 369
    label "forest"
  ]
  node [
    id 370
    label "las"
  ]
  node [
    id 371
    label "skupienie"
  ]
  node [
    id 372
    label "formacja_ro&#347;linna"
  ]
  node [
    id 373
    label "pl&#261;tanina"
  ]
  node [
    id 374
    label "warunek_lokalowy"
  ]
  node [
    id 375
    label "plac"
  ]
  node [
    id 376
    label "location"
  ]
  node [
    id 377
    label "uwaga"
  ]
  node [
    id 378
    label "przestrze&#324;"
  ]
  node [
    id 379
    label "status"
  ]
  node [
    id 380
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 381
    label "chwila"
  ]
  node [
    id 382
    label "cia&#322;o"
  ]
  node [
    id 383
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 384
    label "praca"
  ]
  node [
    id 385
    label "rz&#261;d"
  ]
  node [
    id 386
    label "podszyt"
  ]
  node [
    id 387
    label "dno_lasu"
  ]
  node [
    id 388
    label "nadle&#347;nictwo"
  ]
  node [
    id 389
    label "teren_le&#347;ny"
  ]
  node [
    id 390
    label "zalesienie"
  ]
  node [
    id 391
    label "karczowa&#263;"
  ]
  node [
    id 392
    label "mn&#243;stwo"
  ]
  node [
    id 393
    label "wykarczowa&#263;"
  ]
  node [
    id 394
    label "rewir"
  ]
  node [
    id 395
    label "karczowanie"
  ]
  node [
    id 396
    label "obr&#281;b"
  ]
  node [
    id 397
    label "chody"
  ]
  node [
    id 398
    label "wykarczowanie"
  ]
  node [
    id 399
    label "wiatro&#322;om"
  ]
  node [
    id 400
    label "teren"
  ]
  node [
    id 401
    label "podrost"
  ]
  node [
    id 402
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 403
    label "driada"
  ]
  node [
    id 404
    label "le&#347;nictwo"
  ]
  node [
    id 405
    label "runo"
  ]
  node [
    id 406
    label "maze"
  ]
  node [
    id 407
    label "rozmieszczenie"
  ]
  node [
    id 408
    label "chaos"
  ]
  node [
    id 409
    label "agglomeration"
  ]
  node [
    id 410
    label "po&#347;wi&#281;cenie"
  ]
  node [
    id 411
    label "przegrupowanie"
  ]
  node [
    id 412
    label "spowodowanie"
  ]
  node [
    id 413
    label "congestion"
  ]
  node [
    id 414
    label "zgromadzenie"
  ]
  node [
    id 415
    label "kupienie"
  ]
  node [
    id 416
    label "z&#322;&#261;czenie"
  ]
  node [
    id 417
    label "po&#322;&#261;czenie"
  ]
  node [
    id 418
    label "concentration"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 212
  ]
  edge [
    source 9
    target 213
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 215
  ]
  edge [
    source 9
    target 216
  ]
  edge [
    source 9
    target 217
  ]
  edge [
    source 9
    target 218
  ]
  edge [
    source 9
    target 219
  ]
  edge [
    source 9
    target 220
  ]
  edge [
    source 9
    target 221
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 222
  ]
  edge [
    source 9
    target 223
  ]
  edge [
    source 9
    target 224
  ]
  edge [
    source 9
    target 225
  ]
  edge [
    source 9
    target 226
  ]
  edge [
    source 9
    target 227
  ]
  edge [
    source 9
    target 228
  ]
  edge [
    source 9
    target 229
  ]
  edge [
    source 9
    target 230
  ]
  edge [
    source 9
    target 231
  ]
  edge [
    source 9
    target 232
  ]
  edge [
    source 9
    target 233
  ]
  edge [
    source 9
    target 234
  ]
  edge [
    source 9
    target 235
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 236
  ]
  edge [
    source 11
    target 237
  ]
  edge [
    source 11
    target 238
  ]
  edge [
    source 11
    target 239
  ]
  edge [
    source 11
    target 240
  ]
  edge [
    source 11
    target 241
  ]
  edge [
    source 11
    target 242
  ]
  edge [
    source 11
    target 243
  ]
  edge [
    source 11
    target 244
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 245
  ]
  edge [
    source 13
    target 246
  ]
  edge [
    source 13
    target 247
  ]
  edge [
    source 13
    target 248
  ]
  edge [
    source 13
    target 249
  ]
  edge [
    source 13
    target 250
  ]
  edge [
    source 13
    target 251
  ]
  edge [
    source 13
    target 252
  ]
  edge [
    source 13
    target 253
  ]
  edge [
    source 13
    target 254
  ]
  edge [
    source 13
    target 255
  ]
  edge [
    source 13
    target 256
  ]
  edge [
    source 13
    target 257
  ]
  edge [
    source 13
    target 258
  ]
  edge [
    source 13
    target 259
  ]
  edge [
    source 13
    target 260
  ]
  edge [
    source 13
    target 261
  ]
  edge [
    source 13
    target 262
  ]
  edge [
    source 13
    target 263
  ]
  edge [
    source 13
    target 264
  ]
  edge [
    source 13
    target 265
  ]
  edge [
    source 13
    target 266
  ]
  edge [
    source 13
    target 267
  ]
  edge [
    source 13
    target 268
  ]
  edge [
    source 13
    target 269
  ]
  edge [
    source 13
    target 270
  ]
  edge [
    source 13
    target 271
  ]
  edge [
    source 13
    target 272
  ]
  edge [
    source 13
    target 273
  ]
  edge [
    source 13
    target 274
  ]
  edge [
    source 13
    target 275
  ]
  edge [
    source 13
    target 276
  ]
  edge [
    source 13
    target 277
  ]
  edge [
    source 13
    target 278
  ]
  edge [
    source 13
    target 279
  ]
  edge [
    source 13
    target 280
  ]
  edge [
    source 13
    target 281
  ]
  edge [
    source 13
    target 282
  ]
  edge [
    source 13
    target 283
  ]
  edge [
    source 13
    target 284
  ]
  edge [
    source 13
    target 285
  ]
  edge [
    source 13
    target 286
  ]
  edge [
    source 13
    target 287
  ]
  edge [
    source 13
    target 288
  ]
  edge [
    source 13
    target 289
  ]
  edge [
    source 13
    target 290
  ]
  edge [
    source 13
    target 291
  ]
  edge [
    source 13
    target 292
  ]
  edge [
    source 13
    target 293
  ]
  edge [
    source 13
    target 294
  ]
  edge [
    source 13
    target 295
  ]
  edge [
    source 13
    target 296
  ]
  edge [
    source 13
    target 297
  ]
  edge [
    source 13
    target 298
  ]
  edge [
    source 13
    target 299
  ]
  edge [
    source 13
    target 300
  ]
  edge [
    source 13
    target 301
  ]
  edge [
    source 13
    target 302
  ]
  edge [
    source 13
    target 303
  ]
  edge [
    source 13
    target 304
  ]
  edge [
    source 13
    target 305
  ]
  edge [
    source 13
    target 306
  ]
  edge [
    source 13
    target 307
  ]
  edge [
    source 13
    target 308
  ]
  edge [
    source 13
    target 309
  ]
  edge [
    source 13
    target 310
  ]
  edge [
    source 13
    target 311
  ]
  edge [
    source 13
    target 312
  ]
  edge [
    source 13
    target 313
  ]
  edge [
    source 13
    target 314
  ]
  edge [
    source 13
    target 315
  ]
  edge [
    source 13
    target 316
  ]
  edge [
    source 13
    target 317
  ]
  edge [
    source 13
    target 318
  ]
  edge [
    source 13
    target 319
  ]
  edge [
    source 13
    target 320
  ]
  edge [
    source 13
    target 321
  ]
  edge [
    source 13
    target 322
  ]
  edge [
    source 13
    target 323
  ]
  edge [
    source 13
    target 324
  ]
  edge [
    source 13
    target 325
  ]
  edge [
    source 13
    target 326
  ]
  edge [
    source 13
    target 327
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 328
  ]
  edge [
    source 13
    target 329
  ]
  edge [
    source 13
    target 330
  ]
  edge [
    source 13
    target 331
  ]
  edge [
    source 13
    target 332
  ]
  edge [
    source 13
    target 333
  ]
  edge [
    source 13
    target 334
  ]
  edge [
    source 13
    target 335
  ]
  edge [
    source 13
    target 336
  ]
  edge [
    source 13
    target 337
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 338
  ]
  edge [
    source 13
    target 339
  ]
  edge [
    source 13
    target 340
  ]
  edge [
    source 13
    target 341
  ]
  edge [
    source 13
    target 342
  ]
  edge [
    source 13
    target 343
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 344
  ]
  edge [
    source 15
    target 345
  ]
  edge [
    source 15
    target 346
  ]
  edge [
    source 15
    target 347
  ]
  edge [
    source 15
    target 348
  ]
  edge [
    source 15
    target 349
  ]
  edge [
    source 15
    target 350
  ]
  edge [
    source 15
    target 351
  ]
  edge [
    source 15
    target 352
  ]
  edge [
    source 15
    target 353
  ]
  edge [
    source 15
    target 354
  ]
  edge [
    source 15
    target 355
  ]
  edge [
    source 15
    target 356
  ]
  edge [
    source 15
    target 357
  ]
  edge [
    source 15
    target 358
  ]
  edge [
    source 15
    target 359
  ]
  edge [
    source 15
    target 360
  ]
  edge [
    source 15
    target 361
  ]
  edge [
    source 15
    target 362
  ]
  edge [
    source 15
    target 363
  ]
  edge [
    source 15
    target 364
  ]
  edge [
    source 15
    target 365
  ]
  edge [
    source 15
    target 366
  ]
  edge [
    source 15
    target 367
  ]
  edge [
    source 15
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 322
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 277
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 394
  ]
  edge [
    source 16
    target 395
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 397
  ]
  edge [
    source 16
    target 398
  ]
  edge [
    source 16
    target 399
  ]
  edge [
    source 16
    target 400
  ]
  edge [
    source 16
    target 401
  ]
  edge [
    source 16
    target 402
  ]
  edge [
    source 16
    target 403
  ]
  edge [
    source 16
    target 404
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 406
  ]
  edge [
    source 16
    target 407
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 412
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 16
    target 418
  ]
]
