graph [
  node [
    id 0
    label "warto&#347;&#263;"
    origin "text"
  ]
  node [
    id 1
    label "dzia&#322;ka"
    origin "text"
  ]
  node [
    id 2
    label "legalizacyjny"
    origin "text"
  ]
  node [
    id 3
    label "umowny"
    origin "text"
  ]
  node [
    id 4
    label "wyrazi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "jednostka"
    origin "text"
  ]
  node [
    id 6
    label "miara"
    origin "text"
  ]
  node [
    id 7
    label "masa"
    origin "text"
  ]
  node [
    id 8
    label "stosowany"
    origin "text"
  ]
  node [
    id 9
    label "badanie"
    origin "text"
  ]
  node [
    id 10
    label "kontrola"
    origin "text"
  ]
  node [
    id 11
    label "klasyfikacja"
    origin "text"
  ]
  node [
    id 12
    label "waga"
    origin "text"
  ]
  node [
    id 13
    label "rozmiar"
  ]
  node [
    id 14
    label "zrewaluowa&#263;"
  ]
  node [
    id 15
    label "zmienna"
  ]
  node [
    id 16
    label "wskazywanie"
  ]
  node [
    id 17
    label "rewaluowanie"
  ]
  node [
    id 18
    label "cel"
  ]
  node [
    id 19
    label "wskazywa&#263;"
  ]
  node [
    id 20
    label "korzy&#347;&#263;"
  ]
  node [
    id 21
    label "poj&#281;cie"
  ]
  node [
    id 22
    label "worth"
  ]
  node [
    id 23
    label "cecha"
  ]
  node [
    id 24
    label "zrewaluowanie"
  ]
  node [
    id 25
    label "rewaluowa&#263;"
  ]
  node [
    id 26
    label "wabik"
  ]
  node [
    id 27
    label "strona"
  ]
  node [
    id 28
    label "pos&#322;uchanie"
  ]
  node [
    id 29
    label "skumanie"
  ]
  node [
    id 30
    label "orientacja"
  ]
  node [
    id 31
    label "wytw&#243;r"
  ]
  node [
    id 32
    label "teoria"
  ]
  node [
    id 33
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 34
    label "clasp"
  ]
  node [
    id 35
    label "przem&#243;wienie"
  ]
  node [
    id 36
    label "forma"
  ]
  node [
    id 37
    label "zorientowanie"
  ]
  node [
    id 38
    label "kartka"
  ]
  node [
    id 39
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 40
    label "logowanie"
  ]
  node [
    id 41
    label "plik"
  ]
  node [
    id 42
    label "s&#261;d"
  ]
  node [
    id 43
    label "adres_internetowy"
  ]
  node [
    id 44
    label "linia"
  ]
  node [
    id 45
    label "serwis_internetowy"
  ]
  node [
    id 46
    label "posta&#263;"
  ]
  node [
    id 47
    label "bok"
  ]
  node [
    id 48
    label "skr&#281;canie"
  ]
  node [
    id 49
    label "skr&#281;ca&#263;"
  ]
  node [
    id 50
    label "orientowanie"
  ]
  node [
    id 51
    label "skr&#281;ci&#263;"
  ]
  node [
    id 52
    label "uj&#281;cie"
  ]
  node [
    id 53
    label "ty&#322;"
  ]
  node [
    id 54
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 55
    label "fragment"
  ]
  node [
    id 56
    label "layout"
  ]
  node [
    id 57
    label "obiekt"
  ]
  node [
    id 58
    label "zorientowa&#263;"
  ]
  node [
    id 59
    label "pagina"
  ]
  node [
    id 60
    label "podmiot"
  ]
  node [
    id 61
    label "g&#243;ra"
  ]
  node [
    id 62
    label "orientowa&#263;"
  ]
  node [
    id 63
    label "voice"
  ]
  node [
    id 64
    label "prz&#243;d"
  ]
  node [
    id 65
    label "internet"
  ]
  node [
    id 66
    label "powierzchnia"
  ]
  node [
    id 67
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 68
    label "skr&#281;cenie"
  ]
  node [
    id 69
    label "charakterystyka"
  ]
  node [
    id 70
    label "m&#322;ot"
  ]
  node [
    id 71
    label "znak"
  ]
  node [
    id 72
    label "drzewo"
  ]
  node [
    id 73
    label "pr&#243;ba"
  ]
  node [
    id 74
    label "attribute"
  ]
  node [
    id 75
    label "marka"
  ]
  node [
    id 76
    label "punkt"
  ]
  node [
    id 77
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 78
    label "miejsce"
  ]
  node [
    id 79
    label "rezultat"
  ]
  node [
    id 80
    label "thing"
  ]
  node [
    id 81
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 82
    label "rzecz"
  ]
  node [
    id 83
    label "warunek_lokalowy"
  ]
  node [
    id 84
    label "liczba"
  ]
  node [
    id 85
    label "circumference"
  ]
  node [
    id 86
    label "odzie&#380;"
  ]
  node [
    id 87
    label "ilo&#347;&#263;"
  ]
  node [
    id 88
    label "znaczenie"
  ]
  node [
    id 89
    label "dymensja"
  ]
  node [
    id 90
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 91
    label "variable"
  ]
  node [
    id 92
    label "wielko&#347;&#263;"
  ]
  node [
    id 93
    label "podniesienie"
  ]
  node [
    id 94
    label "zaleta"
  ]
  node [
    id 95
    label "warto&#347;ciowy"
  ]
  node [
    id 96
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 97
    label "dobro"
  ]
  node [
    id 98
    label "czynnik"
  ]
  node [
    id 99
    label "przedmiot"
  ]
  node [
    id 100
    label "magnes"
  ]
  node [
    id 101
    label "appreciate"
  ]
  node [
    id 102
    label "podnosi&#263;"
  ]
  node [
    id 103
    label "podnie&#347;&#263;"
  ]
  node [
    id 104
    label "przewarto&#347;ciowywanie"
  ]
  node [
    id 105
    label "podnoszenie"
  ]
  node [
    id 106
    label "command"
  ]
  node [
    id 107
    label "wywodzenie"
  ]
  node [
    id 108
    label "wyraz"
  ]
  node [
    id 109
    label "pokierowanie"
  ]
  node [
    id 110
    label "wywiedzenie"
  ]
  node [
    id 111
    label "wybieranie"
  ]
  node [
    id 112
    label "podkre&#347;lanie"
  ]
  node [
    id 113
    label "pokazywanie"
  ]
  node [
    id 114
    label "show"
  ]
  node [
    id 115
    label "assignment"
  ]
  node [
    id 116
    label "t&#322;umaczenie"
  ]
  node [
    id 117
    label "indication"
  ]
  node [
    id 118
    label "podawanie"
  ]
  node [
    id 119
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 120
    label "set"
  ]
  node [
    id 121
    label "podkre&#347;la&#263;"
  ]
  node [
    id 122
    label "by&#263;"
  ]
  node [
    id 123
    label "podawa&#263;"
  ]
  node [
    id 124
    label "pokazywa&#263;"
  ]
  node [
    id 125
    label "wybiera&#263;"
  ]
  node [
    id 126
    label "signify"
  ]
  node [
    id 127
    label "represent"
  ]
  node [
    id 128
    label "indicate"
  ]
  node [
    id 129
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 130
    label "odst&#281;p"
  ]
  node [
    id 131
    label "room"
  ]
  node [
    id 132
    label "podzia&#322;ka"
  ]
  node [
    id 133
    label "obszar"
  ]
  node [
    id 134
    label "kielich"
  ]
  node [
    id 135
    label "dawka"
  ]
  node [
    id 136
    label "dziedzina"
  ]
  node [
    id 137
    label "package"
  ]
  node [
    id 138
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 139
    label "abcug"
  ]
  node [
    id 140
    label "Rzym_Zachodni"
  ]
  node [
    id 141
    label "whole"
  ]
  node [
    id 142
    label "element"
  ]
  node [
    id 143
    label "Rzym_Wschodni"
  ]
  node [
    id 144
    label "urz&#261;dzenie"
  ]
  node [
    id 145
    label "porcja"
  ]
  node [
    id 146
    label "zas&#243;b"
  ]
  node [
    id 147
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 148
    label "sfera"
  ]
  node [
    id 149
    label "zbi&#243;r"
  ]
  node [
    id 150
    label "zakres"
  ]
  node [
    id 151
    label "funkcja"
  ]
  node [
    id 152
    label "bezdro&#380;e"
  ]
  node [
    id 153
    label "poddzia&#322;"
  ]
  node [
    id 154
    label "p&#243;&#322;noc"
  ]
  node [
    id 155
    label "Kosowo"
  ]
  node [
    id 156
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 157
    label "Zab&#322;ocie"
  ]
  node [
    id 158
    label "zach&#243;d"
  ]
  node [
    id 159
    label "po&#322;udnie"
  ]
  node [
    id 160
    label "Pow&#261;zki"
  ]
  node [
    id 161
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 162
    label "Piotrowo"
  ]
  node [
    id 163
    label "Olszanica"
  ]
  node [
    id 164
    label "Ruda_Pabianicka"
  ]
  node [
    id 165
    label "holarktyka"
  ]
  node [
    id 166
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 167
    label "Ludwin&#243;w"
  ]
  node [
    id 168
    label "Arktyka"
  ]
  node [
    id 169
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 170
    label "Zabu&#380;e"
  ]
  node [
    id 171
    label "antroposfera"
  ]
  node [
    id 172
    label "Neogea"
  ]
  node [
    id 173
    label "terytorium"
  ]
  node [
    id 174
    label "Syberia_Zachodnia"
  ]
  node [
    id 175
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 176
    label "pas_planetoid"
  ]
  node [
    id 177
    label "Syberia_Wschodnia"
  ]
  node [
    id 178
    label "Antarktyka"
  ]
  node [
    id 179
    label "Rakowice"
  ]
  node [
    id 180
    label "akrecja"
  ]
  node [
    id 181
    label "wymiar"
  ]
  node [
    id 182
    label "&#321;&#281;g"
  ]
  node [
    id 183
    label "Kresy_Zachodnie"
  ]
  node [
    id 184
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 185
    label "przestrze&#324;"
  ]
  node [
    id 186
    label "wsch&#243;d"
  ]
  node [
    id 187
    label "Notogea"
  ]
  node [
    id 188
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 189
    label "mienie"
  ]
  node [
    id 190
    label "zasta&#322;o&#347;&#263;"
  ]
  node [
    id 191
    label "stan"
  ]
  node [
    id 192
    label "immoblizacja"
  ]
  node [
    id 193
    label "przymiar"
  ]
  node [
    id 194
    label "masztab"
  ]
  node [
    id 195
    label "kreska"
  ]
  node [
    id 196
    label "podzia&#322;"
  ]
  node [
    id 197
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 198
    label "part"
  ]
  node [
    id 199
    label "proporcja"
  ]
  node [
    id 200
    label "zero"
  ]
  node [
    id 201
    label "roztruchan"
  ]
  node [
    id 202
    label "kszta&#322;t"
  ]
  node [
    id 203
    label "naczynie"
  ]
  node [
    id 204
    label "kwiat"
  ]
  node [
    id 205
    label "puch_kielichowy"
  ]
  node [
    id 206
    label "zawarto&#347;&#263;"
  ]
  node [
    id 207
    label "Graal"
  ]
  node [
    id 208
    label "symbolicznie"
  ]
  node [
    id 209
    label "konwencjonalnie"
  ]
  node [
    id 210
    label "okre&#347;lony"
  ]
  node [
    id 211
    label "nieprawdziwy"
  ]
  node [
    id 212
    label "wiadomy"
  ]
  node [
    id 213
    label "nieprawdziwie"
  ]
  node [
    id 214
    label "niezgodny"
  ]
  node [
    id 215
    label "nierzeczywi&#347;cie"
  ]
  node [
    id 216
    label "udawany"
  ]
  node [
    id 217
    label "prawda"
  ]
  node [
    id 218
    label "nieszczery"
  ]
  node [
    id 219
    label "niehistoryczny"
  ]
  node [
    id 220
    label "konwencjonalny"
  ]
  node [
    id 221
    label "obowi&#261;zuj&#261;co"
  ]
  node [
    id 222
    label "tradycyjny"
  ]
  node [
    id 223
    label "bezbarwnie"
  ]
  node [
    id 224
    label "powszechnie"
  ]
  node [
    id 225
    label "modelowo"
  ]
  node [
    id 226
    label "symboliczny"
  ]
  node [
    id 227
    label "ma&#322;o"
  ]
  node [
    id 228
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 229
    label "testify"
  ]
  node [
    id 230
    label "zakomunikowa&#263;"
  ]
  node [
    id 231
    label "oznaczy&#263;"
  ]
  node [
    id 232
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 233
    label "vent"
  ]
  node [
    id 234
    label "wskaza&#263;"
  ]
  node [
    id 235
    label "appoint"
  ]
  node [
    id 236
    label "okre&#347;li&#263;"
  ]
  node [
    id 237
    label "sign"
  ]
  node [
    id 238
    label "ustali&#263;"
  ]
  node [
    id 239
    label "spowodowa&#263;"
  ]
  node [
    id 240
    label "przyswoi&#263;"
  ]
  node [
    id 241
    label "ludzko&#347;&#263;"
  ]
  node [
    id 242
    label "one"
  ]
  node [
    id 243
    label "ewoluowanie"
  ]
  node [
    id 244
    label "supremum"
  ]
  node [
    id 245
    label "skala"
  ]
  node [
    id 246
    label "przyswajanie"
  ]
  node [
    id 247
    label "wyewoluowanie"
  ]
  node [
    id 248
    label "reakcja"
  ]
  node [
    id 249
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 250
    label "przeliczy&#263;"
  ]
  node [
    id 251
    label "wyewoluowa&#263;"
  ]
  node [
    id 252
    label "ewoluowa&#263;"
  ]
  node [
    id 253
    label "matematyka"
  ]
  node [
    id 254
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 255
    label "rzut"
  ]
  node [
    id 256
    label "liczba_naturalna"
  ]
  node [
    id 257
    label "czynnik_biotyczny"
  ]
  node [
    id 258
    label "g&#322;owa"
  ]
  node [
    id 259
    label "figura"
  ]
  node [
    id 260
    label "individual"
  ]
  node [
    id 261
    label "portrecista"
  ]
  node [
    id 262
    label "przyswaja&#263;"
  ]
  node [
    id 263
    label "przyswojenie"
  ]
  node [
    id 264
    label "uk&#322;ad_dziesi&#281;tny"
  ]
  node [
    id 265
    label "profanum"
  ]
  node [
    id 266
    label "mikrokosmos"
  ]
  node [
    id 267
    label "starzenie_si&#281;"
  ]
  node [
    id 268
    label "duch"
  ]
  node [
    id 269
    label "przeliczanie"
  ]
  node [
    id 270
    label "osoba"
  ]
  node [
    id 271
    label "oddzia&#322;ywanie"
  ]
  node [
    id 272
    label "antropochoria"
  ]
  node [
    id 273
    label "homo_sapiens"
  ]
  node [
    id 274
    label "przelicza&#263;"
  ]
  node [
    id 275
    label "infimum"
  ]
  node [
    id 276
    label "przeliczenie"
  ]
  node [
    id 277
    label "co&#347;"
  ]
  node [
    id 278
    label "budynek"
  ]
  node [
    id 279
    label "program"
  ]
  node [
    id 280
    label "Chocho&#322;"
  ]
  node [
    id 281
    label "Herkules_Poirot"
  ]
  node [
    id 282
    label "Edyp"
  ]
  node [
    id 283
    label "parali&#380;owa&#263;"
  ]
  node [
    id 284
    label "Harry_Potter"
  ]
  node [
    id 285
    label "Casanova"
  ]
  node [
    id 286
    label "Zgredek"
  ]
  node [
    id 287
    label "Gargantua"
  ]
  node [
    id 288
    label "Winnetou"
  ]
  node [
    id 289
    label "Kozio&#322;ek_Mato&#322;ek"
  ]
  node [
    id 290
    label "Dulcynea"
  ]
  node [
    id 291
    label "kategoria_gramatyczna"
  ]
  node [
    id 292
    label "person"
  ]
  node [
    id 293
    label "Plastu&#347;"
  ]
  node [
    id 294
    label "Quasimodo"
  ]
  node [
    id 295
    label "Sherlock_Holmes"
  ]
  node [
    id 296
    label "Faust"
  ]
  node [
    id 297
    label "Wallenrod"
  ]
  node [
    id 298
    label "Dwukwiat"
  ]
  node [
    id 299
    label "Don_Juan"
  ]
  node [
    id 300
    label "koniugacja"
  ]
  node [
    id 301
    label "Don_Kiszot"
  ]
  node [
    id 302
    label "Hamlet"
  ]
  node [
    id 303
    label "Werter"
  ]
  node [
    id 304
    label "istota"
  ]
  node [
    id 305
    label "Szwejk"
  ]
  node [
    id 306
    label "integer"
  ]
  node [
    id 307
    label "zlewanie_si&#281;"
  ]
  node [
    id 308
    label "uk&#322;ad"
  ]
  node [
    id 309
    label "liczba_ca&#322;kowita"
  ]
  node [
    id 310
    label "niepodzielno&#347;&#263;"
  ]
  node [
    id 311
    label "pe&#322;ny"
  ]
  node [
    id 312
    label "nierozdzielno&#347;&#263;"
  ]
  node [
    id 313
    label "interwa&#322;"
  ]
  node [
    id 314
    label "struktura"
  ]
  node [
    id 315
    label "dominanta"
  ]
  node [
    id 316
    label "tetrachord"
  ]
  node [
    id 317
    label "scale"
  ]
  node [
    id 318
    label "przedzia&#322;"
  ]
  node [
    id 319
    label "podzakres"
  ]
  node [
    id 320
    label "rejestr"
  ]
  node [
    id 321
    label "subdominanta"
  ]
  node [
    id 322
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 323
    label "cz&#322;owiek"
  ]
  node [
    id 324
    label "odbicie"
  ]
  node [
    id 325
    label "atom"
  ]
  node [
    id 326
    label "przyroda"
  ]
  node [
    id 327
    label "Ziemia"
  ]
  node [
    id 328
    label "kosmos"
  ]
  node [
    id 329
    label "miniatura"
  ]
  node [
    id 330
    label "czyn"
  ]
  node [
    id 331
    label "addytywno&#347;&#263;"
  ]
  node [
    id 332
    label "function"
  ]
  node [
    id 333
    label "zastosowanie"
  ]
  node [
    id 334
    label "funkcjonowanie"
  ]
  node [
    id 335
    label "praca"
  ]
  node [
    id 336
    label "monotoniczno&#347;&#263;"
  ]
  node [
    id 337
    label "powierzanie"
  ]
  node [
    id 338
    label "przeciwdziedzina"
  ]
  node [
    id 339
    label "awansowa&#263;"
  ]
  node [
    id 340
    label "stawia&#263;"
  ]
  node [
    id 341
    label "wakowa&#263;"
  ]
  node [
    id 342
    label "postawi&#263;"
  ]
  node [
    id 343
    label "awansowanie"
  ]
  node [
    id 344
    label "wymienienie"
  ]
  node [
    id 345
    label "przerachowanie"
  ]
  node [
    id 346
    label "skontrolowanie"
  ]
  node [
    id 347
    label "count"
  ]
  node [
    id 348
    label "sprawdza&#263;"
  ]
  node [
    id 349
    label "zmienia&#263;"
  ]
  node [
    id 350
    label "przerachowywa&#263;"
  ]
  node [
    id 351
    label "ograniczenie"
  ]
  node [
    id 352
    label "armia"
  ]
  node [
    id 353
    label "nawr&#243;t_choroby"
  ]
  node [
    id 354
    label "potomstwo"
  ]
  node [
    id 355
    label "odwzorowanie"
  ]
  node [
    id 356
    label "rysunek"
  ]
  node [
    id 357
    label "scene"
  ]
  node [
    id 358
    label "throw"
  ]
  node [
    id 359
    label "float"
  ]
  node [
    id 360
    label "projection"
  ]
  node [
    id 361
    label "injection"
  ]
  node [
    id 362
    label "blow"
  ]
  node [
    id 363
    label "pomys&#322;"
  ]
  node [
    id 364
    label "ruch"
  ]
  node [
    id 365
    label "k&#322;ad"
  ]
  node [
    id 366
    label "mold"
  ]
  node [
    id 367
    label "sprawdzanie"
  ]
  node [
    id 368
    label "zast&#281;powanie"
  ]
  node [
    id 369
    label "przerachowywanie"
  ]
  node [
    id 370
    label "rachunek_operatorowy"
  ]
  node [
    id 371
    label "nauka_matematyczno-przyrodnicza"
  ]
  node [
    id 372
    label "kryptologia"
  ]
  node [
    id 373
    label "logicyzm"
  ]
  node [
    id 374
    label "logika"
  ]
  node [
    id 375
    label "matematyka_czysta"
  ]
  node [
    id 376
    label "forsing"
  ]
  node [
    id 377
    label "modelowanie_matematyczne"
  ]
  node [
    id 378
    label "matma"
  ]
  node [
    id 379
    label "teoria_katastrof"
  ]
  node [
    id 380
    label "kierunek"
  ]
  node [
    id 381
    label "fizyka_matematyczna"
  ]
  node [
    id 382
    label "teoria_graf&#243;w"
  ]
  node [
    id 383
    label "rachunki"
  ]
  node [
    id 384
    label "topologia_algebraiczna"
  ]
  node [
    id 385
    label "matematyka_stosowana"
  ]
  node [
    id 386
    label "sprawdzi&#263;"
  ]
  node [
    id 387
    label "change"
  ]
  node [
    id 388
    label "przerachowa&#263;"
  ]
  node [
    id 389
    label "zmieni&#263;"
  ]
  node [
    id 390
    label "wyszkoli&#263;_si&#281;"
  ]
  node [
    id 391
    label "organizm"
  ]
  node [
    id 392
    label "translate"
  ]
  node [
    id 393
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 394
    label "kultura"
  ]
  node [
    id 395
    label "pobra&#263;"
  ]
  node [
    id 396
    label "thrill"
  ]
  node [
    id 397
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 398
    label "przeobra&#380;a&#263;_si&#281;"
  ]
  node [
    id 399
    label "pobranie"
  ]
  node [
    id 400
    label "wyniesienie"
  ]
  node [
    id 401
    label "wdro&#380;enie_si&#281;"
  ]
  node [
    id 402
    label "assimilation"
  ]
  node [
    id 403
    label "emotion"
  ]
  node [
    id 404
    label "nauczenie_si&#281;"
  ]
  node [
    id 405
    label "zaczerpni&#281;cie"
  ]
  node [
    id 406
    label "mechanizm_obronny"
  ]
  node [
    id 407
    label "convention"
  ]
  node [
    id 408
    label "przeobra&#380;anie_si&#281;"
  ]
  node [
    id 409
    label "przeobra&#380;enie_si&#281;"
  ]
  node [
    id 410
    label "czerpanie"
  ]
  node [
    id 411
    label "uczenie_si&#281;"
  ]
  node [
    id 412
    label "pobieranie"
  ]
  node [
    id 413
    label "acquisition"
  ]
  node [
    id 414
    label "od&#380;ywianie"
  ]
  node [
    id 415
    label "wynoszenie"
  ]
  node [
    id 416
    label "absorption"
  ]
  node [
    id 417
    label "wdra&#380;anie_si&#281;"
  ]
  node [
    id 418
    label "react"
  ]
  node [
    id 419
    label "zachowanie"
  ]
  node [
    id 420
    label "reaction"
  ]
  node [
    id 421
    label "rozmowa"
  ]
  node [
    id 422
    label "response"
  ]
  node [
    id 423
    label "respondent"
  ]
  node [
    id 424
    label "przeobrazi&#263;_si&#281;"
  ]
  node [
    id 425
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 426
    label "treat"
  ]
  node [
    id 427
    label "czerpa&#263;"
  ]
  node [
    id 428
    label "uczy&#263;_si&#281;"
  ]
  node [
    id 429
    label "pobiera&#263;"
  ]
  node [
    id 430
    label "rede"
  ]
  node [
    id 431
    label "pryncypa&#322;"
  ]
  node [
    id 432
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 433
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 434
    label "wiedza"
  ]
  node [
    id 435
    label "kierowa&#263;"
  ]
  node [
    id 436
    label "alkohol"
  ]
  node [
    id 437
    label "zdolno&#347;&#263;"
  ]
  node [
    id 438
    label "&#380;ycie"
  ]
  node [
    id 439
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 440
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 441
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 442
    label "sztuka"
  ]
  node [
    id 443
    label "dekiel"
  ]
  node [
    id 444
    label "ro&#347;lina"
  ]
  node [
    id 445
    label "&#347;ci&#281;cie"
  ]
  node [
    id 446
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 447
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 448
    label "&#347;ci&#281;gno"
  ]
  node [
    id 449
    label "noosfera"
  ]
  node [
    id 450
    label "byd&#322;o"
  ]
  node [
    id 451
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 452
    label "makrocefalia"
  ]
  node [
    id 453
    label "ucho"
  ]
  node [
    id 454
    label "m&#243;zg"
  ]
  node [
    id 455
    label "kierownictwo"
  ]
  node [
    id 456
    label "fryzura"
  ]
  node [
    id 457
    label "umys&#322;"
  ]
  node [
    id 458
    label "cia&#322;o"
  ]
  node [
    id 459
    label "cz&#322;onek"
  ]
  node [
    id 460
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 461
    label "czaszka"
  ]
  node [
    id 462
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 463
    label "powodowanie"
  ]
  node [
    id 464
    label "hipnotyzowanie"
  ]
  node [
    id 465
    label "&#347;lad"
  ]
  node [
    id 466
    label "docieranie"
  ]
  node [
    id 467
    label "natural_process"
  ]
  node [
    id 468
    label "reakcja_chemiczna"
  ]
  node [
    id 469
    label "wdzieranie_si&#281;"
  ]
  node [
    id 470
    label "zjawisko"
  ]
  node [
    id 471
    label "act"
  ]
  node [
    id 472
    label "lobbysta"
  ]
  node [
    id 473
    label "allochoria"
  ]
  node [
    id 474
    label "wygl&#261;d"
  ]
  node [
    id 475
    label "fotograf"
  ]
  node [
    id 476
    label "malarz"
  ]
  node [
    id 477
    label "artysta"
  ]
  node [
    id 478
    label "p&#322;aszczyzna"
  ]
  node [
    id 479
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 480
    label "bierka_szachowa"
  ]
  node [
    id 481
    label "obiekt_matematyczny"
  ]
  node [
    id 482
    label "gestaltyzm"
  ]
  node [
    id 483
    label "styl"
  ]
  node [
    id 484
    label "obraz"
  ]
  node [
    id 485
    label "Osjan"
  ]
  node [
    id 486
    label "d&#378;wi&#281;k"
  ]
  node [
    id 487
    label "character"
  ]
  node [
    id 488
    label "kto&#347;"
  ]
  node [
    id 489
    label "rze&#378;ba"
  ]
  node [
    id 490
    label "stylistyka"
  ]
  node [
    id 491
    label "figure"
  ]
  node [
    id 492
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 493
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 494
    label "antycypacja"
  ]
  node [
    id 495
    label "ornamentyka"
  ]
  node [
    id 496
    label "informacja"
  ]
  node [
    id 497
    label "Aspazja"
  ]
  node [
    id 498
    label "facet"
  ]
  node [
    id 499
    label "popis"
  ]
  node [
    id 500
    label "wiersz"
  ]
  node [
    id 501
    label "kompleksja"
  ]
  node [
    id 502
    label "budowa"
  ]
  node [
    id 503
    label "symetria"
  ]
  node [
    id 504
    label "lingwistyka_kognitywna"
  ]
  node [
    id 505
    label "karta"
  ]
  node [
    id 506
    label "shape"
  ]
  node [
    id 507
    label "podzbi&#243;r"
  ]
  node [
    id 508
    label "przedstawienie"
  ]
  node [
    id 509
    label "point"
  ]
  node [
    id 510
    label "perspektywa"
  ]
  node [
    id 511
    label "piek&#322;o"
  ]
  node [
    id 512
    label "human_body"
  ]
  node [
    id 513
    label "ofiarowywanie"
  ]
  node [
    id 514
    label "sfera_afektywna"
  ]
  node [
    id 515
    label "nekromancja"
  ]
  node [
    id 516
    label "Po&#347;wist"
  ]
  node [
    id 517
    label "podekscytowanie"
  ]
  node [
    id 518
    label "deformowanie"
  ]
  node [
    id 519
    label "sumienie"
  ]
  node [
    id 520
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 521
    label "deformowa&#263;"
  ]
  node [
    id 522
    label "osobowo&#347;&#263;"
  ]
  node [
    id 523
    label "psychika"
  ]
  node [
    id 524
    label "zjawa"
  ]
  node [
    id 525
    label "zmar&#322;y"
  ]
  node [
    id 526
    label "istota_nadprzyrodzona"
  ]
  node [
    id 527
    label "power"
  ]
  node [
    id 528
    label "entity"
  ]
  node [
    id 529
    label "ofiarowywa&#263;"
  ]
  node [
    id 530
    label "oddech"
  ]
  node [
    id 531
    label "seksualno&#347;&#263;"
  ]
  node [
    id 532
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 533
    label "byt"
  ]
  node [
    id 534
    label "si&#322;a"
  ]
  node [
    id 535
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 536
    label "ego"
  ]
  node [
    id 537
    label "ofiarowanie"
  ]
  node [
    id 538
    label "charakter"
  ]
  node [
    id 539
    label "fizjonomia"
  ]
  node [
    id 540
    label "kompleks"
  ]
  node [
    id 541
    label "zapalno&#347;&#263;"
  ]
  node [
    id 542
    label "T&#281;sknica"
  ]
  node [
    id 543
    label "ofiarowa&#263;"
  ]
  node [
    id 544
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 545
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 546
    label "passion"
  ]
  node [
    id 547
    label "proportion"
  ]
  node [
    id 548
    label "continence"
  ]
  node [
    id 549
    label "odwiedziny"
  ]
  node [
    id 550
    label "granica"
  ]
  node [
    id 551
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 552
    label "desygnat"
  ]
  node [
    id 553
    label "circle"
  ]
  node [
    id 554
    label "rzadko&#347;&#263;"
  ]
  node [
    id 555
    label "measure"
  ]
  node [
    id 556
    label "opinia"
  ]
  node [
    id 557
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 558
    label "potencja"
  ]
  node [
    id 559
    label "property"
  ]
  node [
    id 560
    label "go&#347;&#263;"
  ]
  node [
    id 561
    label "pobyt"
  ]
  node [
    id 562
    label "odwiedzalno&#347;&#263;"
  ]
  node [
    id 563
    label "przej&#347;cie"
  ]
  node [
    id 564
    label "kres"
  ]
  node [
    id 565
    label "granica_pa&#324;stwa"
  ]
  node [
    id 566
    label "Ural"
  ]
  node [
    id 567
    label "end"
  ]
  node [
    id 568
    label "pu&#322;ap"
  ]
  node [
    id 569
    label "koniec"
  ]
  node [
    id 570
    label "granice"
  ]
  node [
    id 571
    label "frontier"
  ]
  node [
    id 572
    label "kategoria"
  ]
  node [
    id 573
    label "pierwiastek"
  ]
  node [
    id 574
    label "number"
  ]
  node [
    id 575
    label "grupa"
  ]
  node [
    id 576
    label "kwadrat_magiczny"
  ]
  node [
    id 577
    label "wyra&#380;enie"
  ]
  node [
    id 578
    label "parametr"
  ]
  node [
    id 579
    label "dane"
  ]
  node [
    id 580
    label "enormousness"
  ]
  node [
    id 581
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 582
    label "sum"
  ]
  node [
    id 583
    label "masa_relatywistyczna"
  ]
  node [
    id 584
    label "mass"
  ]
  node [
    id 585
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 586
    label "jednostka_monetarna"
  ]
  node [
    id 587
    label "catfish"
  ]
  node [
    id 588
    label "ryba"
  ]
  node [
    id 589
    label "sumowate"
  ]
  node [
    id 590
    label "Uzbekistan"
  ]
  node [
    id 591
    label "praktyczny"
  ]
  node [
    id 592
    label "racjonalny"
  ]
  node [
    id 593
    label "u&#380;yteczny"
  ]
  node [
    id 594
    label "praktycznie"
  ]
  node [
    id 595
    label "obserwowanie"
  ]
  node [
    id 596
    label "zrecenzowanie"
  ]
  node [
    id 597
    label "analysis"
  ]
  node [
    id 598
    label "rektalny"
  ]
  node [
    id 599
    label "ustalenie"
  ]
  node [
    id 600
    label "macanie"
  ]
  node [
    id 601
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 602
    label "usi&#322;owanie"
  ]
  node [
    id 603
    label "udowadnianie"
  ]
  node [
    id 604
    label "bia&#322;a_niedziela"
  ]
  node [
    id 605
    label "diagnostyka"
  ]
  node [
    id 606
    label "dociekanie"
  ]
  node [
    id 607
    label "penetrowanie"
  ]
  node [
    id 608
    label "czynno&#347;&#263;"
  ]
  node [
    id 609
    label "krytykowanie"
  ]
  node [
    id 610
    label "omawianie"
  ]
  node [
    id 611
    label "ustalanie"
  ]
  node [
    id 612
    label "rozpatrywanie"
  ]
  node [
    id 613
    label "investigation"
  ]
  node [
    id 614
    label "wziernikowanie"
  ]
  node [
    id 615
    label "examination"
  ]
  node [
    id 616
    label "discussion"
  ]
  node [
    id 617
    label "dyskutowanie"
  ]
  node [
    id 618
    label "temat"
  ]
  node [
    id 619
    label "czepianie_si&#281;"
  ]
  node [
    id 620
    label "opiniowanie"
  ]
  node [
    id 621
    label "ocenianie"
  ]
  node [
    id 622
    label "zaopiniowanie"
  ]
  node [
    id 623
    label "przeszukiwanie"
  ]
  node [
    id 624
    label "penetration"
  ]
  node [
    id 625
    label "decyzja"
  ]
  node [
    id 626
    label "umocnienie"
  ]
  node [
    id 627
    label "appointment"
  ]
  node [
    id 628
    label "spowodowanie"
  ]
  node [
    id 629
    label "localization"
  ]
  node [
    id 630
    label "zdecydowanie"
  ]
  node [
    id 631
    label "zrobienie"
  ]
  node [
    id 632
    label "colony"
  ]
  node [
    id 633
    label "robienie"
  ]
  node [
    id 634
    label "colonization"
  ]
  node [
    id 635
    label "decydowanie"
  ]
  node [
    id 636
    label "umacnianie"
  ]
  node [
    id 637
    label "liquidation"
  ]
  node [
    id 638
    label "przemy&#347;liwanie"
  ]
  node [
    id 639
    label "dzia&#322;anie"
  ]
  node [
    id 640
    label "typ"
  ]
  node [
    id 641
    label "event"
  ]
  node [
    id 642
    label "przyczyna"
  ]
  node [
    id 643
    label "legalizacja_ponowna"
  ]
  node [
    id 644
    label "instytucja"
  ]
  node [
    id 645
    label "w&#322;adza"
  ]
  node [
    id 646
    label "perlustracja"
  ]
  node [
    id 647
    label "legalizacja_pierwotna"
  ]
  node [
    id 648
    label "activity"
  ]
  node [
    id 649
    label "bezproblemowy"
  ]
  node [
    id 650
    label "wydarzenie"
  ]
  node [
    id 651
    label "podejmowanie"
  ]
  node [
    id 652
    label "effort"
  ]
  node [
    id 653
    label "staranie_si&#281;"
  ]
  node [
    id 654
    label "essay"
  ]
  node [
    id 655
    label "pod&#322;&#261;czenie"
  ]
  node [
    id 656
    label "redagowanie"
  ]
  node [
    id 657
    label "pod&#322;&#261;czanie"
  ]
  node [
    id 658
    label "przymierzanie"
  ]
  node [
    id 659
    label "przymierzenie"
  ]
  node [
    id 660
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 661
    label "najem"
  ]
  node [
    id 662
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 663
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 664
    label "zak&#322;ad"
  ]
  node [
    id 665
    label "stosunek_pracy"
  ]
  node [
    id 666
    label "benedykty&#324;ski"
  ]
  node [
    id 667
    label "poda&#380;_pracy"
  ]
  node [
    id 668
    label "pracowanie"
  ]
  node [
    id 669
    label "tyrka"
  ]
  node [
    id 670
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 671
    label "zaw&#243;d"
  ]
  node [
    id 672
    label "tynkarski"
  ]
  node [
    id 673
    label "pracowa&#263;"
  ]
  node [
    id 674
    label "zmiana"
  ]
  node [
    id 675
    label "czynnik_produkcji"
  ]
  node [
    id 676
    label "zobowi&#261;zanie"
  ]
  node [
    id 677
    label "siedziba"
  ]
  node [
    id 678
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 679
    label "analizowanie"
  ]
  node [
    id 680
    label "uzasadnianie"
  ]
  node [
    id 681
    label "presentation"
  ]
  node [
    id 682
    label "endoscopy"
  ]
  node [
    id 683
    label "rozmy&#347;lanie"
  ]
  node [
    id 684
    label "quest"
  ]
  node [
    id 685
    label "dop&#322;ywanie"
  ]
  node [
    id 686
    label "examen"
  ]
  node [
    id 687
    label "diagnosis"
  ]
  node [
    id 688
    label "medycyna"
  ]
  node [
    id 689
    label "anamneza"
  ]
  node [
    id 690
    label "dotykanie"
  ]
  node [
    id 691
    label "dr&#243;b"
  ]
  node [
    id 692
    label "pomacanie"
  ]
  node [
    id 693
    label "feel"
  ]
  node [
    id 694
    label "palpation"
  ]
  node [
    id 695
    label "namacanie"
  ]
  node [
    id 696
    label "hodowanie"
  ]
  node [
    id 697
    label "patrzenie"
  ]
  node [
    id 698
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 699
    label "doszukanie_si&#281;"
  ]
  node [
    id 700
    label "dostrzeganie"
  ]
  node [
    id 701
    label "poobserwowanie"
  ]
  node [
    id 702
    label "observation"
  ]
  node [
    id 703
    label "bocianie_gniazdo"
  ]
  node [
    id 704
    label "prawo"
  ]
  node [
    id 705
    label "rz&#261;dzenie"
  ]
  node [
    id 706
    label "panowanie"
  ]
  node [
    id 707
    label "Kreml"
  ]
  node [
    id 708
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 709
    label "wydolno&#347;&#263;"
  ]
  node [
    id 710
    label "rz&#261;d"
  ]
  node [
    id 711
    label "osoba_prawna"
  ]
  node [
    id 712
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 713
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 714
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 715
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 716
    label "biuro"
  ]
  node [
    id 717
    label "organizacja"
  ]
  node [
    id 718
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 719
    label "Fundusze_Unijne"
  ]
  node [
    id 720
    label "zamyka&#263;"
  ]
  node [
    id 721
    label "establishment"
  ]
  node [
    id 722
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 723
    label "urz&#261;d"
  ]
  node [
    id 724
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 725
    label "afiliowa&#263;"
  ]
  node [
    id 726
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 727
    label "standard"
  ]
  node [
    id 728
    label "zamykanie"
  ]
  node [
    id 729
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 730
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 731
    label "division"
  ]
  node [
    id 732
    label "plasowa&#263;_si&#281;"
  ]
  node [
    id 733
    label "uplasowa&#263;_si&#281;"
  ]
  node [
    id 734
    label "plasowanie_si&#281;"
  ]
  node [
    id 735
    label "stopie&#324;"
  ]
  node [
    id 736
    label "kolejno&#347;&#263;"
  ]
  node [
    id 737
    label "uplasowanie_si&#281;"
  ]
  node [
    id 738
    label "competence"
  ]
  node [
    id 739
    label "ocena"
  ]
  node [
    id 740
    label "distribution"
  ]
  node [
    id 741
    label "pogl&#261;d"
  ]
  node [
    id 742
    label "sofcik"
  ]
  node [
    id 743
    label "kryterium"
  ]
  node [
    id 744
    label "appraisal"
  ]
  node [
    id 745
    label "eksdywizja"
  ]
  node [
    id 746
    label "blastogeneza"
  ]
  node [
    id 747
    label "fission"
  ]
  node [
    id 748
    label "p&#322;&#243;d"
  ]
  node [
    id 749
    label "work"
  ]
  node [
    id 750
    label "podstopie&#324;"
  ]
  node [
    id 751
    label "rank"
  ]
  node [
    id 752
    label "minuta"
  ]
  node [
    id 753
    label "wschodek"
  ]
  node [
    id 754
    label "przymiotnik"
  ]
  node [
    id 755
    label "gama"
  ]
  node [
    id 756
    label "schody"
  ]
  node [
    id 757
    label "poziom"
  ]
  node [
    id 758
    label "przys&#322;&#243;wek"
  ]
  node [
    id 759
    label "degree"
  ]
  node [
    id 760
    label "szczebel"
  ]
  node [
    id 761
    label "podn&#243;&#380;ek"
  ]
  node [
    id 762
    label "odwa&#380;nik"
  ]
  node [
    id 763
    label "report"
  ]
  node [
    id 764
    label "&#263;wiczenie"
  ]
  node [
    id 765
    label "pomiar"
  ]
  node [
    id 766
    label "weight"
  ]
  node [
    id 767
    label "zawa&#380;y&#263;"
  ]
  node [
    id 768
    label "zawa&#380;enie"
  ]
  node [
    id 769
    label "load"
  ]
  node [
    id 770
    label "szala"
  ]
  node [
    id 771
    label "j&#281;zyczek_u_wagi"
  ]
  node [
    id 772
    label "type"
  ]
  node [
    id 773
    label "klasa"
  ]
  node [
    id 774
    label "pomiara"
  ]
  node [
    id 775
    label "survey"
  ]
  node [
    id 776
    label "wskazanie"
  ]
  node [
    id 777
    label "dynamometryczny"
  ]
  node [
    id 778
    label "asymilowanie"
  ]
  node [
    id 779
    label "wapniak"
  ]
  node [
    id 780
    label "asymilowa&#263;"
  ]
  node [
    id 781
    label "os&#322;abia&#263;"
  ]
  node [
    id 782
    label "hominid"
  ]
  node [
    id 783
    label "podw&#322;adny"
  ]
  node [
    id 784
    label "os&#322;abianie"
  ]
  node [
    id 785
    label "dwun&#243;g"
  ]
  node [
    id 786
    label "nasada"
  ]
  node [
    id 787
    label "wz&#243;r"
  ]
  node [
    id 788
    label "senior"
  ]
  node [
    id 789
    label "Adam"
  ]
  node [
    id 790
    label "polifag"
  ]
  node [
    id 791
    label "rozwijanie"
  ]
  node [
    id 792
    label "doskonalenie"
  ]
  node [
    id 793
    label "training"
  ]
  node [
    id 794
    label "use"
  ]
  node [
    id 795
    label "po&#263;wiczenie"
  ]
  node [
    id 796
    label "szlifowanie"
  ]
  node [
    id 797
    label "trening"
  ]
  node [
    id 798
    label "doskonalszy"
  ]
  node [
    id 799
    label "poruszanie_si&#281;"
  ]
  node [
    id 800
    label "utw&#243;r"
  ]
  node [
    id 801
    label "zadanie"
  ]
  node [
    id 802
    label "egzercycja"
  ]
  node [
    id 803
    label "obw&#243;d"
  ]
  node [
    id 804
    label "ch&#322;ostanie"
  ]
  node [
    id 805
    label "ulepszanie"
  ]
  node [
    id 806
    label "kom&#243;rka"
  ]
  node [
    id 807
    label "furnishing"
  ]
  node [
    id 808
    label "zabezpieczenie"
  ]
  node [
    id 809
    label "wyrz&#261;dzenie"
  ]
  node [
    id 810
    label "zagospodarowanie"
  ]
  node [
    id 811
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 812
    label "ig&#322;a"
  ]
  node [
    id 813
    label "narz&#281;dzie"
  ]
  node [
    id 814
    label "wirnik"
  ]
  node [
    id 815
    label "aparatura"
  ]
  node [
    id 816
    label "system_energetyczny"
  ]
  node [
    id 817
    label "impulsator"
  ]
  node [
    id 818
    label "mechanizm"
  ]
  node [
    id 819
    label "sprz&#281;t"
  ]
  node [
    id 820
    label "blokowanie"
  ]
  node [
    id 821
    label "zablokowanie"
  ]
  node [
    id 822
    label "przygotowanie"
  ]
  node [
    id 823
    label "komora"
  ]
  node [
    id 824
    label "j&#281;zyk"
  ]
  node [
    id 825
    label "turbospr&#281;&#380;arka"
  ]
  node [
    id 826
    label "umowa"
  ]
  node [
    id 827
    label "cover"
  ]
  node [
    id 828
    label "zdarzenie_si&#281;"
  ]
  node [
    id 829
    label "oddzia&#322;anie"
  ]
  node [
    id 830
    label "ci&#281;&#380;ar"
  ]
  node [
    id 831
    label "weigh"
  ]
  node [
    id 832
    label "oddzia&#322;a&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
  edge [
    source 1
    target 190
  ]
  edge [
    source 1
    target 191
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 192
  ]
  edge [
    source 1
    target 193
  ]
  edge [
    source 1
    target 194
  ]
  edge [
    source 1
    target 195
  ]
  edge [
    source 1
    target 196
  ]
  edge [
    source 1
    target 197
  ]
  edge [
    source 1
    target 198
  ]
  edge [
    source 1
    target 199
  ]
  edge [
    source 1
    target 200
  ]
  edge [
    source 1
    target 201
  ]
  edge [
    source 1
    target 202
  ]
  edge [
    source 1
    target 203
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 204
  ]
  edge [
    source 1
    target 205
  ]
  edge [
    source 1
    target 206
  ]
  edge [
    source 1
    target 207
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 229
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 233
  ]
  edge [
    source 4
    target 234
  ]
  edge [
    source 4
    target 235
  ]
  edge [
    source 4
    target 236
  ]
  edge [
    source 4
    target 237
  ]
  edge [
    source 4
    target 238
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
  edge [
    source 5
    target 365
  ]
  edge [
    source 5
    target 366
  ]
  edge [
    source 5
    target 367
  ]
  edge [
    source 5
    target 368
  ]
  edge [
    source 5
    target 369
  ]
  edge [
    source 5
    target 370
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 371
  ]
  edge [
    source 5
    target 372
  ]
  edge [
    source 5
    target 373
  ]
  edge [
    source 5
    target 374
  ]
  edge [
    source 5
    target 375
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 377
  ]
  edge [
    source 5
    target 378
  ]
  edge [
    source 5
    target 379
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 381
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 383
  ]
  edge [
    source 5
    target 384
  ]
  edge [
    source 5
    target 385
  ]
  edge [
    source 5
    target 386
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 388
  ]
  edge [
    source 5
    target 389
  ]
  edge [
    source 5
    target 390
  ]
  edge [
    source 5
    target 391
  ]
  edge [
    source 5
    target 392
  ]
  edge [
    source 5
    target 393
  ]
  edge [
    source 5
    target 394
  ]
  edge [
    source 5
    target 395
  ]
  edge [
    source 5
    target 396
  ]
  edge [
    source 5
    target 397
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 399
  ]
  edge [
    source 5
    target 400
  ]
  edge [
    source 5
    target 401
  ]
  edge [
    source 5
    target 402
  ]
  edge [
    source 5
    target 403
  ]
  edge [
    source 5
    target 404
  ]
  edge [
    source 5
    target 405
  ]
  edge [
    source 5
    target 406
  ]
  edge [
    source 5
    target 407
  ]
  edge [
    source 5
    target 408
  ]
  edge [
    source 5
    target 409
  ]
  edge [
    source 5
    target 410
  ]
  edge [
    source 5
    target 411
  ]
  edge [
    source 5
    target 412
  ]
  edge [
    source 5
    target 413
  ]
  edge [
    source 5
    target 414
  ]
  edge [
    source 5
    target 415
  ]
  edge [
    source 5
    target 416
  ]
  edge [
    source 5
    target 417
  ]
  edge [
    source 5
    target 418
  ]
  edge [
    source 5
    target 419
  ]
  edge [
    source 5
    target 420
  ]
  edge [
    source 5
    target 421
  ]
  edge [
    source 5
    target 422
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 423
  ]
  edge [
    source 5
    target 424
  ]
  edge [
    source 5
    target 425
  ]
  edge [
    source 5
    target 426
  ]
  edge [
    source 5
    target 427
  ]
  edge [
    source 5
    target 428
  ]
  edge [
    source 5
    target 429
  ]
  edge [
    source 5
    target 430
  ]
  edge [
    source 5
    target 431
  ]
  edge [
    source 5
    target 432
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 433
  ]
  edge [
    source 5
    target 434
  ]
  edge [
    source 5
    target 435
  ]
  edge [
    source 5
    target 436
  ]
  edge [
    source 5
    target 437
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 438
  ]
  edge [
    source 5
    target 439
  ]
  edge [
    source 5
    target 440
  ]
  edge [
    source 5
    target 441
  ]
  edge [
    source 5
    target 442
  ]
  edge [
    source 5
    target 443
  ]
  edge [
    source 5
    target 444
  ]
  edge [
    source 5
    target 445
  ]
  edge [
    source 5
    target 446
  ]
  edge [
    source 5
    target 447
  ]
  edge [
    source 5
    target 448
  ]
  edge [
    source 5
    target 449
  ]
  edge [
    source 5
    target 450
  ]
  edge [
    source 5
    target 451
  ]
  edge [
    source 5
    target 452
  ]
  edge [
    source 5
    target 453
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 454
  ]
  edge [
    source 5
    target 455
  ]
  edge [
    source 5
    target 456
  ]
  edge [
    source 5
    target 457
  ]
  edge [
    source 5
    target 458
  ]
  edge [
    source 5
    target 459
  ]
  edge [
    source 5
    target 460
  ]
  edge [
    source 5
    target 461
  ]
  edge [
    source 5
    target 462
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 464
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 466
  ]
  edge [
    source 5
    target 467
  ]
  edge [
    source 5
    target 468
  ]
  edge [
    source 5
    target 469
  ]
  edge [
    source 5
    target 470
  ]
  edge [
    source 5
    target 471
  ]
  edge [
    source 5
    target 472
  ]
  edge [
    source 5
    target 473
  ]
  edge [
    source 5
    target 474
  ]
  edge [
    source 5
    target 475
  ]
  edge [
    source 5
    target 476
  ]
  edge [
    source 5
    target 477
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 479
  ]
  edge [
    source 5
    target 480
  ]
  edge [
    source 5
    target 481
  ]
  edge [
    source 5
    target 482
  ]
  edge [
    source 5
    target 483
  ]
  edge [
    source 5
    target 484
  ]
  edge [
    source 5
    target 485
  ]
  edge [
    source 5
    target 486
  ]
  edge [
    source 5
    target 487
  ]
  edge [
    source 5
    target 488
  ]
  edge [
    source 5
    target 489
  ]
  edge [
    source 5
    target 490
  ]
  edge [
    source 5
    target 491
  ]
  edge [
    source 5
    target 492
  ]
  edge [
    source 5
    target 493
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 494
  ]
  edge [
    source 5
    target 495
  ]
  edge [
    source 5
    target 496
  ]
  edge [
    source 5
    target 497
  ]
  edge [
    source 5
    target 498
  ]
  edge [
    source 5
    target 499
  ]
  edge [
    source 5
    target 500
  ]
  edge [
    source 5
    target 501
  ]
  edge [
    source 5
    target 502
  ]
  edge [
    source 5
    target 503
  ]
  edge [
    source 5
    target 504
  ]
  edge [
    source 5
    target 505
  ]
  edge [
    source 5
    target 506
  ]
  edge [
    source 5
    target 507
  ]
  edge [
    source 5
    target 508
  ]
  edge [
    source 5
    target 509
  ]
  edge [
    source 5
    target 510
  ]
  edge [
    source 5
    target 511
  ]
  edge [
    source 5
    target 512
  ]
  edge [
    source 5
    target 513
  ]
  edge [
    source 5
    target 514
  ]
  edge [
    source 5
    target 515
  ]
  edge [
    source 5
    target 516
  ]
  edge [
    source 5
    target 517
  ]
  edge [
    source 5
    target 518
  ]
  edge [
    source 5
    target 519
  ]
  edge [
    source 5
    target 520
  ]
  edge [
    source 5
    target 521
  ]
  edge [
    source 5
    target 522
  ]
  edge [
    source 5
    target 523
  ]
  edge [
    source 5
    target 524
  ]
  edge [
    source 5
    target 525
  ]
  edge [
    source 5
    target 526
  ]
  edge [
    source 5
    target 527
  ]
  edge [
    source 5
    target 528
  ]
  edge [
    source 5
    target 529
  ]
  edge [
    source 5
    target 530
  ]
  edge [
    source 5
    target 531
  ]
  edge [
    source 5
    target 532
  ]
  edge [
    source 5
    target 533
  ]
  edge [
    source 5
    target 534
  ]
  edge [
    source 5
    target 535
  ]
  edge [
    source 5
    target 536
  ]
  edge [
    source 5
    target 537
  ]
  edge [
    source 5
    target 538
  ]
  edge [
    source 5
    target 539
  ]
  edge [
    source 5
    target 540
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 542
  ]
  edge [
    source 5
    target 543
  ]
  edge [
    source 5
    target 544
  ]
  edge [
    source 5
    target 545
  ]
  edge [
    source 5
    target 546
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 18
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 13
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 580
  ]
  edge [
    source 7
    target 581
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 582
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 583
  ]
  edge [
    source 7
    target 584
  ]
  edge [
    source 7
    target 585
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 13
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 586
  ]
  edge [
    source 7
    target 587
  ]
  edge [
    source 7
    target 588
  ]
  edge [
    source 7
    target 589
  ]
  edge [
    source 7
    target 590
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 591
  ]
  edge [
    source 8
    target 592
  ]
  edge [
    source 8
    target 593
  ]
  edge [
    source 8
    target 594
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 466
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 9
    target 626
  ]
  edge [
    source 9
    target 627
  ]
  edge [
    source 9
    target 628
  ]
  edge [
    source 9
    target 629
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 630
  ]
  edge [
    source 9
    target 631
  ]
  edge [
    source 9
    target 632
  ]
  edge [
    source 9
    target 463
  ]
  edge [
    source 9
    target 633
  ]
  edge [
    source 9
    target 634
  ]
  edge [
    source 9
    target 635
  ]
  edge [
    source 9
    target 636
  ]
  edge [
    source 9
    target 637
  ]
  edge [
    source 9
    target 638
  ]
  edge [
    source 9
    target 639
  ]
  edge [
    source 9
    target 640
  ]
  edge [
    source 9
    target 641
  ]
  edge [
    source 9
    target 642
  ]
  edge [
    source 9
    target 643
  ]
  edge [
    source 9
    target 644
  ]
  edge [
    source 9
    target 645
  ]
  edge [
    source 9
    target 646
  ]
  edge [
    source 9
    target 647
  ]
  edge [
    source 9
    target 648
  ]
  edge [
    source 9
    target 649
  ]
  edge [
    source 9
    target 650
  ]
  edge [
    source 9
    target 651
  ]
  edge [
    source 9
    target 652
  ]
  edge [
    source 9
    target 653
  ]
  edge [
    source 9
    target 654
  ]
  edge [
    source 9
    target 655
  ]
  edge [
    source 9
    target 656
  ]
  edge [
    source 9
    target 657
  ]
  edge [
    source 9
    target 658
  ]
  edge [
    source 9
    target 659
  ]
  edge [
    source 9
    target 660
  ]
  edge [
    source 9
    target 661
  ]
  edge [
    source 9
    target 662
  ]
  edge [
    source 9
    target 663
  ]
  edge [
    source 9
    target 664
  ]
  edge [
    source 9
    target 665
  ]
  edge [
    source 9
    target 666
  ]
  edge [
    source 9
    target 667
  ]
  edge [
    source 9
    target 668
  ]
  edge [
    source 9
    target 669
  ]
  edge [
    source 9
    target 670
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 671
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 672
  ]
  edge [
    source 9
    target 673
  ]
  edge [
    source 9
    target 674
  ]
  edge [
    source 9
    target 675
  ]
  edge [
    source 9
    target 676
  ]
  edge [
    source 9
    target 455
  ]
  edge [
    source 9
    target 677
  ]
  edge [
    source 9
    target 678
  ]
  edge [
    source 9
    target 679
  ]
  edge [
    source 9
    target 680
  ]
  edge [
    source 9
    target 681
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 682
  ]
  edge [
    source 9
    target 683
  ]
  edge [
    source 9
    target 684
  ]
  edge [
    source 9
    target 685
  ]
  edge [
    source 9
    target 686
  ]
  edge [
    source 9
    target 687
  ]
  edge [
    source 9
    target 688
  ]
  edge [
    source 9
    target 689
  ]
  edge [
    source 9
    target 690
  ]
  edge [
    source 9
    target 691
  ]
  edge [
    source 9
    target 692
  ]
  edge [
    source 9
    target 693
  ]
  edge [
    source 9
    target 694
  ]
  edge [
    source 9
    target 695
  ]
  edge [
    source 9
    target 696
  ]
  edge [
    source 9
    target 697
  ]
  edge [
    source 9
    target 698
  ]
  edge [
    source 9
    target 699
  ]
  edge [
    source 9
    target 700
  ]
  edge [
    source 9
    target 701
  ]
  edge [
    source 9
    target 702
  ]
  edge [
    source 9
    target 703
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 608
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 615
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 704
  ]
  edge [
    source 10
    target 323
  ]
  edge [
    source 10
    target 705
  ]
  edge [
    source 10
    target 706
  ]
  edge [
    source 10
    target 707
  ]
  edge [
    source 10
    target 708
  ]
  edge [
    source 10
    target 709
  ]
  edge [
    source 10
    target 575
  ]
  edge [
    source 10
    target 710
  ]
  edge [
    source 10
    target 711
  ]
  edge [
    source 10
    target 712
  ]
  edge [
    source 10
    target 713
  ]
  edge [
    source 10
    target 21
  ]
  edge [
    source 10
    target 714
  ]
  edge [
    source 10
    target 715
  ]
  edge [
    source 10
    target 716
  ]
  edge [
    source 10
    target 717
  ]
  edge [
    source 10
    target 718
  ]
  edge [
    source 10
    target 719
  ]
  edge [
    source 10
    target 720
  ]
  edge [
    source 10
    target 721
  ]
  edge [
    source 10
    target 722
  ]
  edge [
    source 10
    target 723
  ]
  edge [
    source 10
    target 724
  ]
  edge [
    source 10
    target 725
  ]
  edge [
    source 10
    target 726
  ]
  edge [
    source 10
    target 727
  ]
  edge [
    source 10
    target 728
  ]
  edge [
    source 10
    target 729
  ]
  edge [
    source 10
    target 730
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 731
  ]
  edge [
    source 11
    target 732
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 733
  ]
  edge [
    source 11
    target 196
  ]
  edge [
    source 11
    target 734
  ]
  edge [
    source 11
    target 735
  ]
  edge [
    source 11
    target 21
  ]
  edge [
    source 11
    target 736
  ]
  edge [
    source 11
    target 737
  ]
  edge [
    source 11
    target 738
  ]
  edge [
    source 11
    target 739
  ]
  edge [
    source 11
    target 740
  ]
  edge [
    source 11
    target 741
  ]
  edge [
    source 11
    target 625
  ]
  edge [
    source 11
    target 742
  ]
  edge [
    source 11
    target 743
  ]
  edge [
    source 11
    target 496
  ]
  edge [
    source 11
    target 744
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 37
  ]
  edge [
    source 11
    target 32
  ]
  edge [
    source 11
    target 33
  ]
  edge [
    source 11
    target 34
  ]
  edge [
    source 11
    target 36
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 745
  ]
  edge [
    source 11
    target 650
  ]
  edge [
    source 11
    target 746
  ]
  edge [
    source 11
    target 747
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 748
  ]
  edge [
    source 11
    target 749
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 308
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 750
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 751
  ]
  edge [
    source 11
    target 752
  ]
  edge [
    source 11
    target 486
  ]
  edge [
    source 11
    target 753
  ]
  edge [
    source 11
    target 754
  ]
  edge [
    source 11
    target 755
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 756
  ]
  edge [
    source 11
    target 291
  ]
  edge [
    source 11
    target 757
  ]
  edge [
    source 11
    target 758
  ]
  edge [
    source 11
    target 759
  ]
  edge [
    source 11
    target 760
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 761
  ]
  edge [
    source 12
    target 762
  ]
  edge [
    source 12
    target 572
  ]
  edge [
    source 12
    target 323
  ]
  edge [
    source 12
    target 763
  ]
  edge [
    source 12
    target 764
  ]
  edge [
    source 12
    target 581
  ]
  edge [
    source 12
    target 765
  ]
  edge [
    source 12
    target 766
  ]
  edge [
    source 12
    target 767
  ]
  edge [
    source 12
    target 768
  ]
  edge [
    source 12
    target 23
  ]
  edge [
    source 12
    target 769
  ]
  edge [
    source 12
    target 770
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 771
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 31
  ]
  edge [
    source 12
    target 772
  ]
  edge [
    source 12
    target 21
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 773
  ]
  edge [
    source 12
    target 774
  ]
  edge [
    source 12
    target 775
  ]
  edge [
    source 12
    target 776
  ]
  edge [
    source 12
    target 608
  ]
  edge [
    source 12
    target 777
  ]
  edge [
    source 12
    target 241
  ]
  edge [
    source 12
    target 778
  ]
  edge [
    source 12
    target 779
  ]
  edge [
    source 12
    target 780
  ]
  edge [
    source 12
    target 781
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 782
  ]
  edge [
    source 12
    target 783
  ]
  edge [
    source 12
    target 784
  ]
  edge [
    source 12
    target 258
  ]
  edge [
    source 12
    target 259
  ]
  edge [
    source 12
    target 261
  ]
  edge [
    source 12
    target 785
  ]
  edge [
    source 12
    target 265
  ]
  edge [
    source 12
    target 266
  ]
  edge [
    source 12
    target 786
  ]
  edge [
    source 12
    target 268
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 787
  ]
  edge [
    source 12
    target 788
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 789
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 790
  ]
  edge [
    source 12
    target 791
  ]
  edge [
    source 12
    target 792
  ]
  edge [
    source 12
    target 793
  ]
  edge [
    source 12
    target 794
  ]
  edge [
    source 12
    target 795
  ]
  edge [
    source 12
    target 796
  ]
  edge [
    source 12
    target 797
  ]
  edge [
    source 12
    target 798
  ]
  edge [
    source 12
    target 799
  ]
  edge [
    source 12
    target 800
  ]
  edge [
    source 12
    target 801
  ]
  edge [
    source 12
    target 802
  ]
  edge [
    source 12
    target 803
  ]
  edge [
    source 12
    target 804
  ]
  edge [
    source 12
    target 805
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 806
  ]
  edge [
    source 12
    target 807
  ]
  edge [
    source 12
    target 808
  ]
  edge [
    source 12
    target 631
  ]
  edge [
    source 12
    target 809
  ]
  edge [
    source 12
    target 810
  ]
  edge [
    source 12
    target 811
  ]
  edge [
    source 12
    target 812
  ]
  edge [
    source 12
    target 813
  ]
  edge [
    source 12
    target 814
  ]
  edge [
    source 12
    target 815
  ]
  edge [
    source 12
    target 816
  ]
  edge [
    source 12
    target 817
  ]
  edge [
    source 12
    target 818
  ]
  edge [
    source 12
    target 819
  ]
  edge [
    source 12
    target 820
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 821
  ]
  edge [
    source 12
    target 822
  ]
  edge [
    source 12
    target 823
  ]
  edge [
    source 12
    target 824
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 825
  ]
  edge [
    source 12
    target 826
  ]
  edge [
    source 12
    target 827
  ]
  edge [
    source 12
    target 828
  ]
  edge [
    source 12
    target 829
  ]
  edge [
    source 12
    target 830
  ]
  edge [
    source 12
    target 228
  ]
  edge [
    source 12
    target 831
  ]
  edge [
    source 12
    target 832
  ]
  edge [
    source 12
    target 142
  ]
]
