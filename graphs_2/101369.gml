graph [
  node [
    id 0
    label "wayne"
    origin "text"
  ]
  node [
    id 1
    label "routledge"
    origin "text"
  ]
  node [
    id 2
    label "Wayne"
  ]
  node [
    id 3
    label "Routledge"
  ]
  node [
    id 4
    label "Newcastle"
  ]
  node [
    id 5
    label "United"
  ]
  node [
    id 6
    label "Crystal"
  ]
  node [
    id 7
    label "Palace"
  ]
  node [
    id 8
    label "Wolverhampton"
  ]
  node [
    id 9
    label "Wanderers"
  ]
  node [
    id 10
    label "The"
  ]
  node [
    id 11
    label "Championship"
  ]
  node [
    id 12
    label "Tottenham"
  ]
  node [
    id 13
    label "Hotspur"
  ]
  node [
    id 14
    label "White"
  ]
  node [
    id 15
    label "hart"
  ]
  node [
    id 16
    label "la&#263;"
  ]
  node [
    id 17
    label "Aaron"
  ]
  node [
    id 18
    label "Lennon"
  ]
  node [
    id 19
    label "Teemu"
  ]
  node [
    id 20
    label "Tainio"
  ]
  node [
    id 21
    label "Andy"
  ]
  node [
    id 22
    label "Reid"
  ]
  node [
    id 23
    label "Pompeys"
  ]
  node [
    id 24
    label "Aston"
  ]
  node [
    id 25
    label "Vill&#261;"
  ]
  node [
    id 26
    label "Boltonem"
  ]
  node [
    id 27
    label "puchar"
  ]
  node [
    id 28
    label "UEFA"
  ]
  node [
    id 29
    label "Intertoto"
  ]
  node [
    id 30
    label "Cardiff"
  ]
  node [
    id 31
    label "city"
  ]
  node [
    id 32
    label "Plymouth"
  ]
  node [
    id 33
    label "Argyle"
  ]
  node [
    id 34
    label "Queens"
  ]
  node [
    id 35
    label "park"
  ]
  node [
    id 36
    label "Rangers"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 23
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 35
    target 36
  ]
]
