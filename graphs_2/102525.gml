graph [
  node [
    id 0
    label "ten"
    origin "text"
  ]
  node [
    id 1
    label "por"
    origin "text"
  ]
  node [
    id 2
    label "energia"
    origin "text"
  ]
  node [
    id 3
    label "elektryczny"
    origin "text"
  ]
  node [
    id 4
    label "pozyskiwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "ziemniak"
    origin "text"
  ]
  node [
    id 6
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 7
    label "przedmiot"
    origin "text"
  ]
  node [
    id 8
    label "jedynie"
    origin "text"
  ]
  node [
    id 9
    label "szkolny"
    origin "text"
  ]
  node [
    id 10
    label "lub"
    origin "text"
  ]
  node [
    id 11
    label "domowy"
    origin "text"
  ]
  node [
    id 12
    label "eksperyment"
    origin "text"
  ]
  node [
    id 13
    label "naukowiec"
    origin "text"
  ]
  node [
    id 14
    label "postanowi&#263;"
    origin "text"
  ]
  node [
    id 15
    label "zbada&#263;"
    origin "text"
  ]
  node [
    id 16
    label "ukry&#263;"
    origin "text"
  ]
  node [
    id 17
    label "potencja&#322;"
    origin "text"
  ]
  node [
    id 18
    label "powa&#380;nie"
    origin "text"
  ]
  node [
    id 19
    label "wykorzystywa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "prosta"
    origin "text"
  ]
  node [
    id 21
    label "ogniwo"
    origin "text"
  ]
  node [
    id 22
    label "galwaniczny"
    origin "text"
  ]
  node [
    id 23
    label "jerozolima"
    origin "text"
  ]
  node [
    id 24
    label "uda&#263;"
    origin "text"
  ]
  node [
    id 25
    label "si&#281;"
    origin "text"
  ]
  node [
    id 26
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 27
    label "gotowany"
    origin "text"
  ]
  node [
    id 28
    label "ziemniaczek"
    origin "text"
  ]
  node [
    id 29
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 30
    label "by&#263;"
    origin "text"
  ]
  node [
    id 31
    label "ca&#322;kiem"
    origin "text"
  ]
  node [
    id 32
    label "wydajny"
    origin "text"
  ]
  node [
    id 33
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 34
    label "pr&#261;d"
    origin "text"
  ]
  node [
    id 35
    label "okre&#347;lony"
  ]
  node [
    id 36
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 37
    label "wiadomy"
  ]
  node [
    id 38
    label "w&#322;oszczyzna"
  ]
  node [
    id 39
    label "czosnek"
  ]
  node [
    id 40
    label "warzywo"
  ]
  node [
    id 41
    label "kapelusz"
  ]
  node [
    id 42
    label "otw&#243;r"
  ]
  node [
    id 43
    label "uj&#347;cie"
  ]
  node [
    id 44
    label "pouciekanie"
  ]
  node [
    id 45
    label "odpr&#281;&#380;enie"
  ]
  node [
    id 46
    label "sp&#322;oszenie"
  ]
  node [
    id 47
    label "spieprzenie"
  ]
  node [
    id 48
    label "wsi&#261;kni&#281;cie"
  ]
  node [
    id 49
    label "wzi&#281;cie"
  ]
  node [
    id 50
    label "wylot"
  ]
  node [
    id 51
    label "ulotnienie_si&#281;"
  ]
  node [
    id 52
    label "ciecz"
  ]
  node [
    id 53
    label "ciek_wodny"
  ]
  node [
    id 54
    label "przedostanie_si&#281;"
  ]
  node [
    id 55
    label "miejsce"
  ]
  node [
    id 56
    label "wyp&#322;yni&#281;cie"
  ]
  node [
    id 57
    label "odp&#322;yw"
  ]
  node [
    id 58
    label "release"
  ]
  node [
    id 59
    label "vent"
  ]
  node [
    id 60
    label "departure"
  ]
  node [
    id 61
    label "zwianie"
  ]
  node [
    id 62
    label "rozlanie_si&#281;"
  ]
  node [
    id 63
    label "oddalenie_si&#281;"
  ]
  node [
    id 64
    label "geofit_cebulowy"
  ]
  node [
    id 65
    label "czoch"
  ]
  node [
    id 66
    label "bylina"
  ]
  node [
    id 67
    label "czosnkowe"
  ]
  node [
    id 68
    label "z&#261;bek"
  ]
  node [
    id 69
    label "przyprawa"
  ]
  node [
    id 70
    label "ro&#347;lina"
  ]
  node [
    id 71
    label "cebulka"
  ]
  node [
    id 72
    label "blanszownik"
  ]
  node [
    id 73
    label "produkt"
  ]
  node [
    id 74
    label "ogrodowizna"
  ]
  node [
    id 75
    label "zielenina"
  ]
  node [
    id 76
    label "obieralnia"
  ]
  node [
    id 77
    label "nieuleczalnie_chory"
  ]
  node [
    id 78
    label "przestrze&#324;"
  ]
  node [
    id 79
    label "wyd&#322;uba&#263;"
  ]
  node [
    id 80
    label "wybicie"
  ]
  node [
    id 81
    label "wyd&#322;ubanie"
  ]
  node [
    id 82
    label "przerwa"
  ]
  node [
    id 83
    label "powybijanie"
  ]
  node [
    id 84
    label "wybijanie"
  ]
  node [
    id 85
    label "wiercenie"
  ]
  node [
    id 86
    label "nakrycie_g&#322;owy"
  ]
  node [
    id 87
    label "kapotka"
  ]
  node [
    id 88
    label "hymenofor"
  ]
  node [
    id 89
    label "g&#322;&#243;wka"
  ]
  node [
    id 90
    label "kresa"
  ]
  node [
    id 91
    label "grzyb_kapeluszowy"
  ]
  node [
    id 92
    label "rondo"
  ]
  node [
    id 93
    label "makaroniarski"
  ]
  node [
    id 94
    label "j&#281;zyk_roma&#324;ski"
  ]
  node [
    id 95
    label "jedzenie"
  ]
  node [
    id 96
    label "Bona"
  ]
  node [
    id 97
    label "towar"
  ]
  node [
    id 98
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 99
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 100
    label "egzergia"
  ]
  node [
    id 101
    label "emitowa&#263;"
  ]
  node [
    id 102
    label "kwant_energii"
  ]
  node [
    id 103
    label "szwung"
  ]
  node [
    id 104
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 105
    label "power"
  ]
  node [
    id 106
    label "zjawisko"
  ]
  node [
    id 107
    label "cecha"
  ]
  node [
    id 108
    label "emitowanie"
  ]
  node [
    id 109
    label "energy"
  ]
  node [
    id 110
    label "proces"
  ]
  node [
    id 111
    label "boski"
  ]
  node [
    id 112
    label "krajobraz"
  ]
  node [
    id 113
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 114
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 115
    label "przywidzenie"
  ]
  node [
    id 116
    label "presence"
  ]
  node [
    id 117
    label "charakter"
  ]
  node [
    id 118
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 119
    label "ton"
  ]
  node [
    id 120
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 121
    label "charakterystyka"
  ]
  node [
    id 122
    label "m&#322;ot"
  ]
  node [
    id 123
    label "znak"
  ]
  node [
    id 124
    label "drzewo"
  ]
  node [
    id 125
    label "pr&#243;ba"
  ]
  node [
    id 126
    label "attribute"
  ]
  node [
    id 127
    label "marka"
  ]
  node [
    id 128
    label "rynek"
  ]
  node [
    id 129
    label "nadawa&#263;"
  ]
  node [
    id 130
    label "wysy&#322;a&#263;"
  ]
  node [
    id 131
    label "nada&#263;"
  ]
  node [
    id 132
    label "tembr"
  ]
  node [
    id 133
    label "air"
  ]
  node [
    id 134
    label "wydoby&#263;"
  ]
  node [
    id 135
    label "emit"
  ]
  node [
    id 136
    label "wys&#322;a&#263;"
  ]
  node [
    id 137
    label "wydzieli&#263;"
  ]
  node [
    id 138
    label "wydziela&#263;"
  ]
  node [
    id 139
    label "program"
  ]
  node [
    id 140
    label "wprowadzi&#263;"
  ]
  node [
    id 141
    label "wydobywa&#263;"
  ]
  node [
    id 142
    label "wprowadza&#263;"
  ]
  node [
    id 143
    label "termodynamika_klasyczna"
  ]
  node [
    id 144
    label "wys&#322;anie"
  ]
  node [
    id 145
    label "wysy&#322;anie"
  ]
  node [
    id 146
    label "wydzielenie"
  ]
  node [
    id 147
    label "wprowadzenie"
  ]
  node [
    id 148
    label "wydobycie"
  ]
  node [
    id 149
    label "wydzielanie"
  ]
  node [
    id 150
    label "wydobywanie"
  ]
  node [
    id 151
    label "nadawanie"
  ]
  node [
    id 152
    label "emission"
  ]
  node [
    id 153
    label "wprowadzanie"
  ]
  node [
    id 154
    label "nadanie"
  ]
  node [
    id 155
    label "issue"
  ]
  node [
    id 156
    label "zapa&#322;"
  ]
  node [
    id 157
    label "elektrycznie"
  ]
  node [
    id 158
    label "uzyskiwa&#263;"
  ]
  node [
    id 159
    label "wytwarza&#263;"
  ]
  node [
    id 160
    label "tease"
  ]
  node [
    id 161
    label "take"
  ]
  node [
    id 162
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 163
    label "robi&#263;"
  ]
  node [
    id 164
    label "determine"
  ]
  node [
    id 165
    label "work"
  ]
  node [
    id 166
    label "powodowa&#263;"
  ]
  node [
    id 167
    label "reakcja_chemiczna"
  ]
  node [
    id 168
    label "create"
  ]
  node [
    id 169
    label "give"
  ]
  node [
    id 170
    label "get"
  ]
  node [
    id 171
    label "mark"
  ]
  node [
    id 172
    label "bulwa_p&#281;dowa"
  ]
  node [
    id 173
    label "p&#281;t&#243;wka"
  ]
  node [
    id 174
    label "grula"
  ]
  node [
    id 175
    label "psianka"
  ]
  node [
    id 176
    label "ro&#347;lina_bulwiasta"
  ]
  node [
    id 177
    label "ba&#322;aban"
  ]
  node [
    id 178
    label "m&#261;ka_ziemniaczana"
  ]
  node [
    id 179
    label "potato"
  ]
  node [
    id 180
    label "ro&#347;lina_truj&#261;ca"
  ]
  node [
    id 181
    label "psiankowate"
  ]
  node [
    id 182
    label "jagoda"
  ]
  node [
    id 183
    label "ro&#347;lina_zielna"
  ]
  node [
    id 184
    label "ludowy"
  ]
  node [
    id 185
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 186
    label "utw&#243;r_epicki"
  ]
  node [
    id 187
    label "pie&#347;&#324;"
  ]
  node [
    id 188
    label "zbiorowisko"
  ]
  node [
    id 189
    label "ro&#347;liny"
  ]
  node [
    id 190
    label "p&#281;d"
  ]
  node [
    id 191
    label "wegetowanie"
  ]
  node [
    id 192
    label "zadziorek"
  ]
  node [
    id 193
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 194
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 195
    label "do&#322;owa&#263;"
  ]
  node [
    id 196
    label "wegetacja"
  ]
  node [
    id 197
    label "owoc"
  ]
  node [
    id 198
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 199
    label "strzyc"
  ]
  node [
    id 200
    label "w&#322;&#243;kno"
  ]
  node [
    id 201
    label "g&#322;uszenie"
  ]
  node [
    id 202
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 203
    label "fitotron"
  ]
  node [
    id 204
    label "bulwka"
  ]
  node [
    id 205
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 206
    label "odn&#243;&#380;ka"
  ]
  node [
    id 207
    label "epiderma"
  ]
  node [
    id 208
    label "gumoza"
  ]
  node [
    id 209
    label "strzy&#380;enie"
  ]
  node [
    id 210
    label "wypotnik"
  ]
  node [
    id 211
    label "flawonoid"
  ]
  node [
    id 212
    label "wyro&#347;le"
  ]
  node [
    id 213
    label "do&#322;owanie"
  ]
  node [
    id 214
    label "g&#322;uszy&#263;"
  ]
  node [
    id 215
    label "pora&#380;a&#263;"
  ]
  node [
    id 216
    label "fitocenoza"
  ]
  node [
    id 217
    label "hodowla"
  ]
  node [
    id 218
    label "fotoautotrof"
  ]
  node [
    id 219
    label "wegetowa&#263;"
  ]
  node [
    id 220
    label "pochewka"
  ]
  node [
    id 221
    label "sok"
  ]
  node [
    id 222
    label "system_korzeniowy"
  ]
  node [
    id 223
    label "zawi&#261;zek"
  ]
  node [
    id 224
    label "ropucha"
  ]
  node [
    id 225
    label "ropuszkowate"
  ]
  node [
    id 226
    label "partnerka"
  ]
  node [
    id 227
    label "cz&#322;owiek"
  ]
  node [
    id 228
    label "aktorka"
  ]
  node [
    id 229
    label "kobieta"
  ]
  node [
    id 230
    label "partner"
  ]
  node [
    id 231
    label "kobita"
  ]
  node [
    id 232
    label "zboczenie"
  ]
  node [
    id 233
    label "om&#243;wienie"
  ]
  node [
    id 234
    label "sponiewieranie"
  ]
  node [
    id 235
    label "discipline"
  ]
  node [
    id 236
    label "rzecz"
  ]
  node [
    id 237
    label "omawia&#263;"
  ]
  node [
    id 238
    label "kr&#261;&#380;enie"
  ]
  node [
    id 239
    label "tre&#347;&#263;"
  ]
  node [
    id 240
    label "robienie"
  ]
  node [
    id 241
    label "sponiewiera&#263;"
  ]
  node [
    id 242
    label "element"
  ]
  node [
    id 243
    label "entity"
  ]
  node [
    id 244
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 245
    label "tematyka"
  ]
  node [
    id 246
    label "w&#261;tek"
  ]
  node [
    id 247
    label "zbaczanie"
  ]
  node [
    id 248
    label "program_nauczania"
  ]
  node [
    id 249
    label "om&#243;wi&#263;"
  ]
  node [
    id 250
    label "omawianie"
  ]
  node [
    id 251
    label "thing"
  ]
  node [
    id 252
    label "kultura"
  ]
  node [
    id 253
    label "istota"
  ]
  node [
    id 254
    label "zbacza&#263;"
  ]
  node [
    id 255
    label "zboczy&#263;"
  ]
  node [
    id 256
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 257
    label "temat"
  ]
  node [
    id 258
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 259
    label "informacja"
  ]
  node [
    id 260
    label "zawarto&#347;&#263;"
  ]
  node [
    id 261
    label "object"
  ]
  node [
    id 262
    label "wpadni&#281;cie"
  ]
  node [
    id 263
    label "mienie"
  ]
  node [
    id 264
    label "przyroda"
  ]
  node [
    id 265
    label "obiekt"
  ]
  node [
    id 266
    label "wpa&#347;&#263;"
  ]
  node [
    id 267
    label "wpadanie"
  ]
  node [
    id 268
    label "wpada&#263;"
  ]
  node [
    id 269
    label "discussion"
  ]
  node [
    id 270
    label "rozpatrywanie"
  ]
  node [
    id 271
    label "dyskutowanie"
  ]
  node [
    id 272
    label "omowny"
  ]
  node [
    id 273
    label "figura_stylistyczna"
  ]
  node [
    id 274
    label "sformu&#322;owanie"
  ]
  node [
    id 275
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 276
    label "odchodzenie"
  ]
  node [
    id 277
    label "aberrance"
  ]
  node [
    id 278
    label "swerve"
  ]
  node [
    id 279
    label "kierunek"
  ]
  node [
    id 280
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 281
    label "distract"
  ]
  node [
    id 282
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 283
    label "odej&#347;&#263;"
  ]
  node [
    id 284
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 285
    label "twist"
  ]
  node [
    id 286
    label "zmieni&#263;"
  ]
  node [
    id 287
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 288
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 289
    label "przedyskutowa&#263;"
  ]
  node [
    id 290
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 291
    label "publicize"
  ]
  node [
    id 292
    label "digress"
  ]
  node [
    id 293
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 294
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 295
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 296
    label "odchodzi&#263;"
  ]
  node [
    id 297
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 298
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 299
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 300
    label "perversion"
  ]
  node [
    id 301
    label "death"
  ]
  node [
    id 302
    label "odej&#347;cie"
  ]
  node [
    id 303
    label "turn"
  ]
  node [
    id 304
    label "k&#261;t"
  ]
  node [
    id 305
    label "poj&#281;cie"
  ]
  node [
    id 306
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 307
    label "odchylenie_si&#281;"
  ]
  node [
    id 308
    label "deviation"
  ]
  node [
    id 309
    label "patologia"
  ]
  node [
    id 310
    label "dyskutowa&#263;"
  ]
  node [
    id 311
    label "formu&#322;owa&#263;"
  ]
  node [
    id 312
    label "discourse"
  ]
  node [
    id 313
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 314
    label "zbi&#243;r"
  ]
  node [
    id 315
    label "wydarzenie"
  ]
  node [
    id 316
    label "osobowo&#347;&#263;"
  ]
  node [
    id 317
    label "psychika"
  ]
  node [
    id 318
    label "posta&#263;"
  ]
  node [
    id 319
    label "kompleksja"
  ]
  node [
    id 320
    label "fizjonomia"
  ]
  node [
    id 321
    label "rotation"
  ]
  node [
    id 322
    label "obieg"
  ]
  node [
    id 323
    label "krew"
  ]
  node [
    id 324
    label "kontrolowanie"
  ]
  node [
    id 325
    label "patrol"
  ]
  node [
    id 326
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 327
    label "lap"
  ]
  node [
    id 328
    label "asymilowanie_si&#281;"
  ]
  node [
    id 329
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 330
    label "Wsch&#243;d"
  ]
  node [
    id 331
    label "praca_rolnicza"
  ]
  node [
    id 332
    label "przejmowanie"
  ]
  node [
    id 333
    label "makrokosmos"
  ]
  node [
    id 334
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 335
    label "konwencja"
  ]
  node [
    id 336
    label "propriety"
  ]
  node [
    id 337
    label "przejmowa&#263;"
  ]
  node [
    id 338
    label "brzoskwiniarnia"
  ]
  node [
    id 339
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 340
    label "sztuka"
  ]
  node [
    id 341
    label "zwyczaj"
  ]
  node [
    id 342
    label "jako&#347;&#263;"
  ]
  node [
    id 343
    label "kuchnia"
  ]
  node [
    id 344
    label "tradycja"
  ]
  node [
    id 345
    label "populace"
  ]
  node [
    id 346
    label "religia"
  ]
  node [
    id 347
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 348
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 349
    label "przej&#281;cie"
  ]
  node [
    id 350
    label "przej&#261;&#263;"
  ]
  node [
    id 351
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 352
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 353
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 354
    label "fabrication"
  ]
  node [
    id 355
    label "bycie"
  ]
  node [
    id 356
    label "zap&#281;dzenie_si&#281;"
  ]
  node [
    id 357
    label "zap&#281;dzanie_si&#281;"
  ]
  node [
    id 358
    label "creation"
  ]
  node [
    id 359
    label "m&#281;czenie_si&#281;"
  ]
  node [
    id 360
    label "zasiedzenie_si&#281;"
  ]
  node [
    id 361
    label "act"
  ]
  node [
    id 362
    label "porobienie"
  ]
  node [
    id 363
    label "czynno&#347;&#263;"
  ]
  node [
    id 364
    label "tentegowanie"
  ]
  node [
    id 365
    label "upi&#263;"
  ]
  node [
    id 366
    label "zm&#281;czy&#263;"
  ]
  node [
    id 367
    label "zniszczy&#263;"
  ]
  node [
    id 368
    label "kontrolowa&#263;"
  ]
  node [
    id 369
    label "carry"
  ]
  node [
    id 370
    label "wheel"
  ]
  node [
    id 371
    label "zniszczenie"
  ]
  node [
    id 372
    label "mentalno&#347;&#263;"
  ]
  node [
    id 373
    label "superego"
  ]
  node [
    id 374
    label "znaczenie"
  ]
  node [
    id 375
    label "wn&#281;trze"
  ]
  node [
    id 376
    label "forum"
  ]
  node [
    id 377
    label "matter"
  ]
  node [
    id 378
    label "topik"
  ]
  node [
    id 379
    label "splot"
  ]
  node [
    id 380
    label "wytw&#243;r"
  ]
  node [
    id 381
    label "ceg&#322;a"
  ]
  node [
    id 382
    label "socket"
  ]
  node [
    id 383
    label "rozmieszczenie"
  ]
  node [
    id 384
    label "fabu&#322;a"
  ]
  node [
    id 385
    label "r&#243;&#380;niczka"
  ]
  node [
    id 386
    label "&#347;rodowisko"
  ]
  node [
    id 387
    label "materia"
  ]
  node [
    id 388
    label "szambo"
  ]
  node [
    id 389
    label "aspo&#322;eczny"
  ]
  node [
    id 390
    label "component"
  ]
  node [
    id 391
    label "szkodnik"
  ]
  node [
    id 392
    label "gangsterski"
  ]
  node [
    id 393
    label "underworld"
  ]
  node [
    id 394
    label "p&#243;&#322;&#347;wiatek"
  ]
  node [
    id 395
    label "typ_spod_ciemnej_gwiazdy"
  ]
  node [
    id 396
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 397
    label "szkolnie"
  ]
  node [
    id 398
    label "podstawowy"
  ]
  node [
    id 399
    label "prosty"
  ]
  node [
    id 400
    label "szkoleniowy"
  ]
  node [
    id 401
    label "skromny"
  ]
  node [
    id 402
    label "po_prostu"
  ]
  node [
    id 403
    label "naturalny"
  ]
  node [
    id 404
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 405
    label "rozprostowanie"
  ]
  node [
    id 406
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 407
    label "prosto"
  ]
  node [
    id 408
    label "prostowanie_si&#281;"
  ]
  node [
    id 409
    label "niepozorny"
  ]
  node [
    id 410
    label "cios"
  ]
  node [
    id 411
    label "prostoduszny"
  ]
  node [
    id 412
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 413
    label "naiwny"
  ]
  node [
    id 414
    label "&#322;atwy"
  ]
  node [
    id 415
    label "prostowanie"
  ]
  node [
    id 416
    label "zwyk&#322;y"
  ]
  node [
    id 417
    label "naukowy"
  ]
  node [
    id 418
    label "niezaawansowany"
  ]
  node [
    id 419
    label "najwa&#380;niejszy"
  ]
  node [
    id 420
    label "pocz&#261;tkowy"
  ]
  node [
    id 421
    label "podstawowo"
  ]
  node [
    id 422
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 423
    label "domowo"
  ]
  node [
    id 424
    label "budynkowy"
  ]
  node [
    id 425
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 426
    label "domestically"
  ]
  node [
    id 427
    label "nale&#380;ny"
  ]
  node [
    id 428
    label "nale&#380;yty"
  ]
  node [
    id 429
    label "typowy"
  ]
  node [
    id 430
    label "uprawniony"
  ]
  node [
    id 431
    label "zasadniczy"
  ]
  node [
    id 432
    label "stosownie"
  ]
  node [
    id 433
    label "taki"
  ]
  node [
    id 434
    label "charakterystyczny"
  ]
  node [
    id 435
    label "prawdziwy"
  ]
  node [
    id 436
    label "dobry"
  ]
  node [
    id 437
    label "assay"
  ]
  node [
    id 438
    label "badanie"
  ]
  node [
    id 439
    label "innowacja"
  ]
  node [
    id 440
    label "obserwowanie"
  ]
  node [
    id 441
    label "zrecenzowanie"
  ]
  node [
    id 442
    label "kontrola"
  ]
  node [
    id 443
    label "analysis"
  ]
  node [
    id 444
    label "rektalny"
  ]
  node [
    id 445
    label "ustalenie"
  ]
  node [
    id 446
    label "macanie"
  ]
  node [
    id 447
    label "dowiadywanie_si&#281;"
  ]
  node [
    id 448
    label "usi&#322;owanie"
  ]
  node [
    id 449
    label "udowadnianie"
  ]
  node [
    id 450
    label "praca"
  ]
  node [
    id 451
    label "bia&#322;a_niedziela"
  ]
  node [
    id 452
    label "diagnostyka"
  ]
  node [
    id 453
    label "dociekanie"
  ]
  node [
    id 454
    label "rezultat"
  ]
  node [
    id 455
    label "sprawdzanie"
  ]
  node [
    id 456
    label "penetrowanie"
  ]
  node [
    id 457
    label "krytykowanie"
  ]
  node [
    id 458
    label "ustalanie"
  ]
  node [
    id 459
    label "investigation"
  ]
  node [
    id 460
    label "wziernikowanie"
  ]
  node [
    id 461
    label "examination"
  ]
  node [
    id 462
    label "knickknack"
  ]
  node [
    id 463
    label "zmiana"
  ]
  node [
    id 464
    label "nowo&#347;&#263;"
  ]
  node [
    id 465
    label "patrzenie"
  ]
  node [
    id 466
    label "doszukiwanie_si&#281;"
  ]
  node [
    id 467
    label "doszukanie_si&#281;"
  ]
  node [
    id 468
    label "dostrzeganie"
  ]
  node [
    id 469
    label "poobserwowanie"
  ]
  node [
    id 470
    label "observation"
  ]
  node [
    id 471
    label "bocianie_gniazdo"
  ]
  node [
    id 472
    label "&#347;ledziciel"
  ]
  node [
    id 473
    label "uczony"
  ]
  node [
    id 474
    label "Miczurin"
  ]
  node [
    id 475
    label "wykszta&#322;cony"
  ]
  node [
    id 476
    label "inteligent"
  ]
  node [
    id 477
    label "intelektualista"
  ]
  node [
    id 478
    label "Awerroes"
  ]
  node [
    id 479
    label "uczenie"
  ]
  node [
    id 480
    label "nauczny"
  ]
  node [
    id 481
    label "m&#261;dry"
  ]
  node [
    id 482
    label "agent"
  ]
  node [
    id 483
    label "podj&#261;&#263;"
  ]
  node [
    id 484
    label "sta&#263;_si&#281;"
  ]
  node [
    id 485
    label "zrobi&#263;"
  ]
  node [
    id 486
    label "zareagowa&#263;"
  ]
  node [
    id 487
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 488
    label "draw"
  ]
  node [
    id 489
    label "allude"
  ]
  node [
    id 490
    label "zacz&#261;&#263;"
  ]
  node [
    id 491
    label "raise"
  ]
  node [
    id 492
    label "post&#261;pi&#263;"
  ]
  node [
    id 493
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 494
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 495
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 496
    label "zorganizowa&#263;"
  ]
  node [
    id 497
    label "appoint"
  ]
  node [
    id 498
    label "wystylizowa&#263;"
  ]
  node [
    id 499
    label "cause"
  ]
  node [
    id 500
    label "przerobi&#263;"
  ]
  node [
    id 501
    label "nabra&#263;"
  ]
  node [
    id 502
    label "make"
  ]
  node [
    id 503
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 504
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 505
    label "wydali&#263;"
  ]
  node [
    id 506
    label "sprawdzi&#263;"
  ]
  node [
    id 507
    label "pozna&#263;"
  ]
  node [
    id 508
    label "zdecydowa&#263;"
  ]
  node [
    id 509
    label "wybada&#263;"
  ]
  node [
    id 510
    label "lekarz"
  ]
  node [
    id 511
    label "examine"
  ]
  node [
    id 512
    label "decide"
  ]
  node [
    id 513
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 514
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 515
    label "zrozumie&#263;"
  ]
  node [
    id 516
    label "feel"
  ]
  node [
    id 517
    label "topographic_point"
  ]
  node [
    id 518
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 519
    label "visualize"
  ]
  node [
    id 520
    label "przyswoi&#263;"
  ]
  node [
    id 521
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 522
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 523
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 524
    label "teach"
  ]
  node [
    id 525
    label "experience"
  ]
  node [
    id 526
    label "try"
  ]
  node [
    id 527
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 528
    label "wykry&#263;"
  ]
  node [
    id 529
    label "wypyta&#263;"
  ]
  node [
    id 530
    label "Mesmer"
  ]
  node [
    id 531
    label "pracownik"
  ]
  node [
    id 532
    label "Galen"
  ]
  node [
    id 533
    label "medyk"
  ]
  node [
    id 534
    label "eskulap"
  ]
  node [
    id 535
    label "lekarze"
  ]
  node [
    id 536
    label "Hipokrates"
  ]
  node [
    id 537
    label "dokt&#243;r"
  ]
  node [
    id 538
    label "hide"
  ]
  node [
    id 539
    label "zmie&#347;ci&#263;"
  ]
  node [
    id 540
    label "zachowa&#263;"
  ]
  node [
    id 541
    label "ensconce"
  ]
  node [
    id 542
    label "przytai&#263;"
  ]
  node [
    id 543
    label "umie&#347;ci&#263;"
  ]
  node [
    id 544
    label "tajemnica"
  ]
  node [
    id 545
    label "pami&#281;&#263;"
  ]
  node [
    id 546
    label "podporz&#261;dkowa&#263;_si&#281;"
  ]
  node [
    id 547
    label "zdyscyplinowanie"
  ]
  node [
    id 548
    label "post"
  ]
  node [
    id 549
    label "przechowa&#263;"
  ]
  node [
    id 550
    label "preserve"
  ]
  node [
    id 551
    label "dieta"
  ]
  node [
    id 552
    label "bury"
  ]
  node [
    id 553
    label "podtrzyma&#263;"
  ]
  node [
    id 554
    label "set"
  ]
  node [
    id 555
    label "put"
  ]
  node [
    id 556
    label "uplasowa&#263;"
  ]
  node [
    id 557
    label "wpierniczy&#263;"
  ]
  node [
    id 558
    label "okre&#347;li&#263;"
  ]
  node [
    id 559
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 560
    label "umieszcza&#263;"
  ]
  node [
    id 561
    label "udost&#281;pni&#263;"
  ]
  node [
    id 562
    label "obj&#261;&#263;"
  ]
  node [
    id 563
    label "clasp"
  ]
  node [
    id 564
    label "hold"
  ]
  node [
    id 565
    label "cover"
  ]
  node [
    id 566
    label "wielko&#347;&#263;"
  ]
  node [
    id 567
    label "warunek_lokalowy"
  ]
  node [
    id 568
    label "rozmiar"
  ]
  node [
    id 569
    label "liczba"
  ]
  node [
    id 570
    label "rzadko&#347;&#263;"
  ]
  node [
    id 571
    label "zaleta"
  ]
  node [
    id 572
    label "ilo&#347;&#263;"
  ]
  node [
    id 573
    label "measure"
  ]
  node [
    id 574
    label "opinia"
  ]
  node [
    id 575
    label "dymensja"
  ]
  node [
    id 576
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 577
    label "zdolno&#347;&#263;"
  ]
  node [
    id 578
    label "potencja"
  ]
  node [
    id 579
    label "property"
  ]
  node [
    id 580
    label "gro&#378;nie"
  ]
  node [
    id 581
    label "ci&#281;&#380;ko"
  ]
  node [
    id 582
    label "niema&#322;o"
  ]
  node [
    id 583
    label "powa&#380;ny"
  ]
  node [
    id 584
    label "ci&#281;&#380;ki"
  ]
  node [
    id 585
    label "bardzo"
  ]
  node [
    id 586
    label "du&#380;y"
  ]
  node [
    id 587
    label "trudny"
  ]
  node [
    id 588
    label "spowa&#380;nienie"
  ]
  node [
    id 589
    label "gro&#378;ny"
  ]
  node [
    id 590
    label "powa&#380;nienie"
  ]
  node [
    id 591
    label "monumentalny"
  ]
  node [
    id 592
    label "mocny"
  ]
  node [
    id 593
    label "kompletny"
  ]
  node [
    id 594
    label "masywny"
  ]
  node [
    id 595
    label "wielki"
  ]
  node [
    id 596
    label "wymagaj&#261;cy"
  ]
  node [
    id 597
    label "przyt&#322;aczaj&#261;cy"
  ]
  node [
    id 598
    label "przyswajalny"
  ]
  node [
    id 599
    label "niezgrabny"
  ]
  node [
    id 600
    label "liczny"
  ]
  node [
    id 601
    label "nieprzejrzysty"
  ]
  node [
    id 602
    label "niedelikatny"
  ]
  node [
    id 603
    label "oci&#281;&#380;a&#322;y"
  ]
  node [
    id 604
    label "intensywny"
  ]
  node [
    id 605
    label "wolny"
  ]
  node [
    id 606
    label "nieudany"
  ]
  node [
    id 607
    label "zbrojny"
  ]
  node [
    id 608
    label "dotkliwy"
  ]
  node [
    id 609
    label "bojowy"
  ]
  node [
    id 610
    label "k&#322;opotliwy"
  ]
  node [
    id 611
    label "ambitny"
  ]
  node [
    id 612
    label "grubo"
  ]
  node [
    id 613
    label "uci&#261;&#380;liwy"
  ]
  node [
    id 614
    label "niebezpiecznie"
  ]
  node [
    id 615
    label "sternly"
  ]
  node [
    id 616
    label "surowie"
  ]
  node [
    id 617
    label "w_chuj"
  ]
  node [
    id 618
    label "monumentalnie"
  ]
  node [
    id 619
    label "charakterystycznie"
  ]
  node [
    id 620
    label "przyt&#322;aczaj&#261;co"
  ]
  node [
    id 621
    label "uci&#261;&#380;liwie"
  ]
  node [
    id 622
    label "nieudanie"
  ]
  node [
    id 623
    label "mocno"
  ]
  node [
    id 624
    label "wolno"
  ]
  node [
    id 625
    label "kompletnie"
  ]
  node [
    id 626
    label "dotkliwie"
  ]
  node [
    id 627
    label "niezgrabnie"
  ]
  node [
    id 628
    label "hard"
  ]
  node [
    id 629
    label "&#378;le"
  ]
  node [
    id 630
    label "masywnie"
  ]
  node [
    id 631
    label "heavily"
  ]
  node [
    id 632
    label "niedelikatnie"
  ]
  node [
    id 633
    label "intensywnie"
  ]
  node [
    id 634
    label "korzysta&#263;"
  ]
  node [
    id 635
    label "liga&#263;"
  ]
  node [
    id 636
    label "distribute"
  ]
  node [
    id 637
    label "u&#380;ywa&#263;"
  ]
  node [
    id 638
    label "wsp&#243;&#322;&#380;y&#263;"
  ]
  node [
    id 639
    label "use"
  ]
  node [
    id 640
    label "krzywdzi&#263;"
  ]
  node [
    id 641
    label "wyr&#281;cza&#263;_si&#281;"
  ]
  node [
    id 642
    label "bash"
  ]
  node [
    id 643
    label "przyjemno&#347;&#263;"
  ]
  node [
    id 644
    label "doznawa&#263;"
  ]
  node [
    id 645
    label "copulate"
  ]
  node [
    id 646
    label "&#380;y&#263;"
  ]
  node [
    id 647
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 648
    label "ukrzywdza&#263;"
  ]
  node [
    id 649
    label "niesprawiedliwy"
  ]
  node [
    id 650
    label "szkodzi&#263;"
  ]
  node [
    id 651
    label "wrong"
  ]
  node [
    id 652
    label "wierzga&#263;"
  ]
  node [
    id 653
    label "punkt"
  ]
  node [
    id 654
    label "krzywa"
  ]
  node [
    id 655
    label "odcinek"
  ]
  node [
    id 656
    label "straight_line"
  ]
  node [
    id 657
    label "czas"
  ]
  node [
    id 658
    label "trasa"
  ]
  node [
    id 659
    label "proste_sko&#347;ne"
  ]
  node [
    id 660
    label "figura_geometryczna"
  ]
  node [
    id 661
    label "linia"
  ]
  node [
    id 662
    label "poprowadzi&#263;"
  ]
  node [
    id 663
    label "prowadzi&#263;"
  ]
  node [
    id 664
    label "prowadzenie"
  ]
  node [
    id 665
    label "curvature"
  ]
  node [
    id 666
    label "curve"
  ]
  node [
    id 667
    label "poprzedzanie"
  ]
  node [
    id 668
    label "czasoprzestrze&#324;"
  ]
  node [
    id 669
    label "laba"
  ]
  node [
    id 670
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 671
    label "chronometria"
  ]
  node [
    id 672
    label "rachuba_czasu"
  ]
  node [
    id 673
    label "przep&#322;ywanie"
  ]
  node [
    id 674
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 675
    label "czasokres"
  ]
  node [
    id 676
    label "odczyt"
  ]
  node [
    id 677
    label "chwila"
  ]
  node [
    id 678
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 679
    label "dzieje"
  ]
  node [
    id 680
    label "kategoria_gramatyczna"
  ]
  node [
    id 681
    label "poprzedzenie"
  ]
  node [
    id 682
    label "trawienie"
  ]
  node [
    id 683
    label "pochodzi&#263;"
  ]
  node [
    id 684
    label "period"
  ]
  node [
    id 685
    label "okres_czasu"
  ]
  node [
    id 686
    label "poprzedza&#263;"
  ]
  node [
    id 687
    label "schy&#322;ek"
  ]
  node [
    id 688
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 689
    label "odwlekanie_si&#281;"
  ]
  node [
    id 690
    label "zegar"
  ]
  node [
    id 691
    label "czwarty_wymiar"
  ]
  node [
    id 692
    label "pochodzenie"
  ]
  node [
    id 693
    label "koniugacja"
  ]
  node [
    id 694
    label "Zeitgeist"
  ]
  node [
    id 695
    label "trawi&#263;"
  ]
  node [
    id 696
    label "pogoda"
  ]
  node [
    id 697
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 698
    label "poprzedzi&#263;"
  ]
  node [
    id 699
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 700
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 701
    label "time_period"
  ]
  node [
    id 702
    label "teren"
  ]
  node [
    id 703
    label "pole"
  ]
  node [
    id 704
    label "kawa&#322;ek"
  ]
  node [
    id 705
    label "part"
  ]
  node [
    id 706
    label "line"
  ]
  node [
    id 707
    label "coupon"
  ]
  node [
    id 708
    label "fragment"
  ]
  node [
    id 709
    label "pokwitowanie"
  ]
  node [
    id 710
    label "moneta"
  ]
  node [
    id 711
    label "epizod"
  ]
  node [
    id 712
    label "po&#322;o&#380;enie"
  ]
  node [
    id 713
    label "sprawa"
  ]
  node [
    id 714
    label "ust&#281;p"
  ]
  node [
    id 715
    label "plan"
  ]
  node [
    id 716
    label "obiekt_matematyczny"
  ]
  node [
    id 717
    label "problemat"
  ]
  node [
    id 718
    label "plamka"
  ]
  node [
    id 719
    label "stopie&#324;_pisma"
  ]
  node [
    id 720
    label "jednostka"
  ]
  node [
    id 721
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 722
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 723
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 724
    label "problematyka"
  ]
  node [
    id 725
    label "zapunktowa&#263;"
  ]
  node [
    id 726
    label "podpunkt"
  ]
  node [
    id 727
    label "wojsko"
  ]
  node [
    id 728
    label "kres"
  ]
  node [
    id 729
    label "point"
  ]
  node [
    id 730
    label "pozycja"
  ]
  node [
    id 731
    label "droga"
  ]
  node [
    id 732
    label "przebieg"
  ]
  node [
    id 733
    label "infrastruktura"
  ]
  node [
    id 734
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 735
    label "w&#281;ze&#322;"
  ]
  node [
    id 736
    label "marszrutyzacja"
  ]
  node [
    id 737
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 738
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 739
    label "podbieg"
  ]
  node [
    id 740
    label "czynnik"
  ]
  node [
    id 741
    label "&#322;a&#324;cuch"
  ]
  node [
    id 742
    label "k&#243;&#322;ko"
  ]
  node [
    id 743
    label "cell"
  ]
  node [
    id 744
    label "filia"
  ]
  node [
    id 745
    label "kontakt"
  ]
  node [
    id 746
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 747
    label "subject"
  ]
  node [
    id 748
    label "kamena"
  ]
  node [
    id 749
    label "&#347;wiadectwo"
  ]
  node [
    id 750
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 751
    label "matuszka"
  ]
  node [
    id 752
    label "pocz&#261;tek"
  ]
  node [
    id 753
    label "geneza"
  ]
  node [
    id 754
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 755
    label "bra&#263;_si&#281;"
  ]
  node [
    id 756
    label "przyczyna"
  ]
  node [
    id 757
    label "poci&#261;ganie"
  ]
  node [
    id 758
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 759
    label "divisor"
  ]
  node [
    id 760
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 761
    label "faktor"
  ]
  node [
    id 762
    label "ekspozycja"
  ]
  node [
    id 763
    label "iloczyn"
  ]
  node [
    id 764
    label "agencja"
  ]
  node [
    id 765
    label "dzia&#322;"
  ]
  node [
    id 766
    label "Rzym_Zachodni"
  ]
  node [
    id 767
    label "whole"
  ]
  node [
    id 768
    label "Rzym_Wschodni"
  ]
  node [
    id 769
    label "urz&#261;dzenie"
  ]
  node [
    id 770
    label "ko&#322;o"
  ]
  node [
    id 771
    label "sphere"
  ]
  node [
    id 772
    label "znak_diakrytyczny"
  ]
  node [
    id 773
    label "zabawa"
  ]
  node [
    id 774
    label "ma&#322;y_przedmiot"
  ]
  node [
    id 775
    label "p&#243;&#322;kole"
  ]
  node [
    id 776
    label "grono"
  ]
  node [
    id 777
    label "communication"
  ]
  node [
    id 778
    label "styk"
  ]
  node [
    id 779
    label "&#322;&#261;cznik_mechanizmowy"
  ]
  node [
    id 780
    label "association"
  ]
  node [
    id 781
    label "&#322;&#261;cznik"
  ]
  node [
    id 782
    label "katalizator"
  ]
  node [
    id 783
    label "instalacja_elektryczna"
  ]
  node [
    id 784
    label "soczewka"
  ]
  node [
    id 785
    label "nasz_cz&#322;owiek"
  ]
  node [
    id 786
    label "formacja_geologiczna"
  ]
  node [
    id 787
    label "&#322;&#261;cznik_elektryczny"
  ]
  node [
    id 788
    label "linkage"
  ]
  node [
    id 789
    label "regulator"
  ]
  node [
    id 790
    label "z&#322;&#261;czenie"
  ]
  node [
    id 791
    label "zwi&#261;zek"
  ]
  node [
    id 792
    label "contact"
  ]
  node [
    id 793
    label "szpaler"
  ]
  node [
    id 794
    label "uporz&#261;dkowanie"
  ]
  node [
    id 795
    label "scope"
  ]
  node [
    id 796
    label "ci&#261;g"
  ]
  node [
    id 797
    label "tract"
  ]
  node [
    id 798
    label "ozdoba"
  ]
  node [
    id 799
    label "uwi&#281;&#378;"
  ]
  node [
    id 800
    label "galwanicznie"
  ]
  node [
    id 801
    label "specjalny"
  ]
  node [
    id 802
    label "intencjonalny"
  ]
  node [
    id 803
    label "op&#243;&#378;niony_w_rozwoju"
  ]
  node [
    id 804
    label "niedorozw&#243;j"
  ]
  node [
    id 805
    label "szczeg&#243;lny"
  ]
  node [
    id 806
    label "specjalnie"
  ]
  node [
    id 807
    label "nieetatowy"
  ]
  node [
    id 808
    label "upo&#347;ledzony_umys&#322;owo"
  ]
  node [
    id 809
    label "nienormalny"
  ]
  node [
    id 810
    label "umy&#347;lnie"
  ]
  node [
    id 811
    label "odpowiedni"
  ]
  node [
    id 812
    label "niepe&#322;nosprawno&#347;&#263;"
  ]
  node [
    id 813
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 814
    label "represent"
  ]
  node [
    id 815
    label "testify"
  ]
  node [
    id 816
    label "przedstawi&#263;"
  ]
  node [
    id 817
    label "poda&#263;"
  ]
  node [
    id 818
    label "poinformowa&#263;"
  ]
  node [
    id 819
    label "udowodni&#263;"
  ]
  node [
    id 820
    label "spowodowa&#263;"
  ]
  node [
    id 821
    label "wyrazi&#263;"
  ]
  node [
    id 822
    label "przeszkoli&#263;"
  ]
  node [
    id 823
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 824
    label "indicate"
  ]
  node [
    id 825
    label "pom&#243;c"
  ]
  node [
    id 826
    label "inform"
  ]
  node [
    id 827
    label "zakomunikowa&#263;"
  ]
  node [
    id 828
    label "uzasadni&#263;"
  ]
  node [
    id 829
    label "oznaczy&#263;"
  ]
  node [
    id 830
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 831
    label "tenis"
  ]
  node [
    id 832
    label "supply"
  ]
  node [
    id 833
    label "da&#263;"
  ]
  node [
    id 834
    label "ustawi&#263;"
  ]
  node [
    id 835
    label "siatk&#243;wka"
  ]
  node [
    id 836
    label "zagra&#263;"
  ]
  node [
    id 837
    label "introduce"
  ]
  node [
    id 838
    label "nafaszerowa&#263;"
  ]
  node [
    id 839
    label "zaserwowa&#263;"
  ]
  node [
    id 840
    label "ukaza&#263;"
  ]
  node [
    id 841
    label "przedstawienie"
  ]
  node [
    id 842
    label "zapozna&#263;"
  ]
  node [
    id 843
    label "express"
  ]
  node [
    id 844
    label "zaproponowa&#263;"
  ]
  node [
    id 845
    label "zademonstrowa&#263;"
  ]
  node [
    id 846
    label "typify"
  ]
  node [
    id 847
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 848
    label "opisa&#263;"
  ]
  node [
    id 849
    label "ciastko"
  ]
  node [
    id 850
    label "wyr&#243;b_cukierniczy"
  ]
  node [
    id 851
    label "wypiek"
  ]
  node [
    id 852
    label "&#322;ako&#263;"
  ]
  node [
    id 853
    label "gotowy"
  ]
  node [
    id 854
    label "might"
  ]
  node [
    id 855
    label "uprawi&#263;"
  ]
  node [
    id 856
    label "public_treasury"
  ]
  node [
    id 857
    label "obrobi&#263;"
  ]
  node [
    id 858
    label "nietrze&#378;wy"
  ]
  node [
    id 859
    label "czekanie"
  ]
  node [
    id 860
    label "martwy"
  ]
  node [
    id 861
    label "bliski"
  ]
  node [
    id 862
    label "gotowo"
  ]
  node [
    id 863
    label "przygotowywanie"
  ]
  node [
    id 864
    label "przygotowanie"
  ]
  node [
    id 865
    label "dyspozycyjny"
  ]
  node [
    id 866
    label "zalany"
  ]
  node [
    id 867
    label "nieuchronny"
  ]
  node [
    id 868
    label "doj&#347;cie"
  ]
  node [
    id 869
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 870
    label "mie&#263;_miejsce"
  ]
  node [
    id 871
    label "equal"
  ]
  node [
    id 872
    label "trwa&#263;"
  ]
  node [
    id 873
    label "chodzi&#263;"
  ]
  node [
    id 874
    label "si&#281;ga&#263;"
  ]
  node [
    id 875
    label "stan"
  ]
  node [
    id 876
    label "obecno&#347;&#263;"
  ]
  node [
    id 877
    label "stand"
  ]
  node [
    id 878
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 879
    label "uczestniczy&#263;"
  ]
  node [
    id 880
    label "participate"
  ]
  node [
    id 881
    label "istnie&#263;"
  ]
  node [
    id 882
    label "pozostawa&#263;"
  ]
  node [
    id 883
    label "zostawa&#263;"
  ]
  node [
    id 884
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 885
    label "adhere"
  ]
  node [
    id 886
    label "compass"
  ]
  node [
    id 887
    label "appreciation"
  ]
  node [
    id 888
    label "osi&#261;ga&#263;"
  ]
  node [
    id 889
    label "dociera&#263;"
  ]
  node [
    id 890
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 891
    label "mierzy&#263;"
  ]
  node [
    id 892
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 893
    label "exsert"
  ]
  node [
    id 894
    label "being"
  ]
  node [
    id 895
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 896
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 897
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 898
    label "p&#322;ywa&#263;"
  ]
  node [
    id 899
    label "run"
  ]
  node [
    id 900
    label "bangla&#263;"
  ]
  node [
    id 901
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 902
    label "przebiega&#263;"
  ]
  node [
    id 903
    label "wk&#322;ada&#263;"
  ]
  node [
    id 904
    label "proceed"
  ]
  node [
    id 905
    label "bywa&#263;"
  ]
  node [
    id 906
    label "dziama&#263;"
  ]
  node [
    id 907
    label "stara&#263;_si&#281;"
  ]
  node [
    id 908
    label "para"
  ]
  node [
    id 909
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 910
    label "str&#243;j"
  ]
  node [
    id 911
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 912
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 913
    label "krok"
  ]
  node [
    id 914
    label "tryb"
  ]
  node [
    id 915
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 916
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 917
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 918
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 919
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 920
    label "continue"
  ]
  node [
    id 921
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 922
    label "Ohio"
  ]
  node [
    id 923
    label "wci&#281;cie"
  ]
  node [
    id 924
    label "Nowy_York"
  ]
  node [
    id 925
    label "warstwa"
  ]
  node [
    id 926
    label "samopoczucie"
  ]
  node [
    id 927
    label "Illinois"
  ]
  node [
    id 928
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 929
    label "state"
  ]
  node [
    id 930
    label "Jukatan"
  ]
  node [
    id 931
    label "Kalifornia"
  ]
  node [
    id 932
    label "Wirginia"
  ]
  node [
    id 933
    label "wektor"
  ]
  node [
    id 934
    label "Goa"
  ]
  node [
    id 935
    label "Teksas"
  ]
  node [
    id 936
    label "Waszyngton"
  ]
  node [
    id 937
    label "Massachusetts"
  ]
  node [
    id 938
    label "Alaska"
  ]
  node [
    id 939
    label "Arakan"
  ]
  node [
    id 940
    label "Hawaje"
  ]
  node [
    id 941
    label "Maryland"
  ]
  node [
    id 942
    label "Michigan"
  ]
  node [
    id 943
    label "Arizona"
  ]
  node [
    id 944
    label "Georgia"
  ]
  node [
    id 945
    label "poziom"
  ]
  node [
    id 946
    label "Pensylwania"
  ]
  node [
    id 947
    label "shape"
  ]
  node [
    id 948
    label "Luizjana"
  ]
  node [
    id 949
    label "Nowy_Meksyk"
  ]
  node [
    id 950
    label "Alabama"
  ]
  node [
    id 951
    label "Kansas"
  ]
  node [
    id 952
    label "Oregon"
  ]
  node [
    id 953
    label "Oklahoma"
  ]
  node [
    id 954
    label "Floryda"
  ]
  node [
    id 955
    label "jednostka_administracyjna"
  ]
  node [
    id 956
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 957
    label "zupe&#322;ny"
  ]
  node [
    id 958
    label "wniwecz"
  ]
  node [
    id 959
    label "zupe&#322;nie"
  ]
  node [
    id 960
    label "og&#243;lnie"
  ]
  node [
    id 961
    label "w_pizdu"
  ]
  node [
    id 962
    label "ca&#322;y"
  ]
  node [
    id 963
    label "&#322;&#261;czny"
  ]
  node [
    id 964
    label "pe&#322;ny"
  ]
  node [
    id 965
    label "wydajnie"
  ]
  node [
    id 966
    label "efektywny"
  ]
  node [
    id 967
    label "nasilony"
  ]
  node [
    id 968
    label "bogaty"
  ]
  node [
    id 969
    label "obfituj&#261;cy"
  ]
  node [
    id 970
    label "nabab"
  ]
  node [
    id 971
    label "r&#243;&#380;norodny"
  ]
  node [
    id 972
    label "spania&#322;y"
  ]
  node [
    id 973
    label "obficie"
  ]
  node [
    id 974
    label "sytuowany"
  ]
  node [
    id 975
    label "och&#281;do&#380;ny"
  ]
  node [
    id 976
    label "forsiasty"
  ]
  node [
    id 977
    label "zapa&#347;ny"
  ]
  node [
    id 978
    label "bogato"
  ]
  node [
    id 979
    label "dobroczynny"
  ]
  node [
    id 980
    label "czw&#243;rka"
  ]
  node [
    id 981
    label "spokojny"
  ]
  node [
    id 982
    label "skuteczny"
  ]
  node [
    id 983
    label "&#347;mieszny"
  ]
  node [
    id 984
    label "mi&#322;y"
  ]
  node [
    id 985
    label "grzeczny"
  ]
  node [
    id 986
    label "powitanie"
  ]
  node [
    id 987
    label "dobrze"
  ]
  node [
    id 988
    label "zwrot"
  ]
  node [
    id 989
    label "pomy&#347;lny"
  ]
  node [
    id 990
    label "moralny"
  ]
  node [
    id 991
    label "drogi"
  ]
  node [
    id 992
    label "pozytywny"
  ]
  node [
    id 993
    label "korzystny"
  ]
  node [
    id 994
    label "pos&#322;uszny"
  ]
  node [
    id 995
    label "efektywnie"
  ]
  node [
    id 996
    label "sprawny"
  ]
  node [
    id 997
    label "dow&#243;d"
  ]
  node [
    id 998
    label "o&#347;wiadczenie"
  ]
  node [
    id 999
    label "za&#347;wiadczenie"
  ]
  node [
    id 1000
    label "certificate"
  ]
  node [
    id 1001
    label "promocja"
  ]
  node [
    id 1002
    label "dokument"
  ]
  node [
    id 1003
    label "pierworodztwo"
  ]
  node [
    id 1004
    label "faza"
  ]
  node [
    id 1005
    label "upgrade"
  ]
  node [
    id 1006
    label "nast&#281;pstwo"
  ]
  node [
    id 1007
    label "nimfa"
  ]
  node [
    id 1008
    label "wieszczka"
  ]
  node [
    id 1009
    label "rzymski"
  ]
  node [
    id 1010
    label "Egeria"
  ]
  node [
    id 1011
    label "upicie"
  ]
  node [
    id 1012
    label "pull"
  ]
  node [
    id 1013
    label "move"
  ]
  node [
    id 1014
    label "ruszenie"
  ]
  node [
    id 1015
    label "wyszarpanie"
  ]
  node [
    id 1016
    label "pokrycie"
  ]
  node [
    id 1017
    label "myk"
  ]
  node [
    id 1018
    label "wywo&#322;anie"
  ]
  node [
    id 1019
    label "si&#261;kanie"
  ]
  node [
    id 1020
    label "zainstalowanie"
  ]
  node [
    id 1021
    label "przechylenie"
  ]
  node [
    id 1022
    label "przesuni&#281;cie"
  ]
  node [
    id 1023
    label "zaci&#261;ganie"
  ]
  node [
    id 1024
    label "wessanie"
  ]
  node [
    id 1025
    label "powianie"
  ]
  node [
    id 1026
    label "posuni&#281;cie"
  ]
  node [
    id 1027
    label "p&#243;j&#347;cie"
  ]
  node [
    id 1028
    label "nos"
  ]
  node [
    id 1029
    label "przy&#322;&#261;czenie"
  ]
  node [
    id 1030
    label "zaci&#261;gni&#281;cie"
  ]
  node [
    id 1031
    label "dzia&#322;anie"
  ]
  node [
    id 1032
    label "typ"
  ]
  node [
    id 1033
    label "event"
  ]
  node [
    id 1034
    label "implikacja"
  ]
  node [
    id 1035
    label "powodowanie"
  ]
  node [
    id 1036
    label "powiewanie"
  ]
  node [
    id 1037
    label "powleczenie"
  ]
  node [
    id 1038
    label "interesowanie"
  ]
  node [
    id 1039
    label "manienie"
  ]
  node [
    id 1040
    label "upijanie"
  ]
  node [
    id 1041
    label "po&#347;ci&#261;ganie"
  ]
  node [
    id 1042
    label "przechylanie"
  ]
  node [
    id 1043
    label "temptation"
  ]
  node [
    id 1044
    label "pokrywanie"
  ]
  node [
    id 1045
    label "oddzieranie"
  ]
  node [
    id 1046
    label "dzianie_si&#281;"
  ]
  node [
    id 1047
    label "urwanie"
  ]
  node [
    id 1048
    label "oddarcie"
  ]
  node [
    id 1049
    label "przesuwanie"
  ]
  node [
    id 1050
    label "zerwanie"
  ]
  node [
    id 1051
    label "ruszanie"
  ]
  node [
    id 1052
    label "traction"
  ]
  node [
    id 1053
    label "urywanie"
  ]
  node [
    id 1054
    label "powlekanie"
  ]
  node [
    id 1055
    label "wsysanie"
  ]
  node [
    id 1056
    label "popadia"
  ]
  node [
    id 1057
    label "ojczyzna"
  ]
  node [
    id 1058
    label "zesp&#243;&#322;"
  ]
  node [
    id 1059
    label "rodny"
  ]
  node [
    id 1060
    label "powstanie"
  ]
  node [
    id 1061
    label "monogeneza"
  ]
  node [
    id 1062
    label "zaistnienie"
  ]
  node [
    id 1063
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 1064
    label "przep&#322;yw"
  ]
  node [
    id 1065
    label "ideologia"
  ]
  node [
    id 1066
    label "apparent_motion"
  ]
  node [
    id 1067
    label "przyp&#322;yw"
  ]
  node [
    id 1068
    label "metoda"
  ]
  node [
    id 1069
    label "electricity"
  ]
  node [
    id 1070
    label "dreszcz"
  ]
  node [
    id 1071
    label "ruch"
  ]
  node [
    id 1072
    label "praktyka"
  ]
  node [
    id 1073
    label "system"
  ]
  node [
    id 1074
    label "flow"
  ]
  node [
    id 1075
    label "p&#322;yw"
  ]
  node [
    id 1076
    label "wzrost"
  ]
  node [
    id 1077
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1078
    label "fit"
  ]
  node [
    id 1079
    label "reakcja"
  ]
  node [
    id 1080
    label "flux"
  ]
  node [
    id 1081
    label "j&#261;dro"
  ]
  node [
    id 1082
    label "systemik"
  ]
  node [
    id 1083
    label "rozprz&#261;c"
  ]
  node [
    id 1084
    label "oprogramowanie"
  ]
  node [
    id 1085
    label "systemat"
  ]
  node [
    id 1086
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1087
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1088
    label "model"
  ]
  node [
    id 1089
    label "struktura"
  ]
  node [
    id 1090
    label "usenet"
  ]
  node [
    id 1091
    label "s&#261;d"
  ]
  node [
    id 1092
    label "porz&#261;dek"
  ]
  node [
    id 1093
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1094
    label "przyn&#281;ta"
  ]
  node [
    id 1095
    label "p&#322;&#243;d"
  ]
  node [
    id 1096
    label "net"
  ]
  node [
    id 1097
    label "w&#281;dkarstwo"
  ]
  node [
    id 1098
    label "eratem"
  ]
  node [
    id 1099
    label "oddzia&#322;"
  ]
  node [
    id 1100
    label "doktryna"
  ]
  node [
    id 1101
    label "pulpit"
  ]
  node [
    id 1102
    label "konstelacja"
  ]
  node [
    id 1103
    label "jednostka_geologiczna"
  ]
  node [
    id 1104
    label "o&#347;"
  ]
  node [
    id 1105
    label "podsystem"
  ]
  node [
    id 1106
    label "ryba"
  ]
  node [
    id 1107
    label "Leopard"
  ]
  node [
    id 1108
    label "spos&#243;b"
  ]
  node [
    id 1109
    label "Android"
  ]
  node [
    id 1110
    label "zachowanie"
  ]
  node [
    id 1111
    label "cybernetyk"
  ]
  node [
    id 1112
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 1113
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 1114
    label "method"
  ]
  node [
    id 1115
    label "sk&#322;ad"
  ]
  node [
    id 1116
    label "podstawa"
  ]
  node [
    id 1117
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1118
    label "practice"
  ]
  node [
    id 1119
    label "wiedza"
  ]
  node [
    id 1120
    label "znawstwo"
  ]
  node [
    id 1121
    label "skill"
  ]
  node [
    id 1122
    label "czyn"
  ]
  node [
    id 1123
    label "nauka"
  ]
  node [
    id 1124
    label "eksperiencja"
  ]
  node [
    id 1125
    label "mechanika"
  ]
  node [
    id 1126
    label "utrzymywanie"
  ]
  node [
    id 1127
    label "poruszenie"
  ]
  node [
    id 1128
    label "movement"
  ]
  node [
    id 1129
    label "utrzyma&#263;"
  ]
  node [
    id 1130
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 1131
    label "utrzymanie"
  ]
  node [
    id 1132
    label "travel"
  ]
  node [
    id 1133
    label "kanciasty"
  ]
  node [
    id 1134
    label "commercial_enterprise"
  ]
  node [
    id 1135
    label "strumie&#324;"
  ]
  node [
    id 1136
    label "aktywno&#347;&#263;"
  ]
  node [
    id 1137
    label "kr&#243;tki"
  ]
  node [
    id 1138
    label "taktyka"
  ]
  node [
    id 1139
    label "apraksja"
  ]
  node [
    id 1140
    label "natural_process"
  ]
  node [
    id 1141
    label "utrzymywa&#263;"
  ]
  node [
    id 1142
    label "d&#322;ugi"
  ]
  node [
    id 1143
    label "dyssypacja_energii"
  ]
  node [
    id 1144
    label "tumult"
  ]
  node [
    id 1145
    label "stopek"
  ]
  node [
    id 1146
    label "manewr"
  ]
  node [
    id 1147
    label "lokomocja"
  ]
  node [
    id 1148
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 1149
    label "komunikacja"
  ]
  node [
    id 1150
    label "drift"
  ]
  node [
    id 1151
    label "oznaka"
  ]
  node [
    id 1152
    label "doznanie"
  ]
  node [
    id 1153
    label "throb"
  ]
  node [
    id 1154
    label "ci&#261;goty"
  ]
  node [
    id 1155
    label "political_orientation"
  ]
  node [
    id 1156
    label "idea"
  ]
  node [
    id 1157
    label "szko&#322;a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 389
  ]
  edge [
    source 7
    target 390
  ]
  edge [
    source 7
    target 391
  ]
  edge [
    source 7
    target 392
  ]
  edge [
    source 7
    target 393
  ]
  edge [
    source 7
    target 394
  ]
  edge [
    source 7
    target 395
  ]
  edge [
    source 7
    target 396
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 21
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 9
    target 398
  ]
  edge [
    source 9
    target 399
  ]
  edge [
    source 9
    target 400
  ]
  edge [
    source 9
    target 401
  ]
  edge [
    source 9
    target 402
  ]
  edge [
    source 9
    target 403
  ]
  edge [
    source 9
    target 404
  ]
  edge [
    source 9
    target 405
  ]
  edge [
    source 9
    target 406
  ]
  edge [
    source 9
    target 407
  ]
  edge [
    source 9
    target 408
  ]
  edge [
    source 9
    target 409
  ]
  edge [
    source 9
    target 410
  ]
  edge [
    source 9
    target 411
  ]
  edge [
    source 9
    target 412
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 414
  ]
  edge [
    source 9
    target 415
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 417
  ]
  edge [
    source 9
    target 418
  ]
  edge [
    source 9
    target 419
  ]
  edge [
    source 9
    target 420
  ]
  edge [
    source 9
    target 421
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 437
  ]
  edge [
    source 12
    target 438
  ]
  edge [
    source 12
    target 439
  ]
  edge [
    source 12
    target 440
  ]
  edge [
    source 12
    target 441
  ]
  edge [
    source 12
    target 442
  ]
  edge [
    source 12
    target 443
  ]
  edge [
    source 12
    target 444
  ]
  edge [
    source 12
    target 445
  ]
  edge [
    source 12
    target 446
  ]
  edge [
    source 12
    target 447
  ]
  edge [
    source 12
    target 448
  ]
  edge [
    source 12
    target 449
  ]
  edge [
    source 12
    target 450
  ]
  edge [
    source 12
    target 451
  ]
  edge [
    source 12
    target 452
  ]
  edge [
    source 12
    target 453
  ]
  edge [
    source 12
    target 454
  ]
  edge [
    source 12
    target 455
  ]
  edge [
    source 12
    target 456
  ]
  edge [
    source 12
    target 363
  ]
  edge [
    source 12
    target 457
  ]
  edge [
    source 12
    target 250
  ]
  edge [
    source 12
    target 458
  ]
  edge [
    source 12
    target 270
  ]
  edge [
    source 12
    target 459
  ]
  edge [
    source 12
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 472
  ]
  edge [
    source 13
    target 473
  ]
  edge [
    source 13
    target 474
  ]
  edge [
    source 13
    target 475
  ]
  edge [
    source 13
    target 476
  ]
  edge [
    source 13
    target 227
  ]
  edge [
    source 13
    target 477
  ]
  edge [
    source 13
    target 478
  ]
  edge [
    source 13
    target 479
  ]
  edge [
    source 13
    target 480
  ]
  edge [
    source 13
    target 481
  ]
  edge [
    source 13
    target 482
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 483
  ]
  edge [
    source 14
    target 484
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 485
  ]
  edge [
    source 14
    target 486
  ]
  edge [
    source 14
    target 487
  ]
  edge [
    source 14
    target 488
  ]
  edge [
    source 14
    target 489
  ]
  edge [
    source 14
    target 286
  ]
  edge [
    source 14
    target 490
  ]
  edge [
    source 14
    target 491
  ]
  edge [
    source 14
    target 492
  ]
  edge [
    source 14
    target 493
  ]
  edge [
    source 14
    target 494
  ]
  edge [
    source 14
    target 495
  ]
  edge [
    source 14
    target 496
  ]
  edge [
    source 14
    target 497
  ]
  edge [
    source 14
    target 498
  ]
  edge [
    source 14
    target 499
  ]
  edge [
    source 14
    target 500
  ]
  edge [
    source 14
    target 501
  ]
  edge [
    source 14
    target 502
  ]
  edge [
    source 14
    target 503
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 505
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 506
  ]
  edge [
    source 15
    target 507
  ]
  edge [
    source 15
    target 508
  ]
  edge [
    source 15
    target 485
  ]
  edge [
    source 15
    target 509
  ]
  edge [
    source 15
    target 510
  ]
  edge [
    source 15
    target 511
  ]
  edge [
    source 15
    target 484
  ]
  edge [
    source 15
    target 483
  ]
  edge [
    source 15
    target 512
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 513
  ]
  edge [
    source 15
    target 492
  ]
  edge [
    source 15
    target 493
  ]
  edge [
    source 15
    target 494
  ]
  edge [
    source 15
    target 495
  ]
  edge [
    source 15
    target 496
  ]
  edge [
    source 15
    target 497
  ]
  edge [
    source 15
    target 498
  ]
  edge [
    source 15
    target 499
  ]
  edge [
    source 15
    target 500
  ]
  edge [
    source 15
    target 501
  ]
  edge [
    source 15
    target 502
  ]
  edge [
    source 15
    target 503
  ]
  edge [
    source 15
    target 504
  ]
  edge [
    source 15
    target 505
  ]
  edge [
    source 15
    target 514
  ]
  edge [
    source 15
    target 515
  ]
  edge [
    source 15
    target 516
  ]
  edge [
    source 15
    target 517
  ]
  edge [
    source 15
    target 518
  ]
  edge [
    source 15
    target 519
  ]
  edge [
    source 15
    target 520
  ]
  edge [
    source 15
    target 521
  ]
  edge [
    source 15
    target 522
  ]
  edge [
    source 15
    target 523
  ]
  edge [
    source 15
    target 524
  ]
  edge [
    source 15
    target 525
  ]
  edge [
    source 15
    target 526
  ]
  edge [
    source 15
    target 527
  ]
  edge [
    source 15
    target 528
  ]
  edge [
    source 15
    target 529
  ]
  edge [
    source 15
    target 530
  ]
  edge [
    source 15
    target 531
  ]
  edge [
    source 15
    target 532
  ]
  edge [
    source 15
    target 533
  ]
  edge [
    source 15
    target 534
  ]
  edge [
    source 15
    target 535
  ]
  edge [
    source 15
    target 536
  ]
  edge [
    source 15
    target 537
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 538
  ]
  edge [
    source 16
    target 539
  ]
  edge [
    source 16
    target 540
  ]
  edge [
    source 16
    target 541
  ]
  edge [
    source 16
    target 542
  ]
  edge [
    source 16
    target 543
  ]
  edge [
    source 16
    target 492
  ]
  edge [
    source 16
    target 544
  ]
  edge [
    source 16
    target 545
  ]
  edge [
    source 16
    target 546
  ]
  edge [
    source 16
    target 547
  ]
  edge [
    source 16
    target 548
  ]
  edge [
    source 16
    target 485
  ]
  edge [
    source 16
    target 549
  ]
  edge [
    source 16
    target 550
  ]
  edge [
    source 16
    target 551
  ]
  edge [
    source 16
    target 552
  ]
  edge [
    source 16
    target 553
  ]
  edge [
    source 16
    target 554
  ]
  edge [
    source 16
    target 555
  ]
  edge [
    source 16
    target 556
  ]
  edge [
    source 16
    target 557
  ]
  edge [
    source 16
    target 558
  ]
  edge [
    source 16
    target 559
  ]
  edge [
    source 16
    target 286
  ]
  edge [
    source 16
    target 560
  ]
  edge [
    source 16
    target 514
  ]
  edge [
    source 16
    target 561
  ]
  edge [
    source 16
    target 562
  ]
  edge [
    source 16
    target 563
  ]
  edge [
    source 16
    target 564
  ]
  edge [
    source 16
    target 565
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 566
  ]
  edge [
    source 17
    target 567
  ]
  edge [
    source 17
    target 568
  ]
  edge [
    source 17
    target 569
  ]
  edge [
    source 17
    target 570
  ]
  edge [
    source 17
    target 571
  ]
  edge [
    source 17
    target 572
  ]
  edge [
    source 17
    target 573
  ]
  edge [
    source 17
    target 374
  ]
  edge [
    source 17
    target 574
  ]
  edge [
    source 17
    target 575
  ]
  edge [
    source 17
    target 305
  ]
  edge [
    source 17
    target 576
  ]
  edge [
    source 17
    target 577
  ]
  edge [
    source 17
    target 578
  ]
  edge [
    source 17
    target 579
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 580
  ]
  edge [
    source 18
    target 581
  ]
  edge [
    source 18
    target 582
  ]
  edge [
    source 18
    target 583
  ]
  edge [
    source 18
    target 584
  ]
  edge [
    source 18
    target 585
  ]
  edge [
    source 18
    target 586
  ]
  edge [
    source 18
    target 587
  ]
  edge [
    source 18
    target 435
  ]
  edge [
    source 18
    target 588
  ]
  edge [
    source 18
    target 589
  ]
  edge [
    source 18
    target 590
  ]
  edge [
    source 18
    target 591
  ]
  edge [
    source 18
    target 592
  ]
  edge [
    source 18
    target 593
  ]
  edge [
    source 18
    target 594
  ]
  edge [
    source 18
    target 595
  ]
  edge [
    source 18
    target 596
  ]
  edge [
    source 18
    target 597
  ]
  edge [
    source 18
    target 598
  ]
  edge [
    source 18
    target 599
  ]
  edge [
    source 18
    target 600
  ]
  edge [
    source 18
    target 601
  ]
  edge [
    source 18
    target 602
  ]
  edge [
    source 18
    target 603
  ]
  edge [
    source 18
    target 604
  ]
  edge [
    source 18
    target 605
  ]
  edge [
    source 18
    target 606
  ]
  edge [
    source 18
    target 607
  ]
  edge [
    source 18
    target 608
  ]
  edge [
    source 18
    target 434
  ]
  edge [
    source 18
    target 609
  ]
  edge [
    source 18
    target 610
  ]
  edge [
    source 18
    target 611
  ]
  edge [
    source 18
    target 612
  ]
  edge [
    source 18
    target 613
  ]
  edge [
    source 18
    target 614
  ]
  edge [
    source 18
    target 615
  ]
  edge [
    source 18
    target 616
  ]
  edge [
    source 18
    target 617
  ]
  edge [
    source 18
    target 618
  ]
  edge [
    source 18
    target 619
  ]
  edge [
    source 18
    target 620
  ]
  edge [
    source 18
    target 621
  ]
  edge [
    source 18
    target 622
  ]
  edge [
    source 18
    target 623
  ]
  edge [
    source 18
    target 624
  ]
  edge [
    source 18
    target 625
  ]
  edge [
    source 18
    target 626
  ]
  edge [
    source 18
    target 627
  ]
  edge [
    source 18
    target 628
  ]
  edge [
    source 18
    target 629
  ]
  edge [
    source 18
    target 630
  ]
  edge [
    source 18
    target 631
  ]
  edge [
    source 18
    target 632
  ]
  edge [
    source 18
    target 633
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 634
  ]
  edge [
    source 19
    target 635
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 636
  ]
  edge [
    source 19
    target 637
  ]
  edge [
    source 19
    target 638
  ]
  edge [
    source 19
    target 639
  ]
  edge [
    source 19
    target 640
  ]
  edge [
    source 19
    target 641
  ]
  edge [
    source 19
    target 642
  ]
  edge [
    source 19
    target 643
  ]
  edge [
    source 19
    target 644
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 645
  ]
  edge [
    source 19
    target 646
  ]
  edge [
    source 19
    target 647
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 648
  ]
  edge [
    source 19
    target 649
  ]
  edge [
    source 19
    target 650
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 651
  ]
  edge [
    source 19
    target 652
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 653
  ]
  edge [
    source 20
    target 654
  ]
  edge [
    source 20
    target 655
  ]
  edge [
    source 20
    target 656
  ]
  edge [
    source 20
    target 657
  ]
  edge [
    source 20
    target 658
  ]
  edge [
    source 20
    target 659
  ]
  edge [
    source 20
    target 660
  ]
  edge [
    source 20
    target 661
  ]
  edge [
    source 20
    target 662
  ]
  edge [
    source 20
    target 663
  ]
  edge [
    source 20
    target 664
  ]
  edge [
    source 20
    target 665
  ]
  edge [
    source 20
    target 666
  ]
  edge [
    source 20
    target 667
  ]
  edge [
    source 20
    target 668
  ]
  edge [
    source 20
    target 669
  ]
  edge [
    source 20
    target 670
  ]
  edge [
    source 20
    target 671
  ]
  edge [
    source 20
    target 297
  ]
  edge [
    source 20
    target 672
  ]
  edge [
    source 20
    target 673
  ]
  edge [
    source 20
    target 674
  ]
  edge [
    source 20
    target 675
  ]
  edge [
    source 20
    target 676
  ]
  edge [
    source 20
    target 677
  ]
  edge [
    source 20
    target 678
  ]
  edge [
    source 20
    target 679
  ]
  edge [
    source 20
    target 680
  ]
  edge [
    source 20
    target 681
  ]
  edge [
    source 20
    target 682
  ]
  edge [
    source 20
    target 683
  ]
  edge [
    source 20
    target 684
  ]
  edge [
    source 20
    target 685
  ]
  edge [
    source 20
    target 686
  ]
  edge [
    source 20
    target 687
  ]
  edge [
    source 20
    target 688
  ]
  edge [
    source 20
    target 689
  ]
  edge [
    source 20
    target 690
  ]
  edge [
    source 20
    target 287
  ]
  edge [
    source 20
    target 691
  ]
  edge [
    source 20
    target 692
  ]
  edge [
    source 20
    target 693
  ]
  edge [
    source 20
    target 694
  ]
  edge [
    source 20
    target 695
  ]
  edge [
    source 20
    target 696
  ]
  edge [
    source 20
    target 697
  ]
  edge [
    source 20
    target 698
  ]
  edge [
    source 20
    target 699
  ]
  edge [
    source 20
    target 700
  ]
  edge [
    source 20
    target 701
  ]
  edge [
    source 20
    target 702
  ]
  edge [
    source 20
    target 703
  ]
  edge [
    source 20
    target 704
  ]
  edge [
    source 20
    target 705
  ]
  edge [
    source 20
    target 706
  ]
  edge [
    source 20
    target 707
  ]
  edge [
    source 20
    target 708
  ]
  edge [
    source 20
    target 709
  ]
  edge [
    source 20
    target 710
  ]
  edge [
    source 20
    target 396
  ]
  edge [
    source 20
    target 711
  ]
  edge [
    source 20
    target 712
  ]
  edge [
    source 20
    target 713
  ]
  edge [
    source 20
    target 714
  ]
  edge [
    source 20
    target 715
  ]
  edge [
    source 20
    target 716
  ]
  edge [
    source 20
    target 717
  ]
  edge [
    source 20
    target 718
  ]
  edge [
    source 20
    target 719
  ]
  edge [
    source 20
    target 720
  ]
  edge [
    source 20
    target 721
  ]
  edge [
    source 20
    target 55
  ]
  edge [
    source 20
    target 722
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 723
  ]
  edge [
    source 20
    target 724
  ]
  edge [
    source 20
    target 265
  ]
  edge [
    source 20
    target 725
  ]
  edge [
    source 20
    target 726
  ]
  edge [
    source 20
    target 727
  ]
  edge [
    source 20
    target 728
  ]
  edge [
    source 20
    target 78
  ]
  edge [
    source 20
    target 729
  ]
  edge [
    source 20
    target 730
  ]
  edge [
    source 20
    target 731
  ]
  edge [
    source 20
    target 732
  ]
  edge [
    source 20
    target 733
  ]
  edge [
    source 20
    target 734
  ]
  edge [
    source 20
    target 735
  ]
  edge [
    source 20
    target 736
  ]
  edge [
    source 20
    target 737
  ]
  edge [
    source 20
    target 738
  ]
  edge [
    source 20
    target 739
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 740
  ]
  edge [
    source 21
    target 33
  ]
  edge [
    source 21
    target 741
  ]
  edge [
    source 21
    target 742
  ]
  edge [
    source 21
    target 743
  ]
  edge [
    source 21
    target 396
  ]
  edge [
    source 21
    target 744
  ]
  edge [
    source 21
    target 745
  ]
  edge [
    source 21
    target 746
  ]
  edge [
    source 21
    target 747
  ]
  edge [
    source 21
    target 748
  ]
  edge [
    source 21
    target 749
  ]
  edge [
    source 21
    target 750
  ]
  edge [
    source 21
    target 53
  ]
  edge [
    source 21
    target 751
  ]
  edge [
    source 21
    target 752
  ]
  edge [
    source 21
    target 753
  ]
  edge [
    source 21
    target 454
  ]
  edge [
    source 21
    target 754
  ]
  edge [
    source 21
    target 755
  ]
  edge [
    source 21
    target 756
  ]
  edge [
    source 21
    target 757
  ]
  edge [
    source 21
    target 758
  ]
  edge [
    source 21
    target 759
  ]
  edge [
    source 21
    target 760
  ]
  edge [
    source 21
    target 761
  ]
  edge [
    source 21
    target 482
  ]
  edge [
    source 21
    target 762
  ]
  edge [
    source 21
    target 763
  ]
  edge [
    source 21
    target 764
  ]
  edge [
    source 21
    target 765
  ]
  edge [
    source 21
    target 766
  ]
  edge [
    source 21
    target 767
  ]
  edge [
    source 21
    target 572
  ]
  edge [
    source 21
    target 242
  ]
  edge [
    source 21
    target 768
  ]
  edge [
    source 21
    target 769
  ]
  edge [
    source 21
    target 770
  ]
  edge [
    source 21
    target 660
  ]
  edge [
    source 21
    target 771
  ]
  edge [
    source 21
    target 772
  ]
  edge [
    source 21
    target 773
  ]
  edge [
    source 21
    target 774
  ]
  edge [
    source 21
    target 775
  ]
  edge [
    source 21
    target 776
  ]
  edge [
    source 21
    target 777
  ]
  edge [
    source 21
    target 778
  ]
  edge [
    source 21
    target 315
  ]
  edge [
    source 21
    target 779
  ]
  edge [
    source 21
    target 780
  ]
  edge [
    source 21
    target 781
  ]
  edge [
    source 21
    target 782
  ]
  edge [
    source 21
    target 382
  ]
  edge [
    source 21
    target 783
  ]
  edge [
    source 21
    target 784
  ]
  edge [
    source 21
    target 785
  ]
  edge [
    source 21
    target 786
  ]
  edge [
    source 21
    target 787
  ]
  edge [
    source 21
    target 788
  ]
  edge [
    source 21
    target 789
  ]
  edge [
    source 21
    target 790
  ]
  edge [
    source 21
    target 791
  ]
  edge [
    source 21
    target 792
  ]
  edge [
    source 21
    target 793
  ]
  edge [
    source 21
    target 314
  ]
  edge [
    source 21
    target 794
  ]
  edge [
    source 21
    target 795
  ]
  edge [
    source 21
    target 723
  ]
  edge [
    source 21
    target 796
  ]
  edge [
    source 21
    target 797
  ]
  edge [
    source 21
    target 798
  ]
  edge [
    source 21
    target 799
  ]
  edge [
    source 22
    target 800
  ]
  edge [
    source 22
    target 801
  ]
  edge [
    source 22
    target 802
  ]
  edge [
    source 22
    target 803
  ]
  edge [
    source 22
    target 804
  ]
  edge [
    source 22
    target 805
  ]
  edge [
    source 22
    target 806
  ]
  edge [
    source 22
    target 807
  ]
  edge [
    source 22
    target 808
  ]
  edge [
    source 22
    target 809
  ]
  edge [
    source 22
    target 810
  ]
  edge [
    source 22
    target 811
  ]
  edge [
    source 22
    target 812
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 813
  ]
  edge [
    source 24
    target 814
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 815
  ]
  edge [
    source 26
    target 729
  ]
  edge [
    source 26
    target 816
  ]
  edge [
    source 26
    target 817
  ]
  edge [
    source 26
    target 818
  ]
  edge [
    source 26
    target 819
  ]
  edge [
    source 26
    target 820
  ]
  edge [
    source 26
    target 821
  ]
  edge [
    source 26
    target 822
  ]
  edge [
    source 26
    target 823
  ]
  edge [
    source 26
    target 824
  ]
  edge [
    source 26
    target 825
  ]
  edge [
    source 26
    target 826
  ]
  edge [
    source 26
    target 827
  ]
  edge [
    source 26
    target 828
  ]
  edge [
    source 26
    target 514
  ]
  edge [
    source 26
    target 829
  ]
  edge [
    source 26
    target 830
  ]
  edge [
    source 26
    target 59
  ]
  edge [
    source 26
    target 361
  ]
  edge [
    source 26
    target 831
  ]
  edge [
    source 26
    target 832
  ]
  edge [
    source 26
    target 833
  ]
  edge [
    source 26
    target 834
  ]
  edge [
    source 26
    target 835
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 26
    target 836
  ]
  edge [
    source 26
    target 95
  ]
  edge [
    source 26
    target 837
  ]
  edge [
    source 26
    target 838
  ]
  edge [
    source 26
    target 839
  ]
  edge [
    source 26
    target 840
  ]
  edge [
    source 26
    target 841
  ]
  edge [
    source 26
    target 842
  ]
  edge [
    source 26
    target 843
  ]
  edge [
    source 26
    target 814
  ]
  edge [
    source 26
    target 844
  ]
  edge [
    source 26
    target 845
  ]
  edge [
    source 26
    target 846
  ]
  edge [
    source 26
    target 847
  ]
  edge [
    source 26
    target 848
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 849
  ]
  edge [
    source 28
    target 850
  ]
  edge [
    source 28
    target 851
  ]
  edge [
    source 28
    target 852
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 853
  ]
  edge [
    source 29
    target 854
  ]
  edge [
    source 29
    target 855
  ]
  edge [
    source 29
    target 856
  ]
  edge [
    source 29
    target 703
  ]
  edge [
    source 29
    target 857
  ]
  edge [
    source 29
    target 858
  ]
  edge [
    source 29
    target 859
  ]
  edge [
    source 29
    target 860
  ]
  edge [
    source 29
    target 861
  ]
  edge [
    source 29
    target 862
  ]
  edge [
    source 29
    target 863
  ]
  edge [
    source 29
    target 864
  ]
  edge [
    source 29
    target 865
  ]
  edge [
    source 29
    target 866
  ]
  edge [
    source 29
    target 867
  ]
  edge [
    source 29
    target 868
  ]
  edge [
    source 29
    target 869
  ]
  edge [
    source 29
    target 870
  ]
  edge [
    source 29
    target 871
  ]
  edge [
    source 29
    target 872
  ]
  edge [
    source 29
    target 873
  ]
  edge [
    source 29
    target 874
  ]
  edge [
    source 29
    target 875
  ]
  edge [
    source 29
    target 876
  ]
  edge [
    source 29
    target 877
  ]
  edge [
    source 29
    target 878
  ]
  edge [
    source 29
    target 879
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 869
  ]
  edge [
    source 30
    target 870
  ]
  edge [
    source 30
    target 871
  ]
  edge [
    source 30
    target 872
  ]
  edge [
    source 30
    target 873
  ]
  edge [
    source 30
    target 874
  ]
  edge [
    source 30
    target 875
  ]
  edge [
    source 30
    target 876
  ]
  edge [
    source 30
    target 877
  ]
  edge [
    source 30
    target 878
  ]
  edge [
    source 30
    target 879
  ]
  edge [
    source 30
    target 880
  ]
  edge [
    source 30
    target 163
  ]
  edge [
    source 30
    target 881
  ]
  edge [
    source 30
    target 882
  ]
  edge [
    source 30
    target 883
  ]
  edge [
    source 30
    target 884
  ]
  edge [
    source 30
    target 885
  ]
  edge [
    source 30
    target 886
  ]
  edge [
    source 30
    target 634
  ]
  edge [
    source 30
    target 887
  ]
  edge [
    source 30
    target 888
  ]
  edge [
    source 30
    target 889
  ]
  edge [
    source 30
    target 170
  ]
  edge [
    source 30
    target 890
  ]
  edge [
    source 30
    target 891
  ]
  edge [
    source 30
    target 637
  ]
  edge [
    source 30
    target 297
  ]
  edge [
    source 30
    target 892
  ]
  edge [
    source 30
    target 893
  ]
  edge [
    source 30
    target 894
  ]
  edge [
    source 30
    target 895
  ]
  edge [
    source 30
    target 107
  ]
  edge [
    source 30
    target 896
  ]
  edge [
    source 30
    target 897
  ]
  edge [
    source 30
    target 898
  ]
  edge [
    source 30
    target 899
  ]
  edge [
    source 30
    target 900
  ]
  edge [
    source 30
    target 901
  ]
  edge [
    source 30
    target 902
  ]
  edge [
    source 30
    target 903
  ]
  edge [
    source 30
    target 904
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 369
  ]
  edge [
    source 30
    target 905
  ]
  edge [
    source 30
    target 906
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 907
  ]
  edge [
    source 30
    target 908
  ]
  edge [
    source 30
    target 909
  ]
  edge [
    source 30
    target 910
  ]
  edge [
    source 30
    target 911
  ]
  edge [
    source 30
    target 912
  ]
  edge [
    source 30
    target 913
  ]
  edge [
    source 30
    target 914
  ]
  edge [
    source 30
    target 915
  ]
  edge [
    source 30
    target 916
  ]
  edge [
    source 30
    target 917
  ]
  edge [
    source 30
    target 918
  ]
  edge [
    source 30
    target 919
  ]
  edge [
    source 30
    target 920
  ]
  edge [
    source 30
    target 921
  ]
  edge [
    source 30
    target 922
  ]
  edge [
    source 30
    target 923
  ]
  edge [
    source 30
    target 924
  ]
  edge [
    source 30
    target 925
  ]
  edge [
    source 30
    target 926
  ]
  edge [
    source 30
    target 927
  ]
  edge [
    source 30
    target 928
  ]
  edge [
    source 30
    target 929
  ]
  edge [
    source 30
    target 930
  ]
  edge [
    source 30
    target 931
  ]
  edge [
    source 30
    target 932
  ]
  edge [
    source 30
    target 933
  ]
  edge [
    source 30
    target 934
  ]
  edge [
    source 30
    target 935
  ]
  edge [
    source 30
    target 936
  ]
  edge [
    source 30
    target 55
  ]
  edge [
    source 30
    target 937
  ]
  edge [
    source 30
    target 938
  ]
  edge [
    source 30
    target 939
  ]
  edge [
    source 30
    target 940
  ]
  edge [
    source 30
    target 941
  ]
  edge [
    source 30
    target 653
  ]
  edge [
    source 30
    target 942
  ]
  edge [
    source 30
    target 943
  ]
  edge [
    source 30
    target 758
  ]
  edge [
    source 30
    target 944
  ]
  edge [
    source 30
    target 945
  ]
  edge [
    source 30
    target 946
  ]
  edge [
    source 30
    target 947
  ]
  edge [
    source 30
    target 948
  ]
  edge [
    source 30
    target 949
  ]
  edge [
    source 30
    target 950
  ]
  edge [
    source 30
    target 572
  ]
  edge [
    source 30
    target 951
  ]
  edge [
    source 30
    target 952
  ]
  edge [
    source 30
    target 953
  ]
  edge [
    source 30
    target 954
  ]
  edge [
    source 30
    target 955
  ]
  edge [
    source 30
    target 956
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 957
  ]
  edge [
    source 31
    target 958
  ]
  edge [
    source 31
    target 959
  ]
  edge [
    source 31
    target 960
  ]
  edge [
    source 31
    target 961
  ]
  edge [
    source 31
    target 962
  ]
  edge [
    source 31
    target 625
  ]
  edge [
    source 31
    target 963
  ]
  edge [
    source 31
    target 964
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 965
  ]
  edge [
    source 32
    target 966
  ]
  edge [
    source 32
    target 967
  ]
  edge [
    source 32
    target 968
  ]
  edge [
    source 32
    target 436
  ]
  edge [
    source 32
    target 969
  ]
  edge [
    source 32
    target 970
  ]
  edge [
    source 32
    target 227
  ]
  edge [
    source 32
    target 971
  ]
  edge [
    source 32
    target 972
  ]
  edge [
    source 32
    target 973
  ]
  edge [
    source 32
    target 974
  ]
  edge [
    source 32
    target 975
  ]
  edge [
    source 32
    target 976
  ]
  edge [
    source 32
    target 977
  ]
  edge [
    source 32
    target 978
  ]
  edge [
    source 32
    target 979
  ]
  edge [
    source 32
    target 980
  ]
  edge [
    source 32
    target 981
  ]
  edge [
    source 32
    target 982
  ]
  edge [
    source 32
    target 983
  ]
  edge [
    source 32
    target 984
  ]
  edge [
    source 32
    target 985
  ]
  edge [
    source 32
    target 422
  ]
  edge [
    source 32
    target 986
  ]
  edge [
    source 32
    target 987
  ]
  edge [
    source 32
    target 962
  ]
  edge [
    source 32
    target 988
  ]
  edge [
    source 32
    target 989
  ]
  edge [
    source 32
    target 990
  ]
  edge [
    source 32
    target 991
  ]
  edge [
    source 32
    target 992
  ]
  edge [
    source 32
    target 811
  ]
  edge [
    source 32
    target 993
  ]
  edge [
    source 32
    target 994
  ]
  edge [
    source 32
    target 995
  ]
  edge [
    source 32
    target 996
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 746
  ]
  edge [
    source 33
    target 747
  ]
  edge [
    source 33
    target 748
  ]
  edge [
    source 33
    target 740
  ]
  edge [
    source 33
    target 749
  ]
  edge [
    source 33
    target 750
  ]
  edge [
    source 33
    target 53
  ]
  edge [
    source 33
    target 751
  ]
  edge [
    source 33
    target 752
  ]
  edge [
    source 33
    target 753
  ]
  edge [
    source 33
    target 454
  ]
  edge [
    source 33
    target 754
  ]
  edge [
    source 33
    target 755
  ]
  edge [
    source 33
    target 756
  ]
  edge [
    source 33
    target 757
  ]
  edge [
    source 33
    target 997
  ]
  edge [
    source 33
    target 998
  ]
  edge [
    source 33
    target 999
  ]
  edge [
    source 33
    target 1000
  ]
  edge [
    source 33
    target 1001
  ]
  edge [
    source 33
    target 1002
  ]
  edge [
    source 33
    target 1003
  ]
  edge [
    source 33
    target 1004
  ]
  edge [
    source 33
    target 55
  ]
  edge [
    source 33
    target 1005
  ]
  edge [
    source 33
    target 1006
  ]
  edge [
    source 33
    target 396
  ]
  edge [
    source 33
    target 758
  ]
  edge [
    source 33
    target 759
  ]
  edge [
    source 33
    target 760
  ]
  edge [
    source 33
    target 761
  ]
  edge [
    source 33
    target 482
  ]
  edge [
    source 33
    target 762
  ]
  edge [
    source 33
    target 763
  ]
  edge [
    source 33
    target 1007
  ]
  edge [
    source 33
    target 1008
  ]
  edge [
    source 33
    target 1009
  ]
  edge [
    source 33
    target 1010
  ]
  edge [
    source 33
    target 1011
  ]
  edge [
    source 33
    target 1012
  ]
  edge [
    source 33
    target 1013
  ]
  edge [
    source 33
    target 1014
  ]
  edge [
    source 33
    target 1015
  ]
  edge [
    source 33
    target 1016
  ]
  edge [
    source 33
    target 1017
  ]
  edge [
    source 33
    target 1018
  ]
  edge [
    source 33
    target 1019
  ]
  edge [
    source 33
    target 1020
  ]
  edge [
    source 33
    target 1021
  ]
  edge [
    source 33
    target 1022
  ]
  edge [
    source 33
    target 1023
  ]
  edge [
    source 33
    target 1024
  ]
  edge [
    source 33
    target 1025
  ]
  edge [
    source 33
    target 1026
  ]
  edge [
    source 33
    target 1027
  ]
  edge [
    source 33
    target 1028
  ]
  edge [
    source 33
    target 1029
  ]
  edge [
    source 33
    target 1030
  ]
  edge [
    source 33
    target 1031
  ]
  edge [
    source 33
    target 1032
  ]
  edge [
    source 33
    target 1033
  ]
  edge [
    source 33
    target 1034
  ]
  edge [
    source 33
    target 1035
  ]
  edge [
    source 33
    target 1036
  ]
  edge [
    source 33
    target 1037
  ]
  edge [
    source 33
    target 1038
  ]
  edge [
    source 33
    target 1039
  ]
  edge [
    source 33
    target 1040
  ]
  edge [
    source 33
    target 1041
  ]
  edge [
    source 33
    target 1042
  ]
  edge [
    source 33
    target 1043
  ]
  edge [
    source 33
    target 1044
  ]
  edge [
    source 33
    target 1045
  ]
  edge [
    source 33
    target 1046
  ]
  edge [
    source 33
    target 1047
  ]
  edge [
    source 33
    target 1048
  ]
  edge [
    source 33
    target 1049
  ]
  edge [
    source 33
    target 1050
  ]
  edge [
    source 33
    target 1051
  ]
  edge [
    source 33
    target 1052
  ]
  edge [
    source 33
    target 1053
  ]
  edge [
    source 33
    target 1054
  ]
  edge [
    source 33
    target 1055
  ]
  edge [
    source 33
    target 1056
  ]
  edge [
    source 33
    target 1057
  ]
  edge [
    source 33
    target 110
  ]
  edge [
    source 33
    target 1058
  ]
  edge [
    source 33
    target 1059
  ]
  edge [
    source 33
    target 1060
  ]
  edge [
    source 33
    target 1061
  ]
  edge [
    source 33
    target 1062
  ]
  edge [
    source 33
    target 169
  ]
  edge [
    source 33
    target 1063
  ]
  edge [
    source 34
    target 99
  ]
  edge [
    source 34
    target 1064
  ]
  edge [
    source 34
    target 1065
  ]
  edge [
    source 34
    target 1066
  ]
  edge [
    source 34
    target 1067
  ]
  edge [
    source 34
    target 1068
  ]
  edge [
    source 34
    target 1069
  ]
  edge [
    source 34
    target 1070
  ]
  edge [
    source 34
    target 1071
  ]
  edge [
    source 34
    target 106
  ]
  edge [
    source 34
    target 1072
  ]
  edge [
    source 34
    target 1073
  ]
  edge [
    source 34
    target 1074
  ]
  edge [
    source 34
    target 1075
  ]
  edge [
    source 34
    target 1076
  ]
  edge [
    source 34
    target 1077
  ]
  edge [
    source 34
    target 1078
  ]
  edge [
    source 34
    target 1079
  ]
  edge [
    source 34
    target 322
  ]
  edge [
    source 34
    target 1080
  ]
  edge [
    source 34
    target 98
  ]
  edge [
    source 34
    target 100
  ]
  edge [
    source 34
    target 101
  ]
  edge [
    source 34
    target 102
  ]
  edge [
    source 34
    target 103
  ]
  edge [
    source 34
    target 104
  ]
  edge [
    source 34
    target 105
  ]
  edge [
    source 34
    target 107
  ]
  edge [
    source 34
    target 108
  ]
  edge [
    source 34
    target 109
  ]
  edge [
    source 34
    target 110
  ]
  edge [
    source 34
    target 111
  ]
  edge [
    source 34
    target 112
  ]
  edge [
    source 34
    target 113
  ]
  edge [
    source 34
    target 114
  ]
  edge [
    source 34
    target 115
  ]
  edge [
    source 34
    target 116
  ]
  edge [
    source 34
    target 117
  ]
  edge [
    source 34
    target 118
  ]
  edge [
    source 34
    target 1081
  ]
  edge [
    source 34
    target 1082
  ]
  edge [
    source 34
    target 1083
  ]
  edge [
    source 34
    target 1084
  ]
  edge [
    source 34
    target 305
  ]
  edge [
    source 34
    target 1085
  ]
  edge [
    source 34
    target 1086
  ]
  edge [
    source 34
    target 256
  ]
  edge [
    source 34
    target 1087
  ]
  edge [
    source 34
    target 1088
  ]
  edge [
    source 34
    target 1089
  ]
  edge [
    source 34
    target 1090
  ]
  edge [
    source 34
    target 1091
  ]
  edge [
    source 34
    target 314
  ]
  edge [
    source 34
    target 1092
  ]
  edge [
    source 34
    target 1093
  ]
  edge [
    source 34
    target 1094
  ]
  edge [
    source 34
    target 1095
  ]
  edge [
    source 34
    target 1096
  ]
  edge [
    source 34
    target 1097
  ]
  edge [
    source 34
    target 1098
  ]
  edge [
    source 34
    target 1099
  ]
  edge [
    source 34
    target 1100
  ]
  edge [
    source 34
    target 1101
  ]
  edge [
    source 34
    target 1102
  ]
  edge [
    source 34
    target 1103
  ]
  edge [
    source 34
    target 1104
  ]
  edge [
    source 34
    target 1105
  ]
  edge [
    source 34
    target 1106
  ]
  edge [
    source 34
    target 1107
  ]
  edge [
    source 34
    target 1108
  ]
  edge [
    source 34
    target 1109
  ]
  edge [
    source 34
    target 1110
  ]
  edge [
    source 34
    target 1111
  ]
  edge [
    source 34
    target 1112
  ]
  edge [
    source 34
    target 1113
  ]
  edge [
    source 34
    target 1114
  ]
  edge [
    source 34
    target 1115
  ]
  edge [
    source 34
    target 1116
  ]
  edge [
    source 34
    target 1117
  ]
  edge [
    source 34
    target 1118
  ]
  edge [
    source 34
    target 1119
  ]
  edge [
    source 34
    target 1120
  ]
  edge [
    source 34
    target 1121
  ]
  edge [
    source 34
    target 1122
  ]
  edge [
    source 34
    target 1123
  ]
  edge [
    source 34
    target 341
  ]
  edge [
    source 34
    target 1124
  ]
  edge [
    source 34
    target 450
  ]
  edge [
    source 34
    target 1125
  ]
  edge [
    source 34
    target 1126
  ]
  edge [
    source 34
    target 1013
  ]
  edge [
    source 34
    target 1127
  ]
  edge [
    source 34
    target 1128
  ]
  edge [
    source 34
    target 1017
  ]
  edge [
    source 34
    target 1129
  ]
  edge [
    source 34
    target 1130
  ]
  edge [
    source 34
    target 1131
  ]
  edge [
    source 34
    target 1132
  ]
  edge [
    source 34
    target 1133
  ]
  edge [
    source 34
    target 1134
  ]
  edge [
    source 34
    target 1135
  ]
  edge [
    source 34
    target 1136
  ]
  edge [
    source 34
    target 1137
  ]
  edge [
    source 34
    target 1138
  ]
  edge [
    source 34
    target 339
  ]
  edge [
    source 34
    target 1139
  ]
  edge [
    source 34
    target 1140
  ]
  edge [
    source 34
    target 1141
  ]
  edge [
    source 34
    target 1142
  ]
  edge [
    source 34
    target 315
  ]
  edge [
    source 34
    target 1143
  ]
  edge [
    source 34
    target 1144
  ]
  edge [
    source 34
    target 1145
  ]
  edge [
    source 34
    target 363
  ]
  edge [
    source 34
    target 463
  ]
  edge [
    source 34
    target 1146
  ]
  edge [
    source 34
    target 1147
  ]
  edge [
    source 34
    target 1148
  ]
  edge [
    source 34
    target 1149
  ]
  edge [
    source 34
    target 1150
  ]
  edge [
    source 34
    target 1151
  ]
  edge [
    source 34
    target 1152
  ]
  edge [
    source 34
    target 1153
  ]
  edge [
    source 34
    target 1154
  ]
  edge [
    source 34
    target 1155
  ]
  edge [
    source 34
    target 1156
  ]
  edge [
    source 34
    target 1157
  ]
]
