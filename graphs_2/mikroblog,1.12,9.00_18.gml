graph [
  node [
    id 0
    label "chyba"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "podsumowanie"
    origin "text"
  ]
  node [
    id 3
    label "zmiana"
    origin "text"
  ]
  node [
    id 4
    label "rafonixa"
    origin "text"
  ]
  node [
    id 5
    label "dobroczynny"
  ]
  node [
    id 6
    label "czw&#243;rka"
  ]
  node [
    id 7
    label "spokojny"
  ]
  node [
    id 8
    label "skuteczny"
  ]
  node [
    id 9
    label "&#347;mieszny"
  ]
  node [
    id 10
    label "mi&#322;y"
  ]
  node [
    id 11
    label "grzeczny"
  ]
  node [
    id 12
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 13
    label "powitanie"
  ]
  node [
    id 14
    label "dobrze"
  ]
  node [
    id 15
    label "ca&#322;y"
  ]
  node [
    id 16
    label "zwrot"
  ]
  node [
    id 17
    label "pomy&#347;lny"
  ]
  node [
    id 18
    label "moralny"
  ]
  node [
    id 19
    label "drogi"
  ]
  node [
    id 20
    label "pozytywny"
  ]
  node [
    id 21
    label "odpowiedni"
  ]
  node [
    id 22
    label "korzystny"
  ]
  node [
    id 23
    label "pos&#322;uszny"
  ]
  node [
    id 24
    label "moralnie"
  ]
  node [
    id 25
    label "warto&#347;ciowy"
  ]
  node [
    id 26
    label "etycznie"
  ]
  node [
    id 27
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 28
    label "nale&#380;ny"
  ]
  node [
    id 29
    label "nale&#380;yty"
  ]
  node [
    id 30
    label "typowy"
  ]
  node [
    id 31
    label "uprawniony"
  ]
  node [
    id 32
    label "zasadniczy"
  ]
  node [
    id 33
    label "stosownie"
  ]
  node [
    id 34
    label "taki"
  ]
  node [
    id 35
    label "charakterystyczny"
  ]
  node [
    id 36
    label "prawdziwy"
  ]
  node [
    id 37
    label "ten"
  ]
  node [
    id 38
    label "pozytywnie"
  ]
  node [
    id 39
    label "fajny"
  ]
  node [
    id 40
    label "dodatnio"
  ]
  node [
    id 41
    label "przyjemny"
  ]
  node [
    id 42
    label "po&#380;&#261;dany"
  ]
  node [
    id 43
    label "niepowa&#380;ny"
  ]
  node [
    id 44
    label "o&#347;mieszanie"
  ]
  node [
    id 45
    label "&#347;miesznie"
  ]
  node [
    id 46
    label "bawny"
  ]
  node [
    id 47
    label "o&#347;mieszenie"
  ]
  node [
    id 48
    label "dziwny"
  ]
  node [
    id 49
    label "nieadekwatny"
  ]
  node [
    id 50
    label "wolny"
  ]
  node [
    id 51
    label "uspokajanie_si&#281;"
  ]
  node [
    id 52
    label "bezproblemowy"
  ]
  node [
    id 53
    label "spokojnie"
  ]
  node [
    id 54
    label "uspokojenie_si&#281;"
  ]
  node [
    id 55
    label "cicho"
  ]
  node [
    id 56
    label "uspokojenie"
  ]
  node [
    id 57
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 58
    label "nietrudny"
  ]
  node [
    id 59
    label "uspokajanie"
  ]
  node [
    id 60
    label "zale&#380;ny"
  ]
  node [
    id 61
    label "uleg&#322;y"
  ]
  node [
    id 62
    label "pos&#322;usznie"
  ]
  node [
    id 63
    label "grzecznie"
  ]
  node [
    id 64
    label "stosowny"
  ]
  node [
    id 65
    label "niewinny"
  ]
  node [
    id 66
    label "konserwatywny"
  ]
  node [
    id 67
    label "nijaki"
  ]
  node [
    id 68
    label "korzystnie"
  ]
  node [
    id 69
    label "drogo"
  ]
  node [
    id 70
    label "cz&#322;owiek"
  ]
  node [
    id 71
    label "bliski"
  ]
  node [
    id 72
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 73
    label "przyjaciel"
  ]
  node [
    id 74
    label "jedyny"
  ]
  node [
    id 75
    label "du&#380;y"
  ]
  node [
    id 76
    label "zdr&#243;w"
  ]
  node [
    id 77
    label "calu&#347;ko"
  ]
  node [
    id 78
    label "kompletny"
  ]
  node [
    id 79
    label "&#380;ywy"
  ]
  node [
    id 80
    label "pe&#322;ny"
  ]
  node [
    id 81
    label "podobny"
  ]
  node [
    id 82
    label "ca&#322;o"
  ]
  node [
    id 83
    label "poskutkowanie"
  ]
  node [
    id 84
    label "sprawny"
  ]
  node [
    id 85
    label "skutecznie"
  ]
  node [
    id 86
    label "skutkowanie"
  ]
  node [
    id 87
    label "pomy&#347;lnie"
  ]
  node [
    id 88
    label "toto-lotek"
  ]
  node [
    id 89
    label "trafienie"
  ]
  node [
    id 90
    label "zbi&#243;r"
  ]
  node [
    id 91
    label "arkusz_drukarski"
  ]
  node [
    id 92
    label "&#322;&#243;dka"
  ]
  node [
    id 93
    label "four"
  ]
  node [
    id 94
    label "&#263;wiartka"
  ]
  node [
    id 95
    label "hotel"
  ]
  node [
    id 96
    label "cyfra"
  ]
  node [
    id 97
    label "pok&#243;j"
  ]
  node [
    id 98
    label "stopie&#324;"
  ]
  node [
    id 99
    label "obiekt"
  ]
  node [
    id 100
    label "minialbum"
  ]
  node [
    id 101
    label "osada"
  ]
  node [
    id 102
    label "p&#322;yta_winylowa"
  ]
  node [
    id 103
    label "blotka"
  ]
  node [
    id 104
    label "zaprz&#281;g"
  ]
  node [
    id 105
    label "przedtrzonowiec"
  ]
  node [
    id 106
    label "punkt"
  ]
  node [
    id 107
    label "turn"
  ]
  node [
    id 108
    label "turning"
  ]
  node [
    id 109
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 110
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 111
    label "skr&#281;t"
  ]
  node [
    id 112
    label "obr&#243;t"
  ]
  node [
    id 113
    label "fraza_czasownikowa"
  ]
  node [
    id 114
    label "jednostka_leksykalna"
  ]
  node [
    id 115
    label "wyra&#380;enie"
  ]
  node [
    id 116
    label "welcome"
  ]
  node [
    id 117
    label "spotkanie"
  ]
  node [
    id 118
    label "pozdrowienie"
  ]
  node [
    id 119
    label "zwyczaj"
  ]
  node [
    id 120
    label "greeting"
  ]
  node [
    id 121
    label "zdarzony"
  ]
  node [
    id 122
    label "odpowiednio"
  ]
  node [
    id 123
    label "odpowiadanie"
  ]
  node [
    id 124
    label "specjalny"
  ]
  node [
    id 125
    label "kochanek"
  ]
  node [
    id 126
    label "sk&#322;onny"
  ]
  node [
    id 127
    label "wybranek"
  ]
  node [
    id 128
    label "umi&#322;owany"
  ]
  node [
    id 129
    label "przyjemnie"
  ]
  node [
    id 130
    label "mi&#322;o"
  ]
  node [
    id 131
    label "kochanie"
  ]
  node [
    id 132
    label "dyplomata"
  ]
  node [
    id 133
    label "dobroczynnie"
  ]
  node [
    id 134
    label "lepiej"
  ]
  node [
    id 135
    label "wiele"
  ]
  node [
    id 136
    label "spo&#322;eczny"
  ]
  node [
    id 137
    label "addition"
  ]
  node [
    id 138
    label "relacja"
  ]
  node [
    id 139
    label "collection"
  ]
  node [
    id 140
    label "przedstawienie"
  ]
  node [
    id 141
    label "pozbieranie"
  ]
  node [
    id 142
    label "statement"
  ]
  node [
    id 143
    label "ocenienie"
  ]
  node [
    id 144
    label "policzenie"
  ]
  node [
    id 145
    label "recapitulation"
  ]
  node [
    id 146
    label "evaluation"
  ]
  node [
    id 147
    label "wyrachowanie"
  ]
  node [
    id 148
    label "ustalenie"
  ]
  node [
    id 149
    label "zakwalifikowanie"
  ]
  node [
    id 150
    label "wynagrodzenie"
  ]
  node [
    id 151
    label "wyznaczenie"
  ]
  node [
    id 152
    label "wycenienie"
  ]
  node [
    id 153
    label "wyj&#347;cie"
  ]
  node [
    id 154
    label "zbadanie"
  ]
  node [
    id 155
    label "sprowadzenie"
  ]
  node [
    id 156
    label "przeliczenie_si&#281;"
  ]
  node [
    id 157
    label "sprz&#261;tni&#281;cie"
  ]
  node [
    id 158
    label "zgromadzenie"
  ]
  node [
    id 159
    label "gather"
  ]
  node [
    id 160
    label "follow-up"
  ]
  node [
    id 161
    label "przewarto&#347;ciowanie"
  ]
  node [
    id 162
    label "appraisal"
  ]
  node [
    id 163
    label "potraktowanie"
  ]
  node [
    id 164
    label "przyznanie"
  ]
  node [
    id 165
    label "dostanie"
  ]
  node [
    id 166
    label "wywy&#380;szenie"
  ]
  node [
    id 167
    label "przewidzenie"
  ]
  node [
    id 168
    label "pr&#243;bowanie"
  ]
  node [
    id 169
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 170
    label "zademonstrowanie"
  ]
  node [
    id 171
    label "report"
  ]
  node [
    id 172
    label "obgadanie"
  ]
  node [
    id 173
    label "realizacja"
  ]
  node [
    id 174
    label "scena"
  ]
  node [
    id 175
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 176
    label "narration"
  ]
  node [
    id 177
    label "cyrk"
  ]
  node [
    id 178
    label "wytw&#243;r"
  ]
  node [
    id 179
    label "posta&#263;"
  ]
  node [
    id 180
    label "theatrical_performance"
  ]
  node [
    id 181
    label "opisanie"
  ]
  node [
    id 182
    label "malarstwo"
  ]
  node [
    id 183
    label "scenografia"
  ]
  node [
    id 184
    label "teatr"
  ]
  node [
    id 185
    label "ukazanie"
  ]
  node [
    id 186
    label "zapoznanie"
  ]
  node [
    id 187
    label "pokaz"
  ]
  node [
    id 188
    label "podanie"
  ]
  node [
    id 189
    label "spos&#243;b"
  ]
  node [
    id 190
    label "ods&#322;ona"
  ]
  node [
    id 191
    label "exhibit"
  ]
  node [
    id 192
    label "pokazanie"
  ]
  node [
    id 193
    label "wyst&#261;pienie"
  ]
  node [
    id 194
    label "przedstawi&#263;"
  ]
  node [
    id 195
    label "przedstawianie"
  ]
  node [
    id 196
    label "przedstawia&#263;"
  ]
  node [
    id 197
    label "rola"
  ]
  node [
    id 198
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 199
    label "ustosunkowywa&#263;"
  ]
  node [
    id 200
    label "wi&#261;zanie"
  ]
  node [
    id 201
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 202
    label "sprawko"
  ]
  node [
    id 203
    label "bratnia_dusza"
  ]
  node [
    id 204
    label "trasa"
  ]
  node [
    id 205
    label "zwi&#261;zanie"
  ]
  node [
    id 206
    label "ustosunkowywanie"
  ]
  node [
    id 207
    label "marriage"
  ]
  node [
    id 208
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 209
    label "message"
  ]
  node [
    id 210
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 211
    label "ustosunkowa&#263;"
  ]
  node [
    id 212
    label "korespondent"
  ]
  node [
    id 213
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 214
    label "zwi&#261;za&#263;"
  ]
  node [
    id 215
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 216
    label "podzbi&#243;r"
  ]
  node [
    id 217
    label "ustosunkowanie"
  ]
  node [
    id 218
    label "wypowied&#378;"
  ]
  node [
    id 219
    label "zwi&#261;zek"
  ]
  node [
    id 220
    label "rewizja"
  ]
  node [
    id 221
    label "passage"
  ]
  node [
    id 222
    label "oznaka"
  ]
  node [
    id 223
    label "change"
  ]
  node [
    id 224
    label "ferment"
  ]
  node [
    id 225
    label "komplet"
  ]
  node [
    id 226
    label "anatomopatolog"
  ]
  node [
    id 227
    label "zmianka"
  ]
  node [
    id 228
    label "czas"
  ]
  node [
    id 229
    label "zjawisko"
  ]
  node [
    id 230
    label "amendment"
  ]
  node [
    id 231
    label "praca"
  ]
  node [
    id 232
    label "odmienianie"
  ]
  node [
    id 233
    label "tura"
  ]
  node [
    id 234
    label "proces"
  ]
  node [
    id 235
    label "boski"
  ]
  node [
    id 236
    label "krajobraz"
  ]
  node [
    id 237
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 238
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 239
    label "przywidzenie"
  ]
  node [
    id 240
    label "presence"
  ]
  node [
    id 241
    label "charakter"
  ]
  node [
    id 242
    label "pi&#281;kno&#347;&#263;"
  ]
  node [
    id 243
    label "lekcja"
  ]
  node [
    id 244
    label "ensemble"
  ]
  node [
    id 245
    label "grupa"
  ]
  node [
    id 246
    label "klasa"
  ]
  node [
    id 247
    label "zestaw"
  ]
  node [
    id 248
    label "poprzedzanie"
  ]
  node [
    id 249
    label "czasoprzestrze&#324;"
  ]
  node [
    id 250
    label "laba"
  ]
  node [
    id 251
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 252
    label "chronometria"
  ]
  node [
    id 253
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 254
    label "rachuba_czasu"
  ]
  node [
    id 255
    label "przep&#322;ywanie"
  ]
  node [
    id 256
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 257
    label "czasokres"
  ]
  node [
    id 258
    label "odczyt"
  ]
  node [
    id 259
    label "chwila"
  ]
  node [
    id 260
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 261
    label "dzieje"
  ]
  node [
    id 262
    label "kategoria_gramatyczna"
  ]
  node [
    id 263
    label "poprzedzenie"
  ]
  node [
    id 264
    label "trawienie"
  ]
  node [
    id 265
    label "pochodzi&#263;"
  ]
  node [
    id 266
    label "period"
  ]
  node [
    id 267
    label "okres_czasu"
  ]
  node [
    id 268
    label "poprzedza&#263;"
  ]
  node [
    id 269
    label "schy&#322;ek"
  ]
  node [
    id 270
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 271
    label "odwlekanie_si&#281;"
  ]
  node [
    id 272
    label "zegar"
  ]
  node [
    id 273
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 274
    label "czwarty_wymiar"
  ]
  node [
    id 275
    label "pochodzenie"
  ]
  node [
    id 276
    label "koniugacja"
  ]
  node [
    id 277
    label "Zeitgeist"
  ]
  node [
    id 278
    label "trawi&#263;"
  ]
  node [
    id 279
    label "pogoda"
  ]
  node [
    id 280
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 281
    label "poprzedzi&#263;"
  ]
  node [
    id 282
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 283
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 284
    label "time_period"
  ]
  node [
    id 285
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 286
    label "implikowa&#263;"
  ]
  node [
    id 287
    label "signal"
  ]
  node [
    id 288
    label "fakt"
  ]
  node [
    id 289
    label "symbol"
  ]
  node [
    id 290
    label "bia&#322;ko"
  ]
  node [
    id 291
    label "immobilizowa&#263;"
  ]
  node [
    id 292
    label "poruszenie"
  ]
  node [
    id 293
    label "immobilizacja"
  ]
  node [
    id 294
    label "apoenzym"
  ]
  node [
    id 295
    label "zymaza"
  ]
  node [
    id 296
    label "enzyme"
  ]
  node [
    id 297
    label "immobilizowanie"
  ]
  node [
    id 298
    label "biokatalizator"
  ]
  node [
    id 299
    label "proces_my&#347;lowy"
  ]
  node [
    id 300
    label "dow&#243;d"
  ]
  node [
    id 301
    label "krytyka"
  ]
  node [
    id 302
    label "rekurs"
  ]
  node [
    id 303
    label "checkup"
  ]
  node [
    id 304
    label "kontrola"
  ]
  node [
    id 305
    label "odwo&#322;anie"
  ]
  node [
    id 306
    label "correction"
  ]
  node [
    id 307
    label "przegl&#261;d"
  ]
  node [
    id 308
    label "kipisz"
  ]
  node [
    id 309
    label "korekta"
  ]
  node [
    id 310
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 311
    label "najem"
  ]
  node [
    id 312
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 313
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 314
    label "zak&#322;ad"
  ]
  node [
    id 315
    label "stosunek_pracy"
  ]
  node [
    id 316
    label "benedykty&#324;ski"
  ]
  node [
    id 317
    label "poda&#380;_pracy"
  ]
  node [
    id 318
    label "pracowanie"
  ]
  node [
    id 319
    label "tyrka"
  ]
  node [
    id 320
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 321
    label "miejsce"
  ]
  node [
    id 322
    label "zaw&#243;d"
  ]
  node [
    id 323
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 324
    label "tynkarski"
  ]
  node [
    id 325
    label "pracowa&#263;"
  ]
  node [
    id 326
    label "czynno&#347;&#263;"
  ]
  node [
    id 327
    label "czynnik_produkcji"
  ]
  node [
    id 328
    label "zobowi&#261;zanie"
  ]
  node [
    id 329
    label "kierownictwo"
  ]
  node [
    id 330
    label "siedziba"
  ]
  node [
    id 331
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 332
    label "patolog"
  ]
  node [
    id 333
    label "anatom"
  ]
  node [
    id 334
    label "sparafrazowanie"
  ]
  node [
    id 335
    label "zmienianie"
  ]
  node [
    id 336
    label "parafrazowanie"
  ]
  node [
    id 337
    label "zamiana"
  ]
  node [
    id 338
    label "wymienianie"
  ]
  node [
    id 339
    label "Transfiguration"
  ]
  node [
    id 340
    label "przeobra&#380;anie_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 3
    target 338
  ]
  edge [
    source 3
    target 339
  ]
  edge [
    source 3
    target 340
  ]
]
