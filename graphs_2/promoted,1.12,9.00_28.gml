graph [
  node [
    id 0
    label "plakat"
    origin "text"
  ]
  node [
    id 1
    label "podobizna"
    origin "text"
  ]
  node [
    id 2
    label "wisie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 4
    label "osiedlowy"
    origin "text"
  ]
  node [
    id 5
    label "si&#322;ownia"
    origin "text"
  ]
  node [
    id 6
    label "reklama"
  ]
  node [
    id 7
    label "bill"
  ]
  node [
    id 8
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 9
    label "copywriting"
  ]
  node [
    id 10
    label "wypromowa&#263;"
  ]
  node [
    id 11
    label "brief"
  ]
  node [
    id 12
    label "samplowanie"
  ]
  node [
    id 13
    label "akcja"
  ]
  node [
    id 14
    label "promowa&#263;"
  ]
  node [
    id 15
    label "bran&#380;a"
  ]
  node [
    id 16
    label "tekst"
  ]
  node [
    id 17
    label "informacja"
  ]
  node [
    id 18
    label "konterfekt"
  ]
  node [
    id 19
    label "wytw&#243;r"
  ]
  node [
    id 20
    label "przedstawienie"
  ]
  node [
    id 21
    label "posta&#263;"
  ]
  node [
    id 22
    label "dzie&#322;o"
  ]
  node [
    id 23
    label "obraz"
  ]
  node [
    id 24
    label "likeness"
  ]
  node [
    id 25
    label "przedmiot"
  ]
  node [
    id 26
    label "p&#322;&#243;d"
  ]
  node [
    id 27
    label "work"
  ]
  node [
    id 28
    label "rezultat"
  ]
  node [
    id 29
    label "obrazowanie"
  ]
  node [
    id 30
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 31
    label "dorobek"
  ]
  node [
    id 32
    label "forma"
  ]
  node [
    id 33
    label "tre&#347;&#263;"
  ]
  node [
    id 34
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 35
    label "retrospektywa"
  ]
  node [
    id 36
    label "works"
  ]
  node [
    id 37
    label "creation"
  ]
  node [
    id 38
    label "tetralogia"
  ]
  node [
    id 39
    label "komunikat"
  ]
  node [
    id 40
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 41
    label "praca"
  ]
  node [
    id 42
    label "representation"
  ]
  node [
    id 43
    label "effigy"
  ]
  node [
    id 44
    label "podobrazie"
  ]
  node [
    id 45
    label "scena"
  ]
  node [
    id 46
    label "human_body"
  ]
  node [
    id 47
    label "projekcja"
  ]
  node [
    id 48
    label "oprawia&#263;"
  ]
  node [
    id 49
    label "zjawisko"
  ]
  node [
    id 50
    label "postprodukcja"
  ]
  node [
    id 51
    label "t&#322;o"
  ]
  node [
    id 52
    label "inning"
  ]
  node [
    id 53
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 54
    label "pulment"
  ]
  node [
    id 55
    label "pogl&#261;d"
  ]
  node [
    id 56
    label "zbi&#243;r"
  ]
  node [
    id 57
    label "plama_barwna"
  ]
  node [
    id 58
    label "pod&#322;o&#380;e_malarskie"
  ]
  node [
    id 59
    label "oprawianie"
  ]
  node [
    id 60
    label "sztafa&#380;"
  ]
  node [
    id 61
    label "parkiet"
  ]
  node [
    id 62
    label "opinion"
  ]
  node [
    id 63
    label "uj&#281;cie"
  ]
  node [
    id 64
    label "zaj&#347;cie"
  ]
  node [
    id 65
    label "persona"
  ]
  node [
    id 66
    label "filmoteka"
  ]
  node [
    id 67
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 68
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 69
    label "ziarno"
  ]
  node [
    id 70
    label "picture"
  ]
  node [
    id 71
    label "wypunktowa&#263;"
  ]
  node [
    id 72
    label "ostro&#347;&#263;"
  ]
  node [
    id 73
    label "malarz"
  ]
  node [
    id 74
    label "napisy"
  ]
  node [
    id 75
    label "przeplot"
  ]
  node [
    id 76
    label "punktowa&#263;"
  ]
  node [
    id 77
    label "anamorfoza"
  ]
  node [
    id 78
    label "ty&#322;&#243;wka"
  ]
  node [
    id 79
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 80
    label "widok"
  ]
  node [
    id 81
    label "czo&#322;&#243;wka"
  ]
  node [
    id 82
    label "rola"
  ]
  node [
    id 83
    label "perspektywa"
  ]
  node [
    id 84
    label "pr&#243;bowanie"
  ]
  node [
    id 85
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 86
    label "zademonstrowanie"
  ]
  node [
    id 87
    label "report"
  ]
  node [
    id 88
    label "obgadanie"
  ]
  node [
    id 89
    label "realizacja"
  ]
  node [
    id 90
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 91
    label "narration"
  ]
  node [
    id 92
    label "cyrk"
  ]
  node [
    id 93
    label "theatrical_performance"
  ]
  node [
    id 94
    label "opisanie"
  ]
  node [
    id 95
    label "malarstwo"
  ]
  node [
    id 96
    label "scenografia"
  ]
  node [
    id 97
    label "teatr"
  ]
  node [
    id 98
    label "ukazanie"
  ]
  node [
    id 99
    label "zapoznanie"
  ]
  node [
    id 100
    label "pokaz"
  ]
  node [
    id 101
    label "podanie"
  ]
  node [
    id 102
    label "spos&#243;b"
  ]
  node [
    id 103
    label "ods&#322;ona"
  ]
  node [
    id 104
    label "exhibit"
  ]
  node [
    id 105
    label "pokazanie"
  ]
  node [
    id 106
    label "wyst&#261;pienie"
  ]
  node [
    id 107
    label "przedstawi&#263;"
  ]
  node [
    id 108
    label "przedstawianie"
  ]
  node [
    id 109
    label "przedstawia&#263;"
  ]
  node [
    id 110
    label "charakterystyka"
  ]
  node [
    id 111
    label "cz&#322;owiek"
  ]
  node [
    id 112
    label "zaistnie&#263;"
  ]
  node [
    id 113
    label "cecha"
  ]
  node [
    id 114
    label "Osjan"
  ]
  node [
    id 115
    label "kto&#347;"
  ]
  node [
    id 116
    label "wygl&#261;d"
  ]
  node [
    id 117
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 118
    label "osobowo&#347;&#263;"
  ]
  node [
    id 119
    label "trim"
  ]
  node [
    id 120
    label "poby&#263;"
  ]
  node [
    id 121
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 122
    label "Aspazja"
  ]
  node [
    id 123
    label "punkt_widzenia"
  ]
  node [
    id 124
    label "kompleksja"
  ]
  node [
    id 125
    label "wytrzyma&#263;"
  ]
  node [
    id 126
    label "budowa"
  ]
  node [
    id 127
    label "formacja"
  ]
  node [
    id 128
    label "pozosta&#263;"
  ]
  node [
    id 129
    label "point"
  ]
  node [
    id 130
    label "go&#347;&#263;"
  ]
  node [
    id 131
    label "portret"
  ]
  node [
    id 132
    label "m&#281;czy&#263;"
  ]
  node [
    id 133
    label "trwa&#263;"
  ]
  node [
    id 134
    label "by&#263;"
  ]
  node [
    id 135
    label "ci&#261;&#380;y&#263;"
  ]
  node [
    id 136
    label "zagra&#380;a&#263;"
  ]
  node [
    id 137
    label "unosi&#263;_si&#281;"
  ]
  node [
    id 138
    label "bent"
  ]
  node [
    id 139
    label "dynda&#263;"
  ]
  node [
    id 140
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 141
    label "mie&#263;_miejsce"
  ]
  node [
    id 142
    label "equal"
  ]
  node [
    id 143
    label "chodzi&#263;"
  ]
  node [
    id 144
    label "si&#281;ga&#263;"
  ]
  node [
    id 145
    label "stan"
  ]
  node [
    id 146
    label "obecno&#347;&#263;"
  ]
  node [
    id 147
    label "stand"
  ]
  node [
    id 148
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 149
    label "uczestniczy&#263;"
  ]
  node [
    id 150
    label "istnie&#263;"
  ]
  node [
    id 151
    label "pozostawa&#263;"
  ]
  node [
    id 152
    label "zostawa&#263;"
  ]
  node [
    id 153
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 154
    label "adhere"
  ]
  node [
    id 155
    label "mistreat"
  ]
  node [
    id 156
    label "tease"
  ]
  node [
    id 157
    label "nudzi&#263;"
  ]
  node [
    id 158
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 159
    label "krzywdzi&#263;"
  ]
  node [
    id 160
    label "spoczywa&#263;"
  ]
  node [
    id 161
    label "doskwiera&#263;"
  ]
  node [
    id 162
    label "wa&#380;y&#263;"
  ]
  node [
    id 163
    label "urge"
  ]
  node [
    id 164
    label "imperativeness"
  ]
  node [
    id 165
    label "hazard"
  ]
  node [
    id 166
    label "sag"
  ]
  node [
    id 167
    label "swing"
  ]
  node [
    id 168
    label "ko&#322;ysa&#263;_si&#281;"
  ]
  node [
    id 169
    label "macha&#263;"
  ]
  node [
    id 170
    label "jaki&#347;"
  ]
  node [
    id 171
    label "przyzwoity"
  ]
  node [
    id 172
    label "ciekawy"
  ]
  node [
    id 173
    label "jako&#347;"
  ]
  node [
    id 174
    label "jako_tako"
  ]
  node [
    id 175
    label "niez&#322;y"
  ]
  node [
    id 176
    label "dziwny"
  ]
  node [
    id 177
    label "charakterystyczny"
  ]
  node [
    id 178
    label "lokalny"
  ]
  node [
    id 179
    label "lokalnie"
  ]
  node [
    id 180
    label "zak&#322;ad_komunalny"
  ]
  node [
    id 181
    label "budowla"
  ]
  node [
    id 182
    label "pakowa&#263;"
  ]
  node [
    id 183
    label "pakernia"
  ]
  node [
    id 184
    label "obiekt"
  ]
  node [
    id 185
    label "kulturysta"
  ]
  node [
    id 186
    label "co&#347;"
  ]
  node [
    id 187
    label "budynek"
  ]
  node [
    id 188
    label "thing"
  ]
  node [
    id 189
    label "poj&#281;cie"
  ]
  node [
    id 190
    label "program"
  ]
  node [
    id 191
    label "rzecz"
  ]
  node [
    id 192
    label "strona"
  ]
  node [
    id 193
    label "obudowanie"
  ]
  node [
    id 194
    label "obudowywa&#263;"
  ]
  node [
    id 195
    label "zbudowa&#263;"
  ]
  node [
    id 196
    label "obudowa&#263;"
  ]
  node [
    id 197
    label "kolumnada"
  ]
  node [
    id 198
    label "korpus"
  ]
  node [
    id 199
    label "Sukiennice"
  ]
  node [
    id 200
    label "odbudowywa&#263;_si&#281;"
  ]
  node [
    id 201
    label "fundament"
  ]
  node [
    id 202
    label "postanie"
  ]
  node [
    id 203
    label "obudowywanie"
  ]
  node [
    id 204
    label "zbudowanie"
  ]
  node [
    id 205
    label "odbudowywanie_si&#281;"
  ]
  node [
    id 206
    label "stan_surowy"
  ]
  node [
    id 207
    label "konstrukcja"
  ]
  node [
    id 208
    label "krzepa"
  ]
  node [
    id 209
    label "hobbysta"
  ]
  node [
    id 210
    label "koks"
  ]
  node [
    id 211
    label "si&#322;acz"
  ]
  node [
    id 212
    label "gimnastyk"
  ]
  node [
    id 213
    label "typ_atletyczny"
  ]
  node [
    id 214
    label "trenowa&#263;"
  ]
  node [
    id 215
    label "wpiernicza&#263;"
  ]
  node [
    id 216
    label "applaud"
  ]
  node [
    id 217
    label "owija&#263;"
  ]
  node [
    id 218
    label "dane"
  ]
  node [
    id 219
    label "konwertowa&#263;"
  ]
  node [
    id 220
    label "nakazywa&#263;"
  ]
  node [
    id 221
    label "wci&#261;ga&#263;"
  ]
  node [
    id 222
    label "umieszcza&#263;"
  ]
  node [
    id 223
    label "wpycha&#263;"
  ]
  node [
    id 224
    label "pack"
  ]
  node [
    id 225
    label "entangle"
  ]
  node [
    id 226
    label "wk&#322;ada&#263;"
  ]
  node [
    id 227
    label "Ronnie"
  ]
  node [
    id 228
    label "Coleman"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 227
    target 228
  ]
]
