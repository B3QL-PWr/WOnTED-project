graph [
  node [
    id 0
    label "naukowiec"
    origin "text"
  ]
  node [
    id 1
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 2
    label "oksfordzki"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 5
    label "rozwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 6
    label "jeden"
    origin "text"
  ]
  node [
    id 7
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 8
    label "problem"
    origin "text"
  ]
  node [
    id 9
    label "wsp&#243;&#322;czesny"
    origin "text"
  ]
  node [
    id 10
    label "fizyka"
    origin "text"
  ]
  node [
    id 11
    label "&#347;ledziciel"
  ]
  node [
    id 12
    label "uczony"
  ]
  node [
    id 13
    label "Miczurin"
  ]
  node [
    id 14
    label "wykszta&#322;cony"
  ]
  node [
    id 15
    label "inteligent"
  ]
  node [
    id 16
    label "cz&#322;owiek"
  ]
  node [
    id 17
    label "intelektualista"
  ]
  node [
    id 18
    label "Awerroes"
  ]
  node [
    id 19
    label "uczenie"
  ]
  node [
    id 20
    label "nauczny"
  ]
  node [
    id 21
    label "m&#261;dry"
  ]
  node [
    id 22
    label "agent"
  ]
  node [
    id 23
    label "Stanford"
  ]
  node [
    id 24
    label "academy"
  ]
  node [
    id 25
    label "Harvard"
  ]
  node [
    id 26
    label "uczelnia"
  ]
  node [
    id 27
    label "wydzia&#322;"
  ]
  node [
    id 28
    label "Sorbona"
  ]
  node [
    id 29
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 30
    label "Princeton"
  ]
  node [
    id 31
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 32
    label "ku&#378;nia"
  ]
  node [
    id 33
    label "warsztat"
  ]
  node [
    id 34
    label "instytucja"
  ]
  node [
    id 35
    label "fabryka"
  ]
  node [
    id 36
    label "kanclerz"
  ]
  node [
    id 37
    label "szko&#322;a"
  ]
  node [
    id 38
    label "wyk&#322;ada&#263;"
  ]
  node [
    id 39
    label "podkanclerz"
  ]
  node [
    id 40
    label "miasteczko_studenckie"
  ]
  node [
    id 41
    label "kwestura"
  ]
  node [
    id 42
    label "wyk&#322;adanie"
  ]
  node [
    id 43
    label "rektorat"
  ]
  node [
    id 44
    label "school"
  ]
  node [
    id 45
    label "senat"
  ]
  node [
    id 46
    label "promotorstwo"
  ]
  node [
    id 47
    label "jednostka_organizacyjna"
  ]
  node [
    id 48
    label "relation"
  ]
  node [
    id 49
    label "urz&#261;d"
  ]
  node [
    id 50
    label "whole"
  ]
  node [
    id 51
    label "miejsce_pracy"
  ]
  node [
    id 52
    label "podsekcja"
  ]
  node [
    id 53
    label "insourcing"
  ]
  node [
    id 54
    label "politechnika"
  ]
  node [
    id 55
    label "katedra"
  ]
  node [
    id 56
    label "ministerstwo"
  ]
  node [
    id 57
    label "dzia&#322;"
  ]
  node [
    id 58
    label "ameryka&#324;ski"
  ]
  node [
    id 59
    label "paryski"
  ]
  node [
    id 60
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 61
    label "brytyjski"
  ]
  node [
    id 62
    label "angielski"
  ]
  node [
    id 63
    label "morris"
  ]
  node [
    id 64
    label "j&#281;zyk_angielski"
  ]
  node [
    id 65
    label "j&#281;zyk_celtycki"
  ]
  node [
    id 66
    label "anglosaski"
  ]
  node [
    id 67
    label "angielsko"
  ]
  node [
    id 68
    label "brytyjsko"
  ]
  node [
    id 69
    label "europejski"
  ]
  node [
    id 70
    label "zachodnioeuropejski"
  ]
  node [
    id 71
    label "po_brytyjsku"
  ]
  node [
    id 72
    label "j&#281;zyk_martwy"
  ]
  node [
    id 73
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 74
    label "nale&#380;ny"
  ]
  node [
    id 75
    label "nale&#380;yty"
  ]
  node [
    id 76
    label "typowy"
  ]
  node [
    id 77
    label "uprawniony"
  ]
  node [
    id 78
    label "zasadniczy"
  ]
  node [
    id 79
    label "stosownie"
  ]
  node [
    id 80
    label "taki"
  ]
  node [
    id 81
    label "charakterystyczny"
  ]
  node [
    id 82
    label "prawdziwy"
  ]
  node [
    id 83
    label "ten"
  ]
  node [
    id 84
    label "dobry"
  ]
  node [
    id 85
    label "angol"
  ]
  node [
    id 86
    label "po_angielsku"
  ]
  node [
    id 87
    label "English"
  ]
  node [
    id 88
    label "anglicki"
  ]
  node [
    id 89
    label "j&#281;zyk"
  ]
  node [
    id 90
    label "j&#281;zyki_angielskie"
  ]
  node [
    id 91
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 92
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 93
    label "mie&#263;_miejsce"
  ]
  node [
    id 94
    label "equal"
  ]
  node [
    id 95
    label "trwa&#263;"
  ]
  node [
    id 96
    label "chodzi&#263;"
  ]
  node [
    id 97
    label "si&#281;ga&#263;"
  ]
  node [
    id 98
    label "stan"
  ]
  node [
    id 99
    label "obecno&#347;&#263;"
  ]
  node [
    id 100
    label "stand"
  ]
  node [
    id 101
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 102
    label "uczestniczy&#263;"
  ]
  node [
    id 103
    label "participate"
  ]
  node [
    id 104
    label "robi&#263;"
  ]
  node [
    id 105
    label "istnie&#263;"
  ]
  node [
    id 106
    label "pozostawa&#263;"
  ]
  node [
    id 107
    label "zostawa&#263;"
  ]
  node [
    id 108
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 109
    label "adhere"
  ]
  node [
    id 110
    label "compass"
  ]
  node [
    id 111
    label "korzysta&#263;"
  ]
  node [
    id 112
    label "appreciation"
  ]
  node [
    id 113
    label "osi&#261;ga&#263;"
  ]
  node [
    id 114
    label "dociera&#263;"
  ]
  node [
    id 115
    label "get"
  ]
  node [
    id 116
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 117
    label "mierzy&#263;"
  ]
  node [
    id 118
    label "u&#380;ywa&#263;"
  ]
  node [
    id 119
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 120
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 121
    label "exsert"
  ]
  node [
    id 122
    label "being"
  ]
  node [
    id 123
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 124
    label "cecha"
  ]
  node [
    id 125
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 126
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 127
    label "p&#322;ywa&#263;"
  ]
  node [
    id 128
    label "run"
  ]
  node [
    id 129
    label "bangla&#263;"
  ]
  node [
    id 130
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 131
    label "przebiega&#263;"
  ]
  node [
    id 132
    label "wk&#322;ada&#263;"
  ]
  node [
    id 133
    label "proceed"
  ]
  node [
    id 134
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 135
    label "carry"
  ]
  node [
    id 136
    label "bywa&#263;"
  ]
  node [
    id 137
    label "dziama&#263;"
  ]
  node [
    id 138
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 139
    label "stara&#263;_si&#281;"
  ]
  node [
    id 140
    label "para"
  ]
  node [
    id 141
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 142
    label "str&#243;j"
  ]
  node [
    id 143
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 144
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 145
    label "krok"
  ]
  node [
    id 146
    label "tryb"
  ]
  node [
    id 147
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 148
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 149
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 150
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 151
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 152
    label "continue"
  ]
  node [
    id 153
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 154
    label "Ohio"
  ]
  node [
    id 155
    label "wci&#281;cie"
  ]
  node [
    id 156
    label "Nowy_York"
  ]
  node [
    id 157
    label "warstwa"
  ]
  node [
    id 158
    label "samopoczucie"
  ]
  node [
    id 159
    label "Illinois"
  ]
  node [
    id 160
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 161
    label "state"
  ]
  node [
    id 162
    label "Jukatan"
  ]
  node [
    id 163
    label "Kalifornia"
  ]
  node [
    id 164
    label "Wirginia"
  ]
  node [
    id 165
    label "wektor"
  ]
  node [
    id 166
    label "Goa"
  ]
  node [
    id 167
    label "Teksas"
  ]
  node [
    id 168
    label "Waszyngton"
  ]
  node [
    id 169
    label "miejsce"
  ]
  node [
    id 170
    label "Massachusetts"
  ]
  node [
    id 171
    label "Alaska"
  ]
  node [
    id 172
    label "Arakan"
  ]
  node [
    id 173
    label "Hawaje"
  ]
  node [
    id 174
    label "Maryland"
  ]
  node [
    id 175
    label "punkt"
  ]
  node [
    id 176
    label "Michigan"
  ]
  node [
    id 177
    label "Arizona"
  ]
  node [
    id 178
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 179
    label "Georgia"
  ]
  node [
    id 180
    label "poziom"
  ]
  node [
    id 181
    label "Pensylwania"
  ]
  node [
    id 182
    label "shape"
  ]
  node [
    id 183
    label "Luizjana"
  ]
  node [
    id 184
    label "Nowy_Meksyk"
  ]
  node [
    id 185
    label "Alabama"
  ]
  node [
    id 186
    label "ilo&#347;&#263;"
  ]
  node [
    id 187
    label "Kansas"
  ]
  node [
    id 188
    label "Oregon"
  ]
  node [
    id 189
    label "Oklahoma"
  ]
  node [
    id 190
    label "Floryda"
  ]
  node [
    id 191
    label "jednostka_administracyjna"
  ]
  node [
    id 192
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 193
    label "unloose"
  ]
  node [
    id 194
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 195
    label "bring"
  ]
  node [
    id 196
    label "urzeczywistni&#263;"
  ]
  node [
    id 197
    label "undo"
  ]
  node [
    id 198
    label "usun&#261;&#263;"
  ]
  node [
    id 199
    label "wymy&#347;li&#263;"
  ]
  node [
    id 200
    label "przesta&#263;"
  ]
  node [
    id 201
    label "concoct"
  ]
  node [
    id 202
    label "sta&#263;_si&#281;"
  ]
  node [
    id 203
    label "zrobi&#263;"
  ]
  node [
    id 204
    label "sko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 205
    label "coating"
  ]
  node [
    id 206
    label "drop"
  ]
  node [
    id 207
    label "sko&#324;czy&#263;"
  ]
  node [
    id 208
    label "leave_office"
  ]
  node [
    id 209
    label "fail"
  ]
  node [
    id 210
    label "zerwa&#263;"
  ]
  node [
    id 211
    label "kill"
  ]
  node [
    id 212
    label "withdraw"
  ]
  node [
    id 213
    label "motivate"
  ]
  node [
    id 214
    label "pozby&#263;_si&#281;"
  ]
  node [
    id 215
    label "wyrugowa&#263;"
  ]
  node [
    id 216
    label "go"
  ]
  node [
    id 217
    label "zabi&#263;"
  ]
  node [
    id 218
    label "spowodowa&#263;"
  ]
  node [
    id 219
    label "przenie&#347;&#263;"
  ]
  node [
    id 220
    label "przesun&#261;&#263;"
  ]
  node [
    id 221
    label "actualize"
  ]
  node [
    id 222
    label "shot"
  ]
  node [
    id 223
    label "jednakowy"
  ]
  node [
    id 224
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 225
    label "ujednolicenie"
  ]
  node [
    id 226
    label "jaki&#347;"
  ]
  node [
    id 227
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 228
    label "jednolicie"
  ]
  node [
    id 229
    label "kieliszek"
  ]
  node [
    id 230
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 231
    label "w&#243;dka"
  ]
  node [
    id 232
    label "szk&#322;o"
  ]
  node [
    id 233
    label "zawarto&#347;&#263;"
  ]
  node [
    id 234
    label "naczynie"
  ]
  node [
    id 235
    label "alkohol"
  ]
  node [
    id 236
    label "sznaps"
  ]
  node [
    id 237
    label "nap&#243;j"
  ]
  node [
    id 238
    label "gorza&#322;ka"
  ]
  node [
    id 239
    label "mohorycz"
  ]
  node [
    id 240
    label "zr&#243;wnywanie_si&#281;"
  ]
  node [
    id 241
    label "zr&#243;wnanie"
  ]
  node [
    id 242
    label "mundurowanie"
  ]
  node [
    id 243
    label "taki&#380;"
  ]
  node [
    id 244
    label "jednakowo"
  ]
  node [
    id 245
    label "mundurowa&#263;"
  ]
  node [
    id 246
    label "zr&#243;wnywanie"
  ]
  node [
    id 247
    label "identyczny"
  ]
  node [
    id 248
    label "okre&#347;lony"
  ]
  node [
    id 249
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 250
    label "z&#322;o&#380;ony"
  ]
  node [
    id 251
    label "przyzwoity"
  ]
  node [
    id 252
    label "ciekawy"
  ]
  node [
    id 253
    label "jako&#347;"
  ]
  node [
    id 254
    label "jako_tako"
  ]
  node [
    id 255
    label "niez&#322;y"
  ]
  node [
    id 256
    label "dziwny"
  ]
  node [
    id 257
    label "g&#322;&#281;bszy"
  ]
  node [
    id 258
    label "drink"
  ]
  node [
    id 259
    label "jednolity"
  ]
  node [
    id 260
    label "upodobnienie"
  ]
  node [
    id 261
    label "calibration"
  ]
  node [
    id 262
    label "doros&#322;y"
  ]
  node [
    id 263
    label "znaczny"
  ]
  node [
    id 264
    label "niema&#322;o"
  ]
  node [
    id 265
    label "wiele"
  ]
  node [
    id 266
    label "rozwini&#281;ty"
  ]
  node [
    id 267
    label "dorodny"
  ]
  node [
    id 268
    label "wa&#380;ny"
  ]
  node [
    id 269
    label "du&#380;o"
  ]
  node [
    id 270
    label "&#380;ywny"
  ]
  node [
    id 271
    label "szczery"
  ]
  node [
    id 272
    label "naturalny"
  ]
  node [
    id 273
    label "naprawd&#281;"
  ]
  node [
    id 274
    label "realnie"
  ]
  node [
    id 275
    label "podobny"
  ]
  node [
    id 276
    label "zgodny"
  ]
  node [
    id 277
    label "prawdziwie"
  ]
  node [
    id 278
    label "znacznie"
  ]
  node [
    id 279
    label "zauwa&#380;alny"
  ]
  node [
    id 280
    label "wynios&#322;y"
  ]
  node [
    id 281
    label "dono&#347;ny"
  ]
  node [
    id 282
    label "silny"
  ]
  node [
    id 283
    label "wa&#380;nie"
  ]
  node [
    id 284
    label "istotnie"
  ]
  node [
    id 285
    label "eksponowany"
  ]
  node [
    id 286
    label "ukszta&#322;towany"
  ]
  node [
    id 287
    label "do&#347;cig&#322;y"
  ]
  node [
    id 288
    label "&#378;ra&#322;y"
  ]
  node [
    id 289
    label "zdr&#243;w"
  ]
  node [
    id 290
    label "dorodnie"
  ]
  node [
    id 291
    label "okaza&#322;y"
  ]
  node [
    id 292
    label "mocno"
  ]
  node [
    id 293
    label "wiela"
  ]
  node [
    id 294
    label "bardzo"
  ]
  node [
    id 295
    label "cz&#281;sto"
  ]
  node [
    id 296
    label "wydoro&#347;lenie"
  ]
  node [
    id 297
    label "doro&#347;lenie"
  ]
  node [
    id 298
    label "doro&#347;le"
  ]
  node [
    id 299
    label "senior"
  ]
  node [
    id 300
    label "dojrzale"
  ]
  node [
    id 301
    label "wapniak"
  ]
  node [
    id 302
    label "dojrza&#322;y"
  ]
  node [
    id 303
    label "doletni"
  ]
  node [
    id 304
    label "sprawa"
  ]
  node [
    id 305
    label "subiekcja"
  ]
  node [
    id 306
    label "problemat"
  ]
  node [
    id 307
    label "jajko_Kolumba"
  ]
  node [
    id 308
    label "obstruction"
  ]
  node [
    id 309
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 310
    label "problematyka"
  ]
  node [
    id 311
    label "trudno&#347;&#263;"
  ]
  node [
    id 312
    label "pierepa&#322;ka"
  ]
  node [
    id 313
    label "ambaras"
  ]
  node [
    id 314
    label "nawarstwienie_si&#281;"
  ]
  node [
    id 315
    label "napotka&#263;"
  ]
  node [
    id 316
    label "g&#243;wniano&#347;&#263;"
  ]
  node [
    id 317
    label "k&#322;opotliwy"
  ]
  node [
    id 318
    label "napotkanie"
  ]
  node [
    id 319
    label "difficulty"
  ]
  node [
    id 320
    label "obstacle"
  ]
  node [
    id 321
    label "sytuacja"
  ]
  node [
    id 322
    label "kognicja"
  ]
  node [
    id 323
    label "object"
  ]
  node [
    id 324
    label "rozprawa"
  ]
  node [
    id 325
    label "temat"
  ]
  node [
    id 326
    label "wydarzenie"
  ]
  node [
    id 327
    label "szczeg&#243;&#322;"
  ]
  node [
    id 328
    label "proposition"
  ]
  node [
    id 329
    label "przes&#322;anka"
  ]
  node [
    id 330
    label "rzecz"
  ]
  node [
    id 331
    label "idea"
  ]
  node [
    id 332
    label "k&#322;opot"
  ]
  node [
    id 333
    label "zbi&#243;r"
  ]
  node [
    id 334
    label "jednoczesny"
  ]
  node [
    id 335
    label "unowocze&#347;nianie"
  ]
  node [
    id 336
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 337
    label "tera&#378;niejszy"
  ]
  node [
    id 338
    label "sp&#243;&#322;czesny"
  ]
  node [
    id 339
    label "uwsp&#243;&#322;cze&#347;nianie"
  ]
  node [
    id 340
    label "uwsp&#243;&#322;cze&#347;nienie"
  ]
  node [
    id 341
    label "jednocze&#347;nie"
  ]
  node [
    id 342
    label "modernizowanie_si&#281;"
  ]
  node [
    id 343
    label "ulepszanie"
  ]
  node [
    id 344
    label "automatyzowanie"
  ]
  node [
    id 345
    label "unowocze&#347;nienie"
  ]
  node [
    id 346
    label "mechanika"
  ]
  node [
    id 347
    label "fizyka_plazmy"
  ]
  node [
    id 348
    label "przedmiot"
  ]
  node [
    id 349
    label "interferometria"
  ]
  node [
    id 350
    label "akustyka"
  ]
  node [
    id 351
    label "fizyka_cz&#261;stek_elementarnych"
  ]
  node [
    id 352
    label "fizyka_medyczna"
  ]
  node [
    id 353
    label "teoria_p&#243;l_kwantowych"
  ]
  node [
    id 354
    label "mikrofizyka"
  ]
  node [
    id 355
    label "elektrostatyka"
  ]
  node [
    id 356
    label "elektryczno&#347;&#263;"
  ]
  node [
    id 357
    label "kierunek"
  ]
  node [
    id 358
    label "teoria_pola"
  ]
  node [
    id 359
    label "agrofizyka"
  ]
  node [
    id 360
    label "fizyka_statystyczna"
  ]
  node [
    id 361
    label "fizyka_kwantowa"
  ]
  node [
    id 362
    label "optyka"
  ]
  node [
    id 363
    label "elektromagnetyzm"
  ]
  node [
    id 364
    label "dylatometria"
  ]
  node [
    id 365
    label "elektryka"
  ]
  node [
    id 366
    label "dozymetria"
  ]
  node [
    id 367
    label "elektrodynamika"
  ]
  node [
    id 368
    label "spektroskopia"
  ]
  node [
    id 369
    label "rentgenologia"
  ]
  node [
    id 370
    label "fiza"
  ]
  node [
    id 371
    label "fizyka_j&#261;drowa"
  ]
  node [
    id 372
    label "geofizyka"
  ]
  node [
    id 373
    label "termodynamika"
  ]
  node [
    id 374
    label "fizyka_atomowa"
  ]
  node [
    id 375
    label "fizyka_teoretyczna"
  ]
  node [
    id 376
    label "elektrokinetyka"
  ]
  node [
    id 377
    label "fizyka_molekularna"
  ]
  node [
    id 378
    label "fizyka_cia&#322;a_sta&#322;ego"
  ]
  node [
    id 379
    label "pr&#243;&#380;nia"
  ]
  node [
    id 380
    label "nauka_przyrodnicza"
  ]
  node [
    id 381
    label "chemia_powierzchni"
  ]
  node [
    id 382
    label "kriofizyka"
  ]
  node [
    id 383
    label "zboczenie"
  ]
  node [
    id 384
    label "om&#243;wienie"
  ]
  node [
    id 385
    label "sponiewieranie"
  ]
  node [
    id 386
    label "discipline"
  ]
  node [
    id 387
    label "omawia&#263;"
  ]
  node [
    id 388
    label "kr&#261;&#380;enie"
  ]
  node [
    id 389
    label "tre&#347;&#263;"
  ]
  node [
    id 390
    label "robienie"
  ]
  node [
    id 391
    label "sponiewiera&#263;"
  ]
  node [
    id 392
    label "element"
  ]
  node [
    id 393
    label "entity"
  ]
  node [
    id 394
    label "tematyka"
  ]
  node [
    id 395
    label "w&#261;tek"
  ]
  node [
    id 396
    label "charakter"
  ]
  node [
    id 397
    label "zbaczanie"
  ]
  node [
    id 398
    label "program_nauczania"
  ]
  node [
    id 399
    label "om&#243;wi&#263;"
  ]
  node [
    id 400
    label "omawianie"
  ]
  node [
    id 401
    label "thing"
  ]
  node [
    id 402
    label "kultura"
  ]
  node [
    id 403
    label "istota"
  ]
  node [
    id 404
    label "zbacza&#263;"
  ]
  node [
    id 405
    label "zboczy&#263;"
  ]
  node [
    id 406
    label "przebieg"
  ]
  node [
    id 407
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 408
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 409
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 410
    label "praktyka"
  ]
  node [
    id 411
    label "system"
  ]
  node [
    id 412
    label "przeorientowywanie"
  ]
  node [
    id 413
    label "studia"
  ]
  node [
    id 414
    label "linia"
  ]
  node [
    id 415
    label "bok"
  ]
  node [
    id 416
    label "skr&#281;canie"
  ]
  node [
    id 417
    label "skr&#281;ca&#263;"
  ]
  node [
    id 418
    label "przeorientowywa&#263;"
  ]
  node [
    id 419
    label "orientowanie"
  ]
  node [
    id 420
    label "skr&#281;ci&#263;"
  ]
  node [
    id 421
    label "przeorientowanie"
  ]
  node [
    id 422
    label "zorientowanie"
  ]
  node [
    id 423
    label "przeorientowa&#263;"
  ]
  node [
    id 424
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 425
    label "metoda"
  ]
  node [
    id 426
    label "ty&#322;"
  ]
  node [
    id 427
    label "zorientowa&#263;"
  ]
  node [
    id 428
    label "g&#243;ra"
  ]
  node [
    id 429
    label "orientowa&#263;"
  ]
  node [
    id 430
    label "spos&#243;b"
  ]
  node [
    id 431
    label "ideologia"
  ]
  node [
    id 432
    label "orientacja"
  ]
  node [
    id 433
    label "prz&#243;d"
  ]
  node [
    id 434
    label "bearing"
  ]
  node [
    id 435
    label "skr&#281;cenie"
  ]
  node [
    id 436
    label "termodynamika_kwantowa"
  ]
  node [
    id 437
    label "termodynamika_chemiczna"
  ]
  node [
    id 438
    label "termodynamika_klasyczna"
  ]
  node [
    id 439
    label "perpetuum_mobile"
  ]
  node [
    id 440
    label "termodynamika_statystyczna"
  ]
  node [
    id 441
    label "termodynamika_techniczna"
  ]
  node [
    id 442
    label "geologia"
  ]
  node [
    id 443
    label "geodynamika"
  ]
  node [
    id 444
    label "sejsmologia"
  ]
  node [
    id 445
    label "paleomagnetyzm"
  ]
  node [
    id 446
    label "magnetometria"
  ]
  node [
    id 447
    label "nauki_o_Ziemi"
  ]
  node [
    id 448
    label "geoelektryka"
  ]
  node [
    id 449
    label "geotermika"
  ]
  node [
    id 450
    label "grawimetria"
  ]
  node [
    id 451
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 452
    label "energia"
  ]
  node [
    id 453
    label "naelektryzowanie"
  ]
  node [
    id 454
    label "elektryzowa&#263;"
  ]
  node [
    id 455
    label "zjawisko"
  ]
  node [
    id 456
    label "elektryzowanie"
  ]
  node [
    id 457
    label "naelektryzowa&#263;"
  ]
  node [
    id 458
    label "fluorescencyjna_spektroskopia_rentgenowska"
  ]
  node [
    id 459
    label "spektrometria"
  ]
  node [
    id 460
    label "spektroskopia_atomowa"
  ]
  node [
    id 461
    label "radiospektroskopia"
  ]
  node [
    id 462
    label "spektroskopia_optyczna"
  ]
  node [
    id 463
    label "spektroskopia_absorpcyjna"
  ]
  node [
    id 464
    label "spectroscopy"
  ]
  node [
    id 465
    label "spektroskopia_elektronowa"
  ]
  node [
    id 466
    label "magnetyzm"
  ]
  node [
    id 467
    label "agronomia"
  ]
  node [
    id 468
    label "nauka"
  ]
  node [
    id 469
    label "wy&#322;&#261;cznik"
  ]
  node [
    id 470
    label "mikroinstalacja"
  ]
  node [
    id 471
    label "instalacja"
  ]
  node [
    id 472
    label "rozdzielnica"
  ]
  node [
    id 473
    label "obw&#243;d"
  ]
  node [
    id 474
    label "kabel"
  ]
  node [
    id 475
    label "kontakt"
  ]
  node [
    id 476
    label "puszka"
  ]
  node [
    id 477
    label "rozszerzalno&#347;&#263;"
  ]
  node [
    id 478
    label "nauka_medyczna"
  ]
  node [
    id 479
    label "radiologia"
  ]
  node [
    id 480
    label "sejsmoakustyka"
  ]
  node [
    id 481
    label "transjent"
  ]
  node [
    id 482
    label "s&#322;yszalno&#347;&#263;"
  ]
  node [
    id 483
    label "hydroakustyka"
  ]
  node [
    id 484
    label "mod"
  ]
  node [
    id 485
    label "wibroakustyka"
  ]
  node [
    id 486
    label "acoustics"
  ]
  node [
    id 487
    label "dosimetry"
  ]
  node [
    id 488
    label "struktura"
  ]
  node [
    id 489
    label "mechanika_teoretyczna"
  ]
  node [
    id 490
    label "mechanika_gruntu"
  ]
  node [
    id 491
    label "mechanika_g&#243;rotworu"
  ]
  node [
    id 492
    label "mechanika_klasyczna"
  ]
  node [
    id 493
    label "elektromechanika"
  ]
  node [
    id 494
    label "mechanika_o&#347;rodk&#243;w_ci&#261;g&#322;ych"
  ]
  node [
    id 495
    label "ruch"
  ]
  node [
    id 496
    label "aeromechanika"
  ]
  node [
    id 497
    label "telemechanika"
  ]
  node [
    id 498
    label "hydromechanika"
  ]
  node [
    id 499
    label "optyka_geometryczna"
  ]
  node [
    id 500
    label "irradiacja"
  ]
  node [
    id 501
    label "dioptria"
  ]
  node [
    id 502
    label "optyka_adaptacyjna"
  ]
  node [
    id 503
    label "expectation"
  ]
  node [
    id 504
    label "elektrooptyka"
  ]
  node [
    id 505
    label "optyka_nieliniowa"
  ]
  node [
    id 506
    label "optyka_elektronowa"
  ]
  node [
    id 507
    label "aberracyjny"
  ]
  node [
    id 508
    label "fotonika"
  ]
  node [
    id 509
    label "fotometria"
  ]
  node [
    id 510
    label "dioptryka"
  ]
  node [
    id 511
    label "optyka_falowa"
  ]
  node [
    id 512
    label "transmitancja"
  ]
  node [
    id 513
    label "katoptryka"
  ]
  node [
    id 514
    label "wytw&#243;r"
  ]
  node [
    id 515
    label "posta&#263;"
  ]
  node [
    id 516
    label "widzie&#263;"
  ]
  node [
    id 517
    label "optyka_kwantowa"
  ]
  node [
    id 518
    label "holografia"
  ]
  node [
    id 519
    label "patrzenie"
  ]
  node [
    id 520
    label "patrze&#263;"
  ]
  node [
    id 521
    label "decentracja"
  ]
  node [
    id 522
    label "magnetooptyka"
  ]
  node [
    id 523
    label "pojmowanie"
  ]
  node [
    id 524
    label "kolorymetria"
  ]
  node [
    id 525
    label "przestrze&#324;"
  ]
  node [
    id 526
    label "futility"
  ]
  node [
    id 527
    label "nico&#347;&#263;"
  ]
  node [
    id 528
    label "astronom"
  ]
  node [
    id 529
    label "Anda"
  ]
  node [
    id 530
    label "Astrophysics"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 3
    target 161
  ]
  edge [
    source 3
    target 162
  ]
  edge [
    source 3
    target 163
  ]
  edge [
    source 3
    target 164
  ]
  edge [
    source 3
    target 165
  ]
  edge [
    source 3
    target 166
  ]
  edge [
    source 3
    target 167
  ]
  edge [
    source 3
    target 168
  ]
  edge [
    source 3
    target 169
  ]
  edge [
    source 3
    target 170
  ]
  edge [
    source 3
    target 171
  ]
  edge [
    source 3
    target 172
  ]
  edge [
    source 3
    target 173
  ]
  edge [
    source 3
    target 174
  ]
  edge [
    source 3
    target 175
  ]
  edge [
    source 3
    target 176
  ]
  edge [
    source 3
    target 177
  ]
  edge [
    source 3
    target 178
  ]
  edge [
    source 3
    target 179
  ]
  edge [
    source 3
    target 180
  ]
  edge [
    source 3
    target 181
  ]
  edge [
    source 3
    target 182
  ]
  edge [
    source 3
    target 183
  ]
  edge [
    source 3
    target 184
  ]
  edge [
    source 3
    target 185
  ]
  edge [
    source 3
    target 186
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 188
  ]
  edge [
    source 3
    target 189
  ]
  edge [
    source 3
    target 190
  ]
  edge [
    source 3
    target 191
  ]
  edge [
    source 3
    target 192
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 304
  ]
  edge [
    source 8
    target 305
  ]
  edge [
    source 8
    target 306
  ]
  edge [
    source 8
    target 307
  ]
  edge [
    source 8
    target 308
  ]
  edge [
    source 8
    target 309
  ]
  edge [
    source 8
    target 310
  ]
  edge [
    source 8
    target 311
  ]
  edge [
    source 8
    target 312
  ]
  edge [
    source 8
    target 313
  ]
  edge [
    source 8
    target 314
  ]
  edge [
    source 8
    target 315
  ]
  edge [
    source 8
    target 316
  ]
  edge [
    source 8
    target 317
  ]
  edge [
    source 8
    target 318
  ]
  edge [
    source 8
    target 180
  ]
  edge [
    source 8
    target 319
  ]
  edge [
    source 8
    target 320
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 321
  ]
  edge [
    source 8
    target 322
  ]
  edge [
    source 8
    target 323
  ]
  edge [
    source 8
    target 324
  ]
  edge [
    source 8
    target 325
  ]
  edge [
    source 8
    target 326
  ]
  edge [
    source 8
    target 327
  ]
  edge [
    source 8
    target 328
  ]
  edge [
    source 8
    target 329
  ]
  edge [
    source 8
    target 330
  ]
  edge [
    source 8
    target 331
  ]
  edge [
    source 8
    target 332
  ]
  edge [
    source 8
    target 333
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 334
  ]
  edge [
    source 9
    target 335
  ]
  edge [
    source 9
    target 336
  ]
  edge [
    source 9
    target 337
  ]
  edge [
    source 9
    target 338
  ]
  edge [
    source 9
    target 339
  ]
  edge [
    source 9
    target 340
  ]
  edge [
    source 9
    target 341
  ]
  edge [
    source 9
    target 342
  ]
  edge [
    source 9
    target 343
  ]
  edge [
    source 9
    target 344
  ]
  edge [
    source 9
    target 345
  ]
  edge [
    source 10
    target 346
  ]
  edge [
    source 10
    target 347
  ]
  edge [
    source 10
    target 348
  ]
  edge [
    source 10
    target 349
  ]
  edge [
    source 10
    target 350
  ]
  edge [
    source 10
    target 351
  ]
  edge [
    source 10
    target 352
  ]
  edge [
    source 10
    target 353
  ]
  edge [
    source 10
    target 354
  ]
  edge [
    source 10
    target 355
  ]
  edge [
    source 10
    target 356
  ]
  edge [
    source 10
    target 357
  ]
  edge [
    source 10
    target 358
  ]
  edge [
    source 10
    target 359
  ]
  edge [
    source 10
    target 360
  ]
  edge [
    source 10
    target 361
  ]
  edge [
    source 10
    target 362
  ]
  edge [
    source 10
    target 363
  ]
  edge [
    source 10
    target 364
  ]
  edge [
    source 10
    target 365
  ]
  edge [
    source 10
    target 366
  ]
  edge [
    source 10
    target 367
  ]
  edge [
    source 10
    target 368
  ]
  edge [
    source 10
    target 369
  ]
  edge [
    source 10
    target 370
  ]
  edge [
    source 10
    target 371
  ]
  edge [
    source 10
    target 372
  ]
  edge [
    source 10
    target 373
  ]
  edge [
    source 10
    target 374
  ]
  edge [
    source 10
    target 375
  ]
  edge [
    source 10
    target 376
  ]
  edge [
    source 10
    target 377
  ]
  edge [
    source 10
    target 378
  ]
  edge [
    source 10
    target 379
  ]
  edge [
    source 10
    target 380
  ]
  edge [
    source 10
    target 381
  ]
  edge [
    source 10
    target 382
  ]
  edge [
    source 10
    target 383
  ]
  edge [
    source 10
    target 384
  ]
  edge [
    source 10
    target 385
  ]
  edge [
    source 10
    target 386
  ]
  edge [
    source 10
    target 330
  ]
  edge [
    source 10
    target 387
  ]
  edge [
    source 10
    target 388
  ]
  edge [
    source 10
    target 389
  ]
  edge [
    source 10
    target 390
  ]
  edge [
    source 10
    target 391
  ]
  edge [
    source 10
    target 392
  ]
  edge [
    source 10
    target 393
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 394
  ]
  edge [
    source 10
    target 395
  ]
  edge [
    source 10
    target 396
  ]
  edge [
    source 10
    target 397
  ]
  edge [
    source 10
    target 398
  ]
  edge [
    source 10
    target 399
  ]
  edge [
    source 10
    target 400
  ]
  edge [
    source 10
    target 401
  ]
  edge [
    source 10
    target 402
  ]
  edge [
    source 10
    target 403
  ]
  edge [
    source 10
    target 404
  ]
  edge [
    source 10
    target 405
  ]
  edge [
    source 10
    target 406
  ]
  edge [
    source 10
    target 407
  ]
  edge [
    source 10
    target 408
  ]
  edge [
    source 10
    target 409
  ]
  edge [
    source 10
    target 410
  ]
  edge [
    source 10
    target 411
  ]
  edge [
    source 10
    target 412
  ]
  edge [
    source 10
    target 413
  ]
  edge [
    source 10
    target 414
  ]
  edge [
    source 10
    target 415
  ]
  edge [
    source 10
    target 416
  ]
  edge [
    source 10
    target 417
  ]
  edge [
    source 10
    target 418
  ]
  edge [
    source 10
    target 419
  ]
  edge [
    source 10
    target 420
  ]
  edge [
    source 10
    target 421
  ]
  edge [
    source 10
    target 422
  ]
  edge [
    source 10
    target 423
  ]
  edge [
    source 10
    target 424
  ]
  edge [
    source 10
    target 425
  ]
  edge [
    source 10
    target 426
  ]
  edge [
    source 10
    target 427
  ]
  edge [
    source 10
    target 428
  ]
  edge [
    source 10
    target 429
  ]
  edge [
    source 10
    target 430
  ]
  edge [
    source 10
    target 431
  ]
  edge [
    source 10
    target 432
  ]
  edge [
    source 10
    target 433
  ]
  edge [
    source 10
    target 434
  ]
  edge [
    source 10
    target 435
  ]
  edge [
    source 10
    target 436
  ]
  edge [
    source 10
    target 437
  ]
  edge [
    source 10
    target 438
  ]
  edge [
    source 10
    target 439
  ]
  edge [
    source 10
    target 440
  ]
  edge [
    source 10
    target 441
  ]
  edge [
    source 10
    target 442
  ]
  edge [
    source 10
    target 443
  ]
  edge [
    source 10
    target 444
  ]
  edge [
    source 10
    target 445
  ]
  edge [
    source 10
    target 446
  ]
  edge [
    source 10
    target 447
  ]
  edge [
    source 10
    target 448
  ]
  edge [
    source 10
    target 449
  ]
  edge [
    source 10
    target 450
  ]
  edge [
    source 10
    target 451
  ]
  edge [
    source 10
    target 452
  ]
  edge [
    source 10
    target 453
  ]
  edge [
    source 10
    target 454
  ]
  edge [
    source 10
    target 455
  ]
  edge [
    source 10
    target 456
  ]
  edge [
    source 10
    target 457
  ]
  edge [
    source 10
    target 458
  ]
  edge [
    source 10
    target 459
  ]
  edge [
    source 10
    target 460
  ]
  edge [
    source 10
    target 461
  ]
  edge [
    source 10
    target 462
  ]
  edge [
    source 10
    target 463
  ]
  edge [
    source 10
    target 464
  ]
  edge [
    source 10
    target 465
  ]
  edge [
    source 10
    target 466
  ]
  edge [
    source 10
    target 467
  ]
  edge [
    source 10
    target 468
  ]
  edge [
    source 10
    target 469
  ]
  edge [
    source 10
    target 470
  ]
  edge [
    source 10
    target 471
  ]
  edge [
    source 10
    target 472
  ]
  edge [
    source 10
    target 473
  ]
  edge [
    source 10
    target 474
  ]
  edge [
    source 10
    target 475
  ]
  edge [
    source 10
    target 476
  ]
  edge [
    source 10
    target 477
  ]
  edge [
    source 10
    target 478
  ]
  edge [
    source 10
    target 479
  ]
  edge [
    source 10
    target 480
  ]
  edge [
    source 10
    target 481
  ]
  edge [
    source 10
    target 482
  ]
  edge [
    source 10
    target 483
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 484
  ]
  edge [
    source 10
    target 485
  ]
  edge [
    source 10
    target 486
  ]
  edge [
    source 10
    target 487
  ]
  edge [
    source 10
    target 488
  ]
  edge [
    source 10
    target 489
  ]
  edge [
    source 10
    target 490
  ]
  edge [
    source 10
    target 491
  ]
  edge [
    source 10
    target 492
  ]
  edge [
    source 10
    target 493
  ]
  edge [
    source 10
    target 494
  ]
  edge [
    source 10
    target 495
  ]
  edge [
    source 10
    target 496
  ]
  edge [
    source 10
    target 497
  ]
  edge [
    source 10
    target 498
  ]
  edge [
    source 10
    target 499
  ]
  edge [
    source 10
    target 500
  ]
  edge [
    source 10
    target 501
  ]
  edge [
    source 10
    target 502
  ]
  edge [
    source 10
    target 503
  ]
  edge [
    source 10
    target 504
  ]
  edge [
    source 10
    target 505
  ]
  edge [
    source 10
    target 506
  ]
  edge [
    source 10
    target 507
  ]
  edge [
    source 10
    target 508
  ]
  edge [
    source 10
    target 509
  ]
  edge [
    source 10
    target 510
  ]
  edge [
    source 10
    target 511
  ]
  edge [
    source 10
    target 512
  ]
  edge [
    source 10
    target 513
  ]
  edge [
    source 10
    target 514
  ]
  edge [
    source 10
    target 515
  ]
  edge [
    source 10
    target 516
  ]
  edge [
    source 10
    target 517
  ]
  edge [
    source 10
    target 518
  ]
  edge [
    source 10
    target 519
  ]
  edge [
    source 10
    target 520
  ]
  edge [
    source 10
    target 521
  ]
  edge [
    source 10
    target 522
  ]
  edge [
    source 10
    target 523
  ]
  edge [
    source 10
    target 524
  ]
  edge [
    source 10
    target 525
  ]
  edge [
    source 10
    target 526
  ]
  edge [
    source 10
    target 527
  ]
  edge [
    source 528
    target 529
  ]
  edge [
    source 528
    target 530
  ]
  edge [
    source 529
    target 530
  ]
]
