graph [
  node [
    id 0
    label "johnson"
    origin "text"
  ]
  node [
    id 1
    label "5905"
  ]
  node [
    id 2
    label "Johnson"
  ]
  node [
    id 3
    label "1989"
  ]
  node [
    id 4
    label "CJ1"
  ]
  node [
    id 5
    label "Eleanor&#281;"
  ]
  node [
    id 6
    label "Helin"
  ]
  node [
    id 7
    label "dzienny"
  ]
  node [
    id 8
    label "gwiazda"
  ]
  node [
    id 9
    label "p&#243;&#378;no"
  ]
  node [
    id 10
    label "Pravec"
  ]
  node [
    id 11
    label "albo"
  ]
  node [
    id 12
    label "Galad"
  ]
  node [
    id 13
    label "syn"
  ]
  node [
    id 14
    label "Gajdos"
  ]
  node [
    id 15
    label "by&#322;y"
  ]
  node [
    id 16
    label "Warner"
  ]
  node [
    id 17
    label "dzie&#324;"
  ]
  node [
    id 18
    label "Pray"
  ]
  node [
    id 19
    label "Brown"
  ]
  node [
    id 20
    label "zeszyt"
  ]
  node [
    id 21
    label "Krzeminski"
  ]
  node [
    id 22
    label "Kusnirak"
  ]
  node [
    id 23
    label "2005"
  ]
  node [
    id 24
    label "1"
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 19
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 23
    target 24
  ]
]
