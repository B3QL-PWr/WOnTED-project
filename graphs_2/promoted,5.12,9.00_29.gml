graph [
  node [
    id 0
    label "szanowny"
    origin "text"
  ]
  node [
    id 1
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "spora"
    origin "text"
  ]
  node [
    id 4
    label "afera"
    origin "text"
  ]
  node [
    id 5
    label "bank"
    origin "text"
  ]
  node [
    id 6
    label "idea"
    origin "text"
  ]
  node [
    id 7
    label "dzia&#322;a&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ruski"
    origin "text"
  ]
  node [
    id 9
    label "zasada"
    origin "text"
  ]
  node [
    id 10
    label "znaczy&#263;"
    origin "text"
  ]
  node [
    id 11
    label "jak"
    origin "text"
  ]
  node [
    id 12
    label "upomnie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "szacownie"
  ]
  node [
    id 14
    label "szacowny"
  ]
  node [
    id 15
    label "robi&#263;"
  ]
  node [
    id 16
    label "take_care"
  ]
  node [
    id 17
    label "troska&#263;_si&#281;"
  ]
  node [
    id 18
    label "deliver"
  ]
  node [
    id 19
    label "rozpatrywa&#263;"
  ]
  node [
    id 20
    label "zamierza&#263;"
  ]
  node [
    id 21
    label "argue"
  ]
  node [
    id 22
    label "os&#261;dza&#263;"
  ]
  node [
    id 23
    label "strike"
  ]
  node [
    id 24
    label "s&#261;dzi&#263;"
  ]
  node [
    id 25
    label "powodowa&#263;"
  ]
  node [
    id 26
    label "znajdowa&#263;"
  ]
  node [
    id 27
    label "hold"
  ]
  node [
    id 28
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 29
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 30
    label "przeprowadza&#263;"
  ]
  node [
    id 31
    label "consider"
  ]
  node [
    id 32
    label "volunteer"
  ]
  node [
    id 33
    label "organizowa&#263;"
  ]
  node [
    id 34
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 35
    label "czyni&#263;"
  ]
  node [
    id 36
    label "give"
  ]
  node [
    id 37
    label "stylizowa&#263;"
  ]
  node [
    id 38
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 39
    label "falowa&#263;"
  ]
  node [
    id 40
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 41
    label "peddle"
  ]
  node [
    id 42
    label "praca"
  ]
  node [
    id 43
    label "wydala&#263;"
  ]
  node [
    id 44
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "tentegowa&#263;"
  ]
  node [
    id 46
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 47
    label "urz&#261;dza&#263;"
  ]
  node [
    id 48
    label "oszukiwa&#263;"
  ]
  node [
    id 49
    label "work"
  ]
  node [
    id 50
    label "ukazywa&#263;"
  ]
  node [
    id 51
    label "przerabia&#263;"
  ]
  node [
    id 52
    label "act"
  ]
  node [
    id 53
    label "post&#281;powa&#263;"
  ]
  node [
    id 54
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 55
    label "mie&#263;_miejsce"
  ]
  node [
    id 56
    label "equal"
  ]
  node [
    id 57
    label "trwa&#263;"
  ]
  node [
    id 58
    label "chodzi&#263;"
  ]
  node [
    id 59
    label "si&#281;ga&#263;"
  ]
  node [
    id 60
    label "stan"
  ]
  node [
    id 61
    label "obecno&#347;&#263;"
  ]
  node [
    id 62
    label "stand"
  ]
  node [
    id 63
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 64
    label "uczestniczy&#263;"
  ]
  node [
    id 65
    label "participate"
  ]
  node [
    id 66
    label "istnie&#263;"
  ]
  node [
    id 67
    label "pozostawa&#263;"
  ]
  node [
    id 68
    label "zostawa&#263;"
  ]
  node [
    id 69
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 70
    label "adhere"
  ]
  node [
    id 71
    label "compass"
  ]
  node [
    id 72
    label "korzysta&#263;"
  ]
  node [
    id 73
    label "appreciation"
  ]
  node [
    id 74
    label "osi&#261;ga&#263;"
  ]
  node [
    id 75
    label "dociera&#263;"
  ]
  node [
    id 76
    label "get"
  ]
  node [
    id 77
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 78
    label "mierzy&#263;"
  ]
  node [
    id 79
    label "u&#380;ywa&#263;"
  ]
  node [
    id 80
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 81
    label "zaczyna&#263;_si&#281;"
  ]
  node [
    id 82
    label "exsert"
  ]
  node [
    id 83
    label "being"
  ]
  node [
    id 84
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 85
    label "cecha"
  ]
  node [
    id 86
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 87
    label "przytrafia&#263;_si&#281;"
  ]
  node [
    id 88
    label "p&#322;ywa&#263;"
  ]
  node [
    id 89
    label "run"
  ]
  node [
    id 90
    label "bangla&#263;"
  ]
  node [
    id 91
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 92
    label "przebiega&#263;"
  ]
  node [
    id 93
    label "wk&#322;ada&#263;"
  ]
  node [
    id 94
    label "proceed"
  ]
  node [
    id 95
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 96
    label "carry"
  ]
  node [
    id 97
    label "bywa&#263;"
  ]
  node [
    id 98
    label "dziama&#263;"
  ]
  node [
    id 99
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 100
    label "stara&#263;_si&#281;"
  ]
  node [
    id 101
    label "para"
  ]
  node [
    id 102
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 103
    label "str&#243;j"
  ]
  node [
    id 104
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 105
    label "spotyka&#263;_si&#281;"
  ]
  node [
    id 106
    label "krok"
  ]
  node [
    id 107
    label "tryb"
  ]
  node [
    id 108
    label "sprzedawa&#263;_si&#281;"
  ]
  node [
    id 109
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 110
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 111
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 112
    label "ucz&#281;szcza&#263;"
  ]
  node [
    id 113
    label "continue"
  ]
  node [
    id 114
    label "z&#380;yma&#263;_si&#281;"
  ]
  node [
    id 115
    label "Ohio"
  ]
  node [
    id 116
    label "wci&#281;cie"
  ]
  node [
    id 117
    label "Nowy_York"
  ]
  node [
    id 118
    label "warstwa"
  ]
  node [
    id 119
    label "samopoczucie"
  ]
  node [
    id 120
    label "Illinois"
  ]
  node [
    id 121
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 122
    label "state"
  ]
  node [
    id 123
    label "Jukatan"
  ]
  node [
    id 124
    label "Kalifornia"
  ]
  node [
    id 125
    label "Wirginia"
  ]
  node [
    id 126
    label "wektor"
  ]
  node [
    id 127
    label "Teksas"
  ]
  node [
    id 128
    label "Goa"
  ]
  node [
    id 129
    label "Waszyngton"
  ]
  node [
    id 130
    label "miejsce"
  ]
  node [
    id 131
    label "Massachusetts"
  ]
  node [
    id 132
    label "Alaska"
  ]
  node [
    id 133
    label "Arakan"
  ]
  node [
    id 134
    label "Hawaje"
  ]
  node [
    id 135
    label "Maryland"
  ]
  node [
    id 136
    label "punkt"
  ]
  node [
    id 137
    label "Michigan"
  ]
  node [
    id 138
    label "Arizona"
  ]
  node [
    id 139
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 140
    label "Georgia"
  ]
  node [
    id 141
    label "poziom"
  ]
  node [
    id 142
    label "Pensylwania"
  ]
  node [
    id 143
    label "shape"
  ]
  node [
    id 144
    label "Luizjana"
  ]
  node [
    id 145
    label "Nowy_Meksyk"
  ]
  node [
    id 146
    label "Alabama"
  ]
  node [
    id 147
    label "ilo&#347;&#263;"
  ]
  node [
    id 148
    label "Kansas"
  ]
  node [
    id 149
    label "Oregon"
  ]
  node [
    id 150
    label "Floryda"
  ]
  node [
    id 151
    label "Oklahoma"
  ]
  node [
    id 152
    label "jednostka_administracyjna"
  ]
  node [
    id 153
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 154
    label "intyna"
  ]
  node [
    id 155
    label "egzyna"
  ]
  node [
    id 156
    label "spore"
  ]
  node [
    id 157
    label "kom&#243;rka_ro&#347;linna"
  ]
  node [
    id 158
    label "zarodnik"
  ]
  node [
    id 159
    label "sporoderma"
  ]
  node [
    id 160
    label "py&#322;ek"
  ]
  node [
    id 161
    label "smr&#243;d"
  ]
  node [
    id 162
    label "cyrk"
  ]
  node [
    id 163
    label "przest&#281;pstwo"
  ]
  node [
    id 164
    label "skandal"
  ]
  node [
    id 165
    label "occupation"
  ]
  node [
    id 166
    label "argument"
  ]
  node [
    id 167
    label "sensacja"
  ]
  node [
    id 168
    label "brudny"
  ]
  node [
    id 169
    label "przest&#281;pczo&#347;&#263;"
  ]
  node [
    id 170
    label "niesprawiedliwo&#347;&#263;"
  ]
  node [
    id 171
    label "crime"
  ]
  node [
    id 172
    label "sprawstwo"
  ]
  node [
    id 173
    label "nadu&#380;ycie"
  ]
  node [
    id 174
    label "novum"
  ]
  node [
    id 175
    label "zamieszanie"
  ]
  node [
    id 176
    label "rozg&#322;os"
  ]
  node [
    id 177
    label "disclosure"
  ]
  node [
    id 178
    label "niespodzianka"
  ]
  node [
    id 179
    label "podekscytowanie"
  ]
  node [
    id 180
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 181
    label "fug"
  ]
  node [
    id 182
    label "zapach"
  ]
  node [
    id 183
    label "dziecko"
  ]
  node [
    id 184
    label "zapaszek"
  ]
  node [
    id 185
    label "atmosfera"
  ]
  node [
    id 186
    label "wyj&#347;&#263;_na_jaw"
  ]
  node [
    id 187
    label "wolty&#380;erka"
  ]
  node [
    id 188
    label "repryza"
  ]
  node [
    id 189
    label "ekwilibrystyka"
  ]
  node [
    id 190
    label "nied&#378;wiednik"
  ]
  node [
    id 191
    label "tresura"
  ]
  node [
    id 192
    label "hipodrom"
  ]
  node [
    id 193
    label "instytucja"
  ]
  node [
    id 194
    label "przedstawienie"
  ]
  node [
    id 195
    label "namiot"
  ]
  node [
    id 196
    label "budynek"
  ]
  node [
    id 197
    label "circus"
  ]
  node [
    id 198
    label "heca"
  ]
  node [
    id 199
    label "arena"
  ]
  node [
    id 200
    label "akrobacja"
  ]
  node [
    id 201
    label "klownada"
  ]
  node [
    id 202
    label "grupa"
  ]
  node [
    id 203
    label "amfiteatr"
  ]
  node [
    id 204
    label "trybuna"
  ]
  node [
    id 205
    label "parametr"
  ]
  node [
    id 206
    label "s&#261;d"
  ]
  node [
    id 207
    label "operand"
  ]
  node [
    id 208
    label "dow&#243;d"
  ]
  node [
    id 209
    label "zmienna"
  ]
  node [
    id 210
    label "argumentacja"
  ]
  node [
    id 211
    label "rzecz"
  ]
  node [
    id 212
    label "agent_rozliczeniowy"
  ]
  node [
    id 213
    label "kwota"
  ]
  node [
    id 214
    label "zbi&#243;r"
  ]
  node [
    id 215
    label "konto"
  ]
  node [
    id 216
    label "siedziba"
  ]
  node [
    id 217
    label "wk&#322;adca"
  ]
  node [
    id 218
    label "agencja"
  ]
  node [
    id 219
    label "eurorynek"
  ]
  node [
    id 220
    label "egzemplarz"
  ]
  node [
    id 221
    label "series"
  ]
  node [
    id 222
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 223
    label "uprawianie"
  ]
  node [
    id 224
    label "praca_rolnicza"
  ]
  node [
    id 225
    label "collection"
  ]
  node [
    id 226
    label "dane"
  ]
  node [
    id 227
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 228
    label "pakiet_klimatyczny"
  ]
  node [
    id 229
    label "poj&#281;cie"
  ]
  node [
    id 230
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 231
    label "sum"
  ]
  node [
    id 232
    label "gathering"
  ]
  node [
    id 233
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 234
    label "album"
  ]
  node [
    id 235
    label "wynie&#347;&#263;"
  ]
  node [
    id 236
    label "pieni&#261;dze"
  ]
  node [
    id 237
    label "limit"
  ]
  node [
    id 238
    label "wynosi&#263;"
  ]
  node [
    id 239
    label "osoba_prawna"
  ]
  node [
    id 240
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 241
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 242
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 243
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 244
    label "biuro"
  ]
  node [
    id 245
    label "organizacja"
  ]
  node [
    id 246
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 247
    label "Fundusze_Unijne"
  ]
  node [
    id 248
    label "zamyka&#263;"
  ]
  node [
    id 249
    label "establishment"
  ]
  node [
    id 250
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 251
    label "urz&#261;d"
  ]
  node [
    id 252
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 253
    label "afiliowa&#263;"
  ]
  node [
    id 254
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 255
    label "standard"
  ]
  node [
    id 256
    label "zamykanie"
  ]
  node [
    id 257
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 258
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 259
    label "&#321;ubianka"
  ]
  node [
    id 260
    label "miejsce_pracy"
  ]
  node [
    id 261
    label "dzia&#322;_personalny"
  ]
  node [
    id 262
    label "Kreml"
  ]
  node [
    id 263
    label "Bia&#322;y_Dom"
  ]
  node [
    id 264
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 265
    label "sadowisko"
  ]
  node [
    id 266
    label "rynek_finansowy"
  ]
  node [
    id 267
    label "sie&#263;"
  ]
  node [
    id 268
    label "rynek_mi&#281;dzynarodowy"
  ]
  node [
    id 269
    label "ajencja"
  ]
  node [
    id 270
    label "whole"
  ]
  node [
    id 271
    label "przedstawicielstwo"
  ]
  node [
    id 272
    label "firma"
  ]
  node [
    id 273
    label "NASA"
  ]
  node [
    id 274
    label "oddzia&#322;"
  ]
  node [
    id 275
    label "dzia&#322;"
  ]
  node [
    id 276
    label "filia"
  ]
  node [
    id 277
    label "klient"
  ]
  node [
    id 278
    label "rejestrowa&#263;_si&#281;"
  ]
  node [
    id 279
    label "dorobek"
  ]
  node [
    id 280
    label "mienie"
  ]
  node [
    id 281
    label "subkonto"
  ]
  node [
    id 282
    label "logowa&#263;_si&#281;"
  ]
  node [
    id 283
    label "debet"
  ]
  node [
    id 284
    label "ksi&#281;gowo&#347;&#263;"
  ]
  node [
    id 285
    label "kariera"
  ]
  node [
    id 286
    label "reprezentacja"
  ]
  node [
    id 287
    label "dost&#281;p"
  ]
  node [
    id 288
    label "rachunek"
  ]
  node [
    id 289
    label "kredyt"
  ]
  node [
    id 290
    label "ideologia"
  ]
  node [
    id 291
    label "byt"
  ]
  node [
    id 292
    label "intelekt"
  ]
  node [
    id 293
    label "Kant"
  ]
  node [
    id 294
    label "p&#322;&#243;d"
  ]
  node [
    id 295
    label "cel"
  ]
  node [
    id 296
    label "istota"
  ]
  node [
    id 297
    label "pomys&#322;"
  ]
  node [
    id 298
    label "ideacja"
  ]
  node [
    id 299
    label "mentalno&#347;&#263;"
  ]
  node [
    id 300
    label "superego"
  ]
  node [
    id 301
    label "psychika"
  ]
  node [
    id 302
    label "znaczenie"
  ]
  node [
    id 303
    label "wn&#281;trze"
  ]
  node [
    id 304
    label "charakter"
  ]
  node [
    id 305
    label "przew&#243;d_t&#281;tniczy"
  ]
  node [
    id 306
    label "wytw&#243;r"
  ]
  node [
    id 307
    label "moczownik"
  ]
  node [
    id 308
    label "embryo"
  ]
  node [
    id 309
    label "ma&#378;_p&#322;odowa"
  ]
  node [
    id 310
    label "zarodek"
  ]
  node [
    id 311
    label "&#380;y&#322;a_p&#281;powinowa"
  ]
  node [
    id 312
    label "latawiec"
  ]
  node [
    id 313
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 314
    label "rezultat"
  ]
  node [
    id 315
    label "thing"
  ]
  node [
    id 316
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 317
    label "pos&#322;uchanie"
  ]
  node [
    id 318
    label "skumanie"
  ]
  node [
    id 319
    label "orientacja"
  ]
  node [
    id 320
    label "zorientowanie"
  ]
  node [
    id 321
    label "teoria"
  ]
  node [
    id 322
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 323
    label "clasp"
  ]
  node [
    id 324
    label "forma"
  ]
  node [
    id 325
    label "przem&#243;wienie"
  ]
  node [
    id 326
    label "umys&#322;"
  ]
  node [
    id 327
    label "wiedza"
  ]
  node [
    id 328
    label "noosfera"
  ]
  node [
    id 329
    label "utrzymywanie"
  ]
  node [
    id 330
    label "bycie"
  ]
  node [
    id 331
    label "entity"
  ]
  node [
    id 332
    label "subsystencja"
  ]
  node [
    id 333
    label "utrzyma&#263;"
  ]
  node [
    id 334
    label "egzystencja"
  ]
  node [
    id 335
    label "wy&#380;ywienie"
  ]
  node [
    id 336
    label "ontologicznie"
  ]
  node [
    id 337
    label "utrzymanie"
  ]
  node [
    id 338
    label "potencja"
  ]
  node [
    id 339
    label "utrzymywa&#263;"
  ]
  node [
    id 340
    label "pocz&#261;tki"
  ]
  node [
    id 341
    label "ukra&#347;&#263;"
  ]
  node [
    id 342
    label "ukradzenie"
  ]
  node [
    id 343
    label "system"
  ]
  node [
    id 344
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 345
    label "czysty_rozum"
  ]
  node [
    id 346
    label "noumenon"
  ]
  node [
    id 347
    label "filozofia"
  ]
  node [
    id 348
    label "kantysta"
  ]
  node [
    id 349
    label "fenomenologia"
  ]
  node [
    id 350
    label "zdolno&#347;&#263;"
  ]
  node [
    id 351
    label "political_orientation"
  ]
  node [
    id 352
    label "szko&#322;a"
  ]
  node [
    id 353
    label "function"
  ]
  node [
    id 354
    label "determine"
  ]
  node [
    id 355
    label "reakcja_chemiczna"
  ]
  node [
    id 356
    label "commit"
  ]
  node [
    id 357
    label "przyczynia&#263;_si&#281;"
  ]
  node [
    id 358
    label "motywowa&#263;"
  ]
  node [
    id 359
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 360
    label "prze&#380;uwa&#263;"
  ]
  node [
    id 361
    label "rozumie&#263;"
  ]
  node [
    id 362
    label "szczeka&#263;"
  ]
  node [
    id 363
    label "rozmawia&#263;"
  ]
  node [
    id 364
    label "m&#243;wi&#263;"
  ]
  node [
    id 365
    label "funkcjonowa&#263;"
  ]
  node [
    id 366
    label "ko&#322;o"
  ]
  node [
    id 367
    label "spos&#243;b"
  ]
  node [
    id 368
    label "modalno&#347;&#263;"
  ]
  node [
    id 369
    label "z&#261;b"
  ]
  node [
    id 370
    label "kategoria_gramatyczna"
  ]
  node [
    id 371
    label "skala"
  ]
  node [
    id 372
    label "ko&#322;o_z&#281;bate"
  ]
  node [
    id 373
    label "koniugacja"
  ]
  node [
    id 374
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 375
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 376
    label "ostentacyjny"
  ]
  node [
    id 377
    label "ziemniaczany"
  ]
  node [
    id 378
    label "kacapski"
  ]
  node [
    id 379
    label "toporny"
  ]
  node [
    id 380
    label "j&#281;zyki_wschodnios&#322;owia&#324;skie"
  ]
  node [
    id 381
    label "serowy"
  ]
  node [
    id 382
    label "wulgarny"
  ]
  node [
    id 383
    label "po_rosyjsku"
  ]
  node [
    id 384
    label "wielkoruski"
  ]
  node [
    id 385
    label "Russian"
  ]
  node [
    id 386
    label "byle_jaki"
  ]
  node [
    id 387
    label "j&#281;zyk"
  ]
  node [
    id 388
    label "zbytkowny"
  ]
  node [
    id 389
    label "po_rusku"
  ]
  node [
    id 390
    label "rusek"
  ]
  node [
    id 391
    label "tandetny"
  ]
  node [
    id 392
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 393
    label "artykulator"
  ]
  node [
    id 394
    label "kod"
  ]
  node [
    id 395
    label "kawa&#322;ek"
  ]
  node [
    id 396
    label "przedmiot"
  ]
  node [
    id 397
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 398
    label "gramatyka"
  ]
  node [
    id 399
    label "stylik"
  ]
  node [
    id 400
    label "przet&#322;umaczenie"
  ]
  node [
    id 401
    label "formalizowanie"
  ]
  node [
    id 402
    label "ssa&#263;"
  ]
  node [
    id 403
    label "ssanie"
  ]
  node [
    id 404
    label "language"
  ]
  node [
    id 405
    label "liza&#263;"
  ]
  node [
    id 406
    label "napisa&#263;"
  ]
  node [
    id 407
    label "konsonantyzm"
  ]
  node [
    id 408
    label "wokalizm"
  ]
  node [
    id 409
    label "pisa&#263;"
  ]
  node [
    id 410
    label "fonetyka"
  ]
  node [
    id 411
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 412
    label "jeniec"
  ]
  node [
    id 413
    label "but"
  ]
  node [
    id 414
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 415
    label "po_koroniarsku"
  ]
  node [
    id 416
    label "kultura_duchowa"
  ]
  node [
    id 417
    label "t&#322;umaczenie"
  ]
  node [
    id 418
    label "m&#243;wienie"
  ]
  node [
    id 419
    label "pype&#263;"
  ]
  node [
    id 420
    label "lizanie"
  ]
  node [
    id 421
    label "pismo"
  ]
  node [
    id 422
    label "formalizowa&#263;"
  ]
  node [
    id 423
    label "organ"
  ]
  node [
    id 424
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 425
    label "rozumienie"
  ]
  node [
    id 426
    label "makroglosja"
  ]
  node [
    id 427
    label "jama_ustna"
  ]
  node [
    id 428
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 429
    label "formacja_geologiczna"
  ]
  node [
    id 430
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 431
    label "natural_language"
  ]
  node [
    id 432
    label "s&#322;ownictwo"
  ]
  node [
    id 433
    label "urz&#261;dzenie"
  ]
  node [
    id 434
    label "wschodnioeuropejski"
  ]
  node [
    id 435
    label "s&#322;awia&#324;ski"
  ]
  node [
    id 436
    label "poga&#324;ski"
  ]
  node [
    id 437
    label "po_s&#322;owia&#324;sku"
  ]
  node [
    id 438
    label "topielec"
  ]
  node [
    id 439
    label "europejski"
  ]
  node [
    id 440
    label "niezgrabny"
  ]
  node [
    id 441
    label "topornie"
  ]
  node [
    id 442
    label "kaleki"
  ]
  node [
    id 443
    label "ci&#281;&#380;ki"
  ]
  node [
    id 444
    label "kiczowaty"
  ]
  node [
    id 445
    label "banalny"
  ]
  node [
    id 446
    label "nieelegancki"
  ]
  node [
    id 447
    label "&#380;a&#322;osny"
  ]
  node [
    id 448
    label "tani"
  ]
  node [
    id 449
    label "kiepski"
  ]
  node [
    id 450
    label "tandetnie"
  ]
  node [
    id 451
    label "odra&#380;aj&#261;cy"
  ]
  node [
    id 452
    label "nikczemny"
  ]
  node [
    id 453
    label "ostentacyjnie"
  ]
  node [
    id 454
    label "nadmierny"
  ]
  node [
    id 455
    label "obnoszenie_si&#281;"
  ]
  node [
    id 456
    label "&#322;askawy"
  ]
  node [
    id 457
    label "niedyskretny"
  ]
  node [
    id 458
    label "wystawnie"
  ]
  node [
    id 459
    label "zb&#281;dny"
  ]
  node [
    id 460
    label "zbytkownie"
  ]
  node [
    id 461
    label "bogaty"
  ]
  node [
    id 462
    label "prostacki"
  ]
  node [
    id 463
    label "sp&#322;ycony"
  ]
  node [
    id 464
    label "niegrzeczny"
  ]
  node [
    id 465
    label "grubia&#324;ski"
  ]
  node [
    id 466
    label "nachalny"
  ]
  node [
    id 467
    label "ra&#380;&#261;cy"
  ]
  node [
    id 468
    label "wulgarnie"
  ]
  node [
    id 469
    label "ciep&#322;y"
  ]
  node [
    id 470
    label "warzywny"
  ]
  node [
    id 471
    label "przypominaj&#261;cy"
  ]
  node [
    id 472
    label "rosyjski"
  ]
  node [
    id 473
    label "j&#281;zyk_rosyjski"
  ]
  node [
    id 474
    label "po_kacapsku"
  ]
  node [
    id 475
    label "imperialny"
  ]
  node [
    id 476
    label "po_wielkorusku"
  ]
  node [
    id 477
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 478
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 479
    label "regu&#322;a_Allena"
  ]
  node [
    id 480
    label "base"
  ]
  node [
    id 481
    label "umowa"
  ]
  node [
    id 482
    label "obserwacja"
  ]
  node [
    id 483
    label "zasada_d'Alemberta"
  ]
  node [
    id 484
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 485
    label "normalizacja"
  ]
  node [
    id 486
    label "moralno&#347;&#263;"
  ]
  node [
    id 487
    label "criterion"
  ]
  node [
    id 488
    label "opis"
  ]
  node [
    id 489
    label "regu&#322;a_Glogera"
  ]
  node [
    id 490
    label "prawo_Mendla"
  ]
  node [
    id 491
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 492
    label "twierdzenie"
  ]
  node [
    id 493
    label "prawo"
  ]
  node [
    id 494
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 495
    label "dominion"
  ]
  node [
    id 496
    label "qualification"
  ]
  node [
    id 497
    label "podstawa"
  ]
  node [
    id 498
    label "substancja"
  ]
  node [
    id 499
    label "prawid&#322;o"
  ]
  node [
    id 500
    label "dobro&#263;"
  ]
  node [
    id 501
    label "aretologia"
  ]
  node [
    id 502
    label "zesp&#243;&#322;"
  ]
  node [
    id 503
    label "morality"
  ]
  node [
    id 504
    label "zbo&#380;no&#347;&#263;"
  ]
  node [
    id 505
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 506
    label "honesty"
  ]
  node [
    id 507
    label "model"
  ]
  node [
    id 508
    label "ordinariness"
  ]
  node [
    id 509
    label "zorganizowa&#263;"
  ]
  node [
    id 510
    label "taniec_towarzyski"
  ]
  node [
    id 511
    label "organizowanie"
  ]
  node [
    id 512
    label "zwyk&#322;o&#347;&#263;"
  ]
  node [
    id 513
    label "zorganizowanie"
  ]
  node [
    id 514
    label "wypowied&#378;"
  ]
  node [
    id 515
    label "exposition"
  ]
  node [
    id 516
    label "czynno&#347;&#263;"
  ]
  node [
    id 517
    label "obja&#347;nienie"
  ]
  node [
    id 518
    label "zawarcie"
  ]
  node [
    id 519
    label "zawrze&#263;"
  ]
  node [
    id 520
    label "czyn"
  ]
  node [
    id 521
    label "warunek"
  ]
  node [
    id 522
    label "gestia_transportowa"
  ]
  node [
    id 523
    label "contract"
  ]
  node [
    id 524
    label "porozumienie"
  ]
  node [
    id 525
    label "klauzula"
  ]
  node [
    id 526
    label "przenikanie"
  ]
  node [
    id 527
    label "materia"
  ]
  node [
    id 528
    label "cz&#261;steczka"
  ]
  node [
    id 529
    label "temperatura_krytyczna"
  ]
  node [
    id 530
    label "przenika&#263;"
  ]
  node [
    id 531
    label "smolisty"
  ]
  node [
    id 532
    label "pot&#281;ga"
  ]
  node [
    id 533
    label "documentation"
  ]
  node [
    id 534
    label "column"
  ]
  node [
    id 535
    label "zasadzenie"
  ]
  node [
    id 536
    label "za&#322;o&#380;enie"
  ]
  node [
    id 537
    label "punkt_odniesienia"
  ]
  node [
    id 538
    label "zasadzi&#263;"
  ]
  node [
    id 539
    label "bok"
  ]
  node [
    id 540
    label "d&#243;&#322;"
  ]
  node [
    id 541
    label "dzieci&#281;ctwo"
  ]
  node [
    id 542
    label "background"
  ]
  node [
    id 543
    label "podstawowy"
  ]
  node [
    id 544
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 545
    label "strategia"
  ]
  node [
    id 546
    label "&#347;ciana"
  ]
  node [
    id 547
    label "narz&#281;dzie"
  ]
  node [
    id 548
    label "nature"
  ]
  node [
    id 549
    label "shoetree"
  ]
  node [
    id 550
    label "twierdzenie_tangens&#243;w"
  ]
  node [
    id 551
    label "alternatywa_Fredholma"
  ]
  node [
    id 552
    label "oznajmianie"
  ]
  node [
    id 553
    label "to&#380;samo&#347;&#263;_Brahmagupty"
  ]
  node [
    id 554
    label "twierdzenie_sinus&#243;w"
  ]
  node [
    id 555
    label "paradoks_Leontiefa"
  ]
  node [
    id 556
    label "twierdzenie_Wedderburna"
  ]
  node [
    id 557
    label "twierdzenie_Kroneckera-Capellego"
  ]
  node [
    id 558
    label "teza"
  ]
  node [
    id 559
    label "Twierdzenie_Hilberta_o_zerach"
  ]
  node [
    id 560
    label "twierdzenie_Ptolemeusza"
  ]
  node [
    id 561
    label "twierdzenie_Pettisa"
  ]
  node [
    id 562
    label "twierdzenie_cosinus&#243;w"
  ]
  node [
    id 563
    label "twierdzenie_Maya"
  ]
  node [
    id 564
    label "centralne_twierdzenie_graniczne"
  ]
  node [
    id 565
    label "twierdzenie_o_bezw&#322;adno&#347;ci_form_kwadratowych"
  ]
  node [
    id 566
    label "twierdzenie_o_przekszta&#322;ceniu_liniowym_zadanym_na_bazie"
  ]
  node [
    id 567
    label "twierdzenie_Arzeli-Ascolego"
  ]
  node [
    id 568
    label "zasada_dobrego_uporz&#261;dkowania"
  ]
  node [
    id 569
    label "zapewnianie"
  ]
  node [
    id 570
    label "podstawowe_twierdzenie_arytmetyki"
  ]
  node [
    id 571
    label "zasada_szufladkowa_Dirichleta"
  ]
  node [
    id 572
    label "twierdzenie_o_rz&#281;dzie"
  ]
  node [
    id 573
    label "twierdzenie_Cayleya&#8211;Hamiltona"
  ]
  node [
    id 574
    label "twierdzenie_Stokesa"
  ]
  node [
    id 575
    label "twierdzenie_o_zbie&#380;no&#347;ci_&#347;rednich"
  ]
  node [
    id 576
    label "twierdzenie_Cevy"
  ]
  node [
    id 577
    label "twierdzenie_Pascala"
  ]
  node [
    id 578
    label "proposition"
  ]
  node [
    id 579
    label "Twierdzenie_Lagrange'a"
  ]
  node [
    id 580
    label "komunikowanie"
  ]
  node [
    id 581
    label "twierdzenie_Mordella-Weila"
  ]
  node [
    id 582
    label "twierdzenie_Abela-Ruffiniego"
  ]
  node [
    id 583
    label "twierdzenie_Pitagorasa"
  ]
  node [
    id 584
    label "twierdzenie_Menelaosa"
  ]
  node [
    id 585
    label "relacja"
  ]
  node [
    id 586
    label "badanie"
  ]
  node [
    id 587
    label "proces_my&#347;lowy"
  ]
  node [
    id 588
    label "remark"
  ]
  node [
    id 589
    label "metoda"
  ]
  node [
    id 590
    label "stwierdzenie"
  ]
  node [
    id 591
    label "observation"
  ]
  node [
    id 592
    label "calibration"
  ]
  node [
    id 593
    label "operacja"
  ]
  node [
    id 594
    label "proces"
  ]
  node [
    id 595
    label "porz&#261;dek"
  ]
  node [
    id 596
    label "dominance"
  ]
  node [
    id 597
    label "zabieg"
  ]
  node [
    id 598
    label "standardization"
  ]
  node [
    id 599
    label "zmiana"
  ]
  node [
    id 600
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 601
    label "umocowa&#263;"
  ]
  node [
    id 602
    label "procesualistyka"
  ]
  node [
    id 603
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 604
    label "kryminalistyka"
  ]
  node [
    id 605
    label "struktura"
  ]
  node [
    id 606
    label "kierunek"
  ]
  node [
    id 607
    label "normatywizm"
  ]
  node [
    id 608
    label "jurisprudence"
  ]
  node [
    id 609
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 610
    label "przepis"
  ]
  node [
    id 611
    label "prawo_karne_procesowe"
  ]
  node [
    id 612
    label "kazuistyka"
  ]
  node [
    id 613
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 614
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 615
    label "kryminologia"
  ]
  node [
    id 616
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 617
    label "prawo_karne"
  ]
  node [
    id 618
    label "legislacyjnie"
  ]
  node [
    id 619
    label "cywilistyka"
  ]
  node [
    id 620
    label "judykatura"
  ]
  node [
    id 621
    label "kanonistyka"
  ]
  node [
    id 622
    label "nauka_prawa"
  ]
  node [
    id 623
    label "podmiot"
  ]
  node [
    id 624
    label "law"
  ]
  node [
    id 625
    label "wykonawczy"
  ]
  node [
    id 626
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 627
    label "spell"
  ]
  node [
    id 628
    label "wyraz"
  ]
  node [
    id 629
    label "zdobi&#263;"
  ]
  node [
    id 630
    label "zostawia&#263;"
  ]
  node [
    id 631
    label "represent"
  ]
  node [
    id 632
    label "odgrywa&#263;_rol&#281;"
  ]
  node [
    id 633
    label "count"
  ]
  node [
    id 634
    label "supply"
  ]
  node [
    id 635
    label "testify"
  ]
  node [
    id 636
    label "op&#322;aca&#263;"
  ]
  node [
    id 637
    label "sk&#322;ada&#263;"
  ]
  node [
    id 638
    label "pracowa&#263;"
  ]
  node [
    id 639
    label "us&#322;uga"
  ]
  node [
    id 640
    label "bespeak"
  ]
  node [
    id 641
    label "opowiada&#263;"
  ]
  node [
    id 642
    label "attest"
  ]
  node [
    id 643
    label "informowa&#263;"
  ]
  node [
    id 644
    label "czyni&#263;_dobro"
  ]
  node [
    id 645
    label "wida&#263;"
  ]
  node [
    id 646
    label "upi&#281;ksza&#263;"
  ]
  node [
    id 647
    label "sprawowa&#263;"
  ]
  node [
    id 648
    label "trim"
  ]
  node [
    id 649
    label "przekazywa&#263;"
  ]
  node [
    id 650
    label "yield"
  ]
  node [
    id 651
    label "opuszcza&#263;"
  ]
  node [
    id 652
    label "g&#243;rowa&#263;"
  ]
  node [
    id 653
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 654
    label "wydawa&#263;"
  ]
  node [
    id 655
    label "tworzy&#263;"
  ]
  node [
    id 656
    label "impart"
  ]
  node [
    id 657
    label "dawa&#263;"
  ]
  node [
    id 658
    label "pomija&#263;"
  ]
  node [
    id 659
    label "doprowadza&#263;"
  ]
  node [
    id 660
    label "zachowywa&#263;"
  ]
  node [
    id 661
    label "zabiera&#263;"
  ]
  node [
    id 662
    label "liszy&#263;"
  ]
  node [
    id 663
    label "bequeath"
  ]
  node [
    id 664
    label "zrywa&#263;"
  ]
  node [
    id 665
    label "porzuca&#263;"
  ]
  node [
    id 666
    label "base_on_balls"
  ]
  node [
    id 667
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 668
    label "permit"
  ]
  node [
    id 669
    label "wyznacza&#263;"
  ]
  node [
    id 670
    label "rezygnowa&#263;"
  ]
  node [
    id 671
    label "krzywdzi&#263;"
  ]
  node [
    id 672
    label "term"
  ]
  node [
    id 673
    label "oznaka"
  ]
  node [
    id 674
    label "cz&#322;on_syntaktyczny"
  ]
  node [
    id 675
    label "leksem"
  ]
  node [
    id 676
    label "posta&#263;"
  ]
  node [
    id 677
    label "element"
  ]
  node [
    id 678
    label "&#347;wiadczenie"
  ]
  node [
    id 679
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 680
    label "zobo"
  ]
  node [
    id 681
    label "yakalo"
  ]
  node [
    id 682
    label "byd&#322;o"
  ]
  node [
    id 683
    label "dzo"
  ]
  node [
    id 684
    label "kr&#281;torogie"
  ]
  node [
    id 685
    label "g&#322;owa"
  ]
  node [
    id 686
    label "czochrad&#322;o"
  ]
  node [
    id 687
    label "posp&#243;lstwo"
  ]
  node [
    id 688
    label "kraal"
  ]
  node [
    id 689
    label "livestock"
  ]
  node [
    id 690
    label "prze&#380;uwacz"
  ]
  node [
    id 691
    label "bizon"
  ]
  node [
    id 692
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 693
    label "zebu"
  ]
  node [
    id 694
    label "byd&#322;o_domowe"
  ]
  node [
    id 695
    label "caution"
  ]
  node [
    id 696
    label "zakomunikowa&#263;"
  ]
  node [
    id 697
    label "spowodowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 201
  ]
  edge [
    source 4
    target 202
  ]
  edge [
    source 4
    target 203
  ]
  edge [
    source 4
    target 204
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
  edge [
    source 6
    target 341
  ]
  edge [
    source 6
    target 342
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 344
  ]
  edge [
    source 6
    target 345
  ]
  edge [
    source 6
    target 346
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 374
  ]
  edge [
    source 8
    target 375
  ]
  edge [
    source 8
    target 376
  ]
  edge [
    source 8
    target 377
  ]
  edge [
    source 8
    target 378
  ]
  edge [
    source 8
    target 379
  ]
  edge [
    source 8
    target 380
  ]
  edge [
    source 8
    target 381
  ]
  edge [
    source 8
    target 382
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 385
  ]
  edge [
    source 8
    target 386
  ]
  edge [
    source 8
    target 387
  ]
  edge [
    source 8
    target 388
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 8
    target 393
  ]
  edge [
    source 8
    target 394
  ]
  edge [
    source 8
    target 395
  ]
  edge [
    source 8
    target 396
  ]
  edge [
    source 8
    target 397
  ]
  edge [
    source 8
    target 398
  ]
  edge [
    source 8
    target 399
  ]
  edge [
    source 8
    target 400
  ]
  edge [
    source 8
    target 401
  ]
  edge [
    source 8
    target 402
  ]
  edge [
    source 8
    target 403
  ]
  edge [
    source 8
    target 404
  ]
  edge [
    source 8
    target 405
  ]
  edge [
    source 8
    target 406
  ]
  edge [
    source 8
    target 407
  ]
  edge [
    source 8
    target 408
  ]
  edge [
    source 8
    target 409
  ]
  edge [
    source 8
    target 410
  ]
  edge [
    source 8
    target 411
  ]
  edge [
    source 8
    target 412
  ]
  edge [
    source 8
    target 413
  ]
  edge [
    source 8
    target 414
  ]
  edge [
    source 8
    target 415
  ]
  edge [
    source 8
    target 416
  ]
  edge [
    source 8
    target 417
  ]
  edge [
    source 8
    target 418
  ]
  edge [
    source 8
    target 419
  ]
  edge [
    source 8
    target 420
  ]
  edge [
    source 8
    target 421
  ]
  edge [
    source 8
    target 422
  ]
  edge [
    source 8
    target 361
  ]
  edge [
    source 8
    target 423
  ]
  edge [
    source 8
    target 424
  ]
  edge [
    source 8
    target 425
  ]
  edge [
    source 8
    target 367
  ]
  edge [
    source 8
    target 426
  ]
  edge [
    source 8
    target 364
  ]
  edge [
    source 8
    target 427
  ]
  edge [
    source 8
    target 428
  ]
  edge [
    source 8
    target 429
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 431
  ]
  edge [
    source 8
    target 432
  ]
  edge [
    source 8
    target 433
  ]
  edge [
    source 8
    target 434
  ]
  edge [
    source 8
    target 435
  ]
  edge [
    source 8
    target 436
  ]
  edge [
    source 8
    target 437
  ]
  edge [
    source 8
    target 438
  ]
  edge [
    source 8
    target 439
  ]
  edge [
    source 8
    target 440
  ]
  edge [
    source 8
    target 441
  ]
  edge [
    source 8
    target 442
  ]
  edge [
    source 8
    target 443
  ]
  edge [
    source 8
    target 444
  ]
  edge [
    source 8
    target 445
  ]
  edge [
    source 8
    target 446
  ]
  edge [
    source 8
    target 447
  ]
  edge [
    source 8
    target 448
  ]
  edge [
    source 8
    target 449
  ]
  edge [
    source 8
    target 450
  ]
  edge [
    source 8
    target 451
  ]
  edge [
    source 8
    target 452
  ]
  edge [
    source 8
    target 453
  ]
  edge [
    source 8
    target 454
  ]
  edge [
    source 8
    target 455
  ]
  edge [
    source 8
    target 456
  ]
  edge [
    source 8
    target 457
  ]
  edge [
    source 8
    target 458
  ]
  edge [
    source 8
    target 459
  ]
  edge [
    source 8
    target 460
  ]
  edge [
    source 8
    target 461
  ]
  edge [
    source 8
    target 462
  ]
  edge [
    source 8
    target 463
  ]
  edge [
    source 8
    target 464
  ]
  edge [
    source 8
    target 465
  ]
  edge [
    source 8
    target 466
  ]
  edge [
    source 8
    target 467
  ]
  edge [
    source 8
    target 468
  ]
  edge [
    source 8
    target 469
  ]
  edge [
    source 8
    target 470
  ]
  edge [
    source 8
    target 471
  ]
  edge [
    source 8
    target 472
  ]
  edge [
    source 8
    target 473
  ]
  edge [
    source 8
    target 474
  ]
  edge [
    source 8
    target 475
  ]
  edge [
    source 8
    target 476
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 477
  ]
  edge [
    source 9
    target 478
  ]
  edge [
    source 9
    target 479
  ]
  edge [
    source 9
    target 480
  ]
  edge [
    source 9
    target 481
  ]
  edge [
    source 9
    target 482
  ]
  edge [
    source 9
    target 483
  ]
  edge [
    source 9
    target 484
  ]
  edge [
    source 9
    target 485
  ]
  edge [
    source 9
    target 486
  ]
  edge [
    source 9
    target 487
  ]
  edge [
    source 9
    target 488
  ]
  edge [
    source 9
    target 489
  ]
  edge [
    source 9
    target 490
  ]
  edge [
    source 9
    target 491
  ]
  edge [
    source 9
    target 492
  ]
  edge [
    source 9
    target 493
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 494
  ]
  edge [
    source 9
    target 367
  ]
  edge [
    source 9
    target 495
  ]
  edge [
    source 9
    target 496
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 9
    target 497
  ]
  edge [
    source 9
    target 498
  ]
  edge [
    source 9
    target 499
  ]
  edge [
    source 9
    target 500
  ]
  edge [
    source 9
    target 501
  ]
  edge [
    source 9
    target 502
  ]
  edge [
    source 9
    target 503
  ]
  edge [
    source 9
    target 504
  ]
  edge [
    source 9
    target 505
  ]
  edge [
    source 9
    target 506
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 507
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 508
  ]
  edge [
    source 9
    target 193
  ]
  edge [
    source 9
    target 509
  ]
  edge [
    source 9
    target 510
  ]
  edge [
    source 9
    target 511
  ]
  edge [
    source 9
    target 512
  ]
  edge [
    source 9
    target 513
  ]
  edge [
    source 9
    target 514
  ]
  edge [
    source 9
    target 515
  ]
  edge [
    source 9
    target 516
  ]
  edge [
    source 9
    target 517
  ]
  edge [
    source 9
    target 518
  ]
  edge [
    source 9
    target 519
  ]
  edge [
    source 9
    target 520
  ]
  edge [
    source 9
    target 521
  ]
  edge [
    source 9
    target 522
  ]
  edge [
    source 9
    target 523
  ]
  edge [
    source 9
    target 524
  ]
  edge [
    source 9
    target 525
  ]
  edge [
    source 9
    target 526
  ]
  edge [
    source 9
    target 291
  ]
  edge [
    source 9
    target 527
  ]
  edge [
    source 9
    target 528
  ]
  edge [
    source 9
    target 529
  ]
  edge [
    source 9
    target 530
  ]
  edge [
    source 9
    target 531
  ]
  edge [
    source 9
    target 532
  ]
  edge [
    source 9
    target 533
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 534
  ]
  edge [
    source 9
    target 535
  ]
  edge [
    source 9
    target 536
  ]
  edge [
    source 9
    target 537
  ]
  edge [
    source 9
    target 538
  ]
  edge [
    source 9
    target 539
  ]
  edge [
    source 9
    target 540
  ]
  edge [
    source 9
    target 541
  ]
  edge [
    source 9
    target 542
  ]
  edge [
    source 9
    target 543
  ]
  edge [
    source 9
    target 544
  ]
  edge [
    source 9
    target 545
  ]
  edge [
    source 9
    target 297
  ]
  edge [
    source 9
    target 546
  ]
  edge [
    source 9
    target 547
  ]
  edge [
    source 9
    target 214
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 548
  ]
  edge [
    source 9
    target 549
  ]
  edge [
    source 9
    target 550
  ]
  edge [
    source 9
    target 551
  ]
  edge [
    source 9
    target 552
  ]
  edge [
    source 9
    target 553
  ]
  edge [
    source 9
    target 321
  ]
  edge [
    source 9
    target 554
  ]
  edge [
    source 9
    target 555
  ]
  edge [
    source 9
    target 206
  ]
  edge [
    source 9
    target 556
  ]
  edge [
    source 9
    target 557
  ]
  edge [
    source 9
    target 558
  ]
  edge [
    source 9
    target 559
  ]
  edge [
    source 9
    target 560
  ]
  edge [
    source 9
    target 561
  ]
  edge [
    source 9
    target 562
  ]
  edge [
    source 9
    target 563
  ]
  edge [
    source 9
    target 564
  ]
  edge [
    source 9
    target 565
  ]
  edge [
    source 9
    target 566
  ]
  edge [
    source 9
    target 567
  ]
  edge [
    source 9
    target 568
  ]
  edge [
    source 9
    target 569
  ]
  edge [
    source 9
    target 570
  ]
  edge [
    source 9
    target 571
  ]
  edge [
    source 9
    target 572
  ]
  edge [
    source 9
    target 573
  ]
  edge [
    source 9
    target 574
  ]
  edge [
    source 9
    target 575
  ]
  edge [
    source 9
    target 576
  ]
  edge [
    source 9
    target 577
  ]
  edge [
    source 9
    target 578
  ]
  edge [
    source 9
    target 579
  ]
  edge [
    source 9
    target 580
  ]
  edge [
    source 9
    target 581
  ]
  edge [
    source 9
    target 582
  ]
  edge [
    source 9
    target 583
  ]
  edge [
    source 9
    target 584
  ]
  edge [
    source 9
    target 585
  ]
  edge [
    source 9
    target 586
  ]
  edge [
    source 9
    target 587
  ]
  edge [
    source 9
    target 588
  ]
  edge [
    source 9
    target 589
  ]
  edge [
    source 9
    target 590
  ]
  edge [
    source 9
    target 591
  ]
  edge [
    source 9
    target 592
  ]
  edge [
    source 9
    target 593
  ]
  edge [
    source 9
    target 594
  ]
  edge [
    source 9
    target 595
  ]
  edge [
    source 9
    target 596
  ]
  edge [
    source 9
    target 597
  ]
  edge [
    source 9
    target 598
  ]
  edge [
    source 9
    target 599
  ]
  edge [
    source 9
    target 600
  ]
  edge [
    source 9
    target 601
  ]
  edge [
    source 9
    target 602
  ]
  edge [
    source 9
    target 603
  ]
  edge [
    source 9
    target 604
  ]
  edge [
    source 9
    target 605
  ]
  edge [
    source 9
    target 352
  ]
  edge [
    source 9
    target 606
  ]
  edge [
    source 9
    target 607
  ]
  edge [
    source 9
    target 608
  ]
  edge [
    source 9
    target 609
  ]
  edge [
    source 9
    target 416
  ]
  edge [
    source 9
    target 610
  ]
  edge [
    source 9
    target 611
  ]
  edge [
    source 9
    target 612
  ]
  edge [
    source 9
    target 613
  ]
  edge [
    source 9
    target 614
  ]
  edge [
    source 9
    target 615
  ]
  edge [
    source 9
    target 616
  ]
  edge [
    source 9
    target 617
  ]
  edge [
    source 9
    target 618
  ]
  edge [
    source 9
    target 619
  ]
  edge [
    source 9
    target 620
  ]
  edge [
    source 9
    target 621
  ]
  edge [
    source 9
    target 622
  ]
  edge [
    source 9
    target 623
  ]
  edge [
    source 9
    target 624
  ]
  edge [
    source 9
    target 625
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 626
  ]
  edge [
    source 10
    target 627
  ]
  edge [
    source 10
    target 628
  ]
  edge [
    source 10
    target 629
  ]
  edge [
    source 10
    target 630
  ]
  edge [
    source 10
    target 631
  ]
  edge [
    source 10
    target 632
  ]
  edge [
    source 10
    target 633
  ]
  edge [
    source 10
    target 54
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 634
  ]
  edge [
    source 10
    target 635
  ]
  edge [
    source 10
    target 636
  ]
  edge [
    source 10
    target 637
  ]
  edge [
    source 10
    target 638
  ]
  edge [
    source 10
    target 639
  ]
  edge [
    source 10
    target 640
  ]
  edge [
    source 10
    target 641
  ]
  edge [
    source 10
    target 642
  ]
  edge [
    source 10
    target 643
  ]
  edge [
    source 10
    target 644
  ]
  edge [
    source 10
    target 645
  ]
  edge [
    source 10
    target 646
  ]
  edge [
    source 10
    target 647
  ]
  edge [
    source 10
    target 648
  ]
  edge [
    source 10
    target 649
  ]
  edge [
    source 10
    target 650
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 10
    target 651
  ]
  edge [
    source 10
    target 652
  ]
  edge [
    source 10
    target 653
  ]
  edge [
    source 10
    target 654
  ]
  edge [
    source 10
    target 655
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 656
  ]
  edge [
    source 10
    target 657
  ]
  edge [
    source 10
    target 658
  ]
  edge [
    source 10
    target 659
  ]
  edge [
    source 10
    target 660
  ]
  edge [
    source 10
    target 661
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 662
  ]
  edge [
    source 10
    target 663
  ]
  edge [
    source 10
    target 664
  ]
  edge [
    source 10
    target 665
  ]
  edge [
    source 10
    target 666
  ]
  edge [
    source 10
    target 667
  ]
  edge [
    source 10
    target 668
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 669
  ]
  edge [
    source 10
    target 670
  ]
  edge [
    source 10
    target 671
  ]
  edge [
    source 10
    target 672
  ]
  edge [
    source 10
    target 673
  ]
  edge [
    source 10
    target 674
  ]
  edge [
    source 10
    target 675
  ]
  edge [
    source 10
    target 676
  ]
  edge [
    source 10
    target 677
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 678
  ]
  edge [
    source 11
    target 679
  ]
  edge [
    source 11
    target 680
  ]
  edge [
    source 11
    target 681
  ]
  edge [
    source 11
    target 682
  ]
  edge [
    source 11
    target 683
  ]
  edge [
    source 11
    target 684
  ]
  edge [
    source 11
    target 214
  ]
  edge [
    source 11
    target 685
  ]
  edge [
    source 11
    target 686
  ]
  edge [
    source 11
    target 687
  ]
  edge [
    source 11
    target 688
  ]
  edge [
    source 11
    target 689
  ]
  edge [
    source 11
    target 690
  ]
  edge [
    source 11
    target 691
  ]
  edge [
    source 11
    target 692
  ]
  edge [
    source 11
    target 693
  ]
  edge [
    source 11
    target 694
  ]
  edge [
    source 12
    target 695
  ]
  edge [
    source 12
    target 696
  ]
  edge [
    source 12
    target 697
  ]
]
