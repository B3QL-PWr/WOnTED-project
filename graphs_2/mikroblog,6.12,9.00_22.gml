graph [
  node [
    id 0
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 1
    label "nasz"
    origin "text"
  ]
  node [
    id 2
    label "c&#243;rka"
    origin "text"
  ]
  node [
    id 3
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 4
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 5
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 6
    label "doba"
  ]
  node [
    id 7
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 8
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 9
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 10
    label "dzi&#347;"
  ]
  node [
    id 11
    label "teraz"
  ]
  node [
    id 12
    label "czas"
  ]
  node [
    id 13
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 14
    label "jednocze&#347;nie"
  ]
  node [
    id 15
    label "noc"
  ]
  node [
    id 16
    label "dzie&#324;"
  ]
  node [
    id 17
    label "godzina"
  ]
  node [
    id 18
    label "long_time"
  ]
  node [
    id 19
    label "jednostka_geologiczna"
  ]
  node [
    id 20
    label "czyj&#347;"
  ]
  node [
    id 21
    label "prywatny"
  ]
  node [
    id 22
    label "dziewka"
  ]
  node [
    id 23
    label "sikorka"
  ]
  node [
    id 24
    label "kora"
  ]
  node [
    id 25
    label "cz&#322;owiek"
  ]
  node [
    id 26
    label "dziewcz&#281;"
  ]
  node [
    id 27
    label "dziewoja"
  ]
  node [
    id 28
    label "m&#322;&#243;dka"
  ]
  node [
    id 29
    label "dziecina"
  ]
  node [
    id 30
    label "dziecko"
  ]
  node [
    id 31
    label "dziunia"
  ]
  node [
    id 32
    label "dziewczynina"
  ]
  node [
    id 33
    label "siksa"
  ]
  node [
    id 34
    label "potomkini"
  ]
  node [
    id 35
    label "utulenie"
  ]
  node [
    id 36
    label "pediatra"
  ]
  node [
    id 37
    label "dzieciak"
  ]
  node [
    id 38
    label "utulanie"
  ]
  node [
    id 39
    label "dzieciarnia"
  ]
  node [
    id 40
    label "niepe&#322;noletni"
  ]
  node [
    id 41
    label "organizm"
  ]
  node [
    id 42
    label "utula&#263;"
  ]
  node [
    id 43
    label "cz&#322;owieczek"
  ]
  node [
    id 44
    label "fledgling"
  ]
  node [
    id 45
    label "zwierz&#281;"
  ]
  node [
    id 46
    label "utuli&#263;"
  ]
  node [
    id 47
    label "m&#322;odzik"
  ]
  node [
    id 48
    label "pedofil"
  ]
  node [
    id 49
    label "m&#322;odziak"
  ]
  node [
    id 50
    label "potomek"
  ]
  node [
    id 51
    label "entliczek-pentliczek"
  ]
  node [
    id 52
    label "potomstwo"
  ]
  node [
    id 53
    label "sraluch"
  ]
  node [
    id 54
    label "krewna"
  ]
  node [
    id 55
    label "ludzko&#347;&#263;"
  ]
  node [
    id 56
    label "asymilowanie"
  ]
  node [
    id 57
    label "wapniak"
  ]
  node [
    id 58
    label "asymilowa&#263;"
  ]
  node [
    id 59
    label "os&#322;abia&#263;"
  ]
  node [
    id 60
    label "posta&#263;"
  ]
  node [
    id 61
    label "hominid"
  ]
  node [
    id 62
    label "podw&#322;adny"
  ]
  node [
    id 63
    label "os&#322;abianie"
  ]
  node [
    id 64
    label "g&#322;owa"
  ]
  node [
    id 65
    label "figura"
  ]
  node [
    id 66
    label "portrecista"
  ]
  node [
    id 67
    label "dwun&#243;g"
  ]
  node [
    id 68
    label "profanum"
  ]
  node [
    id 69
    label "mikrokosmos"
  ]
  node [
    id 70
    label "nasada"
  ]
  node [
    id 71
    label "duch"
  ]
  node [
    id 72
    label "antropochoria"
  ]
  node [
    id 73
    label "osoba"
  ]
  node [
    id 74
    label "wz&#243;r"
  ]
  node [
    id 75
    label "senior"
  ]
  node [
    id 76
    label "oddzia&#322;ywanie"
  ]
  node [
    id 77
    label "Adam"
  ]
  node [
    id 78
    label "homo_sapiens"
  ]
  node [
    id 79
    label "polifag"
  ]
  node [
    id 80
    label "laska"
  ]
  node [
    id 81
    label "dziewczyna"
  ]
  node [
    id 82
    label "sikora"
  ]
  node [
    id 83
    label "panna"
  ]
  node [
    id 84
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 85
    label "prostytutka"
  ]
  node [
    id 86
    label "ma&#322;olata"
  ]
  node [
    id 87
    label "crust"
  ]
  node [
    id 88
    label "ciasto"
  ]
  node [
    id 89
    label "szabla"
  ]
  node [
    id 90
    label "drzewko"
  ]
  node [
    id 91
    label "drzewo"
  ]
  node [
    id 92
    label "po&#347;ciel&#243;wka"
  ]
  node [
    id 93
    label "harfa"
  ]
  node [
    id 94
    label "bawe&#322;na"
  ]
  node [
    id 95
    label "tkanka_sta&#322;a"
  ]
  node [
    id 96
    label "piskl&#281;"
  ]
  node [
    id 97
    label "samica"
  ]
  node [
    id 98
    label "ptak"
  ]
  node [
    id 99
    label "upierzenie"
  ]
  node [
    id 100
    label "kobieta"
  ]
  node [
    id 101
    label "m&#322;odzie&#380;"
  ]
  node [
    id 102
    label "mo&#322;odyca"
  ]
  node [
    id 103
    label "zwrot"
  ]
  node [
    id 104
    label "satisfy"
  ]
  node [
    id 105
    label "robi&#263;"
  ]
  node [
    id 106
    label "close"
  ]
  node [
    id 107
    label "determine"
  ]
  node [
    id 108
    label "przestawa&#263;"
  ]
  node [
    id 109
    label "zako&#324;cza&#263;"
  ]
  node [
    id 110
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 111
    label "stanowi&#263;"
  ]
  node [
    id 112
    label "organizowa&#263;"
  ]
  node [
    id 113
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 114
    label "czyni&#263;"
  ]
  node [
    id 115
    label "give"
  ]
  node [
    id 116
    label "stylizowa&#263;"
  ]
  node [
    id 117
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 118
    label "falowa&#263;"
  ]
  node [
    id 119
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 120
    label "peddle"
  ]
  node [
    id 121
    label "praca"
  ]
  node [
    id 122
    label "wydala&#263;"
  ]
  node [
    id 123
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 124
    label "tentegowa&#263;"
  ]
  node [
    id 125
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 126
    label "urz&#261;dza&#263;"
  ]
  node [
    id 127
    label "oszukiwa&#263;"
  ]
  node [
    id 128
    label "work"
  ]
  node [
    id 129
    label "ukazywa&#263;"
  ]
  node [
    id 130
    label "przerabia&#263;"
  ]
  node [
    id 131
    label "act"
  ]
  node [
    id 132
    label "post&#281;powa&#263;"
  ]
  node [
    id 133
    label "by&#263;"
  ]
  node [
    id 134
    label "decide"
  ]
  node [
    id 135
    label "pies_my&#347;liwski"
  ]
  node [
    id 136
    label "decydowa&#263;"
  ]
  node [
    id 137
    label "represent"
  ]
  node [
    id 138
    label "zatrzymywa&#263;"
  ]
  node [
    id 139
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 140
    label "typify"
  ]
  node [
    id 141
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 142
    label "dopracowywa&#263;"
  ]
  node [
    id 143
    label "elaborate"
  ]
  node [
    id 144
    label "finish_up"
  ]
  node [
    id 145
    label "rezygnowa&#263;"
  ]
  node [
    id 146
    label "nadawa&#263;"
  ]
  node [
    id 147
    label "&#380;y&#263;"
  ]
  node [
    id 148
    label "coating"
  ]
  node [
    id 149
    label "przebywa&#263;"
  ]
  node [
    id 150
    label "zadowala&#263;_si&#281;"
  ]
  node [
    id 151
    label "ko&#324;czy&#263;_si&#281;"
  ]
  node [
    id 152
    label "weekend"
  ]
  node [
    id 153
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 154
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 155
    label "miesi&#261;c"
  ]
  node [
    id 156
    label "poprzedzanie"
  ]
  node [
    id 157
    label "czasoprzestrze&#324;"
  ]
  node [
    id 158
    label "laba"
  ]
  node [
    id 159
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 160
    label "chronometria"
  ]
  node [
    id 161
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 162
    label "rachuba_czasu"
  ]
  node [
    id 163
    label "przep&#322;ywanie"
  ]
  node [
    id 164
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 165
    label "czasokres"
  ]
  node [
    id 166
    label "odczyt"
  ]
  node [
    id 167
    label "chwila"
  ]
  node [
    id 168
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 169
    label "dzieje"
  ]
  node [
    id 170
    label "kategoria_gramatyczna"
  ]
  node [
    id 171
    label "poprzedzenie"
  ]
  node [
    id 172
    label "trawienie"
  ]
  node [
    id 173
    label "pochodzi&#263;"
  ]
  node [
    id 174
    label "period"
  ]
  node [
    id 175
    label "okres_czasu"
  ]
  node [
    id 176
    label "poprzedza&#263;"
  ]
  node [
    id 177
    label "schy&#322;ek"
  ]
  node [
    id 178
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 179
    label "odwlekanie_si&#281;"
  ]
  node [
    id 180
    label "zegar"
  ]
  node [
    id 181
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 182
    label "czwarty_wymiar"
  ]
  node [
    id 183
    label "pochodzenie"
  ]
  node [
    id 184
    label "koniugacja"
  ]
  node [
    id 185
    label "Zeitgeist"
  ]
  node [
    id 186
    label "trawi&#263;"
  ]
  node [
    id 187
    label "pogoda"
  ]
  node [
    id 188
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 189
    label "poprzedzi&#263;"
  ]
  node [
    id 190
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 191
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 192
    label "time_period"
  ]
  node [
    id 193
    label "niedziela"
  ]
  node [
    id 194
    label "sobota"
  ]
  node [
    id 195
    label "miech"
  ]
  node [
    id 196
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 197
    label "rok"
  ]
  node [
    id 198
    label "kalendy"
  ]
  node [
    id 199
    label "raj_utracony"
  ]
  node [
    id 200
    label "umieranie"
  ]
  node [
    id 201
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 202
    label "prze&#380;ywanie"
  ]
  node [
    id 203
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 204
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 205
    label "po&#322;&#243;g"
  ]
  node [
    id 206
    label "umarcie"
  ]
  node [
    id 207
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 208
    label "subsistence"
  ]
  node [
    id 209
    label "power"
  ]
  node [
    id 210
    label "okres_noworodkowy"
  ]
  node [
    id 211
    label "prze&#380;ycie"
  ]
  node [
    id 212
    label "wiek_matuzalemowy"
  ]
  node [
    id 213
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 214
    label "entity"
  ]
  node [
    id 215
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 216
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 217
    label "do&#380;ywanie"
  ]
  node [
    id 218
    label "byt"
  ]
  node [
    id 219
    label "dzieci&#324;stwo"
  ]
  node [
    id 220
    label "andropauza"
  ]
  node [
    id 221
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 222
    label "rozw&#243;j"
  ]
  node [
    id 223
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 224
    label "menopauza"
  ]
  node [
    id 225
    label "&#347;mier&#263;"
  ]
  node [
    id 226
    label "koleje_losu"
  ]
  node [
    id 227
    label "bycie"
  ]
  node [
    id 228
    label "zegar_biologiczny"
  ]
  node [
    id 229
    label "szwung"
  ]
  node [
    id 230
    label "przebywanie"
  ]
  node [
    id 231
    label "warunki"
  ]
  node [
    id 232
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 233
    label "niemowl&#281;ctwo"
  ]
  node [
    id 234
    label "&#380;ywy"
  ]
  node [
    id 235
    label "life"
  ]
  node [
    id 236
    label "staro&#347;&#263;"
  ]
  node [
    id 237
    label "energy"
  ]
  node [
    id 238
    label "trwanie"
  ]
  node [
    id 239
    label "wra&#380;enie"
  ]
  node [
    id 240
    label "przej&#347;cie"
  ]
  node [
    id 241
    label "doznanie"
  ]
  node [
    id 242
    label "poradzenie_sobie"
  ]
  node [
    id 243
    label "przetrwanie"
  ]
  node [
    id 244
    label "survival"
  ]
  node [
    id 245
    label "przechodzenie"
  ]
  node [
    id 246
    label "wytrzymywanie"
  ]
  node [
    id 247
    label "zaznawanie"
  ]
  node [
    id 248
    label "obejrzenie"
  ]
  node [
    id 249
    label "widzenie"
  ]
  node [
    id 250
    label "urzeczywistnianie"
  ]
  node [
    id 251
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 252
    label "przeszkodzenie"
  ]
  node [
    id 253
    label "produkowanie"
  ]
  node [
    id 254
    label "being"
  ]
  node [
    id 255
    label "znikni&#281;cie"
  ]
  node [
    id 256
    label "robienie"
  ]
  node [
    id 257
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 258
    label "przeszkadzanie"
  ]
  node [
    id 259
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 260
    label "wyprodukowanie"
  ]
  node [
    id 261
    label "utrzymywanie"
  ]
  node [
    id 262
    label "subsystencja"
  ]
  node [
    id 263
    label "utrzyma&#263;"
  ]
  node [
    id 264
    label "egzystencja"
  ]
  node [
    id 265
    label "wy&#380;ywienie"
  ]
  node [
    id 266
    label "ontologicznie"
  ]
  node [
    id 267
    label "utrzymanie"
  ]
  node [
    id 268
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 269
    label "potencja"
  ]
  node [
    id 270
    label "utrzymywa&#263;"
  ]
  node [
    id 271
    label "status"
  ]
  node [
    id 272
    label "sytuacja"
  ]
  node [
    id 273
    label "ocieranie_si&#281;"
  ]
  node [
    id 274
    label "otoczenie_si&#281;"
  ]
  node [
    id 275
    label "posiedzenie"
  ]
  node [
    id 276
    label "otarcie_si&#281;"
  ]
  node [
    id 277
    label "atakowanie"
  ]
  node [
    id 278
    label "otaczanie_si&#281;"
  ]
  node [
    id 279
    label "wyj&#347;cie"
  ]
  node [
    id 280
    label "zmierzanie"
  ]
  node [
    id 281
    label "residency"
  ]
  node [
    id 282
    label "sojourn"
  ]
  node [
    id 283
    label "wychodzenie"
  ]
  node [
    id 284
    label "tkwienie"
  ]
  node [
    id 285
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 286
    label "absolutorium"
  ]
  node [
    id 287
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 288
    label "dzia&#322;anie"
  ]
  node [
    id 289
    label "activity"
  ]
  node [
    id 290
    label "ton"
  ]
  node [
    id 291
    label "&#380;ywotno&#347;&#263;"
  ]
  node [
    id 292
    label "cecha"
  ]
  node [
    id 293
    label "korkowanie"
  ]
  node [
    id 294
    label "death"
  ]
  node [
    id 295
    label "zabijanie"
  ]
  node [
    id 296
    label "martwy"
  ]
  node [
    id 297
    label "przestawanie"
  ]
  node [
    id 298
    label "odumieranie"
  ]
  node [
    id 299
    label "zdychanie"
  ]
  node [
    id 300
    label "stan"
  ]
  node [
    id 301
    label "&#380;egnanie_si&#281;_ze_&#347;wiatem"
  ]
  node [
    id 302
    label "zanikanie"
  ]
  node [
    id 303
    label "ko&#324;czenie"
  ]
  node [
    id 304
    label "nieuleczalnie_chory"
  ]
  node [
    id 305
    label "ciekawy"
  ]
  node [
    id 306
    label "szybki"
  ]
  node [
    id 307
    label "&#380;ywotny"
  ]
  node [
    id 308
    label "naturalny"
  ]
  node [
    id 309
    label "&#380;ywo"
  ]
  node [
    id 310
    label "o&#380;ywianie"
  ]
  node [
    id 311
    label "silny"
  ]
  node [
    id 312
    label "g&#322;&#281;boki"
  ]
  node [
    id 313
    label "wyra&#378;ny"
  ]
  node [
    id 314
    label "czynny"
  ]
  node [
    id 315
    label "aktualny"
  ]
  node [
    id 316
    label "zgrabny"
  ]
  node [
    id 317
    label "prawdziwy"
  ]
  node [
    id 318
    label "realistyczny"
  ]
  node [
    id 319
    label "energiczny"
  ]
  node [
    id 320
    label "odumarcie"
  ]
  node [
    id 321
    label "przestanie"
  ]
  node [
    id 322
    label "dysponowanie_si&#281;"
  ]
  node [
    id 323
    label "pomarcie"
  ]
  node [
    id 324
    label "die"
  ]
  node [
    id 325
    label "sko&#324;czenie"
  ]
  node [
    id 326
    label "przekr&#281;cenie_si&#281;"
  ]
  node [
    id 327
    label "zdechni&#281;cie"
  ]
  node [
    id 328
    label "zabicie"
  ]
  node [
    id 329
    label "procedura"
  ]
  node [
    id 330
    label "proces"
  ]
  node [
    id 331
    label "proces_biologiczny"
  ]
  node [
    id 332
    label "z&#322;ote_czasy"
  ]
  node [
    id 333
    label "bezszkodowo&#347;&#263;"
  ]
  node [
    id 334
    label "process"
  ]
  node [
    id 335
    label "cycle"
  ]
  node [
    id 336
    label "m&#322;odzie&#324;czo&#347;&#263;"
  ]
  node [
    id 337
    label "adolescence"
  ]
  node [
    id 338
    label "wiek"
  ]
  node [
    id 339
    label "szczeni&#281;ce_lata"
  ]
  node [
    id 340
    label "zielone_lata"
  ]
  node [
    id 341
    label "rozwi&#261;zanie"
  ]
  node [
    id 342
    label "zlec"
  ]
  node [
    id 343
    label "zlegni&#281;cie"
  ]
  node [
    id 344
    label "defenestracja"
  ]
  node [
    id 345
    label "agonia"
  ]
  node [
    id 346
    label "kres"
  ]
  node [
    id 347
    label "mogi&#322;a"
  ]
  node [
    id 348
    label "kres_&#380;ycia"
  ]
  node [
    id 349
    label "upadek"
  ]
  node [
    id 350
    label "szeol"
  ]
  node [
    id 351
    label "pogrzebanie"
  ]
  node [
    id 352
    label "istota_nadprzyrodzona"
  ]
  node [
    id 353
    label "&#380;a&#322;oba"
  ]
  node [
    id 354
    label "pogrzeb"
  ]
  node [
    id 355
    label "majority"
  ]
  node [
    id 356
    label "osiemnastoletni"
  ]
  node [
    id 357
    label "zjawisko_fizjologiczne"
  ]
  node [
    id 358
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 359
    label "age"
  ]
  node [
    id 360
    label "przekwitanie"
  ]
  node [
    id 361
    label "wczesnodzieci&#281;cy"
  ]
  node [
    id 362
    label "dzieci&#281;ctwo"
  ]
  node [
    id 363
    label "energia"
  ]
  node [
    id 364
    label "zapa&#322;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 348
  ]
  edge [
    source 5
    target 349
  ]
  edge [
    source 5
    target 350
  ]
  edge [
    source 5
    target 351
  ]
  edge [
    source 5
    target 352
  ]
  edge [
    source 5
    target 353
  ]
  edge [
    source 5
    target 354
  ]
  edge [
    source 5
    target 355
  ]
  edge [
    source 5
    target 356
  ]
  edge [
    source 5
    target 357
  ]
  edge [
    source 5
    target 358
  ]
  edge [
    source 5
    target 359
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 360
  ]
  edge [
    source 5
    target 361
  ]
  edge [
    source 5
    target 362
  ]
  edge [
    source 5
    target 363
  ]
  edge [
    source 5
    target 364
  ]
]
