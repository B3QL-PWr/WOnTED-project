graph [
  node [
    id 0
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wernisa&#380;"
    origin "text"
  ]
  node [
    id 2
    label "wystawa"
    origin "text"
  ]
  node [
    id 3
    label "malarstwo"
    origin "text"
  ]
  node [
    id 4
    label "ozorkowianki"
    origin "text"
  ]
  node [
    id 5
    label "beata"
    origin "text"
  ]
  node [
    id 6
    label "koszyk"
    origin "text"
  ]
  node [
    id 7
    label "ta&#324;cz&#261;ca"
    origin "text"
  ]
  node [
    id 8
    label "anio&#322;"
    origin "text"
  ]
  node [
    id 9
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 10
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 11
    label "koniec"
    origin "text"
  ]
  node [
    id 12
    label "grudzie&#324;"
    origin "text"
  ]
  node [
    id 13
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 14
    label "invite"
  ]
  node [
    id 15
    label "ask"
  ]
  node [
    id 16
    label "oferowa&#263;"
  ]
  node [
    id 17
    label "zach&#281;ca&#263;"
  ]
  node [
    id 18
    label "volunteer"
  ]
  node [
    id 19
    label "impreza"
  ]
  node [
    id 20
    label "impra"
  ]
  node [
    id 21
    label "rozrywka"
  ]
  node [
    id 22
    label "przyj&#281;cie"
  ]
  node [
    id 23
    label "okazja"
  ]
  node [
    id 24
    label "party"
  ]
  node [
    id 25
    label "ekspozycja"
  ]
  node [
    id 26
    label "szyba"
  ]
  node [
    id 27
    label "okno"
  ]
  node [
    id 28
    label "kolekcja"
  ]
  node [
    id 29
    label "kustosz"
  ]
  node [
    id 30
    label "miejsce"
  ]
  node [
    id 31
    label "kurator"
  ]
  node [
    id 32
    label "galeria"
  ]
  node [
    id 33
    label "muzeum"
  ]
  node [
    id 34
    label "sklep"
  ]
  node [
    id 35
    label "Agropromocja"
  ]
  node [
    id 36
    label "Arsena&#322;"
  ]
  node [
    id 37
    label "parapet"
  ]
  node [
    id 38
    label "okiennica"
  ]
  node [
    id 39
    label "interfejs"
  ]
  node [
    id 40
    label "prze&#347;wit"
  ]
  node [
    id 41
    label "pulpit"
  ]
  node [
    id 42
    label "transenna"
  ]
  node [
    id 43
    label "kwatera_okienna"
  ]
  node [
    id 44
    label "inspekt"
  ]
  node [
    id 45
    label "nora"
  ]
  node [
    id 46
    label "skrzyd&#322;o"
  ]
  node [
    id 47
    label "nadokiennik"
  ]
  node [
    id 48
    label "futryna"
  ]
  node [
    id 49
    label "lufcik"
  ]
  node [
    id 50
    label "program"
  ]
  node [
    id 51
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 52
    label "casement"
  ]
  node [
    id 53
    label "menad&#380;er_okien"
  ]
  node [
    id 54
    label "otw&#243;r"
  ]
  node [
    id 55
    label "warunek_lokalowy"
  ]
  node [
    id 56
    label "plac"
  ]
  node [
    id 57
    label "location"
  ]
  node [
    id 58
    label "uwaga"
  ]
  node [
    id 59
    label "przestrze&#324;"
  ]
  node [
    id 60
    label "status"
  ]
  node [
    id 61
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 62
    label "chwila"
  ]
  node [
    id 63
    label "cia&#322;o"
  ]
  node [
    id 64
    label "cecha"
  ]
  node [
    id 65
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 66
    label "praca"
  ]
  node [
    id 67
    label "rz&#261;d"
  ]
  node [
    id 68
    label "linia"
  ]
  node [
    id 69
    label "zbi&#243;r"
  ]
  node [
    id 70
    label "stage_set"
  ]
  node [
    id 71
    label "collection"
  ]
  node [
    id 72
    label "album"
  ]
  node [
    id 73
    label "glass"
  ]
  node [
    id 74
    label "antyrama"
  ]
  node [
    id 75
    label "witryna"
  ]
  node [
    id 76
    label "p&#243;&#322;ka"
  ]
  node [
    id 77
    label "firma"
  ]
  node [
    id 78
    label "stoisko"
  ]
  node [
    id 79
    label "lokal_u&#380;ytkowy"
  ]
  node [
    id 80
    label "sk&#322;ad"
  ]
  node [
    id 81
    label "obiekt_handlowy"
  ]
  node [
    id 82
    label "zaplecze"
  ]
  node [
    id 83
    label "instytucja"
  ]
  node [
    id 84
    label "kuratorstwo"
  ]
  node [
    id 85
    label "po&#322;o&#380;enie"
  ]
  node [
    id 86
    label "scena"
  ]
  node [
    id 87
    label "parametr"
  ]
  node [
    id 88
    label "wst&#281;p"
  ]
  node [
    id 89
    label "operacja"
  ]
  node [
    id 90
    label "akcja"
  ]
  node [
    id 91
    label "wystawienie"
  ]
  node [
    id 92
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 93
    label "strona_&#347;wiata"
  ]
  node [
    id 94
    label "wspinaczka"
  ]
  node [
    id 95
    label "spot"
  ]
  node [
    id 96
    label "fotografia"
  ]
  node [
    id 97
    label "czynnik"
  ]
  node [
    id 98
    label "wprowadzenie"
  ]
  node [
    id 99
    label "urz&#281;dnik"
  ]
  node [
    id 100
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 101
    label "cz&#322;owiek"
  ]
  node [
    id 102
    label "opiekun"
  ]
  node [
    id 103
    label "kuratorium"
  ]
  node [
    id 104
    label "pe&#322;nomocnik"
  ]
  node [
    id 105
    label "muzealnik"
  ]
  node [
    id 106
    label "funkcjonariusz"
  ]
  node [
    id 107
    label "wyznawca"
  ]
  node [
    id 108
    label "przedstawiciel"
  ]
  node [
    id 109
    label "nadzorca"
  ]
  node [
    id 110
    label "popularyzator"
  ]
  node [
    id 111
    label "balkon"
  ]
  node [
    id 112
    label "eskalator"
  ]
  node [
    id 113
    label "&#322;&#261;cznik"
  ]
  node [
    id 114
    label "sala"
  ]
  node [
    id 115
    label "publiczno&#347;&#263;"
  ]
  node [
    id 116
    label "Galeria_Arsena&#322;"
  ]
  node [
    id 117
    label "centrum_handlowe"
  ]
  node [
    id 118
    label "ci&#261;g_komunikacyjny"
  ]
  node [
    id 119
    label "zakonnik"
  ]
  node [
    id 120
    label "zwierzchnik"
  ]
  node [
    id 121
    label "syntetyzm"
  ]
  node [
    id 122
    label "kompozycja"
  ]
  node [
    id 123
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 124
    label "skr&#243;t_perspektywiczny"
  ]
  node [
    id 125
    label "linearyzm"
  ]
  node [
    id 126
    label "rezultat"
  ]
  node [
    id 127
    label "tempera"
  ]
  node [
    id 128
    label "gwasz"
  ]
  node [
    id 129
    label "plastyka"
  ]
  node [
    id 130
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 131
    label "art"
  ]
  node [
    id 132
    label "plastic_surgery"
  ]
  node [
    id 133
    label "przedmiot"
  ]
  node [
    id 134
    label "kszta&#322;towanie"
  ]
  node [
    id 135
    label "wyra&#378;no&#347;&#263;"
  ]
  node [
    id 136
    label "sugestywno&#347;&#263;"
  ]
  node [
    id 137
    label "color"
  ]
  node [
    id 138
    label "chirurgia"
  ]
  node [
    id 139
    label "przestrzenno&#347;&#263;"
  ]
  node [
    id 140
    label "sztuka"
  ]
  node [
    id 141
    label "dzia&#322;anie"
  ]
  node [
    id 142
    label "typ"
  ]
  node [
    id 143
    label "event"
  ]
  node [
    id 144
    label "przyczyna"
  ]
  node [
    id 145
    label "blend"
  ]
  node [
    id 146
    label "struktura"
  ]
  node [
    id 147
    label "prawo_karne"
  ]
  node [
    id 148
    label "leksem"
  ]
  node [
    id 149
    label "dzie&#322;o"
  ]
  node [
    id 150
    label "figuracja"
  ]
  node [
    id 151
    label "chwyt"
  ]
  node [
    id 152
    label "okup"
  ]
  node [
    id 153
    label "muzykologia"
  ]
  node [
    id 154
    label "&#347;redniowiecze"
  ]
  node [
    id 155
    label "szko&#322;a"
  ]
  node [
    id 156
    label "farba"
  ]
  node [
    id 157
    label "obraz"
  ]
  node [
    id 158
    label "gouache"
  ]
  node [
    id 159
    label "distemper"
  ]
  node [
    id 160
    label "temperature"
  ]
  node [
    id 161
    label "basket"
  ]
  node [
    id 162
    label "pojemnik"
  ]
  node [
    id 163
    label "wiklina"
  ]
  node [
    id 164
    label "zestaw"
  ]
  node [
    id 165
    label "surowiec"
  ]
  node [
    id 166
    label "willow"
  ]
  node [
    id 167
    label "krzew"
  ]
  node [
    id 168
    label "wierzba"
  ]
  node [
    id 169
    label "wierzbina"
  ]
  node [
    id 170
    label "wicker"
  ]
  node [
    id 171
    label "hygrofit"
  ]
  node [
    id 172
    label "towar"
  ]
  node [
    id 173
    label "kraw&#281;d&#378;"
  ]
  node [
    id 174
    label "elektrolizer"
  ]
  node [
    id 175
    label "zawarto&#347;&#263;"
  ]
  node [
    id 176
    label "zbiornikowiec"
  ]
  node [
    id 177
    label "opakowanie"
  ]
  node [
    id 178
    label "instalowa&#263;"
  ]
  node [
    id 179
    label "oprogramowanie"
  ]
  node [
    id 180
    label "odinstalowywa&#263;"
  ]
  node [
    id 181
    label "spis"
  ]
  node [
    id 182
    label "zaprezentowanie"
  ]
  node [
    id 183
    label "podprogram"
  ]
  node [
    id 184
    label "ogranicznik_referencyjny"
  ]
  node [
    id 185
    label "course_of_study"
  ]
  node [
    id 186
    label "booklet"
  ]
  node [
    id 187
    label "dzia&#322;"
  ]
  node [
    id 188
    label "odinstalowanie"
  ]
  node [
    id 189
    label "broszura"
  ]
  node [
    id 190
    label "wytw&#243;r"
  ]
  node [
    id 191
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 192
    label "kana&#322;"
  ]
  node [
    id 193
    label "teleferie"
  ]
  node [
    id 194
    label "zainstalowanie"
  ]
  node [
    id 195
    label "struktura_organizacyjna"
  ]
  node [
    id 196
    label "pirat"
  ]
  node [
    id 197
    label "zaprezentowa&#263;"
  ]
  node [
    id 198
    label "prezentowanie"
  ]
  node [
    id 199
    label "prezentowa&#263;"
  ]
  node [
    id 200
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 201
    label "blok"
  ]
  node [
    id 202
    label "punkt"
  ]
  node [
    id 203
    label "folder"
  ]
  node [
    id 204
    label "zainstalowa&#263;"
  ]
  node [
    id 205
    label "za&#322;o&#380;enie"
  ]
  node [
    id 206
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 207
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 208
    label "ram&#243;wka"
  ]
  node [
    id 209
    label "tryb"
  ]
  node [
    id 210
    label "emitowa&#263;"
  ]
  node [
    id 211
    label "emitowanie"
  ]
  node [
    id 212
    label "odinstalowywanie"
  ]
  node [
    id 213
    label "instrukcja"
  ]
  node [
    id 214
    label "informatyka"
  ]
  node [
    id 215
    label "deklaracja"
  ]
  node [
    id 216
    label "menu"
  ]
  node [
    id 217
    label "sekcja_krytyczna"
  ]
  node [
    id 218
    label "furkacja"
  ]
  node [
    id 219
    label "podstawa"
  ]
  node [
    id 220
    label "instalowanie"
  ]
  node [
    id 221
    label "oferta"
  ]
  node [
    id 222
    label "odinstalowa&#263;"
  ]
  node [
    id 223
    label "sk&#322;ada&#263;"
  ]
  node [
    id 224
    label "sygna&#322;"
  ]
  node [
    id 225
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 226
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 227
    label "anielski"
  ]
  node [
    id 228
    label "duch"
  ]
  node [
    id 229
    label "anielstwo"
  ]
  node [
    id 230
    label "niebianin"
  ]
  node [
    id 231
    label "ludzko&#347;&#263;"
  ]
  node [
    id 232
    label "asymilowanie"
  ]
  node [
    id 233
    label "wapniak"
  ]
  node [
    id 234
    label "asymilowa&#263;"
  ]
  node [
    id 235
    label "os&#322;abia&#263;"
  ]
  node [
    id 236
    label "posta&#263;"
  ]
  node [
    id 237
    label "hominid"
  ]
  node [
    id 238
    label "podw&#322;adny"
  ]
  node [
    id 239
    label "os&#322;abianie"
  ]
  node [
    id 240
    label "g&#322;owa"
  ]
  node [
    id 241
    label "figura"
  ]
  node [
    id 242
    label "portrecista"
  ]
  node [
    id 243
    label "dwun&#243;g"
  ]
  node [
    id 244
    label "profanum"
  ]
  node [
    id 245
    label "mikrokosmos"
  ]
  node [
    id 246
    label "nasada"
  ]
  node [
    id 247
    label "antropochoria"
  ]
  node [
    id 248
    label "osoba"
  ]
  node [
    id 249
    label "wz&#243;r"
  ]
  node [
    id 250
    label "senior"
  ]
  node [
    id 251
    label "oddzia&#322;ywanie"
  ]
  node [
    id 252
    label "Adam"
  ]
  node [
    id 253
    label "homo_sapiens"
  ]
  node [
    id 254
    label "polifag"
  ]
  node [
    id 255
    label "piek&#322;o"
  ]
  node [
    id 256
    label "human_body"
  ]
  node [
    id 257
    label "ofiarowywanie"
  ]
  node [
    id 258
    label "sfera_afektywna"
  ]
  node [
    id 259
    label "nekromancja"
  ]
  node [
    id 260
    label "Po&#347;wist"
  ]
  node [
    id 261
    label "podekscytowanie"
  ]
  node [
    id 262
    label "deformowanie"
  ]
  node [
    id 263
    label "sumienie"
  ]
  node [
    id 264
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 265
    label "deformowa&#263;"
  ]
  node [
    id 266
    label "osobowo&#347;&#263;"
  ]
  node [
    id 267
    label "psychika"
  ]
  node [
    id 268
    label "zjawa"
  ]
  node [
    id 269
    label "zmar&#322;y"
  ]
  node [
    id 270
    label "istota_nadprzyrodzona"
  ]
  node [
    id 271
    label "power"
  ]
  node [
    id 272
    label "entity"
  ]
  node [
    id 273
    label "ofiarowywa&#263;"
  ]
  node [
    id 274
    label "oddech"
  ]
  node [
    id 275
    label "seksualno&#347;&#263;"
  ]
  node [
    id 276
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 277
    label "byt"
  ]
  node [
    id 278
    label "si&#322;a"
  ]
  node [
    id 279
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 280
    label "ego"
  ]
  node [
    id 281
    label "ofiarowanie"
  ]
  node [
    id 282
    label "kompleksja"
  ]
  node [
    id 283
    label "charakter"
  ]
  node [
    id 284
    label "fizjonomia"
  ]
  node [
    id 285
    label "kompleks"
  ]
  node [
    id 286
    label "shape"
  ]
  node [
    id 287
    label "zapalno&#347;&#263;"
  ]
  node [
    id 288
    label "T&#281;sknica"
  ]
  node [
    id 289
    label "ofiarowa&#263;"
  ]
  node [
    id 290
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 291
    label "nad&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 292
    label "passion"
  ]
  node [
    id 293
    label "b&#243;g"
  ]
  node [
    id 294
    label "Apollo"
  ]
  node [
    id 295
    label "zbawienie"
  ]
  node [
    id 296
    label "anielsko"
  ]
  node [
    id 297
    label "wspania&#322;y"
  ]
  node [
    id 298
    label "doskona&#322;y"
  ]
  node [
    id 299
    label "pi&#281;kny"
  ]
  node [
    id 300
    label "anio&#322;owy"
  ]
  node [
    id 301
    label "wielki"
  ]
  node [
    id 302
    label "dobro&#263;"
  ]
  node [
    id 303
    label "free"
  ]
  node [
    id 304
    label "notice"
  ]
  node [
    id 305
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 306
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 307
    label "styka&#263;_si&#281;"
  ]
  node [
    id 308
    label "zaczyna&#263;"
  ]
  node [
    id 309
    label "nastawia&#263;"
  ]
  node [
    id 310
    label "get_in_touch"
  ]
  node [
    id 311
    label "odkr&#281;ca&#263;_kurek"
  ]
  node [
    id 312
    label "dokoptowywa&#263;"
  ]
  node [
    id 313
    label "uruchamia&#263;"
  ]
  node [
    id 314
    label "involve"
  ]
  node [
    id 315
    label "umieszcza&#263;"
  ]
  node [
    id 316
    label "connect"
  ]
  node [
    id 317
    label "ostatnie_podrygi"
  ]
  node [
    id 318
    label "visitation"
  ]
  node [
    id 319
    label "agonia"
  ]
  node [
    id 320
    label "defenestracja"
  ]
  node [
    id 321
    label "kres"
  ]
  node [
    id 322
    label "wydarzenie"
  ]
  node [
    id 323
    label "mogi&#322;a"
  ]
  node [
    id 324
    label "kres_&#380;ycia"
  ]
  node [
    id 325
    label "szereg"
  ]
  node [
    id 326
    label "szeol"
  ]
  node [
    id 327
    label "pogrzebanie"
  ]
  node [
    id 328
    label "&#380;a&#322;oba"
  ]
  node [
    id 329
    label "zabicie"
  ]
  node [
    id 330
    label "przebiec"
  ]
  node [
    id 331
    label "czynno&#347;&#263;"
  ]
  node [
    id 332
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 333
    label "motyw"
  ]
  node [
    id 334
    label "przebiegni&#281;cie"
  ]
  node [
    id 335
    label "fabu&#322;a"
  ]
  node [
    id 336
    label "Rzym_Zachodni"
  ]
  node [
    id 337
    label "whole"
  ]
  node [
    id 338
    label "ilo&#347;&#263;"
  ]
  node [
    id 339
    label "element"
  ]
  node [
    id 340
    label "Rzym_Wschodni"
  ]
  node [
    id 341
    label "urz&#261;dzenie"
  ]
  node [
    id 342
    label "time"
  ]
  node [
    id 343
    label "czas"
  ]
  node [
    id 344
    label "&#347;mier&#263;"
  ]
  node [
    id 345
    label "death"
  ]
  node [
    id 346
    label "upadek"
  ]
  node [
    id 347
    label "zmierzch"
  ]
  node [
    id 348
    label "stan"
  ]
  node [
    id 349
    label "nieuleczalnie_chory"
  ]
  node [
    id 350
    label "spocz&#261;&#263;"
  ]
  node [
    id 351
    label "spocz&#281;cie"
  ]
  node [
    id 352
    label "pochowanie"
  ]
  node [
    id 353
    label "spoczywa&#263;"
  ]
  node [
    id 354
    label "chowanie"
  ]
  node [
    id 355
    label "park_sztywnych"
  ]
  node [
    id 356
    label "pomnik"
  ]
  node [
    id 357
    label "nagrobek"
  ]
  node [
    id 358
    label "prochowisko"
  ]
  node [
    id 359
    label "miejsce_poch&#243;wku"
  ]
  node [
    id 360
    label "spoczywanie"
  ]
  node [
    id 361
    label "za&#347;wiaty"
  ]
  node [
    id 362
    label "judaizm"
  ]
  node [
    id 363
    label "wyrzucenie"
  ]
  node [
    id 364
    label "defenestration"
  ]
  node [
    id 365
    label "zaj&#347;cie"
  ]
  node [
    id 366
    label "&#380;al"
  ]
  node [
    id 367
    label "paznokie&#263;"
  ]
  node [
    id 368
    label "symbol"
  ]
  node [
    id 369
    label "kir"
  ]
  node [
    id 370
    label "brud"
  ]
  node [
    id 371
    label "burying"
  ]
  node [
    id 372
    label "zasypanie"
  ]
  node [
    id 373
    label "zw&#322;oki"
  ]
  node [
    id 374
    label "burial"
  ]
  node [
    id 375
    label "w&#322;o&#380;enie"
  ]
  node [
    id 376
    label "porobienie"
  ]
  node [
    id 377
    label "gr&#243;b"
  ]
  node [
    id 378
    label "uniemo&#380;liwienie"
  ]
  node [
    id 379
    label "destruction"
  ]
  node [
    id 380
    label "zabrzmienie"
  ]
  node [
    id 381
    label "skrzywdzenie"
  ]
  node [
    id 382
    label "pozabijanie"
  ]
  node [
    id 383
    label "zniszczenie"
  ]
  node [
    id 384
    label "zaszkodzenie"
  ]
  node [
    id 385
    label "usuni&#281;cie"
  ]
  node [
    id 386
    label "spowodowanie"
  ]
  node [
    id 387
    label "killing"
  ]
  node [
    id 388
    label "zdarzenie_si&#281;"
  ]
  node [
    id 389
    label "czyn"
  ]
  node [
    id 390
    label "umarcie"
  ]
  node [
    id 391
    label "granie"
  ]
  node [
    id 392
    label "zamkni&#281;cie"
  ]
  node [
    id 393
    label "compaction"
  ]
  node [
    id 394
    label "sprawa"
  ]
  node [
    id 395
    label "ust&#281;p"
  ]
  node [
    id 396
    label "plan"
  ]
  node [
    id 397
    label "obiekt_matematyczny"
  ]
  node [
    id 398
    label "problemat"
  ]
  node [
    id 399
    label "plamka"
  ]
  node [
    id 400
    label "stopie&#324;_pisma"
  ]
  node [
    id 401
    label "jednostka"
  ]
  node [
    id 402
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 403
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 404
    label "mark"
  ]
  node [
    id 405
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 406
    label "prosta"
  ]
  node [
    id 407
    label "problematyka"
  ]
  node [
    id 408
    label "obiekt"
  ]
  node [
    id 409
    label "zapunktowa&#263;"
  ]
  node [
    id 410
    label "podpunkt"
  ]
  node [
    id 411
    label "wojsko"
  ]
  node [
    id 412
    label "point"
  ]
  node [
    id 413
    label "pozycja"
  ]
  node [
    id 414
    label "szpaler"
  ]
  node [
    id 415
    label "column"
  ]
  node [
    id 416
    label "uporz&#261;dkowanie"
  ]
  node [
    id 417
    label "mn&#243;stwo"
  ]
  node [
    id 418
    label "unit"
  ]
  node [
    id 419
    label "rozmieszczenie"
  ]
  node [
    id 420
    label "tract"
  ]
  node [
    id 421
    label "wyra&#380;enie"
  ]
  node [
    id 422
    label "infimum"
  ]
  node [
    id 423
    label "powodowanie"
  ]
  node [
    id 424
    label "liczenie"
  ]
  node [
    id 425
    label "skutek"
  ]
  node [
    id 426
    label "podzia&#322;anie"
  ]
  node [
    id 427
    label "supremum"
  ]
  node [
    id 428
    label "kampania"
  ]
  node [
    id 429
    label "uruchamianie"
  ]
  node [
    id 430
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 431
    label "hipnotyzowanie"
  ]
  node [
    id 432
    label "robienie"
  ]
  node [
    id 433
    label "uruchomienie"
  ]
  node [
    id 434
    label "nakr&#281;canie"
  ]
  node [
    id 435
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 436
    label "matematyka"
  ]
  node [
    id 437
    label "reakcja_chemiczna"
  ]
  node [
    id 438
    label "tr&#243;jstronny"
  ]
  node [
    id 439
    label "natural_process"
  ]
  node [
    id 440
    label "nakr&#281;cenie"
  ]
  node [
    id 441
    label "zatrzymanie"
  ]
  node [
    id 442
    label "wp&#322;yw"
  ]
  node [
    id 443
    label "rzut"
  ]
  node [
    id 444
    label "podtrzymywanie"
  ]
  node [
    id 445
    label "w&#322;&#261;czanie"
  ]
  node [
    id 446
    label "liczy&#263;"
  ]
  node [
    id 447
    label "operation"
  ]
  node [
    id 448
    label "dzianie_si&#281;"
  ]
  node [
    id 449
    label "zadzia&#322;anie"
  ]
  node [
    id 450
    label "priorytet"
  ]
  node [
    id 451
    label "bycie"
  ]
  node [
    id 452
    label "rozpocz&#281;cie"
  ]
  node [
    id 453
    label "docieranie"
  ]
  node [
    id 454
    label "funkcja"
  ]
  node [
    id 455
    label "czynny"
  ]
  node [
    id 456
    label "impact"
  ]
  node [
    id 457
    label "zako&#324;czenie"
  ]
  node [
    id 458
    label "act"
  ]
  node [
    id 459
    label "wdzieranie_si&#281;"
  ]
  node [
    id 460
    label "w&#322;&#261;czenie"
  ]
  node [
    id 461
    label "Barb&#243;rka"
  ]
  node [
    id 462
    label "miesi&#261;c"
  ]
  node [
    id 463
    label "Bo&#380;e_Narodzenie"
  ]
  node [
    id 464
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 465
    label "Sylwester"
  ]
  node [
    id 466
    label "tydzie&#324;"
  ]
  node [
    id 467
    label "miech"
  ]
  node [
    id 468
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 469
    label "rok"
  ]
  node [
    id 470
    label "kalendy"
  ]
  node [
    id 471
    label "g&#243;rnik"
  ]
  node [
    id 472
    label "comber"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
  edge [
    source 3
    target 149
  ]
  edge [
    source 3
    target 150
  ]
  edge [
    source 3
    target 151
  ]
  edge [
    source 3
    target 152
  ]
  edge [
    source 3
    target 153
  ]
  edge [
    source 3
    target 154
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 156
  ]
  edge [
    source 3
    target 157
  ]
  edge [
    source 3
    target 158
  ]
  edge [
    source 3
    target 159
  ]
  edge [
    source 3
    target 160
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 227
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 232
  ]
  edge [
    source 8
    target 233
  ]
  edge [
    source 8
    target 234
  ]
  edge [
    source 8
    target 235
  ]
  edge [
    source 8
    target 236
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 238
  ]
  edge [
    source 8
    target 239
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 241
  ]
  edge [
    source 8
    target 242
  ]
  edge [
    source 8
    target 243
  ]
  edge [
    source 8
    target 244
  ]
  edge [
    source 8
    target 245
  ]
  edge [
    source 8
    target 246
  ]
  edge [
    source 8
    target 247
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 8
    target 255
  ]
  edge [
    source 8
    target 256
  ]
  edge [
    source 8
    target 257
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 259
  ]
  edge [
    source 8
    target 260
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 261
  ]
  edge [
    source 8
    target 262
  ]
  edge [
    source 8
    target 263
  ]
  edge [
    source 8
    target 264
  ]
  edge [
    source 8
    target 265
  ]
  edge [
    source 8
    target 266
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 268
  ]
  edge [
    source 8
    target 269
  ]
  edge [
    source 8
    target 270
  ]
  edge [
    source 8
    target 271
  ]
  edge [
    source 8
    target 272
  ]
  edge [
    source 8
    target 273
  ]
  edge [
    source 8
    target 274
  ]
  edge [
    source 8
    target 275
  ]
  edge [
    source 8
    target 276
  ]
  edge [
    source 8
    target 277
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 279
  ]
  edge [
    source 8
    target 280
  ]
  edge [
    source 8
    target 281
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 283
  ]
  edge [
    source 8
    target 284
  ]
  edge [
    source 8
    target 285
  ]
  edge [
    source 8
    target 286
  ]
  edge [
    source 8
    target 287
  ]
  edge [
    source 8
    target 288
  ]
  edge [
    source 8
    target 289
  ]
  edge [
    source 8
    target 290
  ]
  edge [
    source 8
    target 291
  ]
  edge [
    source 8
    target 292
  ]
  edge [
    source 8
    target 293
  ]
  edge [
    source 8
    target 294
  ]
  edge [
    source 8
    target 295
  ]
  edge [
    source 8
    target 296
  ]
  edge [
    source 8
    target 297
  ]
  edge [
    source 8
    target 298
  ]
  edge [
    source 8
    target 299
  ]
  edge [
    source 8
    target 300
  ]
  edge [
    source 8
    target 301
  ]
  edge [
    source 8
    target 302
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 303
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 304
  ]
  edge [
    source 10
    target 305
  ]
  edge [
    source 10
    target 306
  ]
  edge [
    source 10
    target 307
  ]
  edge [
    source 10
    target 308
  ]
  edge [
    source 10
    target 309
  ]
  edge [
    source 10
    target 310
  ]
  edge [
    source 10
    target 311
  ]
  edge [
    source 10
    target 312
  ]
  edge [
    source 10
    target 313
  ]
  edge [
    source 10
    target 314
  ]
  edge [
    source 10
    target 315
  ]
  edge [
    source 10
    target 316
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 317
  ]
  edge [
    source 11
    target 318
  ]
  edge [
    source 11
    target 319
  ]
  edge [
    source 11
    target 320
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 321
  ]
  edge [
    source 11
    target 322
  ]
  edge [
    source 11
    target 323
  ]
  edge [
    source 11
    target 324
  ]
  edge [
    source 11
    target 325
  ]
  edge [
    source 11
    target 326
  ]
  edge [
    source 11
    target 327
  ]
  edge [
    source 11
    target 30
  ]
  edge [
    source 11
    target 62
  ]
  edge [
    source 11
    target 328
  ]
  edge [
    source 11
    target 65
  ]
  edge [
    source 11
    target 329
  ]
  edge [
    source 11
    target 330
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 331
  ]
  edge [
    source 11
    target 332
  ]
  edge [
    source 11
    target 333
  ]
  edge [
    source 11
    target 334
  ]
  edge [
    source 11
    target 335
  ]
  edge [
    source 11
    target 336
  ]
  edge [
    source 11
    target 337
  ]
  edge [
    source 11
    target 338
  ]
  edge [
    source 11
    target 339
  ]
  edge [
    source 11
    target 340
  ]
  edge [
    source 11
    target 341
  ]
  edge [
    source 11
    target 55
  ]
  edge [
    source 11
    target 56
  ]
  edge [
    source 11
    target 57
  ]
  edge [
    source 11
    target 58
  ]
  edge [
    source 11
    target 59
  ]
  edge [
    source 11
    target 60
  ]
  edge [
    source 11
    target 61
  ]
  edge [
    source 11
    target 63
  ]
  edge [
    source 11
    target 64
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 342
  ]
  edge [
    source 11
    target 343
  ]
  edge [
    source 11
    target 344
  ]
  edge [
    source 11
    target 345
  ]
  edge [
    source 11
    target 346
  ]
  edge [
    source 11
    target 347
  ]
  edge [
    source 11
    target 348
  ]
  edge [
    source 11
    target 349
  ]
  edge [
    source 11
    target 350
  ]
  edge [
    source 11
    target 351
  ]
  edge [
    source 11
    target 352
  ]
  edge [
    source 11
    target 353
  ]
  edge [
    source 11
    target 354
  ]
  edge [
    source 11
    target 355
  ]
  edge [
    source 11
    target 356
  ]
  edge [
    source 11
    target 357
  ]
  edge [
    source 11
    target 358
  ]
  edge [
    source 11
    target 359
  ]
  edge [
    source 11
    target 360
  ]
  edge [
    source 11
    target 361
  ]
  edge [
    source 11
    target 255
  ]
  edge [
    source 11
    target 362
  ]
  edge [
    source 11
    target 363
  ]
  edge [
    source 11
    target 364
  ]
  edge [
    source 11
    target 365
  ]
  edge [
    source 11
    target 366
  ]
  edge [
    source 11
    target 367
  ]
  edge [
    source 11
    target 368
  ]
  edge [
    source 11
    target 369
  ]
  edge [
    source 11
    target 370
  ]
  edge [
    source 11
    target 371
  ]
  edge [
    source 11
    target 372
  ]
  edge [
    source 11
    target 373
  ]
  edge [
    source 11
    target 374
  ]
  edge [
    source 11
    target 375
  ]
  edge [
    source 11
    target 376
  ]
  edge [
    source 11
    target 377
  ]
  edge [
    source 11
    target 378
  ]
  edge [
    source 11
    target 379
  ]
  edge [
    source 11
    target 380
  ]
  edge [
    source 11
    target 381
  ]
  edge [
    source 11
    target 382
  ]
  edge [
    source 11
    target 383
  ]
  edge [
    source 11
    target 384
  ]
  edge [
    source 11
    target 385
  ]
  edge [
    source 11
    target 386
  ]
  edge [
    source 11
    target 387
  ]
  edge [
    source 11
    target 388
  ]
  edge [
    source 11
    target 389
  ]
  edge [
    source 11
    target 390
  ]
  edge [
    source 11
    target 391
  ]
  edge [
    source 11
    target 392
  ]
  edge [
    source 11
    target 393
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 394
  ]
  edge [
    source 11
    target 395
  ]
  edge [
    source 11
    target 396
  ]
  edge [
    source 11
    target 397
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 11
    target 408
  ]
  edge [
    source 11
    target 409
  ]
  edge [
    source 11
    target 410
  ]
  edge [
    source 11
    target 411
  ]
  edge [
    source 11
    target 412
  ]
  edge [
    source 11
    target 413
  ]
  edge [
    source 11
    target 414
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 415
  ]
  edge [
    source 11
    target 416
  ]
  edge [
    source 11
    target 417
  ]
  edge [
    source 11
    target 418
  ]
  edge [
    source 11
    target 419
  ]
  edge [
    source 11
    target 420
  ]
  edge [
    source 11
    target 421
  ]
  edge [
    source 11
    target 422
  ]
  edge [
    source 11
    target 423
  ]
  edge [
    source 11
    target 424
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 425
  ]
  edge [
    source 11
    target 426
  ]
  edge [
    source 11
    target 427
  ]
  edge [
    source 11
    target 428
  ]
  edge [
    source 11
    target 429
  ]
  edge [
    source 11
    target 430
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 431
  ]
  edge [
    source 11
    target 432
  ]
  edge [
    source 11
    target 433
  ]
  edge [
    source 11
    target 434
  ]
  edge [
    source 11
    target 435
  ]
  edge [
    source 11
    target 436
  ]
  edge [
    source 11
    target 437
  ]
  edge [
    source 11
    target 438
  ]
  edge [
    source 11
    target 439
  ]
  edge [
    source 11
    target 440
  ]
  edge [
    source 11
    target 441
  ]
  edge [
    source 11
    target 442
  ]
  edge [
    source 11
    target 443
  ]
  edge [
    source 11
    target 444
  ]
  edge [
    source 11
    target 445
  ]
  edge [
    source 11
    target 446
  ]
  edge [
    source 11
    target 447
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 448
  ]
  edge [
    source 11
    target 449
  ]
  edge [
    source 11
    target 450
  ]
  edge [
    source 11
    target 451
  ]
  edge [
    source 11
    target 452
  ]
  edge [
    source 11
    target 453
  ]
  edge [
    source 11
    target 454
  ]
  edge [
    source 11
    target 455
  ]
  edge [
    source 11
    target 456
  ]
  edge [
    source 11
    target 221
  ]
  edge [
    source 11
    target 457
  ]
  edge [
    source 11
    target 458
  ]
  edge [
    source 11
    target 459
  ]
  edge [
    source 11
    target 460
  ]
  edge [
    source 12
    target 461
  ]
  edge [
    source 12
    target 462
  ]
  edge [
    source 12
    target 463
  ]
  edge [
    source 12
    target 464
  ]
  edge [
    source 12
    target 465
  ]
  edge [
    source 12
    target 466
  ]
  edge [
    source 12
    target 467
  ]
  edge [
    source 12
    target 468
  ]
  edge [
    source 12
    target 343
  ]
  edge [
    source 12
    target 469
  ]
  edge [
    source 12
    target 470
  ]
  edge [
    source 12
    target 471
  ]
  edge [
    source 12
    target 472
  ]
]
