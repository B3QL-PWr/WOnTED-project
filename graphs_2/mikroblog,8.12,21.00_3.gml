graph [
  node [
    id 0
    label "kur"
    origin "text"
  ]
  node [
    id 1
    label "maca"
    origin "text"
  ]
  node [
    id 2
    label "zapia&#263;"
  ]
  node [
    id 3
    label "pia&#263;"
  ]
  node [
    id 4
    label "kura"
  ]
  node [
    id 5
    label "r&#243;&#380;yczka"
  ]
  node [
    id 6
    label "zapianie"
  ]
  node [
    id 7
    label "samiec"
  ]
  node [
    id 8
    label "pianie"
  ]
  node [
    id 9
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 10
    label "zwierz&#281;"
  ]
  node [
    id 11
    label "dymorfizm_p&#322;ciowy"
  ]
  node [
    id 12
    label "dr&#243;b"
  ]
  node [
    id 13
    label "kurczak"
  ]
  node [
    id 14
    label "ptak"
  ]
  node [
    id 15
    label "samica"
  ]
  node [
    id 16
    label "gdakanie"
  ]
  node [
    id 17
    label "no&#347;no&#347;&#263;"
  ]
  node [
    id 18
    label "zagdaka&#263;"
  ]
  node [
    id 19
    label "kur_bankiwa"
  ]
  node [
    id 20
    label "ptak_&#322;owny"
  ]
  node [
    id 21
    label "gdakni&#281;cie"
  ]
  node [
    id 22
    label "wys&#322;awianie"
  ]
  node [
    id 23
    label "kogut"
  ]
  node [
    id 24
    label "pienie"
  ]
  node [
    id 25
    label "wydawanie"
  ]
  node [
    id 26
    label "brag"
  ]
  node [
    id 27
    label "ba&#380;ant"
  ]
  node [
    id 28
    label "wyda&#263;_g&#322;os"
  ]
  node [
    id 29
    label "gaworzy&#263;"
  ]
  node [
    id 30
    label "gloat"
  ]
  node [
    id 31
    label "wys&#322;awia&#263;"
  ]
  node [
    id 32
    label "wydanie"
  ]
  node [
    id 33
    label "cz&#261;stka"
  ]
  node [
    id 34
    label "wirus_r&#243;&#380;yczki"
  ]
  node [
    id 35
    label "choroba_wirusowa"
  ]
  node [
    id 36
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 37
    label "surowiec"
  ]
  node [
    id 38
    label "placek"
  ]
  node [
    id 39
    label "jednostka_obj&#281;to&#347;ci_cia&#322;_sypkich"
  ]
  node [
    id 40
    label "korzec"
  ]
  node [
    id 41
    label "pieprzyca"
  ]
  node [
    id 42
    label "m&#261;czny"
  ]
  node [
    id 43
    label "maka"
  ]
  node [
    id 44
    label "korze&#324;"
  ]
  node [
    id 45
    label "pieczywo_chrupkie"
  ]
  node [
    id 46
    label "ro&#347;lina_przyprawowa"
  ]
  node [
    id 47
    label "przyprawy_korzenne"
  ]
  node [
    id 48
    label "w&#322;o&#347;nik"
  ]
  node [
    id 49
    label "cz&#322;on_p&#281;dowy"
  ]
  node [
    id 50
    label "kormus"
  ]
  node [
    id 51
    label "bylina"
  ]
  node [
    id 52
    label "stela"
  ]
  node [
    id 53
    label "organ"
  ]
  node [
    id 54
    label "przyprawa"
  ]
  node [
    id 55
    label "selerowate"
  ]
  node [
    id 56
    label "organ_ro&#347;linny"
  ]
  node [
    id 57
    label "element_anatomiczny"
  ]
  node [
    id 58
    label "lubczyk"
  ]
  node [
    id 59
    label "&#322;akotne_ziele"
  ]
  node [
    id 60
    label "system_korzeniowy"
  ]
  node [
    id 61
    label "lovage"
  ]
  node [
    id 62
    label "ro&#347;lina"
  ]
  node [
    id 63
    label "psoralen"
  ]
  node [
    id 64
    label "sk&#322;adnik"
  ]
  node [
    id 65
    label "tworzywo"
  ]
  node [
    id 66
    label "kapustowate"
  ]
  node [
    id 67
    label "choroba_paso&#380;ytnicza"
  ]
  node [
    id 68
    label "ro&#347;lina_zielna"
  ]
  node [
    id 69
    label "choroba_somatyczna"
  ]
  node [
    id 70
    label "potrawa"
  ]
  node [
    id 71
    label "ciasto"
  ]
  node [
    id 72
    label "miarka"
  ]
  node [
    id 73
    label "&#322;aszt"
  ]
  node [
    id 74
    label "p&#243;&#322;korzec"
  ]
  node [
    id 75
    label "bia&#322;y"
  ]
  node [
    id 76
    label "zbo&#380;owy"
  ]
  node [
    id 77
    label "przypominaj&#261;cy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
]
