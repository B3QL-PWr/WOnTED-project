graph [
  node [
    id 0
    label "&#347;wie&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "nagranie"
    origin "text"
  ]
  node [
    id 2
    label "nowy"
  ]
  node [
    id 3
    label "jasny"
  ]
  node [
    id 4
    label "&#347;wie&#380;o"
  ]
  node [
    id 5
    label "dobry"
  ]
  node [
    id 6
    label "surowy"
  ]
  node [
    id 7
    label "orze&#378;wienie"
  ]
  node [
    id 8
    label "energiczny"
  ]
  node [
    id 9
    label "orze&#378;wianie"
  ]
  node [
    id 10
    label "rze&#347;ki"
  ]
  node [
    id 11
    label "zdrowy"
  ]
  node [
    id 12
    label "czysty"
  ]
  node [
    id 13
    label "oryginalnie"
  ]
  node [
    id 14
    label "przyjemny"
  ]
  node [
    id 15
    label "o&#380;ywczy"
  ]
  node [
    id 16
    label "m&#322;ody"
  ]
  node [
    id 17
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 18
    label "inny"
  ]
  node [
    id 19
    label "&#380;ywy"
  ]
  node [
    id 20
    label "soczysty"
  ]
  node [
    id 21
    label "nowotny"
  ]
  node [
    id 22
    label "ciekawy"
  ]
  node [
    id 23
    label "szybki"
  ]
  node [
    id 24
    label "&#380;ywotny"
  ]
  node [
    id 25
    label "naturalny"
  ]
  node [
    id 26
    label "&#380;ywo"
  ]
  node [
    id 27
    label "cz&#322;owiek"
  ]
  node [
    id 28
    label "o&#380;ywianie"
  ]
  node [
    id 29
    label "&#380;ycie"
  ]
  node [
    id 30
    label "silny"
  ]
  node [
    id 31
    label "g&#322;&#281;boki"
  ]
  node [
    id 32
    label "wyra&#378;ny"
  ]
  node [
    id 33
    label "czynny"
  ]
  node [
    id 34
    label "aktualny"
  ]
  node [
    id 35
    label "zgrabny"
  ]
  node [
    id 36
    label "prawdziwy"
  ]
  node [
    id 37
    label "realistyczny"
  ]
  node [
    id 38
    label "kolejny"
  ]
  node [
    id 39
    label "nowo"
  ]
  node [
    id 40
    label "bie&#380;&#261;cy"
  ]
  node [
    id 41
    label "drugi"
  ]
  node [
    id 42
    label "narybek"
  ]
  node [
    id 43
    label "obcy"
  ]
  node [
    id 44
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 45
    label "j&#281;drny"
  ]
  node [
    id 46
    label "mocny"
  ]
  node [
    id 47
    label "soczy&#347;cie"
  ]
  node [
    id 48
    label "nasycony"
  ]
  node [
    id 49
    label "pe&#322;ny"
  ]
  node [
    id 50
    label "o&#347;wietlenie"
  ]
  node [
    id 51
    label "szczery"
  ]
  node [
    id 52
    label "b&#322;yszcz&#261;cy"
  ]
  node [
    id 53
    label "jasno"
  ]
  node [
    id 54
    label "o&#347;wietlanie"
  ]
  node [
    id 55
    label "przytomny"
  ]
  node [
    id 56
    label "zrozumia&#322;y"
  ]
  node [
    id 57
    label "niezm&#261;cony"
  ]
  node [
    id 58
    label "bia&#322;y"
  ]
  node [
    id 59
    label "jednoznaczny"
  ]
  node [
    id 60
    label "klarowny"
  ]
  node [
    id 61
    label "pogodny"
  ]
  node [
    id 62
    label "zdrowo"
  ]
  node [
    id 63
    label "wyzdrowienie"
  ]
  node [
    id 64
    label "uzdrowienie"
  ]
  node [
    id 65
    label "wyleczenie_si&#281;"
  ]
  node [
    id 66
    label "zr&#243;wnowa&#380;ony"
  ]
  node [
    id 67
    label "normalny"
  ]
  node [
    id 68
    label "rozs&#261;dny"
  ]
  node [
    id 69
    label "korzystny"
  ]
  node [
    id 70
    label "zdrowienie"
  ]
  node [
    id 71
    label "solidny"
  ]
  node [
    id 72
    label "uzdrawianie"
  ]
  node [
    id 73
    label "m&#322;odo"
  ]
  node [
    id 74
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 75
    label "nowo&#380;eniec"
  ]
  node [
    id 76
    label "nie&#380;onaty"
  ]
  node [
    id 77
    label "wczesny"
  ]
  node [
    id 78
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 79
    label "m&#261;&#380;"
  ]
  node [
    id 80
    label "charakterystyczny"
  ]
  node [
    id 81
    label "pewny"
  ]
  node [
    id 82
    label "przezroczy&#347;cie"
  ]
  node [
    id 83
    label "nieemisyjny"
  ]
  node [
    id 84
    label "bezb&#322;&#281;dny"
  ]
  node [
    id 85
    label "kompletny"
  ]
  node [
    id 86
    label "umycie"
  ]
  node [
    id 87
    label "ekologiczny"
  ]
  node [
    id 88
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 89
    label "bezpieczny"
  ]
  node [
    id 90
    label "dopuszczalny"
  ]
  node [
    id 91
    label "ca&#322;y"
  ]
  node [
    id 92
    label "mycie"
  ]
  node [
    id 93
    label "jednolity"
  ]
  node [
    id 94
    label "udany"
  ]
  node [
    id 95
    label "czysto"
  ]
  node [
    id 96
    label "klarowanie"
  ]
  node [
    id 97
    label "bezchmurny"
  ]
  node [
    id 98
    label "ostry"
  ]
  node [
    id 99
    label "legalny"
  ]
  node [
    id 100
    label "wyczyszczenie_si&#281;"
  ]
  node [
    id 101
    label "prze&#378;roczy"
  ]
  node [
    id 102
    label "wolny"
  ]
  node [
    id 103
    label "czyszczenie_si&#281;"
  ]
  node [
    id 104
    label "uczciwy"
  ]
  node [
    id 105
    label "do_czysta"
  ]
  node [
    id 106
    label "klarowanie_si&#281;"
  ]
  node [
    id 107
    label "sklarowanie"
  ]
  node [
    id 108
    label "cnotliwy"
  ]
  node [
    id 109
    label "ewidentny"
  ]
  node [
    id 110
    label "wspinaczka"
  ]
  node [
    id 111
    label "porz&#261;dny"
  ]
  node [
    id 112
    label "schludny"
  ]
  node [
    id 113
    label "doskona&#322;y"
  ]
  node [
    id 114
    label "orze&#378;wiaj&#261;cy"
  ]
  node [
    id 115
    label "nieodrodny"
  ]
  node [
    id 116
    label "wyklarowanie_si&#281;"
  ]
  node [
    id 117
    label "dobroczynny"
  ]
  node [
    id 118
    label "czw&#243;rka"
  ]
  node [
    id 119
    label "spokojny"
  ]
  node [
    id 120
    label "skuteczny"
  ]
  node [
    id 121
    label "&#347;mieszny"
  ]
  node [
    id 122
    label "mi&#322;y"
  ]
  node [
    id 123
    label "grzeczny"
  ]
  node [
    id 124
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 125
    label "powitanie"
  ]
  node [
    id 126
    label "dobrze"
  ]
  node [
    id 127
    label "zwrot"
  ]
  node [
    id 128
    label "pomy&#347;lny"
  ]
  node [
    id 129
    label "moralny"
  ]
  node [
    id 130
    label "drogi"
  ]
  node [
    id 131
    label "pozytywny"
  ]
  node [
    id 132
    label "odpowiedni"
  ]
  node [
    id 133
    label "pos&#322;uszny"
  ]
  node [
    id 134
    label "przyjemnie"
  ]
  node [
    id 135
    label "pobudzaj&#261;cy"
  ]
  node [
    id 136
    label "zbawienny"
  ]
  node [
    id 137
    label "stymuluj&#261;cy"
  ]
  node [
    id 138
    label "o&#380;ywczo"
  ]
  node [
    id 139
    label "energicznie"
  ]
  node [
    id 140
    label "jary"
  ]
  node [
    id 141
    label "osobno"
  ]
  node [
    id 142
    label "r&#243;&#380;ny"
  ]
  node [
    id 143
    label "inszy"
  ]
  node [
    id 144
    label "inaczej"
  ]
  node [
    id 145
    label "ch&#322;odny"
  ]
  node [
    id 146
    label "rze&#347;ko"
  ]
  node [
    id 147
    label "odmiennie"
  ]
  node [
    id 148
    label "niestandardowo"
  ]
  node [
    id 149
    label "oryginalny"
  ]
  node [
    id 150
    label "refreshingly"
  ]
  node [
    id 151
    label "gro&#378;nie"
  ]
  node [
    id 152
    label "twardy"
  ]
  node [
    id 153
    label "trudny"
  ]
  node [
    id 154
    label "srogi"
  ]
  node [
    id 155
    label "powa&#380;ny"
  ]
  node [
    id 156
    label "dokuczliwy"
  ]
  node [
    id 157
    label "surowo"
  ]
  node [
    id 158
    label "oszcz&#281;dny"
  ]
  node [
    id 159
    label "stymuluj&#261;co"
  ]
  node [
    id 160
    label "traktowanie"
  ]
  node [
    id 161
    label "powodowanie"
  ]
  node [
    id 162
    label "refresher"
  ]
  node [
    id 163
    label "refresher_course"
  ]
  node [
    id 164
    label "doznanie"
  ]
  node [
    id 165
    label "spowodowanie"
  ]
  node [
    id 166
    label "coolness"
  ]
  node [
    id 167
    label "potraktowanie"
  ]
  node [
    id 168
    label "refreshment"
  ]
  node [
    id 169
    label "pierwotnie"
  ]
  node [
    id 170
    label "niezwykle"
  ]
  node [
    id 171
    label "wytw&#243;r"
  ]
  node [
    id 172
    label "wys&#322;uchanie"
  ]
  node [
    id 173
    label "utrwalenie"
  ]
  node [
    id 174
    label "wys&#322;ucha&#263;"
  ]
  node [
    id 175
    label "recording"
  ]
  node [
    id 176
    label "ustalenie"
  ]
  node [
    id 177
    label "trwalszy"
  ]
  node [
    id 178
    label "confirmation"
  ]
  node [
    id 179
    label "zachowanie"
  ]
  node [
    id 180
    label "przedmiot"
  ]
  node [
    id 181
    label "p&#322;&#243;d"
  ]
  node [
    id 182
    label "work"
  ]
  node [
    id 183
    label "rezultat"
  ]
  node [
    id 184
    label "pos&#322;uchanie"
  ]
  node [
    id 185
    label "spe&#322;nienie"
  ]
  node [
    id 186
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 187
    label "hearing"
  ]
  node [
    id 188
    label "muzyka"
  ]
  node [
    id 189
    label "spe&#322;ni&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 0
    target 91
  ]
  edge [
    source 0
    target 92
  ]
  edge [
    source 0
    target 93
  ]
  edge [
    source 0
    target 94
  ]
  edge [
    source 0
    target 95
  ]
  edge [
    source 0
    target 96
  ]
  edge [
    source 0
    target 97
  ]
  edge [
    source 0
    target 98
  ]
  edge [
    source 0
    target 99
  ]
  edge [
    source 0
    target 100
  ]
  edge [
    source 0
    target 101
  ]
  edge [
    source 0
    target 102
  ]
  edge [
    source 0
    target 103
  ]
  edge [
    source 0
    target 104
  ]
  edge [
    source 0
    target 105
  ]
  edge [
    source 0
    target 106
  ]
  edge [
    source 0
    target 107
  ]
  edge [
    source 0
    target 108
  ]
  edge [
    source 0
    target 109
  ]
  edge [
    source 0
    target 110
  ]
  edge [
    source 0
    target 111
  ]
  edge [
    source 0
    target 112
  ]
  edge [
    source 0
    target 113
  ]
  edge [
    source 0
    target 114
  ]
  edge [
    source 0
    target 115
  ]
  edge [
    source 0
    target 116
  ]
  edge [
    source 0
    target 117
  ]
  edge [
    source 0
    target 118
  ]
  edge [
    source 0
    target 119
  ]
  edge [
    source 0
    target 120
  ]
  edge [
    source 0
    target 121
  ]
  edge [
    source 0
    target 122
  ]
  edge [
    source 0
    target 123
  ]
  edge [
    source 0
    target 124
  ]
  edge [
    source 0
    target 125
  ]
  edge [
    source 0
    target 126
  ]
  edge [
    source 0
    target 127
  ]
  edge [
    source 0
    target 128
  ]
  edge [
    source 0
    target 129
  ]
  edge [
    source 0
    target 130
  ]
  edge [
    source 0
    target 131
  ]
  edge [
    source 0
    target 132
  ]
  edge [
    source 0
    target 133
  ]
  edge [
    source 0
    target 134
  ]
  edge [
    source 0
    target 135
  ]
  edge [
    source 0
    target 136
  ]
  edge [
    source 0
    target 137
  ]
  edge [
    source 0
    target 138
  ]
  edge [
    source 0
    target 139
  ]
  edge [
    source 0
    target 140
  ]
  edge [
    source 0
    target 141
  ]
  edge [
    source 0
    target 142
  ]
  edge [
    source 0
    target 143
  ]
  edge [
    source 0
    target 144
  ]
  edge [
    source 0
    target 145
  ]
  edge [
    source 0
    target 146
  ]
  edge [
    source 0
    target 147
  ]
  edge [
    source 0
    target 148
  ]
  edge [
    source 0
    target 149
  ]
  edge [
    source 0
    target 150
  ]
  edge [
    source 0
    target 151
  ]
  edge [
    source 0
    target 152
  ]
  edge [
    source 0
    target 153
  ]
  edge [
    source 0
    target 154
  ]
  edge [
    source 0
    target 155
  ]
  edge [
    source 0
    target 156
  ]
  edge [
    source 0
    target 157
  ]
  edge [
    source 0
    target 158
  ]
  edge [
    source 0
    target 159
  ]
  edge [
    source 0
    target 160
  ]
  edge [
    source 0
    target 161
  ]
  edge [
    source 0
    target 162
  ]
  edge [
    source 0
    target 163
  ]
  edge [
    source 0
    target 164
  ]
  edge [
    source 0
    target 165
  ]
  edge [
    source 0
    target 166
  ]
  edge [
    source 0
    target 167
  ]
  edge [
    source 0
    target 168
  ]
  edge [
    source 0
    target 169
  ]
  edge [
    source 0
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 1
    target 172
  ]
  edge [
    source 1
    target 173
  ]
  edge [
    source 1
    target 174
  ]
  edge [
    source 1
    target 175
  ]
  edge [
    source 1
    target 176
  ]
  edge [
    source 1
    target 177
  ]
  edge [
    source 1
    target 178
  ]
  edge [
    source 1
    target 179
  ]
  edge [
    source 1
    target 180
  ]
  edge [
    source 1
    target 181
  ]
  edge [
    source 1
    target 182
  ]
  edge [
    source 1
    target 183
  ]
  edge [
    source 1
    target 184
  ]
  edge [
    source 1
    target 185
  ]
  edge [
    source 1
    target 186
  ]
  edge [
    source 1
    target 187
  ]
  edge [
    source 1
    target 188
  ]
  edge [
    source 1
    target 189
  ]
]
