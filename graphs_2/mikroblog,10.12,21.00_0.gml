graph [
  node [
    id 0
    label "pyta&#263;"
    origin "text"
  ]
  node [
    id 1
    label "tutaj"
    origin "text"
  ]
  node [
    id 2
    label "elektroda"
    origin "text"
  ]
  node [
    id 3
    label "pewnie"
    origin "text"
  ]
  node [
    id 4
    label "zbanowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "sprawdza&#263;"
  ]
  node [
    id 7
    label "ankieter"
  ]
  node [
    id 8
    label "inspect"
  ]
  node [
    id 9
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 10
    label "przes&#322;uchiwa&#263;"
  ]
  node [
    id 11
    label "question"
  ]
  node [
    id 12
    label "examine"
  ]
  node [
    id 13
    label "robi&#263;"
  ]
  node [
    id 14
    label "szpiegowa&#263;"
  ]
  node [
    id 15
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 16
    label "przepytywa&#263;"
  ]
  node [
    id 17
    label "interrogate"
  ]
  node [
    id 18
    label "wypytywa&#263;"
  ]
  node [
    id 19
    label "s&#322;ucha&#263;"
  ]
  node [
    id 20
    label "tam"
  ]
  node [
    id 21
    label "tu"
  ]
  node [
    id 22
    label "electrode"
  ]
  node [
    id 23
    label "przedmiot"
  ]
  node [
    id 24
    label "elektrolizer"
  ]
  node [
    id 25
    label "ogniwo_galwaniczne"
  ]
  node [
    id 26
    label "zboczenie"
  ]
  node [
    id 27
    label "om&#243;wienie"
  ]
  node [
    id 28
    label "sponiewieranie"
  ]
  node [
    id 29
    label "discipline"
  ]
  node [
    id 30
    label "rzecz"
  ]
  node [
    id 31
    label "omawia&#263;"
  ]
  node [
    id 32
    label "kr&#261;&#380;enie"
  ]
  node [
    id 33
    label "tre&#347;&#263;"
  ]
  node [
    id 34
    label "robienie"
  ]
  node [
    id 35
    label "sponiewiera&#263;"
  ]
  node [
    id 36
    label "element"
  ]
  node [
    id 37
    label "entity"
  ]
  node [
    id 38
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 39
    label "tematyka"
  ]
  node [
    id 40
    label "w&#261;tek"
  ]
  node [
    id 41
    label "charakter"
  ]
  node [
    id 42
    label "zbaczanie"
  ]
  node [
    id 43
    label "program_nauczania"
  ]
  node [
    id 44
    label "om&#243;wi&#263;"
  ]
  node [
    id 45
    label "omawianie"
  ]
  node [
    id 46
    label "thing"
  ]
  node [
    id 47
    label "kultura"
  ]
  node [
    id 48
    label "istota"
  ]
  node [
    id 49
    label "zbacza&#263;"
  ]
  node [
    id 50
    label "zboczy&#263;"
  ]
  node [
    id 51
    label "pojemnik"
  ]
  node [
    id 52
    label "wanna"
  ]
  node [
    id 53
    label "najpewniej"
  ]
  node [
    id 54
    label "pewny"
  ]
  node [
    id 55
    label "wiarygodnie"
  ]
  node [
    id 56
    label "mocno"
  ]
  node [
    id 57
    label "pewniej"
  ]
  node [
    id 58
    label "bezpiecznie"
  ]
  node [
    id 59
    label "zwinnie"
  ]
  node [
    id 60
    label "bezpieczny"
  ]
  node [
    id 61
    label "&#322;atwo"
  ]
  node [
    id 62
    label "bezpieczno"
  ]
  node [
    id 63
    label "credibly"
  ]
  node [
    id 64
    label "wiarygodny"
  ]
  node [
    id 65
    label "believably"
  ]
  node [
    id 66
    label "intensywny"
  ]
  node [
    id 67
    label "mocny"
  ]
  node [
    id 68
    label "silny"
  ]
  node [
    id 69
    label "przekonuj&#261;co"
  ]
  node [
    id 70
    label "niema&#322;o"
  ]
  node [
    id 71
    label "powerfully"
  ]
  node [
    id 72
    label "widocznie"
  ]
  node [
    id 73
    label "szczerze"
  ]
  node [
    id 74
    label "konkretnie"
  ]
  node [
    id 75
    label "niepodwa&#380;alnie"
  ]
  node [
    id 76
    label "stabilnie"
  ]
  node [
    id 77
    label "silnie"
  ]
  node [
    id 78
    label "zdecydowanie"
  ]
  node [
    id 79
    label "strongly"
  ]
  node [
    id 80
    label "mo&#380;liwy"
  ]
  node [
    id 81
    label "spokojny"
  ]
  node [
    id 82
    label "upewnianie_si&#281;"
  ]
  node [
    id 83
    label "ufanie"
  ]
  node [
    id 84
    label "wierzenie"
  ]
  node [
    id 85
    label "upewnienie_si&#281;"
  ]
  node [
    id 86
    label "zwinny"
  ]
  node [
    id 87
    label "polotnie"
  ]
  node [
    id 88
    label "p&#322;ynnie"
  ]
  node [
    id 89
    label "sprawnie"
  ]
  node [
    id 90
    label "p&#243;&#322;rocze"
  ]
  node [
    id 91
    label "martwy_sezon"
  ]
  node [
    id 92
    label "kalendarz"
  ]
  node [
    id 93
    label "cykl_astronomiczny"
  ]
  node [
    id 94
    label "lata"
  ]
  node [
    id 95
    label "pora_roku"
  ]
  node [
    id 96
    label "stulecie"
  ]
  node [
    id 97
    label "kurs"
  ]
  node [
    id 98
    label "czas"
  ]
  node [
    id 99
    label "jubileusz"
  ]
  node [
    id 100
    label "grupa"
  ]
  node [
    id 101
    label "kwarta&#322;"
  ]
  node [
    id 102
    label "miesi&#261;c"
  ]
  node [
    id 103
    label "summer"
  ]
  node [
    id 104
    label "odm&#322;adzanie"
  ]
  node [
    id 105
    label "liga"
  ]
  node [
    id 106
    label "jednostka_systematyczna"
  ]
  node [
    id 107
    label "asymilowanie"
  ]
  node [
    id 108
    label "gromada"
  ]
  node [
    id 109
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 110
    label "asymilowa&#263;"
  ]
  node [
    id 111
    label "egzemplarz"
  ]
  node [
    id 112
    label "Entuzjastki"
  ]
  node [
    id 113
    label "zbi&#243;r"
  ]
  node [
    id 114
    label "kompozycja"
  ]
  node [
    id 115
    label "Terranie"
  ]
  node [
    id 116
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 117
    label "category"
  ]
  node [
    id 118
    label "pakiet_klimatyczny"
  ]
  node [
    id 119
    label "oddzia&#322;"
  ]
  node [
    id 120
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 121
    label "cz&#261;steczka"
  ]
  node [
    id 122
    label "stage_set"
  ]
  node [
    id 123
    label "type"
  ]
  node [
    id 124
    label "specgrupa"
  ]
  node [
    id 125
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 126
    label "&#346;wietliki"
  ]
  node [
    id 127
    label "odm&#322;odzenie"
  ]
  node [
    id 128
    label "Eurogrupa"
  ]
  node [
    id 129
    label "odm&#322;adza&#263;"
  ]
  node [
    id 130
    label "formacja_geologiczna"
  ]
  node [
    id 131
    label "harcerze_starsi"
  ]
  node [
    id 132
    label "poprzedzanie"
  ]
  node [
    id 133
    label "czasoprzestrze&#324;"
  ]
  node [
    id 134
    label "laba"
  ]
  node [
    id 135
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 136
    label "chronometria"
  ]
  node [
    id 137
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 138
    label "rachuba_czasu"
  ]
  node [
    id 139
    label "przep&#322;ywanie"
  ]
  node [
    id 140
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 141
    label "czasokres"
  ]
  node [
    id 142
    label "odczyt"
  ]
  node [
    id 143
    label "chwila"
  ]
  node [
    id 144
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 145
    label "dzieje"
  ]
  node [
    id 146
    label "kategoria_gramatyczna"
  ]
  node [
    id 147
    label "poprzedzenie"
  ]
  node [
    id 148
    label "trawienie"
  ]
  node [
    id 149
    label "pochodzi&#263;"
  ]
  node [
    id 150
    label "period"
  ]
  node [
    id 151
    label "okres_czasu"
  ]
  node [
    id 152
    label "poprzedza&#263;"
  ]
  node [
    id 153
    label "schy&#322;ek"
  ]
  node [
    id 154
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 155
    label "odwlekanie_si&#281;"
  ]
  node [
    id 156
    label "zegar"
  ]
  node [
    id 157
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 158
    label "czwarty_wymiar"
  ]
  node [
    id 159
    label "pochodzenie"
  ]
  node [
    id 160
    label "koniugacja"
  ]
  node [
    id 161
    label "Zeitgeist"
  ]
  node [
    id 162
    label "trawi&#263;"
  ]
  node [
    id 163
    label "pogoda"
  ]
  node [
    id 164
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 165
    label "poprzedzi&#263;"
  ]
  node [
    id 166
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 167
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 168
    label "time_period"
  ]
  node [
    id 169
    label "tydzie&#324;"
  ]
  node [
    id 170
    label "miech"
  ]
  node [
    id 171
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 172
    label "kalendy"
  ]
  node [
    id 173
    label "term"
  ]
  node [
    id 174
    label "rok_akademicki"
  ]
  node [
    id 175
    label "rok_szkolny"
  ]
  node [
    id 176
    label "semester"
  ]
  node [
    id 177
    label "anniwersarz"
  ]
  node [
    id 178
    label "rocznica"
  ]
  node [
    id 179
    label "obszar"
  ]
  node [
    id 180
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 181
    label "long_time"
  ]
  node [
    id 182
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 183
    label "almanac"
  ]
  node [
    id 184
    label "rozk&#322;ad"
  ]
  node [
    id 185
    label "wydawnictwo"
  ]
  node [
    id 186
    label "Juliusz_Cezar"
  ]
  node [
    id 187
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 188
    label "zwy&#380;kowanie"
  ]
  node [
    id 189
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 190
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 191
    label "zaj&#281;cia"
  ]
  node [
    id 192
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 193
    label "trasa"
  ]
  node [
    id 194
    label "przeorientowywanie"
  ]
  node [
    id 195
    label "przejazd"
  ]
  node [
    id 196
    label "kierunek"
  ]
  node [
    id 197
    label "przeorientowywa&#263;"
  ]
  node [
    id 198
    label "nauka"
  ]
  node [
    id 199
    label "przeorientowanie"
  ]
  node [
    id 200
    label "klasa"
  ]
  node [
    id 201
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 202
    label "przeorientowa&#263;"
  ]
  node [
    id 203
    label "manner"
  ]
  node [
    id 204
    label "course"
  ]
  node [
    id 205
    label "passage"
  ]
  node [
    id 206
    label "zni&#380;kowanie"
  ]
  node [
    id 207
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 208
    label "seria"
  ]
  node [
    id 209
    label "stawka"
  ]
  node [
    id 210
    label "way"
  ]
  node [
    id 211
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 212
    label "spos&#243;b"
  ]
  node [
    id 213
    label "deprecjacja"
  ]
  node [
    id 214
    label "cedu&#322;a"
  ]
  node [
    id 215
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 216
    label "drive"
  ]
  node [
    id 217
    label "bearing"
  ]
  node [
    id 218
    label "Lira"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 128
  ]
  edge [
    source 5
    target 129
  ]
  edge [
    source 5
    target 130
  ]
  edge [
    source 5
    target 131
  ]
  edge [
    source 5
    target 132
  ]
  edge [
    source 5
    target 133
  ]
  edge [
    source 5
    target 134
  ]
  edge [
    source 5
    target 135
  ]
  edge [
    source 5
    target 136
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 138
  ]
  edge [
    source 5
    target 139
  ]
  edge [
    source 5
    target 140
  ]
  edge [
    source 5
    target 141
  ]
  edge [
    source 5
    target 142
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 144
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 146
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 148
  ]
  edge [
    source 5
    target 149
  ]
  edge [
    source 5
    target 150
  ]
  edge [
    source 5
    target 151
  ]
  edge [
    source 5
    target 152
  ]
  edge [
    source 5
    target 153
  ]
  edge [
    source 5
    target 154
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 5
    target 156
  ]
  edge [
    source 5
    target 157
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 159
  ]
  edge [
    source 5
    target 160
  ]
  edge [
    source 5
    target 161
  ]
  edge [
    source 5
    target 162
  ]
  edge [
    source 5
    target 163
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 165
  ]
  edge [
    source 5
    target 166
  ]
  edge [
    source 5
    target 167
  ]
  edge [
    source 5
    target 168
  ]
  edge [
    source 5
    target 169
  ]
  edge [
    source 5
    target 170
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
]
