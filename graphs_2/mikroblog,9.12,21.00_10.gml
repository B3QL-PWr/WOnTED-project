graph [
  node [
    id 0
    label "dzienia"
    origin "text"
  ]
  node [
    id 1
    label "dobry"
    origin "text"
  ]
  node [
    id 2
    label "wykopki"
    origin "text"
  ]
  node [
    id 3
    label "dobroczynny"
  ]
  node [
    id 4
    label "czw&#243;rka"
  ]
  node [
    id 5
    label "spokojny"
  ]
  node [
    id 6
    label "skuteczny"
  ]
  node [
    id 7
    label "&#347;mieszny"
  ]
  node [
    id 8
    label "mi&#322;y"
  ]
  node [
    id 9
    label "grzeczny"
  ]
  node [
    id 10
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 11
    label "powitanie"
  ]
  node [
    id 12
    label "dobrze"
  ]
  node [
    id 13
    label "ca&#322;y"
  ]
  node [
    id 14
    label "zwrot"
  ]
  node [
    id 15
    label "pomy&#347;lny"
  ]
  node [
    id 16
    label "moralny"
  ]
  node [
    id 17
    label "drogi"
  ]
  node [
    id 18
    label "pozytywny"
  ]
  node [
    id 19
    label "odpowiedni"
  ]
  node [
    id 20
    label "korzystny"
  ]
  node [
    id 21
    label "pos&#322;uszny"
  ]
  node [
    id 22
    label "moralnie"
  ]
  node [
    id 23
    label "warto&#347;ciowy"
  ]
  node [
    id 24
    label "etycznie"
  ]
  node [
    id 25
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 26
    label "nale&#380;ny"
  ]
  node [
    id 27
    label "nale&#380;yty"
  ]
  node [
    id 28
    label "typowy"
  ]
  node [
    id 29
    label "uprawniony"
  ]
  node [
    id 30
    label "zasadniczy"
  ]
  node [
    id 31
    label "stosownie"
  ]
  node [
    id 32
    label "taki"
  ]
  node [
    id 33
    label "charakterystyczny"
  ]
  node [
    id 34
    label "prawdziwy"
  ]
  node [
    id 35
    label "ten"
  ]
  node [
    id 36
    label "pozytywnie"
  ]
  node [
    id 37
    label "fajny"
  ]
  node [
    id 38
    label "dodatnio"
  ]
  node [
    id 39
    label "przyjemny"
  ]
  node [
    id 40
    label "po&#380;&#261;dany"
  ]
  node [
    id 41
    label "niepowa&#380;ny"
  ]
  node [
    id 42
    label "o&#347;mieszanie"
  ]
  node [
    id 43
    label "&#347;miesznie"
  ]
  node [
    id 44
    label "bawny"
  ]
  node [
    id 45
    label "o&#347;mieszenie"
  ]
  node [
    id 46
    label "dziwny"
  ]
  node [
    id 47
    label "nieadekwatny"
  ]
  node [
    id 48
    label "zale&#380;ny"
  ]
  node [
    id 49
    label "uleg&#322;y"
  ]
  node [
    id 50
    label "pos&#322;usznie"
  ]
  node [
    id 51
    label "grzecznie"
  ]
  node [
    id 52
    label "stosowny"
  ]
  node [
    id 53
    label "niewinny"
  ]
  node [
    id 54
    label "konserwatywny"
  ]
  node [
    id 55
    label "nijaki"
  ]
  node [
    id 56
    label "wolny"
  ]
  node [
    id 57
    label "uspokajanie_si&#281;"
  ]
  node [
    id 58
    label "bezproblemowy"
  ]
  node [
    id 59
    label "spokojnie"
  ]
  node [
    id 60
    label "uspokojenie_si&#281;"
  ]
  node [
    id 61
    label "cicho"
  ]
  node [
    id 62
    label "uspokojenie"
  ]
  node [
    id 63
    label "niezak&#322;&#243;cony"
  ]
  node [
    id 64
    label "nietrudny"
  ]
  node [
    id 65
    label "uspokajanie"
  ]
  node [
    id 66
    label "korzystnie"
  ]
  node [
    id 67
    label "drogo"
  ]
  node [
    id 68
    label "cz&#322;owiek"
  ]
  node [
    id 69
    label "bliski"
  ]
  node [
    id 70
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 71
    label "przyjaciel"
  ]
  node [
    id 72
    label "jedyny"
  ]
  node [
    id 73
    label "du&#380;y"
  ]
  node [
    id 74
    label "zdr&#243;w"
  ]
  node [
    id 75
    label "calu&#347;ko"
  ]
  node [
    id 76
    label "kompletny"
  ]
  node [
    id 77
    label "&#380;ywy"
  ]
  node [
    id 78
    label "pe&#322;ny"
  ]
  node [
    id 79
    label "podobny"
  ]
  node [
    id 80
    label "ca&#322;o"
  ]
  node [
    id 81
    label "poskutkowanie"
  ]
  node [
    id 82
    label "sprawny"
  ]
  node [
    id 83
    label "skutecznie"
  ]
  node [
    id 84
    label "skutkowanie"
  ]
  node [
    id 85
    label "pomy&#347;lnie"
  ]
  node [
    id 86
    label "toto-lotek"
  ]
  node [
    id 87
    label "trafienie"
  ]
  node [
    id 88
    label "zbi&#243;r"
  ]
  node [
    id 89
    label "arkusz_drukarski"
  ]
  node [
    id 90
    label "&#322;&#243;dka"
  ]
  node [
    id 91
    label "four"
  ]
  node [
    id 92
    label "&#263;wiartka"
  ]
  node [
    id 93
    label "hotel"
  ]
  node [
    id 94
    label "cyfra"
  ]
  node [
    id 95
    label "pok&#243;j"
  ]
  node [
    id 96
    label "stopie&#324;"
  ]
  node [
    id 97
    label "obiekt"
  ]
  node [
    id 98
    label "minialbum"
  ]
  node [
    id 99
    label "osada"
  ]
  node [
    id 100
    label "p&#322;yta_winylowa"
  ]
  node [
    id 101
    label "blotka"
  ]
  node [
    id 102
    label "zaprz&#281;g"
  ]
  node [
    id 103
    label "przedtrzonowiec"
  ]
  node [
    id 104
    label "punkt"
  ]
  node [
    id 105
    label "turn"
  ]
  node [
    id 106
    label "turning"
  ]
  node [
    id 107
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 108
    label "p&#322;atno&#347;&#263;"
  ]
  node [
    id 109
    label "skr&#281;t"
  ]
  node [
    id 110
    label "obr&#243;t"
  ]
  node [
    id 111
    label "fraza_czasownikowa"
  ]
  node [
    id 112
    label "jednostka_leksykalna"
  ]
  node [
    id 113
    label "zmiana"
  ]
  node [
    id 114
    label "wyra&#380;enie"
  ]
  node [
    id 115
    label "welcome"
  ]
  node [
    id 116
    label "spotkanie"
  ]
  node [
    id 117
    label "pozdrowienie"
  ]
  node [
    id 118
    label "zwyczaj"
  ]
  node [
    id 119
    label "greeting"
  ]
  node [
    id 120
    label "zdarzony"
  ]
  node [
    id 121
    label "odpowiednio"
  ]
  node [
    id 122
    label "odpowiadanie"
  ]
  node [
    id 123
    label "specjalny"
  ]
  node [
    id 124
    label "kochanek"
  ]
  node [
    id 125
    label "sk&#322;onny"
  ]
  node [
    id 126
    label "wybranek"
  ]
  node [
    id 127
    label "umi&#322;owany"
  ]
  node [
    id 128
    label "przyjemnie"
  ]
  node [
    id 129
    label "mi&#322;o"
  ]
  node [
    id 130
    label "kochanie"
  ]
  node [
    id 131
    label "dyplomata"
  ]
  node [
    id 132
    label "dobroczynnie"
  ]
  node [
    id 133
    label "lepiej"
  ]
  node [
    id 134
    label "wiele"
  ]
  node [
    id 135
    label "spo&#322;eczny"
  ]
  node [
    id 136
    label "sadzeniak"
  ]
  node [
    id 137
    label "egzemplarz"
  ]
  node [
    id 138
    label "series"
  ]
  node [
    id 139
    label "rodzina_zbior&#243;w"
  ]
  node [
    id 140
    label "uprawianie"
  ]
  node [
    id 141
    label "praca_rolnicza"
  ]
  node [
    id 142
    label "collection"
  ]
  node [
    id 143
    label "dane"
  ]
  node [
    id 144
    label "ksi&#261;&#380;ka"
  ]
  node [
    id 145
    label "pakiet_klimatyczny"
  ]
  node [
    id 146
    label "poj&#281;cie"
  ]
  node [
    id 147
    label "ci&#261;g_zbior&#243;w"
  ]
  node [
    id 148
    label "sum"
  ]
  node [
    id 149
    label "gathering"
  ]
  node [
    id 150
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 151
    label "album"
  ]
  node [
    id 152
    label "ziemniak"
  ]
  node [
    id 153
    label "monstera"
  ]
  node [
    id 154
    label "hunter"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 153
    target 154
  ]
]
