graph [
  node [
    id 0
    label "fundacja"
    origin "text"
  ]
  node [
    id 1
    label "nowa"
    origin "text"
  ]
  node [
    id 2
    label "media"
    origin "text"
  ]
  node [
    id 3
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sesja"
    origin "text"
  ]
  node [
    id 5
    label "temat"
    origin "text"
  ]
  node [
    id 6
    label "rola"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "krzy&#380;"
    origin "text"
  ]
  node [
    id 9
    label "krakowskie"
    origin "text"
  ]
  node [
    id 10
    label "przedmie&#347;cie"
    origin "text"
  ]
  node [
    id 11
    label "darowizna"
  ]
  node [
    id 12
    label "foundation"
  ]
  node [
    id 13
    label "instytucja"
  ]
  node [
    id 14
    label "dar"
  ]
  node [
    id 15
    label "pocz&#261;tek"
  ]
  node [
    id 16
    label "osoba_prawna"
  ]
  node [
    id 17
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 18
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 19
    label "poj&#281;cie"
  ]
  node [
    id 20
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 21
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 22
    label "biuro"
  ]
  node [
    id 23
    label "organizacja"
  ]
  node [
    id 24
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 25
    label "Fundusze_Unijne"
  ]
  node [
    id 26
    label "zamyka&#263;"
  ]
  node [
    id 27
    label "establishment"
  ]
  node [
    id 28
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 29
    label "urz&#261;d"
  ]
  node [
    id 30
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 31
    label "afiliowa&#263;"
  ]
  node [
    id 32
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 33
    label "standard"
  ]
  node [
    id 34
    label "zamykanie"
  ]
  node [
    id 35
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 36
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 37
    label "przeniesienie_praw"
  ]
  node [
    id 38
    label "zapomoga"
  ]
  node [
    id 39
    label "transakcja"
  ]
  node [
    id 40
    label "pierworodztwo"
  ]
  node [
    id 41
    label "faza"
  ]
  node [
    id 42
    label "miejsce"
  ]
  node [
    id 43
    label "upgrade"
  ]
  node [
    id 44
    label "nast&#281;pstwo"
  ]
  node [
    id 45
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 46
    label "dyspozycja"
  ]
  node [
    id 47
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 48
    label "da&#324;"
  ]
  node [
    id 49
    label "faculty"
  ]
  node [
    id 50
    label "stygmat"
  ]
  node [
    id 51
    label "dobro"
  ]
  node [
    id 52
    label "rzecz"
  ]
  node [
    id 53
    label "&#322;atwo&#347;&#263;"
  ]
  node [
    id 54
    label "gwiazda"
  ]
  node [
    id 55
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 56
    label "Arktur"
  ]
  node [
    id 57
    label "kszta&#322;t"
  ]
  node [
    id 58
    label "Gwiazda_Polarna"
  ]
  node [
    id 59
    label "agregatka"
  ]
  node [
    id 60
    label "gromada"
  ]
  node [
    id 61
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 62
    label "S&#322;o&#324;ce"
  ]
  node [
    id 63
    label "Nibiru"
  ]
  node [
    id 64
    label "konstelacja"
  ]
  node [
    id 65
    label "ornament"
  ]
  node [
    id 66
    label "delta_Scuti"
  ]
  node [
    id 67
    label "&#347;wiat&#322;o"
  ]
  node [
    id 68
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 69
    label "obiekt"
  ]
  node [
    id 70
    label "s&#322;awa"
  ]
  node [
    id 71
    label "promie&#324;"
  ]
  node [
    id 72
    label "star"
  ]
  node [
    id 73
    label "gwiazdosz"
  ]
  node [
    id 74
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 75
    label "asocjacja_gwiazd"
  ]
  node [
    id 76
    label "supergrupa"
  ]
  node [
    id 77
    label "mass-media"
  ]
  node [
    id 78
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 79
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 80
    label "przekazior"
  ]
  node [
    id 81
    label "uzbrajanie"
  ]
  node [
    id 82
    label "medium"
  ]
  node [
    id 83
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 84
    label "&#347;rodek"
  ]
  node [
    id 85
    label "jasnowidz"
  ]
  node [
    id 86
    label "hipnoza"
  ]
  node [
    id 87
    label "cz&#322;owiek"
  ]
  node [
    id 88
    label "spirytysta"
  ]
  node [
    id 89
    label "otoczenie"
  ]
  node [
    id 90
    label "publikator"
  ]
  node [
    id 91
    label "warunki"
  ]
  node [
    id 92
    label "strona"
  ]
  node [
    id 93
    label "przeka&#378;nik"
  ]
  node [
    id 94
    label "&#347;rodek_przekazu"
  ]
  node [
    id 95
    label "armament"
  ]
  node [
    id 96
    label "arming"
  ]
  node [
    id 97
    label "instalacja"
  ]
  node [
    id 98
    label "wyposa&#380;anie"
  ]
  node [
    id 99
    label "dozbrajanie"
  ]
  node [
    id 100
    label "dozbrojenie"
  ]
  node [
    id 101
    label "montowanie"
  ]
  node [
    id 102
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 103
    label "invite"
  ]
  node [
    id 104
    label "ask"
  ]
  node [
    id 105
    label "oferowa&#263;"
  ]
  node [
    id 106
    label "zach&#281;ca&#263;"
  ]
  node [
    id 107
    label "volunteer"
  ]
  node [
    id 108
    label "egzamin"
  ]
  node [
    id 109
    label "dzie&#324;_pracy"
  ]
  node [
    id 110
    label "dogrywka"
  ]
  node [
    id 111
    label "spotkanie"
  ]
  node [
    id 112
    label "seria"
  ]
  node [
    id 113
    label "rok_akademicki"
  ]
  node [
    id 114
    label "konsylium"
  ]
  node [
    id 115
    label "psychoterapia"
  ]
  node [
    id 116
    label "sesyjka"
  ]
  node [
    id 117
    label "conference"
  ]
  node [
    id 118
    label "dyskusja"
  ]
  node [
    id 119
    label "gie&#322;da_papier&#243;w_warto&#347;ciowych"
  ]
  node [
    id 120
    label "doznanie"
  ]
  node [
    id 121
    label "gathering"
  ]
  node [
    id 122
    label "zawarcie"
  ]
  node [
    id 123
    label "wydarzenie"
  ]
  node [
    id 124
    label "znajomy"
  ]
  node [
    id 125
    label "powitanie"
  ]
  node [
    id 126
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 127
    label "spowodowanie"
  ]
  node [
    id 128
    label "zdarzenie_si&#281;"
  ]
  node [
    id 129
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 130
    label "znalezienie"
  ]
  node [
    id 131
    label "match"
  ]
  node [
    id 132
    label "employment"
  ]
  node [
    id 133
    label "po&#380;egnanie"
  ]
  node [
    id 134
    label "gather"
  ]
  node [
    id 135
    label "spotykanie"
  ]
  node [
    id 136
    label "spotkanie_si&#281;"
  ]
  node [
    id 137
    label "co&#347;"
  ]
  node [
    id 138
    label "budynek"
  ]
  node [
    id 139
    label "thing"
  ]
  node [
    id 140
    label "program"
  ]
  node [
    id 141
    label "set"
  ]
  node [
    id 142
    label "przebieg"
  ]
  node [
    id 143
    label "zbi&#243;r"
  ]
  node [
    id 144
    label "jednostka"
  ]
  node [
    id 145
    label "jednostka_systematyczna"
  ]
  node [
    id 146
    label "stage_set"
  ]
  node [
    id 147
    label "przekr&#243;j_geologiczny"
  ]
  node [
    id 148
    label "d&#378;wi&#281;k"
  ]
  node [
    id 149
    label "komplet"
  ]
  node [
    id 150
    label "line"
  ]
  node [
    id 151
    label "sekwencja"
  ]
  node [
    id 152
    label "zestawienie"
  ]
  node [
    id 153
    label "partia"
  ]
  node [
    id 154
    label "produkcja"
  ]
  node [
    id 155
    label "rozmowa"
  ]
  node [
    id 156
    label "sympozjon"
  ]
  node [
    id 157
    label "terapia"
  ]
  node [
    id 158
    label "oblewanie"
  ]
  node [
    id 159
    label "sesja_egzaminacyjna"
  ]
  node [
    id 160
    label "oblewa&#263;"
  ]
  node [
    id 161
    label "praca_pisemna"
  ]
  node [
    id 162
    label "sprawdzian"
  ]
  node [
    id 163
    label "magiel"
  ]
  node [
    id 164
    label "pr&#243;ba"
  ]
  node [
    id 165
    label "arkusz"
  ]
  node [
    id 166
    label "examination"
  ]
  node [
    id 167
    label "dodatek"
  ]
  node [
    id 168
    label "rozgrywka"
  ]
  node [
    id 169
    label "konsultacja"
  ]
  node [
    id 170
    label "obrady"
  ]
  node [
    id 171
    label "wyraz_pochodny"
  ]
  node [
    id 172
    label "zboczenie"
  ]
  node [
    id 173
    label "om&#243;wienie"
  ]
  node [
    id 174
    label "cecha"
  ]
  node [
    id 175
    label "omawia&#263;"
  ]
  node [
    id 176
    label "fraza"
  ]
  node [
    id 177
    label "tre&#347;&#263;"
  ]
  node [
    id 178
    label "entity"
  ]
  node [
    id 179
    label "forum"
  ]
  node [
    id 180
    label "topik"
  ]
  node [
    id 181
    label "tematyka"
  ]
  node [
    id 182
    label "w&#261;tek"
  ]
  node [
    id 183
    label "zbaczanie"
  ]
  node [
    id 184
    label "forma"
  ]
  node [
    id 185
    label "om&#243;wi&#263;"
  ]
  node [
    id 186
    label "omawianie"
  ]
  node [
    id 187
    label "melodia"
  ]
  node [
    id 188
    label "otoczka"
  ]
  node [
    id 189
    label "istota"
  ]
  node [
    id 190
    label "zbacza&#263;"
  ]
  node [
    id 191
    label "zboczy&#263;"
  ]
  node [
    id 192
    label "konstrukcja_sk&#322;adniowa"
  ]
  node [
    id 193
    label "wypowiedzenie"
  ]
  node [
    id 194
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 195
    label "zdanie"
  ]
  node [
    id 196
    label "zwi&#261;zek_wyrazowy"
  ]
  node [
    id 197
    label "motyw"
  ]
  node [
    id 198
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 199
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 200
    label "informacja"
  ]
  node [
    id 201
    label "zawarto&#347;&#263;"
  ]
  node [
    id 202
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 203
    label "poznanie"
  ]
  node [
    id 204
    label "leksem"
  ]
  node [
    id 205
    label "dzie&#322;o"
  ]
  node [
    id 206
    label "stan"
  ]
  node [
    id 207
    label "blaszka"
  ]
  node [
    id 208
    label "kantyzm"
  ]
  node [
    id 209
    label "zdolno&#347;&#263;"
  ]
  node [
    id 210
    label "do&#322;ek"
  ]
  node [
    id 211
    label "formality"
  ]
  node [
    id 212
    label "struktura"
  ]
  node [
    id 213
    label "wygl&#261;d"
  ]
  node [
    id 214
    label "mode"
  ]
  node [
    id 215
    label "morfem"
  ]
  node [
    id 216
    label "rdze&#324;"
  ]
  node [
    id 217
    label "posta&#263;"
  ]
  node [
    id 218
    label "kielich"
  ]
  node [
    id 219
    label "ornamentyka"
  ]
  node [
    id 220
    label "pasmo"
  ]
  node [
    id 221
    label "zwyczaj"
  ]
  node [
    id 222
    label "punkt_widzenia"
  ]
  node [
    id 223
    label "g&#322;owa"
  ]
  node [
    id 224
    label "naczynie"
  ]
  node [
    id 225
    label "p&#322;at"
  ]
  node [
    id 226
    label "maszyna_drukarska"
  ]
  node [
    id 227
    label "style"
  ]
  node [
    id 228
    label "linearno&#347;&#263;"
  ]
  node [
    id 229
    label "wyra&#380;enie"
  ]
  node [
    id 230
    label "formacja"
  ]
  node [
    id 231
    label "spirala"
  ]
  node [
    id 232
    label "odmiana"
  ]
  node [
    id 233
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 234
    label "wz&#243;r"
  ]
  node [
    id 235
    label "October"
  ]
  node [
    id 236
    label "creation"
  ]
  node [
    id 237
    label "p&#281;tla"
  ]
  node [
    id 238
    label "arystotelizm"
  ]
  node [
    id 239
    label "szablon"
  ]
  node [
    id 240
    label "miniatura"
  ]
  node [
    id 241
    label "zanucenie"
  ]
  node [
    id 242
    label "nuta"
  ]
  node [
    id 243
    label "zakosztowa&#263;"
  ]
  node [
    id 244
    label "zajawka"
  ]
  node [
    id 245
    label "zanuci&#263;"
  ]
  node [
    id 246
    label "emocja"
  ]
  node [
    id 247
    label "oskoma"
  ]
  node [
    id 248
    label "melika"
  ]
  node [
    id 249
    label "nucenie"
  ]
  node [
    id 250
    label "nuci&#263;"
  ]
  node [
    id 251
    label "brzmienie"
  ]
  node [
    id 252
    label "zjawisko"
  ]
  node [
    id 253
    label "taste"
  ]
  node [
    id 254
    label "muzyka"
  ]
  node [
    id 255
    label "inclination"
  ]
  node [
    id 256
    label "charakterystyka"
  ]
  node [
    id 257
    label "m&#322;ot"
  ]
  node [
    id 258
    label "znak"
  ]
  node [
    id 259
    label "drzewo"
  ]
  node [
    id 260
    label "attribute"
  ]
  node [
    id 261
    label "marka"
  ]
  node [
    id 262
    label "mentalno&#347;&#263;"
  ]
  node [
    id 263
    label "superego"
  ]
  node [
    id 264
    label "psychika"
  ]
  node [
    id 265
    label "znaczenie"
  ]
  node [
    id 266
    label "wn&#281;trze"
  ]
  node [
    id 267
    label "charakter"
  ]
  node [
    id 268
    label "matter"
  ]
  node [
    id 269
    label "splot"
  ]
  node [
    id 270
    label "wytw&#243;r"
  ]
  node [
    id 271
    label "ceg&#322;a"
  ]
  node [
    id 272
    label "socket"
  ]
  node [
    id 273
    label "rozmieszczenie"
  ]
  node [
    id 274
    label "fabu&#322;a"
  ]
  node [
    id 275
    label "okrywa"
  ]
  node [
    id 276
    label "kontekst"
  ]
  node [
    id 277
    label "object"
  ]
  node [
    id 278
    label "przedmiot"
  ]
  node [
    id 279
    label "wpadni&#281;cie"
  ]
  node [
    id 280
    label "mienie"
  ]
  node [
    id 281
    label "przyroda"
  ]
  node [
    id 282
    label "kultura"
  ]
  node [
    id 283
    label "wpa&#347;&#263;"
  ]
  node [
    id 284
    label "wpadanie"
  ]
  node [
    id 285
    label "wpada&#263;"
  ]
  node [
    id 286
    label "discussion"
  ]
  node [
    id 287
    label "rozpatrywanie"
  ]
  node [
    id 288
    label "dyskutowanie"
  ]
  node [
    id 289
    label "omowny"
  ]
  node [
    id 290
    label "figura_stylistyczna"
  ]
  node [
    id 291
    label "sformu&#322;owanie"
  ]
  node [
    id 292
    label "przemieszczanie_si&#281;"
  ]
  node [
    id 293
    label "odchodzenie"
  ]
  node [
    id 294
    label "aberrance"
  ]
  node [
    id 295
    label "swerve"
  ]
  node [
    id 296
    label "kierunek"
  ]
  node [
    id 297
    label "odchyli&#263;_si&#281;"
  ]
  node [
    id 298
    label "distract"
  ]
  node [
    id 299
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 300
    label "odej&#347;&#263;"
  ]
  node [
    id 301
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 302
    label "twist"
  ]
  node [
    id 303
    label "zmieni&#263;"
  ]
  node [
    id 304
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 305
    label "przekrzywi&#263;_si&#281;"
  ]
  node [
    id 306
    label "przedyskutowa&#263;"
  ]
  node [
    id 307
    label "sformu&#322;owa&#263;"
  ]
  node [
    id 308
    label "publicize"
  ]
  node [
    id 309
    label "digress"
  ]
  node [
    id 310
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 311
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 312
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 313
    label "odchodzi&#263;"
  ]
  node [
    id 314
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 315
    label "przekrzywia&#263;_si&#281;"
  ]
  node [
    id 316
    label "odchyla&#263;_si&#281;"
  ]
  node [
    id 317
    label "perversion"
  ]
  node [
    id 318
    label "death"
  ]
  node [
    id 319
    label "odej&#347;cie"
  ]
  node [
    id 320
    label "turn"
  ]
  node [
    id 321
    label "k&#261;t"
  ]
  node [
    id 322
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 323
    label "odchylenie_si&#281;"
  ]
  node [
    id 324
    label "deviation"
  ]
  node [
    id 325
    label "patologia"
  ]
  node [
    id 326
    label "dyskutowa&#263;"
  ]
  node [
    id 327
    label "formu&#322;owa&#263;"
  ]
  node [
    id 328
    label "discourse"
  ]
  node [
    id 329
    label "kognicja"
  ]
  node [
    id 330
    label "rozprawa"
  ]
  node [
    id 331
    label "szczeg&#243;&#322;"
  ]
  node [
    id 332
    label "proposition"
  ]
  node [
    id 333
    label "przes&#322;anka"
  ]
  node [
    id 334
    label "idea"
  ]
  node [
    id 335
    label "paj&#261;k"
  ]
  node [
    id 336
    label "przewodnik"
  ]
  node [
    id 337
    label "odcinek"
  ]
  node [
    id 338
    label "topikowate"
  ]
  node [
    id 339
    label "grupa_dyskusyjna"
  ]
  node [
    id 340
    label "s&#261;d"
  ]
  node [
    id 341
    label "plac"
  ]
  node [
    id 342
    label "bazylika"
  ]
  node [
    id 343
    label "przestrze&#324;"
  ]
  node [
    id 344
    label "portal"
  ]
  node [
    id 345
    label "konferencja"
  ]
  node [
    id 346
    label "agora"
  ]
  node [
    id 347
    label "grupa"
  ]
  node [
    id 348
    label "uprawienie"
  ]
  node [
    id 349
    label "dialog"
  ]
  node [
    id 350
    label "p&#322;osa"
  ]
  node [
    id 351
    label "wykonywanie"
  ]
  node [
    id 352
    label "plik"
  ]
  node [
    id 353
    label "ziemia"
  ]
  node [
    id 354
    label "wykonywa&#263;"
  ]
  node [
    id 355
    label "czyn"
  ]
  node [
    id 356
    label "ustawienie"
  ]
  node [
    id 357
    label "scenariusz"
  ]
  node [
    id 358
    label "pole"
  ]
  node [
    id 359
    label "gospodarstwo"
  ]
  node [
    id 360
    label "uprawi&#263;"
  ]
  node [
    id 361
    label "function"
  ]
  node [
    id 362
    label "zreinterpretowa&#263;"
  ]
  node [
    id 363
    label "zastosowanie"
  ]
  node [
    id 364
    label "reinterpretowa&#263;"
  ]
  node [
    id 365
    label "wrench"
  ]
  node [
    id 366
    label "irygowanie"
  ]
  node [
    id 367
    label "ustawi&#263;"
  ]
  node [
    id 368
    label "irygowa&#263;"
  ]
  node [
    id 369
    label "zreinterpretowanie"
  ]
  node [
    id 370
    label "cel"
  ]
  node [
    id 371
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 372
    label "gra&#263;"
  ]
  node [
    id 373
    label "aktorstwo"
  ]
  node [
    id 374
    label "kostium"
  ]
  node [
    id 375
    label "zagon"
  ]
  node [
    id 376
    label "zagra&#263;"
  ]
  node [
    id 377
    label "reinterpretowanie"
  ]
  node [
    id 378
    label "sk&#322;ad"
  ]
  node [
    id 379
    label "tekst"
  ]
  node [
    id 380
    label "zagranie"
  ]
  node [
    id 381
    label "radlina"
  ]
  node [
    id 382
    label "granie"
  ]
  node [
    id 383
    label "comeliness"
  ]
  node [
    id 384
    label "face"
  ]
  node [
    id 385
    label "podkatalog"
  ]
  node [
    id 386
    label "nadpisa&#263;"
  ]
  node [
    id 387
    label "nadpisanie"
  ]
  node [
    id 388
    label "bundle"
  ]
  node [
    id 389
    label "folder"
  ]
  node [
    id 390
    label "nadpisywanie"
  ]
  node [
    id 391
    label "paczka"
  ]
  node [
    id 392
    label "nadpisywa&#263;"
  ]
  node [
    id 393
    label "dokument"
  ]
  node [
    id 394
    label "menad&#380;er_plik&#243;w"
  ]
  node [
    id 395
    label "zaistnie&#263;"
  ]
  node [
    id 396
    label "Osjan"
  ]
  node [
    id 397
    label "kto&#347;"
  ]
  node [
    id 398
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 399
    label "osobowo&#347;&#263;"
  ]
  node [
    id 400
    label "trim"
  ]
  node [
    id 401
    label "poby&#263;"
  ]
  node [
    id 402
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 403
    label "Aspazja"
  ]
  node [
    id 404
    label "kompleksja"
  ]
  node [
    id 405
    label "wytrzyma&#263;"
  ]
  node [
    id 406
    label "budowa"
  ]
  node [
    id 407
    label "pozosta&#263;"
  ]
  node [
    id 408
    label "point"
  ]
  node [
    id 409
    label "przedstawienie"
  ]
  node [
    id 410
    label "go&#347;&#263;"
  ]
  node [
    id 411
    label "ekscerpcja"
  ]
  node [
    id 412
    label "j&#281;zykowo"
  ]
  node [
    id 413
    label "wypowied&#378;"
  ]
  node [
    id 414
    label "redakcja"
  ]
  node [
    id 415
    label "pomini&#281;cie"
  ]
  node [
    id 416
    label "preparacja"
  ]
  node [
    id 417
    label "odmianka"
  ]
  node [
    id 418
    label "opu&#347;ci&#263;"
  ]
  node [
    id 419
    label "koniektura"
  ]
  node [
    id 420
    label "pisa&#263;"
  ]
  node [
    id 421
    label "obelga"
  ]
  node [
    id 422
    label "punkt"
  ]
  node [
    id 423
    label "przysz&#322;o&#347;&#263;"
  ]
  node [
    id 424
    label "rezultat"
  ]
  node [
    id 425
    label "s&#322;u&#380;y&#263;"
  ]
  node [
    id 426
    label "odk&#322;adanie"
  ]
  node [
    id 427
    label "condition"
  ]
  node [
    id 428
    label "liczenie"
  ]
  node [
    id 429
    label "stawianie"
  ]
  node [
    id 430
    label "bycie"
  ]
  node [
    id 431
    label "assay"
  ]
  node [
    id 432
    label "wskazywanie"
  ]
  node [
    id 433
    label "wyraz"
  ]
  node [
    id 434
    label "gravity"
  ]
  node [
    id 435
    label "weight"
  ]
  node [
    id 436
    label "command"
  ]
  node [
    id 437
    label "odgrywanie_roli"
  ]
  node [
    id 438
    label "okre&#347;lanie"
  ]
  node [
    id 439
    label "u&#322;o&#380;enie"
  ]
  node [
    id 440
    label "t&#322;o"
  ]
  node [
    id 441
    label "rozgrywaj&#261;cy"
  ]
  node [
    id 442
    label "room"
  ]
  node [
    id 443
    label "dw&#243;r"
  ]
  node [
    id 444
    label "okazja"
  ]
  node [
    id 445
    label "rozmiar"
  ]
  node [
    id 446
    label "compass"
  ]
  node [
    id 447
    label "square"
  ]
  node [
    id 448
    label "zmienna"
  ]
  node [
    id 449
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 450
    label "socjologia"
  ]
  node [
    id 451
    label "boisko"
  ]
  node [
    id 452
    label "dziedzina"
  ]
  node [
    id 453
    label "baza_danych"
  ]
  node [
    id 454
    label "region"
  ]
  node [
    id 455
    label "obszar"
  ]
  node [
    id 456
    label "powierzchnia"
  ]
  node [
    id 457
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 458
    label "plane"
  ]
  node [
    id 459
    label "Mazowsze"
  ]
  node [
    id 460
    label "Anglia"
  ]
  node [
    id 461
    label "Amazonia"
  ]
  node [
    id 462
    label "Bordeaux"
  ]
  node [
    id 463
    label "Naddniestrze"
  ]
  node [
    id 464
    label "plantowa&#263;"
  ]
  node [
    id 465
    label "Europa_Zachodnia"
  ]
  node [
    id 466
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 467
    label "Armagnac"
  ]
  node [
    id 468
    label "zapadnia"
  ]
  node [
    id 469
    label "Zamojszczyzna"
  ]
  node [
    id 470
    label "Amhara"
  ]
  node [
    id 471
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 472
    label "skorupa_ziemska"
  ]
  node [
    id 473
    label "Ma&#322;opolska"
  ]
  node [
    id 474
    label "Turkiestan"
  ]
  node [
    id 475
    label "Noworosja"
  ]
  node [
    id 476
    label "Mezoameryka"
  ]
  node [
    id 477
    label "glinowanie"
  ]
  node [
    id 478
    label "Lubelszczyzna"
  ]
  node [
    id 479
    label "Ba&#322;kany"
  ]
  node [
    id 480
    label "Kurdystan"
  ]
  node [
    id 481
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 482
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 483
    label "martwica"
  ]
  node [
    id 484
    label "Baszkiria"
  ]
  node [
    id 485
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 486
    label "Szkocja"
  ]
  node [
    id 487
    label "Tonkin"
  ]
  node [
    id 488
    label "Maghreb"
  ]
  node [
    id 489
    label "teren"
  ]
  node [
    id 490
    label "litosfera"
  ]
  node [
    id 491
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 492
    label "penetrator"
  ]
  node [
    id 493
    label "Nadrenia"
  ]
  node [
    id 494
    label "glinowa&#263;"
  ]
  node [
    id 495
    label "Wielkopolska"
  ]
  node [
    id 496
    label "Zabajkale"
  ]
  node [
    id 497
    label "Apulia"
  ]
  node [
    id 498
    label "domain"
  ]
  node [
    id 499
    label "Bojkowszczyzna"
  ]
  node [
    id 500
    label "podglebie"
  ]
  node [
    id 501
    label "kompleks_sorpcyjny"
  ]
  node [
    id 502
    label "Liguria"
  ]
  node [
    id 503
    label "Pamir"
  ]
  node [
    id 504
    label "Indochiny"
  ]
  node [
    id 505
    label "Podlasie"
  ]
  node [
    id 506
    label "Polinezja"
  ]
  node [
    id 507
    label "Kurpie"
  ]
  node [
    id 508
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 509
    label "S&#261;decczyzna"
  ]
  node [
    id 510
    label "Umbria"
  ]
  node [
    id 511
    label "Karaiby"
  ]
  node [
    id 512
    label "Ukraina_Zachodnia"
  ]
  node [
    id 513
    label "Kielecczyzna"
  ]
  node [
    id 514
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 515
    label "kort"
  ]
  node [
    id 516
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 517
    label "czynnik_produkcji"
  ]
  node [
    id 518
    label "Skandynawia"
  ]
  node [
    id 519
    label "Kujawy"
  ]
  node [
    id 520
    label "Tyrol"
  ]
  node [
    id 521
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 522
    label "Huculszczyzna"
  ]
  node [
    id 523
    label "pojazd"
  ]
  node [
    id 524
    label "Turyngia"
  ]
  node [
    id 525
    label "jednostka_administracyjna"
  ]
  node [
    id 526
    label "Toskania"
  ]
  node [
    id 527
    label "Podhale"
  ]
  node [
    id 528
    label "Bory_Tucholskie"
  ]
  node [
    id 529
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 530
    label "Kalabria"
  ]
  node [
    id 531
    label "pr&#243;chnica"
  ]
  node [
    id 532
    label "Hercegowina"
  ]
  node [
    id 533
    label "Lotaryngia"
  ]
  node [
    id 534
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 535
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 536
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 537
    label "Walia"
  ]
  node [
    id 538
    label "pomieszczenie"
  ]
  node [
    id 539
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 540
    label "Opolskie"
  ]
  node [
    id 541
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 542
    label "Kampania"
  ]
  node [
    id 543
    label "Sand&#380;ak"
  ]
  node [
    id 544
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 545
    label "Syjon"
  ]
  node [
    id 546
    label "Kabylia"
  ]
  node [
    id 547
    label "ryzosfera"
  ]
  node [
    id 548
    label "Lombardia"
  ]
  node [
    id 549
    label "Warmia"
  ]
  node [
    id 550
    label "Kaszmir"
  ]
  node [
    id 551
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 552
    label "&#321;&#243;dzkie"
  ]
  node [
    id 553
    label "Kaukaz"
  ]
  node [
    id 554
    label "Europa_Wschodnia"
  ]
  node [
    id 555
    label "Biskupizna"
  ]
  node [
    id 556
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 557
    label "Afryka_Wschodnia"
  ]
  node [
    id 558
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 559
    label "Podkarpacie"
  ]
  node [
    id 560
    label "Afryka_Zachodnia"
  ]
  node [
    id 561
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 562
    label "Bo&#347;nia"
  ]
  node [
    id 563
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 564
    label "p&#322;aszczyzna"
  ]
  node [
    id 565
    label "dotleni&#263;"
  ]
  node [
    id 566
    label "Oceania"
  ]
  node [
    id 567
    label "Pomorze_Zachodnie"
  ]
  node [
    id 568
    label "Powi&#347;le"
  ]
  node [
    id 569
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 570
    label "Podbeskidzie"
  ]
  node [
    id 571
    label "&#321;emkowszczyzna"
  ]
  node [
    id 572
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 573
    label "Opolszczyzna"
  ]
  node [
    id 574
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 575
    label "Kaszuby"
  ]
  node [
    id 576
    label "Ko&#322;yma"
  ]
  node [
    id 577
    label "Szlezwik"
  ]
  node [
    id 578
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 579
    label "glej"
  ]
  node [
    id 580
    label "Mikronezja"
  ]
  node [
    id 581
    label "pa&#324;stwo"
  ]
  node [
    id 582
    label "posadzka"
  ]
  node [
    id 583
    label "Polesie"
  ]
  node [
    id 584
    label "Kerala"
  ]
  node [
    id 585
    label "Mazury"
  ]
  node [
    id 586
    label "Palestyna"
  ]
  node [
    id 587
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 588
    label "Lauda"
  ]
  node [
    id 589
    label "Azja_Wschodnia"
  ]
  node [
    id 590
    label "Galicja"
  ]
  node [
    id 591
    label "Zakarpacie"
  ]
  node [
    id 592
    label "Lubuskie"
  ]
  node [
    id 593
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 594
    label "Laponia"
  ]
  node [
    id 595
    label "Yorkshire"
  ]
  node [
    id 596
    label "Bawaria"
  ]
  node [
    id 597
    label "Zag&#243;rze"
  ]
  node [
    id 598
    label "geosystem"
  ]
  node [
    id 599
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 600
    label "Andaluzja"
  ]
  node [
    id 601
    label "&#379;ywiecczyzna"
  ]
  node [
    id 602
    label "Oksytania"
  ]
  node [
    id 603
    label "Kociewie"
  ]
  node [
    id 604
    label "Lasko"
  ]
  node [
    id 605
    label "po&#322;o&#380;enie"
  ]
  node [
    id 606
    label "g&#322;&#243;wno&#347;&#263;"
  ]
  node [
    id 607
    label "play"
  ]
  node [
    id 608
    label "zabrzmie&#263;"
  ]
  node [
    id 609
    label "leave"
  ]
  node [
    id 610
    label "instrument_muzyczny"
  ]
  node [
    id 611
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 612
    label "flare"
  ]
  node [
    id 613
    label "rozegra&#263;"
  ]
  node [
    id 614
    label "zrobi&#263;"
  ]
  node [
    id 615
    label "zaszczeka&#263;"
  ]
  node [
    id 616
    label "sound"
  ]
  node [
    id 617
    label "represent"
  ]
  node [
    id 618
    label "wykorzysta&#263;"
  ]
  node [
    id 619
    label "zatokowa&#263;"
  ]
  node [
    id 620
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 621
    label "uda&#263;_si&#281;"
  ]
  node [
    id 622
    label "zacz&#261;&#263;"
  ]
  node [
    id 623
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 624
    label "wykona&#263;"
  ]
  node [
    id 625
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 626
    label "typify"
  ]
  node [
    id 627
    label "ustalenie"
  ]
  node [
    id 628
    label "erection"
  ]
  node [
    id 629
    label "setup"
  ]
  node [
    id 630
    label "erecting"
  ]
  node [
    id 631
    label "poustawianie"
  ]
  node [
    id 632
    label "zinterpretowanie"
  ]
  node [
    id 633
    label "porozstawianie"
  ]
  node [
    id 634
    label "czynno&#347;&#263;"
  ]
  node [
    id 635
    label "roz&#322;o&#380;enie"
  ]
  node [
    id 636
    label "interpretowa&#263;"
  ]
  node [
    id 637
    label "move"
  ]
  node [
    id 638
    label "zawa&#380;enie"
  ]
  node [
    id 639
    label "za&#347;wiecenie"
  ]
  node [
    id 640
    label "zaszczekanie"
  ]
  node [
    id 641
    label "myk"
  ]
  node [
    id 642
    label "wzi&#281;cie_udzia&#322;u"
  ]
  node [
    id 643
    label "wykonanie"
  ]
  node [
    id 644
    label "rozegranie"
  ]
  node [
    id 645
    label "travel"
  ]
  node [
    id 646
    label "gra"
  ]
  node [
    id 647
    label "uzewn&#281;trznienie_si&#281;"
  ]
  node [
    id 648
    label "gra_w_karty"
  ]
  node [
    id 649
    label "maneuver"
  ]
  node [
    id 650
    label "accident"
  ]
  node [
    id 651
    label "gambit"
  ]
  node [
    id 652
    label "zabrzmienie"
  ]
  node [
    id 653
    label "zachowanie_si&#281;"
  ]
  node [
    id 654
    label "manewr"
  ]
  node [
    id 655
    label "wyst&#261;pienie"
  ]
  node [
    id 656
    label "posuni&#281;cie"
  ]
  node [
    id 657
    label "udanie_si&#281;"
  ]
  node [
    id 658
    label "zacz&#281;cie"
  ]
  node [
    id 659
    label "zrobienie"
  ]
  node [
    id 660
    label "poprawi&#263;"
  ]
  node [
    id 661
    label "nada&#263;"
  ]
  node [
    id 662
    label "peddle"
  ]
  node [
    id 663
    label "marshal"
  ]
  node [
    id 664
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 665
    label "wyznaczy&#263;"
  ]
  node [
    id 666
    label "stanowisko"
  ]
  node [
    id 667
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 668
    label "spowodowa&#263;"
  ]
  node [
    id 669
    label "zabezpieczy&#263;"
  ]
  node [
    id 670
    label "umie&#347;ci&#263;"
  ]
  node [
    id 671
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 672
    label "zinterpretowa&#263;"
  ]
  node [
    id 673
    label "wskaza&#263;"
  ]
  node [
    id 674
    label "przyzna&#263;"
  ]
  node [
    id 675
    label "sk&#322;oni&#263;"
  ]
  node [
    id 676
    label "uporz&#261;dkowa&#263;"
  ]
  node [
    id 677
    label "pouk&#322;ada&#263;"
  ]
  node [
    id 678
    label "zdecydowa&#263;"
  ]
  node [
    id 679
    label "accommodate"
  ]
  node [
    id 680
    label "ustali&#263;"
  ]
  node [
    id 681
    label "situate"
  ]
  node [
    id 682
    label "sztuka_performatywna"
  ]
  node [
    id 683
    label "zaw&#243;d"
  ]
  node [
    id 684
    label "pr&#243;bowanie"
  ]
  node [
    id 685
    label "instrumentalizacja"
  ]
  node [
    id 686
    label "uzewn&#281;trznianie_si&#281;"
  ]
  node [
    id 687
    label "pasowanie"
  ]
  node [
    id 688
    label "staranie_si&#281;"
  ]
  node [
    id 689
    label "wybijanie"
  ]
  node [
    id 690
    label "odegranie_si&#281;"
  ]
  node [
    id 691
    label "dogrywanie"
  ]
  node [
    id 692
    label "rozgrywanie"
  ]
  node [
    id 693
    label "grywanie"
  ]
  node [
    id 694
    label "przygrywanie"
  ]
  node [
    id 695
    label "lewa"
  ]
  node [
    id 696
    label "wyst&#281;powanie"
  ]
  node [
    id 697
    label "robienie"
  ]
  node [
    id 698
    label "uderzenie"
  ]
  node [
    id 699
    label "zwalczenie"
  ]
  node [
    id 700
    label "mienienie_si&#281;"
  ]
  node [
    id 701
    label "wydawanie"
  ]
  node [
    id 702
    label "pretense"
  ]
  node [
    id 703
    label "prezentowanie"
  ]
  node [
    id 704
    label "na&#347;ladowanie"
  ]
  node [
    id 705
    label "dogranie"
  ]
  node [
    id 706
    label "wybicie"
  ]
  node [
    id 707
    label "playing"
  ]
  node [
    id 708
    label "rozegranie_si&#281;"
  ]
  node [
    id 709
    label "ust&#281;powanie"
  ]
  node [
    id 710
    label "dzianie_si&#281;"
  ]
  node [
    id 711
    label "otwarcie"
  ]
  node [
    id 712
    label "glitter"
  ]
  node [
    id 713
    label "igranie"
  ]
  node [
    id 714
    label "odgrywanie_si&#281;"
  ]
  node [
    id 715
    label "pogranie"
  ]
  node [
    id 716
    label "wyr&#243;wnywanie"
  ]
  node [
    id 717
    label "szczekanie"
  ]
  node [
    id 718
    label "przedstawianie"
  ]
  node [
    id 719
    label "wyr&#243;wnanie"
  ]
  node [
    id 720
    label "nagranie_si&#281;"
  ]
  node [
    id 721
    label "migotanie"
  ]
  node [
    id 722
    label "&#347;ciganie"
  ]
  node [
    id 723
    label "interpretowanie"
  ]
  node [
    id 724
    label "&#347;wieci&#263;"
  ]
  node [
    id 725
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 726
    label "muzykowa&#263;"
  ]
  node [
    id 727
    label "robi&#263;"
  ]
  node [
    id 728
    label "majaczy&#263;"
  ]
  node [
    id 729
    label "szczeka&#263;"
  ]
  node [
    id 730
    label "napierdziela&#263;"
  ]
  node [
    id 731
    label "dzia&#322;a&#263;"
  ]
  node [
    id 732
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 733
    label "pasowa&#263;"
  ]
  node [
    id 734
    label "dally"
  ]
  node [
    id 735
    label "i&#347;&#263;"
  ]
  node [
    id 736
    label "stara&#263;_si&#281;"
  ]
  node [
    id 737
    label "tokowa&#263;"
  ]
  node [
    id 738
    label "wida&#263;"
  ]
  node [
    id 739
    label "prezentowa&#263;"
  ]
  node [
    id 740
    label "rozgrywa&#263;"
  ]
  node [
    id 741
    label "do"
  ]
  node [
    id 742
    label "brzmie&#263;"
  ]
  node [
    id 743
    label "wykorzystywa&#263;"
  ]
  node [
    id 744
    label "cope"
  ]
  node [
    id 745
    label "przedstawia&#263;"
  ]
  node [
    id 746
    label "str&#243;j_oficjalny"
  ]
  node [
    id 747
    label "str&#243;j"
  ]
  node [
    id 748
    label "karnawa&#322;"
  ]
  node [
    id 749
    label "onkos"
  ]
  node [
    id 750
    label "element"
  ]
  node [
    id 751
    label "charakteryzacja"
  ]
  node [
    id 752
    label "sp&#243;dnica"
  ]
  node [
    id 753
    label "&#380;akiet"
  ]
  node [
    id 754
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 755
    label "wytwarza&#263;"
  ]
  node [
    id 756
    label "work"
  ]
  node [
    id 757
    label "create"
  ]
  node [
    id 758
    label "praca"
  ]
  node [
    id 759
    label "zarz&#261;dzanie"
  ]
  node [
    id 760
    label "koszt_ca&#322;kowity"
  ]
  node [
    id 761
    label "dopracowanie"
  ]
  node [
    id 762
    label "fabrication"
  ]
  node [
    id 763
    label "dzia&#322;anie"
  ]
  node [
    id 764
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 765
    label "urzeczywistnianie"
  ]
  node [
    id 766
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 767
    label "zaprz&#281;ganie"
  ]
  node [
    id 768
    label "pojawianie_si&#281;"
  ]
  node [
    id 769
    label "realization"
  ]
  node [
    id 770
    label "wyrabianie"
  ]
  node [
    id 771
    label "use"
  ]
  node [
    id 772
    label "gospodarka"
  ]
  node [
    id 773
    label "przepracowanie"
  ]
  node [
    id 774
    label "przepracowywanie"
  ]
  node [
    id 775
    label "awansowanie"
  ]
  node [
    id 776
    label "zapracowanie"
  ]
  node [
    id 777
    label "wyrobienie"
  ]
  node [
    id 778
    label "stosowanie"
  ]
  node [
    id 779
    label "nau&#380;ywanie_si&#281;"
  ]
  node [
    id 780
    label "funkcja"
  ]
  node [
    id 781
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 782
    label "act"
  ]
  node [
    id 783
    label "bycie_w_stanie"
  ]
  node [
    id 784
    label "obrobienie"
  ]
  node [
    id 785
    label "public_treasury"
  ]
  node [
    id 786
    label "obrobi&#263;"
  ]
  node [
    id 787
    label "m&#243;c"
  ]
  node [
    id 788
    label "nawadnia&#263;"
  ]
  node [
    id 789
    label "nawadnianie"
  ]
  node [
    id 790
    label "kwestia"
  ]
  node [
    id 791
    label "cisza"
  ]
  node [
    id 792
    label "odpowied&#378;"
  ]
  node [
    id 793
    label "utw&#243;r"
  ]
  node [
    id 794
    label "rozhowor"
  ]
  node [
    id 795
    label "porozumienie"
  ]
  node [
    id 796
    label "zesp&#243;&#322;"
  ]
  node [
    id 797
    label "blokada"
  ]
  node [
    id 798
    label "hurtownia"
  ]
  node [
    id 799
    label "pas"
  ]
  node [
    id 800
    label "basic"
  ]
  node [
    id 801
    label "sk&#322;adnik"
  ]
  node [
    id 802
    label "sklep"
  ]
  node [
    id 803
    label "obr&#243;bka"
  ]
  node [
    id 804
    label "constitution"
  ]
  node [
    id 805
    label "fabryka"
  ]
  node [
    id 806
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 807
    label "syf"
  ]
  node [
    id 808
    label "rank_and_file"
  ]
  node [
    id 809
    label "tabulacja"
  ]
  node [
    id 810
    label "wa&#322;"
  ]
  node [
    id 811
    label "dramat"
  ]
  node [
    id 812
    label "plan"
  ]
  node [
    id 813
    label "prognoza"
  ]
  node [
    id 814
    label "scenario"
  ]
  node [
    id 815
    label "inwentarz"
  ]
  node [
    id 816
    label "miejsce_pracy"
  ]
  node [
    id 817
    label "dom"
  ]
  node [
    id 818
    label "stodo&#322;a"
  ]
  node [
    id 819
    label "gospodarowanie"
  ]
  node [
    id 820
    label "obora"
  ]
  node [
    id 821
    label "gospodarowa&#263;"
  ]
  node [
    id 822
    label "spichlerz"
  ]
  node [
    id 823
    label "dom_rodzinny"
  ]
  node [
    id 824
    label "przebiec"
  ]
  node [
    id 825
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 826
    label "przebiegni&#281;cie"
  ]
  node [
    id 827
    label "ideologia"
  ]
  node [
    id 828
    label "byt"
  ]
  node [
    id 829
    label "intelekt"
  ]
  node [
    id 830
    label "Kant"
  ]
  node [
    id 831
    label "p&#322;&#243;d"
  ]
  node [
    id 832
    label "pomys&#322;"
  ]
  node [
    id 833
    label "ideacja"
  ]
  node [
    id 834
    label "rozumowanie"
  ]
  node [
    id 835
    label "opracowanie"
  ]
  node [
    id 836
    label "proces"
  ]
  node [
    id 837
    label "cytat"
  ]
  node [
    id 838
    label "obja&#347;nienie"
  ]
  node [
    id 839
    label "s&#261;dzenie"
  ]
  node [
    id 840
    label "zniuansowa&#263;_si&#281;"
  ]
  node [
    id 841
    label "niuansowa&#263;"
  ]
  node [
    id 842
    label "niuansowa&#263;_si&#281;"
  ]
  node [
    id 843
    label "zniuansowa&#263;"
  ]
  node [
    id 844
    label "fakt"
  ]
  node [
    id 845
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 846
    label "przyczyna"
  ]
  node [
    id 847
    label "wnioskowanie"
  ]
  node [
    id 848
    label "czynno&#347;&#263;_prawna"
  ]
  node [
    id 849
    label "traverse"
  ]
  node [
    id 850
    label "kara_&#347;mierci"
  ]
  node [
    id 851
    label "d&#322;o&#324;"
  ]
  node [
    id 852
    label "cierpienie"
  ]
  node [
    id 853
    label "symbol"
  ]
  node [
    id 854
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 855
    label "biblizm"
  ]
  node [
    id 856
    label "order"
  ]
  node [
    id 857
    label "gest"
  ]
  node [
    id 858
    label "odznaka"
  ]
  node [
    id 859
    label "kawaler"
  ]
  node [
    id 860
    label "sponiewieranie"
  ]
  node [
    id 861
    label "discipline"
  ]
  node [
    id 862
    label "kr&#261;&#380;enie"
  ]
  node [
    id 863
    label "sponiewiera&#263;"
  ]
  node [
    id 864
    label "kr&#261;&#380;y&#263;"
  ]
  node [
    id 865
    label "program_nauczania"
  ]
  node [
    id 866
    label "akceptowanie"
  ]
  node [
    id 867
    label "j&#281;czenie"
  ]
  node [
    id 868
    label "czucie"
  ]
  node [
    id 869
    label "cier&#324;"
  ]
  node [
    id 870
    label "toleration"
  ]
  node [
    id 871
    label "pain"
  ]
  node [
    id 872
    label "grief"
  ]
  node [
    id 873
    label "m&#281;ka_Pa&#324;ska"
  ]
  node [
    id 874
    label "pochorowanie"
  ]
  node [
    id 875
    label "drzazga"
  ]
  node [
    id 876
    label "badgering"
  ]
  node [
    id 877
    label "namartwienie_si&#281;"
  ]
  node [
    id 878
    label "zaznawanie"
  ]
  node [
    id 879
    label "prze&#380;ycie"
  ]
  node [
    id 880
    label "chory"
  ]
  node [
    id 881
    label "znak_pisarski"
  ]
  node [
    id 882
    label "notacja"
  ]
  node [
    id 883
    label "wcielenie"
  ]
  node [
    id 884
    label "brzoza_kr&#243;lewska"
  ]
  node [
    id 885
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 886
    label "character"
  ]
  node [
    id 887
    label "symbolizowanie"
  ]
  node [
    id 888
    label "dobrodziejstwo"
  ]
  node [
    id 889
    label "narz&#281;dzie"
  ]
  node [
    id 890
    label "gesture"
  ]
  node [
    id 891
    label "gestykulacja"
  ]
  node [
    id 892
    label "pantomima"
  ]
  node [
    id 893
    label "ruch"
  ]
  node [
    id 894
    label "motion"
  ]
  node [
    id 895
    label "alfa_i_omega"
  ]
  node [
    id 896
    label "samarytanka"
  ]
  node [
    id 897
    label "&#322;ono_Abrahama"
  ]
  node [
    id 898
    label "niewola_egipska"
  ]
  node [
    id 899
    label "s&#243;l_ziemi"
  ]
  node [
    id 900
    label "dziecko_Beliala"
  ]
  node [
    id 901
    label "je&#378;dziec_Apokalipsy"
  ]
  node [
    id 902
    label "syn_marnotrawny"
  ]
  node [
    id 903
    label "korona_cierniowa"
  ]
  node [
    id 904
    label "niebieski_ptak"
  ]
  node [
    id 905
    label "grdyka"
  ]
  node [
    id 906
    label "droga_krzy&#380;owa"
  ]
  node [
    id 907
    label "manna_z_nieba"
  ]
  node [
    id 908
    label "niewierny_Tomasz"
  ]
  node [
    id 909
    label "tr&#261;ba_jerycho&#324;ska"
  ]
  node [
    id 910
    label "drabina_Jakubowa"
  ]
  node [
    id 911
    label "Herod"
  ]
  node [
    id 912
    label "ucho_igielne"
  ]
  node [
    id 913
    label "krzew_gorej&#261;cy"
  ]
  node [
    id 914
    label "&#380;ona_Lota"
  ]
  node [
    id 915
    label "kolos_na_glinianych_nogach"
  ]
  node [
    id 916
    label "kozio&#322;_ofiarny"
  ]
  node [
    id 917
    label "ga&#322;&#261;zka_oliwna"
  ]
  node [
    id 918
    label "odst&#281;pca"
  ]
  node [
    id 919
    label "list_Uriasza"
  ]
  node [
    id 920
    label "plaga_egipska"
  ]
  node [
    id 921
    label "wdowi_grosz"
  ]
  node [
    id 922
    label "&#380;ona_Putyfara"
  ]
  node [
    id 923
    label "szatan"
  ]
  node [
    id 924
    label "chleb_powszedni"
  ]
  node [
    id 925
    label "Sodoma"
  ]
  node [
    id 926
    label "judaszowe_srebrniki"
  ]
  node [
    id 927
    label "wiek_matuzalemowy"
  ]
  node [
    id 928
    label "arka_przymierza"
  ]
  node [
    id 929
    label "tr&#261;by_jerycho&#324;skie"
  ]
  node [
    id 930
    label "lewiatan"
  ]
  node [
    id 931
    label "miedziane_czo&#322;o"
  ]
  node [
    id 932
    label "kamie&#324;_w&#281;gielny"
  ]
  node [
    id 933
    label "cnotliwa_Zuzanna"
  ]
  node [
    id 934
    label "herod-baba"
  ]
  node [
    id 935
    label "palec_bo&#380;y"
  ]
  node [
    id 936
    label "zb&#322;&#261;kana_owca"
  ]
  node [
    id 937
    label "listek_figowy"
  ]
  node [
    id 938
    label "Behemot"
  ]
  node [
    id 939
    label "mury_Jerycha"
  ]
  node [
    id 940
    label "kainowe_pi&#281;tno"
  ]
  node [
    id 941
    label "z&#322;oty_cielec"
  ]
  node [
    id 942
    label "o&#347;lica_Balaama"
  ]
  node [
    id 943
    label "arka_Noego"
  ]
  node [
    id 944
    label "Gomora"
  ]
  node [
    id 945
    label "wie&#380;a_Babel"
  ]
  node [
    id 946
    label "winnica_Nabota"
  ]
  node [
    id 947
    label "miecz_obosieczny"
  ]
  node [
    id 948
    label "fa&#322;szywy_prorok"
  ]
  node [
    id 949
    label "gr&#243;b_pobielany"
  ]
  node [
    id 950
    label "miska_soczewicy"
  ]
  node [
    id 951
    label "rze&#378;_niewini&#261;tek"
  ]
  node [
    id 952
    label "linia_mi&#322;o&#347;ci"
  ]
  node [
    id 953
    label "wyklepanie"
  ]
  node [
    id 954
    label "palec"
  ]
  node [
    id 955
    label "chiromancja"
  ]
  node [
    id 956
    label "klepanie"
  ]
  node [
    id 957
    label "wyklepa&#263;"
  ]
  node [
    id 958
    label "nadgarstek"
  ]
  node [
    id 959
    label "szeroko&#347;&#263;_d&#322;oni"
  ]
  node [
    id 960
    label "dotykanie"
  ]
  node [
    id 961
    label "graba"
  ]
  node [
    id 962
    label "klepa&#263;"
  ]
  node [
    id 963
    label "r&#261;czyna"
  ]
  node [
    id 964
    label "cmoknonsens"
  ]
  node [
    id 965
    label "chwyta&#263;"
  ]
  node [
    id 966
    label "r&#281;ka"
  ]
  node [
    id 967
    label "chwytanie"
  ]
  node [
    id 968
    label "linia_&#380;ycia"
  ]
  node [
    id 969
    label "hasta"
  ]
  node [
    id 970
    label "linia_rozumu"
  ]
  node [
    id 971
    label "poduszka"
  ]
  node [
    id 972
    label "dotyka&#263;"
  ]
  node [
    id 973
    label "segment_ruchowy"
  ]
  node [
    id 974
    label "kr&#281;g"
  ]
  node [
    id 975
    label "otw&#243;r_mi&#281;dzykr&#281;gowy"
  ]
  node [
    id 976
    label "szkielet"
  ]
  node [
    id 977
    label "stenoza"
  ]
  node [
    id 978
    label "rozszczep_kr&#281;gos&#322;upa"
  ]
  node [
    id 979
    label "dysk"
  ]
  node [
    id 980
    label "podstawa"
  ]
  node [
    id 981
    label "kifoza_piersiowa"
  ]
  node [
    id 982
    label "moralno&#347;&#263;"
  ]
  node [
    id 983
    label "obrze&#380;e"
  ]
  node [
    id 984
    label "Rzym_Zachodni"
  ]
  node [
    id 985
    label "whole"
  ]
  node [
    id 986
    label "ilo&#347;&#263;"
  ]
  node [
    id 987
    label "Rzym_Wschodni"
  ]
  node [
    id 988
    label "urz&#261;dzenie"
  ]
  node [
    id 989
    label "brzeg"
  ]
  node [
    id 990
    label "granica"
  ]
  node [
    id 991
    label "nowy"
  ]
  node [
    id 992
    label "krakowski"
  ]
  node [
    id 993
    label "stowarzyszy&#263;"
  ]
  node [
    id 994
    label "dziennikarz"
  ]
  node [
    id 995
    label "polskie"
  ]
  node [
    id 996
    label "centrum"
  ]
  node [
    id 997
    label "monitoring"
  ]
  node [
    id 998
    label "wolno&#347;&#263;"
  ]
  node [
    id 999
    label "prasa"
  ]
  node [
    id 1000
    label "polski"
  ]
  node [
    id 1001
    label "agencja"
  ]
  node [
    id 1002
    label "prasowy"
  ]
  node [
    id 1003
    label "Janina"
  ]
  node [
    id 1004
    label "Jankowska"
  ]
  node [
    id 1005
    label "Piotr"
  ]
  node [
    id 1006
    label "Zaremba"
  ]
  node [
    id 1007
    label "Roberta"
  ]
  node [
    id 1008
    label "Bogda&#324;ski"
  ]
  node [
    id 1009
    label "zaj&#261;c"
  ]
  node [
    id 1010
    label "Krzysztofa"
  ]
  node [
    id 1011
    label "Skowro&#324;ski"
  ]
  node [
    id 1012
    label "radio"
  ]
  node [
    id 1013
    label "wnet"
  ]
  node [
    id 1014
    label "wiktor"
  ]
  node [
    id 1015
    label "&#347;wietlik"
  ]
  node [
    id 1016
    label "Bogus&#322;awa"
  ]
  node [
    id 1017
    label "Chraboty"
  ]
  node [
    id 1018
    label "Tomasz"
  ]
  node [
    id 1019
    label "Sakiewicza"
  ]
  node [
    id 1020
    label "Bogumi&#322;a"
  ]
  node [
    id 1021
    label "&#321;ozi&#324;ski"
  ]
  node [
    id 1022
    label "Wojciecha"
  ]
  node [
    id 1023
    label "Maziarski"
  ]
  node [
    id 1024
    label "niedzielny"
  ]
  node [
    id 1025
    label "gazeta"
  ]
  node [
    id 1026
    label "wyborczy"
  ]
  node [
    id 1027
    label "Rafa&#322;"
  ]
  node [
    id 1028
    label "zimny"
  ]
  node [
    id 1029
    label "uniwersytet"
  ]
  node [
    id 1030
    label "Kazimierz"
  ]
  node [
    id 1031
    label "wielki"
  ]
  node [
    id 1032
    label "s&#322;ownik"
  ]
  node [
    id 1033
    label "polszczyzna"
  ]
  node [
    id 1034
    label "polityczny"
  ]
  node [
    id 1035
    label "po"
  ]
  node [
    id 1036
    label "rok"
  ]
  node [
    id 1037
    label "1989"
  ]
  node [
    id 1038
    label "albo"
  ]
  node [
    id 1039
    label "Nomejki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 991
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 81
  ]
  edge [
    source 2
    target 82
  ]
  edge [
    source 2
    target 83
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 118
  ]
  edge [
    source 4
    target 119
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 121
  ]
  edge [
    source 4
    target 122
  ]
  edge [
    source 4
    target 123
  ]
  edge [
    source 4
    target 124
  ]
  edge [
    source 4
    target 125
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 127
  ]
  edge [
    source 4
    target 128
  ]
  edge [
    source 4
    target 129
  ]
  edge [
    source 4
    target 130
  ]
  edge [
    source 4
    target 131
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 7
  ]
  edge [
    source 5
    target 171
  ]
  edge [
    source 5
    target 172
  ]
  edge [
    source 5
    target 173
  ]
  edge [
    source 5
    target 174
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 175
  ]
  edge [
    source 5
    target 176
  ]
  edge [
    source 5
    target 177
  ]
  edge [
    source 5
    target 178
  ]
  edge [
    source 5
    target 179
  ]
  edge [
    source 5
    target 180
  ]
  edge [
    source 5
    target 181
  ]
  edge [
    source 5
    target 182
  ]
  edge [
    source 5
    target 183
  ]
  edge [
    source 5
    target 184
  ]
  edge [
    source 5
    target 185
  ]
  edge [
    source 5
    target 186
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 188
  ]
  edge [
    source 5
    target 189
  ]
  edge [
    source 5
    target 190
  ]
  edge [
    source 5
    target 191
  ]
  edge [
    source 5
    target 192
  ]
  edge [
    source 5
    target 143
  ]
  edge [
    source 5
    target 193
  ]
  edge [
    source 5
    target 194
  ]
  edge [
    source 5
    target 195
  ]
  edge [
    source 5
    target 196
  ]
  edge [
    source 5
    target 197
  ]
  edge [
    source 5
    target 198
  ]
  edge [
    source 5
    target 199
  ]
  edge [
    source 5
    target 200
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 145
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 211
  ]
  edge [
    source 5
    target 212
  ]
  edge [
    source 5
    target 213
  ]
  edge [
    source 5
    target 214
  ]
  edge [
    source 5
    target 215
  ]
  edge [
    source 5
    target 216
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 218
  ]
  edge [
    source 5
    target 219
  ]
  edge [
    source 5
    target 220
  ]
  edge [
    source 5
    target 221
  ]
  edge [
    source 5
    target 222
  ]
  edge [
    source 5
    target 223
  ]
  edge [
    source 5
    target 224
  ]
  edge [
    source 5
    target 225
  ]
  edge [
    source 5
    target 226
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 227
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 229
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 232
  ]
  edge [
    source 5
    target 233
  ]
  edge [
    source 5
    target 234
  ]
  edge [
    source 5
    target 235
  ]
  edge [
    source 5
    target 236
  ]
  edge [
    source 5
    target 237
  ]
  edge [
    source 5
    target 238
  ]
  edge [
    source 5
    target 239
  ]
  edge [
    source 5
    target 240
  ]
  edge [
    source 5
    target 241
  ]
  edge [
    source 5
    target 242
  ]
  edge [
    source 5
    target 243
  ]
  edge [
    source 5
    target 244
  ]
  edge [
    source 5
    target 245
  ]
  edge [
    source 5
    target 246
  ]
  edge [
    source 5
    target 247
  ]
  edge [
    source 5
    target 248
  ]
  edge [
    source 5
    target 249
  ]
  edge [
    source 5
    target 250
  ]
  edge [
    source 5
    target 251
  ]
  edge [
    source 5
    target 252
  ]
  edge [
    source 5
    target 253
  ]
  edge [
    source 5
    target 254
  ]
  edge [
    source 5
    target 255
  ]
  edge [
    source 5
    target 256
  ]
  edge [
    source 5
    target 257
  ]
  edge [
    source 5
    target 258
  ]
  edge [
    source 5
    target 259
  ]
  edge [
    source 5
    target 164
  ]
  edge [
    source 5
    target 260
  ]
  edge [
    source 5
    target 261
  ]
  edge [
    source 5
    target 262
  ]
  edge [
    source 5
    target 263
  ]
  edge [
    source 5
    target 264
  ]
  edge [
    source 5
    target 265
  ]
  edge [
    source 5
    target 266
  ]
  edge [
    source 5
    target 267
  ]
  edge [
    source 5
    target 268
  ]
  edge [
    source 5
    target 269
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 271
  ]
  edge [
    source 5
    target 272
  ]
  edge [
    source 5
    target 273
  ]
  edge [
    source 5
    target 274
  ]
  edge [
    source 5
    target 275
  ]
  edge [
    source 5
    target 276
  ]
  edge [
    source 5
    target 277
  ]
  edge [
    source 5
    target 278
  ]
  edge [
    source 5
    target 279
  ]
  edge [
    source 5
    target 280
  ]
  edge [
    source 5
    target 281
  ]
  edge [
    source 5
    target 282
  ]
  edge [
    source 5
    target 283
  ]
  edge [
    source 5
    target 284
  ]
  edge [
    source 5
    target 285
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 287
  ]
  edge [
    source 5
    target 288
  ]
  edge [
    source 5
    target 289
  ]
  edge [
    source 5
    target 290
  ]
  edge [
    source 5
    target 291
  ]
  edge [
    source 5
    target 292
  ]
  edge [
    source 5
    target 293
  ]
  edge [
    source 5
    target 294
  ]
  edge [
    source 5
    target 295
  ]
  edge [
    source 5
    target 296
  ]
  edge [
    source 5
    target 297
  ]
  edge [
    source 5
    target 298
  ]
  edge [
    source 5
    target 299
  ]
  edge [
    source 5
    target 300
  ]
  edge [
    source 5
    target 301
  ]
  edge [
    source 5
    target 302
  ]
  edge [
    source 5
    target 303
  ]
  edge [
    source 5
    target 304
  ]
  edge [
    source 5
    target 305
  ]
  edge [
    source 5
    target 306
  ]
  edge [
    source 5
    target 307
  ]
  edge [
    source 5
    target 308
  ]
  edge [
    source 5
    target 309
  ]
  edge [
    source 5
    target 310
  ]
  edge [
    source 5
    target 311
  ]
  edge [
    source 5
    target 312
  ]
  edge [
    source 5
    target 313
  ]
  edge [
    source 5
    target 314
  ]
  edge [
    source 5
    target 315
  ]
  edge [
    source 5
    target 316
  ]
  edge [
    source 5
    target 317
  ]
  edge [
    source 5
    target 318
  ]
  edge [
    source 5
    target 319
  ]
  edge [
    source 5
    target 320
  ]
  edge [
    source 5
    target 321
  ]
  edge [
    source 5
    target 322
  ]
  edge [
    source 5
    target 323
  ]
  edge [
    source 5
    target 324
  ]
  edge [
    source 5
    target 325
  ]
  edge [
    source 5
    target 326
  ]
  edge [
    source 5
    target 327
  ]
  edge [
    source 5
    target 328
  ]
  edge [
    source 5
    target 329
  ]
  edge [
    source 5
    target 330
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 331
  ]
  edge [
    source 5
    target 332
  ]
  edge [
    source 5
    target 333
  ]
  edge [
    source 5
    target 334
  ]
  edge [
    source 5
    target 335
  ]
  edge [
    source 5
    target 336
  ]
  edge [
    source 5
    target 337
  ]
  edge [
    source 5
    target 338
  ]
  edge [
    source 5
    target 339
  ]
  edge [
    source 5
    target 340
  ]
  edge [
    source 5
    target 341
  ]
  edge [
    source 5
    target 342
  ]
  edge [
    source 5
    target 343
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 344
  ]
  edge [
    source 5
    target 345
  ]
  edge [
    source 5
    target 346
  ]
  edge [
    source 5
    target 347
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 6
    target 348
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 349
  ]
  edge [
    source 6
    target 350
  ]
  edge [
    source 6
    target 351
  ]
  edge [
    source 6
    target 352
  ]
  edge [
    source 6
    target 353
  ]
  edge [
    source 6
    target 354
  ]
  edge [
    source 6
    target 355
  ]
  edge [
    source 6
    target 356
  ]
  edge [
    source 6
    target 357
  ]
  edge [
    source 6
    target 358
  ]
  edge [
    source 6
    target 359
  ]
  edge [
    source 6
    target 360
  ]
  edge [
    source 6
    target 361
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 362
  ]
  edge [
    source 6
    target 363
  ]
  edge [
    source 6
    target 364
  ]
  edge [
    source 6
    target 365
  ]
  edge [
    source 6
    target 366
  ]
  edge [
    source 6
    target 367
  ]
  edge [
    source 6
    target 368
  ]
  edge [
    source 6
    target 369
  ]
  edge [
    source 6
    target 370
  ]
  edge [
    source 6
    target 371
  ]
  edge [
    source 6
    target 372
  ]
  edge [
    source 6
    target 373
  ]
  edge [
    source 6
    target 374
  ]
  edge [
    source 6
    target 375
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 376
  ]
  edge [
    source 6
    target 377
  ]
  edge [
    source 6
    target 378
  ]
  edge [
    source 6
    target 379
  ]
  edge [
    source 6
    target 380
  ]
  edge [
    source 6
    target 381
  ]
  edge [
    source 6
    target 382
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 383
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 384
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 385
  ]
  edge [
    source 6
    target 386
  ]
  edge [
    source 6
    target 387
  ]
  edge [
    source 6
    target 388
  ]
  edge [
    source 6
    target 389
  ]
  edge [
    source 6
    target 390
  ]
  edge [
    source 6
    target 391
  ]
  edge [
    source 6
    target 392
  ]
  edge [
    source 6
    target 393
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 394
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 395
  ]
  edge [
    source 6
    target 396
  ]
  edge [
    source 6
    target 397
  ]
  edge [
    source 6
    target 398
  ]
  edge [
    source 6
    target 399
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 400
  ]
  edge [
    source 6
    target 401
  ]
  edge [
    source 6
    target 402
  ]
  edge [
    source 6
    target 403
  ]
  edge [
    source 6
    target 404
  ]
  edge [
    source 6
    target 405
  ]
  edge [
    source 6
    target 406
  ]
  edge [
    source 6
    target 407
  ]
  edge [
    source 6
    target 408
  ]
  edge [
    source 6
    target 409
  ]
  edge [
    source 6
    target 410
  ]
  edge [
    source 6
    target 411
  ]
  edge [
    source 6
    target 412
  ]
  edge [
    source 6
    target 413
  ]
  edge [
    source 6
    target 414
  ]
  edge [
    source 6
    target 415
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 416
  ]
  edge [
    source 6
    target 417
  ]
  edge [
    source 6
    target 418
  ]
  edge [
    source 6
    target 419
  ]
  edge [
    source 6
    target 420
  ]
  edge [
    source 6
    target 421
  ]
  edge [
    source 6
    target 422
  ]
  edge [
    source 6
    target 423
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 424
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 425
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 426
  ]
  edge [
    source 6
    target 427
  ]
  edge [
    source 6
    target 428
  ]
  edge [
    source 6
    target 429
  ]
  edge [
    source 6
    target 430
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 431
  ]
  edge [
    source 6
    target 432
  ]
  edge [
    source 6
    target 433
  ]
  edge [
    source 6
    target 434
  ]
  edge [
    source 6
    target 435
  ]
  edge [
    source 6
    target 436
  ]
  edge [
    source 6
    target 437
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 438
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 439
  ]
  edge [
    source 6
    target 440
  ]
  edge [
    source 6
    target 441
  ]
  edge [
    source 6
    target 442
  ]
  edge [
    source 6
    target 443
  ]
  edge [
    source 6
    target 444
  ]
  edge [
    source 6
    target 445
  ]
  edge [
    source 6
    target 446
  ]
  edge [
    source 6
    target 447
  ]
  edge [
    source 6
    target 448
  ]
  edge [
    source 6
    target 449
  ]
  edge [
    source 6
    target 450
  ]
  edge [
    source 6
    target 451
  ]
  edge [
    source 6
    target 452
  ]
  edge [
    source 6
    target 453
  ]
  edge [
    source 6
    target 454
  ]
  edge [
    source 6
    target 343
  ]
  edge [
    source 6
    target 455
  ]
  edge [
    source 6
    target 456
  ]
  edge [
    source 6
    target 457
  ]
  edge [
    source 6
    target 458
  ]
  edge [
    source 6
    target 459
  ]
  edge [
    source 6
    target 460
  ]
  edge [
    source 6
    target 461
  ]
  edge [
    source 6
    target 462
  ]
  edge [
    source 6
    target 463
  ]
  edge [
    source 6
    target 464
  ]
  edge [
    source 6
    target 465
  ]
  edge [
    source 6
    target 466
  ]
  edge [
    source 6
    target 467
  ]
  edge [
    source 6
    target 468
  ]
  edge [
    source 6
    target 469
  ]
  edge [
    source 6
    target 470
  ]
  edge [
    source 6
    target 471
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 472
  ]
  edge [
    source 6
    target 473
  ]
  edge [
    source 6
    target 474
  ]
  edge [
    source 6
    target 475
  ]
  edge [
    source 6
    target 476
  ]
  edge [
    source 6
    target 477
  ]
  edge [
    source 6
    target 478
  ]
  edge [
    source 6
    target 479
  ]
  edge [
    source 6
    target 480
  ]
  edge [
    source 6
    target 481
  ]
  edge [
    source 6
    target 482
  ]
  edge [
    source 6
    target 483
  ]
  edge [
    source 6
    target 484
  ]
  edge [
    source 6
    target 485
  ]
  edge [
    source 6
    target 486
  ]
  edge [
    source 6
    target 487
  ]
  edge [
    source 6
    target 488
  ]
  edge [
    source 6
    target 489
  ]
  edge [
    source 6
    target 490
  ]
  edge [
    source 6
    target 491
  ]
  edge [
    source 6
    target 492
  ]
  edge [
    source 6
    target 493
  ]
  edge [
    source 6
    target 494
  ]
  edge [
    source 6
    target 495
  ]
  edge [
    source 6
    target 496
  ]
  edge [
    source 6
    target 497
  ]
  edge [
    source 6
    target 498
  ]
  edge [
    source 6
    target 499
  ]
  edge [
    source 6
    target 500
  ]
  edge [
    source 6
    target 501
  ]
  edge [
    source 6
    target 502
  ]
  edge [
    source 6
    target 503
  ]
  edge [
    source 6
    target 504
  ]
  edge [
    source 6
    target 505
  ]
  edge [
    source 6
    target 506
  ]
  edge [
    source 6
    target 507
  ]
  edge [
    source 6
    target 508
  ]
  edge [
    source 6
    target 509
  ]
  edge [
    source 6
    target 510
  ]
  edge [
    source 6
    target 511
  ]
  edge [
    source 6
    target 512
  ]
  edge [
    source 6
    target 513
  ]
  edge [
    source 6
    target 514
  ]
  edge [
    source 6
    target 515
  ]
  edge [
    source 6
    target 516
  ]
  edge [
    source 6
    target 517
  ]
  edge [
    source 6
    target 518
  ]
  edge [
    source 6
    target 519
  ]
  edge [
    source 6
    target 520
  ]
  edge [
    source 6
    target 521
  ]
  edge [
    source 6
    target 522
  ]
  edge [
    source 6
    target 523
  ]
  edge [
    source 6
    target 524
  ]
  edge [
    source 6
    target 525
  ]
  edge [
    source 6
    target 526
  ]
  edge [
    source 6
    target 527
  ]
  edge [
    source 6
    target 528
  ]
  edge [
    source 6
    target 529
  ]
  edge [
    source 6
    target 530
  ]
  edge [
    source 6
    target 531
  ]
  edge [
    source 6
    target 532
  ]
  edge [
    source 6
    target 533
  ]
  edge [
    source 6
    target 534
  ]
  edge [
    source 6
    target 535
  ]
  edge [
    source 6
    target 536
  ]
  edge [
    source 6
    target 537
  ]
  edge [
    source 6
    target 538
  ]
  edge [
    source 6
    target 539
  ]
  edge [
    source 6
    target 540
  ]
  edge [
    source 6
    target 541
  ]
  edge [
    source 6
    target 542
  ]
  edge [
    source 6
    target 543
  ]
  edge [
    source 6
    target 544
  ]
  edge [
    source 6
    target 545
  ]
  edge [
    source 6
    target 546
  ]
  edge [
    source 6
    target 547
  ]
  edge [
    source 6
    target 548
  ]
  edge [
    source 6
    target 549
  ]
  edge [
    source 6
    target 550
  ]
  edge [
    source 6
    target 551
  ]
  edge [
    source 6
    target 552
  ]
  edge [
    source 6
    target 553
  ]
  edge [
    source 6
    target 554
  ]
  edge [
    source 6
    target 555
  ]
  edge [
    source 6
    target 556
  ]
  edge [
    source 6
    target 557
  ]
  edge [
    source 6
    target 558
  ]
  edge [
    source 6
    target 559
  ]
  edge [
    source 6
    target 560
  ]
  edge [
    source 6
    target 561
  ]
  edge [
    source 6
    target 562
  ]
  edge [
    source 6
    target 563
  ]
  edge [
    source 6
    target 564
  ]
  edge [
    source 6
    target 565
  ]
  edge [
    source 6
    target 566
  ]
  edge [
    source 6
    target 567
  ]
  edge [
    source 6
    target 568
  ]
  edge [
    source 6
    target 569
  ]
  edge [
    source 6
    target 570
  ]
  edge [
    source 6
    target 571
  ]
  edge [
    source 6
    target 572
  ]
  edge [
    source 6
    target 573
  ]
  edge [
    source 6
    target 574
  ]
  edge [
    source 6
    target 575
  ]
  edge [
    source 6
    target 576
  ]
  edge [
    source 6
    target 577
  ]
  edge [
    source 6
    target 578
  ]
  edge [
    source 6
    target 579
  ]
  edge [
    source 6
    target 580
  ]
  edge [
    source 6
    target 581
  ]
  edge [
    source 6
    target 582
  ]
  edge [
    source 6
    target 583
  ]
  edge [
    source 6
    target 584
  ]
  edge [
    source 6
    target 585
  ]
  edge [
    source 6
    target 586
  ]
  edge [
    source 6
    target 587
  ]
  edge [
    source 6
    target 588
  ]
  edge [
    source 6
    target 589
  ]
  edge [
    source 6
    target 590
  ]
  edge [
    source 6
    target 591
  ]
  edge [
    source 6
    target 592
  ]
  edge [
    source 6
    target 593
  ]
  edge [
    source 6
    target 594
  ]
  edge [
    source 6
    target 595
  ]
  edge [
    source 6
    target 596
  ]
  edge [
    source 6
    target 597
  ]
  edge [
    source 6
    target 598
  ]
  edge [
    source 6
    target 599
  ]
  edge [
    source 6
    target 600
  ]
  edge [
    source 6
    target 601
  ]
  edge [
    source 6
    target 602
  ]
  edge [
    source 6
    target 603
  ]
  edge [
    source 6
    target 604
  ]
  edge [
    source 6
    target 605
  ]
  edge [
    source 6
    target 606
  ]
  edge [
    source 6
    target 607
  ]
  edge [
    source 6
    target 608
  ]
  edge [
    source 6
    target 609
  ]
  edge [
    source 6
    target 610
  ]
  edge [
    source 6
    target 611
  ]
  edge [
    source 6
    target 612
  ]
  edge [
    source 6
    target 613
  ]
  edge [
    source 6
    target 614
  ]
  edge [
    source 6
    target 615
  ]
  edge [
    source 6
    target 616
  ]
  edge [
    source 6
    target 617
  ]
  edge [
    source 6
    target 618
  ]
  edge [
    source 6
    target 619
  ]
  edge [
    source 6
    target 620
  ]
  edge [
    source 6
    target 621
  ]
  edge [
    source 6
    target 622
  ]
  edge [
    source 6
    target 623
  ]
  edge [
    source 6
    target 624
  ]
  edge [
    source 6
    target 625
  ]
  edge [
    source 6
    target 626
  ]
  edge [
    source 6
    target 627
  ]
  edge [
    source 6
    target 628
  ]
  edge [
    source 6
    target 629
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 630
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 631
  ]
  edge [
    source 6
    target 632
  ]
  edge [
    source 6
    target 633
  ]
  edge [
    source 6
    target 634
  ]
  edge [
    source 6
    target 635
  ]
  edge [
    source 6
    target 636
  ]
  edge [
    source 6
    target 637
  ]
  edge [
    source 6
    target 638
  ]
  edge [
    source 6
    target 639
  ]
  edge [
    source 6
    target 640
  ]
  edge [
    source 6
    target 641
  ]
  edge [
    source 6
    target 642
  ]
  edge [
    source 6
    target 643
  ]
  edge [
    source 6
    target 644
  ]
  edge [
    source 6
    target 645
  ]
  edge [
    source 6
    target 646
  ]
  edge [
    source 6
    target 647
  ]
  edge [
    source 6
    target 648
  ]
  edge [
    source 6
    target 649
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 650
  ]
  edge [
    source 6
    target 651
  ]
  edge [
    source 6
    target 652
  ]
  edge [
    source 6
    target 653
  ]
  edge [
    source 6
    target 654
  ]
  edge [
    source 6
    target 655
  ]
  edge [
    source 6
    target 656
  ]
  edge [
    source 6
    target 657
  ]
  edge [
    source 6
    target 658
  ]
  edge [
    source 6
    target 659
  ]
  edge [
    source 6
    target 660
  ]
  edge [
    source 6
    target 661
  ]
  edge [
    source 6
    target 662
  ]
  edge [
    source 6
    target 663
  ]
  edge [
    source 6
    target 664
  ]
  edge [
    source 6
    target 665
  ]
  edge [
    source 6
    target 666
  ]
  edge [
    source 6
    target 667
  ]
  edge [
    source 6
    target 668
  ]
  edge [
    source 6
    target 669
  ]
  edge [
    source 6
    target 670
  ]
  edge [
    source 6
    target 671
  ]
  edge [
    source 6
    target 672
  ]
  edge [
    source 6
    target 673
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 674
  ]
  edge [
    source 6
    target 675
  ]
  edge [
    source 6
    target 676
  ]
  edge [
    source 6
    target 677
  ]
  edge [
    source 6
    target 678
  ]
  edge [
    source 6
    target 679
  ]
  edge [
    source 6
    target 680
  ]
  edge [
    source 6
    target 681
  ]
  edge [
    source 6
    target 682
  ]
  edge [
    source 6
    target 683
  ]
  edge [
    source 6
    target 684
  ]
  edge [
    source 6
    target 685
  ]
  edge [
    source 6
    target 686
  ]
  edge [
    source 6
    target 687
  ]
  edge [
    source 6
    target 688
  ]
  edge [
    source 6
    target 689
  ]
  edge [
    source 6
    target 690
  ]
  edge [
    source 6
    target 691
  ]
  edge [
    source 6
    target 692
  ]
  edge [
    source 6
    target 693
  ]
  edge [
    source 6
    target 694
  ]
  edge [
    source 6
    target 695
  ]
  edge [
    source 6
    target 696
  ]
  edge [
    source 6
    target 697
  ]
  edge [
    source 6
    target 698
  ]
  edge [
    source 6
    target 699
  ]
  edge [
    source 6
    target 700
  ]
  edge [
    source 6
    target 701
  ]
  edge [
    source 6
    target 702
  ]
  edge [
    source 6
    target 703
  ]
  edge [
    source 6
    target 704
  ]
  edge [
    source 6
    target 705
  ]
  edge [
    source 6
    target 706
  ]
  edge [
    source 6
    target 707
  ]
  edge [
    source 6
    target 708
  ]
  edge [
    source 6
    target 709
  ]
  edge [
    source 6
    target 710
  ]
  edge [
    source 6
    target 711
  ]
  edge [
    source 6
    target 712
  ]
  edge [
    source 6
    target 713
  ]
  edge [
    source 6
    target 714
  ]
  edge [
    source 6
    target 715
  ]
  edge [
    source 6
    target 716
  ]
  edge [
    source 6
    target 717
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 718
  ]
  edge [
    source 6
    target 719
  ]
  edge [
    source 6
    target 720
  ]
  edge [
    source 6
    target 721
  ]
  edge [
    source 6
    target 722
  ]
  edge [
    source 6
    target 723
  ]
  edge [
    source 6
    target 724
  ]
  edge [
    source 6
    target 725
  ]
  edge [
    source 6
    target 726
  ]
  edge [
    source 6
    target 727
  ]
  edge [
    source 6
    target 728
  ]
  edge [
    source 6
    target 729
  ]
  edge [
    source 6
    target 730
  ]
  edge [
    source 6
    target 731
  ]
  edge [
    source 6
    target 732
  ]
  edge [
    source 6
    target 733
  ]
  edge [
    source 6
    target 734
  ]
  edge [
    source 6
    target 735
  ]
  edge [
    source 6
    target 736
  ]
  edge [
    source 6
    target 737
  ]
  edge [
    source 6
    target 738
  ]
  edge [
    source 6
    target 739
  ]
  edge [
    source 6
    target 740
  ]
  edge [
    source 6
    target 741
  ]
  edge [
    source 6
    target 742
  ]
  edge [
    source 6
    target 743
  ]
  edge [
    source 6
    target 744
  ]
  edge [
    source 6
    target 745
  ]
  edge [
    source 6
    target 746
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 747
  ]
  edge [
    source 6
    target 748
  ]
  edge [
    source 6
    target 749
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 750
  ]
  edge [
    source 6
    target 751
  ]
  edge [
    source 6
    target 752
  ]
  edge [
    source 6
    target 753
  ]
  edge [
    source 6
    target 754
  ]
  edge [
    source 6
    target 755
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 756
  ]
  edge [
    source 6
    target 757
  ]
  edge [
    source 6
    target 758
  ]
  edge [
    source 6
    target 759
  ]
  edge [
    source 6
    target 760
  ]
  edge [
    source 6
    target 761
  ]
  edge [
    source 6
    target 762
  ]
  edge [
    source 6
    target 763
  ]
  edge [
    source 6
    target 764
  ]
  edge [
    source 6
    target 765
  ]
  edge [
    source 6
    target 766
  ]
  edge [
    source 6
    target 767
  ]
  edge [
    source 6
    target 768
  ]
  edge [
    source 6
    target 769
  ]
  edge [
    source 6
    target 770
  ]
  edge [
    source 6
    target 771
  ]
  edge [
    source 6
    target 772
  ]
  edge [
    source 6
    target 773
  ]
  edge [
    source 6
    target 774
  ]
  edge [
    source 6
    target 775
  ]
  edge [
    source 6
    target 776
  ]
  edge [
    source 6
    target 777
  ]
  edge [
    source 6
    target 778
  ]
  edge [
    source 6
    target 779
  ]
  edge [
    source 6
    target 780
  ]
  edge [
    source 6
    target 781
  ]
  edge [
    source 6
    target 782
  ]
  edge [
    source 6
    target 783
  ]
  edge [
    source 6
    target 784
  ]
  edge [
    source 6
    target 785
  ]
  edge [
    source 6
    target 786
  ]
  edge [
    source 6
    target 787
  ]
  edge [
    source 6
    target 788
  ]
  edge [
    source 6
    target 789
  ]
  edge [
    source 6
    target 790
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 791
  ]
  edge [
    source 6
    target 792
  ]
  edge [
    source 6
    target 793
  ]
  edge [
    source 6
    target 794
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 795
  ]
  edge [
    source 6
    target 796
  ]
  edge [
    source 6
    target 797
  ]
  edge [
    source 6
    target 798
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 799
  ]
  edge [
    source 6
    target 800
  ]
  edge [
    source 6
    target 801
  ]
  edge [
    source 6
    target 802
  ]
  edge [
    source 6
    target 803
  ]
  edge [
    source 6
    target 804
  ]
  edge [
    source 6
    target 805
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 806
  ]
  edge [
    source 6
    target 807
  ]
  edge [
    source 6
    target 808
  ]
  edge [
    source 6
    target 809
  ]
  edge [
    source 6
    target 810
  ]
  edge [
    source 6
    target 811
  ]
  edge [
    source 6
    target 812
  ]
  edge [
    source 6
    target 813
  ]
  edge [
    source 6
    target 814
  ]
  edge [
    source 6
    target 815
  ]
  edge [
    source 6
    target 816
  ]
  edge [
    source 6
    target 817
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 818
  ]
  edge [
    source 6
    target 819
  ]
  edge [
    source 6
    target 820
  ]
  edge [
    source 6
    target 821
  ]
  edge [
    source 6
    target 822
  ]
  edge [
    source 6
    target 347
  ]
  edge [
    source 6
    target 823
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 824
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 634
  ]
  edge [
    source 7
    target 825
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 826
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 827
  ]
  edge [
    source 7
    target 828
  ]
  edge [
    source 7
    target 829
  ]
  edge [
    source 7
    target 830
  ]
  edge [
    source 7
    target 831
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 832
  ]
  edge [
    source 7
    target 833
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 834
  ]
  edge [
    source 7
    target 835
  ]
  edge [
    source 7
    target 836
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 837
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 838
  ]
  edge [
    source 7
    target 839
  ]
  edge [
    source 7
    target 840
  ]
  edge [
    source 7
    target 841
  ]
  edge [
    source 7
    target 750
  ]
  edge [
    source 7
    target 842
  ]
  edge [
    source 7
    target 801
  ]
  edge [
    source 7
    target 843
  ]
  edge [
    source 7
    target 844
  ]
  edge [
    source 7
    target 845
  ]
  edge [
    source 7
    target 846
  ]
  edge [
    source 7
    target 847
  ]
  edge [
    source 7
    target 848
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 1038
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 278
  ]
  edge [
    source 8
    target 849
  ]
  edge [
    source 8
    target 850
  ]
  edge [
    source 8
    target 851
  ]
  edge [
    source 8
    target 852
  ]
  edge [
    source 8
    target 853
  ]
  edge [
    source 8
    target 854
  ]
  edge [
    source 8
    target 855
  ]
  edge [
    source 8
    target 856
  ]
  edge [
    source 8
    target 857
  ]
  edge [
    source 8
    target 858
  ]
  edge [
    source 8
    target 859
  ]
  edge [
    source 8
    target 172
  ]
  edge [
    source 8
    target 173
  ]
  edge [
    source 8
    target 860
  ]
  edge [
    source 8
    target 861
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 175
  ]
  edge [
    source 8
    target 862
  ]
  edge [
    source 8
    target 177
  ]
  edge [
    source 8
    target 697
  ]
  edge [
    source 8
    target 863
  ]
  edge [
    source 8
    target 750
  ]
  edge [
    source 8
    target 178
  ]
  edge [
    source 8
    target 864
  ]
  edge [
    source 8
    target 181
  ]
  edge [
    source 8
    target 182
  ]
  edge [
    source 8
    target 267
  ]
  edge [
    source 8
    target 183
  ]
  edge [
    source 8
    target 865
  ]
  edge [
    source 8
    target 185
  ]
  edge [
    source 8
    target 186
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 282
  ]
  edge [
    source 8
    target 189
  ]
  edge [
    source 8
    target 190
  ]
  edge [
    source 8
    target 191
  ]
  edge [
    source 8
    target 866
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 867
  ]
  edge [
    source 8
    target 430
  ]
  edge [
    source 8
    target 868
  ]
  edge [
    source 8
    target 869
  ]
  edge [
    source 8
    target 870
  ]
  edge [
    source 8
    target 871
  ]
  edge [
    source 8
    target 872
  ]
  edge [
    source 8
    target 873
  ]
  edge [
    source 8
    target 874
  ]
  edge [
    source 8
    target 875
  ]
  edge [
    source 8
    target 876
  ]
  edge [
    source 8
    target 877
  ]
  edge [
    source 8
    target 878
  ]
  edge [
    source 8
    target 879
  ]
  edge [
    source 8
    target 880
  ]
  edge [
    source 8
    target 881
  ]
  edge [
    source 8
    target 258
  ]
  edge [
    source 8
    target 882
  ]
  edge [
    source 8
    target 883
  ]
  edge [
    source 8
    target 884
  ]
  edge [
    source 8
    target 885
  ]
  edge [
    source 8
    target 886
  ]
  edge [
    source 8
    target 887
  ]
  edge [
    source 8
    target 888
  ]
  edge [
    source 8
    target 889
  ]
  edge [
    source 8
    target 890
  ]
  edge [
    source 8
    target 891
  ]
  edge [
    source 8
    target 892
  ]
  edge [
    source 8
    target 893
  ]
  edge [
    source 8
    target 894
  ]
  edge [
    source 8
    target 230
  ]
  edge [
    source 8
    target 222
  ]
  edge [
    source 8
    target 213
  ]
  edge [
    source 8
    target 223
  ]
  edge [
    source 8
    target 231
  ]
  edge [
    source 8
    target 225
  ]
  edge [
    source 8
    target 383
  ]
  edge [
    source 8
    target 218
  ]
  edge [
    source 8
    target 384
  ]
  edge [
    source 8
    target 207
  ]
  edge [
    source 8
    target 237
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 220
  ]
  edge [
    source 8
    target 174
  ]
  edge [
    source 8
    target 228
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 240
  ]
  edge [
    source 8
    target 895
  ]
  edge [
    source 8
    target 896
  ]
  edge [
    source 8
    target 897
  ]
  edge [
    source 8
    target 898
  ]
  edge [
    source 8
    target 899
  ]
  edge [
    source 8
    target 900
  ]
  edge [
    source 8
    target 901
  ]
  edge [
    source 8
    target 902
  ]
  edge [
    source 8
    target 903
  ]
  edge [
    source 8
    target 904
  ]
  edge [
    source 8
    target 905
  ]
  edge [
    source 8
    target 906
  ]
  edge [
    source 8
    target 907
  ]
  edge [
    source 8
    target 908
  ]
  edge [
    source 8
    target 909
  ]
  edge [
    source 8
    target 910
  ]
  edge [
    source 8
    target 911
  ]
  edge [
    source 8
    target 912
  ]
  edge [
    source 8
    target 913
  ]
  edge [
    source 8
    target 914
  ]
  edge [
    source 8
    target 915
  ]
  edge [
    source 8
    target 916
  ]
  edge [
    source 8
    target 917
  ]
  edge [
    source 8
    target 918
  ]
  edge [
    source 8
    target 919
  ]
  edge [
    source 8
    target 920
  ]
  edge [
    source 8
    target 921
  ]
  edge [
    source 8
    target 922
  ]
  edge [
    source 8
    target 923
  ]
  edge [
    source 8
    target 924
  ]
  edge [
    source 8
    target 925
  ]
  edge [
    source 8
    target 926
  ]
  edge [
    source 8
    target 927
  ]
  edge [
    source 8
    target 928
  ]
  edge [
    source 8
    target 929
  ]
  edge [
    source 8
    target 930
  ]
  edge [
    source 8
    target 931
  ]
  edge [
    source 8
    target 932
  ]
  edge [
    source 8
    target 933
  ]
  edge [
    source 8
    target 934
  ]
  edge [
    source 8
    target 935
  ]
  edge [
    source 8
    target 936
  ]
  edge [
    source 8
    target 937
  ]
  edge [
    source 8
    target 938
  ]
  edge [
    source 8
    target 939
  ]
  edge [
    source 8
    target 940
  ]
  edge [
    source 8
    target 941
  ]
  edge [
    source 8
    target 942
  ]
  edge [
    source 8
    target 943
  ]
  edge [
    source 8
    target 229
  ]
  edge [
    source 8
    target 944
  ]
  edge [
    source 8
    target 945
  ]
  edge [
    source 8
    target 946
  ]
  edge [
    source 8
    target 947
  ]
  edge [
    source 8
    target 948
  ]
  edge [
    source 8
    target 949
  ]
  edge [
    source 8
    target 950
  ]
  edge [
    source 8
    target 951
  ]
  edge [
    source 8
    target 952
  ]
  edge [
    source 8
    target 953
  ]
  edge [
    source 8
    target 954
  ]
  edge [
    source 8
    target 955
  ]
  edge [
    source 8
    target 956
  ]
  edge [
    source 8
    target 957
  ]
  edge [
    source 8
    target 958
  ]
  edge [
    source 8
    target 959
  ]
  edge [
    source 8
    target 960
  ]
  edge [
    source 8
    target 961
  ]
  edge [
    source 8
    target 962
  ]
  edge [
    source 8
    target 963
  ]
  edge [
    source 8
    target 964
  ]
  edge [
    source 8
    target 965
  ]
  edge [
    source 8
    target 966
  ]
  edge [
    source 8
    target 967
  ]
  edge [
    source 8
    target 968
  ]
  edge [
    source 8
    target 969
  ]
  edge [
    source 8
    target 970
  ]
  edge [
    source 8
    target 971
  ]
  edge [
    source 8
    target 972
  ]
  edge [
    source 8
    target 973
  ]
  edge [
    source 8
    target 974
  ]
  edge [
    source 8
    target 975
  ]
  edge [
    source 8
    target 216
  ]
  edge [
    source 8
    target 976
  ]
  edge [
    source 8
    target 977
  ]
  edge [
    source 8
    target 978
  ]
  edge [
    source 8
    target 979
  ]
  edge [
    source 8
    target 980
  ]
  edge [
    source 8
    target 981
  ]
  edge [
    source 8
    target 982
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 1038
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 45
  ]
  edge [
    source 10
    target 983
  ]
  edge [
    source 10
    target 984
  ]
  edge [
    source 10
    target 985
  ]
  edge [
    source 10
    target 986
  ]
  edge [
    source 10
    target 750
  ]
  edge [
    source 10
    target 987
  ]
  edge [
    source 10
    target 988
  ]
  edge [
    source 10
    target 989
  ]
  edge [
    source 10
    target 990
  ]
  edge [
    source 10
    target 992
  ]
  edge [
    source 82
    target 991
  ]
  edge [
    source 82
    target 1038
  ]
  edge [
    source 261
    target 1009
  ]
  edge [
    source 410
    target 1024
  ]
  edge [
    source 993
    target 994
  ]
  edge [
    source 993
    target 995
  ]
  edge [
    source 994
    target 995
  ]
  edge [
    source 996
    target 997
  ]
  edge [
    source 996
    target 998
  ]
  edge [
    source 996
    target 999
  ]
  edge [
    source 997
    target 998
  ]
  edge [
    source 997
    target 999
  ]
  edge [
    source 998
    target 999
  ]
  edge [
    source 1000
    target 1001
  ]
  edge [
    source 1000
    target 1002
  ]
  edge [
    source 1000
    target 1025
  ]
  edge [
    source 1001
    target 1002
  ]
  edge [
    source 1003
    target 1004
  ]
  edge [
    source 1005
    target 1006
  ]
  edge [
    source 1007
    target 1008
  ]
  edge [
    source 1010
    target 1011
  ]
  edge [
    source 1012
    target 1013
  ]
  edge [
    source 1014
    target 1015
  ]
  edge [
    source 1016
    target 1017
  ]
  edge [
    source 1018
    target 1019
  ]
  edge [
    source 1020
    target 1021
  ]
  edge [
    source 1022
    target 1023
  ]
  edge [
    source 1022
    target 1039
  ]
  edge [
    source 1025
    target 1026
  ]
  edge [
    source 1027
    target 1028
  ]
  edge [
    source 1029
    target 1030
  ]
  edge [
    source 1029
    target 1031
  ]
  edge [
    source 1030
    target 1031
  ]
  edge [
    source 1032
    target 1033
  ]
  edge [
    source 1032
    target 1034
  ]
  edge [
    source 1032
    target 1035
  ]
  edge [
    source 1032
    target 1036
  ]
  edge [
    source 1032
    target 1037
  ]
  edge [
    source 1033
    target 1034
  ]
  edge [
    source 1033
    target 1035
  ]
  edge [
    source 1033
    target 1036
  ]
  edge [
    source 1033
    target 1037
  ]
  edge [
    source 1034
    target 1035
  ]
  edge [
    source 1034
    target 1036
  ]
  edge [
    source 1034
    target 1037
  ]
  edge [
    source 1035
    target 1036
  ]
  edge [
    source 1035
    target 1037
  ]
  edge [
    source 1036
    target 1037
  ]
]
