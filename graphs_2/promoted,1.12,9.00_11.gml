graph [
  node [
    id 0
    label "po&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "rok"
    origin "text"
  ]
  node [
    id 2
    label "ograniczenie"
    origin "text"
  ]
  node [
    id 3
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "forma"
    origin "text"
  ]
  node [
    id 5
    label "praca"
    origin "text"
  ]
  node [
    id 6
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 7
    label "wymiar"
    origin "text"
  ]
  node [
    id 8
    label "godzina"
    origin "text"
  ]
  node [
    id 9
    label "miesi&#261;c"
    origin "text"
  ]
  node [
    id 10
    label "taka"
    origin "text"
  ]
  node [
    id 11
    label "kara"
    origin "text"
  ]
  node [
    id 12
    label "orzec"
    origin "text"
  ]
  node [
    id 13
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 14
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 15
    label "okr&#281;gowy"
    origin "text"
  ]
  node [
    id 16
    label "gda&#324;ski"
    origin "text"
  ]
  node [
    id 17
    label "dla"
    origin "text"
  ]
  node [
    id 18
    label "wnuk"
    origin "text"
  ]
  node [
    id 19
    label "lech"
    origin "text"
  ]
  node [
    id 20
    label "wa&#322;&#281;sa"
    origin "text"
  ]
  node [
    id 21
    label "fragment"
  ]
  node [
    id 22
    label "str&#243;j"
  ]
  node [
    id 23
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 24
    label "utw&#243;r"
  ]
  node [
    id 25
    label "gorset"
  ]
  node [
    id 26
    label "zrzucenie"
  ]
  node [
    id 27
    label "znoszenie"
  ]
  node [
    id 28
    label "kr&#243;j"
  ]
  node [
    id 29
    label "struktura"
  ]
  node [
    id 30
    label "ubranie_si&#281;"
  ]
  node [
    id 31
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 32
    label "znosi&#263;"
  ]
  node [
    id 33
    label "pochodzi&#263;"
  ]
  node [
    id 34
    label "zrzuci&#263;"
  ]
  node [
    id 35
    label "pasmanteria"
  ]
  node [
    id 36
    label "pochodzenie"
  ]
  node [
    id 37
    label "za&#322;o&#380;y&#263;"
  ]
  node [
    id 38
    label "odzie&#380;"
  ]
  node [
    id 39
    label "ubiera&#263;_si&#281;"
  ]
  node [
    id 40
    label "wyko&#324;czenie"
  ]
  node [
    id 41
    label "nosi&#263;"
  ]
  node [
    id 42
    label "zasada"
  ]
  node [
    id 43
    label "w&#322;o&#380;enie"
  ]
  node [
    id 44
    label "garderoba"
  ]
  node [
    id 45
    label "odziewek"
  ]
  node [
    id 46
    label "p&#243;&#322;rocze"
  ]
  node [
    id 47
    label "martwy_sezon"
  ]
  node [
    id 48
    label "kalendarz"
  ]
  node [
    id 49
    label "cykl_astronomiczny"
  ]
  node [
    id 50
    label "lata"
  ]
  node [
    id 51
    label "pora_roku"
  ]
  node [
    id 52
    label "stulecie"
  ]
  node [
    id 53
    label "kurs"
  ]
  node [
    id 54
    label "czas"
  ]
  node [
    id 55
    label "jubileusz"
  ]
  node [
    id 56
    label "grupa"
  ]
  node [
    id 57
    label "kwarta&#322;"
  ]
  node [
    id 58
    label "summer"
  ]
  node [
    id 59
    label "odm&#322;adzanie"
  ]
  node [
    id 60
    label "liga"
  ]
  node [
    id 61
    label "jednostka_systematyczna"
  ]
  node [
    id 62
    label "asymilowanie"
  ]
  node [
    id 63
    label "gromada"
  ]
  node [
    id 64
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 65
    label "asymilowa&#263;"
  ]
  node [
    id 66
    label "egzemplarz"
  ]
  node [
    id 67
    label "Entuzjastki"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "kompozycja"
  ]
  node [
    id 70
    label "Terranie"
  ]
  node [
    id 71
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 72
    label "category"
  ]
  node [
    id 73
    label "pakiet_klimatyczny"
  ]
  node [
    id 74
    label "oddzia&#322;"
  ]
  node [
    id 75
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 76
    label "cz&#261;steczka"
  ]
  node [
    id 77
    label "stage_set"
  ]
  node [
    id 78
    label "type"
  ]
  node [
    id 79
    label "specgrupa"
  ]
  node [
    id 80
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 81
    label "&#346;wietliki"
  ]
  node [
    id 82
    label "odm&#322;odzenie"
  ]
  node [
    id 83
    label "Eurogrupa"
  ]
  node [
    id 84
    label "odm&#322;adza&#263;"
  ]
  node [
    id 85
    label "formacja_geologiczna"
  ]
  node [
    id 86
    label "harcerze_starsi"
  ]
  node [
    id 87
    label "poprzedzanie"
  ]
  node [
    id 88
    label "czasoprzestrze&#324;"
  ]
  node [
    id 89
    label "laba"
  ]
  node [
    id 90
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 91
    label "chronometria"
  ]
  node [
    id 92
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 93
    label "rachuba_czasu"
  ]
  node [
    id 94
    label "przep&#322;ywanie"
  ]
  node [
    id 95
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 96
    label "czasokres"
  ]
  node [
    id 97
    label "odczyt"
  ]
  node [
    id 98
    label "chwila"
  ]
  node [
    id 99
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 100
    label "dzieje"
  ]
  node [
    id 101
    label "kategoria_gramatyczna"
  ]
  node [
    id 102
    label "poprzedzenie"
  ]
  node [
    id 103
    label "trawienie"
  ]
  node [
    id 104
    label "period"
  ]
  node [
    id 105
    label "okres_czasu"
  ]
  node [
    id 106
    label "poprzedza&#263;"
  ]
  node [
    id 107
    label "schy&#322;ek"
  ]
  node [
    id 108
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 109
    label "odwlekanie_si&#281;"
  ]
  node [
    id 110
    label "zegar"
  ]
  node [
    id 111
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 112
    label "czwarty_wymiar"
  ]
  node [
    id 113
    label "koniugacja"
  ]
  node [
    id 114
    label "Zeitgeist"
  ]
  node [
    id 115
    label "trawi&#263;"
  ]
  node [
    id 116
    label "pogoda"
  ]
  node [
    id 117
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 118
    label "poprzedzi&#263;"
  ]
  node [
    id 119
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 120
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 121
    label "time_period"
  ]
  node [
    id 122
    label "tydzie&#324;"
  ]
  node [
    id 123
    label "miech"
  ]
  node [
    id 124
    label "ksi&#281;&#380;yc"
  ]
  node [
    id 125
    label "kalendy"
  ]
  node [
    id 126
    label "term"
  ]
  node [
    id 127
    label "rok_akademicki"
  ]
  node [
    id 128
    label "rok_szkolny"
  ]
  node [
    id 129
    label "semester"
  ]
  node [
    id 130
    label "anniwersarz"
  ]
  node [
    id 131
    label "rocznica"
  ]
  node [
    id 132
    label "obszar"
  ]
  node [
    id 133
    label "&#263;wier&#263;wiecze"
  ]
  node [
    id 134
    label "long_time"
  ]
  node [
    id 135
    label "p&#243;&#322;wiecze"
  ]
  node [
    id 136
    label "almanac"
  ]
  node [
    id 137
    label "rozk&#322;ad"
  ]
  node [
    id 138
    label "wydawnictwo"
  ]
  node [
    id 139
    label "Juliusz_Cezar"
  ]
  node [
    id 140
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 141
    label "zwy&#380;kowanie"
  ]
  node [
    id 142
    label "papier_warto&#347;ciowy"
  ]
  node [
    id 143
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 144
    label "zaj&#281;cia"
  ]
  node [
    id 145
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 146
    label "trasa"
  ]
  node [
    id 147
    label "przeorientowywanie"
  ]
  node [
    id 148
    label "przejazd"
  ]
  node [
    id 149
    label "kierunek"
  ]
  node [
    id 150
    label "przeorientowywa&#263;"
  ]
  node [
    id 151
    label "nauka"
  ]
  node [
    id 152
    label "przeorientowanie"
  ]
  node [
    id 153
    label "klasa"
  ]
  node [
    id 154
    label "zni&#380;kowa&#263;"
  ]
  node [
    id 155
    label "przeorientowa&#263;"
  ]
  node [
    id 156
    label "manner"
  ]
  node [
    id 157
    label "course"
  ]
  node [
    id 158
    label "passage"
  ]
  node [
    id 159
    label "zni&#380;kowanie"
  ]
  node [
    id 160
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 161
    label "seria"
  ]
  node [
    id 162
    label "stawka"
  ]
  node [
    id 163
    label "way"
  ]
  node [
    id 164
    label "w&#281;dr&#243;wka"
  ]
  node [
    id 165
    label "spos&#243;b"
  ]
  node [
    id 166
    label "deprecjacja"
  ]
  node [
    id 167
    label "cedu&#322;a"
  ]
  node [
    id 168
    label "zwy&#380;kowa&#263;"
  ]
  node [
    id 169
    label "drive"
  ]
  node [
    id 170
    label "bearing"
  ]
  node [
    id 171
    label "Lira"
  ]
  node [
    id 172
    label "g&#322;upstwo"
  ]
  node [
    id 173
    label "pomroczno&#347;&#263;_jasna"
  ]
  node [
    id 174
    label "prevention"
  ]
  node [
    id 175
    label "pomiarkowanie"
  ]
  node [
    id 176
    label "przeszkoda"
  ]
  node [
    id 177
    label "intelekt"
  ]
  node [
    id 178
    label "zmniejszenie"
  ]
  node [
    id 179
    label "reservation"
  ]
  node [
    id 180
    label "przekroczenie"
  ]
  node [
    id 181
    label "finlandyzacja"
  ]
  node [
    id 182
    label "otoczenie"
  ]
  node [
    id 183
    label "osielstwo"
  ]
  node [
    id 184
    label "zdyskryminowanie"
  ]
  node [
    id 185
    label "warunek"
  ]
  node [
    id 186
    label "limitation"
  ]
  node [
    id 187
    label "cecha"
  ]
  node [
    id 188
    label "przekroczy&#263;"
  ]
  node [
    id 189
    label "przekraczanie"
  ]
  node [
    id 190
    label "przekracza&#263;"
  ]
  node [
    id 191
    label "barrier"
  ]
  node [
    id 192
    label "dzielenie"
  ]
  node [
    id 193
    label "je&#378;dziectwo"
  ]
  node [
    id 194
    label "obstruction"
  ]
  node [
    id 195
    label "trudno&#347;&#263;"
  ]
  node [
    id 196
    label "bieg_prze&#322;ajowy"
  ]
  node [
    id 197
    label "podzielenie"
  ]
  node [
    id 198
    label "condition"
  ]
  node [
    id 199
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 200
    label "za&#322;o&#380;enie"
  ]
  node [
    id 201
    label "faktor"
  ]
  node [
    id 202
    label "agent"
  ]
  node [
    id 203
    label "ekspozycja"
  ]
  node [
    id 204
    label "umowa"
  ]
  node [
    id 205
    label "charakterystyka"
  ]
  node [
    id 206
    label "m&#322;ot"
  ]
  node [
    id 207
    label "znak"
  ]
  node [
    id 208
    label "drzewo"
  ]
  node [
    id 209
    label "pr&#243;ba"
  ]
  node [
    id 210
    label "attribute"
  ]
  node [
    id 211
    label "marka"
  ]
  node [
    id 212
    label "umys&#322;"
  ]
  node [
    id 213
    label "wiedza"
  ]
  node [
    id 214
    label "noosfera"
  ]
  node [
    id 215
    label "odwodnienie"
  ]
  node [
    id 216
    label "relaxation"
  ]
  node [
    id 217
    label "zmiana"
  ]
  node [
    id 218
    label "zmienienie"
  ]
  node [
    id 219
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 220
    label "okrycie"
  ]
  node [
    id 221
    label "class"
  ]
  node [
    id 222
    label "spowodowanie"
  ]
  node [
    id 223
    label "background"
  ]
  node [
    id 224
    label "zdarzenie_si&#281;"
  ]
  node [
    id 225
    label "crack"
  ]
  node [
    id 226
    label "cortege"
  ]
  node [
    id 227
    label "okolica"
  ]
  node [
    id 228
    label "czynno&#347;&#263;"
  ]
  node [
    id 229
    label "huczek"
  ]
  node [
    id 230
    label "zrobienie"
  ]
  node [
    id 231
    label "zjawisko"
  ]
  node [
    id 232
    label "proces"
  ]
  node [
    id 233
    label "mini&#281;cie"
  ]
  node [
    id 234
    label "przepuszczenie"
  ]
  node [
    id 235
    label "discourtesy"
  ]
  node [
    id 236
    label "przebycie"
  ]
  node [
    id 237
    label "transgression"
  ]
  node [
    id 238
    label "transgresja"
  ]
  node [
    id 239
    label "emergence"
  ]
  node [
    id 240
    label "offense"
  ]
  node [
    id 241
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 242
    label "przeby&#263;"
  ]
  node [
    id 243
    label "open"
  ]
  node [
    id 244
    label "min&#261;&#263;"
  ]
  node [
    id 245
    label "zrobi&#263;"
  ]
  node [
    id 246
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 247
    label "cut"
  ]
  node [
    id 248
    label "pique"
  ]
  node [
    id 249
    label "robi&#263;"
  ]
  node [
    id 250
    label "przebywa&#263;"
  ]
  node [
    id 251
    label "conflict"
  ]
  node [
    id 252
    label "transgress"
  ]
  node [
    id 253
    label "appear"
  ]
  node [
    id 254
    label "osi&#261;ga&#263;"
  ]
  node [
    id 255
    label "mija&#263;"
  ]
  node [
    id 256
    label "przepuszczanie"
  ]
  node [
    id 257
    label "powodowanie"
  ]
  node [
    id 258
    label "przebywanie"
  ]
  node [
    id 259
    label "robienie"
  ]
  node [
    id 260
    label "misdemeanor"
  ]
  node [
    id 261
    label "passing"
  ]
  node [
    id 262
    label "egress"
  ]
  node [
    id 263
    label "mijanie"
  ]
  node [
    id 264
    label "osi&#261;ganie"
  ]
  node [
    id 265
    label "g&#322;upota"
  ]
  node [
    id 266
    label "farmazon"
  ]
  node [
    id 267
    label "wypowied&#378;"
  ]
  node [
    id 268
    label "banalny"
  ]
  node [
    id 269
    label "sofcik"
  ]
  node [
    id 270
    label "czyn"
  ]
  node [
    id 271
    label "szczeg&#243;&#322;"
  ]
  node [
    id 272
    label "baj&#281;da"
  ]
  node [
    id 273
    label "nonsense"
  ]
  node [
    id 274
    label "g&#243;wno"
  ]
  node [
    id 275
    label "stupidity"
  ]
  node [
    id 276
    label "furda"
  ]
  node [
    id 277
    label "zorientowanie_si&#281;"
  ]
  node [
    id 278
    label "skrzywdzenie"
  ]
  node [
    id 279
    label "freedom"
  ]
  node [
    id 280
    label "absolutno&#347;&#263;"
  ]
  node [
    id 281
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 282
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 283
    label "obecno&#347;&#263;"
  ]
  node [
    id 284
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 285
    label "uwi&#281;zienie"
  ]
  node [
    id 286
    label "status"
  ]
  node [
    id 287
    label "relacja"
  ]
  node [
    id 288
    label "independence"
  ]
  node [
    id 289
    label "charakter"
  ]
  node [
    id 290
    label "brak"
  ]
  node [
    id 291
    label "stan"
  ]
  node [
    id 292
    label "being"
  ]
  node [
    id 293
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 294
    label "monarchiczno&#347;&#263;"
  ]
  node [
    id 295
    label "zabranie"
  ]
  node [
    id 296
    label "uniemo&#380;liwienie"
  ]
  node [
    id 297
    label "captivity"
  ]
  node [
    id 298
    label "perpetrate"
  ]
  node [
    id 299
    label "zabra&#263;"
  ]
  node [
    id 300
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 301
    label "kszta&#322;t"
  ]
  node [
    id 302
    label "temat"
  ]
  node [
    id 303
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 304
    label "poznanie"
  ]
  node [
    id 305
    label "leksem"
  ]
  node [
    id 306
    label "dzie&#322;o"
  ]
  node [
    id 307
    label "blaszka"
  ]
  node [
    id 308
    label "poj&#281;cie"
  ]
  node [
    id 309
    label "kantyzm"
  ]
  node [
    id 310
    label "zdolno&#347;&#263;"
  ]
  node [
    id 311
    label "do&#322;ek"
  ]
  node [
    id 312
    label "zawarto&#347;&#263;"
  ]
  node [
    id 313
    label "gwiazda"
  ]
  node [
    id 314
    label "formality"
  ]
  node [
    id 315
    label "wygl&#261;d"
  ]
  node [
    id 316
    label "mode"
  ]
  node [
    id 317
    label "morfem"
  ]
  node [
    id 318
    label "rdze&#324;"
  ]
  node [
    id 319
    label "posta&#263;"
  ]
  node [
    id 320
    label "kielich"
  ]
  node [
    id 321
    label "ornamentyka"
  ]
  node [
    id 322
    label "pasmo"
  ]
  node [
    id 323
    label "zwyczaj"
  ]
  node [
    id 324
    label "punkt_widzenia"
  ]
  node [
    id 325
    label "g&#322;owa"
  ]
  node [
    id 326
    label "naczynie"
  ]
  node [
    id 327
    label "p&#322;at"
  ]
  node [
    id 328
    label "maszyna_drukarska"
  ]
  node [
    id 329
    label "obiekt"
  ]
  node [
    id 330
    label "style"
  ]
  node [
    id 331
    label "linearno&#347;&#263;"
  ]
  node [
    id 332
    label "wyra&#380;enie"
  ]
  node [
    id 333
    label "formacja"
  ]
  node [
    id 334
    label "spirala"
  ]
  node [
    id 335
    label "dyspozycja"
  ]
  node [
    id 336
    label "odmiana"
  ]
  node [
    id 337
    label "dzi&#281;kowa&#263;"
  ]
  node [
    id 338
    label "wz&#243;r"
  ]
  node [
    id 339
    label "October"
  ]
  node [
    id 340
    label "creation"
  ]
  node [
    id 341
    label "p&#281;tla"
  ]
  node [
    id 342
    label "arystotelizm"
  ]
  node [
    id 343
    label "szablon"
  ]
  node [
    id 344
    label "miniatura"
  ]
  node [
    id 345
    label "acquaintance"
  ]
  node [
    id 346
    label "zawarcie_znajomo&#347;ci"
  ]
  node [
    id 347
    label "spotkanie"
  ]
  node [
    id 348
    label "nauczenie_si&#281;"
  ]
  node [
    id 349
    label "poczucie"
  ]
  node [
    id 350
    label "knowing"
  ]
  node [
    id 351
    label "zapoznanie_si&#281;"
  ]
  node [
    id 352
    label "wy&#347;wiadczenie"
  ]
  node [
    id 353
    label "inclusion"
  ]
  node [
    id 354
    label "zrozumienie"
  ]
  node [
    id 355
    label "zawarcie"
  ]
  node [
    id 356
    label "designation"
  ]
  node [
    id 357
    label "umo&#380;liwienie"
  ]
  node [
    id 358
    label "sensing"
  ]
  node [
    id 359
    label "rozr&#243;&#380;nienie"
  ]
  node [
    id 360
    label "gathering"
  ]
  node [
    id 361
    label "zapoznanie"
  ]
  node [
    id 362
    label "znajomy"
  ]
  node [
    id 363
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 364
    label "obrazowanie"
  ]
  node [
    id 365
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 366
    label "dorobek"
  ]
  node [
    id 367
    label "tre&#347;&#263;"
  ]
  node [
    id 368
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 369
    label "retrospektywa"
  ]
  node [
    id 370
    label "works"
  ]
  node [
    id 371
    label "tekst"
  ]
  node [
    id 372
    label "tetralogia"
  ]
  node [
    id 373
    label "komunikat"
  ]
  node [
    id 374
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 375
    label "mutant"
  ]
  node [
    id 376
    label "rewizja"
  ]
  node [
    id 377
    label "gramatyka"
  ]
  node [
    id 378
    label "typ"
  ]
  node [
    id 379
    label "paradygmat"
  ]
  node [
    id 380
    label "change"
  ]
  node [
    id 381
    label "podgatunek"
  ]
  node [
    id 382
    label "ferment"
  ]
  node [
    id 383
    label "rasa"
  ]
  node [
    id 384
    label "pos&#322;uchanie"
  ]
  node [
    id 385
    label "skumanie"
  ]
  node [
    id 386
    label "orientacja"
  ]
  node [
    id 387
    label "wytw&#243;r"
  ]
  node [
    id 388
    label "zorientowanie"
  ]
  node [
    id 389
    label "teoria"
  ]
  node [
    id 390
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 391
    label "clasp"
  ]
  node [
    id 392
    label "przem&#243;wienie"
  ]
  node [
    id 393
    label "idealizm"
  ]
  node [
    id 394
    label "szko&#322;a"
  ]
  node [
    id 395
    label "koncepcja"
  ]
  node [
    id 396
    label "imperatyw_kategoryczny"
  ]
  node [
    id 397
    label "signal"
  ]
  node [
    id 398
    label "przedmiot"
  ]
  node [
    id 399
    label "li&#347;&#263;"
  ]
  node [
    id 400
    label "tw&#243;r"
  ]
  node [
    id 401
    label "odznaczenie"
  ]
  node [
    id 402
    label "kapelusz"
  ]
  node [
    id 403
    label "Arktur"
  ]
  node [
    id 404
    label "Gwiazda_Polarna"
  ]
  node [
    id 405
    label "agregatka"
  ]
  node [
    id 406
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 407
    label "uk&#322;ad_gwiazd"
  ]
  node [
    id 408
    label "S&#322;o&#324;ce"
  ]
  node [
    id 409
    label "Nibiru"
  ]
  node [
    id 410
    label "konstelacja"
  ]
  node [
    id 411
    label "ornament"
  ]
  node [
    id 412
    label "delta_Scuti"
  ]
  node [
    id 413
    label "&#347;wiat&#322;o"
  ]
  node [
    id 414
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 415
    label "s&#322;awa"
  ]
  node [
    id 416
    label "promie&#324;"
  ]
  node [
    id 417
    label "star"
  ]
  node [
    id 418
    label "gwiazdosz"
  ]
  node [
    id 419
    label "zlot_gwia&#378;dzisty"
  ]
  node [
    id 420
    label "asocjacja_gwiazd"
  ]
  node [
    id 421
    label "supergrupa"
  ]
  node [
    id 422
    label "sid&#322;a"
  ]
  node [
    id 423
    label "ko&#322;o"
  ]
  node [
    id 424
    label "p&#281;tlica"
  ]
  node [
    id 425
    label "hank"
  ]
  node [
    id 426
    label "zawi&#261;zywa&#263;"
  ]
  node [
    id 427
    label "akrobacja_lotnicza"
  ]
  node [
    id 428
    label "zawi&#261;zywanie"
  ]
  node [
    id 429
    label "z&#322;&#261;czenie"
  ]
  node [
    id 430
    label "zawi&#261;zanie"
  ]
  node [
    id 431
    label "arrest"
  ]
  node [
    id 432
    label "zawi&#261;za&#263;"
  ]
  node [
    id 433
    label "koniec"
  ]
  node [
    id 434
    label "kra&#324;c&#243;wka"
  ]
  node [
    id 435
    label "roztruchan"
  ]
  node [
    id 436
    label "dzia&#322;ka"
  ]
  node [
    id 437
    label "kwiat"
  ]
  node [
    id 438
    label "puch_kielichowy"
  ]
  node [
    id 439
    label "Graal"
  ]
  node [
    id 440
    label "pryncypa&#322;"
  ]
  node [
    id 441
    label "nerw_j&#281;zykowo-gard&#322;owy"
  ]
  node [
    id 442
    label "przew&#243;d_nosowo-&#322;zowy"
  ]
  node [
    id 443
    label "cz&#322;owiek"
  ]
  node [
    id 444
    label "kierowa&#263;"
  ]
  node [
    id 445
    label "alkohol"
  ]
  node [
    id 446
    label "&#380;ycie"
  ]
  node [
    id 447
    label "t&#281;tnica_twarzowa"
  ]
  node [
    id 448
    label "&#380;y&#322;a_twarzowa"
  ]
  node [
    id 449
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 450
    label "sztuka"
  ]
  node [
    id 451
    label "dekiel"
  ]
  node [
    id 452
    label "ro&#347;lina"
  ]
  node [
    id 453
    label "&#347;ci&#281;cie"
  ]
  node [
    id 454
    label "&#347;ci&#261;&#263;"
  ]
  node [
    id 455
    label "&#380;y&#322;a_za&#380;uchwowa"
  ]
  node [
    id 456
    label "&#347;ci&#281;gno"
  ]
  node [
    id 457
    label "byd&#322;o"
  ]
  node [
    id 458
    label "t&#281;tnica_szcz&#281;kowa"
  ]
  node [
    id 459
    label "makrocefalia"
  ]
  node [
    id 460
    label "ucho"
  ]
  node [
    id 461
    label "g&#243;ra"
  ]
  node [
    id 462
    label "m&#243;zg"
  ]
  node [
    id 463
    label "kierownictwo"
  ]
  node [
    id 464
    label "fryzura"
  ]
  node [
    id 465
    label "cia&#322;o"
  ]
  node [
    id 466
    label "cz&#322;onek"
  ]
  node [
    id 467
    label "t&#281;tnica_kr&#281;gowa"
  ]
  node [
    id 468
    label "czaszka"
  ]
  node [
    id 469
    label "t&#281;tnica_&#322;zowa"
  ]
  node [
    id 470
    label "whirl"
  ]
  node [
    id 471
    label "krzywa"
  ]
  node [
    id 472
    label "spiralny"
  ]
  node [
    id 473
    label "nagromadzenie"
  ]
  node [
    id 474
    label "wk&#322;adka"
  ]
  node [
    id 475
    label "spirograf"
  ]
  node [
    id 476
    label "spiral"
  ]
  node [
    id 477
    label "przebieg"
  ]
  node [
    id 478
    label "wydarzenie"
  ]
  node [
    id 479
    label "pas"
  ]
  node [
    id 480
    label "swath"
  ]
  node [
    id 481
    label "streak"
  ]
  node [
    id 482
    label "kana&#322;"
  ]
  node [
    id 483
    label "strip"
  ]
  node [
    id 484
    label "ulica"
  ]
  node [
    id 485
    label "postarzenie"
  ]
  node [
    id 486
    label "postarzanie"
  ]
  node [
    id 487
    label "brzydota"
  ]
  node [
    id 488
    label "portrecista"
  ]
  node [
    id 489
    label "postarza&#263;"
  ]
  node [
    id 490
    label "nadawanie"
  ]
  node [
    id 491
    label "postarzy&#263;"
  ]
  node [
    id 492
    label "widok"
  ]
  node [
    id 493
    label "prostota"
  ]
  node [
    id 494
    label "ubarwienie"
  ]
  node [
    id 495
    label "shape"
  ]
  node [
    id 496
    label "comeliness"
  ]
  node [
    id 497
    label "face"
  ]
  node [
    id 498
    label "kopia"
  ]
  node [
    id 499
    label "obraz"
  ]
  node [
    id 500
    label "ilustracja"
  ]
  node [
    id 501
    label "miniature"
  ]
  node [
    id 502
    label "kawa&#322;ek"
  ]
  node [
    id 503
    label "centrop&#322;at"
  ]
  node [
    id 504
    label "organ"
  ]
  node [
    id 505
    label "airfoil"
  ]
  node [
    id 506
    label "&#347;mig&#322;owiec"
  ]
  node [
    id 507
    label "samolot"
  ]
  node [
    id 508
    label "piece"
  ]
  node [
    id 509
    label "plaster"
  ]
  node [
    id 510
    label "organizacja"
  ]
  node [
    id 511
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 512
    label "sk&#322;ada&#263;"
  ]
  node [
    id 513
    label "odmawia&#263;"
  ]
  node [
    id 514
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 515
    label "wyra&#380;a&#263;"
  ]
  node [
    id 516
    label "thank"
  ]
  node [
    id 517
    label "etykieta"
  ]
  node [
    id 518
    label "areszt"
  ]
  node [
    id 519
    label "golf"
  ]
  node [
    id 520
    label "faza"
  ]
  node [
    id 521
    label "Wybrze&#380;e_Koromandelskie"
  ]
  node [
    id 522
    label "l&#261;d"
  ]
  node [
    id 523
    label "depressive_disorder"
  ]
  node [
    id 524
    label "bruzda"
  ]
  node [
    id 525
    label "Nizina_Nadwi&#347;la&#324;ska"
  ]
  node [
    id 526
    label "Pampa"
  ]
  node [
    id 527
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 528
    label "odlewnictwo"
  ]
  node [
    id 529
    label "za&#322;amanie"
  ]
  node [
    id 530
    label "tomizm"
  ]
  node [
    id 531
    label "akt"
  ]
  node [
    id 532
    label "kalokagatia"
  ]
  node [
    id 533
    label "potencja"
  ]
  node [
    id 534
    label "wordnet"
  ]
  node [
    id 535
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 536
    label "wypowiedzenie"
  ]
  node [
    id 537
    label "s&#322;ownictwo"
  ]
  node [
    id 538
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 539
    label "wykrzyknik"
  ]
  node [
    id 540
    label "pole_semantyczne"
  ]
  node [
    id 541
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 542
    label "pisanie_si&#281;"
  ]
  node [
    id 543
    label "nag&#322;os"
  ]
  node [
    id 544
    label "wyg&#322;os"
  ]
  node [
    id 545
    label "jednostka_leksykalna"
  ]
  node [
    id 546
    label "zaistnie&#263;"
  ]
  node [
    id 547
    label "Osjan"
  ]
  node [
    id 548
    label "kto&#347;"
  ]
  node [
    id 549
    label "&#346;pi&#261;ca_Kr&#243;lewna"
  ]
  node [
    id 550
    label "osobowo&#347;&#263;"
  ]
  node [
    id 551
    label "trim"
  ]
  node [
    id 552
    label "poby&#263;"
  ]
  node [
    id 553
    label "&#379;yd_Wieczny_Tu&#322;acz"
  ]
  node [
    id 554
    label "Aspazja"
  ]
  node [
    id 555
    label "kompleksja"
  ]
  node [
    id 556
    label "wytrzyma&#263;"
  ]
  node [
    id 557
    label "budowa"
  ]
  node [
    id 558
    label "pozosta&#263;"
  ]
  node [
    id 559
    label "point"
  ]
  node [
    id 560
    label "przedstawienie"
  ]
  node [
    id 561
    label "go&#347;&#263;"
  ]
  node [
    id 562
    label "ilo&#347;&#263;"
  ]
  node [
    id 563
    label "wn&#281;trze"
  ]
  node [
    id 564
    label "informacja"
  ]
  node [
    id 565
    label "Ohio"
  ]
  node [
    id 566
    label "wci&#281;cie"
  ]
  node [
    id 567
    label "Nowy_York"
  ]
  node [
    id 568
    label "warstwa"
  ]
  node [
    id 569
    label "samopoczucie"
  ]
  node [
    id 570
    label "Illinois"
  ]
  node [
    id 571
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 572
    label "state"
  ]
  node [
    id 573
    label "Jukatan"
  ]
  node [
    id 574
    label "Kalifornia"
  ]
  node [
    id 575
    label "Wirginia"
  ]
  node [
    id 576
    label "wektor"
  ]
  node [
    id 577
    label "by&#263;"
  ]
  node [
    id 578
    label "Teksas"
  ]
  node [
    id 579
    label "Goa"
  ]
  node [
    id 580
    label "Waszyngton"
  ]
  node [
    id 581
    label "miejsce"
  ]
  node [
    id 582
    label "Massachusetts"
  ]
  node [
    id 583
    label "Alaska"
  ]
  node [
    id 584
    label "Arakan"
  ]
  node [
    id 585
    label "Hawaje"
  ]
  node [
    id 586
    label "Maryland"
  ]
  node [
    id 587
    label "punkt"
  ]
  node [
    id 588
    label "Michigan"
  ]
  node [
    id 589
    label "Arizona"
  ]
  node [
    id 590
    label "Georgia"
  ]
  node [
    id 591
    label "poziom"
  ]
  node [
    id 592
    label "Pensylwania"
  ]
  node [
    id 593
    label "Luizjana"
  ]
  node [
    id 594
    label "Nowy_Meksyk"
  ]
  node [
    id 595
    label "Alabama"
  ]
  node [
    id 596
    label "Kansas"
  ]
  node [
    id 597
    label "Oregon"
  ]
  node [
    id 598
    label "Floryda"
  ]
  node [
    id 599
    label "Oklahoma"
  ]
  node [
    id 600
    label "jednostka_administracyjna"
  ]
  node [
    id 601
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 602
    label "naczynia_po&#322;&#261;czone"
  ]
  node [
    id 603
    label "vessel"
  ]
  node [
    id 604
    label "sprz&#281;t"
  ]
  node [
    id 605
    label "statki"
  ]
  node [
    id 606
    label "rewaskularyzacja"
  ]
  node [
    id 607
    label "ceramika"
  ]
  node [
    id 608
    label "drewno"
  ]
  node [
    id 609
    label "przew&#243;d"
  ]
  node [
    id 610
    label "unaczyni&#263;"
  ]
  node [
    id 611
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 612
    label "receptacle"
  ]
  node [
    id 613
    label "co&#347;"
  ]
  node [
    id 614
    label "budynek"
  ]
  node [
    id 615
    label "thing"
  ]
  node [
    id 616
    label "program"
  ]
  node [
    id 617
    label "rzecz"
  ]
  node [
    id 618
    label "strona"
  ]
  node [
    id 619
    label "posiada&#263;"
  ]
  node [
    id 620
    label "potencja&#322;"
  ]
  node [
    id 621
    label "zapomnienie"
  ]
  node [
    id 622
    label "zapomina&#263;"
  ]
  node [
    id 623
    label "zapominanie"
  ]
  node [
    id 624
    label "ability"
  ]
  node [
    id 625
    label "obliczeniowo"
  ]
  node [
    id 626
    label "zapomnie&#263;"
  ]
  node [
    id 627
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 628
    label "zachowanie"
  ]
  node [
    id 629
    label "kultura_duchowa"
  ]
  node [
    id 630
    label "kultura"
  ]
  node [
    id 631
    label "ceremony"
  ]
  node [
    id 632
    label "sformu&#322;owanie"
  ]
  node [
    id 633
    label "poinformowanie"
  ]
  node [
    id 634
    label "wording"
  ]
  node [
    id 635
    label "oznaczenie"
  ]
  node [
    id 636
    label "znak_j&#281;zykowy"
  ]
  node [
    id 637
    label "zwi&#261;zek_frazeologiczny"
  ]
  node [
    id 638
    label "ozdobnik"
  ]
  node [
    id 639
    label "grupa_sk&#322;adniowa"
  ]
  node [
    id 640
    label "grupa_imienna"
  ]
  node [
    id 641
    label "sformu&#322;owanie_si&#281;"
  ]
  node [
    id 642
    label "wypowiedzenie_si&#281;"
  ]
  node [
    id 643
    label "ujawnienie"
  ]
  node [
    id 644
    label "affirmation"
  ]
  node [
    id 645
    label "zapisanie"
  ]
  node [
    id 646
    label "rzucenie"
  ]
  node [
    id 647
    label "zapis"
  ]
  node [
    id 648
    label "figure"
  ]
  node [
    id 649
    label "mildew"
  ]
  node [
    id 650
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 651
    label "ideal"
  ]
  node [
    id 652
    label "rule"
  ]
  node [
    id 653
    label "ruch"
  ]
  node [
    id 654
    label "dekal"
  ]
  node [
    id 655
    label "motyw"
  ]
  node [
    id 656
    label "projekt"
  ]
  node [
    id 657
    label "mechanika"
  ]
  node [
    id 658
    label "o&#347;"
  ]
  node [
    id 659
    label "usenet"
  ]
  node [
    id 660
    label "rozprz&#261;c"
  ]
  node [
    id 661
    label "cybernetyk"
  ]
  node [
    id 662
    label "podsystem"
  ]
  node [
    id 663
    label "system"
  ]
  node [
    id 664
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 665
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 666
    label "sk&#322;ad"
  ]
  node [
    id 667
    label "systemat"
  ]
  node [
    id 668
    label "konstrukcja"
  ]
  node [
    id 669
    label "model"
  ]
  node [
    id 670
    label "jig"
  ]
  node [
    id 671
    label "drabina_analgetyczna"
  ]
  node [
    id 672
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 673
    label "C"
  ]
  node [
    id 674
    label "D"
  ]
  node [
    id 675
    label "exemplar"
  ]
  node [
    id 676
    label "sprawa"
  ]
  node [
    id 677
    label "wyraz_pochodny"
  ]
  node [
    id 678
    label "zboczenie"
  ]
  node [
    id 679
    label "om&#243;wienie"
  ]
  node [
    id 680
    label "omawia&#263;"
  ]
  node [
    id 681
    label "fraza"
  ]
  node [
    id 682
    label "entity"
  ]
  node [
    id 683
    label "forum"
  ]
  node [
    id 684
    label "topik"
  ]
  node [
    id 685
    label "tematyka"
  ]
  node [
    id 686
    label "w&#261;tek"
  ]
  node [
    id 687
    label "zbaczanie"
  ]
  node [
    id 688
    label "om&#243;wi&#263;"
  ]
  node [
    id 689
    label "omawianie"
  ]
  node [
    id 690
    label "melodia"
  ]
  node [
    id 691
    label "otoczka"
  ]
  node [
    id 692
    label "istota"
  ]
  node [
    id 693
    label "zbacza&#263;"
  ]
  node [
    id 694
    label "zboczy&#263;"
  ]
  node [
    id 695
    label "morpheme"
  ]
  node [
    id 696
    label "rzemios&#322;o_artystyczne"
  ]
  node [
    id 697
    label "figura_stylistyczna"
  ]
  node [
    id 698
    label "decoration"
  ]
  node [
    id 699
    label "dekoracja"
  ]
  node [
    id 700
    label "reaktor_j&#261;drowy"
  ]
  node [
    id 701
    label "magnes"
  ]
  node [
    id 702
    label "spowalniacz"
  ]
  node [
    id 703
    label "transformator"
  ]
  node [
    id 704
    label "mi&#281;kisz"
  ]
  node [
    id 705
    label "marrow"
  ]
  node [
    id 706
    label "pocisk"
  ]
  node [
    id 707
    label "pr&#281;t_kontrolny"
  ]
  node [
    id 708
    label "procesor"
  ]
  node [
    id 709
    label "ch&#322;odziwo"
  ]
  node [
    id 710
    label "uk&#322;ad_nerwowy"
  ]
  node [
    id 711
    label "surowiak"
  ]
  node [
    id 712
    label "kr&#281;gos&#322;up"
  ]
  node [
    id 713
    label "core"
  ]
  node [
    id 714
    label "plan"
  ]
  node [
    id 715
    label "kondycja"
  ]
  node [
    id 716
    label "polecenie"
  ]
  node [
    id 717
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 718
    label "capability"
  ]
  node [
    id 719
    label "prawo"
  ]
  node [
    id 720
    label "Bund"
  ]
  node [
    id 721
    label "Mazowsze"
  ]
  node [
    id 722
    label "PPR"
  ]
  node [
    id 723
    label "Jakobici"
  ]
  node [
    id 724
    label "zesp&#243;&#322;"
  ]
  node [
    id 725
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 726
    label "SLD"
  ]
  node [
    id 727
    label "zespolik"
  ]
  node [
    id 728
    label "Razem"
  ]
  node [
    id 729
    label "PiS"
  ]
  node [
    id 730
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 731
    label "partia"
  ]
  node [
    id 732
    label "Kuomintang"
  ]
  node [
    id 733
    label "ZSL"
  ]
  node [
    id 734
    label "jednostka"
  ]
  node [
    id 735
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 736
    label "rugby"
  ]
  node [
    id 737
    label "AWS"
  ]
  node [
    id 738
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 739
    label "blok"
  ]
  node [
    id 740
    label "PO"
  ]
  node [
    id 741
    label "si&#322;a"
  ]
  node [
    id 742
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 743
    label "Federali&#347;ci"
  ]
  node [
    id 744
    label "PSL"
  ]
  node [
    id 745
    label "wojsko"
  ]
  node [
    id 746
    label "Wigowie"
  ]
  node [
    id 747
    label "ZChN"
  ]
  node [
    id 748
    label "egzekutywa"
  ]
  node [
    id 749
    label "rocznik"
  ]
  node [
    id 750
    label "The_Beatles"
  ]
  node [
    id 751
    label "futbol_ameryka&#324;ski"
  ]
  node [
    id 752
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 753
    label "unit"
  ]
  node [
    id 754
    label "Depeche_Mode"
  ]
  node [
    id 755
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 756
    label "najem"
  ]
  node [
    id 757
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 758
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 759
    label "zak&#322;ad"
  ]
  node [
    id 760
    label "stosunek_pracy"
  ]
  node [
    id 761
    label "benedykty&#324;ski"
  ]
  node [
    id 762
    label "poda&#380;_pracy"
  ]
  node [
    id 763
    label "pracowanie"
  ]
  node [
    id 764
    label "tyrka"
  ]
  node [
    id 765
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 766
    label "zaw&#243;d"
  ]
  node [
    id 767
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 768
    label "tynkarski"
  ]
  node [
    id 769
    label "pracowa&#263;"
  ]
  node [
    id 770
    label "czynnik_produkcji"
  ]
  node [
    id 771
    label "zobowi&#261;zanie"
  ]
  node [
    id 772
    label "siedziba"
  ]
  node [
    id 773
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 774
    label "p&#322;&#243;d"
  ]
  node [
    id 775
    label "work"
  ]
  node [
    id 776
    label "rezultat"
  ]
  node [
    id 777
    label "activity"
  ]
  node [
    id 778
    label "bezproblemowy"
  ]
  node [
    id 779
    label "warunek_lokalowy"
  ]
  node [
    id 780
    label "plac"
  ]
  node [
    id 781
    label "location"
  ]
  node [
    id 782
    label "uwaga"
  ]
  node [
    id 783
    label "przestrze&#324;"
  ]
  node [
    id 784
    label "rz&#261;d"
  ]
  node [
    id 785
    label "stosunek_prawny"
  ]
  node [
    id 786
    label "oblig"
  ]
  node [
    id 787
    label "uregulowa&#263;"
  ]
  node [
    id 788
    label "oddzia&#322;anie"
  ]
  node [
    id 789
    label "occupation"
  ]
  node [
    id 790
    label "duty"
  ]
  node [
    id 791
    label "ugoda_restrukturyzacyjna"
  ]
  node [
    id 792
    label "zapowied&#378;"
  ]
  node [
    id 793
    label "obowi&#261;zek"
  ]
  node [
    id 794
    label "statement"
  ]
  node [
    id 795
    label "zapewnienie"
  ]
  node [
    id 796
    label "miejsce_pracy"
  ]
  node [
    id 797
    label "zak&#322;adka"
  ]
  node [
    id 798
    label "jednostka_organizacyjna"
  ]
  node [
    id 799
    label "instytucja"
  ]
  node [
    id 800
    label "firma"
  ]
  node [
    id 801
    label "company"
  ]
  node [
    id 802
    label "instytut"
  ]
  node [
    id 803
    label "&#321;ubianka"
  ]
  node [
    id 804
    label "dzia&#322;_personalny"
  ]
  node [
    id 805
    label "Kreml"
  ]
  node [
    id 806
    label "Bia&#322;y_Dom"
  ]
  node [
    id 807
    label "Pa&#322;ac_Kultury"
  ]
  node [
    id 808
    label "sadowisko"
  ]
  node [
    id 809
    label "oznaka"
  ]
  node [
    id 810
    label "komplet"
  ]
  node [
    id 811
    label "anatomopatolog"
  ]
  node [
    id 812
    label "zmianka"
  ]
  node [
    id 813
    label "amendment"
  ]
  node [
    id 814
    label "odmienianie"
  ]
  node [
    id 815
    label "tura"
  ]
  node [
    id 816
    label "cierpliwy"
  ]
  node [
    id 817
    label "mozolny"
  ]
  node [
    id 818
    label "wytrwa&#322;y"
  ]
  node [
    id 819
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 820
    label "benedykty&#324;sko"
  ]
  node [
    id 821
    label "typowy"
  ]
  node [
    id 822
    label "po_benedykty&#324;sku"
  ]
  node [
    id 823
    label "endeavor"
  ]
  node [
    id 824
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 825
    label "mie&#263;_miejsce"
  ]
  node [
    id 826
    label "podejmowa&#263;"
  ]
  node [
    id 827
    label "dziama&#263;"
  ]
  node [
    id 828
    label "do"
  ]
  node [
    id 829
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 830
    label "bangla&#263;"
  ]
  node [
    id 831
    label "dobiega&#263;_si&#281;"
  ]
  node [
    id 832
    label "maszyna"
  ]
  node [
    id 833
    label "dzia&#322;a&#263;"
  ]
  node [
    id 834
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 835
    label "tryb"
  ]
  node [
    id 836
    label "funkcjonowa&#263;"
  ]
  node [
    id 837
    label "zawodoznawstwo"
  ]
  node [
    id 838
    label "emocja"
  ]
  node [
    id 839
    label "office"
  ]
  node [
    id 840
    label "kwalifikacje"
  ]
  node [
    id 841
    label "craft"
  ]
  node [
    id 842
    label "przepracowanie_si&#281;"
  ]
  node [
    id 843
    label "zarz&#261;dzanie"
  ]
  node [
    id 844
    label "przepracowywanie_si&#281;"
  ]
  node [
    id 845
    label "podlizanie_si&#281;"
  ]
  node [
    id 846
    label "dopracowanie"
  ]
  node [
    id 847
    label "podlizywanie_si&#281;"
  ]
  node [
    id 848
    label "uruchamianie"
  ]
  node [
    id 849
    label "dzia&#322;anie"
  ]
  node [
    id 850
    label "d&#261;&#380;enie"
  ]
  node [
    id 851
    label "wys&#322;u&#380;enie"
  ]
  node [
    id 852
    label "uruchomienie"
  ]
  node [
    id 853
    label "nakr&#281;canie"
  ]
  node [
    id 854
    label "funkcjonowanie"
  ]
  node [
    id 855
    label "tr&#243;jstronny"
  ]
  node [
    id 856
    label "postaranie_si&#281;"
  ]
  node [
    id 857
    label "odpocz&#281;cie"
  ]
  node [
    id 858
    label "nakr&#281;cenie"
  ]
  node [
    id 859
    label "zatrzymanie"
  ]
  node [
    id 860
    label "spracowanie_si&#281;"
  ]
  node [
    id 861
    label "skakanie"
  ]
  node [
    id 862
    label "zaprz&#281;&#380;enie"
  ]
  node [
    id 863
    label "podtrzymywanie"
  ]
  node [
    id 864
    label "w&#322;&#261;czanie"
  ]
  node [
    id 865
    label "zaprz&#281;ganie"
  ]
  node [
    id 866
    label "podejmowanie"
  ]
  node [
    id 867
    label "wyrabianie"
  ]
  node [
    id 868
    label "dzianie_si&#281;"
  ]
  node [
    id 869
    label "use"
  ]
  node [
    id 870
    label "przepracowanie"
  ]
  node [
    id 871
    label "poruszanie_si&#281;"
  ]
  node [
    id 872
    label "funkcja"
  ]
  node [
    id 873
    label "impact"
  ]
  node [
    id 874
    label "przepracowywanie"
  ]
  node [
    id 875
    label "awansowanie"
  ]
  node [
    id 876
    label "courtship"
  ]
  node [
    id 877
    label "zapracowanie"
  ]
  node [
    id 878
    label "wyrobienie"
  ]
  node [
    id 879
    label "pokr&#281;cenie_si&#281;"
  ]
  node [
    id 880
    label "w&#322;&#261;czenie"
  ]
  node [
    id 881
    label "transakcja"
  ]
  node [
    id 882
    label "biuro"
  ]
  node [
    id 883
    label "lead"
  ]
  node [
    id 884
    label "w&#322;adza"
  ]
  node [
    id 885
    label "spo&#322;ecznie"
  ]
  node [
    id 886
    label "publiczny"
  ]
  node [
    id 887
    label "niepubliczny"
  ]
  node [
    id 888
    label "publicznie"
  ]
  node [
    id 889
    label "upublicznianie"
  ]
  node [
    id 890
    label "jawny"
  ]
  node [
    id 891
    label "upublicznienie"
  ]
  node [
    id 892
    label "parametr"
  ]
  node [
    id 893
    label "liczba"
  ]
  node [
    id 894
    label "dane"
  ]
  node [
    id 895
    label "znaczenie"
  ]
  node [
    id 896
    label "wielko&#347;&#263;"
  ]
  node [
    id 897
    label "dymensja"
  ]
  node [
    id 898
    label "rozmiar"
  ]
  node [
    id 899
    label "rzadko&#347;&#263;"
  ]
  node [
    id 900
    label "zaleta"
  ]
  node [
    id 901
    label "measure"
  ]
  node [
    id 902
    label "opinia"
  ]
  node [
    id 903
    label "w&#322;a&#347;ciwo&#347;&#263;"
  ]
  node [
    id 904
    label "property"
  ]
  node [
    id 905
    label "kartka"
  ]
  node [
    id 906
    label "logowanie"
  ]
  node [
    id 907
    label "plik"
  ]
  node [
    id 908
    label "adres_internetowy"
  ]
  node [
    id 909
    label "linia"
  ]
  node [
    id 910
    label "serwis_internetowy"
  ]
  node [
    id 911
    label "bok"
  ]
  node [
    id 912
    label "skr&#281;canie"
  ]
  node [
    id 913
    label "skr&#281;ca&#263;"
  ]
  node [
    id 914
    label "orientowanie"
  ]
  node [
    id 915
    label "skr&#281;ci&#263;"
  ]
  node [
    id 916
    label "uj&#281;cie"
  ]
  node [
    id 917
    label "ty&#322;"
  ]
  node [
    id 918
    label "layout"
  ]
  node [
    id 919
    label "zorientowa&#263;"
  ]
  node [
    id 920
    label "pagina"
  ]
  node [
    id 921
    label "podmiot"
  ]
  node [
    id 922
    label "orientowa&#263;"
  ]
  node [
    id 923
    label "voice"
  ]
  node [
    id 924
    label "prz&#243;d"
  ]
  node [
    id 925
    label "internet"
  ]
  node [
    id 926
    label "powierzchnia"
  ]
  node [
    id 927
    label "skr&#281;cenie"
  ]
  node [
    id 928
    label "po&#322;o&#380;enie"
  ]
  node [
    id 929
    label "jako&#347;&#263;"
  ]
  node [
    id 930
    label "p&#322;aszczyzna"
  ]
  node [
    id 931
    label "wyk&#322;adnik"
  ]
  node [
    id 932
    label "szczebel"
  ]
  node [
    id 933
    label "wysoko&#347;&#263;"
  ]
  node [
    id 934
    label "ranga"
  ]
  node [
    id 935
    label "odk&#322;adanie"
  ]
  node [
    id 936
    label "liczenie"
  ]
  node [
    id 937
    label "stawianie"
  ]
  node [
    id 938
    label "bycie"
  ]
  node [
    id 939
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 940
    label "assay"
  ]
  node [
    id 941
    label "wskazywanie"
  ]
  node [
    id 942
    label "wyraz"
  ]
  node [
    id 943
    label "gravity"
  ]
  node [
    id 944
    label "weight"
  ]
  node [
    id 945
    label "command"
  ]
  node [
    id 946
    label "odgrywanie_roli"
  ]
  node [
    id 947
    label "okre&#347;lanie"
  ]
  node [
    id 948
    label "edytowa&#263;"
  ]
  node [
    id 949
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 950
    label "spakowanie"
  ]
  node [
    id 951
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 952
    label "pakowa&#263;"
  ]
  node [
    id 953
    label "rekord"
  ]
  node [
    id 954
    label "korelator"
  ]
  node [
    id 955
    label "wyci&#261;ganie"
  ]
  node [
    id 956
    label "pakowanie"
  ]
  node [
    id 957
    label "sekwencjonowa&#263;"
  ]
  node [
    id 958
    label "w&#322;ama&#263;_si&#281;"
  ]
  node [
    id 959
    label "jednostka_informacji"
  ]
  node [
    id 960
    label "evidence"
  ]
  node [
    id 961
    label "w&#322;amywa&#263;_si&#281;"
  ]
  node [
    id 962
    label "rozpakowywanie"
  ]
  node [
    id 963
    label "wyci&#261;ga&#263;"
  ]
  node [
    id 964
    label "rozpakowanie"
  ]
  node [
    id 965
    label "rozpakowywa&#263;"
  ]
  node [
    id 966
    label "konwersja"
  ]
  node [
    id 967
    label "nap&#322;ywanie"
  ]
  node [
    id 968
    label "rozpakowa&#263;"
  ]
  node [
    id 969
    label "spakowa&#263;"
  ]
  node [
    id 970
    label "w&#322;amywanie_si&#281;"
  ]
  node [
    id 971
    label "edytowanie"
  ]
  node [
    id 972
    label "w&#322;amanie_si&#281;"
  ]
  node [
    id 973
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 974
    label "sekwencjonowanie"
  ]
  node [
    id 975
    label "kategoria"
  ]
  node [
    id 976
    label "pierwiastek"
  ]
  node [
    id 977
    label "number"
  ]
  node [
    id 978
    label "kwadrat_magiczny"
  ]
  node [
    id 979
    label "part"
  ]
  node [
    id 980
    label "zmienna"
  ]
  node [
    id 981
    label "time"
  ]
  node [
    id 982
    label "doba"
  ]
  node [
    id 983
    label "p&#243;&#322;godzina"
  ]
  node [
    id 984
    label "jednostka_czasu"
  ]
  node [
    id 985
    label "minuta"
  ]
  node [
    id 986
    label "kwadrans"
  ]
  node [
    id 987
    label "sekunda"
  ]
  node [
    id 988
    label "stopie&#324;"
  ]
  node [
    id 989
    label "design"
  ]
  node [
    id 990
    label "noc"
  ]
  node [
    id 991
    label "dzie&#324;"
  ]
  node [
    id 992
    label "jednostka_geologiczna"
  ]
  node [
    id 993
    label "faza_Ksi&#281;&#380;yca"
  ]
  node [
    id 994
    label "satelita"
  ]
  node [
    id 995
    label "peryselenium"
  ]
  node [
    id 996
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 997
    label "aposelenium"
  ]
  node [
    id 998
    label "kalendarz_ksi&#281;&#380;ycowy"
  ]
  node [
    id 999
    label "Tytan"
  ]
  node [
    id 1000
    label "moon"
  ]
  node [
    id 1001
    label "aparat_fotograficzny"
  ]
  node [
    id 1002
    label "bag"
  ]
  node [
    id 1003
    label "sakwa"
  ]
  node [
    id 1004
    label "torba"
  ]
  node [
    id 1005
    label "przyrz&#261;d"
  ]
  node [
    id 1006
    label "w&#243;r"
  ]
  node [
    id 1007
    label "weekend"
  ]
  node [
    id 1008
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 1009
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 1010
    label "Bangladesz"
  ]
  node [
    id 1011
    label "jednostka_monetarna"
  ]
  node [
    id 1012
    label "Bengal"
  ]
  node [
    id 1013
    label "kwota"
  ]
  node [
    id 1014
    label "nemezis"
  ]
  node [
    id 1015
    label "konsekwencja"
  ]
  node [
    id 1016
    label "punishment"
  ]
  node [
    id 1017
    label "klacz"
  ]
  node [
    id 1018
    label "forfeit"
  ]
  node [
    id 1019
    label "roboty_przymusowe"
  ]
  node [
    id 1020
    label "odczuwa&#263;"
  ]
  node [
    id 1021
    label "zwi&#261;zek_przyczynowy"
  ]
  node [
    id 1022
    label "skrupienie_si&#281;"
  ]
  node [
    id 1023
    label "sta&#322;o&#347;&#263;"
  ]
  node [
    id 1024
    label "skrupia&#263;_si&#281;"
  ]
  node [
    id 1025
    label "odczucie"
  ]
  node [
    id 1026
    label "skrupi&#263;_si&#281;"
  ]
  node [
    id 1027
    label "koszula_Dejaniry"
  ]
  node [
    id 1028
    label "odczuwanie"
  ]
  node [
    id 1029
    label "event"
  ]
  node [
    id 1030
    label "skrupianie_si&#281;"
  ]
  node [
    id 1031
    label "odczu&#263;"
  ]
  node [
    id 1032
    label "ko&#324;"
  ]
  node [
    id 1033
    label "mare"
  ]
  node [
    id 1034
    label "samica"
  ]
  node [
    id 1035
    label "wynie&#347;&#263;"
  ]
  node [
    id 1036
    label "pieni&#261;dze"
  ]
  node [
    id 1037
    label "limit"
  ]
  node [
    id 1038
    label "wynosi&#263;"
  ]
  node [
    id 1039
    label "stwierdzi&#263;"
  ]
  node [
    id 1040
    label "decide"
  ]
  node [
    id 1041
    label "connote"
  ]
  node [
    id 1042
    label "zdecydowa&#263;"
  ]
  node [
    id 1043
    label "sta&#263;_si&#281;"
  ]
  node [
    id 1044
    label "podj&#261;&#263;"
  ]
  node [
    id 1045
    label "determine"
  ]
  node [
    id 1046
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 1047
    label "testify"
  ]
  node [
    id 1048
    label "powiedzie&#263;"
  ]
  node [
    id 1049
    label "uzna&#263;"
  ]
  node [
    id 1050
    label "oznajmi&#263;"
  ]
  node [
    id 1051
    label "declare"
  ]
  node [
    id 1052
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 1053
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 1054
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 1055
    label "teraz"
  ]
  node [
    id 1056
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 1057
    label "jednocze&#347;nie"
  ]
  node [
    id 1058
    label "podejrzany"
  ]
  node [
    id 1059
    label "s&#261;downictwo"
  ]
  node [
    id 1060
    label "court"
  ]
  node [
    id 1061
    label "bronienie"
  ]
  node [
    id 1062
    label "urz&#261;d"
  ]
  node [
    id 1063
    label "oskar&#380;yciel"
  ]
  node [
    id 1064
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 1065
    label "skazany"
  ]
  node [
    id 1066
    label "post&#281;powanie"
  ]
  node [
    id 1067
    label "broni&#263;"
  ]
  node [
    id 1068
    label "my&#347;l"
  ]
  node [
    id 1069
    label "pods&#261;dny"
  ]
  node [
    id 1070
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 1071
    label "obrona"
  ]
  node [
    id 1072
    label "antylogizm"
  ]
  node [
    id 1073
    label "konektyw"
  ]
  node [
    id 1074
    label "&#347;wiadek"
  ]
  node [
    id 1075
    label "procesowicz"
  ]
  node [
    id 1076
    label "whole"
  ]
  node [
    id 1077
    label "skupienie"
  ]
  node [
    id 1078
    label "zabudowania"
  ]
  node [
    id 1079
    label "group"
  ]
  node [
    id 1080
    label "schorzenie"
  ]
  node [
    id 1081
    label "batch"
  ]
  node [
    id 1082
    label "stanowisko"
  ]
  node [
    id 1083
    label "position"
  ]
  node [
    id 1084
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 1085
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1086
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 1087
    label "mianowaniec"
  ]
  node [
    id 1088
    label "dzia&#322;"
  ]
  node [
    id 1089
    label "okienko"
  ]
  node [
    id 1090
    label "przebiec"
  ]
  node [
    id 1091
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 1092
    label "przebiegni&#281;cie"
  ]
  node [
    id 1093
    label "fabu&#322;a"
  ]
  node [
    id 1094
    label "osoba_prawna"
  ]
  node [
    id 1095
    label "Krajowy_Rejestr_Karny"
  ]
  node [
    id 1096
    label "Agencja_Rozwoju_Przemys&#322;u"
  ]
  node [
    id 1097
    label "Europejski_Instytut_Innowacji_i_Technologii"
  ]
  node [
    id 1098
    label "Europejski_Bank_Inwestycyjny"
  ]
  node [
    id 1099
    label "Kasa_Rolniczego_Ubezpieczenia_Spo&#322;ecznego"
  ]
  node [
    id 1100
    label "Fundusze_Unijne"
  ]
  node [
    id 1101
    label "zamyka&#263;"
  ]
  node [
    id 1102
    label "establishment"
  ]
  node [
    id 1103
    label "Europejski_Bank_Odbudowy_i_Rozwoju"
  ]
  node [
    id 1104
    label "Agencja_Rozwoju_Regionalnego"
  ]
  node [
    id 1105
    label "afiliowa&#263;"
  ]
  node [
    id 1106
    label "Zak&#322;ad_Ubezpiecze&#324;_Spo&#322;ecznych"
  ]
  node [
    id 1107
    label "standard"
  ]
  node [
    id 1108
    label "zamykanie"
  ]
  node [
    id 1109
    label "Biuro_Pomocy_Technicznej_i_Wymiany_Infomacji"
  ]
  node [
    id 1110
    label "potencja&#322;_instytucjonalny"
  ]
  node [
    id 1111
    label "thinking"
  ]
  node [
    id 1112
    label "political_orientation"
  ]
  node [
    id 1113
    label "pomys&#322;"
  ]
  node [
    id 1114
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 1115
    label "idea"
  ]
  node [
    id 1116
    label "fantomatyka"
  ]
  node [
    id 1117
    label "sparafrazowanie"
  ]
  node [
    id 1118
    label "pos&#322;ucha&#263;"
  ]
  node [
    id 1119
    label "strawestowa&#263;"
  ]
  node [
    id 1120
    label "sparafrazowa&#263;"
  ]
  node [
    id 1121
    label "kontaktowanie_si&#281;"
  ]
  node [
    id 1122
    label "trawestowa&#263;"
  ]
  node [
    id 1123
    label "parafrazowanie"
  ]
  node [
    id 1124
    label "delimitacja"
  ]
  node [
    id 1125
    label "parafrazowa&#263;"
  ]
  node [
    id 1126
    label "stylizacja"
  ]
  node [
    id 1127
    label "trawestowanie"
  ]
  node [
    id 1128
    label "strawestowanie"
  ]
  node [
    id 1129
    label "kognicja"
  ]
  node [
    id 1130
    label "campaign"
  ]
  node [
    id 1131
    label "rozprawa"
  ]
  node [
    id 1132
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 1133
    label "fashion"
  ]
  node [
    id 1134
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 1135
    label "zmierzanie"
  ]
  node [
    id 1136
    label "przes&#322;anka"
  ]
  node [
    id 1137
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 1138
    label "kazanie"
  ]
  node [
    id 1139
    label "funktor"
  ]
  node [
    id 1140
    label "ob&#380;a&#322;owany"
  ]
  node [
    id 1141
    label "skar&#380;yciel"
  ]
  node [
    id 1142
    label "dysponowanie"
  ]
  node [
    id 1143
    label "dysponowa&#263;"
  ]
  node [
    id 1144
    label "podejrzanie"
  ]
  node [
    id 1145
    label "pos&#261;dzanie"
  ]
  node [
    id 1146
    label "nieprzejrzysty"
  ]
  node [
    id 1147
    label "niepewny"
  ]
  node [
    id 1148
    label "z&#322;y"
  ]
  node [
    id 1149
    label "egzamin"
  ]
  node [
    id 1150
    label "walka"
  ]
  node [
    id 1151
    label "gracz"
  ]
  node [
    id 1152
    label "protection"
  ]
  node [
    id 1153
    label "poparcie"
  ]
  node [
    id 1154
    label "mecz"
  ]
  node [
    id 1155
    label "reakcja"
  ]
  node [
    id 1156
    label "defense"
  ]
  node [
    id 1157
    label "auspices"
  ]
  node [
    id 1158
    label "gra"
  ]
  node [
    id 1159
    label "ochrona"
  ]
  node [
    id 1160
    label "sp&#243;r"
  ]
  node [
    id 1161
    label "manewr"
  ]
  node [
    id 1162
    label "defensive_structure"
  ]
  node [
    id 1163
    label "guard_duty"
  ]
  node [
    id 1164
    label "uczestnik"
  ]
  node [
    id 1165
    label "dru&#380;ba"
  ]
  node [
    id 1166
    label "obserwator"
  ]
  node [
    id 1167
    label "osoba_fizyczna"
  ]
  node [
    id 1168
    label "niedost&#281;pny"
  ]
  node [
    id 1169
    label "obstawanie"
  ]
  node [
    id 1170
    label "adwokatowanie"
  ]
  node [
    id 1171
    label "zdawanie"
  ]
  node [
    id 1172
    label "walczenie"
  ]
  node [
    id 1173
    label "zabezpieczenie"
  ]
  node [
    id 1174
    label "t&#322;umaczenie"
  ]
  node [
    id 1175
    label "parry"
  ]
  node [
    id 1176
    label "or&#281;dowanie"
  ]
  node [
    id 1177
    label "granie"
  ]
  node [
    id 1178
    label "fend"
  ]
  node [
    id 1179
    label "reprezentowa&#263;"
  ]
  node [
    id 1180
    label "zdawa&#263;"
  ]
  node [
    id 1181
    label "czuwa&#263;"
  ]
  node [
    id 1182
    label "preach"
  ]
  node [
    id 1183
    label "chroni&#263;"
  ]
  node [
    id 1184
    label "ujmowa&#263;_si&#281;"
  ]
  node [
    id 1185
    label "walczy&#263;"
  ]
  node [
    id 1186
    label "resist"
  ]
  node [
    id 1187
    label "adwokatowa&#263;"
  ]
  node [
    id 1188
    label "rebuff"
  ]
  node [
    id 1189
    label "udowadnia&#263;"
  ]
  node [
    id 1190
    label "gra&#263;"
  ]
  node [
    id 1191
    label "sprawowa&#263;"
  ]
  node [
    id 1192
    label "refuse"
  ]
  node [
    id 1193
    label "biurko"
  ]
  node [
    id 1194
    label "boks"
  ]
  node [
    id 1195
    label "palestra"
  ]
  node [
    id 1196
    label "Biuro_Lustracyjne"
  ]
  node [
    id 1197
    label "agency"
  ]
  node [
    id 1198
    label "board"
  ]
  node [
    id 1199
    label "pomieszczenie"
  ]
  node [
    id 1200
    label "j&#261;dro"
  ]
  node [
    id 1201
    label "systemik"
  ]
  node [
    id 1202
    label "oprogramowanie"
  ]
  node [
    id 1203
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 1204
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 1205
    label "porz&#261;dek"
  ]
  node [
    id 1206
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 1207
    label "przyn&#281;ta"
  ]
  node [
    id 1208
    label "net"
  ]
  node [
    id 1209
    label "w&#281;dkarstwo"
  ]
  node [
    id 1210
    label "eratem"
  ]
  node [
    id 1211
    label "doktryna"
  ]
  node [
    id 1212
    label "pulpit"
  ]
  node [
    id 1213
    label "metoda"
  ]
  node [
    id 1214
    label "ryba"
  ]
  node [
    id 1215
    label "Leopard"
  ]
  node [
    id 1216
    label "Android"
  ]
  node [
    id 1217
    label "method"
  ]
  node [
    id 1218
    label "podstawa"
  ]
  node [
    id 1219
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 1220
    label "relacja_logiczna"
  ]
  node [
    id 1221
    label "judiciary"
  ]
  node [
    id 1222
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 1223
    label "grupa_dyskusyjna"
  ]
  node [
    id 1224
    label "bazylika"
  ]
  node [
    id 1225
    label "portal"
  ]
  node [
    id 1226
    label "konferencja"
  ]
  node [
    id 1227
    label "agora"
  ]
  node [
    id 1228
    label "renesansowy"
  ]
  node [
    id 1229
    label "zdobny"
  ]
  node [
    id 1230
    label "po_gda&#324;sku"
  ]
  node [
    id 1231
    label "pomorski"
  ]
  node [
    id 1232
    label "barokowy"
  ]
  node [
    id 1233
    label "masywny"
  ]
  node [
    id 1234
    label "polski"
  ]
  node [
    id 1235
    label "regionalny"
  ]
  node [
    id 1236
    label "po_pomorsku"
  ]
  node [
    id 1237
    label "etnolekt"
  ]
  node [
    id 1238
    label "zdobnie"
  ]
  node [
    id 1239
    label "strojny"
  ]
  node [
    id 1240
    label "ozdobny"
  ]
  node [
    id 1241
    label "szesnastowieczny"
  ]
  node [
    id 1242
    label "wielostronny"
  ]
  node [
    id 1243
    label "renesansowo"
  ]
  node [
    id 1244
    label "galiarda"
  ]
  node [
    id 1245
    label "wszechstronny"
  ]
  node [
    id 1246
    label "concerto_grosso"
  ]
  node [
    id 1247
    label "barokowo"
  ]
  node [
    id 1248
    label "siedemnastowieczny"
  ]
  node [
    id 1249
    label "kaprys"
  ]
  node [
    id 1250
    label "du&#380;y"
  ]
  node [
    id 1251
    label "g&#322;o&#347;ny"
  ]
  node [
    id 1252
    label "ci&#281;&#380;ki"
  ]
  node [
    id 1253
    label "pe&#322;ny"
  ]
  node [
    id 1254
    label "masywnie"
  ]
  node [
    id 1255
    label "potomek"
  ]
  node [
    id 1256
    label "wnucz&#281;"
  ]
  node [
    id 1257
    label "krewny"
  ]
  node [
    id 1258
    label "wnucz&#281;ta"
  ]
  node [
    id 1259
    label "w"
  ]
  node [
    id 1260
    label "Lech"
  ]
  node [
    id 1261
    label "Dominik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 1
    target 114
  ]
  edge [
    source 1
    target 115
  ]
  edge [
    source 1
    target 116
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 118
  ]
  edge [
    source 1
    target 119
  ]
  edge [
    source 1
    target 120
  ]
  edge [
    source 1
    target 121
  ]
  edge [
    source 1
    target 122
  ]
  edge [
    source 1
    target 123
  ]
  edge [
    source 1
    target 124
  ]
  edge [
    source 1
    target 125
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 1
    target 127
  ]
  edge [
    source 1
    target 128
  ]
  edge [
    source 1
    target 129
  ]
  edge [
    source 1
    target 130
  ]
  edge [
    source 1
    target 131
  ]
  edge [
    source 1
    target 132
  ]
  edge [
    source 1
    target 133
  ]
  edge [
    source 1
    target 134
  ]
  edge [
    source 1
    target 135
  ]
  edge [
    source 1
    target 136
  ]
  edge [
    source 1
    target 137
  ]
  edge [
    source 1
    target 138
  ]
  edge [
    source 1
    target 139
  ]
  edge [
    source 1
    target 140
  ]
  edge [
    source 1
    target 141
  ]
  edge [
    source 1
    target 142
  ]
  edge [
    source 1
    target 143
  ]
  edge [
    source 1
    target 144
  ]
  edge [
    source 1
    target 145
  ]
  edge [
    source 1
    target 146
  ]
  edge [
    source 1
    target 147
  ]
  edge [
    source 1
    target 148
  ]
  edge [
    source 1
    target 149
  ]
  edge [
    source 1
    target 150
  ]
  edge [
    source 1
    target 151
  ]
  edge [
    source 1
    target 152
  ]
  edge [
    source 1
    target 153
  ]
  edge [
    source 1
    target 154
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 156
  ]
  edge [
    source 1
    target 157
  ]
  edge [
    source 1
    target 158
  ]
  edge [
    source 1
    target 159
  ]
  edge [
    source 1
    target 160
  ]
  edge [
    source 1
    target 161
  ]
  edge [
    source 1
    target 162
  ]
  edge [
    source 1
    target 163
  ]
  edge [
    source 1
    target 164
  ]
  edge [
    source 1
    target 165
  ]
  edge [
    source 1
    target 166
  ]
  edge [
    source 1
    target 167
  ]
  edge [
    source 1
    target 168
  ]
  edge [
    source 1
    target 169
  ]
  edge [
    source 1
    target 170
  ]
  edge [
    source 1
    target 171
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 2
    target 204
  ]
  edge [
    source 2
    target 205
  ]
  edge [
    source 2
    target 206
  ]
  edge [
    source 2
    target 207
  ]
  edge [
    source 2
    target 208
  ]
  edge [
    source 2
    target 209
  ]
  edge [
    source 2
    target 210
  ]
  edge [
    source 2
    target 211
  ]
  edge [
    source 2
    target 212
  ]
  edge [
    source 2
    target 213
  ]
  edge [
    source 2
    target 214
  ]
  edge [
    source 2
    target 215
  ]
  edge [
    source 2
    target 216
  ]
  edge [
    source 2
    target 217
  ]
  edge [
    source 2
    target 218
  ]
  edge [
    source 2
    target 219
  ]
  edge [
    source 2
    target 220
  ]
  edge [
    source 2
    target 221
  ]
  edge [
    source 2
    target 222
  ]
  edge [
    source 2
    target 223
  ]
  edge [
    source 2
    target 224
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 225
  ]
  edge [
    source 2
    target 226
  ]
  edge [
    source 2
    target 227
  ]
  edge [
    source 2
    target 228
  ]
  edge [
    source 2
    target 229
  ]
  edge [
    source 2
    target 230
  ]
  edge [
    source 2
    target 231
  ]
  edge [
    source 2
    target 232
  ]
  edge [
    source 2
    target 233
  ]
  edge [
    source 2
    target 234
  ]
  edge [
    source 2
    target 235
  ]
  edge [
    source 2
    target 236
  ]
  edge [
    source 2
    target 237
  ]
  edge [
    source 2
    target 238
  ]
  edge [
    source 2
    target 239
  ]
  edge [
    source 2
    target 240
  ]
  edge [
    source 2
    target 241
  ]
  edge [
    source 2
    target 242
  ]
  edge [
    source 2
    target 243
  ]
  edge [
    source 2
    target 244
  ]
  edge [
    source 2
    target 245
  ]
  edge [
    source 2
    target 246
  ]
  edge [
    source 2
    target 247
  ]
  edge [
    source 2
    target 248
  ]
  edge [
    source 2
    target 249
  ]
  edge [
    source 2
    target 250
  ]
  edge [
    source 2
    target 251
  ]
  edge [
    source 2
    target 252
  ]
  edge [
    source 2
    target 253
  ]
  edge [
    source 2
    target 254
  ]
  edge [
    source 2
    target 255
  ]
  edge [
    source 2
    target 256
  ]
  edge [
    source 2
    target 257
  ]
  edge [
    source 2
    target 258
  ]
  edge [
    source 2
    target 259
  ]
  edge [
    source 2
    target 260
  ]
  edge [
    source 2
    target 261
  ]
  edge [
    source 2
    target 262
  ]
  edge [
    source 2
    target 263
  ]
  edge [
    source 2
    target 264
  ]
  edge [
    source 2
    target 265
  ]
  edge [
    source 2
    target 266
  ]
  edge [
    source 2
    target 267
  ]
  edge [
    source 2
    target 268
  ]
  edge [
    source 2
    target 269
  ]
  edge [
    source 2
    target 270
  ]
  edge [
    source 2
    target 271
  ]
  edge [
    source 2
    target 272
  ]
  edge [
    source 2
    target 273
  ]
  edge [
    source 2
    target 274
  ]
  edge [
    source 2
    target 275
  ]
  edge [
    source 2
    target 276
  ]
  edge [
    source 2
    target 277
  ]
  edge [
    source 2
    target 278
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 187
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 301
  ]
  edge [
    source 4
    target 302
  ]
  edge [
    source 4
    target 303
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 304
  ]
  edge [
    source 4
    target 305
  ]
  edge [
    source 4
    target 306
  ]
  edge [
    source 4
    target 291
  ]
  edge [
    source 4
    target 307
  ]
  edge [
    source 4
    target 308
  ]
  edge [
    source 4
    target 309
  ]
  edge [
    source 4
    target 310
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 311
  ]
  edge [
    source 4
    target 312
  ]
  edge [
    source 4
    target 313
  ]
  edge [
    source 4
    target 314
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 315
  ]
  edge [
    source 4
    target 316
  ]
  edge [
    source 4
    target 317
  ]
  edge [
    source 4
    target 318
  ]
  edge [
    source 4
    target 319
  ]
  edge [
    source 4
    target 320
  ]
  edge [
    source 4
    target 321
  ]
  edge [
    source 4
    target 322
  ]
  edge [
    source 4
    target 323
  ]
  edge [
    source 4
    target 324
  ]
  edge [
    source 4
    target 325
  ]
  edge [
    source 4
    target 326
  ]
  edge [
    source 4
    target 327
  ]
  edge [
    source 4
    target 328
  ]
  edge [
    source 4
    target 329
  ]
  edge [
    source 4
    target 330
  ]
  edge [
    source 4
    target 331
  ]
  edge [
    source 4
    target 332
  ]
  edge [
    source 4
    target 333
  ]
  edge [
    source 4
    target 334
  ]
  edge [
    source 4
    target 335
  ]
  edge [
    source 4
    target 336
  ]
  edge [
    source 4
    target 337
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 228
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 230
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 231
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
  edge [
    source 4
    target 413
  ]
  edge [
    source 4
    target 414
  ]
  edge [
    source 4
    target 415
  ]
  edge [
    source 4
    target 416
  ]
  edge [
    source 4
    target 417
  ]
  edge [
    source 4
    target 418
  ]
  edge [
    source 4
    target 419
  ]
  edge [
    source 4
    target 420
  ]
  edge [
    source 4
    target 421
  ]
  edge [
    source 4
    target 422
  ]
  edge [
    source 4
    target 423
  ]
  edge [
    source 4
    target 424
  ]
  edge [
    source 4
    target 425
  ]
  edge [
    source 4
    target 426
  ]
  edge [
    source 4
    target 427
  ]
  edge [
    source 4
    target 428
  ]
  edge [
    source 4
    target 429
  ]
  edge [
    source 4
    target 430
  ]
  edge [
    source 4
    target 431
  ]
  edge [
    source 4
    target 432
  ]
  edge [
    source 4
    target 433
  ]
  edge [
    source 4
    target 434
  ]
  edge [
    source 4
    target 435
  ]
  edge [
    source 4
    target 436
  ]
  edge [
    source 4
    target 437
  ]
  edge [
    source 4
    target 438
  ]
  edge [
    source 4
    target 439
  ]
  edge [
    source 4
    target 440
  ]
  edge [
    source 4
    target 441
  ]
  edge [
    source 4
    target 442
  ]
  edge [
    source 4
    target 213
  ]
  edge [
    source 4
    target 443
  ]
  edge [
    source 4
    target 444
  ]
  edge [
    source 4
    target 445
  ]
  edge [
    source 4
    target 446
  ]
  edge [
    source 4
    target 447
  ]
  edge [
    source 4
    target 448
  ]
  edge [
    source 4
    target 449
  ]
  edge [
    source 4
    target 450
  ]
  edge [
    source 4
    target 451
  ]
  edge [
    source 4
    target 452
  ]
  edge [
    source 4
    target 453
  ]
  edge [
    source 4
    target 454
  ]
  edge [
    source 4
    target 455
  ]
  edge [
    source 4
    target 456
  ]
  edge [
    source 4
    target 214
  ]
  edge [
    source 4
    target 457
  ]
  edge [
    source 4
    target 458
  ]
  edge [
    source 4
    target 459
  ]
  edge [
    source 4
    target 460
  ]
  edge [
    source 4
    target 461
  ]
  edge [
    source 4
    target 462
  ]
  edge [
    source 4
    target 463
  ]
  edge [
    source 4
    target 464
  ]
  edge [
    source 4
    target 212
  ]
  edge [
    source 4
    target 465
  ]
  edge [
    source 4
    target 466
  ]
  edge [
    source 4
    target 467
  ]
  edge [
    source 4
    target 468
  ]
  edge [
    source 4
    target 469
  ]
  edge [
    source 4
    target 470
  ]
  edge [
    source 4
    target 471
  ]
  edge [
    source 4
    target 472
  ]
  edge [
    source 4
    target 473
  ]
  edge [
    source 4
    target 474
  ]
  edge [
    source 4
    target 475
  ]
  edge [
    source 4
    target 476
  ]
  edge [
    source 4
    target 477
  ]
  edge [
    source 4
    target 478
  ]
  edge [
    source 4
    target 479
  ]
  edge [
    source 4
    target 480
  ]
  edge [
    source 4
    target 481
  ]
  edge [
    source 4
    target 482
  ]
  edge [
    source 4
    target 483
  ]
  edge [
    source 4
    target 484
  ]
  edge [
    source 4
    target 485
  ]
  edge [
    source 4
    target 486
  ]
  edge [
    source 4
    target 487
  ]
  edge [
    source 4
    target 488
  ]
  edge [
    source 4
    target 489
  ]
  edge [
    source 4
    target 490
  ]
  edge [
    source 4
    target 491
  ]
  edge [
    source 4
    target 492
  ]
  edge [
    source 4
    target 493
  ]
  edge [
    source 4
    target 494
  ]
  edge [
    source 4
    target 495
  ]
  edge [
    source 4
    target 496
  ]
  edge [
    source 4
    target 497
  ]
  edge [
    source 4
    target 289
  ]
  edge [
    source 4
    target 498
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 499
  ]
  edge [
    source 4
    target 500
  ]
  edge [
    source 4
    target 501
  ]
  edge [
    source 4
    target 502
  ]
  edge [
    source 4
    target 503
  ]
  edge [
    source 4
    target 504
  ]
  edge [
    source 4
    target 505
  ]
  edge [
    source 4
    target 506
  ]
  edge [
    source 4
    target 507
  ]
  edge [
    source 4
    target 508
  ]
  edge [
    source 4
    target 509
  ]
  edge [
    source 4
    target 510
  ]
  edge [
    source 4
    target 511
  ]
  edge [
    source 4
    target 512
  ]
  edge [
    source 4
    target 513
  ]
  edge [
    source 4
    target 514
  ]
  edge [
    source 4
    target 515
  ]
  edge [
    source 4
    target 516
  ]
  edge [
    source 4
    target 517
  ]
  edge [
    source 4
    target 518
  ]
  edge [
    source 4
    target 519
  ]
  edge [
    source 4
    target 520
  ]
  edge [
    source 4
    target 521
  ]
  edge [
    source 4
    target 522
  ]
  edge [
    source 4
    target 523
  ]
  edge [
    source 4
    target 524
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 525
  ]
  edge [
    source 4
    target 526
  ]
  edge [
    source 4
    target 527
  ]
  edge [
    source 4
    target 528
  ]
  edge [
    source 4
    target 529
  ]
  edge [
    source 4
    target 530
  ]
  edge [
    source 4
    target 531
  ]
  edge [
    source 4
    target 532
  ]
  edge [
    source 4
    target 533
  ]
  edge [
    source 4
    target 534
  ]
  edge [
    source 4
    target 535
  ]
  edge [
    source 4
    target 536
  ]
  edge [
    source 4
    target 537
  ]
  edge [
    source 4
    target 538
  ]
  edge [
    source 4
    target 539
  ]
  edge [
    source 4
    target 540
  ]
  edge [
    source 4
    target 541
  ]
  edge [
    source 4
    target 542
  ]
  edge [
    source 4
    target 543
  ]
  edge [
    source 4
    target 544
  ]
  edge [
    source 4
    target 545
  ]
  edge [
    source 4
    target 205
  ]
  edge [
    source 4
    target 546
  ]
  edge [
    source 4
    target 547
  ]
  edge [
    source 4
    target 548
  ]
  edge [
    source 4
    target 549
  ]
  edge [
    source 4
    target 550
  ]
  edge [
    source 4
    target 551
  ]
  edge [
    source 4
    target 552
  ]
  edge [
    source 4
    target 553
  ]
  edge [
    source 4
    target 554
  ]
  edge [
    source 4
    target 555
  ]
  edge [
    source 4
    target 556
  ]
  edge [
    source 4
    target 557
  ]
  edge [
    source 4
    target 558
  ]
  edge [
    source 4
    target 559
  ]
  edge [
    source 4
    target 560
  ]
  edge [
    source 4
    target 561
  ]
  edge [
    source 4
    target 562
  ]
  edge [
    source 4
    target 563
  ]
  edge [
    source 4
    target 564
  ]
  edge [
    source 4
    target 565
  ]
  edge [
    source 4
    target 566
  ]
  edge [
    source 4
    target 567
  ]
  edge [
    source 4
    target 568
  ]
  edge [
    source 4
    target 569
  ]
  edge [
    source 4
    target 570
  ]
  edge [
    source 4
    target 571
  ]
  edge [
    source 4
    target 572
  ]
  edge [
    source 4
    target 573
  ]
  edge [
    source 4
    target 574
  ]
  edge [
    source 4
    target 575
  ]
  edge [
    source 4
    target 576
  ]
  edge [
    source 4
    target 577
  ]
  edge [
    source 4
    target 578
  ]
  edge [
    source 4
    target 579
  ]
  edge [
    source 4
    target 580
  ]
  edge [
    source 4
    target 581
  ]
  edge [
    source 4
    target 582
  ]
  edge [
    source 4
    target 583
  ]
  edge [
    source 4
    target 584
  ]
  edge [
    source 4
    target 585
  ]
  edge [
    source 4
    target 586
  ]
  edge [
    source 4
    target 587
  ]
  edge [
    source 4
    target 588
  ]
  edge [
    source 4
    target 589
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 590
  ]
  edge [
    source 4
    target 591
  ]
  edge [
    source 4
    target 592
  ]
  edge [
    source 4
    target 593
  ]
  edge [
    source 4
    target 594
  ]
  edge [
    source 4
    target 595
  ]
  edge [
    source 4
    target 596
  ]
  edge [
    source 4
    target 597
  ]
  edge [
    source 4
    target 598
  ]
  edge [
    source 4
    target 599
  ]
  edge [
    source 4
    target 600
  ]
  edge [
    source 4
    target 601
  ]
  edge [
    source 4
    target 602
  ]
  edge [
    source 4
    target 603
  ]
  edge [
    source 4
    target 604
  ]
  edge [
    source 4
    target 605
  ]
  edge [
    source 4
    target 606
  ]
  edge [
    source 4
    target 607
  ]
  edge [
    source 4
    target 608
  ]
  edge [
    source 4
    target 609
  ]
  edge [
    source 4
    target 610
  ]
  edge [
    source 4
    target 611
  ]
  edge [
    source 4
    target 612
  ]
  edge [
    source 4
    target 613
  ]
  edge [
    source 4
    target 614
  ]
  edge [
    source 4
    target 615
  ]
  edge [
    source 4
    target 616
  ]
  edge [
    source 4
    target 617
  ]
  edge [
    source 4
    target 618
  ]
  edge [
    source 4
    target 619
  ]
  edge [
    source 4
    target 620
  ]
  edge [
    source 4
    target 621
  ]
  edge [
    source 4
    target 622
  ]
  edge [
    source 4
    target 623
  ]
  edge [
    source 4
    target 624
  ]
  edge [
    source 4
    target 625
  ]
  edge [
    source 4
    target 626
  ]
  edge [
    source 4
    target 627
  ]
  edge [
    source 4
    target 628
  ]
  edge [
    source 4
    target 629
  ]
  edge [
    source 4
    target 630
  ]
  edge [
    source 4
    target 631
  ]
  edge [
    source 4
    target 632
  ]
  edge [
    source 4
    target 224
  ]
  edge [
    source 4
    target 633
  ]
  edge [
    source 4
    target 634
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 635
  ]
  edge [
    source 4
    target 636
  ]
  edge [
    source 4
    target 637
  ]
  edge [
    source 4
    target 638
  ]
  edge [
    source 4
    target 639
  ]
  edge [
    source 4
    target 640
  ]
  edge [
    source 4
    target 126
  ]
  edge [
    source 4
    target 641
  ]
  edge [
    source 4
    target 642
  ]
  edge [
    source 4
    target 643
  ]
  edge [
    source 4
    target 644
  ]
  edge [
    source 4
    target 645
  ]
  edge [
    source 4
    target 646
  ]
  edge [
    source 4
    target 647
  ]
  edge [
    source 4
    target 648
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 649
  ]
  edge [
    source 4
    target 650
  ]
  edge [
    source 4
    target 651
  ]
  edge [
    source 4
    target 652
  ]
  edge [
    source 4
    target 653
  ]
  edge [
    source 4
    target 654
  ]
  edge [
    source 4
    target 655
  ]
  edge [
    source 4
    target 656
  ]
  edge [
    source 4
    target 206
  ]
  edge [
    source 4
    target 207
  ]
  edge [
    source 4
    target 208
  ]
  edge [
    source 4
    target 209
  ]
  edge [
    source 4
    target 210
  ]
  edge [
    source 4
    target 211
  ]
  edge [
    source 4
    target 657
  ]
  edge [
    source 4
    target 658
  ]
  edge [
    source 4
    target 659
  ]
  edge [
    source 4
    target 660
  ]
  edge [
    source 4
    target 661
  ]
  edge [
    source 4
    target 662
  ]
  edge [
    source 4
    target 663
  ]
  edge [
    source 4
    target 664
  ]
  edge [
    source 4
    target 665
  ]
  edge [
    source 4
    target 666
  ]
  edge [
    source 4
    target 667
  ]
  edge [
    source 4
    target 668
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 669
  ]
  edge [
    source 4
    target 670
  ]
  edge [
    source 4
    target 671
  ]
  edge [
    source 4
    target 672
  ]
  edge [
    source 4
    target 673
  ]
  edge [
    source 4
    target 674
  ]
  edge [
    source 4
    target 675
  ]
  edge [
    source 4
    target 676
  ]
  edge [
    source 4
    target 677
  ]
  edge [
    source 4
    target 678
  ]
  edge [
    source 4
    target 679
  ]
  edge [
    source 4
    target 680
  ]
  edge [
    source 4
    target 681
  ]
  edge [
    source 4
    target 682
  ]
  edge [
    source 4
    target 683
  ]
  edge [
    source 4
    target 684
  ]
  edge [
    source 4
    target 685
  ]
  edge [
    source 4
    target 686
  ]
  edge [
    source 4
    target 687
  ]
  edge [
    source 4
    target 688
  ]
  edge [
    source 4
    target 689
  ]
  edge [
    source 4
    target 690
  ]
  edge [
    source 4
    target 691
  ]
  edge [
    source 4
    target 692
  ]
  edge [
    source 4
    target 693
  ]
  edge [
    source 4
    target 694
  ]
  edge [
    source 4
    target 695
  ]
  edge [
    source 4
    target 696
  ]
  edge [
    source 4
    target 697
  ]
  edge [
    source 4
    target 698
  ]
  edge [
    source 4
    target 699
  ]
  edge [
    source 4
    target 700
  ]
  edge [
    source 4
    target 701
  ]
  edge [
    source 4
    target 702
  ]
  edge [
    source 4
    target 703
  ]
  edge [
    source 4
    target 704
  ]
  edge [
    source 4
    target 705
  ]
  edge [
    source 4
    target 706
  ]
  edge [
    source 4
    target 707
  ]
  edge [
    source 4
    target 708
  ]
  edge [
    source 4
    target 709
  ]
  edge [
    source 4
    target 710
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 711
  ]
  edge [
    source 4
    target 712
  ]
  edge [
    source 4
    target 713
  ]
  edge [
    source 4
    target 714
  ]
  edge [
    source 4
    target 715
  ]
  edge [
    source 4
    target 716
  ]
  edge [
    source 4
    target 717
  ]
  edge [
    source 4
    target 718
  ]
  edge [
    source 4
    target 719
  ]
  edge [
    source 4
    target 720
  ]
  edge [
    source 4
    target 721
  ]
  edge [
    source 4
    target 722
  ]
  edge [
    source 4
    target 723
  ]
  edge [
    source 4
    target 724
  ]
  edge [
    source 4
    target 725
  ]
  edge [
    source 4
    target 726
  ]
  edge [
    source 4
    target 727
  ]
  edge [
    source 4
    target 728
  ]
  edge [
    source 4
    target 729
  ]
  edge [
    source 4
    target 730
  ]
  edge [
    source 4
    target 731
  ]
  edge [
    source 4
    target 732
  ]
  edge [
    source 4
    target 733
  ]
  edge [
    source 4
    target 734
  ]
  edge [
    source 4
    target 232
  ]
  edge [
    source 4
    target 735
  ]
  edge [
    source 4
    target 736
  ]
  edge [
    source 4
    target 737
  ]
  edge [
    source 4
    target 738
  ]
  edge [
    source 4
    target 739
  ]
  edge [
    source 4
    target 740
  ]
  edge [
    source 4
    target 741
  ]
  edge [
    source 4
    target 742
  ]
  edge [
    source 4
    target 743
  ]
  edge [
    source 4
    target 744
  ]
  edge [
    source 4
    target 745
  ]
  edge [
    source 4
    target 746
  ]
  edge [
    source 4
    target 747
  ]
  edge [
    source 4
    target 748
  ]
  edge [
    source 4
    target 749
  ]
  edge [
    source 4
    target 750
  ]
  edge [
    source 4
    target 751
  ]
  edge [
    source 4
    target 752
  ]
  edge [
    source 4
    target 753
  ]
  edge [
    source 4
    target 754
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 755
  ]
  edge [
    source 5
    target 756
  ]
  edge [
    source 5
    target 757
  ]
  edge [
    source 5
    target 758
  ]
  edge [
    source 5
    target 759
  ]
  edge [
    source 5
    target 760
  ]
  edge [
    source 5
    target 761
  ]
  edge [
    source 5
    target 762
  ]
  edge [
    source 5
    target 763
  ]
  edge [
    source 5
    target 764
  ]
  edge [
    source 5
    target 765
  ]
  edge [
    source 5
    target 387
  ]
  edge [
    source 5
    target 581
  ]
  edge [
    source 5
    target 766
  ]
  edge [
    source 5
    target 767
  ]
  edge [
    source 5
    target 768
  ]
  edge [
    source 5
    target 769
  ]
  edge [
    source 5
    target 228
  ]
  edge [
    source 5
    target 217
  ]
  edge [
    source 5
    target 770
  ]
  edge [
    source 5
    target 771
  ]
  edge [
    source 5
    target 463
  ]
  edge [
    source 5
    target 772
  ]
  edge [
    source 5
    target 773
  ]
  edge [
    source 5
    target 398
  ]
  edge [
    source 5
    target 774
  ]
  edge [
    source 5
    target 775
  ]
  edge [
    source 5
    target 776
  ]
  edge [
    source 5
    target 777
  ]
  edge [
    source 5
    target 778
  ]
  edge [
    source 5
    target 478
  ]
  edge [
    source 5
    target 779
  ]
  edge [
    source 5
    target 780
  ]
  edge [
    source 5
    target 781
  ]
  edge [
    source 5
    target 782
  ]
  edge [
    source 5
    target 783
  ]
  edge [
    source 5
    target 286
  ]
  edge [
    source 5
    target 541
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 465
  ]
  edge [
    source 5
    target 187
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 784
  ]
  edge [
    source 5
    target 785
  ]
  edge [
    source 5
    target 786
  ]
  edge [
    source 5
    target 787
  ]
  edge [
    source 5
    target 788
  ]
  edge [
    source 5
    target 789
  ]
  edge [
    source 5
    target 790
  ]
  edge [
    source 5
    target 791
  ]
  edge [
    source 5
    target 792
  ]
  edge [
    source 5
    target 793
  ]
  edge [
    source 5
    target 794
  ]
  edge [
    source 5
    target 795
  ]
  edge [
    source 5
    target 796
  ]
  edge [
    source 5
    target 797
  ]
  edge [
    source 5
    target 798
  ]
  edge [
    source 5
    target 799
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 800
  ]
  edge [
    source 5
    target 270
  ]
  edge [
    source 5
    target 801
  ]
  edge [
    source 5
    target 802
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 803
  ]
  edge [
    source 5
    target 804
  ]
  edge [
    source 5
    target 805
  ]
  edge [
    source 5
    target 806
  ]
  edge [
    source 5
    target 614
  ]
  edge [
    source 5
    target 807
  ]
  edge [
    source 5
    target 808
  ]
  edge [
    source 5
    target 376
  ]
  edge [
    source 5
    target 158
  ]
  edge [
    source 5
    target 809
  ]
  edge [
    source 5
    target 380
  ]
  edge [
    source 5
    target 382
  ]
  edge [
    source 5
    target 810
  ]
  edge [
    source 5
    target 811
  ]
  edge [
    source 5
    target 812
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 231
  ]
  edge [
    source 5
    target 813
  ]
  edge [
    source 5
    target 814
  ]
  edge [
    source 5
    target 815
  ]
  edge [
    source 5
    target 816
  ]
  edge [
    source 5
    target 817
  ]
  edge [
    source 5
    target 818
  ]
  edge [
    source 5
    target 819
  ]
  edge [
    source 5
    target 820
  ]
  edge [
    source 5
    target 821
  ]
  edge [
    source 5
    target 822
  ]
  edge [
    source 5
    target 823
  ]
  edge [
    source 5
    target 824
  ]
  edge [
    source 5
    target 825
  ]
  edge [
    source 5
    target 826
  ]
  edge [
    source 5
    target 827
  ]
  edge [
    source 5
    target 828
  ]
  edge [
    source 5
    target 829
  ]
  edge [
    source 5
    target 830
  ]
  edge [
    source 5
    target 831
  ]
  edge [
    source 5
    target 832
  ]
  edge [
    source 5
    target 833
  ]
  edge [
    source 5
    target 834
  ]
  edge [
    source 5
    target 835
  ]
  edge [
    source 5
    target 836
  ]
  edge [
    source 5
    target 837
  ]
  edge [
    source 5
    target 838
  ]
  edge [
    source 5
    target 839
  ]
  edge [
    source 5
    target 840
  ]
  edge [
    source 5
    target 841
  ]
  edge [
    source 5
    target 842
  ]
  edge [
    source 5
    target 843
  ]
  edge [
    source 5
    target 844
  ]
  edge [
    source 5
    target 845
  ]
  edge [
    source 5
    target 846
  ]
  edge [
    source 5
    target 847
  ]
  edge [
    source 5
    target 848
  ]
  edge [
    source 5
    target 849
  ]
  edge [
    source 5
    target 850
  ]
  edge [
    source 5
    target 851
  ]
  edge [
    source 5
    target 852
  ]
  edge [
    source 5
    target 853
  ]
  edge [
    source 5
    target 854
  ]
  edge [
    source 5
    target 855
  ]
  edge [
    source 5
    target 856
  ]
  edge [
    source 5
    target 857
  ]
  edge [
    source 5
    target 858
  ]
  edge [
    source 5
    target 859
  ]
  edge [
    source 5
    target 860
  ]
  edge [
    source 5
    target 861
  ]
  edge [
    source 5
    target 862
  ]
  edge [
    source 5
    target 863
  ]
  edge [
    source 5
    target 864
  ]
  edge [
    source 5
    target 865
  ]
  edge [
    source 5
    target 866
  ]
  edge [
    source 5
    target 867
  ]
  edge [
    source 5
    target 868
  ]
  edge [
    source 5
    target 869
  ]
  edge [
    source 5
    target 870
  ]
  edge [
    source 5
    target 871
  ]
  edge [
    source 5
    target 872
  ]
  edge [
    source 5
    target 873
  ]
  edge [
    source 5
    target 874
  ]
  edge [
    source 5
    target 875
  ]
  edge [
    source 5
    target 876
  ]
  edge [
    source 5
    target 877
  ]
  edge [
    source 5
    target 878
  ]
  edge [
    source 5
    target 879
  ]
  edge [
    source 5
    target 880
  ]
  edge [
    source 5
    target 881
  ]
  edge [
    source 5
    target 882
  ]
  edge [
    source 5
    target 883
  ]
  edge [
    source 5
    target 724
  ]
  edge [
    source 5
    target 884
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 885
  ]
  edge [
    source 6
    target 886
  ]
  edge [
    source 6
    target 887
  ]
  edge [
    source 6
    target 888
  ]
  edge [
    source 6
    target 889
  ]
  edge [
    source 6
    target 890
  ]
  edge [
    source 6
    target 891
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 892
  ]
  edge [
    source 7
    target 779
  ]
  edge [
    source 7
    target 893
  ]
  edge [
    source 7
    target 894
  ]
  edge [
    source 7
    target 562
  ]
  edge [
    source 7
    target 591
  ]
  edge [
    source 7
    target 895
  ]
  edge [
    source 7
    target 896
  ]
  edge [
    source 7
    target 897
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 618
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 898
  ]
  edge [
    source 7
    target 899
  ]
  edge [
    source 7
    target 900
  ]
  edge [
    source 7
    target 901
  ]
  edge [
    source 7
    target 902
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 903
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 533
  ]
  edge [
    source 7
    target 904
  ]
  edge [
    source 7
    target 905
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 906
  ]
  edge [
    source 7
    target 907
  ]
  edge [
    source 7
    target 14
  ]
  edge [
    source 7
    target 908
  ]
  edge [
    source 7
    target 909
  ]
  edge [
    source 7
    target 910
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 911
  ]
  edge [
    source 7
    target 912
  ]
  edge [
    source 7
    target 913
  ]
  edge [
    source 7
    target 914
  ]
  edge [
    source 7
    target 915
  ]
  edge [
    source 7
    target 916
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 7
    target 917
  ]
  edge [
    source 7
    target 541
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 918
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 919
  ]
  edge [
    source 7
    target 920
  ]
  edge [
    source 7
    target 921
  ]
  edge [
    source 7
    target 461
  ]
  edge [
    source 7
    target 922
  ]
  edge [
    source 7
    target 923
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 924
  ]
  edge [
    source 7
    target 925
  ]
  edge [
    source 7
    target 926
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 927
  ]
  edge [
    source 7
    target 928
  ]
  edge [
    source 7
    target 929
  ]
  edge [
    source 7
    target 930
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 931
  ]
  edge [
    source 7
    target 520
  ]
  edge [
    source 7
    target 932
  ]
  edge [
    source 7
    target 614
  ]
  edge [
    source 7
    target 933
  ]
  edge [
    source 7
    target 934
  ]
  edge [
    source 7
    target 935
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 936
  ]
  edge [
    source 7
    target 937
  ]
  edge [
    source 7
    target 938
  ]
  edge [
    source 7
    target 939
  ]
  edge [
    source 7
    target 940
  ]
  edge [
    source 7
    target 941
  ]
  edge [
    source 7
    target 942
  ]
  edge [
    source 7
    target 943
  ]
  edge [
    source 7
    target 944
  ]
  edge [
    source 7
    target 945
  ]
  edge [
    source 7
    target 946
  ]
  edge [
    source 7
    target 692
  ]
  edge [
    source 7
    target 564
  ]
  edge [
    source 7
    target 947
  ]
  edge [
    source 7
    target 548
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 948
  ]
  edge [
    source 7
    target 949
  ]
  edge [
    source 7
    target 950
  ]
  edge [
    source 7
    target 951
  ]
  edge [
    source 7
    target 952
  ]
  edge [
    source 7
    target 953
  ]
  edge [
    source 7
    target 954
  ]
  edge [
    source 7
    target 955
  ]
  edge [
    source 7
    target 956
  ]
  edge [
    source 7
    target 957
  ]
  edge [
    source 7
    target 958
  ]
  edge [
    source 7
    target 959
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 960
  ]
  edge [
    source 7
    target 961
  ]
  edge [
    source 7
    target 962
  ]
  edge [
    source 7
    target 963
  ]
  edge [
    source 7
    target 964
  ]
  edge [
    source 7
    target 965
  ]
  edge [
    source 7
    target 966
  ]
  edge [
    source 7
    target 967
  ]
  edge [
    source 7
    target 968
  ]
  edge [
    source 7
    target 969
  ]
  edge [
    source 7
    target 970
  ]
  edge [
    source 7
    target 971
  ]
  edge [
    source 7
    target 972
  ]
  edge [
    source 7
    target 973
  ]
  edge [
    source 7
    target 974
  ]
  edge [
    source 7
    target 975
  ]
  edge [
    source 7
    target 976
  ]
  edge [
    source 7
    target 977
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 978
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 979
  ]
  edge [
    source 7
    target 980
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 981
  ]
  edge [
    source 8
    target 982
  ]
  edge [
    source 8
    target 983
  ]
  edge [
    source 8
    target 984
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 985
  ]
  edge [
    source 8
    target 986
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 113
  ]
  edge [
    source 8
    target 114
  ]
  edge [
    source 8
    target 115
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 647
  ]
  edge [
    source 8
    target 987
  ]
  edge [
    source 8
    target 734
  ]
  edge [
    source 8
    target 988
  ]
  edge [
    source 8
    target 989
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 990
  ]
  edge [
    source 8
    target 991
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 992
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 993
  ]
  edge [
    source 9
    target 994
  ]
  edge [
    source 9
    target 995
  ]
  edge [
    source 9
    target 996
  ]
  edge [
    source 9
    target 413
  ]
  edge [
    source 9
    target 997
  ]
  edge [
    source 9
    target 998
  ]
  edge [
    source 9
    target 999
  ]
  edge [
    source 9
    target 1000
  ]
  edge [
    source 9
    target 1001
  ]
  edge [
    source 9
    target 1002
  ]
  edge [
    source 9
    target 1003
  ]
  edge [
    source 9
    target 1004
  ]
  edge [
    source 9
    target 1005
  ]
  edge [
    source 9
    target 1006
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 36
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 982
  ]
  edge [
    source 9
    target 1007
  ]
  edge [
    source 9
    target 1008
  ]
  edge [
    source 9
    target 1009
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 1010
  ]
  edge [
    source 10
    target 1011
  ]
  edge [
    source 10
    target 1012
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 1013
  ]
  edge [
    source 11
    target 1014
  ]
  edge [
    source 11
    target 1015
  ]
  edge [
    source 11
    target 1016
  ]
  edge [
    source 11
    target 1017
  ]
  edge [
    source 11
    target 1018
  ]
  edge [
    source 11
    target 1019
  ]
  edge [
    source 11
    target 1020
  ]
  edge [
    source 11
    target 1021
  ]
  edge [
    source 11
    target 1022
  ]
  edge [
    source 11
    target 1023
  ]
  edge [
    source 11
    target 1024
  ]
  edge [
    source 11
    target 1025
  ]
  edge [
    source 11
    target 1026
  ]
  edge [
    source 11
    target 1027
  ]
  edge [
    source 11
    target 1028
  ]
  edge [
    source 11
    target 1029
  ]
  edge [
    source 11
    target 776
  ]
  edge [
    source 11
    target 1030
  ]
  edge [
    source 11
    target 1031
  ]
  edge [
    source 11
    target 1032
  ]
  edge [
    source 11
    target 1033
  ]
  edge [
    source 11
    target 1034
  ]
  edge [
    source 11
    target 1035
  ]
  edge [
    source 11
    target 1036
  ]
  edge [
    source 11
    target 562
  ]
  edge [
    source 11
    target 1037
  ]
  edge [
    source 11
    target 1038
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 1039
  ]
  edge [
    source 12
    target 1040
  ]
  edge [
    source 12
    target 1041
  ]
  edge [
    source 12
    target 1042
  ]
  edge [
    source 12
    target 1043
  ]
  edge [
    source 12
    target 1044
  ]
  edge [
    source 12
    target 1045
  ]
  edge [
    source 12
    target 245
  ]
  edge [
    source 12
    target 1046
  ]
  edge [
    source 12
    target 1047
  ]
  edge [
    source 12
    target 1048
  ]
  edge [
    source 12
    target 1049
  ]
  edge [
    source 12
    target 1050
  ]
  edge [
    source 12
    target 1051
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 982
  ]
  edge [
    source 13
    target 1052
  ]
  edge [
    source 13
    target 1053
  ]
  edge [
    source 13
    target 1054
  ]
  edge [
    source 13
    target 1055
  ]
  edge [
    source 13
    target 54
  ]
  edge [
    source 13
    target 1056
  ]
  edge [
    source 13
    target 1057
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 990
  ]
  edge [
    source 13
    target 991
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 992
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 724
  ]
  edge [
    source 14
    target 1058
  ]
  edge [
    source 14
    target 1059
  ]
  edge [
    source 14
    target 663
  ]
  edge [
    source 14
    target 882
  ]
  edge [
    source 14
    target 387
  ]
  edge [
    source 14
    target 1060
  ]
  edge [
    source 14
    target 683
  ]
  edge [
    source 14
    target 1061
  ]
  edge [
    source 14
    target 1062
  ]
  edge [
    source 14
    target 478
  ]
  edge [
    source 14
    target 1063
  ]
  edge [
    source 14
    target 1064
  ]
  edge [
    source 14
    target 1065
  ]
  edge [
    source 14
    target 1066
  ]
  edge [
    source 14
    target 1067
  ]
  edge [
    source 14
    target 1068
  ]
  edge [
    source 14
    target 1069
  ]
  edge [
    source 14
    target 1070
  ]
  edge [
    source 14
    target 1071
  ]
  edge [
    source 14
    target 267
  ]
  edge [
    source 14
    target 799
  ]
  edge [
    source 14
    target 1072
  ]
  edge [
    source 14
    target 1073
  ]
  edge [
    source 14
    target 1074
  ]
  edge [
    source 14
    target 1075
  ]
  edge [
    source 14
    target 618
  ]
  edge [
    source 14
    target 398
  ]
  edge [
    source 14
    target 774
  ]
  edge [
    source 14
    target 775
  ]
  edge [
    source 14
    target 776
  ]
  edge [
    source 14
    target 721
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 68
  ]
  edge [
    source 14
    target 1076
  ]
  edge [
    source 14
    target 1077
  ]
  edge [
    source 14
    target 750
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 1078
  ]
  edge [
    source 14
    target 1079
  ]
  edge [
    source 14
    target 727
  ]
  edge [
    source 14
    target 1080
  ]
  edge [
    source 14
    target 452
  ]
  edge [
    source 14
    target 56
  ]
  edge [
    source 14
    target 754
  ]
  edge [
    source 14
    target 1081
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 1082
  ]
  edge [
    source 14
    target 1083
  ]
  edge [
    source 14
    target 772
  ]
  edge [
    source 14
    target 504
  ]
  edge [
    source 14
    target 1084
  ]
  edge [
    source 14
    target 1085
  ]
  edge [
    source 14
    target 1086
  ]
  edge [
    source 14
    target 1087
  ]
  edge [
    source 14
    target 1088
  ]
  edge [
    source 14
    target 1089
  ]
  edge [
    source 14
    target 884
  ]
  edge [
    source 14
    target 1090
  ]
  edge [
    source 14
    target 289
  ]
  edge [
    source 14
    target 228
  ]
  edge [
    source 14
    target 1091
  ]
  edge [
    source 14
    target 655
  ]
  edge [
    source 14
    target 1092
  ]
  edge [
    source 14
    target 1093
  ]
  edge [
    source 14
    target 1094
  ]
  edge [
    source 14
    target 1095
  ]
  edge [
    source 14
    target 1096
  ]
  edge [
    source 14
    target 308
  ]
  edge [
    source 14
    target 1097
  ]
  edge [
    source 14
    target 1098
  ]
  edge [
    source 14
    target 510
  ]
  edge [
    source 14
    target 1099
  ]
  edge [
    source 14
    target 1100
  ]
  edge [
    source 14
    target 1101
  ]
  edge [
    source 14
    target 1102
  ]
  edge [
    source 14
    target 1103
  ]
  edge [
    source 14
    target 1104
  ]
  edge [
    source 14
    target 1105
  ]
  edge [
    source 14
    target 1106
  ]
  edge [
    source 14
    target 1107
  ]
  edge [
    source 14
    target 1108
  ]
  edge [
    source 14
    target 1109
  ]
  edge [
    source 14
    target 1110
  ]
  edge [
    source 14
    target 394
  ]
  edge [
    source 14
    target 1111
  ]
  edge [
    source 14
    target 212
  ]
  edge [
    source 14
    target 1112
  ]
  edge [
    source 14
    target 692
  ]
  edge [
    source 14
    target 1113
  ]
  edge [
    source 14
    target 1114
  ]
  edge [
    source 14
    target 1115
  ]
  edge [
    source 14
    target 1116
  ]
  edge [
    source 14
    target 384
  ]
  edge [
    source 14
    target 1117
  ]
  edge [
    source 14
    target 1118
  ]
  edge [
    source 14
    target 1119
  ]
  edge [
    source 14
    target 1120
  ]
  edge [
    source 14
    target 1121
  ]
  edge [
    source 14
    target 1122
  ]
  edge [
    source 14
    target 632
  ]
  edge [
    source 14
    target 1123
  ]
  edge [
    source 14
    target 638
  ]
  edge [
    source 14
    target 1124
  ]
  edge [
    source 14
    target 1125
  ]
  edge [
    source 14
    target 1126
  ]
  edge [
    source 14
    target 373
  ]
  edge [
    source 14
    target 1127
  ]
  edge [
    source 14
    target 1128
  ]
  edge [
    source 14
    target 1129
  ]
  edge [
    source 14
    target 1130
  ]
  edge [
    source 14
    target 1131
  ]
  edge [
    source 14
    target 628
  ]
  edge [
    source 14
    target 1132
  ]
  edge [
    source 14
    target 1133
  ]
  edge [
    source 14
    target 259
  ]
  edge [
    source 14
    target 1134
  ]
  edge [
    source 14
    target 1135
  ]
  edge [
    source 14
    target 1136
  ]
  edge [
    source 14
    target 1137
  ]
  edge [
    source 14
    target 1138
  ]
  edge [
    source 14
    target 1139
  ]
  edge [
    source 14
    target 1140
  ]
  edge [
    source 14
    target 1141
  ]
  edge [
    source 14
    target 443
  ]
  edge [
    source 14
    target 1142
  ]
  edge [
    source 14
    target 1143
  ]
  edge [
    source 14
    target 1144
  ]
  edge [
    source 14
    target 921
  ]
  edge [
    source 14
    target 1145
  ]
  edge [
    source 14
    target 1146
  ]
  edge [
    source 14
    target 1147
  ]
  edge [
    source 14
    target 1148
  ]
  edge [
    source 14
    target 1149
  ]
  edge [
    source 14
    target 1150
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 1151
  ]
  edge [
    source 14
    target 1152
  ]
  edge [
    source 14
    target 1153
  ]
  edge [
    source 14
    target 1154
  ]
  edge [
    source 14
    target 1155
  ]
  edge [
    source 14
    target 1156
  ]
  edge [
    source 14
    target 1157
  ]
  edge [
    source 14
    target 1158
  ]
  edge [
    source 14
    target 1159
  ]
  edge [
    source 14
    target 1160
  ]
  edge [
    source 14
    target 745
  ]
  edge [
    source 14
    target 1161
  ]
  edge [
    source 14
    target 1162
  ]
  edge [
    source 14
    target 1163
  ]
  edge [
    source 14
    target 1164
  ]
  edge [
    source 14
    target 1165
  ]
  edge [
    source 14
    target 1166
  ]
  edge [
    source 14
    target 1167
  ]
  edge [
    source 14
    target 1168
  ]
  edge [
    source 14
    target 1169
  ]
  edge [
    source 14
    target 1170
  ]
  edge [
    source 14
    target 1171
  ]
  edge [
    source 14
    target 1172
  ]
  edge [
    source 14
    target 1173
  ]
  edge [
    source 14
    target 1174
  ]
  edge [
    source 14
    target 1175
  ]
  edge [
    source 14
    target 1176
  ]
  edge [
    source 14
    target 1177
  ]
  edge [
    source 14
    target 905
  ]
  edge [
    source 14
    target 303
  ]
  edge [
    source 14
    target 906
  ]
  edge [
    source 14
    target 907
  ]
  edge [
    source 14
    target 908
  ]
  edge [
    source 14
    target 909
  ]
  edge [
    source 14
    target 910
  ]
  edge [
    source 14
    target 319
  ]
  edge [
    source 14
    target 911
  ]
  edge [
    source 14
    target 912
  ]
  edge [
    source 14
    target 913
  ]
  edge [
    source 14
    target 914
  ]
  edge [
    source 14
    target 915
  ]
  edge [
    source 14
    target 916
  ]
  edge [
    source 14
    target 388
  ]
  edge [
    source 14
    target 917
  ]
  edge [
    source 14
    target 541
  ]
  edge [
    source 14
    target 21
  ]
  edge [
    source 14
    target 918
  ]
  edge [
    source 14
    target 329
  ]
  edge [
    source 14
    target 919
  ]
  edge [
    source 14
    target 920
  ]
  edge [
    source 14
    target 461
  ]
  edge [
    source 14
    target 922
  ]
  edge [
    source 14
    target 923
  ]
  edge [
    source 14
    target 386
  ]
  edge [
    source 14
    target 924
  ]
  edge [
    source 14
    target 925
  ]
  edge [
    source 14
    target 926
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 927
  ]
  edge [
    source 14
    target 1178
  ]
  edge [
    source 14
    target 1179
  ]
  edge [
    source 14
    target 249
  ]
  edge [
    source 14
    target 1180
  ]
  edge [
    source 14
    target 1181
  ]
  edge [
    source 14
    target 1182
  ]
  edge [
    source 14
    target 1183
  ]
  edge [
    source 14
    target 1184
  ]
  edge [
    source 14
    target 1185
  ]
  edge [
    source 14
    target 1186
  ]
  edge [
    source 14
    target 1187
  ]
  edge [
    source 14
    target 1188
  ]
  edge [
    source 14
    target 293
  ]
  edge [
    source 14
    target 1189
  ]
  edge [
    source 14
    target 1190
  ]
  edge [
    source 14
    target 1191
  ]
  edge [
    source 14
    target 1192
  ]
  edge [
    source 14
    target 1193
  ]
  edge [
    source 14
    target 1194
  ]
  edge [
    source 14
    target 1195
  ]
  edge [
    source 14
    target 1196
  ]
  edge [
    source 14
    target 1197
  ]
  edge [
    source 14
    target 1198
  ]
  edge [
    source 14
    target 1199
  ]
  edge [
    source 14
    target 1200
  ]
  edge [
    source 14
    target 1201
  ]
  edge [
    source 14
    target 660
  ]
  edge [
    source 14
    target 1202
  ]
  edge [
    source 14
    target 667
  ]
  edge [
    source 14
    target 1203
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 1204
  ]
  edge [
    source 14
    target 669
  ]
  edge [
    source 14
    target 29
  ]
  edge [
    source 14
    target 659
  ]
  edge [
    source 14
    target 1205
  ]
  edge [
    source 14
    target 1206
  ]
  edge [
    source 14
    target 1207
  ]
  edge [
    source 14
    target 1208
  ]
  edge [
    source 14
    target 1209
  ]
  edge [
    source 14
    target 1210
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 1211
  ]
  edge [
    source 14
    target 1212
  ]
  edge [
    source 14
    target 410
  ]
  edge [
    source 14
    target 992
  ]
  edge [
    source 14
    target 658
  ]
  edge [
    source 14
    target 662
  ]
  edge [
    source 14
    target 1213
  ]
  edge [
    source 14
    target 1214
  ]
  edge [
    source 14
    target 1215
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 1216
  ]
  edge [
    source 14
    target 661
  ]
  edge [
    source 14
    target 664
  ]
  edge [
    source 14
    target 665
  ]
  edge [
    source 14
    target 1217
  ]
  edge [
    source 14
    target 666
  ]
  edge [
    source 14
    target 1218
  ]
  edge [
    source 14
    target 1219
  ]
  edge [
    source 14
    target 1220
  ]
  edge [
    source 14
    target 1221
  ]
  edge [
    source 14
    target 1222
  ]
  edge [
    source 14
    target 1223
  ]
  edge [
    source 14
    target 780
  ]
  edge [
    source 14
    target 1224
  ]
  edge [
    source 14
    target 783
  ]
  edge [
    source 14
    target 581
  ]
  edge [
    source 14
    target 1225
  ]
  edge [
    source 14
    target 1226
  ]
  edge [
    source 14
    target 1227
  ]
  edge [
    source 14
    target 1259
  ]
  edge [
    source 14
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 1259
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 1228
  ]
  edge [
    source 16
    target 1229
  ]
  edge [
    source 16
    target 1230
  ]
  edge [
    source 16
    target 1231
  ]
  edge [
    source 16
    target 1232
  ]
  edge [
    source 16
    target 1233
  ]
  edge [
    source 16
    target 1234
  ]
  edge [
    source 16
    target 1235
  ]
  edge [
    source 16
    target 1236
  ]
  edge [
    source 16
    target 1237
  ]
  edge [
    source 16
    target 1238
  ]
  edge [
    source 16
    target 1239
  ]
  edge [
    source 16
    target 1240
  ]
  edge [
    source 16
    target 1241
  ]
  edge [
    source 16
    target 1242
  ]
  edge [
    source 16
    target 1243
  ]
  edge [
    source 16
    target 1244
  ]
  edge [
    source 16
    target 1245
  ]
  edge [
    source 16
    target 1246
  ]
  edge [
    source 16
    target 1247
  ]
  edge [
    source 16
    target 1248
  ]
  edge [
    source 16
    target 1249
  ]
  edge [
    source 16
    target 1250
  ]
  edge [
    source 16
    target 1251
  ]
  edge [
    source 16
    target 1252
  ]
  edge [
    source 16
    target 1253
  ]
  edge [
    source 16
    target 1254
  ]
  edge [
    source 16
    target 1259
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 1255
  ]
  edge [
    source 18
    target 1256
  ]
  edge [
    source 18
    target 1257
  ]
  edge [
    source 18
    target 1258
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 1260
  ]
  edge [
    source 1259
    target 1261
  ]
]
