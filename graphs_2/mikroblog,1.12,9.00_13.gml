graph [
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "kto"
    origin "text"
  ]
  node [
    id 2
    label "zaplusuje"
    origin "text"
  ]
  node [
    id 3
    label "dosta&#263;"
    origin "text"
  ]
  node [
    id 4
    label "mema"
    origin "text"
  ]
  node [
    id 5
    label "jaki&#347;"
  ]
  node [
    id 6
    label "przyzwoity"
  ]
  node [
    id 7
    label "ciekawy"
  ]
  node [
    id 8
    label "jako&#347;"
  ]
  node [
    id 9
    label "jako_tako"
  ]
  node [
    id 10
    label "niez&#322;y"
  ]
  node [
    id 11
    label "dziwny"
  ]
  node [
    id 12
    label "charakterystyczny"
  ]
  node [
    id 13
    label "zapanowa&#263;"
  ]
  node [
    id 14
    label "develop"
  ]
  node [
    id 15
    label "schorzenie"
  ]
  node [
    id 16
    label "nabawienie_si&#281;"
  ]
  node [
    id 17
    label "obskoczy&#263;"
  ]
  node [
    id 18
    label "si&#281;gn&#261;&#263;"
  ]
  node [
    id 19
    label "catch"
  ]
  node [
    id 20
    label "zrobi&#263;"
  ]
  node [
    id 21
    label "get"
  ]
  node [
    id 22
    label "zwiastun"
  ]
  node [
    id 23
    label "doczeka&#263;"
  ]
  node [
    id 24
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 25
    label "kupi&#263;"
  ]
  node [
    id 26
    label "wysta&#263;"
  ]
  node [
    id 27
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 28
    label "wystarczy&#263;"
  ]
  node [
    id 29
    label "wzi&#261;&#263;"
  ]
  node [
    id 30
    label "naby&#263;"
  ]
  node [
    id 31
    label "nabawianie_si&#281;"
  ]
  node [
    id 32
    label "range"
  ]
  node [
    id 33
    label "uzyska&#263;"
  ]
  node [
    id 34
    label "suffice"
  ]
  node [
    id 35
    label "spowodowa&#263;"
  ]
  node [
    id 36
    label "stan&#261;&#263;"
  ]
  node [
    id 37
    label "zaspokoi&#263;"
  ]
  node [
    id 38
    label "odziedziczy&#263;"
  ]
  node [
    id 39
    label "ruszy&#263;"
  ]
  node [
    id 40
    label "take"
  ]
  node [
    id 41
    label "zaatakowa&#263;"
  ]
  node [
    id 42
    label "skorzysta&#263;"
  ]
  node [
    id 43
    label "uciec"
  ]
  node [
    id 44
    label "receive"
  ]
  node [
    id 45
    label "nakaza&#263;"
  ]
  node [
    id 46
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 47
    label "bra&#263;"
  ]
  node [
    id 48
    label "u&#380;y&#263;"
  ]
  node [
    id 49
    label "wyrucha&#263;"
  ]
  node [
    id 50
    label "World_Health_Organization"
  ]
  node [
    id 51
    label "wyciupcia&#263;"
  ]
  node [
    id 52
    label "wygra&#263;"
  ]
  node [
    id 53
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 54
    label "withdraw"
  ]
  node [
    id 55
    label "wzi&#281;cie"
  ]
  node [
    id 56
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 57
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 58
    label "poczyta&#263;"
  ]
  node [
    id 59
    label "obj&#261;&#263;"
  ]
  node [
    id 60
    label "seize"
  ]
  node [
    id 61
    label "aim"
  ]
  node [
    id 62
    label "chwyci&#263;"
  ]
  node [
    id 63
    label "przyj&#261;&#263;"
  ]
  node [
    id 64
    label "pokona&#263;"
  ]
  node [
    id 65
    label "arise"
  ]
  node [
    id 66
    label "uda&#263;_si&#281;"
  ]
  node [
    id 67
    label "zacz&#261;&#263;"
  ]
  node [
    id 68
    label "otrzyma&#263;"
  ]
  node [
    id 69
    label "wej&#347;&#263;"
  ]
  node [
    id 70
    label "poruszy&#263;"
  ]
  node [
    id 71
    label "poradzi&#263;_sobie"
  ]
  node [
    id 72
    label "osaczy&#263;"
  ]
  node [
    id 73
    label "okra&#347;&#263;"
  ]
  node [
    id 74
    label "okr&#261;&#380;y&#263;"
  ]
  node [
    id 75
    label "posi&#261;&#347;&#263;"
  ]
  node [
    id 76
    label "obiec"
  ]
  node [
    id 77
    label "powstrzyma&#263;"
  ]
  node [
    id 78
    label "podporz&#261;dkowa&#263;"
  ]
  node [
    id 79
    label "manipulate"
  ]
  node [
    id 80
    label "rule"
  ]
  node [
    id 81
    label "cope"
  ]
  node [
    id 82
    label "post&#261;pi&#263;"
  ]
  node [
    id 83
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 84
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 85
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 86
    label "zorganizowa&#263;"
  ]
  node [
    id 87
    label "appoint"
  ]
  node [
    id 88
    label "wystylizowa&#263;"
  ]
  node [
    id 89
    label "cause"
  ]
  node [
    id 90
    label "przerobi&#263;"
  ]
  node [
    id 91
    label "nabra&#263;"
  ]
  node [
    id 92
    label "make"
  ]
  node [
    id 93
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 94
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 95
    label "wydali&#263;"
  ]
  node [
    id 96
    label "pozyska&#263;"
  ]
  node [
    id 97
    label "ustawi&#263;"
  ]
  node [
    id 98
    label "uwierzy&#263;"
  ]
  node [
    id 99
    label "zagra&#263;"
  ]
  node [
    id 100
    label "beget"
  ]
  node [
    id 101
    label "uzna&#263;"
  ]
  node [
    id 102
    label "draw"
  ]
  node [
    id 103
    label "pozosta&#263;"
  ]
  node [
    id 104
    label "poczeka&#263;"
  ]
  node [
    id 105
    label "wytrwa&#263;"
  ]
  node [
    id 106
    label "realize"
  ]
  node [
    id 107
    label "promocja"
  ]
  node [
    id 108
    label "wytworzy&#263;"
  ]
  node [
    id 109
    label "give_birth"
  ]
  node [
    id 110
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 111
    label "appreciation"
  ]
  node [
    id 112
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 113
    label "allude"
  ]
  node [
    id 114
    label "dotrze&#263;"
  ]
  node [
    id 115
    label "fall_upon"
  ]
  node [
    id 116
    label "przewidywanie"
  ]
  node [
    id 117
    label "oznaka"
  ]
  node [
    id 118
    label "harbinger"
  ]
  node [
    id 119
    label "obwie&#347;ciciel"
  ]
  node [
    id 120
    label "zapowied&#378;"
  ]
  node [
    id 121
    label "declaration"
  ]
  node [
    id 122
    label "reklama"
  ]
  node [
    id 123
    label "film_kr&#243;tkometra&#380;owy"
  ]
  node [
    id 124
    label "ognisko"
  ]
  node [
    id 125
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 126
    label "powalenie"
  ]
  node [
    id 127
    label "odezwanie_si&#281;"
  ]
  node [
    id 128
    label "atakowanie"
  ]
  node [
    id 129
    label "grupa_ryzyka"
  ]
  node [
    id 130
    label "przypadek"
  ]
  node [
    id 131
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 132
    label "inkubacja"
  ]
  node [
    id 133
    label "kryzys"
  ]
  node [
    id 134
    label "powali&#263;"
  ]
  node [
    id 135
    label "remisja"
  ]
  node [
    id 136
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 137
    label "zajmowa&#263;"
  ]
  node [
    id 138
    label "zaburzenie"
  ]
  node [
    id 139
    label "badanie_histopatologiczne"
  ]
  node [
    id 140
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 141
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 142
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 143
    label "odzywanie_si&#281;"
  ]
  node [
    id 144
    label "diagnoza"
  ]
  node [
    id 145
    label "atakowa&#263;"
  ]
  node [
    id 146
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 147
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 148
    label "zajmowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 132
  ]
  edge [
    source 3
    target 133
  ]
  edge [
    source 3
    target 134
  ]
  edge [
    source 3
    target 135
  ]
  edge [
    source 3
    target 136
  ]
  edge [
    source 3
    target 137
  ]
  edge [
    source 3
    target 138
  ]
  edge [
    source 3
    target 139
  ]
  edge [
    source 3
    target 140
  ]
  edge [
    source 3
    target 141
  ]
  edge [
    source 3
    target 142
  ]
  edge [
    source 3
    target 143
  ]
  edge [
    source 3
    target 144
  ]
  edge [
    source 3
    target 145
  ]
  edge [
    source 3
    target 146
  ]
  edge [
    source 3
    target 147
  ]
  edge [
    source 3
    target 148
  ]
]
