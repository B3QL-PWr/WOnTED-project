graph [
  node [
    id 0
    label "przeprasza&#263;"
    origin "text"
  ]
  node [
    id 1
    label "wszyscy"
    origin "text"
  ]
  node [
    id 2
    label "tak"
    origin "text"
  ]
  node [
    id 3
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 4
    label "sk&#322;ada&#263;"
  ]
  node [
    id 5
    label "przekazywa&#263;"
  ]
  node [
    id 6
    label "zbiera&#263;"
  ]
  node [
    id 7
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 8
    label "przywraca&#263;"
  ]
  node [
    id 9
    label "dawa&#263;"
  ]
  node [
    id 10
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 11
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 12
    label "convey"
  ]
  node [
    id 13
    label "publicize"
  ]
  node [
    id 14
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 15
    label "render"
  ]
  node [
    id 16
    label "uk&#322;ada&#263;"
  ]
  node [
    id 17
    label "opracowywa&#263;"
  ]
  node [
    id 18
    label "set"
  ]
  node [
    id 19
    label "oddawa&#263;"
  ]
  node [
    id 20
    label "train"
  ]
  node [
    id 21
    label "zmienia&#263;"
  ]
  node [
    id 22
    label "dzieli&#263;"
  ]
  node [
    id 23
    label "scala&#263;"
  ]
  node [
    id 24
    label "zestaw"
  ]
  node [
    id 25
    label "p&#243;&#378;ny"
  ]
  node [
    id 26
    label "do_p&#243;&#378;na"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
]
