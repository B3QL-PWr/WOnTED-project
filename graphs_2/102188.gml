graph [
  node [
    id 0
    label "nazajutrz"
    origin "text"
  ]
  node [
    id 1
    label "kola"
    origin "text"
  ]
  node [
    id 2
    label "po&#322;udnie"
    origin "text"
  ]
  node [
    id 3
    label "wej&#347;&#263;"
    origin "text"
  ]
  node [
    id 4
    label "izba"
    origin "text"
  ]
  node [
    id 5
    label "jambro&#380;y"
    origin "text"
  ]
  node [
    id 6
    label "jutrzejszy"
  ]
  node [
    id 7
    label "blisko"
  ]
  node [
    id 8
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 9
    label "bliski"
  ]
  node [
    id 10
    label "dok&#322;adnie"
  ]
  node [
    id 11
    label "silnie"
  ]
  node [
    id 12
    label "przysz&#322;y"
  ]
  node [
    id 13
    label "odmienny"
  ]
  node [
    id 14
    label "odpowiedni"
  ]
  node [
    id 15
    label "jutro"
  ]
  node [
    id 16
    label "krzew_kokainowy"
  ]
  node [
    id 17
    label "nap&#243;j_gazowany"
  ]
  node [
    id 18
    label "&#347;lazowate"
  ]
  node [
    id 19
    label "egzotyk"
  ]
  node [
    id 20
    label "zatwarowate"
  ]
  node [
    id 21
    label "drzewo"
  ]
  node [
    id 22
    label "kola_zaostrzona"
  ]
  node [
    id 23
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 24
    label "ro&#347;lina"
  ]
  node [
    id 25
    label "przedmiot"
  ]
  node [
    id 26
    label "ska&#322;a"
  ]
  node [
    id 27
    label "organizm"
  ]
  node [
    id 28
    label "osobliwo&#347;&#263;"
  ]
  node [
    id 29
    label "drewno"
  ]
  node [
    id 30
    label "pier&#347;nica"
  ]
  node [
    id 31
    label "parzelnia"
  ]
  node [
    id 32
    label "zadrzewienie"
  ]
  node [
    id 33
    label "&#380;ywica"
  ]
  node [
    id 34
    label "cecha"
  ]
  node [
    id 35
    label "fanerofit"
  ]
  node [
    id 36
    label "zacios"
  ]
  node [
    id 37
    label "graf"
  ]
  node [
    id 38
    label "las"
  ]
  node [
    id 39
    label "karczowa&#263;"
  ]
  node [
    id 40
    label "wykarczowa&#263;"
  ]
  node [
    id 41
    label "karczowanie"
  ]
  node [
    id 42
    label "surowiec"
  ]
  node [
    id 43
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 44
    label "&#322;yko"
  ]
  node [
    id 45
    label "szpaler"
  ]
  node [
    id 46
    label "chodnik"
  ]
  node [
    id 47
    label "wykarczowanie"
  ]
  node [
    id 48
    label "skupina"
  ]
  node [
    id 49
    label "pie&#324;"
  ]
  node [
    id 50
    label "kora"
  ]
  node [
    id 51
    label "drzewostan"
  ]
  node [
    id 52
    label "brodaczka"
  ]
  node [
    id 53
    label "korona"
  ]
  node [
    id 54
    label "zbiorowisko"
  ]
  node [
    id 55
    label "ro&#347;liny"
  ]
  node [
    id 56
    label "p&#281;d"
  ]
  node [
    id 57
    label "wegetowanie"
  ]
  node [
    id 58
    label "zadziorek"
  ]
  node [
    id 59
    label "organizm_wielokom&#243;rkowy"
  ]
  node [
    id 60
    label "asocjacja_ro&#347;lin"
  ]
  node [
    id 61
    label "do&#322;owa&#263;"
  ]
  node [
    id 62
    label "wegetacja"
  ]
  node [
    id 63
    label "owoc"
  ]
  node [
    id 64
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 65
    label "strzyc"
  ]
  node [
    id 66
    label "w&#322;&#243;kno"
  ]
  node [
    id 67
    label "g&#322;uszenie"
  ]
  node [
    id 68
    label "j&#281;zyczek_li&#347;ciowy"
  ]
  node [
    id 69
    label "fitotron"
  ]
  node [
    id 70
    label "bulwka"
  ]
  node [
    id 71
    label "rozmn&#243;&#380;ka"
  ]
  node [
    id 72
    label "odn&#243;&#380;ka"
  ]
  node [
    id 73
    label "epiderma"
  ]
  node [
    id 74
    label "gumoza"
  ]
  node [
    id 75
    label "strzy&#380;enie"
  ]
  node [
    id 76
    label "wypotnik"
  ]
  node [
    id 77
    label "flawonoid"
  ]
  node [
    id 78
    label "wyro&#347;le"
  ]
  node [
    id 79
    label "do&#322;owanie"
  ]
  node [
    id 80
    label "g&#322;uszy&#263;"
  ]
  node [
    id 81
    label "pora&#380;a&#263;"
  ]
  node [
    id 82
    label "fitocenoza"
  ]
  node [
    id 83
    label "hodowla"
  ]
  node [
    id 84
    label "fotoautotrof"
  ]
  node [
    id 85
    label "nieuleczalnie_chory"
  ]
  node [
    id 86
    label "wegetowa&#263;"
  ]
  node [
    id 87
    label "pochewka"
  ]
  node [
    id 88
    label "sok"
  ]
  node [
    id 89
    label "system_korzeniowy"
  ]
  node [
    id 90
    label "zawi&#261;zek"
  ]
  node [
    id 91
    label "&#347;lazowce"
  ]
  node [
    id 92
    label "&#347;rodek"
  ]
  node [
    id 93
    label "obszar"
  ]
  node [
    id 94
    label "Ziemia"
  ]
  node [
    id 95
    label "dzie&#324;"
  ]
  node [
    id 96
    label "dwunasta"
  ]
  node [
    id 97
    label "strona_&#347;wiata"
  ]
  node [
    id 98
    label "godzina"
  ]
  node [
    id 99
    label "pora"
  ]
  node [
    id 100
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 101
    label "run"
  ]
  node [
    id 102
    label "okres_czasu"
  ]
  node [
    id 103
    label "czas"
  ]
  node [
    id 104
    label "punkt"
  ]
  node [
    id 105
    label "spos&#243;b"
  ]
  node [
    id 106
    label "miejsce"
  ]
  node [
    id 107
    label "abstrakcja"
  ]
  node [
    id 108
    label "chemikalia"
  ]
  node [
    id 109
    label "substancja"
  ]
  node [
    id 110
    label "time"
  ]
  node [
    id 111
    label "doba"
  ]
  node [
    id 112
    label "p&#243;&#322;godzina"
  ]
  node [
    id 113
    label "jednostka_czasu"
  ]
  node [
    id 114
    label "minuta"
  ]
  node [
    id 115
    label "kwadrans"
  ]
  node [
    id 116
    label "Rzym_Zachodni"
  ]
  node [
    id 117
    label "whole"
  ]
  node [
    id 118
    label "ilo&#347;&#263;"
  ]
  node [
    id 119
    label "element"
  ]
  node [
    id 120
    label "Rzym_Wschodni"
  ]
  node [
    id 121
    label "urz&#261;dzenie"
  ]
  node [
    id 122
    label "noc"
  ]
  node [
    id 123
    label "p&#243;&#322;nocek"
  ]
  node [
    id 124
    label "ranek"
  ]
  node [
    id 125
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 126
    label "podwiecz&#243;r"
  ]
  node [
    id 127
    label "przedpo&#322;udnie"
  ]
  node [
    id 128
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 129
    label "long_time"
  ]
  node [
    id 130
    label "wiecz&#243;r"
  ]
  node [
    id 131
    label "t&#322;usty_czwartek"
  ]
  node [
    id 132
    label "popo&#322;udnie"
  ]
  node [
    id 133
    label "walentynki"
  ]
  node [
    id 134
    label "czynienie_si&#281;"
  ]
  node [
    id 135
    label "s&#322;o&#324;ce"
  ]
  node [
    id 136
    label "rano"
  ]
  node [
    id 137
    label "tydzie&#324;"
  ]
  node [
    id 138
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 139
    label "wzej&#347;cie"
  ]
  node [
    id 140
    label "wsta&#263;"
  ]
  node [
    id 141
    label "day"
  ]
  node [
    id 142
    label "termin"
  ]
  node [
    id 143
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 144
    label "wstanie"
  ]
  node [
    id 145
    label "przedwiecz&#243;r"
  ]
  node [
    id 146
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 147
    label "Sylwester"
  ]
  node [
    id 148
    label "Stary_&#346;wiat"
  ]
  node [
    id 149
    label "p&#243;&#322;noc"
  ]
  node [
    id 150
    label "geosfera"
  ]
  node [
    id 151
    label "przyroda"
  ]
  node [
    id 152
    label "rze&#378;ba"
  ]
  node [
    id 153
    label "morze"
  ]
  node [
    id 154
    label "hydrosfera"
  ]
  node [
    id 155
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 156
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 157
    label "geotermia"
  ]
  node [
    id 158
    label "ozonosfera"
  ]
  node [
    id 159
    label "biosfera"
  ]
  node [
    id 160
    label "magnetosfera"
  ]
  node [
    id 161
    label "Nowy_&#346;wiat"
  ]
  node [
    id 162
    label "biegun"
  ]
  node [
    id 163
    label "litosfera"
  ]
  node [
    id 164
    label "mikrokosmos"
  ]
  node [
    id 165
    label "p&#243;&#322;kula"
  ]
  node [
    id 166
    label "barysfera"
  ]
  node [
    id 167
    label "atmosfera"
  ]
  node [
    id 168
    label "geoida"
  ]
  node [
    id 169
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 170
    label "Kosowo"
  ]
  node [
    id 171
    label "Samoa_Ameryka&#324;skie"
  ]
  node [
    id 172
    label "Zab&#322;ocie"
  ]
  node [
    id 173
    label "zach&#243;d"
  ]
  node [
    id 174
    label "Pow&#261;zki"
  ]
  node [
    id 175
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 176
    label "Piotrowo"
  ]
  node [
    id 177
    label "Olszanica"
  ]
  node [
    id 178
    label "zbi&#243;r"
  ]
  node [
    id 179
    label "Ruda_Pabianicka"
  ]
  node [
    id 180
    label "holarktyka"
  ]
  node [
    id 181
    label "Kaw&#281;czyn-Wygoda"
  ]
  node [
    id 182
    label "Ludwin&#243;w"
  ]
  node [
    id 183
    label "Arktyka"
  ]
  node [
    id 184
    label "Bie&#380;an&#243;w"
  ]
  node [
    id 185
    label "Zabu&#380;e"
  ]
  node [
    id 186
    label "antroposfera"
  ]
  node [
    id 187
    label "Neogea"
  ]
  node [
    id 188
    label "terytorium"
  ]
  node [
    id 189
    label "Syberia_Zachodnia"
  ]
  node [
    id 190
    label "Europejski_Obszar_Gospodarczy"
  ]
  node [
    id 191
    label "zakres"
  ]
  node [
    id 192
    label "pas_planetoid"
  ]
  node [
    id 193
    label "Syberia_Wschodnia"
  ]
  node [
    id 194
    label "Antarktyka"
  ]
  node [
    id 195
    label "Rakowice"
  ]
  node [
    id 196
    label "akrecja"
  ]
  node [
    id 197
    label "wymiar"
  ]
  node [
    id 198
    label "&#321;&#281;g"
  ]
  node [
    id 199
    label "Kresy_Zachodnie"
  ]
  node [
    id 200
    label "Syberia_&#346;rodkowa"
  ]
  node [
    id 201
    label "przestrze&#324;"
  ]
  node [
    id 202
    label "wsch&#243;d"
  ]
  node [
    id 203
    label "Notogea"
  ]
  node [
    id 204
    label "sta&#263;_si&#281;"
  ]
  node [
    id 205
    label "move"
  ]
  node [
    id 206
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 207
    label "zaistnie&#263;"
  ]
  node [
    id 208
    label "z&#322;oi&#263;"
  ]
  node [
    id 209
    label "ascend"
  ]
  node [
    id 210
    label "drapn&#261;&#263;_si&#281;"
  ]
  node [
    id 211
    label "przekroczy&#263;"
  ]
  node [
    id 212
    label "nast&#261;pi&#263;"
  ]
  node [
    id 213
    label "zag&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 214
    label "w&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 215
    label "catch"
  ]
  node [
    id 216
    label "intervene"
  ]
  node [
    id 217
    label "get"
  ]
  node [
    id 218
    label "pozna&#263;"
  ]
  node [
    id 219
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 220
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 221
    label "wnikn&#261;&#263;"
  ]
  node [
    id 222
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 223
    label "przenikn&#261;&#263;"
  ]
  node [
    id 224
    label "doj&#347;&#263;"
  ]
  node [
    id 225
    label "zacz&#261;&#263;"
  ]
  node [
    id 226
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 227
    label "wzi&#261;&#263;"
  ]
  node [
    id 228
    label "spotka&#263;"
  ]
  node [
    id 229
    label "submit"
  ]
  node [
    id 230
    label "become"
  ]
  node [
    id 231
    label "supervene"
  ]
  node [
    id 232
    label "dosi&#281;gn&#261;&#263;"
  ]
  node [
    id 233
    label "zaj&#347;&#263;"
  ]
  node [
    id 234
    label "bodziec"
  ]
  node [
    id 235
    label "informacja"
  ]
  node [
    id 236
    label "przesy&#322;ka"
  ]
  node [
    id 237
    label "dodatek"
  ]
  node [
    id 238
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 239
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 240
    label "dop&#322;yn&#261;&#263;"
  ]
  node [
    id 241
    label "heed"
  ]
  node [
    id 242
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 243
    label "spowodowa&#263;"
  ]
  node [
    id 244
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 245
    label "dozna&#263;"
  ]
  node [
    id 246
    label "dokoptowa&#263;"
  ]
  node [
    id 247
    label "postrzega&#263;"
  ]
  node [
    id 248
    label "orgazm"
  ]
  node [
    id 249
    label "dolecie&#263;"
  ]
  node [
    id 250
    label "drive"
  ]
  node [
    id 251
    label "dotrze&#263;"
  ]
  node [
    id 252
    label "uzyska&#263;"
  ]
  node [
    id 253
    label "dop&#322;ata"
  ]
  node [
    id 254
    label "post&#261;pi&#263;"
  ]
  node [
    id 255
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 256
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 257
    label "odj&#261;&#263;"
  ]
  node [
    id 258
    label "zrobi&#263;"
  ]
  node [
    id 259
    label "cause"
  ]
  node [
    id 260
    label "introduce"
  ]
  node [
    id 261
    label "begin"
  ]
  node [
    id 262
    label "do"
  ]
  node [
    id 263
    label "profit"
  ]
  node [
    id 264
    label "score"
  ]
  node [
    id 265
    label "make"
  ]
  node [
    id 266
    label "insert"
  ]
  node [
    id 267
    label "visualize"
  ]
  node [
    id 268
    label "befall"
  ]
  node [
    id 269
    label "go_steady"
  ]
  node [
    id 270
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 271
    label "znale&#378;&#263;"
  ]
  node [
    id 272
    label "ograniczenie"
  ]
  node [
    id 273
    label "przeby&#263;"
  ]
  node [
    id 274
    label "open"
  ]
  node [
    id 275
    label "min&#261;&#263;"
  ]
  node [
    id 276
    label "cut"
  ]
  node [
    id 277
    label "pique"
  ]
  node [
    id 278
    label "penetrate"
  ]
  node [
    id 279
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 280
    label "zaatakowa&#263;"
  ]
  node [
    id 281
    label "nacisn&#261;&#263;"
  ]
  node [
    id 282
    label "gamble"
  ]
  node [
    id 283
    label "appear"
  ]
  node [
    id 284
    label "zrozumie&#263;"
  ]
  node [
    id 285
    label "feel"
  ]
  node [
    id 286
    label "topographic_point"
  ]
  node [
    id 287
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 288
    label "przyswoi&#263;"
  ]
  node [
    id 289
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 290
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 291
    label "teach"
  ]
  node [
    id 292
    label "experience"
  ]
  node [
    id 293
    label "obgada&#263;"
  ]
  node [
    id 294
    label "zer&#380;n&#261;&#263;"
  ]
  node [
    id 295
    label "zatankowa&#263;"
  ]
  node [
    id 296
    label "zbi&#263;"
  ]
  node [
    id 297
    label "wspi&#261;&#263;_si&#281;"
  ]
  node [
    id 298
    label "pokona&#263;"
  ]
  node [
    id 299
    label "przedosta&#263;_si&#281;"
  ]
  node [
    id 300
    label "manipulate"
  ]
  node [
    id 301
    label "z&#322;o&#380;y&#263;_si&#281;"
  ]
  node [
    id 302
    label "erupt"
  ]
  node [
    id 303
    label "absorb"
  ]
  node [
    id 304
    label "nasyci&#263;_si&#281;"
  ]
  node [
    id 305
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 306
    label "infiltrate"
  ]
  node [
    id 307
    label "odziedziczy&#263;"
  ]
  node [
    id 308
    label "ruszy&#263;"
  ]
  node [
    id 309
    label "take"
  ]
  node [
    id 310
    label "skorzysta&#263;"
  ]
  node [
    id 311
    label "uciec"
  ]
  node [
    id 312
    label "receive"
  ]
  node [
    id 313
    label "nakaza&#263;"
  ]
  node [
    id 314
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 315
    label "obskoczy&#263;"
  ]
  node [
    id 316
    label "bra&#263;"
  ]
  node [
    id 317
    label "u&#380;y&#263;"
  ]
  node [
    id 318
    label "wyrucha&#263;"
  ]
  node [
    id 319
    label "World_Health_Organization"
  ]
  node [
    id 320
    label "wyciupcia&#263;"
  ]
  node [
    id 321
    label "wygra&#263;"
  ]
  node [
    id 322
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 323
    label "withdraw"
  ]
  node [
    id 324
    label "wzi&#281;cie"
  ]
  node [
    id 325
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 326
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 327
    label "poczyta&#263;"
  ]
  node [
    id 328
    label "obj&#261;&#263;"
  ]
  node [
    id 329
    label "seize"
  ]
  node [
    id 330
    label "aim"
  ]
  node [
    id 331
    label "chwyci&#263;"
  ]
  node [
    id 332
    label "przyj&#261;&#263;"
  ]
  node [
    id 333
    label "arise"
  ]
  node [
    id 334
    label "uda&#263;_si&#281;"
  ]
  node [
    id 335
    label "otrzyma&#263;"
  ]
  node [
    id 336
    label "poruszy&#263;"
  ]
  node [
    id 337
    label "dosta&#263;"
  ]
  node [
    id 338
    label "NIK"
  ]
  node [
    id 339
    label "parlament"
  ]
  node [
    id 340
    label "urz&#261;d"
  ]
  node [
    id 341
    label "organ"
  ]
  node [
    id 342
    label "pok&#243;j"
  ]
  node [
    id 343
    label "pomieszczenie"
  ]
  node [
    id 344
    label "zwi&#261;zek"
  ]
  node [
    id 345
    label "mir"
  ]
  node [
    id 346
    label "uk&#322;ad"
  ]
  node [
    id 347
    label "pacyfista"
  ]
  node [
    id 348
    label "preliminarium_pokojowe"
  ]
  node [
    id 349
    label "spok&#243;j"
  ]
  node [
    id 350
    label "grupa"
  ]
  node [
    id 351
    label "tkanka"
  ]
  node [
    id 352
    label "jednostka_organizacyjna"
  ]
  node [
    id 353
    label "budowa"
  ]
  node [
    id 354
    label "Rada_Gospodarcza_i_Spo&#322;eczna_ONZ"
  ]
  node [
    id 355
    label "wydawnictwo_periodyczne"
  ]
  node [
    id 356
    label "tw&#243;r"
  ]
  node [
    id 357
    label "organogeneza"
  ]
  node [
    id 358
    label "zesp&#243;&#322;"
  ]
  node [
    id 359
    label "czynno&#347;&#263;_ruchowa"
  ]
  node [
    id 360
    label "struktura_anatomiczna"
  ]
  node [
    id 361
    label "patologia_narz&#261;dowa"
  ]
  node [
    id 362
    label "Europejski_Rzecznik_Praw_Obywatelskich"
  ]
  node [
    id 363
    label "Izba_Konsyliarska"
  ]
  node [
    id 364
    label "Europejski_Komitet_Spo&#322;eczno-Ekonomiczny"
  ]
  node [
    id 365
    label "stomia"
  ]
  node [
    id 366
    label "dekortykacja"
  ]
  node [
    id 367
    label "okolica"
  ]
  node [
    id 368
    label "Komitet_Region&#243;w"
  ]
  node [
    id 369
    label "odwadnia&#263;"
  ]
  node [
    id 370
    label "wi&#261;zanie"
  ]
  node [
    id 371
    label "odwodni&#263;"
  ]
  node [
    id 372
    label "bratnia_dusza"
  ]
  node [
    id 373
    label "powi&#261;zanie"
  ]
  node [
    id 374
    label "zwi&#261;zanie"
  ]
  node [
    id 375
    label "konstytucja"
  ]
  node [
    id 376
    label "organizacja"
  ]
  node [
    id 377
    label "marriage"
  ]
  node [
    id 378
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 379
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 380
    label "zwi&#261;za&#263;"
  ]
  node [
    id 381
    label "odwadnianie"
  ]
  node [
    id 382
    label "odwodnienie"
  ]
  node [
    id 383
    label "marketing_afiliacyjny"
  ]
  node [
    id 384
    label "substancja_chemiczna"
  ]
  node [
    id 385
    label "koligacja"
  ]
  node [
    id 386
    label "bearing"
  ]
  node [
    id 387
    label "lokant"
  ]
  node [
    id 388
    label "azeotrop"
  ]
  node [
    id 389
    label "stanowisko"
  ]
  node [
    id 390
    label "position"
  ]
  node [
    id 391
    label "instytucja"
  ]
  node [
    id 392
    label "siedziba"
  ]
  node [
    id 393
    label "Pa&#324;stwowa_Inspekcja_Sanitarna"
  ]
  node [
    id 394
    label "G&#322;&#243;wny_Urz&#261;d_Statystyczny"
  ]
  node [
    id 395
    label "Europejski_Urz&#261;d_Statystyczny"
  ]
  node [
    id 396
    label "mianowaniec"
  ]
  node [
    id 397
    label "dzia&#322;"
  ]
  node [
    id 398
    label "okienko"
  ]
  node [
    id 399
    label "w&#322;adza"
  ]
  node [
    id 400
    label "amfilada"
  ]
  node [
    id 401
    label "front"
  ]
  node [
    id 402
    label "apartment"
  ]
  node [
    id 403
    label "udost&#281;pnienie"
  ]
  node [
    id 404
    label "pod&#322;oga"
  ]
  node [
    id 405
    label "sklepienie"
  ]
  node [
    id 406
    label "sufit"
  ]
  node [
    id 407
    label "umieszczenie"
  ]
  node [
    id 408
    label "zakamarek"
  ]
  node [
    id 409
    label "europarlament"
  ]
  node [
    id 410
    label "plankton_polityczny"
  ]
  node [
    id 411
    label "grupa_bilateralna"
  ]
  node [
    id 412
    label "ustawodawca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 34
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 1
    target 64
  ]
  edge [
    source 1
    target 65
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 71
  ]
  edge [
    source 1
    target 72
  ]
  edge [
    source 1
    target 73
  ]
  edge [
    source 1
    target 74
  ]
  edge [
    source 1
    target 75
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 2
    target 108
  ]
  edge [
    source 2
    target 109
  ]
  edge [
    source 2
    target 110
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 2
    target 112
  ]
  edge [
    source 2
    target 113
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 2
    target 115
  ]
  edge [
    source 2
    target 116
  ]
  edge [
    source 2
    target 117
  ]
  edge [
    source 2
    target 118
  ]
  edge [
    source 2
    target 119
  ]
  edge [
    source 2
    target 120
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 2
    target 122
  ]
  edge [
    source 2
    target 123
  ]
  edge [
    source 2
    target 124
  ]
  edge [
    source 2
    target 125
  ]
  edge [
    source 2
    target 126
  ]
  edge [
    source 2
    target 127
  ]
  edge [
    source 2
    target 128
  ]
  edge [
    source 2
    target 129
  ]
  edge [
    source 2
    target 130
  ]
  edge [
    source 2
    target 131
  ]
  edge [
    source 2
    target 132
  ]
  edge [
    source 2
    target 133
  ]
  edge [
    source 2
    target 134
  ]
  edge [
    source 2
    target 135
  ]
  edge [
    source 2
    target 136
  ]
  edge [
    source 2
    target 137
  ]
  edge [
    source 2
    target 138
  ]
  edge [
    source 2
    target 139
  ]
  edge [
    source 2
    target 140
  ]
  edge [
    source 2
    target 141
  ]
  edge [
    source 2
    target 142
  ]
  edge [
    source 2
    target 143
  ]
  edge [
    source 2
    target 144
  ]
  edge [
    source 2
    target 145
  ]
  edge [
    source 2
    target 146
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 2
    target 148
  ]
  edge [
    source 2
    target 149
  ]
  edge [
    source 2
    target 150
  ]
  edge [
    source 2
    target 151
  ]
  edge [
    source 2
    target 152
  ]
  edge [
    source 2
    target 153
  ]
  edge [
    source 2
    target 154
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 156
  ]
  edge [
    source 2
    target 157
  ]
  edge [
    source 2
    target 158
  ]
  edge [
    source 2
    target 159
  ]
  edge [
    source 2
    target 160
  ]
  edge [
    source 2
    target 161
  ]
  edge [
    source 2
    target 162
  ]
  edge [
    source 2
    target 163
  ]
  edge [
    source 2
    target 164
  ]
  edge [
    source 2
    target 165
  ]
  edge [
    source 2
    target 166
  ]
  edge [
    source 2
    target 167
  ]
  edge [
    source 2
    target 168
  ]
  edge [
    source 2
    target 169
  ]
  edge [
    source 2
    target 170
  ]
  edge [
    source 2
    target 171
  ]
  edge [
    source 2
    target 172
  ]
  edge [
    source 2
    target 173
  ]
  edge [
    source 2
    target 174
  ]
  edge [
    source 2
    target 175
  ]
  edge [
    source 2
    target 176
  ]
  edge [
    source 2
    target 177
  ]
  edge [
    source 2
    target 178
  ]
  edge [
    source 2
    target 179
  ]
  edge [
    source 2
    target 180
  ]
  edge [
    source 2
    target 181
  ]
  edge [
    source 2
    target 182
  ]
  edge [
    source 2
    target 183
  ]
  edge [
    source 2
    target 184
  ]
  edge [
    source 2
    target 185
  ]
  edge [
    source 2
    target 186
  ]
  edge [
    source 2
    target 187
  ]
  edge [
    source 2
    target 188
  ]
  edge [
    source 2
    target 189
  ]
  edge [
    source 2
    target 190
  ]
  edge [
    source 2
    target 191
  ]
  edge [
    source 2
    target 192
  ]
  edge [
    source 2
    target 193
  ]
  edge [
    source 2
    target 194
  ]
  edge [
    source 2
    target 195
  ]
  edge [
    source 2
    target 196
  ]
  edge [
    source 2
    target 197
  ]
  edge [
    source 2
    target 198
  ]
  edge [
    source 2
    target 199
  ]
  edge [
    source 2
    target 200
  ]
  edge [
    source 2
    target 201
  ]
  edge [
    source 2
    target 202
  ]
  edge [
    source 2
    target 203
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 3
    target 205
  ]
  edge [
    source 3
    target 206
  ]
  edge [
    source 3
    target 207
  ]
  edge [
    source 3
    target 208
  ]
  edge [
    source 3
    target 209
  ]
  edge [
    source 3
    target 210
  ]
  edge [
    source 3
    target 211
  ]
  edge [
    source 3
    target 212
  ]
  edge [
    source 3
    target 213
  ]
  edge [
    source 3
    target 214
  ]
  edge [
    source 3
    target 215
  ]
  edge [
    source 3
    target 216
  ]
  edge [
    source 3
    target 217
  ]
  edge [
    source 3
    target 218
  ]
  edge [
    source 3
    target 219
  ]
  edge [
    source 3
    target 220
  ]
  edge [
    source 3
    target 221
  ]
  edge [
    source 3
    target 222
  ]
  edge [
    source 3
    target 223
  ]
  edge [
    source 3
    target 224
  ]
  edge [
    source 3
    target 225
  ]
  edge [
    source 3
    target 226
  ]
  edge [
    source 3
    target 227
  ]
  edge [
    source 3
    target 228
  ]
  edge [
    source 3
    target 229
  ]
  edge [
    source 3
    target 230
  ]
  edge [
    source 3
    target 231
  ]
  edge [
    source 3
    target 232
  ]
  edge [
    source 3
    target 233
  ]
  edge [
    source 3
    target 234
  ]
  edge [
    source 3
    target 235
  ]
  edge [
    source 3
    target 236
  ]
  edge [
    source 3
    target 237
  ]
  edge [
    source 3
    target 238
  ]
  edge [
    source 3
    target 239
  ]
  edge [
    source 3
    target 240
  ]
  edge [
    source 3
    target 241
  ]
  edge [
    source 3
    target 242
  ]
  edge [
    source 3
    target 243
  ]
  edge [
    source 3
    target 244
  ]
  edge [
    source 3
    target 245
  ]
  edge [
    source 3
    target 246
  ]
  edge [
    source 3
    target 247
  ]
  edge [
    source 3
    target 248
  ]
  edge [
    source 3
    target 249
  ]
  edge [
    source 3
    target 250
  ]
  edge [
    source 3
    target 251
  ]
  edge [
    source 3
    target 252
  ]
  edge [
    source 3
    target 253
  ]
  edge [
    source 3
    target 254
  ]
  edge [
    source 3
    target 255
  ]
  edge [
    source 3
    target 256
  ]
  edge [
    source 3
    target 257
  ]
  edge [
    source 3
    target 258
  ]
  edge [
    source 3
    target 259
  ]
  edge [
    source 3
    target 260
  ]
  edge [
    source 3
    target 261
  ]
  edge [
    source 3
    target 262
  ]
  edge [
    source 3
    target 263
  ]
  edge [
    source 3
    target 264
  ]
  edge [
    source 3
    target 265
  ]
  edge [
    source 3
    target 266
  ]
  edge [
    source 3
    target 267
  ]
  edge [
    source 3
    target 268
  ]
  edge [
    source 3
    target 269
  ]
  edge [
    source 3
    target 270
  ]
  edge [
    source 3
    target 271
  ]
  edge [
    source 3
    target 272
  ]
  edge [
    source 3
    target 273
  ]
  edge [
    source 3
    target 274
  ]
  edge [
    source 3
    target 275
  ]
  edge [
    source 3
    target 276
  ]
  edge [
    source 3
    target 277
  ]
  edge [
    source 3
    target 278
  ]
  edge [
    source 3
    target 279
  ]
  edge [
    source 3
    target 280
  ]
  edge [
    source 3
    target 281
  ]
  edge [
    source 3
    target 282
  ]
  edge [
    source 3
    target 283
  ]
  edge [
    source 3
    target 284
  ]
  edge [
    source 3
    target 285
  ]
  edge [
    source 3
    target 286
  ]
  edge [
    source 3
    target 287
  ]
  edge [
    source 3
    target 288
  ]
  edge [
    source 3
    target 289
  ]
  edge [
    source 3
    target 290
  ]
  edge [
    source 3
    target 291
  ]
  edge [
    source 3
    target 292
  ]
  edge [
    source 3
    target 293
  ]
  edge [
    source 3
    target 294
  ]
  edge [
    source 3
    target 295
  ]
  edge [
    source 3
    target 296
  ]
  edge [
    source 3
    target 297
  ]
  edge [
    source 3
    target 298
  ]
  edge [
    source 3
    target 299
  ]
  edge [
    source 3
    target 300
  ]
  edge [
    source 3
    target 301
  ]
  edge [
    source 3
    target 302
  ]
  edge [
    source 3
    target 303
  ]
  edge [
    source 3
    target 304
  ]
  edge [
    source 3
    target 305
  ]
  edge [
    source 3
    target 306
  ]
  edge [
    source 3
    target 307
  ]
  edge [
    source 3
    target 308
  ]
  edge [
    source 3
    target 309
  ]
  edge [
    source 3
    target 310
  ]
  edge [
    source 3
    target 311
  ]
  edge [
    source 3
    target 312
  ]
  edge [
    source 3
    target 313
  ]
  edge [
    source 3
    target 314
  ]
  edge [
    source 3
    target 315
  ]
  edge [
    source 3
    target 316
  ]
  edge [
    source 3
    target 317
  ]
  edge [
    source 3
    target 318
  ]
  edge [
    source 3
    target 319
  ]
  edge [
    source 3
    target 320
  ]
  edge [
    source 3
    target 321
  ]
  edge [
    source 3
    target 322
  ]
  edge [
    source 3
    target 323
  ]
  edge [
    source 3
    target 324
  ]
  edge [
    source 3
    target 325
  ]
  edge [
    source 3
    target 326
  ]
  edge [
    source 3
    target 327
  ]
  edge [
    source 3
    target 328
  ]
  edge [
    source 3
    target 329
  ]
  edge [
    source 3
    target 330
  ]
  edge [
    source 3
    target 331
  ]
  edge [
    source 3
    target 332
  ]
  edge [
    source 3
    target 333
  ]
  edge [
    source 3
    target 334
  ]
  edge [
    source 3
    target 335
  ]
  edge [
    source 3
    target 336
  ]
  edge [
    source 3
    target 337
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 338
  ]
  edge [
    source 4
    target 339
  ]
  edge [
    source 4
    target 340
  ]
  edge [
    source 4
    target 341
  ]
  edge [
    source 4
    target 342
  ]
  edge [
    source 4
    target 343
  ]
  edge [
    source 4
    target 344
  ]
  edge [
    source 4
    target 345
  ]
  edge [
    source 4
    target 346
  ]
  edge [
    source 4
    target 347
  ]
  edge [
    source 4
    target 348
  ]
  edge [
    source 4
    target 349
  ]
  edge [
    source 4
    target 350
  ]
  edge [
    source 4
    target 351
  ]
  edge [
    source 4
    target 352
  ]
  edge [
    source 4
    target 353
  ]
  edge [
    source 4
    target 354
  ]
  edge [
    source 4
    target 355
  ]
  edge [
    source 4
    target 356
  ]
  edge [
    source 4
    target 357
  ]
  edge [
    source 4
    target 358
  ]
  edge [
    source 4
    target 359
  ]
  edge [
    source 4
    target 360
  ]
  edge [
    source 4
    target 361
  ]
  edge [
    source 4
    target 362
  ]
  edge [
    source 4
    target 363
  ]
  edge [
    source 4
    target 364
  ]
  edge [
    source 4
    target 365
  ]
  edge [
    source 4
    target 366
  ]
  edge [
    source 4
    target 367
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 368
  ]
  edge [
    source 4
    target 369
  ]
  edge [
    source 4
    target 370
  ]
  edge [
    source 4
    target 371
  ]
  edge [
    source 4
    target 372
  ]
  edge [
    source 4
    target 373
  ]
  edge [
    source 4
    target 374
  ]
  edge [
    source 4
    target 375
  ]
  edge [
    source 4
    target 376
  ]
  edge [
    source 4
    target 377
  ]
  edge [
    source 4
    target 239
  ]
  edge [
    source 4
    target 378
  ]
  edge [
    source 4
    target 379
  ]
  edge [
    source 4
    target 380
  ]
  edge [
    source 4
    target 381
  ]
  edge [
    source 4
    target 382
  ]
  edge [
    source 4
    target 383
  ]
  edge [
    source 4
    target 384
  ]
  edge [
    source 4
    target 385
  ]
  edge [
    source 4
    target 386
  ]
  edge [
    source 4
    target 387
  ]
  edge [
    source 4
    target 388
  ]
  edge [
    source 4
    target 389
  ]
  edge [
    source 4
    target 390
  ]
  edge [
    source 4
    target 391
  ]
  edge [
    source 4
    target 392
  ]
  edge [
    source 4
    target 393
  ]
  edge [
    source 4
    target 394
  ]
  edge [
    source 4
    target 395
  ]
  edge [
    source 4
    target 396
  ]
  edge [
    source 4
    target 397
  ]
  edge [
    source 4
    target 398
  ]
  edge [
    source 4
    target 399
  ]
  edge [
    source 4
    target 400
  ]
  edge [
    source 4
    target 401
  ]
  edge [
    source 4
    target 402
  ]
  edge [
    source 4
    target 403
  ]
  edge [
    source 4
    target 404
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 405
  ]
  edge [
    source 4
    target 406
  ]
  edge [
    source 4
    target 407
  ]
  edge [
    source 4
    target 408
  ]
  edge [
    source 4
    target 409
  ]
  edge [
    source 4
    target 410
  ]
  edge [
    source 4
    target 411
  ]
  edge [
    source 4
    target 412
  ]
]
